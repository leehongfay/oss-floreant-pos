package com.floreantpos.update;

public enum Action
{
  MOVE,  DELETE,  EXECUTE;
  
  private Action() {}
}
