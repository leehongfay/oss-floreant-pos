package com.floreantpos.update;

public abstract interface UpdateListener
{
  public abstract void downloadComplete();
  
  public abstract void updateComplete();
}
