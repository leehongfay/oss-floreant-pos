package com.floreantpos.update;

import java.util.ArrayList;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class UpdateXMLParserHandler
  extends DefaultHandler
{
  private String currentelement = "";
  private ArrayList<Information> informations = new ArrayList();
  private Information information = new Information();
  private boolean ininformation = false;
  

  public UpdateXMLParserHandler() {}
  

  public void startElement(String uri, String name, String qName, Attributes atts)
  {
    currentelement = qName;
    ininformation = true;
  }
  
  public void endElement(String namespaceURI, String localName, String qName)
  {
    ininformation = false;
    
    if (qName.equals("information")) {
      informations.add(information);
      information = new Information();
      currentelement = "";
    }
  }
  
  public void characters(char[] ch, int start, int length)
  {
    String value = null;
    if (!currentelement.equals("")) {
      value = String.copyValueOf(ch, start, length).trim();
    }
    
    if (ininformation)
    {
      if (currentelement.equals("action")) {
        information.setAction(value);
      }
      else if (currentelement.equals("file")) {
        information.setFilename(value);
      }
      else if (currentelement.equals("pubDate")) {
        information.setPubDate(value);
      }
      else if (currentelement.equals("pkgver")) {
        information.setPkgver(value);
      }
      else if (currentelement.equals("pkgrel")) {
        information.setPkgrel(Integer.valueOf(value).intValue());
      }
      else if (currentelement.equals("plugin")) {
        information.setPlugin(Boolean.valueOf(value));
      }
      else if (currentelement.equals("changesLogFile")) {
        information.setChangesLogFile(value);
      }
      else if (currentelement.equals("app-name")) {
        information.setAppName(value);
      }
      else if (currentelement.equals("removeOldPlugins")) {
        information.setRemoveOldPlugins(Boolean.valueOf(value).booleanValue());
      }
      currentelement = "";
    }
  }
  
  public ArrayList<Information> getInformations()
  {
    return informations;
  }
}
