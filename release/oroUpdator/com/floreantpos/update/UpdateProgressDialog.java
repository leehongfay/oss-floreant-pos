package com.floreantpos.update;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

public class UpdateProgressDialog extends JDialog implements ProgressObserver
{
  private JProgressBar pbFile = new JProgressBar();
  private JLabel lblProgress;
  private static int v = 0;
  private Component parent;
  private String url;
  private UpdateListener listener;
  private static boolean update;
  
  public UpdateProgressDialog(Component parent, boolean update, UpdateListener lisener, String url) {
    super((JFrame)parent, true);
    this.parent = parent;
    listener = lisener;
    update = update;
    this.url = url;
    setLayout(new BorderLayout());
    setIconImage(((JFrame)parent).getIconImage());
    
    JPanel contentPanel = new JPanel();
    contentPanel.setLayout(new BorderLayout());
    contentPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
    
    pbFile.setValue(0);
    pbFile.setMaximum(100);
    pbFile.setStringPainted(true);
    pbFile.setBorder(BorderFactory.createTitledBorder(""));
    
    JPanel progressPanel = new JPanel(new BorderLayout());
    lblProgress = new JLabel("Updating..");
    progressPanel.add(lblProgress, "North");
    progressPanel.add(pbFile);
    
    contentPanel.add(progressPanel);
    
    JButton btnCancel = new JButton("Cancel");
    JPanel buttonPanel = new JPanel();
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });
    buttonPanel.add(btnCancel);
    contentPanel.add(buttonPanel, "South");
    add(contentPanel);
  }
  
  public void setTitle(String title, String labelText) {
    setTitle(title);
  }
  
  public void setProgressTitle(String progressTitle) {
    lblProgress.setText(progressTitle);
  }
  
  public void setProgressValue(int value) {
    v = value;
    pbFile.setValue(value);
  }
  
  public void setVisible(boolean b)
  {
    if (b)
    {

















      new Thread()
      {
        public void run()
        {
          UpdateProgressDialog.access$002(0);
          if (UpdateProgressDialog.update) {
            if (!UpdateService.update(UpdateProgressDialog.this, UpdateProgressDialog.this, url)) {
              System.exit(0);
            }
            else {
              listener.updateComplete();
            }
          }
          else {
            if (UpdateService.download(UpdateProgressDialog.this, url)) {
              listener.downloadComplete();
            }
            dispose();
          }
        }
      }.start();
    }
    super.setVisible(b);
  }
  
  public void stopProcessing() {}
  
  public void incrementValue()
  {
    v += 1;
    pbFile.setValue(v);
  }
  
  public void progress(int percent, String text)
  {
    pbFile.setValue(percent);
    setProgressTitle(text);
  }
  
  public void progress(int percent)
  {
    pbFile.setValue(percent);
  }
  
  public Component getParentComponent()
  {
    return parent;
  }
}
