package com.floreantpos.update;

import java.awt.Component;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


























public class PosOptionPane
{
  public PosOptionPane() {}
  
  private static void showDialog(Component parent, String message, int messageType, int optionType)
  {
    JOptionPane optionPane = new JOptionPane(message, messageType, optionType);
    Object[] options = optionPane.getComponents();
    for (Object object : options) {
      if ((object instanceof JPanel)) {
        JPanel panel = (JPanel)object;
        Component[] components = panel.getComponents();
        for (Component component : components) {
          if ((component instanceof JButton)) {
            component.setPreferredSize(new Dimension(80, 60));
          }
        }
      }
    }
    
    JDialog dialog = optionPane.createDialog(parent, "");
    dialog.setModal(true);
    dialog.setVisible(true);
  }
  
  public static void showMessage(String message) {
    showDialog(null, message, 1, -1);
  }
  
  public static void showMessage(Component parent, String message) {
    showDialog(parent, message, 1, -1);
  }
  
  public static void showError(String message) {
    showDialog(null, message, 0, -1);
  }
  
  public static void showError(Component parent, String message) {
    showDialog(parent, message, 0, -1);
  }
  
  public static void showError(Component parent, String message, Throwable x) {
    showDialog(parent, message, 0, -1);
  }
  
  public static int showYesNoQuestionDialog(Component parent, String message, String title) {
    return showYesNoQuestionDialog(parent, message, title, null, null);
  }
  
  public static int showYesNoQuestionDialog(Component parent, String message, String title, String yesButtonText, String noButtonText) {
    JOptionPane optionPane = null;
    if ((yesButtonText != null) && (noButtonText != null)) {
      optionPane = new JOptionPane(message, 3, 1, null, new String[] { yesButtonText, noButtonText });

    }
    else
    {
      optionPane = new JOptionPane(message, 3, 0);
    }
    Object[] options = optionPane.getComponents();
    for (Object object : options) {
      if ((object instanceof JPanel)) {
        JPanel panel = (JPanel)object;
        Component[] components = panel.getComponents();
        for (Component component : components) {
          if ((component instanceof JButton)) {
            component.setPreferredSize(new Dimension(getPreferredSizewidth, 60));
          }
        }
      }
    }
    
    JDialog dialog = optionPane.createDialog(parent, title);
    dialog.setVisible(true);
    
    Object selectedValue = optionPane.getValue();
    if (selectedValue == null) {
      return -1;
    }
    if ((selectedValue instanceof String)) {
      return selectedValue.equals(noButtonText) ? 1 : 0;
    }
    return ((Integer)selectedValue).intValue();
  }
}
