package com.floreantpos.update;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;

public class UpdatePromptDialog
  extends JDialog
{
  private boolean canceled;
  private String url;
  private Component parent;
  
  public UpdatePromptDialog(Component parentC, String link, String currentVersion, String appName, String newVersion, int numVersion)
  {
    super((JFrame)parentC, true);
    setTitle("Update");
    url = link;
    parent = parentC;
    setIconImage(((JFrame)parent).getIconImage());
    setLayout(new BorderLayout());
    
    JPanel headerPanel = new JPanel(new MigLayout("fillx,ins 0", "[444px]", ""));
    headerPanel.setBackground(Color.white);
    
    JLabel lblUpdateTitle = new JLabel("Update Available");
    lblUpdateTitle.setFont(new Font(null, 1, 16));
    lblUpdateTitle.setOpaque(false);
    headerPanel.add(lblUpdateTitle, "gapleft 10,growx,span");
    headerPanel.add(new JSeparator(), "growx,span");
    
    add(headerPanel, "North");
    
    JPanel contentPanel = new JPanel(new MigLayout("fillx,ins 10 30 5 5"));
    
    String titleHeader = "An Update for " + appName + " is available:";
    if ((appName == null) || (appName.isEmpty())) {
      titleHeader = "New version available.";
    }
    JLabel lblUpdatemsg = new JLabel(titleHeader);
    lblUpdatemsg.setFont(new Font(null, 0, 14));
    lblUpdatemsg.setOpaque(false);
    contentPanel.add(lblUpdatemsg, "growx,span");
    
    JLabel lblUpdateVersion = new JLabel(appName + " " + newVersion);
    lblUpdateVersion.setFont(new Font(null, 1, 16));
    lblUpdateVersion.setOpaque(false);
    contentPanel.add(lblUpdateVersion, "growx,span,gapbottom 30");
    
    JButton btnViewChanges = new JButton("View Changes");
    btnViewChanges.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        UpdateChangesInfoDialog dialog = new UpdateChangesInfoDialog(parent, url);
        dialog.setSize(500, 600);
        dialog.setLocationRelativeTo(parent);
        dialog.setVisible(true);
        if (dialog.isCanceled()) {}
      }
      
    });
    contentPanel.add(btnViewChanges, "left,h 25!,gapright 10,right");
    
    add(contentPanel);
    
    JPanel buttonPanel = new JPanel(new MigLayout("fillx,ins 10 0 15 0"));
    JLabel lblCurrentVersion = new JLabel("Current Version: " + currentVersion);
    lblCurrentVersion.setFont(new Font(null, 1, 10));
    lblCurrentVersion.setOpaque(false);
    buttonPanel.add(lblCurrentVersion, "left,gapleft 15,wrap");
    buttonPanel.add(new JSeparator(), "growx,span,gapbottom 15");
    JButton btnLaterUpdate = new JButton("Later");
    btnLaterUpdate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        canceled = true;
        dispose();
      }
    });
    buttonPanel.add(btnLaterUpdate, "left,gapleft 15,h 35!,w 80!");
    
    JButton btnDownloadAndInstallNow = new JButton("Download & Update Now >> ");
    btnDownloadAndInstallNow.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        canceled = false;
        dispose();
      }
    });
    buttonPanel.add(btnDownloadAndInstallNow, "right,gapright 15,h 35!");
    add(buttonPanel, "South");
  }
  
  public void setTitle(String title, String labelText) {
    setTitle(title);
  }
  
  public boolean isCanceled() {
    return canceled;
  }
}
