package com.floreantpos.update;

import java.util.ArrayList;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class ChangesXMLParserHandler
  extends DefaultHandler
{
  private String currentelement = "";
  private String attributeValue = "";
  private ArrayList<Version> versions = new ArrayList();
  private Version version = new Version();
  private boolean inversion = false;
  

  public ChangesXMLParserHandler() {}
  

  public void startElement(String uri, String name, String qName, Attributes atts)
  {
    currentelement = qName;
    if (currentelement.equals("version")) {
      attributeValue = atts.getValue("number");
    }
    inversion = true;
  }
  
  public void endElement(String namespaceURI, String localName, String qName)
  {
    inversion = false;
    if (qName.equals("version")) {
      versions.add(version);
      version = new Version();
      currentelement = "";
    }
  }
  
  public void characters(char[] ch, int start, int length)
  {
    String value = null;
    if (!currentelement.equals("")) {
      value = String.copyValueOf(ch, start, length).trim();
    }
    
    if (inversion) {
      if (currentelement.equals("version")) {
        version.setVersionNo(attributeValue);
      }
      else if (currentelement.equals("change")) {
        version.getChanges().add(value);
      }
      currentelement = "";
      attributeValue = "";
    }
  }
  
  public ArrayList<Version> getVersions() {
    return versions;
  }
}
