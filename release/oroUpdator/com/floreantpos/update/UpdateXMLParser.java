package com.floreantpos.update;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class UpdateXMLParser
{
  public UpdateXMLParser() {}
  
  public ArrayList<Information> parse(String filename, Modes mode) throws SAXException, java.io.FileNotFoundException, java.io.IOException, InterruptedException
  {
    XMLReader reader = org.xml.sax.helpers.XMLReaderFactory.createXMLReader();
    UpdateXMLParserHandler handler = new UpdateXMLParserHandler();
    reader.setContentHandler(handler);
    reader.setErrorHandler(handler);
    
    if (mode == Modes.FILE) {
      reader.parse(new InputSource(new java.io.FileReader(new java.io.File(filename))));
    }
    else {
      URL u = new URL(filename);
      URLConnection conn = u.openConnection();
      InputStream in = conn.getInputStream();
      reader.parse(new InputSource(in));
    }
    
    return handler.getInformations();
  }
}
