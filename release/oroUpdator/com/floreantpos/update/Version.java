package com.floreantpos.update;

import java.util.ArrayList;

public class Version { private String versionNo;
  
  public Version() {}
  private ArrayList<String> changes = new ArrayList();
  
  public String getVersionNo() {
    return versionNo;
  }
  
  public void setVersionNo(String versionNo) {
    this.versionNo = versionNo;
  }
  
  public ArrayList<String> getChanges() {
    return changes;
  }
  
  public void setChanges(ArrayList<String> changes) {
    this.changes = changes;
  }
}
