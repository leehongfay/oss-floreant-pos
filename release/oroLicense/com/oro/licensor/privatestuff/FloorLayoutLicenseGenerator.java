package com.oro.licensor.privatestuff;

import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;
import net.nicholaswilliams.java.licensing.License;
import net.nicholaswilliams.java.licensing.License.Builder;
import net.nicholaswilliams.java.licensing.licensor.LicenseCreator;
import net.nicholaswilliams.java.licensing.licensor.LicenseCreatorProperties;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.io.IOUtils;

public class FloorLayoutLicenseGenerator
{
  public FloorLayoutLicenseGenerator() {}
  
  public static void main(String[] args) throws Exception, java.io.IOException
  {
    Options options = new Options();
    OptionGroup optionGroup = new OptionGroup().addOption(new Option("email", true, "Email of customer"));
    optionGroup.setRequired(true);
    options.addOptionGroup(optionGroup);
    
    optionGroup = new OptionGroup().addOption(new Option("uid", true, "UID of user"));
    optionGroup.setRequired(true);
    options.addOptionGroup(optionGroup);
    
    CommandLineParser parser = new BasicParser();
    CommandLine commandLine = parser.parse(options, args);
    
    String uid = commandLine.getOptionValue("uid");
    String email = commandLine.getOptionValue("email");
    
    LicenseCreatorProperties.setPrivateKeyDataProvider(new FloorLayoutPrivateKeyProvider());
    LicenseCreatorProperties.setPrivateKeyPasswordProvider(new com.oro.licensor.FlPasswordProvider());
    
    LicenseCreator.getInstance();
    
    Calendar c = Calendar.getInstance();
    c.set(1, c.get(1) + 1);
    

    License license = new License.Builder().withHolder(email).withProductKey(uid).withIssueDate(new Date().getTime()).withGoodBeforeDate(c.getTimeInMillis()).withIssuer("ORO").withNumberOfLicenses(1).build();
    
    byte[] licenseData = LicenseCreator.getInstance().signAndSerializeLicense(license);
    
    FileOutputStream output = new FileOutputStream("fl-license.lic");
    IOUtils.write(licenseData, output);
    output.close();
    
    System.out.println("License for " + email + " saved in fl-license.lic file");
  }
}
