package com.oro.licensor.privatestuff;

import com.oro.licensor.FlPasswordProvider;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import net.nicholaswilliams.java.licensing.License;
import net.nicholaswilliams.java.licensing.License.Builder;
import net.nicholaswilliams.java.licensing.licensor.LicenseCreator;
import net.nicholaswilliams.java.licensing.licensor.LicenseCreatorProperties;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.time.DateUtils;












public class FifteenDaysLicenseGenerator
{
  public FifteenDaysLicenseGenerator() {}
  
  public static void main(String[] args)
    throws Exception, IOException
  {
    LicenseCreatorProperties.setPrivateKeyDataProvider(new FloorLayoutPrivateKeyProvider());
    LicenseCreatorProperties.setPrivateKeyPasswordProvider(new FlPasswordProvider());
    
    LicenseCreator.getInstance();
    

    Date date = new Date();
    long currentTime = date.getTime();
    long startDate = DateUtils.addDays(date, -1).getTime();
    long endTime = DateUtils.addMonths(date, 1).getTime();
    







    License license = new License.Builder().withHolder("demo").withProductKey("demo").withIssueDate(currentTime).withGoodAfterDate(startDate).withGoodBeforeDate(endTime).withIssuer("ORO").withNumberOfLicenses(1).build();
    
    byte[] licenseData = LicenseCreator.getInstance().signAndSerializeLicense(license);
    
    FileOutputStream output = new FileOutputStream("demo-license.lic");
    IOUtils.write(licenseData, output);
    output.close();
    
    System.out.println("License is saved in demo-license.lic file");
  }
}
