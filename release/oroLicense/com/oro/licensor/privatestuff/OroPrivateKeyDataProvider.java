package com.oro.licensor.privatestuff;

import net.nicholaswilliams.java.licensing.exception.KeyNotFoundException;
import org.apache.commons.codec.DecoderException;

public abstract class OroPrivateKeyDataProvider implements net.nicholaswilliams.java.licensing.encryption.PrivateKeyDataProvider
{
  public OroPrivateKeyDataProvider() {}
  
  public byte[] getEncryptedPrivateKeyData() throws KeyNotFoundException
  {
    try
    {
      return org.apache.commons.codec.binary.Hex.decodeHex(getKey().toCharArray());
    }
    catch (DecoderException e) {
      throw new KeyNotFoundException("Private key not found.");
    }
  }
  
  public abstract String getKey();
}
