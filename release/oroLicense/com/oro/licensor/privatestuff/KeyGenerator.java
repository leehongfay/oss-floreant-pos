package com.oro.licensor.privatestuff;

import sun.security.rsa.RSAKeyPairGenerator;

public class KeyGenerator {
  public KeyGenerator() {}
  
  public static void main(String[] arguments) throws Exception {
    String PUBLIC_KEY_FILE = "public.key";
    String PRIVATE_KEY_FILE = "private.key";
    String SECRET_KEY = "private.key";
    
    RSAKeyPairGenerator generator = new RSAKeyPairGenerator();
    java.security.KeyPair keyPair = generator.generateKeyPair();
  }
}
