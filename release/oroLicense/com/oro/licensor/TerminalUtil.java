package com.oro.licensor;

import com.sun.accessibility.internal.resources.accessibility;
import java.util.UUID;
import java.util.prefs.Preferences;
import org.apache.commons.lang.StringUtils;

public class TerminalUtil
{
  private static final String FLUID = "a$@d55#";
  private static String uid;
  
  static
  {
    Preferences preferences = Preferences.userNodeForPackage(accessibility.class);
    uid = preferences.get("a$@d55#", null);
    
    if (StringUtils.isEmpty(uid)) {
      uid = UUID.randomUUID().toString();
      preferences.put("a$@d55#", uid);
    }
  }
  
  public static String getSystemUID() {
    return uid;
  }
  
  public TerminalUtil() {}
}
