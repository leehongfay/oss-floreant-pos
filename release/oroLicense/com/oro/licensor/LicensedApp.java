package com.oro.licensor;

public enum LicensedApp {
  FLOOR_LAYOUT("Floor Layout", "$$GHUMPARANIMASIPISI%%"), 
  ORO_STOCK("Oro Stock", "$$HIROKRAJARDESHE%%"), 
  ORO_CUST("Oro Cust", "$$MONERORONGERANGABO%%");
  
  private String displayName;
  private String secretKey;
  
  private LicensedApp(String s, String key) {
    displayName = s;
    secretKey = key;
  }
  
  public String getDisplayName() {
    return displayName;
  }
  
  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }
  
  public String toString()
  {
    return getDisplayName();
  }
  
  public String getSecretKey() {
    return secretKey;
  }
  
  public void setSecretKey(String secretKey) {
    this.secretKey = secretKey;
  }
}
