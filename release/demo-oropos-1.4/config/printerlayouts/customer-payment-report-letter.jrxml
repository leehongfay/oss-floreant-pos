<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="CustomerPaymentReport" pageWidth="792" pageHeight="612" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="764" leftMargin="14" rightMargin="14" topMargin="5" bottomMargin="5" isFloatColumnFooter="true">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0000000000000004"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="serverName" class="java.lang.String" isForPrompting="false"/>
	<parameter name="reportDate" class="java.lang.String" isForPrompting="false"/>
	<parameter name="headerLine1" class="java.lang.String" isForPrompting="false"/>
	<parameter name="headerLine2" class="java.lang.String" isForPrompting="false"/>
	<parameter name="headerLine3" class="java.lang.String" isForPrompting="false"/>
	<parameter name="headerLine4" class="java.lang.String" isForPrompting="false"/>
	<parameter name="headerLine5" class="java.lang.String" isForPrompting="false"/>
	<parameter name="receiptType" class="java.lang.String"/>
	<parameter name="startDate" class="java.lang.String" isForPrompting="false"/>
	<parameter name="status" class="java.lang.String"/>
	<parameter name="endDate" class="java.lang.String" isForPrompting="false"/>
	<parameter name="restaurantName" class="java.lang.String"/>
	<parameter name="addressLine1" class="java.lang.String"/>
	<parameter name="addressLine2" class="java.lang.String"/>
	<parameter name="addressLine3" class="java.lang.String"/>
	<parameter name="phone" class="java.lang.String"/>
	<parameter name="reportHeader" class="net.sf.jasperreports.engine.JasperReport"/>
	<parameter name="reportTitle" class="java.lang.String"/>
	<parameter name="totalAmount" class="java.lang.String"/>
	<parameter name="tips" class="java.lang.String"/>
	<field name="customerName" class="java.lang.String"/>
	<field name="customerId" class="java.lang.String"/>
	<field name="ticketNo" class="java.lang.String"/>
	<field name="transNo" class="java.lang.String"/>
	<field name="transTime" class="java.lang.String"/>
	<field name="tips" class="java.lang.Double"/>
	<field name="total" class="java.lang.Double"/>
	<variable name="totalTips" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[$F{tips}]]></variableExpression>
	</variable>
	<variable name="totalAmount" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[$F{total}]]></variableExpression>
	</variable>
	<group name="group">
		<groupExpression><![CDATA[$F{customerName}]]></groupExpression>
		<groupHeader>
			<band height="21">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField" x="0" y="0" width="122" height="20" isRemoveLineWhenBlank="true"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="SansSerif" size="12"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{customerName}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="27">
				<line>
					<reportElement positionType="Float" x="134" y="0" width="629" height="1"/>
					<graphicElement>
						<pen lineStyle="Dashed"/>
					</graphicElement>
				</line>
				<textField pattern="###0.00;-###0.00" isBlankWhenNull="false">
					<reportElement key="textField-10" x="686" y="2" width="77" height="20"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="12"/>
					</textElement>
					<textFieldExpression class="java.lang.Double"><![CDATA[$V{totalAmount}]]></textFieldExpression>
				</textField>
				<textField pattern="###0.00;-###0.00" isBlankWhenNull="false">
					<reportElement key="textField-10" x="602" y="2" width="84" height="20"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="12"/>
					</textElement>
					<textFieldExpression class="java.lang.Double"><![CDATA[$V{totalTips}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="478" y="1" width="122" height="21"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font size="12"/>
					</textElement>
					<text><![CDATA[Total]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="134" splitType="Stretch">
			<textField isBlankWhenNull="false">
				<reportElement key="textField-2" x="-30" y="-227" width="595" height="25"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Monospaced" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{headerLine1}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-3" x="-30" y="-202" width="595" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Monospaced" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{headerLine2}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-4" x="-30" y="-182" width="595" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Monospaced" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{headerLine3}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-5" x="-30" y="-162" width="595" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Monospaced" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{headerLine4}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-6" x="-30" y="-142" width="595" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Monospaced" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{headerLine5}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-7" x="-30" y="-227" width="595" height="25"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Monospaced" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{headerLine1}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-8" x="-30" y="-202" width="595" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Monospaced" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{headerLine2}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-9" x="-30" y="-182" width="595" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Monospaced" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{headerLine3}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-10" x="-30" y="-162" width="595" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Monospaced" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{headerLine4}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-11" x="-30" y="-142" width="595" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Monospaced" size="9" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{headerLine5}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-16" x="0" y="96" width="342" height="17" isRemoveLineWhenBlank="true"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" markup="html">
					<font fontName="SansSerif" size="12" isBold="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{reportDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-16" x="0" y="115" width="146" height="17" isRemoveLineWhenBlank="true"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" markup="html">
					<font fontName="SansSerif" size="12" isBold="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{startDate}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement positionType="Float" x="0" y="133" width="763" height="1"/>
				<graphicElement>
					<pen lineStyle="Dashed"/>
				</graphicElement>
			</line>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-16" x="146" y="115" width="196" height="17" isRemoveLineWhenBlank="true"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle" markup="html">
					<font fontName="SansSerif" size="12" isBold="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{endDate}]]></textFieldExpression>
			</textField>
			<subreport isUsingCache="true">
				<reportElement positionType="Float" stretchType="RelativeToBandHeight" x="122" y="2" width="642" height="28"/>
				<subreportParameter name="restaurantName">
					<subreportParameterExpression><![CDATA[$P{restaurantName}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="addressLine1">
					<subreportParameterExpression><![CDATA[$P{addressLine1}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="addressLine2">
					<subreportParameterExpression><![CDATA[$P{addressLine2}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="addressLine3">
					<subreportParameterExpression><![CDATA[$P{addressLine3}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="phone">
					<subreportParameterExpression><![CDATA[$P{phone}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportExpression class="net.sf.jasperreports.engine.JasperReport"><![CDATA[$P{reportHeader}]]></subreportExpression>
			</subreport>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-1" positionType="Float" mode="Transparent" x="0" y="32" width="764" height="21" backcolor="#FFFFFF"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="14" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{reportTitle}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<columnHeader>
		<band height="25" splitType="Stretch">
			<staticText>
				<reportElement x="122" y="0" width="115" height="20"/>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<text><![CDATA[Customer ID]]></text>
			</staticText>
			<line>
				<reportElement positionType="Float" x="0" y="22" width="764" height="1"/>
				<graphicElement>
					<pen lineStyle="Dashed"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement x="0" y="0" width="122" height="21"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<text><![CDATA[Customer Name]]></text>
			</staticText>
			<staticText>
				<reportElement x="358" y="0" width="120" height="20"/>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<text><![CDATA[Transaction No]]></text>
			</staticText>
			<staticText>
				<reportElement x="237" y="0" width="121" height="20"/>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<text><![CDATA[Ticket No]]></text>
			</staticText>
			<staticText>
				<reportElement x="478" y="0" width="124" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<text><![CDATA[Date]]></text>
			</staticText>
			<textField>
				<reportElement x="686" y="0" width="77" height="20"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{totalAmount}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="602" y="0" width="84" height="20"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{tips}]]></textFieldExpression>
			</textField>
		</band>
	</columnHeader>
	<detail>
		<band height="23" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="122" y="0" width="115" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="SansSerif" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{customerId}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="237" y="0" width="121" height="20" isRemoveLineWhenBlank="true"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font fontName="SansSerif" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{ticketNo}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField" x="686" y="0" width="77" height="20" isRemoveLineWhenBlank="true"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="SansSerif" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.Double"><![CDATA[$F{total}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="358" y="0" width="120" height="20" isRemoveLineWhenBlank="true"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font fontName="SansSerif" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{transNo}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="478" y="0" width="124" height="20" isRemoveLineWhenBlank="true"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{transTime}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField" x="602" y="0" width="84" height="20" isRemoveLineWhenBlank="true"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="SansSerif" size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.Double"><![CDATA[$F{tips}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<lastPageFooter>
		<band splitType="Prevent"/>
	</lastPageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
