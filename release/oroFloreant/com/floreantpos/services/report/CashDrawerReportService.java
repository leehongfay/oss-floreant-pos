package com.floreantpos.services.report;

import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.CashDropTransaction;
import com.floreantpos.model.CashTransaction;
import com.floreantpos.model.CreditCardTransaction;
import com.floreantpos.model.CustomPaymentTransaction;
import com.floreantpos.model.CustomerAccountTransaction;
import com.floreantpos.model.DebitCardTransaction;
import com.floreantpos.model.GiftCertificateTransaction;
import com.floreantpos.model.GratuityPaymentHistory;
import com.floreantpos.model.PayOutTransaction;
import com.floreantpos.model.PaymentType;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.RefundTransaction;
import com.floreantpos.model.TransactionType;
import com.floreantpos.model.dao.GenericDAO;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;


public class CashDrawerReportService
{
  private CashDrawer cashDrawer;
  private Session session;
  
  public CashDrawerReportService(CashDrawer cashDrawer)
  {
    this.cashDrawer = cashDrawer;
  }
  
  public void populateReport() {
    GenericDAO dao = new GenericDAO();
    try {
      session = dao.createNewSession();
      calculateNetSales();
      calculateRefundAmount();
      calculateCashTips();
      calculateChargedTips();
      cashDrawer.setTotalRevenue(Double.valueOf(POSUtil.getRoundedDouble(Double.valueOf(cashDrawer.getNetSales().doubleValue() - cashDrawer.getRefundAmount().doubleValue() + cashDrawer.getSalesTax().doubleValue()))));
      cashDrawer.setGrossReceipts(Double.valueOf(POSUtil.getRoundedDouble(Double.valueOf(cashDrawer.getTotalRevenue().doubleValue() + cashDrawer.getCashTips().doubleValue() + cashDrawer.getChargedTips().doubleValue()))));
      
      calculateCashReceipt();
      calculateCreditReceipt();
      calculateDebitReceipt();
      calculateMemberPayment();
      
      calculateCustomPayment();
      calculateGiftCertReceipts();
      calculateVoidAmount();
      calculateTipsPaid();
      calculateCashPayout();
      calculateCashBack();
      calculateDrawerBleed();
      
      calculateReceiptDiff();
      
      calculateTipsDiff();
      
      calculateDrawerAccountable();
      

      if (session != null) {
        session.close();
      }
    }
    finally
    {
      if (session != null) {
        session.close();
      }
    }
  }
  
  private void calculateMemberPayment() {
    Criteria criteria = getCriteriaForTransaction(CustomerAccountTransaction.class);
    criteria.add(Restrictions.eq(PosTransaction.PROP_PAYMENT_TYPE, PaymentType.CUSTOMER_ACCOUNT.getDisplayString()));
    criteria.add(Restrictions.eq(PosTransaction.PROP_VOIDED, Boolean.FALSE));
    criteria.setProjection(Projections.sum(PosTransaction.PROP_AMOUNT));
    cashDrawer.setCustomerPaymentAmount(Double.valueOf(POSUtil.getRoundedDouble(criteria.uniqueResult())));
  }
  
  private void calculateDrawerAccountable()
  {
    double drawerAccountableAmnt = cashDrawer.getCashReceiptAmount().doubleValue() - cashDrawer.getTipsPaid().doubleValue() - cashDrawer.getPayOutAmount().doubleValue() - cashDrawer.getCashBack().doubleValue() + cashDrawer.getBeginCash().doubleValue() - cashDrawer.getDrawerBleedAmount().doubleValue();
    cashDrawer.setDrawerAccountable(Double.valueOf(POSUtil.getRoundedDouble(Double.valueOf(drawerAccountableAmnt))));
  }
  
  private void calculateTipsDiff() {
    double tipsDiff = cashDrawer.getCashTips().doubleValue() + cashDrawer.getChargedTips().doubleValue() - cashDrawer.getTipsPaid().doubleValue();
    cashDrawer.setTipsDifferential(Double.valueOf(POSUtil.getRoundedDouble(Double.valueOf(tipsDiff))));
  }
  

  private void calculateReceiptDiff()
  {
    double receiptDiff = cashDrawer.getGrossReceipts().doubleValue() - cashDrawer.getCashReceiptAmount().doubleValue() - cashDrawer.getCreditCardReceiptAmount().doubleValue() - cashDrawer.getDebitCardReceiptAmount().doubleValue() - cashDrawer.getCustomerPaymentAmount().doubleValue() - cashDrawer.getCustomPaymentAmount().doubleValue() + cashDrawer.getGiftCertChangeAmount().doubleValue() - cashDrawer.getGiftCertReturnAmount().doubleValue() + cashDrawer.getTotalVoid().doubleValue() + cashDrawer.getRefundAmount().doubleValue();
    cashDrawer.setReceiptDifferential(Double.valueOf(POSUtil.getRoundedDouble(Double.valueOf(receiptDiff))));
  }
  
  private Criteria getCriteriaForTransaction(Class transactionClass) {
    Criteria criteria = session.createCriteria(transactionClass);
    if ((transactionClass.equals(RefundTransaction.class)) || (transactionClass.equals(PayOutTransaction.class))) {
      criteria.add(Restrictions.eq(PosTransaction.PROP_TRANSACTION_TYPE, TransactionType.DEBIT.name()));
    } else
      criteria.add(Restrictions.eq(PosTransaction.PROP_TRANSACTION_TYPE, TransactionType.CREDIT.name()));
    criteria.add(Restrictions.eq(PosTransaction.PROP_CASH_DRAWER_ID, cashDrawer.getId()));
    criteria.add(Restrictions.eq(PosTransaction.PROP_VOIDED, Boolean.FALSE));
    return criteria;
  }
  
  private void calculateNetSales() {
    Criteria criteria = getCriteriaForTransaction(PosTransaction.class);
    
    ProjectionList projectionList = Projections.projectionList();
    projectionList.add(Projections.sum(PosTransaction.PROP_AMOUNT));
    projectionList.add(Projections.sum(PosTransaction.PROP_TAX_AMOUNT));
    projectionList.add(Projections.sum(PosTransaction.PROP_TIPS_AMOUNT));
    criteria.setProjection(projectionList);
    
    List<Object[]> list = criteria.list();
    
    for (Iterator iterator = list.iterator(); iterator.hasNext();) {
      Object[] objects = (Object[])iterator.next();
      if (objects[0] == null) {
        return;
      }
      double salesAmnt = ((Number)objects[0]).doubleValue();
      double taxAmnt = ((Number)objects[1]).doubleValue();
      double tipsAmnt = ((Number)objects[2]).doubleValue();
      
      double netSales = salesAmnt - taxAmnt - tipsAmnt;
      cashDrawer.setNetSales(Double.valueOf(NumberUtil.round(netSales)));
      cashDrawer.setSalesTax(Double.valueOf(NumberUtil.round(taxAmnt)));
    }
  }
  
  private void calculateCashTips() {
    Criteria criteria = getCriteriaForTransaction(CashTransaction.class);
    criteria.add(Restrictions.eq(PosTransaction.PROP_PAYMENT_TYPE, PaymentType.CASH.name()));
    criteria.setProjection(Projections.sum(PosTransaction.PROP_TIPS_AMOUNT));
    cashDrawer.setCashTips(Double.valueOf(POSUtil.getRoundedDouble(criteria.uniqueResult())));
  }
  
  private void calculateChargedTips() {
    Criteria criteria = getCriteriaForTransaction(CreditCardTransaction.class);
    criteria.setProjection(Projections.sum(PosTransaction.PROP_TIPS_AMOUNT));
    cashDrawer.setChargedTips(Double.valueOf(POSUtil.getRoundedDouble(criteria.uniqueResult())));
    
    criteria = getCriteriaForTransaction(DebitCardTransaction.class);
    criteria.setProjection(Projections.sum(PosTransaction.PROP_TIPS_AMOUNT));
    cashDrawer.setChargedTips(Double.valueOf(cashDrawer.getChargedTips().doubleValue() + POSUtil.getRoundedDouble(criteria.uniqueResult())));
    
    criteria = getCriteriaForTransaction(CustomPaymentTransaction.class);
    criteria.setProjection(Projections.sum(PosTransaction.PROP_TIPS_AMOUNT));
    cashDrawer.setChargedTips(Double.valueOf(cashDrawer.getChargedTips().doubleValue() + POSUtil.getRoundedDouble(criteria.uniqueResult())));
    
    criteria = getCriteriaForTransaction(GiftCertificateTransaction.class);
    criteria.setProjection(Projections.sum(PosTransaction.PROP_TIPS_AMOUNT));
    cashDrawer.setChargedTips(Double.valueOf(cashDrawer.getChargedTips().doubleValue() + POSUtil.getRoundedDouble(criteria.uniqueResult())));
  }
  
  private void calculateCashReceipt() {
    Criteria criteria = getCriteriaForTransaction(CashTransaction.class);
    criteria.setProjection(Projections.sum(PosTransaction.PROP_AMOUNT));
    cashDrawer.setCashReceiptAmount(Double.valueOf(POSUtil.getRoundedDouble(criteria.uniqueResult())));
  }
  
  private void calculateCreditReceipt() {
    Criteria criteria = getCriteriaForTransaction(CreditCardTransaction.class);
    criteria.setProjection(Projections.sum(PosTransaction.PROP_AMOUNT));
    
    cashDrawer.setCreditCardReceiptAmount(Double.valueOf(POSUtil.getRoundedDouble(criteria.uniqueResult())));
  }
  
  private void calculateDebitReceipt() {
    Criteria criteria = getCriteriaForTransaction(DebitCardTransaction.class);
    criteria.setProjection(Projections.sum(CashTransaction.PROP_AMOUNT));
    cashDrawer.setDebitCardReceiptAmount(Double.valueOf(POSUtil.getRoundedDouble(criteria.uniqueResult())));
  }
  
  private void calculateCustomPayment() {
    Criteria criteria = getCriteriaForTransaction(CustomPaymentTransaction.class);
    criteria.setProjection(Projections.sum(CustomPaymentTransaction.PROP_AMOUNT));
    cashDrawer.setCustomPaymentAmount(Double.valueOf(POSUtil.getRoundedDouble(criteria.uniqueResult())));
  }
  
  private void calculateGiftCertReceipts() {
    Criteria criteria = getCriteriaForTransaction(GiftCertificateTransaction.class);
    criteria.setProjection(Projections.sum(GiftCertificateTransaction.PROP_AMOUNT));
    cashDrawer.setGiftCertChangeAmount(Double.valueOf(POSUtil.getRoundedDouble(criteria.uniqueResult())));
  }
  
  private void calculateRefundAmount() {
    Criteria criteria = getCriteriaForTransaction(RefundTransaction.class);
    criteria.setProjection(Projections.sum(RefundTransaction.PROP_AMOUNT));
    cashDrawer.setRefundAmount(Double.valueOf(POSUtil.getRoundedDouble(criteria.uniqueResult())));
  }
  
  private void calculateVoidAmount() {
    Criteria criteria = session.createCriteria(PosTransaction.class);
    criteria.add(Restrictions.eq(PosTransaction.PROP_TRANSACTION_TYPE, TransactionType.CREDIT.name()));
    criteria.add(Restrictions.eq(PosTransaction.PROP_CASH_DRAWER_ID, cashDrawer.getId()));
    criteria.add(Restrictions.eq(PosTransaction.PROP_VOIDED, Boolean.TRUE));
    criteria.setProjection(Projections.sum(RefundTransaction.PROP_AMOUNT));
    cashDrawer.setCardVoidAmount(Double.valueOf(POSUtil.getRoundedDouble(criteria.uniqueResult())));
  }
  
  private void calculateTipsPaid() {
    Criteria criteria = session.createCriteria(GratuityPaymentHistory.class);
    criteria.add(Restrictions.eq(GratuityPaymentHistory.PROP_CASH_DRAWER, cashDrawer));
    criteria.setProjection(Projections.sum(GratuityPaymentHistory.PROP_AMOUNT));
    cashDrawer.setTipsPaid(Double.valueOf(POSUtil.getRoundedDouble(criteria.uniqueResult())));
  }
  
  private void calculateCashPayout()
  {
    Criteria criteria = getCriteriaForTransaction(PayOutTransaction.class);
    criteria.setProjection(Projections.sum(PayOutTransaction.PROP_AMOUNT));
    cashDrawer.setPayOutAmount(Double.valueOf(POSUtil.getRoundedDouble(criteria.uniqueResult())));
  }
  
  private void calculateDrawerBleed() {
    Criteria criteria = getCriteriaForTransaction(CashDropTransaction.class);
    criteria.setProjection(Projections.sum(PayOutTransaction.PROP_AMOUNT));
    cashDrawer.setDrawerBleedAmount(Double.valueOf(POSUtil.getRoundedDouble(criteria.uniqueResult())));
  }
  
  private void calculateCashBack() {
    Criteria criteria = getCriteriaForTransaction(RefundTransaction.class);
    criteria.add(Restrictions.eq(PosTransaction.PROP_PAYMENT_TYPE, PaymentType.CASH.name()));
    criteria.setProjection(Projections.sum(RefundTransaction.PROP_AMOUNT));
    cashDrawer.setCashBack(Double.valueOf(POSUtil.getRoundedDouble(criteria.uniqueResult())));
  }
}
