package com.floreantpos.services.report;

import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.model.ActionHistory;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.CashTransaction;
import com.floreantpos.model.CreditCardTransaction;
import com.floreantpos.model.CustomPaymentTransaction;
import com.floreantpos.model.CustomerAccountTransaction;
import com.floreantpos.model.DebitCardTransaction;
import com.floreantpos.model.Discount;
import com.floreantpos.model.GiftCertificateTransaction;
import com.floreantpos.model.Gratuity;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.PayOutTransaction;
import com.floreantpos.model.PaymentType;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.RefundTransaction;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TransactionType;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.DiscountDAO;
import com.floreantpos.model.dao.GenericDAO;
import com.floreantpos.report.JournalReportModel;
import com.floreantpos.report.JournalReportModel.JournalReportData;
import com.floreantpos.report.MenuUsageReport;
import com.floreantpos.report.MenuUsageReport.MenuUsageReportData;
import com.floreantpos.report.SalesBalanceReport;
import com.floreantpos.report.SalesDetailedReport;
import com.floreantpos.report.SalesDetailedReport.DrawerPullData;
import com.floreantpos.report.SalesExceptionReport;
import com.floreantpos.report.ServerProductivityReport;
import com.floreantpos.report.ServerProductivityReport.ServerProductivityReportData;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
















public class ReportService
{
  public ReportService() {}
  
  private static SimpleDateFormat fullDateFormatter = new SimpleDateFormat("MMM dd yyyy, hh:mm a");
  private static SimpleDateFormat shortDateFormatter = new SimpleDateFormat("MMM dd yyyy ");
  
  public static String formatFullDate(Date date) {
    return fullDateFormatter.format(date);
  }
  
  public static String formatShortDate(Date date) {
    return shortDateFormatter.format(date);
  }
  
  public MenuUsageReport getMenuUsageReport(Date fromDate, Date toDate) {
    GenericDAO dao = new GenericDAO();
    MenuUsageReport report = new MenuUsageReport();
    Session session = null;
    
    try
    {
      session = dao.getSession();
      
      Criteria criteria = session.createCriteria(MenuCategory.class);
      List<MenuCategory> categories = criteria.list();
      MenuCategory miscCategory = new MenuCategory();
      miscCategory.setName(POSConstants.MISC_BUTTON_TEXT);
      categories.add(miscCategory);
      
      for (Object localObject1 = categories.iterator(); ((Iterator)localObject1).hasNext();) { MenuCategory category = (MenuCategory)((Iterator)localObject1).next();
        criteria = session.createCriteria(TicketItem.class, "item");
        criteria.createCriteria("ticket", "t");
        ProjectionList projectionList = Projections.projectionList();
        projectionList.add(Projections.sum(TicketItem.PROP_QUANTITY));
        projectionList.add(Projections.sum(TicketItem.PROP_SUBTOTAL_AMOUNT));
        projectionList.add(Projections.sum(TicketItem.PROP_DISCOUNT_AMOUNT));
        criteria.setProjection(projectionList);
        criteria.add(Restrictions.eq("item." + TicketItem.PROP_CATEGORY_ID, category.getId()));
        criteria.add(Restrictions.ge("t." + Ticket.PROP_CREATE_DATE, fromDate));
        criteria.add(Restrictions.le("t." + Ticket.PROP_CREATE_DATE, toDate));
        criteria.add(Restrictions.eq("t." + Ticket.PROP_PAID, Boolean.TRUE));
        List datas = criteria.list();
        if (datas.size() > 0) {
          Object[] objects = (Object[])datas.get(0);
          
          MenuUsageReport.MenuUsageReportData data = new MenuUsageReport.MenuUsageReportData();
          data.setCategoryName(category.getName());
          
          if ((objects.length > 0) && (objects[0] != null)) {
            data.setCount(((Number)objects[0]).intValue());
          }
          if ((objects.length > 1) && (objects[1] != null)) {
            data.setGrossSales(((Number)objects[1]).doubleValue());
          }
          if ((objects.length > 2) && (objects[2] != null)) {
            data.setDiscount(((Number)objects[2]).doubleValue());
          }
          data.calculate();
          report.addReportData(data);
        }
      }
      
      return report;
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public ServerProductivityReport getServerProductivityReport(Date fromDate, Date toDate) {
    GenericDAO dao = new GenericDAO();
    ServerProductivityReport report = new ServerProductivityReport();
    Session session = null;
    
    try
    {
      session = dao.getSession();
      
      Criteria criteria = session.createCriteria(User.class);
      
      List<User> servers = criteria.list();
      
      criteria = session.createCriteria(MenuCategory.class);
      List<MenuCategory> categories = criteria.list();
      MenuCategory miscCategory = new MenuCategory();
      miscCategory.setName(POSConstants.MISC_BUTTON_TEXT);
      categories.add(miscCategory);
      
      for (Object localObject1 = servers.iterator(); ((Iterator)localObject1).hasNext();) { server = (User)((Iterator)localObject1).next();
        data = new ServerProductivityReport.ServerProductivityReportData();
        data.setServerName(server.getId() + "/" + server.toString());
        criteria = session.createCriteria(Ticket.class);
        criteria.add(Restrictions.eq(Ticket.PROP_OWNER_ID, server == null ? null : server.getId()));
        criteria.add(Restrictions.eq(Ticket.PROP_PAID, Boolean.TRUE));
        criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, fromDate));
        criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, toDate));
        
        projectionList = Projections.projectionList();
        projectionList.add(Projections.rowCount());
        projectionList.add(Projections.sum(Ticket.PROP_NUMBER_OF_GUESTS));
        projectionList.add(Projections.sum(TicketItem.PROP_TOTAL_AMOUNT));
        
        criteria.setProjection(projectionList);
        
        Object[] o = (Object[])criteria.uniqueResult();
        totalCheckCount = 0;
        totalServerSale = 0.0D;
        if (o != null) {
          if ((o.length > 0) && (o[0] != null)) {
            int i = ((Number)o[0]).intValue();
            data.setTotalCheckCount(totalCheckCount = i);
          }
          if ((o.length > 1) && (o[1] != null)) {
            i = ((Number)o[1]).intValue();
            data.setTotalGuestCount(i);
          }
          if ((o.length > 2) && (o[2] != null)) {
            totalServerSale = ((Number)o[2]).doubleValue();
            data.setTotalSales(totalServerSale);
          }
        }
        
        data.calculate();
        report.addReportData(data);
        
        for (MenuCategory category : categories) {
          data = new ServerProductivityReport.ServerProductivityReportData();
          data.setServerName(server.getId() + "/" + server.toString());
          
          criteria = session.createCriteria(TicketItem.class, "item");
          criteria.createCriteria(TicketItem.PROP_TICKET, "t");
          
          projectionList = Projections.projectionList();
          criteria.setProjection(projectionList);
          projectionList.add(Projections.sum(TicketItem.PROP_QUANTITY));
          projectionList.add(Projections.sum(TicketItem.PROP_SUBTOTAL_AMOUNT));
          projectionList.add(Projections.sum("t." + Ticket.PROP_DISCOUNT_AMOUNT));
          projectionList.add(Projections.rowCount());
          
          criteria.add(Restrictions.eq("item." + TicketItem.PROP_CATEGORY_NAME, category.getName()));
          criteria.add(Restrictions.ge("t." + Ticket.PROP_CREATE_DATE, fromDate));
          criteria.add(Restrictions.le("t." + Ticket.PROP_CREATE_DATE, toDate));
          criteria.add(Restrictions.eq("t." + Ticket.PROP_OWNER_ID, server == null ? null : server.getId()));
          criteria.add(Restrictions.eq("t." + Ticket.PROP_PAID, Boolean.TRUE));
          
          List datas = criteria.list();
          if (datas.size() > 0) {
            Object[] objects = (Object[])datas.get(0);
            
            data.setCategoryName(category.getName());
            data.setTotalCheckCount(totalCheckCount);
            if ((objects.length > 0) && (objects[0] != null)) {
              int i = ((Number)objects[0]).intValue();
              data.setCheckCount(i);
            }
            
            if ((objects.length > 1) && (objects[1] != null)) {
              double d = ((Number)objects[1]).doubleValue();
              data.setGrossSales(d);
            }
            
            if ((objects.length > 2) && (objects[2] != null)) {
              double d = ((Number)objects[2]).doubleValue();
              data.setSalesDiscount(d);
            }
            if ((data.getGrossSales() != 0.0D) && (totalServerSale != 0.0D)) {
              data.setAllocation(data.getGrossSales() / totalServerSale * 100.0D);
            }
            else {
              data.setAllocation(0.0D);
            }
            
            data.calculate();
            report.addReportData(data); } } }
      User server;
      ServerProductivityReport.ServerProductivityReportData data;
      ProjectionList projectionList;
      int totalCheckCount; double totalServerSale; int i; return report;
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public JournalReportModel getJournalReport(Date fromDate, Date toDate) {
    GenericDAO dao = new GenericDAO();
    JournalReportModel report = new JournalReportModel();
    Session session = null;
    
    report.setFromDate(fromDate);
    report.setToDate(toDate);
    report.setReportTime(new Date());
    try
    {
      session = dao.getSession();
      Criteria criteria = session.createCriteria(ActionHistory.class);
      criteria.add(Restrictions.ge(ActionHistory.PROP_ACTION_TIME, fromDate));
      criteria.add(Restrictions.le(ActionHistory.PROP_ACTION_TIME, toDate));
      List<ActionHistory> list = criteria.list();
      
      for (Object localObject1 = list.iterator(); ((Iterator)localObject1).hasNext();) { ActionHistory history = (ActionHistory)((Iterator)localObject1).next();
        JournalReportModel.JournalReportData data = new JournalReportModel.JournalReportData();
        data.setRefId(history.getId());
        data.setAction(history.getActionName());
        data.setUserInfo(history.getPerformer().getId() + "/" + history.getPerformer());
        data.setTime(history.getActionTime());
        data.setComments(history.getDescription());
        report.addReportData(data);
      }
      
      return report;
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public SalesBalanceReport getSalesBalanceReport(Date fromDate, Date toDate, User user) {
    GenericDAO dao = new GenericDAO();
    SalesBalanceReport report = new SalesBalanceReport();
    Session session = null;
    
    report.setFromDate(fromDate);
    report.setToDate(toDate);
    report.setReportTime(new Date());
    try
    {
      session = dao.getSession();
      

      report.setNetSalesAmount(getNetSales(session, fromDate, toDate, user));
      
      report.setGrossTaxableSalesAmount(calculateGrossSales(session, fromDate, toDate, user, true));
      
      report.setGrossNonTaxableSalesAmount(calculateGrossSales(session, fromDate, toDate, user, false));
      
      report.setDiscountAmount(calculateDiscount(session, fromDate, toDate, user));
      
      report.setSalesTaxAmount(calculateTax(session, fromDate, toDate, user));
      report.setCashTipsAmount(calculateCashTips(session, fromDate, toDate, user));
      report.setChargedTipsAmount(calculateChargedTips(session, fromDate, toDate, user));
      
      report.setCashReceiptsAmount(calculateCashReceipt(session, fromDate, toDate));
      report.setCreditCardReceiptsAmount(calculateCreditReceipt(session, CreditCardTransaction.class, fromDate, toDate, user));
      report.setDebitCardReceiptsAmount(calculateDebitReceipt(session, DebitCardTransaction.class, fromDate, toDate, user));
      report.setMemberPaymentAmount(calculateMemberPayment(session, CustomerAccountTransaction.class, fromDate, toDate, user));
      report.setCustomPaymentAmount(calculateCustomPayment(session, CustomPaymentTransaction.class, fromDate, toDate, user));
      report.setGiftCertReceipts(calculateGiftCertReceipts(session, fromDate, toDate));
      report.setCashBackAmount(calculateRefundAmount(session, RefundTransaction.class, fromDate, toDate));
      report.setCashRefundAmount(calculateCashRefundAmount(session, RefundTransaction.class, fromDate, toDate));
      
























      report.setGrossTipsPaidAmount(calculateTipsPaid(session, fromDate, toDate, user));
      

      report.setCashPayoutAmount(calculateCashPayout(session, fromDate, toDate, user));
      

      calculateDrawerPullAmount(session, report, fromDate, toDate, user);
      
      report.setVisaCreditCardAmount(calculateVisaCreditCardSummery(session, CreditCardTransaction.class, fromDate, toDate, user));
      report.setMasterCardAmount(calculateMasterCardSummery(session, CreditCardTransaction.class, fromDate, toDate, user));
      report.setAmexAmount(calculateAmexSummery(session, CreditCardTransaction.class, fromDate, toDate, user));
      report.setDiscoveryAmount(calculateDiscoverySummery(session, CreditCardTransaction.class, fromDate, toDate, user));
      
      report.calculate();
      return report;
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  private void calculateDrawerPullAmount(Session session, SalesBalanceReport report, Date fromDate, Date toDate, User user) {
    Criteria criteria = session.createCriteria(CashDrawer.class);
    criteria.add(Restrictions.ge(CashDrawer.PROP_REPORT_TIME, fromDate));
    criteria.add(Restrictions.le(CashDrawer.PROP_REPORT_TIME, toDate));
    
    addMultiUserFilter(user, criteria, CashDrawer.PROP_ASSIGNED_USER_ID);
    
    ProjectionList projectionList = Projections.projectionList();
    projectionList.add(Projections.sum(CashDrawer.PROP_DRAWER_ACCOUNTABLE));
    projectionList.add(Projections.sum(CashDrawer.PROP_BEGIN_CASH));
    criteria.setProjection(projectionList);
    
    Object[] o = (Object[])criteria.uniqueResult();
    if ((o.length > 0) && ((o[0] instanceof Number))) {
      double amount = ((Number)o[0]).doubleValue();
      report.setDrawerPullsAmount(amount);
    }
    if ((o.length > 1) && ((o[1] instanceof Number))) {
      double amount = ((Number)o[1]).doubleValue();
      report.setDrawerPullsAmount(report.getDrawerPullsAmount() - amount);
    }
  }
  
  private double calculateCashPayout(Session session, Date fromDate, Date toDate, User user) {
    Criteria criteria = session.createCriteria(PayOutTransaction.class);
    criteria.add(Restrictions.ge(PayOutTransaction.PROP_TRANSACTION_TIME, fromDate));
    criteria.add(Restrictions.le(PayOutTransaction.PROP_TRANSACTION_TIME, toDate));
    
    addMultiUserFilter(user, criteria, PosTransaction.PROP_USER_ID);
    
    criteria.setProjection(Projections.sum(PayOutTransaction.PROP_AMOUNT));
    
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  private double calculateTipsPaid(Session session, Date fromDate, Date toDate, User user) {
    Criteria criteria = session.createCriteria(Ticket.class);
    criteria.createAlias(Ticket.PROP_GRATUITY, "gratuity");
    criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, fromDate));
    criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, toDate));
    
    criteria.add(Restrictions.eq("gratuity." + Gratuity.PROP_PAID, Boolean.TRUE));
    
    addMultiUserFilter(user, criteria, Ticket.PROP_OWNER_ID);
    
    criteria.setProjection(Projections.sum("gratuity." + Gratuity.PROP_AMOUNT));
    
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  private double calculateCreditReceipt(Session session, Class transactionClass, Date fromDate, Date toDate, User user)
  {
    Criteria criteria = session.createCriteria(transactionClass);
    criteria.add(Restrictions.ge(PosTransaction.PROP_TRANSACTION_TIME, fromDate));
    criteria.add(Restrictions.le(PosTransaction.PROP_TRANSACTION_TIME, toDate));
    criteria.add(Restrictions.eq(PosTransaction.PROP_TRANSACTION_TYPE, TransactionType.CREDIT.name()));
    criteria.add(Restrictions.eq(PosTransaction.PROP_VOIDED, Boolean.FALSE));
    addMultiUserFilter(user, criteria, PosTransaction.PROP_USER_ID);
    
    criteria.setProjection(Projections.sum(CreditCardTransaction.PROP_AMOUNT));
    
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  private double calculateRefundAmount(Session session, Class transactionClass, Date fromDate, Date toDate)
  {
    Criteria criteria = session.createCriteria(transactionClass);
    criteria.add(Restrictions.ge(PosTransaction.PROP_TRANSACTION_TIME, fromDate));
    criteria.add(Restrictions.le(PosTransaction.PROP_TRANSACTION_TIME, toDate));
    criteria.add(Restrictions.eq(PosTransaction.PROP_TRANSACTION_TYPE, TransactionType.DEBIT.name()));
    
    criteria.setProjection(Projections.sum(RefundTransaction.PROP_AMOUNT));
    
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  private double calculateCashRefundAmount(Session session, Class transactionClass, Date fromDate, Date toDate)
  {
    Criteria criteria = session.createCriteria(transactionClass);
    criteria.add(Restrictions.ge(PosTransaction.PROP_TRANSACTION_TIME, fromDate));
    criteria.add(Restrictions.le(PosTransaction.PROP_TRANSACTION_TIME, toDate));
    criteria.add(Restrictions.eq(PosTransaction.PROP_PAYMENT_TYPE, PaymentType.CASH.name()));
    criteria.add(Restrictions.eq(PosTransaction.PROP_TRANSACTION_TYPE, TransactionType.DEBIT.name()));
    
    criteria.setProjection(Projections.sum(RefundTransaction.PROP_AMOUNT));
    
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  private double calculateDebitReceipt(Session session, Class transactionClass, Date fromDate, Date toDate, User user)
  {
    Criteria criteria = session.createCriteria(transactionClass);
    criteria.add(Restrictions.ge(PosTransaction.PROP_TRANSACTION_TIME, fromDate));
    criteria.add(Restrictions.le(PosTransaction.PROP_TRANSACTION_TIME, toDate));
    criteria.add(Restrictions.eq(PosTransaction.PROP_TRANSACTION_TYPE, TransactionType.CREDIT.name()));
    addMultiUserFilter(user, criteria, PosTransaction.PROP_USER_ID);
    criteria.setProjection(Projections.sum(CashTransaction.PROP_AMOUNT));
    
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  private double calculateCustomPayment(Session session, Class transactionClass, Date fromDate, Date toDate, User user)
  {
    Criteria criteria = session.createCriteria(transactionClass);
    criteria.add(Restrictions.ge(PosTransaction.PROP_TRANSACTION_TIME, fromDate));
    criteria.add(Restrictions.le(PosTransaction.PROP_TRANSACTION_TIME, toDate));
    criteria.add(Restrictions.eq(PosTransaction.PROP_TRANSACTION_TYPE, TransactionType.CREDIT.name()));
    criteria.add(Restrictions.eq(PosTransaction.PROP_VOIDED, Boolean.FALSE));
    addMultiUserFilter(user, criteria, PosTransaction.PROP_USER_ID);
    
    criteria.setProjection(Projections.sum(CustomPaymentTransaction.PROP_AMOUNT));
    
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  private double calculateCashReceipt(Session session, Date fromDate, Date toDate)
  {
    Criteria criteria = session.createCriteria(CashTransaction.class);
    criteria.add(Restrictions.ge(CashTransaction.PROP_TRANSACTION_TIME, fromDate));
    criteria.add(Restrictions.le(CashTransaction.PROP_TRANSACTION_TIME, toDate));
    criteria.add(Restrictions.eq(CashTransaction.PROP_TRANSACTION_TYPE, TransactionType.CREDIT.name()));
    criteria.add(Restrictions.eq(PosTransaction.PROP_PAYMENT_TYPE, PaymentType.CASH.name()));
    criteria.add(Restrictions.eq(PosTransaction.PROP_VOIDED, Boolean.FALSE));
    criteria.setProjection(Projections.sum(PosTransaction.PROP_AMOUNT));
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  public double calculateGiftCertReceipts(Session session, Date fromDate, Date toDate)
  {
    Criteria criteria = session.createCriteria(GiftCertificateTransaction.class);
    criteria.add(Restrictions.ge(GiftCertificateTransaction.PROP_TRANSACTION_TIME, fromDate));
    criteria.add(Restrictions.le(GiftCertificateTransaction.PROP_TRANSACTION_TIME, toDate));
    criteria.add(Restrictions.eq(GiftCertificateTransaction.PROP_TRANSACTION_TYPE, TransactionType.CREDIT.name()));
    criteria.add(Restrictions.eq(PosTransaction.PROP_VOIDED, Boolean.FALSE));
    criteria.setProjection(Projections.sum(GiftCertificateTransaction.PROP_AMOUNT));
    
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  private double calculateCashTips(Session session, Date fromDate, Date toDate, User user)
  {
    Criteria criteria = session.createCriteria(PosTransaction.class);
    criteria.add(Restrictions.between(PosTransaction.PROP_TRANSACTION_TIME, fromDate, toDate));
    criteria.add(Restrictions.eq(PosTransaction.PROP_PAYMENT_TYPE, PaymentType.CASH.name()));
    criteria.add(Restrictions.eq(PosTransaction.PROP_VOIDED, Boolean.FALSE));
    criteria.setProjection(Projections.sum(PosTransaction.PROP_TIPS_AMOUNT));
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  private double calculateMemberPayment(Session session, Class transactionClass, Date fromDate, Date toDate, User user)
  {
    Criteria criteria = session.createCriteria(transactionClass);
    criteria.add(Restrictions.between(PosTransaction.PROP_TRANSACTION_TIME, fromDate, toDate));
    criteria.add(Restrictions.eq(PosTransaction.PROP_PAYMENT_TYPE, PaymentType.CUSTOMER_ACCOUNT.getDisplayString()));
    criteria.add(Restrictions.eq(PosTransaction.PROP_VOIDED, Boolean.FALSE));
    criteria.setProjection(Projections.sum(PosTransaction.PROP_AMOUNT));
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  private double calculateChargedTips(Session session, Date fromDate, Date toDate, User user)
  {
    Criteria criteria = session.createCriteria(PosTransaction.class);
    criteria.add(Restrictions.between(PosTransaction.PROP_TRANSACTION_TIME, fromDate, toDate));
    criteria.add(Restrictions.ne(PosTransaction.PROP_PAYMENT_TYPE, PaymentType.CASH.name()));
    criteria.add(Restrictions.eq(PosTransaction.PROP_VOIDED, Boolean.FALSE));
    criteria.setProjection(Projections.sum(PosTransaction.PROP_TIPS_AMOUNT));
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  private double calculateDiscount(Session session, Date fromDate, Date toDate, User user)
  {
    Criteria criteria = session.createCriteria(Ticket.class);
    criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, fromDate));
    criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, toDate));
    criteria.add(Restrictions.eq(Ticket.PROP_VOIDED, Boolean.FALSE));
    criteria.add(Restrictions.eq(Ticket.PROP_REFUNDED, Boolean.FALSE));
    
    addMultiUserFilter(user, criteria, Ticket.PROP_OWNER_ID);
    
    criteria.setProjection(Projections.sum(Ticket.PROP_DISCOUNT_AMOUNT));
    
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  private double getDoubleAmount(Object result) {
    if ((result != null) && ((result instanceof Number))) {
      return ((Number)result).doubleValue();
    }
    return 0.0D;
  }
  
  private double calculateTax(Session session, Date fromDate, Date toDate, User user)
  {
    Criteria criteria = session.createCriteria(Ticket.class);
    criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, fromDate));
    criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, toDate));
    criteria.add(Restrictions.eq(Ticket.PROP_VOIDED, Boolean.FALSE));
    

    addMultiUserFilter(user, criteria, Ticket.PROP_OWNER_ID);
    
    criteria.setProjection(Projections.sum(Ticket.PROP_TAX_AMOUNT));
    
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  private double getNetSales(Session session, Date fromDate, Date toDate, User user) {
    Criteria criteria = session.createCriteria(Ticket.class);
    criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, fromDate));
    criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, toDate));
    criteria.add(Restrictions.eq(Ticket.PROP_VOIDED, Boolean.FALSE));
    

    addMultiUserFilter(user, criteria, Ticket.PROP_OWNER_ID);
    criteria.setProjection(Projections.sum(Ticket.PROP_SUBTOTAL_AMOUNT));
    
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  private double calculateGrossSales(Session session, Date fromDate, Date toDate, User user, boolean taxableSales) {
    Criteria criteria = session.createCriteria(Ticket.class);
    criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, fromDate));
    criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, toDate));
    criteria.add(Restrictions.eq(Ticket.PROP_VOIDED, Boolean.FALSE));
    criteria.add(Restrictions.eq(Ticket.PROP_REFUNDED, Boolean.FALSE));
    
    addMultiUserFilter(user, criteria, Ticket.PROP_OWNER_ID);
    
    criteria.add(Restrictions.eq(Ticket.PROP_TAX_EXEMPT, Boolean.valueOf(!taxableSales)));
    
    criteria.setProjection(Projections.sum(Ticket.PROP_SUBTOTAL_AMOUNT));
    
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  public SalesExceptionReport getSalesExceptionReport(Date fromDate, Date toDate) {
    GenericDAO dao = new GenericDAO();
    SalesExceptionReport report = new SalesExceptionReport();
    Session session = null;
    
    report.setFromDate(fromDate);
    report.setToDate(toDate);
    report.setReportTime(new Date());
    try
    {
      session = dao.getSession();
      



      Criteria criteria = session.createCriteria(Ticket.class);
      criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, fromDate));
      criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, toDate));
      criteria.add(Restrictions.eq(Ticket.PROP_VOIDED, Boolean.TRUE));
      
      List list = criteria.list();
      for (Iterator iter = list.iterator(); iter.hasNext();) {
        Ticket ticket = (Ticket)iter.next();
        report.addVoidToVoidData(ticket);
      }
      

      criteria = session.createCriteria(Ticket.class);
      criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, fromDate));
      criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, toDate));
      criteria.add(Restrictions.eq(Ticket.PROP_VOIDED, Boolean.FALSE));
      criteria.add(Restrictions.eq(Ticket.PROP_REFUNDED, Boolean.FALSE));
      
      list = criteria.list();
      for (Iterator iter = list.iterator(); iter.hasNext();) {
        Ticket ticket = (Ticket)iter.next();
        report.addDiscountData(ticket);
      }
      

      DiscountDAO discountDAO = new DiscountDAO();
      List<Discount> availableCoupons = discountDAO.getValidCoupons();
      report.addEmptyDiscounts(availableCoupons);
      
      return report;
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public SalesDetailedReport getSalesDetailedReport(Date fromDate, Date toDate) {
    GenericDAO dao = new GenericDAO();
    SalesDetailedReport report = new SalesDetailedReport();
    Session session = null;
    
    report.setFromDate(fromDate);
    report.setToDate(toDate);
    report.setReportTime(new Date());
    try
    {
      session = dao.getSession();
      
      Criteria criteria = session.createCriteria(CashDrawer.class);
      criteria.add(Restrictions.ge(CashDrawer.PROP_REPORT_TIME, fromDate));
      criteria.add(Restrictions.le(CashDrawer.PROP_REPORT_TIME, toDate));
      List list = criteria.list();
      for (Iterator iter = list.iterator(); iter.hasNext();) {
        CashDrawer cashDrawer = (CashDrawer)iter.next();
        data = new SalesDetailedReport.DrawerPullData();
        data.setDrawerPullId(cashDrawer.getId());
        data.setTicketCount(cashDrawer.getTicketCount().intValue());
        data.setIdealAmount(cashDrawer.getDrawerAccountable().doubleValue());
        data.setActualAmount(cashDrawer.getCashToDeposit().doubleValue());
        data.setVarinceAmount(cashDrawer.getDrawerAccountable().doubleValue() - cashDrawer.getCashToDeposit().doubleValue());
        report.addDrawerPullData(data);
      }
      SalesDetailedReport.DrawerPullData data;
      criteria = session.createCriteria(CreditCardTransaction.class);
      criteria.add(Restrictions.ge(CreditCardTransaction.PROP_TRANSACTION_TIME, fromDate));
      criteria.add(Restrictions.le(CreditCardTransaction.PROP_TRANSACTION_TIME, toDate));
      list = criteria.list();
      
      for (Iterator iter = list.iterator(); iter.hasNext();) {
        CreditCardTransaction t = (CreditCardTransaction)iter.next();
        report.addCreditCardData(t);
      }
      
      criteria = session.createCriteria(DebitCardTransaction.class);
      criteria.add(Restrictions.ge(DebitCardTransaction.PROP_TRANSACTION_TIME, fromDate));
      criteria.add(Restrictions.le(DebitCardTransaction.PROP_TRANSACTION_TIME, toDate));
      list = criteria.list();
      
      for (Iterator iter = list.iterator(); iter.hasNext();) {
        DebitCardTransaction t = (DebitCardTransaction)iter.next();
        report.addCreditCardData(t);
      }
      
      criteria = session.createCriteria(GiftCertificateTransaction.class);
      criteria.add(Restrictions.ge(GiftCertificateTransaction.PROP_TRANSACTION_TIME, fromDate));
      criteria.add(Restrictions.le(GiftCertificateTransaction.PROP_TRANSACTION_TIME, toDate));
      ProjectionList projectionList = Projections.projectionList();
      projectionList.add(Projections.rowCount());
      projectionList.add(Projections.sum(GiftCertificateTransaction.PROP_AMOUNT));
      criteria.setProjection(projectionList);
      Object[] object = (Object[])criteria.uniqueResult();
      if ((object != null) && (object.length > 0) && ((object[0] instanceof Number))) {
        report.setGiftCertReturnCount(((Number)object[0]).intValue());
      }
      if ((object != null) && (object.length > 1) && ((object[1] instanceof Number))) {
        report.setGiftCertReturnAmount(((Number)object[1]).doubleValue());
      }
      
      criteria = session.createCriteria(GiftCertificateTransaction.class);
      criteria.add(Restrictions.ge(GiftCertificateTransaction.PROP_TRANSACTION_TIME, fromDate));
      criteria.add(Restrictions.le(GiftCertificateTransaction.PROP_TRANSACTION_TIME, toDate));
      criteria.add(Restrictions.gt(GiftCertificateTransaction.PROP_GIFT_CERT_CASH_BACK_AMOUNT, Double.valueOf(0.0D)));
      projectionList = Projections.projectionList();
      projectionList.add(Projections.rowCount());
      projectionList.add(Projections.sum(GiftCertificateTransaction.PROP_GIFT_CERT_CASH_BACK_AMOUNT));
      criteria.setProjection(projectionList);
      object = (Object[])criteria.uniqueResult();
      if ((object != null) && (object.length > 0) && ((object[0] instanceof Number))) {
        report.setGiftCertChangeCount(((Number)object[0]).intValue());
      }
      if ((object != null) && (object.length > 1) && ((object[1] instanceof Number))) {
        report.setGiftCertChangeAmount(((Number)object[1]).doubleValue());
      }
      
      criteria = session.createCriteria(Ticket.class);
      criteria.createAlias(Ticket.PROP_GRATUITY, "g");
      criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, fromDate));
      criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, toDate));
      criteria.add(Restrictions.gt("g." + Gratuity.PROP_AMOUNT, Double.valueOf(0.0D)));
      projectionList = Projections.projectionList();
      projectionList.add(Projections.rowCount());
      projectionList.add(Projections.sum("g." + Gratuity.PROP_AMOUNT));
      criteria.setProjection(projectionList);
      object = (Object[])criteria.uniqueResult();
      if ((object != null) && (object.length > 0) && ((object[0] instanceof Number))) {
        report.setTipsCount(((Number)object[0]).intValue());
      }
      if ((object != null) && (object.length > 1) && ((object[1] instanceof Number))) {
        report.setChargedTips(((Number)object[1]).doubleValue());
      }
      
      criteria = session.createCriteria(Ticket.class);
      criteria.createAlias(Ticket.PROP_GRATUITY, "g");
      criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, fromDate));
      criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, toDate));
      criteria.add(Restrictions.gt("g." + Gratuity.PROP_AMOUNT, Double.valueOf(0.0D)));
      criteria.add(Restrictions.gt("g." + Gratuity.PROP_PAID, Boolean.TRUE));
      projectionList = Projections.projectionList();
      projectionList.add(Projections.sum("g." + Gratuity.PROP_AMOUNT));
      criteria.setProjection(projectionList);
      object = (Object[])criteria.uniqueResult();
      if ((object != null) && (object.length > 0) && ((object[0] instanceof Number))) {
        report.setTipsPaid(((Number)object[0]).doubleValue());
      }
      
      return report;
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public SalesDetailedReport getCloudSalesDetailedReport(Date fromDate, Date toDate) {
    GenericDAO dao = new GenericDAO();
    SalesDetailedReport report = new SalesDetailedReport();
    Session session = null;
    
    report.setFromDate(fromDate);
    report.setToDate(toDate);
    report.setReportTime(new Date());
    try
    {
      session = dao.getSession();
      
      Criteria criteria = session.createCriteria(CashDrawer.class);
      criteria.add(Restrictions.or(Restrictions.isNull(CashDrawer.PROP_REPORT_TIME), Restrictions.ge(CashDrawer.PROP_REPORT_TIME, fromDate)));
      criteria.add(Restrictions.or(Restrictions.isNull(CashDrawer.PROP_REPORT_TIME), Restrictions.le(CashDrawer.PROP_REPORT_TIME, toDate)));
      List list = criteria.list();
      for (Iterator iter = list.iterator(); iter.hasNext();) {
        CashDrawer cashDrawer = (CashDrawer)iter.next();
        data = new SalesDetailedReport.DrawerPullData();
        data.setDrawerPullId(cashDrawer.getId());
        data.setTicketCount(cashDrawer.getTicketCount().intValue());
        data.setIdealAmount(cashDrawer.getDrawerAccountable().doubleValue());
        data.setActualAmount(cashDrawer.getCashToDeposit().doubleValue());
        data.setVarinceAmount(cashDrawer.getDrawerAccountable().doubleValue() - cashDrawer.getCashToDeposit().doubleValue());
        report.addDrawerPullData(data);
      }
      SalesDetailedReport.DrawerPullData data;
      criteria = session.createCriteria(CreditCardTransaction.class);
      criteria.add(Restrictions.ge(CreditCardTransaction.PROP_TRANSACTION_TIME, fromDate));
      criteria.add(Restrictions.le(CreditCardTransaction.PROP_TRANSACTION_TIME, toDate));
      list = criteria.list();
      
      for (Iterator iter = list.iterator(); iter.hasNext();) {
        CreditCardTransaction t = (CreditCardTransaction)iter.next();
        report.addCreditCardData(t);
      }
      
      criteria = session.createCriteria(DebitCardTransaction.class);
      criteria.add(Restrictions.ge(DebitCardTransaction.PROP_TRANSACTION_TIME, fromDate));
      criteria.add(Restrictions.le(DebitCardTransaction.PROP_TRANSACTION_TIME, toDate));
      list = criteria.list();
      
      for (Iterator iter = list.iterator(); iter.hasNext();) {
        DebitCardTransaction t = (DebitCardTransaction)iter.next();
        report.addCreditCardData(t);
      }
      
      criteria = session.createCriteria(GiftCertificateTransaction.class);
      criteria.add(Restrictions.ge(GiftCertificateTransaction.PROP_TRANSACTION_TIME, fromDate));
      criteria.add(Restrictions.le(GiftCertificateTransaction.PROP_TRANSACTION_TIME, toDate));
      ProjectionList projectionList = Projections.projectionList();
      projectionList.add(Projections.rowCount());
      projectionList.add(Projections.sum(GiftCertificateTransaction.PROP_AMOUNT));
      criteria.setProjection(projectionList);
      Object[] object = (Object[])criteria.uniqueResult();
      if ((object != null) && (object.length > 0) && ((object[0] instanceof Number))) {
        report.setGiftCertReturnCount(((Number)object[0]).intValue());
      }
      if ((object != null) && (object.length > 1) && ((object[1] instanceof Number))) {
        report.setGiftCertReturnAmount(((Number)object[1]).doubleValue());
      }
      
      criteria = session.createCriteria(GiftCertificateTransaction.class);
      criteria.add(Restrictions.ge(GiftCertificateTransaction.PROP_TRANSACTION_TIME, fromDate));
      criteria.add(Restrictions.le(GiftCertificateTransaction.PROP_TRANSACTION_TIME, toDate));
      criteria.add(Restrictions.gt(GiftCertificateTransaction.PROP_GIFT_CERT_CASH_BACK_AMOUNT, Double.valueOf(0.0D)));
      projectionList = Projections.projectionList();
      projectionList.add(Projections.rowCount());
      projectionList.add(Projections.sum(GiftCertificateTransaction.PROP_GIFT_CERT_CASH_BACK_AMOUNT));
      criteria.setProjection(projectionList);
      object = (Object[])criteria.uniqueResult();
      if ((object != null) && (object.length > 0) && ((object[0] instanceof Number))) {
        report.setGiftCertChangeCount(((Number)object[0]).intValue());
      }
      if ((object != null) && (object.length > 1) && ((object[1] instanceof Number))) {
        report.setGiftCertChangeAmount(((Number)object[1]).doubleValue());
      }
      































      return report;
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  private double calculateVisaCreditCardSummery(Session session, Class transactionClass, Date fromDate, Date toDate, User user)
  {
    Criteria criteria = session.createCriteria(transactionClass);
    criteria.add(Restrictions.ge(PosTransaction.PROP_TRANSACTION_TIME, fromDate));
    criteria.add(Restrictions.le(PosTransaction.PROP_TRANSACTION_TIME, toDate));
    criteria.add(Restrictions.eq(PosTransaction.PROP_TRANSACTION_TYPE, TransactionType.CREDIT.name()));
    criteria.add(Restrictions.eq(PosTransaction.PROP_CARD_TYPE, PaymentType.CREDIT_VISA.getDisplayString()));
    
    addMultiUserFilter(user, criteria, PosTransaction.PROP_USER_ID);
    
    criteria.setProjection(Projections.sum(CashTransaction.PROP_AMOUNT));
    
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  private double calculateMasterCardSummery(Session session, Class transactionClass, Date fromDate, Date toDate, User user)
  {
    Criteria criteria = session.createCriteria(transactionClass);
    criteria.add(Restrictions.ge(PosTransaction.PROP_TRANSACTION_TIME, fromDate));
    criteria.add(Restrictions.le(PosTransaction.PROP_TRANSACTION_TIME, toDate));
    criteria.add(Restrictions.eq(PosTransaction.PROP_TRANSACTION_TYPE, TransactionType.CREDIT.name()));
    criteria.add(Restrictions.or(Restrictions.eq(PosTransaction.PROP_CARD_TYPE, PaymentType.CREDIT_MASTER_CARD.getDisplayString()), 
      Restrictions.eq(PosTransaction.PROP_CARD_TYPE, PaymentType.CREDIT_MASTER_CARD.getDisplayString().replaceAll("\\s+", "_"))));
    
    addMultiUserFilter(user, criteria, PosTransaction.PROP_USER_ID);
    
    criteria.setProjection(Projections.sum(CashTransaction.PROP_AMOUNT));
    
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  private double calculateAmexSummery(Session session, Class transactionClass, Date fromDate, Date toDate, User user)
  {
    Criteria criteria = session.createCriteria(transactionClass);
    criteria.add(Restrictions.ge(PosTransaction.PROP_TRANSACTION_TIME, fromDate));
    criteria.add(Restrictions.le(PosTransaction.PROP_TRANSACTION_TIME, toDate));
    criteria.add(Restrictions.eq(PosTransaction.PROP_TRANSACTION_TYPE, TransactionType.CREDIT.name()));
    criteria.add(Restrictions.or(Restrictions.eq(PosTransaction.PROP_CARD_TYPE, PaymentType.CREDIT_AMEX.getDisplayString()), 
      Restrictions.eq(PosTransaction.PROP_CARD_TYPE, "AMERICAN_EXPRESS")));
    
    addMultiUserFilter(user, criteria, PosTransaction.PROP_USER_ID);
    
    criteria.setProjection(Projections.sum(CashTransaction.PROP_AMOUNT));
    
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  private double calculateDiscoverySummery(Session session, Class transactionClass, Date fromDate, Date toDate, User user)
  {
    Criteria criteria = session.createCriteria(transactionClass);
    criteria.add(Restrictions.ge(PosTransaction.PROP_TRANSACTION_TIME, fromDate));
    criteria.add(Restrictions.le(PosTransaction.PROP_TRANSACTION_TIME, toDate));
    criteria.add(Restrictions.eq(PosTransaction.PROP_TRANSACTION_TYPE, TransactionType.CREDIT.name()));
    criteria.add(Restrictions.eq(PosTransaction.PROP_CARD_TYPE, PaymentType.CREDIT_DISCOVERY.getDisplayString()));
    
    addMultiUserFilter(user, criteria, PosTransaction.PROP_USER_ID);
    
    criteria.setProjection(Projections.sum(CashTransaction.PROP_AMOUNT));
    
    return getDoubleAmount(criteria.uniqueResult());
  }
  
  private void addMultiUserFilter(User user, Criteria criteria, String fieldName) {
    if (user != null) {
      PosLog.info(getClass(), "setting multi user filter for root user '" + user.getFullName() + "', id: " + user.getId());
      Disjunction disjunction = Restrictions.disjunction();
      disjunction.add(Restrictions.eq(fieldName, user.getId()));
      List<User> linkedUsers = user.getLinkedUser();
      if (linkedUsers != null) {
        for (User linkedUser : linkedUsers)
          if (!linkedUser.getId().equals(user.getId()))
          {

            PosLog.info(getClass(), "linked user '" + linkedUser.getFullName() + "', id: " + linkedUser.getId());
            disjunction.add(Restrictions.eq(fieldName, linkedUser.getId()));
          }
      }
      criteria.add(disjunction);
    }
  }
}
