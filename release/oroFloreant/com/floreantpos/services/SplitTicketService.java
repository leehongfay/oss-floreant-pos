package com.floreantpos.services;

import com.floreantpos.model.Customer;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketDiscount;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemDiscount;
import com.floreantpos.model.TicketItemSeat;
import com.floreantpos.util.NumberUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.beanutils.PropertyUtils;


















public class SplitTicketService
{
  public static final String SPLIT_TYPE = "SPLIT_TYPE";
  public static final int SPLIT_EQUALLY = 0;
  public static final int SPLIT_BY_SEAT = 1;
  public static final int SPLIT_MANUALLY = 2;
  
  public SplitTicketService() {}
  
  static List<Double> splitValue(double value, int count)
  {
    List<Double> result = new ArrayList(count);
    
    double runningTotal = 0.0D;
    for (int i = 0; i < count; i++) {
      double remainder = value - runningTotal;
      double d = count - i;
      double share = remainder > 0.0D ? Math.max(NumberUtil.roundToTwoDigit(remainder / d), 0.01D) : 0.0D;
      result.add(Double.valueOf(share));
      runningTotal += share;
    }
    Double double1 = (Double)result.get(count - 1);
    if (runningTotal < value) {
      result.set(count - 1, double1 = Double.valueOf(double1.doubleValue() + (value - runningTotal)));
    }
    return result;
  }
  
  public static List<Ticket> doEquallySplit(Ticket ticket, int splitQuantity) throws Exception {
    Map<Integer, Ticket> listOfSplitTickets = new HashMap();
    listOfSplitTickets.put(Integer.valueOf(1), ticket);
    for (int i = 1; i < splitQuantity; i++) {
      Ticket splitTicket = createNewTicket(ticket, splitQuantity);
      splitTicket.setSplitOrder(i + 1);
      for (TicketItem item : ticket.getTicketItems()) {
        transferTicketItem(item, splitTicket, splitQuantity);
      }
      splitTicket.calculatePrice();
      listOfSplitTickets.put(Integer.valueOf(i + 1), splitTicket);
    }
    updateTicket(ticket, null, 0, splitQuantity);
    return new ArrayList(listOfSplitTickets.values());
  }
  
  public static List<Ticket> doSplitBySeatNumber(Ticket ticket) throws Exception {
    List<Ticket> splitTickets = new ArrayList();
    List<TicketItem> ticketItems = ticket.getTicketItems();
    List<TicketItem> sharedTicketItems = new ArrayList();
    
    Map<Integer, Customer> customerMap = new HashMap();
    Map<Integer, List<TicketItem>> itemMap = new HashMap();
    for (TicketItem ticketItem : ticketItems) {
      TicketItemSeat seat = ticketItem.getSeat();
      Integer seatNumber = seat == null ? ticketItem.getSeatNumber() : seat.getSeatNumber();
      List<TicketItem> ticketItemList = (List)itemMap.get(seatNumber);
      if (seatNumber.intValue() == 0) {
        sharedTicketItems.add(ticketItem);

      }
      else if (ticketItemList == null) {
        ticketItemList = new ArrayList();
        ticketItemList.add(ticketItem);
        if (seat != null)
          customerMap.put(ticketItem.getSeatNumber(), seat.getMember());
        itemMap.put(ticketItem.getSeatNumber(), ticketItemList);
      }
      else {
        ticketItemList.add(ticketItem);
      }
    }
    if (itemMap.size() <= 1) {
      return null;
    }
    int splitNumber = 1;
    
    for (Iterator iterator = itemMap.keySet().iterator(); iterator.hasNext();) {
      Integer seatNumber = (Integer)iterator.next();
      List<TicketItem> splitTicketItems = (List)itemMap.get(seatNumber);
      if ((splitTicketItems != null) && (splitTicketItems.size() == 1) && 
        (((TicketItem)splitTicketItems.get(0)).isTreatAsSeat().booleanValue())) {
        iterator.remove();
      }
    }
    int totalSplitQuantity = 0;
    if (itemMap.keySet().size() > 1) {
      totalSplitQuantity = itemMap.keySet().size();
    }
    Integer firstTicketSeatNumber = (Integer)itemMap.keySet().iterator().next();
    ticket.setTicketItems((List)itemMap.get(firstTicketSeatNumber));
    
    splitTickets.add(ticket);
    for (Iterator iterator = itemMap.keySet().iterator(); iterator.hasNext();) {
      Integer seatNumber = (Integer)iterator.next();
      if (seatNumber.intValue() != firstTicketSeatNumber.intValue())
      {

        List<TicketItem> splitTicketItems = (List)itemMap.get(seatNumber);
        Ticket cloneTicket = cloneTicket(ticket, splitTicketItems, sharedTicketItems, splitNumber, totalSplitQuantity, 1);
        Customer customer = (Customer)customerMap.get(seatNumber);
        if (customer != null)
          cloneTicket.setCustomer(customer);
        splitTickets.add(cloneTicket);
        splitNumber++;
      }
    }
    updateTicket(ticket, sharedTicketItems, splitNumber, totalSplitQuantity, 1);
    Customer customer = (Customer)customerMap.get(firstTicketSeatNumber);
    if (customer != null)
      ticket.setCustomer(customer);
    return splitTickets;
  }
  
  private static Ticket cloneTicket(Ticket ticket, List<TicketItem> ticketItems, List<TicketItem> sharedTicketItems, int splitViewNumber, int totalSplitQuantity, int splitValue)
    throws Exception
  {
    Ticket splitCheck = createNewTicket(ticket, splitValue);
    splitCheck.setSplitOrder(splitViewNumber + 1);
    
    for (TicketItem ticketItem : ticketItems) {
      TicketItem newTicketItem = createNewTicketItem(ticketItem, 1);
      newTicketItem.setTicket(splitCheck);
      splitCheck.addToticketItems(newTicketItem);
    }
    if ((sharedTicketItems != null) && (totalSplitQuantity > 1)) {
      for (TicketItem ticketItem : sharedTicketItems) {
        TicketItem sharedNewItem = createNewTicketItem(ticketItem, totalSplitQuantity);
        sharedNewItem.setFractionalUnit(Boolean.valueOf(true));
        splitCheck.addToticketItems(sharedNewItem);
        sharedNewItem.setTicket(splitCheck);
      }
    }
    splitCheck.calculatePrice();
    return splitCheck;
  }
  
  private static List<TicketDiscount> cloneTicketDiscounts(List<TicketDiscount> ticketDiscounts, int splitQuantity) throws Exception {
    List<TicketDiscount> newTicketDiscounts = new ArrayList();
    if ((ticketDiscounts != null) && (!ticketDiscounts.isEmpty())) {
      for (TicketDiscount ticketDiscount : ticketDiscounts) {
        TicketDiscount newTicketDiscount = new TicketDiscount();
        PropertyUtils.copyProperties(newTicketDiscount, ticketDiscount);
        newTicketDiscount.setId(null);
        if (ticketDiscount.getType().intValue() != 1) {
          newTicketDiscount.setCouponQuantity(Double.valueOf(ticketDiscount.getCouponQuantity().doubleValue() / splitQuantity));
        }
        newTicketDiscounts.add(newTicketDiscount);
      }
    }
    return newTicketDiscounts;
  }
  
  public static void transferTicketItem(TicketItem ticketItem, Ticket toTicket, int splitQuantity) {
    TicketItem newTicketItem = createNewTicketItem(ticketItem, splitQuantity);
    if (Double.isInfinite(splitQuantity)) {
      return;
    }
    if (splitQuantity % 1 != 0) {
      newTicketItem.setFractionalUnit(Boolean.valueOf(true));
    }
    newTicketItem.setMenuItemId(ticketItem.getMenuItemId());
    newTicketItem.setTicket(toTicket);
    toTicket.addToticketItems(newTicketItem);
  }
  
  private static TicketItem createNewTicketItem(TicketItem ticketItem, int splitQuantity) {
    try {
      TicketItem newTicketItem = ticketItem.cloneAsNew();
      newTicketItem.setQuantity(Double.valueOf(ticketItem.getQuantity().doubleValue() / splitQuantity));
      List<TicketItemDiscount> newDiscounts = newTicketItem.getDiscounts();
      newTicketItem.calculatePrice();
      updateTicketItemDiscounts(newDiscounts, splitQuantity);
      return newTicketItem;
    }
    catch (Exception localException) {}
    return ticketItem;
  }
  
  private static Ticket createNewTicket(Ticket ticket, int splitQuantity) throws Exception {
    Ticket splitTicket = new Ticket();
    PropertyUtils.copyProperties(splitTicket, ticket);
    splitTicket.setProperties(null);
    splitTicket.setTransactions(null);
    splitTicket.setId(null);
    splitTicket.setTokenNo(null);
    splitTicket.setShortId(null);
    splitTicket.setTicketItems(null);
    splitTicket.setDiscounts(cloneTicketDiscounts(ticket.getDiscounts(), splitQuantity));
    List<Integer> tableNumbers = new ArrayList();
    List<Integer> ticketTableNumbers = ticket.getTableNumbers();
    if (ticketTableNumbers != null)
      tableNumbers.addAll(ticketTableNumbers);
    splitTicket.setTableNumbers(tableNumbers);
    return splitTicket;
  }
  
  private static void updateTicket(Ticket ticket, List<TicketItem> sharedTicketItems, int splitViewNumber, int totalSplitQuantity) throws Exception {
    updateTicket(ticket, sharedTicketItems, splitViewNumber, totalSplitQuantity, totalSplitQuantity);
  }
  
  private static void updateTicket(Ticket ticket, List<TicketItem> sharedTicketItems, int splitViewNumber, int totalSplitQuantity, int splitValue) throws Exception
  {
    ticket.setSplitOrder(splitViewNumber + 1);
    updateTicketItems(ticket.getTicketItems(), splitValue);
    Iterator iterator; if ((sharedTicketItems != null) && (totalSplitQuantity > 1)) {
      List<TicketItem> sharedNewItemList = new ArrayList();
      for (iterator = sharedTicketItems.iterator(); iterator.hasNext();) {
        TicketItem ticketItem = (TicketItem)iterator.next();
        TicketItem sharedNewItem = createNewTicketItem(ticketItem, totalSplitQuantity);
        sharedNewItem.setFractionalUnit(Boolean.valueOf(true));
        sharedNewItemList.add(sharedNewItem);
        sharedNewItem.setTicket(ticket);
      }
      for (TicketItem sharedNewItem : sharedNewItemList) {
        ticket.addToticketItems(sharedNewItem);
      }
    }
    updateTicketDiscounts(ticket.getDiscounts(), totalSplitQuantity);
    ticket.calculatePrice();
  }
  
  private static void updateTicketItems(List<TicketItem> ticketItems, int totalSplitQuantity) throws Exception {
    for (TicketItem ticketItem : ticketItems) {
      ticketItem.setQuantity(Double.valueOf(ticketItem.getQuantity().doubleValue() / totalSplitQuantity));
      ticketItem.setFractionalUnit(Boolean.valueOf(true));
      ticketItem.calculatePrice();
      updateTicketItemDiscounts(ticketItem.getDiscounts(), totalSplitQuantity);
    }
  }
  
  private static void updateTicketDiscounts(List<TicketDiscount> ticketDiscounts, int splitQuantity) throws Exception {
    if ((ticketDiscounts != null) && (!ticketDiscounts.isEmpty())) {
      for (TicketDiscount ticketDiscount : ticketDiscounts) {
        if (ticketDiscount.getType().intValue() != 1) {
          ticketDiscount.setCouponQuantity(Double.valueOf(ticketDiscount.getCouponQuantity().doubleValue() / splitQuantity));
        }
      }
    }
  }
  
  private static void updateTicketItemDiscounts(List<TicketItemDiscount> ticketItemDiscounts, int splitQuantity) throws Exception {
    if ((ticketItemDiscounts != null) && (!ticketItemDiscounts.isEmpty())) {
      for (TicketItemDiscount discount : ticketItemDiscounts) {
        if (discount.getType().intValue() != 1) {
          discount.setCouponQuantity(Double.valueOf(discount.getCouponQuantity().doubleValue() / splitQuantity));
        }
        discount.calculateDiscount(discount.getTicketItem().getSubtotalAmount().doubleValue());
      }
    }
  }
}
