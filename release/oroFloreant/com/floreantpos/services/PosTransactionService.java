package com.floreantpos.services;

import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.config.CardConfig;
import com.floreantpos.config.GiftCardConfig;
import com.floreantpos.extension.GiftCardPaymentPlugin;
import com.floreantpos.extension.PaymentGatewayPlugin;
import com.floreantpos.main.Application;
import com.floreantpos.model.ActionHistory;
import com.floreantpos.model.CreditCardTransaction;
import com.floreantpos.model.Customer;
import com.floreantpos.model.CustomerAccountTransaction;
import com.floreantpos.model.GiftCard;
import com.floreantpos.model.Gratuity;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PaymentType;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.RefundTransaction;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TransactionType;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.ActionHistoryDAO;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.dao.GenericDAO;
import com.floreantpos.model.dao.GiftCardDAO;
import com.floreantpos.model.dao.StoreDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.payment.CardProcessor;
import com.floreantpos.ui.views.payment.GiftCardProcessor;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;















public class PosTransactionService
{
  public PosTransactionService() {}
  
  private static PosTransactionService paymentService = new PosTransactionService();
  
  public void settleTicket(Ticket ticket, PosTransaction transaction, User currentUser) throws Exception {
    settleTicket(ticket, transaction, currentUser, null);
  }
  
  public void settleTicket(Ticket ticket, PosTransaction transaction, User currentUser, PostPaymentProcessor postPaymentService) throws Exception {
    if (currentUser == null) {
      currentUser = Application.getCurrentUser();
    }
    Terminal terminal = DataProvider.get().getCurrentTerminal();
    
    Session session = null;
    Transaction tx = null;
    try
    {
      session = TerminalDAO.getInstance().createNewSession();
      tx = session.beginTransaction();
      
      Date currentDate = StoreDAO.geServerTimestamp();
      
      if (terminal.isEnableMultiCurrency().booleanValue()) {
        session.saveOrUpdate(currentUser.getActiveDrawerPullReport());
      }
      
      if ((transaction instanceof CustomerAccountTransaction)) {
        if (StringUtils.isEmpty(ticket.getCustomerId())) {
          throw new PosException("Cannot pay from customer balance. Customer ID is not set");
        }
        
        Customer customer = CustomerDAO.getInstance().findById(ticket.getCustomerId());
        customer.setBalance(Double.valueOf(customer.getBalance().doubleValue() - transaction.getTenderAmount().doubleValue()));
        CustomerDAO.getInstance().saveOrUpdate(customer, session);
      }
      

      ticket.setVoided(Boolean.valueOf(false));
      ticket.setTerminal(terminal);
      ticket.setPaidAmount(Double.valueOf(ticket.getPaidAmount().doubleValue() + transaction.getAmount().doubleValue()));
      

      if (ticket.isSourceOnline()) {
        ticket.setDueAmount(Double.valueOf(ticket.getTotalAmountWithTips().doubleValue() - ticket.getPaidAmount().doubleValue()));
      }
      else {
        ticket.calculatePrice();
      }
      
      if (NumberUtil.roundToOneDigit(ticket.getDueAmount().doubleValue()) == 0.0D) {
        ticket.setPaid(Boolean.valueOf(true));
        closeTicketIfApplicable(ticket, currentDate);
      }
      else {
        ticket.setPaid(Boolean.valueOf(false));
        ticket.setClosed(Boolean.valueOf(false));
      }
      
      transaction.setTransactionType(TransactionType.CREDIT.name());
      transaction.setPaymentType(transaction.getPaymentType());
      transaction.setTerminal(terminal);
      transaction.setUser(currentUser);
      transaction.setCashDrawer(currentUser.getActiveDrawerPullReport());
      transaction.setTransactionTime(currentDate);
      
      ticket.setCashier(currentUser);
      if (transaction.getAmount().doubleValue() > 0.0D) {
        ticket.addTotransactions(transaction);
      }
      adjustGratuityIfNeeded(ticket, transaction);
      transaction.calculateTaxAmount();
      
      if ((ticket.getOrderType() != null) && (ticket.getOrderType().getName() == "BAR_TAB")) {
        ticket.removeProperty("payment_method");
        ticket.removeProperty("card_name");
        ticket.removeProperty("card_transaction_id");
        ticket.removeProperty("card_tracks");
        ticket.removeProperty("card_reader");
        ticket.removeProperty("advance_payment");
        ticket.removeProperty("card_number");
        ticket.removeProperty("card_exp_year");
        ticket.removeProperty("card_exp_month");
        ticket.removeProperty("card_auth_code");
      }
      
      TicketDAO.getInstance().saveOrUpdate(ticket, session);
      if (postPaymentService != null) {
        postPaymentService.paymentDone(transaction, session);
      }
      
      String giftCardNumber = ticket.getProperty("cardNumber");
      if (giftCardNumber != null) {
        GiftCard giftCard = GiftCardDAO.getInstance().findByCardNumber(giftCardNumber);
        
        transaction.setTransactionType(TransactionType.DEBIT.name());
        transaction.setGiftCertNumber(giftCardNumber);
        Double totalAmount = ticket.getTotalAmountWithTips();
        giftCard.setBalance(Double.valueOf(giftCard.getBalance().doubleValue() + totalAmount.doubleValue()));
        GiftCardDAO.getInstance().saveOrUpdate(giftCard, session);
      }
      tx.commit();
    } catch (Exception e) {
      try {
        tx.rollback();
      } catch (Exception x) {
        PosLog.error(PosTransactionService.class, x);
        x.printStackTrace();
      }
      throw e;
    } finally {
      TerminalDAO.getInstance().closeSession(session);
    }
    

    String actionMessage = POSConstants.RECEIPT_REPORT_TICKET_NO_LABEL + ":" + ticket.getId();
    actionMessage = actionMessage + ";" + POSConstants.TOTAL + ":" + NumberUtil.formatNumber(ticket.getTotalAmountWithTips());
    ActionHistoryDAO.getInstance().saveHistory(Application.getCurrentUser(), ActionHistory.SETTLE_CHECK, actionMessage);
  }
  
  public void settleBarTabTicket(Ticket ticket, PosTransaction transaction, boolean closed, User currentUser) throws Exception {
    Application application = Application.getInstance();
    Terminal terminal = application.refreshAndGetTerminal();
    
    Session session = null;
    Transaction tx = null;
    
    GenericDAO dao = new GenericDAO();
    try
    {
      Date currentDate = StoreDAO.geServerTimestamp();
      
      session = dao.createNewSession();
      tx = session.beginTransaction();
      
      ticket.setVoided(Boolean.valueOf(false));
      ticket.setTerminal(terminal);
      ticket.setPaidAmount(Double.valueOf(ticket.getPaidAmount().doubleValue() + transaction.getAmount().doubleValue()));
      
      ticket.calculatePrice();
      
      if (closed) {
        ticket.setPaid(Boolean.valueOf(true));
        closeTicketIfApplicable(ticket, currentDate);
      }
      else {
        ticket.setPaid(Boolean.valueOf(false));
        ticket.setClosed(Boolean.valueOf(false));
      }
      
      transaction.setTransactionType(TransactionType.CREDIT.name());
      transaction.setPaymentType(transaction.getPaymentType());
      transaction.setTerminal(terminal);
      transaction.setUser(currentUser);
      transaction.setCashDrawer(currentUser.getActiveDrawerPullReport());
      transaction.setTransactionTime(currentDate);
      
      ticket.setCashier(currentUser);
      ticket.addTotransactions(transaction);
      
      TicketDAO.getInstance().saveOrUpdate(ticket, session);
      
      tx.commit();
    } catch (Exception e) {
      try {
        tx.rollback();
      }
      catch (Exception localException1) {}
      throw e;
    } finally {
      dao.closeSession(session);
    }
    
    String actionMessage = POSConstants.RECEIPT_REPORT_TICKET_NO_LABEL + ":" + ticket.getId();
    actionMessage = actionMessage + ";" + POSConstants.TOTAL + ":" + NumberUtil.formatNumber(ticket.getTotalAmountWithTips());
    ActionHistoryDAO.getInstance().saveHistory(Application.getCurrentUser(), ActionHistory.SETTLE_CHECK, actionMessage);
  }
  
  private void closeTicketIfApplicable(Ticket ticket, Date currentDate) {
    OrderType ticketType = ticket.getOrderType();
    
    if ((ticketType.isCloseOnPaid().booleanValue()) || (ticketType.isBarTab().booleanValue())) {
      ticket.setClosed(Boolean.valueOf(true));
      ticket.setClosingDate(currentDate);
    }
  }
  
  private void adjustGratuityIfNeeded(Ticket ticket, PosTransaction transaction) {
    double gratuityAmount = ticket.getGratuityAmount();
    if (gratuityAmount <= 0.0D) {
      return;
    }
    double gratuityPaidAmount = 0.0D;
    
    Set<PosTransaction> transactions = ticket.getTransactions();
    if ((transactions != null) && (transactions.size() > 0)) {
      for (PosTransaction posTransaction : transactions) {
        if ((!(posTransaction instanceof RefundTransaction)) && (!posTransaction.isVoided().booleanValue()))
        {
          gratuityPaidAmount += posTransaction.getTipsAmount().doubleValue(); }
      }
    }
    double gratuityDueAmount = gratuityAmount - gratuityPaidAmount;
    double payableTipsAmount = gratuityDueAmount + ticket.getPaidAmount().doubleValue() - ticket.getTotalAmountWithTips().doubleValue();
    
    if (gratuityDueAmount > 0.0D) {
      if (ticket.getDueAmount().doubleValue() == 0.0D) {
        transaction.setTipsAmount(Double.valueOf(gratuityDueAmount));
      }
      else if (payableTipsAmount > 0.0D) {
        if (payableTipsAmount > transaction.getAmount().doubleValue()) {
          transaction.setTipsAmount(transaction.getAmount());
        }
        else {
          transaction.setTipsAmount(Double.valueOf(NumberUtil.roundToTwoDigit(payableTipsAmount)));
        }
      }
    }
  }
  
  public void voidTicket(Ticket ticket, User currentUser) throws Exception {
    Terminal terminal = Application.getInstance().getTerminal();
    ticket.setVoidedBy(currentUser);
    ticket.setTerminal(terminal);
    ticket.calculatePrice();
    TicketDAO.getInstance().voidTicket(ticket);
    try {
      ReceiptPrintService.printVoidTicket(ticket);
    }
    catch (Exception localException) {}
  }
  
  private double getRefundableAmount(PosTransaction posTransaction) {
    double refundedAmountForTransaction = 0.0D;
    String refundedAmountText = posTransaction.getProperty("REFUNDED_AMOUNT");
    if (StringUtils.isNotEmpty(refundedAmountText)) {
      try {
        refundedAmountForTransaction = Double.parseDouble(refundedAmountText);
      }
      catch (Exception localException) {}
    }
    return NumberUtil.roundToTwoDigit(posTransaction.getAmount().doubleValue() - refundedAmountForTransaction);
  }
  
  public double refundTicket(Ticket ticket, double refundTenderedAmount, Double refundItemTaxAmount, User currentUser, List<PosTransaction> transactions, boolean forceCashRefund) throws Exception
  {
    return refundTicket(ticket, refundTenderedAmount, refundItemTaxAmount, currentUser, transactions, forceCashRefund, null);
  }
  
  public double refundTicket(Ticket ticket, double refundTenderedAmount, Double refundItemTaxAmount, User currentUser, List<PosTransaction> transactions, boolean forceCashRefund, String giftCardNo) throws Exception
  {
    double refundTenderedRemaining = refundTenderedAmount;
    boolean refundToCustomerBalance = false;
    Iterator iterator;
    if (transactions.size() > 1) {
      for (iterator = transactions.iterator(); iterator.hasNext();) {
        PosTransaction posTransaction = (PosTransaction)iterator.next();
        if (getRefundableAmount(posTransaction) == refundTenderedAmount) {
          transactions = new ArrayList();
          transactions.add(posTransaction);
          break;
        }
      }
    }
    for (PosTransaction posTransaction : transactions) {
      double refundAmount = getRefundableAmount(posTransaction);
      if (refundAmount > refundTenderedRemaining) {
        refundAmount = refundTenderedRemaining;
      }
      if (refundAmount <= refundTenderedRemaining) {
        double refundTaxAmount = refundItemTaxAmount.doubleValue() * refundAmount / refundTenderedAmount;
        refundTicket(ticket, refundAmount, refundTaxAmount, currentUser, posTransaction, forceCashRefund, giftCardNo);
        if (posTransaction.isRefunded())
        {

          refundTenderedRemaining -= refundAmount;
          if ((posTransaction instanceof CustomerAccountTransaction))
            refundToCustomerBalance = true;
        }
      }
    }
    if (refundTenderedAmount != refundTenderedRemaining) {
      ticket.setRefunded(Boolean.valueOf(true));
      ticket.setClosed(Boolean.valueOf(true));
      ticket.setClosingDate(StoreDAO.geServerTimestamp());
      ticket.setCashier(currentUser);
      ticket.calculateRefundAmount();
      ticket.setRefundableAmount(Double.valueOf(ticket.getRefundableAmount().doubleValue() - refundTenderedAmount - refundTenderedRemaining));
      ticket.calculatePrice();
      mergeCashRefundTransactions(ticket);
      
      Session session = null;
      Transaction transaction = null;
      try
      {
        session = TicketDAO.getInstance().createNewSession();
        transaction = session.beginTransaction();
        TicketDAO.getInstance().saveOrUpdate(ticket, session);
        if (refundToCustomerBalance) {
          CustomerDAO.getInstance().saveOrUpdate(ticket.getCustomer(), session);
        }
        transaction.commit();
      } catch (Exception e) {
        if (transaction != null) {
          transaction.rollback();
        }
        
        throw e;
      } finally {
        TicketDAO.getInstance().closeSession(session);
      }
    }
    
    return refundTenderedAmount - refundTenderedRemaining;
  }
  
  private void mergeCashRefundTransactions(Ticket ticket) {
    RefundTransaction refundTransaction = null;
    for (Iterator iterator = ticket.getTransactions().iterator(); iterator.hasNext();) {
      PosTransaction posTransaction = (PosTransaction)iterator.next();
      if (((posTransaction instanceof RefundTransaction)) && (posTransaction.getId() == null) && 
        (posTransaction.getPaymentType().equals(PaymentType.CASH.getDisplayString()))) {
        if (refundTransaction != null) {
          refundTransaction.setAmount(Double.valueOf(refundTransaction.getAmount().doubleValue() + posTransaction.getAmount().doubleValue()));
          refundTransaction.setTipsAmount(Double.valueOf(refundTransaction.getTipsAmount().doubleValue() + posTransaction.getTipsAmount().doubleValue()));
          refundTransaction.setTaxAmount(Double.valueOf(refundTransaction.getTaxAmount().doubleValue() + posTransaction.getTaxAmount().doubleValue()));
          iterator.remove();
        }
        else if (posTransaction.getId() == null) {
          refundTransaction = (RefundTransaction)posTransaction;
        }
      }
    }
  }
  
  public void refundTicket(Ticket ticket, double refundAmount, double refundTaxAmount, User currentUser, PosTransaction transaction, boolean forceCashRefund) throws Exception
  {
    refundTicket(ticket, refundAmount, refundTaxAmount, currentUser, transaction, forceCashRefund, null);
  }
  
  public void refundTicket(Ticket ticket, double refundAmount, double refundTaxAmount, User currentUser, PosTransaction transaction, boolean forceCashRefund, String giftCardNo) throws Exception
  {
    try {
      if (refundAmount <= 0.0D)
        return;
      boolean refundedUsingCard = false;
      boolean voidedUsingCard = false;
      
      if (StringUtils.isNotEmpty(giftCardNo)) {
        GiftCardPaymentPlugin paymentGateway = GiftCardConfig.getPaymentGateway();
        GiftCardProcessor giftCardProcessor = paymentGateway.getProcessor();
        giftCardProcessor.refund(giftCardNo, refundAmount);
      }
      else if (!forceCashRefund) {
        if (transaction != null) {
          if ((transaction instanceof CustomerAccountTransaction)) {
            Customer customer = ticket.getCustomer();
            if (customer == null) {
              throw new PosException("Failed to refund because member not found.");
            }
            customer.setBalance(Double.valueOf(customer.getBalance().doubleValue() + refundAmount));
            transaction.setRefunded(true);
          }
          else if ((transaction instanceof CreditCardTransaction)) {
            double transactionAmount = transaction.getAmount().doubleValue();
            CardProcessor cardProcessor = CardConfig.getPaymentGateway().getProcessor();
            try {
              transaction.setAmount(Double.valueOf(refundAmount));
              if (transactionAmount == refundAmount) {
                cardProcessor.voidTransaction(transaction);
                voidedUsingCard = transaction.isVoided().booleanValue();
              }
              if (!voidedUsingCard) {
                cardProcessor.refundTransaction(transaction, refundAmount);
                refundedUsingCard = transaction.isRefunded();
              }
            } catch (Exception e) {
              try {
                cardProcessor.refundTransaction(transaction, refundAmount);
                refundedUsingCard = transaction.isRefunded();
              }
              catch (Exception localException1) {}
            } finally {
              transaction.setAmount(Double.valueOf(transactionAmount));
            }
          }
        }
        if (((transaction instanceof CreditCardTransaction)) && (!refundedUsingCard) && (!voidedUsingCard)) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Failed to refund.");
          throw new PosException("Failed to refund.");
        }
      }
      String paymentType = "";
      if (StringUtils.isNotEmpty(giftCardNo)) {
        paymentType = PaymentType.GIFT_CERTIFICATE.getDisplayString();
      }
      else {
        paymentType = forceCashRefund ? PaymentType.CASH.getDisplayString() : transaction.getPaymentType();
      }
      RefundTransaction posTransaction = createRefundTransaction(ticket, transaction, refundAmount);
      posTransaction.setPaymentType(paymentType);
      posTransaction.setUser(currentUser);
      posTransaction.setTaxAmount(Double.valueOf(refundTaxAmount));
      if ((transaction instanceof CustomerAccountTransaction)) {
        posTransaction.setCustomerId(ticket.getCustomerId());
      }
      if (StringUtils.isNotEmpty(giftCardNo)) {
        posTransaction.setGiftCertNumber(giftCardNo);
      }
      if (transaction.getTipsAmount().doubleValue() > 0.0D) {
        Gratuity gratuity = ticket.getGratuity();
        gratuity.setAmount(Double.valueOf(gratuity.getAmount().doubleValue() - transaction.getTipsAmount().doubleValue()));
      }
      ticket.addTotransactions(posTransaction);
      transaction.setRefunded(true);
    } catch (Exception e) {
      throw e;
    }
  }
  
  private RefundTransaction createRefundTransaction(Ticket ticket, PosTransaction transaction, double refundAmount) {
    RefundTransaction posTransaction = new RefundTransaction();
    posTransaction.setTicket(ticket);
    posTransaction.setTerminal(ticket.getTerminal());
    posTransaction.setTransactionType(TransactionType.DEBIT.name());
    posTransaction.setCashDrawer(Application.getCurrentUser().getActiveDrawerPullReport());
    posTransaction.setTransactionTime(new Date());
    posTransaction.setCardExpMonth(transaction.getCardExpMonth());
    posTransaction.setCardHolderName(transaction.getCardHolderName());
    posTransaction.setCardAuthCode(transaction.getCardAuthCode());
    posTransaction.setCardMerchantGateway(transaction.getCardMerchantGateway());
    posTransaction.setCardNumber(transaction.getCardNumber());
    posTransaction.setCardTrack(transaction.getCardTrack());
    posTransaction.setCardTransactionId(transaction.getCardTransactionId());
    posTransaction.setCardReader(transaction.getCardReader());
    posTransaction.setCardType(transaction.getCardType());
    posTransaction.addProperty("REFUNDED_TRANSACTION_ID", transaction.getId());
    
    String refundedAmountText = transaction.getProperty("REFUNDED_AMOUNT");
    double previousRefundedAmount = 0.0D;
    if (StringUtils.isNotEmpty(refundedAmountText)) {
      try {
        previousRefundedAmount = Double.parseDouble(refundedAmountText);
      }
      catch (Exception localException) {}
    }
    posTransaction.setAmount(Double.valueOf(refundAmount));
    transaction.addProperty("REFUNDED_AMOUNT", String.valueOf(refundAmount + previousRefundedAmount));
    
    if (transaction.getTipsAmount().doubleValue() > 0.0D) {
      String refundedTipsAmountText = transaction.getProperty("REFUNDED_TIPS_AMOUNT");
      double previousRefundedTipsAmount = 0.0D;
      if (StringUtils.isNotEmpty(refundedAmountText)) {
        try {
          previousRefundedTipsAmount = Double.parseDouble(refundedTipsAmountText);
        }
        catch (Exception localException1) {}
      }
      double transactionAmountWithoutTips = transaction.getAmount().doubleValue() - transaction.getTipsAmount().doubleValue();
      double totalRefundedAmount = previousRefundedAmount + refundAmount;
      double tipsRefundAmount = totalRefundedAmount - transactionAmountWithoutTips - previousRefundedTipsAmount;
      if (tipsRefundAmount > 0.0D) {
        posTransaction.setTipsAmount(Double.valueOf(tipsRefundAmount));
        transaction.addProperty("REFUNDED_TIPS_AMOUNT", String.valueOf(tipsRefundAmount + previousRefundedTipsAmount));
        Gratuity gratuity = ticket.getGratuity();
        gratuity.setAmount(Double.valueOf(gratuity.getAmount().doubleValue() - tipsRefundAmount));
      }
    }
    return posTransaction;
  }
  
  public static PosTransactionService getInstance() {
    return paymentService;
  }
}
