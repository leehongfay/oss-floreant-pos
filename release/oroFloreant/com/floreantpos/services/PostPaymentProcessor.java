package com.floreantpos.services;

import com.floreantpos.model.PosTransaction;
import org.hibernate.Session;

public abstract interface PostPaymentProcessor
{
  public abstract void paymentDone(PosTransaction paramPosTransaction, Session paramSession);
}
