package com.floreantpos.services;

import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.TicketDAO;














public class TicketService
{
  public TicketService() {}
  
  public static Ticket getTicket(String ticketId)
  {
    TicketDAO dao = new TicketDAO();
    Ticket ticket = dao.get(ticketId);
    
    if (ticket == null) {
      throw new PosException(POSConstants.NO_TICKET_WITH_ID + " " + ticketId + " " + POSConstants.FOUND);
    }
    
    return ticket;
  }
}
