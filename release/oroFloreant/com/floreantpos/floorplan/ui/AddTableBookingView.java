package com.floreantpos.floorplan.ui;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.bo.ui.Command;
import com.floreantpos.main.Application;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.QwertyKeyPad;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import net.miginfocom.swing.MigLayout;



















public class AddTableBookingView
  extends JPanel
  implements ActionListener, ListSelectionListener
{
  protected BeanEditor beanEditor;
  protected JPanel buttonPanel;
  protected PosButton btnSave = new PosButton(Messages.getString("ModelBrowser.2"));
  private PosButton btnBookingCancel = new PosButton("CLOSE");
  
  private POSDialog dialog;
  private QwertyKeyPad qwertyKeyPad;
  
  public AddTableBookingView(POSDialog dialog, BeanEditor beanEditor)
  {
    this.dialog = dialog;
    this.beanEditor = beanEditor;
    init();
  }
  

  public void init()
  {
    setLayout(new BorderLayout());
    setBorder(null);
    
    buttonPanel = new JPanel(new MigLayout("fill"));
    
    qwertyKeyPad = new QwertyKeyPad();
    qwertyKeyPad.setCollapsed(true);
    
    PosButton btnKeyboard = new PosButton(IconFactory.getIcon("/images/", "keyboard.png"));
    btnKeyboard.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        qwertyKeyPad.setCollapsed(!qwertyKeyPad.isCollapsed());
      }
      
    });
    buttonPanel.add(btnSave, "split 2,right, gapleft 100");
    buttonPanel.add(btnBookingCancel);
    buttonPanel.add(btnKeyboard, "span, right");
    
    beanEditor.setBorder(BorderFactory.createTitledBorder("Booking"));
    
    add(beanEditor, "Center");
    JPanel southPanel = new JPanel(new BorderLayout());
    southPanel.add(qwertyKeyPad, "North");
    southPanel.add(buttonPanel, "Center");
    add(southPanel, "South");
    
    btnSave.addActionListener(this);
    btnBookingCancel.addActionListener(this);
    

    beanEditor.setFieldsEnable(true);
    btnSave.setEnabled(true);
    beanEditor.setFieldsEnable(true);
  }
  


  public void actionPerformed(ActionEvent e)
  {
    Command command = Command.fromString(e.getActionCommand());
    try
    {
      switch (2.$SwitchMap$com$floreantpos$bo$ui$Command[command.ordinal()]) {
      case 1: 
        doCancelEditing();
        break;
      
      case 2: 
        if (beanEditor.save()) {
          beanEditor.setFieldsEnable(false);
          btnSave.setEnabled(false);
          dialog.setCanceled(false);
          dialog.dispose();
        }
        

        break;
      }
      
      
      if (e.getSource() == btnBookingCancel) {
        dialog.setCanceled(true);
        dialog.dispose();
      }
    }
    catch (Exception e2) {
      POSMessageDialog.showError(Application.getPosWindow(), e2.getMessage(), e2);
    }
  }
  
  private void doCancelEditing() {
    beanEditor.setFieldsEnable(false);
    btnSave.setEnabled(false);
    btnBookingCancel.setEnabled(true);
    beanEditor.cancel();
  }
  
  public void valueChanged(ListSelectionEvent e)
  {
    if (e.getValueIsAdjusting()) {}
  }
  


  public void setModels(List models) {}
  


  public BeanEditor getBeanEditor()
  {
    return beanEditor;
  }
  
  public void refreshButtonPanel() {
    beanEditor.clearFields();
    btnSave.setEnabled(false);
  }
}
