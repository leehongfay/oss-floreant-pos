package com.floreantpos.floorplan.ui;

import com.floreantpos.bo.ui.ModelBrowser;
import com.floreantpos.model.BookingInfo;
import com.floreantpos.model.dao.BookingInfoDAO;
import com.floreantpos.ui.SearchPanel;
import com.floreantpos.ui.util.UiUtil;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXDatePicker;

public class TableBookingSearchPanel extends SearchPanel<BookingInfo>
{
  private JXDatePicker tbStartDate;
  private JXDatePicker tbEndDate;
  private JLabel lblFromDate;
  private JLabel lblToDate;
  private JButton btnSearch;
  private JToggleButton btnTodaysView;
  
  public TableBookingSearchPanel()
  {
    setLayout(new MigLayout("", "[][]20[]10[]10[]", "[][]"));
    tbStartDate = UiUtil.getCurrentMonthStart();
    tbEndDate = UiUtil.getCurrentMonthEnd();
    
    lblFromDate = new JLabel(Messages.getString("TableBookingSearchPanel.0"));
    lblToDate = new JLabel(Messages.getString("TableBookingSearchPanel.1"));
    
    btnTodaysView = new JToggleButton(Messages.getString("TableBookingSearchPanel.2"));
    btnSearch = new JButton(Messages.getString("TableBookingSearchPanel.4"));
    btnSearch.setPreferredSize(new Dimension(60, 50));
    btnTodaysView.setPreferredSize(new Dimension(60, 50));
    
    add(lblFromDate, "cell 0 0");
    add(tbStartDate, "cell 1 0");
    add(lblToDate, "cell 0 1");
    add(tbEndDate, "cell 1 1");
    add(btnSearch, "cell 2 0 0 2");
    add(new JSeparator(1), "cell 3 0 0 2,grow");
    add(btnTodaysView, "cell 4 0 0 2,align center");
    
    setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(1), Messages.getString("TableBookingSearchPanel.12")));
    
    initActionHandlers();
  }
  
  private void initActionHandlers()
  {
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnTodaysView.setSelected(false);
        
        if (tbStartDate.getDate().getTime() > tbEndDate.getDate().getTime()) {
          JOptionPane.showMessageDialog(null, Messages.getString("TableBookingSearchPanel.13"));
          return;
        }
        try
        {
          getModelBrowser().refreshButtonPanel();
          Date startDate = TableBookingSearchPanel.this.getStartDate();
          Date endDate = TableBookingSearchPanel.this.getEndDate();
          
          List bookingList = BookingInfoDAO.getInstance().getAllBookingInfoByDate(startDate, endDate);
          modelBrowser.setModels(bookingList);
          TableBookingSearchPanel.this.refreshSearchTable("search", startDate, endDate);
        }
        catch (Exception e2) {
          e2.printStackTrace();

        }
        
      }
      

    });
    btnTodaysView.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try
        {
          getModelBrowser().refreshButtonPanel();
          AbstractButton abstractButton = (AbstractButton)e.getSource();
          boolean selected = abstractButton.getModel().isSelected();
          
          if (selected) {
            List todaysBookingList = BookingInfoDAO.getInstance().getTodaysBooking();
            modelBrowser.setModels(todaysBookingList);
            TableBookingSearchPanel.this.refreshSearchTable("todaysview", null, null);
            return;
          }
          
          List<BookingInfo> bookingList = BookingInfoDAO.getInstance().getAllOpenBooking();
          modelBrowser.setModels(bookingList);
          TableBookingSearchPanel.this.refreshSearchTable("open", null, null);
        }
        catch (Exception e1) {
          e1.printStackTrace();
        }
      }
    });
  }
  
  private Date getEndDate()
  {
    Calendar endDate = Calendar.getInstance();
    endDate.setLenient(false);
    endDate.setTime(tbEndDate.getDate());
    endDate.set(11, 23);
    endDate.set(12, 59);
    endDate.set(13, 59);
    return endDate.getTime();
  }
  
  private Date getStartDate() {
    Calendar startDate = Calendar.getInstance();
    startDate.setLenient(false);
    startDate.setTime(tbStartDate.getDate());
    startDate.set(11, 0);
    startDate.set(12, 0);
    startDate.set(13, 0);
    startDate.set(14, 0);
    return startDate.getTime();
  }
  
  private void refreshSearchTable(String searchParameter, Date startDate, Date endDate) {
    TableBookingForm tableBookingForm = (TableBookingForm)modelBrowser.getBeanEditor();
    searchParameter = searchParameter;
    stardDateForSearchBooking = startDate;
    endDateForSearchBooking = endDate;
  }
  
  public void refreshSearchPanel()
  {
    super.refreshSearchPanel();
    btnTodaysView.setSelected(false);
  }
}
