package com.floreantpos.floorplan.ui;

import com.floreantpos.bo.ui.ModelBrowser;
import com.floreantpos.model.ShopTableType;
import com.floreantpos.model.dao.ShopTableTypeDAO;
import com.floreantpos.swing.BeanTableModel;
import java.util.List;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.jdesktop.swingx.JXTable;




public class TableTypeBrowserPanel
  extends ModelBrowser<ShopTableType>
{
  public TableTypeBrowserPanel()
  {
    super(new ShopTableTypeForm());
    
    BeanTableModel<ShopTableType> tableModel = new BeanTableModel(ShopTableType.class);
    tableModel.addColumn(Messages.getString("TableTypeBrowserPanel.0"), ShopTableType.PROP_ID);
    tableModel.addColumn(Messages.getString("TableTypeBrowserPanel.1"), ShopTableType.PROP_NAME);
    tableModel.addColumn(Messages.getString("TableTypeBrowserPanel.2"), ShopTableType.PROP_DESCRIPTION);
    
    init(tableModel);
    
    browserTable.setAutoResizeMode(4);
    setColumnWidth(0, 120);
    setColumnWidth(1, 120);
  }
  
  public void refreshTable()
  {
    List<ShopTableType> tables = ShopTableTypeDAO.getInstance().findAll();
    BeanTableModel tableModel = (BeanTableModel)browserTable.getModel();
    tableModel.removeAll();
    tableModel.addRows(tables);
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = browserTable.getColumnModel().getColumn(columnNumber);
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
}
