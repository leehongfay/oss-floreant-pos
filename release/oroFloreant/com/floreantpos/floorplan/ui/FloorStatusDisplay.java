package com.floreantpos.floorplan.ui;

import com.floreantpos.actions.ActionCommand;
import com.floreantpos.main.Application;
import com.floreantpos.model.ShopFloor;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.ShopFloorDAO;
import com.floreantpos.model.dao.ShopTableDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.ImageComponent;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.ShopTableButton;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.Timer;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;

public class FloorStatusDisplay extends JFrame
{
  private DefaultListModel<ShopFloor> listModel = new DefaultListModel();
  private JList<ShopFloor> floorsList = new JList(listModel);
  
  private JLayeredPane floorPanel = new JLayeredPane();
  
  private JPanel contentPanel;
  private TicketAndTablesTableModel tableModel = new TicketAndTablesTableModel();
  
  private static int buttonWidth = 44;
  private static int buttonHeight = 44;
  Timer updateTimer;
  
  public FloorStatusDisplay()
  {
    setLayout(new BorderLayout());
    setDefaultCloseOperation(2);
    
    setTitle(Messages.getString("FloorStatusDisplay.0"));
    
    contentPanel = new JPanel(new BorderLayout(10, 10));
    contentPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 0, 10));
    
    renderFloorsList();
    createLayoutPanel();
    
    contentPanel.add(new JSeparator(0), "South");
    add(contentPanel, "Center");
    
    createButtonPanel();
    
    populateFloors();
    populateTicketAndTables();
    
    updateTimer = new Timer(30000, new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        FloorStatusDisplay.this.populateFloors();
        FloorStatusDisplay.this.populateTicketAndTables();
      }
      
    });
    setSize(Application.getPosWindow().getSize());
    setExtendedState(6);
  }
  
  private void populateTicketAndTables() {
    List<Ticket> openTickets = TicketDAO.getInstance().findOpenTickets();
    
    if (tableModel.getRows() != null) {
      tableModel.getRows().clear();
    }
    
    for (Iterator localIterator1 = openTickets.iterator(); localIterator1.hasNext();) { ticket = (Ticket)localIterator1.next();
      List<ShopTable> tables = ShopTableDAO.getInstance().getTables(ticket);
      if (tables != null)
      {


        for (ShopTable shopTable : tables)
          tableModel.addItem(new TicketAndTable(ticket, shopTable)); }
    }
    Ticket ticket;
  }
  
  private void createButtonPanel() {
    JPanel buttonPanel = new JPanel(new MigLayout("align 50% 50%"));
    
    PosButton btnOk = new PosButton(ActionCommand.CLOSE.name());
    btnOk.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        dispose();
      }
      
    });
    buttonPanel.add(btnOk, "w 80!");
    
    add(buttonPanel, "South");
  }
  
  private void populateFloors() {
    try {
      listModel.clear();
      
      List<ShopFloor> shopFloors = ShopFloorDAO.getInstance().findAll();
      for (ShopFloor shopFloor : shopFloors) {
        listModel.addElement(shopFloor);
      }
      if (listModel.getSize() > 0) {
        floorsList.setSelectedIndex(0);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  private void renderFloorsList()
  {
    JPanel listPanel = new JPanel(new MigLayout("fill, ins 0"));
    JPanel floorListPanel = new JPanel(new BorderLayout());
    floorListPanel.setBorder(BorderFactory.createTitledBorder(Messages.getString("FloorStatusDisplay.4")));
    
    JScrollPane listScrollPane = new JScrollPane(floorsList);
    listScrollPane.setPreferredSize(new Dimension(120, 240));
    floorListPanel.add(listScrollPane);
    
    DefaultListCellRenderer renderer = new DefaultListCellRenderer() {
      public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        Component rendererComponent = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        rendererComponent.setPreferredSize(new Dimension(60, 40));
        return rendererComponent;
      }
    };
    floorsList.setCellRenderer(renderer);
    
    floorsList.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e) {
        ShopFloor selectedValue = (ShopFloor)floorsList.getSelectedValue();
        if (selectedValue == null) {
          return;
        }
        try
        {
          FloorStatusDisplay.this.renderFloor(selectedValue);
        } catch (Exception e1) {
          e1.printStackTrace();
        }
        
      }
    });
    JPanel tableListPanel = new JPanel(new BorderLayout(5, 5));
    tableListPanel.setBorder(BorderFactory.createTitledBorder(Messages.getString("FloorStatusDisplay.5")));
    JXTable ticketAndTablesTable = new JXTable(tableModel);
    ticketAndTablesTable.getColumnModel().getColumn(1).setPreferredWidth(100);
    ticketAndTablesTable.setRowHeight(40);
    tableListPanel.add(new JScrollPane(ticketAndTablesTable));
    
    listPanel.add(floorListPanel, "grow, h 200!");
    listPanel.add(tableListPanel, "newline, grow");
    listPanel.setPreferredSize(new Dimension(250, 0));
    contentPanel.add(listPanel, "West");
  }
  
  private void createLayoutPanel()
  {
    floorPanel.setOpaque(true);
    
    JPanel floorPanelContainer = new JPanel();
    floorPanelContainer.add(floorPanel);
    floorPanelContainer.setAutoscrolls(true);
    floorPanelContainer.setBackground(Color.white);
    
    PosScrollPane scrollPaneFloorPanelContainer = new PosScrollPane(floorPanelContainer);
    scrollPaneFloorPanelContainer.getVerticalScrollBar().setPreferredSize(new Dimension(50, 0));
    scrollPaneFloorPanelContainer.getHorizontalScrollBar().setPreferredSize(new Dimension(0, 50));
    scrollPaneFloorPanelContainer.setBorder(BorderFactory.createTitledBorder(Messages.getString("FloorStatusDisplay.8")));
    
    contentPanel.add(scrollPaneFloorPanelContainer, "Center");
  }
  
  private void renderFloor(ShopFloor floor) throws Exception {
    floorPanel.removeAll();
    
    if (floor == null) {
      return;
    }
    
    ImageIcon image = floor.getImage();
    ImageComponent imageComponent = null;
    if (image != null) {
      imageComponent = new ImageComponent(image.getImage());
      imageComponent.setBounds(0, 0, imageComponent.getImageWidth().intValue(), imageComponent.getImageHeight().intValue());
      floorPanel.setPreferredSize(new Dimension(imageComponent.getImageWidth().intValue(), imageComponent.getImageHeight().intValue()));
      floorPanel.add(imageComponent);
    }
    
    renderTables(floor);
    
    if (imageComponent != null) {
      floorPanel.moveToBack(imageComponent);
      floorPanel.revalidate();
      floorPanel.repaint();
    }
  }
  
  private void renderTables(ShopFloor floor) {
    Set<ShopTable> tables = floor.getTables();
    if (tables == null) {
      return;
    }
    
    for (final ShopTable shopTable : tables) {
      final ShopTableButton tableButton = new ShopTableButton(shopTable);
      tableButton.setBounds(shopTable.getX().intValue(), shopTable.getY().intValue(), buttonWidth, buttonHeight);
      tableButton.addMouseListener(new java.awt.event.MouseAdapter()
      {
        public void mouseClicked(MouseEvent e)
        {
          TableStatusDialog dialog = new TableStatusDialog(shopTable, tableButton);
          dialog.setSize(350, 300);
          dialog.setDefaultCloseOperation(2);
          dialog.setLocationRelativeTo(Application.getPosWindow());
          dialog.setVisible(true);
        }
        
      });
      floorPanel.add(tableButton);
    }
  }
  
  public void setVisible(boolean b)
  {
    super.setVisible(b);
    
    if (b) {
      updateTimer.start();
    }
    else {
      updateTimer.stop();
    }
  }
  
  class TicketAndTable {
    Ticket ticket;
    ShopTable table;
    
    public TicketAndTable(Ticket ticket, ShopTable table) {
      this.ticket = ticket;
      this.table = table;
    }
  }
  
  class TicketAndTablesTableModel extends ListTableModel<FloorStatusDisplay.TicketAndTable>
  {
    TicketAndTablesTableModel() {
      super();
    }
    
    TicketAndTablesTableModel() {
      super(list);
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      FloorStatusDisplay.TicketAndTable rowData = (FloorStatusDisplay.TicketAndTable)getRowData(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return ticket.getId();
      
      case 1: 
        return ticket.getOwner().getFirstName();
      
      case 2: 
        return table.getTableNumber();
      }
      
      return null;
    }
  }
}
