package com.floreantpos.floorplan.ui.booking;

import com.floreantpos.model.BookingInfo;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.ui.BeanEditor;
import com.orocube.tablebooking.model.BookingRequest;
import com.orocube.tablebooking.model.Status;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;

public class BookingRequestUpdateForm
  extends BeanEditor<BookingRequest>
{
  private BookingRequest bookingRequest;
  private JComboBox cbStatus;
  private BookingInfo tableBookingInfo;
  
  public BookingRequestUpdateForm()
  {
    initComponents();
  }
  
  public BookingRequestUpdateForm(BookingRequest bookingRequest) {
    this.bookingRequest = bookingRequest;
    initComponents();
    setBean(bookingRequest);
  }
  
  private void initComponents() {
    JPanel mainPanel = new JPanel(new MigLayout("fill"));
    
    cbStatus = new JComboBox();
    cbStatus.addItem(Status.CONFIRM);
    cbStatus.addItem(Status.REJECTED);
    
    mainPanel.add(cbStatus);
    add(mainPanel);
  }
  



































  public boolean save()
  {
    return false;
  }
  
  protected void updateView()
  {
    BookingRequest bookingRequest = (BookingRequest)getBean();
    
    cbStatus.setSelectedItem(bookingRequest.getStatus());
  }
  
  protected boolean updateModel()
    throws IllegalModelStateException
  {
    BookingRequest bookingRequest = (BookingRequest)getBean();
    bookingRequest.setIsProcessed(Boolean.valueOf(true));
    bookingRequest.setStatus((Status)cbStatus.getSelectedItem());
    return true;
  }
  
  public String getDisplayText()
  {
    BookingRequest bookingRequest = (BookingRequest)getBean();
    if (bookingRequest.getId() == null) {
      return "Booking Request Add";
    }
    return "Booking Request Update";
  }
  
  public BookingInfo getBookingInfo() {
    return tableBookingInfo;
  }
}
