package com.floreantpos.floorplan.ui.booking;

import com.floreantpos.floorplan.ui.TableBookingForm;
import com.floreantpos.model.Customer;
import com.floreantpos.model.OrderType;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import javax.swing.JDialog;
import javax.swing.JPanel;

public class BookingCustomerSelectionDialog
  extends JDialog
{
  private BookingCustomerSelectionView customerSelectionView;
  private JPanel contentPanel;
  private CardLayout cardLayout;
  private boolean canceled = true;
  private TableBookingForm bookingInfo;
  private OrderType ticketType;
  
  public BookingCustomerSelectionDialog() {
    setTitle("Select customer and booking info");
    createUI();
    setDefaultCloseOperation(2);
  }
  
  public BookingCustomerSelectionDialog(TableBookingForm bookingInfo) {
    this.bookingInfo = bookingInfo;
    setTitle("Select customer and booking info");
    createUI();
    setDefaultCloseOperation(2);
  }
  
  private void createUI() {
    getContentPane().setLayout(new BorderLayout(0, 0));
    
    contentPanel = new JPanel();
    getContentPane().add(contentPanel, "Center");
    cardLayout = new CardLayout(0, 0);
    contentPanel.setLayout(cardLayout);
    
    customerSelectionView = new BookingCustomerSelectionView(this);
    
    contentPanel.add(customerSelectionView, customerSelectionView.getName());
    
    cardLayout.show(contentPanel, customerSelectionView.getName());
  }
  
  public boolean isCanceled() {
    return canceled;
  }
  
  public void setCanceled(boolean canceled) {
    this.canceled = canceled;
  }
  
  public Customer getCustomer() {
    return customerSelectionView.getCustomer();
  }
  
  public OrderType getTicketType() {
    return ticketType;
  }
  
  public void setTicketType(OrderType ticketType) {
    this.ticketType = ticketType;
  }
  
  public TableBookingForm getBookingInfo() {
    return bookingInfo;
  }
}
