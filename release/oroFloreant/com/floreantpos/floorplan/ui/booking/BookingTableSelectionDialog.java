package com.floreantpos.floorplan.ui.booking;

import com.floreantpos.PosException;
import com.floreantpos.main.Application;
import com.floreantpos.model.ShopFloor;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.dao.ShopFloorDAO;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import net.miginfocom.swing.MigLayout;


























public class BookingTableSelectionDialog
  extends OkCancelOptionDialog
  implements ChangeListener
{
  private static BookingTableSelectionDialog instance;
  private int guestCount;
  public int selectedCapacity = 0;
  public JLabel selectedTableCapacity;
  private JTabbedPane floorTab;
  private List<ShopTable> selectedTables;
  private Date startDate;
  private Date endDate;
  private JPanel topPanel;
  private JLabel lblGuestNumber;
  
  private BookingTableSelectionDialog() {
    super(Application.getPosWindow(), "Table selection dialog");
    initComponent();
    createLayoutPanel();
    setResizable(true);
  }
  
  private void initComponent() {
    setOkButtonText("SELECT");
    topPanel = new JPanel(new MigLayout());
    lblGuestNumber = new JLabel();
    topPanel.add(lblGuestNumber, "wrap");
    
    JLabel lblSelectedCapacity = new JLabel("Selected capacity: ");
    topPanel.add(lblSelectedCapacity);
    selectedTableCapacity = new JLabel();
    topPanel.add(selectedTableCapacity);
    selectedTableCapacity.setText("" + selectedCapacity);
    setSize(1024, 600);
    
    floorTab = new JTabbedPane(1, 1);
    
    floorTab.addChangeListener(this);
    add(topPanel, "North");
    add(floorTab, "Center");
  }
  
  private void createLayoutPanel() {
    try {
      List<ShopFloor> shopFloors = ShopFloorDAO.getInstance().findAll();
      
      for (ShopFloor shopFloor : shopFloors) {
        BookingTableSelectionFloorView floorView = new BookingTableSelectionFloorView(this, shopFloor);
        floorTab.addTab(shopFloor.getName(), floorView);
      }
    }
    catch (PosException e)
    {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getLocalizedMessage(), e);
    }
  }
  
  public void doOk()
  {
    int tabCount = floorTab.getTabCount();
    if (selectedTables == null) {
      selectedTables = new ArrayList();
    }
    selectedTables.clear();
    for (int i = 0; i < tabCount; i++) {
      BookingTableSelectionFloorView floorView = (BookingTableSelectionFloorView)floorTab.getComponentAt(i);
      List<ShopTable> selectedTables2 = floorView.getSelectedTables();
      if (selectedTables2 != null) {
        for (ShopTable shopTable : selectedTables2) {
          if (!selectedTables.contains(shopTable))
          {

            selectedTables.add(shopTable);
          }
        }
      }
    }
    if ((selectedTables == null) || (selectedTables.isEmpty())) {
      POSMessageDialog.showMessage("Please select table according to guest number.");
      return;
    }
    
    if (selectedCapacity < guestCount) {
      POSMessageDialog.showMessage("Selected capacity must be greater than guest number.");
      return;
    }
    setCanceled(false);
    dispose();
  }
  
  public void clearAllFloorSelection(boolean isCancel) {
    int tabCount = floorTab.getTabCount();
    for (int i = 0; i < tabCount; i++) {
      BookingTableSelectionFloorView floorView = (BookingTableSelectionFloorView)floorTab.getComponentAt(i);
      floorView.clearSelection();
      if (isCancel) {
        List<ShopTable> releasedListModel = floorView.getReleasedListModel();
        if ((releasedListModel != null) && (selectedTables != null)) {
          selectedTables.addAll(releasedListModel);
        }
      }
    }
  }
  







  public void doCancel()
  {
    clearAllFloorSelection(true);
    setCanceled(true);
    dispose();
  }
  
  public List<ShopTable> getSelectedTables() {
    return selectedTables;
  }
  
  public Date getStartDate() {
    return startDate;
  }
  
  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }
  
  public Date getEndDate() {
    return endDate;
  }
  
  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }
  
  public static synchronized BookingTableSelectionDialog getInstance() {
    if (instance == null) {
      instance = new BookingTableSelectionDialog();
    }
    return instance;
  }
  
  public int getGuestCount() {
    return guestCount;
  }
  
  public void setGuestCount(int guestCount) {
    this.guestCount = guestCount;
    lblGuestNumber.setText("Guest number: " + guestCount);
  }
  
  public void setSelectedTables(List<ShopTable> selectedTables) {
    this.selectedTables = selectedTables;
  }
  
  public void updateView() {
    if ((selectedTables != null) && (selectedTables.size() != 0)) {
      for (ShopTable shopTable : selectedTables) {
        selectedCapacity += shopTable.getCapacity().intValue();
        selectedTableCapacity.setText("" + selectedCapacity);
      }
    }
    else {
      selectedCapacity = 0;
      selectedTableCapacity.setText("" + selectedCapacity);
    }
    renderFloorView();
  }
  
  private void renderFloorView() {
    BookingTableSelectionFloorView floorView = (BookingTableSelectionFloorView)floorTab.getSelectedComponent();
    if (floorView == null) {
      return;
    }
    floorView.renderFloor();
    floorView.updateView();
    topPanel.repaint();
    floorView.repaint();
  }
  
  public void stateChanged(ChangeEvent e)
  {
    renderFloorView();
  }
  
  public int getSelectedCapacity() {
    return selectedCapacity;
  }
  
  public void setSelectedCapacity(int selectedCapacity) {
    this.selectedCapacity = selectedCapacity;
    selectedTableCapacity.setText("" + selectedCapacity);
  }
}
