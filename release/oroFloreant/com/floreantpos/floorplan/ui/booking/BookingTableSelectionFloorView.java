package com.floreantpos.floorplan.ui.booking;

import com.floreantpos.model.ShopFloor;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.TableStatus;
import com.floreantpos.model.dao.BookingInfoDAO;
import com.floreantpos.model.dao.ShopTableDAO;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.ScrollableFlowPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;


public class BookingTableSelectionFloorView
  extends JPanel
{
  private BookingTableSelectionDialog dialog;
  private ShopFloor shopFloor;
  private ScrollableFlowPanel floorPanel;
  private List<ShopTable> addedTableListModel = new ArrayList();
  private List<ShopTable> releasedListModel = new ArrayList();
  
  private boolean initialized = false;
  private List<ShopTable> tables;
  
  public BookingTableSelectionFloorView(BookingTableSelectionDialog dialog, ShopFloor shopFloor) {
    this.dialog = dialog;
    this.shopFloor = shopFloor;
    setLayout(new BorderLayout(10, 10));
    createLayoutPanel();
  }
  
  private void createLayoutPanel() {
    floorPanel = new ScrollableFlowPanel(3);
    JScrollPane scrollPane = new PosScrollPane(floorPanel, 20, 31);
    scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(80, 0));
    
    add(scrollPane, "Center");
  }
  
  public void renderFloor() {
    if (shopFloor == null) {
      return;
    }
    if (initialized) {
      updateView();
      return;
    }
    renderTables();
    initialized = true;
  }
  
  private void renderTables() {
    floorPanel.getContentPane().removeAll();
    addedTableListModel.clear();
    tables = ShopTableDAO.getInstance().getByFloor(shopFloor);
    if ((tables == null) || (tables.isEmpty())) {
      return;
    }
    
    doSortTables(tables);
    for (ShopTable shopTable : tables) {
      TableButton tableButton = new TableButton(shopTable);
      floorPanel.add(tableButton);
    }
    floorPanel.getContentPane().revalidate();
    floorPanel.getContentPane().repaint();
    revalidate();
    repaint();
  }
  
  private void doSortTables(List<ShopTable> tables)
  {
    Collections.sort(tables, new Comparator()
    {
      public int compare(ShopTable lhs, ShopTable rhs) {
        return lhs.getId().compareTo(rhs.getId());
      }
    });
  }
  
  private class TableButton extends POSToggleButton implements ActionListener {
    private ShopTable table;
    
    TableButton(ShopTable table) {
      this.table = table;
      setBorder(null);
      setBackground(Color.WHITE);
      setForeground(Color.BLACK);
      setFont(getFont().deriveFont(1, PosUIManager.getFontSize(16)));
      setText("<html><body><center>#" + table.getId() + "<br>" + "Cap:" + table.getCapacity() + "</center></body></html>");
      setPreferredSize(PosUIManager.getSize(157, 138));
      addActionListener(this);
    }
    
    public ShopTable getTable() {
      return table;
    }
    
    public void actionPerformed(ActionEvent e) {
      if (isSelected()) {
        addedTableListModel.add(table);
        dialog.selectedCapacity += table.getCapacity().intValue();
        dialog.selectedTableCapacity.setText("" + dialog.selectedCapacity);
      }
      else {
        addedTableListModel.remove(table);
        List<ShopTable> selectedTables = dialog.getSelectedTables();
        if (selectedTables != null) {
          selectedTables.remove(table);
        }
        releasedListModel.add(table);
        dialog.selectedCapacity -= table.getCapacity().intValue();
        dialog.selectedTableCapacity.setText("" + dialog.selectedCapacity);
      }
    }
  }
  
  public List<ShopTable> getSelectedTables()
  {
    return addedTableListModel;
  }
  
  public void updateView() {
    List<ShopTable> bookedTableList = BookingInfoDAO.getInstance().getAllBookedTablesByDate(dialog.getStartDate(), dialog.getEndDate());
    
    Component[] components = floorPanel.getContentPane().getComponents();
    
    List<ShopTable> selectedTables = dialog.getSelectedTables();
    
    for (Component component : components) {
      if ((component instanceof TableButton))
      {

        TableButton tableButton = (TableButton)component;
        ShopTable shopTable = tableButton.getTable();
        
        if ((selectedTables != null) && 
          (selectedTables.contains(shopTable))) {
          if (!addedTableListModel.contains(shopTable)) {
            addedTableListModel.add(shopTable);
          }
          tableButton.setSelected(true);
        }
        

        if ((bookedTableList.contains(shopTable)) && (!addedTableListModel.contains(shopTable)) && (!releasedListModel.contains(shopTable))) {
          tableButton.setText("<html><body><center>#" + shopTable.getId() + "<br>" + "Cap:" + shopTable.getCapacity() + "<br/>Booked</center></body></html>");
          
          tableButton.setBackground(TableStatus.Booked.getBgColor());
          tableButton.setForeground(Color.BLACK);
          tableButton.setEnabled(false);
        }
        else {
          tableButton.setText("<html><body><center>#" + shopTable.getId() + "<br>" + "Cap:" + shopTable.getCapacity() + "</center></body></html>");
          tableButton.setBackground(Color.WHITE);
          tableButton.setForeground(Color.BLACK);
          tableButton.setEnabled(true);
        }
      }
    }
  }
  
  public void clearSelection()
  {
    dialog.selectedCapacity = 0;
    addedTableListModel.clear();
    releasedListModel.clear();
    Component[] components = floorPanel.getContentPane().getComponents();
    
    for (Component component : components)
      if ((component instanceof TableButton))
      {

        TableButton tableButton = (TableButton)component;
        tableButton.setSelected(false);
      }
  }
  
  public List<ShopTable> getReleasedListModel() {
    return releasedListModel;
  }
  
  public void setReleasedListModel(List<ShopTable> releasedListModel) {
    this.releasedListModel = releasedListModel;
  }
}
