package com.floreantpos.floorplan.ui;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.floorplan.ui.booking.BookingRequestUpdateForm;
import com.floreantpos.model.BookingInfo;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orocube.tablebooking.model.BookingRequest;
import com.orocube.tablebooking.model.UpdateInfo;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.ws.rs.core.GenericEntity;
import net.miginfocom.swing.MigLayout;


public class OnlineTableBookingBrowser
  extends TransparentPanel
{
  private JTable table;
  private OnlineTableBookingModel tableModel;
  private String SERVICE_URL = "http://localhost:8080/service/data/";
  private List<BookingRequest> bookingReqList;
  
  public OnlineTableBookingBrowser() {
    initComponents();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    
    tableModel = new OnlineTableBookingModel();
    table = new JTable(tableModel);
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    add(new JScrollPane(table));
    try
    {
      bookingReqList = getBookingReq();
      if (!bookingReqList.isEmpty())
        tableModel.setRows(bookingReqList);
    } catch (Exception e) {
      e.printStackTrace();
    }
    
    JPanel bottomPanel = new JPanel(new MigLayout("filly,align center", "", ""));
    
    JButton btnSync = new JButton("Sync");
    JButton btnUpdate = new JButton("Update");
    
    btnSync.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          tableModel.removeAll();
          bookingReqList = getBookingReq();
          if (!bookingReqList.isEmpty()) {
            tableModel.setRows(bookingReqList);
          }
        } catch (Exception es) {
          es.printStackTrace();
        }
        
      }
      
    });
    btnUpdate.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        OnlineTableBookingBrowser.this.updateRow();
      }
      

    });
    bottomPanel.add(btnSync);
    bottomPanel.add(btnUpdate);
    
    add(bottomPanel, "South");
  }
  
  private void getAllRequest() {}
  
  public List<BookingRequest> getBookingReq()
    throws Exception
  {
    Client client = Client.create();
    client.getProperties();
    WebResource webResource = client.resource(SERVICE_URL + "getBookingRequests");
    ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
    GenericType<List<BookingRequest>> listType = new GenericType() {};
    updateInfoUpdate();
    
    return (List)response.getEntity(listType);
  }
  
  private class OnlineTableBookingModel extends ListTableModel
  {
    public OnlineTableBookingModel()
    {
      super();
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      List<BookingRequest> bookingRequestList = getRows();
      
      BookingRequest bookingRequest = (BookingRequest)bookingRequestList.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return null;
      
      case 1: 
        return bookingRequest.getNumberOfGuest();
      
      case 2: 
        Date date = bookingRequest.getDate();
        DateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
        return formatter.format(date);
      
      case 3: 
        return bookingRequest.getDate();
      
      case 4: 
        return bookingRequest.isIsProcessed();
      

      case 5: 
        return bookingRequest.getStatus();
      }
      
      return null;
    }
    
    public void addModifier(BookingRequest category) {
      int size = getRows().size();
      getRows().add(category);
      fireTableRowsInserted(size, size);
    }
    
    public void deleteModifier(BookingRequest category, int index)
    {
      getRows().remove(category);
      fireTableRowsDeleted(index, index);
    }
    
    public void removeAll() {
      rows.clear();
      fireTableDataChanged();
    }
    
    public void addRows(List<BookingRequest> rows) {
      if (rows == null) {
        return;
      }
      for (BookingRequest row : rows) {
        addModifier(row);
      }
      fireTableDataChanged();
    }
  }
  
  private void updateRow()
  {
    try
    {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      BookingRequest bookingRequest = (BookingRequest)tableModel.getRowData(index);
      



      BookingRequestUpdateForm editor = new BookingRequestUpdateForm(bookingRequest);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
      


      if (uploadTableBooking(bookingReqList)) {
        POSMessageDialog.showMessage(this, "Successfully updated");
      }
      else {
        POSMessageDialog.showError("Something wrong!");
      }
      if (editor.getBookingInfo() != null) {
        BookingInfo bookingInfo = editor.getBookingInfo();
        POSMessageDialog.showMessage(this, "Your booking id is " + bookingInfo.getBookingId());
      }
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public boolean uploadTableBooking(List<BookingRequest> bookingRequestList) throws Exception {
    Client client = Client.create();
    client.getProperties();
    WebResource webResource = client.resource(SERVICE_URL + "updateBookingRequest");
    GenericEntity entity = new GenericEntity(bookingRequestList) {};
    try
    {
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).post(ClientResponse.class, entity);
      if (response.getStatus() == 200) {
        return true;
      }
    }
    catch (Exception ex) {
      throw new Exception(ex);
    }
    return false;
  }
  
  private void updateInfoUpdate() {
    Client client = Client.create();
    client.getProperties();
    UpdateInfo updateInfo = updateInfo();
    updateInfo.setUpdateAvailable(Boolean.valueOf(false));
    updateInfo.setCount(Integer.valueOf(0));
    WebResource webResource = client.resource(SERVICE_URL + "updateBookingRequestsUpdateInfo");
    GenericEntity<UpdateInfo> entity = new GenericEntity(updateInfo) {};
    try
    {
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).post(ClientResponse.class, entity);
      if (response.getStatus() == 200) {
        System.out.println("updateInfo UPdated");
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  
  private UpdateInfo updateInfo() {
    Client client = Client.create();
    client.getProperties();
    
    WebResource webResource = client.resource(SERVICE_URL + "getBookingRequestsUpdateInfo");
    ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
    GenericType<UpdateInfo> updateInfo = new GenericType() {};
    UpdateInfo ui = (UpdateInfo)response.getEntity(updateInfo);
    return ui;
  }
}
