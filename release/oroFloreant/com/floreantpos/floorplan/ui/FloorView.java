package com.floreantpos.floorplan.ui;

import com.floreantpos.POSConstants;
import com.floreantpos.actions.ReorderTicketAction;
import com.floreantpos.actions.SettleTicketAction;
import com.floreantpos.actions.SplitTicketAction;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.extension.OrderServiceFactory;
import com.floreantpos.main.Application;
import com.floreantpos.model.ShopFloor;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.ShopTableStatus;
import com.floreantpos.model.TableStatus;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.ShopFloorDAO;
import com.floreantpos.model.dao.ShopTableDAO;
import com.floreantpos.model.dao.ShopTableStatusDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.ImageComponent;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.ShopTableButton;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.OrderInfoDialog;
import com.floreantpos.ui.views.OrderInfoView;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.ui.views.order.TicketView;
import com.floreantpos.ui.views.payment.SplitedTicketSelectionDialog;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.TicketAlreadyExistsException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;

public class FloorView extends JPanel implements java.awt.event.MouseListener, MouseMotionListener
{
  private JLayeredPane floorPanel = new JLayeredPane();
  
  private PosScrollPane scrollPane;
  private Map<Integer, ShopTableButton> tableButtonMap = new HashMap();
  
  public List<Integer> addedTableListModel = new ArrayList();
  public List<Integer> removeTableListModel = new ArrayList();
  
  private MapTableSelectionView mapTableSelectionView;
  
  private TableLayoutView tableLayoutView;
  
  private ShopFloor floor;
  private int pressedX;
  private int pressedY;
  private Point dragLocation;
  private boolean rearrange;
  
  public FloorView(MapTableSelectionView mapTableSelectionView, TableLayoutView tableLayoutView, ShopFloor floor)
  {
    this.mapTableSelectionView = mapTableSelectionView;
    this.tableLayoutView = tableLayoutView;
    this.floor = floor;
    setLayout(new BorderLayout(10, 10));
    createLayoutPanel();
  }
  
  private void createLayoutPanel() {
    floorPanel.setOpaque(true);
    floorPanel.setBackground(Color.white);
    floorPanel.setAutoscrolls(true);
    
    scrollPane = new PosScrollPane(floorPanel);
    scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(50, 0));
    scrollPane.getHorizontalScrollBar().setPreferredSize(new Dimension(0, 50));
    
    add(scrollPane, "Center");
  }
  
  public void renderFloor() {
    floorPanel.removeAll();
    if (floor == null) {
      return;
    }
    Session session = null;
    try {
      session = ShopFloorDAO.getInstance().createNewSession();
      floor = ShopFloorDAO.getInstance().get(floor.getId(), session);
      ImageIcon image = floor.getImage();
      ImageComponent imageComponent = null;
      Dimension floorSize = floor.getFloorSize();
      floorPanel.setPreferredSize(floorSize);
      
      floorPanel.setOpaque(true);
      if (image != null) {
        imageComponent = new ImageComponent();
        imageComponent.setImage(image.getImage());
        imageComponent.setBounds(0, 0, (int)floorSize.getWidth(), (int)floorSize.getHeight());
        imageComponent.addMouseListener(this);
        imageComponent.addMouseMotionListener(this);
        floorPanel.add(imageComponent);
        if (hasBackgroundColor()) {
          floorPanel.setBackground(floor.getBackgroundColor());
        }
        else {
          floorPanel.setBackground(Color.WHITE);
        }
      }
      else if (hasBackgroundColor()) {
        floorPanel.setBackground(floor.getBackgroundColor());
      }
      else {
        floorPanel.setBackground(Color.WHITE);
      }
      renderTables();
      
      revalidate();
      repaint();
    } finally {
      ShopFloorDAO.getInstance().closeSession(session);
    }
  }
  
  private boolean hasBackgroundColor() {
    Color color = floor.getBackgroundColor();
    return color != null;
  }
  
  public void renderTables() {
    clearSelection();
    for (Component c : floorPanel.getComponents()) {
      if (!(c instanceof ImageComponent))
      {
        floorPanel.remove(c); }
    }
    Object tables = floor.getTables();
    if ((tables == null) || (((Set)tables).isEmpty())) {
      return;
    }
    int buttonWidth = TerminalConfig.getFloorButtonWidth();
    int buttonHeight = TerminalConfig.getFloorButtonHeight();
    int buttonFontSize = TerminalConfig.getFloorButtonFontSize();
    
    for (ShopTable shopTable : (Set)tables) {
      if (StringUtils.isEmpty(shopTable.getTicketId())) {
        shopTable.setTableStatus(TableStatus.Available);
      }
      ShopTableButton tableButton = (ShopTableButton)tableButtonMap.get(shopTable.getId());
      if (tableButton == null) {
        tableButton = new ShopTableButton(shopTable);
        tableButton.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e) {
            if (!rearrange) {
              FloorView.this.addTable(e);
            }
          }
        });
        tableButton.addMouseListener(this);
        tableButton.addMouseMotionListener(this);
        tableButtonMap.put(shopTable.getId(), tableButton);
      }
      else {
        shopTable.setShopTableStatus(ShopTableStatusDAO.getInstance().get(shopTable.getId()));
        tableButton.setShopTable(shopTable);
      }
      floorPanel.add(tableButton);
      tableButton.setBounds(shopTable.getX().intValue(), shopTable.getY().intValue(), buttonWidth, buttonHeight);
      tableButton.setFont(new Font(tableButton.getFont().getName(), 0, buttonFontSize));
      tableButton.update();
      if (addedTableListModel.contains(shopTable.getId())) {
        tableButton.setBackground(Color.GREEN);
      }
    }
    doImageMoveToBack();
  }
  
  public void updateTables() {
    clearSelection();
    Set<ShopTable> tables = floor.getTables();
    if ((tables == null) || (tables.isEmpty())) {
      return;
    }
    
    for (ShopTable shopTable : tables) {
      ShopTableButton tableButton = (ShopTableButton)tableButtonMap.get(shopTable.getId());
      if (tableButton != null)
      {


        tableButton.update();
        tableButton.repaint();
        
        floorPanel.repaint();
        doImageMoveToBack();
      }
    }
  }
  
  private void doImageMoveToBack() { Component[] components = getFloorPanel().getComponents();
    for (Component component : components) {
      if ((component instanceof ImageComponent)) {
        floorPanel.moveToBack(component);
      }
    }
  }
  
  private void addTable(ActionEvent e) {
    ShopTableButton button = (ShopTableButton)e.getSource();
    ShopTable storedShopTable = button.getShopTable();
    ShopTableStatus shopTaleStatus = storedShopTable.getStatus();
    if (!TerminalDAO.getInstance().isVersionEqual(ShopTableStatus.class, shopTaleStatus.getId(), shopTaleStatus.getVersion())) {
      POSMessageDialog.showMessage("Table has been changed in other terminal. Please reload this window and try again.");
      return;
    }
    
    int tableNumber = button.getId();
    ShopTable shopTable = ShopTableDAO.getInstance().getByNumber(tableNumber);
    shopTaleStatus = shopTable.getStatus();
    if (shopTable.getTableStatus() == null) {
      shopTable.setTableStatus(TableStatus.Available);
    }
    if (shopTable.getTableStatus().equals(TableStatus.Booked)) {
      return;
    }
    button.setShopTable(shopTable);
    button.initializeUser();
    button.initializeTicket();
    
    shopTable.setReArrange(storedShopTable.isReArrange());
    
    if (shopTable.isReArrange()) {
      return;
    }
    if (tableLayoutView.shouldGroup()) {
      if (addedTableListModel.contains(Integer.valueOf(button.getId()))) {
        return;
      }
      if (button.getShopTable().getTableStatus().equals(TableStatus.Seat)) {
        return;
      }
      
      button.setBackground(Color.green);
      button.setForeground(Color.black);
      addedTableListModel.add(Integer.valueOf(button.getId()));
      return;
    }
    
    if (tableLayoutView.shouldUnGroup()) {
      if (removeTableListModel.contains(button)) {
        return;
      }
      String ticketId = button.getShopTable().getTicketId();
      if (StringUtils.isEmpty(ticketId)) {
        return;
      }
      Ticket ticket = button.getTicket();
      
      for (Integer tableId : removeTableListModel) {
        ShopTableButton shopTableButton = (ShopTableButton)tableButtonMap.get(tableId);
        if (shopTableButton.getShopTable().getTicketId() != ticketId) {
          return;
        }
      }
      
      if (removeTableListModel.size() >= ticket.getTableNumbers().size() - 1) {
        return;
      }
      button.getShopTable().setTableStatus(TableStatus.Seat);
      button.setBackground(Color.white);
      button.setForeground(Color.black);
      removeTableListModel.add(Integer.valueOf(button.getId()));
      return;
    }
    
    if ((shopTaleStatus.hasMultipleTickets()) && (!tableLayoutView.shouldRelease())) {
      showSplitTickets(shopTaleStatus);
      return;
    }
    if (tableLayoutView.shouldHoldFire()) {
      performHoldFireTicket(button, shopTable);
      return;
    }
    
    if (tableLayoutView.shouldGuestCheck()) {
      showGuestCheckTicket(button, shopTable);
      return;
    }
    
    if (tableLayoutView.shouldSplitCheck()) {
      doSplitCheckTicket(button, shopTable);
      return;
    }
    
    if (tableLayoutView.shouldSettle()) {
      doSettleTicket(button, shopTable);
      return;
    }
    
    if (tableLayoutView.shouldRelease()) {
      doReleaseTable(button, shopTable);
      return;
    }
    
    if (tableLayoutView.shouldReorder()) {
      doReorderTicket(button, shopTable);
      return;
    }
    
    if ((!tableLayoutView.isComplete()) && (shopTable.getTableStatus().equals(TableStatus.Seat))) {
      if (!button.hasUserAccess()) {
        return;
      }
      if (mapTableSelectionView.isCreateNewTicket()) {
        editTicket(button.getTicket());
        closeDialog(false);
      }
    }
    
    if ((!tableLayoutView.isComplete()) && (shopTable.getTableStatus().equals(TableStatus.Seat)) && (!tableLayoutView.shouldGroup())) {
      if (!button.hasUserAccess()) {
        return;
      }
      if (mapTableSelectionView.isCreateNewTicket()) {
        editTicket(button.getTicket());
        closeDialog(false);
      }
      return;
    }
    

    if ((tableLayoutView.isSeat()) && (!tableLayoutView.shouldGroup()) && (!tableLayoutView.shouldUnGroup())) {
      if (addedTableListModel.contains(Integer.valueOf(button.getId()))) {
        return;
      }
      if (button.getShopTable().getTableStatus().equals(TableStatus.Seat)) {
        return;
      }
      
      button.setBackground(Color.green);
      button.setForeground(Color.black);
      addedTableListModel.add(Integer.valueOf(button.getId()));
      return;
    }
    if ((tableLayoutView.isComplete()) && (!tableLayoutView.shouldGroup()) && (!tableLayoutView.shouldUnGroup())) {
      doSettleTicket(button, shopTable);
      return;
    }
    
    if ((!tableLayoutView.shouldGroup()) && (!tableLayoutView.shouldUnGroup())) {
      addedTableListModel.clear();
      if (!addedTableListModel.contains(Integer.valueOf(button.getId()))) {
        addedTableListModel.add(Integer.valueOf(button.getId()));
      }
      if (mapTableSelectionView.isCreateNewTicket()) {
        doCreateNewTicket();
      }
      
      closeDialog(false);
    }
  }
  
  private void showSplitTickets(ShopTableStatus shopTaleStatus)
  {
    List<Ticket> splitTickets = new ArrayList();
    splitTickets.add(null);
    ShopTableButton button = (ShopTableButton)tableButtonMap.get(shopTaleStatus.getId());
    Ticket selectedTicket = button.getTicket();
    for (String ticketId : shopTaleStatus.getListOfTicketNumbers()) {
      if (selectedTicket.getId().equals(ticketId)) {
        splitTickets.add(selectedTicket);
      }
      else
        splitTickets.add(TicketDAO.getInstance().loadFullTicket(ticketId));
    }
    String action = "";
    if (tableLayoutView.shouldHoldFire()) {
      action = POSConstants.SEND_TO_KITCHEN;
    }
    else if (tableLayoutView.shouldGuestCheck()) {
      action = POSConstants.ORDER_INFO;
    }
    else if (tableLayoutView.shouldSettle()) {
      action = POSConstants.SETTLE;
    }
    else if (tableLayoutView.shouldSplitCheck()) {
      action = POSConstants.SPLIT_TICKET;
    }
    else if (tableLayoutView.shouldReorder()) {
      action = POSConstants.REORDER_TICKET_BUTTON_TEXT;
    }
    else {
      action = POSConstants.EDIT;
    }
    SplitedTicketSelectionDialog posDialog = new SplitedTicketSelectionDialog(splitTickets);
    List<ShopTable> selectedTables = getSelectedTables();
    ShopTableButton shopTableButton; if (selectedTables.isEmpty()) {
      shopTableButton = (ShopTableButton)tableButtonMap.get(shopTaleStatus.getId());
      selectedTables.add(shopTableButton.getShopTable());
    }
    posDialog.setSelectedTables(selectedTables);
    posDialog.setOrderType(mapTableSelectionView.getOrderType());
    posDialog.setSelectedAction(action);
    posDialog.setSize(800, 600);
    posDialog.open();
    if (!posDialog.isCanceled()) {
      for (ShopTable shopTable : selectedTables) {
        ShopTableButton tableButton = (ShopTableButton)tableButtonMap.get(shopTable.getId());
        ShopTableStatus shopTableStatus = tableButton.getShopTable().getStatus();
        ShopTableStatusDAO.getInstance().refresh(shopTableStatus);
        tableButton.update();
      }
    }
    if (action.equals(POSConstants.EDIT))
      closeDialog(false);
  }
  
  private void doReleaseTables() {
    Ticket ticket = mapTableSelectionView.getTicket();
    if (ticket != null) {
      List<Integer> tableNumbers = ticket.getTableNumbers();
      if ((tableNumbers != null) && (!tableNumbers.isEmpty()))
        removeTableListModel.addAll(tableNumbers);
    }
    ShopTableDAO.getInstance().updateTableStatus(removeTableListModel, null, null, false);
  }
  
  private boolean editTicket(Ticket ticket) {
    if (ticket == null) {
      return false;
    }
    closeDialog(false);
    TicketDAO.getInstance().loadFullTicket(ticket);
    
    OrderView.getInstance().setCurrentTicket(ticket);
    RootView.getInstance().showView("ORDER_VIEW");
    OrderView.getInstance().getTicketView().getTxtSearchItem().requestFocus();
    return true;
  }
  
  private void doSettleTicket(ShopTableButton button, ShopTable shopTable) {
    if (shopTable.getTableStatus().equals(TableStatus.Seat)) {
      if (!button.hasUserAccess()) {
        return;
      }
      
      showTicketSettleDialog(button.getTicket());
      closeDialog(false);
      
      clearSelection();
      return;
    }
    
    POSMessageDialog.showMessage("No Order has been submitted yet");
    clearSelection();
  }
  

  private void doReleaseTable(ShopTableButton button, ShopTable shopTable)
  {
    if (shopTable.getTableStatus().equals(TableStatus.Seat)) {
      if (!button.hasUserAccess()) {
        return;
      }
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Do you want to release this table?", "Release table:");
      if (option != 0) {
        clearSelection();
        return;
      }
      shopTable.setTableStatus(TableStatus.Available);
      removeTableListModel.add(shopTable.getId());
      ShopTableDAO.getInstance().updateTableStatus(removeTableListModel, null, null, false);
      closeDialog(false);
      clearSelection();
      renderFloor();
      return;
    }
    
    POSMessageDialog.showMessage("No Order has been submitted yet");
    clearSelection();
  }
  

  private void doReorderTicket(ShopTableButton button, ShopTable shopTable)
  {
    if (shopTable.getTableStatus().equals(TableStatus.Seat)) {
      if (!button.hasUserAccess()) {
        return;
      }
      showReorderDialog(button.getTicket());
      closeDialog(false);
      clearSelection();
      return;
    }
    
    POSMessageDialog.showMessage("No Order has been submitted yet");
    clearSelection();
  }
  

  private void doSplitCheckTicket(ShopTableButton button, ShopTable shopTable)
  {
    if (shopTable.getTableStatus().equals(TableStatus.Seat)) {
      if (!button.hasUserAccess()) {
        return;
      }
      

      splitCheckTicketView(button.getTicket());
      closeDialog(false);
      
      clearSelection();
      return;
    }
    
    POSMessageDialog.showMessage("No Order has been submitted yet");
    clearSelection();
  }
  

  private void showGuestCheckTicket(ShopTableButton button, ShopTable shopTable)
  {
    if (shopTable.getTableStatus().equals(TableStatus.Seat)) {
      if (!button.hasUserAccess()) {
        return;
      }
      

      showOrderView(button.getTicket());
      closeDialog(false);
      
      clearSelection();
      return;
    }
    

    POSMessageDialog.showMessage("No Order has been submitted yet");
    clearSelection();
  }
  

  private void performHoldFireTicket(ShopTableButton button, ShopTable shopTable)
  {
    if (shopTable.getTableStatus().equals(TableStatus.Seat)) {
      if (!button.hasUserAccess()) {
        return;
      }
      

      sendTicketOrderToKitchen(button.getTicket());
      closeDialog(false);
      
      clearSelection();
      return;
    }
    
    POSMessageDialog.showMessage("No Order has been submitted yet");
    clearSelection();
  }
  

  public void clearSelection()
  {
    tableLayoutView.clearSelection();
    addedTableListModel.clear();
    removeTableListModel.clear();
  }
  
  private boolean showTicketSettleDialog(Ticket ticket) {
    if (ticket == null) {
      return false;
    }
    closeDialog(false);
    
    new SettleTicketAction(ticket).execute();
    renderFloor();
    return true;
  }
  
  private boolean showReorderDialog(Ticket ticket)
  {
    if (ticket == null) {
      return false;
    }
    closeDialog(false);
    
    new ReorderTicketAction(ticket).execute();
    
    renderFloor();
    return true;
  }
  
  private boolean splitCheckTicketView(Ticket ticket)
  {
    if (ticket == null) {
      return false;
    }
    closeDialog(false);
    
    SplitTicketAction action = new SplitTicketAction(ticket);
    action.execute();
    
    return true;
  }
  
  private boolean showOrderView(Ticket ticket)
  {
    if (ticket == null) {
      return false;
    }
    closeDialog(false);
    
    Ticket ticketToEdit = TicketDAO.getInstance().loadFullTicket(ticket.getId());
    List<Ticket> ticketsToShow = new ArrayList();
    ticketsToShow.add(ticketToEdit);
    try
    {
      OrderInfoView view = new OrderInfoView(ticketsToShow);
      
      OrderInfoDialog dialog = new OrderInfoDialog(view);
      dialog.setSize(PosUIManager.getSize(400), PosUIManager.getSize(600));
      dialog.setDefaultCloseOperation(2);
      dialog.setLocationRelativeTo(Application.getPosWindow());
      dialog.setVisible(true);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return true;
  }
  
  private boolean sendTicketOrderToKitchen(Ticket ticket) {
    if (ticket == null) {
      return false;
    }
    closeDialog(false);
    
    Ticket ticketToEdit = TicketDAO.getInstance().loadFullTicket(ticket.getId());
    
    OrderView.getInstance().setCurrentTicket(ticketToEdit);
    OrderView.getInstance().getTicketView().sendTicketToKitchen();
    if (ticketToEdit.needsKitchenPrint()) {
      POSMessageDialog.showMessage("Ticket successfully sent to kitchen");
    } else {
      POSMessageDialog.showMessage("Ticket has already sent to kitchen");
    }
    return true;
  }
  
  private void closeDialog(boolean canceled) {
    Window windowAncestor = SwingUtilities.getWindowAncestor(this);
    if ((windowAncestor instanceof POSDialog)) {
      ((POSDialog)windowAncestor).setCanceled(canceled);
      windowAncestor.dispose();
    }
  }
  
  public void doGroupAction() {
    if (mapTableSelectionView.isCreateNewTicket()) {
      doCreateNewTicket();
    }
    closeDialog(false);
  }
  
  public void doCreateNewTicket() {
    try {
      List<ShopTable> selectedTables = getSelectedTables();
      
      if (selectedTables.isEmpty()) {
        clearSelection();
        return;
      }
      OrderServiceFactory.getOrderService().createNewTicket(mapTableSelectionView.getOrderType(), selectedTables, null);
      clearSelection();
    }
    catch (TicketAlreadyExistsException localTicketAlreadyExistsException) {}
  }
  

  public void doUnGroupAction()
  {
    if ((removeTableListModel == null) || (removeTableListModel.isEmpty())) {
      return;
    }
    ShopTableButton firstTable = (ShopTableButton)tableButtonMap.get(removeTableListModel.get(0));
    if (!firstTable.hasUserAccess()) {
      return;
    }
    List<ShopTable> removableTables = new ArrayList();
    Ticket ticketToEdit = null;
    for (Integer tableId : removeTableListModel) {
      ShopTableButton tblButton = (ShopTableButton)tableButtonMap.get(tableId);
      if (ticketToEdit == null)
        ticketToEdit = tblButton.getTicket();
      if (addedTableListModel.contains(tableId)) {
        addedTableListModel.remove(tableId);
      }
      removableTables.add(tblButton.getShopTable());
    }
    if (!mapTableSelectionView.isCreateNewTicket()) {
      closeDialog(false);
    }
  }
  
  public JLayeredPane getFloorPanel() {
    return floorPanel;
  }
  
  public List<ShopTable> getSelectedTables() {
    List<ShopTable> tables = new ArrayList();
    
    for (Integer tableId : addedTableListModel) {
      ShopTableButton shopTableButton = (ShopTableButton)tableButtonMap.get(tableId);
      tables.add(shopTableButton.getShopTable());
    }
    return tables;
  }
  
  public ShopFloor getFloor() {
    return floor;
  }
  
  public void mouseDragged(MouseEvent e)
  {
    try
    {
      if (!rearrange) {
        return;
      }
      Object object = e.getSource();
      if ((object instanceof ShopTableButton)) {
        ShopTableButton tableBtn = (ShopTableButton)e.getSource();
        
        int x = e.getX() - pressedX + getLocationx;
        int y = e.getY() - pressedY + getLocationy;
        
        int floorWidth = floorPanel.getBounds().width - 40;
        int floorHeight = floorPanel.getBounds().height - 40;
        if ((x < 0) || (x > floorWidth)) {
          return;
        }
        
        if ((y < 0) || (y > floorHeight)) {
          return;
        }
        
        tableBtn.getShopTable().setX(Integer.valueOf(x));
        tableBtn.getShopTable().setY(Integer.valueOf(y));
        
        ShopTableDAO.getInstance().saveOrUpdate(tableBtn.getShopTable());
        
        tableBtn.setLocation(x, y);
        repaint();
      }
      else if ((object instanceof ImageComponent)) {
        ImageComponent imageComponent = (ImageComponent)object;
        floorPanel.moveToBack(imageComponent);
        Dimension preferredSize = imageComponent.getSize();
        
        if ((getCursor().getType() == 11) && (dragLocation.getX() <= getWidth() - 5)) {
          int w = (int)(preferredSize.getWidth() + (e.getPoint().getX() - dragLocation.getX()));
          imageComponent.setSize(w, (int)preferredSize.getHeight());
          floor.setWidth(Integer.valueOf(w));
        }
        else if ((getCursor().getType() == 9) && (dragLocation.getY() <= getHeight() - 5)) {
          int h = (int)(preferredSize.getHeight() + (e.getPoint().getY() - dragLocation.getY()));
          imageComponent.setSize((int)preferredSize.getWidth(), h);
          floor.setHeight(Integer.valueOf(h));
        }
        
        ShopFloorDAO.getInstance().saveOrUpdate(floor);
        dragLocation = e.getPoint();
        imageComponent.revalidate();
        imageComponent.repaint();
      }
    } catch (Exception e2) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), Messages.getString("FloorView.24"));
    }
  }
  
  public void mouseMoved(MouseEvent e)
  {
    Object object = e.getSource();
    this.dragLocation = e.getPoint();
    if (!rearrange) {
      setCursor(Cursor.getDefaultCursor());
      return;
    }
    if ((object instanceof ImageComponent)) {
      ImageComponent imageComponent = (ImageComponent)object;
      Point dragLocation = e.getPoint();
      Dimension preferredSize = imageComponent.getSize();
      if ((x >= width - 10) && (x <= width - 2)) {
        setCursor(Cursor.getPredefinedCursor(11));
      }
      else if ((y >= height - 10) && (y <= height - 2)) {
        setCursor(Cursor.getPredefinedCursor(9));
      }
      else {
        setCursor(Cursor.getDefaultCursor());
      }
    }
    else {
      setCursor(Cursor.getDefaultCursor());
    }
  }
  

  public void mouseClicked(MouseEvent e) {}
  

  public void mousePressed(MouseEvent e)
  {
    pressedX = e.getX();
    pressedY = e.getY();
    dragLocation = e.getPoint();
  }
  


  public void mouseReleased(MouseEvent e) {}
  

  public void mouseEntered(MouseEvent e) {}
  

  public void mouseExited(MouseEvent e) {}
  

  public boolean isRearrange()
  {
    return rearrange;
  }
  
  public void setRearrange(boolean rearrange) {
    this.rearrange = rearrange;
  }
}
