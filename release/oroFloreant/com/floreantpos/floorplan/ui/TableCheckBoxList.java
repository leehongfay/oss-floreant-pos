package com.floreantpos.floorplan.ui;

import com.floreantpos.model.ShopTable;
import com.floreantpos.swing.CheckBoxList;
import com.floreantpos.swing.CheckBoxList.Entry;
import com.floreantpos.swing.CheckBoxListModel;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;




public class TableCheckBoxList<E>
  extends CheckBoxList
{
  public TableCheckBoxList() {}
  
  public void setModel(List items)
  {
    setModel(new TableCheckBoxListModel(items));
    init();
  }
  
  public List getCheckedValues()
  {
    List values = new ArrayList();
    TableCheckBoxListModel model = (TableCheckBoxListModel)getModel();
    for (int i = 0; i < items.size(); i++) {
      CheckBoxList.Entry<E> entry = (CheckBoxList.Entry)items.get(i);
      if (checked) {
        values.add(value);
      }
    }
    return values;
  }
  
  public void unCheckAll()
  {
    TableCheckBoxListModel model = (TableCheckBoxListModel)getModel();
    for (int i = 0; i < items.size(); i++) {
      CheckBoxList.Entry entry = (CheckBoxList.Entry)items.get(i);
      checked = false;
    }
    

    model.fireTableRowsUpdated(0, model.getRowCount());
  }
  
  public void selectItems(List types)
  {
    TableCheckBoxListModel model = (TableCheckBoxListModel)getModel();
    
    if (types != null) {
      for (int i = 0; i < items.size(); i++) {
        CheckBoxList.Entry entry = (CheckBoxList.Entry)items.get(i);
        
        for (int j = 0; j < types.size(); j++)
        {
          Object type = types.get(j);
          
          if (type.equals(value)) {
            checked = true;
            break;
          }
        }
      }
      model.fireTableRowsUpdated(0, model.getRowCount());
    }
  }
  
  public TableCellRenderer getCellRenderer(int row, int column)
  {
    if (column == 0) {
      return super.getCellRenderer(row, column);
    }
    DefaultTableCellRenderer center = new DefaultTableCellRenderer();
    center.setHorizontalAlignment(0);
    getColumnModel().getColumn(column).setCellRenderer(center);
    return super.getCellRenderer(row, column);
  }
  
  public void init()
  {
    getSelectionModel().setSelectionMode(0);
    setShowGrid(true);
    setAutoResizeMode(3);
    TableColumn column = getColumnModel().getColumn(0);
    int checkBoxWidth = JCheckBoxgetPreferredSizewidth;
    column.setPreferredWidth(checkBoxWidth);
    column.setMinWidth(checkBoxWidth);
    column.setWidth(checkBoxWidth);
    column.setMaxWidth(checkBoxWidth);
    column.setResizable(false);
    
    setTableHeader(null);
  }
  
  public static class TableCheckBoxListModel<E> extends CheckBoxListModel<E> {
    List<CheckBoxList.Entry<E>> items;
    
    TableCheckBoxListModel(List<E> _items) {
      super();
      items = new ArrayList(_items.size());
      for (int i = 0; i < _items.size(); i++) {
        items.add(createEntry(_items.get(i)));
      }
    }
    
    public int getColumnCount()
    {
      return 3;
    }
    

    public String getColumnName(int col)
    {
      switch (col) {
      case 1: 
        return "Table Number";
      
      case 2: 
        return "Capacity";
      }
      
      return null;
    }
    

    public Object getValueAt(int row, int col)
    {
      CheckBoxList.Entry entry = (CheckBoxList.Entry)items.get(row);
      switch (col)
      {
      case 0: 
        return Boolean.valueOf(checked);
      

      case 1: 
        return value;
      case 2: 
        return ((ShopTable)value).getCapacity();
      }
      throw new InternalError();
    }
    

    public void setValueAt(Object value, int row, int col)
    {
      if (col == 0) {
        CheckBoxList.Entry entry = (CheckBoxList.Entry)items.get(row);
        checked = value.equals(Boolean.TRUE);
        
        fireTableRowsUpdated(row, row);
      }
    }
  }
}
