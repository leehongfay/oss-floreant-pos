package com.floreantpos.floorplan.ui;

import com.floreantpos.config.ui.ConfigurationView;
import com.floreantpos.model.ShopFloor;
import com.floreantpos.model.ShopFloorTemplate;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.dao.ShopFloorDAO;
import com.floreantpos.model.dao.ShopFloorTemplateDAO;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class FloorConfigurationView
  extends ConfigurationView
{
  private DefaultListModel<ShopFloor> listModel = new DefaultListModel();
  private JList<ShopFloor> floorsList = new JList(listModel);
  private FloorConfigurationFloorView floorView = new FloorConfigurationFloorView(floorsList);
  
  private JFileChooser fileChooser;
  private DefaultListModel<ShopFloorTemplate> listTemplateModel = new DefaultListModel();
  private JList<ShopFloorTemplate> floorTemplatesList = new JList(listTemplateModel);
  private JButton btnAddFloor;
  private JButton btnRemoveTemplate;
  private boolean deleteFloor;
  
  public FloorConfigurationFloorView getFloorView() {
    return floorView;
  }
  
  public boolean isDeleteFloor() {
    return deleteFloor;
  }
  
  public FloorConfigurationView() {
    initUI();
  }
  
  private void initUI() {
    setLayout(new BorderLayout(10, 10));
    
    renderFloorsList();
    
    add(floorView);
  }
  
  private void renderFloorsList() {
    JPanel floorListPanel = new JPanel(new BorderLayout(5, 5));
    floorListPanel.setBorder(BorderFactory.createTitledBorder("Floors"));
    
    JScrollPane listScrollPane = new JScrollPane(floorsList);
    listScrollPane.setPreferredSize(new Dimension(150, 100));
    floorListPanel.add(listScrollPane);
    
    floorsList.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e) {
        ShopFloor selectedValue = (ShopFloor)floorsList.getSelectedValue();
        if ((selectedValue == null) || (selectedValue == floorView.getFloor())) {
          return;
        }
        
        floorView.setFloor(selectedValue);
        List<ShopFloorTemplate> templates = ShopFloorTemplateDAO.getInstance().findByParent(selectedValue);
        listTemplateModel.clear();
        if ((templates != null) && (!templates.isEmpty())) {
          ShopFloorTemplate defaultTemplate = null;
          for (ShopFloorTemplate template : templates) {
            if (template.isDefaultFloor().booleanValue()) {
              defaultTemplate = template;
            }
            listTemplateModel.addElement(template);
          }
          floorTemplatesList.setSelectedValue(defaultTemplate, true);
        }
        
      }
    });
    JPanel buttonPanel = new JPanel();
    JButton btnAddFloor = new JButton("ADD");
    btnAddFloor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        FloorConfigurationView.this.addNewFloor();
      }
    });
    buttonPanel.add(btnAddFloor);
    
    JButton btnRemoveFloor = new JButton("REMOVE");
    btnRemoveFloor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        FloorConfigurationView.this.clearTemplates();
        if (isDeleteFloor()) {
          FloorConfigurationView.this.removeSelectedFloor();
        }
        
      }
    });
    buttonPanel.add(btnRemoveFloor);
    
    JPanel leftPanel = new JPanel(new BorderLayout(5, 5));
    floorListPanel.add(buttonPanel, "South");
    leftPanel.add(floorListPanel, "Center");
    renderFloorTemplatesList(leftPanel);
    add(leftPanel, "West");
  }
  
  private void renderFloorTemplatesList(JPanel leftPanel) {
    JPanel floorTemplateListPanel = new JPanel(new BorderLayout(5, 5));
    floorTemplateListPanel.setBorder(BorderFactory.createTitledBorder("Templates"));
    
    JScrollPane listScrollPane = new JScrollPane(floorTemplatesList);
    listScrollPane.setPreferredSize(new Dimension(150, 100));
    floorTemplateListPanel.add(listScrollPane);
    
    floorTemplatesList.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e) {
        ShopFloorTemplate selectedValue = (ShopFloorTemplate)floorTemplatesList.getSelectedValue();
        if (selectedValue == null) {
          btnAddFloor.setEnabled(false);
          btnRemoveTemplate.setEnabled(false);
          return;
        }
        
        floorView.setFloorTemplate(floorTemplatesList, selectedValue);
        btnAddFloor.setEnabled(true);
        btnRemoveTemplate.setEnabled(true);
      }
      
    });
    JPanel buttonPanel = new JPanel();
    
    btnAddFloor = new JButton("SET AS SCREEN");
    btnAddFloor.setEnabled(false);
    
    btnAddFloor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        saveAsFloorScreen();
      }
    });
    buttonPanel.add(btnAddFloor);
    
    btnRemoveTemplate = new JButton("REMOVE");
    btnRemoveTemplate.setEnabled(false);
    btnRemoveTemplate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        Window window = SwingUtilities.getWindowAncestor(FloorConfigurationView.this);
        ShopFloorTemplate selectedValue = (ShopFloorTemplate)floorTemplatesList.getSelectedValue();
        if (selectedValue == null) {
          return;
        }
        int option = JOptionPane.showOptionDialog(window, "Are you sure to remove selected template?", "Confirm", 0, 3, null, null, null);
        
        if (option != 0) {
          return;
        }
        listTemplateModel.removeElement(selectedValue);
        ShopFloorTemplateDAO.getInstance().delete(selectedValue);
        floorTemplatesList.setSelectedIndex(listTemplateModel.getSize() - 1);
      }
    });
    buttonPanel.add(btnRemoveTemplate);
    
    floorTemplateListPanel.add(buttonPanel, "South");
    
    leftPanel.add(floorTemplateListPanel, "South");
  }
  
  protected void saveAsFloorScreen() {
    ShopFloorTemplate template = (ShopFloorTemplate)floorTemplatesList.getSelectedValue();
    












    copyAndSaveAsScreen(template);
  }
  
  private void copyAndSaveAsScreen(ShopFloorTemplate template) {
    ShopFloor selectedFloor = (ShopFloor)floorsList.getSelectedValue();
    if (selectedFloor.getTables() == null) {
      POSMessageDialog.showError(this, "Cannot set default since this floor has no table.");
      return;
    }
    for (ShopTable table : selectedFloor.getTables()) {
      String position = template.getProperty(String.valueOf(table.getId()));
      if (position != null) {
        String[] split = position.split(",");
        table.setX(Integer.valueOf(split[0]));
        table.setY(Integer.valueOf(split[1]));
      }
    }
    ShopFloorTemplateDAO.getInstance().setDefaultTemplate(template, selectedFloor);
  }
  
  public boolean save() throws Exception
  {
    ShopFloorDAO dao = ShopFloorDAO.getInstance();
    
    Session session = null;
    Transaction tx = null;
    
    try
    {
      session = dao.getSession();
      tx = session.beginTransaction();
      
      int size = listModel.getSize();
      for (i = 0; i < size; i++) {
        ShopFloor shopFloor = (ShopFloor)listModel.get(i);
        session.saveOrUpdate(shopFloor);
      }
      
      tx.commit();
      return 1;
    } catch (Exception e) {
      int i;
      tx.rollback();
      POSMessageDialog.showError(SwingUtilities.windowForComponent(this), e.getMessage(), e);
      return 0;
    } finally {
      dao.closeSession(session);
    }
  }
  
  public void initialize() throws Exception
  {
    listModel.clear();
    
    List<ShopFloor> shopFloors = ShopFloorDAO.getInstance().findAll();
    for (ShopFloor shopFloor : shopFloors) {
      listModel.addElement(shopFloor);
    }
    



    if (listModel.getSize() > 0) {
      floorsList.setSelectedIndex(0);
    }
    floorView.setFloorTemplate(floorTemplatesList, null);
    
    setInitialized(true);
  }
  
  public String getName()
  {
    return "Floor Plan";
  }
  
  private void addNewFloor() {
    try {
      ShopFloor floor = new ShopFloor();
      floor.setName("Untitled");
      listModel.addElement(floor);
      floorsList.setSelectedIndex(listModel.getSize() - 1);
      floorView.setFloor(floor);
    }
    catch (Exception e1) {
      POSMessageDialog.showError(this, e1.getMessage(), e1);
    }
  }
  
  private void clearTemplates() {
    Window window = SwingUtilities.getWindowAncestor(this);
    ShopFloor floor = (ShopFloor)floorsList.getSelectedValue();
    
    if (floor == null) {
      POSMessageDialog.showError(window, "Please select floor to remove");
      return;
    }
    List<ShopFloorTemplate> templateList = new ArrayList();
    Enumeration<ShopFloorTemplate> elements = listTemplateModel.elements();
    while (elements.hasMoreElements()) {
      ShopFloorTemplate item = (ShopFloorTemplate)elements.nextElement();
      templateList.add(item);
    }
    int option = JOptionPane.showOptionDialog(window, "Are you sure to remove selected floor?", "Confirm", 0, 3, null, null, null);
    
    if (option == 0) {
      listTemplateModel.clear();
      ShopFloorTemplateDAO.getInstance().deleteTemplates(templateList);
      deleteFloor = true;

    }
    else if (option == 1) {
      deleteFloor = false;
    }
  }
  
  private void removeSelectedFloor()
  {
    try {
      Window window = SwingUtilities.getWindowAncestor(this);
      ShopFloor floor = (ShopFloor)floorsList.getSelectedValue();
      











      ShopFloorDAO dao = ShopFloorDAO.getInstance();
      if (floor != null) {
        dao.delete(floor);
        floorView.setSaveRequired(false);
        listModel.remove(floorsList.getSelectedIndex());
      }
      
      floorView.setFloor(null);



    }
    catch (Exception e)
    {


      POSMessageDialog.showError(this, e.getMessage(), e);
    }
  }
}
