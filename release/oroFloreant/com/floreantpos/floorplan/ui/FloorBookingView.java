package com.floreantpos.floorplan.ui;

import com.floreantpos.PosLog;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.extension.OrderServiceFactory;
import com.floreantpos.floorplan.ui.booking.BookingTableSelectionDialog;
import com.floreantpos.model.BookingInfo;
import com.floreantpos.model.BookingStatus;
import com.floreantpos.model.Customer;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.dao.BookingInfoDAO;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.TicketAlreadyExistsException;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;

public class FloorBookingView
  extends JPanel
  implements ActionListener
{
  private BookingInfo tableBookingInfo;
  private JLabel lblCustomerName;
  private JLabel lblBookingDate;
  private SimpleDateFormat dt = new SimpleDateFormat("hh:mm a");
  private JButton btnStatus;
  private MapTableSelectionView mapTableSelectionView;
  private StatusManagementDialog dialog;
  private JButton btnSettings;
  private JButton btnModify;
  private JButton btnTimer;
  private JButton btnMobile;
  private JLabel lblCustomerPhone;
  
  public FloorBookingView(BookingInfo tableBookingInfo, MapTableSelectionView mapTableSelectionView) {
    this.tableBookingInfo = tableBookingInfo;
    this.mapTableSelectionView = mapTableSelectionView;
    initComponents();
    updateView();
  }
  
  private void initComponents() {
    try {
      setLayout(new MigLayout("fillx"));
      setPreferredSize(PosUIManager.getSize(200, 0));
      setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
      
      lblCustomerName = new JLabel();
      lblCustomerPhone = new JLabel();
      lblBookingDate = new JLabel("", 4);
      btnStatus = new JButton();
      btnSettings = new JButton();
      btnModify = new JButton();
      btnTimer = new JButton();
      btnMobile = new JButton();
      
      Image imgSettings = ImageIO.read(getClass().getResource("/com/floreantpos/floorlayout/settings.png"));
      btnSettings.setIcon(new ImageIcon(imgSettings));
      
      Image imgModify = ImageIO.read(getClass().getResource("/com/floreantpos/floorlayout/modify.png"));
      btnModify.setIcon(new ImageIcon(imgModify));
      
      Image imgTimer = ImageIO.read(getClass().getResource("/com/floreantpos/floorlayout/timer.png"));
      btnTimer.setIcon(new ImageIcon(imgTimer));
      
      Image imgMobile = ImageIO.read(getClass().getResource("/com/floreantpos/floorlayout/mobile.png"));
      btnMobile.setIcon(new ImageIcon(imgMobile));
      
      btnModify.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          FloorBookingView.this.doShowStatusManagementDialog();
        }
        
      });
      add(lblCustomerName, "span, growx");
      add(lblCustomerPhone);
      add(lblBookingDate, "right, wrap");
      JPanel iconPanel = new JPanel(new MigLayout("fillx,insets 0 0 0 0"));
      
      iconPanel.add(btnSettings, "growx");
      iconPanel.add(btnModify, "growx");
      iconPanel.add(btnTimer, "growx");
      iconPanel.add(btnMobile, "growx");
      add(iconPanel, "span,growx");
      

      btnSettings.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          FloorBookingView.this.doShowBookingInfoDialog();
        }
      });
    } catch (Exception e1) {
      PosLog.error(FloorBookingView.class, e1);
    }
  }
  
  private void updateView() {
    if (tableBookingInfo == null) {
      return;
    }
    
    if (tableBookingInfo.getCustomer() != null) {
      lblCustomerName.setText(tableBookingInfo.getCustomer().getName());
    }
    btnStatus.setText(tableBookingInfo.getStatus());
    if (tableBookingInfo.getCustomer() != null) {
      lblCustomerPhone.setText(tableBookingInfo.getCustomer().getMobileNo());
    }
    lblBookingDate.setText(dt.format(tableBookingInfo.getFromDate()));
  }
  
  public void actionPerformed(ActionEvent e)
  {
    try {
      Object object = e.getActionCommand();
      if (object.equals("seat")) {
        doAllocateTable();
      }
      else if (!object.equals(BookingStatus.Complete.toString()))
      {
        if (!object.equals("Ok"))
        {
          if (!object.equals(BookingStatus.Cancel.toString())) {} }
      }
      dialog.dispose();
    } catch (Exception e1) {
      PosLog.error(FloorBookingView.class, e1);
    }
  }
  
  private void doAllocateTable() throws TicketAlreadyExistsException {
    if (tableBookingInfo == null) {
      return;
    }
    List<ShopTable> selectedTables = tableBookingInfo.getTables();
    int numberOfGuest = tableBookingInfo.getGuestCount().intValue();
    if (numberOfGuest == 0) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Guest number should not be zero.");
      return;
    }
    if ((selectedTables == null) || (selectedTables.size() == 0)) {
      Date fromDate = tableBookingInfo.getFromDate();
      Date toDate = tableBookingInfo.getToDate();
      if ((fromDate == null) || (toDate == null)) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("TableBookingForm.30"));
        return;
      }
      
      Date today = new Date();
      if (fromDate.getTime() < today.getTime()) {
        Calendar calendar = Calendar.getInstance();
        int hourOfTheDay = calendar.get(10);
        calendar.set(10, hourOfTheDay);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        fromDate = calendar.getTime();
        int setHour = hourOfTheDay + 1;
        calendar.set(10, setHour);
        toDate = calendar.getTime();
        
        tableBookingInfo.setFromDate(fromDate);
        tableBookingInfo.setToDate(toDate);
        BookingInfoDAO.getInstance().saveOrUpdate(tableBookingInfo);
      }
      
      BookingTableSelectionDialog tableSelectionDialog = BookingTableSelectionDialog.getInstance();
      tableSelectionDialog.setStartDate(tableBookingInfo.getFromDate());
      tableSelectionDialog.setEndDate(tableBookingInfo.getToDate());
      tableSelectionDialog.setGuestCount(tableBookingInfo.getGuestCount().intValue());
      tableSelectionDialog.setSelectedTables(tableBookingInfo.getTables());
      tableSelectionDialog.clearAllFloorSelection(false);
      tableSelectionDialog.updateView();
      tableSelectionDialog.openFullScreen();
      
      if (tableSelectionDialog.isCanceled()) {
        return;
      }
      selectedTables = tableSelectionDialog.getSelectedTables();
      tableBookingInfo.setTables(selectedTables);
      BookingInfoDAO.getInstance().saveOrUpdate(tableBookingInfo);
    }
    
    Customer customer = tableBookingInfo.getCustomer();
    
    OrderServiceFactory.getOrderService().createNewTicket(mapTableSelectionView.getOrderType(), selectedTables, customer, numberOfGuest);
  }
  
  private List<ShopTable> removeDuplicate(List<ShopTable> allTables)
  {
    Collections.sort(allTables, new Comparator()
    {
      public int compare(ShopTable o1, ShopTable o2) {
        return o1.getId().compareTo(o2.getId());
      }
      
    });
    Set removeDuplicate = new LinkedHashSet();
    removeDuplicate.addAll(allTables);
    
    ArrayList allDuplicateFreeTables = new ArrayList();
    allDuplicateFreeTables.addAll(removeDuplicate);
    
    return allDuplicateFreeTables;
  }
  
  private void doShowStatusManagementDialog() {
    dialog = new StatusManagementDialog(tableBookingInfo, this);
    dialog.setTitle("Status");
    dialog.setDefaultCloseOperation(2);
    dialog.setSize(PosUIManager.getSize(500, 230));
    dialog.setLocationRelativeTo(POSUtil.getFocusedWindow());
    dialog.setVisible(true);
  }
  
  private void doShowBookingInfoDialog() {
    BookingInfo cloneBookingInfo = tableBookingInfo.clone(tableBookingInfo);
    POSDialog posDialog = new POSDialog();
    posDialog.setLayout(new MigLayout("fill"));
    posDialog.add(new AddTableBookingView(posDialog, new TableBookingForm(cloneBookingInfo)), "grow");
    posDialog.setTitle("Add Booking");
    posDialog.setDefaultCloseOperation(2);
    posDialog.openFullScreen();
    
    if (!posDialog.isCanceled()) {
      mapTableSelectionView.setCustomerBookingInfoPanel();
    } else {
      BookingInfoDAO.getInstance().saveOrUpdate(tableBookingInfo);
    }
  }
}
