package com.floreantpos.floorplan.ui;

import com.floreantpos.config.TerminalConfig;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.TableStatus;
import com.floreantpos.model.dao.ShopTableDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.ShopTableButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;

public class TableStatusDialog
  extends POSDialog
{
  private JRadioButton rbFree;
  private JRadioButton rbBooked;
  private JRadioButton rbDirty;
  private JRadioButton rbServing;
  private JRadioButton rbDisable;
  private ShopTable table;
  private ShopTableButton shopTableButton;
  
  public TableStatusDialog()
  {
    createUI();
  }
  
  public TableStatusDialog(ShopTable table, ShopTableButton button) {
    this.table = table;
    shopTableButton = button;
    createUI();
    updateView();
  }
  
  private void updateView()
  {
    if (table != null) {
      rbFree.setSelected(true);
      rbServing.setSelected(table.getTableStatus().equals(TableStatus.Seat));
      rbBooked.setSelected(table.getTableStatus().equals(TableStatus.Booked));
      rbDirty.setSelected(table.getTableStatus().equals(TableStatus.Dirty));
      rbDisable.setSelected(table.getTableStatus().equals(TableStatus.Disable));
    }
  }
  
  private void createUI()
  {
    setTitle(Messages.getString("TableStatusDialog.0"));
    
    rbFree = new JRadioButton(Messages.getString("TableStatusDialog.1"));
    rbBooked = new JRadioButton(Messages.getString("TableStatusDialog.2"));
    rbDirty = new JRadioButton(Messages.getString("TableStatusDialog.3"));
    rbServing = new JRadioButton(Messages.getString("TableStatusDialog.4"));
    rbDisable = new JRadioButton(Messages.getString("TableStatusDialog.5"));
    
    ButtonGroup buttonGroup = new ButtonGroup();
    buttonGroup.add(rbFree);
    buttonGroup.add(rbBooked);
    buttonGroup.add(rbDirty);
    buttonGroup.add(rbServing);
    buttonGroup.add(rbDisable);
    
    JPanel statusPanel = new JPanel();
    statusPanel.setLayout(new FlowLayout(1, 5, 5));
    statusPanel.add(rbFree);
    statusPanel.add(rbBooked);
    statusPanel.add(rbDirty);
    statusPanel.add(rbServing);
    statusPanel.add(rbDisable);
    
    add(statusPanel);
    
    JPanel panel = new JPanel();
    panel.setLayout(new BorderLayout());
    panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle(Messages.getString("TableStatusDialog.6"));
    
    panel.add(titlePanel);
    add(panel, "North");
    
    JPanel footerPanel = new JPanel(new BorderLayout());
    footerPanel.add(new JSeparator(), "North");
    JPanel buttonPanel = new JPanel(new MigLayout("fill"));
    
    footerPanel.add(buttonPanel);
    getContentPane().add(footerPanel, "South");
    
    PosButton btnOk = new PosButton();
    btnOk.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (rbFree.isSelected()) {
          table.setTableStatus(TableStatus.Available);
        }
        else if (rbServing.isSelected()) {
          table.setTableStatus(TableStatus.Seat);
        }
        else if (rbBooked.isSelected()) {
          table.setTableStatus(TableStatus.Booked);
        }
        else if (rbDirty.isSelected()) {
          table.setTableStatus(TableStatus.Dirty);
        }
        else if (rbDisable.isSelected()) {
          table.setTableStatus(TableStatus.Disable);
        }
        
        if (table != null) {
          ShopTableDAO.getInstance().saveOrUpdate(table);
          dispose();
          shopTableButton.update();
        }
        
      }
    });
    btnOk.setText(Messages.getString("TableStatusDialog.8"));
    btnOk.setPreferredSize(new Dimension(70, TerminalConfig.getTouchScreenButtonHeight()));
    buttonPanel.add(btnOk, "split 2,align center");
    
    PosButton btnCancel = new PosButton();
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        dispose();
      }
    });
    btnCancel.setText(Messages.getString("TableStatusDialog.10"));
    buttonPanel.add(btnCancel);
  }
}
