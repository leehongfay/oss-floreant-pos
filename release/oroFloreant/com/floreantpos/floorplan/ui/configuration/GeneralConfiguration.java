package com.floreantpos.floorplan.ui.configuration;

import com.floreantpos.main.Application;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;




public class GeneralConfiguration
  extends TransparentPanel
  implements ActionListener
{
  private static final long serialVersionUID = 1L;
  private Terminal terminal;
  private JRadioButton rbShowTableNumber;
  private JRadioButton rbShowTableName;
  private JCheckBox chkShowServerName;
  private JCheckBox chkShowTokenNum;
  private IntegerTextField tfPrimaryFontSize;
  private IntegerTextField tfSecondaryFontSize;
  private Color seatForeColor;
  private Color seatBgColor;
  private Map<String, JButton> buttonMap = new HashMap();
  private String[] colArrays;
  private Color freeForeColor;
  private Color freeBgColor;
  private Color bookForeColor;
  private Color bookBgColor;
  
  public GeneralConfiguration() {
    initComponents();
    updateView();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    setBorder(new TitledBorder(null, "General Configuration", 2, 2, null, null));
    
    JPanel tableConfigurationPanel = new JPanel(new MigLayout("", "[][][]", ""));
    
    rbShowTableNumber = new JRadioButton("Show table number on table");
    rbShowTableName = new JRadioButton("Show table name on table");
    
    chkShowServerName = new JCheckBox("Show server name on table");
    chkShowTokenNum = new JCheckBox("Show token number on table");
    
    ButtonGroup btntableOptionGroup = new ButtonGroup();
    btntableOptionGroup.add(rbShowTableNumber);
    btntableOptionGroup.add(rbShowTableName);
    
    tfPrimaryFontSize = new IntegerTextField(10);
    tfSecondaryFontSize = new IntegerTextField(10);
    
    tableConfigurationPanel.add(rbShowTableNumber, "wrap");
    tableConfigurationPanel.add(rbShowTableName, "wrap");
    tableConfigurationPanel.add(new JSeparator(), "gapleft 20,grow,span, wrap");
    tableConfigurationPanel.add(chkShowServerName, "wrap");
    tableConfigurationPanel.add(chkShowTokenNum, "wrap");
    tableConfigurationPanel.add(new JSeparator(), "gapleft 20,grow,span, wrap");
    
    tableConfigurationPanel.add(new JLabel("Primary font size:"), "");
    tableConfigurationPanel.add(tfPrimaryFontSize, "wrap");
    tableConfigurationPanel.add(new JLabel("Secondary font size:"), "");
    tableConfigurationPanel.add(tfSecondaryFontSize, "wrap");
    tableConfigurationPanel.add(new JSeparator(), "gapleft 20,grow,span, wrap");
    
    colArrays = new String[] { "Seat", "Book", "Free" };
    
    for (String colTxt : colArrays) {
      tableConfigurationPanel.add(createColCnfgPanel(colTxt), "span, grow, wrap");
    }
    
    JPanel buttonPanel = new JPanel(new MigLayout("center"));
    PosButton btnSave = new PosButton("SAVE");
    btnSave.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        GeneralConfiguration.this.updateModel();
      }
    });
    buttonPanel.add(btnSave);
    
    add(tableConfigurationPanel, "Center");
    add(buttonPanel, "South");
  }
  
  private JPanel createColCnfgPanel(String title) {
    JPanel panel = new JPanel(new MigLayout("", "[][]20[][]20[]", ""));
    panel.setBorder(BorderFactory.createTitledBorder(title));
    
    JLabel lblForeColorTxt = new JLabel("Foreground color:");
    JButton btnForeColor = new JButton();
    btnForeColor.setEnabled(false);
    btnForeColor.setBorder(new EtchedBorder(1, null, null));
    
    JLabel lblBgColorTxt = new JLabel("Background color:");
    JButton btnBgColor = new JButton();
    btnBgColor.setEnabled(false);
    btnBgColor.setBorder(new EtchedBorder(1, null, null));
    
    JButton btnBrowse = new JButton("Browse");
    btnBrowse.setActionCommand(title);
    btnBrowse.addActionListener(this);
    
    buttonMap.put(title + "FC", btnForeColor);
    buttonMap.put(title + "BC", btnBgColor);
    
    panel.add(lblForeColorTxt);
    panel.add(btnForeColor, "w 30!, grow");
    panel.add(lblBgColorTxt);
    panel.add(btnBgColor, "w 30!,grow");
    panel.add(btnBrowse);
    return panel;
  }
  
  private void updateView() {
    terminal = Application.getInstance().getTerminal();
    
    boolean isShowTableName = Boolean.valueOf(terminal.getProperty("floorplan.showTableName")).booleanValue();
    
    boolean isShowTableNumber = terminal.getProperty("floorplan.showTableNumber") == null ? true : Boolean.valueOf(terminal.getProperty("floorplan.showTableNumber")).booleanValue();
    
    boolean isShowServerName = terminal.getProperty("floorplan.showServerName") == null ? true : Boolean.valueOf(terminal.getProperty("floorplan.showServerName")).booleanValue();
    
    boolean isShowTokenNum = terminal.getProperty("floorplan.showTokenName") == null ? true : Boolean.valueOf(terminal.getProperty("floorplan.showTokenName")).booleanValue();
    

    String priSize = terminal.getProperty("floorplan.primaryFontSize") == null ? String.valueOf(12) : terminal.getProperty("floorplan.primaryFontSize");
    
    String secSize = terminal.getProperty("floorplan.secondaryFontSize") == null ? String.valueOf(2) : terminal.getProperty("floorplan.secondaryFontSize");
    

    seatForeColor = (StringUtils.isEmpty(terminal.getProperty("floorplan.seatForeColor")) ? Color.BLACK : new Color(Integer.parseInt(terminal.getProperty("floorplan.seatForeColor"))));
    
    seatBgColor = (StringUtils.isEmpty(terminal.getProperty("floorplan.seatBGColor")) ? new Color(255, 102, 102) : new Color(Integer.parseInt(terminal.getProperty("floorplan.seatBGColor"))));
    
    bookForeColor = (StringUtils.isEmpty(terminal.getProperty("floorplan.bookForeColor")) ? Color.BLACK : new Color(Integer.parseInt(terminal.getProperty("floorplan.bookForeColor"))));
    
    bookBgColor = (StringUtils.isEmpty(terminal.getProperty("floorplan.bookBGColor")) ? Color.orange : new Color(Integer.parseInt(terminal.getProperty("floorplan.bookBGColor"))));
    
    freeForeColor = (StringUtils.isEmpty(terminal.getProperty("floorplan.freeForeColor")) ? Color.black : new Color(Integer.parseInt(terminal.getProperty("floorplan.freeForeColor"))));
    
    freeBgColor = (StringUtils.isEmpty(terminal.getProperty("floorplan.freeBGColor")) ? Color.white : new Color(Integer.parseInt(terminal.getProperty("floorplan.freeBGColor"))));
    
    rbShowTableName.setSelected(isShowTableName);
    rbShowTableNumber.setSelected(isShowTableNumber);
    chkShowServerName.setSelected(isShowServerName);
    chkShowTokenNum.setSelected(isShowTokenNum);
    
    tfPrimaryFontSize.setText(priSize);
    tfSecondaryFontSize.setText(secSize);
    
    for (String colTxt : colArrays) {
      JButton btnFC = (JButton)buttonMap.get(colTxt + "FC");
      JButton btnBC = (JButton)buttonMap.get(colTxt + "BC");
      if ((btnFC != null) && (btnBC != null))
      {

        if (colTxt.equals(colArrays[0])) {
          if ((seatForeColor != null) || (seatBgColor != null))
          {

            btnFC.setBackground(seatForeColor);
            btnBC.setBackground(seatBgColor);
          }
        } else if (colTxt.equals(colArrays[1])) {
          if ((bookForeColor != null) || (bookBgColor != null))
          {

            btnFC.setBackground(bookForeColor);
            btnBC.setBackground(bookBgColor);
          }
        } else if ((colTxt.equals(colArrays[2])) && (
          (freeForeColor != null) || (freeBgColor != null)))
        {

          btnFC.setBackground(freeForeColor);
          btnBC.setBackground(freeBgColor);
        }
      }
    }
  }
  
  private void updateModel() {
    int priFont = tfPrimaryFontSize.getInteger();
    int secFont = tfSecondaryFontSize.getInteger();
    
    if ((priFont < 0) || (secFont < 0)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Font size must be greater than 0.");
      return;
    }
    
    terminal.addProperty("floorplan.showTableName", String.valueOf(rbShowTableName.isSelected()));
    terminal.addProperty("floorplan.showTableNumber", String.valueOf(rbShowTableNumber.isSelected()));
    terminal.addProperty("floorplan.showServerName", String.valueOf(chkShowServerName.isSelected()));
    terminal.addProperty("floorplan.showTokenName", String.valueOf(chkShowTokenNum.isSelected()));
    terminal.addProperty("floorplan.primaryFontSize", String.valueOf(priFont));
    terminal.addProperty("floorplan.secondaryFontSize", String.valueOf(secFont));
    
    terminal.addProperty("floorplan.seatForeColor", seatForeColor != null ? String.valueOf(seatForeColor.getRGB()) : "");
    terminal.addProperty("floorplan.seatBGColor", seatBgColor != null ? String.valueOf(seatBgColor.getRGB()) : "");
    
    terminal.addProperty("floorplan.bookForeColor", bookForeColor != null ? String.valueOf(bookForeColor.getRGB()) : "");
    terminal.addProperty("floorplan.bookBGColor", bookBgColor != null ? String.valueOf(bookBgColor.getRGB()) : "");
    
    terminal.addProperty("floorplan.freeForeColor", freeForeColor != null ? String.valueOf(freeForeColor.getRGB()) : "");
    terminal.addProperty("floorplan.freeBGColor", freeBgColor != null ? String.valueOf(freeBgColor.getRGB()) : "");
    
    TerminalDAO.getInstance().saveOrUpdate(terminal);
    
    POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Configuration Successfully Updated.");
  }
  
  private CustomColorChooserDialog doOpenColorChooserDialog()
  {
    CustomColorChooserDialog dialog = new CustomColorChooserDialog();
    dialog.setSize(PosUIManager.getSize(800, 500));
    dialog.setLocationRelativeTo(POSUtil.getBackOfficeWindow());
    dialog.setDefaultCloseOperation(2);
    dialog.setVisible(true);
    return dialog;
  }
  
  public void actionPerformed(ActionEvent e)
  {
    JButton btn = (JButton)e.getSource();
    if (btn.getActionCommand().equals(colArrays[0])) {
      CustomColorChooserDialog dialog = doOpenColorChooserDialog();
      if (dialog.isCanceled()) {
        return;
      }
      if (dialog.getForeColor() != null) {
        seatForeColor = dialog.getForeColor();
      }
      if (dialog.getBgColor() != null) {
        seatBgColor = dialog.getBgColor();
      }
      JButton btnFC = (JButton)buttonMap.get(colArrays[0] + "FC");
      JButton btnBC = (JButton)buttonMap.get(colArrays[0] + "BC");
      if (btnFC != null) {
        btnFC.setBackground(seatForeColor);
      }
      if (btnBC != null) {
        btnBC.setBackground(seatBgColor);
      }
    }
    else if (btn.getActionCommand().equals(colArrays[1])) {
      CustomColorChooserDialog dialog = doOpenColorChooserDialog();
      if (dialog.isCanceled()) {
        return;
      }
      if (dialog.getForeColor() != null) {
        bookForeColor = dialog.getForeColor();
      }
      if (dialog.getBgColor() != null) {
        bookBgColor = dialog.getBgColor();
      }
      
      JButton btnFC = (JButton)buttonMap.get(colArrays[1] + "FC");
      JButton btnBC = (JButton)buttonMap.get(colArrays[1] + "BC");
      if (btnFC != null) {
        btnFC.setBackground(bookForeColor);
      }
      if (btnBC != null) {
        btnBC.setBackground(bookBgColor);
      }
    }
    else if (btn.getActionCommand().equals(colArrays[2])) {
      CustomColorChooserDialog dialog = doOpenColorChooserDialog();
      if (dialog.isCanceled()) {
        return;
      }
      if (dialog.getForeColor() != null) {
        freeForeColor = dialog.getForeColor();
      }
      if (dialog.getBgColor() != null) {
        freeBgColor = dialog.getBgColor();
      }
      
      JButton btnFC = (JButton)buttonMap.get(colArrays[2] + "FC");
      JButton btnBC = (JButton)buttonMap.get(colArrays[2] + "BC");
      if (btnFC != null) {
        btnFC.setBackground(freeForeColor);
      }
      if (btnBC != null) {
        btnBC.setBackground(freeBgColor);
      }
    }
  }
}
