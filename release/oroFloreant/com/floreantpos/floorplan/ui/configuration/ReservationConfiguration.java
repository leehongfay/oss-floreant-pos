package com.floreantpos.floorplan.ui.configuration;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.ReservationShift;
import com.floreantpos.model.dao.ReservationShiftDAO;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.model.ReservationShiftEntryDialog;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.ShiftUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;

public class ReservationConfiguration
  extends TransparentPanel
{
  private IntegerTextField tfLeadHour;
  private IntegerTextField tfWaitTime;
  private IntegerTextField tfCleanUpTime;
  private IntegerTextField tfSeatTime;
  private ReservationShiftTableModel tableModel;
  private JTable table;
  private ReservationShift reservationShift;
  
  public ReservationConfiguration()
  {
    initComponents();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    setBorder(new TitledBorder(null, "Reservation Configuration", 2, 2, null, null));
    
    JPanel topPanel = new JPanel(new MigLayout("fill"));
    
    JPanel topConfigPanel = new JPanel(new MigLayout("", "[]30[][]", ""));
    
    int comboWidth = 70;
    JLabel lblMinLeadHour = new JLabel("Lead hour :");
    
    tfLeadHour = new IntegerTextField();
    tfLeadHour.setPreferredSize(PosUIManager.getSize(comboWidth, 0));
    tfLeadHour.setFocusable(true);
    
    JLabel lblReservationWaitTime = new JLabel("Reservation wait time:");
    
    tfWaitTime = new IntegerTextField();
    
    JLabel lblAvgSeatTime = new JLabel("Avg seat time:");
    
    tfSeatTime = new IntegerTextField();
    
    JLabel lblCleanUpTime = new JLabel("Clean up time:");
    
    tfCleanUpTime = new IntegerTextField();
    
    topConfigPanel.add(lblMinLeadHour);
    topConfigPanel.add(tfLeadHour, "grow");
    topConfigPanel.add(new JLabel("Hour"), "wrap");
    
    topConfigPanel.add(lblReservationWaitTime);
    topConfigPanel.add(tfWaitTime, "grow");
    topConfigPanel.add(new JLabel("Min"), "wrap");
    
    topConfigPanel.add(lblAvgSeatTime);
    topConfigPanel.add(tfSeatTime, "grow");
    topConfigPanel.add(new JLabel("Min"), "wrap");
    
    topConfigPanel.add(lblCleanUpTime);
    topConfigPanel.add(tfCleanUpTime, "grow");
    topConfigPanel.add(new JLabel("Min"), "wrap");
    
    topPanel.add(topConfigPanel, "grow, wrap");
    topPanel.add(createReservationTimeConfigPanel(), "grow");
    
    JPanel buttonPanel = new JPanel(new MigLayout("center"));
    PosButton btnSave = new PosButton("SAVE");
    btnSave.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ReservationConfiguration.this.updateModel();
      }
    });
    buttonPanel.add(btnSave);
    
    add(topPanel, "Center");
    add(buttonPanel, "South");
  }
  
  private void editSelectedRow()
  {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      ReservationShift shift = (ReservationShift)tableModel.getRowData(index);
      ReservationShiftEntryDialog dialog = new ReservationShiftEntryDialog();
      dialog.setReservationShift(shift);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      tableModel.updateItem(index);
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private JPanel createReservationTimeConfigPanel() {
    JPanel reservationTimeConfigPanel = new JPanel(new BorderLayout(5, 5));
    reservationTimeConfigPanel.setBorder(new TitledBorder(null, "Reservation Time Management", 2, 2, null, null));
    
    List<ReservationShift> shifts = new ReservationShiftDAO().findAll();
    
    tableModel = new ReservationShiftTableModel(shifts);
    table = new JTable(tableModel);
    
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          ReservationConfiguration.this.editSelectedRow();
        }
      }
    });
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    table.setRowHeight(PosUIManager.getSize(30));
    
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          ReservationShiftEntryDialog dialog = new ReservationShiftEntryDialog();
          dialog.open();
          if (dialog.isCanceled())
            return;
          reservationShift = dialog.getReservationShift();
          tableModel.addItem(reservationShift);
          table.repaint();
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        ReservationConfiguration.this.editSelectedRow();
      }
      
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0)
            return;
          ReservationShift priceShift = (ReservationShift)tableModel.getRowData(index);
          if (ConfirmDeleteDialog.showMessage(ReservationConfiguration.this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0)
          {
            ReservationShiftDAO.getInstance().releaseParentAndDelete(priceShift);
            tableModel.deleteItem(index);
          }
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JPanel buttonPanel = new JPanel(new MigLayout("center"));
    buttonPanel.add(addButton);
    buttonPanel.add(editButton);
    buttonPanel.add(deleteButton);
    
    reservationTimeConfigPanel.add(new JScrollPane(table), "Center");
    reservationTimeConfigPanel.add(buttonPanel, "South");
    return reservationTimeConfigPanel;
  }
  



























  private void updateView() {}
  


























  private void updateModel()
  {
    POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Configuration Successfully Updated.");
  }
  
  class ReservationShiftTableModel extends ListTableModel
  {
    ReservationShiftTableModel(List list) {
      super(list);
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      ReservationShift shift = (ReservationShift)rows.get(rowIndex);
      if (shift == null) {
        return null;
      }
      switch (columnIndex) {
      case 0: 
        return shift.getName();
      
      case 1: 
        return shift.getDescription();
      
      case 2: 
        return shift.getDayOfWeekAsString();
      
      case 3: 
        return ShiftUtil.buildShiftTimeRepresentation(shift.getStartTime());
      
      case 4: 
        return ShiftUtil.buildShiftTimeRepresentation(shift.getEndTime());
      
      case 5: 
        return shift.isEnable();
      }
      return null;
    }
  }
}
