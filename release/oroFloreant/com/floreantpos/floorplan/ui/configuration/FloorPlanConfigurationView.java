package com.floreantpos.floorplan.ui.configuration;

import com.floreantpos.swing.TransparentPanel;
import java.awt.BorderLayout;
import javax.swing.JTabbedPane;

public class FloorPlanConfigurationView
  extends TransparentPanel
{
  public FloorPlanConfigurationView()
  {
    initComponents();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    JTabbedPane tabbedPane = new JTabbedPane();
    tabbedPane.addTab("General", new GeneralConfiguration());
    
    add(tabbedPane, "Center");
  }
}
