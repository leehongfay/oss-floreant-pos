package com.floreantpos.floorplan.ui.configuration;

import com.floreantpos.config.AppProperties;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.util.POSUtil;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;


public class CustomColorChooserDialog
  extends POSDialog
{
  private JColorChooser colorChooser;
  private JButton backgroundButton;
  private JButton foregroundButton;
  private JButton btnOk;
  private JButton btnCancel;
  private Color bgColor;
  private Color foreColor;
  
  public CustomColorChooserDialog()
  {
    super(POSUtil.getBackOfficeWindow(), "Color chooser", true);
    setTitle(AppProperties.getAppName());
    initComponents();
  }
  
  private void initComponents() {
    colorChooser = new JColorChooser();
    
    ButtonActionListener listener = new ButtonActionListener(null);
    
    backgroundButton = new JButton("Set Background");
    backgroundButton.addActionListener(listener);
    
    foregroundButton = new JButton("Set Foreground");
    foregroundButton.addActionListener(listener);
    
    btnOk = new JButton("OK");
    btnOk.addActionListener(listener);
    
    btnCancel = new JButton("CANCEL");
    btnCancel.addActionListener(listener);
    
    JPanel buttonPanel = new JPanel(new MigLayout("center"));
    buttonPanel.add(backgroundButton);
    buttonPanel.add(foregroundButton);
    buttonPanel.add(btnOk);
    buttonPanel.add(btnCancel);
    
    getContentPane().add(colorChooser, "Center");
    getContentPane().add(buttonPanel, "Last");
  }
  
  public Color getBgColor()
  {
    return bgColor;
  }
  
  public void setBgColor(Color bgColor) {
    this.bgColor = bgColor;
  }
  
  public Color getForeColor() {
    return foreColor;
  }
  

  public void setForeColor(Color foreColor) { this.foreColor = foreColor; }
  
  private class ButtonActionListener implements ActionListener {
    private ButtonActionListener() {}
    
    public void actionPerformed(ActionEvent e) {
      if (e.getSource().equals(backgroundButton)) {
        backgroundButton.setBackground(colorChooser.getColor());
        setBgColor(colorChooser.getColor());
      }
      else if (e.getSource().equals(foregroundButton)) {
        foregroundButton.setBackground(colorChooser.getColor());
        setForeColor(colorChooser.getColor());
      }
      else if (e.getSource().equals(btnOk)) {
        setCanceled(false);
        dispose();
      }
      else if (e.getSource().equals(btnCancel)) {
        setCanceled(true);
        dispose();
      }
    }
  }
}
