package com.floreantpos.floorplan.ui;

import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.model.ShopFloor;
import com.floreantpos.model.dao.ShopFloorDAO;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.ScrollableFlowPanel;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;





























public class FloorSelectionDialog
  extends POSDialog
{
  private ScrollableFlowPanel buttonsPanel;
  private ShopFloor selectedFloor;
  
  public FloorSelectionDialog()
  {
    initializeComponent();
    initializeData();
    
    setResizable(true);
  }
  
  private void initializeComponent() {
    setTitle("Select Floor");
    setLayout(new BorderLayout());
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Select Floor");
    add(titlePanel, "North");
    
    JPanel buttonActionPanel = new JPanel(new MigLayout("fill"));
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL.toUpperCase());
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        selectedFloor = null;
        setCanceled(true);
        dispose();
      }
      
    });
    buttonActionPanel.add(btnCancel, "grow, span");
    
    JPanel footerPanel = new JPanel(new BorderLayout());
    footerPanel.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
    footerPanel.add(new JSeparator(), "North");
    footerPanel.add(buttonActionPanel);
    
    add(footerPanel, "South");
    
    buttonsPanel = new ScrollableFlowPanel(3);
    
    JScrollPane scrollPane = new PosScrollPane(buttonsPanel, 20, 31);
    scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(80, 0));
    scrollPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5), scrollPane.getBorder()));
    
    add(scrollPane, "Center");
    
    setSize(550, 450);
  }
  
  private void initializeData() {
    try {
      List<ShopFloor> shopFloors = ShopFloorDAO.getInstance().findAll();
      group = new ButtonGroup();
      for (ShopFloor shopFloor : shopFloors) {
        ShopFloorButton shopFloorButton = new ShopFloorButton(shopFloor);
        buttonsPanel.add(shopFloorButton);
        group.add(shopFloorButton);
      }
    } catch (PosException e) {
      ButtonGroup group;
      POSMessageDialog.showError(this, e.getLocalizedMessage(), e);
    }
  }
  
  protected void doFinish() {
    if (selectedFloor == null) {
      POSMessageDialog.showMessage("Please select one floor");
      return;
    }
    setCanceled(false);
    dispose();
  }
  
  public ShopFloor getSelectedFloor() {
    return selectedFloor;
  }
  
  private class ShopFloorButton extends POSToggleButton implements ActionListener {
    private static final int BUTTON_SIZE = 120;
    ShopFloor shopFloor;
    
    ShopFloorButton(ShopFloor shopFloor) {
      this.shopFloor = shopFloor;
      setFont(getFont().deriveFont(18.0F));
      setText(shopFloor.getName());
      setPreferredSize(new Dimension(120, 120));
      
      addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e) {
      selectedFloor = shopFloor;
      doFinish();
    }
  }
}
