package com.floreantpos.floorplan.ui;

import com.floreantpos.model.BookingInfo;
import com.floreantpos.model.dao.BookingInfoDAO;
import com.floreantpos.swing.BeanTableModel;
import java.util.List;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.jdesktop.swingx.JXTable;

public class TableBookingBrowser extends ReservationModelBrowser<BookingInfo>
{
  public TableBookingBrowser()
  {
    super(new TableBookingForm(new BookingInfo()), new TableBookingSearchPanel());
    
    BeanTableModel<BookingInfo> tableModel = new BeanTableModel(BookingInfo.class);
    
    tableModel.addColumn("BOOKING ID", BookingInfo.PROP_BOOKING_ID);
    tableModel.addColumn(Messages.getString("TableBookingBrowserPanel.2"), BookingInfo.PROP_FROM_DATE);
    tableModel.addColumn(Messages.getString("TableBookingBrowser.0"), "customerInfo");
    tableModel.addColumn(Messages.getString("TableBookingBrowser.2"), "bookedTableNumbers");
    tableModel.addColumn(Messages.getString("TableBookingBrowser.1"), BookingInfo.PROP_STATUS);
    
    init(tableModel);
    browserTable.setAutoResizeMode(4);
    setColumnWidth(0, 140);
    setColumnWidth(2, 100);
    setColumnWidth(3, 100);
    
    TableBookingForm bookingForm = (TableBookingForm)getBeanEditor();
    bookingForm.setBookingBrowser(this);
    
    refreshTable();
  }
  

  public void refreshTable()
  {
    List<BookingInfo> bookingList = BookingInfoDAO.getInstance().getAllOpenBooking();
    BeanTableModel tableModel = (BeanTableModel)browserTable.getModel();
    tableModel.removeAll();
    tableModel.addRows(bookingList);
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = browserTable.getColumnModel().getColumn(columnNumber);
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
}
