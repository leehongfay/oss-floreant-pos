package com.floreantpos.floorplan.ui;

import com.floreantpos.IconFactory;
import com.floreantpos.floorplan.action.OnlineTableBookingAction;
import com.floreantpos.main.Application;
import com.floreantpos.model.Customer;
import com.floreantpos.swing.PosButton;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.Border;
import net.miginfocom.swing.MigLayout;
















public class PopUpWindow
  extends JFrame
{
  private JLabel lblCallerId;
  private JPanel contentPanel;
  private PosButton btnMaximum;
  private PosButton btnMinimize;
  private PosButton btnClose;
  private String msg;
  
  public PopUpWindow(String msg)
  {
    setLayout(new MigLayout("fill,inset 5, hidemode 3", "fill,grow", ""));
    setIconImage(Application.getApplicationIcon().getImage());
    this.msg = msg;
    positionWindow(330, 130);
    setFocusable(true);
    setUndecorated(true);
    setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
    
    lblCallerId = new JLabel();
    btnMinimize = new PosButton(IconFactory.getIcon("/images/", "minimize.png"));
    btnMaximum = new PosButton(IconFactory.getIcon("/images/", "maximize.png"));
    btnClose = new PosButton(IconFactory.getIcon("/images/", "close.png"));
    
    JPanel headerPanel = new CallerHeaderPanel();
    
    headerPanel.add(btnMinimize, "split 3,w 20!,h 20!, grow, right");
    headerPanel.add(btnMaximum, "w 20!,h 20!");
    headerPanel.add(btnClose, "w 20!,h 20!");
    
    headerPanel.setBackground(new Color(44, 44, 44));
    getContentPane().setBackground(new Color(44, 44, 44));
    
    add(headerPanel, "grow,wrap");
    
    Font font = new Font("Verdana", 1, 16);
    Color color = Color.WHITE;
    
    contentPanel = new JPanel(new MigLayout("hidemode 3,inset 0,fill", "fill,grow", ""));
    
    JSeparator sep = new JSeparator();
    sep.setBackground(Color.white);
    sep.setForeground(Color.white);
    
    contentPanel.add(sep, "cell 0 0 3 1,grow,span");
    
    JLabel lblMessage = new JLabel();
    
    lblMessage.setText(msg);
    lblMessage.setFont(font);
    lblMessage.setForeground(color);
    
    btnMinimize.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        contentPanel.setVisible(false);
        PopUpWindow.this.positionWindow(330, 25);
      }
      
    });
    btnMaximum.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        contentPanel.setVisible(true);
        PopUpWindow.this.positionWindow(330, 130);
      }
      
    });
    btnClose.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        setVisible(false);
        dispose();
      }
      
    });
    PosButton btnUpdate = new PosButton("Update");
    btnUpdate.setForeground(color);
    btnUpdate.setFont(new Font("Verdana", 1, 12));
    btnUpdate.setBorder(new RoundedBorder(10));
    btnUpdate.setBackground(new Color(77, 201, 92));
    
    btnUpdate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (Application.getCurrentUser() != null) {
          new OnlineTableBookingAction().execute();
        }
        dispose();
      }
      
    });
    PosButton btnCancel = new PosButton("Cancel");
    btnCancel.setForeground(color);
    btnCancel.setBorder(new RoundedBorder(10));
    btnCancel.setBackground(new Color(130, 188, 8));
    
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setVisible(false);
        dispose();
      }
    });
    contentPanel.add(lblMessage, "cell 1 0,gaptop 2, span 2");
    contentPanel.add(btnUpdate, "cell 2 3,split 2,h 30!");
    contentPanel.add(btnCancel, "cell 2 4,h 30!");
    
    contentPanel.setBackground(new Color(44, 44, 44));
    
    setAlwaysOnTop(true);
    add(contentPanel, "grow");
  }
  
  public void setMaximize(boolean maximize)
  {
    setVisible(true);
    contentPanel.setVisible(maximize);
  }
  
  private void positionWindow(int width, int height) {
    setSize(width, height);
    
    Rectangle screenSize = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
    int x = width - width;
    int y = height - height;
    
    setLocation(x, y);
  }
  
  private static class RoundedBorder implements Border
  {
    private int radius;
    
    RoundedBorder(int radius) {
      this.radius = radius;
    }
    
    public Insets getBorderInsets(Component c) {
      return new Insets(radius + 1, radius + 1, radius + 2, radius);
    }
    
    public boolean isBorderOpaque() {
      return true;
    }
    
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
      g.drawRoundRect(x, y, width - 1, height - 1, radius, radius);
    }
  }
  
  public class CallerHeaderPanel extends JPanel {
    Customer customer;
    
    CallerHeaderPanel() {
      setLayout(new MigLayout("fill, alignx right", "", ""));
    }
    
    public void setCustomer(Customer customer) {
      this.customer = customer;
    }
  }
  
  public static void main(String[] arg) {
    new PopUpWindow("2 update available").setVisible(true);
  }
}
