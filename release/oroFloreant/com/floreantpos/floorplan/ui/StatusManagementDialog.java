package com.floreantpos.floorplan.ui;

import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.model.BookingInfo;
import com.floreantpos.model.BookingStatus;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSDialog;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;


public class StatusManagementDialog
  extends POSDialog
{
  private BookingInfo tableBookingInfo;
  private FloorBookingView floorBookingView;
  
  public StatusManagementDialog(BookingInfo tableBookingInfo, FloorBookingView floorBookingView)
  {
    this.tableBookingInfo = tableBookingInfo;
    this.floorBookingView = floorBookingView;
    initComponents();
    updateView();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    
    JPanel centerPanel = new JPanel(new MigLayout("fillx"));
    
    int width = 220;
    int height = 150;
    
    PosButton btnBookingSeat = new PosButton("Seat");
    PosButton btnBookingComplete = new PosButton(BookingStatus.Complete.toString());
    PosButton btnBookingOk = new PosButton("Ok");
    PosButton btnBookingCancel = new PosButton(BookingStatus.Cancel.toString());
    
    btnBookingSeat.setPreferredSize(PosUIManager.getSize(width, height));
    btnBookingComplete.setPreferredSize(PosUIManager.getSize(width, height));
    btnBookingOk.setPreferredSize(PosUIManager.getSize(width, height));
    btnBookingCancel.setPreferredSize(PosUIManager.getSize(width, height));
    
    centerPanel.add(btnBookingSeat);
    centerPanel.add(btnBookingComplete);
    centerPanel.add(btnBookingOk);
    centerPanel.add(btnBookingCancel, "wrap");
    
    JButton btnCancel = new JButton("Close");
    centerPanel.add(btnCancel, "span 4, growx");
    
    btnBookingSeat.addActionListener(floorBookingView);
    btnBookingSeat.setActionCommand("seat");
    
    btnBookingComplete.addActionListener(floorBookingView);
    btnBookingComplete.setActionCommand(BookingStatus.Complete.toString());
    
    btnBookingOk.addActionListener(floorBookingView);
    btnBookingOk.setActionCommand("Ok");
    
    btnBookingCancel.addActionListener(floorBookingView);
    btnBookingCancel.setActionCommand("Cancel");
    
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setCanceled(true);
        dispose();
      }
      

    });
    JPanel buttonPanel = new JPanel(new MigLayout("al center", "sg, fill", ""));
    
    PosButton btnOk = new PosButton("Ok");
    buttonPanel.add(btnOk, "grow");
    
    btnOk.addActionListener(new ActionListener()
    {


      public void actionPerformed(ActionEvent e) {}


    });
    buttonPanel.add(new PosButton(new CloseDialogAction(this, "Cancel")));
    
    add(centerPanel, "Center");
  }
  

  private boolean updateModel()
  {
    return true;
  }
  
  private void updateView() {}
}
