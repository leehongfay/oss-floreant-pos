package com.floreantpos.floorplan.ui;

import com.floreantpos.bo.ui.Command;
import com.floreantpos.bo.ui.ModelBrowser;
import com.floreantpos.model.BookingStatus;
import com.floreantpos.ui.BookingBeanEditor;
import com.floreantpos.ui.SearchPanel;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableModel;
import org.jdesktop.swingx.JXTable;

public class ReservationModelBrowser<E> extends ModelBrowser
{
  private BookingBeanEditor beanEditor;
  private JButton btnSeat = new JButton(Messages.getString("ReservationModelBrowser.0"));
  private JButton btnNoAppear = new JButton(Messages.getString("ReservationModelBrowser.1"));
  private JButton btnDelay = new JButton(Messages.getString("ReservationModelBrowser.2"));
  private JButton btnBookingCancel = new JButton(Messages.getString("ReservationModelBrowser.3"));
  private JButton btnBookingClose = new JButton(Messages.getString("ReservationModelBrowser.4"));
  private int selectedRow;
  
  public ReservationModelBrowser(BookingBeanEditor<E> beanEditor, SearchPanel<E> searchPanel) {
    super(beanEditor, searchPanel);
    this.beanEditor = beanEditor;
    btnSeat.setEnabled(false);
    btnNoAppear.setEnabled(false);
    btnDelay.setEnabled(false);
    btnBookingCancel.setEnabled(false);
    btnBookingClose.setEnabled(false);
  }
  

  public void init(TableModel tableModel)
  {
    super.init(tableModel);
    buttonPanel.add(btnSeat);
    buttonPanel.add(btnDelay);
    buttonPanel.add(btnNoAppear);
    buttonPanel.add(btnBookingCancel);
    buttonPanel.add(btnBookingClose);
    
    btnSeat.addActionListener(this);
    btnDelay.addActionListener(this);
    btnNoAppear.addActionListener(this);
    btnBookingCancel.addActionListener(this);
    btnBookingClose.addActionListener(this);
  }
  
  public void actionPerformed(ActionEvent e)
  {
    Command command = Command.fromString(e.getActionCommand());
    try {
      selectedRow = browserTable.getSelectedRow();
      switch (1.$SwitchMap$com$floreantpos$bo$ui$Command[command.ordinal()]) {
      case 1: 
        beanEditor.createNew();
        beanEditor.setFieldsEnable(true);
        btnNew.setEnabled(false);
        btnEdit.setEnabled(false);
        btnSave.setEnabled(true);
        btnDelete.setEnabled(false);
        btnCancel.setEnabled(true);
        browserTable.clearSelection();
        setBookingStatus(false);
        break;
      
      case 2: 
        beanEditor.edit();
        beanEditor.setFieldsEnable(true);
        btnNew.setEnabled(false);
        btnEdit.setEnabled(false);
        btnSave.setEnabled(true);
        btnDelete.setEnabled(false);
        btnCancel.setEnabled(true);
        setBookingStatus(false);
        break;
      
      case 3: 
        doCancelEditing();
        break;
      
      case 4: 
        if (beanEditor.save()) {
          beanEditor.setFieldsEnable(false);
          btnNew.setEnabled(true);
          btnEdit.setEnabled(false);
          btnSave.setEnabled(false);
          btnDelete.setEnabled(false);
          btnCancel.setEnabled(false);
          setBookingStatus(false);
          refreshTable();
          refreshTableBookingSearchPanel();
          customSelectedRow();
        }
        
        break;
      case 5: 
        if (beanEditor.delete()) {
          beanEditor.setBean(null);
          beanEditor.setFieldsEnable(false);
          btnNew.setEnabled(true);
          btnEdit.setEnabled(false);
          btnSave.setEnabled(false);
          btnDelete.setEnabled(false);
          btnCancel.setEnabled(false);
          setBookingStatus(false);
          refreshTableBookingSearchPanel();
          refreshTable();
        }
        

        break;
      }
      
      
      handleAdditionaButtonActionIfApplicable(e);
      
      if (e.getSource() == btnSeat) {
        beanEditor.bookingStatus(BookingStatus.Confirm.toString());
        customSelectedRow();
      }
      else if (e.getSource() == btnBookingCancel) {
        beanEditor.bookingStatus(BookingStatus.Cancel.toString());
        customSelectedRow();
      }
      else if (e.getSource() == btnDelay) {
        beanEditor.bookingStatus(BookingStatus.Request.toString());
        customSelectedRow();
      }
      else if (e.getSource() == btnNoAppear) {
        beanEditor.bookingStatus(BookingStatus.NoAppear.toString());
        customSelectedRow();
      }
      else if (e.getSource() == btnBookingClose) {
        beanEditor.bookingStatus(BookingStatus.Complete.toString());
        customSelectedRow();
        
        if (browserTable.getRowCount() == 0) {
          beanEditor.clearFields();
          setBookingStatus(false);
          btnDelete.setEnabled(false);
          btnEdit.setEnabled(false);
        }
      }
    }
    catch (Exception e2) {
      e2.printStackTrace();
    }
  }
  
  public void valueChanged(ListSelectionEvent e)
  {
    super.valueChanged(e);
    if (browserTable.getSelectedRow() != -1) {
      setBookingStatus(true);
    }
  }
  
  public void doCancelEditing()
  {
    super.doCancelEditing();
    
    if (browserTable.getSelectedRow() != -1) {
      setBookingStatus(true);
      return;
    }
    setBookingStatus(false);
  }
  
  private void refreshTableBookingSearchPanel() {
    getSearchPanel().refreshSearchPanel();
  }
  
















  private void customSelectedRow()
  {
    if (selectedRow == -1) {
      browserTable.setRowSelectionInterval(browserTable.getRowCount() - 1, browserTable.getRowCount() - 1);
      return;
    }
    
    if (browserTable.getRowCount() == 0) {
      return;
    }
    
    if (selectedRow >= browserTable.getRowCount()) {
      browserTable.setRowSelectionInterval(selectedRow - 1, selectedRow - 1);
    }
    else {
      browserTable.setRowSelectionInterval(selectedRow, selectedRow);
    }
  }
  
  public void refreshButtonPanel()
  {
    super.refreshButtonPanel();
    
    btnSeat.setEnabled(false);
    btnNoAppear.setEnabled(false);
    btnDelay.setEnabled(false);
    btnBookingCancel.setEnabled(false);
    btnBookingClose.setEnabled(false);
  }
  
  private void setBookingStatus(boolean enable) {
    btnSeat.setEnabled(enable);
    btnBookingCancel.setEnabled(enable);
    btnDelay.setEnabled(enable);
    btnNoAppear.setEnabled(enable);
    btnBookingClose.setEnabled(enable);
  }
}
