package com.floreantpos.floorplan.ui;

import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.ShopTableType;
import com.floreantpos.model.dao.ShopTableTypeDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.FixedLengthDocument;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;
import org.hibernate.StaleObjectStateException;






public class ShopTableTypeForm
  extends BeanEditor<ShopTableType>
{
  private JTextArea tfTableTypeDescription;
  private FixedLengthTextField tfTableTypeName;
  private JPanel tableTypeCentralPanel;
  private JLabel typeName;
  private JLabel description;
  
  public ShopTableTypeForm()
  {
    tableTypeCentralPanel = new JPanel();
    tableTypeCentralPanel.setLayout(new MigLayout());
    
    tfTableTypeName = new FixedLengthTextField(40);
    tfTableTypeDescription = new JTextArea(10, 30);
    tfTableTypeDescription.setDocument(new FixedLengthDocument(120));
    
    typeName = new JLabel(Messages.getString("ShopTabeTypeForm.0"));
    description = new JLabel(Messages.getString("ShopTabeTypeForm.1"));
    
    tableTypeCentralPanel.add(typeName);
    tableTypeCentralPanel.add(tfTableTypeName, "wrap");
    tableTypeCentralPanel.add(description, "top");
    tableTypeCentralPanel.add(new JScrollPane(tfTableTypeDescription), "wrap,grow");
    
    add(tableTypeCentralPanel);
  }
  
  public void setFieldsEditable(boolean editable) {
    tfTableTypeName.setEditable(editable);
    tfTableTypeDescription.setEditable(editable);
  }
  
  public void setOnlyStatusEnable() {
    tfTableTypeName.setEditable(false);
    tfTableTypeDescription.setEditable(false);
  }
  
  public void createNew()
  {
    setBean(new ShopTableType());
  }
  
  public boolean delete()
  {
    try {
      ShopTableType bean2 = (ShopTableType)getBean();
      if (bean2 == null)
        return false;
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), 
        Messages.getString("ShopTabeTypeForm.2"), Messages.getString("ShopTabeTypeForm.3"));
      if (option != 0) {
        return false;
      }
      ShopTableTypeDAO.getInstance().delete(bean2);
      
      tfTableTypeDescription.setText("");
      tfTableTypeName.setText("");
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e);
    }
    return false;
  }
  
  public void setFieldsEnable(boolean enable)
  {
    tfTableTypeName.setEnabled(enable);
    tfTableTypeDescription.setEnabled(enable);
  }
  
  public boolean save()
  {
    try {
      if (!updateModel())
        return false;
      ShopTableType tableType = (ShopTableType)getBean();
      ShopTableTypeDAO.getInstance().saveOrUpdate(tableType);
      updateView();
      return true;
    }
    catch (IllegalModelStateException localIllegalModelStateException) {}catch (StaleObjectStateException e) {
      BOMessageDialog.showError(this, Messages.getString("ShopTabeTypeForm.4"));
    }
    return false;
  }
  
  protected void updateView()
  {
    ShopTableType tableType = (ShopTableType)getBean();
    if (tableType == null) {
      return;
    }
    tfTableTypeName.setText(tableType.getName());
    tfTableTypeDescription.setText(tableType.getDescription());
  }
  
  protected boolean updateModel() throws IllegalModelStateException
  {
    ShopTableType tableType = (ShopTableType)getBean();
    if (tableType == null) {
      tableType = new ShopTableType();
      setBean(tableType, false);
    }
    String name = tfTableTypeName.getText();
    String description2 = tfTableTypeDescription.getText();
    if ((StringUtils.isEmpty(name)) || (StringUtils.isEmpty(description2))) {
      JOptionPane.showMessageDialog(null, Messages.getString("ShopTabeTypeForm.5"));
      return false;
    }
    tableType.setName(name);
    tableType.setDescription(description2);
    return true;
  }
  
  public String getDisplayText()
  {
    return "Create Type";
  }
}
