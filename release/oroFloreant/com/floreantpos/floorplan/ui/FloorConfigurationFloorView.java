package com.floreantpos.floorplan.ui;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.model.ImageResource;
import com.floreantpos.model.ImageResource.IMAGE_CATEGORY;
import com.floreantpos.model.ShopFloor;
import com.floreantpos.model.ShopFloorTemplate;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.dao.ImageResourceDAO;
import com.floreantpos.model.dao.ShopFloorDAO;
import com.floreantpos.model.dao.ShopFloorTemplateDAO;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.ImageComponent;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.table.ShopTableForm;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.forms.ShopTableSelectionDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.imageio.ImageIO;
import javax.sql.rowset.serial.SerialBlob;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;


public class FloorConfigurationFloorView
  extends JPanel
{
  private ShopFloor floor;
  private FixedLengthTextField tfFloorName = new FixedLengthTextField(60);
  private JLayeredPane floorPanel = new JLayeredPane();
  private POSFileChooser fileChooser = new POSFileChooser();
  private JPanel buttonPanel = new JPanel();
  
  private JList<ShopFloor> floorsList;
  private JCheckBox reArrange;
  private IntegerTextField tfButtonWidth;
  private IntegerTextField tfButtonHeight;
  private IntegerTextField tfButtonFontSize;
  private IntegerTextField tfSortOrder = new IntegerTextField(4);
  
  private JList<ShopFloorTemplate> floorTemplatesList;
  private boolean saveRequired = false;
  private JButton btnUpdate;
  private ShopFloorTemplate selectedTemplate;
  private JButton btnDetachSelectedTables;
  private JButton btnSetFloorImage;
  private JButton btnRemoveAllTables;
  JButton btnSaveAsTemplate;
  private PosButton btnBackgroundColor;
  private PosButton btnForegroundColor;
  protected boolean drag;
  protected Point dragLocation;
  private JScrollPane scrollPane;
  protected Point origin;
  
  public boolean isSaveRequired()
  {
    return saveRequired;
  }
  
  public FloorConfigurationFloorView(JList<ShopFloor> floorsList) {
    this.floorsList = floorsList;
    
    setLayout(new BorderLayout(5, 5));
    setBorder(BorderFactory.createTitledBorder(Messages.getString("FloorView.0")));
    
    createHeaderPanel();
    createLayoutPanel();
    
    createButtonPanel();
    btnUpdate.setEnabled(saveRequired);
  }
  
  private void createLayoutPanel() {
    floorPanel.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent e) {
        FloorConfigurationFloorView.this.insertTable(e);
      }
      
      public void mousePressed(MouseEvent e)
      {
        dragLocation = e.getPoint();
        origin = e.getPoint();
        Dimension preferredSize = floorPanel.getPreferredSize();
        if ((height - 5 <= dragLocation.y) && (width - 5 <= dragLocation.x)) {
          drag = true;
        }
        else if (height - 5 <= dragLocation.y) {
          drag = true;
        }
        else if (width - 5 <= dragLocation.x) {
          drag = true;
        }
      }
      
      public void mouseReleased(MouseEvent e)
      {
        setCursor(Cursor.getDefaultCursor());
        drag = false;
        if (saveRequired) {
          int btnWidth = TerminalConfig.getFloorButtonWidth();
          int btnHeight = TerminalConfig.getFloorButtonHeight();
          int width = (int)floorPanel.getPreferredSize().getWidth();
          int height = (int)floorPanel.getPreferredSize().getHeight();
          floor.setWidth(Integer.valueOf(width));
          floor.setHeight(Integer.valueOf(height));
          
          boolean tableRearrage = false;
          if (floor.getTables() != null) {
            for (ShopTable table : floor.getTables()) {
              Integer shopTableWidth = table.getWidth();
              if (shopTableWidth.intValue() == 0) {
                shopTableWidth = Integer.valueOf(btnWidth);
              }
              Integer shopTableHeight = table.getHeight();
              if (shopTableHeight.intValue() == 0) {
                shopTableHeight = Integer.valueOf(btnHeight);
              }
              if (table.getX().intValue() + shopTableWidth.intValue() > width) {
                table.setX(Integer.valueOf(width - shopTableWidth.intValue() - 10));
                if (!tableRearrage)
                  tableRearrage = true;
              }
              if (table.getY().intValue() + shopTableHeight.intValue() > height) {
                table.setY(Integer.valueOf(height - shopTableHeight.intValue() - 10));
                if (!tableRearrage)
                  tableRearrage = true;
              }
            }
          }
          btnUpdate.setEnabled(saveRequired);
          if (tableRearrage) {
            try {
              FloorConfigurationFloorView.this.renderFloor();
            }
            catch (Exception localException1) {}
          }
        }
        FloorConfigurationFloorView.this.updateImagePostion();
      }
    });
    floorPanel.setBorder(BorderFactory.createLineBorder(Color.gray));
    floorPanel.addMouseMotionListener(new MouseMotionAdapter()
    {
      public void mouseMoved(MouseEvent e)
      {
        if (!drag) {
          Point dragLocation = e.getPoint();
          Dimension preferredSize = floorPanel.getPreferredSize();
          if ((height - 5 <= y) && (width - 5 <= x)) {
            setCursor(Cursor.getPredefinedCursor(5));
          }
          else if (height - 5 <= y) {
            setCursor(Cursor.getPredefinedCursor(9));
          }
          else if (width - 5 <= x) {
            setCursor(Cursor.getPredefinedCursor(11));
          }
          else {
            setCursor(Cursor.getDefaultCursor());
          }
        }
        else {
          setCursor(Cursor.getDefaultCursor());
        }
      }
      
      public void mouseDragged(MouseEvent e)
      {
        if (drag) {
          Dimension preferredSize = floorPanel.getPreferredSize();
          if ((dragLocation.getX() > preferredSize.getWidth() - 10.0D) && (dragLocation.getY() > preferredSize.getHeight() - 10.0D)) {
            floorPanel.setPreferredSize(new Dimension((int)(preferredSize.getWidth() + (e.getPoint().getX() - dragLocation.getX())), 
              (int)(preferredSize.getHeight() + (e.getPoint().getY() - dragLocation.getY()))));
            saveRequired = true;
            
            int scrollStepX = (int)(e.getPoint().getX() - dragLocation.getX());
            int scrollStepY = (int)(e.getPoint().getY() - dragLocation.getY());
            
            int originalValX = scrollPane.getHorizontalScrollBar().getValue();
            scrollPane.getHorizontalScrollBar().setValue(originalValX + scrollStepX);
            
            int originalValY = scrollPane.getVerticalScrollBar().getValue();
            scrollPane.getVerticalScrollBar().setValue(originalValY + scrollStepY);
          }
          else if (getCursor().getType() == 11) {
            int w = (int)(preferredSize.getWidth() + (e.getPoint().getX() - dragLocation.getX()));
            floorPanel.setPreferredSize(new Dimension(w, (int)preferredSize.getHeight()));
            
            saveRequired = true;
            int scrollStepX = (int)(e.getPoint().getX() - dragLocation.getX());
            
            int originalValX = scrollPane.getHorizontalScrollBar().getValue();
            scrollPane.getHorizontalScrollBar().setValue(originalValX + scrollStepX);
          }
          else if (getCursor().getType() == 9) {
            int h = (int)(preferredSize.getHeight() + (e.getPoint().getY() - dragLocation.getY()));
            floorPanel.setPreferredSize(new Dimension((int)preferredSize.getWidth(), h));
            saveRequired = true;
            int scrollStepY = (int)(e.getPoint().getY() - dragLocation.getY());
            
            int originalValY = scrollPane.getVerticalScrollBar().getValue();
            scrollPane.getVerticalScrollBar().setValue(originalValY + scrollStepY);
          }
          dragLocation = e.getPoint();
          floorPanel.revalidate();
          floorPanel.repaint();
        }
        FloorConfigurationFloorView.this.updateImagePostion();
      }
      
    });
    JPanel floorPanelContainer = new JPanel();
    floorPanel.setAutoscrolls(true);
    floorPanelContainer.add(floorPanel);
    floorPanelContainer.setAutoscrolls(true);
    scrollPane = new JScrollPane(floorPanelContainer);
    add(scrollPane);
    
    floorPanelContainer.addMouseMotionListener(new MouseMotionListener()
    {
      public void mouseMoved(MouseEvent e)
      {
        setCursor(Cursor.getDefaultCursor());
        drag = false;
      }
      

      public void mouseDragged(MouseEvent e) {}
    });
  }
  
  private void createHeaderPanel()
  {
    JPanel headerPanel = new JPanel(new MigLayout("inset 0 5 0 5,fill"));
    
    tfFloorName.setColumns(30);
    
    btnUpdate = new JButton(IconFactory.getIcon("save.png"));
    btnUpdate.setBorder(null);
    btnUpdate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        if (floor == null) {
          return;
        }
        FloorConfigurationFloorView.this.updateFloorNameAndSortOrder();
        doSaveFloorTemplate();
        saveRequired = false;
        btnUpdate.setEnabled(false);
      }
      
    });
    tfFloorName.addKeyListener(new KeyListener()
    {
      public void keyTyped(KeyEvent e)
      {
        saveRequired = true;
        btnUpdate.setEnabled(true);
      }
      



      public void keyReleased(KeyEvent e) {}
      



      public void keyPressed(KeyEvent e) {}
    });
    tfSortOrder.addKeyListener(new KeyListener()
    {
      public void keyTyped(KeyEvent e)
      {
        saveRequired = true;
        btnUpdate.setEnabled(true);
      }
      



      public void keyReleased(KeyEvent e) {}
      



      public void keyPressed(KeyEvent e) {}
    });
    tfFloorName.setMinimumSize(PosUIManager.getSize(80, 0));
    headerPanel.add(new JLabel(Messages.getString("FloorView.1")), "split 5");
    headerPanel.add(tfFloorName, "growx");
    headerPanel.add(new JLabel("SO"));
    headerPanel.add(tfSortOrder, "width 20!");
    headerPanel.add(btnUpdate);
    
    final JPanel inputPanel = new JPanel(new MigLayout("fill"));
    
    reArrange = new JCheckBox(Messages.getString("FloorView.3"));
    reArrange.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent actionEvent) {
        AbstractButton abstractButton = (AbstractButton)actionEvent.getSource();
        boolean selected = abstractButton.getModel().isSelected();
        inputPanel.setVisible(selected);
      }
      
    });
    tfButtonWidth = new IntegerTextField(3);
    tfButtonHeight = new IntegerTextField(3);
    tfButtonFontSize = new IntegerTextField(3);
    
    tfButtonWidth.setText(String.valueOf(TerminalConfig.getFloorButtonWidth()));
    tfButtonHeight.setText(String.valueOf(TerminalConfig.getFloorButtonHeight()));
    tfButtonFontSize.setText(String.valueOf(TerminalConfig.getFloorButtonFontSize()));
    
    JButton btnDone = new JButton("Update");
    btnDone.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doSave();
      }
      
    });
    btnBackgroundColor = new PosButton()
    {
      protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(getBackground());
        g.fillRect(getWidth() / 2 - 10, getHeight() - 5, getWidth() - 10, 2);
      }
      
    };
    btnForegroundColor = new PosButton()
    {
      protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(getBackground());
        g.fillRect(getWidth() / 2 - 10, getHeight() - 5, getWidth() - 10, 2);
      }
      
    };
    Border border = BorderFactory.createLineBorder(Color.LIGHT_GRAY);
    btnBackgroundColor.setBorder(border);
    btnForegroundColor.setBorder(border);
    btnBackgroundColor.setIcon(IconFactory.getIcon("/ui_icons/", "bg_color_icon.png"));
    btnForegroundColor.setIcon(IconFactory.getIcon("/ui_icons/", "text_color_icon.png"));
    btnBackgroundColor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        Color color = JColorChooser.showDialog(POSUtil.getBackOfficeWindow(), "SELECT BACKGROUND", Color.WHITE);
        if (color == null) {
          return;
        }
        floor.setBackgroundColor(color);
        updateBackgroud(color);
        setSaveRequired(true);
      }
    });
    btnForegroundColor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        Color color = JColorChooser.showDialog(POSUtil.getBackOfficeWindow(), "SELECT BUTTON TEXT COLOR", Color.WHITE);
        if (color == null)
          return;
        floor.setForegroundColor(color);
        updateForeground(color);
        setSaveRequired(true);
      }
    });
    int size = PosUIManager.getSize(30);
    String sizeConstraint = "h " + size + "!, w " + size + "!";
    
    headerPanel.add(reArrange, "gapleft 10,split 9");
    inputPanel.add(btnBackgroundColor, sizeConstraint);
    inputPanel.add(btnForegroundColor, sizeConstraint);
    inputPanel.add(new JLabel(Messages.getString("FloorView.4")));
    inputPanel.add(tfButtonWidth, "width 30!");
    inputPanel.add(new JLabel(Messages.getString("FloorView.5")));
    inputPanel.add(tfButtonHeight, "width 30!");
    inputPanel.add(new JLabel(Messages.getString("FloorView.6")));
    inputPanel.add(tfButtonFontSize, "width 30!");
    inputPanel.add(btnDone);
    inputPanel.setVisible(false);
    
    headerPanel.add(inputPanel, "growx, align right");
    add(headerPanel, "North");
  }
  
  private boolean hasBackgroundColor() {
    Color color = floor.getBackgroundColor();
    return color != null;
  }
  
  protected void updateBackgroud(Color color) {
    floorPanel.setBackground(color);
    btnBackgroundColor.setBackground(color);
    btnForegroundColor.setBackground(floor.getForegroundColor());
  }
  
  protected void updateForeground(Color color) {
    btnForegroundColor.setBackground(color);
    for (Component c : floorPanel.getComponents()) {
      if ((c instanceof PosButton)) {
        PosButton button = (PosButton)c;
        button.setForeground(color);
      }
    }
  }
  
  protected void doSaveFloorTemplate() {
    if (selectedTemplate == null) {
      return;
    }
    if (floor.getTables() != null)
    {
      for (ShopTable table : floor.getTables()) {
        selectedTemplate.addProperty(String.valueOf(table.getId()), table.getX() + "," + table.getY());
      }
    }
    
    ShopFloorTemplateDAO.getInstance().saveOrUpdate(selectedTemplate);
  }
  
  protected void doSave() {
    int width = Integer.parseInt(tfButtonWidth.getText());
    int height = Integer.parseInt(tfButtonHeight.getText());
    int fontSize = Integer.parseInt(tfButtonFontSize.getText());
    
    TerminalConfig.setFloorButtonWidth(width);
    TerminalConfig.setFloorButtonHeight(height);
    TerminalConfig.setFloorButtonFontSize(fontSize);
    
    floor.setSortOrder(Integer.valueOf(tfSortOrder.getInteger()));
    ShopFloorDAO.getInstance().saveOrUpdate(floor);
    try {
      renderFloor();
    } catch (Exception ex) {
      LogFactory.getLog(FloorConfigurationFloorView.class).error(ex);
    }
  }
  
  private void createButtonPanel() {
    btnSetFloorImage = new JButton(Messages.getString("FloorView.9"));
    btnSetFloorImage.setEnabled(false);
    btnSetFloorImage.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        selectImageFromFile();
      }
    });
    buttonPanel.add(btnSetFloorImage);
    
    JButton btnClearFloorImage = new JButton("CLEAR FLOOR IMAGE");
    btnClearFloorImage.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        FloorConfigurationFloorView.this.doClearBackgroudImage();
      }
    });
    buttonPanel.add(btnClearFloorImage);
    
    btnDetachSelectedTables = new JButton(Messages.getString("FloorView.10"));
    btnDetachSelectedTables.setEnabled(false);
    btnDetachSelectedTables.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          List<ShopTable> selectedTables = FloorConfigurationFloorView.this.getSelectedTables();
          
          if (selectedTables.size() == 0) {
            return;
          }
          
          int option = JOptionPane.showOptionDialog(POSUtil.getBackOfficeWindow(), Messages.getString("FloorView.11"), POSConstants.CONFIRM, 0, 3, null, null, null);
          
          if (option != 0) {
            return;
          }
          floor.getTables().removeAll(selectedTables);
          FloorConfigurationFloorView.this.renderFloor();
        } catch (Exception e2) {
          POSMessageDialog.showError(FloorConfigurationFloorView.this, e2.getMessage(), e2);
        }
      }
    });
    buttonPanel.add(btnDetachSelectedTables);
    
    btnRemoveAllTables = new JButton(Messages.getString("FloorView.13"));
    btnRemoveAllTables.setEnabled(false);
    btnRemoveAllTables.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        FloorConfigurationFloorView.this.removeTables();
      }
    });
    buttonPanel.add(btnRemoveAllTables);
    
    JPanel buttonPanelContainer = new JPanel(new BorderLayout(5, 5));
    buttonPanelContainer.add(new JSeparator(0), "North");
    buttonPanelContainer.add(buttonPanel);
    
    btnSaveAsTemplate = new JButton("Save as template");
    btnSaveAsTemplate.setEnabled(false);
    btnSaveAsTemplate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doSaveFloorAsTemplate();
      }
    });
    buttonPanelContainer.add(btnSaveAsTemplate, "West");
    
    add(buttonPanelContainer, "South");
  }
  
  protected void doSaveFloorAsTemplate()
  {
    String templateName = null;
    boolean repeat = false;
    do {
      templateName = JOptionPane.showInputDialog(POSUtil.getBackOfficeWindow(), "Enter template name");
      if ((templateName != null) && (templateName.equals(""))) {
        POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), "Templete name can not be empty.");
        repeat = true;
      }
      else {
        repeat = false;
      }
      
    } while (repeat);
    
    if (templateName == null) {
      return;
    }
    if (templateName.length() > 30) {
      BOMessageDialog.showError(POSUtil.getBackOfficeWindow(), "Name too long.");
      return;
    }
    
    ShopFloorTemplate template = new ShopFloorTemplate();
    template.setName(templateName);
    template.setFloor(floor);
    DefaultListModel model = (DefaultListModel)floorTemplatesList.getModel();
    
    List<ShopFloorTemplate> templateList = new ArrayList();
    Set<ShopTable> tables = floor.getTables();
    if (model.getSize() == 0)
    {





      if (tables != null) {
        for (ShopTable table : tables)
        {
          template.addProperty(String.valueOf(table.getId()), table.getX() + "," + table.getY());
        }
      }
      


      model.addElement(template);
      templateList.add(template);
      template.setDefaultFloor(Boolean.valueOf(true));
      
      if (isSaveRequired()) {
        int floorNameChange = JOptionPane.showConfirmDialog(POSUtil.getBackOfficeWindow(), "Do you want to save the floor name changes?", null, 0);
        

        if (floorNameChange == 0) {
          floor.setName(tfFloorName.getText());
          setSaveRequired(false);
        }
        else {
          setSaveRequired(false);
        }
      }
      
      ShopFloorDAO.getInstance().saveOrUpdate(floor);
      ShopFloorTemplateDAO.getInstance().saveOrUpdateTemplates(templateList);
      floorTemplatesList.setSelectedValue(template, true);
      return;
    }
    







    model.addElement(template);
    templateList.add(template);
    int modelLen = model.size();
    if (modelLen <= 1) {
      template.setDefaultFloor(Boolean.valueOf(true));
    }
    
    if (isSaveRequired()) {
      int floorNameChange = JOptionPane.showConfirmDialog(POSUtil.getBackOfficeWindow(), "Do you want to save the floor name changes?", null, 0);
      

      if (floorNameChange == 0) {
        floor.setName(tfFloorName.getText());
        setSaveRequired(false);
      }
      else {
        setSaveRequired(false);
      }
    }
    
    ShopFloorDAO.getInstance().saveOrUpdate(floor);
    ShopFloorTemplateDAO.getInstance().saveOrUpdateTemplates(templateList);
    floorTemplatesList.setSelectedValue(template, true);
  }
  
  private void renderFloor()
    throws Exception
  {
    floorPanel.removeAll();
    Session session = null;
    try {
      btnSaveAsTemplate.setEnabled(floor != null);
      btnSetFloorImage.setEnabled(floor != null);
      btnDetachSelectedTables.setEnabled(floor != null);
      btnRemoveAllTables.setEnabled(floor != null);
      
      if (floor == null) {
        tfFloorName.setText("");
        return;
      }
      if (floor.getId() != null) {
        session = ShopFloorDAO.getInstance().getSession();
        floor = ShopFloorDAO.getInstance().get(floor.getId(), session);
      }
      tfSortOrder.setText(String.valueOf(floor.getSortOrder()));
      tfFloorName.setText(floor.getName());
      
      ImageIcon image = floor.getImage();
      
      ImageComponent imageComponent = null;
      Dimension floorSize = floor.getFloorSize();
      floorPanel.setPreferredSize(floorSize);
      floorPanel.setOpaque(true);
      if (image != null) {
        imageComponent = new ImageComponent();
        floorPanel.add(imageComponent);
        imageComponent.setImage(image.getImage());
        imageComponent.setBounds(0, 0, (int)floorSize.getWidth(), (int)floorSize.getHeight());
        if (hasBackgroundColor()) {
          updateBackgroud(floor.getBackgroundColor());
        }
        else {
          floorPanel.setBackground(Color.WHITE);
        }
      }
      else if (hasBackgroundColor()) {
        updateBackgroud(floor.getBackgroundColor());
      }
      else {
        floorPanel.setBackground(Color.WHITE);
      }
      Set<ShopTable> tables = floor.getTables();
      if (tables != null) {
        for (ShopTable shopTable : tables) {
          floorPanel.add(new TableButton(shopTable));
        }
      }
      
      if (imageComponent != null) {
        floorPanel.moveToBack(imageComponent);
      }
      floorPanel.revalidate();
      floorPanel.repaint();
    } finally {
      ShopFloorDAO.getInstance().closeSession(session);
    }
  }
  
  public ShopFloor getFloor() {
    return floor;
  }
  
  public void setFloor(ShopFloor floor)
  {
    if (saveRequired) {
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), "Do you want to save changes to " + this.floor.getName(), "Confirm") == 0)
      {
        updateFloorNameAndSortOrder();
        saveRequired = false;
        btnUpdate.setEnabled(false);
      }
      else {
        saveRequired = false;
        btnUpdate.setEnabled(false);
      }
    }
    this.floor = floor;
    try {
      renderFloor();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  public void setSaveRequired(boolean saveRequired) {
    this.saveRequired = saveRequired;
    btnUpdate.setEnabled(saveRequired);
  }
  
  public void selectImageFromFile() {
    int option = fileChooser.showOpenDialog(POSUtil.getBackOfficeWindow());
    if (option != 0) {
      return;
    }
    
    File file = fileChooser.getSelectedFile();
    fileChooser.setCurrentDirectory(file);
    



    try
    {
      byte[] imageData = FileUtils.readFileToByteArray(file);
      ImageResource imageResource = new ImageResource();
      imageResource.setImageCategory(ImageResource.IMAGE_CATEGORY.FLOORPLAN);
      Image rImage = null;
      int imageSize = imageData.length / 1024;
      if (imageSize > 500) {
        if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "The image is too large. Do you want to resize?", "Confirm") != 0)
        {
          throw new PosException("The image is too large. Please select an image within 500kb in size");
        }
        rImage = ImageIO.read(file).getScaledInstance(1024, 768, 4);
        BufferedImage bufferedImage = new BufferedImage(rImage.getWidth(null), rImage.getHeight(null), 2);
        
        Graphics2D bGr = bufferedImage.createGraphics();
        bGr.drawImage(rImage, 0, 0, null);
        bGr.dispose();
        File outputfile = new File("saved.png");
        try {
          ImageIO.write(bufferedImage, "png", outputfile);
          byte[] resizeImageData = FileUtils.readFileToByteArray(outputfile);
          imageResource.setImageData(new SerialBlob(resizeImageData));
        } catch (IOException e) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage());
        }
      }
      else {
        imageResource.setImageData(new SerialBlob(imageData));
      }
      ImageResourceDAO.getInstance().save(imageResource);
      floor.setImageId(imageResource.getId());
      ShopFloorDAO.getInstance().saveOrUpdate(floor);
      renderFloor();
    } catch (Exception e1) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e1.getMessage(), e1);
    }
  }
  
  private void updateFloorNameAndSortOrder() {
    try {
      String text = tfFloorName.getText();
      if (StringUtils.isEmpty(text)) {
        POSMessageDialog.showError(SwingUtilities.windowForComponent(this), Messages.getString("FloorView.16"));
        return;
      }
      
      floor.setName(text);
      floor.setSortOrder(Integer.valueOf(tfSortOrder.getInteger()));
      
      ShopFloorDAO.getInstance().saveOrUpdate(floor);
      
      floorsList.repaint();
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e);
    }
  }
  
  private void insertTable(MouseEvent e)
  {
    try {
      if (floor == null) {
        return;
      }
      
      final ShopTableSelectionDialog dialog = new ShopTableSelectionDialog().getInstance();
      
      JButton btnNewTable = new JButton(Messages.getString("FloorView.7"));
      btnNewTable.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          ShopTableForm shopTableForm = new ShopTableForm(new ShopTable(), false);
          


          BeanEditorDialog shopTableDialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), shopTableForm);
          shopTableDialog.setTitle(Messages.getString("FloorView.18"));
          shopTableDialog.setPreferredSize(new Dimension(700, 500));
          shopTableDialog.open();
          dialog.refresh();
        }
        
      });
      dialog.getButtonPanel().add(btnNewTable, 0);
      dialog.pack();
      dialog.open();
      
      if (dialog.isCanceled()) {
        return;
      }
      
      ShopTable selectedTable = dialog.getSelectedTable();
      if (selectedTable == null) {
        JOptionPane.showMessageDialog(this, Messages.getString("FloorView.19"));
        return;
      }
      selectedTable.setFloor(floor);
      selectedTable.setX(Integer.valueOf(e.getX() - 20));
      selectedTable.setY(Integer.valueOf(e.getY() - 20));
      
      floor.addTotables(selectedTable);
      
      ShopFloorDAO.getInstance().saveOrUpdate(floor);
      
      TableButton button = new TableButton(selectedTable);
      floorPanel.add(button);
      floorPanel.moveToFront(button);
      floorPanel.revalidate();
      floorPanel.repaint();
    } catch (PosException e2) {
      POSMessageDialog.showError(SwingUtilities.getWindowAncestor(this), e2.getMessage());
    } catch (Exception e2) {
      POSMessageDialog.showError(SwingUtilities.getWindowAncestor(this), e2.getMessage(), e2);
    }
  }
  
  private void removeTables() {
    try {
      if (floor == null) {
        return;
      }
      
      Set<ShopTable> tables = floor.getTables();
      
      if (tables == null) {
        return;
      }
      int option = POSMessageDialog.showYesNoQuestionDialog(getParent(), Messages.getString("FloorView.20") + floor.getName() + "?", "Confirm");
      if (option != 0) {
        return;
      }
      
      floor.getTables().removeAll(tables);
      
      ShopFloorDAO.getInstance().saveOrUpdate(floor);
      renderFloor();
    } catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage(), e);
    }
  }
  
  private List<ShopTable> getSelectedTables() {
    Component[] components = floorPanel.getComponents();
    List<ShopTable> selectedTables = new ArrayList();
    for (Component component : components) {
      if ((component instanceof TableButton)) {
        TableButton tb = (TableButton)component;
        
        if (tb.isSelected()) {
          selectedTables.add(table);
        }
      }
    }
    return selectedTables;
  }
  
  class TableButton extends PosButton implements MouseListener, MouseMotionListener
  {
    ShopTable table;
    int pressedX;
    int pressedY;
    
    public TableButton(ShopTable table) {
      this.table = table;
      setText(table.getTableNumber() + "");
      setBounds(table.getX().intValue(), table.getY().intValue(), TerminalConfig.getFloorButtonWidth(), TerminalConfig.getFloorButtonHeight());
      setFont(new Font(getFont().getName(), getFont().getStyle(), TerminalConfig.getFloorButtonFontSize()));
      
      setForeground(floor.getForegroundColor());
      
      addMouseListener(this);
      addMouseMotionListener(this);
    }
    
    public ShopTable getTable() {
      return table;
    }
    

    public void mouseDragged(MouseEvent e)
    {
      if (!reArrange.isSelected()) {
        return;
      }
      try {
        int x = e.getX() - pressedX + getLocationx;
        int y = e.getY() - pressedY + getLocationy;
        
        int floorWidth = floorPanel.getBounds().width - 40;
        int floorHeight = floorPanel.getBounds().height - 40;
        if ((x < 0) || (x > floorWidth)) {
          return;
        }
        
        if ((y < 0) || (y > floorHeight)) {
          return;
        }
        
        table.setX(Integer.valueOf(x));
        table.setY(Integer.valueOf(y));
        

        saveRequired = true;
        btnUpdate.setEnabled(saveRequired);
        
        setLocation(x, y);
      } catch (Exception e2) {
        POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), Messages.getString("FloorView.24"));
      }
    }
    














    public void mouseMoved(MouseEvent e) {}
    














    public void mouseClicked(MouseEvent e) {}
    













    public void mousePressed(MouseEvent e)
    {
      pressedX = e.getX();
      pressedY = e.getY();
    }
    


    public void mouseReleased(MouseEvent e) {}
    

    public void mouseEntered(MouseEvent e) {}
    

    public void mouseExited(MouseEvent e) {}
  }
  

  public void setFloorTemplate(JList<ShopFloorTemplate> floorTemplatesList, ShopFloorTemplate selectedValue)
  {
    if ((saveRequired) && 
      (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), "Do you want to save changes", "Confirm") == 0)) {
      doSaveFloorTemplate();
    }
    
    selectedTemplate = selectedValue;
    this.floorTemplatesList = floorTemplatesList;
    if (selectedValue != null) {
      copyTablePositionFromTemplate(selectedValue);
      try {
        renderFloor();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    saveRequired = false;
    btnUpdate.setEnabled(false);
  }
  
  private void copyTablePositionFromTemplate(ShopFloorTemplate template) {
    if (floor.getTables() == null) {
      return;
    }
    for (ShopTable table : floor.getTables()) {
      String position = template.getProperty(String.valueOf(table.getId()));
      if (position != null) {
        String[] split = position.split(",");
        table.setX(Integer.valueOf(split[0]));
        table.setY(Integer.valueOf(split[1]));
      }
    }
  }
  
  private void doClearBackgroudImage()
  {
    try {
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Are you sure to remove background floor image?", "Remove image");
      if (option != 0) {
        return;
      }
      floor.setImageId(null);
      ShopFloorDAO.getInstance().saveOrUpdate(floor);
      renderFloor();
    } catch (Exception e2) {
      PosLog.error(FloorConfigurationFloorView.class, e2.getMessage(), e2);
      POSMessageDialog.showError(this, e2.getMessage(), e2);
    }
  }
  
  private void updateImagePostion() {
    Component[] components = floorPanel.getComponents();
    for (Component component : components) {
      if ((component instanceof ImageComponent)) {
        Dimension floorSize = floor.getFloorSize();
        component.setBounds(0, 0, (int)floorSize.getWidth(), (int)floorSize.getHeight());
      }
    }
  }
}
