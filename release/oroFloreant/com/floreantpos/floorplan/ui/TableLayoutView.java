package com.floreantpos.floorplan.ui;

import com.floreantpos.POSConstants;
import com.floreantpos.PosCacheEventListener;
import com.floreantpos.PosException;
import com.floreantpos.actions.GroupSettleTicketAction;
import com.floreantpos.actions.MergeTicketsAction;
import com.floreantpos.actions.NewBarTabAction;
import com.floreantpos.actions.ShowTransactionsAuthorizationsAction;
import com.floreantpos.actions.TransferTicketItemsAction;
import com.floreantpos.main.Application;
import com.floreantpos.model.ShopFloor;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.ShopTableStatus;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.ShopFloorDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosBlinkButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.GuestCheckTktFirstOpenDialog;
import com.floreantpos.ui.dialog.GuestChkBillDialog;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.tableselection.BarTabSelectionView;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.FontMetrics;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import net.miginfocom.swing.MigLayout;
import net.sf.ehcache.Element;









public class TableLayoutView
  extends JPanel
  implements ActionListener, ChangeListener, DataChangeListener
{
  private POSToggleButton btnGroup;
  private POSToggleButton btnUnGroup;
  private PosButton btnDone;
  private PosButton btnCancel;
  private PosBlinkButton btnRefresh;
  private PosButton btnAuthorize;
  private POSToggleButton btnHoldFire;
  private POSToggleButton btnGuestCheck;
  private POSToggleButton btnSplitCheck;
  private POSToggleButton btnSettle;
  public POSToggleButton btnReorder;
  public POSToggleButton btnRelease;
  public PosButton btnCancelDialog;
  private ButtonGroup btnGroups;
  public JTabbedPane floorTab;
  private FloorView selectedFloorView;
  private boolean seat;
  private boolean complete;
  private MapTableSelectionView mapTableSelectionView;
  private Timer blinkTimer = new Timer(5000, this);
  
  private PosButton btnSwitchOptions;
  
  private boolean isOptions;
  private PosButton btnTmFirstOpened;
  private PosButton btnTmBillPrint;
  public PosButton btnTimeLog;
  public PosButton btnStatus;
  public PosButton btnNewBarTab;
  private PosButton btnMargeTickets;
  private PosButton btnTransferTicketItems;
  private POSToggleButton btnRearrange;
  private PosButton btnGroupSettle;
  private static final String BAR_TAB = "Bar Tab";
  
  public TableLayoutView(MapTableSelectionView mapTableSelectionView)
  {
    setLayout(new BorderLayout(10, 10));
    this.mapTableSelectionView = mapTableSelectionView;
    floorTab = new JTabbedPane(1, 1);
    floorTab.addChangeListener(this);
    floorTab.setUI(new BasicTabbedPaneUI()
    {
      protected int calculateTabHeight(int tabPlacement, int tabIndex, int fontHeight) {
        return 30;
      }
      
      protected int calculateTabWidth(int tabPlacement, int tabIndex, FontMetrics metrics)
      {
        return 150;
      }
      

    });
    createButtonActionPanel();
    createLayoutPanel();
    

    PosCacheEventListener.getInstance().addDataChangeListener(this);
  }
  
  private void createLayoutPanel() {
    try {
      List<ShopFloor> shopFloors = ShopFloorDAO.getInstance().findAll();
      
      for (ShopFloor shopFloor : shopFloors) {
        FloorView floorView = new FloorView(mapTableSelectionView, this, shopFloor);
        floorTab.addTab(shopFloor.getName(), floorView);
      }
      
      add(floorTab, "Center");
    }
    catch (PosException e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getLocalizedMessage(), e);
    }
  }
  
  public void rendererTables() {
    if ((floorTab.getSelectedComponent() instanceof FloorView)) {
      FloorView selectedComponent = (FloorView)floorTab.getSelectedComponent();
      selectedComponent.renderFloor();
    }
    else if ((floorTab.getSelectedComponent() instanceof BarTabSelectionView)) {
      BarTabSelectionView selectedComponent = (BarTabSelectionView)floorTab.getSelectedComponent();
      selectedComponent.updateView(mapTableSelectionView.getOrderType());
    }
    setVisibleBarTabBtn(floorTab.getSelectedComponent() instanceof BarTabSelectionView);
  }
  
  private void createButtonActionPanel()
  {
    TitledBorder titledBorder2 = BorderFactory.createTitledBorder(null, "-", 2, 0);
    
    JPanel rightPanel = new JPanel(new BorderLayout(20, 20));
    rightPanel.setPreferredSize(PosUIManager.getSize(130, 0));
    rightPanel.setBorder(new CompoundBorder(titledBorder2, new EmptyBorder(2, 2, 6, 2)));
    
    JPanel actionBtnPanel = new JPanel(new MigLayout("hidemode 3,wrap 1", "grow,sg,fill", ""));
    
    btnGroups = new ButtonGroup();
    
    btnGroup = new POSToggleButton(POSConstants.GROUP);
    btnUnGroup = new POSToggleButton(POSConstants.UNGROUP);
    btnDone = new PosButton(POSConstants.SAVE_BUTTON_TEXT);
    btnCancel = new PosButton(POSConstants.CANCEL);
    
    btnHoldFire = new POSToggleButton("HOLD FIRE");
    btnGuestCheck = new POSToggleButton("GUEST CHECK");
    btnSplitCheck = new POSToggleButton("SPLIT CHECK");
    btnSettle = new POSToggleButton("SETTLE");
    btnGroupSettle = new PosButton("GROUP SETTLE");
    btnAuthorize = new PosButton("AUTHORIZE");
    btnReorder = new POSToggleButton("REORDER");
    btnRelease = new POSToggleButton("RELEASE");
    
    btnGroup.addActionListener(this);
    btnUnGroup.addActionListener(this);
    btnDone.addActionListener(this);
    btnCancel.addActionListener(this);
    
    btnDone.setVisible(false);
    btnCancel.setVisible(false);
    
    btnGroup.setIcon(new ImageIcon(getClass().getResource("/images/plus.png")));
    btnUnGroup.setIcon(new ImageIcon(getClass().getResource("/images/minus2.png")));
    
    btnGroups.add(btnGroup);
    btnGroups.add(btnUnGroup);
    
    btnGroups.add(btnHoldFire);
    btnGroups.add(btnGuestCheck);
    btnGroups.add(btnSplitCheck);
    btnGroups.add(btnSettle);
    btnGroups.add(btnReorder);
    btnGroups.add(btnRelease);
    
    btnNewBarTab = new PosButton("New Tab");
    btnNewBarTab.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        List<ShopTable> selectedTables = getSelectedTables();
        new NewBarTabAction(mapTableSelectionView.getOrderType(), selectedTables, Application.getPosWindow()).actionPerformed(e);
      }
      
    });
    btnMargeTickets = new PosButton("MERGE");
    btnMargeTickets.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TableLayoutView.this.doShowMargeTicketDialog();
      }
      
    });
    btnTransferTicketItems = new PosButton("<html><body><center>TRANSFER<br/>ITEMS</center></body></html>");
    btnTransferTicketItems.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TableLayoutView.this.doShowTransferTicketItemsDialog();
      }
      
    });
    actionBtnPanel.add(btnGroup, "grow");
    actionBtnPanel.add(btnUnGroup, "grow");
    
    actionBtnPanel.add(btnNewBarTab, "grow");
    
    actionBtnPanel.add(btnDone, "grow");
    actionBtnPanel.add(btnCancel, "grow");
    
    actionBtnPanel.add(btnHoldFire, "grow");
    actionBtnPanel.add(btnGuestCheck, "grow");
    actionBtnPanel.add(btnSplitCheck, "grow");
    actionBtnPanel.add(btnMargeTickets, "grow");
    actionBtnPanel.add(btnTransferTicketItems, "grow");
    actionBtnPanel.add(btnSettle, "grow");
    actionBtnPanel.add(btnGroupSettle, "grow");
    actionBtnPanel.add(btnReorder, "grow");
    actionBtnPanel.add(btnAuthorize, "grow");
    actionBtnPanel.add(btnRelease, "grow");
    
    btnTmFirstOpened = new PosButton("<html><center>TIME SINCE FIRST OPENED</center></html>");
    btnTmBillPrint = new PosButton("<html><center>TIME SINCE BILL PRINT</center></html>");
    
    btnTimeLog = new PosButton("TIME LOG");
    btnStatus = new PosButton("STATUS");
    
    btnRearrange = new POSToggleButton("REARRANGE");
    btnRearrange.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent arg0)
      {
        TableLayoutView.this.doSetRearrangeMode();
      }
      
    });
    btnTimeLog.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent arg0)
      {
        TableLayoutView.this.toggleTimeLog(isOptions, false);
      }
      
    });
    btnStatus.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent arg0)
      {
        TableLayoutView.this.toggleTimeLog(isOptions, true);
      }
      
    });
    btnTmFirstOpened.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TableLayoutView.this.doShowTimeFirstOpen();
      }
      
    });
    btnTmBillPrint.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TableLayoutView.this.doShowTimeBillPrint();
      }
      
    });
    actionBtnPanel.add(btnTimeLog, "grow");
    actionBtnPanel.add(btnStatus, "grow");
    
    actionBtnPanel.add(btnTmFirstOpened, "grow");
    actionBtnPanel.add(btnTmBillPrint, "grow");
    actionBtnPanel.add(btnRearrange, "grow");
    
    rightPanel.add(actionBtnPanel, "Center");
    
    isOptions = false;
    btnSwitchOptions = new PosButton("MORE..");
    btnSwitchOptions.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TableLayoutView.this.doShowSpecialFunc();
      }
    });
    actionBtnPanel.add(btnSwitchOptions, "grow");
    
    btnRefresh = new PosBlinkButton(POSConstants.REFRESH);
    btnRefresh.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doRefresh();
      }
    });
    actionBtnPanel.add(btnRefresh, "grow");
    
    btnAuthorize.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        new ShowTransactionsAuthorizationsAction().execute();
      }
      

    });
    btnGroupSettle.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        new GroupSettleTicketAction(Application.getCurrentUser()).actionPerformed(null);
        doRefresh();
      }
      
    });
    btnCancelDialog = new PosButton(POSConstants.CANCEL);
    btnCancelDialog.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        TableLayoutView.this.closeDialog(true);
      }
      
    });
    actionBtnPanel.add(btnCancelDialog, "grow");
    
    add(rightPanel, "East");
  }
  
  private void closeDialog(boolean canceled)
  {
    Window windowAncestor = SwingUtilities.getWindowAncestor(this);
    if ((windowAncestor instanceof POSDialog)) {
      ((POSDialog)windowAncestor).setCanceled(canceled);
      windowAncestor.dispose();
    }
  }
  
  public void actionPerformed(ActionEvent e) {
    if (!(floorTab.getSelectedComponent() instanceof FloorView)) {
      return;
    }
    Object object = e.getSource();
    selectedFloorView = ((FloorView)floorTab.getSelectedComponent());
    if (object == btnGroup) {
      if (mapTableSelectionView.isCreateNewTicket()) {
        selectedFloorView.addedTableListModel.clear();
      }
      btnUnGroup.setVisible(false);
      btnDone.setVisible(true);
      btnCancel.setVisible(true);
    }
    else if (object == btnUnGroup) {
      selectedFloorView.removeTableListModel.clear();
      btnGroup.setVisible(false);
      btnDone.setVisible(true);
      btnCancel.setVisible(true);
    }
    else if (object == btnDone) {
      if (btnGroup.isSelected()) {
        selectedFloorView.doGroupAction();
        if (mapTableSelectionView.isCreateNewTicket())
          clearFloorView();
        clearSelection();
      }
      else if (btnUnGroup.isSelected()) {
        selectedFloorView.doUnGroupAction();
        clearFloorView();
        clearSelection();
      }
    }
    else if (object == btnCancel) {
      clearFloorView();
      clearSelection();
      
      if (mapTableSelectionView.isCreateNewTicket()) {
        rendererTables();
      }
    }
    else if (object == blinkTimer) {
      btnRefresh.setBlinking(true);
    }
  }
  

  public void stateChanged(ChangeEvent e)
  {
    if ((e.getSource() instanceof JTabbedPane)) {
      JTabbedPane pane = (JTabbedPane)e.getSource();
      isOptions = false;
      if ((pane.getSelectedComponent() instanceof FloorView)) {
        selectedFloorView = ((FloorView)pane.getSelectedComponent());
        selectedFloorView.renderFloor();
      }
      else if ((pane.getSelectedComponent() instanceof BarTabSelectionView)) {
        BarTabSelectionView barTabSelectionView = (BarTabSelectionView)pane.getSelectedComponent();
        barTabSelectionView.updateView(mapTableSelectionView.getOrderType());
      }
      setVisibleBarTabBtn(pane.getSelectedComponent() instanceof BarTabSelectionView);
    }
  }
  
  public void clearFloorView()
  {
    selectedFloorView = ((FloorView)floorTab.getSelectedComponent());
    selectedFloorView.clearSelection();
  }
  
  public void clearSelection() {
    btnGroups.clearSelection();
    btnGroup.setVisible(true);
    btnUnGroup.setVisible(true);
    btnDone.setVisible(false);
    btnCancel.setVisible(false);
    showOptions(isOptions);
  }
  
  public boolean shouldGroup() {
    return btnGroup.isSelected();
  }
  
  public boolean shouldUnGroup() {
    return btnUnGroup.isSelected();
  }
  
  public boolean shouldHoldFire() {
    return btnHoldFire.isSelected();
  }
  
  public boolean shouldGuestCheck() {
    return btnGuestCheck.isSelected();
  }
  
  public boolean shouldSplitCheck() {
    return btnSplitCheck.isSelected();
  }
  
  public boolean shouldSettle() {
    return btnSettle.isSelected();
  }
  
  public boolean shouldReorder() {
    return btnReorder.isSelected();
  }
  
  public boolean shouldRelease() {
    return btnRelease.isSelected();
  }
  
  public boolean isSeat() {
    return seat;
  }
  
  public void setSeat(boolean seat) {
    this.seat = seat;
  }
  
  public boolean isComplete() {
    return complete;
  }
  
  public void setComplete(boolean complete) {
    this.complete = complete;
  }
  
  public void setOk() {
    selectedFloorView.doGroupAction();
    clearFloorView();
    clearSelection();
  }
  
  public void setCancel() {
    selectedFloorView.doUnGroupAction();
    clearFloorView();
    clearSelection();
    rendererTables();
  }
  
  public List<ShopTable> getSelectedTables() {
    return selectedFloorView.getSelectedTables();
  }
  
  public void setTicket(Ticket ticket) {
    selectedFloorView.addedTableListModel.addAll(ticket.getTableNumbers());
  }
  
  private void doShowSpecialFunc() {
    if (!isOptions) {
      btnSwitchOptions.setText("BACK");
      isOptions = true;
      showOptions(isOptions);
      btnRearrange.setSelected(false);
      doSetRearrangeMode();
    }
    else {
      isOptions = false;
      toggleTimeLog(isOptions, false);
      showOptions(isOptions);
    }
  }
  
  private void doShowMargeTicketDialog() {
    new MergeTicketsAction().actionPerformed(null);
    doRefresh();
  }
  
  private void doShowTransferTicketItemsDialog() {
    new TransferTicketItemsAction().actionPerformed(null);
    doRefresh();
  }
  
  private void showOptions(boolean isVisible) {
    if (!isVisible) {
      btnSwitchOptions.setText("MORE..");
      selectedFloorView.setRearrange(false);
    }
    btnTmFirstOpened.setVisible(isVisible);
    btnTmBillPrint.setVisible(isVisible);
    btnTimeLog.setVisible(isVisible);
    btnStatus.setVisible(isVisible);
    btnRearrange.setVisible(isVisible);
    
    btnGroup.setVisible(!isVisible);
    btnUnGroup.setVisible(!isVisible);
    btnHoldFire.setVisible(!isVisible);
    btnGuestCheck.setVisible(!isVisible);
    btnSplitCheck.setVisible(!isVisible);
    btnSettle.setVisible(!isVisible);
    btnGroupSettle.setVisible(!isVisible);
    btnRelease.setVisible(!isVisible);
    btnAuthorize.setVisible(!isVisible);
    btnRefresh.setVisible(!isVisible);
    btnMargeTickets.setVisible(!isVisible);
    btnTransferTicketItems.setVisible(!isVisible);
  }
  
  private void toggleTimeLog(boolean on, boolean isShowStatus) {
    try {
      if (selectedFloorView == null) {
        return;
      }
      
      Set<ShopTable> tables = selectedFloorView.getFloor().getTables();
      if (tables == null) {
        return;
      }
      
      ArrayList<Ticket> availableTickets = new ArrayList();
      
      for (ShopTable shopTable : tables) {
        if (on) {
          List<String> ticketNumbers = shopTable.getShopTableStatus().getListOfTicketNumbers();
          if ((ticketNumbers != null) && (ticketNumbers.size() > 0)) {
            int guestNumber = 0;
            for (String string : ticketNumbers) {
              Ticket ticket = TicketDAO.getInstance().get(string);
              if (ticket != null) {
                shopTable.setTicketCreateTime(ticket.getCreateDate());
                availableTickets.add(ticket);
                guestNumber += ticket.getNumberOfGuests().intValue();
              }
              else {
                shopTable.setTicketCreateTime(null);
              }
            }
            
            shopTable.setGuestNumber(guestNumber >= 0 ? guestNumber : 0);
            shopTable.setShowStatus(isShowStatus);
          }
          else {
            shopTable.setTicketCreateTime(null);
            shopTable.setShowStatus(false);
          }
        }
        else {
          shopTable.setTicketCreateTime(null);
          shopTable.setShowStatus(false);
        }
      }
      selectedFloorView.updateTables();
    } catch (Exception e) {
      e.printStackTrace();
      POSMessageDialog.showError(POSConstants.ERROR_MESSAGE + e);
    }
  }
  
  private void doShowTimeFirstOpen() {
    GuestCheckTktFirstOpenDialog dialog = new GuestCheckTktFirstOpenDialog();
    dialog.setData(getAvailableTickets());
    dialog.setSize(PosUIManager.getSize(700, 500));
    dialog.open();
  }
  
  private void doShowTimeBillPrint() {
    GuestChkBillDialog dialog = new GuestChkBillDialog();
    dialog.setSize(PosUIManager.getSize(700, 500));
    dialog.open();
  }
  
  private ArrayList<Ticket> getAvailableTickets() {
    ArrayList<Ticket> availableTickets = new ArrayList();
    try
    {
      if (selectedFloorView == null) {
        return availableTickets;
      }
      
      Set<ShopTable> tables = selectedFloorView.getFloor().getTables();
      if (tables == null) {
        return availableTickets;
      }
      
      for (ShopTable shopTable : tables) {
        List<String> ticketNumbers = shopTable.getShopTableStatus().getListOfTicketNumbers();
        if ((ticketNumbers != null) && (ticketNumbers.size() > 0)) {
          String string = (String)ticketNumbers.get(0);
          Ticket ticket = TicketDAO.getInstance().get(string);
          if (ticket != null) {
            availableTickets.add(ticket);
          }
        }
      }
    } catch (Exception e) {
      POSMessageDialog.showError(POSConstants.ERROR_MESSAGE + e);
    }
    return availableTickets;
  }
  
  public void doRefresh() {
    btnRefresh.setBlinking(false);
    blinkTimer.stop();
    mapTableSelectionView.setCustomerBookingInfoPanel();
    clearSelection();
    rendererTables();
    blinkTimer.restart();
  }
  
  public void setVisibleBarTabBtn(boolean isVisible) {
    btnNewBarTab.setVisible(isVisible);
    btnHoldFire.setVisible(!isVisible);
    btnGuestCheck.setVisible(!isVisible);
    btnSplitCheck.setVisible(!isVisible);
    btnSettle.setVisible(!isVisible);
    btnGroupSettle.setVisible(!isVisible);
    btnAuthorize.setVisible(!isVisible);
    btnRelease.setVisible(!isVisible);
    btnSwitchOptions.setVisible(!isVisible);
  }
  
  private void doSetRearrangeMode() {
    try {
      if (selectedFloorView == null) {
        return;
      }
      selectedFloorView.setRearrange(btnRearrange.isSelected());
    } catch (Exception e) {
      e.printStackTrace();
      POSMessageDialog.showError(POSConstants.ERROR_MESSAGE + e);
    }
  }
  

  public void dataAdded(Object object) {}
  

  public void dataChanged(Object object)
  {
    Element element = (Element)object;
    Object objectKey = element.getObjectKey();
    if (objectKey.toString().contains("ShopTable")) {
      btnRefresh.setBlinking(true);
    }
  }
  

  public void dataRemoved(Object object) {}
  

  public Object getSelectedData()
  {
    return null;
  }
  
  public void dataSetUpdated() {}
  
  public void dataChangeCanceled(Object object) {}
}
