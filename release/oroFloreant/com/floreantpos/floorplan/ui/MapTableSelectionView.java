package com.floreantpos.floorplan.ui;

import com.floreantpos.model.BookingInfo;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.BookingInfoDAO;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.tableselection.BarTabSelectionView;
import com.floreantpos.ui.tableselection.TableSelector;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;

public class MapTableSelectionView extends TableSelector implements com.floreantpos.ui.views.order.actions.DataChangeListener
{
  private JPanel leftBookingOrderPanel;
  private TableLayoutView tableLayoutView;
  private BarTabSelectionView barTabSelectionView;
  private static MapTableSelectionView instance;
  
  public MapTableSelectionView()
  {
    initComponents();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    
    createTableLayoutViewPanel();
  }
  
  private void createLeftBookingInfoPanel() {
    JPanel leftPanel = new JPanel(new BorderLayout());
    leftBookingOrderPanel = new JPanel(new MigLayout("fillx"));
    TitledBorder titledBorder = BorderFactory.createTitledBorder(null, "-", 2, 0);
    leftBookingOrderPanel.setBorder(new CompoundBorder(titledBorder, new javax.swing.border.EmptyBorder(0, 5, 0, 5)));
    JScrollPane scrollPane = new JScrollPane(leftBookingOrderPanel);
    setCustomerBookingInfoPanel();
    JPanel topButtonPanel = new JPanel(new MigLayout("fill"));
    JButton btnAddBooking = new JButton("Add Booking");
    btnAddBooking.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        POSDialog posDialog = new POSDialog();
        posDialog.setLayout(new MigLayout("fill"));
        posDialog.add(new AddTableBookingView(posDialog, new TableBookingForm(new BookingInfo())), "grow");
        posDialog.setTitle("Add Booking");
        posDialog.setDefaultCloseOperation(2);
        posDialog.openFullScreen();
        
        if (!posDialog.isCanceled()) {
          setCustomerBookingInfoPanel();
        }
        
      }
    });
    topButtonPanel.add(btnAddBooking, "left");
    leftPanel.add(topButtonPanel, "North");
    leftPanel.add(scrollPane, "Center");
    add(leftPanel, "West");
  }
  
  private void createTableLayoutViewPanel()
  {
    tableLayoutView = new TableLayoutView(this);
    barTabSelectionView = new BarTabSelectionView();
    add(tableLayoutView, "Center");
  }
  
  public void setCustomerBookingInfoPanel() {
    if (leftBookingOrderPanel == null)
      return;
    leftBookingOrderPanel.removeAll();
    List todaysBookingList = BookingInfoDAO.getInstance().getTodaysBooking();
    Iterator iterator;
    if (todaysBookingList != null) {
      for (iterator = todaysBookingList.iterator(); iterator.hasNext();) {
        BookingInfo bookingInfo = (BookingInfo)iterator.next();
        FloorBookingView floorBookingView = new FloorBookingView(bookingInfo, this);
        leftBookingOrderPanel.add(floorBookingView, "growx,wrap");
      }
    }
  }
  

  public void rendererTables()
  {
    if ((tableLayoutView.floorTab.getSelectedComponent() instanceof FloorView)) {
      FloorView selectedComponent = (FloorView)tableLayoutView.floorTab.getSelectedComponent();
      if (selectedComponent == null) {
        return;
      }
      selectedComponent.renderFloor();
    }
    
    if ((tableLayoutView.floorTab.getSelectedComponent() instanceof BarTabSelectionView)) {
      BarTabSelectionView selectedComponent = (BarTabSelectionView)tableLayoutView.floorTab.getSelectedComponent();
      if (selectedComponent == null) {
        return;
      }
      selectedComponent.updateView(orderType);
    }
    
    if (getOrderType() == null) {
      return;
    }
    
    if (getOrderType().isEnableReorder().booleanValue()) {
      tableLayoutView.btnReorder.setVisible(true);
    }
    else {
      tableLayoutView.btnReorder.setVisible(false);
    }
    
    boolean isContain = false;
    
    Component[] components = tableLayoutView.floorTab.getComponents();
    for (Component component : components) {
      if ((component instanceof BarTabSelectionView)) {
        isContain = true;
      }
    }
    
    if (!orderType.isBarTab().booleanValue()) {
      if (isContain) {
        tableLayoutView.floorTab.remove(barTabSelectionView);
      }
    } else {
      if (!isContain) {
        tableLayoutView.floorTab.addTab("Bar Tab", barTabSelectionView);
        barTabSelectionView.updateView(orderType);
      }
      tableLayoutView.floorTab.setSelectedComponent(barTabSelectionView);
    }
  }
  

  public List<ShopTable> getSelectedTables()
  {
    return tableLayoutView.getSelectedTables();
  }
  
  public void setTicket(Ticket ticket)
  {
    super.setTicket(ticket);
    tableLayoutView.setTicket(ticket);
  }
  
  public void updateView(boolean update)
  {
    tableLayoutView.btnCancelDialog.setVisible(update);
  }
  


  public static MapTableSelectionView getInstance()
  {
    if (null == instance)
      instance = new MapTableSelectionView();
    return instance;
  }
  


  public void dataAdded(Object object) {}
  


  public void dataChanged(Object object) {}
  

  public void dataRemoved(Object object) {}
  

  public Object getSelectedData()
  {
    return null;
  }
  
  public void dataSetUpdated() {}
  
  public void dataChangeCanceled(Object object) {}
}
