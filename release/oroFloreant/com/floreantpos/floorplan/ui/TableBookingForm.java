package com.floreantpos.floorplan.ui;

import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.customer.CustomerSelectorDialog;
import com.floreantpos.customer.CustomerSelectorFactory;
import com.floreantpos.floorplan.ui.booking.BookingTableSelectionDialog;
import com.floreantpos.main.Application;
import com.floreantpos.model.BookingInfo;
import com.floreantpos.model.Customer;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.ShopTableStatus;
import com.floreantpos.model.TableStatus;
import com.floreantpos.model.dao.BookingInfoDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.POSComboBox;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BookingBeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.POSUtil;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.StaleObjectStateException;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXMonthView;


public class TableBookingForm
  extends BookingBeanEditor<BookingInfo>
{
  private static Log logger = LogFactory.getLog(TableBookingForm.class);
  
  private JLabel tables;
  
  private JTextField txtTax;
  
  private DoubleTextField txtBookingCharge;
  private DoubleTextField txtRemainingBalance;
  private DoubleTextField txtPaidAmount;
  private IntegerTextField txtGuestCount;
  private PosButton btnSearchTables;
  private JXDatePicker tbStartDate;
  private JXDatePicker tbEndDate;
  private POSComboBox cStartHour;
  private POSComboBox cStartMin;
  private JRadioButton rbStartAm;
  private JRadioButton rbStartPm;
  private POSComboBox cEndHour;
  private POSComboBox cEndMin;
  private JRadioButton rbEndAm;
  private JRadioButton rbEndPm;
  private PosButton btnSelectCustomer;
  private JTextField txtSelectedCustomer;
  protected String searchParameter;
  protected Date stardDateForSearchBooking;
  protected Date endDateForSearchBooking;
  private ButtonGroup btnGroupStartAmPm;
  private ButtonGroup btnGroupEndAmPm;
  private Customer customer;
  private TableBookingBrowser bookingBrowser;
  private List<ShopTable> selectedTableList;
  private final int TF_HEIGHT = 30;
  private final int TB_HEIGHT = 40;
  private final int TB_WIDTH = 40;
  private final int FONT_SIZE = 15;
  private final int COMBO_WIDTH = 100;
  private JTextComponent tfTableList;
  
  public TableBookingForm(BookingInfo tableBookingInfo)
  {
    createTableBookingPanel();
    createNew();
    setBean(tableBookingInfo);
  }
  
  private void createTableBookingPanel() {
    txtSelectedCustomer = new JTextField();
    btnSelectCustomer = new PosButton(Messages.getString("TableBookingForm.25"));
    
    btnGroupStartAmPm = new ButtonGroup();
    rbStartAm = new JRadioButton(Messages.getString("TableBookingForm.27"));
    rbStartPm = new JRadioButton(Messages.getString("TableBookingForm.28"));
    btnGroupStartAmPm.add(rbStartAm);
    btnGroupStartAmPm.add(rbStartPm);
    rbStartPm.setSelected(true);
    
    btnGroupEndAmPm = new ButtonGroup();
    rbEndAm = new JRadioButton(Messages.getString("TableBookingForm.33"));
    rbEndPm = new JRadioButton(Messages.getString("TableBookingForm.34"));
    btnGroupEndAmPm.add(rbEndAm);
    btnGroupEndAmPm.add(rbEndPm);
    rbEndPm.setSelected(true);
    
    tbStartDate = UiUtil.getCurrentMonthStart();
    tbStartDate.setDate(new Date());
    
    Calendar calendar = tbStartDate.getMonthView().getCalendar();
    calendar.setTime(new Date());
    
    tbStartDate.setPreferredSize(PosUIManager.getSize(0, 40));
    JButton btnTbStartDate = (JButton)tbStartDate.getComponent(1);
    btnTbStartDate.setPreferredSize(PosUIManager.getSize(40, 0));
    JXMonthView monthViewStartDate = tbStartDate.getMonthView();
    monthViewStartDate.setLowerBound(calendar.getTime());
    monthViewStartDate.setFont(new Font(monthViewStartDate.getFont().getName(), 0, 15));
    
    tbStartDate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tbEndDate.setDate(tbStartDate.getDate());
      }
      
    });
    tbEndDate = UiUtil.getCurrentMonthEnd();
    tbEndDate.setDate(new Date());
    
    tbEndDate.setPreferredSize(PosUIManager.getSize(0, 40));
    JButton btnTbEndDate = (JButton)tbEndDate.getComponent(1);
    btnTbEndDate.setPreferredSize(PosUIManager.getSize(40, 0));
    JXMonthView monthViewEndDate = tbEndDate.getMonthView();
    monthViewEndDate.setLowerBound(calendar.getTime());
    monthViewEndDate.setFont(new Font(monthViewEndDate.getFont().getName(), 0, 15));
    


    Vector<Integer> hours = new Vector();
    for (int i = 1; i <= 12; i++) {
      hours.add(Integer.valueOf(i));
    }
    
    setLayout(new MigLayout("fillx", " [][grow][]", ""));
    
    JLabel from = new JLabel(Messages.getString("TableBookingForm.0"));
    JLabel startDate = new JLabel(Messages.getString("TableBookingForm.1"));
    JLabel startHours = new JLabel(Messages.getString("TableBookingForm.2"));
    JLabel startMin = new JLabel(Messages.getString("TableBookingForm.3"));
    
    JLabel to = new JLabel(Messages.getString("TableBookingForm.4"));
    JLabel endDate = new JLabel(Messages.getString("TableBookingForm.5"));
    JLabel endHours = new JLabel(Messages.getString("TableBookingForm.6"));
    JLabel endMin = new JLabel(Messages.getString("TableBookingForm.7"));
    
    JLabel customer = new JLabel(Messages.getString("TableBookingForm.8"));
    tables = new JLabel();
    JLabel bookingCharge = new JLabel(Messages.getString("TableBookingForm.12"));
    JLabel remainingBalance = new JLabel(Messages.getString("TableBookingForm.13"));
    JLabel paidAmount = new JLabel(Messages.getString("TableBookingForm.14"));
    JLabel guestCount = new JLabel("Guest number");
    
    btnSearchTables = new PosButton(Messages.getString("TableBookingForm.23"));
    txtBookingCharge = new DoubleTextField();
    txtBookingCharge.setText(Messages.getString("TableBookingForm.67"));
    
    txtPaidAmount = new DoubleTextField();
    txtPaidAmount.setText(Messages.getString("TableBookingForm.68"));
    
    txtGuestCount = new IntegerTextField();
    txtGuestCount.setText(Messages.getString("TableBookingForm.69"));
    
    txtRemainingBalance = new DoubleTextField();
    txtRemainingBalance.setText(Messages.getString("TableBookingForm.70"));
    
    txtTax = new JTextField();
    
    DefaultComboBoxModel stMinModel = new DefaultComboBoxModel();
    stMinModel.addElement(Integer.valueOf(0));
    stMinModel.addElement(Integer.valueOf(15));
    stMinModel.addElement(Integer.valueOf(30));
    stMinModel.addElement(Integer.valueOf(45));
    
    DefaultComboBoxModel<Integer> etMinModel = new DefaultComboBoxModel();
    etMinModel.addElement(Integer.valueOf(0));
    etMinModel.addElement(Integer.valueOf(15));
    etMinModel.addElement(Integer.valueOf(30));
    etMinModel.addElement(Integer.valueOf(45));
    
    cStartHour = new POSComboBox();
    cStartHour.setPreferredSize(PosUIManager.getSize(100, 0));
    cStartHour.setModel(new DefaultComboBoxModel(hours));
    cStartMin = new POSComboBox();
    cStartMin.setPreferredSize(PosUIManager.getSize(100, 0));
    cStartMin.setModel(stMinModel);
    cEndHour = new POSComboBox();
    cEndHour.setPreferredSize(PosUIManager.getSize(100, 0));
    cEndHour.setModel(new DefaultComboBoxModel(hours));
    cEndHour.setSelectedIndex(1);
    cEndMin = new POSComboBox();
    cEndMin.setPreferredSize(PosUIManager.getSize(100, 0));
    cEndMin.setModel(etMinModel);
    
    cStartHour.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        Integer selectedItem = (Integer)cStartHour.getSelectedItem();
        if (selectedItem.intValue() == 12) {
          selectedItem = Integer.valueOf(1);
        }
        else {
          selectedItem = Integer.valueOf(selectedItem.intValue() + 1);
        }
        cEndHour.setSelectedItem(selectedItem);
      }
      
    });
    add(from, "right");
    add(startDate, "split 8");
    add(tbStartDate, "gapright 10");
    add(startHours);
    add(cStartHour, "gapright 10");
    add(startMin);
    add(cStartMin, "gapright 10");
    add(rbStartAm, "gapright 10");
    add(rbStartPm, "wrap");
    
    add(to, "right");
    add(endDate, "split 8");
    add(tbEndDate, "gapright 10");
    add(endHours);
    add(cEndHour, "gapright 10");
    add(endMin);
    add(cEndMin, "gapright 10");
    add(rbEndAm, "gapright 10");
    add(rbEndPm, "wrap");
    
    btnSelectCustomer.setPreferredSize(PosUIManager.getSize(0, 30));
    txtSelectedCustomer.setPreferredSize(PosUIManager.getSize(0, 30));
    txtGuestCount.setPreferredSize(PosUIManager.getSize(0, 30));
    txtBookingCharge.setPreferredSize(PosUIManager.getSize(0, 30));
    txtPaidAmount.setPreferredSize(PosUIManager.getSize(0, 30));
    txtRemainingBalance.setPreferredSize(PosUIManager.getSize(0, 30));
    add(guestCount, "right");
    add(txtGuestCount, "grow, wrap");
    
    add(customer, "right");
    add(txtSelectedCustomer, "split 2, grow");
    add(btnSelectCustomer, "wrap");
    
    add(bookingCharge, "right");
    add(txtBookingCharge, "grow,wrap");
    
    add(paidAmount, "right");
    add(txtPaidAmount, "grow,wrap");
    
    add(remainingBalance, "right");
    add(txtRemainingBalance, "grow,wrap");
    
    PosButton btnTableSelect = new PosButton("Select table");
    btnTableSelect.setPreferredSize(PosUIManager.getSize(0, 30));
    tfTableList = new JTextField();
    tfTableList.setEditable(false);
    add(new JLabel("Table"), "right");
    add(tfTableList, "split 2, grow");
    add(btnTableSelect, "w 105!, wrap");
    
    btnTableSelect.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TableBookingForm.this.doTableSelection();
      }
      
    });
    tables.setText(Messages.getString("TableBookingForm.10"));
    txtBookingCharge.setEnabled(false);
    txtRemainingBalance.setEnabled(false);
    txtPaidAmount.setEnabled(false);
    txtGuestCount.setEnabled(false);
    tbStartDate.setEnabled(false);
    tbEndDate.setEnabled(false);
    cStartHour.setEnabled(false);
    cStartMin.setEnabled(false);
    cEndHour.setEnabled(false);
    cEndMin.setEnabled(false);
    btnSearchTables.setEnabled(false);
    txtSelectedCustomer.setEnabled(false);
    rbStartAm.setEnabled(false);
    rbStartPm.setEnabled(false);
    rbEndAm.setEnabled(false);
    rbEndPm.setEnabled(false);
    txtTax.setEnabled(false);
    initActionHandlers();
  }
  

  public void initActionHandlers()
  {
    btnSelectCustomer.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TableBookingForm.this.addCustomerToBooking();
      }
    });
  }
  


  public void setFieldsEnable(boolean enable)
  {
    btnSearchTables.setEnabled(enable);
    txtBookingCharge.setEnabled(enable);
    txtRemainingBalance.setEnabled(enable);
    txtPaidAmount.setEnabled(enable);
    txtGuestCount.setEnabled(enable);
    tbStartDate.setEnabled(enable);
    tbEndDate.setEnabled(enable);
    cStartHour.setEnabled(enable);
    cStartMin.setEnabled(enable);
    cEndHour.setEnabled(enable);
    cEndMin.setEnabled(enable);
    rbStartAm.setEnabled(enable);
    rbStartPm.setEnabled(enable);
    rbEndAm.setEnabled(enable);
    rbEndPm.setEnabled(enable);
    txtSelectedCustomer.setEnabled(enable);
    txtSelectedCustomer.setEditable(false);
    txtSelectedCustomer.setBackground(Color.white);
    txtTax.setEnabled(enable);
    btnSelectCustomer.setEnabled(enable);
  }
  
  public void createNew()
  {
    tables.setText(Messages.getString("TableBookingForm.54"));
    rbStartPm.setSelected(true);
    rbEndPm.setSelected(true);
    tbStartDate.setDate(new Date());
    tbEndDate.setDate(new Date());
    setStartHour();
    cStartMin.setSelectedIndex(0);
    cEndMin.setSelectedIndex(0);
    txtSelectedCustomer.setText("");
    txtGuestCount.setText("2");
  }
  
  private void setStartHour() {
    Calendar cal = Calendar.getInstance();
    int hourOfTheDay = cal.get(10);
    int setHour = hourOfTheDay + 1;
    cStartHour.setSelectedItem(Integer.valueOf(setHour));
    int am_pm = cal.get(9);
    if (setHour < 12) {
      if (am_pm == 0) {
        rbStartAm.setSelected(true);
      }
      else {
        rbStartPm.setSelected(true);
      }
      
    }
    else if (am_pm == 0) {
      rbStartPm.setSelected(true);
    }
    else {
      rbStartAm.setSelected(true);
    }
  }
  

  public boolean delete()
  {
    try
    {
      BookingInfo tableBookingBean = (BookingInfo)getBean();
      if (tableBookingBean == null) {
        return false;
      }
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), 
        Messages.getString("TableBookingForm.18"), Messages.getString("TableBookingForm.19"));
      if (option != 0) {
        return false;
      }
      BookingInfoDAO.getInstance().delete(tableBookingBean);
      clearFields();
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e);
    }
    return false;
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      BookingInfo table = (BookingInfo)getBean();
      BookingInfoDAO.getInstance().saveOrUpdate(table);
      updateView();
      return true;
    }
    catch (IllegalModelStateException localIllegalModelStateException) {}catch (StaleObjectStateException e) {
      BOMessageDialog.showError(this, Messages.getString("TableBookingForm.20"));
    }
    return false;
  }
  

  protected void updateView()
  {
    BookingInfo bookingInfo = (BookingInfo)getBean();
    if (bookingInfo == null) {
      return;
    }
    if (bookingInfo.getId() == null) {
      return;
    }
    edit();
    txtBookingCharge.setText(bookingInfo.getBookingCharge().toString());
    
    txtRemainingBalance.setText(bookingInfo.getRemainingBalance().toString());
    txtPaidAmount.setText(bookingInfo.getPaidAmount().toString());
    txtGuestCount.setText(bookingInfo.getGuestCount().toString());
    
    customer = bookingInfo.getCustomer();
    if (customer != null) {
      txtSelectedCustomer.setText(customer.toString());
    }
    selectedTableList = bookingInfo.getTables();
    doSortTables(selectedTableList);
    if (selectedTableList != null) {
      String strTableList = "";
      boolean isSetComma = false;
      for (ShopTable shopTable : selectedTableList) {
        if (isSetComma) {
          strTableList = strTableList + ", ";
        }
        strTableList = strTableList + shopTable.getId();
        isSetComma = true;
      }
      tfTableList.setText(strTableList);
    }
    if (bookingInfo.getFromDate() != null) {
      Calendar startCalendar = Calendar.getInstance();
      startCalendar.setTime(bookingInfo.getFromDate());
      
      tbStartDate.setDate(startCalendar.getTime());
      
      Integer hour = Integer.valueOf(startCalendar.get(10));
      if (hour.equals(Integer.valueOf(0))) {
        hour = Integer.valueOf(12);
      }
      
      cStartHour.setSelectedItem(hour);
      cStartMin.setSelectedItem(Integer.valueOf(startCalendar.get(12)));
      
      if (startCalendar.get(9) == 0) {
        rbStartAm.setSelected(true);
      }
      else {
        rbStartPm.setSelected(true);
      }
    }
    
    if (bookingInfo.getToDate() != null) {
      Calendar endCalendar = Calendar.getInstance();
      endCalendar.setTime(bookingInfo.getToDate());
      tbEndDate.setDate(endCalendar.getTime());
      
      Integer hour = Integer.valueOf(endCalendar.get(10));
      if (hour.equals(Integer.valueOf(0))) {
        hour = Integer.valueOf(12);
      }
      
      cEndHour.setSelectedItem(hour);
      cEndMin.setSelectedItem(Integer.valueOf(endCalendar.get(12)));
      
      if (endCalendar.get(9) == 0) {
        rbEndAm.setSelected(true);
      }
      else {
        rbEndPm.setSelected(true);
      }
    }
    if (customer == null) {
      txtSelectedCustomer.setText("");
    }
    else {
      txtSelectedCustomer.setText(customer.toString());
    }
  }
  
  protected boolean updateModel()
    throws IllegalModelStateException
  {
    BookingInfo objTableBooking = (BookingInfo)getBean();
    
    Date clStartDate = getStartDate();
    Date clEndDate = getEndDate();
    
    if (clStartDate.getTime() < getTodaysDateTime()) {
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), Messages.getString("TableBookingForm.31"));
      return false;
    }
    
    if (txtGuestCount.getText().equals("")) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please enter guest number.");
      return false;
    }
    
    objTableBooking.setTables(selectedTableList);
    
    objTableBooking.setPaymentStatus(txtBookingCharge.getText());
    
    if ((clStartDate == null) || (clEndDate == null)) {
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), Messages.getString("TableBookingForm.73"));
      return false;
    }
    objTableBooking.setFromDate(getStartDate());
    objTableBooking.setToDate(getEndDate());
    
    if (!txtPaidAmount.getText().isEmpty()) {
      objTableBooking.setPaidAmount(Double.valueOf(txtPaidAmount.getText()));
    }
    if (!txtRemainingBalance.getText().isEmpty()) {
      objTableBooking.setRemainingBalance(Double.valueOf(txtRemainingBalance.getText()));
    }
    if (!txtGuestCount.getText().isEmpty()) {
      int guest = Integer.parseInt(txtGuestCount.getText());
      if (guest == 0) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Guest number must be greater than 0.");
        return false;
      }
      int capacity = 0;
      if ((selectedTableList != null) && (selectedTableList.size() != 0)) {
        for (ShopTable shopTable : selectedTableList) {
          capacity += shopTable.getCapacity().intValue();
        }
        if (capacity < guest) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Table capacity must be greater than or equal to guest number.");
          return false;
        }
      }
      objTableBooking.setGuestCount(Integer.valueOf(guest));
    }
    if (!txtSelectedCustomer.getText().equals("")) {
      objTableBooking.setCustomer(getCustomer());
    }
    
    if (objTableBooking.getStatus() == null) {
      objTableBooking.setStatus("open");
    }
    return true;
  }
  
  private Date getStartDate() {
    if (tbStartDate.getDate() == null) {
      return null;
    }
    
    Calendar clStartDate = Calendar.getInstance();
    clStartDate.setTime(tbStartDate.getDate());
    
    Integer hour = (Integer)cStartHour.getSelectedItem();
    if (hour.intValue() == 12) {
      hour = Integer.valueOf(0);
    }
    
    clStartDate.set(10, hour.intValue());
    clStartDate.set(12, ((Integer)cStartMin.getSelectedItem()).intValue());
    
    if (rbStartAm.isSelected()) {
      clStartDate.set(9, 0);
    }
    else if (rbStartPm.isSelected()) {
      clStartDate.set(9, 1);
    }
    
    return clStartDate.getTime();
  }
  
  private Date getEndDate() {
    if (tbEndDate.getDate() == null) {
      return null;
    }
    
    Calendar clEndDate = Calendar.getInstance();
    clEndDate.setTime(tbEndDate.getDate());
    
    Integer hour = (Integer)cEndHour.getSelectedItem();
    if (hour.intValue() == 12) {
      hour = Integer.valueOf(0);
    }
    
    clEndDate.set(10, hour.intValue());
    clEndDate.set(12, ((Integer)cEndMin.getSelectedItem()).intValue());
    
    if (rbEndAm.isSelected()) {
      clEndDate.set(9, 0);
    }
    else if (rbEndPm.isSelected()) {
      clEndDate.set(9, 1);
    }
    
    return clEndDate.getTime();
  }
  
  private long getTodaysDateTime() {
    Calendar startDate = Calendar.getInstance();
    startDate.setLenient(false);
    startDate.setTime(new Date());
    return startDate.getTime().getTime();
  }
  
  public void cancel()
  {
    tables.setText(Messages.getString("TableBookingForm.10"));
  }
  
  public void clearFields()
  {
    txtBookingCharge.setText("0.0");
    txtGuestCount.setText("0");
    txtPaidAmount.setText("0.0");
    txtRemainingBalance.setText("0.0");
    rbStartPm.setSelected(true);
    rbEndPm.setSelected(true);
    tbStartDate.setDate(new Date());
    tbEndDate.setDate(new Date());
    cEndHour.setSelectedIndex(1);
    setStartHour();
    cStartMin.setSelectedIndex(0);
    cEndMin.setSelectedIndex(0);
    txtSelectedCustomer.setText("");
  }
  
  public String getDisplayText()
  {
    return null;
  }
  
  public void edit()
  {
    BookingInfo bookingInfo = (BookingInfo)getBean();
    if (bookingInfo == null) {
      return;
    }
    List<ShopTable> tables = bookingInfo.getTables();
    if ((tables == null) || (tables.size() == 0)) {}
  }
  




















  public TableBookingBrowser getBookingBrowser()
  {
    return bookingBrowser;
  }
  
  public void setBookingBrowser(TableBookingBrowser bookingBrowser) {
    this.bookingBrowser = bookingBrowser;
  }
  
  public void refreshBookingTable()
  {
    if (searchParameter == null) {
      bookingBrowser.refreshTable();
      return;
    }
    if (searchParameter.equals("todaysview")) {
      List todaysBookingList = BookingInfoDAO.getInstance().getTodaysBooking();
      bookingBrowser.setModels(todaysBookingList);
      return;
    }
    if (searchParameter.equals("open")) {
      List openBookingList = BookingInfoDAO.getInstance().getAllOpenBooking();
      bookingBrowser.setModels(openBookingList);
      return;
    }
    if (searchParameter.equals("search")) {
      List bookingList = BookingInfoDAO.getInstance().getAllBookingInfoByDate(stardDateForSearchBooking, endDateForSearchBooking);
      bookingBrowser.setModels(bookingList);
      return;
    }
  }
  
  private void addCustomerToBooking()
  {
    CustomerSelectorDialog dialog = CustomerSelectorFactory.createCustomerSelectorDialog(Application.getInstance().getCurrentOrderType());
    dialog.setCreateNewTicket(false);
    dialog.updateView(true);
    
    dialog.setSize(PosUIManager.getSize(1020, 650));
    dialog.open();
    
    if (dialog.isCanceled()) {
      return;
    }
    Customer customer = dialog.getSelectedCustomer();
    
    if (customer != null) {
      setCustomer(customer);
    }
  }
  
  public void setCustomer(Customer customer)
  {
    if (!customer.getFirstName().isEmpty()) {
      txtSelectedCustomer.setText(customer.getFirstName());
    }
    else if (!customer.getMobileNo().isEmpty()) {
      txtSelectedCustomer.setText(customer.getMobileNo());
    }
    else {
      txtSelectedCustomer.setText(customer.getLoyaltyNo());
    }
    this.customer = customer;
  }
  
  public Customer getCustomer() {
    return customer;
  }
  

  public void bookingStatus(String status)
  {
    if ((BookingInfo)getBean() == null) {
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), Messages.getString("TableBookingForm.36"));
      return;
    }
    try
    {
      String bookingStatus = null;
      int dialogResult = -1;
      
      if (status.equals("seat"))
      {
        dialogResult = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), 
          Messages.getString("TableBookingForm.37") + ((BookingInfo)getBean()).getId() + 
          Messages.getString("TableBookingForm.38"), Messages.getString("TableBookingForm.39"));
        
        bookingStatus = "seat";

      }
      else if (status.equals("cancel")) {
        dialogResult = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), 
          Messages.getString("TableBookingForm.42") + ((BookingInfo)getBean()).getId() + 
          Messages.getString("TableBookingForm.50"), Messages.getString("TableBookingForm.52"));
        
        bookingStatus = "cancel";
      }
      else if (status.equals("delay")) {
        dialogResult = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), 
          Messages.getString("TableBookingForm.45") + ((BookingInfo)getBean()).getId() + 
          Messages.getString("TableBookingForm.77"), Messages.getString("TableBookingForm.78"));
        bookingStatus = "delay";

      }
      else if (status.equals("no appear")) {
        dialogResult = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), 
          Messages.getString("TableBookingForm.45") + ((BookingInfo)getBean()).getId() + " will be mark as no appear. Continue ?", "Confirm");
        
        bookingStatus = "no appear";
      }
      else if (status.equals("close")) {
        dialogResult = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), 
          Messages.getString("TableBookingForm.45") + ((BookingInfo)getBean()).getId() + " will be mark as close. Continue ?", "Confirm");
        
        bookingStatus = "close";
      }
      
      if (dialogResult != 0) {
        return;
      }
      

      if ((selectedTableList == null) || (selectedTableList.size() == 0)) {
        return;
      }
      List<ShopTableStatus> statusList = new ArrayList();
      for (ShopTable shopTable : selectedTableList) {
        ShopTableStatus tableStatus = shopTable.getStatus();
        if (tableStatus != null)
        {
          statusList.add(tableStatus); }
      }
      BookingInfo bookingInfo = (BookingInfo)getBean();
      BookingInfoDAO.getInstance().setBookingStatus(bookingInfo, bookingStatus, statusList);
      
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), Messages.getString("TableBookingForm.64"));
      if (bookingBrowser != null) {
        bookingBrowser.doCancelEditing();
      }
      refreshBookingTable();
    }
    catch (Exception e2) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), Messages.getString("TableBookingForm.65"));
      logger.error(e2);
    }
  }
  
  private void doTableSelection()
  {
    if ((tbStartDate.getDate() == null) || (tbEndDate.getDate() == null)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("TableBookingForm.30"));
      return;
    }
    
    Date clStartDate = getStartDate();
    Date clEndDate = getEndDate();
    
    if (clStartDate.getTime() < getTodaysDateTime()) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("TableBookingForm.31"));
      return;
    }
    
    if (clStartDate.getTime() > clEndDate.getTime()) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("TableBookingForm.32"));
      return;
    }
    
    int guestCount = 0;
    if (txtGuestCount.getText().equals("")) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please enter guest number.");
      return;
    }
    guestCount = Integer.parseInt(txtGuestCount.getText());
    BookingTableSelectionDialog tableSelectionDialog = BookingTableSelectionDialog.getInstance();
    tableSelectionDialog.setStartDate(clStartDate);
    tableSelectionDialog.setEndDate(clEndDate);
    tableSelectionDialog.setGuestCount(guestCount);
    tableSelectionDialog.setSelectedTables(selectedTableList);
    tableSelectionDialog.clearAllFloorSelection(false);
    tableSelectionDialog.updateView();
    tableSelectionDialog.openFullScreen();
    
    if (tableSelectionDialog.isCanceled()) {
      return;
    }
    
    selectedTableList = tableSelectionDialog.getSelectedTables();
    doSortTables(selectedTableList);
    if (selectedTableList == null) {
      return;
    }
    
    String strTableList = "";
    boolean isSetComma = false;
    for (Iterator localIterator = selectedTableList.iterator(); localIterator.hasNext();) { shopTable = (ShopTable)localIterator.next();
      if (isSetComma) {
        strTableList = strTableList + ", ";
      }
      strTableList = strTableList + shopTable.getId();
      isSetComma = true; }
    ShopTable shopTable;
    tfTableList.setText(strTableList);
    BookingInfo bookingInfo = (BookingInfo)getBean();
    for (ShopTable table : selectedTableList) {
      table.setTableStatus(TableStatus.Booked);
    }
    bookingInfo.setTables(selectedTableList);
    BookingInfoDAO.getInstance().saveOrUpdate(bookingInfo);
  }
  
  private void doSortTables(List<ShopTable> tables) {
    if (tables == null) {
      return;
    }
    Collections.sort(tables, new Comparator()
    {
      public int compare(ShopTable lhs, ShopTable rhs) {
        return lhs.getId().compareTo(rhs.getId());
      }
    });
  }
}
