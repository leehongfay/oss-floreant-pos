package com.floreantpos.floorplan.action;

import com.floreantpos.actions.PosAction;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.floorplan.ui.configuration.FloorPlanConfigurationView;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import javax.swing.JTabbedPane;

public class FloorPlanConfigurationAction extends PosAction
{
  public FloorPlanConfigurationAction()
  {
    super("Configuration");
  }
  
  public void execute()
  {
    try {
      BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
      
      FloorPlanConfigurationView explorer = null;
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab("Configuration");
      if (index == -1) {
        explorer = new FloorPlanConfigurationView();
        tabbedPane.addTab("FloorPlan Configuration", explorer);
      }
      else {
        explorer = (FloorPlanConfigurationView)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(explorer);
    } catch (Exception e) {
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), e.getMessage());
    }
  }
}
