package com.floreantpos.floorplan.action;

import com.floreantpos.actions.PosAction;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.floorplan.ui.FloorConfigurationView;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import javax.swing.JTabbedPane;

public class ShowFloorsAction extends PosAction
{
  public ShowFloorsAction()
  {
    super("Floors");
  }
  
  public void execute()
  {
    try {
      BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
      
      FloorConfigurationView explorer = null;
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab("Floors");
      if (index == -1) {
        explorer = new FloorConfigurationView();
        explorer.initialize();
        tabbedPane.addTab("Floors", explorer);
      }
      else {
        explorer = (FloorConfigurationView)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(explorer);
    } catch (Exception e) {
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), e.getMessage());
    }
  }
}
