package com.floreantpos.floorplan.action;

import com.floreantpos.actions.PosAction;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.floorplan.ui.OnlineTableBookingBrowser;
import javax.swing.JTabbedPane;

public class OnlineTableBookingAction extends PosAction
{
  public OnlineTableBookingAction()
  {
    super("Online Table Booking");
  }
  
  public void execute()
  {
    BackOfficeWindow backOfficeWindow = com.floreantpos.util.POSUtil.getBackOfficeWindow();
    JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
    
    OnlineTableBookingBrowser explorer = null;
    int index = tabbedPane.indexOfTab("Online Table Booking");
    if (index == -1) {
      explorer = new OnlineTableBookingBrowser();
      tabbedPane.addTab("Online Table Booking", explorer);
    }
    else {
      explorer = (OnlineTableBookingBrowser)tabbedPane.getComponentAt(index);
    }
    tabbedPane.setSelectedComponent(explorer);
  }
}
