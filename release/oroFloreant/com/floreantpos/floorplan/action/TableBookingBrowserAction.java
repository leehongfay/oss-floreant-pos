package com.floreantpos.floorplan.action;

import com.floreantpos.actions.PosAction;
import com.floreantpos.floorplan.ui.TableBookingBrowser;
import javax.swing.JTabbedPane;

public class TableBookingBrowserAction extends PosAction
{
  public TableBookingBrowserAction()
  {
    super("Table Booking");
  }
  
  public void execute()
  {
    com.floreantpos.bo.ui.BackOfficeWindow backOfficeWindow = com.floreantpos.util.POSUtil.getBackOfficeWindow();
    JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
    
    TableBookingBrowser explorer = null;
    int index = tabbedPane.indexOfTab("Table Bookig");
    if (index == -1) {
      explorer = new TableBookingBrowser();
      tabbedPane.addTab("Table Booking", explorer);
    }
    else {
      explorer = (TableBookingBrowser)tabbedPane.getComponentAt(index);
    }
    tabbedPane.setSelectedComponent(explorer);
  }
}
