package com.floreantpos.floorplan.action;

import com.floreantpos.actions.PosAction;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.floorplan.ui.TableTypeBrowserPanel;
import javax.swing.JTabbedPane;

public class ShowTableTypesBrowserAction extends PosAction
{
  public ShowTableTypesBrowserAction()
  {
    super("Table Types");
  }
  
  public void execute()
  {
    BackOfficeWindow backOfficeWindow = com.floreantpos.util.POSUtil.getBackOfficeWindow();
    JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
    
    TableTypeBrowserPanel explorer = null;
    int index = tabbedPane.indexOfTab("Table Types");
    if (index == -1) {
      explorer = new TableTypeBrowserPanel();
      tabbedPane.addTab("Table Types", explorer);
    }
    else {
      explorer = (TableTypeBrowserPanel)tabbedPane.getComponentAt(index);
    }
    tabbedPane.setSelectedComponent(explorer);
  }
}
