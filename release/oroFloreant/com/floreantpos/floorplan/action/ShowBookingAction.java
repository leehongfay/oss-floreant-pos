package com.floreantpos.floorplan.action;

import com.floreantpos.actions.PosAction;
import com.floreantpos.floorplan.ui.TableBookingBrowser;
import com.floreantpos.model.UserPermission;
import com.floreantpos.ui.dialog.POSDialog;
import java.awt.Toolkit;


public class ShowBookingAction
  extends PosAction
{
  public ShowBookingAction()
  {
    super("BOOKING");
    
    setRequiredPermission(UserPermission.TABLE_BOOKING);
  }
  
  public ShowBookingAction(boolean showText, boolean showIcon) {
    if (showText) {
      putValue("Name", UserPermission.TABLE_BOOKING);
    }
    
    setRequiredPermission(UserPermission.TABLE_BOOKING);
  }
  
  public void execute()
  {
    POSDialog bookingDialog = new POSDialog();
    TableBookingBrowser explorer = new TableBookingBrowser();
    bookingDialog.add(explorer, "Center");
    bookingDialog.setSize(Toolkit.getDefaultToolkit().getScreenSize());
    bookingDialog.open();
  }
}
