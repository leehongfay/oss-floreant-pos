package com.floreantpos.customer;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.extension.OrderServiceFactory;
import com.floreantpos.main.Application;
import com.floreantpos.model.Customer;
import com.floreantpos.model.Store;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.QwertyKeyPad;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.forms.QuickCustomerForm;
import com.floreantpos.util.TicketAlreadyExistsException;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.JTableHeader;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;




















public class DefaultCustomerListView
  extends CustomerSelector
{
  private PosButton btnCreateNewCustomer;
  private CustomerTable customerTable;
  private POSTextField tfMobile;
  private POSTextField tfLoyaltyNo;
  private POSTextField tfName;
  private PosButton btnInfo;
  protected Customer selectedCustomer;
  private PosButton btnRemoveCustomer;
  private Ticket ticket;
  private PosButton btnCancel;
  private QwertyKeyPad qwertyKeyPad;
  private PosButton btnEditCustomer;
  private CustomerListTableModel customerListTableModel;
  private PosButton btnNext;
  private PosButton btnPrevious;
  private JLabel lblNumberOfItem;
  
  public DefaultCustomerListView()
  {
    initUI();
    doSearchCustomerPagingStart();
  }
  
  public DefaultCustomerListView(Ticket ticket) {
    this.ticket = ticket;
    initUI();
    loadCustomerFromTicket();
  }
  
  public void initUI() {
    setLayout(new MigLayout("fill", "[grow]", "[][grow][grow]"));
    
    JPanel searchPanel = new JPanel(new MigLayout());
    
    JLabel lblByPhone = new JLabel(Messages.getString("CustomerSelectionDialog.1"));
    JLabel lblByLoyality = new JLabel(Messages.getString("CustomerSelectionDialog.16"));
    JLabel lblByName = new JLabel(Messages.getString("CustomerSelectionDialog.19"));
    
    tfMobile = new POSTextField(16);
    tfLoyaltyNo = new POSTextField(16);
    tfName = new POSTextField(16);
    
    tfName.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        DefaultCustomerListView.this.doSearchCustomerPagingStart();
      }
    });
    tfLoyaltyNo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        DefaultCustomerListView.this.doSearchCustomerPagingStart();
      }
    });
    tfMobile.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        DefaultCustomerListView.this.doSearchCustomerPagingStart();
      }
      
    });
    PosButton btnSearch = new PosButton(Messages.getString("CustomerSelectionDialog.15"));
    btnSearch.setFocusable(false);
    btnSearch.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        DefaultCustomerListView.this.doSearchCustomerPagingStart();
      }
      
    });
    PosButton btnKeyboard = new PosButton(IconFactory.getIcon("/images/", "keyboard.png"));
    btnKeyboard.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        qwertyKeyPad.setCollapsed(!qwertyKeyPad.isCollapsed());
      }
      
    });
    searchPanel.add(lblByPhone, "growy");
    searchPanel.add(tfMobile, "growy");
    searchPanel.add(lblByLoyality, "growy");
    searchPanel.add(tfLoyaltyNo, "growy");
    searchPanel.add(lblByName, "growy");
    searchPanel.add(tfName, "growy");
    searchPanel.add(btnKeyboard, "growy,w " + PosUIManager.getSize(80) + "!,h " + PosUIManager.getSize(35) + "!");
    searchPanel.add(btnSearch, ",growy,h " + PosUIManager.getSize(35) + "!");
    
    add(searchPanel, "cell 0 1, growx");
    
    JPanel centerPanel = new JPanel(new BorderLayout());
    setBorder(new TitledBorder(null, POSConstants.SELECT_CUSTOMER.toUpperCase(), 2, 2, null, null));
    
    JPanel customerListPanel = new JPanel(new BorderLayout(0, 0));
    customerListPanel.setBorder(new EmptyBorder(5, 5, 0, 5));
    
    customerTable = new CustomerTable();
    customerListTableModel = new CustomerListTableModel();
    customerTable.setModel(customerListTableModel);
    customerTable.setFocusable(false);
    customerTable.setRowHeight(30);
    customerTable.getTableHeader().setPreferredSize(new Dimension(100, 35));
    customerTable.getSelectionModel().setSelectionMode(0);
    customerTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        selectedCustomer = customerTable.getSelectedCustomer();
        if (selectedCustomer == null)
        {


          btnInfo.setEnabled(false);
        }
      }
    });
    PosScrollPane scrollPane = new PosScrollPane();
    scrollPane.setFocusable(false);
    scrollPane.setViewportView(customerTable);
    
    customerListPanel.add(scrollPane, "Center");
    
    JPanel panel = new JPanel(new MigLayout("fill, hidemode 3", "[][center, grow][]", ""));
    btnInfo = new PosButton(Messages.getString("CustomerSelectionDialog.23"));
    btnInfo.setFocusable(false);
    panel.add(btnInfo, " skip 1, split 7");
    btnInfo.setEnabled(false);
    
    PosButton btnHistory = new PosButton(Messages.getString("CustomerSelectionDialog.24"));
    btnHistory.setEnabled(false);
    panel.add(btnHistory, "");
    
    btnCreateNewCustomer = new PosButton(Messages.getString("CustomerSelectionDialog.25"));
    btnCreateNewCustomer.setFocusable(false);
    panel.add(btnCreateNewCustomer, "");
    btnCreateNewCustomer.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doCreateNewCustomer();
      }
      
    });
    btnEditCustomer = new PosButton("EDIT");
    btnEditCustomer.setFocusable(false);
    panel.add(btnEditCustomer, "");
    btnEditCustomer.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doEditCustomer();
      }
      
    });
    btnRemoveCustomer = new PosButton(Messages.getString("CustomerSelectionDialog.26"));
    btnRemoveCustomer.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doRemoveCustomer();
        doRemoveCustomerFromTicket();
        CustomerListTableModel model = (CustomerListTableModel)customerTable.getModel();
        model.fireTableDataChanged();
        customerTable.revalidate();
        customerTable.repaint();
      }
    });
    panel.add(btnRemoveCustomer, "");
    
    PosButton btnSelect = new PosButton(Messages.getString("CustomerSelectionDialog.28"));
    btnSelect.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (isCreateNewTicket()) {
          DefaultCustomerListView.this.doCreateNewTicket();
        }
        else {
          DefaultCustomerListView.this.closeDialog(false);
        }
      }
    });
    panel.add(btnSelect, "");
    
    btnCancel = new PosButton(Messages.getString("CustomerSelectionDialog.29"));
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        DefaultCustomerListView.this.closeDialog(true);
      }
    });
    panel.add(btnCancel, "");
    btnNext = new PosButton("NEXT");
    btnNext.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (customerListTableModel.hasNext()) {
          customerListTableModel.setCurrentRowIndex(customerListTableModel.getNextRowIndex());
          doSearchCustomer();
        }
        
      }
    });
    btnPrevious = new PosButton("PREVIOUS");
    btnPrevious.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (customerListTableModel.hasPrevious()) {
          customerListTableModel.setCurrentRowIndex(customerListTableModel.getPreviousRowIndex());
          doSearchCustomer();
        }
        updateButtonStatus();
      }
      
    });
    lblNumberOfItem = new JLabel();
    panel.add(new JSeparator());
    panel.add(lblNumberOfItem);
    panel.add(btnPrevious);
    panel.add(btnNext);
    customerListPanel.add(panel, "South");
    centerPanel.add(customerListPanel, "Center");
    
    add(centerPanel, "cell 0 2,grow");
    
    qwertyKeyPad = new QwertyKeyPad();
    qwertyKeyPad.setCollapsed(false);
    add(qwertyKeyPad, "cell 0 3,grow");
    updateButtonStatus();
  }
  
  public void updateButtonStatus() {
    btnNext.setEnabled(customerListTableModel.hasNext());
    btnPrevious.setEnabled(customerListTableModel.hasPrevious());
  }
  
  private void loadCustomerFromTicket() {
    String customerIdString = ticket.getProperty("CUSTOMER_ID");
    if (StringUtils.isNotEmpty(customerIdString)) {
      Customer customer = CustomerDAO.getInstance().get(customerIdString);
      
      List<Customer> list = new ArrayList();
      list.add(customer);
      customerTable.setModel(new CustomerListTableModel(list));
    }
  }
  
  private void closeDialog(boolean canceled) {
    Window windowAncestor = SwingUtilities.getWindowAncestor(this);
    if ((windowAncestor instanceof POSDialog)) {
      ((POSDialog)windowAncestor).setCanceled(canceled);
      windowAncestor.dispose();
    }
  }
  
  protected void doSetCustomer(Customer customer) {
    if (ticket != null) {
      ticket.setCustomer(customer);
      TicketDAO.getInstance().saveOrUpdate(ticket);
    }
  }
  
  private void doCreateNewTicket() {
    try {
      Customer selectedCustomer = getSelectedCustomer();
      
      if (selectedCustomer == null) {
        return;
      }
      closeDialog(false);
      OrderServiceFactory.getOrderService().createNewTicket(getOrderType(), null, selectedCustomer);
    }
    catch (TicketAlreadyExistsException e) {
      POSMessageDialog.showError(this, e.getMessage());
    }
  }
  
  protected void doRemoveCustomerFromTicket()
  {
    if (ticket == null) {
      return;
    }
    int option = POSMessageDialog.showYesNoQuestionDialog(this, Messages.getString("CustomerSelectionDialog.2"), 
      Messages.getString("CustomerSelectionDialog.32"));
    if (option != 0) {
      return;
    }
    ticket.removeCustomer();
    TicketDAO.getInstance().saveOrUpdate(ticket);
  }
  


  private void doSearchCustomerPagingStart()
  {
    customerListTableModel.setCurrentRowIndex(0);
    doSearchCustomer();
  }
  
  protected void doSearchCustomer() {
    try {
      setCursor(Cursor.getPredefinedCursor(3));
      qwertyKeyPad.setCollapsed(true);
      String mobile = tfMobile.getText();
      String name = tfName.getText();
      String loyalty = tfLoyaltyNo.getText();
      
      if ((StringUtils.isEmpty(mobile)) && (StringUtils.isEmpty(loyalty)) && (StringUtils.isEmpty(name))) {
        customerListTableModel.setNumRows(CustomerDAO.getInstance().getNumberOfCustomers());
        CustomerDAO.getInstance().loadCustomers(customerListTableModel);
      }
      else {
        CustomerDAO.getInstance().findBy(mobile, loyalty, name, customerListTableModel);
      }
      
      if (customerListTableModel.getRows().size() == 0) {
        customerTable.getSelectionModel().addSelectionInterval(0, 0);
      }
      
      int startNumber = customerListTableModel.getCurrentRowIndex() + 1;
      int endNumber = customerListTableModel.getNextRowIndex();
      int totalNumber = customerListTableModel.getNumRows();
      if (endNumber > totalNumber) {
        endNumber = totalNumber;
      }
      lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
      
      customerListTableModel.fireTableDataChanged();
      customerTable.repaint();
      updateButtonStatus();
    } catch (Exception e) {
      PosLog.error(DefaultCustomerListView.class, e);
      e.printStackTrace();
    } finally {
      setCursor(Cursor.getDefaultCursor());
    }
  }
  
  protected void doCreateNewCustomer() {
    boolean setKeyPad = true;
    
    QuickCustomerForm form = new QuickCustomerForm(setKeyPad);
    


    form.enableCustomerFields(true);
    BeanEditorDialog dialog = new BeanEditorDialog(form);
    dialog.setResizable(false);
    dialog.open();
    
    if (!dialog.isCanceled()) {
      selectedCustomer = ((Customer)form.getBean());
      
      CustomerListTableModel model = (CustomerListTableModel)customerTable.getModel();
      model.addItem(selectedCustomer);
    }
  }
  
  protected void doEditCustomer() {
    int index = customerTable.getSelectedRow();
    
    if (index < 0)
      return;
    CustomerListTableModel dataModel = (CustomerListTableModel)customerTable.getModel();
    Customer customer = (Customer)dataModel.getRowData(index);
    if (customer == null) {
      return;
    }
    
    QuickCustomerForm form = new QuickCustomerForm(customer);
    


    form.enableCustomerFields(true);
    BeanEditorDialog dialog = new BeanEditorDialog(form);
    dialog.setResizable(false);
    dialog.open();
    
    if (!dialog.isCanceled()) {
      dataModel.fireTableRowsUpdated(index, index);
    }
  }
  
  protected void doRemoveCustomer() {
    int option = POSMessageDialog.showYesNoQuestionDialog(this, "Do you want to remove customer?", "Remove Customer");
    if (option != 0) {
      return;
    }
    
    Customer selectedCustomer = getSelectedCustomer();
    
    CustomerListTableModel model = (CustomerListTableModel)customerTable.getModel();
    model.deleteItem(selectedCustomer);
  }
  

  public String getName()
  {
    return "C";
  }
  
  public Customer getSelectedCustomer() {
    return selectedCustomer;
  }
  
  public void setRemoveButtonEnable(boolean enable) {
    btnRemoveCustomer.setEnabled(enable);
  }
  
  public void updateView(boolean update)
  {
    Store store = Application.getInstance().getStore();
    btnCreateNewCustomer.setVisible(store.hasCreateMemberPermission());
    btnCancel.setVisible(update);
    loadCustomer();
  }
  
  private void loadCustomer() {
    Customer customer = getCustomer();
    if (customer != null) {
      doSearchCustomerPagingStart();
    }
    else {
      doSearchCustomerPagingStart();
    }
  }
  
  public void redererCustomers() {}
}
