package com.floreantpos.customer;

import com.floreantpos.extension.OrderServiceExtension;

public class CustomerSelectorFactory
{
  private static CustomerSelector customerSelector;
  
  public CustomerSelectorFactory() {}
  
  public static CustomerSelectorDialog createCustomerSelectorDialog(com.floreantpos.model.OrderType orderType)
  {
    OrderServiceExtension orderServicePlugin = (OrderServiceExtension)com.floreantpos.extension.ExtensionManager.getPlugin(OrderServiceExtension.class);
    if (customerSelector == null) {
      if (orderServicePlugin == null) {
        customerSelector = new DefaultCustomerListView();
      }
      else {
        customerSelector = orderServicePlugin.createNewCustomerSelector();
      }
    }
    customerSelector.setOrderType(orderType);
    customerSelector.redererCustomers();
    customerSelector.setVisibleOnlySelectionButtons(false);
    
    return new CustomerSelectorDialog(customerSelector);
  }
  
  public static CustomerSelectorDialog createCustomerSelectorDialog(javax.swing.JFrame frame, com.floreantpos.model.OrderType orderType) {
    OrderServiceExtension orderServicePlugin = (OrderServiceExtension)com.floreantpos.extension.ExtensionManager.getPlugin(OrderServiceExtension.class);
    if (customerSelector == null) {
      if (orderServicePlugin == null) {
        customerSelector = new DefaultCustomerListView();
      }
      else {
        customerSelector = orderServicePlugin.createNewCustomerSelector();
      }
    }
    customerSelector.setOrderType(orderType);
    customerSelector.redererCustomers();
    customerSelector.setVisibleOnlySelectionButtons(false);
    
    return new CustomerSelectorDialog(frame, customerSelector);
  }
}
