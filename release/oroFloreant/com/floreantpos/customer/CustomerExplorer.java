package com.floreantpos.customer;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.Customer;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.forms.CustomerForm;
import com.floreantpos.util.PosGuiUtil;
import java.awt.BorderLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import net.miginfocom.swing.MigLayout;




















public class CustomerExplorer
  extends TransparentPanel
{
  private JTable table;
  private BeanTableModel<Customer> tableModel;
  private JButton btnBack;
  private JButton btnForward;
  private JLabel lblNumberOfItem;
  private JTextField tfSearch;
  
  public CustomerExplorer()
  {
    initComponents();
    updateCustomer();
  }
  
  private void updateCustomer() {
    String searchString = tfSearch.getText();
    CustomerDAO.getInstance().findByPhoneOrName(searchString, tableModel);
    
    int startNumber = tableModel.getCurrentRowIndex() + 1;
    int endNumber = tableModel.getNextRowIndex();
    int totalNumber = tableModel.getNumRows();
    if (endNumber > totalNumber) {
      endNumber = totalNumber;
    }
    lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
    btnBack.setEnabled(tableModel.hasPrevious());
    btnForward.setEnabled(tableModel.hasNext());
  }
  
  private void initComponents() {
    tableModel = new BeanTableModel(Customer.class, 50);
    tableModel.addColumn("NAME", "name");
    tableModel.addColumn("PHONE", "mobileNo");
    tableModel.addColumn("EMAIL", "email");
    tableModel.addColumn("ADDRESS", "address");
    tableModel.addColumn("CITY", "city");
    tableModel.addColumn("COUNTRY", "country");
    tableModel.addColumn("BALANCE", "balance");
    tableModel.addColumn("CREDIT LIMIT", "creditLimit");
    tableModel.addColumn("LOYALTY", "loyaltyNo");
    tableModel.addColumn("VIP", "vip");
    tableModel.addColumn("TAX EXEMPT", "taxExempt");
    
    table = new JTable(tableModel);
    
    table.setRowHeight(PosUIManager.getSize(30));
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    PosGuiUtil.setColumnWidth(table, 0, 150);
    PosGuiUtil.setColumnWidth(table, 1, 150);
    PosGuiUtil.setColumnWidth(table, 2, 120);
    PosGuiUtil.setColumnWidth(table, 3, 150);
    PosGuiUtil.setColumnWidth(table, 4, 100);
    PosGuiUtil.setColumnWidth(table, 5, 100);
    PosGuiUtil.setColumnWidth(table, 6, 35);
    PosGuiUtil.setColumnWidth(table, 7, 55);
    PosGuiUtil.setColumnWidth(table, 8, 30);
    PosGuiUtil.setColumnWidth(table, 9, 20);
    PosGuiUtil.setColumnWidth(table, 10, 50);
    
    setLayout(new BorderLayout(5, 0));
    
    JPanel searchPanel = new JPanel(new MigLayout("fillx, ins 10"));
    JLabel lblSearchTxt = new JLabel("Name: ");
    tfSearch = new JTextField(20);
    tfSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        CustomerExplorer.this.updateCustomer();
      }
    });
    JButton btnSearch = new JButton("Search");
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        CustomerExplorer.this.updateCustomer();
      }
      
    });
    JButton btnClear = new JButton("Clear");
    btnClear.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tfSearch.setText("");
        CustomerExplorer.this.updateCustomer();
      }
    });
    searchPanel.add(lblSearchTxt, "split 4");
    searchPanel.add(tfSearch, "");
    searchPanel.add(btnSearch);
    searchPanel.add(btnClear);
    add(searchPanel, "North");
    add(new JScrollPane(table), "Center");
    
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        CustomerExplorer.this.doAddCustomer();
      }
      

    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        CustomerExplorer.this.doEditCustomer();
      }
      
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        CustomerExplorer.this.doDeleteCustomer();
      }
      

    });
    btnBack = new JButton("<<< Previous");
    btnForward = new JButton("Next >>>");
    lblNumberOfItem = new JLabel();
    
    btnBack.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        tableModel.setCurrentRowIndex(tableModel.getPreviousRowIndex());
        CustomerExplorer.this.updateCustomer();
      }
      
    });
    btnForward.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        tableModel.setCurrentRowIndex(tableModel.getNextRowIndex());
        CustomerExplorer.this.updateCustomer();
      }
      
    });
    TransparentPanel panel = new TransparentPanel(new MigLayout("fillx"));
    panel.add(addButton, "right, split 3");
    panel.add(editButton);
    panel.add(deleteButton);
    
    panel.add(lblNumberOfItem, "right, split 3");
    panel.add(btnBack);
    panel.add(btnForward);
    
    add(panel, "South");
  }
  
  private void doAddCustomer() {
    try {
      boolean setKeyPad = true;
      CustomerForm editor = new CustomerForm(setKeyPad);
      editor.enableCustomerFields(true);
      
      Window windowAncestor = SwingUtilities.getWindowAncestor(this);
      
      BeanEditorDialog dialog = new BeanEditorDialog(windowAncestor, editor);
      dialog.setPreferredSize(PosUIManager.getSize(960, 700));
      dialog.open();
      if (dialog.isCanceled())
        return;
      Customer customer = (Customer)editor.getBean();
      tableModel.addRow(customer);
      updateCustomer();
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doEditCustomer() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      Customer customer = (Customer)tableModel.getRow(index);
      
      boolean setKeyPad = true;
      CustomerForm editor = new CustomerForm(setKeyPad);
      editor.enableCustomerFields(true);
      
      editor.setBean(customer);
      
      Window windowAncestor = SwingUtilities.getWindowAncestor(this);
      
      BeanEditorDialog dialog = new BeanEditorDialog(windowAncestor, editor);
      dialog.setPreferredSize(PosUIManager.getSize(960, 700));
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doDeleteCustomer() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      if (ConfirmDeleteDialog.showMessage(this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0) {
        Customer customer = (Customer)tableModel.getRow(index);
        CustomerDAO dao = new CustomerDAO();
        dao.delete(customer);
        tableModel.removeRow(customer);
        updateCustomer();
      }
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
}
