package com.floreantpos.customer;

import com.floreantpos.Messages;
import com.floreantpos.config.AppProperties;
import com.floreantpos.main.Application;
import com.floreantpos.model.Customer;
import com.floreantpos.model.Ticket;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.util.POSUtil;
import java.awt.Container;
import java.awt.HeadlessException;
import javax.swing.JFrame;

















public class CustomerSelectorDialog
  extends POSDialog
{
  private final CustomerSelector customerSelector;
  
  public CustomerSelectorDialog(CustomerSelector customerSelector)
    throws HeadlessException
  {
    super(POSUtil.getBackOfficeWindow(), true);
    this.customerSelector = customerSelector;
    
    init(POSUtil.getBackOfficeWindow());
  }
  
  public CustomerSelectorDialog(JFrame parent, CustomerSelector customerSelector) throws HeadlessException {
    super(parent, true);
    this.customerSelector = customerSelector;
    
    init(parent);
  }
  
  private void init(JFrame parent) {
    setTitle(AppProperties.getAppName());
    TitlePanel titlePane = new TitlePanel();
    titlePane.setTitle(Messages.getString("CustomerSelectorDialog.0"));
    
    getContentPane().add(titlePane, "North");
    getContentPane().add(customerSelector);
    if (parent == null) {
      parent = Application.getPosWindow();
    }
    
    setSize(parent.getSize());
    setLocationRelativeTo(parent);
  }
  
  public void setCreateNewTicket(boolean createNewTicket) {
    customerSelector.setCreateNewTicket(createNewTicket);
  }
  
  public void updateView(boolean update) {
    customerSelector.updateView(update);
  }
  
  public Customer getSelectedCustomer() {
    return customerSelector.getSelectedCustomer();
  }
  
  public void setTicket(Ticket thisTicket) {
    customerSelector.setTicket(thisTicket);
  }
  
  public void setCustomer(Customer customer) {
    customerSelector.setCustomer(customer);
  }
  
  public void setCallerId(String callerId) {
    customerSelector.setCallerId(callerId);
  }
  
  public void setVisibleOnlySelectionButtons(boolean b) {
    customerSelector.setVisibleOnlySelectionButtons(b);
    customerSelector.updateView(true);
  }
}
