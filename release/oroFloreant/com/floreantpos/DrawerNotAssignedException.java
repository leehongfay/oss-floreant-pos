package com.floreantpos;










public class DrawerNotAssignedException
  extends RuntimeException
{
  public DrawerNotAssignedException() {}
  








  public DrawerNotAssignedException(String arg0)
  {
    super(arg0);
  }
  
  public DrawerNotAssignedException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }
  
  public DrawerNotAssignedException(Throwable arg0) {
    super(arg0);
  }
}
