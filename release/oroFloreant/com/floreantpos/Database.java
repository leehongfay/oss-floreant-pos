package com.floreantpos;

import org.apache.commons.lang.StringUtils;

















public enum Database
{
  DERBY_SINGLE("Derby Single", "jdbc:derby:database/derby-single/posdb", "jdbc:derby:database/derby-single/posdb;create=true", "", "org.apache.derby.jdbc.EmbeddedDriver", "org.hibernate.dialect.DerbyDialect"), 
  DERBY_SERVER("Derby Server", "jdbc:derby://<host>:<port>/<db>", "jdbc:derby://<host>:<port>/<db>;create=true", "51527", "org.apache.derby.jdbc.ClientDriver", "org.hibernate.dialect.DerbyDialect"), 
  MYSQL("MySQL", "jdbc:mysql://<host>:<port>/<db>?characterEncoding=UTF-8", "jdbc:mysql://<host>:<port>/<db>?characterEncoding=UTF-8", "3306", "com.mysql.jdbc.Driver", "org.hibernate.dialect.MySQLDialect"), 
  POSTGRES("POSTGRES", "jdbc:postgresql://<host>:<port>/<db>", "jdbc:postgresql://<host>:<port>/<db>", "5432", "org.postgresql.Driver", "org.hibernate.dialect.PostgreSQLDialect"), 
  MS_SQL("MSSQL Server", "jdbc:jtds:sqlserver://<host>:<port>/<db>", "jdbc:jtds:sqlserver://<host>:<port>/<db>", "1433", "net.sourceforge.jtds.jdbc.Driver", "org.hibernate.dialect.SQLServerDialect");
  

  private String providerName;
  private String jdbcUrlFormat;
  private String jdbcUrlFormatToCreateDb;
  private String defaultPort;
  private String driverClass;
  private String hibernateDialect;
  
  private Database(String providerName, String jdbcURL, String jdbcURL2CreateDb, String defaultPort, String driverClass, String hibernateDialect)
  {
    this.providerName = providerName;
    jdbcUrlFormat = jdbcURL;
    jdbcUrlFormatToCreateDb = jdbcURL2CreateDb;
    this.defaultPort = defaultPort;
    this.driverClass = driverClass;
    this.hibernateDialect = hibernateDialect;
  }
  
  public String getConnectString(String host, String port, String databaseName) {
    String connectionURL = jdbcUrlFormat.replace("<host>", host);
    
    if (StringUtils.isEmpty(port)) {
      port = defaultPort;
    }
    
    connectionURL = connectionURL.replace("<port>", port);
    connectionURL = connectionURL.replace("<db>", databaseName);
    
    return connectionURL;
  }
  
  public String getCreateDbConnectString(String host, String port, String databaseName) {
    String connectionURL = jdbcUrlFormatToCreateDb.replace("<host>", host);
    
    if (StringUtils.isEmpty(port)) {
      port = defaultPort;
    }
    
    connectionURL = connectionURL.replace("<port>", port);
    connectionURL = connectionURL.replace("<db>", databaseName);
    
    return connectionURL;
  }
  
  public String getProviderName() {
    return providerName;
  }
  
  public String getJdbcUrlFormat() {
    return jdbcUrlFormat;
  }
  
  public String getDefaultPort() {
    return defaultPort;
  }
  
  public String toString()
  {
    return providerName;
  }
  
  public String getHibernateConnectionDriverClass() {
    return driverClass;
  }
  
  public String getHibernateDialect() {
    return hibernateDialect;
  }
  





  public static Database getByProviderName(String providerName)
  {
    Database[] databases = values();
    for (Database database : databases) {
      if (providerName.equals(providerName)) {
        return database;
      }
    }
    
    return null;
  }
}
