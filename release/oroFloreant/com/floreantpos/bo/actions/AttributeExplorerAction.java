package com.floreantpos.bo.actions;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.bo.ui.explorer.AttributeExplorer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;

public class AttributeExplorerAction
  extends AbstractAction
{
  public AttributeExplorerAction()
  {
    super("Attributes");
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      AttributeExplorer browser = null;
      
      int index = tabbedPane.indexOfTab("Attributes");
      if (index == -1) {
        browser = new AttributeExplorer();
        tabbedPane.addTab("Attributes", browser);
      }
      else {
        browser = (AttributeExplorer)tabbedPane.getComponentAt(index);
      }
      
      tabbedPane.setSelectedComponent(browser);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
