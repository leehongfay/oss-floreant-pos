package com.floreantpos.bo.actions;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.bo.ui.explorer.InventoryUnitGroupExplorer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

















public class InventoryUnitGroupAction
  extends AbstractAction
{
  public InventoryUnitGroupAction()
  {
    super("Unit Group");
  }
  
  public InventoryUnitGroupAction(String name) {
    super(name);
  }
  
  public InventoryUnitGroupAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    
    try
    {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab("Inventory Unit Group Explorer");
      InventoryUnitGroupExplorer inventoryUnitGroupExplorer; if (index == -1) {
        InventoryUnitGroupExplorer inventoryUnitGroupExplorer = new InventoryUnitGroupExplorer();
        tabbedPane.addTab("Inventory Unit Group Explorer", inventoryUnitGroupExplorer);
      }
      else {
        inventoryUnitGroupExplorer = (InventoryUnitGroupExplorer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(inventoryUnitGroupExplorer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
