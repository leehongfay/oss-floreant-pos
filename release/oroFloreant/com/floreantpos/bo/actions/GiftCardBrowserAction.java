package com.floreantpos.bo.actions;

import com.floreantpos.Messages;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.bo.ui.explorer.GiftCardExplorer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

public class GiftCardBrowserAction extends AbstractAction
{
  public GiftCardBrowserAction()
  {
    super(Messages.getString("GiftCardBrowserAction.0"));
  }
  
  public GiftCardBrowserAction(String name) {
    super(name);
  }
  
  public GiftCardBrowserAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    
    try
    {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab(Messages.getString("GiftCardBrowserAction.1"));
      GiftCardExplorer item; if (index == -1) {
        GiftCardExplorer item = new GiftCardExplorer();
        tabbedPane.addTab(Messages.getString("GiftCardBrowserAction.1"), item);
      }
      else {
        item = (GiftCardExplorer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(item);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
