package com.floreantpos.bo.actions;

import com.floreantpos.Messages;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.bo.ui.explorer.PizzaModifierExplorer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

















public class PizzaModifierExplorerAction
  extends AbstractAction
{
  public PizzaModifierExplorerAction()
  {
    super(Messages.getString("PizzaModifierExplorerAction.0"));
  }
  
  public PizzaModifierExplorerAction(String name) {
    super(name);
  }
  
  public PizzaModifierExplorerAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    
    try
    {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab(Messages.getString("PizzaModifierExplorerAction.1"));
      PizzaModifierExplorer modifier; if (index == -1) {
        PizzaModifierExplorer modifier = new PizzaModifierExplorer();
        tabbedPane.addTab(Messages.getString("PizzaModifierExplorerAction.1"), modifier);
      }
      else {
        modifier = (PizzaModifierExplorer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(modifier);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
