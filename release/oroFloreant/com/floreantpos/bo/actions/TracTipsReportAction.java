package com.floreantpos.bo.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.report.TracTipsReportView;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;
















public class TracTipsReportAction
  extends AbstractAction
{
  public TracTipsReportAction()
  {
    super(POSConstants.TRACK_TIPS_REPORT);
  }
  
  public TracTipsReportAction(String name) {
    super(name);
  }
  
  public TracTipsReportAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      
      TracTipsReportView reportView = null;
      int index = tabbedPane.indexOfTab(POSConstants.TRACK_TIPS_REPORT);
      if (index == -1) {
        reportView = new TracTipsReportView();
        tabbedPane.addTab(POSConstants.TRACK_TIPS_REPORT, reportView);
      }
      else {
        reportView = (TracTipsReportView)tabbedPane.getComponentAt(index);
      }
      
      tabbedPane.setSelectedComponent(reportView);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
