package com.floreantpos.bo.actions;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.report.CustomerPaymentReportView;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

















public class CustomerPaymentReportAction
  extends AbstractAction
{
  public CustomerPaymentReportAction()
  {
    super("Customer Payment Report");
  }
  
  public CustomerPaymentReportAction(String name) {
    super(name);
  }
  
  public CustomerPaymentReportAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      
      CustomerPaymentReportView reportView = null;
      int index = tabbedPane.indexOfTab("Customer Payment Report");
      if (index == -1) {
        reportView = new CustomerPaymentReportView();
        tabbedPane.addTab("Customer Payment Report", reportView);
      }
      else {
        reportView = (CustomerPaymentReportView)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(reportView);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
