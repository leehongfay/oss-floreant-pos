package com.floreantpos.bo.actions;

import com.floreantpos.Messages;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.GiftCardGeneratorView;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;




public class GiftCardGenerateAction
  extends AbstractAction
{
  public GiftCardGenerateAction()
  {
    super("Generate");
  }
  
  public GiftCardGenerateAction(String name) {
    super(name);
  }
  
  public GiftCardGenerateAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    try {
      GiftCardGeneratorView dialog = new GiftCardGeneratorView();
      dialog.setTitle(Messages.getString("GiftCardGenerateAction.1"));
      dialog.setDefaultCloseOperation(2);
      dialog.setSize(PosUIManager.getSize(600, 400));
      dialog.setLocationRelativeTo(POSUtil.getBackOfficeWindow());
      dialog.setVisible(true);
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    }
  }
}
