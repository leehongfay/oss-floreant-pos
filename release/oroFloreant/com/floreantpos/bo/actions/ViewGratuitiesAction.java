package com.floreantpos.bo.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.GratuityDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;

















public class ViewGratuitiesAction
  extends AbstractAction
{
  public ViewGratuitiesAction()
  {
    super(POSConstants.GRATUITY_ADMINISTRATION);
  }
  
  public ViewGratuitiesAction(String name) {
    super(name);
  }
  
  public ViewGratuitiesAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    try {
      GratuityDialog dialog = new GratuityDialog(POSUtil.getBackOfficeWindow(), Application.getCurrentUser());
      dialog.setCaption("SERVER GRATUITY");
      dialog.setOkButtonText("PAY");
      dialog.setSize(PosUIManager.getSize(650, 550));
      dialog.open();
      if (dialog.isCanceled())
        return;
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e);
    }
  }
}
