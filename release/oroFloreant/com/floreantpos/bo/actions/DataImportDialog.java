package com.floreantpos.bo.actions;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.ModifierGroup;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.BeanTableModel.EditMode;
import com.floreantpos.swing.PosTable;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.ProgressObserver;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.datamigrate.Elements;
import java.awt.BorderLayout;
import java.awt.Component;
import java.io.File;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.InputMap;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;

public class DataImportDialog extends OkCancelOptionDialog implements ProgressObserver
{
  private Elements elements;
  private JTabbedPane tabPane;
  private File file;
  private JLabel lblProgress;
  private JPanel progressPanel;
  
  public DataImportDialog(File file)
  {
    super(POSUtil.getBackOfficeWindow(), true);
    this.file = file;
    setTitle("Import");
    setOkButtonText("Import");
    setCaption("Review Import");
    JPanel contentPanel = getContentPanel();
    contentPanel.setLayout(new BorderLayout());
    tabPane = new JTabbedPane();
    contentPanel.add(tabPane);
    
    progressPanel = new JPanel(new MigLayout("fillx"));
    lblProgress = new JLabel("Importing..");
    progressPanel.add(lblProgress, "grow,span,wrap");
    progressPanel.setVisible(false);
    
    contentPanel.add(progressPanel, "South");
  }
  
  protected void showImportedItems() {
    try {
      progressPanel.setVisible(true);
      progress(0, "Loading items.. Please wait..");
      
      elements = DataImportAction.importMenuItemsFromFile(file);
      
      progress(100, "Loading complete..");
      progressPanel.setVisible(false);
      
      String[] menuItemsColumn = { "name", "translatedName", "price", "availableUnit", "unitOnHand", "visible", "taxGroup", "sortOrder", "buttonColor", "textColor", "image" };
      Integer[] menuItemColumnsWidth = { Integer.valueOf(200), Integer.valueOf(180), Integer.valueOf(90), Integer.valueOf(70) };
      Integer[] editableColumns = { Integer.valueOf(0), Integer.valueOf(1), Integer.valueOf(2), Integer.valueOf(3), Integer.valueOf(4), Integer.valueOf(7) };
      tabPane.addTab(POSConstants.MENU_ITEMS, new ElementTab(MenuItem.class, elements.getMenuItems(), menuItemsColumn, menuItemColumnsWidth, editableColumns));
      
      editableColumns = new Integer[] { Integer.valueOf(0), Integer.valueOf(1), Integer.valueOf(4) };
      String[] menuGroupColumns = { "name", "translatedName", "visible", "parent", "sortOrder", "buttonColor", "textColor" };
      tabPane.addTab(POSConstants.MENU_GROUPS, new ElementTab(MenuGroup.class, elements.getMenuGroups(), menuGroupColumns, null, editableColumns));
      
      editableColumns = new Integer[] { Integer.valueOf(0), Integer.valueOf(1), Integer.valueOf(4) };
      String[] categoryColumns = { "name", "translatedName", "beverage", "visible", "sortOrder", "buttonColor", "textColor" };
      tabPane.addTab(POSConstants.MENU_CATEGORIES, new ElementTab(MenuCategory.class, elements.getMenuCategories(), categoryColumns, null, editableColumns));
      
      editableColumns = new Integer[] { Integer.valueOf(0), Integer.valueOf(1) };
      String[] menuModifierGroupsColumns = { "name", "translatedName", "modifiers" };
      tabPane.addTab(POSConstants.MODIFIER_GROUPS, new ElementTab(ModifierGroup.class, elements.getModifierGroups(), menuModifierGroupsColumns, null, editableColumns));
      
      editableColumns = new Integer[] { Integer.valueOf(0), Integer.valueOf(1), Integer.valueOf(2) };
      String[] menuModifiers = { "name", "translatedName", "price", "sortOrder", "buttonColor", "textColor" };
      tabPane.addTab(POSConstants.MENU_MODIFIERS, new ElementTab(MenuModifier.class, elements.getMenuModifiers(), menuModifiers, null, editableColumns));
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e);
    }
  }
  
  public void doOk()
  {
    progressPanel.setVisible(true);
    progress(0, "Importing..");
    new Thread() {
      public void run() {
        doImport();
      }
    }.start();
  }
  
  protected void doImport()
  {
    try {
      getButtonPanel().getComponent(0).setEnabled(false);
      DataImportAction.saveImportedMenuItems(elements, this);
      progress(100, "Import complete");
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), Messages.getString("DataImportAction.1"));
      setCanceled(false);
      dispose();
    } catch (Exception e) {
      BOMessageDialog.showError(e);
      progress(0, "Import failed");
    } finally {
      getButtonPanel().getComponent(0).setEnabled(true);
    }
  }
  
  public void setVisible(boolean b)
  {
    if (b)
    {



      new Thread()
      {
        public void run()
        {
          showImportedItems();
        }
      }.start();
    }
    super.setVisible(b);
  }
  
  private class ElementTab<E> extends JPanel {
    private PosTable jtable;
    private BeanTableModel<E> tableModel;
    
    public ElementTab(Class className, List dataList, String[] columns) {
      this(className, dataList, columns, null);
    }
    
    public ElementTab(Class className, List dataList, String[] columns, Integer[] colWidths) {
      this(className, dataList, columns, colWidths, null);
    }
    
    public ElementTab(Class className, List dataList, String[] columns, Integer[] colWidths, Integer[] editableColumns) {
      tableModel = new BeanTableModel(className, 20);
      int i; if ((columns != null) && (columns.length > 0)) {
        for (i = 0; i < columns.length; i++) {
          BeanTableModel.EditMode editable = BeanTableModel.EditMode.NON_EDITABLE;
          if ((editableColumns != null) && (editableColumns.length > i) && (editableColumns[i].intValue() == i)) {
            editable = BeanTableModel.EditMode.EDITABLE;
          }
          tableModel.addColumn(convertToCaption(columns[i]), columns[i], editable);
        }
      }
      


      tableModel.addRows(dataList);
      jtable = new PosTable();
      jtable.setRowHeight(PosUIManager.getSize(30));
      jtable.setSelectionMode(0);
      jtable.setDefaultRenderer(Object.class, new CustomCellRenderer());
      jtable.getInputMap().put(KeyStroke.getKeyStroke(32, 0), "startEditing");
      jtable.setModel(tableModel);
      setLayout(new BorderLayout(5, 5));
      add(new JScrollPane(jtable));
      if (colWidths != null) {
        resizeColumnWidth(jtable, colWidths);
      }
    }
    



    public void resizeColumnWidth(JTable table, Integer[] colWidths)
    {
      TableColumnModel columnModel = table.getColumnModel();
      for (int column = 0; column < table.getColumnCount(); column++) {
        if (colWidths.length == column)
          break;
        columnModel.getColumn(column).setPreferredWidth(colWidths[column].intValue());
      }
    }
    
    private String convertToCaption(String s) {
      StringBuilder out = new StringBuilder(s);
      Pattern p = Pattern.compile("[A-Z]");
      Matcher m = p.matcher(s);
      int extraFeed = 0;
      while (m.find()) {
        if (m.start() != 0) {
          out = out.insert(m.start() + extraFeed, " ");
          extraFeed++;
        }
      }
      return out.toString().toUpperCase();
    }
  }
  
  public void progress(int percent, String text)
  {
    lblProgress.setText(text);
  }
  

  public void progress(int percent) {}
  

  public Component getParentComponent()
  {
    return POSUtil.getFocusedWindow();
  }
  
  public void progress(String text)
  {
    lblProgress.setText("Importing... " + text);
  }
}
