package com.floreantpos.bo.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.bo.ui.explorer.UserExplorer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

















public class UserExplorerAction
  extends AbstractAction
{
  public UserExplorerAction()
  {
    super(POSConstants.USERS);
  }
  
  public UserExplorerAction(String name) {
    super(name);
  }
  
  public UserExplorerAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      UserExplorer explorer = null;
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab(POSConstants.USERS);
      if (index == -1) {
        explorer = new UserExplorer();
        tabbedPane.addTab(POSConstants.USERS, explorer);
      }
      else {
        explorer = (UserExplorer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(explorer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
