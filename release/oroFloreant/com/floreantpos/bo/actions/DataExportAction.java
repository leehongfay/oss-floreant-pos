package com.floreantpos.bo.actions;

import com.floreantpos.Messages;
import com.floreantpos.PosLog;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuItemModifierPage;
import com.floreantpos.model.MenuItemModifierPageItem;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.dao.GenericDAO;
import com.floreantpos.model.dao.InventoryUnitDAO;
import com.floreantpos.model.dao.InventoryUnitGroupDAO;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.model.dao.MenuItemModifierSpecDAO;
import com.floreantpos.model.dao.MenuModifierDAO;
import com.floreantpos.model.dao.ModifierGroupDAO;
import com.floreantpos.model.dao.TaxDAO;
import com.floreantpos.model.dao.TaxGroupDAO;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.datamigrate.Elements;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import org.apache.commons.io.IOUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
















public class DataExportAction
  extends AbstractAction
{
  public DataExportAction()
  {
    super(Messages.getString("DataExportAction.0"));
  }
  
  public void actionPerformed(ActionEvent e)
  {
    Session session = null;
    Transaction transaction = null;
    FileWriter fileWriter = null;
    GenericDAO dao = new GenericDAO();
    try
    {
      JFileChooser fileChooser = getFileChooser();
      int option = fileChooser.showSaveDialog(POSUtil.getBackOfficeWindow());
      if (option != 0) {
        return;
      }
      
      File file = fileChooser.getSelectedFile();
      if (file.exists()) {
        option = JOptionPane.showConfirmDialog(POSUtil.getBackOfficeWindow(), 
          Messages.getString("DataExportAction.1") + file.getName() + "?", Messages.getString("DataExportAction.3"), 0);
        
        if (option != 0) {
          return;
        }
      }
      


      JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] { Elements.class });
      Marshaller m = jaxbContext.createMarshaller();
      m.setProperty("jaxb.formatted.output", Boolean.TRUE);
      m.setProperty("jaxb.fragment", Boolean.TRUE);
      
      StringWriter writer = new StringWriter();
      
      session = dao.createNewSession();
      transaction = session.beginTransaction();
      
      Elements elements = new Elements();
      













      elements.setTaxes(TaxDAO.getInstance().findAll(session));
      elements.setTaxGroups(TaxGroupDAO.getInstance().findAll(session));
      elements.setUnits(InventoryUnitDAO.getInstance().findAll(session));
      elements.setUnitGroups(InventoryUnitGroupDAO.getInstance().findAll(session));
      elements.setMenuCategories(MenuCategoryDAO.getInstance().findAll(session));
      elements.setMenuGroups(MenuGroupDAO.getInstance().findAll(session));
      elements.setMenuModifiers(MenuModifierDAO.getInstance().findAll(session));
      elements.setModifierGroups(ModifierGroupDAO.getInstance().findAll(session));
      elements.setMenuItems(MenuItemDAO.getInstance().findAll(session));
      List<MenuItemModifierSpec> menuItemModifierSpecs = MenuItemModifierSpecDAO.getInstance().findAll(session);
      elements.setMenuItemModifierGroups(menuItemModifierSpecs);
      







      for (Iterator localIterator1 = menuItemModifierSpecs.iterator(); localIterator1.hasNext();) { spec = (MenuItemModifierSpec)localIterator1.next();
        Set<MenuItemModifierPage> pages = spec.getModifierPages();
        if (pages != null) {
          for (MenuItemModifierPage menuItemModifierPage : pages) {
            menuItemModifierPage.setModifierSpecId(spec.getId());
            List<MenuItemModifierPageItem> items = menuItemModifierPage.getPageItems();
            if (items != null) {
              for (MenuItemModifierPageItem menuItemModifierPageItem : items)
                menuItemModifierPageItem.setParentPage(null);
            }
          }
        }
      }
      MenuItemModifierSpec spec;
      m.marshal(elements, writer);
      
      transaction.commit();
      
      fileWriter = new FileWriter(file);
      fileWriter.write(writer.toString());
      fileWriter.close();
      
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), Messages.getString("DataExportAction.4"));
    }
    catch (Exception e1) {
      if (transaction != null) {
        transaction.rollback();
      }
      
      PosLog.error(getClass(), e1);
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), e1.getMessage());
    } finally {
      IOUtils.closeQuietly(fileWriter);
      dao.closeSession(session);
    }
  }
  
  public static JFileChooser getFileChooser() {
    JFileChooser fileChooser = new JFileChooser();
    fileChooser.setFileSelectionMode(0);
    fileChooser.setMultiSelectionEnabled(false);
    fileChooser.setSelectedFile(new File("menu-items.xml"));
    fileChooser.setFileFilter(new FileFilter()
    {
      public String getDescription()
      {
        return "XML File";
      }
      
      public boolean accept(File f)
      {
        if (f.getName().endsWith(".xml")) {
          return true;
        }
        
        return false;
      }
    });
    return fileChooser;
  }
  
  private void fixMenuItemModifierGroups() {
    MenuItemModifierSpecDAO menuItemModifierGroupDAO = MenuItemModifierSpecDAO.getInstance();
    Session session = menuItemModifierGroupDAO.createNewSession();
    Transaction transaction = session.beginTransaction();
    
    try
    {
      List<MenuItem> menuItems = MenuItemDAO.getInstance().findAll(session);
      
      for (MenuItem menuItem : menuItems) {
        List<MenuItemModifierSpec> modiferGroups = menuItem.getMenuItemModiferSpecs();
        for (MenuItemModifierSpec menuItemModifierGroup : modiferGroups)
        {
          menuItemModifierGroupDAO.saveOrUpdate(menuItemModifierGroup, session);
        }
      }
      
      transaction.commit();
    } catch (Exception x) {
      if (transaction != null) {
        transaction.rollback();
      }
    } finally {
      menuItemModifierGroupDAO.closeSession(session);
    }
  }
}
