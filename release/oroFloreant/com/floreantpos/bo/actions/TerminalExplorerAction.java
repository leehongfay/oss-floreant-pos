package com.floreantpos.bo.actions;

import com.floreantpos.Messages;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.bo.ui.explorer.TerminalExplorer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;

public class TerminalExplorerAction
  extends AbstractAction
{
  private static final String TERMINALS = Messages.getString("TerminalExplorerAction.TERMINALS");
  
  public TerminalExplorerAction() {
    super(TERMINALS);
  }
  

  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      TerminalExplorer terminalExplorer = null;
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab(TERMINALS);
      if (index == -1) {
        terminalExplorer = new TerminalExplorer();
        tabbedPane.addTab(TERMINALS, terminalExplorer);
      }
      else {
        terminalExplorer = (TerminalExplorer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(terminalExplorer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
