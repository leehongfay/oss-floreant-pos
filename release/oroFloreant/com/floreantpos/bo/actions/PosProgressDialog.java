package com.floreantpos.bo.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.ProgressObserver;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.Timer;
import net.miginfocom.swing.MigLayout;

public abstract class PosProgressDialog
  extends POSDialog
  implements ProgressObserver
{
  private JLabel lblProgress;
  private JLabel lblDotString = new JLabel("");
  private JPanel progressPanel;
  private Timer timer;
  private JProgressBar jpb = new JProgressBar();
  private PosButton btnOk;
  
  public PosProgressDialog() {
    super(POSUtil.getBackOfficeWindow(), "", true);
    JPanel contentPanel = new JPanel();
    contentPanel.setLayout(new BorderLayout());
    progressPanel = new JPanel(new MigLayout("fillx,ins 20 20 5 20,center", "[250]", "[]"));
    lblProgress = new JLabel();
    progressPanel.add(lblProgress, "split 2");
    progressPanel.add(lblDotString, "wrap");
    progressPanel.add(jpb, "grow,span,wrap");
    
    jpb.setPreferredSize(PosUIManager.getSize(0, 30));
    
    btnOk = new PosButton(POSConstants.OK);
    btnOk.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        close();
      }
    });
    progressPanel.add(btnOk, "center,gaptop 5,gapbottom 5");
    
    contentPanel.setMinimumSize(PosUIManager.getSize(300, 200));
    contentPanel.add(progressPanel);
    timer = new Timer(200, new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        progress(jpb.getValue() + 1);
        if (lblDotString.equals(".")) {
          lblDotString.setText("..");
        }
        else if (lblDotString.equals("..")) {
          lblDotString.setText("...");
        }
        else if (lblDotString.equals("...")) {
          lblDotString.setText(".");
        }
      }
    });
    add(contentPanel);
  }
  
  public void close() {
    timer.stop();
    setCanceled(false);
    dispose();
  }
  
  public void setVisible(boolean b)
  {
    if (b)
    {


















      new Thread()
      {
        public void run()
        {
          btnOk.setEnabled(false);
          progress(0);
          lblDotString.setText(".");
          timer.start();
          try {
            doBackgroundTask();
            close();
            done();
          } catch (Exception e) {
            close();
            error(e);
          } finally {
            btnOk.setEnabled(true);
            timer.stop();
          }
          
        }
        
      }.start();
    } else {
      timer.stop();
    }
    super.setVisible(b);
  }
  
  public abstract void doBackgroundTask() throws Exception;
  
  public abstract void done();
  
  public abstract void error(Exception paramException);
  
  public void progress(int percent, String text)
  {
    lblProgress.setText(text);
    jpb.setValue(percent);
  }
  
  public void progress(int percent)
  {
    jpb.setValue(percent);
  }
  
  public Component getParentComponent()
  {
    return POSUtil.getFocusedWindow();
  }
  
  public void progress(String text)
  {
    lblProgress.setText(text);
  }
  
  public void setProgressLabelText(String text) {
    lblProgress.setText(text);
  }
}
