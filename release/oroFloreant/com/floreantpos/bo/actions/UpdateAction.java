package com.floreantpos.bo.actions;

import com.floreantpos.main.Application;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;


















public class UpdateAction
  extends AbstractAction
{
  public UpdateAction()
  {
    super("Update");
  }
  
  public UpdateAction(String name) {
    super(name);
  }
  
  public UpdateAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent e) {
    try {
      Application.getInstance().checkForUpdate();
    } catch (Exception ex) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), ex.getMessage());
    }
  }
}
