package com.floreantpos.bo.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.report.DailySummaryReportView;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;
















public class DailySummaryReportAction
  extends AbstractAction
{
  public DailySummaryReportAction()
  {
    super(POSConstants.DAILY_SUMMARY_REPORT);
  }
  
  public DailySummaryReportAction(String name) {
    super(name);
  }
  
  public DailySummaryReportAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      
      DailySummaryReportView reportView = null;
      int index = tabbedPane.indexOfTab(POSConstants.DAILY_SUMMARY_REPORT);
      if (index == -1) {
        reportView = new DailySummaryReportView();
        tabbedPane.addTab(POSConstants.DAILY_SUMMARY_REPORT, reportView);
      }
      else {
        reportView = (DailySummaryReportView)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(reportView);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
