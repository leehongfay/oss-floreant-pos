package com.floreantpos.bo.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.config.ui.TerminalConfigurationDialog;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;


















public class TerminalConfigurationAction
  extends AbstractAction
{
  private Terminal terminal;
  
  public TerminalConfigurationAction(Terminal terminal)
  {
    this.terminal = terminal;
  }
  
  public TerminalConfigurationAction() {
    super(POSConstants.CONFIGURATION);
  }
  
  public TerminalConfigurationAction(String name) {
    super(name);
  }
  
  public TerminalConfigurationAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    try {
      if (terminal == null) {
        terminal = DataProvider.get().getCurrentTerminal();
      }
      TerminalConfigurationDialog dialog = new TerminalConfigurationDialog(terminal);
      dialog.setSize(PosUIManager.getSize(700, 600));
      dialog.open();
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e);
    }
  }
}
