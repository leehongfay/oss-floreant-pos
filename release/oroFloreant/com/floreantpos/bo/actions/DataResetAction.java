package com.floreantpos.bo.actions;

import com.floreantpos.PosLog;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

















public class DataResetAction
  extends AbstractAction
{
  public DataResetAction()
  {
    super("Cleanup DB");
  }
  
  public void actionPerformed(ActionEvent e)
  {
    try {
      DataResetDialog dialog = new DataResetDialog();
      dialog.setSize(PosUIManager.getSize(500, 550));
      dialog.open();
      if (dialog.isCanceled())
        return;
    } catch (Exception e1) {
      PosLog.error(getClass(), e1);
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), e1.getMessage());
    }
  }
}
