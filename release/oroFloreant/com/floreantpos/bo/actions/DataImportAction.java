package com.floreantpos.bo.actions;

import com.floreantpos.Messages;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.InventoryUnitGroup;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuItemModifierPage;
import com.floreantpos.model.MenuItemModifierPageItem;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.MenuPage;
import com.floreantpos.model.MenuPageItem;
import com.floreantpos.model.ModifierGroup;
import com.floreantpos.model.PrinterGroup;
import com.floreantpos.model.Tax;
import com.floreantpos.model.TaxGroup;
import com.floreantpos.model.User;
import com.floreantpos.model.UserType;
import com.floreantpos.model.dao.InventoryUnitDAO;
import com.floreantpos.model.dao.InventoryUnitGroupDAO;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.model.dao.MenuItemModifierSpecDAO;
import com.floreantpos.model.dao.MenuModifierDAO;
import com.floreantpos.model.dao.MenuPageDAO;
import com.floreantpos.model.dao.MenuPageItemDAO;
import com.floreantpos.model.dao.ModifierGroupDAO;
import com.floreantpos.model.dao.PrinterGroupDAO;
import com.floreantpos.model.dao.TaxDAO;
import com.floreantpos.model.dao.TaxGroupDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.model.dao.UserTypeDAO;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.ProgressObserver;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.datamigrate.Elements;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.IOUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;


















public class DataImportAction
  extends AbstractAction
{
  public DataImportAction()
  {
    super(Messages.getString("DataImportAction.0"));
  }
  
  public void actionPerformed(ActionEvent e)
  {
    JFileChooser fileChooser = DataExportAction.getFileChooser();
    int option = fileChooser.showOpenDialog(POSUtil.getBackOfficeWindow());
    if (option != 0) {
      return;
    }
    
    File file = fileChooser.getSelectedFile();
    try {
      DataImportDialog dialog = new DataImportDialog(file);
      dialog.setSize(PosUIManager.getSize(800, 550));
      dialog.open();
      if (dialog.isCanceled())
        return;
    } catch (Exception e1) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e1.getMessage(), e1);
    }
  }
  
  public static Elements importMenuItemsFromFile(File file) throws Exception
  {
    if (file == null) {
      new FileNotFoundException("File Not Found");
    }
    FileInputStream inputStream = new FileInputStream(file);
    return loadMenuItems(inputStream);
  }
  

  public static boolean saveImportedMenuItems(Elements elements, ProgressObserver progress)
    throws Exception
  {
    try
    {
      importMenuItems(elements, progress);
    } catch (Exception e) {
      e = 
      





        e;e.printStackTrace();throw e; } finally {}
    generateSampleMenuPages();
    return true;
  }
  
  public static Elements loadMenuItems(InputStream inputStream) throws Exception {
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] { Elements.class });
      Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
      Elements elements = (Elements)unmarshaller.unmarshal(inputStream);
      return elements;
    } catch (Exception e) {
      throw e;
    } finally {
      IOUtils.closeQuietly(inputStream);
    }
  }
  
  public static void importMenuItems(Elements elements, ProgressObserver progress) throws Exception
  {
    Map<String, Object> objectMap = new HashMap();
    try {
      TaxGroup defaultTaxGroup = ensureTaxGroup();
      
      List<TaxGroup> taxGroups = elements.getTaxGroups();
      Iterator localIterator1; TaxGroup taxGroup; if (taxGroups != null) {
        for (localIterator1 = taxGroups.iterator(); localIterator1.hasNext();) { taxGroup = (TaxGroup)localIterator1.next();
          TaxGroup existingTaxGroup = TaxGroupDAO.getInstance().get(taxGroup.getId());
          List<Tax> newTaxes = new ArrayList();
          if (taxGroup.getTaxes() != null) {
            for (Tax tax : taxGroup.getTaxes()) {
              Tax newTax = (Tax)objectMap.get(tax.getUniqueId());
              newTaxes.add(newTax);
            }
          }
          taxGroup.setTaxes(newTaxes);
          if (existingTaxGroup == null) {
            TaxGroupDAO.getInstance().save(taxGroup);
          } else {
            taxGroup.setId(existingTaxGroup.getId());
            taxGroup.setVersion(existingTaxGroup.getVersion());
            PropertyUtils.copyProperties(existingTaxGroup, taxGroup);
            TaxGroupDAO.getInstance().update(existingTaxGroup);
            taxGroup = existingTaxGroup;
          }
          objectMap.put(taxGroup.getUniqueId(), taxGroup);
          progress.progress("Tax Group " + taxGroup.toString());
        }
      }
      else {
        Object taxes = elements.getTaxes();
        if (taxes != null) {
          for (Tax tax : (List)taxes) {
            Tax existingTax = TaxDAO.getInstance().get(tax.getId());
            if (existingTax == null) {
              TaxDAO.getInstance().save(tax);
              defaultTaxGroup.addTotaxes(existingTax);
              TaxGroupDAO.getInstance().update(defaultTaxGroup);
            }
            else {
              tax.setId(existingTax.getId());
              tax.setVersion(existingTax.getVersion());
              PropertyUtils.copyProperties(existingTax, tax);
              TaxDAO.getInstance().update(existingTax);
              defaultTaxGroup.addTotaxes(existingTax);
              TaxGroupDAO.getInstance().update(defaultTaxGroup);
              tax = existingTax;
            }
            progress.progress("Tax " + tax.toString());
            objectMap.put(tax.getUniqueId(), tax);
          }
        }
      }
      
      Object unitGroups = elements.getUnitGroups();
      if (unitGroups != null)
        for (taxGroup = ((List)unitGroups).iterator(); taxGroup.hasNext();) { unitGroup = (InventoryUnitGroup)taxGroup.next();
          InventoryUnitGroup existingUnitGroup = InventoryUnitGroupDAO.getInstance().findByName(unitGroup.getName());
          unitGroup.setUnits(null);
          if (existingUnitGroup == null) {
            InventoryUnitGroupDAO.getInstance().save(unitGroup);
          }
          else {
            unitGroup.setId(existingUnitGroup.getId());
            unitGroup.setVersion(existingUnitGroup.getVersion());
            PropertyUtils.copyProperties(existingUnitGroup, unitGroup);
            InventoryUnitGroupDAO.getInstance().update(existingUnitGroup);
            unitGroup = existingUnitGroup;
          }
          if (existingUnitGroup == null) {
            objectMap.put(unitGroup.getId(), unitGroup);
          } else
            objectMap.put(existingUnitGroup.getId(), unitGroup);
          progress.progress("Unit Group " + unitGroup.toString());
        }
      InventoryUnitGroup unitGroup;
      List<InventoryUnit> units = elements.getUnits();
      if (units != null) {
        for (unitGroup = units.iterator(); unitGroup.hasNext();) { unit = (InventoryUnit)unitGroup.next();
          InventoryUnit existingUnit = InventoryUnitDAO.getInstance().get(unit.getId());
          String unitGroupId = unit.getUnitGroupId();
          InventoryUnitGroup inventoryUnitGroup = (InventoryUnitGroup)objectMap.get(unitGroupId);
          
          if (inventoryUnitGroup != null) {
            unit.setUnitGroupId(inventoryUnitGroup.getId());
          } else {
            unit.setUnitGroupId(null);
          }
          if (existingUnit == null) {
            InventoryUnitDAO.getInstance().save(unit);
          } else {
            unit.setId(existingUnit.getId());
            unit.setVersion(existingUnit.getVersion());
            PropertyUtils.copyProperties(existingUnit, unit);
            InventoryUnitDAO.getInstance().update(existingUnit);
            unit = existingUnit;
          }
          objectMap.put(unit.getUniqueId(), unit);
          progress.progress("Unit " + unit.toString());
        }
      }
      InventoryUnit unit;
      List<MenuCategory> menuCategories = elements.getMenuCategories();
      if (menuCategories != null) {
        for (unit = menuCategories.iterator(); unit.hasNext();) { menuCategory = (MenuCategory)unit.next();
          MenuCategory existingMenuCategory = MenuCategoryDAO.getInstance().get(((MenuCategory)menuCategory).getId());
          if (existingMenuCategory == null) {
            MenuCategoryDAO.getInstance().save((MenuCategory)menuCategory);
          }
          else {
            ((MenuCategory)menuCategory).setId(existingMenuCategory.getId());
            ((MenuCategory)menuCategory).setVersion(existingMenuCategory.getVersion());
            PropertyUtils.copyProperties(existingMenuCategory, menuCategory);
            MenuCategoryDAO.getInstance().update((MenuCategory)menuCategory);
            menuCategory = existingMenuCategory;
          }
          objectMap.put(((MenuCategory)menuCategory).getUniqueId(), menuCategory);
          progress.progress("Menu Category " + ((MenuCategory)menuCategory).toString());
        }
      }
      Object menuCategory;
      List<MenuGroup> menuGroups = elements.getMenuGroups();
      if (menuGroups != null) {
        for (menuCategory = menuGroups.iterator(); ((Iterator)menuCategory).hasNext();) { menuGroup = (MenuGroup)((Iterator)menuCategory).next();
          MenuGroup existingMenuGroup = MenuGroupDAO.getInstance().get(menuGroup.getId());
          if (existingMenuGroup == null) {
            MenuGroupDAO.getInstance().save(menuGroup);
          }
          else {
            menuGroup.setId(existingMenuGroup.getId());
            menuGroup.setVersion(existingMenuGroup.getVersion());
            PropertyUtils.copyProperties(existingMenuGroup, menuGroup);
            MenuGroupDAO.getInstance().update(existingMenuGroup);
            menuGroup = existingMenuGroup;
          }
          objectMap.put(menuGroup.getUniqueId(), menuGroup);
          progress.progress("Menu Group " + menuGroup.toString());
        }
      }
      MenuGroup menuGroup;
      Object menuModifiers = elements.getMenuModifiers();
      if (menuModifiers != null) {
        for (menuGroup = ((List)menuModifiers).iterator(); menuGroup.hasNext();) { menuModifier = (MenuModifier)menuGroup.next();
          MenuModifier existingMenuModifier = MenuModifierDAO.getInstance().get(menuModifier.getId());
          TaxGroup taxGroup = menuModifier.getTaxGroup();
          if (taxGroup != null) {
            taxGroup = (TaxGroup)objectMap.get(taxGroup.getUniqueId());
            menuModifier.setTaxGroup(taxGroup);
          }
          menuModifier.setTaxGroup(null);
          if (existingMenuModifier == null) {
            MenuModifierDAO.getInstance().save(menuModifier);
          } else {
            menuModifier.setId(existingMenuModifier.getId());
            menuModifier.setVersion(existingMenuModifier.getVersion());
            PropertyUtils.copyProperties(existingMenuModifier, menuModifier);
            MenuModifierDAO.getInstance().update(existingMenuModifier);
            menuModifier = existingMenuModifier;
          }
          objectMap.put(menuModifier.getUniqueId(), menuModifier);
          progress.progress("Menu Modifier " + menuModifier.toString());
        }
      }
      MenuModifier menuModifier;
      List<ModifierGroup> modifierGroups = elements.getModifierGroups();
      if (modifierGroups != null) {
        for (menuModifier = modifierGroups.iterator(); menuModifier.hasNext();) { modifierGroup = (ModifierGroup)menuModifier.next();
          ModifierGroup existingModifierGroup = ModifierGroupDAO.getInstance().get(modifierGroup.getId());
          List<MenuModifier> newModifiersForGroup = new ArrayList();
          if (modifierGroup.getModifiers() != null) {
            for (MenuModifier modifier : modifierGroup.getModifiers()) {
              MenuModifier newModifier = (MenuModifier)objectMap.get(modifier.getUniqueId());
              newModifiersForGroup.add(newModifier);
            }
          }
          modifierGroup.setModifiers(newModifiersForGroup);
          if (existingModifierGroup == null) {
            ModifierGroupDAO.getInstance().save(modifierGroup);
          } else {
            modifierGroup.setId(existingModifierGroup.getId());
            modifierGroup.setVersion(existingModifierGroup.getVersion());
            PropertyUtils.copyProperties(existingModifierGroup, modifierGroup);
            ModifierGroupDAO.getInstance().update(existingModifierGroup);
            modifierGroup = existingModifierGroup;
          }
          objectMap.put(modifierGroup.getUniqueId(), modifierGroup);
          progress.progress("Modifier Group " + modifierGroup.toString());
        }
      }
      ModifierGroup modifierGroup;
      List<MenuItem> menuItems = elements.getMenuItems();
      if (menuItems != null) {
        for (modifierGroup = menuItems.iterator(); modifierGroup.hasNext();) { menuItem = (MenuItem)modifierGroup.next();
          MenuItem existingMenuItem = null;
          if (menuItem.getId() != null) {
            existingMenuItem = MenuItemDAO.getInstance().get(menuItem.getId());
          }
          













          PrinterGroup printerGroup = menuItem.getPrinterGroup();
          if (printerGroup != null) {
            PrinterGroup pg = PrinterGroupDAO.getInstance().findByName(printerGroup.getName());
            if (pg == null) {
              pg = new PrinterGroup();
              PropertyUtils.copyProperties(pg, printerGroup);
              PrinterGroupDAO.getInstance().save(pg);
            }
            printerGroup.setId(pg.getId());
          }
          
          TaxGroup taxGroup = menuItem.getTaxGroup();
          if (taxGroup != null) {
            taxGroup = (TaxGroup)objectMap.get(taxGroup.getUniqueId());
            menuItem.setTaxGroup(taxGroup);
          }
          
          InventoryUnit inventoryUnit = menuItem.getUnit();
          if (inventoryUnit != null) {
            inventoryUnit = (InventoryUnit)objectMap.get(inventoryUnit.getUniqueId());
            menuItem.setUnit(inventoryUnit);
          }
          List<MenuItemModifierSpec> menuItemModiferGroups = menuItem.getMenuItemModiferSpecs();
          if (menuItemModiferGroups != null) {
            MenuItem.copyModifierSpecsToMenuItem(menuItem, menuItemModiferGroups);
            for (MenuItemModifierSpec menuItemModifierSpec : menuItem.getMenuItemModiferSpecs()) {
              ModifierGroup modifierGroup = menuItemModifierSpec.getModifierGroup();
              if (modifierGroup != null)
                menuItemModifierSpec.setModifierGroup((ModifierGroup)objectMap.get(modifierGroup.getUniqueId()));
              Set<MenuItemModifierPage> specPages = menuItemModifierSpec.getModifierPages();
              if (specPages != null) {
                for (MenuItemModifierPage menuItemModifierPage : specPages) {
                  List<MenuItemModifierPageItem> pageItems = menuItemModifierPage.getPageItems();
                  if (pageItems != null) {
                    for (MenuItemModifierPageItem menuItemModifierPageItem : pageItems) {
                      MenuModifier modifier = menuItemModifierPageItem.getMenuModifier();
                      if (modifier != null)
                        menuItemModifierPageItem.setMenuModifier((MenuModifier)objectMap.get(modifier.getUniqueId()));
                    }
                  }
                }
              }
              MenuItemModifierSpecDAO.getInstance().save(menuItemModifierSpec);
            }
          }
          menuItem.setAttributes(null);
          menuItem.setComboItems(null);
          menuItem.setComboGroups(null);
          menuItem.setParentMenuItem(null);
          menuItem.setVariants(null);
          menuItem.setStockUnits(null);
          try {
            if (existingMenuItem == null) {
              menuItem.setVersion(0L);
              Session session = MenuItemDAO.getInstance().createNewSession();
              Transaction transaction = session.beginTransaction();
              MenuItemDAO.getInstance().save(menuItem, session);
              transaction.commit();
              session.close();
            }
            else {
              menuItem.setId(existingMenuItem.getId());
              menuItem.setVersion(existingMenuItem.getVersion());
              PropertyUtils.copyProperties(existingMenuItem, menuItem);
              MenuItemDAO.getInstance().update(existingMenuItem);
              menuItem = existingMenuItem;
            }
            objectMap.put(menuItem.getUniqueId(), menuItem);
            progress.progress("Menu Item " + menuItem.toString());
          }
          catch (Exception e) {}
        }
      }
      
      MenuItem menuItem;
      List<User> users = elements.getUsers();
      if (users != null) {
        for (menuItem = users.iterator(); menuItem.hasNext();) { user = (User)menuItem.next();
          User existingUser = UserDAO.getInstance().get(user.getId());
          if (existingUser == null) {
            UserDAO.getInstance().save(user);
          }
          else {
            user.setId(existingUser.getId());
            user.setVersion(existingUser.getVersion());
            PropertyUtils.copyProperties(existingUser, user);
            UserDAO.getInstance().update(user);
            user = existingUser;
          }
          objectMap.put(user.getId(), user);
          progress.progress("User " + user.toString());
        }
      }
      User user;
      List<UserType> userTypes = elements.getUserTypes();
      if (userTypes != null) {
        for (UserType userType : userTypes) {
          UserType existingUserType = UserTypeDAO.getInstance().get(userType.getId());
          if (existingUserType == null) {
            UserTypeDAO.getInstance().save(userType);
          }
          else {
            userType.setId(existingUserType.getId());
            userType.setVersion(existingUserType.getVersion());
            PropertyUtils.copyProperties(existingUserType, userType);
            UserTypeDAO.getInstance().update(userType);
            userType = existingUserType;
          }
          objectMap.put(userType.getId(), userType);
          progress.progress("User type " + userType.toString());
        }
      }
    } catch (Exception ex) {
      ex = 
      


        ex;throw ex;
    } finally {}
  }
  
  private static TaxGroup ensureTaxGroup() {
    List<TaxGroup> taxGroups = TaxGroupDAO.getInstance().findAll();
    TaxGroup taxGroup; TaxGroup taxGroup; if (taxGroups.size() > 0) {
      taxGroup = (TaxGroup)taxGroups.get(0);
    }
    else {
      taxGroup = new TaxGroup();
      taxGroup.setName("default");
      TaxGroupDAO.getInstance().save(taxGroup);
    }
    return taxGroup;
  }
  
  private static void generateSampleMenuPages() {
    List<MenuGroup> groups = MenuGroupDAO.getInstance().findAll();
    if (groups == null)
      return;
    for (MenuGroup menuGroup : groups) {
      List<MenuItem> items = MenuItemDAO.getInstance().findByParent(null, menuGroup, true);
      if ((items != null) && (!items.isEmpty()))
      {

        int count = 0;
        genenatePage(menuGroup, items, count, 1);
      }
    }
  }
  
  private static void genenatePage(MenuGroup menuGroup, List<MenuItem> items, int count, int pageNumber) {
    try { MenuPage menuPage = new MenuPage();
      menuPage.setMenuGroupId(menuGroup.getId());
      menuPage.setName("Page " + pageNumber);
      menuPage.setRows(Integer.valueOf(4));
      menuPage.setCols(Integer.valueOf(4));
      
      for (int row = 0; row < 4; row++) {
        if (count >= items.size())
          break;
        for (int col = 0; col < 4; col++) {
          if (count >= items.size())
            break;
          MenuPageItem pageItem = new MenuPageItem(Integer.valueOf(col), Integer.valueOf(row));
          pageItem.setMenuPage(menuPage);
          pageItem.setMenuItem((MenuItem)items.get(count));
          MenuPageItemDAO.getInstance().saveOrUpdate(pageItem);
          count++;
        }
      }
      MenuPageDAO.getInstance().saveOrUpdate(menuPage);
      if (count < items.size()) {
        genenatePage(menuGroup, items, count, pageNumber++);
      }
      else {
        return;
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      BOMessageDialog.showError(ex.getMessage());
    }
  }
}
