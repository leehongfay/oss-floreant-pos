package com.floreantpos.bo.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.report.ReportViewer;
import com.floreantpos.report.SalesReport;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;


















public class SalesReportAction
  extends AbstractAction
{
  private boolean isInventoryItem;
  
  public SalesReportAction(boolean isInventoryItem)
  {
    this.isInventoryItem = isInventoryItem;
    if (isInventoryItem) {
      putValue("Name", POSConstants.SALES_REPORT_INVENTORY);
    }
    else {
      putValue("Name", POSConstants.SALES_REPORT_NONINVENTORY);
    }
  }
  
  public SalesReportAction(String name)
  {
    super(name);
  }
  
  public SalesReportAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      
      ReportViewer viewer = null;
      String indexName = isInventoryItem ? POSConstants.SALES_REPORT_INVENTORY : POSConstants.SALES_REPORT_NONINVENTORY;
      int index = tabbedPane.indexOfTab(indexName);
      if (index == -1) {
        viewer = new ReportViewer(new SalesReport(isInventoryItem));
        tabbedPane.addTab(indexName, viewer);
      }
      else {
        viewer = (ReportViewer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(viewer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
