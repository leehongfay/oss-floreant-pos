package com.floreantpos.bo.actions;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.bo.ui.explorer.PizzaExplorer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

public class PizzaItemExplorerAction extends AbstractAction
{
  public PizzaItemExplorerAction()
  {
    super("Pizza");
  }
  
  public PizzaItemExplorerAction(String name) {
    super(name);
  }
  
  public PizzaItemExplorerAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      
      PizzaExplorer explorer = null;
      int index = tabbedPane.indexOfTab("Pizza");
      if (index == -1) {
        explorer = new PizzaExplorer();
        tabbedPane.addTab("Pizza", explorer);
      }
      else {
        explorer = (PizzaExplorer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(explorer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
