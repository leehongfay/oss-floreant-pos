package com.floreantpos.bo.actions;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.report.GiftCardDetailReportView;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

















public class GiftCardDetailReportAction
  extends AbstractAction
{
  public GiftCardDetailReportAction()
  {
    super("Gift Card Detail Report");
  }
  
  public GiftCardDetailReportAction(String name) {
    super(name);
  }
  
  public GiftCardDetailReportAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      
      GiftCardDetailReportView reportView = null;
      int index = tabbedPane.indexOfTab("Gift Card Detail Report");
      if (index == -1) {
        reportView = new GiftCardDetailReportView();
        tabbedPane.addTab("Gift Card Detail Report", reportView);
      }
      else {
        reportView = (GiftCardDetailReportView)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(reportView);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
