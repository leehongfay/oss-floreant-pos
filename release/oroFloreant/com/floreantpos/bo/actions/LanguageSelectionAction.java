package com.floreantpos.bo.actions;

import com.floreantpos.Messages;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.LanguageSelectionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;



















public class LanguageSelectionAction
  extends AbstractAction
{
  public LanguageSelectionAction()
  {
    super(Messages.getString("LanguageSelectionAction.0"));
  }
  
  public LanguageSelectionAction(String name) {
    super(name);
  }
  
  public LanguageSelectionAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    try {
      LanguageSelectionDialog dialog = new LanguageSelectionDialog();
      dialog.setTitle(Messages.getString("LanguageSelectionAction.1"));
      dialog.setDefaultCloseOperation(2);
      dialog.setSize(PosUIManager.getSize(600, 400));
      dialog.setLocationRelativeTo(POSUtil.getFocusedWindow());
      dialog.setVisible(true);
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e);
    }
  }
}
