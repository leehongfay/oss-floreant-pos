package com.floreantpos.bo.actions;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.bo.ui.menudesigner.MenuPageDesigner;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

















public class MenuPageExplorerAction
  extends AbstractAction
{
  public MenuPageExplorerAction()
  {
    super("Menu page designer");
  }
  
  public MenuPageExplorerAction(String name) {
    super(name);
  }
  
  public MenuPageExplorerAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    
    try
    {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab("Menu page designer");
      MenuPageDesigner pageExplorer; if (index == -1) {
        MenuPageDesigner pageExplorer = new MenuPageDesigner();
        tabbedPane.addTab("Menu page designer", pageExplorer);
        pageExplorer.initData();
      }
      else {
        pageExplorer = (MenuPageDesigner)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(pageExplorer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
