package com.floreantpos.bo.actions;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.bo.ui.explorer.StoreSessionExplorer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

















public class StoreSessionExplorerAction
  extends AbstractAction
{
  public StoreSessionExplorerAction()
  {
    super("Store Session History");
  }
  
  public StoreSessionExplorerAction(String name) {
    super(name);
  }
  
  public StoreSessionExplorerAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      StoreSessionExplorer explorer = null;
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab("Store Session History");
      if (index == -1) {
        explorer = new StoreSessionExplorer();
        tabbedPane.addTab("Store Session History", explorer);
      }
      else {
        explorer = (StoreSessionExplorer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(explorer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
