package com.floreantpos.bo.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.main.Application;
import com.floreantpos.model.AttendenceHistory;
import com.floreantpos.model.Shift;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.AttendenceHistoryDAO;
import com.floreantpos.model.dao.GenericDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.CheckBoxList;
import com.floreantpos.swing.ProgressObserver;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.StoreUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import net.miginfocom.swing.MigLayout;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class DataResetDialog
  extends OkCancelOptionDialog
  implements ProgressObserver
{
  private JTabbedPane tabPane;
  private JLabel lblProgress;
  private JPanel progressPanel;
  private CheckBoxList chkDataTables = new CheckBoxList();
  
  public DataResetDialog() {
    super(POSUtil.getBackOfficeWindow(), true);
    
    setTitle("Cleanup DB");
    setOkButtonText("Cleanup DB");
    setCaption("Cleanup DB");
    JPanel contentPanel = getContentPanel();
    contentPanel.setLayout(new BorderLayout());
    
    List<String> beans = new ArrayList();
    beans.add("Ticket");
    beans.add("Cash Drawer & Session History");
    beans.add("Inventory Transaction");
    beans.add("Inventory Stock");
    beans.add("Reset Item Stock Avaiable & On Hand Qty");
    beans.add("Purchase Order");
    beans.add("Stock Count");
    
    chkDataTables.setModel(beans);
    chkDataTables.selectItems(beans);
    
    contentPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
    
    JPanel dataListPanel = new JPanel(new BorderLayout());
    dataListPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 0, 0));
    dataListPanel.setBackground(Color.white);
    dataListPanel.add(chkDataTables);
    tabPane = new JTabbedPane();
    tabPane.addTab("Data Table", dataListPanel);
    contentPanel.add(tabPane);
    
    progressPanel = new JPanel(new MigLayout("fillx"));
    lblProgress = new JLabel("Deleting..");
    progressPanel.add(lblProgress, "grow,span,wrap");
    progressPanel.setVisible(false);
    
    contentPanel.add(progressPanel, "South");
  }
  
  public void doOk()
  {
    if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Operation cannot be undone.\nAre you sure you want to cleanup db?", POSConstants.CONFIRM) != 0)
    {
      return;
    }
    progressPanel.setVisible(true);
    new Thread() {
      public void run() {
        doResetData();
        if (!StoreUtil.isStoreOpen())
          Application.getInstance().doLogout();
      }
    }.start();
  }
  
  protected void doResetData() {
    try {
      getButtonPanel().getComponent(0).setEnabled(false);
      progressPanel.setVisible(true);
      progress(0, "Deleting tables data.. Please wait..");
      try {
        List<String> beanTables = chkDataTables.getCheckedValues();
        for (String string : beanTables) {
          switch (string) {
          case "Ticket": 
            doDeleteTickets();
            break;
          case "Cash Drawer & Session History": 
            doDeleteCashDrawerAndSessionHistory();
            break;
          case "Inventory Transaction": 
            doDeleteInventoryTransactions();
            break;
          case "Inventory Stock": 
            doDeleteInventoryStock();
            break;
          case "Reset Item Stock Avaiable & On Hand Qty": 
            doUpdateMenuItemStockQuantity();
            break;
          
          case "Purchase Order": 
            doDeletePurchaseOrder();
            break;
          
          case "Stock Count": 
            doDeleteStockCount();
          

          }
          
        }
      }
      catch (Exception e)
      {
        BOMessageDialog.showError(e.getMessage());
      }
      progress(100, "Complete..");
      progressPanel.setVisible(false);
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), "Successfully clear table data.");
      setCanceled(false);
      dispose();
    } catch (Exception e) {
      BOMessageDialog.showError(e);
      progress(0, "Data reset failed");
    } finally {
      getButtonPanel().getComponent(0).setEnabled(true);
    }
  }
  
  private void doDeleteCashDrawerAndSessionHistory() {
    List<String> dataTables0 = new ArrayList();
    dataTables0.add("Terminal set currentCashDrawer=null, assignedUser=null");
    dataTables0.add("User set currentCashDrawer=null");
    executeQuery(dataTables0, "update ");
    
    List<String> dataTables1 = new ArrayList();
    dataTables1.add("OTHER_REVENUE_CATEGORY");
    doDelete(dataTables1, true);
    
    List<String> dataTables = new ArrayList();
    dataTables.add("ActionHistory");
    dataTables.add("AttendenceHistory");
    dataTables.add("CashDrawer");
    dataTables.add("CashBreakdown");
    dataTables.add("StoreSessionControl");
    dataTables.add("StoreSession where closeTime!=null");
    doDelete(dataTables);
    
    Application instance = Application.getInstance();
    User user = Application.getCurrentUser();
    AttendenceHistoryDAO attendenceHistoryDAO = new AttendenceHistoryDAO();
    AttendenceHistory attendenceHistory = attendenceHistoryDAO.findHistoryByClockedInTime(user);
    if (attendenceHistory == null) {
      attendenceHistory = new AttendenceHistory();
      Date lastClockInTime = user.getLastClockInTime();
      Calendar c = Calendar.getInstance();
      c.setTime(lastClockInTime);
      attendenceHistory.setClockInTime(lastClockInTime);
      attendenceHistory.setClockInHour(Short.valueOf((short)c.get(10)));
      attendenceHistory.setUser(user);
      attendenceHistory.setTerminal(Application.getInstance().getTerminal());
      attendenceHistory.setShift(user.getCurrentShift());
    }
    
    Shift shift = user.getCurrentShift();
    Calendar calendar = Calendar.getInstance();
    user.doClockOut(attendenceHistory, shift, calendar);
    instance.refreshAndGetTerminal();
    instance.refreshCurrentUser();
  }
  
  private void doUpdateMenuItemStockQuantity() {
    List<String> sqlList = new ArrayList();
    String sql = String.format("MenuItemInventoryStatus set availableUnit =  %s,unitOnHand = %s", new Object[] { new Double(0.0D), new Double(0.0D) });
    sqlList.add(sql);
    executeQuery(sqlList, "update ");
  }
  
  private void doDeleteTickets() {
    List<Ticket> tickets = TicketDAO.getInstance().findAll();
    for (Ticket ticket : tickets) {
      TicketDAO.getInstance().delete(ticket);
    }
    Object dataSqlTables = new ArrayList();
    ((List)dataSqlTables).add("KIT_TICKET_TABLE_NUM");
    doDelete((List)dataSqlTables, true);
    
    List<String> dataTables = new ArrayList();
    dataTables.add("VoidItem");
    dataTables.add("KitchenTicketItem");
    dataTables.add("KitchenTicket");
    doDelete(dataTables);
  }
  
  private void doDeletePurchaseOrder() {
    List<String> dataTables = new ArrayList();
    dataTables.add("PurchaseOrderItem");
    dataTables.add("PurchaseOrder");
    doDelete(dataTables);
  }
  
  private void doDeleteInventoryTransactions() {
    List<String> dataTables = new ArrayList();
    dataTables.add("InventoryTransaction");
    doDelete(dataTables);
  }
  
  private void doDeleteInventoryStock() {
    List<String> dataTables = new ArrayList();
    dataTables.add("InventoryStock");
    doDelete(dataTables);
  }
  
  private void doDeleteStockCount() {
    List<String> dataTables = new ArrayList();
    dataTables.add("StockCountItem");
    dataTables.add("StockCount");
    doDelete(dataTables);
  }
  
  private void doDelete(List<String> dataTables) {
    executeQuery(dataTables, "delete from ");
  }
  
  private void doDelete(List<String> dataTables, boolean tableSql) {
    executeQuery(dataTables, "delete from ", tableSql);
  }
  
  private void executeQuery(List<String> dataTables, String action) {
    executeQuery(dataTables, action, false);
  }
  
  private void executeQuery(List<String> dataTables, String action, boolean tableSql) {
    Transaction tx = null;
    Session session = null;
    GenericDAO dao = new GenericDAO();
    try {
      session = dao.createNewSession();
      tx = session.beginTransaction();
      for (String tableClass : dataTables) {
        String sql = action + tableClass;
        if (tableSql) {
          Query createQuery = session.createSQLQuery(sql);
          createQuery.executeUpdate();
        }
        else {
          Query createQuery = session.createQuery(sql);
          createQuery.executeUpdate();
        }
      }
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
    } finally {
      dao.closeSession(session);
    }
  }
  
  public void progress(int percent, String text)
  {
    lblProgress.setText(text);
  }
  

  public void progress(int percent) {}
  

  public Component getParentComponent()
  {
    return POSUtil.getFocusedWindow();
  }
  
  public void progress(String text)
  {
    lblProgress.setText("Deleting... " + text);
  }
}
