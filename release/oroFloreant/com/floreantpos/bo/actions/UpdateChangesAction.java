package com.floreantpos.bo.actions;

import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.update.UpdateChangesInfoDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;


















public class UpdateChangesAction
  extends AbstractAction
{
  public UpdateChangesAction()
  {
    super("What is new");
  }
  
  public UpdateChangesAction(String name) {
    super(name);
  }
  
  public UpdateChangesAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent e) {
    try {
      UpdateChangesInfoDialog dialog = new UpdateChangesInfoDialog(POSUtil.getBackOfficeWindow(), getClass().getResource("/oropos-change.log.xml"), true);
      dialog.setSize(500, 600);
      dialog.setLocationRelativeTo(POSUtil.getBackOfficeWindow());
      dialog.setVisible(true);
      if (dialog.isCanceled())
        return;
    } catch (Exception ex) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), ex.getMessage());
    }
  }
}
