package com.floreantpos.bo.actions;

import com.floreantpos.Messages;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.report.PurchaseReportView;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

public class PurchaseReportAction extends AbstractAction
{
  public PurchaseReportAction()
  {
    super(Messages.getString("PurchaseReportAction.0"));
  }
  
  public PurchaseReportAction(String name) {
    super(name);
  }
  
  public PurchaseReportAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      
      PurchaseReportView reportView = null;
      int index = tabbedPane.indexOfTab("Purchase Report");
      if (index == -1) {
        reportView = new PurchaseReportView();
        tabbedPane.addTab("Purchase Report", reportView);
      }
      else {
        reportView = (PurchaseReportView)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(reportView);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
