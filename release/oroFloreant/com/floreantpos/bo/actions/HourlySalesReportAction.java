package com.floreantpos.bo.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.report.HourlySalesReportView;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;
















public class HourlySalesReportAction
  extends AbstractAction
{
  public HourlySalesReportAction()
  {
    super(POSConstants.HOURLY_LABOR_REPORT);
  }
  
  public HourlySalesReportAction(String name) {
    super(name);
  }
  
  public HourlySalesReportAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      
      HourlySalesReportView reportView = null;
      int index = tabbedPane.indexOfTab(POSConstants.HOURLY_LABOR_REPORT);
      if (index == -1) {
        reportView = new HourlySalesReportView();
        tabbedPane.addTab(POSConstants.HOURLY_LABOR_REPORT, reportView);
      }
      else {
        reportView = (HourlySalesReportView)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(reportView);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
