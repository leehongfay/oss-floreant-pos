package com.floreantpos.bo.actions;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.report.PaymentReceivedReportView;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

















public class PaymentReceivedReportAction
  extends AbstractAction
{
  public PaymentReceivedReportAction()
  {
    super("Payment Received Report");
  }
  
  public PaymentReceivedReportAction(String name) {
    super(name);
  }
  
  public PaymentReceivedReportAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      
      PaymentReceivedReportView reportView = null;
      int index = tabbedPane.indexOfTab("Payment Received Report");
      if (index == -1) {
        reportView = new PaymentReceivedReportView();
        tabbedPane.addTab("Payment Received Report", reportView);
      }
      else {
        reportView = (PaymentReceivedReportView)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(reportView);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
