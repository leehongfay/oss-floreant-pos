package com.floreantpos.bo.actions;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.report.ReportViewer;
import com.floreantpos.report.VoidItemReport;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

















public class VoidItemReportAction
  extends AbstractAction
{
  private static final String VOID_ITEM_REPORT = "Void Item Report";
  
  public VoidItemReportAction()
  {
    super("Void Item Report");
  }
  
  public VoidItemReportAction(String name) {
    super(name);
  }
  
  public VoidItemReportAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      
      ReportViewer viewer = null;
      int index = tabbedPane.indexOfTab("Void Item Report");
      if (index == -1) {
        viewer = new ReportViewer(new VoidItemReport());
        tabbedPane.addTab("Void Item Report", viewer);
      }
      else {
        viewer = (ReportViewer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(viewer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
