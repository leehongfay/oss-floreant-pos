package com.floreantpos.bo.actions;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.report.WeeklyPayrollReportView;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

















public class WeeklyPayrollReportAction
  extends AbstractAction
{
  public WeeklyPayrollReportAction()
  {
    super("Weekly Payroll Report");
  }
  
  public WeeklyPayrollReportAction(String name) {
    super(name);
  }
  
  public WeeklyPayrollReportAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      
      WeeklyPayrollReportView reportView = null;
      int index = tabbedPane.indexOfTab("Weekly Payroll Report");
      if (index == -1) {
        reportView = new WeeklyPayrollReportView();
        tabbedPane.addTab("Weekly Payroll Report", reportView);
      }
      else {
        reportView = (WeeklyPayrollReportView)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(reportView);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
