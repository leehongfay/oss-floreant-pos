package com.floreantpos.bo.actions;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.bo.ui.explorer.SalesAreaExplorer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;


















public class SalesAreaExplorerAction
  extends AbstractAction
{
  public SalesAreaExplorerAction()
  {
    super("Sales Area");
  }
  
  public SalesAreaExplorerAction(String name) {
    super(name);
  }
  
  public SalesAreaExplorerAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      SalesAreaExplorer explorer = null;
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab("Sales Area Explorer");
      if (index == -1) {
        explorer = new SalesAreaExplorer();
        tabbedPane.addTab("Sales Area Explorer", explorer);
      }
      else {
        explorer = (SalesAreaExplorer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(explorer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
