package com.floreantpos.bo.actions;

import com.floreantpos.Messages;
import com.floreantpos.bo.ui.explorer.CurrencyDialog;
import com.floreantpos.main.Application;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;























public class CurrencyExplorerAction
  extends AbstractAction
{
  public CurrencyExplorerAction()
  {
    super(Messages.getString("CurrencyExplorerAction.0"));
  }
  
  public CurrencyExplorerAction(String name) {
    super(name);
  }
  
  public CurrencyExplorerAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    try {
      CurrencyDialog dialog = new CurrencyDialog();
      dialog.setTitle(Application.getTitle());
      dialog.setSize(800, 600);
      dialog.open();
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e);
    }
  }
}
