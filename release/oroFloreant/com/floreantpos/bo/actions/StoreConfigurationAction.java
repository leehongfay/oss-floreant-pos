package com.floreantpos.bo.actions;

import com.floreantpos.config.ui.StoreConfigurationDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;

public class StoreConfigurationAction extends AbstractAction
{
  public StoreConfigurationAction()
  {
    super(com.floreantpos.POSConstants.STORECONFIGURATION);
  }
  
  public StoreConfigurationAction(String name) {
    super(name);
  }
  
  public StoreConfigurationAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    try {
      StoreConfigurationDialog dialog = new StoreConfigurationDialog();
      dialog.setSize(750, 700);
      dialog.open();
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e);
    }
  }
}
