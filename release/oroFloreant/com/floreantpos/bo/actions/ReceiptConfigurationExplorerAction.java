package com.floreantpos.bo.actions;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.bo.ui.explorer.ReceiptConfigurationExplorer;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.dao.OrderTypeDAO;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;

public class ReceiptConfigurationExplorerAction
  extends AbstractAction
{
  public ReceiptConfigurationExplorerAction()
  {
    super(POSConstants.RECEIPT_CONFIGURATION);
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      List<OrderType> orderTypes = OrderTypeDAO.getInstance().findAll();
      if ((orderTypes == null) || (orderTypes.isEmpty())) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("ReceiptConfigurationView.66"));
        return;
      }
      
      ReceiptConfigurationExplorer receiptConfigurationExplorer = null;
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab(POSConstants.RECEIPT_CONFIGURATION);
      if (index == -1) {
        receiptConfigurationExplorer = new ReceiptConfigurationExplorer();
        tabbedPane.addTab(POSConstants.RECEIPT_CONFIGURATION, receiptConfigurationExplorer);
      }
      else {
        receiptConfigurationExplorer = (ReceiptConfigurationExplorer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(receiptConfigurationExplorer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
