package com.floreantpos.bo.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.bo.ui.explorer.MenuExplorer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;
















public class ItemExplorerAction
  extends AbstractAction
{
  public ItemExplorerAction()
  {
    super(POSConstants.MENU_ITEMS);
  }
  
  public ItemExplorerAction(String name) {
    super(name);
  }
  
  public ItemExplorerAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    
    try
    {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab(POSConstants.ITEM_EXPLORER);
      MenuExplorer item; if (index == -1) {
        MenuExplorer item = new MenuExplorer();
        tabbedPane.addTab(POSConstants.ITEM_EXPLORER, item);
      }
      else {
        item = (MenuExplorer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(item);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
