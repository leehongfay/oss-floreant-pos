package com.floreantpos.bo.actions;

import com.floreantpos.Messages;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.bo.ui.explorer.DepartmentExplorer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

















public class DepartmentAction
  extends AbstractAction
{
  public DepartmentAction()
  {
    super(Messages.getString("DepartmentAction.0"));
  }
  
  public DepartmentAction(String name) {
    super(name);
  }
  
  public DepartmentAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    
    try
    {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab(Messages.getString("DepartmentAction.1"));
      DepartmentExplorer deptExplorer; if (index == -1) {
        DepartmentExplorer deptExplorer = new DepartmentExplorer();
        tabbedPane.addTab(Messages.getString("DepartmentAction.1"), deptExplorer);
      }
      else {
        deptExplorer = (DepartmentExplorer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(deptExplorer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
