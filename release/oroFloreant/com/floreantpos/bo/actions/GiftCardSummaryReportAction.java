package com.floreantpos.bo.actions;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.report.GiftCardSummaryReportView;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

















public class GiftCardSummaryReportAction
  extends AbstractAction
{
  public GiftCardSummaryReportAction()
  {
    super("Gift Card Summary Report");
  }
  
  public GiftCardSummaryReportAction(String name) {
    super(name);
  }
  
  public GiftCardSummaryReportAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      
      GiftCardSummaryReportView reportView = null;
      int index = tabbedPane.indexOfTab("Gift Card Summary Report");
      if (index == -1) {
        reportView = new GiftCardSummaryReportView();
        tabbedPane.addTab("Gift Card Summary Report", reportView);
      }
      else {
        reportView = (GiftCardSummaryReportView)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(reportView);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
