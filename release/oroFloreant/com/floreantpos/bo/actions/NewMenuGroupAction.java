package com.floreantpos.bo.actions;

import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.model.MenuGroupForm;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;




















public class NewMenuGroupAction
  extends AbstractAction
{
  public NewMenuGroupAction() {}
  
  public NewMenuGroupAction(String name)
  {
    super(name);
  }
  
  public NewMenuGroupAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    try {
      MenuGroupForm editor = new MenuGroupForm();
      BeanEditorDialog dialog = new BeanEditorDialog(editor);
      dialog.open();
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    }
  }
}
