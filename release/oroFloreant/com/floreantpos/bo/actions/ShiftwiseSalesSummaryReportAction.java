package com.floreantpos.bo.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.report.ShiftwiseSalesSummaryReportView;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;
















public class ShiftwiseSalesSummaryReportAction
  extends AbstractAction
{
  public ShiftwiseSalesSummaryReportAction()
  {
    super(POSConstants.SALES_ANALYSIS);
  }
  
  public ShiftwiseSalesSummaryReportAction(String name) {
    super(name);
  }
  
  public ShiftwiseSalesSummaryReportAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      ShiftwiseSalesSummaryReportView reportView = null;
      int index = tabbedPane.indexOfTab(POSConstants.SALES_ANALYSIS);
      if (index == -1) {
        reportView = new ShiftwiseSalesSummaryReportView();
        reportView.setReportType(2);
        tabbedPane.addTab(POSConstants.SALES_ANALYSIS, reportView);
      }
      else {
        reportView = (ShiftwiseSalesSummaryReportView)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(reportView);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
