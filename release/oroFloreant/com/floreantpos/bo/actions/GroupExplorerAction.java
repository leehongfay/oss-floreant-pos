package com.floreantpos.bo.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.bo.ui.explorer.MenuGroupExplorer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;
















public class GroupExplorerAction
  extends AbstractAction
{
  public GroupExplorerAction()
  {
    super(POSConstants.MENU_GROUPS);
  }
  
  public GroupExplorerAction(String name) {
    super(name);
  }
  
  public GroupExplorerAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    
    try
    {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab(POSConstants.GROUP_EXPLORER);
      MenuGroupExplorer group; if (index == -1) {
        MenuGroupExplorer group = new MenuGroupExplorer();
        tabbedPane.addTab(POSConstants.GROUP_EXPLORER, group);
      }
      else {
        group = (MenuGroupExplorer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(group);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
