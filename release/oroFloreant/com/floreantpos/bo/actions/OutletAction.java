package com.floreantpos.bo.actions;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.bo.ui.explorer.OutletExplorer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;


public class OutletAction
  extends AbstractAction
{
  public OutletAction()
  {
    super("Outlet");
  }
  
  public OutletAction(String name) {
    super(name);
  }
  
  public OutletAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    
    try
    {
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab("Outlet Explorer");
      OutletExplorer outletexplorer; if (index == -1) {
        OutletExplorer outletexplorer = new OutletExplorer();
        tabbedPane.addTab("Outlet Explorer", outletexplorer);
      }
      else {
        outletexplorer = (OutletExplorer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(outletexplorer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
