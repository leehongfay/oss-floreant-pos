package com.floreantpos.bo.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.bo.ui.explorer.TaxGroupExplorer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

















public class TaxExplorerAction
  extends AbstractAction
{
  public TaxExplorerAction()
  {
    super(POSConstants.TAX);
  }
  
  public TaxExplorerAction(String name) {
    super(name);
  }
  
  public TaxExplorerAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      TaxGroupExplorer explorer = null;
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab(POSConstants.TAX_EXPLORER);
      if (index == -1) {
        explorer = new TaxGroupExplorer();
        tabbedPane.addTab(POSConstants.TAX_EXPLORER, explorer);
      }
      else {
        explorer = (TaxGroupExplorer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(explorer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
