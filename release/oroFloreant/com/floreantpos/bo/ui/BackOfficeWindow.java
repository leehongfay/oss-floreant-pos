package com.floreantpos.bo.ui;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.actions.AboutAction;
import com.floreantpos.actions.DefaultPriceAction;
import com.floreantpos.actions.ImageGalleryAction;
import com.floreantpos.bo.actions.AttendanceHistoryAction;
import com.floreantpos.bo.actions.AttributeExplorerAction;
import com.floreantpos.bo.actions.CookingInstructionExplorerAction;
import com.floreantpos.bo.actions.CouponExplorerAction;
import com.floreantpos.bo.actions.CourseExplorerAction;
import com.floreantpos.bo.actions.CreditCardReportAction;
import com.floreantpos.bo.actions.CurrencyExplorerAction;
import com.floreantpos.bo.actions.CustomerPaymentReportAction;
import com.floreantpos.bo.actions.DailySummaryReportAction;
import com.floreantpos.bo.actions.DataExportAction;
import com.floreantpos.bo.actions.DataImportAction;
import com.floreantpos.bo.actions.DataResetAction;
import com.floreantpos.bo.actions.DepartmentAction;
import com.floreantpos.bo.actions.DrawerPullReportExplorerAction;
import com.floreantpos.bo.actions.EmployeeAttendanceAction;
import com.floreantpos.bo.actions.GiftCardBrowserAction;
import com.floreantpos.bo.actions.GiftCardDetailReportAction;
import com.floreantpos.bo.actions.GiftCardGenerateAction;
import com.floreantpos.bo.actions.GiftCardSummaryReportAction;
import com.floreantpos.bo.actions.HourlySalesReportAction;
import com.floreantpos.bo.actions.ItemExplorerAction;
import com.floreantpos.bo.actions.JournalReportAction;
import com.floreantpos.bo.actions.KeyStatisticsSalesReportAction;
import com.floreantpos.bo.actions.LanguageSelectionAction;
import com.floreantpos.bo.actions.MenuPageExplorerAction;
import com.floreantpos.bo.actions.MenuUsageReportAction;
import com.floreantpos.bo.actions.MultiplierExplorerAction;
import com.floreantpos.bo.actions.OpenTicketSummaryReportAction;
import com.floreantpos.bo.actions.OrdersTypeExplorerAction;
import com.floreantpos.bo.actions.PaymentReceivedReportAction;
import com.floreantpos.bo.actions.PayrollReportAction;
import com.floreantpos.bo.actions.PizzaItemExplorerAction;
import com.floreantpos.bo.actions.PriceExplorerAction;
import com.floreantpos.bo.actions.ReceiptConfigurationExplorerAction;
import com.floreantpos.bo.actions.SalesAreaExplorerAction;
import com.floreantpos.bo.actions.SalesBalanceReportAction;
import com.floreantpos.bo.actions.SalesDetailReportAction;
import com.floreantpos.bo.actions.SalesExceptionReportAction;
import com.floreantpos.bo.actions.SalesReportAction;
import com.floreantpos.bo.actions.ServerProductivityReportAction;
import com.floreantpos.bo.actions.ShiftExplorerAction;
import com.floreantpos.bo.actions.ShiftwiseSalesSummaryReportAction;
import com.floreantpos.bo.actions.StoreConfigurationAction;
import com.floreantpos.bo.actions.StoreSessionExplorerAction;
import com.floreantpos.bo.actions.TaxExplorerAction;
import com.floreantpos.bo.actions.TerminalConfigurationAction;
import com.floreantpos.bo.actions.TerminalExplorerAction;
import com.floreantpos.bo.actions.TicketExplorerAction;
import com.floreantpos.bo.actions.TracTipsReportAction;
import com.floreantpos.bo.actions.UpdateChangesAction;
import com.floreantpos.bo.actions.UserExplorerAction;
import com.floreantpos.bo.actions.UserTypeExplorerAction;
import com.floreantpos.bo.actions.VoidItemReportAction;
import com.floreantpos.bo.actions.WeeklyPayrollReportAction;
import com.floreantpos.config.AppConfig;
import com.floreantpos.config.AppProperties;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.customPayment.CustomPaymentBrowserAction;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.FloreantPlugin;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.main.Application;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.UserType;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.table.ShowTableBrowserAction;
import com.floreantpos.teminaltype.TerminalTypeAction;
import com.floreantpos.webservice.CloudSyncAction;
import com.jidesoft.swing.JideTabbedPane;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Locale;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;





















public class BackOfficeWindow
  extends JFrame
{
  private static final String POSY = "bwy";
  private static final String POSX = "bwx";
  private static final String WINDOW_HEIGHT = "bwheight";
  private static final String WINDOW_WIDTH = "bwwidth";
  private JMenu floorPlanMenu;
  private static BackOfficeWindow instance;
  private JMenuBar menuBar;
  private Set<UserPermission> permissions;
  private final User currentUser;
  private JPanel jPanel1;
  private JideTabbedPane tabbedPane;
  
  public BackOfficeWindow(User user)
  {
    currentUser = user;
    setIconImage(new ImageIcon(getClass().getResource("/icons/icon.png")).getImage());
    
    initComponents();
    
    createMenus();
    positionWindow();
    
    setDefaultCloseOperation(0);
    
    addWindowListener(new WindowAdapter()
    {
      public void windowClosing(WindowEvent e) {
        close();
      }
      
    });
    String appTitle = AppProperties.getAppName() + " " + AppProperties.getAppVersion();
    setTitle(appTitle + "- " + POSConstants.BACK_OFFICE);
    applyComponentOrientation(ComponentOrientation.getOrientation(Locale.getDefault()));
    
    instance = this;
    
    initPlugins();
    
    JMenu helpMenu = new JMenu(Messages.getString("BackOfficeWindow.0"));
    helpMenu.add(new AboutAction());
    helpMenu.add(new UpdateChangesAction());
    menuBar.add(helpMenu);
  }
  
  private void positionWindow() {
    int width = AppConfig.getInt("bwwidth", 900);
    int height = AppConfig.getInt("bwheight", 650);
    setSize(width, height);
    
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    int x = width - width >> 1;
    int y = height - height >> 1;
    
    x = AppConfig.getInt("bwx", x);
    y = AppConfig.getInt("bwy", y);
    
    setLocation(x, y);
  }
  
  protected void createMenus() {
    UserType newUserType = currentUser.getType();
    
    if (newUserType != null) {
      permissions = newUserType.getPermissions();
    }
    
    menuBar = new JMenuBar();
    
    if (newUserType == null) {
      createAdminMenu(menuBar);
      createExplorerMenu(menuBar);
      createReportMenu(menuBar);
      createFloorMenu(menuBar);
    }
    else
    {
      if ((permissions != null) && (permissions.contains(UserPermission.PERFORM_ADMINISTRATIVE_TASK))) {
        createAdminMenu(menuBar);
      }
      if ((permissions != null) && (permissions.contains(UserPermission.VIEW_EXPLORERS))) {
        createExplorerMenu(menuBar);
      }
      if ((permissions != null) && (permissions.contains(UserPermission.VIEW_REPORTS))) {
        createReportMenu(menuBar);
      }
    }
    
    createFloorMenu(menuBar);
    setJMenuBar(menuBar);
  }
  
  private void initPlugins() {
    for (FloreantPlugin plugin : ) {
      plugin.initBackoffice(this);
    }
  }
  
  protected void createReportMenu(JMenuBar menuBar) {
    JMenu reportMenu = new JMenu(POSConstants.REPORTS);
    reportMenu.add(new SalesReportAction(true));
    reportMenu.add(new SalesReportAction(false));
    reportMenu.add(new ShiftwiseSalesSummaryReportAction());
    reportMenu.add(new SalesBalanceReportAction());
    reportMenu.add(new KeyStatisticsSalesReportAction());
    reportMenu.add(new SalesExceptionReportAction());
    reportMenu.add(new SalesDetailReportAction());
    reportMenu.add(new MenuUsageReportAction());
    reportMenu.add(new VoidItemReportAction());
    reportMenu.add(new JournalReportAction());
    reportMenu.add(new JSeparator());
    reportMenu.add(new CreditCardReportAction());
    reportMenu.add(new OpenTicketSummaryReportAction());
    reportMenu.add(new JSeparator());
    reportMenu.add(new HourlySalesReportAction());
    reportMenu.add(new DailySummaryReportAction());
    reportMenu.add(new TracTipsReportAction());
    reportMenu.add(new PayrollReportAction());
    reportMenu.add(new WeeklyPayrollReportAction());
    reportMenu.add(new ServerProductivityReportAction());
    reportMenu.add(new EmployeeAttendanceAction());
    reportMenu.add(new PaymentReceivedReportAction());
    reportMenu.add(new CustomerPaymentReportAction());
    reportMenu.add(new JSeparator());
    reportMenu.add(new GiftCardSummaryReportAction());
    reportMenu.add(new GiftCardDetailReportAction());
    


    menuBar.add(reportMenu);
  }
  
  private void createExplorerMenu(JMenuBar menuBar) {
    JMenu explorerMenu = new JMenu(POSConstants.EXPLORERS);
    menuBar.add(explorerMenu);
    



    explorerMenu.add(new ItemExplorerAction());
    explorerMenu.add(new PizzaItemExplorerAction());
    explorerMenu.add(new MenuPageExplorerAction());
    explorerMenu.add(new JSeparator());
    explorerMenu.add(new ShiftExplorerAction());
    explorerMenu.add(new AttributeExplorerAction());
    explorerMenu.add(new MultiplierExplorerAction());
    explorerMenu.add(new CookingInstructionExplorerAction());
    explorerMenu.add(new JSeparator());
    explorerMenu.add(new CourseExplorerAction());
    explorerMenu.add(new CouponExplorerAction());
    explorerMenu.add(new DefaultPriceAction());
    explorerMenu.add(new PriceExplorerAction());
    explorerMenu.add(new JSeparator());
    if ((permissions != null) && (permissions.contains(UserPermission.VIEW_GIFT_CARD))) {
      explorerMenu.add(new GiftCardBrowserAction());
    }
    explorerMenu.add(new StoreSessionExplorerAction());
    explorerMenu.add(new TicketExplorerAction());
    explorerMenu.add(new DrawerPullReportExplorerAction());
    explorerMenu.add(new JSeparator());
    explorerMenu.add(new ImageGalleryAction());
    explorerMenu.add(new JSeparator());
    explorerMenu.add(new AttendanceHistoryAction());
    







    OrderServiceExtension plugin = (OrderServiceExtension)ExtensionManager.getPlugin(OrderServiceExtension.class);
    if (plugin == null) {
      return;
    }
    
    plugin.createCustomerMenu(explorerMenu);
  }
  
  protected void createAdminMenu(JMenuBar menuBar) {
    JMenu adminMenu = new JMenu(POSConstants.ADMIN);
    adminMenu.add(new StoreConfigurationAction());
    adminMenu.add(new TerminalConfigurationAction());
    if ((permissions != null) && (
      (permissions.contains(UserPermission.PERFORM_ADMINISTRATIVE_TASK)) || (permissions.contains(UserPermission.PERFORM_MANAGER_TASK)))) {
      adminMenu.add(new ReceiptConfigurationExplorerAction());
    }
    adminMenu.add(new JSeparator());
    adminMenu.add(new UserExplorerAction());
    adminMenu.add(new UserTypeExplorerAction());
    adminMenu.add(new JSeparator());
    adminMenu.add(new DepartmentAction());
    adminMenu.add(new SalesAreaExplorerAction());
    adminMenu.add(new TerminalExplorerAction());
    adminMenu.add(new TerminalTypeAction());
    if (TerminalConfig.isMultipleOrderSupported()) {
      adminMenu.add(new OrdersTypeExplorerAction());
    }
    adminMenu.add(new JSeparator());
    

    adminMenu.add(new CurrencyExplorerAction());
    adminMenu.add(new TaxExplorerAction());
    adminMenu.add(new CustomPaymentBrowserAction());
    adminMenu.add(new JSeparator());
    adminMenu.add(new LanguageSelectionAction());
    adminMenu.add(new JSeparator());
    adminMenu.add(new CloudSyncAction());
    

    adminMenu.add(new DataExportAction());
    adminMenu.add(new DataImportAction());
    
    if (Application.getInstance().isDevelopmentMode()) {
      adminMenu.add(new DataResetAction());
    }
    adminMenu.add("Close");
    
    menuBar.add(adminMenu);
  }
  
  protected void createFloorMenu(JMenuBar menuBar)
  {
    floorPlanMenu = new JMenu(Messages.getString("BackOfficeWindow.2"));
    floorPlanMenu.add(new ShowTableBrowserAction());
    
    menuBar.add(floorPlanMenu);
  }
  
  protected void createGiftCardMenu(JMenuBar menuBar)
  {
    JMenu giftCardMenu = new JMenu(Messages.getString("BackOfficeWindow.3"));
    giftCardMenu.add(new GiftCardGenerateAction());
    giftCardMenu.add(new GiftCardBrowserAction());
    menuBar.add(giftCardMenu);
  }
  




  private void initComponents()
  {
    jPanel1 = new JPanel();
    tabbedPane = new JideTabbedPane();
    tabbedPane.setFont(new Font("Sans", 0, PosUIManager.getDefaultFontSize()));
    tabbedPane.setTabShape(1);
    tabbedPane.setShowCloseButtonOnTab(true);
    tabbedPane.setTabInsets(new Insets(5, 5, 5, 5));
    
    getContentPane().setLayout(new BorderLayout(5, 0));
    
    setDefaultCloseOperation(3);
    jPanel1.setLayout(new BorderLayout(5, 0));
    
    jPanel1.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    jPanel1.add(tabbedPane, "Center");
    
    getContentPane().add(jPanel1, "Center");
  }
  





  public JTabbedPane getTabbedPane()
  {
    return tabbedPane;
  }
  
  private void saveSizeAndLocation() {
    AppConfig.putInt("bwwidth", getWidth());
    AppConfig.putInt("bwheight", getHeight());
    AppConfig.putInt("bwx", getX());
    AppConfig.putInt("bwy", getY());
  }
  
  public void close() {
    saveSizeAndLocation();
    
    dispose();
  }
  



  public static BackOfficeWindow getInstance()
  {
    return instance;
  }
  
  public JMenuBar getBackOfficeMenuBar() {
    return menuBar;
  }
  


  public JMenu getFloorPlanMenu()
  {
    return floorPlanMenu;
  }
  


  public void setFloorPlanMenu(JMenu floorPlanMenu)
  {
    this.floorPlanMenu = floorPlanMenu;
  }
  
  public void dispose()
  {
    instance = null;
    super.dispose();
  }
}
