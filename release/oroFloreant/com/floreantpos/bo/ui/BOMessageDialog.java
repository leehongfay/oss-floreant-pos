package com.floreantpos.bo.ui;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.util.POSUtil;
import java.awt.Component;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
















public class BOMessageDialog
{
  public BOMessageDialog() {}
  
  private static Logger logger = Logger.getLogger(Application.class);
  
  public static void showError(Component parent, String message) {
    JOptionPane.showMessageDialog(parent, message, POSConstants.MDS_POS, 0, null);
  }
  
  public static void showError(Component parent, String message, Throwable x)
  {
    logger.error(message, x);
    JOptionPane.showMessageDialog(parent, message, POSConstants.MDS_POS, 0, null);
  }
  
  public static void showError(String errorMessage) {
    JOptionPane.showMessageDialog(POSUtil.getFocusedWindow(), errorMessage, 
      Messages.getString("BOMessageDialog.0"), 0);
  }
  
  public static void showError(String errorMessage, Throwable t) {
    logger.error(errorMessage, t);
    if ((t instanceof ConstraintViolationException)) {
      String cause = t.getCause().getMessage();
      if (cause.contains("DELETE")) {
        errorMessage = "Unable to delete. Some objects are using this item.\n If you want to delete you have to release them first.";
      } else if (cause.contains("duplicate key")) {
        errorMessage = "The Item with same name already exists.";
      }
    }
    JOptionPane.showMessageDialog(POSUtil.getFocusedWindow(), errorMessage, 
      Messages.getString("BOMessageDialog.1"), 0);
  }
  
  public static void showError(Throwable t) {
    logger.error(Messages.getString("BOMessageDialog.2"), t);
    JOptionPane.showMessageDialog(POSUtil.getFocusedWindow(), 
      Messages.getString("BOMessageDialog.3"), Messages.getString("BOMessageDialog.4"), 0);
  }
}
