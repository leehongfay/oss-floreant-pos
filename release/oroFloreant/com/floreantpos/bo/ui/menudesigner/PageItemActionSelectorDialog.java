package com.floreantpos.bo.ui.menudesigner;

import com.floreantpos.POSConstants;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuPageItem;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;


















public class PageItemActionSelectorDialog
  extends POSDialog
  implements ActionListener
{
  private String actionCommand;
  
  public PageItemActionSelectorDialog(MenuPageItem pageItem)
  {
    super(POSUtil.getBackOfficeWindow(), "Select an action");
    setLayout(new BorderLayout(5, 5));
    
    MenuItem menuItem = pageItem.getMenuItem();
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle(menuItem.getName());
    
    add(titlePanel, "North");
    
    JPanel itemInfoPanel = new JPanel(new MigLayout("fill,inset 0"));
    itemInfoPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder("-"), BorderFactory.createEmptyBorder(10, 20, 10, 10)));
    
    String description = menuItem.getDescription();
    
    JLabel lblDescription = new JLabel();
    lblDescription.setText("<html><body>" + (description == null ? "" : description) + "</body></html>");
    
    JLabel pictureLabel = new JLabel(menuItem.getImage());
    if (menuItem.getImage() == null) {
      pictureLabel.setText("NO IMAGE");
      pictureLabel.setForeground(Color.gray);
    }
    pictureLabel.setPreferredSize(new Dimension(120, 120));
    pictureLabel.setBorder(BorderFactory.createBevelBorder(0));
    


    String standardPrice = "Standard Price: " + menuItem.getPrice();
    
    JLabel lblMenuItemInfo = new JLabel("<html><body>" + standardPrice + "</body></html>");
    lblMenuItemInfo.setFont(new Font(null, 0, 14));
    itemInfoPanel.add(lblMenuItemInfo);
    itemInfoPanel.add(pictureLabel, "span,wrap");
    itemInfoPanel.add(lblDescription, "wrap");
    
    add(itemInfoPanel);
    
    JPanel actionButtonPanel = new JPanel(new GridLayout(1, 0, 2, 2));
    actionButtonPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
    
    PosButton btnDetach = new PosButton("DETACH");
    btnDetach.addActionListener(this);
    actionButtonPanel.add(btnDetach);
    
    PosButton btnEdit = new PosButton("EDIT");
    btnEdit.addActionListener(this);
    actionButtonPanel.add(btnEdit);
    
    PosButton btnReplace = new PosButton("REPLACE");
    btnReplace.addActionListener(this);
    actionButtonPanel.add(btnReplace);
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL_BUTTON_TEXT);
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
      
    });
    actionButtonPanel.add(btnCancel);
    add(actionButtonPanel, "South");
  }
  
  public String getActionCommand() {
    return actionCommand;
  }
  
  public void actionPerformed(ActionEvent e)
  {
    actionCommand = e.getActionCommand();
    setCanceled(false);
    dispose();
  }
}
