package com.floreantpos.bo.ui.menudesigner;

import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.bo.ui.explorer.PageSelectionListener;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuPage;
import com.floreantpos.model.MenuPageItem;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.model.dao.MenuPageDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.MenuItemSelectionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.model.MenuItemForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
































public class MenuPageDesignView
  extends TransparentPanel
{
  public static final String VIEW_NAME = "ITEM_VIEW";
  private static final int HORIZONTAL_GAP = 5;
  private static final int VERTICAL_GAP = 5;
  private List<MenuPageItem> items;
  private Dimension buttonSize;
  private MenuPage menuPage;
  private JPanel buttonPanelContainer = new JPanel(new MigLayout("center,wrap 4"));
  
  private PageSelectionListener pageListener;
  
  private Integer cols;
  private Integer rows;
  
  public MenuPageDesignView()
  {
    buttonSize = new Dimension(TerminalConfig.getMenuItemButtonWidth(), TerminalConfig.getMenuItemButtonHeight());
    TitledBorder border = new TitledBorder(POSConstants.ITEMS);
    border.setTitleJustification(2);
    setBorder(new CompoundBorder(border, new EmptyBorder(2, 2, 2, 2)));
    setLayout(new BorderLayout(5, 5));
    buttonPanelContainer.setBackground(Color.GRAY);
    buttonPanelContainer.setOpaque(true);
    add(buttonPanelContainer);
  }
  
  public void setPageListener(PageSelectionListener listener) {
    pageListener = listener;
  }
  
  public MenuPage getMenuPage() {
    return menuPage;
  }
  
  public void setMenuPage(MenuPage menuPage) {
    this.menuPage = menuPage;
    reset();
    try {
      if (menuPage == null) {
        return;
      }
      MenuPageDAO.getInstance().initialize(menuPage);
      cols = menuPage.getCols();
      if (cols.intValue() <= 0) {
        cols = Integer.valueOf(4);
      }
      rows = menuPage.getRows();
      if (rows.intValue() <= 0) {
        rows = Integer.valueOf(4);
      }
      MigLayout migLayout = new MigLayout("center,wrap " + cols);
      if (menuPage.isFlixibleButtonSize().booleanValue()) {
        migLayout.setLayoutConstraints("fill");
        migLayout.setColumnConstraints("fill,grow");
        migLayout.setRowConstraints("fill,grow");
      }
      buttonPanelContainer.setLayout(migLayout);
      items = menuPage.getPageItems();
      renderItems();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public void setMenuItems(List<MenuPageItem> items) {
    this.items = items;
    try {
      renderItems();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  protected void renderItems() {
    reset();
    if (items == null)
      items = new ArrayList();
    Map<String, ItemButton> cellItemMap;
    int row;
    try { cellItemMap = new HashMap();
      for (row = 0; row < rows.intValue(); row++) {
        for (int col = 0; col < cols.intValue(); col++) {
          String cellKey = String.valueOf(col) + String.valueOf(row);
          MenuPageItem itemForCell = new MenuPageItem(Integer.valueOf(col), Integer.valueOf(row));
          itemForCell.setMenuPage(menuPage);
          ItemButton itemButton = (ItemButton)createItemButton(itemForCell);
          cellItemMap.put(cellKey, itemButton);
          String constraint = String.format("cell %s %s", new Object[] { Integer.valueOf(col), Integer.valueOf(row) });
          if (!menuPage.isFlixibleButtonSize().booleanValue()) {
            constraint = constraint + String.format(", w %s!, h %s!", new Object[] { menuPage.getButtonWidth(), menuPage.getButtonHeight() });
          }
          buttonPanelContainer.add(itemButton, constraint);
        }
      }
      for (MenuPageItem item : items) {
        String cellKey = String.valueOf(item.getCol()) + String.valueOf(item.getRow());
        ItemButton itemCellButton = (ItemButton)cellItemMap.get(cellKey);
        if (itemCellButton != null)
        {
          itemCellButton.setMenuItem(item.getMenuItem());
        }
      }
    } catch (Exception localException) {}
    revalidate();
    repaint();
  }
  
  public void reset() {
    buttonPanelContainer.removeAll();
  }
  
  public void fillSelectedPageItems(List<MenuPageItem> items) {
    int count = 0;
    for (int i = 0; i < buttonPanelContainer.getComponents().length; i++) {
      ItemButton button = (ItemButton)buttonPanelContainer.getComponent(i);
      if (button.isEmptyItem()) {
        MenuPageItem pageItem = (MenuPageItem)items.get(count);
        button.setMenuItem(pageItem.getMenuItem());
        count++;
      }
      if (count == items.size())
        break;
    }
  }
  
  protected AbstractButton createItemButton(Object item) {
    MenuPageItem menuPageItem = (MenuPageItem)item;
    
    ItemButton itemButton = new ItemButton(menuPageItem);
    return itemButton;
  }
  
  public class ItemButton extends PosButton implements ActionListener {
    MenuPageItem menuPageItem;
    
    ItemButton(MenuPageItem menuPageItem) {
      this.menuPageItem = menuPageItem;
      setFocusable(true);
      setFocusPainted(true);
      setVerticalTextPosition(3);
      setHorizontalTextPosition(0);
      setIconTextGap(0);
      setText("+");
      setPreferredSize(buttonSize);
      addActionListener(this);
      updateView();
    }
    
    public void setMenuPageItem(MenuPageItem item) {
      menuPageItem = item;
    }
    
    public void setMenuItem(MenuItem menuItem) {
      menuPageItem.setMenuItem(menuItem);
      menuPageItem.setMenuPage(menuPage);
      updateView();
    }
    
    public boolean isEmptyItem() {
      return menuPageItem.getMenuItem() == null;
    }
    
    private void updateView() {
      MenuItem menuItem = menuPageItem.getMenuItem();
      if (menuItem == null) {
        setIcon(null);
        setBackground(UIManager.getColor("control"));
        setForeground(Color.BLACK);
        setText("<html><body><center>+</center></body></html>");
        return;
      }
      ImageIcon image = menuItem.getImage();
      if (image != null) {
        if (menuItem.isShowImageOnly().booleanValue()) {
          setText("");
          setIcon(image);
        }
        else {
          setIcon(image);
          setText("<html><body><center>" + menuItem.getDisplayName() + "</center></body></html>");
        }
        
      }
      else {
        setText("<html><body><center>" + menuItem.getName() + "</center></body></html>");
      }
      
      Color buttonColor = menuItem.getButtonColor();
      if (buttonColor != null) {
        setBackground(buttonColor);
      }
      Color textColor = menuItem.getTextColor();
      if (textColor != null) {
        setForeground(textColor);
      }
    }
    
    public void actionPerformed(ActionEvent e) {
      doSelectMenuItem();
    }
    
    private void doSelectMenuItem() {
      try {
        MenuItem menuItem = menuPageItem.getMenuItem();
        List<MenuItem> existingItems = new ArrayList();
        if (menuItem != null) {
          existingItems.add(menuItem);
        }
        if (menuItem == null) {
          menuItem = doAddMenuItem();
          if (menuItem != null) {}
        }
        else
        {
          PageItemActionSelectorDialog dialog = new PageItemActionSelectorDialog(menuPageItem);
          dialog.setSize(PosUIManager.getSize(500, 350));
          dialog.open();
          if (dialog.isCanceled())
            return;
          String actionCommand = dialog.getActionCommand();
          if (actionCommand.equals("REPLACE")) {
            menuItem = doAddMenuItem();
            if (menuItem != null) {}

          }
          else if (actionCommand.equals("EDIT")) {
            MenuItemDAO.getInstance().initialize(menuItem);
            MenuItemForm editor = new MenuItemForm(menuItem);
            BeanEditorDialog formDialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
            formDialog.open();
            if (formDialog.isCanceled()) {
              return;
            }
          }
          else if (actionCommand.equals("DETACH")) {
            menuItem = null;
          }
        }
        menuPageItem.setMenuItem(menuItem);
        menuPageItem.setMenuPage(menuPage);
        updateView();
        
        if (pageListener == null)
          return;
        menuPage.addTopageItems(menuPageItem);
        menuPageItem.setMenuPage(menuPage);
        pageListener.itemSelected(menuPageItem);
      } catch (Exception e) {
        PosLog.error(getClass(), e);
      }
    }
    
    private MenuItem doAddMenuItem() {
      MenuItemSelectionDialog dialog = MenuItemSelectionDialog.getInstance();
      dialog.setSelectionMode(0);
      dialog.setSize(PosUIManager.getSize(800, 600));
      dialog.open();
      if (dialog.isCanceled())
        return null;
      return dialog.getSelectedRowData();
    }
  }
  
  public void resetPage() {
    for (int i = 0; i < buttonPanelContainer.getComponents().length; i++) {
      ItemButton button = (ItemButton)buttonPanelContainer.getComponent(i);
      if (!button.isEmptyItem()) {
        MenuPageItem pageItem = menuPageItem;
        if (pageItem.getId() == null)
          button.setMenuItem(null);
      }
    }
    revalidate();
    repaint();
  }
}
