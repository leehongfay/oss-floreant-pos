package com.floreantpos.bo.ui.menudesigner;

import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.explorer.PageSelectionListener;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuPage;
import com.floreantpos.model.MenuPageItem;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.model.dao.MenuPageDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.MenuItemSelectionDialog;
import com.floreantpos.ui.dialog.NumberSelectionDialog2;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.model.MenuCategoryForm;
import com.floreantpos.ui.model.MenuGroupForm;
import com.floreantpos.ui.model.MenuPageForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import net.miginfocom.swing.MigLayout;






















public class MenuPageDesigner
  extends TransparentPanel
  implements PageSelectionListener
{
  private JList<MenuCategory> cbCategory;
  private JList<MenuGroup> cbGroup;
  private MenuPageDesignView itemView;
  private JList<MenuPage> listMenuPages;
  private ComboBoxModel menuPageListModel;
  private JButton btnSave;
  private JButton btnCancel;
  private JButton btnAutoBuildPages;
  private JButton btnHidePage;
  private JButton btnNewPage;
  private JButton btnEditPage;
  private JButton btnRemoveAllPage;
  private JButton btnRemovePage;
  private JButton btnNewCategory;
  private JButton btnNewGroup;
  private JButton btnDetachedAll;
  
  public MenuPageDesigner()
  {
    initComponents();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    
    JPanel topActionPanel = new JPanel(new GridLayout(0, 1));
    JPanel lefQuickAccessPanel = new JPanel(new BorderLayout());
    lefQuickAccessPanel.setPreferredSize(PosUIManager.getSize(300, 0));
    lefQuickAccessPanel.setBorder(new TitledBorder("-"));
    
    JPanel rightCategroyPanel = new JPanel();
    rightCategroyPanel.setPreferredSize(PosUIManager.getSize(130, 82));
    
    add(rightCategroyPanel, "East");
    
    cbCategory = new JList(new ComboBoxModel());
    cbCategory.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
          return;
        }
        MenuPageDesigner.this.doSaveChanges();
        MenuPageDesigner.this.rendererSelectedCategory();
      }
      
    });
    cbGroup = new JList(new ComboBoxModel());
    cbGroup.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
          return;
        }
        MenuPageDesigner.this.doSaveChanges();
        MenuPageDesigner.this.rendererSelectedGroup();
      }
      
    });
    btnNewCategory = new JButton("+");
    btnNewGroup = new JButton("+");
    btnNewPage = new JButton("New Page");
    btnEditPage = new JButton("Edit Page");
    btnHidePage = new JButton("Hide Page");
    btnRemovePage = new JButton("Remove");
    btnRemoveAllPage = new JButton("Remove All");
    
    btnRemovePage.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        MenuPageDesigner.this.doRemovePage();
      }
      
    });
    btnRemoveAllPage.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        MenuPageDesigner.this.doRemoveAllPage();
      }
      
    });
    btnHidePage.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        MenuPage selectedPage = (MenuPage)listMenuPages.getSelectedValue();
        if (selectedPage == null)
          return;
        selectedPage.setVisible(Boolean.valueOf(!selectedPage.isVisible().booleanValue()));
        MenuPageDAO.getInstance().saveOrUpdate(selectedPage);
        btnHidePage.setText(selectedPage.isVisible().booleanValue() ? "Hide Page" : "Show Page");
      }
      
    });
    btnNewCategory.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuPageDesigner.this.doAddNewCategory();
      }
      
    });
    btnNewGroup.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuPageDesigner.this.doAddNewGroup();
      }
      
    });
    btnNewPage.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuPageDesigner.this.doSaveChanges();
        MenuPageDesigner.this.doAddNewMenuPage();
      }
      
    });
    btnEditPage.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuPageDesigner.this.doSaveChanges();
        MenuPageDesigner.this.doEditMenuPage();
      }
      
    });
    btnAutoBuildPages = new JButton("Auto Build Pages");
    btnAutoBuildPages.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuPageDesigner.this.doGenenateMenuPages();
      }
      
    });
    JScrollPane categoryScroller = new JScrollPane(cbCategory);
    categoryScroller.setBorder(new TitledBorder("Categories"));
    topActionPanel.add(categoryScroller, String.format("grow, h %s", new Object[] { Integer.valueOf(200) }));
    

    JScrollPane groupScroller = new JScrollPane(cbGroup);
    groupScroller.setBorder(new TitledBorder("Groups"));
    topActionPanel.add(groupScroller, String.format("newline, grow, h %s", new Object[] { Integer.valueOf(200) }));
    

    menuPageListModel = new ComboBoxModel();
    listMenuPages = new JList(menuPageListModel);
    listMenuPages.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting())
          return;
        MenuPageDesigner.this.doSaveChanges();
        MenuPageDesigner.this.rendererSelectedPage();
      }
    });
    JScrollPane pageScroller = new JScrollPane(listMenuPages);
    pageScroller.setBorder(new TitledBorder("Pages"));
    topActionPanel.add(pageScroller, String.format("newline, grow, wrap", new Object[0]));
    
    lefQuickAccessPanel.add(topActionPanel);
    
    JPanel leftButtonPanel = new JPanel(new MigLayout("fill"));
    
    leftButtonPanel.add(btnAutoBuildPages, "growx");
    leftButtonPanel.add(btnNewPage, "growx");
    leftButtonPanel.add(btnEditPage, "growx,wrap");
    leftButtonPanel.add(btnHidePage, "growx");
    leftButtonPanel.add(btnRemovePage, "growx");
    leftButtonPanel.add(btnRemoveAllPage, "growx");
    btnRemoveAllPage.setEnabled(false);
    lefQuickAccessPanel.add(leftButtonPanel, "South");
    
    JPanel contentPanel = new JPanel(new BorderLayout());
    
    itemView = new MenuPageDesignView();
    itemView.setPageListener(this);
    
    JPanel southItemActionPanel = new JPanel(new MigLayout("fillx"));
    JButton btnAddItem = new JButton("Add Item");
    btnAddItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuPageDesigner.this.doSelectPageItems();
      }
      
    });
    btnSave = new JButton("Save");
    btnSave.setEnabled(false);
    btnSave.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doSaveMenuPage();
      }
    });
    btnCancel = new JButton("Cancel");
    btnCancel.setEnabled(false);
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuPage menuPage = (MenuPage)listMenuPages.getSelectedValue();
        menuPage = MenuPageDAO.getInstance().get(menuPage.getId());
        btnCancel.setEnabled(false);
        btnSave.setEnabled(false);
        itemView.setMenuPage(menuPage);
      }
      
    });
    btnDetachedAll = new JButton("Detach All");
    btnDetachedAll.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuPageDesigner.this.doDetachAllItems();
      }
    });
    southItemActionPanel.add(new JSeparator(), "grow,span");
    
    southItemActionPanel.add(btnSave, "split 4");
    southItemActionPanel.add(btnCancel);
    
    southItemActionPanel.add(btnDetachedAll);
    
    itemView.add(southItemActionPanel, "South");
    contentPanel.add(itemView);
    
    add(lefQuickAccessPanel, "West");
    add(contentPanel);
    enableDisablePageButtons(false);
  }
  
  private void doSaveChanges() {
    if (btnSave.isEnabled()) {
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), "Do you want to save the changes?", "Confirm") == 0) {
        doSaveMenuPage();
      }
      else {
        itemView.resetPage();
        btnSave.setEnabled(false);
        btnCancel.setEnabled(false);
      }
    }
  }
  
  protected void doSaveMenuPage() {
    MenuPage menuPage = (MenuPage)listMenuPages.getSelectedValue();
    try {
      MenuPageDAO.getInstance().saveOrUpdate(menuPage);
      btnSave.setEnabled(false);
      btnCancel.setEnabled(false);
    }
    catch (Exception e) {
      BOMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e);
    }
  }
  
  private void doRemovePage() {
    try {
      MenuPage menuPage = (MenuPage)listMenuPages.getSelectedValue();
      if (menuPage == null)
        return;
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), "Are you sure you want to delete?", "Confirm") != 0) {
        return;
      }
      MenuPageDAO.getInstance().delete(menuPage);
      menuPageListModel.removeElement(menuPage);
      if (menuPageListModel.getSize() > 0) {
        listMenuPages.setSelectedIndex(menuPageListModel.getSize() - 1);
      }
      else
        rendererSelectedPage();
    } catch (Exception ex) {
      POSMessageDialog.showError(ex.getMessage());
    }
  }
  
  private void doRemoveAllPage() {
    try {
      List<MenuPage> menuPageList = menuPageListModel.getDataList();
      if ((menuPageList == null) || (menuPageList.isEmpty()))
        return;
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), "Are you sure you want to remove all pages from group " + cbGroup
        .getSelectedValue() + "?", "Confirm") != 0) {
        return;
      }
      MenuPageDAO.getInstance().deleteAll(menuPageList);
      menuPageListModel.removeAllElements();
      btnRemoveAllPage.setEnabled(false);
      rendererSelectedPage();
    } catch (Exception ex) {
      POSMessageDialog.showError(ex.getMessage());
    }
  }
  
  public void initData() {
    List<MenuCategory> categories = MenuCategoryDAO.getInstance().findAll();
    ComboBoxModel categoryListModel = (ComboBoxModel)cbCategory.getModel();
    categoryListModel.setDataList(categories);
  }
  
  private void rendererSelectedCategory() {
    MenuCategory selectedCategory = (MenuCategory)cbCategory.getSelectedValue();
    if (selectedCategory == null) {
      return;
    }
    ComboBoxModel groupModel = (ComboBoxModel)cbGroup.getModel();
    groupModel.removeAllElements();
    menuPageListModel.removeAllElements();
    
    List<MenuGroup> groupList = MenuGroupDAO.getInstance().findByParent(selectedCategory);
    if (groupList != null) {
      groupModel.setDataList(groupList);
      if (groupList.size() > 0) {
        cbGroup.setSelectedIndex(0);
      }
    }
    listMenuPages.setSelectedValue(null, false);
    itemView.reset();
    itemView.revalidate();
    itemView.repaint();
  }
  
  private void rendererSelectedGroup() {
    menuPageListModel.removeAllElements();
    
    MenuGroup selectedGroup = (MenuGroup)cbGroup.getSelectedValue();
    if (selectedGroup != null) {
      List<MenuPage> menuPages = MenuPageDAO.getInstance().findByGroup(selectedGroup);
      if ((menuPages != null) && (menuPages.size() > 0)) {
        menuPageListModel.setDataList(menuPages);
        btnRemoveAllPage.setEnabled(true);
      }
      else {
        btnRemoveAllPage.setEnabled(false);
      }
    }
    


    rendererSelectedPage();
  }
  
  private void rendererSelectedPage()
  {
    MenuPage menuPage = null;
    if (menuPageListModel.getSize() > 0) {
      menuPage = (MenuPage)listMenuPages.getSelectedValue();
      if (menuPage != null) {
        btnHidePage.setText(menuPage.isVisible().booleanValue() ? "Hide Page" : "Show Page");
        enableDisablePageButtons(true);
        btnRemoveAllPage.setEnabled(true);
      }
      else {
        enableDisablePageButtons(false);
      }
    }
    else {
      enableDisablePageButtons(false);
    }
    itemView.setMenuPage(menuPage);
  }
  
  private void enableDisablePageButtons(boolean enable) {
    btnEditPage.setEnabled(enable);
    btnHidePage.setEnabled(enable);
    btnRemovePage.setEnabled(enable);
    btnDetachedAll.setEnabled(enable);
  }
  
  public void doCreateNewPage() {
    if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), "Page not found.\n Do you want to create page?", "Confirm") != 0) {
      return;
    }
    int pageQuatity = (int)NumberSelectionDialog2.takeIntInput("Enter page quantity");
    if (pageQuatity < 1) {
      return;
    }
    List<MenuPage> menuPages = new ArrayList();
    for (int i = 0; i < pageQuatity; i++) {
      MenuPage page = new MenuPage();
      page.setName("Page " + (i + 1));
      menuPages.add(page);
    }
    try {
      MenuPageDAO.getInstance().saveOrUpdatePages(menuPages);
      menuPageListModel.setDataList(menuPages);
      listMenuPages.setSelectedIndex(0);
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    }
  }
  
  protected List<MenuItem> getSelectedMenuItems(List<MenuItem> selectedItems) {
    MenuItemSelectionDialog dialog = new MenuItemSelectionDialog(new ArrayList());
    dialog.setSelectionMode(1);
    dialog.setSize(PosUIManager.getSize(750, 515));
    dialog.open();
    if (dialog.isCanceled()) {
      return null;
    }
    List<MenuItem> items = dialog.getSelectedItems();
    MenuPage page = (MenuPage)listMenuPages.getSelectedValue();
    int pageSize = page.getCols().intValue() * page.getRows().intValue();
    if ((items != null) && (items.size() > pageSize)) {
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), "Maximum size is " + pageSize);
      return getSelectedMenuItems(items);
    }
    return items;
  }
  
  private void doAddNewCategory() {
    try {
      MenuCategoryForm editor = new MenuCategoryForm();
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      
      if (dialog.isCanceled()) {
        return;
      }
      MenuCategory foodCategory = (MenuCategory)editor.getBean();
      ComboBoxModel model = (ComboBoxModel)cbCategory.getModel();
      model.addElement(foodCategory);
      cbCategory.setSelectedValue(foodCategory, true);
    }
    catch (Exception e2) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, e2);
    }
  }
  
  private void doAddNewGroup() {
    try {
      MenuGroup group = new MenuGroup();
      MenuCategory menuCategory = (MenuCategory)cbCategory.getSelectedValue();
      if (menuCategory != null) {
        group.setMenuCategoryId(menuCategory.getId());
      }
      else {
        group.setMenuCategoryId(null);
      }
      MenuGroupForm editor = new MenuGroupForm(group);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled())
        return;
      MenuGroup foodGroup = (MenuGroup)editor.getBean();
      
      ComboBoxModel model = (ComboBoxModel)cbGroup.getModel();
      model.addElement(foodGroup);
      cbCategory.setSelectedValue(foodGroup, true);
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doSelectPageItems() {
    MenuPage menuPage = (MenuPage)listMenuPages.getSelectedValue();
    if (menuPage == null) {
      POSMessageDialog.showMessage("Select page first.");
      return;
    }
    List<MenuItem> menuItems = new ArrayList();
    Map<String, MenuPageItem> itemMap = new HashMap();
    if (menuPage.getPageItems() != null) {
      for (MenuPageItem item : menuPage.getPageItems())
        if (item.getMenuItem() != null)
        {
          itemMap.put(item.getMenuItem().getId(), item);
          menuItems.add(item.getMenuItem());
        }
    }
    Object selectedItems = getSelectedMenuItems(menuItems);
    if (selectedItems == null) {
      return;
    }
    if (menuPage.getPageItems() != null) {
      menuPage.getPageItems().clear();
    }
    List<MenuPageItem> newItems = new ArrayList();
    for (Iterator iterator = ((List)selectedItems).iterator(); iterator.hasNext();) {
      MenuItem menuItem = (MenuItem)iterator.next();
      MenuPageItem pageItem = (MenuPageItem)itemMap.get(menuItem.getId());
      if (pageItem == null) {
        pageItem = new MenuPageItem();
        pageItem.setMenuItem(menuItem);
        pageItem.setMenuPage(menuPage);
        newItems.add(pageItem);
      }
      else {
        menuPage.addTopageItems(pageItem);
      }
    }
    btnSave.setEnabled(true);
    btnCancel.setEnabled(true);
    itemView.fillSelectedPageItems(newItems);
  }
  
  private void doGenenateMenuPages() {
    MenuGroup menuGroup = (MenuGroup)cbGroup.getSelectedValue();
    if (menuGroup == null) {
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), "Please select group first.");
      return;
    }
    List<MenuItem> items = MenuItemDAO.getInstance().findByParent(null, menuGroup, true);
    if ((items == null) || (items.isEmpty())) {
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), "Selected group has no item.");
      return;
    }
    




    MenuPageForm editor = new MenuPageForm(new MenuPage(), true);
    BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
    dialog.open();
    if (dialog.isCanceled()) {
      return;
    }
    MenuPage menuPageWithInfo = (MenuPage)editor.getBean();
    
    int rows = menuPageWithInfo.getRows().intValue();
    int cols = menuPageWithInfo.getCols().intValue();
    
    int totalItemSize = items.size();
    int devideCount = (int)Math.ceil(totalItemSize / (cols * rows));
    int loop = devideCount == 0 ? 1 : devideCount;
    
    List<MenuPageItem> pageItems = new ArrayList();
    int index = 0;
    int count = 0;
    for (int i = 0; i < loop; i++) {
      try {
        int size = menuPageListModel.getSize();
        MenuPage menuPage = null;
        if (menuPage == null) {
          menuPage = new MenuPage();
          menuPage.setMenuGroupId(menuGroup.getId());
          menuPage.setName("Page " + (size + 1));
          menuPage.setRows(Integer.valueOf(rows));
          menuPage.setCols(Integer.valueOf(cols));
          menuPage.setSortOrder(Integer.valueOf(i));
          menuPage.setFlixibleButtonSize(menuPageWithInfo.isFlixibleButtonSize());
          menuPage.setButtonHeight(menuPageWithInfo.getButtonHeight());
          menuPage.setButtonWidth(menuPageWithInfo.getButtonWidth());
          MenuPageDAO.getInstance().saveOrUpdate(menuPage);
          menuPageListModel.addElement(menuPage);
        }
        int totalItemPerPage = cols * rows;
        if (totalItemSize < totalItemPerPage) {
          for (; index < items.size(); index++) {
            MenuPageItem pageItem = new MenuPageItem();
            pageItem.setMenuPage(menuPage);
            pageItem.setMenuItem((MenuItem)items.get(index));
            pageItems.add(pageItem);
            menuPage.addTopageItems(pageItem);
          }
        }
        
        for (int sub = 0; sub < totalItemPerPage; index++) {
          MenuPageItem pageItem = new MenuPageItem();
          pageItem.setMenuPage(menuPage);
          pageItem.setMenuItem((MenuItem)items.get(index));
          pageItems.add(pageItem);
          menuPage.addTopageItems(pageItem);sub++;
        }
        




        totalItemSize -= totalItemPerPage;
        
        for (int row = 0; row < rows; row++) {
          for (int col = 0; col < cols; col++) {
            if (count > pageItems.size() - 1) {
              break;
            }
            if (pageItems.get(count) == null) {
              break;
            }
            ((MenuPageItem)pageItems.get(count)).setRow(Integer.valueOf(row));
            ((MenuPageItem)pageItems.get(count)).setCol(Integer.valueOf(col));
            count++;
          }
        }
        MenuPageDAO.getInstance().saveOrUpdate(menuPage);
        listMenuPages.setSelectedValue(menuPage, true);
      } catch (Exception ex) {
        ex.printStackTrace();
        BOMessageDialog.showError(ex.getMessage());
      }
    }
    
    itemView.repaint();
    itemView.revalidate();
  }
  
  private void doAddNewMenuPage() {
    MenuGroup selectedGroup = (MenuGroup)cbGroup.getSelectedValue();
    if (selectedGroup == null) {
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), "Select group first.");
      return;
    }
    try {
      MenuPage page = new MenuPage();
      page.setMenuGroupId(selectedGroup.getId());
      page.setCols(Integer.valueOf(4));
      page.setRows(Integer.valueOf(4));
      int lastNum = getRandomNumber();
      page.setName("Page " + lastNum);
      page.setSortOrder(Integer.valueOf(lastNum));
      MenuPageDAO.getInstance().saveOrUpdate(page);
      menuPageListModel.addElement(page);
      listMenuPages.setSelectedValue(page, true);
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    }
  }
  
  private int getRandomNumber() {
    String in = "";
    if (menuPageListModel.getSize() > 0) {
      MenuPage lastPage = (MenuPage)menuPageListModel.getElementAt(menuPageListModel.getSize() - 1);
      in = lastPage.getName();
    }
    Pattern p = Pattern.compile(".* ([0-9]+)");
    Matcher m = p.matcher(in);
    int nextNumber = menuPageListModel.getSize() + 1;
    if (m.find()) {
      try {
        int previousNumber = Integer.parseInt(m.group(1));
        if (previousNumber + 1 > nextNumber) {
          nextNumber = previousNumber + 1;
        }
      }
      catch (Exception localException) {}
    }
    
    return nextNumber;
  }
  
  private void doEditMenuPage() {
    MenuPage page = (MenuPage)listMenuPages.getSelectedValue();
    if (page == null)
      return;
    MenuPageForm editor = new MenuPageForm(page, false);
    BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
    dialog.open();
    if (dialog.isCanceled()) {
      return;
    }
    MenuPage menuPage = (MenuPage)editor.getBean();
    MenuPageDAO dao = new MenuPageDAO();
    dao.saveOrUpdate(menuPage);
    listMenuPages.setSelectedValue(menuPage, true);
    rendererSelectedPage();
  }
  
  private void doDetachAllItems() {
    MenuPage menuPage = (MenuPage)listMenuPages.getSelectedValue();
    if ((menuPage == null) || (menuPage.getPageItems() == null)) {
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), "No item to detach.");
      return;
    }
    if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), "Are you sure you want to detach all?", "Confirm") != 0) {
      return;
    }
    if (menuPage.getPageItems() != null)
      menuPage.getPageItems().clear();
    MenuPageDAO.getInstance().saveOrUpdate(menuPage);
    itemView.setMenuPage(menuPage);
  }
  
  public void itemSelected(Object item)
  {
    btnSave.setEnabled(true);
    btnCancel.setEnabled(true);
    
    MenuPageItem pageItem = (MenuPageItem)item;
    if (pageItem.getMenuItem() == null) {}
  }
}
