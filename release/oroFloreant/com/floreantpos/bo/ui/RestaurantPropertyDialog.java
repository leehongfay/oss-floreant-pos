package com.floreantpos.bo.ui;

import com.floreantpos.POSConstants;
import com.floreantpos.model.Store;
import com.floreantpos.model.dao.StoreDAO;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import org.jdesktop.layout.GroupLayout;
import org.jdesktop.layout.GroupLayout.ParallelGroup;
import org.jdesktop.layout.GroupLayout.SequentialGroup;





public class RestaurantPropertyDialog
  extends POSDialog
{
  Store store;
  private JButton btnCancel;
  private JButton btnOk;
  private JLabel jLabel1;
  private JLabel jLabel2;
  private JLabel jLabel3;
  private JLabel jLabel4;
  private JLabel jLabel5;
  private JSeparator jSeparator1;
  private JTextField tfAddrLine1;
  private JTextField tfAddrLine2;
  private JTextField tfCapacity;
  private JTextField tfName;
  private JTextField tfTables;
  
  public RestaurantPropertyDialog()
  {
    setTitle(POSConstants.CONFIGURE);
    
    initComponents();
    
    store = StoreDAO.getRestaurant();
    
    tfName.setText(store.getName());
    tfAddrLine1.setText(store.getAddressLine1());
    tfAddrLine2.setText(store.getAddressLine2());
    tfCapacity.setText(String.valueOf(store.getCapacity()));
    tfTables.setText(String.valueOf(store.getTables()));
  }
  






  private void initComponents()
  {
    jLabel1 = new JLabel();
    jLabel2 = new JLabel();
    jLabel3 = new JLabel();
    jLabel4 = new JLabel();
    jLabel5 = new JLabel();
    tfName = new JTextField();
    tfAddrLine1 = new JTextField();
    tfAddrLine2 = new JTextField();
    tfCapacity = new JTextField();
    tfTables = new JTextField();
    btnCancel = new JButton();
    btnOk = new JButton();
    jSeparator1 = new JSeparator();
    
    setDefaultCloseOperation(2);
    
    jLabel1.setText(POSConstants.RESTAURANT_NAME + ":");
    
    jLabel2.setText(POSConstants.ADDRESS_LINE1 + ":");
    
    jLabel3.setText(POSConstants.ADDRESS_LINE2 + ":");
    
    jLabel4.setText(POSConstants.CAPACITY + ":");
    
    jLabel5.setText(POSConstants.TABLES + ":");
    
    btnCancel.setText(POSConstants.CLOSE);
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        RestaurantPropertyDialog.this.doClose(evt);
      }
      
    });
    btnOk.setText(POSConstants.SAVE);
    btnOk.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        RestaurantPropertyDialog.this.doSave(evt);
      }
      
    });
    GroupLayout layout = new GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(1).add(2, layout
    
      .createSequentialGroup().addContainerGap().add(layout
      .createParallelGroup(2).add(jSeparator1, -1, 380, 32767).add(1, layout
      
      .createSequentialGroup()
      .add(layout.createParallelGroup(1).add(jLabel1).add(jLabel2).add(jLabel3).add(jLabel4).add(jLabel5)).addPreferredGap(0)
      .add(layout
      .createParallelGroup(1).add(tfName, -1, 288, 32767)
      .add(tfAddrLine1, -1, 288, 32767).add(tfAddrLine2, -1, 288, 32767)
      .add(layout
      .createParallelGroup(2, false).add(1, tfTables).add(1, tfCapacity, -1, 110, 32767))))
      
      .add(layout.createSequentialGroup().add(btnOk).addPreferredGap(0).add(btnCancel))).addContainerGap()));
    layout.setVerticalGroup(layout.createParallelGroup(1).add(layout
      .createSequentialGroup().addContainerGap().add(layout
      .createParallelGroup(3).add(jLabel1).add(tfName, -2, -1, -2))
      .addPreferredGap(0).add(layout
      .createParallelGroup(3).add(jLabel2).add(tfAddrLine1, -2, -1, -2))
      .addPreferredGap(0).add(layout
      .createParallelGroup(3).add(jLabel3).add(tfAddrLine2, -2, -1, -2))
      .addPreferredGap(0).add(layout
      .createParallelGroup(3).add(jLabel4).add(tfCapacity, -2, -1, -2))
      .addPreferredGap(0).add(layout
      .createParallelGroup(3).add(jLabel5).add(tfTables, -2, -1, -2))
      .addPreferredGap(0, 115, 32767)
      .add(jSeparator1, -2, 10, -2).addPreferredGap(0)
      .add(layout.createParallelGroup(3).add(btnCancel).add(btnOk)).addContainerGap()));
    
    pack();
  }
  
  private void doClose(ActionEvent evt) {
    setCanceled(true);
    dispose();
  }
  
  private void doSave(ActionEvent evt) {
    try {
      String name = null;
      String addr1 = null;
      String addr2 = null;
      int capacity = 299;
      int tables = 74;
      
      name = tfName.getText();
      addr1 = tfAddrLine1.getText();
      addr2 = tfAddrLine2.getText();
      try
      {
        capacity = Integer.parseInt(tfCapacity.getText());
      } catch (Exception e) {
        POSMessageDialog.showError(this, POSConstants.CAPACITY_IS_NOT_VALID_);
        return;
      }
      try
      {
        tables = Integer.parseInt(tfTables.getText());
      } catch (Exception e) {
        POSMessageDialog.showError(this, POSConstants.NUMBER_OF_TABLES_IS_NOT_VALID);
        return;
      }
      
      store.setName(name);
      store.setAddressLine1(addr1);
      store.setAddressLine2(addr2);
      store.setCapacity(Integer.valueOf(capacity));
      store.setTables(Integer.valueOf(tables));
      
      StoreDAO.getInstance().saveOrUpdate(store);
    } catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
}
