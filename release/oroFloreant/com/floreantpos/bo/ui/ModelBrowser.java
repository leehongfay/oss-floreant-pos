package com.floreantpos.bo.ui;

import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.SearchPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.swingx.JXTable;




















public class ModelBrowser<E>
  extends JPanel
  implements ActionListener, ListSelectionListener
{
  protected JXTable browserTable;
  protected BeanEditor<E> beanEditor;
  protected SearchPanel<E> searchPanel;
  protected JPanel buttonPanel;
  protected JPanel browserPanel = new JPanel(new BorderLayout());
  protected JPanel beanPanel = new JPanel(new BorderLayout());
  
  protected JButton btnNew = new JButton(Messages.getString("ModelBrowser.0"));
  protected JButton btnEdit = new JButton(Messages.getString("ModelBrowser.1"));
  protected JButton btnSave = new JButton(Messages.getString("ModelBrowser.2"));
  protected JButton btnDelete = new JButton(Messages.getString("ModelBrowser.3"));
  protected JButton btnCancel = new JButton(Messages.getString("ModelBrowser.4"));
  private String layoutConstraints;
  private String tableBrowserConstriant;
  private String editorConstriant;
  
  public ModelBrowser() {
    this(null);
  }
  
  public ModelBrowser(BeanEditor<E> beanEditor) {
    this(beanEditor, "", "", "");
  }
  
  public ModelBrowser(BeanEditor<E> beanEditor, String layoutConstraints, String tableConstriant, String editorConstriant)
  {
    this.beanEditor = beanEditor;
    this.layoutConstraints = layoutConstraints;
    tableBrowserConstriant = tableConstriant;
    this.editorConstriant = editorConstriant;
  }
  

  public ModelBrowser(BeanEditor<E> beanEditor, SearchPanel<E> searchPanel)
  {
    this.beanEditor = beanEditor;
    this.searchPanel = searchPanel;
  }
  
  public void init(TableModel tableModel) {
    browserTable = new JXTable();
    browserTable.setRowHeight(PosUIManager.getSize(30));
    browserTable.getSelectionModel().setSelectionMode(0);
    browserTable.getSelectionModel().addListSelectionListener(this);
    browserTable.setDefaultRenderer(Date.class, new CustomCellRenderer());
    
    if (tableModel != null) {
      browserTable.setModel(tableModel);
    }
    
    if (searchPanel != null) {
      searchPanel.setModelBrowser(this);
      browserPanel.add(searchPanel, "North");
    }
    browserPanel.add(new JScrollPane(browserTable));
    beanPanel.setBorder(BorderFactory.createEtchedBorder());
    
    JPanel beanEditorPanel = new JPanel();
    
    if (StringUtils.isNotEmpty(layoutConstraints)) {
      beanEditorPanel.setLayout(new MigLayout("fill,hidemode 3"));
      beanEditorPanel.add(beanEditor, "grow,span");
      setLayout(new MigLayout("fill,hidemode 3", layoutConstraints));
      add(browserPanel, tableBrowserConstriant);
      add(beanPanel, editorConstriant);
    }
    else {
      beanEditorPanel.setLayout(new MigLayout());
      beanEditorPanel.add(beanEditor);
      setLayout(new BorderLayout(10, 10));
      beanPanel.setPreferredSize(PosUIManager.getSize(700, 400));
      add(browserPanel);
      add(beanPanel, "East");
    }
    beanPanel.add(beanEditorPanel);
    
    buttonPanel = new JPanel();
    
    JButton additionalButton = getAdditionalButton();
    if (additionalButton != null) {
      buttonPanel.add(additionalButton);
      additionalButton.addActionListener(this);
    }
    
    buttonPanel.add(btnNew);
    buttonPanel.add(btnEdit);
    buttonPanel.add(btnSave);
    buttonPanel.add(btnDelete);
    buttonPanel.add(btnCancel);
    
    beanPanel.add(buttonPanel, "South");
    
    setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    btnNew.addActionListener(this);
    btnEdit.addActionListener(this);
    btnDelete.addActionListener(this);
    btnSave.addActionListener(this);
    btnCancel.addActionListener(this);
    
    btnNew.setEnabled(true);
    btnEdit.setEnabled(false);
    btnSave.setEnabled(false);
    btnDelete.setEnabled(false);
    btnCancel.setEnabled(false);
    
    beanEditor.clearFields();
    beanEditor.setFieldsEnable(false);
    
    refreshTable();
  }
  
  public void refreshTable() {}
  
  protected JButton getAdditionalButton()
  {
    return null;
  }
  


  protected void handleAdditionaButtonActionIfApplicable(ActionEvent e) {}
  

  public void actionPerformed(ActionEvent e)
  {
    Command command = Command.fromString(e.getActionCommand());
    try
    {
      switch (1.$SwitchMap$com$floreantpos$bo$ui$Command[command.ordinal()]) {
      case 1: 
        beanEditor.createNew();
        beanEditor.setFieldsEnable(true);
        btnNew.setEnabled(false);
        btnEdit.setEnabled(false);
        btnSave.setEnabled(true);
        btnDelete.setEnabled(false);
        btnCancel.setEnabled(true);
        browserTable.clearSelection();
        break;
      
      case 2: 
        beanEditor.edit();
        beanEditor.setFieldsEnable(true);
        btnNew.setEnabled(false);
        btnEdit.setEnabled(false);
        btnSave.setEnabled(true);
        btnDelete.setEnabled(false);
        btnCancel.setEnabled(true);
        break;
      
      case 3: 
        doCancelEditing();
        break;
      
      case 4: 
        if (beanEditor.save()) {
          beanEditor.setFieldsEnable(false);
          btnNew.setEnabled(true);
          btnEdit.setEnabled(false);
          btnSave.setEnabled(false);
          btnDelete.setEnabled(false);
          btnCancel.setEnabled(false);
          refreshTable();
        }
        
        break;
      case 5: 
        if (beanEditor.delete()) {
          beanEditor.setBean(null);
          beanEditor.setFieldsEnable(false);
          btnNew.setEnabled(true);
          btnEdit.setEnabled(false);
          btnSave.setEnabled(false);
          btnDelete.setEnabled(false);
          btnCancel.setEnabled(false);
          refreshTable();
        }
        

        break;
      }
      
      
      handleAdditionaButtonActionIfApplicable(e);
    }
    catch (Exception e2) {
      POSMessageDialog.showError(Application.getPosWindow(), e2.getMessage(), e2);
    }
  }
  
  public void doCancelEditing()
  {
    if (browserTable.getSelectedRow() != -1) {
      beanEditor.setFieldsEnable(false);
      btnNew.setEnabled(true);
      btnEdit.setEnabled(true);
      btnSave.setEnabled(false);
      btnDelete.setEnabled(true);
      btnCancel.setEnabled(false);
      beanEditor.cancel();
    }
    else {
      beanEditor.cancel();
      beanEditor.clearFields();
      beanEditor.setBean(null);
      beanEditor.setFieldsEnable(false);
      btnNew.setEnabled(true);
      btnEdit.setEnabled(false);
      btnSave.setEnabled(false);
      btnDelete.setEnabled(false);
      btnCancel.setEnabled(false);
    }
  }
  
  public void valueChanged(ListSelectionEvent e)
  {
    if (e.getValueIsAdjusting()) {
      return;
    }
    
    BeanTableModel<E> model = (BeanTableModel)browserTable.getModel();
    int selectedRow = browserTable.getSelectedRow();
    if (selectedRow < 0) {
      return;
    }
    selectedRow = browserTable.convertRowIndexToModel(selectedRow);
    
    if (selectedRow < 0) {
      return;
    }
    E data = model.getRow(selectedRow);
    beanEditor.setBean(data);
    btnNew.setEnabled(true);
    btnEdit.setEnabled(true);
    btnSave.setEnabled(false);
    btnDelete.setEnabled(true);
    btnCancel.setEnabled(false);
    beanEditor.setFieldsEnable(false);
  }
  
  public void setModels(List<E> models)
  {
    BeanTableModel<E> tableModel = (BeanTableModel)browserTable.getModel();
    tableModel.removeAll();
    tableModel.addRows(models);
  }
  
  public BeanEditor<E> getBeanEditor()
  {
    return beanEditor;
  }
  
  public SearchPanel<E> getSearchPanel() {
    return searchPanel;
  }
  
  public void refreshButtonPanel() {
    beanEditor.clearFields();
    btnNew.setEnabled(true);
    btnEdit.setEnabled(false);
    btnSave.setEnabled(false);
    btnDelete.setEnabled(false);
    btnCancel.setEnabled(false);
  }
}
