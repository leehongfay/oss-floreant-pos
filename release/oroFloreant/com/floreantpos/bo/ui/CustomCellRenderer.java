package com.floreantpos.bo.ui;

import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.BeanTableModel.BeanColumn;
import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;


















public class CustomCellRenderer
  extends DefaultTableCellRenderer
{
  private Border unselectedBorder = null;
  private Border selectedBorder = null;
  


  public CustomCellRenderer() {}
  

  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    TableModel tableModel = table.getModel();
    if ((tableModel instanceof BeanTableModel)) {
      BeanTableModel model = (BeanTableModel)tableModel;
      BeanTableModel.BeanColumn beanColumn = model.getColumn(column);
      setHorizontalAlignment(beanColumn.getHorizontalAlignment());
    }
    

    setHorizontalAlignment(10);
    if (selectedBorder == null) {
      selectedBorder = BorderFactory.createMatteBorder(5, 5, 5, 5, table.getSelectionBackground());
    }
    if (unselectedBorder == null) {
      unselectedBorder = BorderFactory.createMatteBorder(5, 5, 5, 5, table.getBackground());
    }
    
    if ((value instanceof byte[]))
    {
      byte[] imageData = (byte[])value;
      ImageIcon image = new ImageIcon(imageData);
      image = new ImageIcon(image.getImage().getScaledInstance(100, 100, 4));
      if (imageData != null) {
        table.setRowHeight(row, 120);
      }
      
      JLabel l = new JLabel(image);
      if (isSelected) {
        l.setBorder(selectedBorder);
      }
      else {
        l.setBorder(unselectedBorder);
      }
      return l;
    }
    
    if ((value instanceof Color)) {
      JLabel l = new JLabel();
      
      Color newColor = (Color)value;
      l.setOpaque(true);
      l.setBackground(newColor);
      if (isSelected) {
        l.setBorder(selectedBorder);
      }
      else {
        l.setBorder(unselectedBorder);
      }
      return l;
    }
    
    if ((value instanceof Date)) {
      String pattern = "MM/dd hh:mm a";
      SimpleDateFormat format = new SimpleDateFormat(pattern);
      String valueStr = format.format((Date)value);
      return super.getTableCellRendererComponent(table, valueStr, isSelected, hasFocus, row, column);
    }
    if ((value instanceof Number)) {
      setHorizontalAlignment(11);
    }
    
    if ((value instanceof String)) {
      value = "<html>" + value + "</html>";
    }
    
    return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
  }
}
