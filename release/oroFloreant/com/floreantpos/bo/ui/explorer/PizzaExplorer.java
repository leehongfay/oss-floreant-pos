package com.floreantpos.bo.ui.explorer;

import com.floreantpos.swing.TransparentPanel;
import java.awt.BorderLayout;
import java.awt.FontMetrics;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

public class PizzaExplorer extends TransparentPanel
{
  private JTabbedPane mainTab;
  
  public PizzaExplorer()
  {
    initComponents();
  }
  
  private void initComponents() {
    mainTab = new JTabbedPane(1, 1);
    setLayout(new BorderLayout());
    mainTab.setUI(new BasicTabbedPaneUI()
    {
      protected int calculateTabHeight(int tabPlacement, int tabIndex, int fontHeight) {
        return 30;
      }
      
      protected int calculateTabWidth(int tabPlacement, int tabIndex, FontMetrics metrics)
      {
        return 125;
      }
      

    });
    final JComponent pizzaItemPanel = new JPanel(new BorderLayout());
    mainTab.addTab("Item", pizzaItemPanel);
    pizzaItemPanel.add(new PizzaItemExplorer());
    final JComponent modifierGroupExplorerPanel = new JPanel(new BorderLayout());
    mainTab.addTab("Modifier Group", modifierGroupExplorerPanel);
    final JComponent modifierExplorerPanel = new JPanel(new BorderLayout());
    mainTab.addTab("Modifier", modifierExplorerPanel);
    final JComponent sizeExplorerPanel = new JPanel(new BorderLayout());
    mainTab.addTab("Size", sizeExplorerPanel);
    final JComponent crustExplorerPanel = new JPanel(new BorderLayout());
    mainTab.addTab("Crust", crustExplorerPanel);
    
    add(mainTab);
    mainTab.addChangeListener(new ChangeListener()
    {
      public void stateChanged(ChangeEvent evt) {
        JTabbedPane mainTab = (JTabbedPane)evt.getSource();
        
        int selectedTabIndex = mainTab.getSelectedIndex();
        if (selectedTabIndex == 0) {
          if (pizzaItemPanel.getComponentCount() == 0) {
            pizzaItemPanel.add(new PizzaItemExplorer());
          }
        }
        else if (selectedTabIndex == 1) {
          if (modifierGroupExplorerPanel.getComponentCount() == 0) {
            modifierGroupExplorerPanel.add(new PizzaModifierGroupExplorer());
          }
        } else if (selectedTabIndex == 2) {
          if (modifierExplorerPanel.getComponentCount() == 0) {
            modifierExplorerPanel.add(new PizzaModifierExplorer());
          }
        } else if (selectedTabIndex == 3) {
          if (sizeExplorerPanel.getComponentCount() == 0) {
            sizeExplorerPanel.add(new MenuItemSizeExplorer());
          }
        } else if ((selectedTabIndex == 4) && 
          (crustExplorerPanel.getComponentCount() == 0)) {
          crustExplorerPanel.add(new PizzaCrustExplorer());
        }
      }
    });
  }
}
