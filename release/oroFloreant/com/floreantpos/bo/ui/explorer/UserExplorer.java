package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.swing.PaginationSupport;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.forms.UserForm;
import com.floreantpos.ui.model.UserRoleForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.UIResource;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.TreePath;
import net.miginfocom.swing.MigLayout;
import org.hibernate.exception.ConstraintViolationException;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;
import org.jdesktop.swingx.treetable.TreeTableNode;

















public class UserExplorer
  extends TransparentPanel
{
  private JXTreeTable treeTable;
  private UserTreeTableModel treeTableModel;
  private JButton btnAddRole;
  private JButton btnCopy;
  private JButton btnBack;
  private JButton btnForward;
  private JLabel lblNumberOfItem;
  private JTextField txtSearchField;
  
  public UserExplorer()
  {
    treeTable = new JXTreeTable();
    treeTable.setRootVisible(false);
    treeTable.setShowGrid(true, true);
    treeTable.setRowHeight(PosUIManager.getSize(30));
    treeTable.setLeafIcon(null);
    treeTable.setOpenIcon(null);
    treeTable.setClosedIcon(null);
    
    treeTable.setDefaultRenderer(Object.class, new PosTableRenderer());
    
    btnBack = new JButton("<<< Previous");
    btnForward = new JButton("Next >>>");
    lblNumberOfItem = new JLabel();
    
    treeTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        int index = treeTable.getSelectedRow();
        if (index < 0)
          return;
        TreePath treePath = treeTable.getPathForRow(index);
        DefaultMutableTreeTableNode lastPathComponent = (DefaultMutableTreeTableNode)treePath.getLastPathComponent();
        User user = (User)lastPathComponent.getUserObject();
        if (user == null) {
          return;
        }
        btnAddRole.setEnabled(user.isRoot().booleanValue());
        btnCopy.setEnabled(user.isRoot().booleanValue());
      }
    });
    setLayout(new BorderLayout(5, 5));
    add(createSearchPanel(), "North");
    buildUserTree();
    treeTable.expandAll();
    treeTable.collapseAll();
    
    treeTable.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        int col = treeTable.columnAtPoint(me.getPoint());
        if ((me.getClickCount() == 2) && (col == 0)) {
          treeTable.expandPath(treeTable.getPathForRow(treeTable.getSelectedRow()));
        }
        else if ((me.getClickCount() == 2) && (col != 0)) {
          UserExplorer.this.editSelectedRow();
        }
        
      }
    });
    add(new JScrollPane(treeTable), "Center");
    
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          UserForm editor = new UserForm();
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          if (dialog.isCanceled())
            return;
          User newUser = (User)editor.getBean();
          MutableTreeTableNode root = (MutableTreeTableNode)treeTableModel.getRoot();
          treeTableModel.insertNodeInto(new DefaultMutableTreeTableNode(newUser), root, root.getChildCount());
        } catch (Exception x) {
          PosLog.error(getClass(), x);
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    btnCopy = new JButton(POSConstants.COPY);
    btnCopy.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = treeTable.getSelectedRow();
          if (index < 0)
            return;
          TreePath treePath = treeTable.getPathForRow(index);
          DefaultMutableTreeTableNode lastPathComponent = (DefaultMutableTreeTableNode)treePath.getLastPathComponent();
          User user = (User)lastPathComponent.getUserObject();
          
          if (!user.isRoot().booleanValue()) {
            return;
          }
          User user2 = new User();
          user2.setId(user.getId());
          user2.setType(user.getType());
          user2.setFirstName(user.getFirstName());
          user2.setLastName(user.getLastName());
          user2.setPassword(user.getPassword());
          user2.setSsn(user.getSsn());
          
          UserForm editor = new UserForm();
          editor.setEditMode(false);
          editor.setBean(user2);
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          if (dialog.isCanceled()) {
            return;
          }
          User newUser = (User)editor.getBean();
          MutableTreeTableNode root = (MutableTreeTableNode)treeTableModel.getRoot();
          treeTableModel.insertNodeInto(new DefaultMutableTreeTableNode(newUser), root, root.getChildCount());
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        UserExplorer.this.editSelectedRow();
      }
      
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        int index = treeTable.getSelectedRow();
        if (index < 0)
          return;
        TreePath treePath = treeTable.getPathForRow(index);
        DefaultMutableTreeTableNode lastPathComponent = (DefaultMutableTreeTableNode)treePath.getLastPathComponent();
        User user = (User)lastPathComponent.getUserObject();
        if (user == null) {
          return;
        }
        try {
          if (ConfirmDeleteDialog.showMessage(UserExplorer.this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0) {
            UserDAO.getInstance().delete(user);
            MutableTreeTableNode tableNode = UserExplorer.this.findTreeNodeForUser((MutableTreeTableNode)treeTableModel.getRoot(), user.getId());
            if (tableNode.getParent() != null) {
              treeTableModel.removeNodeFromParent(tableNode);
            }
          }
        }
        catch (ConstraintViolationException x) {
          String message = POSConstants.USER + " " + user.getFirstName() + " " + user.getLastName() + " (" + user.getType() + ") " + POSConstants.ERROR_MESSAGE;
          BOMessageDialog.showError(message, x);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    btnAddRole = new JButton("Add Role");
    btnAddRole.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        UserExplorer.this.addNewRole();
      }
      
    });
    btnBack.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        treeTableModel.setCurrentRowIndex(treeTableModel.getPreviousRowIndex());
        UserExplorer.this.buildUserTree();
        treeTable.expandAll();
        treeTable.collapseAll();
      }
      
    });
    btnForward.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        treeTableModel.setCurrentRowIndex(treeTableModel.getNextRowIndex());
        UserExplorer.this.buildUserTree();
        treeTable.expandAll();
        treeTable.collapseAll();
      }
      
    });
    JPanel bottomPanel = new JPanel(new MigLayout("fill", "[right][]"));
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton, "split 5");
    panel.add(btnCopy);
    panel.add(editButton);
    panel.add(deleteButton);
    panel.add(btnAddRole);
    bottomPanel.add(panel, "");
    
    JPanel navigationPanel = new JPanel(new FlowLayout(4));
    navigationPanel.add(lblNumberOfItem);
    navigationPanel.add(btnBack);
    navigationPanel.add(btnForward);
    bottomPanel.add(navigationPanel, "grow");
    
    add(bottomPanel, "South");
  }
  
  private JPanel createSearchPanel() {
    JPanel searchPanel = new JPanel(new MigLayout());
    JLabel lblFind = new JLabel("Id / Name:");
    txtSearchField = new JTextField(20);
    JButton btnSearch = new JButton("Search");
    JButton btnClear = new JButton("Clear");
    
    txtSearchField.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        UserExplorer.this.doSearchUser();
      }
    });
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        UserExplorer.this.doSearchUser();
      }
    });
    btnClear.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        txtSearchField.setText("");
      }
      
    });
    searchPanel.add(lblFind);
    searchPanel.add(txtSearchField, "grow");
    searchPanel.add(btnSearch);
    searchPanel.add(btnClear);
    return searchPanel;
  }
  
  private MutableTreeTableNode findTreeNodeForUser(MutableTreeTableNode userNode, String id) {
    User user = (User)userNode.getUserObject();
    if (id.equals(user.getId())) {
      return userNode;
    }
    
    Enumeration<? extends TreeTableNode> children = userNode.children();
    while (children.hasMoreElements()) {
      MutableTreeTableNode treeTableNode = (MutableTreeTableNode)children.nextElement();
      MutableTreeTableNode findUserById = findTreeNodeForUser(treeTableNode, id);
      if (findUserById != null) {
        return findUserById;
      }
    }
    return null;
  }
  
  private void buildUserTree() {
    User dummy = new User();
    dummy.setId("0");
    dummy.setFirstName("Root");
    DefaultMutableTreeTableNode root = new DefaultMutableTreeTableNode(dummy);
    root.setUserObject(dummy);
    
    if (treeTableModel == null) {
      treeTableModel = new UserTreeTableModel(root);
      treeTable.setTreeTableModel(treeTableModel);
    }
    else {
      treeTableModel.setRoot(root);
    }
    String strFilter = txtSearchField.getText();
    treeTableModel.setNumRows(UserDAO.getInstance().rowCount(strFilter));
    List<User> userList = UserDAO.getInstance().loadUsers(treeTableModel, strFilter);
    for (User user : userList) {
      DefaultMutableTreeTableNode userNode = new DefaultMutableTreeTableNode(user);
      List<User> linkedUserList = user.getLinkedUser();
      for (User linkedUser : linkedUserList)
        if (!linkedUser.isRoot().booleanValue())
        {
          DefaultMutableTreeTableNode node = new DefaultMutableTreeTableNode(linkedUser);
          userNode.add(node);
        }
      root.add(userNode);
    }
    
    int startNumber = treeTableModel.getCurrentRowIndex() + 1;
    int endNumber = treeTableModel.getNextRowIndex();
    int totalNumber = treeTableModel.getNumRows();
    if (endNumber > totalNumber) {
      endNumber = totalNumber;
    }
    
    lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
    btnBack.setEnabled(treeTableModel.hasPrevious());
    btnForward.setEnabled(treeTableModel.hasNext());
  }
  
  private void editSelectedRow() {
    try {
      int index = treeTable.getSelectedRow();
      if (index < 0)
        return;
      TreePath treePath = treeTable.getPathForRow(index);
      DefaultMutableTreeTableNode lastPathComponent = (DefaultMutableTreeTableNode)treePath.getLastPathComponent();
      User user = (User)lastPathComponent.getUserObject();
      
      if (!user.isRoot().booleanValue()) {
        UserRoleForm editor = new UserRoleForm(user);
        BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
        dialog.open();
        if (dialog.isCanceled()) {
          return;
        }
      } else {
        UserForm editor = new UserForm();
        editor.setEditMode(true);
        editor.setBean(user);
        BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
        dialog.open();
        if (dialog.isCanceled())
          return;
      }
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void addNewRole() {
    try {
      int index = treeTable.getSelectedRow();
      if (index < 0)
        return;
      TreePath treePath = treeTable.getPathForRow(index);
      DefaultMutableTreeTableNode lastPathComponent = (DefaultMutableTreeTableNode)treePath.getLastPathComponent();
      User user = (User)lastPathComponent.getUserObject();
      
      User roleUser = new User();
      roleUser.setParentUser(user);
      
      UserRoleForm editor = new UserRoleForm(roleUser);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      User parentUser = roleUser.getParentUser();
      MutableTreeTableNode root = (MutableTreeTableNode)treeTableModel.getRoot();
      if (parentUser != null) {
        MutableTreeTableNode parentNode = findTreeNodeForUser(root, parentUser.getId());
        if (parentNode != null) {
          treeTableModel.insertNodeInto(new DefaultMutableTreeTableNode(roleUser), parentNode, parentNode.getChildCount());
        }
      }
      else {
        MutableTreeTableNode parentNode = findTreeNodeForUser(root, "0");
        if (parentNode != null) {
          treeTableModel.insertNodeInto(new DefaultMutableTreeTableNode(roleUser), parentNode, parentNode.getChildCount());
        }
      }
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doSearchUser() {
    treeTableModel.setCurrentRowIndex(0);
    buildUserTree();
    treeTable.expandAll();
    treeTable.collapseAll();
  }
  
  class UserTreeTableModel extends DefaultTreeTableModel implements PaginationSupport {
    private final String[] COLUMN_NAMES = { POSConstants.ID, "Image", POSConstants.FIRST_NAME, POSConstants.LAST_NAME, POSConstants.TYPE, "Active", "Has Staff Bank", "Auto Start Staff Bank", "Can receive tips", "Driver" };
    
    private int numRows;
    
    private int currentRowIndex;
    
    private int pageSize = 50;
    

    public UserTreeTableModel(DefaultMutableTreeTableNode rootLocation)
    {
      super();
    }
    
    public void setRoot(TreeTableNode root)
    {
      super.setRoot(root);
    }
    
    public int getColumnCount()
    {
      return COLUMN_NAMES.length;
    }
    
    public String getColumnName(int column)
    {
      return COLUMN_NAMES[column];
    }
    
    public boolean isCellEditable(Object node, int column)
    {
      return false;
    }
    
    public Object getValueAt(Object node, int column)
    {
      if ((node instanceof DefaultMutableTreeTableNode)) {
        User user = (User)((DefaultMutableTreeTableNode)node).getUserObject();
        if (user == null) {
          return "";
        }
        boolean linkedUser = !user.isRoot().booleanValue();
        switch (column)
        {
        case 0: 
          return linkedUser ? "" : String.valueOf(user.getId());
        
        case 1: 
          return linkedUser ? "" : user.getImage();
        
        case 2: 
          return linkedUser ? "" : user.getFirstName();
        
        case 3: 
          return linkedUser ? "" : user.getLastName();
        
        case 4: 
          return user.getType();
        
        case 5: 
          return user.isActive();
        
        case 6: 
          return user.isStaffBank();
        
        case 7: 
          return user.isAutoStartStaffBank();
        
        case 8: 
          return user.isAllowReceiveTips();
        
        case 9: 
          return user.isDriver();
        }
        
      }
      return null;
    }
    
    public int getNumRows() {
      return numRows;
    }
    
    public void setNumRows(int numRows) {
      this.numRows = numRows;
    }
    
    public int getCurrentRowIndex() {
      return currentRowIndex;
    }
    
    public void setCurrentRowIndex(int currentRowIndex) {
      this.currentRowIndex = currentRowIndex;
    }
    
    public int getPageSize() {
      return pageSize;
    }
    
    public void setPageSize(int pageSize) {
      this.pageSize = pageSize;
    }
    
    public boolean hasNext() {
      return currentRowIndex + pageSize < numRows;
    }
    
    public boolean hasPrevious() {
      return currentRowIndex > 0;
    }
    
    public int getNextRowIndex() {
      if (numRows == 0) {
        return 0;
      }
      
      return getCurrentRowIndex() + getPageSize();
    }
    
    public int getPreviousRowIndex() {
      int i = getCurrentRowIndex() - getPageSize();
      if (i < 0) {
        i = 0;
      }
      
      return i;
    }
    
    public void setRows(List rows) {}
  }
  
  class BooleanRenderer
    extends JCheckBox
    implements TableCellRenderer, UIResource
  {
    private final Border noFocusBorder = new EmptyBorder(1, 1, 1, 1);
    
    public BooleanRenderer()
    {
      setHorizontalAlignment(0);
      setBorderPainted(true);
    }
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
      if (isSelected) {
        setForeground(table.getSelectionForeground());
        super.setBackground(table.getSelectionBackground());
      }
      else {
        setForeground(table.getForeground());
        setBackground(table.getBackground());
      }
      setSelected((value != null) && (((Boolean)value).booleanValue()));
      
      if (hasFocus) {
        setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
      }
      else {
        setBorder(noFocusBorder);
      }
      
      return this;
    }
  }
}
