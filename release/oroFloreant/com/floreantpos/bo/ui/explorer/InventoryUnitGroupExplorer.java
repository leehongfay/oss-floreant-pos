package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.InventoryStock;
import com.floreantpos.model.InventoryUnitGroup;
import com.floreantpos.model.dao.InventoryUnitGroupDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SortOrder;
import org.jdesktop.swingx.JXTable;






public class InventoryUnitGroupExplorer
  extends TransparentPanel
{
  private static final long serialVersionUID = 1L;
  private JXTable table;
  private BeanTableModel<InventoryUnitGroup> tableModel;
  private JTextField tfName;
  private JComboBox cbLocation;
  
  public InventoryUnitGroupExplorer()
  {
    tableModel = new BeanTableModel(InventoryUnitGroup.class);
    tableModel.addColumn("ID", "id");
    tableModel.addColumn("NAME", "name");
    
    tableModel.addRows(InventoryUnitGroupDAO.getInstance().findAll());
    
    table = new JXTable(tableModel);
    table.setSortOrder(0, SortOrder.ASCENDING);
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          InventoryUnitGroupExplorer.this.editSelectedRow();
        }
        
      }
    });
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    add(createButtonPanel(), "South");
    add(buildSearchForm(), "North");
  }
  
  private TransparentPanel createButtonPanel()
  {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton editButton = explorerButton.getEditButton();
    JButton addButton = explorerButton.getAddButton();
    JButton deleteButton = explorerButton.getDeleteButton();
    
    addButton.addActionListener(new ActionListener()
    {


      public void actionPerformed(ActionEvent e)
      {


        try
        {


          String groupName = JOptionPane.showInputDialog(POSUtil.getFocusedWindow(), "Enter group name");
          if (groupName == null) {
            BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Group name cannot be empty.");
            return;
          }
          if (groupName.length() > 30) {
            BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Group name too long.");
            return;
          }
          
          InventoryUnitGroup inventoryUnitGroup = new InventoryUnitGroup();
          inventoryUnitGroup.setName(groupName);
          
          InventoryUnitGroupDAO inventoryUnitGroupDAO = new InventoryUnitGroupDAO();
          inventoryUnitGroupDAO.saveOrUpdate(inventoryUnitGroup);
          tableModel.addRow(inventoryUnitGroup);
        }
        catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    editButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        InventoryUnitGroupExplorer.this.editSelectedRow();
      }
      
    });
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          
          if (POSMessageDialog.showYesNoQuestionDialog(InventoryUnitGroupExplorer.this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
            return;
          }
          InventoryUnitGroup inventoryUnitGroup = (InventoryUnitGroup)tableModel.getRow(index);
          
          InventoryUnitGroupDAO inventoryUnitGroupDAO = new InventoryUnitGroupDAO();
          inventoryUnitGroupDAO.delete(inventoryUnitGroup);
          
          tableModel.removeRow(index);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    
    return panel;
  }
  
  private JPanel buildSearchForm() {
    JPanel panel = new JPanel();
    



























































    return panel;
  }
  
  private void searchItem()
  {
    String txName = tfName.getText();
    Object selectedGroup = cbLocation.getSelectedItem();
    
    List<InventoryStock> similarItem = null;
  }
  








  private void editSelectedRow()
  {
    try
    {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      InventoryUnitGroup inventoryUnitGroup = (InventoryUnitGroup)tableModel.getRow(index);
      
      tableModel.setRow(index, inventoryUnitGroup);
      
      String groupName = JOptionPane.showInputDialog(POSUtil.getFocusedWindow(), "Group name", inventoryUnitGroup.getName());
      if (groupName == null) {
        BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Group name cannot be empty.");
        return;
      }
      if (groupName.length() > 30) {
        BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Group name too long.");
        return;
      }
      
      InventoryUnitGroupDAO inventoryUnitGroupDAO = new InventoryUnitGroupDAO();
      inventoryUnitGroupDAO.saveOrUpdate(inventoryUnitGroup);
      
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
}
