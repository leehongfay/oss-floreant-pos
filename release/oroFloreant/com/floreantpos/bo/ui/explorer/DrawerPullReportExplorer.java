package com.floreantpos.bo.ui.explorer;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.CashDrawerDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.print.PosPrintService;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.util.UiUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;
















public class DrawerPullReportExplorer
  extends TransparentPanel
{
  private JXDatePicker fromDatePicker = UiUtil.getCurrentMonthStart();
  private JXDatePicker toDatePicker = UiUtil.getCurrentMonthEnd();
  private JButton btnGo = new JButton(POSConstants.GO);
  private JButton btnEditActualAmount = new JButton(POSConstants.EDIT_ACTUAL_AMOUNT);
  private JButton btnPrint = new JButton(Messages.getString("DrawerPullReportExplorer.0"));
  private static SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
  private TableColumnModelExt columnModel;
  private JXTable table;
  
  public DrawerPullReportExplorer() {
    super(new BorderLayout());
    add(new JScrollPane(this.table = new JXTable(new DrawerPullExplorerTableModel(null))));
    table.getSelectionModel().setSelectionMode(0);
    table.setRowHeight(PosUIManager.getSize(30));
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    table.setAutoResizeMode(0);
    table.setColumnControlVisible(true);
    
    resizeColumnWidth(table);
    
    restoreTableColumnsVisibility();
    addTableColumnListener();
    JPanel topPanel = new JPanel(new MigLayout());
    
    topPanel.add(new JLabel(POSConstants.FROM), "grow");
    topPanel.add(fromDatePicker, "wrap");
    topPanel.add(new JLabel(POSConstants.TO), "grow");
    topPanel.add(toDatePicker, "wrap");
    topPanel.add(btnGo, "skip 1, al right");
    add(topPanel, "North");
    
    JPanel bottomPanel = new JPanel(new FlowLayout(1));
    bottomPanel.add(btnEditActualAmount);
    bottomPanel.add(btnPrint);
    add(bottomPanel, "South");
    
    btnPrint.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        int selectedRow = table.getSelectedRow();
        if (selectedRow < 0) {
          BOMessageDialog.showError(DrawerPullReportExplorer.this, Messages.getString("DrawerPullReportExplorer.1"));
          return;
        }
        DrawerPullReportExplorer.DrawerPullExplorerTableModel model = (DrawerPullReportExplorer.DrawerPullExplorerTableModel)table.getModel();
        CashDrawer report = (CashDrawer)model.getRowData(selectedRow);
        
        PosPrintService.printDrawerPullReport(report);
      }
      
    });
    btnGo.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          DrawerPullReportExplorer.this.viewReport();
          resizeColumnWidth(table);
        } catch (Exception e1) {
          BOMessageDialog.showError(DrawerPullReportExplorer.this, POSConstants.ERROR_MESSAGE, e1);
        }
        
      }
    });
    btnEditActualAmount.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          int selectedRow = table.getSelectedRow();
          if (selectedRow < 0) {
            BOMessageDialog.showError(DrawerPullReportExplorer.this, POSConstants.SELECT_DRAWER_PULL_TO_EDIT);
            return;
          }
          
          String amountString = JOptionPane.showInputDialog(DrawerPullReportExplorer.this, POSConstants.ENTER_ACTUAL_AMOUNT + ":");
          if (amountString == null) {
            return;
          }
          double amount = 0.0D;
          try {
            amount = Double.parseDouble(amountString);
          } catch (Exception x) {
            BOMessageDialog.showError(DrawerPullReportExplorer.this, POSConstants.INVALID_AMOUNT);
            return;
          }
          
          DrawerPullReportExplorer.DrawerPullExplorerTableModel model = (DrawerPullReportExplorer.DrawerPullExplorerTableModel)table.getModel();
          CashDrawer report = (CashDrawer)model.getRowData(selectedRow);
          report.setCashToDeposit(Double.valueOf(amount));
          
          CashDrawerDAO dao = new CashDrawerDAO();
          dao.saveOrUpdate(report);
          model.updateItem(selectedRow);
        } catch (Exception e1) {
          BOMessageDialog.showError(DrawerPullReportExplorer.this, POSConstants.ERROR_MESSAGE, e1);
        }
      }
    });
  }
  
  private void restoreTableColumnsVisibility()
  {
    String recordedSelectedColumns = TerminalConfig.getDrawerPullReportHiddenColumns();
    TableColumnModelExt columnModel = (TableColumnModelExt)table.getColumnModel();
    
    if (recordedSelectedColumns.isEmpty()) {
      return;
    }
    String[] str = recordedSelectedColumns.split("\\*");
    for (int i = 0; i < str.length; i++) {
      Integer columnIndex = Integer.valueOf(Integer.parseInt(str[i]));
      columnModel.getColumnExt(columnIndex.intValue() - i).setVisible(false);
    }
  }
  
  private void viewReport() {
    try {
      Date fromDate = fromDatePicker.getDate();
      Date toDate = toDatePicker.getDate();
      
      fromDate = DateUtil.startOfDay(fromDate);
      toDate = DateUtil.endOfDay(toDate);
      
      List<CashDrawer> list = new CashDrawerDAO().findReports(fromDate, toDate);
      DrawerPullExplorerTableModel model = (DrawerPullExplorerTableModel)table.getModel();
      model.setRows(list);
    } catch (Exception e) {
      BOMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  class DrawerPullExplorerTableModel extends ListTableModel {
    String[] columnNames = { POSConstants.ID, POSConstants.TIME, Messages.getString("DrawerPullReportExplorer.2"), POSConstants.DRAWER_PULL_AMOUNT, 
      Messages.getString("DrawerPullReportExplorer.3"), Messages.getString("DrawerPullReportExplorer.4"), Messages.getString("DrawerPullReportExplorer.5"), Messages.getString("DrawerPullReportExplorer.6"), Messages.getString("DrawerPullReportExplorer.7"), Messages.getString("DrawerPullReportExplorer.8"), Messages.getString("DrawerPullReportExplorer.9"), 
      Messages.getString("DrawerPullReportExplorer.10"), Messages.getString("DrawerPullReportExplorer.11"), Messages.getString("DrawerPullReportExplorer.12"), Messages.getString("DrawerPullReportExplorer.13"), Messages.getString("DrawerPullReportExplorer.14"), Messages.getString("DrawerPullReportExplorer.15"), 
      Messages.getString("DrawerPullReportExplorer.16"), Messages.getString("DrawerPullReportExplorer.17"), Messages.getString("DrawerPullReportExplorer.18"), Messages.getString("DrawerPullReportExplorer.19"), Messages.getString("DrawerPullReportExplorer.20"), 
      Messages.getString("DrawerPullReportExplorer.21"), Messages.getString("DrawerPullReportExplorer.22"), Messages.getString("DrawerPullReportExplorer.23"), Messages.getString("DrawerPullReportExplorer.24"), Messages.getString("DrawerPullReportExplorer.25"), Messages.getString("DrawerPullReportExplorer.26"), Messages.getString("DrawerPullReportExplorer.27"), Messages.getString("DrawerPullReportExplorer.28"), 
      Messages.getString("DrawerPullReportExplorer.29"), Messages.getString("DrawerPullReportExplorer.30"), Messages.getString("DrawerPullReportExplorer.31"), POSConstants.ACTUAL_AMOUNT, Messages.getString("DrawerPullReportExplorer.32"), Messages.getString("DrawerPullReportExplorer.33"), 
      Messages.getString("DrawerPullReportExplorer.34"), Messages.getString("DrawerPullReportExplorer.35"), Messages.getString("DrawerPullReportExplorer.36"), Messages.getString("DrawerPullReportExplorer.37"), Messages.getString("DrawerPullReportExplorer.38"), Messages.getString("DrawerPullReportExplorer.39"), 
      Messages.getString("DrawerPullReportExplorer.40"), Messages.getString("DrawerPullReportExplorer.41"), Messages.getString("DrawerPullReportExplorer.42") };
    
    DrawerPullExplorerTableModel() {
      setRows(list);
      setColumnNames(columnNames);
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      CashDrawer report = (CashDrawer)rows.get(rowIndex);
      
      switch (columnIndex)
      {
      case 0: 
        return report.getId().toString();
      
      case 1: 
        return DrawerPullReportExplorer.dateTimeFormatter.format(report.getReportTime());
      
      case 2: 
        return report.getTicketCount();
      
      case 3: 
        return report.getDrawerAccountable();
      
      case 4: 
        return report.getAssignedUser().getId();
      
      case 5: 
        return report.getTerminal().getId();
      
      case 6: 
        return report.getBeginCash();
      
      case 7: 
        return report.getNetSales();
      
      case 8: 
        return report.getSalesTax();
      
      case 9: 
        return report.getCashTax();
      
      case 10: 
        return report.getTotalRevenue();
      
      case 11: 
        return report.getGrossReceipts();
      
      case 12: 
        return report.getGiftCertReturnCount();
      
      case 13: 
        return report.getGiftCertReturnAmount();
      
      case 14: 
        return report.getGiftCertChangeAmount();
      
      case 15: 
        return report.getCashReceiptNumber();
      
      case 16: 
        return report.getCashReceiptAmount();
      
      case 17: 
        return report.getCreditCardReceiptNumber();
      
      case 18: 
        return report.getCreditCardReceiptAmount();
      
      case 19: 
        return report.getDebitCardReceiptNumber();
      
      case 20: 
        return report.getDebitCardReceiptAmount();
      
      case 21: 
        return report.getRefundReceiptCount();
      
      case 22: 
        return report.getRefundAmount();
      
      case 23: 
        return report.getReceiptDifferential();
      
      case 24: 
        return report.getCashBack();
      
      case 25: 
        return report.getCashTips();
      
      case 26: 
        return report.getChargedTips();
      
      case 27: 
        return report.getTipsPaid();
      
      case 28: 
        return report.getTipsDifferential();
      
      case 29: 
        return report.getPayOutNumber();
      
      case 30: 
        return report.getPayOutAmount();
      
      case 31: 
        return report.getDrawerBleedNumber();
      
      case 32: 
        return report.getDrawerBleedAmount();
      
      case 33: 
        return report.getCashToDeposit();
      
      case 34: 
        return report.getVariance();
      
      case 35: 
        return report.getTotalVoidWst();
      
      case 36: 
        return report.getTotalVoid();
      
      case 37: 
        return report.getTotalDiscountCount();
      
      case 38: 
        return report.getTotalDiscountAmount();
      
      case 39: 
        return report.getTotalDiscountSales();
      
      case 40: 
        return report.getTotalDiscountGuest();
      
      case 41: 
        return report.getTotalDiscountPartySize();
      
      case 42: 
        return report.getTotalDiscountCheckSize();
      
      case 43: 
        return report.getTotalDiscountPercentage();
      
      case 44: 
        return report.getTotalDiscountRatio();
      }
      
      return null;
    }
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++)
    {
      int columnWidthByComponent = getColumnWidthByComponentLenght(table, column);
      int columnWidthByHeader = getColumnWidthByHeaderLenght(table, column);
      if (columnWidthByComponent > columnWidthByHeader) {
        columnModel.getColumn(column).setPreferredWidth(columnWidthByComponent);
      }
      else {
        columnModel.getColumn(column).setPreferredWidth(columnWidthByHeader);
      }
    }
  }
  
  private int getColumnWidthByHeaderLenght(JTable table, int column) {
    int width = 50;
    TableColumn tcolumn = table.getColumnModel().getColumn(column);
    TableCellRenderer headerRenderer = tcolumn.getHeaderRenderer();
    
    if (headerRenderer == null) {
      headerRenderer = table.getTableHeader().getDefaultRenderer();
    }
    Object headerValue = tcolumn.getHeaderValue();
    Component headerComp = headerRenderer.getTableCellRendererComponent(table, headerValue, false, false, 0, column);
    
    width = Math.max(width, getPreferredSizewidth);
    
    return width + 20;
  }
  
  private int getColumnWidthByComponentLenght(JTable table, int column) {
    int width = 50;
    for (int row = 0; row < table.getRowCount(); row++) {
      TableCellRenderer renderer = table.getCellRenderer(row, column);
      Component comp = table.prepareRenderer(renderer, row, column);
      width = Math.max(getPreferredSizewidth + 1, width);
    }
    return width + 20;
  }
  
  private void saveHiddenColumns() {
    List<TableColumn> columns = columnModel.getColumns(true);
    List<Integer> indices = new ArrayList();
    for (TableColumn tableColumn : columns) {
      TableColumnExt c = (TableColumnExt)tableColumn;
      if (!c.isVisible()) {
        indices.add(Integer.valueOf(c.getModelIndex()));
      }
    }
    saveTableColumnsVisibility(indices);
  }
  
  private void saveTableColumnsVisibility(List indices) {
    String selectedColumns = "";
    for (Iterator iterator = indices.iterator(); iterator.hasNext();) {
      String newSelectedColumn = String.valueOf(iterator.next());
      selectedColumns = selectedColumns + newSelectedColumn;
      
      if (iterator.hasNext()) {
        selectedColumns = selectedColumns + "*";
      }
    }
    TerminalConfig.setDrawerPullReportHiddenColumns(selectedColumns);
  }
  
  private void addTableColumnListener() {
    columnModel = ((TableColumnModelExt)table.getColumnModel());
    columnModel.addColumnModelListener(new TableColumnModelListener()
    {
      public void columnSelectionChanged(ListSelectionEvent e) {}
      


      public void columnRemoved(TableColumnModelEvent e)
      {
        DrawerPullReportExplorer.this.saveHiddenColumns();
      }
      


      public void columnMoved(TableColumnModelEvent e) {}
      


      public void columnMarginChanged(ChangeEvent e) {}
      

      public void columnAdded(TableColumnModelEvent e)
      {
        DrawerPullReportExplorer.this.saveHiddenColumns();
      }
    });
  }
}
