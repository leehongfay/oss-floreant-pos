package com.floreantpos.bo.ui.explorer;

import com.floreantpos.config.ui.ReceiptConfigurationView;
import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

public class ReceiptConfigurationExplorer
  extends JPanel
{
  private JTabbedPane mainTab;
  
  public ReceiptConfigurationExplorer()
  {
    initComponents();
  }
  
  private void initComponents() {
    mainTab = new JTabbedPane();
    mainTab.setUI(new BasicTabbedPaneUI());
    setLayout(new BorderLayout());
    final JComponent mainReceiptPanel = new JPanel(new BorderLayout());
    mainTab.addTab("Main Receipt", mainReceiptPanel);
    mainReceiptPanel.add(new ReceiptConfigurationView());
    final JComponent kitchenReceiptPanel = new JPanel(new BorderLayout());
    mainTab.addTab("Kitchen Receipt", kitchenReceiptPanel);
    
    add(mainTab);
    mainTab.addChangeListener(new ChangeListener()
    {
      public void stateChanged(ChangeEvent evt) {
        JTabbedPane mainTab = (JTabbedPane)evt.getSource();
        
        int selectedTabIndex = mainTab.getSelectedIndex();
        if (selectedTabIndex == 0) {
          if (mainReceiptPanel.getComponentCount() == 0) {
            mainReceiptPanel.add(new ReceiptConfigurationView());
          }
        }
        else if ((selectedTabIndex == 1) && 
          (kitchenReceiptPanel.getComponentCount() == 0)) {
          kitchenReceiptPanel.add(new ReceiptConfigurationView(true));
        }
        
        JPanel view = (JPanel)mainTab.getComponentAt(selectedTabIndex);
        if (view != null) {
          Component[] components = view.getComponents();
          for (Component component : components) {
            if ((component instanceof ReceiptConfigurationView)) {
              ReceiptConfigurationView receiptConfigurationView = (ReceiptConfigurationView)component;
              receiptConfigurationView.refresh();
            }
          }
        }
      }
    });
  }
}
