package com.floreantpos.bo.ui.explorer;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.AttendenceHistory;
import com.floreantpos.model.Shift;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.AttendenceHistoryDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.DateChoserDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;

















public class AttendanceHistoryExplorer
  extends TransparentPanel
{
  private SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, YYYY hh:mm a");
  private JButton btnGo = new JButton(POSConstants.GO);
  private JButton btnAdd = new JButton(Messages.getString("AttendanceHistoryExplorer.0"));
  private JButton btnEdit = new JButton(Messages.getString("AttendanceHistoryExplorer.1"));
  private JButton btnDelete = new JButton(Messages.getString("AttendanceHistoryExplorer.2"));
  
  private JXTable table;
  private JComboBox cbUserType;
  private JPanel contentPanel;
  private JXDatePicker fromDatePicker;
  private JXDatePicker toDatePicker;
  
  public AttendanceHistoryExplorer()
  {
    super(new BorderLayout());
    
    JPanel topSearchPanel = new JPanel(new MigLayout());
    cbUserType = new JComboBox();
    
    UserDAO dao = new UserDAO();
    List<User> userTypes = dao.findAll();
    
    Vector list = new Vector();
    list.add(POSConstants.ALL);
    list.addAll(userTypes);
    
    cbUserType.setModel(new DefaultComboBoxModel(list));
    fromDatePicker = UiUtil.getCurrentMonthStart();
    fromDatePicker.setFormats(new String[] { "MMM dd, yyyy" });
    
    topSearchPanel.add(new JLabel(POSConstants.START_DATE), "grow");
    topSearchPanel.add(fromDatePicker);
    
    toDatePicker = UiUtil.getCurrentMonthEnd();
    topSearchPanel.add(new JLabel(POSConstants.END_DATE), "grow");
    toDatePicker.setFormats(new String[] { "MMM dd, yyyy" });
    
    topSearchPanel.add(toDatePicker);
    topSearchPanel.add(new JLabel(POSConstants.USER + ":"));
    topSearchPanel.add(cbUserType);
    topSearchPanel.add(btnGo, "skip 1, al right");
    add(topSearchPanel, "North");
    
    contentPanel = new JPanel(new BorderLayout());
    
    add(contentPanel);
    createBottomActionPanel();
  }
  
  private void createBottomActionPanel() {
    JPanel bottomPanel = new JPanel(new FlowLayout(1));
    bottomPanel.add(btnAdd);
    bottomPanel.add(btnEdit);
    bottomPanel.add(btnDelete);
    add(bottomPanel, "South");
    
    btnGo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          AttendanceHistoryExplorer.this.viewReport();
        } catch (Exception e1) {
          BOMessageDialog.showError(AttendanceHistoryExplorer.this, POSConstants.ERROR_MESSAGE, e1);
        }
        
      }
    });
    btnEdit.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        int selectedRow = table.getSelectedRow();
        if (selectedRow < 0) {
          BOMessageDialog.showError(AttendanceHistoryExplorer.this, Messages.getString("AttendanceHistoryExplorer.4"));
          return;
        }
        int row = table.convertRowIndexToModel(selectedRow);
        AttendanceHistoryExplorer.AttendenceHistoryTableModel model = (AttendanceHistoryExplorer.AttendenceHistoryTableModel)table.getModel();
        AttendenceHistory history = (AttendenceHistory)model.getRowData(row);
        
        DateChoserDialog dialog = new DateChoserDialog(history, Messages.getString("AttendanceHistoryExplorer.5"));
        dialog.pack();
        dialog.open();
        
        if (dialog.isCanceled()) {
          return;
        }
        
        if (dialog.getAttendenceHistory() != null) {
          history = dialog.getAttendenceHistory();
        }
        
        AttendenceHistoryDAO dao = new AttendenceHistoryDAO();
        dao.saveOrUpdate(history);
        model.updateItem(selectedRow);
      }
      
    });
    btnAdd.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        DateChoserDialog dialog = new DateChoserDialog(Messages.getString("AttendanceHistoryExplorer.6"));
        dialog.pack();
        dialog.open();
        
        if (dialog.isCanceled()) {
          return;
        }
        
        AttendenceHistory history = null;
        if (dialog.getAttendenceHistory() != null) {
          history = dialog.getAttendenceHistory();
        }
        
        AttendenceHistoryDAO dao = new AttendenceHistoryDAO();
        dao.saveOrUpdate(history);
        AttendanceHistoryExplorer.AttendenceHistoryTableModel model = (AttendanceHistoryExplorer.AttendenceHistoryTableModel)table.getModel();
        model.addItem(history);
      }
      
    });
    btnDelete.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          
          AttendanceHistoryExplorer.AttendenceHistoryTableModel model = (AttendanceHistoryExplorer.AttendenceHistoryTableModel)table.getModel();
          AttendenceHistory history = (AttendenceHistory)model.getRowData(index);
          
          if (POSMessageDialog.showYesNoQuestionDialog(AttendanceHistoryExplorer.this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
            return;
          }
          
          AttendenceHistoryDAO dao = new AttendenceHistoryDAO();
          dao.delete(history);
          
          model.deleteItem(index);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
      }
    });
  }
  
  private void viewReport()
  {
    try {
      contentPanel.removeAll();
      Date fromDate = DateUtil.startOfDay(fromDatePicker.getDate());
      Date toDate = DateUtil.endOfDay(toDatePicker.getDate());
      
      if (fromDate.after(toDate)) {
        POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
        
        return;
      }
      
      User user = null;
      if (!cbUserType.getSelectedItem().equals(POSConstants.ALL)) {
        user = (User)cbUserType.getSelectedItem();
      }
      AttendenceHistoryDAO dao = new AttendenceHistoryDAO();
      List<AttendenceHistory> historyList = dao.findHistory(fromDate, toDate, user);
      
      if ((historyList == null) || (historyList.isEmpty())) {
        return;
      }
      table = new JXTable();
      
      AttendenceHistoryTableModel attendenceHistoryTableModel = new AttendenceHistoryTableModel();
      attendenceHistoryTableModel.setRows(historyList);
      table.setModel(attendenceHistoryTableModel);
      table.setRowHeight(PosUIManager.getSize(30));
      table.getSelectionModel().setSelectionMode(0);
      table.setDefaultRenderer(Object.class, new PosTableRenderer());
      
      JScrollPane scrollPane = new JScrollPane(table);
      contentPanel.add(scrollPane, "Center");
      resizeColumnWidth(table);
      contentPanel.revalidate();
      contentPanel.repaint();
    } catch (Exception e) {
      BOMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  class AttendenceHistoryTableModel extends ListTableModel {
    String[] columnNames = {
      Messages.getString("AttendanceHistoryExplorer.14"), Messages.getString("AttendanceHistoryExplorer.7"), Messages.getString("AttendanceHistoryExplorer.8"), Messages.getString("AttendanceHistoryExplorer.9"), Messages.getString("AttendanceHistoryExplorer.10"), Messages.getString("AttendanceHistoryExplorer.11"), Messages.getString("AttendanceHistoryExplorer.12"), Messages.getString("AttendanceHistoryExplorer.13") };
    
    public AttendenceHistoryTableModel() {
      setColumnNames(columnNames);
    }
    
    AttendenceHistoryTableModel() {
      setRows(list);
      setColumnNames(columnNames);
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      AttendenceHistory history = (AttendenceHistory)rows.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return Integer.valueOf(rowIndex + 1);
      case 1: 
        return history.getUser().getId();
      
      case 2: 
        return history.getUser().getFirstName() + " " + history.getUser().getLastName();
      

      case 3: 
        Date date = history.getClockInTime();
        if (date != null) {
          return dateFormat.format(date);
        }
        return "";
      

      case 4: 
        Date date2 = history.getClockOutTime();
        if (date2 != null) {
          return dateFormat.format(date2);
        }
        return "";
      
      case 5: 
        return Boolean.valueOf(!history.isClockedOut().booleanValue());
      case 6: 
        if (history.getShift() == null) {
          return "";
        }
        return history.getShift().getId();
      
      case 7: 
        return history.getTerminal().getId();
      }
      
      return null;
    }
  }
  
  public void resizeColumnWidth(JTable table) { TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++)
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(5));
    columnWidth.add(Integer.valueOf(20));
    columnWidth.add(Integer.valueOf(500));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(40));
    columnWidth.add(Integer.valueOf(40));
    columnWidth.add(Integer.valueOf(40));
    return columnWidth;
  }
}
