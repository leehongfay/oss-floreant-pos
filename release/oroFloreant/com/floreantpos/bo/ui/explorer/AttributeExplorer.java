package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.Attribute;
import com.floreantpos.model.AttributeGroup;
import com.floreantpos.model.dao.AttributeDAO;
import com.floreantpos.model.dao.AttributeGroupDAO;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.model.AttributeEntryForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.tree.TreePath;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;
import org.jdesktop.swingx.treetable.TreeTableNode;


public class AttributeExplorer
  extends TransparentPanel
  implements ActionListener, ListSelectionListener
{
  private static final long serialVersionUID = 1L;
  private JXTreeTable treeTable;
  private InvGroupTreeTableModel noRootTreeTableModel;
  private List<AttributeGroup> rootGroupList;
  
  public AttributeExplorer()
  {
    setLayout(new BorderLayout(5, 5));
    treeTable = new JXTreeTable();
    treeTable.setRowHeight(PosUIManager.getSize(30));
    treeTable.setRootVisible(false);
    
    treeTable.setDefaultRenderer(Object.class, new PosTableRenderer());
    
    rootGroupList = AttributeGroupDAO.getInstance().findAll();
    
    createTree();
    treeTable.expandAll();
    treeTable.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        int col = treeTable.columnAtPoint(me.getPoint());
        if ((me.getClickCount() == 2) && (col == 0))
        {
          treeTable.expandPath(treeTable.getPathForRow(treeTable.getSelectedRow()));
        }
        else if ((me.getClickCount() == 2) && (col != 0)) {
          AttributeExplorer.this.editSelectedRow();
        }
        
      }
    });
    add(new JScrollPane(treeTable), "Center");
    
    createButtonPanel();
  }
  
  private void createTree() {
    if (rootGroupList != null) {
      AttributeGroup demo = new AttributeGroup();
      demo.setId("0");
      demo.setName("Root");
      DefaultMutableTreeTableNode rootNode = new DefaultMutableTreeTableNode(demo);
      rootNode.setUserObject(demo);
      
      for (AttributeGroup inventoryGroup : rootGroupList) {
        DefaultMutableTreeTableNode node = new DefaultMutableTreeTableNode(inventoryGroup);
        rootNode.add(node);
        buildAttributeTree(node);
      }
      
      noRootTreeTableModel = new InvGroupTreeTableModel(rootNode);
      treeTable.setTreeTableModel(noRootTreeTableModel);
    }
  }
  
  private void buildAttributeTree(DefaultMutableTreeTableNode defaultMutableTreeTableNode) {
    AttributeGroup attributeGroup = (AttributeGroup)defaultMutableTreeTableNode.getUserObject();
    if (attributeGroup == null) {
      return;
    }
    
    defaultMutableTreeTableNode.setAllowsChildren(true);
    List<Attribute> attributeList = attributeGroup.getAttributes();
    for (Attribute inventoryUnit : attributeList) {
      DefaultMutableTreeTableNode node = new DefaultMutableTreeTableNode(inventoryUnit);
      defaultMutableTreeTableNode.add(node);
    }
  }
  
  class InvGroupTreeTableModel extends DefaultTreeTableModel {
    private final String[] COLUMN_NAMES = { "Group", "Name", "Sort Order", "Default" };
    
    public InvGroupTreeTableModel(DefaultMutableTreeTableNode rootAttribute) {
      super();
    }
    
    public void setRoot(TreeTableNode root)
    {
      super.setRoot(root);
    }
    
    public int getColumnCount()
    {
      return COLUMN_NAMES.length;
    }
    
    public String getColumnName(int column)
    {
      return COLUMN_NAMES[column];
    }
    
    public boolean isCellEditable(Object node, int column)
    {
      return false;
    }
    
    public Object getValueAt(Object node, int column)
    {
      if ((node instanceof DefaultMutableTreeTableNode)) {
        if ((((DefaultMutableTreeTableNode)node).getUserObject() instanceof AttributeGroup)) {
          AttributeGroup inventoryAttribute = (AttributeGroup)((DefaultMutableTreeTableNode)node).getUserObject();
          if (inventoryAttribute == null) {
            return "";
          }
          switch (column) {
          case 0: 
            return inventoryAttribute.getName();
          }
        }
        else if ((((DefaultMutableTreeTableNode)node).getUserObject() instanceof Attribute)) {
          Attribute inventoryUnit = (Attribute)((DefaultMutableTreeTableNode)node).getUserObject();
          if (inventoryUnit == null) {
            return "";
          }
          switch (column) {
          case 1: 
            return inventoryUnit.getName();
          case 2: 
            return inventoryUnit.getSortOrder();
          case 3: 
            return inventoryUnit.isDefaultAttribute();
          }
          
        }
      }
      return null;
    }
  }
  
  private void createButtonPanel() {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    
    JButton btn_newGroup = new JButton("New Attribute Group");
    JButton btn_newAttribute = new JButton("Add Attribute");
    JButton editButton = explorerButton.getEditButton();
    JButton deleteButton = explorerButton.getDeleteButton();
    
    btn_newGroup.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        String groupName = JOptionPane.showInputDialog(POSUtil.getFocusedWindow(), "Enter group name");
        if (groupName == null) {
          return;
        }
        if (groupName.equals("")) {
          BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Group name cannot be empty.");
          return;
        }
        
        if (groupName.length() > 30) {
          BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Group name too long.");
          return;
        }
        
        AttributeGroup inventoryGroup = new AttributeGroup();
        inventoryGroup.setName(groupName);
        
        AttributeGroupDAO inventoryGroupDAO = new AttributeGroupDAO();
        inventoryGroupDAO.saveOrUpdate(inventoryGroup);
        
        if (inventoryGroup != null) {
          MutableTreeTableNode root = (MutableTreeTableNode)noRootTreeTableModel.getRoot();
          noRootTreeTableModel.insertNodeInto(new DefaultMutableTreeTableNode(inventoryGroup), root, root.getChildCount());
        }
        
        treeTable.expandAll();
      }
      
    });
    btn_newAttribute.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        Attribute att = new Attribute();
        att.setGroup(getSelectedAtrributeGroup());
        AttributeEntryForm inventoryUnitFormTree = new AttributeEntryForm(att);
        BeanEditorDialog dialog = new BeanEditorDialog(inventoryUnitFormTree);
        dialog.setPreferredSize(PosUIManager.getSize(500, 400));
        dialog.open();
        if (dialog.isCanceled())
          return;
        Attribute newAtt = (Attribute)inventoryUnitFormTree.getBean();
        AttributeGroup parentOfNewAtt = newAtt.getGroup();
        
        if (newAtt != null) {
          MutableTreeTableNode root = (MutableTreeTableNode)noRootTreeTableModel.getRoot();
          if (parentOfNewAtt != null) {
            MutableTreeTableNode parentNode = findTreeNodeForAttribute(root, parentOfNewAtt.getId());
            if (parentNode != null) {
              noRootTreeTableModel.insertNodeInto(new DefaultMutableTreeTableNode(newAtt), parentNode, parentNode
                .getChildCount());
            }
          }
          else {
            MutableTreeTableNode parentNode = findTreeNodeForAttribute(root, "0");
            if (parentNode != null) {
              noRootTreeTableModel.insertNodeInto(new DefaultMutableTreeTableNode(newAtt), parentNode, parentNode
                .getChildCount());
            }
          }
        }
        
        treeTable.expandAll();
      }
      

    });
    editButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        AttributeExplorer.this.editSelectedRow();
      }
      

    });
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        deleteInventoryAttribute();
      }
      
      private void deleteInventoryAttribute() {
        try {
          int index = treeTable.getSelectedRow();
          if (index < 0) {
            return;
          }
          TreePath treePath = treeTable.getPathForRow(index);
          DefaultMutableTreeTableNode lastPathComponent = (DefaultMutableTreeTableNode)treePath.getLastPathComponent();
          
          if ((lastPathComponent.getUserObject() instanceof AttributeGroup))
          {
            AttributeGroup attributeGroup = (AttributeGroup)lastPathComponent.getUserObject();
            
            if (POSMessageDialog.showYesNoQuestionDialog(getRootPane(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
              return;
            }
            
            MutableTreeTableNode tableNode = findTreeNodeForAttribute((MutableTreeTableNode)noRootTreeTableModel.getRoot(), attributeGroup.getId());
            if (tableNode.getParent() != null) {
              noRootTreeTableModel.removeNodeFromParent(tableNode);
            }
            
            AttributeGroupDAO inventoryGroupDAO = new AttributeGroupDAO();
            inventoryGroupDAO.delete(attributeGroup);

          }
          else if ((lastPathComponent.getUserObject() instanceof Attribute))
          {
            Attribute attribute = (Attribute)lastPathComponent.getUserObject();
            if (POSMessageDialog.showYesNoQuestionDialog(getRootPane(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
              return;
            }
            
            MutableTreeTableNode tableNode = findTreeNodeForAttribute((MutableTreeTableNode)noRootTreeTableModel.getRoot(), attribute.getId());
            if (tableNode.getParent() != null) {
              noRootTreeTableModel.removeNodeFromParent(tableNode);
            }
            
            AttributeDAO inventoryUnitDAO = new AttributeDAO();
            inventoryUnitDAO.delete(attribute);
          }
          

          treeTable.repaint();
          treeTable.revalidate();
        }
        catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      

    });
    TransparentPanel panel = new TransparentPanel();
    
    panel.add(btn_newGroup);
    panel.add(btn_newAttribute);
    panel.add(editButton);
    panel.add(deleteButton);
    add(panel, "South");
  }
  


  public void actionPerformed(ActionEvent e) {}
  

  public void valueChanged(ListSelectionEvent e) {}
  

  private void editSelectedRow()
  {
    try
    {
      int index = treeTable.getSelectedRow();
      if (index < 0)
        return;
      TreePath treePath = treeTable.getPathForRow(index);
      
      DefaultMutableTreeTableNode lastPathComponent = (DefaultMutableTreeTableNode)treePath.getLastPathComponent();
      
      if ((lastPathComponent.getUserObject() instanceof AttributeGroup))
      {
        AttributeGroup attributeGroup = (AttributeGroup)lastPathComponent.getUserObject();
        
        String groupName = JOptionPane.showInputDialog(POSUtil.getFocusedWindow(), "Group name", attributeGroup.getName());
        if (groupName == null) {
          BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Group name cannot be empty.");
          return;
        }
        if (groupName.length() > 30) {
          BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Group name too long.");
          return;
        }
        
        attributeGroup.setName(groupName);
        AttributeGroupDAO inventoryGroupDAO = new AttributeGroupDAO();
        inventoryGroupDAO.saveOrUpdate(attributeGroup);
      }
      else if ((lastPathComponent.getUserObject() instanceof Attribute))
      {
        Attribute attribute = (Attribute)lastPathComponent.getUserObject();
        
        AttributeEntryForm inventoryUnitFormTree = new AttributeEntryForm(attribute);
        BeanEditorDialog dialog = new BeanEditorDialog(inventoryUnitFormTree);
        dialog.setPreferredSize(PosUIManager.getSize(500, 400));
        dialog.open();
        if (dialog.isCanceled()) {
          return;
        }
      }
      rootGroupList = AttributeGroupDAO.getInstance().findAll();
      createTree();
      treeTable.expandAll();
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public Attribute getSelectedAtrribute() {
    try {
      int index = treeTable.getSelectedRow();
      if (index < 0)
        return null;
      TreePath treePath = treeTable.getPathForRow(index);
      
      DefaultMutableTreeTableNode lastPathComponent = (DefaultMutableTreeTableNode)treePath.getLastPathComponent();
      
      if ((lastPathComponent.getUserObject() instanceof AttributeGroup)) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select attribute");
        return null;
      }
      if ((lastPathComponent.getUserObject() instanceof Attribute)) {
        return (Attribute)lastPathComponent.getUserObject();
      }
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
    return null;
  }
  
  public AttributeGroup getSelectedAtrributeGroup() {
    try {
      int index = treeTable.getSelectedRow();
      if (index < 0)
        return null;
      TreePath treePath = treeTable.getPathForRow(index);
      
      DefaultMutableTreeTableNode lastPathComponent = (DefaultMutableTreeTableNode)treePath.getLastPathComponent();
      
      if ((lastPathComponent.getUserObject() instanceof AttributeGroup)) {
        return (AttributeGroup)lastPathComponent.getUserObject();
      }
      if ((lastPathComponent.getUserObject() instanceof Attribute)) {
        return ((Attribute)lastPathComponent.getUserObject()).getGroup();
      }
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
    return null;
  }
  

  public MutableTreeTableNode findTreeNodeForAttribute(MutableTreeTableNode attributeNode, String attributeId)
  {
    if ((((DefaultMutableTreeTableNode)attributeNode).getUserObject() instanceof AttributeGroup)) {
      AttributeGroup groupAttribute = (AttributeGroup)attributeNode.getUserObject();
      if (attributeId.equals(groupAttribute.getId())) {
        return attributeNode;
      }
      
      Enumeration<? extends TreeTableNode> children = attributeNode.children();
      while (children.hasMoreElements()) {
        MutableTreeTableNode treeTableNode = (MutableTreeTableNode)children.nextElement();
        MutableTreeTableNode findAttById = findTreeNodeForAttribute(treeTableNode, attributeId);
        if (findAttById != null) {
          return findAttById;
        }
      }
    }
    else if ((((DefaultMutableTreeTableNode)attributeNode).getUserObject() instanceof Attribute)) {
      Attribute attributeAttribute = (Attribute)attributeNode.getUserObject();
      if (attributeId.equals(attributeAttribute.getId())) {
        return attributeNode;
      }
      
      Enumeration<? extends TreeTableNode> children = attributeNode.children();
      while (children.hasMoreElements()) {
        MutableTreeTableNode treeTableNode = (MutableTreeTableNode)children.nextElement();
        MutableTreeTableNode findAttById = findTreeNodeForAttribute(treeTableNode, attributeId);
        if (findAttById != null) {
          return findAttById;
        }
      }
    }
    
    return null;
  }
}
