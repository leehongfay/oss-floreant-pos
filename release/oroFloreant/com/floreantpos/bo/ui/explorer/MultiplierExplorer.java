package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.Multiplier;
import com.floreantpos.model.dao.MultiplierDAO;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.model.MultiplierForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;















public class MultiplierExplorer
  extends TransparentPanel
{
  private List<Multiplier> multiplierList;
  private JTable table;
  private MultiplierExplorerTableModel tableModel;
  private JButton editButton;
  private JButton deleteButton;
  
  public MultiplierExplorer()
  {
    multiplierList = MultiplierDAO.getInstance().findAll();
    
    tableModel = new MultiplierExplorerTableModel();
    table = new JTable(tableModel);
    table.setRowHeight(PosUIManager.getSize(30));
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.getColumnModel().getColumn(6).setCellRenderer(new PosTableRenderer());
    table.setSelectionMode(0);
    table.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        int index = table.getSelectedRow();
        if (index < 0) {
          return;
        }
        Multiplier multiplier = (Multiplier)multiplierList.get(index);
        editButton.setEnabled(!multiplier.isMain().booleanValue());
        deleteButton.setEnabled(!multiplier.isMain().booleanValue());
      }
      
    });
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          MultiplierForm editor = new MultiplierForm();
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          if (dialog.isCanceled()) {
            return;
          }
          tableModel.addMultiplier((Multiplier)editor.getBean());
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          Multiplier multiplier = (Multiplier)multiplierList.get(index);
          if (multiplier.isMain().booleanValue()) {
            return;
          }
          MultiplierForm multiplierForm = new MultiplierForm(multiplier);
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), multiplierForm);
          dialog.open();
          if (dialog.isCanceled()) {
            return;
          }
          table.repaint();
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0)
            return;
          Multiplier multiplier = (Multiplier)multiplierList.get(index);
          if (multiplier.isMain().booleanValue())
            return;
          if (ConfirmDeleteDialog.showMessage(MultiplierExplorer.this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0)
          {
            MultiplierDAO.getInstance().deleteMultiplier(multiplier);
            tableModel.deleteMultiplier(multiplier, index);
          }
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    JButton btnSetAsDefault = new JButton("Set default");
    btnSetAsDefault.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0)
            return;
          Multiplier selectedMultiplier = (Multiplier)multiplierList.get(index);
          for (Multiplier item : multiplierList) {
            if (selectedMultiplier.getId().equals(item.getId())) {
              item.setDefaultMultiplier(Boolean.valueOf(true));
            } else
              item.setDefaultMultiplier(Boolean.valueOf(false));
          }
          MultiplierDAO.getInstance().saveOrUpdateMultipliers(multiplierList);
          tableModel.fireTableDataChanged();
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    panel.add(btnSetAsDefault);
    add(panel, "South");
  }
  
  class MultiplierExplorerTableModel extends AbstractTableModel {
    String[] columnNames = { POSConstants.NAME, "Prefix", POSConstants.RATE, POSConstants.SORT_ORDER, POSConstants.BUTTON_COLOR, POSConstants.TEXT_COLOR, "Default" };
    
    MultiplierExplorerTableModel() {}
    
    public int getRowCount() { if (multiplierList == null) {
        return 0;
      }
      return multiplierList.size();
    }
    
    public int getColumnCount() {
      return columnNames.length;
    }
    
    public String getColumnName(int column)
    {
      return columnNames[column];
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      return false;
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      if (multiplierList == null) {
        return "";
      }
      Multiplier multiplier = (Multiplier)multiplierList.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return multiplier.getId();
      case 1: 
        return multiplier.getTicketPrefix();
      case 2: 
        return multiplier.getRate();
      case 3: 
        return multiplier.getSortOrder();
      case 4: 
        if (multiplier.getButtonColor() != null) {
          return new Color(multiplier.getButtonColor().intValue());
        }
        
        return null;
      case 5: 
        if (multiplier.getTextColor() != null) {
          return new Color(multiplier.getTextColor().intValue());
        }
        
        return null;
      case 6: 
        return multiplier.isDefaultMultiplier();
      }
      
      return null;
    }
    
    public void addMultiplier(Multiplier multiplier) {
      int size = multiplierList.size();
      multiplierList.add(multiplier);
      fireTableRowsInserted(size, size);
    }
    
    public void deleteMultiplier(Multiplier multiplier, int index) {
      multiplierList.remove(multiplier);
      fireTableRowsDeleted(index, index);
    }
  }
}
