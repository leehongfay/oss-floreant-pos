package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.ComboGroup;
import com.floreantpos.model.ComboItem;
import com.floreantpos.model.MenuItem;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.dialog.MenuItemSelectionDialog;
import com.floreantpos.ui.model.ComboGroupItemSelectionDialog;
import com.floreantpos.ui.model.ComboItemEntryDialog;
import com.floreantpos.util.NumberUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;















public class ComboItemExplorer
  extends TransparentPanel
  implements ActionListener
{
  private JTable comboItemTable;
  private JTable comboGroupTable;
  private final ComboItemExplorerTableModel comboItemModel;
  private final ComboGroupExplorerTableModel comboGroupModel;
  private MenuItem menuItem;
  
  public ComboItemExplorer()
  {
    initComponents();
    
    comboItemModel = new ComboItemExplorerTableModel();
    comboItemTable.setModel(comboItemModel);
    
    comboGroupModel = new ComboGroupExplorerTableModel();
    comboGroupTable.setModel(comboGroupModel);
    resizeTableColumns();
  }
  
  private void initComponents() {
    setLayout(new MigLayout("fill"));
    setBorder(new EmptyBorder(10, 10, 10, 10));
    
    comboItemTable = new JTable();
    comboItemTable.setDefaultRenderer(Object.class, new PosTableRenderer());
    comboItemTable.setRowHeight(30);
    
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.setActionCommand(POSConstants.ADD);
    addButton.addActionListener(this);
    
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.setActionCommand(POSConstants.EDIT);
    editButton.addActionListener(this);
    
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.setActionCommand(POSConstants.DELETE);
    deleteButton.addActionListener(this);
    
    TransparentPanel comboItemActionPanel = new TransparentPanel(new MigLayout("inset 0 0 10 0"));
    comboItemActionPanel.add(addButton);
    comboItemActionPanel.add(editButton);
    comboItemActionPanel.add(deleteButton);
    
    add(new JScrollPane(comboItemTable), "grow");
    add(comboItemActionPanel, "grow,newline");
    
    comboGroupTable = new JTable();
    comboGroupTable.setDefaultRenderer(Object.class, new PosTableRenderer());
    comboGroupTable.setRowHeight(30);
    
    JButton addGroupButton = new JButton(POSConstants.ADD);
    addGroupButton.setActionCommand(POSConstants.ADD);
    
    JButton editGroupButton = new JButton(POSConstants.EDIT);
    editGroupButton.setActionCommand(POSConstants.EDIT);
    
    JButton deleteGroupButton = new JButton(POSConstants.DELETE);
    deleteGroupButton.setActionCommand(POSConstants.DELETE);
    
    ActionListener groupAction = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        String actionCommand = e.getActionCommand();
        if (POSConstants.ADD.equals(actionCommand)) {
          ComboItemExplorer.this.addNewComboGroup();
        }
        else if (POSConstants.EDIT.equals(actionCommand)) {
          int index = comboGroupTable.getSelectedRow();
          if (index < 0) {
            BOMessageDialog.showError("Please select one item.");
            return;
          }
          ComboGroup menuItem = comboGroupModel.getComboGroup(index);
          ComboItemExplorer.this.editComboGroup(menuItem);
        }
        else if (POSConstants.DELETE.equals(actionCommand)) {
          int index = comboGroupTable.getSelectedRow();
          if (index < 0) {
            BOMessageDialog.showError(POSConstants.SELECT_ITEM_TO_DELETE);
            return;
          }
          ComboGroup menuItem = comboGroupModel.getComboGroup(index);
          ComboItemExplorer.this.deleteComboGroup(index, menuItem);
        }
      }
    };
    addGroupButton.addActionListener(groupAction);
    editGroupButton.addActionListener(groupAction);
    deleteGroupButton.addActionListener(groupAction);
    
    TransparentPanel comboGroupButtonActionPanel = new TransparentPanel(new MigLayout("inset 0 0 10 0"));
    comboGroupButtonActionPanel.add(addGroupButton);
    comboGroupButtonActionPanel.add(editGroupButton);
    comboGroupButtonActionPanel.add(deleteGroupButton);
    
    add(new JScrollPane(comboGroupTable), "grow,newline");
    add(comboGroupButtonActionPanel, "grow,newline");
  }
  
  private void resizeTableColumns() {
    comboGroupTable.setAutoResizeMode(4);
    setColumnWidth(0, PosUIManager.getSize(300));
    setColumnWidth(1, PosUIManager.getSize(100));
    setColumnWidth(2, PosUIManager.getSize(100));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = comboGroupTable.getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  public void setMenuItem(MenuItem menuItem) {
    this.menuItem = menuItem;
    comboItemModel.setItems(menuItem.getComboItems());
    comboGroupModel.setItems(menuItem.getComboGroups());
  }
  
  public void actionPerformed(ActionEvent e)
  {
    String actionCommand = e.getActionCommand();
    if (POSConstants.ADD.equals(actionCommand)) {
      addNewComboItem();
    }
    else if (POSConstants.EDIT.equals(actionCommand)) {
      int index = comboItemTable.getSelectedRow();
      if (index < 0) {
        BOMessageDialog.showError("Please select one item.");
        return;
      }
      ComboItem menuItem = comboItemModel.getComboItem(index);
      editComboItem(menuItem);
    }
    else if (POSConstants.DELETE.equals(actionCommand)) {
      int index = comboItemTable.getSelectedRow();
      if (index < 0) {
        BOMessageDialog.showError(POSConstants.SELECT_ITEM_TO_DELETE);
        return;
      }
      ComboItem menuItem = comboItemModel.getComboItem(index);
      deleteComboItem(index, menuItem);
    }
  }
  




  private void addNewComboItem()
  {
    try
    {
      MenuItemSelectionDialog dialog = new MenuItemSelectionDialog(new ArrayList());
      dialog.setSize(PosUIManager.getSize(600, 515));
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      existingItemList = new ArrayList();
      for (Iterator localIterator = comboItemModel.getItems().iterator(); localIterator.hasNext();) { comboItem = (ComboItem)localIterator.next();
        existingItemList.add(comboItem.getMenuItem());
      }
      
      Object selectedMenuItems = dialog.getSelectedItems();
      for (MenuItem menuItem : (List)selectedMenuItems)
        if ((existingItemList != null) && (!existingItemList.contains(menuItem))) {
          ComboItem comboItem = new ComboItem();
          comboItem.setMenuItem(menuItem);
          comboItem.setQuantity(Double.valueOf(1.0D));
          

          comboItemModel.addComboItem(comboItem);
        }
    } catch (Exception x) {
      List<MenuItem> existingItemList;
      ComboItem comboItem;
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void editComboItem(ComboItem comboMenuItem) {
    try {
      ComboItemEntryDialog dialog = new ComboItemEntryDialog(menuItem);
      dialog.setSelectedItem(comboMenuItem.getMenuItem());
      dialog.setQuantity(comboMenuItem.getQuantity());
      dialog.open();
      
      if (dialog.isCanceled()) {
        return;
      }
      MenuItem selectedMenuItem = dialog.getItem();
      comboMenuItem.setMenuItem(selectedMenuItem);
      comboMenuItem.setQuantity(dialog.getQuantity());
      
      comboItemModel.fireTableDataChanged();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void deleteComboItem(int index, ComboItem comboItem) {
    try {
      if (ConfirmDeleteDialog.showMessage(this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0) {
        comboItemModel.deleteItem(comboItem, index);
      }
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void addNewComboGroup() {
    try {
      ComboGroup comboGroup = new ComboGroup();
      ComboGroupItemSelectionDialog dialog = new ComboGroupItemSelectionDialog(menuItem, comboGroup);
      dialog.setSize(PosUIManager.getSize(600, 600));
      dialog.open();
      
      if (dialog.isCanceled())
        return;
      comboGroupModel.addComboGroup(comboGroup);
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void editComboGroup(ComboGroup comboGroup) {
    try {
      ComboGroupItemSelectionDialog dialog = new ComboGroupItemSelectionDialog(menuItem, comboGroup, true);
      dialog.setSize(PosUIManager.getSize(600, 600));
      dialog.open();
      
      if (dialog.isCanceled()) {
        return;
      }
      comboGroupModel.fireTableDataChanged();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void deleteComboGroup(int index, ComboGroup comboGroup) {
    try {
      if (ConfirmDeleteDialog.showMessage(this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0) {
        comboGroupModel.deleteItem(comboGroup, index);
      }
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private class ComboItemExplorerTableModel extends AbstractTableModel {
    String[] columnNames = { "Item Name", "Quantity", "Price" };
    List<ComboItem> comboItemList;
    
    public ComboItemExplorerTableModel() {
      comboItemList = new ArrayList();
    }
    
    public void setItems(List<ComboItem> comboItems) {
      if (comboItems == null)
        return;
      comboItemList.addAll(comboItems);
    }
    
    public List<ComboItem> getItems() {
      return comboItemList;
    }
    
    public int getRowCount()
    {
      if (comboItemList == null) {
        return 0;
      }
      return comboItemList.size();
    }
    
    public int getColumnCount()
    {
      return columnNames.length;
    }
    
    public String getColumnName(int index)
    {
      return columnNames[index];
    }
    
    public Object getValueAt(int row, int column)
    {
      if (comboItemList == null) {
        return "";
      }
      ComboItem comboItem = (ComboItem)comboItemList.get(row);
      if (comboItem == null) {
        return "";
      }
      switch (column) {
      case 0: 
        return comboItem.getName();
      
      case 1: 
        return NumberUtil.trimDecilamIfNotNeeded(comboItem.getQuantity());
      case 2: 
        return Double.valueOf(comboItem.getPrice().doubleValue() * comboItem.getQuantity().doubleValue());
      }
      return null;
    }
    
    public void addComboItem(ComboItem item) {
      int size = comboItemList.size();
      comboItemList.add(item);
      fireTableRowsInserted(size, size);
    }
    
    public void deleteItem(ComboItem comboItem, int index) {
      for (Iterator iterator = comboItemList.iterator(); iterator.hasNext();) {
        ComboItem item = (ComboItem)iterator.next();
        if (comboItem.getMenuItem() == item.getMenuItem()) {
          iterator.remove();
        }
      }
      fireTableRowsDeleted(index, index);
    }
    
    public ComboItem getComboItem(int index) {
      return (ComboItem)comboItemList.get(index);
    }
  }
  
  private class ComboGroupExplorerTableModel extends AbstractTableModel {
    String[] columnNames = { "Group Name", "Min Quantity", "Max Quantity", "Items" };
    List<ComboGroup> comboGroups;
    
    public ComboGroupExplorerTableModel() {
      comboGroups = new ArrayList();
    }
    
    public void setItems(List<ComboGroup> comboGroup) {
      if (comboGroup == null)
        return;
      comboGroups.addAll(comboGroup);
    }
    
    public int getRowCount()
    {
      if (comboGroups == null) {
        return 0;
      }
      return comboGroups.size();
    }
    
    public int getColumnCount()
    {
      return columnNames.length;
    }
    
    public String getColumnName(int index)
    {
      return columnNames[index];
    }
    
    public Object getValueAt(int row, int column)
    {
      if (comboGroups == null) {
        return "";
      }
      ComboGroup comboGroup = (ComboGroup)comboGroups.get(row);
      if (comboGroup == null) {
        return "";
      }
      switch (column) {
      case 0: 
        return comboGroup.getName();
      
      case 1: 
        return comboGroup.getMinQuantity();
      case 2: 
        return comboGroup.getMaxQuantity();
      case 3: 
        return comboGroup.getItems();
      }
      return null;
    }
    
    public void addComboGroup(ComboGroup comboGroup) {
      int size = comboGroups.size();
      comboGroups.add(comboGroup);
      fireTableRowsInserted(size, size);
    }
    
    public void deleteItem(ComboGroup group, int index) {
      for (Iterator iterator = comboGroups.iterator(); iterator.hasNext();) {
        ComboGroup item = (ComboGroup)iterator.next();
        if (group.getName().equals(item.getName())) {
          iterator.remove();
          break;
        }
      }
      fireTableRowsDeleted(index, index);
    }
    
    public ComboGroup getComboGroup(int index) {
      return (ComboGroup)comboGroups.get(index);
    }
    
    public List<ComboGroup> getGroups() {
      return comboGroups;
    }
  }
  
  public List<ComboItem> getSelectedComboItems() {
    return comboItemModel.getItems();
  }
  
  public List<ComboGroup> getSelectedComboGroups() {
    return comboGroupModel.getGroups();
  }
}
