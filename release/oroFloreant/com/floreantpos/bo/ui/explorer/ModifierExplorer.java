package com.floreantpos.bo.ui.explorer;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.dao.MenuModifierDAO;
import com.floreantpos.model.dao.ModifierDAO;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.model.MenuModifierForm;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.JXTable;


















public class ModifierExplorer
  extends TransparentPanel
  implements ExplorerView
{
  private String currencySymbol;
  private JXTable table;
  private ModifierExplorerModel tableModel;
  private JTextField nameField;
  
  public ModifierExplorer()
  {
    setLayout(new BorderLayout(5, 5));
    
    currencySymbol = CurrencyUtil.getCurrencySymbol();
    tableModel = new ModifierExplorerModel();
    table = new JXTable(tableModel);
    table.setRowHeight(PosUIManager.getSize(30));
    add(new JScrollPane(table));
    
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          ModifierExplorer.this.editSelectedRow();
        }
        
      }
    });
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.setSelectionMode(0);
    table.getColumnModel().getColumn(8).setCellRenderer(new DefaultTableCellRenderer() {
      private Border unselectedBorder = null;
      private Border selectedBorder = null;
      
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
      {
        ModifierExplorer.ModifierExplorerModel model = (ModifierExplorer.ModifierExplorerModel)table.getModel();
        Object object = model.getRowData(row);
        
        if (selectedBorder == null) {
          selectedBorder = BorderFactory.createMatteBorder(5, 5, 5, 5, table.getSelectionBackground());
        }
        if (unselectedBorder == null) {
          unselectedBorder = BorderFactory.createMatteBorder(5, 5, 5, 5, table.getBackground());
        }
        
        if (((object instanceof MenuModifier)) && 
          ((value instanceof ImageIcon))) {
          JLabel lblColor = new JLabel(((MenuModifier)object).getImage());
          if (isSelected) {
            lblColor.setBorder(selectedBorder);
          }
          else {
            lblColor.setBorder(unselectedBorder);
          }
          return lblColor;
        }
        
        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      }
      
    });
    table.setRowHeight(PosUIManager.getSize(60));
    createActionButtons();
    add(buildSearchForm(), "North");
  }
  
  private void createActionButtons() {
    ExplorerButtonPanel explorerButtonPanel = new ExplorerButtonPanel();
    explorerButtonPanel.getAddButton().addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          MenuModifierForm editor = new MenuModifierForm();
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          if (dialog.isCanceled())
            return;
          MenuModifier modifier = (MenuModifier)editor.getBean();
          tableModel.addModifier(modifier);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    explorerButtonPanel.getEditButton().addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        ModifierExplorer.this.editSelectedRow();
      }
      

    });
    explorerButtonPanel.getDeleteButton().addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          
          if (ConfirmDeleteDialog.showMessage(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 1)
          {
            MenuModifier category = (MenuModifier)tableModel.getRowData(index);
            ModifierDAO modifierDAO = new ModifierDAO();
            modifierDAO.delete(category);
            tableModel.deleteModifier(category, index);
          }
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      

    });
    JButton btnDuplicate = new JButton("Dup");
    btnDuplicate.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          
          MenuModifier existingModifier = (MenuModifier)tableModel.getRowData(index);
          
          MenuModifier newMenuModifier = new MenuModifier();
          PropertyUtils.copyProperties(newMenuModifier, existingModifier);
          newMenuModifier.setId(null);
          String newName = ModifierExplorer.this.doDuplicateName(existingModifier);
          newMenuModifier.setName(newName);
          
          MenuModifierForm editor = new MenuModifierForm(newMenuModifier);
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          if (dialog.isCanceled()) {
            return;
          }
          MenuModifier menuModifier = (MenuModifier)editor.getBean();
          tableModel.addModifier(menuModifier);
          table.getSelectionModel().addSelectionInterval(tableModel.getRowCount() - 1, tableModel.getRowCount() - 1);
          table.scrollRowToVisible(tableModel.getRowCount() - 1);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    explorerButtonPanel.add(btnDuplicate);
    
    add(explorerButtonPanel, "South");
  }
  
  private String doDuplicateName(MenuModifier existingModifier) {
    String existingName = existingModifier.getName();
    String newName = new String();
    int lastIndexOf = existingName.lastIndexOf(" ");
    if (lastIndexOf == -1) {
      newName = existingName + " 1";
    }
    else {
      String processName = existingName.substring(lastIndexOf + 1, existingName.length());
      if (StringUtils.isNumeric(processName)) {
        Integer count = Integer.valueOf(processName);
        count = Integer.valueOf(count.intValue() + 1);
        newName = existingName.replace(processName, String.valueOf(count));
      }
      else {
        newName = existingName + " 1";
      }
    }
    return newName;
  }
  
  private int getRandomNumber(MenuModifier selectedModifier) {
    String in = selectedModifier.getName();
    Pattern p = Pattern.compile(" .* ([0-9]+)");
    Matcher m = p.matcher(in);
    int nextNumber = 1;
    if (m.find()) {
      try {
        int previousNumber = Integer.parseInt(m.group(1));
        if (previousNumber + 1 > nextNumber) {
          nextNumber = previousNumber + 1;
        }
      }
      catch (Exception localException) {}
    }
    
    return nextNumber;
  }
  
  private JPanel buildSearchForm() {
    JPanel panel = new JPanel();
    panel.setLayout(new MigLayout("", "[][]30[][]30[]", "[]20[]"));
    
    JLabel nameLabel = new JLabel(Messages.getString("ModifierExplorer.3"));
    JLabel groupLabel = new JLabel(Messages.getString("ModifierExplorer.4"));
    nameField = new JTextField(15);
    JComboBox cbGroup = new JComboBox();
    cbGroup.addItem(Messages.getString("ModifierExplorer.5"));
    
    JButton searchBttn = new JButton(Messages.getString("ModifierExplorer.6"));
    panel.add(nameLabel, "align label");
    panel.add(nameField);
    

    panel.add(searchBttn);
    


    Border loweredetched = BorderFactory.createEtchedBorder(1);
    TitledBorder title = BorderFactory.createTitledBorder(loweredetched, Messages.getString("ModifierExplorer.8"));
    title.setTitleJustification(1);
    panel.setBorder(title);
    searchBttn.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        ModifierExplorer.this.searchItem();
      }
    });
    return panel;
  }
  
  private class ModifierExplorerModel extends ListTableModel
  {
    public ModifierExplorerModel()
    {
      super();
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      List<MenuModifier> modifierList = getRows();
      
      MenuModifier modifier = (MenuModifier)modifierList.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return modifier.getName();
      
      case 1: 
        return modifier.getTranslatedName();
      
      case 2: 
        return Double.valueOf(modifier.getPrice().doubleValue());
      
      case 3: 
        return Double.valueOf(modifier.getExtraPrice().doubleValue());
      
      case 4: 
        if (modifier.getTaxGroup() == null) {
          return "";
        }
        return modifier.getTaxGroup();
      
      case 5: 
        if (modifier.getButtonColor() != null) {
          return new Color(modifier.getButtonColor().intValue());
        }
      
      case 6: 
        if (modifier.getTextColor() != null) {
          return new Color(modifier.getTextColor().intValue());
        }
        
        return null;
      
      case 7: 
        return modifier.getSortOrder();
      
      case 8: 
        return modifier.getImage();
      }
      return null;
    }
    
    public void addModifier(MenuModifier category) {
      int size = getRows().size();
      getRows().add(category);
      fireTableRowsInserted(size, size);
    }
    
    public void deleteModifier(MenuModifier category, int index)
    {
      getRows().remove(category);
      fireTableRowsDeleted(index, index);
    }
    
    public void removeAll() {
      rows.clear();
      fireTableDataChanged();
    }
    
    public void addRows(List<MenuModifier> rows) {
      if (rows == null) {
        return;
      }
      for (MenuModifier row : rows) {
        addModifier(row);
      }
      fireTableDataChanged();
    }
  }
  
  private void editSelectedRow()
  {
    try
    {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      MenuModifier modifier = (MenuModifier)tableModel.getRowData(index);
      MenuModifierDAO.getInstance().initialize(modifier);
      MenuModifierForm editor = new MenuModifierForm(modifier);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void searchItem()
  {
    String txName = nameField.getText();
    
    if (tableModel.getRowCount() > 0) {
      tableModel.removeAll();
    }
    List<MenuModifier> modifierList = MenuModifierDAO.getInstance().getMenuModifiers(txName);
    if (modifierList == null) {
      modifierList = new ArrayList();
    }
    tableModel.setRows(modifierList);
  }
  
  public void initData()
  {
    searchItem();
  }
}
