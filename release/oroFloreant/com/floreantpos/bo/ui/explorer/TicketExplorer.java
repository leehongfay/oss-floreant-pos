package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.ui.views.OrderInfoDialog;
import com.floreantpos.ui.views.OrderInfoView;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;






















public class TicketExplorer
  extends TransparentPanel
{
  private SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy, h:m a");
  
  private JXDatePicker fromDatePicker = UiUtil.getCurrentMonthStart();
  private JXDatePicker toDatePicker = UiUtil.getCurrentMonthEnd();
  private JButton btnGo = new JButton(POSConstants.GO);
  
  private JXTable table;
  
  private TicketExplorerTableModel tableModel;
  
  private List<Ticket> tickets;
  private JButton btnDelete;
  private JButton btnDeleteAll;
  private JButton btnOrderInfo;
  private Ticket ticket;
  
  public TicketExplorer()
  {
    setLayout(new BorderLayout());
    
    table = new JXTable();
    table.setRowHeight(PosUIManager.getSize(30));
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    tableModel = new TicketExplorerTableModel();
    
    table.setModel(tableModel);
    table.setAutoResizeMode(4);
    table.setRowHeight(PosUIManager.getSize(30));
    
    addTopPanel();
    add(new JScrollPane(table), "Center");
    addButtonPanel();
    
    refresh();
  }
  
  private void addTopPanel() {
    JPanel topPanel = new JPanel(new MigLayout());
    
    btnGo.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          TicketExplorer.this.refresh();
        } catch (Exception e1) {
          BOMessageDialog.showError(POSUtil.getBackOfficeWindow(), POSConstants.ERROR_MESSAGE, e1);
        }
        
      }
      
    });
    topPanel.add(new JLabel(POSConstants.FROM), "grow");
    topPanel.add(fromDatePicker, "gapright 10");
    topPanel.add(new JLabel(POSConstants.TO), "grow");
    topPanel.add(toDatePicker);
    topPanel.add(btnGo, "width 60!");
    add(topPanel, "North");
  }
  
  private void addButtonPanel()
  {
    btnOrderInfo = new JButton("Oreder Info");
    btnOrderInfo.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TicketExplorer.this.showOrderInfo();
      }
    });
    btnDelete = new JButton(POSConstants.DELETE);
    btnDelete.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        TicketExplorer.this.doDelete();
      }
      

    });
    btnDeleteAll = new JButton(POSConstants.DELETE_ALL);
    btnDeleteAll.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        TicketExplorer.this.doDeleteAll();
      }
      

    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(btnOrderInfo);
    panel.add(btnDelete);
    panel.add(btnDeleteAll);
    add(panel, "South");
  }
  
  class TicketExplorerTableModel extends ListTableModel<Ticket> {
    String[] columnNames = { POSConstants.ID, POSConstants.CREATED_BY.toUpperCase(), POSConstants.CREATE_TIME.toUpperCase(), POSConstants.SETTLE_TIME
      .toUpperCase(), POSConstants.SUBTOTAL.toUpperCase(), POSConstants.DISCOUNT.toUpperCase(), POSConstants.TAX
      .toUpperCase(), POSConstants.TOTAL, POSConstants.PAID, POSConstants.VOID.toUpperCase() };
    
    TicketExplorerTableModel() {}
    
    public String[] getColumnNames() { return columnNames; }
    

    public int getColumnCount()
    {
      return columnNames.length;
    }
    
    public String getColumnName(int column)
    {
      return columnNames[column];
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      Ticket ticket = (Ticket)rows.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return String.valueOf(ticket.getId());
      
      case 1: 
        return ticket.getOwner().toString();
      
      case 2: 
        return dateFormat.format(ticket.getCreateDate());
      
      case 3: 
        if (ticket.getClosingDate() != null) {
          return dateFormat.format(ticket.getClosingDate());
        }
        return "";
      
      case 4: 
        return Double.valueOf(ticket.getSubtotalAmount().doubleValue());
      
      case 5: 
        return Double.valueOf(ticket.getDiscountAmount().doubleValue());
      
      case 6: 
        return Double.valueOf(ticket.getTaxAmount().doubleValue());
      
      case 7: 
        return Double.valueOf(ticket.getTotalAmountWithTips().doubleValue());
      
      case 8: 
        return Boolean.valueOf(ticket.isPaid().booleanValue());
      
      case 9: 
        return Boolean.valueOf(ticket.isVoided().booleanValue());
      }
      return null;
    }
  }
  
  private void refresh() {
    if (tableModel.getRows() != null) {
      tableModel.getRows().clear();
    }
    
    Date fromDate = fromDatePicker.getDate();
    Date toDate = toDatePicker.getDate();
    
    fromDate = DateUtil.startOfDay(fromDate);
    toDate = DateUtil.endOfDay(toDate);
    
    TicketDAO dao = new TicketDAO();
    tickets = dao.findClosedTickets(fromDate, toDate);
    tableModel.setRows(tickets);
    table.repaint();
  }
  
  private void doDeleteAll() {
    try {
      List<Ticket> tickets = tableModel.getRows();
      
      if (tickets.isEmpty()) {
        return;
      }
      
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE_ALL) != 0)
      {
        return;
      }
      
      TicketDAO.getInstance().deleteTickets(tickets);
      refresh();
    }
    catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doDelete() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), POSConstants.SELECT_ONE_TICKET_TO_VOID);
        return;
      }
      
      index = table.convertRowIndexToModel(index);
      List<Ticket> tickets = new ArrayList();
      Ticket ticket = (Ticket)tableModel.getRows().get(index);
      tickets.add(ticket);
      
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0)
      {
        return;
      }
      
      TicketDAO.getInstance().deleteTickets(tickets);
      
      tableModel.deleteItem(index);
      table.repaint();
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void showOrderInfo() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), POSConstants.SELECT_ONE_TICKET_TO_VOID);
        return;
      }
      
      index = table.convertRowIndexToModel(index);
      Ticket ticket = (Ticket)tableModel.getRows().get(index);
      Ticket ticketFull = TicketDAO.getInstance().loadFullTicket(ticket.getId());
      List<Ticket> ticketsToShow = new ArrayList();
      ticketsToShow.add(ticketFull);
      try
      {
        OrderInfoView view = new OrderInfoView(ticketsToShow);
        
        OrderInfoDialog dialog = new OrderInfoDialog(view);
        dialog.setSize(600, 700);
        dialog.setDefaultCloseOperation(2);
        dialog.open();
      } catch (Exception e) {
        PosLog.error(getClass(), e);
      }
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, e);
    }
  }
}
