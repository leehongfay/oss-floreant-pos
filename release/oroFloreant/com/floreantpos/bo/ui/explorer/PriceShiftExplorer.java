package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.PriceShift;
import com.floreantpos.model.dao.PriceShiftDAO;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.model.PriceShiftEntryDialog;
import com.floreantpos.util.ShiftUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;

















public class PriceShiftExplorer
  extends TransparentPanel
{
  private JTable table;
  private PriceShiftTableModel tableModel;
  
  public PriceShiftExplorer()
  {
    List<PriceShift> shifts = new PriceShiftDAO().findAll();
    
    tableModel = new PriceShiftTableModel(shifts);
    table = new JTable(tableModel);
    
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          PriceShiftExplorer.this.editSelectedRow();
        }
      }
    });
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    table.setRowHeight(PosUIManager.getSize(30));
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          PriceShiftEntryDialog dialog = new PriceShiftEntryDialog();
          dialog.open();
          if (dialog.isCanceled())
            return;
          PriceShift shift = dialog.getPriceShift();
          tableModel.addItem(shift);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        PriceShiftExplorer.this.editSelectedRow();
      }
      
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0)
            return;
          PriceShift priceShift = (PriceShift)tableModel.getRowData(index);
          if (ConfirmDeleteDialog.showMessage(PriceShiftExplorer.this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0)
          {
            PriceShiftDAO.getInstance().releaseParentAndDelete(priceShift);
            tableModel.deleteItem(index);
          }
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    add(panel, "South");
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      PriceShift shift = (PriceShift)tableModel.getRowData(index);
      PriceShiftEntryDialog dialog = new PriceShiftEntryDialog();
      dialog.setPriceShift(shift);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      tableModel.updateItem(index);
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  class PriceShiftTableModel extends ListTableModel
  {
    PriceShiftTableModel(List list) {
      super(list);
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      PriceShift shift = (PriceShift)rows.get(rowIndex);
      if (shift == null) {
        return null;
      }
      switch (columnIndex) {
      case 0: 
        return shift.getName();
      
      case 1: 
        return shift.getDescription();
      
      case 2: 
        return shift.getDayOfWeekAsString();
      
      case 3: 
        return ShiftUtil.buildShiftTimeRepresentation(shift.getStartTime());
      
      case 4: 
        return ShiftUtil.buildShiftTimeRepresentation(shift.getEndTime());
      
      case 5: 
        return shift.getPriority();
      
      case 6: 
        return Boolean.valueOf(shift.isAnyDay());
      case 7: 
        return shift.isEnable();
      }
      return null;
    }
  }
}
