package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.config.AppProperties;
import com.floreantpos.model.InventoryStock;
import com.floreantpos.model.Outlet;
import com.floreantpos.model.dao.OutletDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import org.jdesktop.swingx.JXTable;








public class OutletExplorer
  extends TransparentPanel
{
  private static final long serialVersionUID = 1L;
  private JXTable table;
  private BeanTableModel<Outlet> tableModel;
  private JTextField tfName;
  private JComboBox cbLocation;
  
  public OutletExplorer()
  {
    tableModel = new BeanTableModel(Outlet.class);
    tableModel.addColumn("ID", "id");
    tableModel.addColumn("NAME", "name");
    tableModel.addColumn("DESCRIPTION", "description");
    tableModel.addColumn("ADDRESS", "address");
    
    tableModel.addRows(OutletDAO.getInstance().findAll());
    
    table = new JXTable(tableModel);
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.setRowHeight(PosUIManager.getSize(30));
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          OutletExplorer.this.editSelectedRow();
        }
        
      }
    });
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    add(createButtonPanel(), "South");
    add(buildSearchForm(), "North");
  }
  
  private TransparentPanel createButtonPanel()
  {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton editButton = explorerButton.getEditButton();
    JButton addButton = explorerButton.getAddButton();
    JButton deleteButton = explorerButton.getDeleteButton();
    
    addButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          OutletEntryForm editor = new OutletEntryForm(new Outlet());
          
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.setSize(PosUIManager.getSize(500, 600));
          dialog.setTitle(AppProperties.getAppName());
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          Outlet outlet = (Outlet)editor.getBean();
          
          tableModel.addRow(outlet);
        }
        catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    editButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        OutletExplorer.this.editSelectedRow();
      }
      
    });
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          
          if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
            return;
          }
          
          Outlet itemOutlet = (Outlet)tableModel.getRow(index);
          OutletDAO outletDAO = new OutletDAO();
          outletDAO.delete(itemOutlet);
          
          tableModel.removeRow(index);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    
    return panel;
  }
  
  private JPanel buildSearchForm() {
    JPanel panel = new JPanel();
    



























































    return panel;
  }
  
  private void searchItem()
  {
    String txName = tfName.getText();
    Object selectedGroup = cbLocation.getSelectedItem();
    
    List<InventoryStock> similarItem = null;
  }
  








  private void editSelectedRow()
  {
    try
    {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      

      Outlet outlet = (Outlet)tableModel.getRow(index);
      
      outlet = OutletDAO.getInstance().initialize(outlet);
      
      tableModel.setRow(index, outlet);
      
      OutletEntryForm editor = new OutletEntryForm(outlet);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.setSize(PosUIManager.getSize(500, 600));
      dialog.setTitle(AppProperties.getAppName());
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
}
