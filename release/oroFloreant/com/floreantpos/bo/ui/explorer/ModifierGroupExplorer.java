package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.ModifierGroup;
import com.floreantpos.model.dao.ModifierGroupDAO;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.model.ModifierGroupForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.table.AbstractTableModel;
import org.jdesktop.swingx.JXTable;


















public class ModifierGroupExplorer
  extends TransparentPanel
  implements ExplorerView
{
  private List<ModifierGroup> mGroupList;
  private JXTable table;
  private ModifierGroupExplorerTableModel tableModel;
  
  public ModifierGroupExplorer()
  {
    tableModel = new ModifierGroupExplorerTableModel();
    table = new JXTable(tableModel);
    table.setRowHeight(PosUIManager.getSize(30));
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    TransparentPanel panel = new TransparentPanel();
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton editButton = explorerButton.getEditButton();
    JButton addButton = explorerButton.getAddButton();
    JButton deleteButton = explorerButton.getDeleteButton();
    
    editButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          
          ModifierGroup category = (ModifierGroup)mGroupList.get(index);
          
          ModifierGroupForm editor = new ModifierGroupForm(category);
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          if (dialog.isCanceled()) {
            return;
          }
          table.repaint();
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    addButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          ModifierGroupForm editor = new ModifierGroupForm();
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          if (dialog.isCanceled())
            return;
          ModifierGroup modifierGroup = (ModifierGroup)editor.getBean();
          tableModel.addModifierGroup(modifierGroup);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      

    });
    deleteButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          
          if (ConfirmDeleteDialog.showMessage(ModifierGroupExplorer.this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 1)
          {
            ModifierGroup category = (ModifierGroup)mGroupList.get(index);
            ModifierGroupDAO modifierCategoryDAO = new ModifierGroupDAO();
            modifierCategoryDAO.delete(category);
            tableModel.deleteModifierGroup(category, index);
          }
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      

    });
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    add(panel, "South");
    table.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2)
          try {
            int index = table.getSelectedRow();
            if (index < 0)
              return;
            index = table.convertRowIndexToModel(index);
            ModifierGroup category = (ModifierGroup)mGroupList.get(index);
            ModifierGroupForm editor = new ModifierGroupForm(category);
            BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
            dialog.open();
            if (dialog.isCanceled()) {
              return;
            }
            table.repaint();
          } catch (Throwable x) {
            BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
          }
      }
    });
  }
  
  class ModifierGroupExplorerTableModel extends AbstractTableModel { ModifierGroupExplorerTableModel() {}
    
    String[] columnNames = { POSConstants.NAME, POSConstants.TRANSLATED_NAME, POSConstants.MODIFIERS };
    
    public int getRowCount() {
      if (mGroupList == null) {
        return 0;
      }
      return mGroupList.size();
    }
    
    public int getColumnCount() {
      return columnNames.length;
    }
    
    public String getColumnName(int column)
    {
      return columnNames[column];
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      return false;
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      if (mGroupList == null) {
        return "";
      }
      ModifierGroup mgroup = (ModifierGroup)mGroupList.get(rowIndex);
      
      switch (columnIndex)
      {
      case 0: 
        return mgroup.getName();
      

      case 1: 
        return mgroup.getTranslatedName();
      

      case 2: 
        return mgroup.getModifiers().toString();
      }
      return null;
    }
    
    public void addModifierGroup(ModifierGroup category) {
      int size = mGroupList.size();
      mGroupList.add(category);
      fireTableRowsInserted(size, size);
    }
    
    public void deleteModifierGroup(ModifierGroup category, int index)
    {
      mGroupList.remove(category);
      fireTableRowsDeleted(index, index);
    }
  }
  
  public void initData()
  {
    ModifierGroupDAO dao = new ModifierGroupDAO();
    dao.updateModifierGroupBooleanPropertyValue(true);
    mGroupList = dao.findNormalModifierGroups();
    tableModel.fireTableDataChanged();
  }
}
