package com.floreantpos.bo.ui.explorer;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.PizzaCrust;
import com.floreantpos.model.dao.PizzaCrustDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.model.PizzaCrustForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import org.jdesktop.swingx.JXTable;




















public class PizzaCrustExplorer
  extends TransparentPanel
{
  private JXTable table;
  private BeanTableModel<PizzaCrust> tableModel;
  
  public PizzaCrustExplorer()
  {
    tableModel = new BeanTableModel(PizzaCrust.class);
    
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "name");
    tableModel.addColumn(Messages.getString("PizzaCrustExplorer.0"), "translatedName");
    tableModel.addColumn(Messages.getString("PizzaCrustExplorer.1"), "description");
    tableModel.addColumn(Messages.getString("PizzaCrustExplorer.2"), "sortOrder");
    tableModel.addColumn("Default", PizzaCrust.PROP_DEFAULT_CRUST);
    
    tableModel.addRows(PizzaCrustDAO.getInstance().findAll());
    table = new JXTable(tableModel);
    table.setRowHeight(PosUIManager.getSize(30));
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          PizzaCrustForm editor = new PizzaCrustForm();
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          PizzaCrust foodCategory = (PizzaCrust)editor.getBean();
          tableModel.addRow(foodCategory);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          PizzaCrust pizzaCrust = (PizzaCrust)tableModel.getRow(index);
          
          PizzaCrustForm editor = new PizzaCrustForm(pizzaCrust);
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          if (dialog.isCanceled()) {
            return;
          }
          table.repaint();
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          if (ConfirmDeleteDialog.showMessage(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0)
          {
            PizzaCrust pizzaCrust = (PizzaCrust)tableModel.getRow(index);
            PizzaCrustDAO dao = new PizzaCrustDAO();
            dao.delete(pizzaCrust);
            tableModel.removeRow(index);
          }
        }
        catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton defaultButton = new JButton("Set Default");
    defaultButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          PizzaCrust selectedCrust = (PizzaCrust)tableModel.getRow(index);
          
          List<PizzaCrust> pizzaCrustList = new ArrayList();
          pizzaCrustList.add(selectedCrust);
          
          for (PizzaCrust item : tableModel.getRows()) {
            if (item.isDefaultCrust().booleanValue()) {
              item.setDefaultCrust(Boolean.valueOf(false));
              pizzaCrustList.add(item);
            }
          }
          selectedCrust.setDefaultCrust(Boolean.valueOf(true));
          
          PizzaCrustDAO dao = new PizzaCrustDAO();
          dao.saveOrUpdateList(pizzaCrustList);
          tableModel.fireTableDataChanged();
          table.revalidate();
          table.repaint();
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    panel.add(defaultButton);
    add(panel, "South");
  }
}
