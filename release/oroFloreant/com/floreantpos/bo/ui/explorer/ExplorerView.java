package com.floreantpos.bo.ui.explorer;

public abstract interface ExplorerView
{
  public abstract void initData();
}
