package com.floreantpos.bo.ui.explorer;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.main.Application;
import com.floreantpos.model.GiftCard;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.UserType;
import com.floreantpos.model.dao.GiftCardDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.GiftCardBatchNumberEntryDialog;
import com.floreantpos.ui.views.GiftCardGeneratorView;
import com.floreantpos.ui.views.GiftCardHolderNameEntryView;
import com.floreantpos.ui.views.GiftCardImportCheckingDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;


public class GiftCardExplorer
  extends TransparentPanel
{
  private JXTable table;
  private BeanTableModel<GiftCard> tableModel;
  private JTextField tfNumber;
  private Date fromDate;
  private Date toDate;
  private JXDatePicker fromDatePicker;
  private JXDatePicker toDatePicker;
  private Set<UserPermission> permissions;
  private Clipboard cb;
  private JButton copyButton;
  private JButton btnActive;
  private JButton btnDeactive;
  private JButton btnDisable;
  private JButton btnChangePinNumber;
  private JTextField tfBatchNumber;
  private JButton btnCopyBatch;
  private JButton btnDelete;
  private JButton btnExport;
  private JButton btnImport;
  private JButton btnBack;
  private JButton btnForward;
  private JLabel lblNumberOfItem;
  
  public GiftCardExplorer()
  {
    initComponents();
    searchItem();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    tableModel = new BeanTableModel(GiftCard.class);
    tableModel.addColumn(Messages.getString("GiftCardExplorer.42"), "customCardNumber");
    tableModel.addColumn(Messages.getString("GiftCardExplorer.44"), "ownerName");
    tableModel.addColumn(Messages.getString("GiftCardExplorer.46"), "pinNumber");
    tableModel.addColumn(Messages.getString("GiftCardExplorer.48"), "batchNo");
    tableModel.addColumn(Messages.getString("GiftCardExplorer.50"), "balance");
    tableModel.addColumn(Messages.getString("GiftCardExplorer.52"), "issueDate");
    tableModel.addColumn(Messages.getString("GiftCardExplorer.54"), "activationDate");
    tableModel.addColumn(Messages.getString("GiftCardExplorer.56"), "deActivationDate");
    tableModel.addColumn(Messages.getString("GiftCardExplorer.58"), "expiryDate");
    tableModel.addColumn(Messages.getString("GiftCardExplorer.60"), "active");
    tableModel.addColumn(Messages.getString("GiftCardExplorer.62"), "disable");
    tableModel.addColumn(Messages.getString("GiftCardExplorer.64"), "duration");
    tableModel.addColumn(Messages.getString("GiftCardExplorer.66"), "durationType");
    tableModel.addColumn(Messages.getString("GiftCardExplorer.68"), "point");
    
    table = new JXTable(tableModel);
    
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.setRowHeight(PosUIManager.getSize(30));
    
    btnBack = new JButton("<<< Previous");
    btnForward = new JButton("Next >>>");
    lblNumberOfItem = new JLabel();
    
    btnBack.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        tableModel.setCurrentRowIndex(tableModel.getPreviousRowIndex());
        GiftCardExplorer.this.searchItem();
      }
      
    });
    btnForward.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        tableModel.setCurrentRowIndex(tableModel.getNextRowIndex());
        GiftCardExplorer.this.searchItem();
      }
      
    });
    JPanel bottomPanel = new JPanel(new MigLayout("fillx"));
    bottomPanel.add(createButtonPanel(), "center");
    bottomPanel.add(lblNumberOfItem, "right, split 3");
    bottomPanel.add(btnBack);
    bottomPanel.add(btnForward);
    add(new JScrollPane(table));
    
    add(bottomPanel, "South");
    add(buildSearchForm(), "North");
    resizeColumnWidth(table);
    
    disableButtonList();
    
    table.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        if (table.getSelectedRow() < 0) {
          GiftCardExplorer.this.disableButtonList();
        }
        else {
          GiftCardExplorer.this.enableButtonList();
        }
      }
    });
  }
  
  private void copyCardNo()
  {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      cb = Toolkit.getDefaultToolkit().getSystemClipboard();
      StringBuilder cardNumber = new StringBuilder(table.getStringAt(index, 0));
      
      for (int i = 0; i < cardNumber.length(); i++) {
        if ((i == 4) || (i == 8) || (i == 12)) {
          cardNumber.deleteCharAt(i);
        }
      }
      
      Transferable tr = new StringSelection(cardNumber.toString());
      cb.setContents(tr, null);
      
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Card no. copied to clipboard.");
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void copyBatchNumber() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      cb = Toolkit.getDefaultToolkit().getSystemClipboard();
      StringBuilder batchNumber = new StringBuilder(table.getStringAt(index, 3));
      






      Transferable tr = new StringSelection(batchNumber.toString());
      cb.setContents(tr, null);
      
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("GiftCardExplorer.0"));
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doImportCSVFile() {
    try {
      JFileChooser fileChooser = new JFileChooser();
      fileChooser.setFileSelectionMode(0);
      fileChooser.setAcceptAllFileFilterUsed(false);
      fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("*.csv", new String[] { "csv" }));
      int result = fileChooser.showOpenDialog(this);
      if (result == 1) {
        return;
      }
      File selectedFile = fileChooser.getSelectedFile();
      if (selectedFile == null) {
        return;
      }
      
      GiftCardImportCheckingDialog dialog = new GiftCardImportCheckingDialog(selectedFile);
      dialog.openFullScreen();
      if (dialog.isCanceled()) {
        return;
      }
      List<GiftCard> giftCards = dialog.getGiftCards();
      
      GiftCardDAO.getInstance().saveAsList(giftCards);
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Data has been imported successfully.");
      searchItem();
    } catch (Exception e) {
      PosLog.error(GiftCardExplorer.class, e.getMessage(), e);
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Some error has been occured during import data!");
    }
  }
  
  public static String getSysClipboardText() {
    String ret = "";
    Clipboard sysClip = Toolkit.getDefaultToolkit().getSystemClipboard();
    
    Transferable clipTf = sysClip.getContents(null);
    
    if (clipTf != null)
    {
      if (clipTf.isDataFlavorSupported(DataFlavor.stringFlavor)) {
        try {
          ret = (String)clipTf.getTransferData(DataFlavor.stringFlavor);
        } catch (Exception e) {
          PosLog.error(GiftCardExplorer.class, e);
        }
      }
    }
    
    return ret;
  }
  
  private JPanel buildSearchForm() {
    JPanel panel = new JPanel();
    panel.setLayout(new MigLayout("", "[][]15[][]15[][]15[]", "[]5[]"));
    fromDatePicker = new JXDatePicker();
    toDatePicker = new JXDatePicker();
    
    JLabel lblNumber = new JLabel(Messages.getString("GiftCardExplorer.45"));
    tfNumber = new JTextField(15);
    
    JLabel lblBatchNumber = new JLabel(Messages.getString("GiftCardExplorer.47"));
    tfBatchNumber = new JTextField(15);
    
    try
    {
      JButton searchBttn = new JButton(Messages.getString("MenuItemExplorer.3"));
      JButton btnRefresh = new JButton(Messages.getString("GiftCardExplorer.1"));
      JButton btnClear = new JButton(Messages.getString("GiftCardExplorer.2"));
      
      panel.add(lblNumber, "align label");
      panel.add(tfNumber);
      panel.add(lblBatchNumber, "align label");
      panel.add(tfBatchNumber);
      panel.add(new JLabel(Messages.getString("GiftCardExplorer.3")));
      panel.add(new JLabel(Messages.getString("GiftCardExplorer.4")));
      panel.add(fromDatePicker);
      panel.add(new JLabel(Messages.getString("GiftCardExplorer.5")));
      panel.add(toDatePicker);
      panel.add(searchBttn);
      panel.add(btnRefresh);
      panel.add(btnClear);
      
      Border loweredetched = BorderFactory.createEtchedBorder(1);
      TitledBorder title = BorderFactory.createTitledBorder(loweredetched, Messages.getString("GiftCardExplorer.49"));
      title.setTitleJustification(1);
      panel.setBorder(title);
      
      tfNumber.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          GiftCardExplorer.this.searchItem();
        }
        

      });
      tfBatchNumber.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          GiftCardExplorer.this.searchItem();
        }
        

      });
      btnClear.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          fromDatePicker.getEditor().setText(null);
          toDatePicker.getEditor().setText(null);
          fromDatePicker.setDate(null);
          toDatePicker.setDate(null);
          tfNumber.setText("");
          tfBatchNumber.setText("");
          fromDate = null;
          toDate = null;
        }
        
      });
      searchBttn.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          GiftCardExplorer.this.searchItem();
        }
        
      });
      btnRefresh.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          GiftCardExplorer.this.searchItem();
        }
        
      });
      tfNumber.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          GiftCardExplorer.this.searchItem();
        }
      });
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
    
    return panel;
  }
  
  private void searchItem() {
    String txNumber = tfNumber.getText();
    String batchNumber = tfBatchNumber.getText();
    
    fromDate = fromDatePicker.getDate();
    toDate = toDatePicker.getDate();
    if ((fromDate != null) && (toDate != null) && 
      (fromDate.after(toDate))) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      return;
    }
    
    if (fromDate != null) {
      fromDate = DateUtil.startOfDay(fromDate);
    }
    if (toDate != null) {
      toDate = DateUtil.endOfDay(toDate);
    }
    
    if ((fromDate == null) && (toDate != null)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), Messages.getString("GiftCardExplorer.6"));
      return;
    }
    
    if ((fromDate != null) && (toDate == null)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), Messages.getString("GiftCardExplorer.7"));
      return;
    }
    
    GiftCardDAO.getInstance().searchByCardAndIssueDate(txNumber, batchNumber, fromDate, toDate, tableModel);
    
    int startNumber = tableModel.getCurrentRowIndex() + 1;
    int endNumber = tableModel.getNextRowIndex();
    int totalNumber = tableModel.getNumRows();
    if (endNumber > totalNumber) {
      endNumber = totalNumber;
    }
    lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
    btnBack.setEnabled(tableModel.hasPrevious());
    btnForward.setEnabled(tableModel.hasNext());
  }
  
  private TransparentPanel createButtonPanel() {
    copyButton = new JButton(Messages.getString("GiftCardExplorer.9"));
    JButton generateButton = new JButton(Messages.getString("GiftCardExplorer.10"));
    btnActive = new JButton(Messages.getString("GiftCardExplorer.11"));
    btnDeactive = new JButton(Messages.getString("GiftCardExplorer.12"));
    btnDisable = new JButton(Messages.getString("GiftCardExplorer.13"));
    btnChangePinNumber = new JButton(Messages.getString("GiftCardExplorer.14"));
    btnCopyBatch = new JButton(Messages.getString("GiftCardExplorer.15"));
    btnDelete = new JButton(Messages.getString("GiftCardExplorer.16"));
    btnImport = new JButton("Import CSV File");
    btnExport = new JButton(Messages.getString("GiftCardExplorer.17"));
    
    User user = Application.getCurrentUser();
    UserType newUserType = user.getType();
    
    if (newUserType != null) {
      permissions = newUserType.getPermissions();
    }
    
    btnImport.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        GiftCardExplorer.this.doImportCSVFile();
      }
      

    });
    btnExport.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          GiftCardExplorer.this.doExportToCSV();
        } catch (Exception e1) {
          POSMessageDialog.showError(GiftCardExplorer.this, e1.getMessage(), e1);
        }
        
      }
    });
    copyButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        GiftCardExplorer.this.copyCardNo();
      }
      

    });
    btnCopyBatch.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        GiftCardExplorer.this.copyBatchNumber();
      }
      

    });
    btnDelete.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        GiftCardExplorer.this.doDeleteCards();
      }
      
    });
    generateButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        GiftCardExplorer.this.doGenerateCard();
      }
      
    });
    btnChangePinNumber.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        GiftCardExplorer.this.doChangePinNumber();
      }
      

    });
    btnActive.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        GiftCardExplorer.this.doActiveCard();
      }
      

    });
    btnDeactive.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        GiftCardExplorer.this.doDeactiveCard();
      }
      

    });
    btnDisable.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        GiftCardExplorer.this.doDisableCard();
      }
      

    });
    TransparentPanel panel = new TransparentPanel();
    if ((permissions != null) && (permissions.contains(UserPermission.GENERATE_GIFT_CARD))) {
      panel.add(generateButton);
      panel.add(copyButton);
      panel.add(btnCopyBatch);
      panel.add(btnActive);
      panel.add(btnChangePinNumber);
      panel.add(btnDeactive);
      panel.add(btnDisable);
      panel.add(btnDelete);
      panel.add(btnImport);
      panel.add(btnExport);
    }
    
    return panel;
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(120));
    columnWidth.add(Integer.valueOf(150));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(85));
    columnWidth.add(Integer.valueOf(90));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(50));
    
    return columnWidth;
  }
  
  public static class CellTransferable implements Transferable
  {
    public static final DataFlavor CELL_DATA_FLAVOR = new DataFlavor(Object.class, "application/x-cell-value");
    private Object cellValue;
    
    public CellTransferable(Object cellValue)
    {
      this.cellValue = cellValue;
    }
    
    public DataFlavor[] getTransferDataFlavors()
    {
      return new DataFlavor[] { CELL_DATA_FLAVOR };
    }
    
    public boolean isDataFlavorSupported(DataFlavor flavor)
    {
      return CELL_DATA_FLAVOR.equals(flavor);
    }
    
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException
    {
      if (!isDataFlavorSupported(flavor)) {
        throw new UnsupportedFlavorException(flavor);
      }
      return cellValue;
    }
  }
  
  private void disableButtonList()
  {
    copyButton.setEnabled(false);
    btnActive.setEnabled(false);
    btnChangePinNumber.setEnabled(false);
    btnDeactive.setEnabled(false);
    btnDisable.setEnabled(false);
    btnCopyBatch.setEnabled(false);
  }
  
  private void enableButtonList()
  {
    copyButton.setEnabled(true);
    btnActive.setEnabled(true);
    btnChangePinNumber.setEnabled(true);
    btnDeactive.setEnabled(true);
    btnDisable.setEnabled(true);
    btnCopyBatch.setEnabled(true);
    btnExport.setEnabled(true);
  }
  
  private void doDeleteCards()
  {
    try {
      GiftCardBatchNumberEntryDialog dialog = new GiftCardBatchNumberEntryDialog();
      dialog.setTitle(Messages.getString("GiftCardExplorer.23"));
      dialog.setDefaultCloseOperation(2);
      dialog.setSize(PosUIManager.getSize(400, 300));
      dialog.setLocationRelativeTo(POSUtil.getFocusedWindow());
      dialog.setVisible(true);
      if (dialog.isCanceled()) {
        return;
      }
      
      if (dialog.isActive()) {
        POSMessageDialog.showError(POSUtil.getFocusedWindow(), Messages.getString("GiftCardExplorer.24"));
        return;
      }
      
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
        return;
      }
      
      GiftCardDAO.getInstance().deleteCardListByBatchNumber(dialog.getBatchNumber());
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("GiftCardExplorer.25"));
      searchItem();
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doExportToCSV() throws Exception {
    GiftCardDAO cardDAO = new GiftCardDAO();
    
    GiftCardBatchNumberEntryDialog dialog = new GiftCardBatchNumberEntryDialog();
    dialog.setTitle(Messages.getString("GiftCardExplorer.18"));
    dialog.setDefaultCloseOperation(2);
    dialog.setSize(PosUIManager.getSize(400, 300));
    dialog.setLocationRelativeTo(POSUtil.getFocusedWindow());
    dialog.setVisible(true);
    
    String batchNumberInput = dialog.getBatchNumber();
    if (dialog.isCanceled()) {
      return;
    }
    if (StringUtils.isEmpty(batchNumberInput)) {
      POSMessageDialog.showMessage(this, Messages.getString("GiftCardExplorer.19"));
      return;
    }
    
    List<GiftCard> giftCardList = cardDAO.findByBatchNumber(batchNumberInput);
    if (giftCardList == null) {
      POSMessageDialog.showMessage(this, Messages.getString("GiftCardExplorer.22") + batchNumberInput);
      return;
    }
    
    JFileChooser fileChooser = createGiftCardExportFileChooser(batchNumberInput);
    Integer option = Integer.valueOf(fileChooser.showSaveDialog(this));
    if (option.intValue() != 0) {
      return;
    }
    
    CSVPrinter csvFilePrinter = null;
    FileWriter writer = null;
    try {
      File file = fileChooser.getSelectedFile();
      writer = new FileWriter(file);
      
      CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator("\n");
      csvFilePrinter = new CSVPrinter(writer, csvFileFormat);
      Object[] headers = new Object[16];
      headers[0] = "Card Number";
      headers[1] = "Owner Name";
      headers[2] = "Balance";
      headers[3] = "Issue Date";
      headers[4] = "Activation Date";
      headers[5] = "Deactivation Date";
      headers[6] = "Expiration Date";
      headers[7] = "Active";
      headers[8] = "Disable";
      headers[9] = "Duration Type";
      headers[10] = "Duration";
      headers[11] = "Pin Number";
      headers[12] = "Point";
      headers[13] = "Batch Number";
      headers[14] = "Email";
      headers[15] = "Type";
      csvFilePrinter.printRecord(headers);
      for (GiftCard giftCard : giftCardList) {
        Object[] arry = new Object[16];
        arry[0] = giftCard.getCardNumber();
        arry[1] = giftCard.getOwnerName();
        arry[2] = giftCard.getBalance();
        arry[3] = (giftCard.getIssueDate() != null ? DateUtil.formatDateAsString(giftCard.getIssueDate()) : "");
        arry[4] = (giftCard.getActivationDate() != null ? DateUtil.formatDateAsString(giftCard.getActivationDate()) : "");
        arry[5] = (giftCard.getDeActivationDate() != null ? DateUtil.formatDateAsString(giftCard.getDeActivationDate()) : "");
        arry[6] = (giftCard.getExpiryDate() != null ? DateUtil.formatDateAsString(giftCard.getExpiryDate()) : "");
        arry[7] = giftCard.isActive();
        arry[8] = giftCard.isDisable();
        arry[9] = giftCard.getDurationType();
        arry[10] = giftCard.getDuration();
        arry[11] = giftCard.getPinNumber();
        arry[12] = giftCard.getPoint();
        arry[13] = giftCard.getBatchNo();
        arry[14] = giftCard.getEmail();
        arry[15] = giftCard.getType();
        
        csvFilePrinter.printRecord(arry);
      }
    } finally {
      IOUtils.closeQuietly(writer);
      if (csvFilePrinter != null) {
        csvFilePrinter.close();
      }
    }
  }
  
  private JFileChooser createGiftCardExportFileChooser(String batchNumberInput) {
    JFileChooser fileChooser = new JFileChooser();
    fileChooser.setFileSelectionMode(0);
    fileChooser.setMultiSelectionEnabled(false);
    fileChooser.setAcceptAllFileFilterUsed(false);
    fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("*.csv", new String[] { "csv" }));
    fileChooser.setSelectedFile(new File(Messages.getString("GiftCardExplorer.20") + batchNumberInput + ".csv"));
    fileChooser.setFileFilter(new FileFilter()
    {
      public String getDescription() {
        return Messages.getString("GiftCardExplorer.21");
      }
      
      public boolean accept(File f)
      {
        if (f.getName().endsWith(".csv")) {
          return true;
        }
        return false;
      }
    });
    return fileChooser;
  }
  
  private void doChangePinNumber() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      GiftCard giftCard = (GiftCard)tableModel.getRow(index);
      giftCard = GiftCardDAO.getInstance().initialize(giftCard);
      
      String pinNumber = JOptionPane.showInputDialog(POSUtil.getFocusedWindow(), Messages.getString("GiftCardExplorer.27"));
      if (!StringUtils.isEmpty(pinNumber)) {
        if (pinNumber.length() > 8) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), Messages.getString("GiftCardExplorer.28"));
          return;
        }
        giftCard.setPinNumber(pinNumber);
        GiftCardDAO.getInstance().saveOrUpdate(giftCard);
        
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("GiftCardExplorer.29"));
        searchItem();
      }
    } catch (Exception e2) {
      POSMessageDialog.showError(this, e2.getMessage(), e2);
    }
  }
  
  private void doActiveCard() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      GiftCard giftCard = (GiftCard)tableModel.getRow(index);
      giftCard = GiftCardDAO.getInstance().initialize(giftCard);
      
      int value = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), Messages.getString("GiftCardExplorer.30"), 
        Messages.getString("GiftCardExplorer.31"));
      
      if (value == 0) {
        if (giftCard.isActive().booleanValue()) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("GiftCardExplorer.32"));
          return;
        }
        if (giftCard.getOwnerName() == null) {
          GiftCardHolderNameEntryView dialog = new GiftCardHolderNameEntryView();
          dialog.setTitle(Messages.getString("GiftCardExplorer.33"));
          dialog.setDefaultCloseOperation(2);
          dialog.setSize(PosUIManager.getSize(400, 300));
          dialog.setLocationRelativeTo(POSUtil.getFocusedWindow());
          dialog.setVisible(true);
          if (dialog.isCanceled()) {
            return;
          }
          giftCard.setOwnerName(dialog.getCardHolderName());
        }
        
        Calendar c = Calendar.getInstance();
        Date activationDate = c.getTime();
        
        if (giftCard.getDurationType() != null) {
          if (giftCard.getDurationType().equals("DAY")) {
            c.add(5, giftCard.getDuration().intValue());
          }
          else if (giftCard.getDurationType().equals("MONTH")) {
            c.add(2, giftCard.getDuration().intValue());
          }
          else {
            c.add(1, giftCard.getDuration().intValue());
          }
        }
        Date expiryDate = c.getTime();
        
        giftCard.setActive(Boolean.valueOf(true));
        giftCard.setActivationDate(activationDate);
        giftCard.setDeActivationDate(null);
        giftCard.setExpiryDate(expiryDate);
        giftCard.setActive(Boolean.valueOf(true));
        giftCard.setDisable(Boolean.valueOf(false));
        
        GiftCardDAO.getInstance().saveOrUpdate(giftCard);
        
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("GiftCardExplorer.34"));
        searchItem();
      }
    } catch (Exception e2) {
      POSMessageDialog.showError(this, e2.getMessage(), e2);
    }
  }
  
  private void doDeactiveCard() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      GiftCard giftCard = (GiftCard)tableModel.getRow(index);
      giftCard = GiftCardDAO.getInstance().initialize(giftCard);
      
      int value = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), Messages.getString("GiftCardExplorer.35"), 
        Messages.getString("GiftCardExplorer.36"));
      
      if (value == 0) {
        if (!giftCard.isActive().booleanValue()) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("GiftCardExplorer.37"));
          return;
        }
        giftCard.setActive(Boolean.valueOf(false));
        giftCard.setDeActivationDate(new Date());
        
        GiftCardDAO.getInstance().saveOrUpdate(giftCard);
        
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("GiftCardExplorer.38"));
        searchItem();
      }
    } catch (Exception e2) {
      POSMessageDialog.showError(this, e2.getMessage(), e2);
    }
  }
  
  private void doDisableCard() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      GiftCard giftCard = (GiftCard)tableModel.getRow(index);
      giftCard = GiftCardDAO.getInstance().initialize(giftCard);
      
      int value = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), Messages.getString("GiftCardExplorer.39"), 
        Messages.getString("GiftCardExplorer.40"));
      
      if (value == 0) {
        giftCard.setDisable(Boolean.valueOf(true));
        giftCard.setActive(Boolean.valueOf(false));
        giftCard.setDeActivationDate(new Date());
        GiftCardDAO.getInstance().saveOrUpdate(giftCard);
        
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("GiftCardExplorer.41"));
        searchItem();
      }
    } catch (Exception e2) {
      POSMessageDialog.showError(this, e2.getMessage(), e2);
    }
  }
  
  private void doGenerateCard() {
    try {
      GiftCardGeneratorView dialog = new GiftCardGeneratorView();
      dialog.setTitle(Messages.getString("GiftCardExplorer.26"));
      dialog.setDefaultCloseOperation(2);
      dialog.setSize(PosUIManager.getSize(600, 400));
      dialog.setLocationRelativeTo(POSUtil.getFocusedWindow());
      dialog.setVisible(true);
      searchItem();
    } catch (Exception e2) {
      POSMessageDialog.showError(this, e2.getMessage(), e2);
    }
  }
}
