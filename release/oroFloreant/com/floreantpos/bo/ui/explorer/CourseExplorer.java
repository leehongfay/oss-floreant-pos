package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.Course;
import com.floreantpos.model.dao.CourseDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.model.CourseForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import org.jdesktop.swingx.JXTable;



















public class CourseExplorer
  extends TransparentPanel
  implements ExplorerView
{
  private JXTable table;
  private BeanTableModel<Course> tableModel;
  
  public CourseExplorer()
  {
    tableModel = new BeanTableModel(Course.class);
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), Course.PROP_NAME);
    tableModel.addColumn("SHORT NAME", Course.PROP_SHORT_NAME);
    tableModel.addColumn(POSConstants.SORT_ORDER.toUpperCase(), Course.PROP_SORT_ORDER);
    tableModel.addColumn("ICON", "icon");
    
    table = new JXTable(tableModel);
    table.setRowHeight(PosUIManager.getSize(30));
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          editSelectedRow();
        }
      }
    });
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    addButtonPanel();
    initData();
  }
  
  private void addButtonPanel() {
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          CourseForm editor = new CourseForm();
          editor.setExistingCourses(tableModel.getRows());
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.setPreferredSize(PosUIManager.getSize(500, 450));
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          Course foodCategory = (Course)editor.getBean();
          tableModel.addRow(foodCategory);
        }
        catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        editSelectedRow();
      }
      
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          Course course = (Course)tableModel.getRow(index);
          
          if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
            return;
          }
          CourseDAO dao = new CourseDAO();
          dao.delete(course);
          tableModel.removeRow(index);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    add(panel, "South");
  }
  
  public void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      Course course = (Course)tableModel.getRow(index);
      
      CourseForm editor = new CourseForm(course);
      editor.setExistingCourses(tableModel.getRows());
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.setPreferredSize(PosUIManager.getSize(500, 450));
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public void initData()
  {
    tableModel.setRows(CourseDAO.getInstance().findAll());
  }
}
