package com.floreantpos.bo.ui.explorer;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.actions.TerminalConfigurationAction;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.main.Application;
import com.floreantpos.model.InventoryStock;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import org.hibernate.exception.ConstraintViolationException;
import org.jdesktop.swingx.JXTable;





public class TerminalExplorer
  extends TransparentPanel
{
  private static final long serialVersionUID = 1L;
  private JXTable table;
  private BeanTableModel<Terminal> tableModel;
  private JTextField tfName;
  private JComboBox cbLocation;
  
  public TerminalExplorer()
  {
    setLayout(new BorderLayout(5, 5));
    tableModel = new BeanTableModel(Terminal.class);
    tableModel.addColumn(Messages.getString("TerminalExplorer.0"), "id");
    tableModel.addColumn(Messages.getString("TerminalExplorer.2"), "name");
    tableModel.addColumn(Messages.getString("TerminalExplorer.4"), "department");
    tableModel.addColumn(Messages.getString("TerminalExplorer.12"), "location");
    tableModel.addColumn(Messages.getString("TerminalExplorer.10"), "hasCashDrawer");
    tableModel.addColumn(Messages.getString("TerminalExplorer.1"), "autoLogOff");
    tableModel.addColumn(Messages.getString("TerminalExplorer.18"), "defaultPassLength");
    tableModel.addColumn(Messages.getString("TerminalExplorer.5"), "active");
    
    tableModel.addRows(TerminalDAO.getInstance().findAll());
    
    table = new JXTable(tableModel);
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    table.setRowHeight(PosUIManager.getSize(30));
    
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          TerminalExplorer.this.editSelectedRow();
        }
      }
    });
    table.getColumn(0).setCellRenderer(new IconRenderer());
    add(new JScrollPane(table));
    add(createButtonPanel(), "South");
    add(buildSearchForm(), "North");
  }
  
  private TransparentPanel createButtonPanel()
  {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton editButton = explorerButton.getEditButton();
    
    JButton deleteButton = explorerButton.getDeleteButton();
    
























    editButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TerminalExplorer.this.editSelectedRow();
      }
      
    });
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          
          if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0)
          {
            return;
          }
          Terminal term = (Terminal)tableModel.getRow(index);
          
          TerminalDAO termDao = new TerminalDAO();
          termDao.delete(term);
          
          tableModel.removeRow(index);
        } catch (ConstraintViolationException ex) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "This menu item is in use and cannot be deleted.");
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    

    panel.add(editButton);
    panel.add(deleteButton);
    
    return panel;
  }
  
  private JPanel buildSearchForm() {
    JPanel panel = new JPanel();
    



























































    return panel;
  }
  
  private void searchItem()
  {
    String txName = tfName.getText();
    Object selectedGroup = cbLocation.getSelectedItem();
    
    List<InventoryStock> similarItem = null;
  }
  








  private void editSelectedRow()
  {
    try
    {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      Terminal terminal = (Terminal)tableModel.getRow(index);
      terminal = TerminalDAO.getInstance().initialize(terminal);
      
      tableModel.setRow(index, terminal);
      TerminalConfigurationAction action = new TerminalConfigurationAction(terminal);
      action.actionPerformed(null);
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  class IconRenderer extends DefaultTableCellRenderer {
    IconRenderer() {}
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
      Terminal terminal = Application.getInstance().getTerminal();
      Terminal terminal2 = (Terminal)tableModel.getRow(row);
      JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      label.setIcon(null);
      
      if (terminal.equals(terminal2)) {
        label.setIcon(IconFactory.getIcon("/ui_icons/", "check_mark.png"));
        return label;
      }
      
      return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }
  }
}
