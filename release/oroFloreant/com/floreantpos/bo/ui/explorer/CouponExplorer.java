package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.Discount;
import com.floreantpos.model.dao.DiscountDAO;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.model.CouponForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
















public class CouponExplorer
  extends TransparentPanel
  implements ActionListener
{
  private JTable explorerView;
  private CouponExplorerTableModel explorerModel;
  
  public CouponExplorer()
  {
    explorerView = new JTable();
    explorerView.setRowHeight(PosUIManager.getSize(30));
    explorerView.setDefaultRenderer(Object.class, new PosTableRenderer());
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(explorerView));
    
    JButton addButton = new JButton(POSConstants.NEW);
    addButton.setActionCommand(POSConstants.ADD);
    addButton.addActionListener(this);
    
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.setActionCommand(POSConstants.EDIT);
    editButton.addActionListener(this);
    
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.setActionCommand(POSConstants.DELETE);
    deleteButton.addActionListener(this);
    
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    add(panel, "South");
  }
  
  public void initData() throws Exception {
    DiscountDAO dao = new DiscountDAO();
    List<Discount> couponList = dao.findAll();
    explorerModel = new CouponExplorerTableModel(couponList);
    explorerView.setModel(explorerModel);
  }
  
  private void addNewCoupon() {
    try {
      CouponForm editor = new CouponForm();
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.openWithScale(900, 500);
      
      if (dialog.isCanceled())
        return;
      Discount coupon = (Discount)editor.getBean();
      explorerModel.addCoupon(coupon);
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void editCoupon(Discount coupon) {
    try {
      CouponForm editor = new CouponForm(coupon);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.openWithScale(900, 500);
      if (dialog.isCanceled()) {
        return;
      }
      explorerView.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void deleteCoupon(int index, Discount coupon) {
    try {
      if (ConfirmDeleteDialog.showMessage(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0)
      {
        DiscountDAO.getInstance().delete(coupon);
        explorerModel.deleteCoupon(coupon, index);
      }
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private class CouponExplorerTableModel extends AbstractTableModel {
    String[] columnNames = { POSConstants.NAME, POSConstants.COUPON_TYPE, "DISCOUNT ON", "MINIMUM VALUE", "MAXIMUM UNIT", POSConstants.COUPON_VALUE, POSConstants.EXPIRY_DATE, POSConstants.ENABLED, POSConstants.NEVER_EXPIRE };
    
    List<Discount> couponList;
    
    CouponExplorerTableModel()
    {
      couponList = list;
    }
    
    public int getRowCount() {
      if (couponList == null) {
        return 0;
      }
      return couponList.size();
    }
    
    public int getColumnCount() {
      return columnNames.length;
    }
    
    public String getColumnName(int index)
    {
      return columnNames[index];
    }
    
    public Object getValueAt(int row, int column) {
      if (couponList == null) {
        return "";
      }
      Discount coupon = (Discount)couponList.get(row);
      switch (column) {
      case 0: 
        return coupon.getName();
      
      case 1: 
        return Discount.COUPON_TYPE_NAMES[coupon.getType().intValue()];
      case 2: 
        return Discount.COUPON_QUALIFICATION_NAMES[coupon.getQualificationType().intValue()];
      
      case 3: 
        return coupon.getMinimumBuy();
      case 4: 
        return coupon.getMaximumOff();
      
      case 5: 
        return Double.valueOf(coupon.getValue().doubleValue());
      
      case 6: 
        return coupon.getExpiryDate();
      
      case 7: 
        return Boolean.valueOf(coupon.isEnabled().booleanValue());
      
      case 8: 
        return Boolean.valueOf(coupon.isNeverExpire().booleanValue());
      }
      
      return null;
    }
    
    public void addCoupon(Discount coupon) {
      int size = couponList.size();
      couponList.add(coupon);
      fireTableRowsInserted(size, size);
    }
    
    public void deleteCoupon(Discount coupon, int index) {
      couponList.remove(coupon);
      fireTableRowsDeleted(index, index);
    }
    
    public Discount getCoupon(int index) {
      return (Discount)couponList.get(index);
    }
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    if (POSConstants.ADD.equals(actionCommand)) {
      addNewCoupon();
    }
    else if (POSConstants.EDIT.equals(actionCommand)) {
      int index = explorerView.getSelectedRow();
      if (index < 0) {
        BOMessageDialog.showError(POSConstants.SELECT_COUPON_TO_EDIT);
        return;
      }
      Discount coupon = explorerModel.getCoupon(index);
      DiscountDAO.getInstance().refresh(coupon);
      editCoupon(coupon);
    }
    else if (POSConstants.DELETE.equals(actionCommand)) {
      int index = explorerView.getSelectedRow();
      if (index < 0) {
        BOMessageDialog.showError(POSConstants.SELECT_COUPON_TO_DELETE);
        return;
      }
      Discount coupon = explorerModel.getCoupon(index);
      DiscountDAO.getInstance().refresh(coupon);
      deleteCoupon(index, coupon);
    }
  }
}
