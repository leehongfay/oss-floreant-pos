package com.floreantpos.bo.ui.explorer;

import java.awt.BorderLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class PriceExplorer extends JPanel
{
  private JTabbedPane mainTab;
  
  public PriceExplorer()
  {
    initComponents();
  }
  
  private void initComponents() {
    mainTab = new JTabbedPane();
    mainTab.setUI(new javax.swing.plaf.basic.BasicTabbedPaneUI());
    setLayout(new BorderLayout());
    final JComponent priceRublePanel = new JPanel(new BorderLayout());
    mainTab.addTab("Price Rules", priceRublePanel);
    priceRublePanel.add(new PriceRuleExplorer());
    final JComponent priceListPanel = new JPanel(new BorderLayout());
    mainTab.addTab("Price List", priceListPanel);
    final JComponent priceShiftPanel = new JPanel(new BorderLayout());
    mainTab.addTab("Price shifts", priceShiftPanel);
    add(mainTab);
    mainTab.addChangeListener(new ChangeListener()
    {
      public void stateChanged(ChangeEvent evt) {
        JTabbedPane mainTab = (JTabbedPane)evt.getSource();
        
        int selectedTabIndex = mainTab.getSelectedIndex();
        if (selectedTabIndex == 0) {
          if (priceRublePanel.getComponentCount() == 0) {
            priceRublePanel.add(new PriceRuleExplorer());
          }
        }
        else if (selectedTabIndex == 1) {
          if (priceListPanel.getComponentCount() == 0) {
            priceListPanel.add(new PriceTableExplorer());
          }
        } else if ((selectedTabIndex == 2) && 
          (priceShiftPanel.getComponentCount() == 0)) {
          priceShiftPanel.add(new PriceShiftExplorer());
        }
      }
    });
  }
}
