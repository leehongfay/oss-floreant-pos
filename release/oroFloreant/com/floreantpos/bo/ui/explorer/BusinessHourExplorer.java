package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.DayPart;
import com.floreantpos.model.Shift;
import com.floreantpos.model.dao.DayPartDAO;
import com.floreantpos.model.dao.ShiftDAO;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.model.ShiftEntryDialog;
import com.floreantpos.util.ShiftUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import org.hibernate.exception.ConstraintViolationException;


















public class BusinessHourExplorer
  extends TransparentPanel
{
  private JTable table;
  private ShiftTableModel tableModel;
  
  public BusinessHourExplorer()
  {
    List<DayPart> shifts = new DayPartDAO().findAll();
    
    tableModel = new ShiftTableModel(shifts);
    table = new JTable(tableModel);
    table.setRowHeight(PosUIManager.getSize(30));
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          editSelectedRow();
        }
      }
      
      private void editSelectedRow() {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          DayPart dayPart = (DayPart)tableModel.getRowData(index);
          ShiftEntryDialog dialog = new ShiftEntryDialog();
          dialog.setShift(dayPart);
          dialog.open();
          if (dialog.isCanceled()) {
            return;
          }
          tableModel.updateItem(index);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          ShiftEntryDialog dialog = new ShiftEntryDialog();
          dialog.open();
          if (dialog.isCanceled())
            return;
          Shift shift = dialog.getShift();
          tableModel.addItem(shift);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          DayPart shift = (DayPart)tableModel.getRowData(index);
          ShiftEntryDialog dialog = new ShiftEntryDialog();
          dialog.setShift(shift);
          dialog.open();
          if (dialog.isCanceled()) {
            return;
          }
          tableModel.updateItem(index);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          if (ConfirmDeleteDialog.showMessage(BusinessHourExplorer.this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0)
          {
            Shift shift = (Shift)tableModel.getRowData(index);
            ShiftDAO.getInstance().delete(shift);
            tableModel.deleteItem(index);
          }
        } catch (ConstraintViolationException ex) {
          BOMessageDialog.showError(POSConstants.SHIFT_ERROR, ex);
        } catch (Exception e2) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, e2);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    add(panel, "South");
  }
  
  class ShiftTableModel extends ListTableModel
  {
    ShiftTableModel(List list) {
      super(list);
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      DayPart shift = (DayPart)rows.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return String.valueOf(shift.getId());
      
      case 1: 
        return shift.getName();
      
      case 2: 
        return ShiftUtil.buildShiftTimeRepresentation(shift.getStartTime());
      
      case 3: 
        return ShiftUtil.buildShiftTimeRepresentation(shift.getEndTime());
      }
      return null;
    }
  }
}
