package com.floreantpos.bo.ui.explorer;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.ModifierMultiplierPrice;
import com.floreantpos.model.PizzaModifierPrice;
import com.floreantpos.model.dao.MenuItemModifierSpecDAO;
import com.floreantpos.model.dao.ModifierDAO;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.model.PizzaModifierForm;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.JXTable;



public class PizzaModifierExplorer
  extends TransparentPanel
{
  private String currencySymbol;
  private JXTable table;
  private PizzaModifierExplorerModel tableModel;
  private JTextField tfName;
  
  public PizzaModifierExplorer()
  {
    setLayout(new BorderLayout(5, 5));
    
    currencySymbol = CurrencyUtil.getCurrencySymbol();
    tableModel = new PizzaModifierExplorerModel();
    table = new JXTable(tableModel);
    table.setRowHeight(PosUIManager.getSize(30));
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.setSelectionMode(0);
    add(new JScrollPane(table));
    
    createActionButtons();
    add(buildSearchForm(), "North");
    
    updateModifierList();
    
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          PizzaModifierExplorer.this.editSelectedRow();
        }
      }
    });
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      MenuModifier modifier = (MenuModifier)tableModel.getRowData(index);
      
      PizzaModifierForm editor = new PizzaModifierForm(modifier);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void createActionButtons()
  {
    ExplorerButtonPanel explorerButtonPanel = new ExplorerButtonPanel();
    explorerButtonPanel.getAddButton().addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          PizzaModifierForm editor = new PizzaModifierForm();
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          if (dialog.isCanceled())
            return;
          MenuModifier modifier = (MenuModifier)editor.getBean();
          tableModel.addModifier(modifier);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    explorerButtonPanel.getEditButton().addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        PizzaModifierExplorer.this.editSelectedRow();
      }
      
    });
    explorerButtonPanel.getDeleteButton().addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          
          if (ConfirmDeleteDialog.showMessage(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 1)
          {
            MenuModifier category = (MenuModifier)tableModel.getRowData(index);
            ModifierDAO modifierDAO = new ModifierDAO();
            modifierDAO.delete(category);
            tableModel.deleteModifier(category, index);
          }
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton duplicateButton = new JButton("Duplicate");
    duplicateButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          
          MenuModifier existingModifier = (MenuModifier)tableModel.getRowData(index);
          
          MenuModifier newMenuModifier = new MenuModifier();
          PropertyUtils.copyProperties(newMenuModifier, existingModifier);
          newMenuModifier.setId(null);
          String newName = PizzaModifierExplorer.this.doDuplicateName(existingModifier);
          newMenuModifier.setName(newName);
          newMenuModifier.setPizzaModifier(Boolean.valueOf(true));
          newMenuModifier.setMultiplierPriceList(null);
          
          List<PizzaModifierPrice> pizzaModifierPriceList = existingModifier.getPizzaModifierPriceList();
          if (pizzaModifierPriceList != null) {
            List<PizzaModifierPrice> newPriceList = new ArrayList();
            for (PizzaModifierPrice price : pizzaModifierPriceList) {
              PizzaModifierPrice newPrice = new PizzaModifierPrice();
              PropertyUtils.copyProperties(newPrice, price);
              newPrice.setId(null);
              newPriceList.add(newPrice);
              List<ModifierMultiplierPrice> multiplierPriceList = newPrice.getMultiplierPriceList();
              if (multiplierPriceList != null) {
                List<ModifierMultiplierPrice> newMultiplierPriceList = new ArrayList();
                for (ModifierMultiplierPrice multiplierPrice : multiplierPriceList) {
                  ModifierMultiplierPrice newMultiplierPrice = new ModifierMultiplierPrice();
                  PropertyUtils.copyProperties(newMultiplierPrice, multiplierPrice);
                  newMultiplierPrice.setId(null);
                  newMultiplierPrice.setModifierId(newMenuModifier.getId());
                  newMultiplierPriceList.add(newMultiplierPrice);
                }
                newPrice.setMultiplierPriceList(newMultiplierPriceList);
              }
            }
            
            newMenuModifier.setPizzaModifierPriceList(newPriceList);
          }
          
          PizzaModifierForm editor = new PizzaModifierForm(newMenuModifier);
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          if (dialog.isCanceled()) {
            return;
          }
          MenuModifier menuModifier = (MenuModifier)editor.getBean();
          tableModel.addModifier(menuModifier);
          table.getSelectionModel().addSelectionInterval(tableModel.getRowCount() - 1, tableModel.getRowCount() - 1);
          table.scrollRowToVisible(tableModel.getRowCount() - 1);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    explorerButtonPanel.add(duplicateButton);
    add(explorerButtonPanel, "South");
  }
  
  private String doDuplicateName(MenuModifier existingModifier) {
    String existingName = existingModifier.getName();
    String newName = new String();
    int lastIndexOf = existingName.lastIndexOf(" ");
    if (lastIndexOf == -1) {
      newName = existingName + " 1";
    }
    else {
      String processName = existingName.substring(lastIndexOf + 1, existingName.length());
      if (StringUtils.isNumeric(processName)) {
        Integer count = Integer.valueOf(processName);
        count = Integer.valueOf(count.intValue() + 1);
        newName = existingName.replace(processName, String.valueOf(count));
        System.out.println(newName);
      }
      else {
        newName = existingName + " 1";
      }
    }
    return newName;
  }
  
  private JPanel buildSearchForm()
  {
    JPanel panel = new JPanel();
    panel.setLayout(new MigLayout("", "[][]30[][]30[]", "[]20[]"));
    
    JLabel nameLabel = new JLabel(Messages.getString("ModifierExplorer.3"));
    JLabel groupLabel = new JLabel(Messages.getString("ModifierExplorer.4"));
    tfName = new JTextField(15);
    tfName.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        updateModifierList();
      }
    });
    List<MenuItemModifierSpec> grpName = MenuItemModifierSpecDAO.getInstance().findAll();
    JComboBox cbGroup = new JComboBox();
    cbGroup.addItem(Messages.getString("ModifierExplorer.5"));
    for (MenuItemModifierSpec s : grpName) {
      cbGroup.addItem(s);
    }
    
    JButton searchBttn = new JButton(Messages.getString("ModifierExplorer.6"));
    panel.add(nameLabel, "align label,split 2");
    panel.add(tfName);
    

    panel.add(searchBttn);
    


    Border loweredetched = BorderFactory.createEtchedBorder(1);
    TitledBorder title = BorderFactory.createTitledBorder(loweredetched, Messages.getString("ModifierExplorer.8"));
    title.setTitleJustification(1);
    panel.setBorder(title);
    searchBttn.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        updateModifierList();
      }
    });
    return panel;
  }
  
  public synchronized void updateModifierList() {
    setModifierList(ModifierDAO.getInstance().getPizzaModifiers(tfName.getText()));
  }
  
  public void setModifierList(List<MenuModifier> modifierList) {
    tableModel.setRows(modifierList);
  }
  
  private class PizzaModifierExplorerModel
    extends ListTableModel
  {
    public PizzaModifierExplorerModel()
    {
      super();
    }
    

    public Object getValueAt(int rowIndex, int columnIndex)
    {
      List<MenuModifier> modifierList = getRows();
      
      MenuModifier modifier = (MenuModifier)modifierList.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return modifier.getName();
      
      case 1: 
        return modifier.getTranslatedName();
      






      case 2: 
        if (modifier.getTaxGroup() == null) {
          return "";
        }
        return modifier.getTaxGroup();
      
      case 3: 
        return "";
      
      case 4: 
        if (modifier.getButtonColor() != null) {
          return new Color(modifier.getButtonColor().intValue());
        }
        
        return null;
      
      case 5: 
        return modifier.getSortOrder();
      }
      return null;
    }
    
    public void addModifier(MenuModifier category) {
      int size = getRows().size();
      getRows().add(category);
      fireTableRowsInserted(size, size);
    }
    
    public void deleteModifier(MenuModifier category, int index)
    {
      getRows().remove(category);
      fireTableRowsDeleted(index, index);
    }
  }
}
