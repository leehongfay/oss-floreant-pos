package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.MenuShift;
import com.floreantpos.model.dao.MenuShiftDAO;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.model.MenuShiftEntryDialog;
import com.floreantpos.util.ShiftUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;

















public class MenuShiftExplorer
  extends TransparentPanel
{
  private JTable table;
  private MenuShiftTableModel tableModel;
  
  public MenuShiftExplorer()
  {
    List<MenuShift> shifts = new MenuShiftDAO().findAll();
    
    tableModel = new MenuShiftTableModel(shifts);
    table = new JTable(tableModel);
    
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          MenuShiftExplorer.this.editSelectedRow();
        }
      }
    });
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    table.setRowHeight(PosUIManager.getSize(30));
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          MenuShiftEntryDialog dialog = new MenuShiftEntryDialog();
          dialog.open();
          if (dialog.isCanceled())
            return;
          MenuShift shift = dialog.getCategoryShift();
          tableModel.addItem(shift);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MenuShiftExplorer.this.editSelectedRow();
      }
      
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0)
            return;
          MenuShift menuShift = (MenuShift)tableModel.getRowData(index);
          if (ConfirmDeleteDialog.showMessage(MenuShiftExplorer.this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0) {
            MenuShiftDAO.getInstance().delete(menuShift);
            tableModel.deleteItem(index);
          }
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    add(panel, "South");
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      MenuShift shift = (MenuShift)tableModel.getRowData(index);
      MenuShiftEntryDialog dialog = new MenuShiftEntryDialog();
      dialog.setMenuShift(shift);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      tableModel.updateItem(index);
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  class MenuShiftTableModel extends ListTableModel
  {
    MenuShiftTableModel(List list) {
      super(list);
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      MenuShift shift = (MenuShift)rows.get(rowIndex);
      if (shift == null) {
        return null;
      }
      switch (columnIndex) {
      case 0: 
        return shift.getName();
      
      case 1: 
        return shift.getDescription();
      
      case 2: 
        return shift.getDayOfWeekAsString();
      
      case 3: 
        return ShiftUtil.buildShiftTimeRepresentation(shift.getStartTime());
      
      case 4: 
        return ShiftUtil.buildShiftTimeRepresentation(shift.getEndTime());
      
      case 5: 
        return shift.getPriority();
      
      case 6: 
        return Boolean.valueOf(shift.isAnyDay());
      case 7: 
        return shift.isEnable();
      }
      return null;
    }
  }
}
