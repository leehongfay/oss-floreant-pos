package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.actions.StoreSessionReportAction;
import com.floreantpos.bo.actions.PosProgressDialog;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.main.Application;
import com.floreantpos.model.StoreSession;
import com.floreantpos.model.dao.StoreSessionDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.DrawerAndStaffBankReportHistoryDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.POSUtil;
import com.floreantpos.webservice.PosWebService;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;




















public class StoreSessionExplorer
  extends TransparentPanel
{
  private JXTable table;
  private BeanTableModel<StoreSession> tableModel;
  private JButton btnBack;
  private JButton btnForward;
  private JButton btnSync;
  private JLabel lblNumberOfItem;
  private JXDatePicker dpStartDate;
  private JXDatePicker dpEndDate;
  private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
  
  public StoreSessionExplorer() {
    init();
    tableModel.setNumRows(StoreSessionDAO.getInstance().rowCount());
    showStoreSession();
  }
  
  private void init() {
    tableModel = new BeanTableModel(StoreSession.class, 10)
    {
      public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 1) {
          return dateFormat.format(((StoreSession)tableModel.getRow(rowIndex)).getOpenTime());
        }
        if ((columnIndex == 2) && 
          (((StoreSession)tableModel.getRow(rowIndex)).getCloseTime() != null)) {
          return dateFormat.format(((StoreSession)tableModel.getRow(rowIndex)).getCloseTime());
        }
        return super.getValueAt(rowIndex, columnIndex);
      }
      
    };
    tableModel.addColumn("OPEN TIME", "openTime");
    tableModel.addColumn("CLOSE TIME", "closeTime");
    tableModel.addColumn("OPENED BY", "openedBy");
    tableModel.addColumn("CLOSED BY", "closedBy");
    
    btnBack = new JButton("<<< Previous");
    btnForward = new JButton("Next >>>");
    lblNumberOfItem = new JLabel();
    
    table = new JXTable(tableModel);
    table.setSortable(false);
    table.setDefaultRenderer(Object.class, new TableRenderer());
    
    table.setRowHeight(PosUIManager.getSize(30));
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          showReport();
        }
        
      }
    });
    btnBack.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        tableModel.setCurrentRowIndex(tableModel.getPreviousRowIndex());
        StoreSessionExplorer.this.showStoreSession();
      }
      
    });
    btnForward.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        tableModel.setCurrentRowIndex(tableModel.getNextRowIndex());
        StoreSessionExplorer.this.showStoreSession();
      }
      
    });
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    add(createButtonPanel(), "South");
    add(buildSearchForm(), "North");
    resizeColumnWidth(table);
  }
  
  private JPanel buildSearchForm()
  {
    JPanel panel = new JPanel();
    panel.setLayout(new MigLayout("", "[][]15[][]15[][]15[]", "[]5[]"));
    try
    {
      JLabel lblFromDate = new JLabel(POSConstants.START_DATE + ":");
      dpStartDate = UiUtil.getCurrentMonthStart();
      
      JLabel lblToDate = new JLabel(POSConstants.END_DATE + ":");
      dpEndDate = UiUtil.getCurrentMonthEnd();
      
      JButton btnRefresh = new JButton();
      
      btnRefresh.setText(POSConstants.SEARCH_ITEM_BUTTON_TEXT);
      btnRefresh.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent evt) {
          StoreSessionExplorer.this.searchItem();
        }
        
      });
      panel.add(lblFromDate);
      panel.add(dpStartDate);
      
      panel.add(lblToDate);
      panel.add(dpEndDate);
      
      panel.add(btnRefresh);
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
    
    return panel;
  }
  
  private void showStoreSession() {
    StoreSessionDAO.getInstance().loadStoreSession(tableModel);
    
    int startNumber = tableModel.getCurrentRowIndex() + 1;
    int endNumber = tableModel.getNextRowIndex();
    int totalNumber = tableModel.getNumRows();
    if (endNumber > totalNumber) {
      endNumber = totalNumber;
    }
    lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
    btnBack.setEnabled(tableModel.hasPrevious());
    btnForward.setEnabled(tableModel.hasNext());
  }
  
  private void searchItem() {
    Date fromDate = DateUtil.startOfDay(dpStartDate.getDate());
    Date toDate = DateUtil.endOfDay(dpEndDate.getDate());
    
    if (fromDate.after(toDate)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      return;
    }
    List<StoreSession> findSessions = StoreSessionDAO.getInstance().findSessions(fromDate, toDate);
    
    tableModel.removeAll();
    tableModel.addRows(findSessions);
  }
  
  private JPanel createButtonPanel()
  {
    JPanel bottomPanel = new JPanel(new MigLayout("fillx", "[33%][33%][33%]"));
    TransparentPanel actionButtonPanel = new TransparentPanel();
    
    JButton btnHistory = new JButton("History");
    btnHistory.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        int index = table.getSelectedRow();
        if (index < 0)
          return;
        index = table.convertRowIndexToModel(index);
        StoreSession storeSessioin = (StoreSession)tableModel.getRow(index);
        
        DrawerAndStaffBankReportHistoryDialog dialog = new DrawerAndStaffBankReportHistoryDialog(POSUtil.getBackOfficeWindow(), Application.getCurrentUser(), storeSessioin);
        dialog.setInfo("Store session history");
        dialog.setSize(PosUIManager.getSize(880, 580));
        dialog.open();
      }
    });
    actionButtonPanel.add(btnHistory, "split 4");
    
    JButton btnDetails = new JButton("Details");
    btnDetails.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        showReport();
      }
    });
    actionButtonPanel.add(btnDetails);
    
    bottomPanel.add(actionButtonPanel, "skip 1,center");
    
    btnSync = new JButton("Upload");
    btnSync.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        StoreSessionExplorer.this.uploadSessionData();
      }
    });
    actionButtonPanel.add(btnSync);
    
    JPanel navigationPanel = new JPanel(new FlowLayout(4));
    navigationPanel.add(lblNumberOfItem);
    navigationPanel.add(btnBack);
    navigationPanel.add(btnForward);
    bottomPanel.add(navigationPanel, "grow");
    
    return bottomPanel;
  }
  
  private void uploadSessionData()
  {
    int index = table.getSelectedRow();
    if (index < 0) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select data first.");
      return;
    }
    PosProgressDialog dialog = new PosProgressDialog()
    {
      public void doBackgroundTask() throws Exception
      {
        int index = table.getSelectedRow();
        index = table.convertRowIndexToModel(index);
        StoreSession storeSessioin = (StoreSession)tableModel.getRow(index);
        PosWebService.uploadStoreSessionData(storeSessioin);
      }
      
      public void done()
      {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Upload Complete.");
      }
      
      public void error(Exception e)
      {
        if (e.getCause().getMessage().contains("Connection refused")) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Connection failed.", e);
        }
        else
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
      }
    };
    dialog.setProgressLabelText("Uploading");
    dialog.pack();
    dialog.open();
  }
  
  protected void showReport() {
    try {
      int index = table.getSelectedRow();
      if (index < 0)
        return;
      index = table.convertRowIndexToModel(index);
      StoreSession storeSessioin = (StoreSession)tableModel.getRow(index);
      StoreSessionReportAction report = new StoreSessionReportAction(storeSessioin);
      report.execute();
    } catch (Exception e) {
      BOMessageDialog.showError(e);
    }
  }
  
  public void resizeColumnWidth(JTable table)
  {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    
    columnWidth.add(Integer.valueOf(200));
    columnWidth.add(Integer.valueOf(200));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(70));
    
    return columnWidth;
  }
  
  class TableRenderer extends DefaultTableCellRenderer {
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
    
    TableRenderer() {}
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) { JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      if ((value instanceof Date)) {
        String string = dateFormat.format(value);
        label.setText(string);
        label.setHorizontalAlignment(4);
      }
      return label;
    }
  }
}
