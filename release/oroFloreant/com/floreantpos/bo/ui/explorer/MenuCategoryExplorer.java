package com.floreantpos.bo.ui.explorer;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.model.MenuCategoryForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import org.jdesktop.swingx.JXTable;



















public class MenuCategoryExplorer
  extends TransparentPanel
  implements ExplorerView
{
  private JXTable table;
  private BeanTableModel<MenuCategory> tableModel;
  
  public MenuCategoryExplorer()
  {
    tableModel = new BeanTableModel(MenuCategory.class);
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "name");
    tableModel.addColumn(POSConstants.TRANSLATED_NAME.toUpperCase(), "translatedName");
    tableModel.addColumn(POSConstants.BEVERAGE.toUpperCase(), "beverage");
    tableModel.addColumn(POSConstants.VISIBLE.toUpperCase(), "visible");
    tableModel.addColumn(POSConstants.SORT_ORDER.toUpperCase(), "sortOrder");
    tableModel.addColumn(POSConstants.BUTTON_COLOR.toUpperCase(), "buttonColor");
    tableModel.addColumn(POSConstants.TEXT_COLOR.toUpperCase(), "textColor");
    
    table = new JXTable(tableModel);
    table.setRowHeight(PosUIManager.getSize(30));
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          editSelectedRow();
        }
      }
    });
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    addButtonPanel();
  }
  
  private void addButtonPanel() {
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          MenuCategoryForm editor = new MenuCategoryForm();
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.setPreferredSize(PosUIManager.getSize(600, 600));
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          MenuCategory foodCategory = (MenuCategory)editor.getBean();
          tableModel.addRow(foodCategory);
        }
        catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        editSelectedRow();
      }
      
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          MenuCategory category = (MenuCategory)tableModel.getRow(index);
          
          if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
            return;
          }
          
          MenuGroupDAO menuGroupDao = new MenuGroupDAO();
          boolean hasMenuGroups = menuGroupDao.existsMenuGroups(category);
          
          if ((hasMenuGroups) && 
            (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), 
            Messages.getString("MenuCategoryExplorer.0"), POSConstants.DELETE) != 0)) {
            return;
          }
          

          MenuCategoryDAO dao = new MenuCategoryDAO();
          dao.releaseParentAndDelete(category);
          
          tableModel.removeRow(index);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    add(panel, "South");
  }
  
  public void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      MenuCategory category = (MenuCategory)tableModel.getRow(index);
      
      MenuCategoryForm editor = new MenuCategoryForm(category);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.setPreferredSize(PosUIManager.getSize(600, 600));
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public void initData()
  {
    tableModel.setRows(MenuCategoryDAO.getInstance().findAll());
  }
}
