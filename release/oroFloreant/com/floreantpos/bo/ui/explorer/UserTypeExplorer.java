package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.UserType;
import com.floreantpos.model.dao.UserTypeDAO;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.forms.UserTypeForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;


















public class UserTypeExplorer
  extends TransparentPanel
{
  private List<UserType> typeList;
  private JTable table;
  private UserTypeExplorerTableModel tableModel;
  
  public UserTypeExplorer()
  {
    UserTypeDAO dao = new UserTypeDAO();
    typeList = dao.findAll();
    
    tableModel = new UserTypeExplorerTableModel();
    table = new JTable(tableModel);
    table.setRowHeight(PosUIManager.getSize(30));
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          UserTypeForm editor = new UserTypeForm();
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.setPreferredSize(PosUIManager.getSize(500, 450));
          dialog.pack();
          dialog.open();
          if (dialog.isCanceled())
            return;
          UserType type = (UserType)editor.getBean();
          tableModel.addType(type);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          UserType type = (UserType)typeList.get(index);
          
          UserTypeForm editor = new UserTypeForm(type);
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.setPreferredSize(PosUIManager.getSize(500, 450));
          dialog.pack();
          dialog.open();
          if (dialog.isCanceled()) {
            return;
          }
          table.repaint();
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          
          if (ConfirmDeleteDialog.showMessage(UserTypeExplorer.this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0) {
            UserType category = (UserType)typeList.get(index);
            UserTypeDAO dao = new UserTypeDAO();
            dao.delete(category);
            tableModel.deleteCategory(category, index);
          }
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    add(panel, "South"); }
  
  class UserTypeExplorerTableModel extends AbstractTableModel { UserTypeExplorerTableModel() {}
    
    String[] columnNames = { POSConstants.TYPE_NAME, POSConstants.PERMISSIONS };
    
    public int getRowCount() {
      if (typeList == null) {
        return 0;
      }
      return typeList.size();
    }
    
    public int getColumnCount() {
      return columnNames.length;
    }
    
    public String getColumnName(int column)
    {
      return columnNames[column];
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      return false;
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      if (typeList == null) {
        return "";
      }
      UserType userType = (UserType)typeList.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return userType.getName();
      
      case 1: 
        return userType.getPermissions();
      }
      return null;
    }
    
    public void addType(UserType type) {
      int size = typeList.size();
      typeList.add(type);
      fireTableRowsInserted(size, size);
    }
    
    public void deleteCategory(UserType type, int index) {
      typeList.remove(type);
      fireTableRowsDeleted(index, index);
    }
  }
}
