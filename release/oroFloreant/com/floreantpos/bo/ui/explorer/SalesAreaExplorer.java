package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.SalesArea;
import com.floreantpos.model.dao.SalesAreaDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.model.SalesAreaEntryForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;

public class SalesAreaExplorer
  extends TransparentPanel
{
  private JXTable table;
  private BeanTableModel<SalesArea> tableModel;
  private JTextField tfNumber;
  private Date fromDate;
  private Date toDate;
  private JXDatePicker fromDatePicker;
  private JXDatePicker toDatePicker;
  private JButton btnDelete;
  private JButton btnAdd;
  private JButton btnEdit;
  
  public SalesAreaExplorer()
  {
    tableModel = new BeanTableModel(SalesArea.class);
    tableModel.addColumn("NAME", "name");
    tableModel.addColumn("DEPARTMENT", "department");
    
    List<SalesArea> findAll = SalesAreaDAO.getInstance().findAll();
    tableModel.addRows(findAll);
    table = new JXTable(tableModel);
    
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.setRowHeight(PosUIManager.getSize(30));
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    add(createButtonPanel(), "South");
    add(buildSearchForm(), "North");
    resizeColumnWidth(table);
    
    table.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        if (table.getSelectedRow() < 0) {}
      }
    });
  }
  


  private JPanel buildSearchForm()
  {
    JPanel panel = new JPanel();
    















































































































    return panel;
  }
  


























  private void searchItem() {}
  

























  private TransparentPanel createButtonPanel()
  {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    btnAdd = explorerButton.getAddButton();
    btnEdit = explorerButton.getEditButton();
    btnDelete = explorerButton.getDeleteButton();
    
    btnEdit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        SalesAreaExplorer.this.editSelectedRow();
      }
      

    });
    btnAdd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          SalesArea item = new SalesArea();
          
          SalesAreaEntryForm editor = new SalesAreaEntryForm(item);
          
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          SalesArea faObj = (SalesArea)editor.getBean();
          
          tableModel.addRow(faObj);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    btnDelete.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          
          if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
            return;
          }
          SalesArea item = (SalesArea)tableModel.getRow(index);
          
          SalesAreaDAO salesAreaDAO = new SalesAreaDAO();
          
          salesAreaDAO.delete(item);
          
          tableModel.removeRow(index);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    
    panel.add(btnAdd);
    panel.add(btnEdit);
    panel.add(btnDelete);
    
    return panel;
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(150));
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(230));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(85));
    columnWidth.add(Integer.valueOf(90));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(50));
    
    return columnWidth;
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      SalesArea item = (SalesArea)tableModel.getRow(index);
      item = SalesAreaDAO.getInstance().find(item);
      
      tableModel.setRow(index, item);
      
      SalesAreaEntryForm editor = new SalesAreaEntryForm(item);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
}
