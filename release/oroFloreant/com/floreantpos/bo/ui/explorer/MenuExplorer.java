package com.floreantpos.bo.ui.explorer;

import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MenuExplorer extends JPanel
{
  private JTabbedPane mainTab;
  
  public MenuExplorer()
  {
    initComponents();
  }
  
  private void initComponents() {
    mainTab = new JTabbedPane();
    mainTab.setUI(new javax.swing.plaf.basic.BasicTabbedPaneUI());
    setLayout(new BorderLayout());
    MenuItemExplorer menuItemExplorer = new MenuItemExplorer();
    mainTab.addTab("Menu Items", menuItemExplorer);
    mainTab.addTab("Menu Categories", new MenuCategoryExplorer());
    mainTab.addTab("Menu Groups", new MenuGroupExplorer());
    mainTab.addTab("Menu Modifier Groups", new ModifierGroupExplorer());
    mainTab.addTab("Menu Modifiers", new ModifierExplorer());
    mainTab.addTab("Variants", new MenuItemExplorer(true));
    add(mainTab);
    mainTab.addChangeListener(new ChangeListener()
    {
      public void stateChanged(ChangeEvent evt) {
        JTabbedPane mainTab = (JTabbedPane)evt.getSource();
        Component selectedComponent = mainTab.getSelectedComponent();
        if ((selectedComponent instanceof ExplorerView)) {
          ((ExplorerView)selectedComponent).initData();
        }
      }
    });
    menuItemExplorer.initData();
  }
}
