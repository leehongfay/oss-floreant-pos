package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.PriceTable;
import com.floreantpos.model.dao.PriceTableDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.model.PriceTableForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import org.jdesktop.swingx.JXTable;





















public class PriceTableExplorer
  extends TransparentPanel
{
  private JXTable table;
  private BeanTableModel<PriceTable> tableModel;
  
  public PriceTableExplorer()
  {
    tableModel = new BeanTableModel(PriceTable.class);
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), PriceTable.PROP_NAME);
    tableModel.addColumn(POSConstants.DESCRIPTION.toUpperCase(), PriceTable.PROP_DESCRIPTION);
    tableModel.addColumn("LAST UPDATED TIME", PriceTable.PROP_LAST_UPDATED_TIME);
    tableModel.addColumn("UPDATED BY", PriceTable.PROP_LAST_UPDATED_BY);
    
    tableModel.addRows(PriceTableDAO.getInstance().findAll());
    
    table = new JXTable(tableModel);
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          openPriceList(false);
        }
      }
    });
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.setRowHeight(PosUIManager.getSize(30));
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    table.getColumn(2).setCellRenderer(new DefaultTableCellRenderer() {
      private SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, YYYY hh:mm a");
      
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
      {
        Date lastUpdateTime = (Date)value;
        setHorizontalAlignment(0);
        if (lastUpdateTime != null) {
          value = dateFormat.format(lastUpdateTime);
        }
        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      }
      
    });
    addButtonPanel();
  }
  
  private void addButtonPanel() {
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          PriceTableForm editor = new PriceTableForm(new PriceTable());
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.openWithScale(650, 600);
          PriceTable priceTable = (PriceTable)editor.getBean();
          if ((dialog.isCanceled()) && (priceTable.getId() == null)) {
            return;
          }
          tableModel.addRow(priceTable);
        }
        catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton detailsButton = new JButton(POSConstants.DETAILS);
    detailsButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        openPriceList(true);
      }
      

    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        openPriceList(false);
      }
      
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          PriceTable priceTable = (PriceTable)tableModel.getRow(index);
          
          if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0)
          {
            return;
          }
          
          PriceTableDAO dao = new PriceTableDAO();
          dao.releaseParentAndDelete(priceTable);
          
          tableModel.removeRow(index);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(detailsButton);
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    add(panel, "South");
  }
  
  public void openPriceList(boolean viewMode) {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      PriceTable priceTable = (PriceTable)tableModel.getRow(index);
      
      PriceTableForm editor = new PriceTableForm(priceTable);
      editor.setEditable(!viewMode);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      if (viewMode)
        dialog.getButtonPanel().remove(0);
      dialog.openWithScale(650, 600);
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
}
