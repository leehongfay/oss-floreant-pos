package com.floreantpos.bo.ui.explorer;

import com.floreantpos.Messages;
import com.floreantpos.model.Department;
import com.floreantpos.model.Outlet;
import com.floreantpos.model.SalesArea;
import com.floreantpos.model.dao.DepartmentDAO;
import com.floreantpos.model.dao.OutletDAO;
import com.floreantpos.model.dao.SalesAreaDAO;
import com.floreantpos.swing.CheckBoxList;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import net.miginfocom.swing.MigLayout;



public class DepartmentEntryForm
  extends BeanEditor<Department>
{
  DefaultMutableTreeNode path;
  private Department dept;
  private POSTextField tfDeptName;
  private POSTextField tfAddress;
  private POSTextField tfDesc;
  private CheckBoxList saleAreaList = new CheckBoxList();
  private CheckBoxList outletList = new CheckBoxList();
  
  public DepartmentEntryForm(Department dept) {
    this.dept = dept;
    DepartmentDAO.getInstance().initialize(dept);
    
    setLayout(new BorderLayout());
    createUI();
    setBean(dept);
  }
  
  public DepartmentEntryForm() {
    setLayout(new BorderLayout());
    createUI();
  }
  
  private void createUI() {
    JPanel itemInfoPanel = new JPanel();
    



    add(itemInfoPanel, "Center");
    
    itemInfoPanel.setLayout(new MigLayout("", "[][grow][]", ""));
    
    JLabel lblDeptName = new JLabel(Messages.getString("DepartmentEntryForm.3"));
    itemInfoPanel.add(lblDeptName, "cell 0 0,alignx trailing");
    
    tfDeptName = new POSTextField();
    itemInfoPanel.add(tfDeptName, "cell 1 0,growx");
    
    JLabel lblAddress = new JLabel(Messages.getString("DepartmentEntryForm.6"));
    itemInfoPanel.add(lblAddress, "cell 0 1,alignx trailing");
    
    tfAddress = new POSTextField();
    itemInfoPanel.add(tfAddress, "cell 1 1,growx");
    
    JLabel lblDesc = new JLabel(Messages.getString("DepartmentEntryForm.9"));
    itemInfoPanel.add(lblDesc, "cell 0 2,alignx trailing");
    
    tfDesc = new POSTextField();
    itemInfoPanel.add(tfDesc, "cell 1 2,growx");
    OutletDAO outletDAO = new OutletDAO();
    List<Outlet> outletTypes = outletDAO.findAll();
    if (outletTypes == null) {
      outletTypes = new ArrayList();
    }
    outletList.setModel(outletTypes);
    TitledBorder titleOutlet = new TitledBorder("Outlets");
    JScrollPane outletPane = new JScrollPane(outletList);
    outletPane.setBorder(titleOutlet);
    itemInfoPanel.add(outletPane, "cell 0 3,growx,span 2,center");
    List<SalesArea> saleAreaTypes = SalesAreaDAO.getInstance().findAll();
    if (saleAreaTypes == null) {
      return;
    }
    saleAreaList.setModel(saleAreaTypes);
    saleAreaList.setEnabled(false);
    TitledBorder titelSalesArea = new TitledBorder("Sales Area");
    JScrollPane salesAreaPane = new JScrollPane(saleAreaList);
    salesAreaPane.setBorder(titelSalesArea);
    itemInfoPanel.add(salesAreaPane, "cell 0 4,growx,span 2,center");
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      
      Department deptItem = (Department)getBean();
      DepartmentDAO deptDAO = new DepartmentDAO();
      deptDAO.saveOrUpdate(deptItem);
      
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(e.getMessage());
    }
    
    return false;
  }
  
  public void updateView() {
    Department dept = (Department)getBean();
    
    if (dept == null)
    {
      return;
    }
    tfDeptName.setText(dept.getName());
    tfAddress.setText(dept.getAddress());
    tfDesc.setText(dept.getDescription());
    outletList.selectItems(dept.getOutlets());
    saleAreaList.selectItems(SalesAreaDAO.getInstance().findSalesAreaByDept(dept));
  }
  
  public boolean updateModel() {
    Department dept = (Department)getBean();
    
    String deptName = tfDeptName.getText();
    if (deptName != null) {
      dept.setName(deptName);
    }
    else {
      POSMessageDialog.showMessage(Messages.getString("DepartmentEntryForm.12"));
    }
    dept.setAddress(tfAddress.getText());
    dept.setDescription(tfDesc.getText());
    dept.setOutlets(outletList.getCheckedValues());
    return true;
  }
  
  public String getDisplayText()
  {
    if (dept.getId() == null) {
      return "Add Department";
    }
    
    return "Edit Department";
  }
}
