package com.floreantpos.bo.ui.explorer;

public abstract interface PageSelectionListener
{
  public abstract void itemSelected(Object paramObject);
}
