package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.Attribute;
import com.floreantpos.model.AttributeGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.dialog.VariantAttributesSelectionDialog;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;















public class MenuItemVariantExplorer
  extends TransparentPanel
  implements ActionListener
{
  private JTable varientTable;
  private final MenuItemExplorerTableModel varientModel;
  private MenuItem parentMenuItem;
  
  public MenuItemVariantExplorer()
  {
    initComponents();
    
    varientModel = new MenuItemExplorerTableModel();
    varientTable.setModel(varientModel);
    varientTable.setDefaultRenderer(Object.class, new DefaultTableCellRenderer()
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (column == 4) {
          setHorizontalAlignment(0);
        } else if (column == 3) {
          setHorizontalAlignment(4);
        } else
          setHorizontalAlignment(2);
        return c;
      }
    });
    resizeTableColumns();
    varientTable.getInputMap().put(KeyStroke.getKeyStroke(32, 0), "startEditing");
    
    FixedLengthTextField tfBarcode = new FixedLengthTextField();
    DefaultCellEditor editorBarcode = new DefaultCellEditor(tfBarcode);
    editorBarcode.setClickCountToStart(1);
    
    FixedLengthTextField tfDisplayText = new FixedLengthTextField();
    DefaultCellEditor editorDisplayText = new DefaultCellEditor(tfDisplayText);
    editorDisplayText.setClickCountToStart(1);
    
    DoubleTextField tfPrice = new DoubleTextField();
    tfPrice.setAllowNegativeValue(true);
    tfPrice.setHorizontalAlignment(4);
    DefaultCellEditor editorPrice = new DefaultCellEditor(tfPrice);
    editorPrice.setClickCountToStart(1);
    
    JCheckBox chkVisible = new JCheckBox();
    chkVisible.setHorizontalAlignment(0);
    DefaultCellEditor editorEnable = new DefaultCellEditor(chkVisible);
    editorEnable.setClickCountToStart(1);
    
    varientTable.setDefaultEditor(varientTable.getColumnClass(1), editorDisplayText);
    varientTable.setDefaultEditor(varientTable.getColumnClass(2), editorBarcode);
    varientTable.setDefaultEditor(varientTable.getColumnClass(3), editorPrice);
    varientTable.setDefaultEditor(varientTable.getColumnClass(4), editorEnable);
  }
  
  private void initComponents() {
    setLayout(new MigLayout("fill"));
    setBorder(new EmptyBorder(10, 10, 10, 10));
    
    varientTable = new JTable() {
      public void changeSelection(int row, int column, boolean toggle, boolean extend) {
        super.changeSelection(row, column, toggle, extend);
        varientTable.editCellAt(row, column);
        varientTable.transferFocus();
        DefaultCellEditor editor = (DefaultCellEditor)varientTable.getCellEditor(row, column);
        if (column == 4) {
          JCheckBox checkBox = (JCheckBox)editor.getComponent();
          checkBox.requestFocus();
        }
        else if (column == 3) {
          DoubleTextField textField = (DoubleTextField)editor.getComponent();
          textField.requestFocus();
          textField.selectAll();
        }
        else if (column == 2) {
          FixedLengthTextField textField = (FixedLengthTextField)editor.getComponent();
          textField.requestFocus();
          textField.selectAll();
        }
        else if (column == 1) {
          FixedLengthTextField textField = (FixedLengthTextField)editor.getComponent();
          textField.setLength(255);
          textField.requestFocus();
          textField.selectAll();
        }
        
      }
    };
    varientTable.setRowHeight(PosUIManager.getSize(40));
    


    JButton addButton = new JButton("Select Attributes");
    addButton.setActionCommand(POSConstants.ADD);
    addButton.addActionListener(this);
    
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.setActionCommand(POSConstants.DELETE);
    deleteButton.addActionListener(this);
    
    TransparentPanel varientActionPanel = new TransparentPanel(new MigLayout("inset 0 0 10 0"));
    varientActionPanel.add(addButton);
    varientActionPanel.add(deleteButton);
    
    add(new JScrollPane(varientTable), "grow");
    add(varientActionPanel, "grow,newline");
  }
  
  private void resizeTableColumns() {
    varientTable.setAutoResizeMode(3);
    setColumnWidth(0, PosUIManager.getSize(200));
    setColumnWidth(1, PosUIManager.getSize(300));
    setColumnWidth(2, PosUIManager.getSize(180));
    setColumnWidth(3, PosUIManager.getSize(120));
    setColumnWidth(4, PosUIManager.getSize(100));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = varientTable.getColumnModel().getColumn(columnNumber);
    column.setPreferredWidth(width);
  }
  

  public void setParentMenuItem(MenuItem menuItem)
  {
    parentMenuItem = menuItem;
    varientModel.setItems(menuItem.getVariants());
  }
  
  public void actionPerformed(ActionEvent e)
  {
    String actionCommand = e.getActionCommand();
    if (POSConstants.ADD.equals(actionCommand)) {
      addNewVariant();
    }
    else if (POSConstants.DELETE.equals(actionCommand)) {
      int index = varientTable.getSelectedRow();
      if (index < 0) {
        BOMessageDialog.showError(POSConstants.SELECT_ITEM_TO_DELETE);
        return;
      }
      MenuItem menuItem = varientModel.getMenuItem(index);
      deleteMenuItem(index, menuItem);
    }
  }
  
  private void addNewVariant() {
    try {
      VariantAttributesSelectionDialog dialog = new VariantAttributesSelectionDialog(varientModel.getItems());
      dialog.setSize(PosUIManager.getSize(550, 450));
      dialog.setParentMenuItem(parentMenuItem);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      List<MenuItem> varients = dialog.getVariants();
      varientModel.getItems().clear();
      varientModel.setItems(varients);
      varientModel.fireTableDataChanged();
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void deleteMenuItem(int index, MenuItem varient) {
    try {
      if (ConfirmDeleteDialog.showMessage(this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0) {
        varientModel.deleteItem(varient, index);
      }
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private class MenuItemExplorerTableModel extends AbstractTableModel {
    String[] columnNames = { "Attributes", "Display Text", "Barcode", "Attribute Price Extra", "Enable" };
    List<MenuItem> varientList;
    
    public MenuItemExplorerTableModel() {
      varientList = new ArrayList();
    }
    
    public void setItems(List<MenuItem> variants) {
      if (variants == null)
        return;
      varientList.addAll(variants);
    }
    
    public List<MenuItem> getItems() {
      return varientList;
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      if ((columnIndex == 1) || (columnIndex == 2) || (columnIndex == 3) || (columnIndex == 4))
        return true;
      return false;
    }
    
    public int getRowCount()
    {
      if (varientList == null) {
        return 0;
      }
      return varientList.size();
    }
    
    public int getColumnCount()
    {
      return columnNames.length;
    }
    
    public String getColumnName(int index)
    {
      return columnNames[index];
    }
    
    public Class<?> getColumnClass(int columnIndex)
    {
      if (columnIndex == 4)
        return Boolean.class;
      if (columnIndex == 3) {
        return Double.class;
      }
      return String.class;
    }
    

    public Object getValueAt(int row, int column)
    {
      if (varientList == null) {
        return "";
      }
      MenuItem varient = (MenuItem)varientList.get(row);
      if (varient == null) {
        return "";
      }
      
      switch (column) {
      case 0: 
        String attString = "<html>";
        List<Attribute> attributes = varient.getAttributes();
        if (attributes == null)
          return attString;
        for (Iterator iterator = attributes.iterator(); iterator.hasNext();) {
          Attribute attribute = (Attribute)iterator.next();
          attString = attString + attribute.getGroup().getName() + ": <font style='color:red'>" + attribute.getName() + "</font>";
          if (iterator.hasNext()) {
            attString = attString + ",&nbsp;";
          }
        }
        
        attString = attString + "</html>";
        return attString;
      case 1: 
        return varient.getTranslatedName();
      case 2: 
        return varient.getBarcode();
      case 3: 
        return varient.getPrice();
      case 4: 
        return varient.isVisible();
      }
      return null;
    }
    
    public void setValueAt(Object aValue, int rowIndex, int columnIndex)
    {
      MenuItem varient = (MenuItem)varientList.get(rowIndex);
      if (varient == null) {
        return;
      }
      if (columnIndex == 1) {
        varient.setTranslatedName(String.valueOf(aValue));
      }
      else if (columnIndex == 2) {
        varient.setBarcode(String.valueOf(aValue));
      }
      else if (columnIndex == 3) {
        String priceString = (String)aValue;
        if (priceString.isEmpty())
          return;
        double price = Double.parseDouble(priceString);
        varient.setPrice(Double.valueOf(price));
      }
      else if (columnIndex == 4) {
        Boolean enable = (Boolean)aValue;
        if (enable == null)
          return;
        varient.setVisible(enable);
      }
      super.setValueAt(aValue, rowIndex, columnIndex);
    }
    
    public void deleteItem(MenuItem varient, int index) {
      for (Iterator iterator = varientList.iterator(); iterator.hasNext();) {
        MenuItem item = (MenuItem)iterator.next();
        if (varient == item) {
          iterator.remove();
        }
      }
      fireTableRowsDeleted(index, index);
    }
    
    public MenuItem getMenuItem(int index) {
      return (MenuItem)varientList.get(index);
    }
  }
  
  public List<MenuItem> getVariants() {
    List<MenuItem> variants = varientModel.getItems();
    if (variants != null)
    {
      for (MenuItem variant : variants)
      {
        variant.setUnit(parentMenuItem.getUnit());
      }
    }
    return variants;
  }
}
