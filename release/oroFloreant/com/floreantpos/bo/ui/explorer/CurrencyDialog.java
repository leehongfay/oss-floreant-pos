package com.floreantpos.bo.ui.explorer;

import com.floreantpos.Messages;
import com.floreantpos.model.Currency;
import com.floreantpos.model.dao.CurrencyDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.util.List;
import javax.swing.JPanel;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class CurrencyDialog
  extends OkCancelOptionDialog
{
  private CurrencyExplorer currencyExplorer;
  
  public CurrencyDialog()
  {
    JPanel contentPanel = getContentPanel();
    setTitle(Messages.getString("CurrencyDialog.0"));
    setCaption(Messages.getString("CurrencyDialog.1"));
    currencyExplorer = new CurrencyExplorer();
    contentPanel.add(currencyExplorer);
  }
  
  public void doOk()
  {
    List<Currency> currencyList = currencyExplorer.getModel().getRows();
    
    Currency mainCurrency = null;
    boolean isMainSelected = false;
    for (Currency currency : currencyList) {
      if (currency.isMain().booleanValue()) {
        isMainSelected = true;
        mainCurrency = currency;
      }
    }
    
    if (!isMainSelected) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("CurrencyDialog.2"));
      return;
    }
    
    if (mainCurrency.getExchangeRate().doubleValue() != 1.0D) {
      if (POSMessageDialog.showYesNoQuestionDialog(this, Messages.getString("CurrencyDialog.3"), Messages.getString("CurrencyDialog.4")) == 0) {
        mainCurrency.setExchangeRate(Double.valueOf(1.0D));
      }
      else {
        return;
      }
    }
    

    Session session = null;
    Transaction tx = null;
    try {
      session = CurrencyDAO.getInstance().createNewSession();
      tx = session.beginTransaction();
      
      for (Currency currency : currencyList) {
        session.saveOrUpdate(currency);
      }
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      return;
    } finally {
      session.close();
    }
    
    setCanceled(true);
    dispose();
  }
}
