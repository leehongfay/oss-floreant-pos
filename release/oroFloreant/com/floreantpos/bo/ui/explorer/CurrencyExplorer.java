package com.floreantpos.bo.ui.explorer;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.Currency;
import com.floreantpos.model.dao.CurrencyDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.model.CurrencyForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import org.jdesktop.swingx.JXTable;


















public class CurrencyExplorer
  extends TransparentPanel
{
  private JXTable table;
  private BeanTableModel<Currency> tableModel;
  
  public CurrencyExplorer()
  {
    tableModel = new BeanTableModel(Currency.class);
    

    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "name");
    tableModel.addColumn(Messages.getString("CurrencyExplorer.0"), "code");
    tableModel.addColumn(Messages.getString("CurrencyExplorer.2"), "symbol");
    tableModel.addColumn(Messages.getString("CurrencyExplorer.4"), "exchangeRate");
    tableModel.addColumn(Messages.getString("CurrencyExplorer.6"), "main");
    tableModel.addColumn(Messages.getString("CurrencyExplorer.8"), "tolerance");
    
    tableModel.addRows(CurrencyDAO.getInstance().findAll());
    table = new JXTable(tableModel);
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    table.setRowHeight(PosUIManager.getSize(30));
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          CurrencyForm editor = new CurrencyForm();
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          if (dialog.isCanceled()) {
            return;
          }
          tableModel.addRow((Currency)editor.getBean());
          CurrencyExplorer.this.refresh();
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          Currency currency = (Currency)tableModel.getRow(index);
          
          CurrencyForm currencyForm = new CurrencyForm(currency);
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), currencyForm);
          dialog.open();
          if (dialog.isCanceled())
            return;
          CurrencyExplorer.this.refresh();
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          
          if (ConfirmDeleteDialog.showMessage(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0)
          {
            Currency currency = (Currency)tableModel.getRow(index);
            CurrencyDAO.getInstance().delete(currency);
            tableModel.removeRow(currency);
          }
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    add(panel, "South");
  }
  
  protected BeanTableModel<Currency> getModel() {
    return tableModel;
  }
  
  private void refresh() {
    List<Currency> currencyList = CurrencyDAO.getInstance().findAll();
    tableModel.getRows().clear();
    tableModel.addRows(currencyList);
    table.repaint();
  }
}
