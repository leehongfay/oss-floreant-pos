package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.PriceRule;
import com.floreantpos.model.dao.PriceRuleDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.model.PriceRuleForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import org.jdesktop.swingx.JXTable;




















public class PriceRuleExplorer
  extends TransparentPanel
{
  private JXTable table;
  private BeanTableModel<PriceRule> tableModel;
  
  public PriceRuleExplorer()
  {
    tableModel = new BeanTableModel(PriceRule.class);
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), PriceRule.PROP_NAME);
    tableModel.addColumn(POSConstants.DESCRIPTION.toUpperCase(), PriceRule.PROP_DESCRIPTION);
    tableModel.addColumn("CODE", PriceRule.PROP_CODE);
    tableModel.addColumn("DEPARTMENT", "department");
    tableModel.addColumn("SALES AREA", "salesArea");
    tableModel.addColumn("ORDER TYPE", "orderType");
    tableModel.addColumn("CUSTOMER GROUP", "customerGroup");
    tableModel.addColumn("PRICE SHIFT", "priceShift");
    tableModel.addColumn("PRICE LIST", "priceTable");
    tableModel.addColumn("ENABLE", PriceRule.PROP_ACTIVE);
    
    tableModel.addRows(PriceRuleDAO.getInstance().findAll());
    
    table = new JXTable(tableModel);
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          editSelectedRow(false);
        }
      }
    });
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.setRowHeight(PosUIManager.getSize(30));
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    addButtonPanel();
  }
  
  private void addButtonPanel() {
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          PriceRuleForm editor = new PriceRuleForm(new PriceRule());
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.openWithScale(400, 500);
          
          if (dialog.isCanceled()) {
            return;
          }
          PriceRule priceTable = (PriceRule)editor.getBean();
          tableModel.addRow(priceTable);
        }
        catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        editSelectedRow(false);
      }
      
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          PriceRule priceTable = (PriceRule)tableModel.getRow(index);
          
          if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
            return;
          }
          
          PriceRuleDAO dao = new PriceRuleDAO();
          dao.delete(priceTable);
          
          tableModel.removeRow(index);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    add(panel, "South");
  }
  
  public void editSelectedRow(boolean viewMode) {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      PriceRule priceTable = (PriceRule)tableModel.getRow(index);
      
      PriceRuleForm editor = new PriceRuleForm(priceTable);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.openWithScale(400, 500);
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
}
