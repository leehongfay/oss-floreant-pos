package com.floreantpos.bo.ui.explorer;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.config.ui.ConfigurationView;
import com.floreantpos.main.Application;
import com.floreantpos.model.Department;
import com.floreantpos.model.SalesArea;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.TerminalType;
import com.floreantpos.model.dao.DepartmentDAO;
import com.floreantpos.model.dao.SalesAreaDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.dao.TerminalTypeDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.PosGuiUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;




public class TerminalForm
  extends ConfigurationView
{
  private JLabel lblTerminalId;
  private POSTextField tfTerminalId;
  private JButton btnTerminalGen;
  private JLabel lblTerminalName;
  private POSTextField tfTerminalName;
  private JLabel lblLocation;
  private POSTextField tfLocation;
  private JLabel lblDeptName;
  private JComboBox cbDeptList;
  private JCheckBox cbTicketSettlement;
  private JCheckBox cbDbConfig;
  private JCheckBox cbEnableAutoLogOff;
  private JCheckBox cbTranslatedName;
  private JCheckBox chbKioskMode;
  private JCheckBox cbBarcode;
  private JCheckBox cbKitchenReceipt;
  private JCheckBox cbMultiCurrency;
  private JLabel lblAutoLogOffSec;
  private IntegerTextField tfAutoLogOffSec;
  private JLabel lblDefaultPassLen;
  private int terminalId;
  private IntegerTextField tfDefaultPassLen;
  private JCheckBox chkHasCashDrawer;
  private JCheckBox chkActive;
  private JLabel lblSalesArea;
  private JComboBox cbSalesArea;
  private JCheckBox cbFixedSalesArea;
  private JSpinner spinner;
  private JComboBox cbTerminalType = new JComboBox();
  private Department department;
  private List selection;
  private Terminal terminal;
  private JLabel lblOrderViewPageSize;
  private IntegerTextField tfOrderViewPageSize;
  private boolean thisTerminal = true;
  private JCheckBox chkShowPrintBtn;
  
  public TerminalForm(Terminal terminal) {
    this.terminal = terminal;
    setLayout(new BorderLayout());
    thisTerminal = (terminal == Application.getInstance().getTerminal());
    createUI();
    initData();
  }
  
  public void initData() {
    DepartmentDAO deptDAO = new DepartmentDAO();
    List<Department> deptList = new ArrayList();
    deptList.add(null);
    deptList.addAll(deptDAO.findAll());
    
    cbDeptList.setModel(new ComboBoxModel(deptList));
    
    SalesAreaDAO salesAreaDAO = new SalesAreaDAO();
    List<SalesArea> salesareaList = new ArrayList();
    salesareaList.add(null);
    salesareaList.addAll(salesAreaDAO.findAll());
    cbSalesArea.setModel(new ComboBoxModel(salesareaList));
  }
  
  private void createTerminal() {
    Random random = new Random();
    terminalId = (random.nextInt(10000) + 1);
    tfTerminalId.setText(String.valueOf(terminalId));
  }
  
  private void createUI()
  {
    JPanel itemInfoPanel = new JPanel();
    JScrollPane scrollPane = new JScrollPane(itemInfoPanel, 20, 31);
    
    scrollPane.setBorder(null);
    
    add(scrollPane, "Center");
    
    itemInfoPanel.setLayout(new MigLayout("hidemode 3", "[][grow][]", ""));
    
    lblTerminalId = new JLabel(Messages.getString("TerminalEntryForm.3"));
    itemInfoPanel.add(lblTerminalId, "alignx trailing");
    
    tfTerminalId = new POSTextField();
    tfTerminalId.setEditable(false);
    itemInfoPanel.add(tfTerminalId, "growx,split 3");
    
    cbTerminalType.setModel(new ComboBoxModel(TerminalTypeDAO.getInstance().findAll()));
    itemInfoPanel.add(new JLabel("Terminal Type:"));
    itemInfoPanel.add(cbTerminalType, "w 100!,growx, wrap");
    
    btnTerminalGen = new JButton(Messages.getString("TerminalEntryForm.6"));
    lblTerminalName = new JLabel(Messages.getString("TerminalEntryForm.8"));
    itemInfoPanel.add(lblTerminalName, "alignx trailing");
    
    tfTerminalName = new POSTextField();
    itemInfoPanel.add(tfTerminalName, "growx, wrap");
    
    lblLocation = new JLabel(Messages.getString("TerminalEntryForm.11"));
    itemInfoPanel.add(lblLocation, "alignx trailing");
    
    tfLocation = new POSTextField();
    itemInfoPanel.add(tfLocation, "growx, wrap");
    
    lblDeptName = new JLabel(Messages.getString("TerminalEntryForm.20"));
    itemInfoPanel.add(lblDeptName, "alignx trailing");
    
    cbDeptList = new JComboBox();
    itemInfoPanel.add(cbDeptList, "growx, wrap");
    
    lblSalesArea = new JLabel("Sales Area");
    itemInfoPanel.add(lblSalesArea, "alignx trailing");
    
    cbSalesArea = new JComboBox();
    itemInfoPanel.add(cbSalesArea, "growx,split 2");
    cbFixedSalesArea = new JCheckBox("Fixed Sales Area");
    itemInfoPanel.add(cbFixedSalesArea, "wrap");
    
    lblOrderViewPageSize = new JLabel("Order View Page Size");
    itemInfoPanel.add(lblOrderViewPageSize, "newline, alignx trailing");
    
    tfOrderViewPageSize = new IntegerTextField();
    itemInfoPanel.add(tfOrderViewPageSize, " growx");
    
    lblDefaultPassLen = new JLabel(Messages.getString("TerminalEntryForm.29"));
    itemInfoPanel.add(lblDefaultPassLen, "newline, alignx trailing");
    
    tfDefaultPassLen = new IntegerTextField();
    itemInfoPanel.add(tfDefaultPassLen, "growx, wrap");
    

    JPanel bottomPanel = new JPanel(new MigLayout("fillx, insets 0 0 0 0", "[]20px[grow]", ""));
    
    cbEnableAutoLogOff = new JCheckBox(Messages.getString("TerminalEntryForm.36"));
    bottomPanel.add(cbEnableAutoLogOff, "split 2,span 2");
    
    JPanel newPanel = new JPanel(new BorderLayout(10, 0));
    newPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
    lblAutoLogOffSec = new JLabel(Messages.getString("TerminalEntryForm.26"));
    newPanel.add(lblAutoLogOffSec, "West");
    tfAutoLogOffSec = new IntegerTextField(8);
    newPanel.add(tfAutoLogOffSec, "Center");
    tfAutoLogOffSec.setEnabled(false);
    bottomPanel.add(newPanel, "");
    
    cbTicketSettlement = new JCheckBox(Messages.getString("TerminalEntryForm.32"));
    bottomPanel.add(cbTicketSettlement, "newline, growx, wrap");
    
    cbDbConfig = new JCheckBox(Messages.getString("TerminalEntryForm.34"));
    bottomPanel.add(cbDbConfig, "growx, wrap");
    cbTranslatedName = new JCheckBox(Messages.getString("TerminalEntryForm.38"));
    bottomPanel.add(cbTranslatedName, "growx, wrap");
    
    chbKioskMode = new JCheckBox(Messages.getString("TerminalEntryForm.40"));
    bottomPanel.add(chbKioskMode, "growx, wrap");
    
    cbBarcode = new JCheckBox(Messages.getString("TerminalEntryForm.42"));
    bottomPanel.add(cbBarcode, "growx, wrap");
    
    cbKitchenReceipt = new JCheckBox(Messages.getString("TerminalEntryForm.44"));
    bottomPanel.add(cbKitchenReceipt, "growx, wrap");
    
    cbMultiCurrency = new JCheckBox(Messages.getString("TerminalEntryForm.46"));
    bottomPanel.add(cbMultiCurrency, "growx, wrap");
    
    chkHasCashDrawer = new JCheckBox("Has cash drawer");
    chkActive = new JCheckBox("Active");
    chkShowPrintBtn = new JCheckBox("Show print button on settle screen");
    
    bottomPanel.add(chkHasCashDrawer, "cell 0 10,growx, wrap");
    bottomPanel.add(chkActive, "cell 0 11,growx, wrap");
    bottomPanel.add(chkShowPrintBtn, "cell 0 12,growx, wrap");
    bottomPanel.add(new JLabel("Change the size of text, button and other items"), "cell 0 14, growx");
    SpinnerModel spinnerModel = new SpinnerNumberModel(1.0D, 0.5D, 2.5D, 0.1D);
    


    spinner = new JSpinner(spinnerModel);
    bottomPanel.add(spinner, "cell 1 14, width " + PosUIManager.getSize(50));
    itemInfoPanel.add(bottomPanel, "cell 1 16, span 1, growx");
    
    installActions();
  }
  
  private void installActions() {
    btnTerminalGen.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        TerminalForm.this.createTerminal();
      }
      
    });
    cbDeptList.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        cbDeptList = ((JComboBox)e.getSource());
        department = ((Department)cbDeptList.getSelectedItem());
        if (department != null) {
          selection = new ArrayList();
          selection.add(null);
          
          List<SalesArea> salesAreaByDept = SalesAreaDAO.getInstance().findSalesAreaByDept(department);
          if (salesAreaByDept != null) {
            selection.addAll(salesAreaByDept);
          }
          cbSalesArea.setModel(new ComboBoxModel(selection));
        }
        else {
          selection = new ArrayList();
          selection.add(null);
          List<SalesArea> list = SalesAreaDAO.getInstance().findAll();
          if (list != null) {
            selection.addAll(list);
          }
          cbSalesArea.setModel(new ComboBoxModel(selection));
        }
        
      }
      
    });
    cbEnableAutoLogOff.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == 1) {
          tfAutoLogOffSec.setEnabled(true);
        } else {
          tfAutoLogOffSec.setEnabled(false);
        }
      }
    });
  }
  
  public boolean save() {
    try {
      if (!updateModel()) {
        return false;
      }
      
      TerminalDAO termDAO = new TerminalDAO();
      termDAO.saveOrUpdate(terminal);
      
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(e.getMessage());
    }
    
    return false;
  }
  
  public void updateView() {
    try {
      if (terminal == null)
      {
        return;
      }
      
      if (terminal.getId() != null) {
        tfTerminalId.setText(String.valueOf(terminal.getId()));
      }
      tfTerminalName.setText(terminal.getName());
      tfLocation.setText(terminal.getLocation());
      

      if (StringUtils.isNotEmpty(terminal.getDepartmentId())) {
        PosGuiUtil.selectComboItemById(cbDeptList, terminal.getDepartmentId());
      }
      
      if (StringUtils.isNotEmpty(terminal.getSalesAreaId())) {
        PosGuiUtil.selectComboItemById(cbSalesArea, terminal.getSalesAreaId());
      }
      tfAutoLogOffSec.setText(String.valueOf(terminal.getAutoLogOffSec()));
      tfDefaultPassLen.setText(String.valueOf(terminal.getDefaultPassLength()));
      cbTicketSettlement.setSelected(terminal.isTicketSettlement().booleanValue());
      cbDbConfig.setSelected(terminal.isDbConfig().booleanValue());
      cbEnableAutoLogOff.setSelected(terminal.isAutoLogOff().booleanValue());
      cbTranslatedName.setSelected(terminal.isTranslatedName().booleanValue());
      cbBarcode.setSelected(terminal.isBarcodeOnReceipt().booleanValue());
      cbKitchenReceipt.setSelected(terminal.isGroupByCatagoryKitReceipt().booleanValue());
      cbMultiCurrency.setSelected(terminal.isEnableMultiCurrency().booleanValue());
      chkHasCashDrawer.setSelected(terminal.isHasCashDrawer().booleanValue());
      chkActive.setSelected(terminal.isActive().booleanValue());
      chkShowPrintBtn.setSelected(terminal.isShowPrntBtn().booleanValue());
      
      PosGuiUtil.selectComboItemById(cbTerminalType, terminal.getTerminalTypeId());
      cbFixedSalesArea.setSelected(terminal.isFixedSalesArea().booleanValue());
      
      if (thisTerminal) {
        chbKioskMode.setSelected(TerminalConfig.isKioskMode());
        tfOrderViewPageSize.setText(String.valueOf(TerminalConfig.getOrderViewPageSize()));
        spinner.setValue(Double.valueOf(TerminalConfig.getScreenScaleFactor()));
      }
      else {
        chbKioskMode.setEnabled(false);
        tfOrderViewPageSize.setEnabled(false);
        spinner.setEnabled(false);
      }
      setInitialized(true);
    } catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  public boolean updateModel()
  {
    int defaultPassLen = tfDefaultPassLen.getInteger();
    if (defaultPassLen == 0)
      defaultPassLen = 4;
    int pageSize = tfOrderViewPageSize.getInteger();
    if ((thisTerminal) && ((pageSize <= 0) || (pageSize > 500))) {
      POSMessageDialog.showMessage("Page size must be 1 to 500");
      return false;
    }
    terminal.setName(tfTerminalName.getText());
    terminal.setLocation(tfLocation.getText());
    terminal.setSalesArea((SalesArea)cbSalesArea.getSelectedItem());
    terminal.setDepartment((Department)cbDeptList.getSelectedItem());
    terminal.setAutoLogOffSec(Integer.valueOf(tfAutoLogOffSec.getInteger() <= 0 ? 10 : tfAutoLogOffSec.getInteger()));
    terminal.setFixedSalesArea(Boolean.valueOf(cbFixedSalesArea.isSelected()));
    
    terminal.setDefaultPassLength(Integer.valueOf(defaultPassLen));
    terminal.setTicketSettlement(Boolean.valueOf(cbTicketSettlement.isSelected()));
    terminal.setDbConfig(Boolean.valueOf(cbDbConfig.isSelected()));
    terminal.setAutoLogOff(Boolean.valueOf(cbEnableAutoLogOff.isSelected()));
    terminal.setTranslatedName(Boolean.valueOf(cbTranslatedName.isSelected()));
    terminal.setBarcodeOnReceipt(Boolean.valueOf(cbBarcode.isSelected()));
    terminal.setGroupByCatagoryKitReceipt(Boolean.valueOf(cbKitchenReceipt.isSelected()));
    terminal.setEnableMultiCurrency(Boolean.valueOf(cbMultiCurrency.isSelected()));
    terminal.setTerminalType((TerminalType)cbTerminalType.getSelectedItem());
    
    terminal.setHasCashDrawer(Boolean.valueOf(chkHasCashDrawer.isSelected()));
    terminal.setActive(Boolean.valueOf(chkActive.isSelected()));
    terminal.setShowPrntBtn(Boolean.valueOf(chkShowPrintBtn.isSelected()));
    
    if (thisTerminal) {
      TerminalConfig.setScreenScaleFactor(((Double)spinner.getValue()).doubleValue());
      TerminalConfig.setKioskMode(chbKioskMode.isSelected());
      TerminalConfig.setOrderViewPageSize(pageSize);
    }
    return true;
  }
  
  public void initialize() throws Exception
  {
    updateView();
  }
  
  public String getName()
  {
    return POSConstants.CONFIG_TAB_STORE;
  }
}
