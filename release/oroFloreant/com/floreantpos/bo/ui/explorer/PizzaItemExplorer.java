package com.floreantpos.bo.ui.explorer;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.main.Application;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.model.PizzaItemForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.hibernate.exception.ConstraintViolationException;
import org.jdesktop.swingx.JXTable;



public class PizzaItemExplorer
  extends TransparentPanel
{
  private JXTable table;
  private BeanTableModel<MenuItem> tableModel;
  private JComboBox cbGroup;
  private JComboBox cbOrderTypes;
  private JTextField tfName;
  
  public PizzaItemExplorer()
  {
    tableModel = new BeanTableModel(MenuItem.class);
    
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "name");
    tableModel.addColumn(POSConstants.TRANSLATED_NAME.toUpperCase(), "translatedName");
    

    tableModel.addColumn(POSConstants.VISIBLE.toUpperCase(), "visible");
    
    tableModel.addColumn("TAX GROUP", "taxGroup");
    tableModel.addColumn(Messages.getString("MenuItemExplorer.21"), "sortOrder");
    tableModel.addColumn(Messages.getString("MenuItemExplorer.23"), "buttonColor");
    tableModel.addColumn(Messages.getString("MenuItemExplorer.25"), "textColor");
    tableModel.addColumn(POSConstants.IMAGE.toUpperCase(), "image");
    
    table = new JXTable(tableModel);
    table.getColumn(0).setMinWidth(100);
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.setRowHeight(PosUIManager.getSize(30));
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    add(createButtonPanel(), "South");
    add(buildSearchForm(), "North");
    resizeColumnWidth(table);
    
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          PizzaItemExplorer.this.editSelectedRow();
        }
      }
    });
    searchItem();
  }
  
  private void editSelectedRow() {
    try {
      int rowIndex = table.getSelectedRow();
      if (rowIndex < 0) {
        return;
      }
      int index = table.convertRowIndexToModel(rowIndex);
      
      MenuItem menuItem = (MenuItem)tableModel.getRow(index);
      MenuItemDAO.getInstance().initialize(menuItem);
      
      PizzaItemForm editor = new PizzaItemForm(menuItem);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.openWithScale(900, 700);
      if (dialog.isCanceled()) {
        return;
      }
      
      tableModel.setRow(index, menuItem);
      tableModel.fireTableRowsUpdated(rowIndex, rowIndex);
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private JPanel buildSearchForm() {
    JPanel panel = new JPanel();
    panel.setLayout(new MigLayout("", "[][]15[][]15[][]15[]", "[]5[]"));
    
    JLabel lblOrderType = new JLabel(Messages.getString("MenuItemExplorer.4"));
    cbOrderTypes = new JComboBox();
    
    cbOrderTypes.addItem(Messages.getString("MenuItemExplorer.5"));
    
    List<OrderType> orderTypes = Application.getInstance().getOrderTypes();
    for (OrderType orderType : orderTypes) {
      cbOrderTypes.addItem(orderType);
    }
    
    JLabel lblName = new JLabel(Messages.getString("MenuItemExplorer.0"));
    tfName = new JTextField(15);
    
    try
    {
      List<MenuGroup> menuGroupList = MenuGroupDAO.getInstance().findAll();
      
      cbGroup = new JComboBox();
      
      cbGroup.addItem(Messages.getString("MenuItemExplorer.2"));
      for (MenuGroup s : menuGroupList) {
        cbGroup.addItem(s);
      }
      
      JButton searchBttn = new JButton(Messages.getString("MenuItemExplorer.3"));
      
      panel.add(lblName, "align label");
      panel.add(tfName);
      panel.add(lblOrderType);
      panel.add(cbOrderTypes);
      panel.add(searchBttn);
      
      Border loweredetched = BorderFactory.createEtchedBorder(1);
      TitledBorder title = BorderFactory.createTitledBorder(loweredetched, Messages.getString("PizzaItemExplorer.0"));
      title.setTitleJustification(1);
      panel.setBorder(title);
      
      searchBttn.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          PizzaItemExplorer.this.searchItem();
        }
        
      });
      tfName.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          PizzaItemExplorer.this.searchItem();
        }
      });
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
    
    return panel;
  }
  
  private void searchItem() {
    Object selectedType = cbOrderTypes.getSelectedItem();
    String txName = tfName.getText();
    Object selectedGroup = cbGroup.getSelectedItem();
    
    List<MenuItem> similarItem = null;
    
    if ((selectedGroup instanceof MenuGroup)) {
      similarItem = MenuItemDAO.getInstance().getPizzaItems(txName, (MenuGroup)selectedGroup, selectedType);
    }
    else {
      similarItem = MenuItemDAO.getInstance().getPizzaItems(txName, null, selectedType);
    }
    
    tableModel.removeAll();
    tableModel.addRows(similarItem);
  }
  
  private TransparentPanel createButtonPanel() {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton editButton = explorerButton.getEditButton();
    JButton addButton = explorerButton.getAddButton();
    JButton deleteButton = explorerButton.getDeleteButton();
    JButton duplicateButton = new JButton("Duplicate");
    
    JButton updateStockAmount = new JButton(Messages.getString("MenuItemExplorer.6"));
    
    updateStockAmount.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        try
        {
          int index = table.getSelectedRow();
          
          if (index < 0) {
            POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), Messages.getString("MenuItemExplorer.7"));
            return;
          }
          
          MenuItem menuItem = (MenuItem)tableModel.getRow(index);
          
          String amountString = JOptionPane.showInputDialog(POSUtil.getBackOfficeWindow(), Messages.getString("MenuItemExplorer.8"), menuItem
            .getAvailableUnit());
          
          if ((amountString == null) || (amountString.equals(""))) {
            return;
          }
          
          double stockAmount = Double.parseDouble(amountString);
          
          if (stockAmount < 0.0D) {
            POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), Messages.getString("MenuItemExplorer.10"));
            return;
          }
          
          menuItem.setAvailableUnit(Double.valueOf(stockAmount));
          MenuItemDAO.getInstance().saveOrUpdate(menuItem);
          table.repaint();
        }
        catch (NumberFormatException e1) {
          POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), Messages.getString("MenuItemExplorer.11"));
          return;
        } catch (Exception e2) {
          BOMessageDialog.showError(POSUtil.getBackOfficeWindow(), POSConstants.ERROR_MESSAGE, e2);
          return;
        }
        
      }
    });
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        PizzaItemExplorer.this.editSelectedRow();
      }
      

    });
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          PizzaItemExplorer.this.doCreateNewItem();
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          PizzaItemExplorer.this.doDeleteItem();
        } catch (ConstraintViolationException ex) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "This menu item is in use and cannot be deleted.");
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    duplicateButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          
          MenuItem existingItem = (MenuItem)tableModel.getRow(index);
          MenuItemDAO.getInstance().initialize(existingItem);
          
          MenuItem newMenuItem = existingItem.clone();
          PizzaItemForm editor = new PizzaItemForm(newMenuItem);
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.openWithScale(900, 700);
          if (dialog.isCanceled()) {
            return;
          }
          MenuItem foodItem = (MenuItem)editor.getBean();
          tableModel.addRow(foodItem);
          table.getSelectionModel().addSelectionInterval(tableModel.getRowCount() - 1, tableModel.getRowCount() - 1);
          table.scrollRowToVisible(tableModel.getRowCount() - 1);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton);
    panel.add(editButton);
    panel.add(updateStockAmount);
    panel.add(deleteButton);
    panel.add(duplicateButton);
    return panel;
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(200));
    columnWidth.add(Integer.valueOf(200));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(140));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(200));
    
    return columnWidth;
  }
  
  private void doCreateNewItem() throws Exception {
    MenuItem menuItem = new MenuItem();
    Object group = cbGroup.getSelectedItem();
    
    if ((group instanceof MenuGroup)) {
      menuItem.setMenuGroup((MenuGroup)group);
    }
    
    Object selectedType = cbOrderTypes.getSelectedItem();
    
    if ((selectedType instanceof OrderType)) {
      List types = new ArrayList();
      types.add((OrderType)selectedType);
      menuItem.setOrderTypeList(types);
    }
    PizzaItemForm editor = new PizzaItemForm(menuItem);
    
    BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
    dialog.openWithScale(900, 700);
    
    if (dialog.isCanceled()) {
      return;
    }
    menuItem = (MenuItem)editor.getBean();
    tableModel.addRow(menuItem);
  }
  
  private void doDeleteItem() {
    int index = table.getSelectedRow();
    if (index < 0) {
      return;
    }
    index = table.convertRowIndexToModel(index);
    
    if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0)
    {
      return;
    }
    MenuItem item = (MenuItem)tableModel.getRow(index);
    
    MenuItemDAO menuItemDAO = new MenuItemDAO();
    menuItemDAO.releaseParentAndDelete(item);
    
    tableModel.removeRow(index);
  }
}
