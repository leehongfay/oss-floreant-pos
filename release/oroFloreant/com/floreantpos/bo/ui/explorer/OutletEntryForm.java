package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.model.Address;
import com.floreantpos.model.Department;
import com.floreantpos.model.Outlet;
import com.floreantpos.model.dao.AddressDAO;
import com.floreantpos.model.dao.DepartmentDAO;
import com.floreantpos.model.dao.OutletDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.CheckBoxList;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;


public class OutletEntryForm
  extends BeanEditor<Outlet>
{
  private Outlet outlet;
  private FixedLengthTextField txtName;
  private JTextField tfAddress;
  private JTextArea tfDesc;
  private CheckBoxList departmentlist = new CheckBoxList();
  
  public OutletEntryForm(Outlet outlet) {
    this.outlet = outlet;
    initComponents();
    
    initData();
    setBean(outlet);
  }
  
  public void initData() {
    List<Department> departmentTypes = DepartmentDAO.getInstance().findAll();
    
    if (departmentTypes == null) {
      return;
    }
    departmentlist.setModel(departmentTypes);
  }
  
  public void initComponents()
  {
    setLayout(new BorderLayout());
    
    JPanel contentPanel = new JPanel();
    
    contentPanel.setLayout(new MigLayout("wrap 2,fillx", "[][grow]", ""));
    
    JLabel lblName = new JLabel("Name");
    txtName = new FixedLengthTextField();
    txtName.setLength(20);
    tfAddress = new JTextField();
    tfDesc = new JTextArea(5, 5);
    contentPanel.add(lblName, "alignx trailing");
    contentPanel.add(txtName, "growx");
    JLabel lblAddress = new JLabel("Address");
    contentPanel.add(lblAddress, "alignx trailing");
    contentPanel.add(tfAddress, "growx");
    JLabel lblDesc = new JLabel("Description");
    contentPanel.add(lblDesc, "alignx trailing");
    tfDesc.setLineWrap(true);
    contentPanel.add(new JScrollPane(tfDesc), "growx,h 100!");
    
    departmentlist.setEnabled(true);
    TitledBorder titelDepertment = new TitledBorder("Departmeents");
    JScrollPane departmentPane = new JScrollPane(departmentlist);
    departmentPane.setBorder(titelDepertment);
    contentPanel.add(departmentPane, "skip 1, h 200px");
    
    add(contentPanel);
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      Outlet item = (Outlet)getBean();
      OutletDAO outletDAO = new OutletDAO();
      outletDAO.saveOrUpdate(item);
    } catch (Exception e) {
      MessageDialog.showError(POSConstants.ERROR_MESSAGE, e);
      return false;
    }
    return true;
  }
  

  protected void updateView()
  {
    Outlet item = (Outlet)getBean();
    
    txtName.setText(item.getName());
    if (item.getAddress() != null) {
      tfAddress.setText(item.getAddress().getAddressLine());
    }
    
    tfDesc.setText(item.getDescription());
    departmentlist.selectItems(item.getDepartments());
  }
  
  protected boolean updateModel()
    throws IllegalModelStateException
  {
    Outlet term = (Outlet)getBean();
    String name = txtName.getText();
    String txtAddress = tfAddress.getText();
    String description = tfDesc.getText();
    Address address = new Address();
    address.setAddressLine(txtAddress);
    if (name == null) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Please enter name");
      return false;
    }
    
    outlet.setName(name);
    outlet.setAddress(address);
    outlet.setDescription(description);
    outlet.setDepartments(departmentlist.getCheckedValues());
    AddressDAO.getInstance().saveOrUpdate(address);
    return true;
  }
  
  public String getDisplayText()
  {
    Outlet item = (Outlet)getBean();
    if (item.getId() == null) {
      return "Add New Outlet";
    }
    return "Edit Outlet";
  }
}
