package com.floreantpos.bo.ui.explorer;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.InventoryTransactionType;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.BeanTableModel.EditMode;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.inv.InventoryStockInForm;
import com.floreantpos.ui.inv.InventoryTransactionEntryForm;
import com.floreantpos.ui.menuitem.variant.VariantForm;
import com.floreantpos.ui.model.MenuItemForm;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import net.miginfocom.swing.MigLayout;
import org.hibernate.exception.ConstraintViolationException;
import org.jdesktop.swingx.JXTable;



















public class MenuItemExplorer
  extends TransparentPanel
  implements ExplorerView
{
  private MenuItemExplorerTable menuItemExplorerTable;
  private BeanTableModel<MenuItem> tableModel;
  private JComboBox cbCategory;
  private JTextField tfName;
  private JCheckBox cbItem;
  private JButton btnBack;
  private JButton btnForward;
  private JLabel lblNumberOfItem;
  private JComboBox cbGroup;
  private boolean variant;
  private JPanel buttonPanel;
  private JPanel buildSearchForm;
  
  public MenuItemExplorer()
  {
    this(false);
  }
  
  public MenuItemExplorer(boolean variant) {
    this.variant = variant;
    init();
  }
  
  private void init() {
    tableModel = new BeanTableModel(MenuItem.class, 20);
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "displayName");
    tableModel.addColumn(POSConstants.TRANSLATED_NAME.toUpperCase(), "translatedName");
    tableModel.addColumn("BARCODE", "barcode");
    tableModel.addColumn("HAS MODIFIER", MenuItem.PROP_HAS_MODIFIERS);
    tableModel.addColumn(POSConstants.PRICE.toUpperCase() + " (" + CurrencyUtil.getCurrencySymbol() + ")", "price");
    tableModel.addColumn(Messages.getString("MenuItemExplorer.14"), "availableUnit");
    tableModel.addColumn(Messages.getString("MenuItemExplorer.26"), "unitOnHand");
    tableModel.addColumn(POSConstants.VISIBLE.toUpperCase(), "visible");
    tableModel.addColumn("TAX GROUP", "taxGroup");
    tableModel.addColumn(Messages.getString("MenuItemExplorer.21"), "sortOrder", BeanTableModel.EditMode.EDITABLE);
    tableModel.addColumn(Messages.getString("MenuItemExplorer.23"), "buttonColor");
    tableModel.addColumn(Messages.getString("MenuItemExplorer.25"), "textColor");
    tableModel.addColumn(POSConstants.IMAGE.toUpperCase(), "image");
    
    btnBack = new JButton("<<< Previous");
    btnForward = new JButton("Next >>>");
    lblNumberOfItem = new JLabel();
    
    menuItemExplorerTable = new MenuItemExplorerTable(tableModel);
    menuItemExplorerTable.setDefaultRenderer(Object.class, new CustomCellRenderer());
    menuItemExplorerTable.setSelectionMode(0);
    
    menuItemExplorerTable.getInputMap().put(KeyStroke.getKeyStroke(32, 0), "startEditing");
    
    IntegerTextField tfEditField = new IntegerTextField();
    tfEditField.setHorizontalAlignment(4);
    DefaultCellEditor editor = new DefaultCellEditor(tfEditField);
    editor.setClickCountToStart(1);
    
    menuItemExplorerTable.setDefaultEditor(menuItemExplorerTable.getColumnClass(8), editor);
    menuItemExplorerTable.setDefaultRenderer(Object.class, new CustomCellRenderer());
    
    menuItemExplorerTable.setRowHeight(PosUIManager.getSize(30));
    menuItemExplorerTable.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          MenuItemExplorer.this.editSelectedRow();
        }
        
      }
    });
    btnBack.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        tableModel.setCurrentRowIndex(tableModel.getPreviousRowIndex());
        MenuItemExplorer.this.showMenuItems();
      }
      
    });
    btnForward.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        tableModel.setCurrentRowIndex(tableModel.getNextRowIndex());
        MenuItemExplorer.this.showMenuItems();
      }
      
    });
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(menuItemExplorerTable));
    
    buttonPanel = createButtonPanel();
    add(buttonPanel, "South");
    buildSearchForm = buildSearchForm();
    add(buildSearchForm, "North");
    resizeColumnWidth(menuItemExplorerTable);
  }
  
  private JPanel buildSearchForm()
  {
    JPanel panel = new JPanel();
    try {
      panel.setLayout(new MigLayout("", "[][]15[][]15[][]15[]", "[]5[]"));
      
      JLabel lblCategory = new JLabel(Messages.getString("MenuItemExplorer.4"));
      cbCategory = new JComboBox();
      
      cbCategory.addItem(Messages.getString("MenuItemExplorer.5"));
      List<MenuCategory> categories = MenuCategoryDAO.getInstance().findAll();
      for (MenuCategory category : categories) {
        cbCategory.addItem(category);
      }
      
      cbCategory.addItemListener(new ItemListener()
      {
        public void itemStateChanged(ItemEvent e)
        {
          MenuItemExplorer.this.doSelectGroup(e);
        }
        
      });
      JLabel lblName = new JLabel(Messages.getString("MenuItemExplorer.0"));
      
      JLabel lblGroup = new JLabel(Messages.getString("MenuItemExplorer.1"));
      cbGroup = new JComboBox();
      cbGroup.addItem(Messages.getString("MenuItemExplorer.5"));
      tfName = new JTextField(15);
      cbItem = new JCheckBox(Messages.getString("MenuItemExplorer.13"));
      JButton searchBttn = new JButton(Messages.getString("MenuItemExplorer.3"));
      
      panel.add(lblName, "align label");
      panel.add(tfName);
      panel.add(lblCategory);
      panel.add(cbCategory);
      panel.add(lblGroup);
      panel.add(cbGroup);
      panel.add(cbItem);
      panel.add(searchBttn);
      
      Border loweredetched = BorderFactory.createEtchedBorder(1);
      TitledBorder title = BorderFactory.createTitledBorder(loweredetched, Messages.getString("MenuItemExplorer.30"));
      title.setTitleJustification(1);
      panel.setBorder(title);
      
      searchBttn.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          MenuItemExplorer.this.showMenuItems();
        }
        
      });
      tfName.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          MenuItemExplorer.this.showMenuItems();
        }
      });
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
    
    return panel;
  }
  
  public void setMenuItems(List<MenuItem> items) {
    tableModel.removeAll();
    buttonPanel.setVisible(false);
    buildSearchForm.setVisible(false);
    tableModel.addRows(items);
  }
  
  private void showMenuItems() {
    Object selectedCategory = cbCategory.getSelectedItem();
    String txName = tfName.getText();
    Boolean showAll = Boolean.valueOf(!cbItem.isSelected());
    MenuGroup group = null;
    MenuCategory category = null;
    Object selectedGroup = cbGroup.getSelectedItem();
    if ((selectedGroup instanceof MenuGroup)) {
      group = (MenuGroup)selectedGroup;
    }
    if ((selectedCategory instanceof MenuCategory)) {
      category = (MenuCategory)selectedCategory;
    }
    MenuItemDAO.getInstance().loadMenuItems(tableModel, showAll, group, txName, category, variant, Boolean.valueOf(false));
    
    int startNumber = tableModel.getCurrentRowIndex() + 1;
    int endNumber = tableModel.getNextRowIndex();
    int totalNumber = tableModel.getNumRows();
    if (endNumber > totalNumber) {
      endNumber = totalNumber;
    }
    lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
    btnBack.setEnabled(tableModel.hasPrevious());
    btnForward.setEnabled(tableModel.hasNext());
  }
  
  private JPanel createButtonPanel() {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton editButton = explorerButton.getEditButton();
    JButton addButton = explorerButton.getAddButton();
    JButton deleteButton = explorerButton.getDeleteButton();
    
    addButton.setText(Messages.getString("MenuItemExplorer.17"));
    editButton.setText(Messages.getString("MenuItemExplorer.18"));
    deleteButton.setText(Messages.getString("MenuItemExplorer.19"));
    
    JButton updateStockAmount = new JButton("UPDATE STOCK AMOUNT");
    updateStockAmount.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        MenuItemExplorer.this.doUpdateStockAmount();
      }
      
    });
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MenuItemExplorer.this.editSelectedRow();
      }
    });
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MenuItemExplorer.this.doCreateNewMenuItem();
      }
      

    });
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MenuItemExplorer.this.doDeleteMenuItem();
      }
      

    });
    JButton btnDuplicate = new JButton("DUPLICATE");
    btnDuplicate.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          MenuItemExplorer.this.doDuplicateMenuItem();
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    JPanel bottomPanel = new JPanel(new MigLayout("fillx", "[][fill]"));
    TransparentPanel actionButtonPanel = new TransparentPanel();
    if (!variant)
      actionButtonPanel.add(addButton);
    actionButtonPanel.add(editButton);
    actionButtonPanel.add(updateStockAmount);
    if (!variant) {
      actionButtonPanel.add(deleteButton);
      actionButtonPanel.add(btnDuplicate);
    }
    addInventoryButtonActions(actionButtonPanel);
    bottomPanel.add(actionButtonPanel, "");
    
    JPanel navigationPanel = new JPanel(new FlowLayout(4));
    navigationPanel.add(lblNumberOfItem);
    navigationPanel.add(btnBack);
    navigationPanel.add(btnForward);
    bottomPanel.add(navigationPanel, "grow");
    
    return bottomPanel;
  }
  
  private void addInventoryButtonActions(JPanel panel) {
    JButton inTransactionButton = new JButton("IN");
    JButton outTransactionButton = new JButton("OUT");
    JButton transferTransactionButton = new JButton("TRANSFER");
    inTransactionButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuItemExplorer.this.doShowStockInDialog(InventoryTransactionType.IN);
      }
      
    });
    outTransactionButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuItemExplorer.this.doAdjustStock(InventoryTransactionType.OUT, false);
      }
      
    });
    transferTransactionButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuItemExplorer.this.doAdjustStock(InventoryTransactionType.UNCHANGED, true);
      }
    });
    panel.add(inTransactionButton);
    panel.add(outTransactionButton);
    panel.add(transferTransactionButton);
  }
  
  private boolean doAdjustStock(InventoryTransactionType type, boolean transfer) {
    MenuItem menuItem = getSelectedItem();
    if (menuItem == null)
      return false;
    InventoryTransaction inventoryTransaction = new InventoryTransaction();
    inventoryTransaction.setTransactionType(type);
    if (transfer)
      inventoryTransaction.setReason("TRANSFER");
    inventoryTransaction.setMenuItem(menuItem);
    InventoryTransactionEntryForm inventoryTransactionEntryForm = new InventoryTransactionEntryForm(inventoryTransaction);
    
    BeanEditorDialog dialog = new BeanEditorDialog(inventoryTransactionEntryForm);
    dialog.setPreferredSize(PosUIManager.getSize(500, 500));
    dialog.open();
    
    if (dialog.isCanceled()) {
      return false;
    }
    return true;
  }
  
  private boolean doShowStockInDialog(InventoryTransactionType type) {
    MenuItem menuItem = getSelectedItem();
    
    InventoryTransaction inventoryTransaction = new InventoryTransaction();
    inventoryTransaction.setTransactionType(type);
    
    InventoryStockInForm editor = null;
    if (menuItem == null) {
      editor = new InventoryStockInForm(inventoryTransaction);
    }
    else {
      inventoryTransaction.setMenuItem(menuItem);
      InventoryUnit unit = menuItem.getUnit();
      if (unit == null) {
        POSMessageDialog.showError(this, "No unit is set for the item. Please set item unit first.");
        return false;
      }
      Double replenishLevel = menuItem.getReplenishLevel();
      inventoryTransaction.setQuantity(replenishLevel);
      Double cost = menuItem.getCost();
      inventoryTransaction.setUnitCost(cost);
      inventoryTransaction.setTotal(Double.valueOf(replenishLevel.doubleValue() * cost.doubleValue()));
      inventoryTransaction.setUnit(menuItem.getUnit().getCode());
      editor = new InventoryStockInForm(inventoryTransaction);
    }
    BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
    
    dialog.openWithScale(830, 630);
    if (dialog.isCanceled())
      return false;
    showMenuItems();
    return true;
  }
  
  private MenuItem getSelectedItem() {
    int index = menuItemExplorerTable.getSelectedRow();
    MenuItem menuItem = null;
    if (index >= 0) {
      index = menuItemExplorerTable.convertRowIndexToModel(index);
      menuItem = (MenuItem)tableModel.getRow(index);
      MenuItemDAO.getInstance().initialize(menuItem);
    }
    return menuItem;
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(200));
    columnWidth.add(Integer.valueOf(180));
    columnWidth.add(Integer.valueOf(90));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(80));
    columnWidth.add(Integer.valueOf(80));
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(140));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(200));
    
    return columnWidth;
  }
  
  private void editSelectedRow() {
    try {
      int index = menuItemExplorerTable.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = menuItemExplorerTable.convertRowIndexToModel(index);
      
      MenuItem menuItem = (MenuItem)tableModel.getRow(index);
      menuItem = MenuItemDAO.getInstance().getInitialized(menuItem.getId());
      
      if (variant) {
        VariantForm editor = new VariantForm(menuItem);
        BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
        doCreateSaveButtons(editor, dialog);
        dialog.openWithScale(900, 700);
        
        if (!dialog.isCanceled()) {
          tableModel.setRow(index, menuItem);
          tableModel.fireTableRowsUpdated(index, index);
        }
      }
      else {
        MenuItemForm editor = new MenuItemForm(menuItem);
        BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
        dialog.openWithScale(900, 700);
        
        if (!dialog.isCanceled()) {
          tableModel.setRow(index, menuItem);
          tableModel.fireTableRowsUpdated(index, index);
        }
      }
    } catch (Throwable x) {
      BOMessageDialog.showError(this, x.getMessage(), x);
    }
  }
  
  private void doCreateSaveButtons(final VariantForm editor, final BeanEditorDialog dialog) {
    PosButton btnSaveAndClose = new PosButton("SAVE");
    btnSaveAndClose.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (!editor.save())
          return;
        dialog.dispose();
      }
    });
    PosButton btnOk = (PosButton)dialog.getButtonPanel().getComponent(0);
    btnOk.setText("<html><center>SAVE &<br> CLOSE</center></html>");
    
    PosButton btnClose = (PosButton)dialog.getButtonPanel().getComponent(1);
    btnClose.setText("CLOSE");
    
    dialog.getButtonPanel().add(btnSaveAndClose, 0);
  }
  
  private void doUpdateStockAmount() {
    try {
      int index = menuItemExplorerTable.getSelectedRow();
      if (index < 0) {
        POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), Messages.getString("MenuItemExplorer.7"));
        return;
      }
      
      MenuItem menuItem = (MenuItem)tableModel.getRow(index);
      String amountString = JOptionPane.showInputDialog(POSUtil.getBackOfficeWindow(), Messages.getString("MenuItemExplorer.8"), menuItem
        .getAvailableUnit());
      
      if ((amountString == null) || (amountString.equals(""))) {
        return;
      }
      double stockAmount = Double.parseDouble(amountString);
      
      if (stockAmount < 0.0D) {
        POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), Messages.getString("MenuItemExplorer.10"));
        return;
      }
      
      menuItem.setAvailableUnit(Double.valueOf(stockAmount));
      MenuItemDAO.getInstance().saveOrUpdate(menuItem);
      menuItemExplorerTable.repaint();
    }
    catch (NumberFormatException e1) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), Messages.getString("MenuItemExplorer.11"));
      return;
    } catch (Exception e2) {
      BOMessageDialog.showError(POSUtil.getBackOfficeWindow(), POSConstants.ERROR_MESSAGE, e2);
      return;
    }
  }
  
  private void doCreateNewMenuItem() {
    try {
      MenuItem menuItem = new MenuItem();
      
      Object selectedType = cbCategory.getSelectedItem();
      
      if ((selectedType instanceof OrderType)) {
        List types = new ArrayList();
        types.add((OrderType)selectedType);
        menuItem.setOrderTypeList(types);
      }
      MenuItemForm editor = new MenuItemForm(menuItem);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.openWithScale(900, 700);
      
      if (dialog.isCanceled()) {
        return;
      }
      MenuItem foodItem = (MenuItem)editor.getBean();
      
      tableModel.addRow(foodItem);
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doDeleteMenuItem() {
    try {
      int index = menuItemExplorerTable.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = menuItemExplorerTable.convertRowIndexToModel(index);
      
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0)
      {
        return;
      }
      MenuItem item = (MenuItem)tableModel.getRow(index);
      
      MenuItemDAO dao = MenuItemDAO.getInstance();
      dao.refresh(item);
      dao.releaseParentAndDelete(item);
      
      tableModel.removeRow(index);
    } catch (ConstraintViolationException ex) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "This menu item is in use and cannot be deleted.");
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private class MenuItemExplorerTable extends JXTable
  {
    public MenuItemExplorerTable(TableModel dm) {
      super();
    }
    
    public void changeSelection(int row, int column, boolean toggle, boolean extend)
    {
      super.changeSelection(row, column, toggle, extend);
      menuItemExplorerTable.editCellAt(row, column);
      DefaultCellEditor editor = (DefaultCellEditor)menuItemExplorerTable.getCellEditor(row, column);
      if ((editor.getComponent() instanceof IntegerTextField)) {
        IntegerTextField textField = (IntegerTextField)editor.getComponent();
        textField.requestFocus();
        textField.selectAll();
      }
    }
    

    public void setValueAt(Object sortOrderObj, int row, int column)
    {
      MenuItem menuItem = (MenuItem)tableModel.getRow(row);
      
      Integer sOrder = Integer.valueOf(0);
      if ((sortOrderObj != null) && ((sortOrderObj instanceof Integer))) {
        sOrder = (Integer)sortOrderObj;
      }
      super.setValueAt(sOrder, row, column);
      if (column == 8) {
        menuItem.setSortOrder(sOrder);
        MenuItemDAO.getInstance().saveOrUpdate(menuItem);
      }
    }
  }
  
  public void initData()
  {
    showMenuItems();
    List<MenuGroup> groups = MenuGroupDAO.getInstance().findAll();
    for (MenuGroup group : groups) {
      cbGroup.addItem(group);
    }
  }
  
  private void doSelectGroup(ItemEvent e) {
    try {
      Object item = e.getItem();
      cbGroup.removeAllItems();
      cbGroup.addItem(Messages.getString("MenuItemExplorer.5"));
      if ((item instanceof MenuCategory)) {
        MenuCategory category = (MenuCategory)item;
        groups = MenuGroupDAO.getInstance().findByParent(category);
        for (MenuGroup group : groups) {
          cbGroup.addItem(group);
        }
      }
      else {
        List<MenuGroup> groups = MenuGroupDAO.getInstance().findAll();
        for (MenuGroup group : groups)
          cbGroup.addItem(group);
      }
    } catch (Exception ex) {
      List<MenuGroup> groups;
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), ex.getMessage(), ex);
    }
  }
  
  private void doDuplicateMenuItem() throws Exception {
    int index = menuItemExplorerTable.getSelectedRow();
    if (index < 0) {
      return;
    }
    index = menuItemExplorerTable.convertRowIndexToModel(index);
    
    MenuItem existingMenuItem = (MenuItem)tableModel.getRow(index);
    MenuItemDAO.getInstance().initialize(existingMenuItem);
    MenuItem newMenuItem = existingMenuItem.clone();
    
    MenuItemForm editor = new MenuItemForm(newMenuItem);
    BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
    dialog.openWithScale(900, 700);
    if (dialog.isCanceled()) {
      return;
    }
    MenuItem menuItem = (MenuItem)editor.getBean();
    tableModel.addRow(menuItem);
    menuItemExplorerTable.getSelectionModel().addSelectionInterval(tableModel.getRowCount() - 1, tableModel.getRowCount() - 1);
    menuItemExplorerTable.scrollRowToVisible(tableModel.getRowCount() - 1);
  }
}
