package com.floreantpos.bo.ui.explorer;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.model.MenuGroupForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import org.jdesktop.swingx.JXTable;



















public class MenuGroupExplorer
  extends TransparentPanel
  implements ExplorerView
{
  private JXTable table;
  private BeanTableModel<MenuGroup> tableModel;
  
  public MenuGroupExplorer()
  {
    tableModel = new BeanTableModel(MenuGroup.class);
    
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "name");
    tableModel.addColumn(POSConstants.TRANSLATED_NAME.toUpperCase(), "translatedName");
    tableModel.addColumn(POSConstants.VISIBLE.toUpperCase(), "visible");
    
    tableModel.addColumn(POSConstants.SORT_ORDER.toUpperCase(), "sortOrder");
    tableModel.addColumn(POSConstants.BUTTON_COLOR.toUpperCase(), "buttonColor");
    tableModel.addColumn(POSConstants.TEXT_COLOR.toUpperCase(), "textColor");
    
    table = new JXTable(tableModel);
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          MenuGroupExplorer.this.editSelectedRow();
        }
      }
    });
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.setRowHeight(PosUIManager.getSize(30));
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    createButtonPanel();
  }
  
  private void createButtonPanel() {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton editButton = explorerButton.getEditButton();
    JButton addButton = explorerButton.getAddButton();
    JButton deleteButton = explorerButton.getDeleteButton();
    
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MenuGroupExplorer.this.editSelectedRow();
      }
      

    });
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          MenuGroupForm editor = new MenuGroupForm();
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          if (dialog.isCanceled())
            return;
          MenuGroup foodGroup = (MenuGroup)editor.getBean();
          tableModel.addRow(foodGroup);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          MenuGroup group = (MenuGroup)tableModel.getRow(index);
          
          if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0)
          {
            return;
          }
          
          MenuItemDAO menuItemDao = new MenuItemDAO();
          boolean hasMenuItems = menuItemDao.existsMenuItem(group);
          
          if ((hasMenuItems) && 
            (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), Messages.getString("MenuGroupExplorer.0"), POSConstants.DELETE) != 0))
          {
            return;
          }
          
          MenuGroupDAO foodGroupDAO = new MenuGroupDAO();
          foodGroupDAO.releaseParentAndDelete(group);
          
          tableModel.removeRow(index);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    add(panel, "South");
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      MenuGroup menuGroup = (MenuGroup)tableModel.getRow(index);
      
      MenuGroupForm editor = new MenuGroupForm(menuGroup);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled())
        return;
      table.repaint();
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public void initData()
  {
    tableModel.setRows(MenuGroupDAO.getInstance().findAll());
  }
}
