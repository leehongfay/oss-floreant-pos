package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.swing.TransparentPanel;
import javax.swing.JButton;
























public class ExplorerButtonPanel
  extends TransparentPanel
{
  private JButton editButton;
  private JButton addButton;
  private JButton deleteButton;
  private JButton detailButton;
  
  public ExplorerButtonPanel()
  {
    initComponents();
  }
  







  private void initComponents()
  {
    detailButton = new JButton();
    editButton = new JButton();
    addButton = new JButton();
    deleteButton = new JButton();
    
    detailButton.setText(POSConstants.DETAILS);
    editButton.setText(POSConstants.EDIT);
    addButton.setText(POSConstants.ADD);
    deleteButton.setText(POSConstants.DELETE);
    
    add(detailButton);
    add(addButton);
    add(editButton);
    add(deleteButton);
  }
  
  public JButton getAddButton() {
    return addButton;
  }
  
  public JButton getDeleteButton() {
    return deleteButton;
  }
  
  public JButton getEditButton() {
    return editButton;
  }
  
  public JButton getDetailButton() {
    return detailButton;
  }
}
