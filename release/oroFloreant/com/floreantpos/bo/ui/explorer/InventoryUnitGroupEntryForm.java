package com.floreantpos.bo.ui.explorer;

import com.floreantpos.model.InventoryUnitGroup;
import com.floreantpos.model.dao.InventoryUnitGroupDAO;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;


public class InventoryUnitGroupEntryForm
  extends BeanEditor<InventoryUnitGroup>
{
  private POSTextField tfGroupName;
  private InventoryUnitGroup inventoryUnitGroup;
  
  public InventoryUnitGroupEntryForm(InventoryUnitGroup inventoryUnitGroup)
  {
    this.inventoryUnitGroup = inventoryUnitGroup;
    setLayout(new BorderLayout());
    
    createUI();
    




    setBean(inventoryUnitGroup);
  }
  


  public InventoryUnitGroupEntryForm()
  {
    setLayout(new BorderLayout());
    
    createUI();
  }
  


  private void createUI()
  {
    JPanel itemInfoPanel = new JPanel();
    
    add(itemInfoPanel, "Center");
    
    itemInfoPanel.setLayout(new MigLayout("fill,center", "[][]", ""));
    
    JLabel lblGroupName = new JLabel("Name");
    itemInfoPanel.add(lblGroupName, "cell 0 0,alignx right");
    
    tfGroupName = new POSTextField();
    itemInfoPanel.add(tfGroupName, "cell 1 0");
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      
      InventoryUnitGroup inventoryUnitGroup = (InventoryUnitGroup)getBean();
      InventoryUnitGroupDAO inventoryUnitGroupDAO = new InventoryUnitGroupDAO();
      inventoryUnitGroupDAO.saveOrUpdate(inventoryUnitGroup);
      
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(e.getMessage());
    }
    
    return false;
  }
  
  public void updateView() {
    InventoryUnitGroup inventoryUnitGroup = (InventoryUnitGroup)getBean();
    
    if (inventoryUnitGroup == null)
    {
      return;
    }
    
    tfGroupName.setText(inventoryUnitGroup.getName());
  }
  
  public boolean updateModel()
  {
    InventoryUnitGroup inventoryUnitGroup = (InventoryUnitGroup)getBean();
    
    String groupName = tfGroupName.getText();
    if (groupName != null) {
      inventoryUnitGroup.setName(groupName);
    }
    else {
      POSMessageDialog.showMessage("Please enter Inventory Unit Group name");
    }
    
    return true;
  }
  
  public String getDisplayText()
  {
    InventoryUnitGroup inventoryUnitGroup = (InventoryUnitGroup)getBean();
    if (inventoryUnitGroup.getId() == null) {
      return "Inventory Unit Group Entry Form";
    }
    return "Inventory Unit Group Edit Form";
  }
}
