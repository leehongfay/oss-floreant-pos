package com.floreantpos.bo.ui.explorer;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.BeanTableModel.DataType;
import com.floreantpos.swing.BeanTableModel.EditMode;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.PosTable;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;






public class DefaultPriceExplorer
  extends TransparentPanel
{
  private MenuItemDefaultPriceTable menuItemDefaultPriceTable;
  private BeanTableModel<MenuItem> tableModel;
  private JButton btnBack;
  private JButton btnForward;
  private JLabel lblNumberOfItem;
  private JTextField tfName;
  private JComboBox cbGroup;
  private JComboBox cbSortByColumn;
  private boolean dataModified;
  private JCheckBox chkSortOrder = new JCheckBox("A-Z");
  
  public DefaultPriceExplorer() {
    initcomponents();
    showMenuItems();
  }
  
  public void initcomponents() {
    setLayout(new MigLayout("fill"));
    JPanel mainPanel = new JPanel(new BorderLayout(5, 5));
    
    JPanel topPanel = new JPanel(new MigLayout("fill", "", ""));
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Default Prices");
    
    tableModel = new BeanTableModel(MenuItem.class, 50);
    tableModel.addColumn("NAME", "displayName");
    tableModel.addColumn("BARCODE", "barcode");
    tableModel.addColumn("COST (" + CurrencyUtil.getCurrencySymbol() + ")", "cost", 11, BeanTableModel.DataType.MONEY);
    tableModel.addColumn("PRICE (" + CurrencyUtil.getCurrencySymbol() + ")", "price", BeanTableModel.EditMode.EDITABLE, 11, BeanTableModel.DataType.MONEY);
    menuItemDefaultPriceTable = new MenuItemDefaultPriceTable(tableModel);
    menuItemDefaultPriceTable.setDefaultRenderer(Object.class, new CustomCellRenderer());
    menuItemDefaultPriceTable.setSelectionMode(0);
    
    menuItemDefaultPriceTable.getInputMap().put(KeyStroke.getKeyStroke(32, 0), "startEditing");
    DoubleTextField tfEditField = new DoubleTextField();
    
    tfEditField.setHorizontalAlignment(4);
    DefaultCellEditor editor = new DefaultCellEditor(tfEditField);
    editor.setClickCountToStart(1);
    
    menuItemDefaultPriceTable.setDefaultEditor(menuItemDefaultPriceTable.getColumnClass(3), editor);
    

    menuItemDefaultPriceTable.setRowHeight(PosUIManager.getSize(30));
    
    JScrollPane scrollPane = new JScrollPane(menuItemDefaultPriceTable);
    
    JPanel centerPanel = new JPanel(new MigLayout("fill"));
    centerPanel.add(scrollPane, "grow");
    
    JPanel bottomPanel = new JPanel(new MigLayout("fillx", "[][]", ""));
    
    btnBack = new JButton("<<< Previous");
    btnBack.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if ((dataModified) && 
          (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Do you want to save?", "Confirm?") == 0)) {
          try {
            MenuItemDAO.getInstance().saveAll(tableModel.getRows());
          } catch (Exception ex) {
            POSMessageDialog.showError(DefaultPriceExplorer.this, POSConstants.ERROR_MESSAGE, ex);
          }
        }
        
        dataModified = false;
        tableModel.setCurrentRowIndex(tableModel.getPreviousRowIndex());
        DefaultPriceExplorer.this.showMenuItems();
      }
    });
    btnForward = new JButton("Next >>>");
    btnForward.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if ((dataModified) && 
          (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Do you want to save?", "Confirm?") == 0)) {
          try {
            MenuItemDAO.getInstance().saveAll(tableModel.getRows());
          } catch (Exception ex) {
            POSMessageDialog.showError(DefaultPriceExplorer.this, POSConstants.ERROR_MESSAGE, ex);
          }
        }
        
        dataModified = false;
        tableModel.setCurrentRowIndex(tableModel.getNextRowIndex());
        DefaultPriceExplorer.this.showMenuItems();
      }
    });
    lblNumberOfItem = new JLabel();
    bottomPanel.add(lblNumberOfItem, "split 3, right");
    bottomPanel.add(btnBack);
    bottomPanel.add(btnForward);
    
    topPanel.add(titlePanel, "wrap,grow");
    topPanel.add(searchPanel(), "grow");
    mainPanel.add(topPanel, "North");
    mainPanel.add(centerPanel, "Center");
    mainPanel.add(bottomPanel, "South");
    
    add(mainPanel, "grow");
  }
  
  private JPanel searchPanel()
  {
    JPanel panel = new JPanel();
    panel.setLayout(new MigLayout("fill", "[][grow]", ""));
    JLabel lblName = new JLabel(Messages.getString("MenuItemExplorer.0"));
    JLabel lblGroup = new JLabel(Messages.getString("MenuItemExplorer.1"));
    cbGroup = new JComboBox();
    cbGroup.addItem(Messages.getString("MenuItemExplorer.5"));
    List<MenuGroup> groups = MenuGroupDAO.getInstance().findAll();
    for (MenuGroup group : groups) {
      cbGroup.addItem(group);
    }
    tfName = new JTextField(15);
    try {
      JButton searchBtn = new JButton(Messages.getString("MenuItemExplorer.3"));
      JButton resetBtn = new JButton("RESET");
      
      panel.add(lblName, "split 6");
      panel.add(tfName);
      panel.add(lblGroup, "");
      panel.add(cbGroup);
      panel.add(searchBtn);
      panel.add(resetBtn);
      
      panel.add(new JLabel("Sort by column"), "split 5,right");
      panel.add(new JLabel("["));
      panel.add(chkSortOrder);
      panel.add(new JLabel("]"));
      cbSortByColumn = new JComboBox();
      cbSortByColumn.addItem(null);
      cbSortByColumn.addItem(MenuItem.PROP_NAME);
      cbSortByColumn.addItem(MenuItem.PROP_BARCODE);
      cbSortByColumn.addItem(MenuItem.PROP_COST);
      cbSortByColumn.addItem(MenuItem.PROP_PRICE);
      cbSortByColumn.setSelectedItem(null);
      cbSortByColumn.addItemListener(new ItemListener()
      {

        public void itemStateChanged(ItemEvent e)
        {

          DefaultPriceExplorer.this.showMenuItems();
        }
      });
      panel.add(cbSortByColumn);
      chkSortOrder.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          DefaultPriceExplorer.this.showMenuItems();
        }
        
      });
      chkSortOrder.setSelected(true);
      
      Border loweredetched = BorderFactory.createEtchedBorder(1);
      TitledBorder title = BorderFactory.createTitledBorder(loweredetched, Messages.getString("MenuItemExplorer.30"));
      title.setTitleJustification(1);
      panel.setBorder(title);
      
      searchBtn.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          DefaultPriceExplorer.this.showMenuItems();
        }
        
      });
      tfName.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          DefaultPriceExplorer.this.showMenuItems();
        }
        
      });
      resetBtn.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          tfName.setText("");
          cbGroup.setSelectedIndex(0);
          DefaultPriceExplorer.this.showMenuItems();
        }
      });
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
    return panel;
  }
  
  private void showMenuItems() {
    String txName = tfName.getText();
    MenuGroup group = null;
    Object selectedGroup = cbGroup.getSelectedItem();
    if ((selectedGroup instanceof MenuGroup)) {
      group = (MenuGroup)selectedGroup;
    }
    try
    {
      tableModel.setNumRows(MenuItemDAO.getInstance().rowCount(Boolean.valueOf(true), group, txName));
      Object selectedItem = cbSortByColumn.getSelectedItem();
      tableModel.setSortBy((String)selectedItem, chkSortOrder.isSelected());
      MenuItemDAO.getInstance().loadMenuItems(tableModel, Boolean.valueOf(true), group, txName);
      
      int startNumber = tableModel.getCurrentRowIndex() + 1;
      int endNumber = tableModel.getNextRowIndex();
      int totalNumber = tableModel.getNumRows();
      if (endNumber > totalNumber) {
        endNumber = totalNumber;
      }
      lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
      btnBack.setEnabled(tableModel.hasPrevious());
      btnForward.setEnabled(tableModel.hasNext());
    } catch (Exception ex) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, ex);
    }
  }
  
  private class MenuItemDefaultPriceTable extends PosTable
  {
    public MenuItemDefaultPriceTable()
    {
      super();
      setSortable(false);
    }
    

    public void setValueAt(Object prices, int row, int column)
    {
      String receiveStr = (String)prices;
      if (receiveStr.isEmpty())
        return;
      Double price = Double.valueOf(Double.parseDouble(receiveStr));
      
      MenuItem menuItem = (MenuItem)tableModel.getRow(row);
      super.setValueAt(price, row, column);
      if (column == 3) {
        menuItem.setPrice(price);
        MenuItemDAO.getInstance().saveOrUpdate(menuItem);
      }
    }
  }
}
