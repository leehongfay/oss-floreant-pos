package com.floreantpos.bo.ui.explorer;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.CookingInstruction;
import com.floreantpos.model.dao.CookingInstructionDAO;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;



















public class CookingInstructionExplorer
  extends TransparentPanel
{
  private List<CookingInstruction> categoryList;
  private JTable table;
  private CookingInstructionTableModel tableModel;
  CookingInstructionDAO dao = new CookingInstructionDAO();
  
  public CookingInstructionExplorer() {
    categoryList = dao.findAll();
    
    tableModel = new CookingInstructionTableModel();
    table = new JTable(tableModel);
    table.setRowHeight(PosUIManager.getSize(30));
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          String instruction = JOptionPane.showInputDialog(POSUtil.getBackOfficeWindow(), POSConstants.ENTER_INSTRUCTION_DESCRIPTION);
          if (instruction == null) {
            BOMessageDialog.showError(POSUtil.getBackOfficeWindow(), POSConstants.INSTRUCTION_CANNOT_BE_EMPTY);
            return;
          }
          if (instruction.length() > 60) {
            BOMessageDialog.showError(POSUtil.getBackOfficeWindow(), POSConstants.LONG_INSTRUCTION_ERROR);
            return;
          }
          
          CookingInstruction cookingInstruction = new CookingInstruction();
          cookingInstruction.setDescription(instruction);
          dao.save(cookingInstruction);
          
          tableModel.add(cookingInstruction);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          CookingInstruction cookingInstruction = (CookingInstruction)categoryList.get(index);
          String instruction = JOptionPane.showInputDialog(POSUtil.getBackOfficeWindow(), POSConstants.ENTER_INSTRUCTION_DESCRIPTION, cookingInstruction
            .getDescription());
          
          if (instruction == null) {
            BOMessageDialog.showError(POSUtil.getBackOfficeWindow(), POSConstants.INSTRUCTION_CANNOT_BE_EMPTY);
            return;
          }
          if (instruction.length() > 60) {
            BOMessageDialog.showError(POSUtil.getBackOfficeWindow(), POSConstants.LONG_INSTRUCTION_ERROR);
            return;
          }
          cookingInstruction.setDescription(instruction);
          dao.saveOrUpdate(cookingInstruction);
          table.repaint();
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          if (ConfirmDeleteDialog.showMessage(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0)
          {
            CookingInstruction cookingInstruction = (CookingInstruction)categoryList.get(index);
            dao.delete(cookingInstruction);
            tableModel.delete(cookingInstruction, index);
          }
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    add(panel, "South"); }
  
  class CookingInstructionTableModel extends AbstractTableModel { CookingInstructionTableModel() {}
    
    String[] columnNames = { POSConstants.DESCRIPTION };
    
    public int getRowCount() {
      if (categoryList == null) {
        return 0;
      }
      return categoryList.size();
    }
    
    public int getColumnCount() {
      return 1;
    }
    
    public String getColumnName(int column)
    {
      return columnNames[column];
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      return false;
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      if (categoryList == null) {
        return "";
      }
      CookingInstruction cookingInstruction = (CookingInstruction)categoryList.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return cookingInstruction.getDescription();
      }
      
      return null;
    }
    
    public void add(CookingInstruction instruction) {
      int size = categoryList.size();
      categoryList.add(instruction);
      fireTableRowsInserted(size, size);
    }
    
    public void delete(CookingInstruction instruction, int index) {
      categoryList.remove(instruction);
      fireTableRowsDeleted(index, index);
    }
  }
}
