package com.floreantpos.bo.ui.explorer;

import com.floreantpos.swing.TransparentPanel;
import java.awt.BorderLayout;
import javax.swing.JTabbedPane;


















public class ShiftExplorer
  extends TransparentPanel
{
  public ShiftExplorer()
  {
    initComponents();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    JTabbedPane jTabbedPane = new JTabbedPane();
    jTabbedPane.addTab("Business hour", new BusinessHourExplorer());
    jTabbedPane.addTab("Menu Shift", new MenuShiftExplorer());
    
    add(jTabbedPane);
  }
}
