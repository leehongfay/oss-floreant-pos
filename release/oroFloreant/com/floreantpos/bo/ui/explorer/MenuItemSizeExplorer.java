package com.floreantpos.bo.ui.explorer;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.MenuItemSize;
import com.floreantpos.model.dao.MenuItemSizeDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.model.MenuItemSizeForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import org.jdesktop.swingx.JXTable;


public class MenuItemSizeExplorer
  extends TransparentPanel
{
  private JXTable table;
  private BeanTableModel<MenuItemSize> tableModel;
  
  public MenuItemSizeExplorer()
  {
    tableModel = new BeanTableModel(MenuItemSize.class);
    
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "name");
    tableModel.addColumn(Messages.getString("MenuItemSizeExplorer.0"), "translatedName");
    tableModel.addColumn(Messages.getString("MenuItemSizeExplorer.2"), "description");
    tableModel.addColumn(Messages.getString("MenuItemSizeExplorer.4"), "sizeInInch");
    tableModel.addColumn(Messages.getString("MenuItemSizeExplorer.6"), "sortOrder");
    tableModel.addColumn("Default", MenuItemSize.PROP_DEFAULT_SIZE);
    
    tableModel.addRows(MenuItemSizeDAO.getInstance().findAll());
    table = new JXTable(tableModel);
    table.setRowHeight(PosUIManager.getSize(30));
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          MenuItemSizeForm editor = new MenuItemSizeForm();
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          MenuItemSize foodCategory = (MenuItemSize)editor.getBean();
          tableModel.addRow(foodCategory);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          MenuItemSize menuItemSize = (MenuItemSize)tableModel.getRow(index);
          
          MenuItemSizeForm editor = new MenuItemSizeForm(menuItemSize);
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          if (dialog.isCanceled()) {
            return;
          }
          table.repaint();
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          if (ConfirmDeleteDialog.showMessage(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0)
          {
            MenuItemSize menuItemSize = (MenuItemSize)tableModel.getRow(index);
            MenuItemSizeDAO dao = new MenuItemSizeDAO();
            dao.delete(menuItemSize);
            tableModel.removeRow(index);
          }
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    JButton defaultButton = new JButton("Set Default");
    defaultButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          MenuItemSize menuItemSize = (MenuItemSize)tableModel.getRow(index);
          
          List<MenuItemSize> sizeList = new ArrayList();
          sizeList.add(menuItemSize);
          
          for (MenuItemSize item : tableModel.getRows()) {
            if (item.isDefaultSize().booleanValue()) {
              item.setDefaultSize(Boolean.valueOf(false));
              sizeList.add(item);
            }
          }
          menuItemSize.setDefaultSize(Boolean.valueOf(true));
          
          MenuItemSizeDAO dao = new MenuItemSizeDAO();
          dao.saveOrUpdateList(sizeList);
          tableModel.fireTableDataChanged();
          table.revalidate();
          table.repaint();
        }
        catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    panel.add(defaultButton);
    add(panel, "South");
  }
}
