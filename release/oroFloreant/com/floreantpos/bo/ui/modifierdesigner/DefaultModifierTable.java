package com.floreantpos.bo.ui.modifierdesigner;

import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.DefaultMenuModifier;
import com.floreantpos.model.Multiplier;
import com.floreantpos.model.dao.MultiplierDAO;
import com.floreantpos.swing.ListTableModel;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.ComboBoxEditor;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.text.JTextComponent;

public class DefaultModifierTable extends JTable
{
  private DefaultModifierTableModel model;
  
  public DefaultModifierTable(List<DefaultMenuModifier> defaultModifiers)
  {
    putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    
    model = new DefaultModifierTableModel();
    model.setRows(defaultModifiers);
    setModel(model);
    
    getTableHeader().setPreferredSize(new Dimension(0, 35));
    setDefaultRenderer(Object.class, new CustomCellRenderer());
    setRowHeight(com.floreantpos.swing.PosUIManager.getSize(40));
    
    List<Multiplier> multipliers = MultiplierDAO.getInstance().findAll();
    JComboBox cbMultipliers = new JComboBox(multipliers.toArray());
    
    cbMultipliers.setSelectedItem("");
    JTextComponent editor = (JTextComponent)cbMultipliers.getEditor().getEditorComponent();
    
    editor.addKeyListener(new KeyListener()
    {
      public void keyTyped(KeyEvent e) {}
      


      public void keyPressed(KeyEvent e)
      {
        if (e.getKeyChar() == '\n') {
          JComponent ja = (JComponent)e.getSource();
          JComboBox jcbloc = (JComboBox)ja.getParent();
          JTable jtb = (JTable)jcbloc.getParent();
          jtb.changeSelection(0, 1, false, false);
        }
      }
      




      public void keyReleased(KeyEvent e) {}
    });
    editor.addFocusListener(new FocusListener()
    {
      public void focusGained(FocusEvent e)
      {
        JComponent ja = (JComponent)e.getSource();
        JComponent jcbloc = (JComponent)ja.getParent();
        JComboBox jcb = (JComboBox)jcbloc;
        jcb.setPopupVisible(true);
        JTextComponent editor = (JTextComponent)jcb.getEditor().getEditorComponent();
        editor.setSelectionStart(0);
        editor.setSelectionEnd(editor.getText().length());
      }
      



      public void focusLost(FocusEvent e) {}
    });
    TableColumn colCombo = getColumnModel().getColumn(1);
    cbMultipliers.setEditable(true);
    ComboBoxEditor cbe = new ComboBoxEditor(cbMultipliers);
    colCombo.setCellEditor(cbe);
    
    TableColumn colQuantity = getColumnModel().getColumn(2);
    TextCellEditor quantityEditor = new TextCellEditor(new JTextField());
    colQuantity.setCellEditor(quantityEditor);
  }
  
  public boolean isCellEditable(int row, int column)
  {
    if ((column == 1) || (column == 2))
      return true;
    return super.isCellEditable(row, column);
  }
  
  public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend)
  {
    super.changeSelection(rowIndex, columnIndex, toggle, extend);
    if (columnIndex > -1)
    {
      if (editCellAt(rowIndex, columnIndex)) {
        Component editor = getEditorComponent();
        editor.requestFocusInWindow();
        if ((editor instanceof JTextField))
        {
          JTextField jf = (JTextField)editor;
          jf.select(0, jf.toString().length());
        }
        if ((editor instanceof JComboBox)) {
          JComboBox jcb = (JComboBox)editor;
          jcb.setPopupVisible(true);
          JTextComponent editorCombo = (JTextComponent)jcb.getEditor().getEditorComponent();
          editorCombo.setSelectionStart(0);
          editorCombo.setSelectionEnd(editorCombo.getText().length());
        }
      }
    }
  }
  
  private class DefaultModifierTableModel extends ListTableModel<DefaultMenuModifier>
  {
    public DefaultModifierTableModel() {
      super();
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      DefaultMenuModifier item = (DefaultMenuModifier)rows.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return item.getModifier();
      
      case 1: 
        return item.getMultiplier();
      
      case 2: 
        return item.getQuantity();
      }
      
      return null;
    }
    
    public void setValueAt(Object aValue, int rowIndex, int columnIndex)
    {
      DefaultMenuModifier item = (DefaultMenuModifier)rows.get(rowIndex);
      switch (columnIndex) {
      case 1: 
        item.setMultiplier((Multiplier)aValue);
        break;
      
      case 2: 
        double quantity = 1.0D;
        try {
          quantity = Double.valueOf(String.valueOf(aValue)).doubleValue();
        }
        catch (Exception localException) {}
        item.setQuantity(Double.valueOf(quantity));
        break;
      default: 
        super.setValueAt(aValue, rowIndex, columnIndex);
      }
    }
  }
  
  public void setItems(List<DefaultMenuModifier> defaultModifers)
  {
    model.setRows(defaultModifers);
  }
  
  public class ComboBoxEditor extends AbstractCellEditor implements TableCellEditor {
    JComboBox comboBox;
    
    public ComboBoxEditor(JComboBox jcb) {
      comboBox = jcb;
    }
    
    public Object getCellEditorValue() {
      return comboBox.getSelectedItem();
    }
    
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
      comboBox.setSelectedItem(value);
      return comboBox;
    }
    
    public boolean stopCellEditing() {
      fireEditingStopped();
      return true;
    }
  }
  
  public class TextCellEditor extends AbstractCellEditor implements TableCellEditor
  {
    JTextField jtextfield;
    
    public TextCellEditor(JTextField jtf) {
      jtextfield = jtf;
    }
    
    public Object getCellEditorValue() {
      return jtextfield.getText();
    }
    
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
      if ((!isSelected) || 
      
        (value == null)) {
        value = "";
      }
      value = value.toString();
      if ((value instanceof Integer)) {
        value = value.toString();
      }
      jtextfield.setText((String)value);
      return jtextfield;
    }
    
    public boolean stopCellEditing() {
      fireEditingStopped();
      return true;
    }
  }
}
