package com.floreantpos.bo.ui.modifierdesigner;

import com.floreantpos.PosLog;
import com.floreantpos.bo.ui.explorer.PageSelectionListener;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.model.MenuItemModifierPage;
import com.floreantpos.model.MenuItemModifierPageItem;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.model.MenuModifierForm;
import com.floreantpos.ui.model.PizzaModifierForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;

































public class ModifierPageDesignView
  extends TransparentPanel
{
  public static final String VIEW_NAME = "ITEM_VIEW";
  private static final int HORIZONTAL_GAP = 5;
  private static final int VERTICAL_GAP = 5;
  private List<MenuItemModifierPageItem> items;
  private final Dimension buttonSize;
  private MenuItemModifierPage menuItemModifierPage;
  private final JPanel buttonPanelContainer = new JPanel(new MigLayout("center,wrap 4"));
  private PageSelectionListener pageListener;
  private Integer cols;
  private Integer rows;
  private boolean pizzaItem;
  
  public ModifierPageDesignView()
  {
    buttonSize = new Dimension(TerminalConfig.getMenuItemButtonWidth(), TerminalConfig.getMenuItemButtonHeight());
    setLayout(new BorderLayout(5, 5));
    add(buttonPanelContainer);
  }
  
  public void setPageListener(PageSelectionListener listener) {
    pageListener = listener;
  }
  
  public void setPizzaItem(boolean pizzaItem) {
    this.pizzaItem = pizzaItem;
  }
  
  public MenuItemModifierPage getMenuPage() {
    return menuItemModifierPage;
  }
  
  public void setMenuItemModifierPage(MenuItemModifierPage page) {
    menuItemModifierPage = page;
    reset();
    try {
      if (menuItemModifierPage == null) {
        return;
      }
      cols = menuItemModifierPage.getCols();
      if (cols.intValue() <= 0) {
        cols = Integer.valueOf(4);
      }
      rows = menuItemModifierPage.getRows();
      if (rows.intValue() <= 0) {
        rows = Integer.valueOf(4);
      }
      MigLayout migLayout = new MigLayout("center,wrap " + cols);
      if (menuItemModifierPage.isFlixibleButtonSize().booleanValue()) {
        migLayout.setLayoutConstraints("fill");
        migLayout.setColumnConstraints("fill,grow");
        migLayout.setRowConstraints("fill,grow");
      }
      buttonPanelContainer.setLayout(migLayout);
      items = menuItemModifierPage.getPageItems();
      renderItems();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public void setMenuItems(List<MenuItemModifierPageItem> items) {
    this.items = items;
    try {
      renderItems();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  protected void renderItems() {
    reset();
    if (items == null)
      items = new ArrayList();
    Map<String, ModifierButton> cellItemMap;
    int row;
    try { cellItemMap = new HashMap();
      for (row = 0; row < rows.intValue(); row++) {
        for (int col = 0; col < cols.intValue(); col++) {
          String cellKey = String.valueOf(col) + String.valueOf(row);
          MenuItemModifierPageItem itemForCell = new MenuItemModifierPageItem(Integer.valueOf(col), Integer.valueOf(row));
          itemForCell.setParentPage(menuItemModifierPage);
          ModifierButton modifierButton = (ModifierButton)createItemButton(itemForCell);
          cellItemMap.put(cellKey, modifierButton);
          String constraint = String.format("cell %s %s", new Object[] { Integer.valueOf(col), Integer.valueOf(row) });
          if (!menuItemModifierPage.isFlixibleButtonSize().booleanValue()) {
            constraint = constraint + String.format(", w %s!, h %s!", new Object[] { menuItemModifierPage.getButtonWidth(), menuItemModifierPage.getButtonHeight() });
          }
          buttonPanelContainer.add(modifierButton, constraint);
        }
      }
      for (MenuItemModifierPageItem item : items) {
        String cellKey = String.valueOf(item.getCol()) + String.valueOf(item.getRow());
        ModifierButton itemCellButton = (ModifierButton)cellItemMap.get(cellKey);
        if (itemCellButton != null)
        {
          itemCellButton.setMenuModifier(item.getMenuModifier());
        }
      }
    } catch (Exception localException) {}
    revalidate();
    repaint();
  }
  
  public void reset() {
    buttonPanelContainer.removeAll();
  }
  
  public void fillSelectedPageItems(List<MenuItemModifierPageItem> items) {
    int count = 0;
    for (int i = 0; i < buttonPanelContainer.getComponents().length; i++) {
      ModifierButton button = (ModifierButton)buttonPanelContainer.getComponent(i);
      if (button.isEmptyItem()) {
        MenuItemModifierPageItem pageItem = (MenuItemModifierPageItem)items.get(count);
        button.setMenuModifier(pageItem.getMenuModifier());
        menuItemModifierPage.addTopageItems(button.getMenuItemModifierPageItem());
        count++;
      }
      if (count == items.size())
        break;
    }
  }
  
  protected AbstractButton createItemButton(Object item) {
    MenuItemModifierPageItem menuPageItem = (MenuItemModifierPageItem)item;
    
    ModifierButton modifierButton = new ModifierButton(menuPageItem);
    return modifierButton;
  }
  
  public class ModifierButton extends PosButton implements ActionListener {
    MenuItemModifierPageItem menuItemModifierPageItem;
    
    ModifierButton(MenuItemModifierPageItem menuPageItem) {
      menuItemModifierPageItem = menuPageItem;
      setFocusable(false);
      setVerticalTextPosition(3);
      setHorizontalTextPosition(0);
      setIconTextGap(0);
      setText("+");
      setPreferredSize(buttonSize);
      addActionListener(this);
      updateView();
    }
    
    public void setMenuItemModifierPageItem(MenuItemModifierPageItem item) {
      menuItemModifierPageItem = item;
    }
    
    public MenuItemModifierPageItem getMenuItemModifierPageItem() {
      return menuItemModifierPageItem;
    }
    
    public void setMenuModifier(MenuModifier menuModifier) {
      menuItemModifierPageItem.setMenuModifier(menuModifier);
      menuItemModifierPageItem.setParentPage(menuItemModifierPage);
      updateView();
    }
    
    public boolean isEmptyItem() {
      return menuItemModifierPageItem.getMenuModifier() == null;
    }
    
    private void updateView() {
      if (StringUtils.isEmpty(menuItemModifierPageItem.getMenuModifierId())) {
        setText("+");
        return;
      }
      
      ImageIcon image = menuItemModifierPageItem.getImage();
      if (image != null) {
        if (menuItemModifierPageItem.isShowImageOnly().booleanValue()) {
          setText("");
          setIcon(image);
        }
        else {
          setIcon(image);
          setText("<html><body><center>" + menuItemModifierPageItem.getMenuModifierName() + "</center></body></html>");
        }
        
      }
      else {
        setText("<html><body><center>" + menuItemModifierPageItem.getMenuModifierName() + "</center></body></html>");
      }
      
      setBackground(menuItemModifierPageItem.getButtonColor());
      setForeground(menuItemModifierPageItem.getTextColor());
    }
    
    public void actionPerformed(ActionEvent e)
    {
      doSelectMenuItem();
    }
    
    private void doSelectMenuItem() {
      try {
        MenuModifier menuModifier = menuItemModifierPageItem.getMenuModifier();
        List<MenuModifier> existingItems = new ArrayList();
        if (menuModifier != null) {
          existingItems.add(menuModifier);
        }
        if (menuModifier == null) {
          menuModifier = doAddMenuModifier();
          if (menuModifier != null) {}
        }
        else
        {
          ModifierActionSelectorDialog dialog = new ModifierActionSelectorDialog(menuItemModifierPageItem);
          dialog.setSize(PosUIManager.getSize(500, 350));
          dialog.open();
          if (dialog.isCanceled())
            return;
          String actionCommand = dialog.getActionCommand();
          if (actionCommand.equals("REPLACE")) {
            menuModifier = doAddMenuModifier();
            if (menuModifier != null) {}

          }
          else if (actionCommand.equals("EDIT")) {
            if (menuModifier.isPizzaModifier().booleanValue()) {
              PizzaModifierForm editor = new PizzaModifierForm();
              BeanEditorDialog dialog2 = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
              dialog2.open();
              if (dialog2.isCanceled()) {
                return;
              }
            }
            else {
              MenuModifierForm editor = new MenuModifierForm(menuModifier);
              BeanEditorDialog formDialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
              formDialog.open();
              if (formDialog.isCanceled()) {
                return;
              }
            }
          } else if (actionCommand.equals("DETACH")) {
            for (Iterator iterator = items.iterator(); iterator.hasNext();) {
              MenuItemModifierPageItem item = (MenuItemModifierPageItem)iterator.next();
              if (item.getMenuModifier() == menuItemModifierPageItem.getMenuModifier()) {
                iterator.remove();
              }
            }
            renderItems();
            if (pageListener == null)
              return;
            pageListener.itemSelected(null);
            return;
          }
        }
        menuItemModifierPageItem.setMenuModifier(menuModifier);
        menuItemModifierPageItem.setParentPage(menuItemModifierPage);
        updateView();
        
        if (pageListener == null)
          return;
        menuItemModifierPage.addTopageItems(menuItemModifierPageItem);
        pageListener.itemSelected(menuItemModifierPageItem);
      } catch (Exception e) {
        PosLog.error(getClass(), e);
      }
    }
    
    private MenuModifier doAddMenuModifier() {
      MenuItemModifierSpec menuItemModifierSpec = menuItemModifierPage.getModifierSpec();
      ModifierSelectionDialog dialog = ModifierSelectionDialog.getInstance(new ArrayList(), pizzaItem, true);
      if (menuItemModifierSpec != null) {
        dialog.setSelectedGroup(menuItemModifierSpec.getModifierGroup());
      }
      dialog.setSize(PosUIManager.getSize(600, 500));
      dialog.open();
      
      if (dialog.isCanceled())
        return null;
      return dialog.getSelectedRowData();
    }
  }
}
