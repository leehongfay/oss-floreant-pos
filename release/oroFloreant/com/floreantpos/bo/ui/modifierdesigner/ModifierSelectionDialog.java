package com.floreantpos.bo.ui.modifierdesigner;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.ModifierGroup;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.model.MenuModifierForm;
import com.floreantpos.ui.model.MultipleModifierSelectionView;
import com.floreantpos.ui.model.PizzaModifierForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
















public class ModifierSelectionDialog
  extends POSDialog
  implements ActionListener
{
  private MultipleModifierSelectionView menuSelectorPanel;
  private List<MenuModifier> modifierList;
  private boolean pizzaModifierGroup;
  private boolean singleSelectionEnable;
  private TitlePanel titelpanel;
  private static ModifierSelectionDialog instance;
  private PosButton btnAdd;
  
  public ModifierSelectionDialog(List<MenuModifier> modifierList, boolean pizzaModifierGroup)
  {
    super(POSUtil.getFocusedWindow(), "");
    this.modifierList = modifierList;
    this.pizzaModifierGroup = pizzaModifierGroup;
    init();
  }
  
  private void init() {
    setLayout(new BorderLayout(5, 5));
    setTitle("Select Modifier");
    titelpanel = new TitlePanel();
    titelpanel.setTitle("Select one or more modifiers");
    
    add(titelpanel, "North");
    
    JPanel contentPane = new JPanel(new MigLayout("fill,hidemode 3,inset 0 10 0 10"));
    menuSelectorPanel = new MultipleModifierSelectionView(modifierList, pizzaModifierGroup);
    contentPane.add(menuSelectorPanel, "grow,span,wrap");
    
    PosButton btnOk = new PosButton("SELECT");
    btnOk.setActionCommand(POSConstants.OK);
    btnOk.setBackground(Color.GREEN);
    
    btnOk.setFocusable(false);
    btnOk.addActionListener(this);
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL);
    btnCancel.setFocusable(false);
    btnCancel.addActionListener(this);
    
    JPanel footerPanel = new JPanel(new MigLayout("center,ins 0 5 5 5", "", ""));
    
    PosButton btnEdit = new PosButton();
    btnAdd = new PosButton();
    
    btnAdd.setText(POSConstants.ADD.toUpperCase());
    btnEdit.setText(POSConstants.EDIT.toUpperCase());
    
    btnEdit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        ModifierSelectionDialog.this.editSelectedRow();
      }
      

    });
    btnAdd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          MenuModifier menuModifier = new MenuModifier();
          BeanEditor<MenuModifier> editor = null;
          
          if (pizzaModifierGroup) {
            editor = new PizzaModifierForm(menuModifier);
          }
          else {
            editor = new MenuModifierForm(menuModifier);
          }
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          MenuModifier modifierItem = (MenuModifier)editor.getBean();
          menuSelectorPanel.getModel().addRow(modifierItem);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    footerPanel.add(btnAdd);
    footerPanel.add(btnEdit);
    footerPanel.add(btnOk);
    footerPanel.add(btnCancel);
    
    add(footerPanel, "South");
    
    add(contentPane);
  }
  
  public void setSingleSelectionEnable(boolean enable) {
    singleSelectionEnable = enable;
    if (enable) {
      titelpanel.setTitle("SELECT A MODIFIER");
    }
    menuSelectorPanel.setSingleSelectionEnable(enable);
  }
  
  public void setSelectedGroup(ModifierGroup group) {
    menuSelectorPanel.setSelectedGroup(group);
  }
  
  private void doOk() {
    if (singleSelectionEnable) {
      MenuModifier modifier = getSelectedRowData();
      if (modifier == null) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select modifier");
        return;
      }
    }
    else {
      List<MenuModifier> menuModifiers = menuSelectorPanel.getSelectedMenuModifierList();
      if ((menuModifiers == null) || (menuModifiers.isEmpty())) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select modifier");
        return;
      }
    }
    setCanceled(false);
    dispose();
  }
  
  private void doCancel() {
    setCanceled(true);
    dispose();
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    
    if (POSConstants.CANCEL.equalsIgnoreCase(actionCommand)) {
      doCancel();
    }
    else if (POSConstants.OK.equalsIgnoreCase(actionCommand)) {
      doOk();
    }
  }
  
  private void editSelectedRow() {
    try {
      int index = menuSelectorPanel.getSelectedRow();
      if (index < 0)
        return;
      MenuModifier menuModifier = (MenuModifier)menuSelectorPanel.getModel().getRow(index);
      menuSelectorPanel.getModel().setRow(index, menuModifier);
      
      BeanEditor<MenuModifier> editor = null;
      
      if (menuModifier.isPizzaModifier().booleanValue()) {
        editor = new PizzaModifierForm(menuModifier);
      }
      else {
        editor = new MenuModifierForm(menuModifier);
      }
      
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      menuSelectorPanel.repaintTable();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public List<MenuModifier> getSelectedMenuModifierList() {
    return menuSelectorPanel.getSelectedMenuModifierList();
  }
  
  public MenuModifier getSelectedRowData() {
    int index = menuSelectorPanel.getSelectedRow();
    if (index < 0)
      return null;
    return (MenuModifier)menuSelectorPanel.getModel().getRow(index);
  }
  
  public static ModifierSelectionDialog getInstance(List<MenuModifier> modifierList, boolean pizzaModifierGroup, boolean singleSelection) {
    if (instance == null) {
      instance = new ModifierSelectionDialog(modifierList, pizzaModifierGroup);
      instance.setSingleSelectionEnable(singleSelection);
    }
    return instance;
  }
  
  public void setModifiers(List<MenuModifier> modifiers) {
    btnAdd.setVisible(false);
    menuSelectorPanel.setModifiers(modifiers);
  }
}
