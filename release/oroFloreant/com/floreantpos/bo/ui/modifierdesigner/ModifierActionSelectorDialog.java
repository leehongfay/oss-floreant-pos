package com.floreantpos.bo.ui.modifierdesigner;

import com.floreantpos.POSConstants;
import com.floreantpos.model.MenuItemModifierPage;
import com.floreantpos.model.MenuItemModifierPageItem;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.dao.MenuItemModifierPageDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;


















public class ModifierActionSelectorDialog
  extends POSDialog
  implements ActionListener
{
  private String actionCommand;
  
  public ModifierActionSelectorDialog(MenuItemModifierPageItem pageItem)
  {
    super(POSUtil.getFocusedWindow(), "Select an action");
    setLayout(new BorderLayout(5, 5));
    
    MenuModifier modifier = pageItem.getMenuModifier();
    MenuItemModifierPage page = MenuItemModifierPageDAO.getInstance().get(pageItem.getParentPageId());
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle(modifier.getName());
    
    add(titlePanel, "North");
    
    JPanel itemInfoPanel = new JPanel(new MigLayout("fill,inset 0"));
    itemInfoPanel.setBorder(BorderFactory.createCompoundBorder(new TitledBorder("-"), BorderFactory.createEmptyBorder(10, 20, 10, 10)));
    
    JLabel pictureLabel = new JLabel(modifier.getImage());
    if (modifier.getImage() == null) {
      pictureLabel.setText("NO IMAGE");
      pictureLabel.setForeground(Color.gray);
    }
    pictureLabel.setPreferredSize(new Dimension(120, 120));
    pictureLabel.setBorder(BorderFactory.createBevelBorder(0));
    
    String group = "";
    if (page != null) {
      MenuItemModifierSpec menuModifierSpec = page.getModifierSpec();
      group = "Group: " + (menuModifierSpec == null ? "" : menuModifierSpec.getName());
    }
    String standardPrice = "Reg. Price: " + modifier.getPrice();
    String extraPrice = "Extra Price: " + modifier.getExtraPrice();
    
    JLabel lblMenuItemInfo = new JLabel("<html><body>" + group + "<br>" + standardPrice + "<br>" + extraPrice + "</body></html>");
    lblMenuItemInfo.setFont(new Font(null, 0, 14));
    itemInfoPanel.add(lblMenuItemInfo);
    itemInfoPanel.add(pictureLabel, "span,wrap");
    
    add(itemInfoPanel);
    
    JPanel actionButtonPanel = new JPanel(new GridLayout(1, 0, 2, 2));
    actionButtonPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
    
    PosButton btnDetach = new PosButton("DETACH");
    btnDetach.addActionListener(this);
    actionButtonPanel.add(btnDetach);
    
    PosButton btnEdit = new PosButton("EDIT");
    btnEdit.addActionListener(this);
    actionButtonPanel.add(btnEdit);
    
    PosButton btnReplace = new PosButton("REPLACE");
    btnReplace.addActionListener(this);
    actionButtonPanel.add(btnReplace);
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL_BUTTON_TEXT);
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
      
    });
    actionButtonPanel.add(btnCancel);
    add(actionButtonPanel, "South");
  }
  
  public String getActionCommand() {
    return actionCommand;
  }
  
  public void actionPerformed(ActionEvent e)
  {
    actionCommand = e.getActionCommand();
    setCanceled(false);
    dispose();
  }
}
