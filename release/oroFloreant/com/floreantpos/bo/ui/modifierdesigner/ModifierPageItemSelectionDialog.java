package com.floreantpos.bo.ui.modifierdesigner;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.bo.ui.explorer.ExplorerButtonPanel;
import com.floreantpos.model.MenuItemModifierPage;
import com.floreantpos.model.MenuItemModifierPageItem;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.ModifierGroup;
import com.floreantpos.model.dao.ModifierGroupDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PaginatedListModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.model.MenuModifierForm;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;






















public class ModifierPageItemSelectionDialog
  extends POSDialog
{
  private JXTable table;
  private BeanTableModel<MenuModifier> tableModel;
  private JTextField tfName;
  private MenuModifier selectedMenuModifier;
  private final PaginatedListModel dataModel = new PaginatedListModel(20);
  private PosButton btnNext;
  private PosButton btnPrev;
  private ModifierGroup selectedGroup;
  private static ModifierPageItemSelectionDialog instance;
  private MenuItemModifierSpec modifierSpec;
  private String exstingPageName;
  private List modifierList;
  private TableRowSorter<TableModel> sorter;
  
  public ModifierPageItemSelectionDialog() {
    super(POSUtil.getFocusedWindow(), "");
    init();
    dataModel.setCurrentRowIndex(0);
  }
  
  private void init() {
    setLayout(new BorderLayout(5, 5));
    tableModel = new BeanTableModel(MenuModifier.class);
    
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "name");
    tableModel.addColumn(POSConstants.PRICE.toUpperCase() + " (" + CurrencyUtil.getCurrencySymbol() + ")", "price");
    
    table = new JXTable(tableModel);
    table.setSelectionMode(0);
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    
    sorter = new TableRowSorter(tableModel);
    table.setRowSorter(sorter);
    table.setRowHeight(PosUIManager.getSize(40));
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          ModifierPageItemSelectionDialog.this.editSelectedRow();
        }
      }
    });
    JPanel contentPanel = new JPanel(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(10, 5, 10, 5));
    contentPanel.add(new JScrollPane(table));
    contentPanel.add(buildSearchForm(), "North");
    
    add(contentPanel);
    resizeColumnWidth(table);
    add(createButtonPanel(), "South");
    
    JPanel paginationButtonPanel = new JPanel(new MigLayout("ins 5 0 0 0,fillx", "", ""));
    
    btnPrev = new PosButton();
    btnPrev.setIcon(IconFactory.getIcon("/ui_icons/", "previous.png"));
    paginationButtonPanel.add(btnPrev, "split 3,center");
    
    PosButton btnDot = new PosButton();
    btnDot.setBorder(null);
    btnDot.setOpaque(false);
    btnDot.setContentAreaFilled(false);
    btnDot.setIcon(IconFactory.getIcon("/ui_icons/", "dot.png"));
    paginationButtonPanel.add(btnDot, "w 10!");
    
    btnNext = new PosButton();
    btnNext.setIcon(IconFactory.getIcon("/ui_icons/", "next.png"));
    paginationButtonPanel.add(btnNext);
    paginationButtonPanel.add(new JSeparator(), "newline,span,grow");
    
    contentPanel.add(paginationButtonPanel, "South");
    
    ActionListener action = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          Object source = e.getSource();
          if (source == btnPrev) {
            ModifierPageItemSelectionDialog.this.scrollUp();
          }
          else if (source == btnNext) {
            ModifierPageItemSelectionDialog.this.scrollDown();
          }
        } catch (Exception e2) {
          POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e2.getMessage(), e2);
        }
        
      }
    };
    btnPrev.addActionListener(action);
    btnNext.addActionListener(action);
    
    btnNext.setEnabled(false);
    btnPrev.setEnabled(false);
  }
  
  private JPanel buildSearchForm() {
    JPanel panel = new JPanel();
    panel.setLayout(new MigLayout("inset 5 0 5 0", "[][grow][][][]", "[]5[]"));
    JLabel lblName = new JLabel(POSConstants.NAME);
    tfName = new JTextField(15);
    JButton searchBttn = new JButton(POSConstants.SEARCH_ITEM_BUTTON_TEXT);
    panel.add(lblName, "align label");
    panel.add(tfName, "grow");
    panel.add(searchBttn);
    
    Border loweredetched = BorderFactory.createEtchedBorder(1);
    TitledBorder title = BorderFactory.createTitledBorder(loweredetched, POSConstants.SEARCH_ITEM_BUTTON_TEXT);
    title.setTitleJustification(1);
    panel.setBorder(title);
    
    searchBttn.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ModifierPageItemSelectionDialog.this.searchItem();
      }
      
    });
    tfName.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ModifierPageItemSelectionDialog.this.searchItem();
      }
    });
    return panel;
  }
  
  private void searchItem() {
    setDataModel(dataModel);
    if (dataModel.getSize() > 0) {
      table.setRowSelectionInterval(0, 0);
    }
  }
  
  private TransparentPanel createButtonPanel() {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton btnEdit = explorerButton.getEditButton();
    JButton btnAdd = explorerButton.getAddButton();
    
    JButton btnOk = new JButton("SELECT");
    btnOk.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ModifierPageItemSelectionDialog.this.doOk();
      }
      
    });
    JButton btnCancel = new JButton(POSConstants.CANCEL);
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setCanceled(true);
        dispose();
      }
      
    });
    btnAdd.setText(POSConstants.ADD);
    btnEdit.setText(POSConstants.EDIT);
    
    btnEdit.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        ModifierPageItemSelectionDialog.this.editSelectedRow();
      }
      

    });
    btnAdd.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          MenuModifier menuItem = new MenuModifier();
          MenuModifierForm editor = new MenuModifierForm(menuItem);
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          MenuModifier foodItem = (MenuModifier)editor.getBean();
          tableModel.addRow(foodItem);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    btnOk.setBackground(Color.GREEN);
    
    TransparentPanel panel = new TransparentPanel(new MigLayout("center,ins 0 0 5 0", "sg,fill", ""));
    int h = PosUIManager.getSize(40);
    panel.add(btnAdd, "h " + h);
    panel.add(btnEdit, "h " + h);
    panel.add(btnOk, "h " + h);
    panel.add(btnCancel, "h " + h);
    return panel;
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(10));
    columnWidth.add(Integer.valueOf(400));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(70));
    
    return columnWidth;
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      MenuModifier menuModifier = (MenuModifier)tableModel.getRow(index);
      tableModel.setRow(index, menuModifier);
      
      MenuModifierForm editor = new MenuModifierForm(menuModifier);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doOk() {
    int index = table.getSelectedRow();
    if (index < 0) {
      return;
    }
    index = table.convertRowIndexToModel(index);
    selectedMenuModifier = ((MenuModifier)tableModel.getRow(index));
    if ((modifierSpec != null) && 
      (hasSimilarItem())) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Item already exists in " + exstingPageName);
      return;
    }
    
    setCanceled(false);
    dispose();
  }
  
  private boolean hasSimilarItem() {
    Set<MenuItemModifierPage> pages = modifierSpec.getModifierPages();
    if (pages == null)
      return false;
    for (Iterator iterator = pages.iterator(); iterator.hasNext();) {
      page = (MenuItemModifierPage)iterator.next();
      List<MenuItemModifierPageItem> pageItems = page.getPageItems();
      if (pageItems != null)
      {
        for (MenuItemModifierPageItem pageItem : pageItems) {
          MenuModifier modifier = pageItem.getMenuModifier();
          if (modifier != null)
          {
            if (modifier.getId().equals(selectedMenuModifier.getId())) {
              exstingPageName = page.getName();
              return true;
            } }
        } } }
    MenuItemModifierPage page;
    return false;
  }
  
  public MenuModifier getSelectedMenuModifier() {
    return selectedMenuModifier;
  }
  
  public void setSelectedMenuModifier(MenuModifier selectedMenuModifier) {
    this.selectedMenuModifier = selectedMenuModifier;
    if (selectedMenuModifier != null) {
      searchItem();
    }
  }
  
  private void scrollDown() {
    dataModel.setCurrentRowIndex(dataModel.getNextRowIndex());
    setDataModel(dataModel);
  }
  
  private void scrollUp() {
    dataModel.setCurrentRowIndex(dataModel.getPreviousRowIndex());
    setDataModel(dataModel);
  }
  
  private void setDataModel(PaginatedListModel dataModel) {
    tableModel.removeAll();
    modifierList = dataModel.getDataList();
    String txName = tfName.getText();
    if (txName.length() == 0) {
      sorter.setRowFilter(null);
    }
    else {
      tableModel.addRows(modifierList);
      sorter.setRowFilter(RowFilter.regexFilter(txName, new int[0]));
    }
    int nextRowIndex = dataModel.getNextRowIndex();
    tableModel.addRows(modifierList.subList(dataModel.getCurrentRowIndex(), nextRowIndex > dataModel.getSize() ? dataModel.getSize() : nextRowIndex));
    btnNext.setEnabled(dataModel.hasNext());
    btnPrev.setEnabled(dataModel.hasPrevious());
  }
  
  public void setSelectedModifierGroup(ModifierGroup modifierGroup) {
    if ((selectedGroup != null) && (modifierGroup.getId().equals(selectedGroup.getId())))
      return;
    selectedGroup = ModifierGroupDAO.getInstance().get(modifierGroup.getId());
    dataModel.setCurrentRowIndex(0);
    List<MenuModifier> modifiers = modifierGroup.getModifiers();
    if (modifiers == null)
      return;
    dataModel.setNumRows(modifiers.size());
    dataModel.setData(modifiers);
    setDataModel(dataModel);
  }
  
  public static ModifierPageItemSelectionDialog getInstance(ModifierGroup modifierGroup) {
    if (instance == null) {
      instance = new ModifierPageItemSelectionDialog();
    }
    instance.setSelectedModifierGroup(modifierGroup);
    return instance;
  }
  
  public void setModifierSpec(MenuItemModifierSpec menuItemModifierSpec) {
    modifierSpec = menuItemModifierSpec;
  }
}
