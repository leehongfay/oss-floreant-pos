package com.floreantpos.bo.ui.modifierdesigner;

import com.floreantpos.POSConstants;
import com.floreantpos.model.DefaultMenuModifier;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.Multiplier;
import com.floreantpos.model.dao.MultiplierDAO;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;



















public class DefaultModifierSelectionDialog
  extends OkCancelOptionDialog
  implements ActionListener
{
  private List<MenuModifier> modifiers;
  private DefaultModifierTable table;
  private Map<String, DefaultMenuModifier> addedDefaultMenuModifierMap = new HashMap();
  private Multiplier defaultMultiplier;
  
  public DefaultModifierSelectionDialog(List<MenuModifier> modifiers, List<DefaultMenuModifier> defaultModifiers, boolean pizzaModifierGroup) {
    super(POSUtil.getFocusedWindow(), "");
    this.modifiers = modifiers;
    init(defaultModifiers);
  }
  
  private void init(List<DefaultMenuModifier> defaultModifiers) {
    setTitle("Select default modifier");
    setCaption("Select default modifiers");
    
    defaultMultiplier = MultiplierDAO.getInstance().getDefaultMutltiplier();
    table = new DefaultModifierTable(defaultModifiers);
    
    if (defaultModifiers != null) {
      for (DefaultMenuModifier item : defaultModifiers) {
        addedDefaultMenuModifierMap.put(item.getId(), item);
      }
    }
    
    JPanel contentPane = new JPanel(new MigLayout("fill,hidemode 3,inset 0 10 0 10"));
    
    JPanel tablePanel = new JPanel(new BorderLayout());
    tablePanel.setBorder(new EmptyBorder(10, 5, 0, 5));
    JScrollPane scroll = new JScrollPane(table);
    scroll.setPreferredSize(PosUIManager.getSize(500, 250));
    tablePanel.add(scroll);
    
    JButton btnAddModifiers = new JButton("Add Modifiers");
    btnAddModifiers.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        List<MenuModifier> addedModifiers = new ArrayList();
        List<DefaultMenuModifier> addedDefaultModifierList = new ArrayList(addedDefaultMenuModifierMap.values());
        for (DefaultMenuModifier defaultModifier : addedDefaultModifierList) {
          addedModifiers.add(defaultModifier.getModifier());
        }
        ModifierSelectionDialog dialog = new ModifierSelectionDialog(addedModifiers, false);
        dialog.setModifiers(DefaultModifierSelectionDialog.this.modifiers);
        dialog.setSize(600, 500);
        dialog.open();
        
        if (dialog.isCanceled())
          return;
        List<MenuModifier> modifiers = dialog.getSelectedMenuModifierList();
        if (modifiers != null) {
          List<DefaultMenuModifier> defaultModifers = new ArrayList();
          for (Iterator iterator = modifiers.iterator(); iterator.hasNext();) {
            MenuModifier menuModifier = (MenuModifier)iterator.next();
            DefaultMenuModifier defaultModifier = (DefaultMenuModifier)addedDefaultMenuModifierMap.get(menuModifier.getId());
            if (defaultModifier == null) {
              defaultModifier = new DefaultMenuModifier();
              defaultModifier.setModifier(menuModifier);
              defaultModifier.setMultiplier(defaultMultiplier);
              defaultModifier.setQuantity(Double.valueOf(1.0D));
            }
            defaultModifers.add(defaultModifier);
          }
          addedDefaultMenuModifierMap.clear();
          for (DefaultMenuModifier defaultMenuModifier : defaultModifers) {
            addedDefaultMenuModifierMap.put(defaultMenuModifier.getId(), defaultMenuModifier);
          }
          table.setItems(defaultModifers);
        }
        
      }
    });
    JPanel paginationButtonPanel = new JPanel(new MigLayout("fillx,ins 5 0 0 0", "", ""));
    paginationButtonPanel.add(btnAddModifiers, "left");
    tablePanel.add(paginationButtonPanel, "South");
    resizeColumnWidth(table);
    contentPane.add(tablePanel, "grow,span");
    JPanel contentPanel = getContentPanel();
    contentPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 5));
    contentPanel.add(contentPane);
  }
  
  public void doOk()
  {
    List<DefaultMenuModifier> menuModifiers = getSelectedDefaultMenuModifierList();
    if ((menuModifiers == null) || (menuModifiers.isEmpty())) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select modifier");
      return;
    }
    setCanceled(false);
    dispose();
  }
  
  public void doCancel() {
    setCanceled(true);
    dispose();
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    
    if (POSConstants.CANCEL.equalsIgnoreCase(actionCommand)) {
      doCancel();
    }
    else if (POSConstants.OK.equalsIgnoreCase(actionCommand)) {
      doOk();
    }
  }
  
  public List<DefaultMenuModifier> getSelectedDefaultModifierList() {
    return getSelectedDefaultMenuModifierList();
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(250));
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(50));
    
    return columnWidth;
  }
  
  public List<DefaultMenuModifier> getSelectedDefaultMenuModifierList() {
    return new ArrayList(addedDefaultMenuModifierMap.values());
  }
  
  public void repaintTable() {
    table.repaint();
  }
}
