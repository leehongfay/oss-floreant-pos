package com.floreantpos.bo.ui.modifierdesigner;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.explorer.PageSelectionListener;
import com.floreantpos.model.DefaultMenuModifier;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuItemModifierPage;
import com.floreantpos.model.MenuItemModifierPageItem;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.ModifierGroup;
import com.floreantpos.swing.PaginatedListModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;




















public class ModifierPageDesigner
  extends TransparentPanel
  implements PageSelectionListener
{
  private ModifierPageDesignView itemView;
  private MenuItemModifierSpec menuItemModifierSpec;
  private ModifierGroup selectedGroup;
  private MenuItemModifierPage selectedModifierSpecPage;
  private JButton btnEditPage;
  private MenuItem menuItem;
  private PosButton btnNext;
  private PosButton btnPrev;
  private PaginatedListModel dataModel = new PaginatedListModel(1);
  private PosButton btnPageNum;
  
  public ModifierPageDesigner(MenuItem menuItem) {
    this.menuItem = menuItem;
    initComponents();
  }
  
  public void setMenuItemModifierSpec(MenuItemModifierSpec menuItemModifierSpec) {
    this.menuItemModifierSpec = menuItemModifierSpec;
    if (menuItemModifierSpec.getModifierPages() == null)
      menuItemModifierSpec.setModifierPages(new HashSet());
    dataModel.setCurrentRowIndex(0);
    initDataModel();
    setDataModel(dataModel);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 0));
    JPanel contentPanel = new JPanel(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
    itemView = new ModifierPageDesignView();
    itemView.setPageListener(this);
    itemView.setPizzaItem(menuItem.isPizzaType().booleanValue());
    
    JPanel southItemActionPanel = new JPanel(new MigLayout("center,fillx", "[33%][33%][33%]", ""));
    
    JButton btnDefaultModifiers = new JButton("Default Modifiers");
    btnDefaultModifiers.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (menuItemModifierSpec == null) {
          return;
        }
        List<MenuModifier> items = new ArrayList(menuItemModifierSpec.getModifiers());
        if ((items == null) || (items.isEmpty())) {
          return;
        }
        List<DefaultMenuModifier> defaultModifierLIst = menuItemModifierSpec.getDefaultModifierList();
        DefaultModifierSelectionDialog dialog = new DefaultModifierSelectionDialog(items, defaultModifierLIst, menuItem.isPizzaType().booleanValue());
        dialog.setSize(600, 500);
        dialog.open();
        
        if (dialog.isCanceled()) {
          return;
        }
        menuItemModifierSpec.setDefaultModifierList(dialog.getSelectedDefaultModifierList());
      }
      
    });
    JButton btnDetachedAll = new JButton("Detach All");
    btnDetachedAll.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ModifierPageDesigner.this.doDetachAllItems();
      }
    });
    JButton btnAutoBuild = new JButton("Auto Build");
    btnAutoBuild.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doGenenateMenuItemModifierPageItems();
      }
    });
    btnEditPage = new JButton("Edit page");
    btnEditPage.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        editPage();
      }
      
    });
    southItemActionPanel.add(new JLabel(""), "grow");
    
    southItemActionPanel.add(btnDefaultModifiers, "split 5");
    southItemActionPanel.add(btnEditPage);
    southItemActionPanel.add(btnAutoBuild);
    southItemActionPanel.add(btnDetachedAll);
    
    JPanel paginationButtonPanel = new JPanel(new MigLayout("ins 0,hidemode 3", "", ""));
    
    Dimension btnSize = new Dimension(20, 20);
    
    btnPrev = new PosButton();
    btnPrev.setIcon(IconFactory.getIcon("/ui_icons/", "previous.png", btnSize));
    paginationButtonPanel.add(btnPrev, "split 3,w 40!, h 35!");
    
    btnPageNum = new PosButton();
    btnPageNum.setBorder(null);
    btnPageNum.setOpaque(false);
    btnPageNum.setContentAreaFilled(false);
    btnPageNum.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        editPage();
      }
    });
    paginationButtonPanel.add(btnPageNum, "h 35!");
    btnPageNum.setVisible(false);
    
    btnNext = new PosButton();
    btnNext.setIcon(IconFactory.getIcon("/ui_icons/", "next.png", btnSize));
    paginationButtonPanel.add(btnNext, "w 40!,h 35!");
    
    southItemActionPanel.add(paginationButtonPanel, "right");
    add(southItemActionPanel, "South");
    
    ActionListener action = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          Object source = e.getSource();
          if (source == btnPrev) {
            ModifierPageDesigner.this.scrollUp();
          }
          else if (source == btnNext) {
            ModifierPageDesigner.this.scrollDown();
          }
        } catch (Exception e2) {
          POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e2.getMessage(), e2);
        }
        
      }
    };
    btnPrev.addActionListener(action);
    btnNext.addActionListener(action);
    
    btnNext.setEnabled(false);
    btnPrev.setEnabled(false);
    
    TitledBorder border = new TitledBorder(POSConstants.MODIFIERS);
    border.setTitleJustification(2);
    itemView.setBorder(new CompoundBorder(border, null));
    
    contentPanel.add(itemView);
    add(contentPanel);
  }
  
  private void rendererSelectedGroup() {
    if (selectedGroup != null) {
      MenuItemModifierSpec modifierSpec = selectedModifierSpecPage.getModifierSpec();
      if (modifierSpec == null) {
        modifierSpec = new MenuItemModifierSpec();
        modifierSpec.setModifierGroup(selectedGroup);
        selectedModifierSpecPage.setModifierSpec(modifierSpec);
      }
      itemView.setMenuItemModifierPage(selectedModifierSpecPage);
    }
  }
  
  private void doDetachAllItems() {
    if ((selectedModifierSpecPage == null) || (selectedModifierSpecPage.getPageItems() == null)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "No item to detach.");
      return;
    }
    if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Are you sure you want to detach all?", "Confirm") != 0) {
      return;
    }
    if (selectedModifierSpecPage.getPageItems() != null)
      selectedModifierSpecPage.getPageItems().clear();
    itemView.setMenuItemModifierPage(selectedModifierSpecPage);
  }
  
  public void doGenenateMenuItemModifierPageItems() {
    if (selectedGroup == null) {
      return;
    }
    List<MenuModifier> items = selectedGroup.getModifiers();
    if ((items == null) || (items.isEmpty())) {
      return;
    }
    try {
      if (selectedModifierSpecPage == null) {
        doCreateNewPage();
      }
      int pageItemCount = selectedModifierSpecPage.getRows().intValue() * selectedModifierSpecPage.getCols().intValue();
      int itemSize = items.size();
      List<MenuItemModifierPageItem> pageItems = new ArrayList();
      if (items.size() > 0) {
        int count = 1;
        for (MenuModifier modifier : items) {
          MenuItemModifierPageItem pageItem = new MenuItemModifierPageItem();
          pageItem.setMenuModifier(modifier);
          pageItem.setParentPage(selectedModifierSpecPage);
          pageItems.add(pageItem);
          if ((count == pageItemCount) || (itemSize == count)) {
            itemView.fillSelectedPageItems(pageItems);
            if (itemSize == count) {
              break;
            }
            doCreateNewPage();
            pageItems = new ArrayList();
            itemSize -= count;
            count = 1;
            setDataModel(dataModel);
            dataModel.setCurrentRowIndex(dataModel.getNextRowIndex());
          }
          else {
            count++;
          }
        }
        setDataModel(dataModel);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      BOMessageDialog.showError(ex.getMessage());
    }
  }
  
  private void doCreateNewPage() {
    selectedModifierSpecPage = new MenuItemModifierPage();
    selectedModifierSpecPage.setName("Page " + (menuItemModifierSpec.getModifierPages().size() + 1));
    selectedModifierSpecPage.setButtonHeight(Integer.valueOf(100));
    selectedModifierSpecPage.setButtonWidth(Integer.valueOf(100));
    selectedModifierSpecPage.setRows(Integer.valueOf(4));
    selectedModifierSpecPage.setCols(Integer.valueOf(4));
    selectedModifierSpecPage.setVisible(Boolean.valueOf(true));
    selectedModifierSpecPage.setSortOrder(Integer.valueOf(menuItemModifierSpec.getModifierPages().size() + 1));
    selectedModifierSpecPage.setFlixibleButtonSize(Boolean.valueOf(false));
    selectedModifierSpecPage.setModifierSpecId(menuItemModifierSpec.getId());
    menuItemModifierSpec.addTomodifierPages(selectedModifierSpecPage);
    dataModel.getDataList().add(selectedModifierSpecPage);
    dataModel.setNumRows(menuItemModifierSpec.getModifierPages().size());
  }
  
  private void setDataModel(PaginatedListModel dataModel) {
    rendererSelectedGroup();
    btnNext.setEnabled(true);
    btnPrev.setEnabled(dataModel.hasPrevious());
    btnPageNum.setText(selectedModifierSpecPage.getName());
    btnPageNum.setVisible(StringUtils.isNotEmpty(selectedModifierSpecPage.getName()));
  }
  
  private void initDataModel() {
    selectedGroup = menuItemModifierSpec.getModifierGroup();
    
    Set<MenuItemModifierPage> modifierPages = menuItemModifierSpec.getModifierPages();
    if (modifierPages == null) {
      modifierPages = new HashSet();
    }
    List<MenuItemModifierPage> pages = new ArrayList();
    for (MenuItemModifierPage page : modifierPages) {
      pages.add(page);
    }
    Collections.sort(pages, new Comparator()
    {
      public int compare(MenuItemModifierPage o1, MenuItemModifierPage o2)
      {
        return o1.getSortOrder().intValue() - o2.getSortOrder().intValue();
      }
    });
    dataModel.setNumRows(pages.size());
    dataModel.setData(pages);
    if (dataModel.getSize() > 0) {
      selectedModifierSpecPage = ((MenuItemModifierPage)dataModel.getElementAt(dataModel.getCurrentRowIndex()));
    } else
      doCreateNewPage();
  }
  
  private void scrollDown() {
    if (dataModel.getNextRowIndex() >= dataModel.getSize()) {
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Do you want to create new page?", "Confirm") != 0) {
        return;
      }
      doCreateNewPage();
      itemSelected(selectedModifierSpecPage);
      dataModel.setCurrentRowIndex(dataModel.getNextRowIndex());
      setDataModel(dataModel);
    }
    else {
      dataModel.setCurrentRowIndex(dataModel.getNextRowIndex());
      selectedModifierSpecPage = ((MenuItemModifierPage)dataModel.getElementAt(dataModel.getCurrentRowIndex()));
      setDataModel(dataModel);
    }
  }
  
  private void scrollUp() {
    dataModel.setCurrentRowIndex(dataModel.getPreviousRowIndex());
    selectedModifierSpecPage = ((MenuItemModifierPage)dataModel.getElementAt(dataModel.getCurrentRowIndex()));
    setDataModel(dataModel);
  }
  
  public MenuItemModifierSpec getMenuItemModifierSpec() {
    return menuItemModifierSpec;
  }
  
  public void reset() {
    itemView.reset();
    itemView.revalidate();
    itemView.repaint();
  }
  
  public void editPage() {
    if (selectedModifierSpecPage == null) {
      POSMessageDialog.showMessage(this, "Please select modifier group first.");
      return;
    }
    ModifierPageForm form = new ModifierPageForm(selectedModifierSpecPage);
    BeanEditorDialog dialog = new BeanEditorDialog(form);
    dialog.open();
    
    if (dialog.isCanceled()) {
      return;
    }
    rendererSelectedGroup();
  }
  
  public void itemSelected(Object item) {}
}
