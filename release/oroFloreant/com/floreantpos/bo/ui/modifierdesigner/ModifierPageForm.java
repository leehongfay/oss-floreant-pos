package com.floreantpos.bo.ui.modifierdesigner;

import com.floreantpos.POSConstants;
import com.floreantpos.model.MenuItemModifierPage;
import com.floreantpos.model.dao.MenuItemModifierPageDAO;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;






















public class ModifierPageForm
  extends BeanEditor
{
  private FixedLengthTextField tfName;
  private IntegerTextField tfSortOrder;
  private IntegerTextField tfNumberOfColumns;
  private IntegerTextField tfNumberOfRows;
  private IntegerTextField tfButtonWidth;
  private IntegerTextField tfButtonHeight;
  private JCheckBox cbFlexibleButtonSize;
  
  public ModifierPageForm()
  {
    this(new MenuItemModifierPage());
  }
  
  public ModifierPageForm(MenuItemModifierPage menuItemModifierPage) {
    initComponents();
    setBean(menuItemModifierPage);
  }
  
  private void initComponents()
  {
    JPanel contentPanel = new JPanel(new MigLayout());
    
    tfName = new FixedLengthTextField();
    tfSortOrder = new IntegerTextField(10);
    
    tfNumberOfColumns = new IntegerTextField(10);
    tfNumberOfRows = new IntegerTextField(10);
    tfButtonWidth = new IntegerTextField(6);
    tfButtonHeight = new IntegerTextField(6);
    
    tfNumberOfColumns.setText("4");
    tfNumberOfRows.setText("4");
    
    contentPanel.add(new JLabel(POSConstants.NAME));
    contentPanel.add(tfName, "wrap");
    
    contentPanel.add(new JLabel(POSConstants.SORT_ORDER));
    contentPanel.add(tfSortOrder, "grow,wrap");
    
    contentPanel.add(new JLabel("Number of columns"));
    contentPanel.add(tfNumberOfColumns, "grow,wrap");
    
    contentPanel.add(new JLabel("Number of rows"));
    contentPanel.add(tfNumberOfRows, "grow");
    
    contentPanel.add(new JLabel("Button width"), "newline");
    contentPanel.add(tfButtonWidth, "grow");
    
    contentPanel.add(new JLabel("Button height"), "newline");
    contentPanel.add(tfButtonHeight, "grow");
    
    cbFlexibleButtonSize = new JCheckBox("Flexible button size");
    contentPanel.add(cbFlexibleButtonSize, "skip 1,newline,grow");
    
    add(contentPanel);
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      MenuItemModifierPage menuItemModifierPage = (MenuItemModifierPage)getBean();
      MenuItemModifierPageDAO dao = new MenuItemModifierPageDAO();
      dao.saveOrUpdate(menuItemModifierPage);
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage(), e); }
    return false;
  }
  

  protected void updateView()
  {
    MenuItemModifierPage menuItemModifierPage = (MenuItemModifierPage)getBean();
    if (menuItemModifierPage.getId() == null)
      return;
    tfName.setText(menuItemModifierPage.getName());
    tfSortOrder.setText(String.valueOf(menuItemModifierPage.getSortOrder()));
    tfNumberOfColumns.setText(String.valueOf(menuItemModifierPage.getCols()));
    tfNumberOfRows.setText(String.valueOf(menuItemModifierPage.getRows()));
    cbFlexibleButtonSize.setSelected(menuItemModifierPage.isFlixibleButtonSize().booleanValue());
    tfButtonWidth.setText(String.valueOf(menuItemModifierPage.getButtonWidth()));
    tfButtonHeight.setText(String.valueOf(menuItemModifierPage.getButtonHeight()));
  }
  
  protected boolean updateModel()
  {
    MenuItemModifierPage menuItemModifierPage = (MenuItemModifierPage)getBean();
    
    String name = tfName.getText();
    if (POSUtil.isBlankOrNull(name)) {
      POSMessageDialog.showError(this, POSConstants.NAME_REQUIRED);
      return false;
    }
    int buttonWidth = tfButtonWidth.getInteger();
    if (buttonWidth < 30) {
      POSMessageDialog.showError(this, "Button width must be greater than or equal to 30");
      return false;
    }
    int buttonHeight = tfButtonHeight.getInteger();
    if (buttonHeight < 30) {
      POSMessageDialog.showError(this, "Button height must be greater than or equal to 30");
      return false;
    }
    
    menuItemModifierPage.setName(name);
    menuItemModifierPage.setSortOrder(Integer.valueOf(tfSortOrder.getInteger()));
    menuItemModifierPage.setCols(Integer.valueOf(tfNumberOfColumns.getInteger()));
    menuItemModifierPage.setRows(Integer.valueOf(tfNumberOfRows.getInteger()));
    menuItemModifierPage.setFlixibleButtonSize(Boolean.valueOf(cbFlexibleButtonSize.isSelected()));
    menuItemModifierPage.setButtonWidth(Integer.valueOf(buttonWidth));
    menuItemModifierPage.setButtonHeight(Integer.valueOf(buttonHeight));
    
    return true;
  }
  
  public String getDisplayText() {
    MenuItemModifierPage menuItemModifierPage = (MenuItemModifierPage)getBean();
    if (menuItemModifierPage.getId() == null) {
      return "New modifier page";
    }
    return "Edit modifier page";
  }
}
