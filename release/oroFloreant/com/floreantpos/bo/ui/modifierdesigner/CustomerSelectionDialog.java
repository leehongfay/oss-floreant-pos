package com.floreantpos.bo.ui.modifierdesigner;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.Customer;
import com.floreantpos.model.CustomerGroup;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.forms.CustomerForm;
import com.floreantpos.ui.model.MultipleCustomerSelectionView;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
















public class CustomerSelectionDialog
  extends POSDialog
  implements ActionListener
{
  private MultipleCustomerSelectionView customerSelectorPanel;
  private List<Customer> customerList;
  private boolean singleSelectionEnable;
  private TitlePanel titelpanel;
  private static CustomerSelectionDialog instance;
  
  public CustomerSelectionDialog(JDialog parent, List<Customer> customerList)
  {
    super(parent, "", true);
    this.customerList = customerList;
    init();
  }
  
  public CustomerSelectionDialog(List<Customer> customerList) {
    super(POSUtil.getFocusedWindow(), "");
    this.customerList = customerList;
    init();
  }
  
  private void init() {
    setLayout(new BorderLayout(5, 5));
    setTitle(Messages.getString("CustomerSelectionDialog.1"));
    titelpanel = new TitlePanel();
    titelpanel.setTitle(Messages.getString("CustomerSelectionDialog.4"));
    
    add(titelpanel, "North");
    
    JPanel contentPane = new JPanel(new MigLayout("fill,hidemode 3,inset 0 10 0 10"));
    customerSelectorPanel = new MultipleCustomerSelectionView(customerList);
    contentPane.add(customerSelectorPanel, "grow,span,wrap");
    
    PosButton btnOk = new PosButton("SELECT");
    btnOk.setActionCommand(POSConstants.OK);
    btnOk.setBackground(Color.GREEN);
    
    btnOk.setFocusable(false);
    btnOk.addActionListener(this);
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL);
    btnCancel.setFocusable(false);
    btnCancel.addActionListener(this);
    
    JPanel footerPanel = new JPanel(new MigLayout("center,ins 0 5 5 5", "", ""));
    
    PosButton btnEdit = new PosButton();
    PosButton btnAdd = new PosButton();
    
    btnAdd.setText(POSConstants.ADD.toUpperCase());
    btnEdit.setText(POSConstants.EDIT.toUpperCase());
    
    btnEdit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        CustomerSelectionDialog.this.editSelectedRow();
      }
      

    });
    btnAdd.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          boolean setKeyPad = true;
          CustomerForm editor = new CustomerForm(setKeyPad);
          editor.enableCustomerFields(true);
          
          BeanEditorDialog dialog = new BeanEditorDialog(editor);
          dialog.open();
          if (dialog.isCanceled())
            return;
          Customer customer = (Customer)editor.getBean();
          customerSelectorPanel.getModel().addRow(customer);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    footerPanel.add(btnAdd);
    footerPanel.add(btnEdit);
    footerPanel.add(btnOk);
    footerPanel.add(btnCancel);
    
    add(footerPanel, "South");
    
    add(contentPane);
  }
  
  public void setSingleSelectionEnable(boolean enable) {
    singleSelectionEnable = enable;
    if (enable) {
      titelpanel.setTitle(Messages.getString("CustomerSelectionDialog.9"));
    }
    customerSelectorPanel.setSingleSelectionEnable(enable);
  }
  
  public void setSelectedGroup(CustomerGroup customerGroup) {
    customerSelectorPanel.setSelectedGroup(customerGroup);
  }
  
  private void doOk() {
    if (singleSelectionEnable) {
      Customer customer = getSelectedRowData();
      if (customer == null) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("CustomerSelectionDialog.10"));
        return;
      }
    }
    else {
      List<Customer> customerList = customerSelectorPanel.getSelectedCustomerList();
      if ((customerList == null) || (customerList.isEmpty())) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("CustomerSelectionDialog.11"));
        return;
      }
    }
    setCanceled(false);
    dispose();
  }
  
  private void doCancel() {
    setCanceled(true);
    dispose();
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    
    if (POSConstants.CANCEL.equalsIgnoreCase(actionCommand)) {
      doCancel();
    }
    else if (POSConstants.OK.equalsIgnoreCase(actionCommand)) {
      doOk();
    }
  }
  
  private void editSelectedRow() {
    try {
      int index = customerSelectorPanel.getSelectedRow();
      if (index < 0) {
        return;
      }
      Customer customer = (Customer)customerSelectorPanel.getModel().getRow(index);
      
      CustomerForm editor = new CustomerForm();
      editor.enableCustomerFields(true);
      
      editor.setBean(customer);
      BeanEditorDialog dialog = new BeanEditorDialog(editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      customerSelectorPanel.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public List<Customer> getSelectedCustomerList() {
    return customerSelectorPanel.getSelectedCustomerList();
  }
  
  public Customer getSelectedRowData() {
    int index = customerSelectorPanel.getSelectedRow();
    if (index < 0)
      return null;
    return (Customer)customerSelectorPanel.getModel().getRow(index);
  }
  
  public static CustomerSelectionDialog getInstance(List<Customer> customerList, boolean singleSelection) {
    if (instance == null) {
      instance = new CustomerSelectionDialog(customerList);
      instance.setSingleSelectionEnable(singleSelection);
    }
    return instance;
  }
}
