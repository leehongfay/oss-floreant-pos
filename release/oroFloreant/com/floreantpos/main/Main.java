package com.floreantpos.main;

import com.floreantpos.config.TerminalConfig;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Locale;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;




















public class Main
{
  private static final String DEVELOPMENT_MODE = "developmentMode";
  
  public Main() {}
  
  public static void main(String[] args)
    throws Exception
  {
    Options options = new Options();
    options.addOption("developmentMode", true, "State if this is developmentMode");
    CommandLineParser parser = new BasicParser();
    CommandLine commandLine = parser.parse(options, args);
    String optionValue = commandLine.getOptionValue("developmentMode");
    Locale defaultLocale = TerminalConfig.getDefaultLocale();
    if (defaultLocale != null) {}
    


    Application application = Application.getInstance();
    
    if (optionValue != null) {
      application.setDevelopmentMode(Boolean.valueOf(optionValue).booleanValue());
    }
    
    application.start();
  }
  
  public static void restart() throws IOException, InterruptedException, URISyntaxException {
    String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
    
    String classPath = System.getProperty("java.class.path");
    String mainClass = System.getProperty("sun.java.command");
    

    ArrayList<String> command = new ArrayList();
    command.add(javaBin);
    command.add("-cp");
    command.add(classPath);
    command.add(mainClass);
    
    ProcessBuilder builder = new ProcessBuilder(command);
    builder.start();
    System.exit(0);
  }
}
