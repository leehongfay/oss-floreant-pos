package com.floreantpos.main;

import com.floreantpos.Database;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.config.AppConfig;
import com.floreantpos.config.AppProperties;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.config.ui.DatabaseConfigurationDialog;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.FloreantPlugin;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Outlet;
import com.floreantpos.model.PosPrinters;
import com.floreantpos.model.Store;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.model.dao._RootDAO;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.OroLicenceActivationDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.ClockInOutDialog;
import com.floreantpos.ui.views.LoginView;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.update.UpdateListener;
import com.floreantpos.update.UpdateService;
import com.floreantpos.util.DatabaseConnectionException;
import com.floreantpos.util.DatabaseUtil;
import com.floreantpos.util.POSUtil;
import com.floreantpos.versioning.VersionInfo;
import com.jgoodies.looks.plastic.PlasticXPLookAndFeel;
import com.jgoodies.looks.plastic.theme.ExperienceBlue;
import com.orocube.common.util.TerminalUtil;
import com.orocube.licensemanager.InvalidLicenseException;
import com.orocube.licensemanager.LicenseUtil;
import com.orocube.licensemanager.OroLicense;
import java.awt.Container;
import java.awt.Font;
import java.io.File;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLDecoder;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;
import jiconfont.icons.FontAwesome;
import jiconfont.swing.IconFontSwing;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;













public class Application
  implements UpdateListener
{
  private static Log logger = LogFactory.getLog(Application.class);
  
  private boolean developmentMode = false;
  
  private PosWindow posWindow;
  
  private User currentUser;
  
  private RootView rootView;
  private static Application instance;
  private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
  
  private static ImageIcon applicationIcon;
  private boolean systemInitialized;
  private boolean viewInitialized;
  private boolean headLess = false;
  
  private static final String PRODUCT_NAME = "OROPOS";
  
  private static final String PRODUCT_VERSION = "1.4";
  private static final String HAS_UPDATE = "has_update";
  private OroLicense license;
  
  protected Application() {}
  
  public void start()
  {
    System.setProperty("data.provider.class", "com.floreantpos.model.dao.util.PosCacheManager");
    
    checkHasRemainingUpdates();
    
    UIManager.put("OptionPane.yesButtonText", Messages.getString("Application.10"));
    UIManager.put("OptionPane.noButtonText", Messages.getString("Application.12"));
    UIManager.put("OptionPane.cancelButtonText", Messages.getString("Application.14"));
    UIManager.put("OptionPane.titleText", Messages.getString("Application.16"));
    UIManager.put("OptionPane.okButtonText", Messages.getString("Application.18"));
    
    setApplicationLook();
    
    applicationIcon = new ImageIcon(getClass().getResource("/icons/icon.png"));
    posWindow = new PosWindow();
    posWindow.setTitle(getTitle());
    posWindow.setIconImage(applicationIcon.getImage());
    posWindow.setupSizeAndLocation();
    if (TerminalConfig.isKioskMode()) {
      posWindow.enterFullScreenMode();
    }
    posWindow.setVisible(true);
    
    rootView = RootView.getInstance();
    rootView.initDefaultViews();
    posWindow.getContentPane().add(rootView);
    
    initLicense();
    initializeSystem();
    
    if (TerminalConfig.isCheckUpdateOnStartUp())
      checkForUpdate();
  }
  
  public void checkForUpdate() {
    UpdateService.checkForUpdate(posWindow, this, "http://oroposcloud.com/update/oropos", VersionInfo.getAppName(), VersionInfo.getVersion(), 
      VersionInfo.getNumericVersion());
  }
  
  private void checkHasRemainingUpdates() {
    if (!AppConfig.getBoolean("has_update", false)) {
      return;
    }
    runUpdateProcess();
    AppConfig.put("has_update", false);
    System.exit(0);
  }
  
  public void runUpdateProcess() {
    try {
      File currentJar = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI());
      String decodedParentPath = URLDecoder.decode(currentJar.getPath(), "UTF-8");
      String decodedUpdaterPath = decodedParentPath.replace(currentJar.getName(), "auto-updater.jar");
      System.out.println(decodedParentPath);
      System.out.println(decodedUpdaterPath);
      ProcessBuilder pb = new ProcessBuilder(new String[] { "java", "-cp", decodedUpdaterPath, "com.floreantpos.update.UpdateServiceWindow", decodedParentPath });
      pb.start();
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    }
  }
  
  public void initLicense() {
    try {
      license = LicenseUtil.loadAndValidate(getProductName(), getProductVersion(), TerminalUtil.getSystemUID());
    } catch (InvalidLicenseException e) {
      LogFactory.getLog(getClass()).error(e);
      
      license = OroLicenceActivationDialog.showDialog(getProductName(), getProductVersion());
    } catch (Exception e) {
      LogFactory.getLog(getClass()).error(e);
    }
  }
  
  public void startInvisible() {
    setApplicationLook();
    
    applicationIcon = new ImageIcon(getClass().getResource("/icons/icon_updated.png"));
    posWindow = new PosWindow();
    posWindow.setTitle(getTitle());
    posWindow.setIconImage(applicationIcon.getImage());
    
    rootView = RootView.getInstance();
    
    posWindow.getContentPane().add(rootView);
    
    initializeSystem();
  }
  



  private void setApplicationLook()
  {
    try
    {
      PlasticXPLookAndFeel.setPlasticTheme(new ExperienceBlue());
      UIManager.setLookAndFeel(new PlasticXPLookAndFeel());
      initializeFont();
      
      IconFontSwing.register(FontAwesome.getIconFont());
    }
    catch (Exception localException) {}
  }
  
  public void initializeSystem() {
    if (isSystemInitialized()) {
      return;
    }
    try
    {
      posWindow.setGlassPaneVisible(true);
      
      DatabaseUtil.initialize();
      DatabaseUtil.updateLegacyDatabase();
      
      performDatabaseUpgrade();
      
      DataProvider.get().initialize();
      
      if (!viewInitialized) {
        initPlugins();
        viewInitialized = true;
      }
      
      LoginView.getInstance().initializeOrderButtonPanel();
      
      getPosWindow().updateView();
      setSystemInitialized(true);
    }
    catch (DatabaseConnectionException|HibernateException e) {
      PosLog.error(getClass(), e);
      
      int option = JOptionPane.showConfirmDialog(getPosWindow(), Messages.getString("Application.0"), Messages.getString("PosMessage.Error"), 0);
      
      if (option == 0) {
        DatabaseConfigurationDialog.show(getPosWindow());
      }
    } catch (Exception e) {
      POSMessageDialog.showError(getPosWindow(), e.getMessage(), e);
      logger.error(e);
    } finally {
      getPosWindow().setGlassPaneVisible(false);
    }
  }
  
  private void performDatabaseUpgrade() {
    if (!DatabaseUtil.isDbUpdateNeeded()) {
      return;
    }
    String message = Messages.getString("DB_UPGRADE_REQUIRED");
    String title = Messages.getString("ALERT");
    int option = POSMessageDialog.showYesNoQuestionDialog(posWindow, message, title);
    if (option != 0) {
      return;
    }
    
    message = Messages.getString("DB_UPGRADE_ALERT_MESSAGE");
    option = POSMessageDialog.showYesNoQuestionDialog(posWindow, message, title);
    if (option != 0) {
      POSMessageDialog.showMessage(posWindow, Messages.getString("SHUTDOWN_MESSAGE"));
      System.exit(1);
    }
    
    Database database = AppConfig.getDefaultDatabase();
    String connectString = AppConfig.getConnectString();
    String databaseUser = AppConfig.getDatabaseUser();
    String databasePassword = AppConfig.getDatabasePassword();
    
    boolean databaseUpdated = DatabaseUtil.updateDatabase(connectString, database.getHibernateDialect(), database.getHibernateConnectionDriverClass(), databaseUser, databasePassword);
    
    if (databaseUpdated) {
      JOptionPane.showMessageDialog(posWindow, Messages.getString("DB_UPGRADE_SUCCESS"));
      System.exit(100);
    }
    else {
      JOptionPane.showMessageDialog(posWindow, Messages.getString("DatabaseConfigurationDialog.3"));
    }
  }
  
  public void initializeHeadless() {
    headLess = true;
    
    _RootDAO.initialize();
    DataProvider.get().initialize();
    
    initStore();
    refreshStore();
    
    setSystemInitialized(true);
  }
  
  private void initPlugins() {
    List<FloreantPlugin> plugins = ExtensionManager.getPlugins();
    for (FloreantPlugin floreantPlugin : plugins) {
      floreantPlugin.initUI(getPosWindow());
    }
  }
  
  private void initStore() {
    try {
      Store store = DataProvider.get().getStore();
      if (!headLess) {
        if (store.isItemPriceIncludesTax().booleanValue()) {
          posWindow.setStatus(Messages.getString("Application.41"));
        }
        else {
          posWindow.setStatus(Messages.getString("Application.42"));
        }
        posWindow.setRestaurantName(store.getName());
      }
      

    }
    catch (Exception e)
    {

      throw new DatabaseConnectionException(e);
    }
  }
  
  public void refreshStore() {
    DataProvider.get().refreshStore();
  }
  
  public List<OrderType> getOrderTypes() {
    return DataProvider.get().getOrderTypes();
  }
  
  public static synchronized Application getInstance() {
    if (instance == null) {
      instance = new Application();
    }
    
    return instance;
  }
  
  public static void setInstance(Application application) {
    instance = application;
  }
  
  public synchronized void doLogin(User user) {
    initializeSystem();
    
    if (user == null) {
      return;
    }
    boolean userClockIn = initCurrentUser(user);
    
    RootView rootView = getRootView();
    
    if (!rootView.hasView("ORDER_VIEW")) {
      rootView.addView(OrderView.getInstance());
    }
    
    if (userClockIn)
      rootView.showDefaultView();
    posWindow.rendererUserInfo();
    posWindow.startAutoLogoffTimer();
  }
  
  public boolean initCurrentUser(User user) {
    if (!user.isClockedIn().booleanValue()) {
      ClockInOutDialog dialog = ClockInOutDialog.getInstance(user, true);
      dialog.openUndecoratedFullScreen();
      
      if (dialog.isCanceled()) {
        return false;
      }
    }
    setCurrentUser(user);
    return true;
  }
  
  public void doLogout() {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    if ((window != null) && (window.isVisible())) {
      window.dispose();
    }
    
    setCurrentUser(null);
    RootView.getInstance().showView(LoginView.getInstance());
    posWindow.rendererUserInfo();
    posWindow.stopAutoLogoffTimer();
  }
  
  public void doAutoLogout() {
    doLogout();
  }
  
  public void doAutoLogin(User user)
  {
    setCurrentUser(user);
  }
  
  public static User getCurrentUser() {
    return getInstancecurrentUser;
  }
  
  public void setCurrentUser(User currentUser) {
    this.currentUser = currentUser;
  }
  
  public RootView getRootView() {
    return rootView;
  }
  
  public void setRootView(RootView rootView) {
    this.rootView = rootView;
  }
  
  public static PosWindow getPosWindow() {
    return getInstanceposWindow;
  }
  
  public Terminal getTerminal() {
    return DataProvider.get().getCurrentTerminal();
  }
  
  public synchronized Terminal refreshAndGetTerminal() {
    DataProvider.get().refreshCurrentTerminal();
    return DataProvider.get().getCurrentTerminal();
  }
  
  public static PosPrinters getPrinters() {
    return DataProvider.get().getPrinters();
  }
  
  public OrderType getCurrentOrderType() {
    return (OrderType)DataProvider.get().getOrderTypes().get(0);
  }
  
  public static String getTitle() {
    return AppProperties.getAppName() + " " + AppProperties.getAppVersion();
  }
  
  public static ImageIcon getApplicationIcon() {
    return applicationIcon;
  }
  
  public static void setApplicationIcon(ImageIcon applicationIcon) {
    applicationIcon = applicationIcon;
  }
  
  public static String formatDate(Date date) {
    return dateFormat.format(date);
  }
  
  public boolean isSystemInitialized() {
    return systemInitialized;
  }
  
  public void setSystemInitialized(boolean systemInitialized) {
    this.systemInitialized = systemInitialized;
  }
  
  public Store getStore() {
    return DataProvider.get().getStore();
  }
  
  public static Outlet getOutlet() {
    return DataProvider.get().getOutlet();
  }
  
  public static File getWorkingDir() {
    File file = new File(Application.class.getProtectionDomain().getCodeSource().getLocation().getPath());
    
    return file.getParentFile();
  }
  
  public boolean isDevelopmentMode() {
    return developmentMode;
  }
  
  public void setDevelopmentMode(boolean developmentMode) {
    this.developmentMode = developmentMode;
  }
  
  public boolean isPriceIncludesTax() {
    Store store = getStore();
    if (store == null) {
      return false;
    }
    
    return POSUtil.getBoolean(store.isItemPriceIncludesTax());
  }
  
  public String getLocation() {
    File file = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getFile());
    return file.getParent();
  }
  
  private void initializeFont() {
    Enumeration keys = UIManager.getDefaults().keys();
    while (keys.hasMoreElements())
    {
      Object key = keys.nextElement();
      Object value = UIManager.get(key);
      
      if ((value != null) && ((value instanceof FontUIResource))) {
        FontUIResource f = (FontUIResource)value;
        String fontName = f.getFontName();
        fontName = "Sans";
        
        Font font = new Font(fontName, f.getStyle(), PosUIManager.getDefaultFontSize());
        UIManager.put(key, new FontUIResource(font));
      }
    }
    IconFontSwing.register(FontAwesome.getIconFont());
  }
  
  public void shutdownPOS() {
    int option = POSMessageDialog.showYesNoQuestionDialog(posWindow, POSConstants.CONFIRM_SHUTDOWN_MESSAGE, POSConstants.CONFIRM);
    if (option == 0) {
      posWindow.saveSizeAndLocation();
      _RootDAO.releaseConnection();
      System.exit(0);
    }
  }
  
  public OroLicense getLicense() {
    return license;
  }
  
  public String getProductName() {
    return "OROPOS";
  }
  
  public String getProductVersion() {
    return "1.4";
  }
  
  public void refreshCurrentUser() {
    if (currentUser != null) {
      currentUser = UserDAO.getInstance().get(currentUser.getId());
    }
  }
  
  public void downloadComplete()
  {
    try {
      AppConfig.put("has_update", true);
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please restart your system to take effect.");
      Main.restart();
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    }
  }
  

  public void updateComplete() {}
  
  public String getHibernateConfigurationFileName()
  {
    return "oropos.hibernate.cfg.xml";
  }
  
  @Deprecated
  public static String getLengthUnit() {
    return "MILE";
  }
}
