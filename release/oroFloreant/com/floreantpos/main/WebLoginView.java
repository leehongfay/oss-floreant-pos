package com.floreantpos.main;

import com.floreantpos.bo.ui.WebBackOffice;
import com.floreantpos.model.dao.UserDAO;
import java.awt.EventQueue;
import java.awt.Toolkit;

public class WebLoginView
{
  public WebLoginView() {}
  
  public static void main(String[] args)
  {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        Application.getInstance().startInvisible();
        com.floreantpos.model.User user = UserDAO.getInstance().findUserBySecretKey("1111");
        Application.getInstance().setCurrentUser(user);
        
        WebBackOffice webBackOffice = new WebBackOffice();
        webBackOffice.setSize(Toolkit.getDefaultToolkit().getScreenSize());
        webBackOffice.setVisible(true);
      }
    });
  }
}
