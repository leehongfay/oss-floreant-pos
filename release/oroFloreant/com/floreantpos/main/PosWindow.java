package com.floreantpos.main;

import com.floreantpos.actions.ShutDownAction;
import com.floreantpos.config.AppConfig;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.UserType;
import com.floreantpos.swing.GlassPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.AutoLogOffAlertDialog;
import com.floreantpos.util.PosGuiUtil;
import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;


















public class PosWindow
  extends JFrame
  implements WindowListener
{
  private static final String EXTENDEDSTATE = "extendedstate";
  private static final String WLOCY = "wlocy";
  private static final String WLOCX = "wlocx";
  private static final String WHEIGHT = "wheight";
  private static final String WWIDTH = "wwidth";
  private GlassPane glassPane;
  private JLabel statusLabel;
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss aaa");
  private Timer clockTimer = new Timer(1000, new ClockTimerHandler(null));
  
  private JLabel lblRestaurantName;
  
  private JLabel lblUser;
  private JLabel lblTerminal;
  private JLabel lblDatabase;
  private JLabel lblTime;
  private Timer autoLogoffTimer;
  private AutoLogoffHandler autoLogoffHandler = new AutoLogoffHandler();
  
  public PosWindow() {
    setIconImage(Application.getApplicationIcon().getImage());
    
    addWindowListener(this);
    
    glassPane = new GlassPane();
    glassPane.setOpacity(0.6F);
    setGlassPane(glassPane);
    
    statusLabel = new JLabel();
    
    JPanel statusBarContainer = new JPanel(new BorderLayout());
    statusBarContainer.add(new JSeparator(0), "North");
    
    JPanel infoPanel = new JPanel(new MigLayout("fillx, ins 5", "[]20px[]20px[]", "[]"));
    infoPanel.setOpaque(true);
    Font f = statusLabel.getFont().deriveFont(1, PosUIManager.getFontSize(10));
    lblRestaurantName = new JLabel();
    lblTerminal = new JLabel("Terminal: " + TerminalConfig.getTerminalId());
    lblUser = new JLabel();
    lblDatabase = new JLabel();
    
    lblTime = new JLabel("");
    lblTime.setBorder(new EmptyBorder(5, 5, 5, 5));
    
    lblRestaurantName.setFont(f);
    lblUser.setFont(f);
    lblTerminal.setFont(f);
    lblDatabase.setFont(f);
    lblTime.setFont(f);
    
    infoPanel.add(lblRestaurantName);
    infoPanel.add(lblTerminal);
    infoPanel.add(lblUser);
    infoPanel.add(lblDatabase, "grow");
    
    statusBarContainer.add(infoPanel, "West");
    statusBarContainer.add(lblTime, "East");
    
    getContentPane().add(statusBarContainer, "South");
    
    clockTimer.start();
  }
  
  public void rendererUserInfo() {
    User currentUser = Application.getCurrentUser();
    if (currentUser != null) {
      lblUser.setText("USER: " + currentUser.getFullName() + " (" + currentUser.getType().getName() + ")");
    }
    else {
      lblUser.setText("USER: NOT LOGGED IN");
    }
  }
  
  public void setRestaurantName(String name) {
    lblRestaurantName.setText(name);
  }
  
  public void setStatus(String status) {
    statusLabel.setText(status);
  }
  
  public void setupSizeAndLocation() {
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    setSize(AppConfig.getInt("wwidth", (int)screenSize.getWidth()), AppConfig.getInt("wheight", (int)screenSize.getHeight()));
    
    setLocation(AppConfig.getInt("wlocx", width - getWidth() >> 1), AppConfig.getInt("wlocy", height - getHeight() >> 1));
    setMinimumSize(new Dimension(1024, 724));
    setDefaultCloseOperation(0);
    
    int extendedState = AppConfig.getInt("extendedstate", -1);
    if (extendedState != -1) {
      setExtendedState(extendedState);
    }
  }
  
  public void enterFullScreenMode() {
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    setSize(screenSize);
    setExtendedState(6);
    setUndecorated(true);
    setLocation(0, 0);
  }
  

  public void leaveFullScreenMode()
  {
    GraphicsDevice window = java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[0];
    setUndecorated(false);
    window.setFullScreenWindow(null);
  }
  
  public void saveSizeAndLocation() {
    int width = getWidth();
    int height = getHeight();
    AppConfig.putInt("wwidth", width);
    AppConfig.putInt("wheight", height);
    
    Point locationOnScreen = getLocationOnScreen();
    AppConfig.putInt("wlocx", x);
    AppConfig.putInt("wlocy", y);
    
    AppConfig.putInt("extendedstate", getExtendedState());
  }
  
  public void setVisible(boolean aFlag)
  {
    super.setVisible(aFlag);
    
    if (aFlag) {
      startTimer();
    }
    else {
      stopTimer();
    }
  }
  
  private void startTimer() {
    clockTimer.start();
  }
  
  private void stopTimer() {
    clockTimer.stop();
  }
  
  public void startAutoLogoffTimer() {
    Terminal terminal = Application.getInstance().getTerminal();
    if ((terminal != null) && (terminal.isAutoLogOff().booleanValue())) {
      autoLogoffTimer = new Timer(terminal.getAutoLogOffSec().intValue() * 1000, autoLogoffHandler);
      autoLogoffTimer.start();
      
      Toolkit.getDefaultToolkit().addAWTEventListener(autoLogoffHandler, 56L);
    }
  }
  
  public void stopAutoLogoffTimer() {
    if (autoLogoffTimer != null) {
      autoLogoffTimer.stop();
      autoLogoffTimer = null;
      Toolkit.getDefaultToolkit().removeAWTEventListener(autoLogoffHandler);
    }
  }
  
  private class ClockTimerHandler implements ActionListener {
    private ClockTimerHandler() {}
    
    public void actionPerformed(ActionEvent e) { if (!isShowing()) {
        clockTimer.stop();
        return;
      }
      
      PosWindow.this.showFooterTimer();
    }
  }
  
  private void showFooterTimer() {
    StringBuilder sb = new StringBuilder();
    sb.append(dateFormat.format(Calendar.getInstance().getTime()));
    lblTime.setText(sb.toString());
  }
  
  public void setGlassPaneVisible(boolean b) {
    glassPane.setVisible(b);
  }
  
  public void updateView() {
    lblTerminal.setText("Terminal: " + TerminalConfig.getTerminalId());
  }
  
  public void windowOpened(WindowEvent e) {}
  
  public void windowClosing(WindowEvent e)
  {
    if (Application.getCurrentUser() != null) {
      new ShutDownAction().actionPerformed(null);
    }
    else {
      Application.getInstance().shutdownPOS();
    }
  }
  

  public void windowClosed(WindowEvent e) {}
  

  public void windowIconified(WindowEvent e) {}
  

  public void windowDeiconified(WindowEvent e) {}
  

  public void windowActivated(WindowEvent e) {}
  

  public void windowDeactivated(WindowEvent e) {}
  
  private class AutoLogoffHandler
    implements ActionListener, AWTEventListener
  {
    public AutoLogoffHandler() {}
    
    public void eventDispatched(AWTEvent event)
    {
      reset();
    }
    
    public void actionPerformed(ActionEvent e)
    {
      autoLogoffTimer.stop();
      if (!isShowing()) {
        return;
      }
      
      if (PosGuiUtil.isModalDialogShowing()) {
        reset();
        return;
      }
      
      AutoLogOffAlertDialog dialog = new AutoLogOffAlertDialog();
      dialog.pack();
      dialog.open();
      if (dialog.isCanceled()) {
        reset();
        return;
      }
      Application.getInstance().doAutoLogout();
    }
    
    public void reset() {
      if (autoLogoffTimer != null) {
        autoLogoffTimer.restart();
      }
      else {
        Toolkit.getDefaultToolkit().removeAWTEventListener(autoLogoffHandler);
      }
    }
  }
}
