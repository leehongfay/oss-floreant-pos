package com.floreantpos.teminaltype;

import com.floreantpos.actions.PosAction;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;


public class TerminalTypeAction
  extends PosAction
{
  public TerminalTypeAction()
  {
    super("Terminal Type");
  }
  












  public void execute()
  {
    try
    {
      TerminalTypeDialog dialog = new TerminalTypeDialog();
      dialog.setSize(900, 600);
      dialog.open();
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e);
    }
  }
}
