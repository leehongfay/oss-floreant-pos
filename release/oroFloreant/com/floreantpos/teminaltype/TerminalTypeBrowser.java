package com.floreantpos.teminaltype;

import com.floreantpos.model.TerminalType;
import com.floreantpos.model.dao.TerminalTypeDAO;
import com.floreantpos.swing.BeanTableModel;
import java.util.List;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.jdesktop.swingx.JXTable;

public class TerminalTypeBrowser extends TerminalTypeModelBrowser<TerminalType>
{
  public TerminalTypeBrowser()
  {
    super(new TerminalTypeForm());
    
    BeanTableModel<TerminalType> tableModel = new BeanTableModel(TerminalType.class);
    tableModel.addColumn("Name", TerminalType.PROP_NAME);
    init(tableModel);
    browserTable.setAutoResizeMode(4);
  }
  
  public void refreshTable()
  {
    List<TerminalType> tables = TerminalTypeDAO.getInstance().findAll();
    BeanTableModel tableModel = (BeanTableModel)browserTable.getModel();
    tableModel.removeAll();
    tableModel.addRows(tables);
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = browserTable.getColumnModel().getColumn(columnNumber);
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
}
