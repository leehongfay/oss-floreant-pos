package com.floreantpos.teminaltype;

import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;


















public class TerminalTypeDialog
  extends POSDialog
{
  private TitlePanel titelpanel;
  
  public TerminalTypeDialog()
  {
    super(POSUtil.getFocusedWindow(), "");
    init();
  }
  
  private void init() {
    setLayout(new BorderLayout(5, 5));
    setTitle("Terminal Type");
    titelpanel = new TitlePanel();
    titelpanel.setTitle("Terminal Types");
    
    add(titelpanel, "North");
    
    JPanel contentPane = new JPanel(new MigLayout("fill,hidemode 3,inset 0 10 0 10"));
    contentPane.add(new TerminalTypeBrowser(), "grow,span,wrap");
    
    PosButton btnCancel = new PosButton("CLOSE");
    btnCancel.setFocusable(false);
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TerminalTypeDialog.this.doCancel();
      }
      
    });
    JPanel footerPanel = new JPanel(new MigLayout("center,ins 0 5 5 5", "", ""));
    
    footerPanel.add(btnCancel);
    add(footerPanel, "South");
    
    add(contentPane);
  }
  
  private void doOk() {
    setCanceled(false);
    dispose();
  }
  
  private void doCancel() {
    setCanceled(true);
    dispose();
  }
}
