package com.floreantpos.teminaltype;

public enum TerminalCategory {
  REGULAR(Integer.valueOf(0)),  WEB(Integer.valueOf(1)),  KIOSK(Integer.valueOf(2)),  TABLET(Integer.valueOf(3));
  
  private Integer type;
  
  private TerminalCategory(Integer type) {
    this.type = type;
  }
  
  public Integer getType() {
    return type;
  }
  
  public void setType(Integer type) {
    this.type = type;
  }
  
  public static TerminalCategory getType(int type) {
    for (TerminalCategory category : ) {
      if (category.getType().intValue() == type) {
        return category;
      }
    }
    return REGULAR;
  }
}
