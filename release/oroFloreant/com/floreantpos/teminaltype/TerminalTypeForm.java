package com.floreantpos.teminaltype;

import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.TerminalType;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.model.dao.OrderTypeDAO;
import com.floreantpos.model.dao.TerminalTypeDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.CheckBoxList;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.Component;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import net.miginfocom.swing.MigLayout;
import org.hibernate.StaleObjectStateException;





public class TerminalTypeForm
  extends BeanEditor<TerminalType>
{
  private FixedLengthTextField tfName;
  private CheckBoxList orderTypeCheckBoxList;
  private CheckBoxList categoryCheckBoxList;
  private String newTerminalType;
  private TerminalCategoryPanel terminalTypePanel;
  
  public TerminalTypeForm()
  {
    setLayout(new MigLayout("", "[][grow]", "[][][][]"));
    setBorder(BorderFactory.createTitledBorder("Terminal Type"));
    
    orderTypeCheckBoxList = new CheckBoxList();
    orderTypeCheckBoxList.setModel(OrderTypeDAO.getInstance().findAll());
    
    categoryCheckBoxList = new CheckBoxList();
    categoryCheckBoxList.setModel(MenuCategoryDAO.getInstance().findAll());
    
    JLabel lblName = new JLabel("Name");
    add(lblName, "alignx right,aligny center");
    
    tfName = new FixedLengthTextField();
    add(tfName, "alignx left,growx,split 2");
    
    terminalTypePanel = new TerminalCategoryPanel();
    add(terminalTypePanel, "wrap");
    
    JScrollPane orderTypeScrollPane = new JScrollPane(orderTypeCheckBoxList);
    orderTypeScrollPane.setBorder(BorderFactory.createTitledBorder("Order Types"));
    
    add(orderTypeScrollPane, "skip 1,split 2,grow");
    
    JScrollPane categoryScrollPane = new JScrollPane(categoryCheckBoxList);
    categoryScrollPane.setBorder(BorderFactory.createTitledBorder("Categories"));
    
    add(categoryScrollPane, "grow");
  }
  
  public void createNew()
  {
    TerminalType bean2 = new TerminalType();
    setBean(bean2);
    tfName.setText("");
    orderTypeCheckBoxList.unCheckAll();
    categoryCheckBoxList.unCheckAll();
    setBorder(BorderFactory.createTitledBorder("New Terminal Type"));
  }
  
  public void cancel()
  {
    setBorder(BorderFactory.createTitledBorder("Terminal Type"));
  }
  
  public void clearFields()
  {
    tfName.setText("");
    orderTypeCheckBoxList.unCheckAll();
    categoryCheckBoxList.unCheckAll();
    terminalTypePanel.setSelectedTerminalCategory(Integer.valueOf(0));
  }
  
  public boolean delete()
  {
    try {
      TerminalType bean2 = (TerminalType)getBean();
      if (bean2 == null) {
        return false;
      }
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), "Are you sure you want to delete?", "Confirm");
      if (option != 0) {
        return false;
      }
      TerminalTypeDAO.getInstance().delete(bean2);
      tfName.setText("");
      orderTypeCheckBoxList.unCheckAll();
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e);
    }
    return false;
  }
  
  public boolean deleteAllTables()
  {
    List<TerminalType> list = TerminalTypeDAO.getInstance().findAll();
    
    if (list.isEmpty()) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), "Empty");
      return false;
    }
    
    int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), "Delete All?", "Confirm");
    if (option != 0) {
      return false;
    }
    for (TerminalType terminalType : list) {
      TerminalTypeDAO.getInstance().delete(terminalType);
    }
    tfName.setText("");
    orderTypeCheckBoxList.unCheckAll();
    
    return true;
  }
  
  public void setFieldsEditerminalType(boolean editerminalType) {
    tfName.setEditable(editerminalType);
  }
  

  public void setFieldsEnable(boolean enable)
  {
    orderTypeCheckBoxList.setEnabled(enable);
    orderTypeCheckBoxList.clearSelection();
    
    categoryCheckBoxList.setEnabled(enable);
    categoryCheckBoxList.clearSelection();
    
    tfName.setEnabled(enable);
    terminalTypePanel.setEnableButtons(enable);
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      TerminalType terminalType = (TerminalType)getBean();
      TerminalTypeDAO.getInstance().saveOrUpdate(terminalType);
      updateView();
      return true;
    }
    catch (IllegalModelStateException localIllegalModelStateException) {}catch (StaleObjectStateException e)
    {
      BOMessageDialog.showError(this, "Error");
    }
    return false;
  }
  
  protected void updateView()
  {
    TerminalType terminalType = (TerminalType)getBean();
    if (terminalType == null) {
      return;
    }
    TerminalTypeDAO.getInstance().initialize(terminalType);
    orderTypeCheckBoxList.unCheckAll();
    categoryCheckBoxList.unCheckAll();
    orderTypeCheckBoxList.selectItems(terminalType.getOrderTypes());
    categoryCheckBoxList.selectItems(terminalType.getCategories());
    tfName.setText(terminalType.getName());
    terminalTypePanel.setSelectedTerminalCategory(terminalType.getType());
    setBorder(BorderFactory.createTitledBorder("Terminal Type"));
  }
  
  protected boolean updateModel()
    throws IllegalModelStateException
  {
    TerminalType terminalType = (TerminalType)getBean();
    
    if (terminalType == null) {
      terminalType = new TerminalType();
      setBean(terminalType, false);
    }
    if (TerminalTypeDAO.getInstance().nameExists(terminalType, tfName.getText())) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Name already exists.");
      return false;
    }
    terminalType.setName(tfName.getText());
    
    List<OrderType> checkValues = orderTypeCheckBoxList.getCheckedValues();
    terminalType.setOrderTypes(checkValues);
    
    List<MenuCategory> checkedCategories = categoryCheckBoxList.getCheckedValues();
    terminalType.setCategories(checkedCategories);
    
    terminalType.setType(Integer.valueOf(terminalTypePanel.getSelectedTerminalCategory()));
    
    newTerminalType = terminalType.getName();
    return true;
  }
  
  public void edit()
  {
    setBorder(BorderFactory.createTitledBorder("Edit terminal type"));
  }
  
  public String getDisplayText()
  {
    return "Terminal Type";
  }
  
  public void setTableTypeCBoxListEnable(boolean enable) {
    orderTypeCheckBoxList.setEnabled(enable);
    categoryCheckBoxList.setEnabled(enable);
  }
  
  public String getNewlyAddedRow() {
    return newTerminalType;
  }
  
  private class TerminalCategoryPanel extends JPanel
  {
    public TerminalCategoryPanel() {
      setLayout(new MigLayout());
      ButtonGroup group = new ButtonGroup();
      for (TerminalCategory category : TerminalCategory.values()) {
        JRadioButton btnCategory = new JRadioButton(category.toString());
        btnCategory.putClientProperty("terminalType", category.getType());
        group.add(btnCategory);
        add(btnCategory);
      }
    }
    
    public void setSelectedTerminalCategory(Integer type) {
      for (Component c : getComponents()) {
        JRadioButton button = (JRadioButton)c;
        int terminalType = ((Integer)button.getClientProperty("terminalType")).intValue();
        if (terminalType == type.intValue()) {
          button.setSelected(true);
          break;
        }
      }
    }
    
    public void setEnableButtons(boolean b) {
      for (Component c : getComponents()) {
        JRadioButton button = (JRadioButton)c;
        button.setEnabled(b);
      }
    }
    
    public int getSelectedTerminalCategory() {
      for (Component c : getComponents()) {
        JRadioButton button = (JRadioButton)c;
        if (button.isSelected())
          return ((Integer)button.getClientProperty("terminalType")).intValue();
      }
      return 0;
    }
  }
}
