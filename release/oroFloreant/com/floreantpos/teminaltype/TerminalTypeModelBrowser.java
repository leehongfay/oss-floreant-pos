package com.floreantpos.teminaltype;

import com.floreantpos.Messages;
import com.floreantpos.PosLog;
import com.floreantpos.bo.ui.Command;
import com.floreantpos.bo.ui.ModelBrowser;
import com.floreantpos.model.TerminalType;
import com.floreantpos.ui.BeanEditor;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableModel;
import org.jdesktop.swingx.JXTable;

public class TerminalTypeModelBrowser<E> extends ModelBrowser
{
  private BeanEditor beanEditor;
  private JButton btnDeleteAll = new JButton(Messages.getString("ShopTableModelBrowser.1"));
  
  public TerminalTypeModelBrowser(BeanEditor<E> beanEditor) {
    super(beanEditor);
    this.beanEditor = beanEditor;
  }
  
  public void init(TableModel tableModel)
  {
    super.init(tableModel);
    
    buttonPanel.add(btnDeleteAll);
    btnDeleteAll.addActionListener(this);
  }
  
  public void actionPerformed(ActionEvent e)
  {
    Command command = Command.fromString(e.getActionCommand());
    try {
      switch (1.$SwitchMap$com$floreantpos$bo$ui$Command[command.ordinal()]) {
      case 1: 
        beanEditor.createNew();
        beanEditor.setFieldsEnable(true);
        btnNew.setEnabled(false);
        btnEdit.setEnabled(false);
        btnSave.setEnabled(true);
        btnDelete.setEnabled(false);
        btnCancel.setEnabled(true);
        browserTable.clearSelection();
        break;
      
      case 2: 
        beanEditor.edit();
        beanEditor.setFieldsEnable(true);
        btnNew.setEnabled(false);
        btnEdit.setEnabled(false);
        btnSave.setEnabled(true);
        btnDelete.setEnabled(false);
        btnCancel.setEnabled(true);
        break;
      
      case 3: 
        doCancelEditing();
        break;
      
      case 4: 
        if (beanEditor.save()) {
          beanEditor.setFieldsEnable(false);
          btnNew.setEnabled(true);
          btnEdit.setEnabled(false);
          btnSave.setEnabled(false);
          btnDelete.setEnabled(false);
          btnCancel.setEnabled(false);
          refreshTable();
          customSelectedRow();
        }
        
        break;
      case 5: 
        if (beanEditor.delete()) {
          beanEditor.setBean(null);
          beanEditor.setFieldsEnable(false);
          btnNew.setEnabled(true);
          btnEdit.setEnabled(false);
          btnSave.setEnabled(false);
          btnDelete.setEnabled(false);
          btnCancel.setEnabled(false);
          refreshTable();
        }
        

        break;
      }
      
      
      handleAdditionaButtonActionIfApplicable(e);
      
      TerminalTypeForm form = (TerminalTypeForm)beanEditor;
      if (e.getSource() == btnDeleteAll)
      {
        if (!form.deleteAllTables()) {
          return;
        }
        refreshTable();
        btnNew.setEnabled(true);
        btnEdit.setEnabled(false);
        btnSave.setEnabled(false);
        btnDelete.setEnabled(false);
        btnCancel.setEnabled(false);
      }
    }
    catch (Exception e2) {
      PosLog.error(getClass(), e2);
    }
  }
  
  public void valueChanged(ListSelectionEvent e)
  {
    super.valueChanged(e);
    btnDeleteAll.setEnabled(true);
  }
  
  public void doCancelEditing()
  {
    super.doCancelEditing();
    if (browserTable.getSelectedRow() != -1) {}
  }
  
  private void customSelectedRow()
  {
    TerminalTypeForm form = (TerminalTypeForm)beanEditor;
    TerminalType newlyAddedRow = (TerminalType)form.getBean();
    if (newlyAddedRow == null)
      return;
    int x = getRowByValue(browserTable.getModel(), newlyAddedRow);
    browserTable.setRowSelectionInterval(x, x);
  }
  
  private int getRowByValue(TableModel model, TerminalType value) {
    for (int i = 0; i <= model.getRowCount(); i++) {
      String terminalType = (String)model.getValueAt(i, 0);
      if (terminalType.equals(value.getName())) {
        return i;
      }
    }
    return -1;
  }
}
