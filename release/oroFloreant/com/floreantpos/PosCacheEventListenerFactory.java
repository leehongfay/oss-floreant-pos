package com.floreantpos;

import java.util.Properties;
import net.sf.ehcache.event.CacheEventListenerFactory;

public class PosCacheEventListenerFactory extends CacheEventListenerFactory
{
  public PosCacheEventListenerFactory() {}
  
  public net.sf.ehcache.event.CacheEventListener createCacheEventListener(Properties properties)
  {
    return new PosCacheEventListener();
  }
}
