package com.floreantpos.table;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.explorer.ExplorerButtonPanel;
import com.floreantpos.model.BookingInfo;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.ShopTableType;
import com.floreantpos.model.TableStatus;
import com.floreantpos.model.dao.BookingInfoDAO;
import com.floreantpos.model.dao.ShopTableDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.NumberSelectionDialog2;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang3.StringUtils;





public class ShopTableBrowser
  extends TransparentPanel
{
  private JTable table;
  private BeanTableModel<ShopTable> tableModel;
  private JTextField tfSearchSubject;
  
  public ShopTableBrowser()
  {
    initComponents();
    initData();
  }
  


  private void initComponents()
  {
    setBorder(new TitledBorder(null, "TABLES", 2, 2, null, null));
    tableModel = new BeanTableModel(ShopTable.class);
    
    tableModel.addColumn(Messages.getString("ShopTableBrowser.0"), ShopTable.PROP_ID);
    tableModel.addColumn("Name", ShopTable.PROP_NAME);
    tableModel.addColumn(Messages.getString("ShopTableBrowser.1"), ShopTable.PROP_CAPACITY);
    tableModel.addColumn("Type", "typesAsString");
    tableModel.addColumn("Min Guest", ShopTable.PROP_MIN_CAPACITY);
    tableModel.addColumn("Reservable", ShopTable.PROP_RESERVABLE);
    tableModel.addColumn(Messages.getString("ShopTableBrowser.2"), ShopTable.PROP_DESCRIPTION);
    
    table = new JTable(tableModel);
    
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    table.setSelectionMode(0);
    table.setRowHeight(PosUIManager.getSize(20));
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          ShopTableBrowser.this.doEditSelectedRow();
        }
      }
    });
    table.setRowHeight(PosUIManager.getSize(30));
    setLayout(new BorderLayout(5, 5));
    add(new PosScrollPane(table), "Center");
    
    add(createButtonPanel(), "South");
    add(createSearchPanel(), "North");
  }
  
  private JPanel createButtonPanel()
  {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton editButton = explorerButton.getEditButton();
    JButton addButton = explorerButton.getAddButton();
    JButton deleteButton = explorerButton.getDeleteButton();
    
    addButton.setText("NEW");
    editButton.setText("EDIT");
    deleteButton.setText("DELETE");
    








    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        ShopTableBrowser.this.doEditSelectedRow();
      }
      
    });
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        ShopTableBrowser.this.doCreateShopTable();
      }
      
    });
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        ShopTableBrowser.this.doDeleteShopTable();
      }
      

    });
    JButton btnDuplicate = new JButton("DUPLICATE");
    btnDuplicate.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        ShopTableBrowser.this.doDuplicateShopTable();
      }
      
    });
    JButton btnDeleteAll = new JButton("DELETE ALL");
    btnDeleteAll.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        ShopTableBrowser.this.doDeleteAllShopTable();
      }
      
    });
    JPanel bottomPanel = new JPanel(new MigLayout("center", ""));
    TransparentPanel actionButtonPanel = new TransparentPanel();
    actionButtonPanel.add(addButton);
    actionButtonPanel.add(editButton);
    actionButtonPanel.add(deleteButton);
    actionButtonPanel.add(btnDuplicate);
    actionButtonPanel.add(btnDeleteAll);
    bottomPanel.add(actionButtonPanel, "");
    
    return bottomPanel;
  }
  
  public void initData() {
    List<ShopTable> tables = ShopTableDAO.getInstance().findAll();
    BeanTableModel tableModel = (BeanTableModel)table.getModel();
    tableModel.removeAll();
    tableModel.addRows(tables);
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = table.getColumnModel().getColumn(columnNumber);
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  public void doRefreshTable() {
    List<ShopTable> tables = ShopTableDAO.getInstance().findAll();
    BeanTableModel tableModel = (BeanTableModel)table.getModel();
    tableModel.removeAll();
    tableModel.addRows(tables);
  }
  
  private void doCreateShopTable() {
    ShopTableForm editor = new ShopTableForm(new ShopTable(), false);
    BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
    dialog.openFullScreen();
    
    if (dialog.isCanceled()) {
      return;
    }
    ShopTable shopTable = (ShopTable)editor.getBean();
    
    tableModel.addRow(shopTable);
  }
  
  private void doEditSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select a table.");
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      ShopTable shopTable = (ShopTable)tableModel.getRow(index);
      
      tableModel.setRow(index, shopTable);
      
      ShopTableForm editor = new ShopTableForm(shopTable, false);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.openFullScreen();
      
      if (dialog.isCanceled()) {
        return;
      }
      
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doDeleteShopTable()
  {
    int index = table.getSelectedRow();
    if (index < 0) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select a table.");
      return;
    }
    
    index = table.convertRowIndexToModel(index);
    
    ShopTable shopTable = (ShopTable)tableModel.getRow(index);
    
    tableModel.setRow(index, shopTable);
    
    ShopTableForm editor = new ShopTableForm(shopTable, false);
    editor.delete();
    
    table.repaint();
    doRefreshTable();
  }
  
  private void doDeleteAllShopTable()
  {
    boolean isDeleted = deleteAllTables();
    if (isDeleted) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "All table successfully deleted!");
    }
    table.repaint();
    doRefreshTable();
  }
  
  public boolean deleteAllTables()
  {
    List<ShopTable> list = ShopTableDAO.getInstance().findAll();
    
    if (list.isEmpty()) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), Messages.getString("ShopTableForm.51"));
      return false;
    }
    
    int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), Messages.getString("ShopTableForm.20"), 
      Messages.getString("ShopTableForm.21"));
    if (option != 0) {
      return false;
    }
    
    List<BookingInfo> bookingList = BookingInfoDAO.getInstance().findAll();
    
    for (BookingInfo info : bookingList) {
      info.setTables(null);
      BookingInfoDAO.getInstance().saveOrUpdate(info);
    }
    
    for (ShopTable table : list) {
      table.setFloor(null);
      table.setTypes(null);
      ShopTableDAO.getInstance().delete(table);
    }
    
    return true;
  }
  
  private void doDuplicateShopTable() {
    int index = table.getSelectedRow();
    if (index < 0) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select a table.");
      return;
    }
    
    index = table.convertRowIndexToModel(index);
    
    ShopTable shopTable = (ShopTable)tableModel.getRow(index);
    


    NumberSelectionDialog2 dialog = new NumberSelectionDialog2();
    dialog.setTitle("How many duplicates?");
    dialog.setFloatingPoint(false);
    dialog.pack();
    dialog.open();
    
    if (dialog.isCanceled()) {
      return;
    }
    int numberOfDuplicates = (int)dialog.getValue();
    
    for (int i = 0; i < numberOfDuplicates; i++) {
      doDuplicate(shopTable);
    }
    
    table.repaint();
    doRefreshTable();
  }
  
  private void doDuplicate(ShopTable originalTable) {
    ShopTable table = new ShopTable();
    int nxtTableNumber = ShopTableDAO.getInstance().getNextTableNumber();
    
    if (table.getId() == null) {
      table.setId(Integer.valueOf(nxtTableNumber + 1));
    }
    List<ShopTableType> types = originalTable.getTypes();
    List<ShopTableType> newTypes = new ArrayList();
    for (ShopTableType shopTableType : types) {
      newTypes.add(shopTableType);
    }
    table.setTypes(newTypes);
    table.setCapacity(originalTable.getCapacity());
    table.setDescription(originalTable.getDescription());
    table.setName(originalTable.getName());
    table.setTableStatus(TableStatus.Available);
    
    table.setMinCapacity(originalTable.getMinCapacity());
    table.setReservable(originalTable.isReservable());
    
    ShopTableDAO.getInstance().saveOrUpdate(table);
  }
  
  private JPanel createSearchPanel() {
    JPanel panel = new JPanel();
    panel.setLayout(new MigLayout("", "[][][][]", ""));
    
    JLabel lblSearch = new JLabel("Search by capacity: ");
    tfSearchSubject = new JTextField(15);
    JButton btnSearch = new JButton("Search");
    JButton btnReset = new JButton("Reset");
    JButton btnRefresh = new JButton("Refresh");
    
    tfSearchSubject.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ShopTableBrowser.this.doSearchTable();
      }
      
    });
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ShopTableBrowser.this.doSearchTable();
      }
      
    });
    btnReset.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tfSearchSubject.setText("");
        doRefreshTable();
      }
      
    });
    btnRefresh.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tfSearchSubject.setText("");
        doRefreshTable();
      }
      
    });
    panel.add(lblSearch, "grow");
    panel.add(tfSearchSubject, "grow");
    panel.add(btnSearch, "grow");
    
    panel.add(btnRefresh, "grow");
    
    return panel;
  }
  
  private void doSearchTable() {
    String searchSubject = tfSearchSubject.getText();
    if (StringUtils.isEmpty(searchSubject)) {
      return;
    }
    List<ShopTable> searchedTables = ShopTableDAO.getInstance().findBy(searchSubject);
    
    if ((searchedTables == null) || (searchedTables.size() == 0)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "No result found!");
      return;
    }
    BeanTableModel tableModel = (BeanTableModel)table.getModel();
    tableModel.removeAll();
    tableModel.addRows(searchedTables);
  }
}
