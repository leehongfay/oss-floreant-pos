package com.floreantpos.table;

import com.floreantpos.Messages;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.FloorLayoutPlugin;
import com.floreantpos.model.BookingInfo;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.ShopTableType;
import com.floreantpos.model.TableStatus;
import com.floreantpos.model.dao.BookingInfoDAO;
import com.floreantpos.model.dao.ShopTableDAO;
import com.floreantpos.model.dao.ShopTableTypeDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.CheckBoxList;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.List;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.hibernate.StaleStateException;





public class ShopTableForm
  extends BeanEditor<ShopTable>
{
  private FixedLengthTextField tfTableDescription;
  private IntegerTextField tfTableCapacity;
  private IntegerTextField tfTableNo;
  private FixedLengthTextField tfTableName;
  private CheckBoxList tableTypeCBoxList;
  private JPanel statusPanel;
  private JRadioButton rbFree;
  private JRadioButton rbDisable;
  private JButton btnCapacityOne;
  private JButton btnCapacityTwo;
  private JButton btnCapacityFour;
  private JButton btnCapacitySix;
  private JButton btnCapacityEight;
  private JButton btnCapacityTen;
  private JButton btnCreateType;
  private int newTable;
  private boolean dupTableEnable;
  private boolean dupTableDisable;
  private String dupName;
  private Integer dupCapacity;
  private String dupDescription;
  private List<ShopTableType> dupCheckValues;
  private int selectedTable;
  private boolean duplicate;
  private IntegerTextField tfTableMinCapacity;
  private JCheckBox chkReservable;
  
  public ShopTableForm(ShopTable shopTable, boolean isDuplicate)
  {
    setDuplicate(isDuplicate);
    initComponents();
    initData();
    setBean(shopTable);
  }
  
  private void initComponents() {
    setBorder(new TitledBorder(null, Messages.getString("ShopTableForm.19"), 2, 2, null, null));
    setPreferredSize(new Dimension(600, 800));
    setLayout(new MigLayout("", "50[][grow]150", ""));
    tableTypeCBoxList = new CheckBoxList();
    tableTypeCBoxList.setModel(ShopTableTypeDAO.getInstance().findAll());
    JScrollPane tableTypeCheckBoxList = new JScrollPane(tableTypeCBoxList);
    tableTypeCheckBoxList.setPreferredSize(new Dimension(0, 350));
    
    JLabel lblTableNo = new JLabel(Messages.getString("ShopTableForm.0"));
    add(lblTableNo, "trailing");
    
    tfTableNo = new IntegerTextField(6);
    tfTableNo.setEnabled(false);
    add(tfTableNo, "wrap");
    
    JLabel lblTableName = new JLabel("Table name");
    add(lblTableName, "trailing");
    
    tfTableName = new FixedLengthTextField(20);
    add(tfTableName, "grow, wrap");
    
    JLabel lblTableDescription = new JLabel(Messages.getString("ShopTableForm.2"));
    add(lblTableDescription, "trailing");
    
    tfTableDescription = new FixedLengthTextField();
    add(tfTableDescription, "grow, wrap");
    
    JLabel lblCapacity = new JLabel(Messages.getString("ShopTableForm.3"), 4);
    add(lblCapacity, "grow, trailing");
    
    tfTableCapacity = new IntegerTextField(6);
    add(tfTableCapacity, "grow, split 7");
    
    ActionListener action = new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        if (e.getSource() == btnCapacityOne) {
          tfTableCapacity.setText("1");
        }
        else if (e.getSource() == btnCapacityTwo) {
          tfTableCapacity.setText("2");
        }
        else if (e.getSource() == btnCapacityFour) {
          tfTableCapacity.setText("4");
        }
        else if (e.getSource() == btnCapacitySix) {
          tfTableCapacity.setText("6");
        }
        else if (e.getSource() == btnCapacityEight) {
          tfTableCapacity.setText("8");
        }
        else if (e.getSource() == btnCapacityTen) {
          tfTableCapacity.setText("10");
        }
        
      }
    };
    btnCapacityOne = new PosButton("1");
    btnCapacityOne.setPreferredSize(new Dimension(52, 52));
    btnCapacityTwo = new PosButton("2");
    btnCapacityTwo.setPreferredSize(new Dimension(52, 52));
    btnCapacityFour = new PosButton("4");
    btnCapacityFour.setPreferredSize(new Dimension(52, 52));
    btnCapacitySix = new PosButton("6");
    btnCapacitySix.setPreferredSize(new Dimension(52, 52));
    btnCapacityEight = new PosButton("8");
    btnCapacityEight.setPreferredSize(new Dimension(52, 52));
    btnCapacityTen = new PosButton("10");
    btnCapacityTen.setPreferredSize(new Dimension(52, 52));
    
    btnCapacityOne.addActionListener(action);
    btnCapacityTwo.addActionListener(action);
    btnCapacityFour.addActionListener(action);
    btnCapacitySix.addActionListener(action);
    btnCapacityEight.addActionListener(action);
    btnCapacityTen.addActionListener(action);
    
    add(btnCapacityOne, "");
    add(btnCapacityTwo, "");
    add(btnCapacityFour, "");
    add(btnCapacitySix, "");
    add(btnCapacityEight, "");
    add(btnCapacityTen, "");
    
    JLabel lblMinCapacity = new JLabel("Min guest", 4);
    add(lblMinCapacity, "newline, trailing");
    
    tfTableMinCapacity = new IntegerTextField(6);
    add(tfTableMinCapacity, "grow, wrap");
    
    chkReservable = new JCheckBox("Reservable");
    chkReservable.setSelected(true);
    add(chkReservable, "skip 1, grow, wrap");
    
    statusPanel = new JPanel();
    statusPanel.setBorder(new TitledBorder(null, Messages.getString("ShopTableForm.4"), 2, 2, null, null));
    add(statusPanel, "skip 1, grow, wrap");
    statusPanel.setLayout(new FlowLayout(0, 5, 5));
    
    rbFree = new JRadioButton(Messages.getString("ShopTableForm.5"));
    statusPanel.add(rbFree);
    
    rbDisable = new JRadioButton(Messages.getString("ShopTableForm.9"));
    statusPanel.add(rbDisable);
    
    ButtonGroup buttonGroup = new ButtonGroup();
    buttonGroup.add(rbFree);
    buttonGroup.add(rbDisable);
    
    add(new JLabel(), "grow,span");
    
    final FloorLayoutPlugin floorLayoutPlugin = (FloorLayoutPlugin)ExtensionManager.getPlugin(FloorLayoutPlugin.class);
    
    if (floorLayoutPlugin != null) {
      btnCreateType = new JButton(Messages.getString("ShopTableForm.40"));
      add(new JLabel(Messages.getString("ShopTableForm.10")), "trailing");
      add(tableTypeCheckBoxList, "span,grow");
      add(btnCreateType, "skip 1");
      
      btnCreateType.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), floorLayoutPlugin.getBeanEditor());
          dialog.open();
          tableTypeCBoxList.setModel(ShopTableTypeDAO.getInstance().findAll());
        }
      });
    }
  }
  
  public void initData() {
    int nxtTableNumber = ShopTableDAO.getInstance().getNextTableNumber();
    if (nxtTableNumber == 0) {
      nxtTableNumber = 1;
    }
    
    for (int i = 1; i <= nxtTableNumber + 1; i++) {
      ShopTable shopTable = ShopTableDAO.getInstance().get(Integer.valueOf(i));
      if (shopTable == null) {
        tfTableNo.setText(String.valueOf(i));
        break;
      }
    }
    tfTableCapacity.setText("4");
    tfTableDescription.setText("");
    tfTableName.setText("");
  }
  



  public void cancel() {}
  


  public void clearFields()
  {
    tfTableNo.setText("");
    tfTableCapacity.setText("");
    tfTableDescription.setText("");
    tfTableName.setText("");
    tableTypeCBoxList.unCheckAll();
    rbFree.setSelected(false);
    rbDisable.setSelected(false);
  }
  
  public boolean delete() {
    try {
      ShopTable bean2 = (ShopTable)getBean();
      if (bean2 == null) {
        return false;
      }
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), Messages.getString("ShopTableForm.14"), 
        Messages.getString("ShopTableForm.15"));
      if (option != 0) {
        return false;
      }
      
      List<BookingInfo> bookingList = BookingInfoDAO.getInstance().findAll();
      
      for (Iterator localIterator1 = bookingList.iterator(); localIterator1.hasNext();) { info = (BookingInfo)localIterator1.next();
        tableList = info.getTables();
        for (ShopTable shopTable : tableList)
          if (shopTable.getId().equals(bean2.getId())) {
            tableList.remove(shopTable);
            info.setTables(tableList);
            BookingInfoDAO.getInstance().saveOrUpdate(info);
            break;
          }
      }
      BookingInfo info;
      List<ShopTable> tableList;
      ShopTableDAO.getInstance().delete(bean2);
      
      tfTableCapacity.setText("");
      tfTableDescription.setText("");
      tfTableName.setText("");
      tfTableNo.setText("");
      tableTypeCBoxList.unCheckAll();
      
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e);
    }
    return false;
  }
  
  public boolean deleteAllTables()
  {
    List<ShopTable> list = ShopTableDAO.getInstance().findAll();
    
    if (list.isEmpty()) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), Messages.getString("ShopTableForm.51"));
      return false;
    }
    
    int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), Messages.getString("ShopTableForm.20"), 
      Messages.getString("ShopTableForm.21"));
    if (option != 0) {
      return false;
    }
    
    List<BookingInfo> bookingList = BookingInfoDAO.getInstance().findAll();
    
    for (BookingInfo info : bookingList) {
      info.setTables(null);
      BookingInfoDAO.getInstance().saveOrUpdate(info);
    }
    
    for (ShopTable table : list) {
      table.setFloor(null);
      table.setTypes(null);
      ShopTableDAO.getInstance().delete(table);
    }
    
    tfTableNo.setText("");
    tfTableCapacity.setText("");
    tfTableDescription.setText("");
    tfTableName.setText("");
    tableTypeCBoxList.unCheckAll();
    
    return true;
  }
  
  public void setFieldsEditable(boolean editable)
  {
    tfTableName.setEditable(editable);
    tfTableDescription.setEditable(editable);
    tfTableCapacity.setEditable(editable);
  }
  

  public void setFieldsEnable(boolean enable)
  {
    tableTypeCBoxList.setEnabled(enable);
    tableTypeCBoxList.clearSelection();
    

    tfTableName.setEnabled(enable);
    tfTableDescription.setEnabled(enable);
    tfTableCapacity.setEnabled(enable);
    
    btnCapacityOne.setEnabled(enable);
    btnCapacityTwo.setEnabled(enable);
    btnCapacityFour.setEnabled(enable);
    btnCapacitySix.setEnabled(enable);
    btnCapacityEight.setEnabled(enable);
    btnCapacityTen.setEnabled(enable);
    
    if (btnCreateType != null) {
      btnCreateType.setEnabled(enable);
    }
    
    rbFree.setEnabled(enable);
    rbDisable.setEnabled(enable);
  }
  
  public void setOnlyStatusEnable() {
    tfTableNo.setEditable(false);
    tfTableName.setEditable(false);
    tfTableDescription.setEditable(false);
    tfTableCapacity.setEditable(false);
    
    btnCapacityOne.setVisible(false);
    btnCapacityTwo.setVisible(false);
    btnCapacityFour.setVisible(false);
    btnCapacitySix.setVisible(false);
    btnCapacityEight.setVisible(false);
    btnCapacityTen.setVisible(false);
    
    tableTypeCBoxList.setEnabled(false);
    
    if (btnCreateType != null) {
      btnCreateType.setVisible(false);
    }
    
    rbFree.setEnabled(true);
    rbDisable.setEnabled(true);
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      ShopTable table = (ShopTable)getBean();
      ShopTableDAO.getInstance().saveOrUpdate(table);
      updateView();
      return true;
    }
    catch (IllegalModelStateException localIllegalModelStateException) {}catch (StaleStateException x)
    {
      BOMessageDialog.showError(this, Messages.getString("ShopTableForm.16"));
      return false;
    }
    return false;
  }
  
  protected void updateView()
  {
    ShopTable table = (ShopTable)getBean();
    
    if (table == null) {
      return;
    }
    
    if (table.getId() == null) {
      tfTableNo.setEnabled(true);
    }
    
    tableTypeCBoxList.setModel(ShopTableTypeDAO.getInstance().findAll());
    tableTypeCBoxList.selectItems(table.getTypes());
    
    tfTableNo.setText(String.valueOf(table.getTableNumber()));
    tfTableName.setText(table.getName());
    tfTableDescription.setText(table.getDescription());
    tfTableCapacity.setText(String.valueOf(table.getCapacity()));
    tfTableMinCapacity.setText(String.valueOf(table.getMinCapacity()));
    if (table.isReservable() != null) {
      chkReservable.setSelected(table.isReservable().booleanValue());
    }
    else {
      chkReservable.setSelected(true);
    }
    
    rbFree.setSelected(true);
    TableStatus tableStatus = table.getTableStatus();
    if (tableStatus == TableStatus.Disable) {
      rbDisable.setSelected(true);
    }
    
    if (table.getTableNumber() != null) {
      selectedTable = table.getTableNumber().intValue();
    }
    
    if (isDuplicateOn()) {
      List<ShopTableType> checkValues = tableTypeCBoxList.getCheckedValues();
      dupCheckValues = checkValues;
      dupCapacity = table.getCapacity();
      dupDescription = table.getDescription();
      dupName = table.getName();
      dupTableEnable = rbFree.isSelected();
      dupTableDisable = rbDisable.isSelected();
    }
  }
  


  protected boolean updateModel()
    throws IllegalModelStateException
  {
    ShopTable table = (ShopTable)getBean();
    
    if (table == null) {
      table = new ShopTable();
      setBean(table, false);
    }
    
    if ((!isDuplicateOn()) && (tfTableNo.getInteger() == 0)) {
      POSMessageDialog.showError(null, Messages.getString("ShopTableForm.57"));
      return false;
    }
    
    ShopTable tableTocheck = ShopTableDAO.getInstance().get(Integer.valueOf(tfTableNo.getInteger()));
    
    if ((tableTocheck != null) && (selectedTable != tableTocheck.getId().intValue())) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), Messages.getString("ShopTableForm.58"));
      return false;
    }
    
    if (isDuplicateOn())
    {
      table = new ShopTable();
      int nxtTableNumber = ShopTableDAO.getInstance().getNextTableNumber();
      
      for (int i = 1; i <= nxtTableNumber; i++) {
        ShopTable shopTable = ShopTableDAO.getInstance().get(Integer.valueOf(i));
        if (shopTable == null) {
          table.setId(Integer.valueOf(i));
          break;
        }
      }
      
      if (table.getId() == null) {
        table.setId(Integer.valueOf(nxtTableNumber + 1));
      }
      table.setTypes(dupCheckValues);
      table.setCapacity(dupCapacity);
      table.setDescription(dupDescription);
      table.setName(dupName);
      if (dupTableEnable) {
        table.setTableStatus(TableStatus.Available);
      }
      if (dupTableDisable) {
        table.setTableStatus(TableStatus.Disable);
      }
      
      table.setMinCapacity(Integer.valueOf(tfTableMinCapacity.getInteger()));
      table.setReservable(Boolean.valueOf(chkReservable.isSelected()));
      
      setDuplicate(false);
      setBean(table);
    }
    else
    {
      table.setId(Integer.valueOf(tfTableNo.getInteger()));
      table.setName(tfTableName.getText());
      table.setDescription(tfTableDescription.getText());
      table.setCapacity(Integer.valueOf(tfTableCapacity.getInteger()));
      
      List<ShopTableType> checkValues = tableTypeCBoxList.getCheckedValues();
      table.setTypes(checkValues);
      
      if (rbFree.isSelected()) {
        table.setTableStatus(TableStatus.Available);
      }
      else if (rbDisable.isSelected()) {
        table.setTableStatus(TableStatus.Disable);
      }
      
      table.setMinCapacity(Integer.valueOf(tfTableMinCapacity.getInteger()));
      table.setReservable(Boolean.valueOf(chkReservable.isSelected()));
    }
    
    setNewTable(table.getId().intValue());
    
    return true;
  }
  
  public void edit()
  {
    System.out.println("");
  }
  

  public String getDisplayText()
  {
    if (((ShopTable)getBean()).getId() == null) {
      return Messages.getString("ShopTableForm.18");
    }
    
    return "Edit Table";
  }
  
  public void setTableTypeCBoxListEnable(boolean enable)
  {
    tableTypeCBoxList.setEnabled(enable);
  }
  
  public boolean isDuplicateOn() {
    return duplicate;
  }
  
  public void setDuplicate(boolean duplicate) {
    this.duplicate = duplicate;
  }
  


  public int getNewTable()
  {
    return newTable;
  }
  


  public void setNewTable(int newTable)
  {
    this.newTable = newTable;
  }
}
