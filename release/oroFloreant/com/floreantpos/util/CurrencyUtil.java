package com.floreantpos.util;

import com.floreantpos.model.Currency;
import com.floreantpos.model.dao.CurrencyDAO;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;















public class CurrencyUtil
{
  private static Currency mainCurrency;
  private static List<Currency> auxiliaryCurrencyList;
  
  public CurrencyUtil() {}
  
  public static void populateCurrency()
  {
    auxiliaryCurrencyList = new ArrayList();
    
    List<Currency> currencyList = CurrencyDAO.getInstance().findAll();
    
    if ((currencyList != null) && (currencyList.size() > 0)) {
      for (Currency currency : currencyList) {
        if (currency.isMain().booleanValue()) {
          mainCurrency = currency;
        }
        else {
          auxiliaryCurrencyList.add(currency);
        }
      }
      if (mainCurrency == null) {
        mainCurrency = (Currency)currencyList.get(0);
        auxiliaryCurrencyList.remove(mainCurrency);
      }
    }
    else {
      Currency currency = new Currency();
      currency.setName("USD");
      currency.setSymbol("$");
      currency.setExchangeRate(Double.valueOf(1.0D));
      currency.setMain(Boolean.valueOf(true));
      CurrencyDAO.getInstance().save(currency);
      mainCurrency = currency;
    }
  }
  
  public static Currency getMainCurrency() {
    return mainCurrency;
  }
  
  public static List<Currency> getAuxiliaryCurrencyList() {
    return auxiliaryCurrencyList;
  }
  
  public static List<Currency> getAllCurrency() {
    List<Currency> currencyList = new ArrayList();
    currencyList.add(mainCurrency);
    if (auxiliaryCurrencyList != null) {
      Collections.sort(auxiliaryCurrencyList, new Comparator()
      {
        public int compare(Currency curr1, Currency curr2) {
          return curr1.getName().compareTo(curr2.getName());
        }
      });
      currencyList.addAll(auxiliaryCurrencyList);
    }
    return currencyList;
  }
  
  public static String getCurrencyName() {
    String currencyName = null;
    if (mainCurrency != null) {
      currencyName = mainCurrency.getName();
    }
    else {
      currencyName = "USD";
    }
    return currencyName;
  }
  
  public static String getCurrencySymbol() {
    String currencySymbol = null;
    if (mainCurrency != null) {
      currencySymbol = mainCurrency.getSymbol();
    }
    else {
      currencySymbol = "$";
    }
    return currencySymbol;
  }
}
