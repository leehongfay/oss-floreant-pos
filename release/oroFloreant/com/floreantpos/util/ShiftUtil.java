package com.floreantpos.util;

import com.floreantpos.model.DayPart;
import com.floreantpos.model.MenuShift;
import com.floreantpos.model.PriceShift;
import com.floreantpos.model.Shift;
import com.floreantpos.model.dao.DayPartDAO;
import com.floreantpos.model.util.DataProvider;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;



















public class ShiftUtil
{
  private static final String DEFAULT_SHIFT = "DEFAULT SHIFT";
  private static final Calendar calendar = ;
  private static final Calendar calendar2 = Calendar.getInstance();
  private static final NumberFormat format = new DecimalFormat("00");
  private static Shift currentShift;
  
  static
  {
    calendar.clear();
  }
  
  public static void reInitialize() {
    calendar.clear();
  }
  





  public static Date formatShiftTime(Date shiftTime)
  {
    calendar.clear();
    calendar2.setTime(shiftTime);
    
    calendar.set(10, calendar2.get(10));
    calendar.set(12, calendar2.get(12));
    calendar.set(9, calendar2.get(9));
    
    return calendar.getTime();
  }
  
  public static Date buildShiftStartTime(int startHour, int startMin, int startAmPm, int endHour, int endMin, int endAmPm) {
    startHour = startHour == 12 ? 0 : startHour;
    
    calendar.clear();
    
    calendar.set(10, startHour);
    calendar.set(12, startMin);
    
    calendar.set(9, startAmPm);
    
    return calendar.getTime();
  }
  
  public static Date buildShiftEndTime(int startHour, int startMin, int startAmPm, int endHour, int endMin, int endAmPm) {
    endHour = endHour == 12 ? 0 : endHour;
    
    calendar.clear();
    
    calendar.set(10, endHour);
    calendar.set(12, endMin);
    
    calendar.set(9, endAmPm);
    
    if ((startAmPm == 1) && (endAmPm == 0)) {
      calendar.add(5, 1);
    }
    
    return calendar.getTime();
  }
  
  public static Date buildShiftWithoutAmPm(int hour, int min, int sec) {
    Calendar calendar = Calendar.getInstance();
    calendar.clear();
    
    calendar.set(11, hour);
    calendar.set(12, min);
    calendar.set(13, sec);
    
    return calendar.getTime();
  }
  
  public static String buildShiftTimeRepresentation(Date shiftTime) {
    calendar.setTime(shiftTime);
    
    String s = "";
    s = format.format(calendar.get(10) == 0 ? 12L : calendar.get(10));
    s = s + ":" + format.format(calendar.get(12));
    s = s + (calendar.get(9) == 0 ? " AM" : " PM");
    return s;
  }
  
  public static Shift getCurrentShift() {
    Calendar calendar = Calendar.getInstance();
    if ((currentShift != null) && (calendar.before(currentShift.getEndTime()))) {
      return currentShift;
    }
    Calendar calendar2 = Calendar.getInstance();
    calendar.clear();
    
    calendar.set(10, calendar2.get(10));
    calendar.set(12, calendar2.get(12));
    calendar.set(9, calendar2.get(9));
    
    Date currentTime = calendar.getTime();
    List<DayPart> daryPartShifts = DataProvider.get().getDaryPartShifts();
    currentShift = findCurrentShift(currentTime, daryPartShifts);
    if (currentShift != null) {
      return currentShift;
    }
    
    calendar.add(5, 1);
    currentTime = calendar.getTime();
    
    currentShift = findCurrentShift(currentTime, daryPartShifts);
    if (currentShift != null) {
      return currentShift;
    }
    currentShift = getDefaultShift();
    return currentShift;
  }
  
  private static Shift findCurrentShift(Date currentTime, List<? extends Shift> shifts) {
    Shift smallestShift = null;
    if (shifts == null) {
      return null;
    }
    for (Shift shift : shifts) {
      Date startTime = new Date(shift.getStartTime().getTime());
      Date endTime = new Date(shift.getEndTime().getTime());
      
      if ((currentTime.after(startTime)) && (currentTime.before(endTime))) {
        if ((smallestShift != null) && (shift.getShiftLength().longValue() < smallestShift.getShiftLength().longValue())) {
          smallestShift = shift;
        }
        else if (smallestShift == null) {
          smallestShift = shift;
        }
      }
    }
    
    return smallestShift;
  }
  
  private static Shift getDefaultShift() {
    Calendar calendar = Calendar.getInstance();
    Calendar calendar2 = Calendar.getInstance();
    
    calendar.clear();
    calendar.set(10, 0);
    calendar.set(12, 0);
    calendar.set(13, 0);
    
    calendar2.clear();
    calendar2.set(10, 23);
    calendar2.set(12, 59);
    calendar2.set(13, 59);
    
    DayPart defaultShift = DayPartDAO.getInstance().findByName("DEFAULT SHIFT");
    if (defaultShift == null) {
      defaultShift = new DayPart();
      defaultShift.setName("DEFAULT SHIFT");
      defaultShift.setStartTime(calendar.getTime());
      defaultShift.setEndTime(calendar2.getTime());
      defaultShift.setShiftLength(Long.valueOf(calendar2.getTimeInMillis() - calendar.getTimeInMillis()));
      
      DayPartDAO.getInstance().saveOrUpdate(defaultShift);
    }
    
    return defaultShift;
  }
  
  public static List<PriceShift> getCurrentPricShifts() {
    List<PriceShift> priceShifts = DataProvider.get().getPriceShifts();
    if ((priceShifts == null) || (priceShifts.isEmpty())) {
      return null;
    }
    Calendar calendar = Calendar.getInstance();
    Calendar calendar2 = Calendar.getInstance();
    calendar.clear();
    
    calendar.set(10, calendar2.get(10));
    calendar.set(12, calendar2.get(12));
    calendar.set(9, calendar2.get(9));
    
    Date currentTime = calendar.getTime();
    List<PriceShift> currentPriceShifts = new ArrayList();
    for (PriceShift shift : priceShifts) {
      Date startTime = new Date(shift.getStartTime().getTime());
      Date endTime = new Date(shift.getEndTime().getTime());
      
      String daysOfWeek = shift.getDaysOfWeek();
      if ((daysOfWeek != null) && (daysOfWeek.contains(String.valueOf(calendar2.get(7)))) && (currentTime.after(startTime)) && 
        (currentTime.before(endTime))) {
        currentPriceShifts.add(shift);
      }
    }
    return currentPriceShifts;
  }
  
  public static MenuShift getCurrentMenuShift() {
    List<MenuShift> menuShifts = DataProvider.get().getMenuShifts();
    if ((menuShifts == null) || (menuShifts.isEmpty())) {
      return null;
    }
    Calendar calendar = Calendar.getInstance();
    Calendar calendar2 = Calendar.getInstance();
    calendar.clear();
    
    calendar.set(10, calendar2.get(10));
    calendar.set(12, calendar2.get(12));
    calendar.set(9, calendar2.get(9));
    
    Date currentTime = calendar.getTime();
    List<MenuShift> currentMenuShifts = new ArrayList();
    for (MenuShift shift : menuShifts) {
      Date startTime = new Date(shift.getStartTime().getTime());
      Date endTime = new Date(shift.getEndTime().getTime());
      
      String daysOfWeek = shift.getDaysOfWeek();
      if ((daysOfWeek != null) && (daysOfWeek.contains(String.valueOf(calendar2.get(7)))) && (currentTime.after(startTime)) && 
        (currentTime.before(endTime))) {
        currentMenuShifts.add(shift);
      }
    }
    
    return (MenuShift)findCurrentShift(currentTime, currentMenuShifts);
  }
  















































  public static Date buildStartTime(int startHour, int startMin, int endHour, int endMin)
  {
    calendar.clear();
    
    calendar.set(11, startHour);
    calendar.set(12, startMin);
    
    return calendar.getTime();
  }
  
  public static Date buildEndTime(int startHour, int startMin, int endHour, int endMin) {
    calendar.clear();
    
    calendar.set(11, endHour);
    calendar.set(12, endMin);
    
    return calendar.getTime();
  }
  
  public ShiftUtil() {}
}
