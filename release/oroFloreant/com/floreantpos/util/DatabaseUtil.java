package com.floreantpos.util;

import com.floreantpos.PosLog;
import com.floreantpos.db.update.UpdateDBTo101;
import com.floreantpos.db.update.UpdateDBTo104;
import com.floreantpos.db.update.UpdateDBTo122;
import com.floreantpos.main.Application;
import com.floreantpos.model.Attribute;
import com.floreantpos.model.Store;
import com.floreantpos.model.dao.StoreDAO;
import com.floreantpos.model.dao._RootDAO;
import java.sql.DriverManager;
import java.util.EnumSet;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataBuilder;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;
import org.hibernate.tool.schema.TargetType;



















public class DatabaseUtil
{
  private static Log logger = LogFactory.getLog(DatabaseUtil.class);
  






  static Attribute smallSize;
  






  static Attribute mediumSize;
  






  static Attribute largeSize;
  






  public static final int DATABASE_VERSION = 123;
  







  public DatabaseUtil() {}
  







  public static void checkConnection(String connectionString, String hibernateDialect, String hibernateConnectionDriverClass, String user, String password)
    throws DatabaseConnectionException
  {
    
    





    try
    {
      Class.forName(hibernateConnectionDriverClass);
      DriverManager.getConnection(connectionString, user, password);
    }
    catch (Exception e) {
      throw new DatabaseConnectionException("Could not connect to database. " + e.getMessage());
    }
  }
  
  public static void initialize() throws DatabaseConnectionException
  {}
  
  public static void initialize(String hibernanetCfgFile) throws DatabaseConnectionException
  {
    _RootDAO.initialize(hibernanetCfgFile);
  }
  
  public static void initialize(String hibernanetCfgFile, String hibernateDialectClass, String driverClass, String connectString, String databaseUser, String databasePassword)
  {
    _RootDAO.initialize(hibernanetCfgFile, hibernateDialectClass, driverClass, connectString, databaseUser, databasePassword);
  }
  
  public static void initialize(String hibernanetCfgFile, Map<String, String> properties) {
    _RootDAO.initialize(hibernanetCfgFile, properties);
  }
  
  public static void initialize(Map<String, String> properties) {
    _RootDAO.initialize(properties);
  }
  
  public static boolean createDatabase(String connectionString, String hibernateDialect, String hibernateConnectionDriverClass, String user, String password, boolean exportSampleData)
  {
    return createDatabase(Application.getInstance().getHibernateConfigurationFileName(), connectionString, hibernateDialect, hibernateConnectionDriverClass, user, password, exportSampleData);
  }
  
  public static boolean createDatabase(String hibernateCfgFileName, String connectionString, String hibernateDialect, String hibernateConnectionDriverClass, String user, String password, boolean exportSampleData)
  {
    try
    {
      _RootDAO.releaseConnection();
      
      StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder().configure(hibernateCfgFileName);
      registryBuilder.applySetting("hibernate.dialect", hibernateDialect);
      registryBuilder.applySetting("hibernate.connection.driver_class", hibernateConnectionDriverClass);
      registryBuilder.applySetting("hibernate.connection.url", connectionString);
      registryBuilder.applySetting("hibernate.connection.username", user);
      registryBuilder.applySetting("hibernate.connection.password", password);
      registryBuilder.applySetting("hibernate.hbm2ddl.auto", "create");
      registryBuilder.applySetting("hibernate.c3p0.checkoutTimeout", "0");
      registryBuilder.applySetting("hibernate.cache.use_second_level_cache", "false");
      
      StandardServiceRegistry standardRegistry = registryBuilder.build();
      Metadata metaData = new MetadataSources(standardRegistry).getMetadataBuilder().build();
      
      SchemaExport schemaExport = new SchemaExport();
      EnumSet<TargetType> enumSet = EnumSet.of(TargetType.DATABASE);
      schemaExport.create(enumSet, metaData);
      
      _RootDAO.setSessionFactory(metaData.buildSessionFactory());
      
      DefaultDataInserter dataInserter = new DefaultDataInserter();
      dataInserter.insertDefaultData(123);
      if (!exportSampleData) {
        return true;
      }
      dataInserter.createSampleData();
      
      StandardServiceRegistryBuilder.destroy(standardRegistry);
      return true;
    } catch (Exception e) {
      PosLog.error(DatabaseUtil.class, e);
      logger.error(e); }
    return false;
  }
  















  public static void updateLegacyDatabase() {}
  














  public static boolean updateDatabase(String connectionString, String hibernateDialect, String hibernateConnectionDriverClass, String user, String password)
  {
    return updateDatabase(Application.getInstance().getHibernateConfigurationFileName(), connectionString, hibernateDialect, hibernateConnectionDriverClass, user, password);
  }
  
  public static boolean updateDatabase(String hibernateCfgFileName, String connectionString, String hibernateDialect, String hibernateConnectionDriverClass, String user, String password)
  {
    try
    {
      _RootDAO.releaseConnection();
      
      StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder().configure(hibernateCfgFileName);
      registryBuilder.applySetting("hibernate.dialect", hibernateDialect);
      registryBuilder.applySetting("hibernate.connection.driver_class", hibernateConnectionDriverClass);
      registryBuilder.applySetting("hibernate.connection.url", connectionString);
      registryBuilder.applySetting("hibernate.connection.username", user);
      registryBuilder.applySetting("hibernate.connection.password", password);
      registryBuilder.applySetting("hibernate.max_fetch_depth", "3");
      registryBuilder.applySetting("hibernate.connection.isolation", String.valueOf(1));
      registryBuilder.applySetting("hibernate.cache.use_second_level_cache", "false");
      
      StandardServiceRegistry standardRegistry = registryBuilder.build();
      Metadata metaData = new MetadataSources(standardRegistry).getMetadataBuilder().build();
      
      SchemaUpdate schemaUpdate = new SchemaUpdate();
      EnumSet<TargetType> enumSet = EnumSet.of(TargetType.DATABASE);
      schemaUpdate.execute(enumSet, metaData);
      
      SessionFactory sessionFactory = metaData.buildSessionFactory();
      _RootDAO.setSessionFactory(sessionFactory);
      
      Store store = StoreDAO.getRestaurant();
      Integer databaseVersion = store.getDatabaseVersion();
      
      if ((databaseVersion == null) || (databaseVersion.intValue() < 101)) {
        UpdateDBTo101 dataConvertionUtil = new UpdateDBTo101(connectionString, user, password, sessionFactory);
        dataConvertionUtil.update();
      }
      if ((databaseVersion == null) || (databaseVersion.intValue() < 104)) {
        UpdateDBTo104 dataConvertionUtil = new UpdateDBTo104();
        dataConvertionUtil.update();
      }
      if ((databaseVersion == null) || (databaseVersion.intValue() < 122)) {
        UpdateDBTo122 dataConvertionUtil = new UpdateDBTo122();
        dataConvertionUtil.update();
      }
      
      store.setDatabaseVersion(123);
      StoreDAO.getInstance().update(store);
      
      StandardServiceRegistryBuilder.destroy(standardRegistry);
      return true;
    } catch (Exception e) {
      PosLog.error(DatabaseUtil.class, e);
      logger.error(e); }
    return false;
  }
  
  public static boolean isDbUpdateNeeded()
  {
    Store store = StoreDAO.getRestaurant();
    Integer databaseVersion = store.getDatabaseVersion();
    if ((databaseVersion != null) && (databaseVersion.intValue() >= 123)) {
      return false;
    }
    return true;
  }
}
