package com.floreantpos.util;

import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketDiscount;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemDiscount;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class DiscountUtil
{
  public DiscountUtil() {}
  
  public static Double calculateDiscountAmount(TicketItemDiscount ticketItemDiscount, double subtotalAmount)
  {
    TicketItem ticketItem = ticketItemDiscount.getTicketItem();
    
    if (ticketItemDiscount.getType().intValue() == 2) {
      return ticketItemDiscount.getValue();
    }
    if (ticketItemDiscount.getMinimumAmount().doubleValue() > 1.0D) {
      double minQuantity = ticketItemDiscount.getMinimumAmount().doubleValue();
      double itemCount = ticketItem.getQuantity().doubleValue();
      
      switch (ticketItemDiscount.getType().intValue()) {
      case 0: 
        return Double.valueOf(Math.floor(itemCount / minQuantity) * ticketItemDiscount.getValue().doubleValue());
      
      case 1: 
        return Double.valueOf(Math.floor(itemCount / minQuantity) * (subtotalAmount / itemCount * ticketItemDiscount.getValue().doubleValue() / 100.0D));
      }
    }
    switch (ticketItemDiscount.getType().intValue()) {
    case 0: 
      return ticketItemDiscount.getValue();
    
    case 1: 
      return Double.valueOf(subtotalAmount * ticketItemDiscount.getValue().doubleValue() / 100.0D);
    }
    
    return Double.valueOf(0.0D);
  }
  
  public static BigDecimal calculateDiscountAmount(TicketItemDiscount ticketItemDiscount, BigDecimal subtotalAmount)
  {
    TicketItem ticketItem = ticketItemDiscount.getTicketItem();
    BigDecimal discountValue = NumberUtil.convertToBigDecimal(ticketItemDiscount.getValue().doubleValue());
    BigDecimal percentage = discountValue.divide(NumberUtil.convertToBigDecimal("100.0"), 4, RoundingMode.FLOOR);
    
    if (ticketItemDiscount.getType().intValue() == 2) {
      return discountValue;
    }
    if (ticketItemDiscount.getMinimumAmount().doubleValue() > 1.0D) {
      BigDecimal minQuantity = NumberUtil.convertToBigDecimal(ticketItemDiscount.getMinimumAmount().doubleValue());
      BigDecimal itemCount = NumberUtil.convertToBigDecimal(ticketItem.getQuantity().doubleValue());
      
      switch (ticketItemDiscount.getType().intValue())
      {

      case 0: 
        return itemCount.divide(minQuantity, 4, RoundingMode.FLOOR).multiply(discountValue);
      
      case 1: 
        BigDecimal itemCountDivideQuantity = itemCount.divide(minQuantity, 4, RoundingMode.FLOOR);
        BigDecimal subtotalDivideItemCount = subtotalAmount.divide(itemCount, 4, RoundingMode.FLOOR);
        

        return itemCountDivideQuantity.multiply(subtotalDivideItemCount).multiply(percentage); }
      
    }
    switch (ticketItemDiscount.getType().intValue()) {
    case 0: 
      return discountValue;
    
    case 1: 
      return subtotalAmount.multiply(percentage);
    }
    
    return new BigDecimal("0");
  }
  

  public static Double calculateDiscountAmount(double price, TicketDiscount discount)
  {
    switch (discount.getType().intValue()) {
    case 0: 
    case 2: 
      return discount.getValue();
    
    case 1: 
      return Double.valueOf(price * discount.getValue().doubleValue() / 100.0D);
    }
    
    return Double.valueOf(price * discount.getValue().doubleValue() / 100.0D);
  }
  
  public static TicketItemDiscount getMaxDiscount(List<TicketItemDiscount> discounts) {
    if ((discounts == null) || (discounts.isEmpty())) {
      return null;
    }
    
    TicketItemDiscount maxDiscount = (TicketItemDiscount)java.util.Collections.max(discounts, new java.util.Comparator()
    {
      public int compare(TicketItemDiscount o1, TicketItemDiscount o2) {
        return (int)(o1.getSubTotalAmountDisplay().doubleValue() - o2.getSubTotalAmountDisplay().doubleValue());
      }
      
    });
    return maxDiscount;
  }
  
  public static TicketDiscount getMaxDiscount(List<TicketDiscount> discounts, double price) {
    if ((discounts == null) || (discounts.isEmpty())) {
      return null;
    }
    
    TicketDiscount maxDiscount = (TicketDiscount)java.util.Collections.max(discounts, new java.util.Comparator()
    {
      public int compare(TicketDiscount o1, TicketDiscount o2) {
        return (int)(DiscountUtil.calculateDiscountAmount(val$price, o1).doubleValue() - DiscountUtil.calculateDiscountAmount(val$price, o2).doubleValue());
      }
      
    });
    return maxDiscount;
  }
  
  public static Double calculateRepriceDiscount(Ticket ticket, double repriceValue)
  {
    Double ticketTotalAmount = ticket.getTotalAmount();
    Double ticketTaxAmount = ticket.getTaxAmount();
    if (ticketTotalAmount.doubleValue() == 0.0D) {
      return Double.valueOf(0.0D);
    }
    
    double repricedTaxAmount = repriceValue * ticketTaxAmount.doubleValue() / ticketTotalAmount.doubleValue();
    Double discountValue = Double.valueOf(ticket.getSubtotalAmount().doubleValue() - ticket.getItemDiscountAmount() - (repriceValue - repricedTaxAmount));
    return discountValue;
  }
}
