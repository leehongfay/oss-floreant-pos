package com.floreantpos.util.datamigrate;

import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.InventoryUnitGroup;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.ModifierGroup;
import com.floreantpos.model.ModifierMultiplierPrice;
import com.floreantpos.model.Multiplier;
import com.floreantpos.model.Tax;
import com.floreantpos.model.TaxGroup;
import com.floreantpos.model.User;
import com.floreantpos.model.UserType;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

























@XmlRootElement(name="elements")
public class Elements
{
  List<TaxGroup> taxGroups;
  List<Tax> taxes;
  List<InventoryUnitGroup> unitGroups;
  List<InventoryUnit> units;
  List<MenuCategory> menuCategories;
  List<MenuGroup> menuGroups;
  List<MenuModifier> menuModifiers;
  List<MenuItemModifierSpec> menuItemModifierGroups;
  List<ModifierGroup> modifierGroups;
  List<MenuItem> menuItems;
  List<Multiplier> multipliers;
  List<ModifierMultiplierPrice> multiplierPriceList;
  List<User> users;
  List<UserType> userTypes;
  
  public Elements() {}
  
  public void setMultipliers(List<Multiplier> multipliers)
  {
    this.multipliers = multipliers;
  }
  
  public List<Multiplier> getMultipliers() {
    return multipliers;
  }
  
  public List<ModifierMultiplierPrice> getMultiplierPriceList() {
    return multiplierPriceList;
  }
  
  public void setMultiplierPriceList(List<ModifierMultiplierPrice> multiplierPriceList) {
    this.multiplierPriceList = multiplierPriceList;
  }
  
  public List<MenuCategory> getMenuCategories() {
    return menuCategories;
  }
  
  public void setMenuCategories(List<MenuCategory> menuCategories) {
    this.menuCategories = menuCategories;
  }
  
  public List<MenuGroup> getMenuGroups() {
    return menuGroups;
  }
  
  public void setMenuGroups(List<MenuGroup> menuGroups) {
    this.menuGroups = menuGroups;
  }
  
  public List<MenuModifier> getMenuModifiers() {
    return menuModifiers;
  }
  
  public void setMenuModifiers(List<MenuModifier> menuModifiers) {
    this.menuModifiers = menuModifiers;
    
    if (menuModifiers == null) {}
  }
  







  public List<ModifierGroup> getModifierGroups()
  {
    return modifierGroups;
  }
  
  public void setModifierGroups(List<ModifierGroup> menuModifierGroups) {
    modifierGroups = menuModifierGroups;
  }
  
  public List<MenuItem> getMenuItems() {
    return menuItems;
  }
  
  public void setMenuItems(List<MenuItem> menuItems) {
    this.menuItems = menuItems;
  }
  
  public List<User> getUsers() {
    return users;
  }
  
  public void setUsers(List<User> users) {
    this.users = users;
  }
  
  public List<Tax> getTaxes() {
    return taxes;
  }
  
  public void setTaxes(List<Tax> taxes) {
    this.taxes = taxes;
  }
  
  public List<TaxGroup> getTaxGroups() {
    return taxGroups;
  }
  
  public void setTaxGroups(List<TaxGroup> taxGroups) {
    this.taxGroups = taxGroups;
  }
  
  public List<InventoryUnitGroup> getUnitGroups() {
    return unitGroups;
  }
  
  public void setUnitGroups(List<InventoryUnitGroup> unitGroups) {
    this.unitGroups = unitGroups;
  }
  
  public List<InventoryUnit> getUnits() {
    return units;
  }
  
  public void setUnits(List<InventoryUnit> units) {
    this.units = units;
  }
  
  public List<MenuItemModifierSpec> getMenuItemModifierGroups() {
    return menuItemModifierGroups;
  }
  
  public void setMenuItemModifierGroups(List<MenuItemModifierSpec> menuItemModifierGroups) {
    this.menuItemModifierGroups = menuItemModifierGroups;
  }
  
  public List<UserType> getUserTypes() { return userTypes; }
  
  public void setUserTypes(List<UserType> userTypes)
  {
    this.userTypes = userTypes;
  }
}
