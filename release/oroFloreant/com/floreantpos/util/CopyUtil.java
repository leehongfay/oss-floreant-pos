package com.floreantpos.util;

import com.floreantpos.model.MenuItem;
import java.io.ObjectOutputStream;

public class CopyUtil
{
  public CopyUtil() {}
  
  public static MenuItem copy(MenuItem source) throws Exception
  {
    return source.clone();
  }
  
  public static Object deepCopy(Object input) throws Exception {
    Object output = null;
    try
    {
      java.io.ByteArrayOutputStream byteArrayOutputStream = new java.io.ByteArrayOutputStream();
      ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
      objectOutputStream.writeObject(input);
      

      java.io.ByteArrayInputStream byteArrayInputStream = new java.io.ByteArrayInputStream(byteArrayOutputStream.toByteArray());
      java.io.ObjectInputStream objectInputStream = new java.io.ObjectInputStream(byteArrayInputStream);
      output = objectInputStream.readObject();
    }
    catch (Exception e) {
      throw e;
    }
    return output;
  }
}
