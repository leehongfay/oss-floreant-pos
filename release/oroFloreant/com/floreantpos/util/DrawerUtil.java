package com.floreantpos.util;

import com.floreantpos.PosLog;
import com.floreantpos.config.TerminalConfig;
import jssc.SerialPort;
import jssc.SerialPortException;





















public class DrawerUtil
{
  private boolean escp24pin;
  private static int MAX_ADVANCE_9PIN = 216;
  private static int MAX_ADVANCE_24PIN = 180;
  private static int MAX_UNITS = 127;
  
  private static final float CM_PER_INCH = 2.54F;
  
  private static final char ESC = '\033';
  
  private static final char AT = '@';
  private static final char LINE_FEED = '\n';
  private static final char PARENTHESIS_LEFT = '(';
  private static final char BACKSLASH = '\\';
  private static final char CR = '\r';
  private static final char TAB = '\t';
  private static final char FF = '\f';
  private static final char g = 'g';
  private static final char p = 'p';
  private static final char t = 't';
  private static final char l = 'l';
  private static final char x = 'x';
  private static final char E = 'E';
  private static final char F = 'F';
  private static final char J = 'J';
  private static final char P = 'P';
  private static final char Q = 'Q';
  private static final char $ = '$';
  private static final char ARGUMENT_0 = '\000';
  private static final char ARGUMENT_1 = '\001';
  private static final char ARGUMENT_2 = '\002';
  private static final char ARGUMENT_3 = '\003';
  private static final char ARGUMENT_4 = '\004';
  private static final char ARGUMENT_5 = '\005';
  private static final char ARGUMENT_6 = '\006';
  private static final char ARGUMENT_7 = '\007';
  private static final char ARGUMENT_25 = '\031';
  private static final char TEAR = 'i';
  private static final char _0 = '\000';
  private static final char _1 = '\001';
  private static final char _25 = '\031';
  private static final char _250 = 'ú';
  private static final char _999 = 'ϧ';
  private static final char _48 = '0';
  private static final char _55 = '7';
  private static final char _121 = 'y';
  public static final char USA = '\001';
  public static final char BRAZIL = '\031';
  public static SerialPort serialPort;
  private static char[] controlCodes;
  
  public DrawerUtil() {}
  
  public void close()
  {
    try
    {
      serialPort.closePort();
    }
    catch (SerialPortException localSerialPortException) {}
  }
  
  public static boolean initialize()
  {
    try {
      serialPort.openPort();
      serialPort.setParams(9600, 8, 1, 0);
      













      setCharacterSet('\001');
    }
    catch (SerialPortException ex) {
      return false;
    }
    
    return true;
  }
  
  public static boolean printToThePort()
  {
    try {
      serialPort.openPort();
      serialPort.setParams(9600, 8, 1, 0);
      



      print('\033');
      print('@');
      







      setCharacterSet('\001');
    }
    catch (SerialPortException ex) {
      return false;
    }
    
    return true;
  }
  
  public static void select10CPI() {
    print('\033');
    print('P');
  }
  
  public static void select15CPI() {
    print('\033');
    print('g');
  }
  
  public static void selectDraftPrinting() {
    print('\033');
    print('x');
    print('0');
  }
  
  public static void selectLQPrinting() {
    print('\033');
    print('x');
    print('1');
  }
  
  public static void setCharacterSet(char charset)
  {
    print('\033');
    print('(');
    print('t');
    print('\003');
    print('\000');
    print('\001');
    print(charset);
    print('\000');
    

    print('\033');
    print('t');
    print('\001');
  }
  
  public static void lineFeed()
  {
    print('\r');
    print('\n');
  }
  
  public static void formFeed()
  {
    print('\r');
    print('\f');
  }
  
  public static void bold(boolean bold) {
    print('\033');
    if (bold) {
      print('E');
    } else
      print('F');
  }
  
  public static void tear() {
    print('\033');
    print('i');
  }
  
  public static void kick() {
    print("Start kicking drawer\n");
    









    if (controlCodes.length >= 5) {
      print(controlCodes[0]);
      print(controlCodes[1]);
      print(controlCodes[2]);
      print(controlCodes[3]);
      print(controlCodes[4]);
    }
    else {
      print('\033');
      print('p');
      print('\000');
      print('\031');
      print('ú');
    }
    



















    print("End kicking drawer\n");
  }
  
  public static void proportionalMode(boolean proportional) {
    print('\033');
    print('p');
    if (proportional) {
      print('1');
    } else {
      print('0');
    }
  }
  


  public static void advanceVertical(float centimeters) {}
  

  public static void advanceHorizontal(float centimeters)
  {
    float inches = centimeters / 2.54F;
    int units_low = (int)(inches * 120.0F) % 256;
    int units_high = (int)(inches * 120.0F) / 256;
    
    print('\033');
    print('\\');
    print((char)units_low);
    print((char)units_high);
  }
  

  public static void setAbsoluteHorizontalPosition(float centimeters)
  {
    float inches = centimeters / 2.54F;
    int units_low = (int)(inches * 60.0F) % 256;
    int units_high = (int)(inches * 60.0F) / 256;
    
    print('\033');
    print('$');
    print((char)units_low);
    print((char)units_high);
  }
  

  public static void horizontalTab(int tabs)
  {
    for (int i = 0; i < tabs; i++) {
      print('\t');
    }
  }
  

  public static void setMargins(int columnsLeft, int columnsRight)
  {
    print('\033');
    print('l');
    print((char)columnsLeft);
    

    print('\033');
    print('Q');
    print((char)columnsRight);
  }
  
  public static void print(String text) {
    try {
      serialPort.writeBytes(text.getBytes());
    }
    catch (SerialPortException localSerialPortException) {}
  }
  
  public static void print(char text)
  {
    try
    {
      serialPort.writeByte((byte)text);
    }
    catch (SerialPortException localSerialPortException) {}
  }
  
  public static boolean isInitialized()
  {
    return true;
  }
  
  public static String getShare()
  {
    return "";
  }
  
  public static void kickDrawer() {
    try {
      String portName = TerminalConfig.getDrawerPortName();
      char[] codesArray = TerminalConfig.getDrawerControlCodesArray();
      
      kickDrawer(portName, codesArray);
    }
    catch (Exception localException) {}
  }
  
  public static void kickDrawer(String portName, char[] codes)
  {
    controlCodes = codes;
    serialPort = new SerialPort(portName);
    
    initialize();
    try
    {
      kick();
      

      serialPort.closePort();
    }
    catch (SerialPortException localSerialPortException) {}
  }
  
  public static void setCustomerDisplayMessage(String portName, String customerDisplayMessage) {
    serialPort = new SerialPort(portName);
    

    initialize();
    try {
      print('\f');
      serialPort.writeBytes(customerDisplayMessage.getBytes());
      
      serialPort.closePort();
    }
    catch (SerialPortException localSerialPortException) {}
  }
  
  /* Error */
  public static void setItemDisplay(String port, String message)
  {
    // Byte code:
    //   0: new 7	jssc/SerialPort
    //   3: dup
    //   4: aload_0
    //   5: invokespecial 26	jssc/SerialPort:<init>	(Ljava/lang/String;)V
    //   8: putstatic 3	com/floreantpos/util/DrawerUtil:serialPort	Ljssc/SerialPort;
    //   11: invokestatic 27	com/floreantpos/util/DrawerUtil:initialize	()Z
    //   14: pop
    //   15: bipush 12
    //   17: invokestatic 10	com/floreantpos/util/DrawerUtil:print	(C)V
    //   20: aload_1
    //   21: invokestatic 12	com/floreantpos/util/DrawerUtil:print	(Ljava/lang/String;)V
    //   24: getstatic 3	com/floreantpos/util/DrawerUtil:serialPort	Ljssc/SerialPort;
    //   27: invokevirtual 4	jssc/SerialPort:closePort	()Z
    //   30: pop
    //   31: goto +37 -> 68
    //   34: astore_2
    //   35: goto +33 -> 68
    //   38: astore_2
    //   39: getstatic 3	com/floreantpos/util/DrawerUtil:serialPort	Ljssc/SerialPort;
    //   42: invokevirtual 4	jssc/SerialPort:closePort	()Z
    //   45: pop
    //   46: goto +22 -> 68
    //   49: astore_2
    //   50: goto +18 -> 68
    //   53: astore_3
    //   54: getstatic 3	com/floreantpos/util/DrawerUtil:serialPort	Ljssc/SerialPort;
    //   57: invokevirtual 4	jssc/SerialPort:closePort	()Z
    //   60: pop
    //   61: goto +5 -> 66
    //   64: astore 4
    //   66: aload_3
    //   67: athrow
    //   68: return
    // Line number table:
    //   Java source line #389	-> byte code offset #0
    //   Java source line #390	-> byte code offset #11
    //   Java source line #391	-> byte code offset #15
    //   Java source line #392	-> byte code offset #20
    //   Java source line #396	-> byte code offset #24
    //   Java source line #398	-> byte code offset #31
    //   Java source line #397	-> byte code offset #34
    //   Java source line #399	-> byte code offset #35
    //   Java source line #393	-> byte code offset #38
    //   Java source line #396	-> byte code offset #39
    //   Java source line #398	-> byte code offset #46
    //   Java source line #397	-> byte code offset #49
    //   Java source line #399	-> byte code offset #50
    //   Java source line #395	-> byte code offset #53
    //   Java source line #396	-> byte code offset #54
    //   Java source line #398	-> byte code offset #61
    //   Java source line #397	-> byte code offset #64
    //   Java source line #398	-> byte code offset #66
    //   Java source line #400	-> byte code offset #68
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	69	0	port	String
    //   0	69	1	message	String
    //   34	1	2	localSerialPortException	SerialPortException
    //   38	1	2	localException	Exception
    //   49	1	2	localSerialPortException1	SerialPortException
    //   53	14	3	localObject	Object
    //   64	1	4	localSerialPortException2	SerialPortException
    // Exception table:
    //   from	to	target	type
    //   24	31	34	jssc/SerialPortException
    //   0	24	38	java/lang/Exception
    //   39	46	49	jssc/SerialPortException
    //   0	24	53	finally
    //   54	61	64	jssc/SerialPortException
  }
  
  public static void testWeightInput()
  {
    try
    {
      SerialPort serialPort = new SerialPort("COM1");
      serialPort.openPort();
      serialPort.setParams(9600, 8, 1, 0);
      













      setCharacterSet('\001');
      
      print('W');
      print('\r');
      
      byte[] readBytes = serialPort.readBytes();
      PosLog.info(DrawerUtil.class, "Response: " + new String(readBytes));
    }
    catch (SerialPortException localSerialPortException) {}
  }
}
