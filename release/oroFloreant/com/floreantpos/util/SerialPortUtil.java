package com.floreantpos.util;

import jssc.SerialPort;
import jssc.SerialPortEvent;

public class SerialPortUtil
{
  public SerialPortUtil() {}
  
  public static String readWeight(String comPort) throws jssc.SerialPortException
  {
    SerialPort serialPort = new SerialPort(comPort);
    serialPort.openPort();
    serialPort.setParams(9600, 7, 2, 2);
    serialPort.setFlowControlMode(15);
    

    final StringBuilder messageBuilder = new StringBuilder();
    
    serialPort.addEventListener(new jssc.SerialPortEventListener()
    {
      public void serialEvent(SerialPortEvent event) {
        try {
          if ((event.isRXCHAR()) && (event.getEventValue() > 0)) {
            byte[] buffer = val$serialPort.readBytes();
            for (byte b : buffer) {
              if (((b == 13) || (b == 10)) && (messageBuilder.length() > 0)) {
                synchronized (messageBuilder) {
                  messageBuilder.notify();
                }
                break;
              }
              
              messageBuilder.append((char)b);
            }
          }
        }
        catch (Exception e) {
          com.floreantpos.PosLog.error(getClass(), e);
        }
      }
    });
    byte[] data = { 87, 13, 0 };
    serialPort.writeBytes(data);
    
    synchronized (messageBuilder) {
      try {
        messageBuilder.wait(2000L);
      } catch (InterruptedException e) {
        serialPort.closePort();
        return messageBuilder.toString();
      }
    }
    
    serialPort.closePort();
    return messageBuilder.toString();
  }
}
