package com.floreantpos.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import org.apache.commons.lang.StringUtils;


















public class NumberUtil
{
  private static final NumberFormat numberFormat = ;
  private static final NumberFormat numberFormat2 = NumberFormat.getNumberInstance();
  private static final DecimalFormat decimalFormat = new DecimalFormat("#.##");
  
  private static final NumberFormat currencyFormat = new DecimalFormat("0.00");
  
  static {
    numberFormat.setMinimumFractionDigits(2);
    numberFormat.setMaximumFractionDigits(2);
    
    numberFormat2.setMinimumFractionDigits(3);
    numberFormat2.setMaximumFractionDigits(3);
    
    numberFormat.setGroupingUsed(false);
    numberFormat2.setGroupingUsed(false);
    currencyFormat.setGroupingUsed(true);
  }
  
  public static double round(double value) {
    if (Double.isNaN(value)) {
      return 0.0D;
    }
    BigDecimal bd = new BigDecimal(String.valueOf(value));
    bd = bd.setScale(2, 6);
    return bd.doubleValue();
  }
  
  public static BigDecimal round(BigDecimal value) {
    return value.setScale(2, 6);
  }
  
  public static double roundToOneDigit(double value) {
    if (Double.isNaN(value)) {
      return 0.0D;
    }
    BigDecimal bd = new BigDecimal(String.valueOf(value));
    bd = bd.setScale(1, 6);
    return bd.doubleValue();
  }
  
  public static double roundToTwoDigit(double value) {
    if (Double.isNaN(value)) {
      return 0.0D;
    }
    BigDecimal bd = new BigDecimal(String.valueOf(value));
    bd = bd.setScale(2, 6);
    return bd.doubleValue();
  }
  
  public static double roundToThreeDigit(double value) {
    if (Double.isNaN(value)) {
      return 0.0D;
    }
    BigDecimal bd = new BigDecimal(String.valueOf(value));
    bd = bd.setScale(3, 6);
    return bd.doubleValue();
  }
  
  public static String format3DigitNumber(Double number) {
    if (number == null) {
      return numberFormat2.format(0L);
    }
    String value = numberFormat2.format(number);
    



    return value;
  }
  
  public static String formatNumber(Double number, boolean isAllowedNegative) {
    if (number == null) {
      return numberFormat.format(0L);
    }
    
    String value = numberFormat.format(number);
    
    if ((!isAllowedNegative) && 
      (value.startsWith("-"))) {
      return numberFormat.format(0L);
    }
    
    return value;
  }
  
  public static String formatNumber(Double number) {
    return formatNumber(number, false);
  }
  
  public static String trimDecilamIfNotNeeded(Double number) {
    return trimDecilamIfNotNeeded(number, false);
  }
  
  public static String trimDecilamIfNotNeeded(Double number, boolean allowNegative) {
    if (number == null) {
      return decimalFormat.format(0L);
    }
    
    String value = decimalFormat.format(number);
    if (allowNegative)
      return value;
    if (value.startsWith("-")) {
      return decimalFormat.format(0L);
    }
    
    return value;
  }
  
  public static String formatNumberAcceptNegative(Double number) {
    if (number == null) {
      return numberFormat.format(0L);
    }
    
    return numberFormat.format(number);
  }
  
  public static Number parse(String number) throws ParseException {
    if (StringUtils.isEmpty(number)) {
      return Integer.valueOf(0);
    }
    
    return numberFormat.parse(number);
  }
  
  public static Number parseOrGetZero(String number) {
    try {
      if (StringUtils.isEmpty(number)) {
        return Integer.valueOf(0);
      }
      
      return numberFormat.parse(number);
    } catch (Exception e) {}
    return Integer.valueOf(0);
  }
  
  public static String getCurrencyFormat(Object d)
  {
    return CurrencyUtil.getCurrencySymbol() + currencyFormat.format(d);
  }
  
  public static String getCurrencyFormatWithoutCurrencySymbol(Object d) {
    return currencyFormat.format(d);
  }
  
  public static BigDecimal convertToBigDecimal(double value) {
    return new BigDecimal(Double.toString(value));
  }
  
  public static BigDecimal convertToBigDecimal(String value) {
    return new BigDecimal(value);
  }
  
  public static boolean isZero(Double number) {
    String value = numberFormat.format(number);
    return (value.equals("0.00")) || (value.equals("-0.00"));
  }
  
  public NumberUtil() {}
}
