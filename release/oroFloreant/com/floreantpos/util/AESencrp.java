package com.floreantpos.util;

import com.floreantpos.PosLog;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;





















public class AESencrp
{
  private static final String ALGO = "AES";
  private static final byte[] keyValue = { 84, 104, 101, 66, 101, 115, 116, 83, 101, 99, 114, 101, 116, 75, 101, 121 };
  
  public AESencrp() {}
  
  public static String encrypt(String Data) throws Exception {
    Key key = generateKey();
    Cipher c = Cipher.getInstance("AES");
    c.init(1, key);
    byte[] encVal = c.doFinal(Data.getBytes());
    Base64 base64 = new Base64();
    String encryptedValue = base64.encodeToString(encVal);
    return encryptedValue;
  }
  
  public static String decrypt(String encryptedData) throws Exception {
    Key key = generateKey();
    Cipher c = Cipher.getInstance("AES");
    c.init(2, key);
    Base64 base64 = new Base64();
    byte[] decordedValue = base64.decode(encryptedData);
    byte[] decValue = c.doFinal(decordedValue);
    String decryptedValue = new String(decValue);
    return decryptedValue;
  }
  
  private static Key generateKey() throws Exception {
    Key key = new SecretKeySpec(keyValue, "AES");
    return key;
  }
  


  public static void main(String[] args)
    throws Exception
  {
    String decrypt = decrypt("4T9H+1LqawVTsVvifd/TxA==");
    PosLog.info(AESencrp.class, decrypt);
  }
}
