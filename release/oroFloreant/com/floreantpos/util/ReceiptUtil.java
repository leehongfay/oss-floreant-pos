package com.floreantpos.util;

import com.floreantpos.model.Store;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;



public class ReceiptUtil
{
  private static Map<String, String> defaultValueMap = new HashMap();
  
  static
  {
    defaultValueMap.put("ticket.header", "<storeName>$storeName</storeName><br><storeAddress1>$storeAddress1</storeAddress1><br><storePhoneNo>$storePhoneNo</storePhoneNo><br>");
    
    defaultValueMap
      .put("ticket.order.info", "<receiptType><b>$receiptType</b></receiptType><br><ticketId>CHK# : $ticketId</ticketId><br><splitTicketId>#Split#  $splitTicketId</splitTicketId><br><tokenNo><b>TOKEN # $tokenNo</b></tokenNo><br>");
    
    defaultValueMap
      .put("ticket.order.extrainfo1", "<orderType>* $orderType *</orderType><br><terminalId>Terminal#: $terminalId</terminalId><br><tableNo>Table#: $tableNo  Guest: $guestCount</tableNo> <br><serverName>Server: $serverName</serverName><br><printDate>Printed: $printDate</");
    
    defaultValueMap
      .put("ticket.order.extrainfo2", "printDate><br><br><customerName>*Delivery to *<br>$customerName</customerName><br><deliveryAddress>$deliveryAddress</deliveryAddress><br><deliveryDate>$deliveryDate</deliveryDate>");
    
    defaultValueMap.put("ticket.footer", "");
    defaultValueMap.put("ticket.bottom", "");
    


    defaultValueMap.put("kitchen.header", "<orderType>* $orderType *</orderType><br>");
    
    defaultValueMap.put("kitchen.order.info", "");
    defaultValueMap
      .put("kitchen.order.extrainfo1", "<br><ticketId>CHK# $ticketId</ticketId><br><tableNo> Table: $tableNo</tableNo><br><customerName>Customer: $customerName</customerName><br><serverName>Server: $serverName</serverName><br><printDate>Printed: $printDate</printDate>");
    

    defaultValueMap.put("kitchen.order.extrainfo2", "");
    
    defaultValueMap.put("kitchen.footer", "");
    defaultValueMap.put("kitchen.bottom", "");
    

    defaultValueMap.put("kitchen.header.logo.show", "false");
    defaultValueMap.put("ticket.header.logo.show", "false");
  }
  
  public static String getReceiptSection(Store store, String propertyName, String orderTypeId) {
    Map<String, String> properties = store.getProperties();
    String property = POSUtil.readLongProperty(properties, orderTypeId + "." + propertyName);
    if (StringUtils.isEmpty(property)) {
      property = POSUtil.readLongProperty(properties, propertyName);
    }
    if (StringUtils.isEmpty(property)) {
      property = (String)defaultValueMap.get(propertyName);
    }
    
    return property;
  }
  
  public static void populateDefaultTicketReceiptProperties(Store store) {
    POSUtil.storeLongProperty(store.getProperties(), "ticket.header", (String)defaultValueMap.get("ticket.header"), 255);
    POSUtil.storeLongProperty(store.getProperties(), "ticket.order.info", (String)defaultValueMap.get("ticket.order.info"), 255);
    POSUtil.storeLongProperty(store.getProperties(), "ticket.order.extrainfo1", 
      (String)defaultValueMap.get("ticket.order.extrainfo1"), 255);
    POSUtil.storeLongProperty(store.getProperties(), "ticket.order.extrainfo2", 
      (String)defaultValueMap.get("ticket.order.extrainfo2"), 255);
  }
  
  public static void populateDefaultKitchenReceiptProperties(Store store)
  {
    POSUtil.storeLongProperty(store.getProperties(), "kitchen.header", (String)defaultValueMap.get("kitchen.header"), 255);
    
    POSUtil.storeLongProperty(store.getProperties(), "kitchen.order.extrainfo1", 
      (String)defaultValueMap.get("kitchen.order.extrainfo1"), 255);
  }
  
  public ReceiptUtil() {}
}
