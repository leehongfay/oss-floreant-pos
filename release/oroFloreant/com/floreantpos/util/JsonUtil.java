package com.floreantpos.util;

import javax.json.JsonValue;

public class JsonUtil
{
  public JsonUtil() {}
  
  public static String getString(javax.json.JsonObject jsonObject, String propertyName)
  {
    if (!jsonObject.containsKey(propertyName)) {
      return null;
    }
    return jsonObject.getString(propertyName);
  }
  
  public static Boolean getBoolean(javax.json.JsonObject jsonObject, String propertyName) {
    if (!jsonObject.containsKey(propertyName)) {
      return null;
    }
    
    JsonValue jsonValue = (JsonValue)jsonObject.get(propertyName);
    javax.json.JsonValue.ValueType valueType = jsonValue.getValueType();
    switch (1.$SwitchMap$javax$json$JsonValue$ValueType[valueType.ordinal()]) {
    case 1: 
      return Boolean.TRUE;
    
    case 2: 
      return Boolean.FALSE;
    }
    return Boolean.valueOf(jsonValue.toString());
  }
  
  public static Integer getInt(javax.json.JsonObject jsonObject, String propertyName) {
    if (!jsonObject.containsKey(propertyName)) {
      return null;
    }
    
    JsonValue jsonValue = (JsonValue)jsonObject.get(propertyName);
    javax.json.JsonValue.ValueType valueType = jsonValue.getValueType();
    switch (1.$SwitchMap$javax$json$JsonValue$ValueType[valueType.ordinal()]) {
    case 3: 
      return Integer.valueOf(((javax.json.JsonNumber)jsonValue).intValue());
    }
    return Integer.valueOf(Integer.parseInt(jsonValue.toString()));
  }
  
  public static Double getDouble(javax.json.JsonObject jsonObject, String propertyName) {
    if (!jsonObject.containsKey(propertyName)) {
      return null;
    }
    
    JsonValue jsonValue = (JsonValue)jsonObject.get(propertyName);
    javax.json.JsonValue.ValueType valueType = jsonValue.getValueType();
    switch (1.$SwitchMap$javax$json$JsonValue$ValueType[valueType.ordinal()]) {
    case 3: 
      return Double.valueOf(((javax.json.JsonNumber)jsonValue).doubleValue());
    }
    return Double.valueOf(Double.parseDouble(jsonValue.toString()));
  }
  
  public static String getStringValue(JsonValue jsonValue)
  {
    if ((jsonValue == JsonValue.NULL) || (jsonValue == null)) {
      return null;
    }
    javax.json.JsonValue.ValueType valueType = jsonValue.getValueType();
    switch (1.$SwitchMap$javax$json$JsonValue$ValueType[valueType.ordinal()]) {
    case 4: 
      return ((javax.json.JsonString)jsonValue).getString();
    }
    
    

    return jsonValue.toString().replaceAll("\"", "");
  }
  
  public static int getIntValue(JsonValue jsonValue) {
    if (jsonValue == null) {
      return 0;
    }
    javax.json.JsonValue.ValueType valueType = jsonValue.getValueType();
    switch (1.$SwitchMap$javax$json$JsonValue$ValueType[valueType.ordinal()]) {
    case 3: 
      return ((javax.json.JsonNumber)jsonValue).intValue();
    }
    
    

    return Integer.parseInt(jsonValue.toString().replaceAll("\"", ""));
  }
  
  public static double getDoubleValue(JsonValue jsonValue) {
    if ((jsonValue == null) || (jsonValue.toString().equals("null"))) {
      return 0.0D;
    }
    javax.json.JsonValue.ValueType valueType = jsonValue.getValueType();
    switch (1.$SwitchMap$javax$json$JsonValue$ValueType[valueType.ordinal()]) {
    case 3: 
      return ((javax.json.JsonNumber)jsonValue).doubleValue();
    }
    
    

    double parseDouble = Double.parseDouble(jsonValue.toString().replaceAll("\"", ""));
    return parseDouble;
  }
}
