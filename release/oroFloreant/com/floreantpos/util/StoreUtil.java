package com.floreantpos.util;

import com.floreantpos.StoreAlreadyOpenException;
import com.floreantpos.model.SequenceNumber;
import com.floreantpos.model.StoreSession;
import com.floreantpos.model.StoreSessionControl;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.SequenceNumberDAO;
import com.floreantpos.model.dao.StoreDAO;
import com.floreantpos.model.dao.StoreSessionControlDAO;
import com.floreantpos.model.dao.TerminalDAO;
import java.util.Date;



public class StoreUtil
{
  public StoreUtil() {}
  
  public static StoreSessionControl getCurrentStoreOperation()
  {
    return StoreSessionControlDAO.getInstance().getCurrent();
  }
  
  public static boolean isStoreOpen() {
    return getCurrentStoreOperation().getCurrentData() != null;
  }
  
  public static void openStore(User openByUser) {
    StoreSessionControl currentStoreOperation = getCurrentStoreOperation();
    if (currentStoreOperation.getCurrentData() != null) {
      throw new StoreAlreadyOpenException("Store is already open.");
    }
    StoreSession data = new StoreSession();
    data.setOpenedBy(openByUser);
    data.setOpenTime(new Date());
    currentStoreOperation.setCurrentData(data);
    

    SequenceNumber sequenceNumber = SequenceNumberDAO.getInstance().get("TICKET_TOKEN_NUMBER");
    if (sequenceNumber == null) {
      sequenceNumber = new SequenceNumber("TICKET_TOKEN_NUMBER");
    }
    sequenceNumber.setNextSequenceNumber(Integer.valueOf(1));
    TerminalDAO.getInstance().performBatchSave(new Object[] { data, currentStoreOperation, sequenceNumber });
  }
  
  public static void closeStore(User closeByUser) throws Exception {
    try {
      StoreDAO.getInstance().closeStore(closeByUser);
    } catch (Exception e) {
      throw e;
    }
  }
}
