package com.floreantpos.util;

import org.json.JSONObject;

public class OrgJsonUtil { public OrgJsonUtil() {}
  
  public static String getString(JSONObject jsonObject, String key) { if (!jsonObject.has(key)) {
      return null;
    }
    
    return jsonObject.getString(key);
  }
  
  public static Boolean getBoolean(JSONObject jsonObject, String key) {
    if (!jsonObject.has(key)) {
      return Boolean.valueOf(false);
    }
    
    return Boolean.valueOf(jsonObject.getBoolean(key));
  }
  
  public static Integer getInt(JSONObject jsonObject, String key) {
    if (!jsonObject.has(key)) {
      return Integer.valueOf(0);
    }
    
    return Integer.valueOf(jsonObject.getInt(key));
  }
  
  public static Double getDouble(JSONObject jsonObject, String key) {
    if (!jsonObject.has(key)) {
      return Double.valueOf(0.0D);
    }
    
    return Double.valueOf(jsonObject.getDouble(key));
  }
}
