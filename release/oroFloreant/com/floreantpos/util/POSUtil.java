package com.floreantpos.util;

import com.floreantpos.Messages;
import com.floreantpos.PosException;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.main.Application;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.PasswordEntryDialog;
import com.floreantpos.ui.kitchendisplay.LineDisplayWindow;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Window;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URLEncoder;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.imageio.ImageIO;
import javax.sql.rowset.serial.SerialBlob;
import javax.swing.FocusManager;
import org.apache.commons.lang.StringUtils;














public class POSUtil
{
  public POSUtil() {}
  
  public static Window getFocusedWindow()
  {
    return FocusManager.getCurrentManager().getFocusedWindow();
  }
  
  public static Image getScaledImage(Image srcImg, int w, int h)
  {
    BufferedImage resizedImg = new BufferedImage(w, h, 2);
    Graphics2D g2 = resizedImg.createGraphics();
    g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    g2.drawImage(srcImg, 0, 0, w, h, null);
    g2.dispose();
    return resizedImg;
  }
  
  public static BackOfficeWindow getBackOfficeWindow() {
    return BackOfficeWindow.getInstance();
  }
  
  public static LineDisplayWindow getLineDisplayWindow() {
    Window[] windows = Window.getWindows();
    for (Window window : windows) {
      if ((window instanceof LineDisplayWindow)) {
        return (LineDisplayWindow)window;
      }
    }
    return null;
  }
  
  public static boolean isBlankOrNull(String str) {
    if (str == null) {
      return true;
    }
    if (str.trim().equals("")) {
      return true;
    }
    return false;
  }
  
  public static String escapePropertyKey(String propertyKey) {
    return propertyKey.replaceAll("\\s+", "_");
  }
  
  public static boolean getBoolean(String b) {
    if (b == null) {
      return false;
    }
    
    return Boolean.valueOf(b).booleanValue();
  }
  
  public static boolean getBoolean(Boolean b) {
    if (b == null) {
      return false;
    }
    
    return b.booleanValue();
  }
  
  public static boolean getBoolean(Boolean b, boolean defaultValue) {
    if (b == null) {
      return defaultValue;
    }
    
    return b.booleanValue();
  }
  
  public static double getDouble(Double d) {
    if (d == null) {
      return 0.0D;
    }
    
    return d.doubleValue();
  }
  
  public static double getDoubleAmount(Object result) {
    if ((result != null) && ((result instanceof Number))) {
      return ((Number)result).doubleValue();
    }
    return 0.0D;
  }
  
  public static double getRoundedDouble(Object result) {
    if ((result != null) && ((result instanceof Number))) {
      return NumberUtil.round(((Number)result).doubleValue());
    }
    return 0.0D;
  }
  
  public static int getInteger(Integer d) {
    if (d == null) {
      return 0;
    }
    
    return d.intValue();
  }
  
  public static int parseInteger(String s) {
    try {
      return Integer.parseInt(s);
    } catch (Exception x) {}
    return 0;
  }
  
  public static int parseInteger(String s, String parseErrorMessage)
  {
    try {
      return Integer.parseInt(s);
    } catch (Exception x) {
      throw new PosException(parseErrorMessage);
    }
  }
  
  public static double parseDouble(String s) {
    try {
      return Double.parseDouble(s);
    } catch (Exception x) {}
    return 0.0D;
  }
  
  public static double parseDouble(String s, String parseErrorMessage, boolean mandatory)
  {
    try {
      return Double.parseDouble(s);
    } catch (Exception x) {
      if (mandatory) {
        throw new PosException(parseErrorMessage);
      }
    }
    return 0.0D;
  }
  
  public static String encodeURLString(String s)
  {
    try
    {
      return URLEncoder.encode(s, "UTF-8");
    } catch (Exception x) {}
    return s;
  }
  
  public static boolean isValidPassword(char[] password)
  {
    for (char c : password) {
      if (!Character.isDigit(c)) {
        return false;
      }
    }
    
    return true;
  }
  
  public static boolean checkDrawerAssignment() {
    Terminal terminal = Application.getInstance().getTerminal();
    User currentUser = Application.getCurrentUser();
    return checkDrawerAssignment(terminal, currentUser);
  }
  
  public static boolean checkDrawerAssignment(Terminal terminal, User user) {
    if (TerminalDAO.getInstance().isDrawerAssigned(terminal)) {
      return true;
    }
    if ((user.isStaffBank().booleanValue()) && (user.isStaffBankStarted().booleanValue())) {
      return true;
    }
    
    showUnableToAcceptPayment();
    return false;
  }
  
  private static void showUnableToAcceptPayment() {
    POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("SwitchboardView.18"));
  }
  
  public static Blob convertImageToBlob(File imageFile) throws Exception
  {
    if (imageFile == null) {
      return null;
    }
    
    BufferedImage bufferedImage = ImageIO.read(imageFile);
    if ((bufferedImage.getWidth() > 400) || (bufferedImage.getHeight() > 400)) {
      int type = bufferedImage.getType() == 0 ? 2 : bufferedImage.getType();
      bufferedImage = resizeImage(bufferedImage, 400, 400, type);
    }
    
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream(50000);
    
    String imageName = imageFile.getName();
    int lastIndexOfImage = imageName.lastIndexOf(".");
    String imageExtension = imageName.substring(lastIndexOfImage + 1);
    
    if (imageExtension.equals("jpg")) {
      ImageIO.write(bufferedImage, "jpg", outputStream);
    }
    else if (imageExtension.equals("jpeg")) {
      ImageIO.write(bufferedImage, "jpeg", outputStream);
    }
    else if (imageExtension.equals("png")) {
      ImageIO.write(bufferedImage, "png", outputStream);
    }
    else {
      ImageIO.write(bufferedImage, "gif", outputStream);
    }
    
    byte[] byteArray = outputStream.toByteArray();
    if (byteArray.length > 500000) {
      throw new RuntimeException("Image size too large. Maximum allowed image size is 500KB");
    }
    return new SerialBlob(byteArray);
  }
  
  public static Blob convertImageToBlob(BufferedImage bufferedImage, String imageExtension) throws Exception
  {
    if (bufferedImage == null) {
      return null;
    }
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream(50000);
    if (imageExtension.equals("jpg")) {
      ImageIO.write(bufferedImage, "jpg", outputStream);
    }
    else if (imageExtension.equals("jpeg")) {
      ImageIO.write(bufferedImage, "jpeg", outputStream);
    }
    else if (imageExtension.equals("png")) {
      ImageIO.write(bufferedImage, "png", outputStream);
    }
    else {
      ImageIO.write(bufferedImage, "gif", outputStream);
    }
    byte[] byteArray = outputStream.toByteArray();
    if (byteArray.length > 500000) {
      throw new RuntimeException("Image size too large. Maximum allowed image size is 500KB");
    }
    return new SerialBlob(byteArray);
  }
  
  public static Blob convertScaledImageToBlob(File imageFile)
    throws Exception
  {
    if (imageFile == null) {
      return null;
    }
    int width = 120;
    int height = 120;
    
    BufferedImage originalImage = ImageIO.read(imageFile);
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream(50000);
    
    int type = originalImage.getType() == 0 ? 2 : originalImage.getType();
    
    BufferedImage resizeImage = resizeImage(originalImage, width, height, type);
    
    String imageName = imageFile.getName();
    int lastIndexOfImage = imageName.lastIndexOf(".");
    String imageExtension = imageName.substring(lastIndexOfImage + 1);
    
    if (imageExtension.equals("jpg")) {
      ImageIO.write(resizeImage, "jpg", outputStream);
    }
    else if (imageExtension.equals("jpeg")) {
      ImageIO.write(resizeImage, "jpeg", outputStream);
    }
    else if (imageExtension.equals("png")) {
      ImageIO.write(resizeImage, "png", outputStream);
    }
    else {
      ImageIO.write(resizeImage, "gif", outputStream);
    }
    
    return new SerialBlob(outputStream.toByteArray());
  }
  
  private static BufferedImage resizeImage(BufferedImage originalImage, int IMG_WIDTH, int IMG_HEIGHT, int type)
  {
    BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
    Graphics2D g = resizedImage.createGraphics();
    g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
    g.dispose();
    
    return resizedImage;
  }
  
  public static User getUserWithPermission(UserPermission requiredPermission) {
    String password = PasswordEntryDialog.show(Application.getPosWindow(), Messages.getString("PosAction.0"));
    if (StringUtils.isEmpty(password)) {
      return null;
    }
    
    User user = UserDAO.getInstance().findUserBySecretKey(password);
    if (user == null) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("PosAction.1"));
      return null;
    }
    if (!user.hasPermission(requiredPermission)) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("PosAction.2"));
      return null;
    }
    return user;
  }
  
  public static void storeLongProperty(Map<String, String> propertyMap, String propertyName, String propertyValue, int length) {
    Set<String> keys = propertyMap.keySet();
    Set<String> existingKeys = new HashSet();
    for (String key : keys) {
      if (key.startsWith(propertyName)) {
        existingKeys.add(key);
      }
    }
    for (String string : existingKeys) {
      propertyMap.remove(string);
    }
    
    if (propertyValue.length() <= length) {
      propertyMap.put(propertyName + "1", propertyValue);
      return;
    }
    
    int count = 1;
    String valueToStore = "";
    for (;;) {
      valueToStore = propertyValue.substring(0, length);
      propertyValue = propertyValue.substring(length);
      String nameToStore = propertyName + count++;
      propertyMap.put(nameToStore, valueToStore);
      
      if (propertyValue.length() < length) {
        if (!StringUtils.isNotEmpty(propertyValue)) break;
        nameToStore = propertyName + count++;
        propertyMap.put(nameToStore, propertyValue); break;
      }
    }
  }
  

  public static String readLongProperty(Map<String, String> propertyMap, String propertyName)
  {
    List<Integer> propertyNumbers = new ArrayList();
    StringBuilder stringBuilder = new StringBuilder();
    Set<String> keys = propertyMap.keySet();
    for (String key : keys) {
      if (key.startsWith(propertyName)) {
        String numberString = key.replaceAll(propertyName, "");
        try {
          propertyNumbers.add(Integer.valueOf(Integer.parseInt(numberString)));
        }
        catch (Exception localException) {}
      }
    }
    
    Collections.sort(propertyNumbers);
    for (Integer integer : propertyNumbers) {
      String string = (String)propertyMap.get(propertyName + integer);
      stringBuilder.append(string);
    }
    
    return stringBuilder.toString();
  }
  
  public static void removeLongProperty(Map<String, String> propertyMap, String propertyName) {
    for (Iterator<Map.Entry<String, String>> it = propertyMap.entrySet().iterator(); it.hasNext();) {
      Map.Entry<String, String> entry = (Map.Entry)it.next();
      if (((String)entry.getKey()).startsWith(propertyName)) {
        it.remove();
      }
    }
  }
  







  public static Set copySelectedValues(Set toCollection, Set selectedValues)
  {
    if (toCollection == null) {
      toCollection = new LinkedHashSet();
      toCollection.addAll(selectedValues);
      return toCollection;
    }
    
    for (Iterator iterator = toCollection.iterator(); iterator.hasNext();) {
      Object existingValue = iterator.next();
      if (!selectedValues.contains(existingValue)) {
        iterator.remove();
      }
    }
    toCollection.addAll(selectedValues);
    return toCollection;
  }
  







  public static List copySelectedValues(List toCollection, List selectedValues)
  {
    if (toCollection == null) {
      toCollection = new ArrayList();
      toCollection.addAll(selectedValues);
      return toCollection;
    }
    
    for (Iterator iterator = toCollection.iterator(); iterator.hasNext();) {
      Object existingValue = iterator.next();
      if (!selectedValues.contains(existingValue)) {
        iterator.remove();
      }
    }
    toCollection.addAll(selectedValues);
    return toCollection;
  }
}
