package com.floreantpos.util;

import com.floreantpos.PosLog;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;























public class JarUtil
{
  public JarUtil() {}
  
  public static String getJarLocation(Class clazz)
  {
    try
    {
      URI uri = clazz.getProtectionDomain().getCodeSource().getLocation().toURI();
      if (uri.toString().endsWith(".jar")) {
        return new File(uri.getPath()).getParentFile().getPath() + "/";
      }
      return uri.getPath();
    }
    catch (URISyntaxException e) {
      PosLog.error(JarUtil.class, e);
      String executableLocation = ".";
      
      return executableLocation + "/";
    }
  }
}
