package com.floreantpos.util;

import com.floreantpos.model.Printer;
import com.floreantpos.model.VirtualPrinter;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;

















public class PrintServiceUtil
{
  private static Printer fallBackPrinter;
  
  static
  {
    PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();
    if (defaultPrintService != null) {
      String defaultPrinterName = defaultPrintService.getName();
      VirtualPrinter virtualPrinter = new VirtualPrinter();
      virtualPrinter.setName(defaultPrinterName);
      fallBackPrinter = new Printer(virtualPrinter, defaultPrinterName);
    }
  }
  





  public static PrintService getPrintServiceForPrinter(String printerName)
  {
    PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
    
    for (int i = 0; i < printServices.length; i++) {
      PrintService printService = printServices[i];
      if (printService.getName().equals(printerName)) {
        return printService;
      }
    }
    
    return PrintServiceLookup.lookupDefaultPrintService();
  }
  
  public static Printer getFallBackPrinter() {
    return fallBackPrinter;
  }
  
  public PrintServiceUtil() {}
}
