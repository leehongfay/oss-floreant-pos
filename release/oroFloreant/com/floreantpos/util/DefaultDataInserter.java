package com.floreantpos.util;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.model.Attribute;
import com.floreantpos.model.AttributeGroup;
import com.floreantpos.model.Currency;
import com.floreantpos.model.DayPart;
import com.floreantpos.model.Discount;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.InventoryUnitGroup;
import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuItemModifierPage;
import com.floreantpos.model.MenuItemModifierPageItem;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.MenuItemSize;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.MenuPage;
import com.floreantpos.model.MenuPageItem;
import com.floreantpos.model.ModifierGroup;
import com.floreantpos.model.Multiplier;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PackagingUnit;
import com.floreantpos.model.PizzaCrust;
import com.floreantpos.model.Recepie;
import com.floreantpos.model.RecepieItem;
import com.floreantpos.model.ShopFloor;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.Store;
import com.floreantpos.model.Tax;
import com.floreantpos.model.TaxGroup;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.UserType;
import com.floreantpos.model.dao.AttributeGroupDAO;
import com.floreantpos.model.dao.CurrencyDAO;
import com.floreantpos.model.dao.DayPartDAO;
import com.floreantpos.model.dao.DiscountDAO;
import com.floreantpos.model.dao.InventoryLocationDAO;
import com.floreantpos.model.dao.InventoryUnitDAO;
import com.floreantpos.model.dao.InventoryUnitGroupDAO;
import com.floreantpos.model.dao.InventoryVendorDAO;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.model.dao.MenuItemModifierPageDAO;
import com.floreantpos.model.dao.MenuItemModifierPageItemDAO;
import com.floreantpos.model.dao.MenuItemModifierSpecDAO;
import com.floreantpos.model.dao.MenuItemSizeDAO;
import com.floreantpos.model.dao.MenuModifierDAO;
import com.floreantpos.model.dao.MenuPageDAO;
import com.floreantpos.model.dao.MenuPageItemDAO;
import com.floreantpos.model.dao.ModifierGroupDAO;
import com.floreantpos.model.dao.MultiplierDAO;
import com.floreantpos.model.dao.OrderTypeDAO;
import com.floreantpos.model.dao.PackagingUnitDAO;
import com.floreantpos.model.dao.PizzaCrustDAO;
import com.floreantpos.model.dao.RecepieDAO;
import com.floreantpos.model.dao.ShopFloorDAO;
import com.floreantpos.model.dao.ShopTableDAO;
import com.floreantpos.model.dao.StoreDAO;
import com.floreantpos.model.dao.TaxDAO;
import com.floreantpos.model.dao.TaxGroupDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.model.dao.UserTypeDAO;
import com.orocube.common.util.TerminalUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class DefaultDataInserter
{
  private TaxGroup taxGroupSix33;
  private TaxGroup taxGroupSix01 = new TaxGroup("taxgroup2", "6.01");
  private TaxGroup taxGroupnineteen32 = new TaxGroup("taxgroup3", "19.32");
  private ModifierGroup cheeseModifierGroup;
  private ModifierGroup bevarageModifierGroup;
  private ModifierGroup toppingsModifierGroup;
  private MenuModifier americanCheese;
  private MenuModifier gouda;
  private MenuModifier chadder;
  private MenuModifier brie;
  private MenuModifier lemon;
  private MenuModifier salt;
  private MenuModifier mashroom;
  private MenuModifier pepparoni;
  private MenuModifier sausage;
  private MenuModifier onionsModifier;
  private InventoryUnit unitPiece;
  private InventoryUnit unitKg;
  private MenuItem beefBurger;
  private MenuItem bread;
  private MenuItem meat;
  private InventoryUnit unitGram;
  
  public DefaultDataInserter() {}
  
  public void insertDefaultData(int databaseVersion) {
    createStore(databaseVersion);
    
    createShifts();
    
    createTerminal();
    
    createDefaultUser();
    
    createDefaultTax();
    
    createDefaultCurrency();
    
    createDefaultFloor();
  }
  

  public void createSampleData()
  {
    createSampleUsers();
    
    createOrderTypes();
    
    createAttributes();
    
    createPizzaCrust();
    
    createMenuItemData();
    
    createRecipe();
    
    createMenuItemSize();
    
    createSampleCurrencies();
    
    createSampleTaxes();
    
    createMultipliers();
    
    createDiscounts();
  }
  
  private void createMenuItemSize() {
    MenuItemSize menuItemSize = new MenuItemSize();
    menuItemSize.setName(Messages.getString("DatabaseUtil.21"));
    menuItemSize.setSortOrder(Integer.valueOf(0));
    MenuItemSizeDAO.getInstance().save(menuItemSize);
    
    menuItemSize = new MenuItemSize();
    menuItemSize.setName(Messages.getString("DatabaseUtil.22"));
    menuItemSize.setSortOrder(Integer.valueOf(1));
    MenuItemSizeDAO.getInstance().save(menuItemSize);
    
    menuItemSize = new MenuItemSize();
    menuItemSize.setName(Messages.getString("DatabaseUtil.23"));
    menuItemSize.setSortOrder(Integer.valueOf(2));
    MenuItemSizeDAO.getInstance().save(menuItemSize);
  }
  
  private void createPizzaCrust() {
    PizzaCrust crust = new PizzaCrust();
    crust.setName(Messages.getString("DatabaseUtil.24"));
    crust.setSortOrder(Integer.valueOf(0));
    PizzaCrustDAO.getInstance().save(crust);
    
    crust = new PizzaCrust();
    crust.setName(Messages.getString("DatabaseUtil.25"));
    crust.setSortOrder(Integer.valueOf(1));
    PizzaCrustDAO.getInstance().save(crust);
  }
  
  private void createSampleCurrencies()
  {
    Currency currency = new Currency();
    currency = new Currency();
    currency.setName("EUR");
    currency.setSymbol("E");
    currency.setExchangeRate(Double.valueOf(0.8D));
    CurrencyDAO.getInstance().save(currency);
    
    currency = new Currency();
    currency.setName("BRL");
    currency.setSymbol("B");
    currency.setExchangeRate(Double.valueOf(3.47D));
    CurrencyDAO.getInstance().save(currency);
    
    currency = new Currency();
    currency.setName("ARS");
    currency.setSymbol("P");
    currency.setExchangeRate(Double.valueOf(13.89D));
    CurrencyDAO.getInstance().save(currency);
    
    currency = new Currency();
    currency.setName("PYG");
    currency.setSymbol("P");
    currency.setExchangeRate(Double.valueOf(5639.78D));
    CurrencyDAO.getInstance().save(currency);
  }
  
  private void createDefaultCurrency()
  {
    Currency currency = new Currency();
    currency.setName("USD");
    currency.setSymbol("$");
    currency.setExchangeRate(Double.valueOf(1.0D));
    currency.setMain(Boolean.valueOf(true));
    CurrencyDAO.getInstance().save(currency);
  }
  
  private void createDefaultTax() {
    Tax tax = new Tax();
    tax.setName("US");
    tax.setRate(Double.valueOf(6.33D));
    taxGroupSix33 = new TaxGroup("taxgroup1", "US 6.33");
    taxGroupSix33.addTotaxes(tax);
    TaxGroupDAO.getInstance().save(taxGroupSix33);
  }
  
  private void createDefaultUser() {
    UserType administrator = new UserType();
    administrator.setName(POSConstants.ADMINISTRATOR);
    administrator.setPermissions(new HashSet(Arrays.asList(UserPermission.permissions)));
    UserTypeDAO.getInstance().saveOrUpdate(administrator);
    
    User administratorUser = new User();
    administratorUser.setId("123");
    administratorUser.setEncryptedPassword("1111");
    administratorUser.setFirstName("admin");
    administratorUser.setLastName("s");
    administratorUser.setType(administrator);
    administratorUser.setRoot(Boolean.valueOf(true));
    administratorUser.setActive(Boolean.valueOf(true));
    
    UserDAO dao = new UserDAO();
    dao.save(administratorUser);
  }
  
  private void createStore(int databaseVersion) {
    Store store = new Store();
    store.setId("1");
    store.setName("Sample Restaurant");
    store.setOutletName("Sample Outlet");
    store.setAddressLine1("Somewhere");
    store.setTelephone("+0123456789");
    store.setProperties(new HashMap());
    store.setDatabaseVersion(databaseVersion);
    ReceiptUtil.populateDefaultTicketReceiptProperties(store);
    ReceiptUtil.populateDefaultKitchenReceiptProperties(store);
    StoreDAO.getInstance().saveOrUpdate(store);
  }
  
  private void createTerminal() {
    int terminalId = TerminalConfig.getTerminalId();
    
    if (terminalId == -1) {
      Random random = new Random();
      terminalId = random.nextInt(10000) + 1;
    }
    Terminal terminal = new Terminal();
    terminal.setId(Integer.valueOf(terminalId));
    terminal.setDefaultPassLength(Integer.valueOf(4));
    terminal.setTerminalKey(TerminalUtil.getSystemUID());
    terminal.setName(String.valueOf(terminalId));
    
    TerminalDAO.getInstance().saveOrUpdate(terminal);
  }
  
  private void createDiscounts() {
    DiscountDAO discountDao = new DiscountDAO();
    
    Discount discount1 = new Discount();
    discount1.setId("buy1get1");
    discount1.setName(Messages.getString("DatabaseUtil.18", "Buy 1 and get 1 free"));
    discount1.setType(Integer.valueOf(1));
    discount1.setValue(Double.valueOf(100.0D));
    discount1.setAutoApply(Boolean.valueOf(false));
    discount1.setMinimumBuy(Double.valueOf(2.0D));
    discount1.setQualificationType(Integer.valueOf(0));
    discount1.setApplyToAll(Boolean.valueOf(true));
    discount1.setNeverExpire(Boolean.valueOf(true));
    discount1.setEnabled(Boolean.valueOf(true));
    
    discountDao.save(discount1);
    
    Discount discount2 = new Discount();
    discount2.setId("buy2get1");
    discount2.setName(Messages.getString("DatabaseUtil.19", "Buy 2 and get 1 free"));
    discount2.setType(Integer.valueOf(1));
    discount2.setValue(Double.valueOf(100.0D));
    discount2.setAutoApply(Boolean.valueOf(true));
    discount2.setMinimumBuy(Double.valueOf(3.0D));
    discount2.setQualificationType(Integer.valueOf(0));
    discount2.setApplyToAll(Boolean.valueOf(true));
    discount2.setNeverExpire(Boolean.valueOf(true));
    discount2.setEnabled(Boolean.valueOf(true));
    
    discountDao.save(discount2);
    
    Discount discount3 = new Discount();
    discount3.setId("10%onItem");
    discount3.setName(Messages.getString("DatabaseUtil.20", "10% Off on Item"));
    discount3.setType(Integer.valueOf(1));
    discount3.setValue(Double.valueOf(10.0D));
    discount3.setAutoApply(Boolean.valueOf(false));
    discount3.setMinimumBuy(Double.valueOf(1.0D));
    discount3.setQualificationType(Integer.valueOf(0));
    discount3.setApplyToAll(Boolean.valueOf(true));
    discount3.setNeverExpire(Boolean.valueOf(true));
    discount3.setEnabled(Boolean.valueOf(true));
    discountDao.save(discount3);
    
    Discount discount4 = new Discount();
    discount4.setId("10%onTicket");
    discount4.setName("10% on Ticket");
    discount4.setType(Integer.valueOf(1));
    discount4.setValue(Double.valueOf(10.0D));
    discount4.setAutoApply(Boolean.valueOf(false));
    discount4.setMinimumBuy(Double.valueOf(1.0D));
    discount4.setQualificationType(Integer.valueOf(1));
    discount4.setApplyToAll(Boolean.valueOf(true));
    discount4.setNeverExpire(Boolean.valueOf(true));
    discount4.setEnabled(Boolean.valueOf(true));
    discountDao.save(discount4);
  }
  
  private void createOrderTypes() {
    OrderTypeDAO orderTypeDAO = new OrderTypeDAO();
    OrderType orderType = new OrderType("dine-in");
    orderType.setName(Messages.getString("DatabaseUtil.14"));
    orderType.setShowTableSelection(Boolean.valueOf(true));
    orderType.setCloseOnPaid(Boolean.valueOf(true));
    orderType.setEnabled(Boolean.valueOf(true));
    orderType.setShouldPrintToKitchen(Boolean.valueOf(true));
    orderType.setShowInLoginScreen(Boolean.valueOf(true));
    orderType.setSortOrder(Integer.valueOf(1));
    orderTypeDAO.save(orderType);
    
    orderType = new OrderType("take-out");
    orderType.setName(Messages.getString("DatabaseUtil.15"));
    orderType.setShowTableSelection(Boolean.valueOf(false));
    orderType.setCloseOnPaid(Boolean.valueOf(true));
    orderType.setEnabled(Boolean.valueOf(true));
    orderType.setPrepaid(Boolean.valueOf(true));
    orderType.setShouldPrintToKitchen(Boolean.valueOf(true));
    orderType.setShowInLoginScreen(Boolean.valueOf(true));
    orderType.setSortOrder(Integer.valueOf(2));
    orderTypeDAO.save(orderType);
    
    orderType = new OrderType("retail");
    orderType.setName(Messages.getString("DatabaseUtil.16"));
    orderType.setShowTableSelection(Boolean.valueOf(false));
    orderType.setCloseOnPaid(Boolean.valueOf(true));
    orderType.setEnabled(Boolean.valueOf(true));
    orderType.setRetailOrder(Boolean.valueOf(true));
    orderType.setShouldPrintToKitchen(Boolean.valueOf(false));
    orderType.setShowInLoginScreen(Boolean.valueOf(true));
    orderType.setSortOrder(Integer.valueOf(3));
    orderTypeDAO.save(orderType);
    
    orderType = new OrderType("home-delivery");
    orderType.setName(Messages.getString("DatabaseUtil.17"));
    orderType.setShowTableSelection(Boolean.valueOf(false));
    orderType.setCloseOnPaid(Boolean.valueOf(false));
    orderType.setEnabled(Boolean.valueOf(true));
    orderType.setShouldPrintToKitchen(Boolean.valueOf(true));
    orderType.setShowInLoginScreen(Boolean.valueOf(true));
    orderType.setRequiredCustomerData(Boolean.valueOf(true));
    orderType.setDelivery(Boolean.valueOf(true));
    orderType.setSortOrder(Integer.valueOf(4));
    orderTypeDAO.save(orderType);
  }
  
  private void createSampleUsers() {
    UserType manager = new UserType();
    manager.setName(POSConstants.MANAGER);
    manager.setPermissions(new HashSet(Arrays.asList(UserPermission.permissions)));
    UserTypeDAO.getInstance().saveOrUpdate(manager);
    
    UserType cashier = new UserType();
    cashier.setName(POSConstants.CASHIER);
    cashier.setPermissions(new HashSet(Arrays.asList(new UserPermission[] { UserPermission.CREATE_TICKET, UserPermission.SETTLE_TICKET, UserPermission.SPLIT_TICKET, UserPermission.EDIT_OTHER_USERS_TICKETS })));
    
    UserTypeDAO.getInstance().saveOrUpdate(cashier);
    
    UserType server = new UserType();
    server.setName("SERVER");
    server.setPermissions(new HashSet(
      Arrays.asList(new UserPermission[] { UserPermission.CREATE_TICKET, UserPermission.SETTLE_TICKET, UserPermission.SPLIT_TICKET })));
    UserTypeDAO.getInstance().saveOrUpdate(server);
    
    UserDAO dao = new UserDAO();
    
    createManagerUser(manager, cashier, server);
    
    User cashierUser = new User();
    cashierUser.setId("125");
    cashierUser.setEncryptedPassword("3333");
    cashierUser.setFirstName("Cashier");
    cashierUser.setLastName("C");
    cashierUser.setType(cashier);
    cashierUser.setRoot(Boolean.valueOf(true));
    cashierUser.setActive(Boolean.valueOf(true));
    dao.save(cashierUser);
    
    User serverUser = new User();
    serverUser.setId("126");
    serverUser.setEncryptedPassword("4444");
    serverUser.setFirstName("John");
    serverUser.setLastName("Doe");
    serverUser.setType(server);
    serverUser.setRoot(Boolean.valueOf(true));
    serverUser.setActive(Boolean.valueOf(true));
    serverUser.setStaffBank(Boolean.valueOf(true));
    serverUser.setAutoStartStaffBank(Boolean.valueOf(true));
    dao.save(serverUser);
    
    serverUser = new User();
    serverUser.setId("127");
    serverUser.setEncryptedPassword("6666");
    serverUser.setFirstName("Juthi");
    serverUser.setLastName("M");
    serverUser.setType(server);
    serverUser.setRoot(Boolean.valueOf(true));
    serverUser.setActive(Boolean.valueOf(true));
    dao.save(serverUser);
  }
  
  private void createSampleTaxes() {
    Tax five89 = new Tax("five89", "five 89", 5.89D);
    TaxDAO.getInstance().save(five89);
    
    Tax zero12 = new Tax("zero12", "zero 12", 0.12D);
    TaxDAO.getInstance().save(zero12);
    
    Tax fifteen45 = new Tax("fifteen45", "fifteen 45", 15.45D);
    TaxDAO.getInstance().save(fifteen45);
    
    Tax three87 = new Tax("three87", "three87", 3.87D);
    TaxDAO.getInstance().save(three87);
    

    taxGroupSix01.addTotaxes(five89);
    taxGroupSix01.addTotaxes(zero12);
    TaxGroupDAO.getInstance().save(taxGroupSix01);
    
    taxGroupnineteen32.addTotaxes(fifteen45);
    taxGroupnineteen32.addTotaxes(three87);
    TaxGroupDAO.getInstance().save(taxGroupnineteen32);
  }
  
  private void createMenuItemData() {
    createInventoryData();
    
    MenuCategory fastfood = new MenuCategory();
    fastfood.setName("FASTFOOD");
    fastfood.setVisible(Boolean.valueOf(true));
    fastfood.setSortOrder(Integer.valueOf(1));
    MenuCategoryDAO.getInstance().save(fastfood);
    
    MenuCategory drinks = new MenuCategory();
    drinks.setName("DRINKS");
    drinks.setVisible(Boolean.valueOf(true));
    drinks.setSortOrder(Integer.valueOf(2));
    MenuCategoryDAO.getInstance().save(drinks);
    
    MenuCategory rawCategory = new MenuCategory();
    rawCategory.setName("RAW");
    rawCategory.setVisible(Boolean.valueOf(true));
    rawCategory.setSortOrder(Integer.valueOf(3));
    MenuCategoryDAO.getInstance().save(rawCategory);
    
    MenuGroup burgerGroup = new MenuGroup();
    burgerGroup.setName("BURGER");
    burgerGroup.setVisible(Boolean.valueOf(true));
    burgerGroup.setMenuCategoryId(fastfood.getId());
    burgerGroup.setSortOrder(Integer.valueOf(1));
    MenuGroupDAO.getInstance().save(burgerGroup);
    
    MenuGroup beverageGroup = new MenuGroup();
    beverageGroup.setName("BEVERAGE");
    beverageGroup.setVisible(Boolean.valueOf(true));
    beverageGroup.setMenuCategoryId(drinks.getId());
    beverageGroup.setSortOrder(Integer.valueOf(2));
    MenuGroupDAO.getInstance().save(beverageGroup);
    
    MenuGroup sandwichGroup = new MenuGroup();
    sandwichGroup.setName("SANDWICH");
    sandwichGroup.setVisible(Boolean.valueOf(true));
    sandwichGroup.setMenuCategoryId(fastfood.getId());
    sandwichGroup.setSortOrder(Integer.valueOf(3));
    MenuGroupDAO.getInstance().save(sandwichGroup);
    
    MenuGroup rawGroup = new MenuGroup();
    rawGroup.setName("BURGER");
    rawGroup.setVisible(Boolean.valueOf(true));
    rawGroup.setMenuCategoryId(rawCategory.getId());
    rawGroup.setSortOrder(Integer.valueOf(4));
    MenuGroupDAO.getInstance().save(rawGroup);
    
    createMenuModifiers();
    
    Session session = MenuItemDAO.getInstance().createNewSession();
    Transaction transaction = session.beginTransaction();
    

    MenuItem americanBurger = new MenuItem("americanBurger", "American Burger", Double.valueOf(77.99D));
    americanBurger.setUnit(unitPiece);
    americanBurger.setCost(Double.valueOf(5.5D));
    americanBurger.setVisible(Boolean.valueOf(true));
    americanBurger.setReorderLevel(Double.valueOf(5.0D));
    americanBurger.setReplenishLevel(Double.valueOf(10.0D));
    americanBurger.setMenuGroup(burgerGroup);
    americanBurger.setTaxGroup(taxGroupnineteen32);
    MenuItemDAO.getInstance().save(americanBurger, session);
    
    beefBurger = new MenuItem("beefBurger", "Beef Burger", Double.valueOf(99.33D));
    beefBurger.setUnit(unitPiece);
    beefBurger.setCost(Double.valueOf(1.5D));
    beefBurger.setVisible(Boolean.valueOf(true));
    beefBurger.setReorderLevel(Double.valueOf(5.0D));
    beefBurger.setReplenishLevel(Double.valueOf(10.0D));
    beefBurger.setMenuGroup(burgerGroup);
    beefBurger.setTaxGroup(taxGroupSix33);
    MenuItemDAO.getInstance().save(beefBurger, session);
    
    MenuItem customBurger = new MenuItem("customBurger", "Custom Burger", Double.valueOf(45.66D));
    customBurger.setUnit(unitPiece);
    customBurger.setCost(Double.valueOf(5.5D));
    customBurger.setReorderLevel(Double.valueOf(5.0D));
    customBurger.setReplenishLevel(Double.valueOf(10.0D));
    customBurger.setMenuGroup(burgerGroup);
    customBurger.setTaxGroup(taxGroupSix33);
    customBurger.setHasModifiers(Boolean.valueOf(true));
    customBurger.setHasMandatoryModifiers(Boolean.valueOf(true));
    createCheeseMenuModifierSpec(customBurger);
    MenuItemDAO.getInstance().save(customBurger, session);
    
    MenuItem numbBurger = new MenuItem("numbBurger", "Numb Burger", Double.valueOf(23.32D));
    numbBurger.setUnit(unitPiece);
    numbBurger.setCost(Double.valueOf(5.5D));
    numbBurger.setReorderLevel(Double.valueOf(5.0D));
    numbBurger.setReplenishLevel(Double.valueOf(10.0D));
    numbBurger.setMenuGroup(burgerGroup);
    numbBurger.setTaxGroup(taxGroupSix01);
    numbBurger.setHasModifiers(Boolean.valueOf(true));
    numbBurger.setHasMandatoryModifiers(Boolean.valueOf(true));
    createCheeseMenuModifierSpec(numbBurger);
    MenuItemDAO.getInstance().save(numbBurger, session);
    
    MenuItem cheeseBurger = new MenuItem("cheeseBurger", "cheeseBurger", Double.valueOf(75.89D));
    cheeseBurger.setUnit(unitPiece);
    cheeseBurger.setCost(Double.valueOf(5.5D));
    cheeseBurger.setReorderLevel(Double.valueOf(5.0D));
    cheeseBurger.setReplenishLevel(Double.valueOf(10.0D));
    cheeseBurger.setMenuGroup(burgerGroup);
    cheeseBurger.setHasModifiers(Boolean.valueOf(true));
    cheeseBurger.setHasMandatoryModifiers(Boolean.valueOf(true));
    cheeseBurger.setTaxGroup(taxGroupnineteen32);
    createToppingsMenuModifierSpec(cheeseBurger);
    MenuItemDAO.getInstance().save(cheeseBurger, session);
    
    MenuItem chickenBurger = new MenuItem("chickenBurger", "Chicken Burger", Double.valueOf(89.99D));
    chickenBurger.setUnit(unitPiece);
    chickenBurger.setCost(Double.valueOf(5.5D));
    chickenBurger.setReorderLevel(Double.valueOf(5.0D));
    chickenBurger.setReplenishLevel(Double.valueOf(10.0D));
    chickenBurger.setMenuGroup(burgerGroup);
    chickenBurger.setTaxGroup(taxGroupSix33);
    MenuItemDAO.getInstance().save(chickenBurger, session);
    
    MenuItem nullBurger = new MenuItem("nullBurger", "Null Burger", Double.valueOf(66.99D));
    nullBurger.setUnit(unitPiece);
    nullBurger.setCost(Double.valueOf(5.5D));
    nullBurger.setReorderLevel(Double.valueOf(5.0D));
    nullBurger.setReplenishLevel(Double.valueOf(10.0D));
    nullBurger.setMenuGroup(burgerGroup);
    MenuItemDAO.getInstance().save(nullBurger, session);
    
    MenuItem hamBurger = new MenuItem("hamBurger", "Ham Burger", Double.valueOf(85.35D));
    hamBurger.setUnit(unitPiece);
    hamBurger.setCost(Double.valueOf(5.5D));
    hamBurger.setReorderLevel(Double.valueOf(5.0D));
    hamBurger.setReplenishLevel(Double.valueOf(10.0D));
    hamBurger.setMenuGroup(burgerGroup);
    hamBurger.setHasModifiers(Boolean.valueOf(true));
    hamBurger.setHasMandatoryModifiers(Boolean.valueOf(true));
    createToppingsMenuModifierSpec(hamBurger);
    MenuItemDAO.getInstance().save(hamBurger, session);
    

    bread = new MenuItem("bread", "Bread", Double.valueOf(10.0D));
    bread.setUnit(unitPiece);
    bread.setCost(Double.valueOf(3.0D));
    bread.setReorderLevel(Double.valueOf(5.0D));
    bread.setReplenishLevel(Double.valueOf(10.0D));
    bread.setMenuGroup(rawGroup);
    MenuItemDAO.getInstance().save(bread, session);
    
    meat = new MenuItem("meat", "Meat", Double.valueOf(400.0D));
    meat.setUnit(unitKg);
    meat.setCost(Double.valueOf(250.0D));
    meat.setReorderLevel(Double.valueOf(5.0D));
    meat.setReplenishLevel(Double.valueOf(10.0D));
    meat.setMenuGroup(rawGroup);
    MenuItemDAO.getInstance().save(meat, session);
    

    MenuItem coke = new MenuItem("coke", "COKE", Double.valueOf(14.99D));
    coke.setReorderLevel(Double.valueOf(5.0D));
    coke.setReplenishLevel(Double.valueOf(10.0D));
    coke.setUnit(unitPiece);
    coke.setCost(Double.valueOf(1.5D));
    coke.setMenuGroup(beverageGroup);
    coke.setTaxGroup(taxGroupSix01);
    coke.setHasVariant(Boolean.valueOf(true));
    MenuItemDAO.getInstance().save(coke, session);
    doCreateVariantForCoke(coke, session);
    

    MenuItem pepsi = new MenuItem("pepsi", "PEPSI", Double.valueOf(7.77D));
    pepsi.setReorderLevel(Double.valueOf(5.0D));
    pepsi.setReplenishLevel(Double.valueOf(10.0D));
    pepsi.setUnit(unitPiece);
    pepsi.setCost(Double.valueOf(1.5D));
    pepsi.setMenuGroup(beverageGroup);
    pepsi.setTaxGroup(taxGroupnineteen32);
    pepsi.setHasModifiers(Boolean.valueOf(true));
    pepsi.setHasMandatoryModifiers(Boolean.valueOf(true));
    pepsi.setHasVariant(Boolean.valueOf(true));
    createBeverageMenuModifierSpec(pepsi);
    MenuItemDAO.getInstance().save(pepsi, session);
    doCreateVariantForPepsi(pepsi, session);
    
    MenuItem fanta = new MenuItem("fanta", "FANTA", Double.valueOf(8.91D));
    fanta.setMenuGroup(beverageGroup);
    fanta.setReorderLevel(Double.valueOf(5.0D));
    fanta.setReplenishLevel(Double.valueOf(10.0D));
    fanta.setTaxGroup(taxGroupSix01);
    fanta.setUnit(unitPiece);
    fanta.setCost(Double.valueOf(1.5D));
    MenuItemDAO.getInstance().save(fanta, session);
    
    MenuItem sprite = new MenuItem("sprite", "SPRITE", Double.valueOf(11.13D));
    sprite.setMenuGroup(beverageGroup);
    sprite.setReorderLevel(Double.valueOf(5.0D));
    sprite.setReplenishLevel(Double.valueOf(10.0D));
    sprite.setTaxGroup(taxGroupSix33);
    sprite.setUnit(unitPiece);
    sprite.setCost(Double.valueOf(1.5D));
    MenuItemDAO.getInstance().save(sprite, session);
    

    MenuItem chickenSandwich = new MenuItem("chickenSandwich", "CHICKEN SANDWICH", Double.valueOf(51.32D));
    chickenSandwich.setTaxGroup(taxGroupnineteen32);
    chickenSandwich.setMenuGroup(sandwichGroup);
    chickenSandwich.setReorderLevel(Double.valueOf(5.0D));
    chickenSandwich.setReplenishLevel(Double.valueOf(10.0D));
    chickenSandwich.setVisible(Boolean.valueOf(true));
    chickenSandwich.setUnit(unitPiece);
    chickenSandwich.setCost(Double.valueOf(1.5D));
    MenuItemDAO.getInstance().save(chickenSandwich, session);
    
    MenuItem beefSandwich = new MenuItem("beeefSandwich", "BEEF SANDWICH", Double.valueOf(113.45D));
    beefSandwich.setTaxGroup(taxGroupSix01);
    beefSandwich.setMenuGroup(sandwichGroup);
    beefSandwich.setReorderLevel(Double.valueOf(5.0D));
    beefSandwich.setVisible(Boolean.valueOf(true));
    beefSandwich.setReplenishLevel(Double.valueOf(10.0D));
    beefSandwich.setUnit(unitPiece);
    beefSandwich.setCost(Double.valueOf(1.5D));
    MenuItemDAO.getInstance().save(beefSandwich, session);
    
    MenuItem hamSandwich = new MenuItem("hamSandwich", "HAM SANDWICH", Double.valueOf(53.37D));
    hamSandwich.setMenuGroup(sandwichGroup);
    hamSandwich.setReorderLevel(Double.valueOf(5.0D));
    hamSandwich.setVisible(Boolean.valueOf(true));
    hamSandwich.setReplenishLevel(Double.valueOf(10.0D));
    hamSandwich.setTaxGroup(taxGroupSix33);
    hamSandwich.setUnit(unitPiece);
    hamSandwich.setCost(Double.valueOf(1.5D));
    MenuItemDAO.getInstance().save(hamSandwich, session);
    
    transaction.commit();
    session.close();
    

    MenuPage burgerPage = new MenuPage();
    burgerPage.setName("burgers");
    burgerPage.setCols(Integer.valueOf(4));
    burgerPage.setRows(Integer.valueOf(4));
    burgerPage.setButtonHeight(Integer.valueOf(100));
    burgerPage.setButtonWidth(Integer.valueOf(120));
    burgerPage.setVisible(Boolean.valueOf(true));
    burgerPage.setMenuGroupId(burgerGroup.getId());
    MenuPageDAO.getInstance().save(burgerPage);
    
    MenuPageItem menuPageItem = new MenuPageItem(Integer.valueOf(0), Integer.valueOf(0), americanBurger, burgerPage);
    menuPageItem.setMenuPageId(burgerPage.getId());
    MenuPageItemDAO.getInstance().save(menuPageItem);
    
    MenuPageItem menuPageItem2 = new MenuPageItem(Integer.valueOf(1), Integer.valueOf(0), beefBurger, burgerPage);
    menuPageItem2.setMenuPageId(burgerPage.getId());
    MenuPageItemDAO.getInstance().save(menuPageItem2);
    
    MenuPageItem menuPageItem3 = new MenuPageItem(Integer.valueOf(2), Integer.valueOf(0), chickenBurger, burgerPage);
    menuPageItem3.setMenuPageId(burgerPage.getId());
    MenuPageItemDAO.getInstance().save(menuPageItem3);
    
    MenuPageItem menuPageItem4 = new MenuPageItem(Integer.valueOf(3), Integer.valueOf(0), cheeseBurger, burgerPage);
    menuPageItem4.setMenuPageId(burgerPage.getId());
    MenuPageItemDAO.getInstance().save(menuPageItem4);
    
    MenuPageItem menuPageItem5 = new MenuPageItem(Integer.valueOf(0), Integer.valueOf(1), customBurger, burgerPage);
    menuPageItem5.setMenuPageId(burgerPage.getId());
    MenuPageItemDAO.getInstance().save(menuPageItem5);
    
    MenuPageItem menuPageItem6 = new MenuPageItem(Integer.valueOf(1), Integer.valueOf(1), hamBurger, burgerPage);
    menuPageItem6.setMenuPageId(burgerPage.getId());
    MenuPageItemDAO.getInstance().save(menuPageItem6);
    
    MenuPageItem menuPageItem7 = new MenuPageItem(Integer.valueOf(2), Integer.valueOf(1), nullBurger, burgerPage);
    menuPageItem7.setMenuPageId(burgerPage.getId());
    MenuPageItemDAO.getInstance().save(menuPageItem7);
    
    MenuPageItem menuPageItem8 = new MenuPageItem(Integer.valueOf(3), Integer.valueOf(1), numbBurger, burgerPage);
    menuPageItem8.setMenuPageId(burgerPage.getId());
    MenuPageItemDAO.getInstance().save(menuPageItem8);
    
    MenuPageDAO.getInstance().saveOrUpdate(burgerPage);
    
    MenuPage sandwichPage = new MenuPage();
    sandwichPage.setName("page 1");
    sandwichPage.setCols(Integer.valueOf(4));
    sandwichPage.setRows(Integer.valueOf(4));
    sandwichPage.setButtonHeight(Integer.valueOf(100));
    sandwichPage.setButtonWidth(Integer.valueOf(120));
    sandwichPage.setVisible(Boolean.valueOf(true));
    sandwichPage.setMenuGroupId(sandwichGroup.getId());
    MenuPageDAO.getInstance().save(sandwichPage);
    
    MenuPageItem sandwichItem1 = new MenuPageItem(Integer.valueOf(0), Integer.valueOf(0), beefSandwich, sandwichPage);
    sandwichItem1.setMenuPage(sandwichPage);
    MenuPageItemDAO.getInstance().save(sandwichItem1);
    
    MenuPageItem sandwichItem2 = new MenuPageItem(Integer.valueOf(1), Integer.valueOf(0), chickenSandwich, sandwichPage);
    sandwichItem2.setMenuPage(sandwichPage);
    MenuPageItemDAO.getInstance().save(sandwichItem2);
    
    MenuPageItem sandwichItem3 = new MenuPageItem(Integer.valueOf(2), Integer.valueOf(0), hamSandwich, sandwichPage);
    sandwichItem3.setMenuPage(sandwichPage);
    MenuPageItemDAO.getInstance().save(sandwichItem3);
    
    MenuPageDAO.getInstance().saveOrUpdate(sandwichPage);
    
    MenuPage drinksPage = new MenuPage();
    drinksPage.setName("page 1");
    drinksPage.setCols(Integer.valueOf(4));
    drinksPage.setRows(Integer.valueOf(4));
    drinksPage.setButtonHeight(Integer.valueOf(100));
    drinksPage.setButtonWidth(Integer.valueOf(120));
    drinksPage.setVisible(Boolean.valueOf(true));
    drinksPage.setMenuGroupId(beverageGroup.getId());
    MenuPageDAO.getInstance().save(drinksPage);
    
    MenuPageItem cokeItem = new MenuPageItem(Integer.valueOf(1), Integer.valueOf(0), coke, drinksPage);
    cokeItem.setMenuPageId(drinksPage.getId());
    MenuPageItemDAO.getInstance().save(cokeItem);
    
    MenuPageItem fantaItem = new MenuPageItem(Integer.valueOf(2), Integer.valueOf(0), fanta, drinksPage);
    fantaItem.setMenuPageId(drinksPage.getId());
    MenuPageItemDAO.getInstance().save(fantaItem);
    
    MenuPageItem spriteItem = new MenuPageItem(Integer.valueOf(3), Integer.valueOf(0), sprite, drinksPage);
    spriteItem.setMenuPageId(drinksPage.getId());
    MenuPageItemDAO.getInstance().save(spriteItem);
    
    MenuPageItem pepsiItem = new MenuPageItem(Integer.valueOf(0), Integer.valueOf(1), pepsi, drinksPage);
    pepsiItem.setMenuPageId(drinksPage.getId());
    MenuPageItemDAO.getInstance().save(pepsiItem);
    
    MenuPageDAO.getInstance().saveOrUpdate(drinksPage);
  }
  
  private void createRecipe() {
    Recepie recipe = new Recepie();
    recipe.setName("Beef Burger Recipe");
    recipe.setPortion(Double.valueOf(1.0D));
    recipe.setYield(Double.valueOf(1.0D));
    
    recipe.setCookingTime(Integer.valueOf(60));
    



    recipe.setMenuItem(beefBurger);
    recipe.setYieldUnit(unitPiece.getCode());
    recipe.setPortionUnit(unitPiece.getUniqueCode());
    
    RecepieItem breadItem = new RecepieItem();
    breadItem.setInventoryItem(bread);
    breadItem.setQuantity(Double.valueOf(1.0D));
    breadItem.setPercentage(Double.valueOf(100.0D));
    breadItem.setUnit(unitPiece);
    breadItem.setUnitCode(unitPiece.getCode());
    recipe.addTorecepieItems(breadItem);
    
    RecepieItem meatItem = new RecepieItem();
    meatItem.setInventoryItem(meat);
    meatItem.setPercentage(Double.valueOf(10.0D));
    meatItem.setQuantity(Double.valueOf(100.0D));
    meatItem.setUnit(unitGram);
    meatItem.setUnitCode(unitGram.getCode());
    recipe.addTorecepieItems(meatItem);
    
    RecepieDAO.getInstance().save(recipe);
    beefBurger.setDefaultRecipeId(recipe.getId());
    MenuItemDAO.getInstance().update(beefBurger);
  }
  
  private void createMenuModifiers()
  {
    americanCheese = new MenuModifier("americanCheese");
    americanCheese.setName("americanCheese");
    americanCheese.setPrice(Double.valueOf(2.99D));
    americanCheese.setTaxGroup(taxGroupnineteen32);
    MenuModifierDAO.getInstance().save(americanCheese);
    
    gouda = new MenuModifier("gouda");
    gouda.setName("gouda");
    gouda.setPrice(Double.valueOf(1.0D));
    gouda.setTaxGroup(taxGroupSix33);
    MenuModifierDAO.getInstance().save(gouda);
    
    chadder = new MenuModifier("chadder");
    chadder.setName("chadder");
    chadder.setPrice(Double.valueOf(1.25D));
    chadder.setTaxGroup(taxGroupSix33);
    MenuModifierDAO.getInstance().save(chadder);
    
    brie = new MenuModifier("brie");
    brie.setName("brie");
    brie.setPrice(Double.valueOf(2.0D));
    brie.setTaxGroup(taxGroupSix33);
    MenuModifierDAO.getInstance().save(brie);
    

    cheeseModifierGroup = new ModifierGroup();
    cheeseModifierGroup.setName("Cheese");
    cheeseModifierGroup.addTomodifiers(americanCheese);
    cheeseModifierGroup.addTomodifiers(gouda);
    cheeseModifierGroup.addTomodifiers(chadder);
    cheeseModifierGroup.addTomodifiers(brie);
    ModifierGroupDAO.getInstance().save(cheeseModifierGroup);
    

    lemon = new MenuModifier("lemon");
    lemon.setName("lemon");
    lemon.setPrice(Double.valueOf(0.79D));
    lemon.setTaxGroup(taxGroupSix01);
    MenuModifierDAO.getInstance().save(lemon);
    
    salt = new MenuModifier("salt");
    salt.setName("salt");
    salt.setPrice(Double.valueOf(0.27D));
    MenuModifierDAO.getInstance().save(salt);
    

    bevarageModifierGroup = new ModifierGroup();
    bevarageModifierGroup.setName("Beverages");
    bevarageModifierGroup.addTomodifiers(lemon);
    bevarageModifierGroup.addTomodifiers(salt);
    ModifierGroupDAO.getInstance().save(bevarageModifierGroup);
    

    mashroom = new MenuModifier("mashroom");
    mashroom.setName("mashroom");
    mashroom.setPrice(Double.valueOf(1.0D));
    MenuModifierDAO.getInstance().save(mashroom);
    
    pepparoni = new MenuModifier("pepparoni");
    pepparoni.setName("Pepparoni");
    pepparoni.setPrice(Double.valueOf(3.33D));
    pepparoni.setTaxGroup(taxGroupSix01);
    MenuModifierDAO.getInstance().save(pepparoni);
    
    sausage = new MenuModifier("sausage");
    sausage.setName("Sausage");
    sausage.setPrice(Double.valueOf(1.25D));
    MenuModifierDAO.getInstance().save(sausage);
    
    onionsModifier = new MenuModifier("onion");
    onionsModifier.setName("Onions");
    onionsModifier.setExtraPrice(Double.valueOf(2.75D));
    
    onionsModifier.setShouldPrintToKitchen(Boolean.valueOf(true));
    onionsModifier.setPrice(Double.valueOf(2.0D));
    MenuModifierDAO.getInstance().save(onionsModifier);
    

    toppingsModifierGroup = new ModifierGroup();
    toppingsModifierGroup.setName("Toppings");
    toppingsModifierGroup.addTomodifiers(mashroom);
    toppingsModifierGroup.addTomodifiers(pepparoni);
    toppingsModifierGroup.addTomodifiers(sausage);
    toppingsModifierGroup.addTomodifiers(onionsModifier);
    ModifierGroupDAO.getInstance().save(toppingsModifierGroup);
  }
  

  private void createCheeseMenuModifierSpec(MenuItem menuItem)
  {
    MenuItemModifierSpec cheeseModifierGroupSpec = new MenuItemModifierSpec();
    cheeseModifierGroupSpec.setModifierGroup(cheeseModifierGroup);
    cheeseModifierGroupSpec.setName("Cheese");
    cheeseModifierGroupSpec.setMinQuantity(Integer.valueOf(1));
    cheeseModifierGroupSpec.setMaxQuantity(Integer.valueOf(2));
    cheeseModifierGroupSpec.setSortOrder(Integer.valueOf(0));
    cheeseModifierGroupSpec.setEnable(Boolean.valueOf(true));
    cheeseModifierGroupSpec.setJumpGroup(Boolean.valueOf(true));
    MenuItemModifierSpecDAO.getInstance().save(cheeseModifierGroupSpec);
    
    MenuItemModifierPage cheeseModifierSpecPage = new MenuItemModifierPage();
    cheeseModifierSpecPage.setName("Page 1");
    cheeseModifierSpecPage.setButtonHeight(Integer.valueOf(100));
    cheeseModifierSpecPage.setButtonWidth(Integer.valueOf(100));
    cheeseModifierSpecPage.setRows(Integer.valueOf(4));
    cheeseModifierSpecPage.setCols(Integer.valueOf(4));
    cheeseModifierSpecPage.setVisible(Boolean.valueOf(true));
    cheeseModifierSpecPage.setSortOrder(Integer.valueOf(1));
    cheeseModifierSpecPage.setFlixibleButtonSize(Boolean.valueOf(false));
    cheeseModifierSpecPage.setModifierSpecId(cheeseModifierGroupSpec.getId());
    MenuItemModifierPageDAO.getInstance().save(cheeseModifierSpecPage);
    
    MenuItemModifierPageItem cheesePageItem = new MenuItemModifierPageItem(Integer.valueOf(0), Integer.valueOf(0), americanCheese, cheeseModifierSpecPage);
    MenuItemModifierPageItemDAO.getInstance().saveOrUpdate(cheesePageItem);
    cheeseModifierSpecPage.addTopageItems(cheesePageItem);
    
    MenuItemModifierPageItem goudaPageItem = new MenuItemModifierPageItem(Integer.valueOf(1), Integer.valueOf(0), gouda, cheeseModifierSpecPage);
    MenuItemModifierPageItemDAO.getInstance().saveOrUpdate(goudaPageItem);
    cheeseModifierSpecPage.addTopageItems(goudaPageItem);
    
    MenuItemModifierPageItem briePageItem = new MenuItemModifierPageItem(Integer.valueOf(2), Integer.valueOf(0), brie, cheeseModifierSpecPage);
    MenuItemModifierPageItemDAO.getInstance().saveOrUpdate(briePageItem);
    cheeseModifierSpecPage.addTopageItems(briePageItem);
    
    MenuItemModifierPageItem cheddarPageItem = new MenuItemModifierPageItem(Integer.valueOf(3), Integer.valueOf(0), chadder, cheeseModifierSpecPage);
    MenuItemModifierPageItemDAO.getInstance().saveOrUpdate(cheddarPageItem);
    cheeseModifierSpecPage.addTopageItems(cheddarPageItem);
    
    cheeseModifierGroupSpec.addTomodifierPages(cheeseModifierSpecPage);
    



    menuItem.addTomenuItemModiferSpecs(cheeseModifierGroupSpec);
  }
  
  private void createBeverageMenuModifierSpec(MenuItem menuItem)
  {
    MenuItemModifierSpec beverageModifierGroupSpec = new MenuItemModifierSpec();
    beverageModifierGroupSpec.setModifierGroup(toppingsModifierGroup);
    beverageModifierGroupSpec.setName("Beverage");
    beverageModifierGroupSpec.setMinQuantity(Integer.valueOf(1));
    beverageModifierGroupSpec.setMaxQuantity(Integer.valueOf(2));
    beverageModifierGroupSpec.setSortOrder(Integer.valueOf(0));
    beverageModifierGroupSpec.setEnable(Boolean.valueOf(true));
    beverageModifierGroupSpec.setJumpGroup(Boolean.valueOf(true));
    MenuItemModifierSpecDAO.getInstance().save(beverageModifierGroupSpec);
    

    MenuItemModifierPage beverageModifierSpecPage = new MenuItemModifierPage();
    beverageModifierSpecPage.setName("Page 1");
    beverageModifierSpecPage.setButtonHeight(Integer.valueOf(100));
    beverageModifierSpecPage.setButtonWidth(Integer.valueOf(100));
    beverageModifierSpecPage.setRows(Integer.valueOf(4));
    beverageModifierSpecPage.setCols(Integer.valueOf(4));
    beverageModifierSpecPage.setVisible(Boolean.valueOf(true));
    beverageModifierSpecPage.setSortOrder(Integer.valueOf(1));
    beverageModifierSpecPage.setFlixibleButtonSize(Boolean.valueOf(false));
    beverageModifierSpecPage.setModifierSpecId(beverageModifierGroupSpec.getId());
    MenuItemModifierPageDAO.getInstance().saveOrUpdate(beverageModifierSpecPage);
    

    MenuItemModifierPageItem saltPageItem = new MenuItemModifierPageItem(Integer.valueOf(0), Integer.valueOf(0), salt, beverageModifierSpecPage);
    MenuItemModifierPageItemDAO.getInstance().saveOrUpdate(saltPageItem);
    beverageModifierSpecPage.addTopageItems(saltPageItem);
    
    MenuItemModifierPageItem lemonPageItem = new MenuItemModifierPageItem(Integer.valueOf(1), Integer.valueOf(0), lemon, beverageModifierSpecPage);
    MenuItemModifierPageItemDAO.getInstance().saveOrUpdate(lemonPageItem);
    beverageModifierSpecPage.addTopageItems(lemonPageItem);
    
    beverageModifierGroupSpec.addTomodifierPages(beverageModifierSpecPage);
    menuItem.addTomenuItemModiferSpecs(beverageModifierGroupSpec);
  }
  

  private void createToppingsMenuModifierSpec(MenuItem menuItem)
  {
    MenuItemModifierSpec toppingsModifierGroupSpec = new MenuItemModifierSpec();
    toppingsModifierGroupSpec.setModifierGroup(toppingsModifierGroup);
    toppingsModifierGroupSpec.setName("toppings");
    toppingsModifierGroupSpec.setMinQuantity(Integer.valueOf(1));
    toppingsModifierGroupSpec.setMaxQuantity(Integer.valueOf(2));
    toppingsModifierGroupSpec.setSortOrder(Integer.valueOf(0));
    toppingsModifierGroupSpec.setEnable(Boolean.valueOf(true));
    toppingsModifierGroupSpec.setJumpGroup(Boolean.valueOf(true));
    MenuItemModifierSpecDAO.getInstance().save(toppingsModifierGroupSpec);
    

    MenuItemModifierPage toppingsModifierSpecPage = new MenuItemModifierPage();
    toppingsModifierSpecPage.setName("Page 1");
    toppingsModifierSpecPage.setButtonHeight(Integer.valueOf(100));
    toppingsModifierSpecPage.setButtonWidth(Integer.valueOf(100));
    toppingsModifierSpecPage.setRows(Integer.valueOf(4));
    toppingsModifierSpecPage.setCols(Integer.valueOf(4));
    toppingsModifierSpecPage.setVisible(Boolean.valueOf(true));
    toppingsModifierSpecPage.setSortOrder(Integer.valueOf(1));
    toppingsModifierSpecPage.setFlixibleButtonSize(Boolean.valueOf(false));
    toppingsModifierSpecPage.setModifierSpecId(toppingsModifierGroupSpec.getId());
    MenuItemModifierPageDAO.getInstance().saveOrUpdate(toppingsModifierSpecPage);
    

    MenuItemModifierPageItem mashroomPageItem = new MenuItemModifierPageItem(Integer.valueOf(0), Integer.valueOf(0), mashroom, toppingsModifierSpecPage);
    MenuItemModifierPageItemDAO.getInstance().saveOrUpdate(mashroomPageItem);
    toppingsModifierSpecPage.addTopageItems(mashroomPageItem);
    
    MenuItemModifierPageItem pepproniPageItem = new MenuItemModifierPageItem(Integer.valueOf(1), Integer.valueOf(0), pepparoni, toppingsModifierSpecPage);
    MenuItemModifierPageItemDAO.getInstance().saveOrUpdate(pepproniPageItem);
    toppingsModifierSpecPage.addTopageItems(pepproniPageItem);
    
    MenuItemModifierPageItem sausagePageItem = new MenuItemModifierPageItem(Integer.valueOf(2), Integer.valueOf(0), sausage, toppingsModifierSpecPage);
    MenuItemModifierPageItemDAO.getInstance().saveOrUpdate(sausagePageItem);
    toppingsModifierSpecPage.addTopageItems(sausagePageItem);
    
    MenuItemModifierPageItem onionPageItem = new MenuItemModifierPageItem(Integer.valueOf(3), Integer.valueOf(0), onionsModifier, toppingsModifierSpecPage);
    MenuItemModifierPageItemDAO.getInstance().saveOrUpdate(onionPageItem);
    toppingsModifierSpecPage.addTopageItems(onionPageItem);
    
    toppingsModifierGroupSpec.addTomodifierPages(toppingsModifierSpecPage);
    menuItem.addTomenuItemModiferSpecs(toppingsModifierGroupSpec);
  }
  
  private void createMultipliers() {
    Multiplier regular = new Multiplier("regular");
    regular.setRate(Double.valueOf(100.0D));
    regular.setTicketPrefix("Regular");
    regular.setDefaultMultiplier(Boolean.valueOf(true));
    MultiplierDAO.getInstance().save(regular);
    
    Multiplier tripple = new Multiplier("tripple");
    tripple.setRate(Double.valueOf(300.0D));
    tripple.setTicketPrefix("Tripple");
    MultiplierDAO.getInstance().save(tripple);
    
    Multiplier quarter = new Multiplier("quarter");
    quarter.setRate(Double.valueOf(25.0D));
    quarter.setTicketPrefix("Quarter");
    MultiplierDAO.getInstance().save(quarter);
    
    Multiplier half = new Multiplier("half");
    half.setRate(Double.valueOf(50.0D));
    half.setTicketPrefix("Half");
    MultiplierDAO.getInstance().save(half);
    
    Multiplier extra = new Multiplier("extra");
    extra.setRate(Double.valueOf(200.0D));
    extra.setTicketPrefix("extra");
    MultiplierDAO.getInstance().save(extra);
  }
  
  private void createShifts() {
    DayPart regularShift = new DayPart();
    regularShift.setId("regular");
    regularShift.setName("Regular");
    Date shiftStartTime = ShiftUtil.buildShiftStartTime(10, 0, 0, 6, 0, 1);
    Date shiftEndTime = ShiftUtil.buildShiftEndTime(10, 0, 0, 6, 0, 1);
    regularShift.setStartTime(shiftStartTime);
    regularShift.setEndTime(shiftEndTime);
    long length = Math.abs(shiftStartTime.getTime() - shiftEndTime.getTime());
    regularShift.setShiftLength(Long.valueOf(length));
    DayPartDAO.getInstance().save(regularShift);
    
    DayPart eveningShift = new DayPart();
    eveningShift.setId("evening");
    eveningShift.setName("Evening");
    shiftStartTime = ShiftUtil.buildShiftStartTime(6, 0, 1, 2, 0, 0);
    shiftEndTime = ShiftUtil.buildShiftEndTime(6, 0, 1, 2, 0, 0);
    eveningShift.setStartTime(shiftStartTime);
    eveningShift.setEndTime(shiftEndTime);
    length = Math.abs(shiftStartTime.getTime() - shiftEndTime.getTime());
    eveningShift.setShiftLength(Long.valueOf(length));
    DayPartDAO.getInstance().save(eveningShift);
    
    DayPart nightShift = new DayPart();
    nightShift.setId("night");
    nightShift.setName("Night");
    shiftStartTime = ShiftUtil.buildShiftStartTime(2, 0, 0, 10, 0, 0);
    shiftEndTime = ShiftUtil.buildShiftEndTime(2, 0, 0, 10, 0, 0);
    nightShift.setStartTime(shiftStartTime);
    nightShift.setEndTime(shiftEndTime);
    length = Math.abs(shiftStartTime.getTime() - shiftEndTime.getTime());
    nightShift.setShiftLength(Long.valueOf(length));
    DayPartDAO.getInstance().save(nightShift);
  }
  
  private void createAttributes() {
    AttributeGroup sizeGroup = new AttributeGroup();
    sizeGroup.setName("Size");
    
    DatabaseUtil.smallSize = new Attribute();
    DatabaseUtil.smallSize.setName(Messages.getString("DatabaseUtil.21"));
    DatabaseUtil.smallSize.setSortOrder(Integer.valueOf(0));
    DatabaseUtil.smallSize.setGroup(sizeGroup);
    sizeGroup.addToattributes(DatabaseUtil.smallSize);
    
    DatabaseUtil.mediumSize = new Attribute();
    DatabaseUtil.mediumSize.setName(Messages.getString("DatabaseUtil.22"));
    DatabaseUtil.mediumSize.setSortOrder(Integer.valueOf(1));
    DatabaseUtil.mediumSize.setGroup(sizeGroup);
    sizeGroup.addToattributes(DatabaseUtil.mediumSize);
    
    DatabaseUtil.largeSize = new Attribute();
    DatabaseUtil.largeSize.setName(Messages.getString("DatabaseUtil.23"));
    DatabaseUtil.largeSize.setSortOrder(Integer.valueOf(2));
    DatabaseUtil.largeSize.setGroup(sizeGroup);
    sizeGroup.addToattributes(DatabaseUtil.largeSize);
    
    AttributeGroup crustGroup = new AttributeGroup();
    crustGroup.setName("Crust");
    
    Attribute pan = new Attribute();
    pan.setName(Messages.getString("DatabaseUtil.24"));
    pan.setSortOrder(Integer.valueOf(0));
    pan.setGroup(crustGroup);
    crustGroup.addToattributes(pan);
    
    Attribute handTossed = new Attribute();
    handTossed.setName(Messages.getString("DatabaseUtil.25"));
    handTossed.setSortOrder(Integer.valueOf(1));
    handTossed.setGroup(crustGroup);
    crustGroup.addToattributes(handTossed);
    
    AttributeGroupDAO.getInstance().saveOrUpdate(sizeGroup);
    AttributeGroupDAO.getInstance().saveOrUpdate(crustGroup);
  }
  
  private void doCreateVariantForCoke(MenuItem item, Session session)
  {
    MenuItem largeItem = new MenuItem();
    largeItem.setId("large-" + item.getId());
    largeItem.setName("Large-" + item.getName());
    largeItem.setParentMenuItemId(item.getId());
    largeItem.setUnit(item.getUnit());
    largeItem.setVariant(Boolean.valueOf(true));
    largeItem.setPrice(Double.valueOf(4.89D));
    largeItem.setInventoryItem(item.isInventoryItem());
    largeItem.setAttributes(Arrays.asList(new Attribute[] { DatabaseUtil.largeSize }));
    MenuItemDAO.getInstance().save(largeItem, session);
    
    MenuItem mediumItem = new MenuItem();
    mediumItem.setId("medium-" + item.getId());
    mediumItem.setName("Medium-" + item.getName());
    mediumItem.setParentMenuItemId(item.getId());
    mediumItem.setUnit(item.getUnit());
    mediumItem.setVariant(Boolean.valueOf(true));
    mediumItem.setPrice(Double.valueOf(2.97D));
    mediumItem.setInventoryItem(item.isInventoryItem());
    mediumItem.setAttributes(Arrays.asList(new Attribute[] { DatabaseUtil.mediumSize }));
    MenuItemDAO.getInstance().save(mediumItem, session);
    
    MenuItem smallItem = new MenuItem();
    smallItem.setId("small-" + item.getId());
    smallItem.setName("Small-" + item.getName());
    smallItem.setParentMenuItemId(item.getId());
    smallItem.setUnit(item.getUnit());
    smallItem.setVariant(Boolean.valueOf(true));
    smallItem.setPrice(Double.valueOf(1.32D));
    smallItem.setInventoryItem(item.isInventoryItem());
    smallItem.setAttributes(Arrays.asList(new Attribute[] { DatabaseUtil.smallSize }));
    MenuItemDAO.getInstance().save(smallItem, session);
  }
  


  private void doCreateVariantForPepsi(MenuItem item, Session session)
  {
    MenuItem largeItem = new MenuItem();
    largeItem.setId("large-" + item.getId());
    largeItem.setName("Large-" + item.getName());
    largeItem.setParentMenuItemId(item.getId());
    largeItem.setUnit(item.getUnit());
    largeItem.setVariant(Boolean.valueOf(true));
    largeItem.setPrice(Double.valueOf(2.33D));
    largeItem.setInventoryItem(item.isInventoryItem());
    largeItem.setAttributes(Arrays.asList(new Attribute[] { DatabaseUtil.largeSize }));
    MenuItemDAO.getInstance().save(largeItem, session);
    
    MenuItem mediumItem = new MenuItem();
    mediumItem.setId("medium-" + item.getId());
    mediumItem.setName("Medium-" + item.getName());
    mediumItem.setParentMenuItemId(item.getId());
    mediumItem.setUnit(item.getUnit());
    mediumItem.setVariant(Boolean.valueOf(true));
    mediumItem.setPrice(Double.valueOf(1.67D));
    mediumItem.setInventoryItem(item.isInventoryItem());
    mediumItem.setAttributes(Arrays.asList(new Attribute[] { DatabaseUtil.mediumSize }));
    MenuItemDAO.getInstance().save(mediumItem, session);
  }
  
  private void createInventoryData() {
    InventoryUnitGroup group = new InventoryUnitGroup();
    group.setName("Measurement");
    InventoryUnitGroupDAO.getInstance().save(group);
    
    InventoryUnitGroup weight = new InventoryUnitGroup();
    weight.setName("Weight");
    InventoryUnitGroupDAO.getInstance().save(weight);
    
    unitPiece = new InventoryUnit();
    unitPiece.setBaseUnit(Boolean.valueOf(true));
    unitPiece.setCode("pc");
    unitPiece.setName("Piece");
    unitPiece.setConversionRate(Double.valueOf(1.0D));
    unitPiece.setUnitGroupId(group.getId());
    InventoryUnitDAO.getInstance().save(unitPiece);
    
    unitGram = new InventoryUnit();
    unitGram.setCode("g");
    unitGram.setName("gram");
    unitGram.setConversionRate(Double.valueOf(1000.0D));
    unitGram.setUnitGroupId(weight.getId());
    InventoryUnitDAO.getInstance().save(unitGram);
    
    unitKg = new InventoryUnit();
    unitKg.setBaseUnit(Boolean.valueOf(true));
    unitKg.setCode("kg");
    unitKg.setName("Kilo gram");
    unitKg.setConversionRate(Double.valueOf(1.0D));
    unitKg.setUnitGroupId(weight.getId());
    InventoryUnitDAO.getInstance().save(unitKg);
    
    PackagingUnit packagingUnit = new PackagingUnit();
    packagingUnit.setCode("box");
    packagingUnit.setName("Box");
    PackagingUnitDAO.getInstance().save(packagingUnit);
    
    packagingUnit = new PackagingUnit();
    packagingUnit.setCode("cartoon");
    packagingUnit.setName("Cartoon");
    PackagingUnitDAO.getInstance().saveOrUpdate(packagingUnit);
    
    InventoryLocation location = new InventoryLocation("store");
    location.setName("Store");
    location.setDefaultInLocation(Boolean.valueOf(true));
    location.setDefaultOutLocation(Boolean.valueOf(true));
    location.setSortOrder(Integer.valueOf(0));
    location.setVisible(Boolean.valueOf(true));
    location.setRoot(Boolean.valueOf(true));
    InventoryLocationDAO.getInstance().save(location);
    
    InventoryVendor vendor = new InventoryVendor("cash");
    vendor.setName("Cash");
    vendor.setVisible(Boolean.valueOf(true));
    InventoryVendorDAO.getInstance().save(vendor);
  }
  
  private void createDefaultFloor()
  {
    ShopFloor floor = new ShopFloor();
    floor.setName("Main Floor");
    
    List<ShopTable> tableList = ShopTableDAO.getInstance().findAll();
    Set<ShopTable> shopTables = new HashSet();
    
    if ((tableList != null) && (!tableList.isEmpty())) {
      for (int i = 0; i < tableList.size(); i++) {
        ShopTable shopTable = (ShopTable)tableList.get(i);
        
        shopTable.setX((Integer)getPositionX().get(i));
        shopTable.setY((Integer)getPositionY().get(i));
        
        ShopTableDAO.getInstance().saveOrUpdate(shopTable);
        
        shopTables.add(shopTable);
        
        if (i == 13) {
          break;
        }
        
      }
      
    } else {
      for (int i = 0; i < getPositionX().size(); i++) {
        shopTables.add(new ShopTable(floor, (Integer)getPositionX().get(i), (Integer)getPositionY().get(i), Integer.valueOf(i + 1)));
      }
    }
    
    floor.setTables(shopTables);
    
    Session session = ShopFloorDAO.getInstance().getSession();
    Transaction transaction = session.beginTransaction();
    session.save(floor);
    transaction.commit();
    session.close();
  }
  
  private List<Integer> getPositionX() {
    List<Integer> positionX = new ArrayList();
    positionX.add(Integer.valueOf(19));
    positionX.add(Integer.valueOf(161));
    positionX.add(Integer.valueOf(309));
    positionX.add(Integer.valueOf(17));
    positionX.add(Integer.valueOf(162));
    positionX.add(Integer.valueOf(318));
    positionX.add(Integer.valueOf(18));
    positionX.add(Integer.valueOf(161));
    positionX.add(Integer.valueOf(330));
    positionX.add(Integer.valueOf(22));
    positionX.add(Integer.valueOf(169));
    positionX.add(Integer.valueOf(329));
    positionX.add(Integer.valueOf(487));
    positionX.add(Integer.valueOf(661));
    
    return positionX;
  }
  
  private List<Integer> getPositionY() {
    List<Integer> positionY = new ArrayList();
    positionY.add(Integer.valueOf(12));
    positionY.add(Integer.valueOf(12));
    positionY.add(Integer.valueOf(14));
    positionY.add(Integer.valueOf(132));
    positionY.add(Integer.valueOf(136));
    positionY.add(Integer.valueOf(140));
    positionY.add(Integer.valueOf(258));
    positionY.add(Integer.valueOf(258));
    positionY.add(Integer.valueOf(260));
    positionY.add(Integer.valueOf(382));
    positionY.add(Integer.valueOf(381));
    positionY.add(Integer.valueOf(387));
    positionY.add(Integer.valueOf(391));
    positionY.add(Integer.valueOf(390));
    
    return positionY;
  }
  
  private void createManagerUser(UserType manager, UserType cashier, UserType server) {
    User managerUser = new User();
    managerUser.setId("124");
    managerUser.setEncryptedPassword("2222");
    managerUser.setFirstName("Manager");
    managerUser.setLastName("M");
    managerUser.setType(manager);
    managerUser.setRoot(Boolean.valueOf(true));
    managerUser.setActive(Boolean.valueOf(true));
    managerUser.setStaffBank(Boolean.valueOf(true));
    
    User cashierUser = new User();
    cashierUser.setId("981");
    cashierUser.setFirstName("Manager");
    cashierUser.setLastName("M");
    cashierUser.setType(cashier);
    cashierUser.setParentUser(managerUser);
    cashierUser.setActive(Boolean.valueOf(true));
    cashierUser.setStaffBank(Boolean.valueOf(true));
    
    User serverUser = new User();
    serverUser.setId("890");
    serverUser.setFirstName("Manager");
    serverUser.setLastName("M");
    serverUser.setType(server);
    serverUser.setParentUser(managerUser);
    serverUser.setActive(Boolean.valueOf(true));
    serverUser.setStaffBank(Boolean.valueOf(true));
    serverUser.setAutoStartStaffBank(Boolean.valueOf(true));
    
    UserDAO dao = UserDAO.getInstance();
    dao.save(managerUser);
    dao.save(cashierUser);
    dao.save(serverUser);
  }
}
