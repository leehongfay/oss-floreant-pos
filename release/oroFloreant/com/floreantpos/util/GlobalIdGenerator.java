package com.floreantpos.util;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Random;
import org.apache.commons.lang.StringUtils;
import org.hibernate.engine.spi.SessionImplementor;

public class GlobalIdGenerator implements org.hibernate.id.IdentifierGenerator
{
  public GlobalIdGenerator() {}
  
  public Serializable generate(SessionImplementor session, Object object) throws org.hibernate.HibernateException
  {
    Class<? extends Object> clazz = object.getClass();
    Serializable generatedId = null;
    try {
      Method method = clazz.getMethod("getId", (Class[])null);
      if (method != null) {
        Object id = method.invoke(object, (Object[])null);
        if (id != null) {
          generatedId = (Serializable)id;
        }
      }
    }
    catch (Exception localException) {}
    
    if (generatedId == null) {
      generatedId = generate();
    }
    









    try
    {
      Method methodGetBarcode = clazz.getMethod("getBarcode", (Class[])null);
      if (methodGetBarcode != null) {
        String barcode = (String)methodGetBarcode.invoke(object, (Object[])null);
        if (StringUtils.isEmpty(barcode)) {
          barcode = generatedId.toString();
          Method methodSetBarcode = clazz.getMethod("setBarcode", new Class[] { String.class });
          methodSetBarcode.invoke(object, new Object[] { barcode });
        }
      }
    }
    catch (Exception localException1) {}
    
    return generatedId;
  }
  
  public String generate() {
    return generateGlobalId();
  }
  
  public static String generateGlobalId() {
    long currentTimeMillis = System.currentTimeMillis();
    Random random = new Random();
    for (int i = 0; i < 3; i++) {
      currentTimeMillis += random.nextInt();
    }
    String idString = String.valueOf(currentTimeMillis);
    int length = idString.length();
    if (length == 16) {
      return idString;
    }
    if (length > 16) {
      return idString.substring(0, 16);
    }
    for (int i = 0; i < 16 - length; i++) {
      char c = (char)(random.nextInt(26) + 97);
      idString = c + idString;
    }
    
    return idString;
  }
}
