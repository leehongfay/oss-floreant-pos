package com.floreantpos.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.ui.kitchendisplay.KitchenDisplayWindow;
import java.awt.Window;



















public class ShowKitchenDisplayAction
  extends PosAction
{
  public ShowKitchenDisplayAction()
  {
    super(POSConstants.KITCHEN_DISPLAY_BUTTON_TEXT);
  }
  
  public void execute()
  {
    Window[] windows = Window.getWindows();
    for (Window window : windows) {
      if ((window instanceof KitchenDisplayWindow)) {
        window.setVisible(true);
        window.toFront();
        return;
      }
    }
    
    KitchenDisplayWindow window = new KitchenDisplayWindow();
    window.setExtendedState(6);
    window.setVisible(true);
  }
}
