package com.floreantpos.actions;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.PasswordEntryDialog;
import com.floreantpos.ui.views.IView;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.ui.views.order.TicketView;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.Icon;
import org.apache.commons.lang.StringUtils;
import org.hibernate.StaleObjectStateException;
import org.hibernate.exception.JDBCConnectionException;


















public abstract class ViewChangeAction
  extends PosAction
{
  private boolean visible = true;
  
  protected UserPermission requiredPermission;
  
  public ViewChangeAction() {}
  
  public ViewChangeAction(String name)
  {
    super(name);
  }
  
  public ViewChangeAction(Icon icon) {
    super(null, icon);
  }
  
  public ViewChangeAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public ViewChangeAction(String name, UserPermission requiredPermission) {
    super(name);
    this.requiredPermission = requiredPermission;
  }
  
  public ViewChangeAction(Icon icon, UserPermission requiredPermission) {
    super(null, icon);
    this.requiredPermission = requiredPermission;
  }
  
  public UserPermission getRequiredPermission() {
    return requiredPermission;
  }
  
  public void setRequiredPermission(UserPermission requiredPermission) {
    this.requiredPermission = requiredPermission;
  }
  
  public void actionPerformed(ActionEvent e)
  {
    try {
      OrderView orderView = OrderView.getInstance();
      
      Ticket currentTicket = orderView.getCurrentTicket();
      if ((currentTicket == null) || ((currentTicket.getId() == null) && (currentTicket.getTicketItems().isEmpty()))) {
        execute();
        return;
      }
      
      boolean version = TerminalDAO.getInstance().isVersionEqual(Ticket.class, currentTicket.getId(), currentTicket.getVersion());
      if (!version) {
        execute();
        return;
      }
      
      User user = Application.getCurrentUser();
      if (user == null) {
        POSMessageDialog.showError("Please login first.");
        return;
      }
      
      IView currentView = RootView.getInstance().getCurrentView();
      if ((currentView != null) && (currentView.getViewName().equals("ORDER_VIEW")) && (!orderView.getTicketView().isAllowToLogOut())) {
        POSMessageDialog.showError(Messages.getString("ViewChangeAction.0"));
        return;
      }
      saveTicketIfNeeded();
      if (requiredPermission == null) {
        execute();
        return;
      }
      
      if (!user.hasPermission(requiredPermission)) {
        String password = PasswordEntryDialog.show(Application.getPosWindow(), Messages.getString("PosAction.0"));
        if (StringUtils.isEmpty(password)) {
          return;
        }
        
        User user2 = UserDAO.getInstance().findUserBySecretKey(password);
        if (user2 == null) {
          POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("PosAction.1"));
          return;
        }
        if (!user2.hasPermission(requiredPermission)) {
          POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("PosAction.2"));
          return;
        }
      }
      execute();
    } catch (JDBCConnectionException e2) {
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Database connection lost. Do you want to continue?", POSConstants.CONFIRM) == 0)
      {
        execute();
      }
    } catch (Exception exception) {
      POSMessageDialog.showError(Application.getPosWindow(), exception.getMessage(), exception);
    }
  }
  
  private void saveTicketIfNeeded() {
    OrderView orderView = OrderView.getInstance();
    if (!orderView.isVisible()) {
      return;
    }
    Ticket currentTicket = orderView.getCurrentTicket();
    if (currentTicket == null)
      return;
    if ((!currentTicket.getTicketItems().isEmpty()) && 
      (hasNewItem(currentTicket)) && 
      (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), Messages.getString("ViewChangeAction.1"), 
      Messages.getString("ViewChangeAction.2")) == 0)) {
      try
      {
        orderView.getTicketView().saveTicketIfNeeded();
      }
      catch (StaleObjectStateException localStaleObjectStateException) {}
    }
  }
  

  private boolean hasNewItem(Ticket currentTicket)
  {
    for (TicketItem item : currentTicket.getTicketItems()) {
      if (item.getId() == null) {
        return true;
      }
    }
    return false;
  }
  
  public abstract void execute();
  
  public boolean isVisible() {
    return visible;
  }
  
  public void setVisible(boolean visible) {
    this.visible = visible;
  }
}
