package com.floreantpos.actions;

import com.floreantpos.Messages;
import com.floreantpos.customer.CustomerSelectorDialog;
import com.floreantpos.customer.DefaultCustomerListView;
import com.floreantpos.mailservices.MailService;
import com.floreantpos.main.Application;
import com.floreantpos.model.Customer;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.ui.dialog.EmailSelectionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;

















public abstract class EmailSendAction
  extends PosAction
{
  private Ticket ticket;
  
  public EmailSendAction()
  {
    super("EMAIL");
  }
  
  public abstract Ticket getTicket();
  
  public void execute()
  {
    try {
      ticket = getTicket();
      if (ticket == null)
        return;
      Customer customer = getCustomer(ticket);
      if (customer == null)
        return;
      EmailSelectionDialog dialog = new EmailSelectionDialog(Messages.getString("EmailSendAction.0"), customer.getEmail(), customer.getEmail2())
      {
        public boolean doSendEmail(String email)
        {
          try {
            MailService.sendTicket(email, ticket);
            POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("EmailSendAction.1") + email + "\"");
            return true;
          } catch (Exception e) {
            POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e); }
          return false;
        }
        
      };
      dialog.pack();
      dialog.open();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private Customer getCustomer(Ticket ticket) throws Exception {
    Customer customer = null;
    String customerId = ticket.getCustomerId();
    if (customerId == null) {
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), Messages.getString("EmailSendAction.3"), 
        Messages.getString("EmailSendAction.4"));
      
      if (option != 0) {
        return null;
      }
      
      CustomerSelectorDialog dialog = new CustomerSelectorDialog(new DefaultCustomerListView());
      dialog.setCreateNewTicket(false);
      dialog.updateView(true);
      dialog.openUndecoratedFullScreen();
      
      if (dialog.isCanceled()) {
        return null;
      }
      customer = dialog.getSelectedCustomer();
      if (customer != null) {
        ticket.setCustomer(customer);
        TicketDAO.getInstance().saveOrUpdate(ticket);
      }
    }
    else {
      customer = CustomerDAO.getInstance().get(customerId);
    }
    return customer;
  }
}
