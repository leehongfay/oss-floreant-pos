package com.floreantpos.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.ui.views.order.actions.DataChangeListener;

public class TicketEditAction extends PosAction
{
  public TicketEditAction()
  {
    super(POSConstants.EDIT_TICKET_BUTTON_TEXT);
  }
  
  public TicketEditAction(DataChangeListener listener) {
    super(POSConstants.EDIT_TICKET_BUTTON_TEXT, listener);
  }
  
  public void execute()
  {
    try {
      Object selectedObject = getSelectedObject();
      if (selectedObject == null) {
        return;
      }
      Ticket ticket = null;
      if ((selectedObject instanceof Ticket)) {
        ticket = (Ticket)selectedObject;
      }
      else {
        ticket = com.floreantpos.services.TicketService.getTicket((String)selectedObject);
      }
      if (ticket.isVoided().booleanValue()) {
        POSMessageDialog.showMessage(Application.getPosWindow(), POSConstants.TICKET_IS_VOIDED);
        return;
      }
      if (!hasPermissionToAccessTicket(ticket))
        return;
      Ticket ticketToEdit = TicketDAO.getInstance().loadFullTicket(ticket.getId());
      if (ticketToEdit.isClosed().booleanValue()) {
        ticketToEdit.setClosed(Boolean.valueOf(false));
        ticketToEdit.setReOpened(Boolean.valueOf(true));
      }
      OrderView.getInstance().setCurrentTicket(ticketToEdit);
      RootView.getInstance().showView("ORDER_VIEW");
    } catch (PosException e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
}
