package com.floreantpos.actions;

import com.floreantpos.IconFactory;
import com.floreantpos.ui.views.SwitchboardOtherFunctionsView;
import com.floreantpos.ui.views.order.RootView;




















public class ShowSpecialFunctionsAction
  extends ViewChangeAction
{
  public ShowSpecialFunctionsAction() {}
  
  public ShowSpecialFunctionsAction(boolean showText, boolean showIcon)
  {
    if (showIcon) {
      putValue("SmallIcon", IconFactory.getIcon("Content-41.png"));
    }
  }
  
  public void execute()
  {
    SwitchboardOtherFunctionsView view = SwitchboardOtherFunctionsView.getInstance();
    RootView.getInstance().showView(view);
  }
}
