package com.floreantpos.actions;

import com.floreantpos.POSConstants;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JDialog;

















public class CloseDialogAction
  extends AbstractAction
{
  private JDialog dialog;
  
  public CloseDialogAction(JDialog dialog)
  {
    super(POSConstants.CLOSE.toUpperCase());
    
    this.dialog = dialog;
  }
  
  public CloseDialogAction(JDialog dialog, String title) {
    super(title);
    this.dialog = dialog;
  }
  
  public void actionPerformed(ActionEvent e)
  {
    dialog.dispose();
  }
}
