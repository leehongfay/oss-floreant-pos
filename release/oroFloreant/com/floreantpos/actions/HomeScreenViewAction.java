package com.floreantpos.actions;

import com.floreantpos.IconFactory;
import com.floreantpos.main.Application;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.RootView;



















public class HomeScreenViewAction
  extends ViewChangeAction
{
  public HomeScreenViewAction() {}
  
  public HomeScreenViewAction(boolean showText, boolean showIcon)
  {
    if (showIcon) {
      putValue("SmallIcon", IconFactory.getIcon("home.png"));
    }
  }
  
  public void execute()
  {
    try {
      RootView.getInstance().showHomeScreen();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
}
