package com.floreantpos.actions;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.services.SplitTicketService;
import com.floreantpos.ui.dialog.NumberSelectionDialog2;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.ManuallySplitTicketDialog;
import com.floreantpos.ui.views.SplitTypeSelectionDialog;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import com.floreantpos.ui.views.payment.SplitedTicketSelectionDialog;
import com.floreantpos.util.CopyUtil;
import com.floreantpos.util.POSUtil;
import java.util.List;
import org.apache.commons.lang.StringUtils;




public class SplitTicketAction
  extends PosAction
{
  private Ticket ticket;
  private boolean allowCustomerSelection = false;
  
  public SplitTicketAction() {
    super(POSConstants.SPLIT_TICKET);
  }
  
  public SplitTicketAction(boolean allowCustomerSelection) {
    super(POSConstants.SPLIT_TICKET);
    this.allowCustomerSelection = allowCustomerSelection;
  }
  
  public SplitTicketAction(DataChangeListener listener) {
    super(POSConstants.SPLIT_TICKET_BUTTON_TEXT, listener);
  }
  
  public SplitTicketAction(DataChangeListener listener, boolean allowCustomerSelection) {
    super(POSConstants.SPLIT_TICKET_BUTTON_TEXT, listener);
    this.allowCustomerSelection = allowCustomerSelection;
  }
  
  public SplitTicketAction(Ticket ticket) {
    super(POSConstants.SPLIT_TICKET);
    this.ticket = ticket;
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void execute()
  {
    try {
      if (listener != null) {
        Object selectedObject = getSelectedObject();
        if (selectedObject == null) {
          return;
        }
        if ((selectedObject instanceof Ticket)) {
          this.ticket = ((Ticket)selectedObject);
        }
      }
      if (this.ticket == null) {
        return;
      }
      if (this.ticket.isVoided().booleanValue()) {
        POSMessageDialog.showMessage(Application.getPosWindow(), POSConstants.TICKET_IS_VOIDED);
        return;
      }
      if (!hasPermissionToAccessTicket(this.ticket))
        return;
      if (StringUtils.isNotEmpty(this.ticket.getId())) {
        this.ticket = TicketDAO.getInstance().loadFullTicket(this.ticket.getId());
      }
      Ticket cloneTicket = (Ticket)CopyUtil.deepCopy(this.ticket);
      if (cloneTicket.getPaidAmount().doubleValue() > 0.0D) {
        POSMessageDialog.showMessage(Application.getPosWindow(), Messages.getString("SplitTicketAction.2"));
        return;
      }
      if ((hasTicketItemDiscount(cloneTicket)) && 
        (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Discount may not be applied. Do you want to continue spliting?", POSConstants.CONFIRM) != 0))
      {
        return;
      }
      
      SplitTypeSelectionDialog dialog = new SplitTypeSelectionDialog(cloneTicket);
      dialog.pack();
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      int splitType = dialog.getSelectedSplitType();
      List<Ticket> splitTickets = doSplitTicket(splitType, cloneTicket);
      if (splitTickets != null) {
        boolean ticketSplited = false;
        for (Ticket ticket : splitTickets) {
          if (StringUtils.isNotEmpty(ticket.getId())) {
            ticketSplited = true;
          }
        }
        if (!ticketSplited) {
          cloneTicket = this.ticket;
        }
      }
      if (listener != null) {
        listener.dataChanged(cloneTicket);
      }
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  private boolean hasTicketItemDiscount(Ticket newTicket) {
    for (TicketItem ticketItem : newTicket.getTicketItems()) {
      if ((ticketItem.getDiscounts() != null) && (ticketItem.getDiscounts().size() > 0))
        return true;
    }
    return false;
  }
  
  private List<Ticket> doSplitTicket(int splitType, Ticket ticket) throws Exception {
    List<Ticket> splitTickets = null;
    if (splitType == 0) {
      NumberSelectionDialog2 numberDialog = new NumberSelectionDialog2();
      numberDialog.setTitle(Messages.getString("SplitTicketAction.3"));
      numberDialog.pack();
      numberDialog.open();
      
      if (numberDialog.isCanceled()) {
        return null;
      }
      int splitQuantity = (int)numberDialog.getValue();
      
      if (splitQuantity > 0) {
        splitTickets = SplitTicketService.doEquallySplit(ticket, splitQuantity);
        ticket.addProperty("SPLIT_TYPE", String.valueOf(0));
      }
    }
    else if (splitType == 1) {
      splitTickets = SplitTicketService.doSplitBySeatNumber(ticket);
      ticket.addProperty("SPLIT_TYPE", String.valueOf(1));
    }
    else {
      NumberSelectionDialog2 numberDialog = new NumberSelectionDialog2();
      numberDialog.setTitle(Messages.getString("SplitTicketAction.3"));
      numberDialog.pack();
      numberDialog.open();
      
      if (numberDialog.isCanceled()) {
        return null;
      }
      int splitQuantity = (int)numberDialog.getValue();
      if (splitQuantity <= 1) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Split quantity must be more than one");
        return null;
      }
      ManuallySplitTicketDialog dialog = new ManuallySplitTicketDialog();
      dialog.setTicket(ticket, splitQuantity);
      dialog.open();
      
      if (dialog.isCanceled()) {
        if (StringUtils.isNotEmpty(ticket.getId())) {
          ticket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
        }
        return null;
      }
      splitTickets = dialog.getSplitTickets();
      ticket.addProperty("SPLIT_TYPE", String.valueOf(2));
    }
    if ((splitTickets == null) || (splitTickets.isEmpty()) || (splitTickets.size() == 1)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Split quantity must be more than one");
      return null;
    }
    showSplitActionDialog(splitTickets);
    return splitTickets;
  }
  
  private void showSplitActionDialog(List<Ticket> splitTickets) {
    SplitedTicketSelectionDialog posDialog = new SplitedTicketSelectionDialog(splitTickets);
    posDialog.setDefaultCloseOperation(2);
    posDialog.allowCustomerSelection(allowCustomerSelection);
    posDialog.setSize(730, 470);
    posDialog.open();
  }
}
