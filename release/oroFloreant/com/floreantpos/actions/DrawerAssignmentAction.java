package com.floreantpos.actions;

import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.Currency;
import com.floreantpos.model.DrawerType;
import com.floreantpos.model.Store;
import com.floreantpos.model.StoreSessionControl;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.print.PosPrintService;
import com.floreantpos.services.report.CashDrawerReportService;
import com.floreantpos.ui.dialog.CashReconciliationDialog;
import com.floreantpos.ui.dialog.MultiCurrencyAmountSelectionDialog;
import com.floreantpos.ui.dialog.NumberSelectionDialog2;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.UserListDialog;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.DrawerUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.StoreUtil;
import java.util.Date;
import java.util.List;























public class DrawerAssignmentAction
  extends PosAction
{
  private Terminal terminal;
  private User responsibleUser;
  
  public DrawerAssignmentAction(Terminal terminal, User responsibleUser)
  {
    super(Messages.getString("DrawerAssignmentAction.0"), UserPermission.DRAWER_ASSIGNMENT);
    this.terminal = terminal;
    this.responsibleUser = responsibleUser;
    updateActionText();
  }
  
  public void updateActionText() {
    if (terminal.isCashDrawerAssigned()) {
      putValue("Name", "CLOSE DRAWER");
    }
    else {
      putValue("Name", Messages.getString("DrawerAssignmentAction.2"));
    }
  }
  
  public void execute()
  {
    try {
      TerminalDAO.getInstance().refresh(terminal);
      if (terminal.isCashDrawerAssigned()) {
        int option = POSMessageDialog.showYesNoQuestionDialog(Application.getPosWindow(), "Are you sure you want to close drawer?", "Confirm");
        if (option != 0) {
          return;
        }
        performDrawerClose();
      }
      else {
        performAssignment();
      }
      boolean thisDrawerTerminal = Application.getInstance().getTerminal().getId().intValue() == terminal.getId().intValue();
      if (thisDrawerTerminal) {
        Application.getInstance().refreshAndGetTerminal();
      }
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public void performAssignment() throws Exception {
    try {
      DrawerUtil.kickDrawer();
      UserListDialog dialog = new UserListDialog();
      dialog.pack();
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      User assignToUser = dialog.getSelectedUser();
      
      if (!assignToUser.isClockedIn().booleanValue()) {
        POSMessageDialog.showError("Can't assign drawer. Selected user is not clocked in.");
        return;
      }
      CashDrawer cashDrawer = new CashDrawer();
      
      double drawerBalance = 0.0D;
      List<Currency> currencyList = CurrencyUtil.getAllCurrency();
      if ((terminal.isEnableMultiCurrency().booleanValue()) && (currencyList.size() > 1)) {
        MultiCurrencyAmountSelectionDialog multiCurrencyDialog = new MultiCurrencyAmountSelectionDialog(cashDrawer, 500.0D, currencyList);
        multiCurrencyDialog.pack();
        multiCurrencyDialog.open();
        if (multiCurrencyDialog.isCanceled()) {
          POSMessageDialog.showError("Drawer assignment canceled.");
          return;
        }
        drawerBalance = multiCurrencyDialog.getTotalAmount();
      }
      else {
        String message = Messages.getString("DrawerAssignmentAction.6");
        String dialogTitle = Messages.getString("DrawerAssignmentAction.7");
        drawerBalance = NumberSelectionDialog2.takeDoubleInput(message, dialogTitle, 500.0D);
        if (Double.isNaN(drawerBalance)) {
          POSMessageDialog.showError("Drawer assignment canceled.");
          return;
        }
      }
      
      terminal.setAssignedUser(assignToUser);
      


      cashDrawer.setStartTime(new Date());
      cashDrawer.setAssignedUser(assignToUser);
      cashDrawer.setTerminal(terminal);
      cashDrawer.setStoreOperationData(StoreUtil.getCurrentStoreOperation().getCurrentData());
      cashDrawer.setDrawerType(DrawerType.DRAWER);
      cashDrawer.setBeginCash(Double.valueOf(drawerBalance));
      cashDrawer.setAssignedBy(responsibleUser);
      
      terminal.setCurrentCashDrawer(cashDrawer);
      
      TerminalDAO.getInstance().performBatchSave(new Object[] { cashDrawer, terminal });
      POSMessageDialog.showMessage(Messages.getString("DrawerAssignmentAction.8") + " " + assignToUser.getFullName());
      updateActionText();
    } catch (Exception e) {
      throw e;
    }
  }
  
  public void performDrawerClose() throws Exception {
    try {
      DrawerUtil.kickDrawer();
      CashDrawer report = terminal.getCurrentCashDrawer();
      CashDrawerReportService reportService2 = new CashDrawerReportService(report);
      reportService2.populateReport();
      
      Double cashToDeposit = Double.valueOf(0.0D);
      Application application = Application.getInstance();
      if (application.getStore().isUseDetailedReconciliation().booleanValue()) {
        CashReconciliationDialog dialog = new CashReconciliationDialog(report);
        dialog.pack();
        dialog.open();
        if (dialog.isCanceled()) {
          return;
        }
        cashToDeposit = Double.valueOf(dialog.getTotalReconcilieAmount());
      }
      else if (application.getTerminal().isEnableMultiCurrency().booleanValue())
      {
        MultiCurrencyAmountSelectionDialog multiCurrencyDialog = new MultiCurrencyAmountSelectionDialog(report, report.getDrawerAccountable().doubleValue(), CurrencyUtil.getAllCurrency());
        multiCurrencyDialog.setTitle("Cash reconciliation");
        multiCurrencyDialog.setCaption("Cash reconciliation");
        multiCurrencyDialog.setReconcile(true);
        multiCurrencyDialog.pack();
        multiCurrencyDialog.open();
        if (multiCurrencyDialog.isCanceled()) {
          return;
        }
        cashToDeposit = Double.valueOf(multiCurrencyDialog.getTotalAmount());
      }
      else {
        cashToDeposit = getReconcileAmount(report);
        if (cashToDeposit.isNaN())
          return;
      }
      report.setCashToDeposit(cashToDeposit);
      
      TerminalDAO dao = new TerminalDAO();
      dao.resetCashDrawer(report, terminal, responsibleUser, 0.0D);
      
      if (terminal.isEnableMultiCurrency().booleanValue()) {}
      













      PosPrintService.printDrawerPullReport(report);
      POSMessageDialog.showMessage(Messages.getString("DrawerAssignmentAction.10"));
      updateActionText();
    } catch (Exception e) {
      throw e;
    }
  }
  
  private Double getReconcileAmount(CashDrawer report) {
    Double reconcileAmount = Double.valueOf(NumberSelectionDialog2.takeDoubleInput("Enter amount", "Enter amount", 
      NumberUtil.roundToTwoDigit(report.getDrawerAccountable().doubleValue())));
    
    if (reconcileAmount.isNaN()) {
      return Double.valueOf(NaN.0D);
    }
    if (reconcileAmount.doubleValue() < report.getDrawerAccountable().doubleValue()) {
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Do you want to save partial reconciliation?", "Confirm") == 0) {
        return reconcileAmount;
      }
      
      return getReconcileAmount(report);
    }
    return reconcileAmount;
  }
}
