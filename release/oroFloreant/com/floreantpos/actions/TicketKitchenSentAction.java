package com.floreantpos.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.TicketView;
import com.floreantpos.ui.views.order.actions.DataChangeListener;

public class TicketKitchenSentAction extends PosAction
{
  private Ticket ticket;
  
  public TicketKitchenSentAction()
  {
    super(POSConstants.SEND_TO_KITCHEN);
  }
  
  public TicketKitchenSentAction(Ticket ticket) {
    super(POSConstants.SEND_TO_KITCHEN);
    this.ticket = ticket;
  }
  
  public TicketKitchenSentAction(DataChangeListener listener) {
    super(POSConstants.SEND_TO_KITCHEN, listener);
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void execute()
  {
    try {
      Object selectedObject = getSelectedObject();
      if (selectedObject == null) {
        return;
      }
      Ticket ticket = null;
      if ((selectedObject instanceof Ticket)) {
        ticket = (Ticket)selectedObject;
      }
      else {
        ticket = com.floreantpos.services.TicketService.getTicket((String)selectedObject);
      }
      if (ticket == null) {
        return;
      }
      Ticket ticketToEdit = TicketDAO.getInstance().loadFullTicket(ticket.getId());
      OrderView.getInstance().setCurrentTicket(ticketToEdit);
      OrderView.getInstance().getTicketView().sendTicketToKitchen();
      POSMessageDialog.showMessage(com.floreantpos.Messages.getString("DefaultTableSelectionView.6"));
    } catch (Exception e) {
      POSMessageDialog.showError(com.floreantpos.main.Application.getPosWindow(), POSConstants.ERROR_MESSAGE, e);
    }
  }
}
