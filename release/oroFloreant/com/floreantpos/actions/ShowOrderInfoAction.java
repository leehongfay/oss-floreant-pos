package com.floreantpos.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.OrderInfoDialog;
import com.floreantpos.ui.views.OrderInfoView;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import com.floreantpos.util.POSUtil;
import java.util.ArrayList;
import java.util.List;

public class ShowOrderInfoAction
  extends PosAction
{
  private Ticket ticket;
  
  public ShowOrderInfoAction()
  {
    super(POSConstants.ORDER_INFO);
  }
  
  public ShowOrderInfoAction(Ticket ticket) {
    super(POSConstants.ORDER_INFO);
    this.ticket = ticket;
  }
  
  public ShowOrderInfoAction(DataChangeListener listener) {
    super(POSConstants.ORDER_INFO_BUTTON_TEXT, listener);
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void execute()
  {
    try {
      if (listener != null) {
        Object selectedObject = getSelectedObject();
        if (selectedObject == null) {
          return;
        }
        if ((selectedObject instanceof Ticket)) {
          ticket = ((Ticket)selectedObject);
        }
      }
      if (ticket == null) {
        return;
      }
      if (!hasPermissionToAccessTicket(ticket))
        return;
      Ticket ticketFull = TicketDAO.getInstance().loadFullTicket(ticket.getId());
      List<Ticket> ticketsToShow = new ArrayList();
      ticketsToShow.add(ticketFull);
      try
      {
        OrderInfoView view = new OrderInfoView(ticketsToShow);
        
        OrderInfoDialog dialog = new OrderInfoDialog(view);
        dialog.setSize(600, 700);
        dialog.setDefaultCloseOperation(2);
        dialog.open();
      } catch (Exception e) {
        PosLog.error(getClass(), e);
      }
    } catch (PosException e) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, e);
    }
  }
}
