package com.floreantpos.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.config.CardConfig;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.OrderTypeSelectionDialog2;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import com.floreantpos.ui.views.payment.SettleTicketDialog;
import com.floreantpos.ui.views.payment.SettleTicketProcessor;
import com.floreantpos.util.POSUtil;
import java.util.Set;













public class SettleTicketAction
  extends PosAction
{
  private Ticket ticket;
  private User currentUser;
  
  public SettleTicketAction(Ticket ticket)
  {
    this.ticket = ticket;
    setRequiredPermission(UserPermission.SETTLE_TICKET);
  }
  
  public SettleTicketAction(Ticket ticket, User user) {
    this.ticket = ticket;
    currentUser = user;
    setRequiredPermission(UserPermission.SETTLE_TICKET);
  }
  
  public SettleTicketAction(DataChangeListener listener) {
    super(POSConstants.SETTLE_TICKET_BUTTON_TEXT, listener);
    setRequiredPermission(UserPermission.SETTLE_TICKET);
  }
  
  public void execute() {
    try {
      if (listener != null) {
        Object selectedObject = getSelectedObject();
        if (selectedObject == null) {
          return;
        }
        if ((selectedObject instanceof Ticket)) {
          ticket = ((Ticket)selectedObject);
        }
      }
      if (!hasPermissionToAccessTicket(ticket))
        return;
      performSettle();
    } catch (PosException e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    } catch (Exception e) {
      PosLog.error(getClass(), e);
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  public boolean performSettle() {
    if (currentUser == null) {
      currentUser = Application.getCurrentUser();
    }
    if (ticket.isVoided().booleanValue()) {
      POSMessageDialog.showMessage(Application.getPosWindow(), POSConstants.TICKET_IS_VOIDED);
      return false;
    }
    if (!POSUtil.checkDrawerAssignment(Application.getInstance().getTerminal(), currentUser)) {
      return false;
    }
    TicketDAO.getInstance().loadFullTicket(ticket);
    if (ticket.isClosed().booleanValue()) {
      ticket.setClosed(Boolean.valueOf(false));
      ticket.setReOpened(Boolean.valueOf(true));
    }
    
    if ((ticket.getOrderType().isHasForHereAndToGo().booleanValue()) && (ticket.getPaidAmount().doubleValue() <= 0.0D)) {
      OrderTypeSelectionDialog2 dialog = new OrderTypeSelectionDialog2();
      dialog.open();
      
      if (dialog.isCanceled()) {
        return false;
      }
      String orderType = dialog.getSelectedOrderType();
      if (orderType != null) {
        ticket.updateTicketItemPriceByOrderType(orderType);
      }
    }
    

    SettleTicketDialog posDialog = new SettleTicketDialog(ticket, currentUser);
    
    if ((ticket.getOrderType().isBarTab().booleanValue()) && (CardConfig.isPreAuthBartab()) && (!ticket.getTransactions().isEmpty())) {
      posDialog.getTicketProcessor().doSettleBarTabTicket(ticket, currentUser);
      return true;
    }
    

    posDialog.setSize(Application.getPosWindow().getSize());
    posDialog.setDefaultCloseOperation(2);
    posDialog.openUndecoratedFullScreen();
    if (posDialog.isCanceled()) {
      return false;
    }
    if (listener != null) {
      listener.dataChanged(ticket);
    }
    return true;
  }
}
