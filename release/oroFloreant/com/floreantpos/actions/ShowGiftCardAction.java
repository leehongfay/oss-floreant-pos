package com.floreantpos.actions;

import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.views.GiftCardManagementView;


















public class ShowGiftCardAction
  extends PosAction
{
  public ShowGiftCardAction()
  {
    super(Messages.getString("ShowGiftCardAction.1"));
  }
  
  public void execute()
  {
    GiftCardManagementView dialog = new GiftCardManagementView(Application.getPosWindow());
    dialog.setTitle(Messages.getString("ShowGiftCardAction.0"));
    dialog.setDefaultCloseOperation(2);
    dialog.setSize(PosUIManager.getSize(600, 400));
    dialog.setLocationRelativeTo(Application.getPosWindow());
    dialog.setVisible(true);
  }
}
