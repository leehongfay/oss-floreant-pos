package com.floreantpos.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.ui.dialog.CashDrawerInfoDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;


















public class DrawerStatusAction
  extends PosAction
{
  public DrawerStatusAction()
  {
    super(POSConstants.DRAWER_PULL_BUTTON_TEXT, UserPermission.DRAWER_PULL);
  }
  
  public void execute()
  {
    try {
      CashDrawerInfoDialog dialog = new CashDrawerInfoDialog(Application.getCurrentUser(), Application.getCurrentUser().getActiveDrawerPullReport());
      dialog.setTitle(POSConstants.DRAWER_PULL_BUTTON_TEXT);
      dialog.refreshReport();
      dialog.setDefaultCloseOperation(2);
      dialog.open();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
}
