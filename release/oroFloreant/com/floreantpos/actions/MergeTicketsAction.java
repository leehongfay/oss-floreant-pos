package com.floreantpos.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.model.Ticket;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.MergeTicketDialog;
import com.floreantpos.ui.views.order.StyledTicketSelectionDialog;
import com.floreantpos.util.POSUtil;
import java.util.List;

public class MergeTicketsAction extends PosAction
{
  public MergeTicketsAction()
  {
    super("Merge");
  }
  
  public void execute()
  {
    try {
      StyledTicketSelectionDialog ticketSelectionDialog = new StyledTicketSelectionDialog();
      ticketSelectionDialog.setMerge(true);
      ticketSelectionDialog.setRequiredNumber(2);
      ticketSelectionDialog.openFullScreen();
      
      if (ticketSelectionDialog.isCanceled()) {
        return;
      }
      
      List<Ticket> selectedTickets = ticketSelectionDialog.getSelectedTickets();
      if (selectedTickets.size() <= 0) {
        return;
      }
      
      MergeTicketDialog dialog = new MergeTicketDialog(selectedTickets, ticketSelectionDialog.getMainTicket());
      dialog.setTitle("Merged Ticket");
      dialog.setSize(PosUIManager.getSize(750, 600));
      dialog.setLocationRelativeTo(POSUtil.getFocusedWindow());
      dialog.setVisible(true);
      
      if (dialog.isCanceled()) {
        return;
      }
      
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Successfully merged to token #" + dialog.getMainTicket().getTokenNo());
    } catch (Exception e) {
      POSMessageDialog.showError(com.floreantpos.main.Application.getPosWindow(), POSConstants.ERROR_MESSAGE, e);
    }
  }
}
