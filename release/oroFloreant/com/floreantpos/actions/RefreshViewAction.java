package com.floreantpos.actions;

import com.floreantpos.IconFactory;
import com.floreantpos.main.Application;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.IView;
import com.floreantpos.ui.views.order.RootView;
















public class RefreshViewAction
  extends ViewChangeAction
{
  public RefreshViewAction()
  {
    putValue("SmallIcon", IconFactory.getIcon("refresh.png"));
  }
  
  public void execute()
  {
    try {
      RootView.getInstance().getCurrentView().refresh();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
}
