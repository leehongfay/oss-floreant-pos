package com.floreantpos.actions;

import com.floreantpos.main.Application;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.DrawerType;
import com.floreantpos.model.Store;
import com.floreantpos.model.StoreSessionControl;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.print.PosPrintService;
import com.floreantpos.services.report.CashDrawerReportService;
import com.floreantpos.ui.dialog.CashReconciliationDialog;
import com.floreantpos.ui.dialog.MultiCurrencyAmountSelectionDialog;
import com.floreantpos.ui.dialog.NumberSelectionDialog2;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.StoreUtil;
import java.util.Date;















public class StaffBankStartStopAction
  extends PosAction
{
  public static final double FORCE_CLOSE_RECONCILE_AMOUNT = 0.0D;
  private User assignToUser;
  
  public StaffBankStartStopAction(User assignedToUser)
  {
    super("Start Staff Bank");
    assignToUser = assignedToUser;
    updateActionText();
  }
  
  public void updateActionText() {}
  
  public void execute()
  {
    try
    {
      if (assignToUser.isStaffBankStarted().booleanValue()) {
        int option = POSMessageDialog.showYesNoQuestionDialog(Application.getPosWindow(), "Are you sure you want to close staff bank?", "Confirm");
        if (option != 0) {
          return;
        }
        performCloseStaffBank();
      }
      else {
        performStartStaffBank();
      }
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public void performStartStaffBank() throws Exception {
    try {
      if (!assignToUser.isClockedIn().booleanValue()) {
        POSMessageDialog.showError("Can't start staff bank. You are not clocked in.");
        return;
      }
      CashDrawer staffBankReport = new CashDrawer();
      staffBankReport.setStartTime(new Date());
      staffBankReport.setAssignedUser(assignToUser);
      staffBankReport.setTerminal(Application.getInstance().getTerminal());
      staffBankReport.setStoreOperationData(StoreUtil.getCurrentStoreOperation().getCurrentData());
      staffBankReport.setDrawerType(DrawerType.STAFF_BANK);
      staffBankReport.setAssignedBy(assignToUser);
      
      assignToUser.setStaffBankStarted(Boolean.valueOf(true));
      assignToUser.setCurrentCashDrawer(staffBankReport);
      
      TerminalDAO.getInstance().performBatchSave(new Object[] { staffBankReport, assignToUser });
      updateActionText();
    } catch (Exception e) {
      throw e;
    }
  }
  
  public void performCloseStaffBank() throws Exception {
    try {
      CashDrawer report = assignToUser.getActiveDrawerPullReport();
      CashDrawerReportService reportService = new CashDrawerReportService(report);
      reportService.populateReport();
      
      Double reconcileAmount = Double.valueOf(0.0D);
      
      Application application = Application.getInstance();
      if (application.getStore().isUseDetailedReconciliation().booleanValue()) {
        CashReconciliationDialog dialog = new CashReconciliationDialog(report);
        dialog.pack();
        dialog.open();
        if (dialog.isCanceled()) {
          return;
        }
        reconcileAmount = Double.valueOf(dialog.getTotalReconcilieAmount());
      }
      else if (application.getTerminal().isEnableMultiCurrency().booleanValue())
      {
        MultiCurrencyAmountSelectionDialog multiCurrencyDialog = new MultiCurrencyAmountSelectionDialog(report, report.getDrawerAccountable().doubleValue(), CurrencyUtil.getAllCurrency());
        multiCurrencyDialog.setTitle("Cash reconciliation");
        multiCurrencyDialog.setCaption("Cash reconciliation");
        multiCurrencyDialog.setReconcile(true);
        multiCurrencyDialog.pack();
        multiCurrencyDialog.open();
        if (multiCurrencyDialog.isCanceled()) {
          return;
        }
        reconcileAmount = Double.valueOf(multiCurrencyDialog.getTotalAmount());
      }
      else {
        reconcileAmount = getReconcileAmount(report);
        if (reconcileAmount.isNaN()) {
          return;
        }
      }
      report.setCashToDeposit(reconcileAmount);
      
      TerminalDAO dao = new TerminalDAO();
      dao.resetStaffBank(assignToUser, report);
      
      User currentUser = Application.getCurrentUser();
      if ((currentUser != null) && (currentUser.getId().equals(report.getAssignedUser().getId()))) {
        Application.getInstance().setCurrentUser(UserDAO.getInstance().get(currentUser.getId()));
      }
      PosPrintService.printDrawerPullReport(report);
      updateActionText();
    } catch (Exception e) {
      throw e;
    }
  }
  
  public void performForceCloseStaffBank(User closedByUser) throws Exception {
    try {
      CashDrawer report = assignToUser.getActiveDrawerPullReport();
      CashDrawerReportService reportService = new CashDrawerReportService(report);
      reportService.populateReport();
      report.setCashToDeposit(Double.valueOf(0.0D));
      
      TerminalDAO dao = new TerminalDAO();
      dao.resetStaffBank(assignToUser, report);
      
      User currentUser = Application.getCurrentUser();
      if ((currentUser != null) && (currentUser.getId().equals(report.getAssignedUser().getId()))) {
        Application.getInstance().setCurrentUser(UserDAO.getInstance().get(currentUser.getId()));
      }
      PosPrintService.printDrawerPullReport(report);
      updateActionText();
    } catch (Exception e) {
      throw e;
    }
  }
  
  private Double getReconcileAmount(CashDrawer report) {
    Double reconcileAmount = Double.valueOf(NumberSelectionDialog2.takeDoubleInput("Enter reconcile amount & tips", "Reconcile", assignToUser
      .isBlindAccountableAmount().booleanValue() ? 0.0D : report.getDrawerAccountable().doubleValue()));
    
    if (reconcileAmount.isNaN()) {
      return Double.valueOf(NaN.0D);
    }
    if (reconcileAmount.doubleValue() < report.getDrawerAccountable().doubleValue()) {
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Do you want to save partial reconciliation?", "Confirm") == 0) {
        return reconcileAmount;
      }
      
      return getReconcileAmount(report);
    }
    return reconcileAmount;
  }
}
