package com.floreantpos.actions;

import com.floreantpos.main.Application;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.Store;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.print.PosPrintService;
import com.floreantpos.services.report.CashDrawerReportService;
import com.floreantpos.ui.dialog.CashReconciliationDialog;
import com.floreantpos.ui.dialog.MultiCurrencyAmountSelectionDialog;
import com.floreantpos.ui.dialog.NumberSelectionDialog2;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;















public class StaffBankCloseAction
  extends PosAction
{
  public static final double FORCE_CLOSE_RECONCILE_AMOUNT = 0.0D;
  private CashDrawer staffBank;
  private User closedByUser;
  
  public StaffBankCloseAction(CashDrawer staffBank, User closedByUser)
  {
    super("CLOSE STAFF BANK");
    this.staffBank = staffBank;
    this.closedByUser = closedByUser;
    updateActionText();
  }
  
  public void updateActionText() {}
  
  public void execute()
  {
    try
    {
      int option = POSMessageDialog.showYesNoQuestionDialog(Application.getPosWindow(), "Are you sure you want to close staff bank?", "Confirm");
      if (option != 0) {
        return;
      }
      performCloseStaffBank();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public void performCloseStaffBank() throws Exception {
    try {
      CashDrawerReportService reportService = new CashDrawerReportService(staffBank);
      reportService.populateReport();
      
      Double reconcileAmount = Double.valueOf(0.0D);
      
      Application application = Application.getInstance();
      if (application.getStore().isUseDetailedReconciliation().booleanValue()) {
        CashReconciliationDialog dialog = new CashReconciliationDialog(staffBank);
        dialog.pack();
        dialog.open();
        if (dialog.isCanceled()) {
          return;
        }
        reconcileAmount = Double.valueOf(dialog.getTotalReconcilieAmount());
      }
      else if (application.getTerminal().isEnableMultiCurrency().booleanValue())
      {
        MultiCurrencyAmountSelectionDialog multiCurrencyDialog = new MultiCurrencyAmountSelectionDialog(staffBank, staffBank.getDrawerAccountable().doubleValue(), CurrencyUtil.getAllCurrency());
        multiCurrencyDialog.setTitle("Cash reconciliation");
        multiCurrencyDialog.setCaption("Cash reconciliation");
        multiCurrencyDialog.setReconcile(true);
        multiCurrencyDialog.pack();
        multiCurrencyDialog.open();
        if (multiCurrencyDialog.isCanceled()) {
          return;
        }
        reconcileAmount = Double.valueOf(multiCurrencyDialog.getTotalAmount());
      }
      else {
        reconcileAmount = getReconcileAmount(staffBank);
        if (reconcileAmount.isNaN()) {
          return;
        }
      }
      staffBank.setCashToDeposit(reconcileAmount);
      
      TerminalDAO dao = new TerminalDAO();
      dao.resetStaffBank(closedByUser, staffBank);
      
      Application.getInstance().refreshCurrentUser();
      PosPrintService.printDrawerPullReport(staffBank);
      updateActionText();
    } catch (Exception e) {
      throw e;
    }
  }
  
  public void performForceCloseStaffBank(User closedByUser) throws Exception {
    try {
      CashDrawerReportService reportService = new CashDrawerReportService(staffBank);
      reportService.populateReport();
      staffBank.setCashToDeposit(Double.valueOf(0.0D));
      
      TerminalDAO dao = new TerminalDAO();
      dao.resetStaffBank(closedByUser, staffBank);
      
      Application.getInstance().refreshCurrentUser();
      PosPrintService.printDrawerPullReport(staffBank);
      updateActionText();
    } catch (Exception e) {
      throw e;
    }
  }
  
  private Double getReconcileAmount(CashDrawer staffBank) {
    Double reconcileAmount = Double.valueOf(NumberSelectionDialog2.takeDoubleInput("Enter reconcile amount & tips", "Reconcile", closedByUser
      .isBlindAccountableAmount().booleanValue() ? 0.0D : NumberUtil.roundToTwoDigit(staffBank.getDrawerAccountable().doubleValue())));
    
    if (reconcileAmount.isNaN()) {
      return Double.valueOf(NaN.0D);
    }
    if (reconcileAmount.doubleValue() < staffBank.getDrawerAccountable().doubleValue()) {
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Do you want to save partial reconciliation?", "Confirm") == 0) {
        return reconcileAmount;
      }
      
      return getReconcileAmount(staffBank);
    }
    return reconcileAmount;
  }
}
