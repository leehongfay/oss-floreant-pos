package com.floreantpos.actions;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.services.TicketService;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.OrderController;
import com.floreantpos.ui.views.order.TicketSelectionDialog;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import com.floreantpos.util.POSUtil;
import java.util.Iterator;
import java.util.List;
import javax.swing.JOptionPane;

public class TicketCloseAction
  extends PosAction
{
  public TicketCloseAction()
  {
    super(POSConstants.CLOSE_ORDER_BUTTON_TEXT);
  }
  
  public TicketCloseAction(DataChangeListener listener) {
    super(POSConstants.CLOSE_ORDER_BUTTON_TEXT, listener);
  }
  
  public void execute()
  {
    try {
      Object selectedObject = getSelectedObject();
      if (selectedObject != null) {
        if (!hasPermissionToAccessTicket((Ticket)selectedObject))
          return;
        closeSingleTicket(selectedObject);
      }
      else {
        closeMultipleTickets();
      }
    } catch (PosException e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private void closeMultipleTickets() {
    List<Ticket> tickets = TicketDAO.getInstance().getTicketsWithSpecificFields(new String[] { Ticket.PROP_ID, Ticket.PROP_DUE_AMOUNT, Ticket.PROP_TOKEN_NO });
    for (Iterator iterator = tickets.iterator(); iterator.hasNext();) {
      Ticket ticket = (Ticket)iterator.next();
      if (ticket.getDueAmount().doubleValue() > 0.0D) {
        iterator.remove();
      }
    }
    TicketSelectionDialog ticketSelectionDialog = new TicketSelectionDialog(tickets);
    ticketSelectionDialog.setCaption("Select tickets to close");
    ticketSelectionDialog.openFullScreen();
    
    if (ticketSelectionDialog.isCanceled()) {
      return;
    }
    
    List<Ticket> selectedTickets = ticketSelectionDialog.getSelectedTickets();
    if (selectedTickets == null) {
      return;
    }
    
    int option = POSMessageDialog.showYesNoQuestionDialog(Application.getPosWindow(), "Are you sure to close tickets?", "Confirmation to close");
    if (option == 0) {
      TicketDAO.closeOrders((Ticket[])selectedTickets.toArray(new Ticket[selectedTickets.size()]));
    }
  }
  
  private void closeSingleTicket(Object selectedObject) {
    Ticket ticket = null;
    if ((selectedObject instanceof Ticket)) {
      ticket = (Ticket)selectedObject;
    }
    else {
      ticket = TicketService.getTicket((String)selectedObject);
    }
    if (ticket == null) {
      return;
    }
    if (ticket.isVoided().booleanValue()) {
      POSMessageDialog.showMessage(Application.getPosWindow(), POSConstants.TICKET_IS_VOIDED);
      return;
    }
    ticket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
    int due = (int)POSUtil.getDouble(ticket.getDueAmount());
    
    if (due != 0) {
      int option = POSMessageDialog.showYesNoQuestionDialog(Application.getPosWindow(), "Ticket is not fully paid, do you still want to close it?", "Confirm");
      if (option != 0) {
        return;
      }
    }
    int option = JOptionPane.showOptionDialog(Application.getPosWindow(), 
      Messages.getString("SwitchboardView.6") + ticket.getId() + Messages.getString("SwitchboardView.7"), POSConstants.CONFIRM, 2, 1, null, null, null);
    

    if (option != 0) {
      return;
    }
    OrderController.closeOrder(ticket);
    if (listener != null) {
      listener.dataChanged(ticket);
    }
  }
}
