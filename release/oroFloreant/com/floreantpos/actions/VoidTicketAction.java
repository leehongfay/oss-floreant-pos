package com.floreantpos.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.services.TicketService;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.VoidTicketDialog;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import com.floreantpos.util.POSUtil;
import java.util.ArrayList;
import java.util.List;



















public class VoidTicketAction
  extends PosAction
{
  Ticket ticket;
  
  public VoidTicketAction(DataChangeListener listener)
  {
    super(POSConstants.VOID_TICKET_BUTTON_TEXT + "/" + POSConstants.REFUND_BUTTON_TEXT, listener);
    super.setRequiredPermission(UserPermission.VOID_TICKET);
    setMandatoryPermission(true);
  }
  
  public VoidTicketAction(Ticket ticket) {
    super(POSConstants.VOID_TICKET_BUTTON_TEXT + "/" + POSConstants.REFUND_BUTTON_TEXT);
    super.setRequiredPermission(UserPermission.VOID_TICKET);
    setMandatoryPermission(true);
    this.ticket = ticket;
  }
  
  public void execute()
  {
    try {
      Ticket selectedTicket = null;
      if (ticket == null) {
        Object selectedObject = getSelectedObject();
        if (selectedObject == null) {
          return;
        }
        if ((selectedObject instanceof Ticket)) {
          selectedTicket = (Ticket)selectedObject;
        }
        else {
          selectedTicket = TicketService.getTicket((String)selectedObject);
        }
      }
      else {
        selectedTicket = ticket;
      }
      selectedTicket = TicketDAO.getInstance().loadFullTicket(selectedTicket.getId());
      if (selectedTicket.isVoided().booleanValue()) {
        POSMessageDialog.showMessage(Application.getPosWindow(), POSConstants.TICKET_IS_VOIDED);
        return;
      }
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Are you sure you want to void?", "Confirm") != 0) {
        return;
      }
      if ((selectedTicket.getPaidAmount().doubleValue() == 0.0D) && (!selectedTicket.isPrintedToKitchenOrInventoryAdjusted())) {
        List ticketsToBeDeleted = new ArrayList();
        ticketsToBeDeleted.add(selectedTicket);
        TicketDAO.getInstance().deleteTickets(ticketsToBeDeleted, true, true);
        if (listener != null) {
          listener.dataRemoved(selectedTicket);
        }
        selectedTicket.setVoided(Boolean.valueOf(true));
      }
      else {
        VoidTicketDialog dialog = new VoidTicketDialog(POSUtil.getFocusedWindow(), selectedTicket);
        dialog.setSize(PosUIManager.getSize(550, 520));
        dialog.open();
        if (dialog.isCanceled()) {
          return;
        }
        if (listener != null) {
          listener.dataChanged(selectedTicket);
        }
      }
      POSMessageDialog.showMessage("Ticket is voided.");
    } catch (PosException e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
}
