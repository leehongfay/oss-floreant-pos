package com.floreantpos.actions;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.main.Application;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.services.TicketService;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.DefaultOrderServiceExtension;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import javax.swing.JOptionPane;

public class AssignDriverAction extends PosAction
{
  public AssignDriverAction()
  {
    super(POSConstants.ASSIGN_DRIVER_BUTTON_TEXT);
  }
  
  public AssignDriverAction(DataChangeListener listener) {
    super(POSConstants.ASSIGN_DRIVER_BUTTON_TEXT, listener);
  }
  
  public void execute()
  {
    try {
      Object selectedObject = getSelectedObject();
      if (selectedObject == null) {
        return;
      }
      Ticket ticket = null;
      if ((selectedObject instanceof Ticket)) {
        ticket = (Ticket)selectedObject;
      }
      else {
        ticket = TicketService.getTicket((String)selectedObject);
      }
      if (ticket == null) {
        return;
      }
      if (!ticket.getOrderType().isDelivery().booleanValue()) {
        POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("SwitchboardView.8"));
        return;
      }
      
      User assignedDriver = ticket.getAssignedDriver();
      if (assignedDriver != null) {
        int option = JOptionPane.showOptionDialog(Application.getPosWindow(), Messages.getString("SwitchboardView.9"), POSConstants.CONFIRM, 0, 3, null, null, null);
        

        if (option != 0) {
          return;
        }
      }
      
      OrderServiceExtension orderServiceExtension = (OrderServiceExtension)com.floreantpos.extension.ExtensionManager.getPlugin(OrderServiceExtension.class);
      if (orderServiceExtension == null) {
        orderServiceExtension = new DefaultOrderServiceExtension();
      }
      orderServiceExtension.assignDriver(ticket.getId());
      if (listener != null)
        listener.dataChanged(ticket);
    } catch (PosException e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
}
