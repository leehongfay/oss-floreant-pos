package com.floreantpos.actions;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.model.UserPermission;
import com.floreantpos.util.POSUtil;

















public class ShowBackofficeAction
  extends PosAction
{
  public ShowBackofficeAction()
  {
    super(POSConstants.BACK_OFFICE_BUTTON_TEXT);
    setRequiredPermission(UserPermission.VIEW_BACK_OFFICE);
  }
  
  public ShowBackofficeAction(boolean showText, boolean showIcon) {
    if (showText) {
      putValue("Name", UserPermission.VIEW_BACK_OFFICE);
    }
    if (showIcon) {
      putValue("SmallIcon", IconFactory.getIcon("backoffice.png"));
    }
    
    setRequiredPermission(UserPermission.VIEW_BACK_OFFICE);
  }
  
  public void execute()
  {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    if (window == null) {
      window = new BackOfficeWindow(getAuthorizedUser());
    }
    window.setVisible(true);
    window.toFront();
  }
}
