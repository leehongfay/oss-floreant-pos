package com.floreantpos.actions;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import com.floreantpos.model.AttendenceHistory;
import com.floreantpos.model.EmployeeInOutHistory;
import com.floreantpos.model.Shift;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.AttendenceHistoryDAO;
import com.floreantpos.model.dao.EmployeeInOutHistoryDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.PasswordEntryDialog;
import com.floreantpos.ui.views.ClockInOutDialog;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.ShiftUtil;
import java.util.Calendar;
import java.util.Date;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



















public class ClockInOutAction
  extends PosAction
{
  public ClockInOutAction()
  {
    super(Messages.getString("ClockInOutAction.0"));
  }
  
  public ClockInOutAction(boolean showText, boolean showIcon) {
    if (showText) {
      putValue("Name", Messages.getString("ClockInOutAction.1"));
    }
    if (showIcon) {
      putValue("SmallIcon", IconFactory.getIcon("/ui_icons/", "clock_out.png"));
    }
  }
  
  public void execute()
  {
    User user = PasswordEntryDialog.getUser(Application.getPosWindow(), Messages.getString("ClockInOutAction.3"));
    if (user == null) {
      return;
    }
    ClockInOutDialog dialog = ClockInOutDialog.getInstance(user, false);
    dialog.openUndecoratedFullScreen();
  }
  
  private void performClockOut(User user) {
    try {
      if (user == null) {
        return;
      }
      
      AttendenceHistoryDAO attendenceHistoryDAO = new AttendenceHistoryDAO();
      AttendenceHistory attendenceHistory = attendenceHistoryDAO.findHistoryByClockedInTime(user);
      if (attendenceHistory == null) {
        attendenceHistory = new AttendenceHistory();
        Date lastClockInTime = user.getLastClockInTime();
        Calendar c = Calendar.getInstance();
        c.setTime(lastClockInTime);
        attendenceHistory.setClockInTime(lastClockInTime);
        attendenceHistory.setClockInHour(Short.valueOf((short)c.get(10)));
        attendenceHistory.setUser(user);
        attendenceHistory.setTerminal(Application.getInstance().getTerminal());
        attendenceHistory.setShift(user.getCurrentShift());
      }
      
      Shift shift = user.getCurrentShift();
      Calendar calendar = Calendar.getInstance();
      
      user.doClockOut(attendenceHistory, shift, calendar);
      

      POSMessageDialog.showMessage(Messages.getString("ClockInOutAction.8") + user.getFirstName() + " " + user.getLastName() + Messages.getString("ClockInOutAction.10"));
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private void performClockIn(User user) {
    try {
      if (user == null) {
        return;
      }
      
      if ((user.isClockedIn() != null) && (user.isClockedIn().booleanValue()))
      {
        POSMessageDialog.showMessage(Messages.getString("ClockInOutAction.11") + user.getFirstName() + " " + user.getLastName() + Messages.getString("ClockInOutAction.13"));
        return;
      }
      
      Shift currentShift = ShiftUtil.getCurrentShift();
      





      Calendar currentTime = Calendar.getInstance();
      user.doClockIn(Application.getInstance().getTerminal(), currentShift, currentTime);
      

      POSMessageDialog.showMessage(Messages.getString("ClockInOutAction.14") + user.getFirstName() + " " + user.getLastName() + Messages.getString("ClockInOutAction.16"));
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public void performDriverOut(User user) {
    try {
      if (user == null) {
        return;
      }
      
      Shift currentShift = ShiftUtil.getCurrentShift();
      Terminal terminal = Application.getInstance().getTerminal();
      
      Calendar currentTime = Calendar.getInstance();
      user.setAvailableForDelivery(Boolean.valueOf(false));
      user.setLastClockOutTime(currentTime.getTime());
      
      LogFactory.getLog(Application.class).info("terminal id befor saving clockIn=" + terminal.getId());
      
      EmployeeInOutHistory attendenceHistory = new EmployeeInOutHistory();
      attendenceHistory.setOutTime(currentTime.getTime());
      attendenceHistory.setOutHour(Short.valueOf((short)currentTime.get(11)));
      attendenceHistory.setClockOut(Boolean.valueOf(true));
      attendenceHistory.setUser(user);
      attendenceHistory.setTerminal(terminal);
      attendenceHistory.setShift(currentShift);
      
      UserDAO.getInstance().saveDriverOut(user, attendenceHistory, currentShift, currentTime);
      
      POSMessageDialog.showMessage("Driver " + user.getFirstName() + " " + user.getLastName() + " is out for delivery.");
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public void performDriverIn(User user) {
    try {
      if (user == null) {
        return;
      }
      if (!user.isClockedIn().booleanValue()) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("ClockInOutAction.2"));
        return;
      }
      
      EmployeeInOutHistoryDAO attendenceHistoryDAO = new EmployeeInOutHistoryDAO();
      EmployeeInOutHistory attendenceHistory = attendenceHistoryDAO.findDriverHistoryByClockedInTime(user);
      if (attendenceHistory == null) {
        attendenceHistory = new EmployeeInOutHistory();
        


        Calendar currentTime = Calendar.getInstance();
        attendenceHistory.setInTime(currentTime.getTime());
        attendenceHistory.setInHour(Short.valueOf((short)currentTime.get(11)));
        attendenceHistory.setUser(user);
        attendenceHistory.setTerminal(Application.getInstance().getTerminal());
        attendenceHistory.setShift(user.getCurrentShift());
      }
      
      Shift shift = user.getCurrentShift();
      Calendar calendar = Calendar.getInstance();
      
      user.setAvailableForDelivery(Boolean.valueOf(true));
      
      attendenceHistory.setClockOut(Boolean.valueOf(false));
      attendenceHistory.setInTime(calendar.getTime());
      attendenceHistory.setInHour(Short.valueOf((short)calendar.get(11)));
      
      UserDAO.getInstance().saveDriverIn(user, attendenceHistory, shift, calendar);
      
      POSMessageDialog.showMessage("Driver " + user.getFirstName() + " " + user.getLastName() + " is in after delivery");
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
}
