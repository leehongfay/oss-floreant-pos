package com.floreantpos.actions;

import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.PasswordEntryDialog;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import org.apache.commons.lang.StringUtils;



















public abstract class PosAction
  extends AbstractAction
{
  private boolean visible = true;
  
  protected UserPermission requiredPermission;
  protected ActionEvent event;
  private User authorizedUser;
  protected DataChangeListener listener;
  protected boolean mandatoryPermission;
  
  public PosAction() {}
  
  public PosAction(String name)
  {
    super(name);
  }
  
  public PosAction(Icon icon) {
    super(null, icon);
  }
  
  public PosAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public PosAction(String name, UserPermission requiredPermission) {
    super(name);
    
    this.requiredPermission = requiredPermission;
  }
  
  public PosAction(Icon icon, UserPermission requiredPermission) {
    super(null, icon);
    
    this.requiredPermission = requiredPermission;
  }
  
  public PosAction(String name, DataChangeListener listener) {
    super(name);
    this.listener = listener;
  }
  
  public PosAction(DataChangeListener listener) {
    this.listener = listener;
  }
  
  public void setDataChangedListener(DataChangeListener listener) {
    this.listener = listener;
  }
  
  public DataChangeListener getDataChangedListener() {
    return listener;
  }
  
  public Object getSelectedObject() {
    if (listener == null)
      return null;
    return listener.getSelectedData();
  }
  
  public UserPermission getRequiredPermission() {
    return requiredPermission;
  }
  
  public void setRequiredPermission(UserPermission requiredPermission) {
    this.requiredPermission = requiredPermission;
  }
  
  public void actionPerformed(ActionEvent e)
  {
    try {
      event = e;
      User user = Application.getCurrentUser();
      if (requiredPermission == null) {
        authorizedUser = user;
        execute();
        return;
      }
      
      if (user == null)
        return;
      if (!hasPermission(user, requiredPermission)) {
        return;
      }
      authorizedUser = user;
      execute();
    } catch (Exception e2) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e2.getMessage(), e2);
    }
  }
  
  public boolean hasPermissionToAccessTicket(Ticket ticket) {
    User currentUser = Application.getCurrentUser();
    if ((ticket != null) && (ticket.getOwner().getId().equals(currentUser.getId())))
      return true;
    return hasPermission(currentUser, UserPermission.EDIT_OTHER_USERS_TICKETS);
  }
  
  private boolean hasPermission(User user, UserPermission requiredPermission) {
    if ((isMandatoryPermission()) || (!user.hasPermission(requiredPermission))) {
      String password = PasswordEntryDialog.show(Application.getPosWindow(), Messages.getString("PosAction.0"));
      if (StringUtils.isEmpty(password)) {
        return false;
      }
      
      User user2 = UserDAO.getInstance().findUserBySecretKey(password);
      if (user2 == null) {
        POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("PosAction.1"));

      }
      else if (!user2.hasPermission(requiredPermission)) {
        POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("PosAction.2"));
      }
      else {
        authorizedUser = user2;
        return true;
      }
      
      return false;
    }
    return true;
  }
  
  public abstract void execute();
  
  public ActionEvent getActionEvent() {
    return event;
  }
  
  public boolean isVisible() {
    return visible;
  }
  
  public void setVisible(boolean visible) {
    this.visible = visible;
  }
  
  public User getAuthorizedUser() {
    return authorizedUser;
  }
  
  public boolean isMandatoryPermission() {
    return mandatoryPermission;
  }
  
  public void setMandatoryPermission(boolean mandatoryPermission) {
    this.mandatoryPermission = mandatoryPermission;
  }
}
