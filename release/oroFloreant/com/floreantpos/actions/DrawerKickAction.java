package com.floreantpos.actions;

import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.UserPermission;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.DrawerUtil;
















public class DrawerKickAction
  extends PosAction
{
  public DrawerKickAction()
  {
    super(Messages.getString("ManagerDialog.1"), UserPermission.DRAWER_PULL);
    
    setEnabled(Application.getInstance().getTerminal().isHasCashDrawer().booleanValue());
  }
  








  public void execute()
  {
    try
    {
      
    }
    catch (Exception e)
    {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
}
