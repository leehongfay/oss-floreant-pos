package com.floreantpos.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.StyledTicketSelectionDialog;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import com.floreantpos.ui.views.payment.GroupSettleTicketDialog;
import com.floreantpos.util.POSUtil;
import java.util.ArrayList;
import java.util.List;

















public class GroupSettleTicketAction
  extends PosAction
{
  private List<Ticket> tickets;
  private User currentUser;
  
  public GroupSettleTicketAction(User currentUser)
  {
    this.currentUser = currentUser;
    setRequiredPermission(UserPermission.SETTLE_TICKET);
  }
  
  public GroupSettleTicketAction(List<Ticket> tickets, User user)
  {
    this.tickets = tickets;
    currentUser = user;
    setRequiredPermission(UserPermission.SETTLE_TICKET);
  }
  
  public GroupSettleTicketAction(DataChangeListener listener) {
    super(POSConstants.GROUP_SETTLE_BUTTON_TEXT, listener);
    setRequiredPermission(UserPermission.SETTLE_TICKET);
  }
  
  public void execute() {
    try {
      if (!POSUtil.checkDrawerAssignment()) {
        return;
      }
      List<Ticket> ticketList = new ArrayList();
      if ((tickets == null) || (tickets.isEmpty())) {
        StyledTicketSelectionDialog ticketSelectionDialog = new StyledTicketSelectionDialog(true);
        ticketSelectionDialog.openFullScreen();
        
        if (ticketSelectionDialog.isCanceled()) {
          return;
        }
        
        List<Ticket> selectedTickets = ticketSelectionDialog.getSelectedTickets();
        if (selectedTickets == null) {
          return;
        }
        
        for (Ticket ticket : selectedTickets) {
          Ticket fullTicket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
          if (!fullTicket.getOrderType().isBarTab().booleanValue())
          {
            ticketList.add(fullTicket);
          }
        }
      } else {
        ticketList.addAll(tickets);
      }
      if (currentUser == null) {
        currentUser = Application.getCurrentUser();
      }
      GroupSettleTicketDialog posDialog = new GroupSettleTicketDialog(ticketList, currentUser);
      posDialog.setSize(Application.getPosWindow().getSize());
      posDialog.setDefaultCloseOperation(2);
      posDialog.openUndecoratedFullScreen();
      if (listener != null) {
        listener.dataSetUpdated();
      }
    } catch (PosException e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    } catch (Exception e) {
      PosLog.error(getClass(), e);
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, e);
    }
  }
}
