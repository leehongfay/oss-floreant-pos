package com.floreantpos.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.TransferTicketDialog;
import com.floreantpos.ui.views.order.StyledTicketSelectionDialog;
import com.floreantpos.util.POSUtil;
import java.util.ArrayList;
import java.util.List;

public class TransferTicketItemsAction extends PosAction
{
  public TransferTicketItemsAction()
  {
    super("Transfer");
  }
  
  public void execute()
  {
    try {
      StyledTicketSelectionDialog ticketSelectionDialog = new StyledTicketSelectionDialog();
      ticketSelectionDialog.setCaption("Select at least 2 items to transfer");
      ticketSelectionDialog.setRequiredNumber(2);
      ticketSelectionDialog.openFullScreen();
      
      if (ticketSelectionDialog.isCanceled()) {
        return;
      }
      
      List<Ticket> selectedTickets = ticketSelectionDialog.getSelectedTickets();
      if (selectedTickets.size() <= 0) {
        return;
      }
      List<Ticket> loadedTickets = new ArrayList();
      for (Ticket ticket : selectedTickets) {
        loadedTickets.add(TicketDAO.getInstance().loadFullTicket(ticket.getId()));
      }
      
      TransferTicketDialog dialog = new TransferTicketDialog(loadedTickets);
      dialog.setTitle("Transfer ticket items");
      dialog.openFullScreen();
      
      if (dialog.isCanceled()) {
        return;
      }
      
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Transferation completed.");
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  private boolean hasTicketItemDiscount(Ticket newTicket) {
    for (TicketItem ticketItem : newTicket.getTicketItems()) {
      if ((ticketItem.getDiscounts() != null) && (ticketItem.getDiscounts().size() > 0))
        return true;
    }
    return false;
  }
}
