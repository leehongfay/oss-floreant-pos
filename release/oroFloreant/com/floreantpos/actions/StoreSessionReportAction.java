package com.floreantpos.actions;

import com.floreantpos.PosException;
import com.floreantpos.main.Application;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.StoreSession;
import com.floreantpos.model.dao.CashDrawerDAO;
import com.floreantpos.print.PosPrintService;
import com.floreantpos.ui.dialog.CashDrawerInfoDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.util.List;

public class StoreSessionReportAction
  extends PosAction
{
  private StoreSession storeSession;
  
  public StoreSessionReportAction(StoreSession storeSession)
  {
    super("Report");
    this.storeSession = storeSession;
  }
  
  public void execute()
  {
    try {
      List<CashDrawer> drawerReports = CashDrawerDAO.getInstance().findByStoreOperationData(storeSession);
      CashDrawer cashDrawersReportSummary = PosPrintService.populateCashDrawerReportSummary(drawerReports);
      cashDrawersReportSummary.setStartTime(storeSession.getOpenTime());
      cashDrawersReportSummary.setAssignedBy(storeSession.getOpenedBy());
      cashDrawersReportSummary.setReportTime(storeSession.getCloseTime());
      cashDrawersReportSummary.setClosedBy(storeSession.getClosedBy());
      
      CashDrawerInfoDialog dialog = new CashDrawerInfoDialog(null, cashDrawersReportSummary, true);
      dialog.setTitle("Store session summary report");
      dialog.showInfoOnly(true);
      dialog.refreshReport();
      dialog.setDefaultCloseOperation(2);
      dialog.open();
    } catch (PosException e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
}
