package com.floreantpos.actions;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.OrderInfoDialog;
import com.floreantpos.ui.views.OrderInfoView;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import java.util.Arrays;

public class TicketReopenAction
  extends PosAction
{
  private Ticket ticket;
  
  public TicketReopenAction()
  {
    super(POSConstants.REOPEN_TICKET_BUTTON_TEXT);
  }
  
  public TicketReopenAction(Ticket ticket) {
    super(POSConstants.REOPEN_TICKET_BUTTON_TEXT);
    this.ticket = ticket;
  }
  
  public TicketReopenAction(DataChangeListener listener) {
    super(POSConstants.REOPEN_TICKET_BUTTON_TEXT, listener);
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void execute()
  {
    try {
      if (listener != null) {
        Object selectedObject = getSelectedObject();
        if (selectedObject == null) {
          return;
        }
        if ((selectedObject instanceof Ticket)) {
          ticket = ((Ticket)selectedObject);
        }
      }
      if (ticket == null) {
        return;
      }
      if (!ticket.isClosed().booleanValue()) {
        throw new PosException(POSConstants.TICKET_IS_NOT_CLOSED);
      }
      
      if (ticket.isVoided().booleanValue()) {
        throw new PosException(Messages.getString("SwitchboardView.11"));
      }
      ticket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
      ticket.setClosed(Boolean.valueOf(false));
      ticket.setClosingDate(null);
      ticket.setReOpened(Boolean.valueOf(true));
      
      TicketDAO.getInstance().saveOrUpdate(ticket);
      
      OrderInfoView view = new OrderInfoView(Arrays.asList(new Ticket[] { ticket }));
      OrderInfoDialog dialog = new OrderInfoDialog(view);
      dialog.setSize(PosUIManager.getSize(400), PosUIManager.getSize(600));
      dialog.setDefaultCloseOperation(2);
      dialog.setLocationRelativeTo(Application.getPosWindow());
      dialog.setVisible(true);
    } catch (PosException e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, e);
    }
  }
}
