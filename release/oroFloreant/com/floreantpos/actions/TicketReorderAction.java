package com.floreantpos.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.main.Application;
import com.floreantpos.model.Department;
import com.floreantpos.model.Outlet;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemModifier;
import com.floreantpos.model.TicketItemTax;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.ShiftUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;





public class TicketReorderAction
  extends PosAction
{
  private Ticket ticket;
  private boolean isReorder;
  
  public TicketReorderAction()
  {
    super(POSConstants.REORDER_TICKET_BUTTON_TEXT);
  }
  
  public TicketReorderAction(Ticket ticket) {
    super(POSConstants.REORDER_TICKET_BUTTON_TEXT);
    this.ticket = ticket;
  }
  
  public TicketReorderAction(DataChangeListener listener) {
    super(POSConstants.REORDER_TICKET_BUTTON_TEXT, listener);
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void execute()
  {
    try {
      if (listener != null) {
        Object selectedObject = getSelectedObject();
        if (selectedObject == null) {
          return;
        }
        if ((selectedObject instanceof Ticket)) {
          ticket = ((Ticket)selectedObject);
        }
      }
      if (ticket == null) {
        return;
      }
      
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Are you sure to reorder this ticket?", "Reorder");
      
      if (option != 0) {
        return;
      }
      
      ticket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
      createReOrder(ticket);
      isReorder = true;
    }
    catch (PosException e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  private void createReOrder(Ticket oldticket) {
    Ticket ticket = new Ticket();
    ticket.setTaxIncluded(Boolean.valueOf(Application.getInstance().isPriceIncludesTax()));
    ticket.setOrderType(oldticket.getOrderType());
    ticket.setProperties(oldticket.getProperties());
    Terminal terminal = Application.getInstance().getTerminal();
    ticket.setTerminal(terminal);
    
    Department department = terminal.getDepartment();
    if (department != null) {
      ticket.setDepartmentId(department.getId());
    }
    Outlet outlet = terminal.getOutlet();
    if (outlet != null) {
      ticket.setOutletId(outlet.getId());
    }
    ticket.setOwner(Application.getCurrentUser());
    ticket.setShift(ShiftUtil.getCurrentShift());
    ticket.setNumberOfGuests(oldticket.getNumberOfGuests());
    
    Calendar currentTime = DateUtil.getServerTimeCalendar();
    ticket.setCreateDate(currentTime.getTime());
    ticket.setCreationHour(Integer.valueOf(currentTime.get(11)));
    
    List<TicketItem> newTicketItems = new ArrayList();
    for (TicketItem oldTicketItem : oldticket.getTicketItems()) {
      TicketItem newTicketItem = oldTicketItem.createNew();
      newTicketItem.setQuantity(oldTicketItem.getQuantity());
      newTicketItem.setQuantity(oldTicketItem.getQuantity());
      newTicketItem.setMenuItemId(oldTicketItem.getMenuItemId());
      newTicketItem.setName(oldTicketItem.getName());
      newTicketItem.setGroupName(oldTicketItem.getGroupName());
      newTicketItem.setCategoryName(oldTicketItem.getCategoryName());
      newTicketItem.setUnitPrice(oldTicketItem.getUnitPrice());
      newTicketItem.setFractionalUnit(oldTicketItem.isFractionalUnit());
      newTicketItem.setUnitName(oldTicketItem.getUnitName());
      










      newTicketItem.setDiscountsProperty(oldTicketItem.getDiscountsProperty());
      
      if (oldTicketItem.getTicketItemModifiers() != null) {
        for (TicketItemModifier ticketItemModifier : oldTicketItem.getTicketItemModifiers()) {
          TicketItemModifier newModifier = new TicketItemModifier();
          
          ticketItemModifier.setTaxIncluded(Boolean.valueOf(Application.getInstance().isPriceIncludesTax()));
          newModifier.setItemId(ticketItemModifier.getItemId());
          newModifier.setGroupId(ticketItemModifier.getGroupId());
          newModifier.setItemQuantity(ticketItemModifier.getItemQuantity());
          newModifier.setName(ticketItemModifier.getName());
          newModifier.setUnitPrice(ticketItemModifier.getUnitPrice());
          List<TicketItemTax> taxes = ticketItemModifier.getTaxes();
          List<TicketItemTax> modTaxes = new ArrayList();
          for (TicketItemTax ticketItemTax : taxes) {
            modTaxes.add(ticketItemTax);
          }
          newModifier.setTaxes(modTaxes);
          newModifier.setModifierType(ticketItemModifier.getModifierType());
          newModifier.setPrintedToKitchen(Boolean.valueOf(false));
          newModifier.setShouldPrintToKitchen(ticketItemModifier.isShouldPrintToKitchen());
          newModifier.setTicketItem(newTicketItem);
          newTicketItem.addToticketItemModifiers(newModifier);
        }
      }
      
      newTicketItem.setTaxesProperty(oldTicketItem.getTaxesProperty());
      newTicketItem.setBeverage(oldTicketItem.isBeverage());
      newTicketItem.setShouldPrintToKitchen(oldTicketItem.isShouldPrintToKitchen());
      newTicketItem.setPrinterGroup(oldTicketItem.getPrinterGroup());
      newTicketItem.setPrintedToKitchen(Boolean.valueOf(false));
      
      newTicketItem.setTicket(ticket);
      newTicketItems.add(newTicketItem);
    }
    ticket.getTicketItems().addAll(newTicketItems);
    
    OrderView.getInstance().setCurrentTicket(ticket);
    RootView.getInstance().showView("ORDER_VIEW");
  }
  
  public boolean isReorder()
  {
    return isReorder;
  }
}
