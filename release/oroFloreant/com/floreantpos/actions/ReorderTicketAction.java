package com.floreantpos.actions;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.ReorderDialog;
import com.floreantpos.ui.views.order.OrderController;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import com.floreantpos.util.POSUtil;
import java.util.Iterator;
import java.util.List;


















public class ReorderTicketAction
  extends PosAction
{
  private Ticket ticket;
  
  public ReorderTicketAction(Ticket ticket)
  {
    this.ticket = ticket;
  }
  
  public ReorderTicketAction(DataChangeListener listener) {
    super("REORDER", listener);
  }
  
  public void execute() {
    try {
      if (listener != null) {
        Object selectedObject = getSelectedObject();
        if (selectedObject == null) {
          return;
        }
        if ((selectedObject instanceof Ticket)) {
          ticket = ((Ticket)selectedObject);
        }
      }
      ticket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
      Ticket cloneTicket = ticket.clone(ticket);
      List<TicketItem> ticketItems = cloneTicket.getTicketItems();
      for (Iterator iterator = ticketItems.iterator(); iterator.hasNext();) {
        TicketItem ticketItem = (TicketItem)iterator.next();
        if (!ticketItem.isPrintedToKitchen().booleanValue()) {
          iterator.remove();
        }
      }
      if (cloneTicket.getTicketItems().size() == 0) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "No items to reorder.");
        return;
      }
      ReorderDialog dialog = new ReorderDialog(cloneTicket);
      dialog.setTitle("OROPOS");
      dialog.setDefaultCloseOperation(2);
      dialog.setSize(PosUIManager.getSize(1024, 650));
      dialog.open();
      
      if (dialog.isCanceled()) {
        return;
      }
      List<TicketItem> ticketItemList = dialog.getTicketItems();
      if (ticketItemList == null) {
        return;
      }
      for (Iterator iterator = ticketItemList.iterator(); iterator.hasNext();) {
        TicketItem ticketItem = (TicketItem)iterator.next();
        ticketItem.setTicket(ticket);
        ticket.addToticketItems(ticketItem);
      }
      ticket.calculatePrice();
      OrderController.saveOrder(ticket);
      
      if ((ticket.getOrderType().isShouldPrintToKitchen().booleanValue()) && 
        (ticket.needsKitchenPrint())) {
        ReceiptPrintService.printToKitchen(ticket);
        POSMessageDialog.showMessage(Messages.getString("OrderView.8"));
      }
    }
    catch (Exception e) {
      PosLog.error(getClass(), e);
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, e);
    }
  }
}
