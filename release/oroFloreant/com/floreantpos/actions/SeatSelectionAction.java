package com.floreantpos.actions;

import com.floreantpos.Messages;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.dialog.NumberSelectionDialog2;
import com.floreantpos.ui.dialog.SeatSelectionDialog;
import com.floreantpos.ui.views.order.OrderView;
import java.util.List;

public class SeatSelectionAction extends PosAction
{
  private PosButton btnSeat;
  
  public SeatSelectionAction()
  {
    super("SEAT");
  }
  
  public void setSource(PosButton btnSeat) {
    this.btnSeat = btnSeat;
  }
  
  public void execute()
  {
    doAddSeatNumber();
  }
  
  protected void doAddSeatNumber() {
    Ticket currentTicket = OrderView.getInstance().getCurrentTicket();
    SeatSelectionDialog seatDialog = new SeatSelectionDialog(currentTicket, getSeatNumbers(currentTicket));
    seatDialog.setTitle(Messages.getString("OrderView.12"));
    seatDialog.pack();
    seatDialog.open();
    
    if (seatDialog.isCanceled()) {
      return;
    }
    int seatNumber = seatDialog.getSeatNumber().intValue();
    if (seatNumber == -1) {
      NumberSelectionDialog2 dialog = new NumberSelectionDialog2();
      dialog.setTitle(Messages.getString("OrderView.13"));
      dialog.pack();
      dialog.open();
      
      if (dialog.isCanceled()) {
        return;
      }
      seatNumber = (int)dialog.getValue();
    }
    
    btnSeat.setText(Messages.getString("OrderView.14") + seatNumber);
    btnSeat.putClientProperty("SEAT_NO", Integer.valueOf(seatNumber));
    doAddSeatTreatTicketItem(Integer.valueOf(seatNumber), currentTicket);
  }
  
  private void doAddSeatTreatTicketItem(Integer seatNumber, Ticket currentTicket) {
    TicketItem ticketItem = new TicketItem();
    if (seatNumber.intValue() == 0) {
      ticketItem.setName(Messages.getString("OrderView.16"));
    } else {
      ticketItem.setName(Messages.getString("OrderView.17") + seatNumber);
    }
    ticketItem.setShouldPrintToKitchen(Boolean.valueOf(true));
    ticketItem.setTreatAsSeat(Boolean.valueOf(true));
    ticketItem.setSeatNumber(seatNumber);
    ticketItem.setTicket(currentTicket);
    OrderView.getInstance().getTicketView().addTicketItem(ticketItem);
  }
  
  public Object getLastSeat(Ticket currentTicket) {
    Integer lastSeatNumber = Integer.valueOf(0);
    List<TicketItem> ticketItems = currentTicket.getTicketItems();
    if ((ticketItems != null) && (!ticketItems.isEmpty())) {
      TicketItem lastTicketItem = (TicketItem)ticketItems.get(ticketItems.size() - 1);
      lastSeatNumber = lastTicketItem.getSeatNumber();
    }
    return lastSeatNumber;
  }
  
  public Object getSelectedSeatNumber() {
    Ticket currentTicket = OrderView.getInstance().getCurrentTicket();
    Object seatNumber = btnSeat.getClientProperty("SEAT_NO");
    if (seatNumber == null) {
      return Integer.valueOf(0);
    }
    Integer seatNo = (Integer)seatNumber;
    
    boolean sendToKitchen = false;
    for (TicketItem ticketItem : currentTicket.getTicketItems()) {
      if (ticketItem.isTreatAsSeat().booleanValue())
      {
        int existingSeatNumber = ticketItem.getSeatNumber().intValue();
        if (existingSeatNumber == seatNo.intValue())
          sendToKitchen = ticketItem.isPrintedToKitchen().booleanValue();
      }
    }
    if (sendToKitchen) {
      doAddSeatTreatTicketItem(seatNo, currentTicket);
    }
    return seatNo;
  }
  
  protected List<Integer> getSeatNumbers(Ticket currentTicket) {
    List<Integer> seatNumbers = new java.util.ArrayList();
    
    for (TicketItem ticketItem : currentTicket.getTicketItems()) {
      if ((ticketItem.isTreatAsSeat().booleanValue()) && (!seatNumbers.contains(ticketItem.getSeatNumber()))) {
        seatNumbers.add(ticketItem.getSeatNumber());
      }
    }
    return seatNumbers;
  }
  
  public boolean updateSeatNumber(TicketItem ticketItem) {
    Ticket ticket = ticketItem.getTicket();
    SeatSelectionDialog seatDialog = new SeatSelectionDialog(ticket.getTableNumbers(), getSeatNumbers(ticket));
    seatDialog.setTitle(Messages.getString("TicketView.16"));
    seatDialog.pack();
    seatDialog.open();
    
    if (seatDialog.isCanceled()) {
      return false;
    }
    int seatNumber = seatDialog.getSeatNumber().intValue();
    if (seatNumber == -1) {
      NumberSelectionDialog2 dialog = new NumberSelectionDialog2();
      dialog.setTitle(Messages.getString("TicketView.17"));
      dialog.setValue(ticketItem.getSeatNumber().intValue());
      dialog.pack();
      dialog.open();
      
      if (dialog.isCanceled()) {
        return false;
      }
      seatNumber = (int)dialog.getValue();
    }
    
    ticketItem.setName(Messages.getString("TicketView.18") + seatNumber);
    ticketItem.setSeatNumber(Integer.valueOf(seatNumber));
    updateTicketItemsSeatNumber(ticketItem);
    return true;
  }
  
  private void updateTicketItemsSeatNumber(TicketItem ticketItem) {
    boolean updateSeatNumber = false;
    for (TicketItem item : ticketItem.getTicket().getTicketItems()) {
      if (item == ticketItem) {
        updateSeatNumber = true;

      }
      else if (updateSeatNumber) {
        if (item.isTreatAsSeat().booleanValue()) break;
        item.setSeatNumber(ticketItem.getSeatNumber());
      }
    }
  }
}
