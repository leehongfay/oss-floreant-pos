package com.floreantpos.actions;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.model.UserPermission;
import com.floreantpos.ui.kitchendisplay.LineDisplayWindow;


















public class ShowLineDisplayAction
  extends PosAction
{
  public ShowLineDisplayAction()
  {
    super(Messages.getString("ShowLineDisplayAction.0"));
    setRequiredPermission(UserPermission.VIEW_BACK_OFFICE);
  }
  
  public ShowLineDisplayAction(boolean showText, boolean showIcon) {
    if (showText) {
      putValue("Name", UserPermission.VIEW_BACK_OFFICE);
    }
    if (showIcon) {
      putValue("SmallIcon", IconFactory.getIcon("backoffice.png"));
    }
    
    setRequiredPermission(UserPermission.VIEW_BACK_OFFICE);
  }
  
  public void execute()
  {
    LineDisplayWindow window = new LineDisplayWindow();
    window.setVisible(true);
    window.openFullScreen();
    window.toFront();
  }
}
