package com.floreantpos.actions;

import com.floreantpos.Messages;

















public enum ActionCommand
{
  CAPTURE(Messages.getString("ActionCommand.0")),  CAPTURE_ALL(Messages.getString("ActionCommand.1")),  EDIT_TIPS(Messages.getString("ActionCommand.2")),  CLOSE(Messages.getString("ActionCommand.3")),  VOID_TRANS(Messages.getString("ActionCommand.5")),  OK(Messages.getString("ActionCommand.4"));
  
  private String displayString;
  
  private ActionCommand(String displayString) {
    this.displayString = displayString;
  }
  
  public String toString() {
    return name().replaceAll("_", " ");
  }
  
  public String getDisplayString() {
    return displayString;
  }
}
