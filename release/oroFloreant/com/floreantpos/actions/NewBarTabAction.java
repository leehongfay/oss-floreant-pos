package com.floreantpos.actions;

import com.floreantpos.ITicketList;
import com.floreantpos.Messages;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.config.CardConfig;
import com.floreantpos.extension.PaymentGatewayPlugin;
import com.floreantpos.main.Application;
import com.floreantpos.model.CardReader;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PaymentType;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.TableStatus;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.services.PosTransactionService;
import com.floreantpos.swing.PosOptionPane;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.PaymentTypeSelectionDialog;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.ui.views.payment.AuthorizationCodeDialog;
import com.floreantpos.ui.views.payment.CardInputListener;
import com.floreantpos.ui.views.payment.CardInputProcessor;
import com.floreantpos.ui.views.payment.CardProcessor;
import com.floreantpos.ui.views.payment.ManualCardEntryDialog;
import com.floreantpos.ui.views.payment.PaymentProcessWaitDialog;
import com.floreantpos.ui.views.payment.SwipeCardDialog;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.ShiftUtil;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.Calendar;
import java.util.List;
import javax.swing.AbstractAction;
import org.apache.commons.lang.StringUtils;



















public class NewBarTabAction
  extends AbstractAction
  implements CardInputListener
{
  private Component parentComponent;
  private PaymentType selectedPaymentType;
  private OrderType orderType;
  private List<ShopTable> selectedTables;
  
  public NewBarTabAction(OrderType orderType, List selectedTables, Component parentComponent)
  {
    this.orderType = orderType;
    this.selectedTables = selectedTables;
    this.parentComponent = parentComponent;
  }
  
  public void actionPerformed(ActionEvent e)
  {
    if (!CardConfig.isPreAuthBartab()) {
      doEditTicket(createTicket());
      return;
    }
    PaymentTypeSelectionDialog paymentTypeSelectionDialog = new PaymentTypeSelectionDialog();
    paymentTypeSelectionDialog.setCashButtonVisible(false);
    paymentTypeSelectionDialog.pack();
    paymentTypeSelectionDialog.setLocationRelativeTo(parentComponent);
    paymentTypeSelectionDialog.setVisible(true);
    
    if (paymentTypeSelectionDialog.isCanceled()) {
      return;
    }
    
    selectedPaymentType = paymentTypeSelectionDialog.getSelectedPaymentType();
    
    String symbol = CurrencyUtil.getCurrencySymbol();
    String message = symbol + CardConfig.getBartabLimit() + Messages.getString("NewBarTabAction.3");
    
    int option = POSMessageDialog.showYesNoQuestionDialog(parentComponent, message, Messages.getString("NewBarTabAction.4"));
    if (option != 0) {
      return;
    }
    
    SwipeCardDialog dialog = new SwipeCardDialog(this);
    dialog.setTitle(Messages.getString("NewBarTabAction.0"));
    dialog.pack();
    dialog.open();
  }
  
  private Ticket createTicket() {
    Ticket ticket = new Ticket();
    
    Application application = Application.getInstance();
    ticket.setTaxIncluded(Boolean.valueOf(application.isPriceIncludesTax()));
    ticket.setOrderType(orderType);
    ticket.setTerminal(application.getTerminal());
    ticket.setOwner(Application.getCurrentUser());
    ticket.setShift(ShiftUtil.getCurrentShift());
    
    Calendar currentTime = DateUtil.getServerTimeCalendar();
    ticket.setCreateDate(currentTime.getTime());
    ticket.setCreationHour(Integer.valueOf(currentTime.get(11)));
    
    if ((selectedTables != null) && (!selectedTables.isEmpty())) {
      for (ShopTable shopTable : selectedTables) {
        shopTable.setTableStatus(TableStatus.Seat);
        ticket.addTable(shopTable.getTableNumber().intValue());
      }
    }
    else {
      String customerTabName = PosOptionPane.showInputDialog("Enter bar tab name");
      ticket.addProperty("CUSTOMER_NAME", customerTabName);
    }
    
    return ticket;
  }
  
  public void cardInputted(CardInputProcessor inputter, PaymentType paymentType)
  {
    PaymentProcessWaitDialog waitDialog = new PaymentProcessWaitDialog(Application.getPosWindow());
    try
    {
      waitDialog.setVisible(true);
      
      PosTransaction transaction = selectedPaymentType.createTransaction();
      
      Ticket ticket = createTicket();
      transaction.setTicket(ticket);
      transaction.setAuthorizable(Boolean.valueOf(false));
      transaction.setTenderAmount(Double.valueOf(CardConfig.getBartabLimit()));
      
      PaymentGatewayPlugin paymentGateway = CardConfig.getPaymentGateway();
      CardProcessor cardProcessor = paymentGateway.getProcessor();
      
      if ((inputter instanceof SwipeCardDialog))
      {
        SwipeCardDialog swipeCardDialog = (SwipeCardDialog)inputter;
        String cardString = swipeCardDialog.getCardString();
        
        if ((StringUtils.isEmpty(cardString)) || (cardString.length() < 16)) {
          throw new RuntimeException(Messages.getString("SettleTicketDialog.16"));
        }
        
        transaction.setCardType(paymentType.getDisplayString());
        transaction.setCardTrack(cardString);
        transaction.setCaptured(Boolean.valueOf(false));
        transaction.setCardMerchantGateway(paymentGateway.getProductName());
        transaction.setCardReader(CardReader.SWIPE.name());
        
        if (ticket.getOrderType().isPreAuthCreditCard().booleanValue()) {
          cardProcessor.preAuth(transaction);
        }
        else {
          cardProcessor.chargeAmount(transaction);
        }
        
        saveTicket(transaction);

      }
      else if ((inputter instanceof ManualCardEntryDialog))
      {
        ManualCardEntryDialog mDialog = (ManualCardEntryDialog)inputter;
        
        transaction.setCaptured(Boolean.valueOf(false));
        transaction.setCardMerchantGateway(paymentGateway.getProductName());
        transaction.setCardReader(CardReader.MANUAL.name());
        transaction.setCardNumber(mDialog.getCardNumber());
        transaction.setCardExpMonth(mDialog.getExpMonth());
        transaction.setCardExpYear(mDialog.getExpYear());
        
        cardProcessor.preAuth(transaction);
        
        saveTicket(transaction);
      }
      else if ((inputter instanceof AuthorizationCodeDialog))
      {
        PosTransaction selectedTransaction = selectedPaymentType.createTransaction();
        selectedTransaction.setTicket(ticket);
        
        AuthorizationCodeDialog authDialog = (AuthorizationCodeDialog)inputter;
        String authorizationCode = authDialog.getAuthorizationCode();
        if (StringUtils.isEmpty(authorizationCode)) {
          throw new PosException(Messages.getString("SettleTicketDialog.17"));
        }
        
        selectedTransaction.setCardType(selectedPaymentType.getDisplayString());
        selectedTransaction.setCaptured(Boolean.valueOf(true));
        selectedTransaction.setAuthorizable(Boolean.valueOf(false));
        selectedTransaction.setCardReader(CardReader.EXTERNAL_TERMINAL.name());
        selectedTransaction.setCardAuthCode(authorizationCode);
        
        saveTicket(selectedTransaction);
      }
    } catch (Exception e) {
      PosLog.error(getClass(), e);
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    } finally {
      waitDialog.setVisible(false);
    }
  }
  
  private void saveTicket(PosTransaction transaction) {
    try {
      PosTransactionService transactionService = PosTransactionService.getInstance();
      transactionService.settleBarTabTicket(transaction.getTicket(), transaction, false, Application.getCurrentUser());
      
      POSMessageDialog.showMessage(Messages.getString("NewBarTabAction.5") + transaction.getTicket().getId());
      if ((parentComponent instanceof ITicketList)) {
        ((ITicketList)parentComponent).updateTicketList();
      }
      
      doEditTicket(transaction.getTicket());
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    }
  }
  
  private void doEditTicket(Ticket ticket) {
    OrderView.getInstance().setCurrentTicket(ticket);
    RootView.getInstance().showView("ORDER_VIEW");
  }
}
