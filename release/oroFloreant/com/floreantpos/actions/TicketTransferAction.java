package com.floreantpos.actions;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.UserListDialog;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import com.floreantpos.util.POSUtil;

public class TicketTransferAction
  extends PosAction
{
  private Ticket ticket;
  private boolean isTransfered = false;
  private User currentUser;
  
  public TicketTransferAction() {
    super("TRANSFER");
  }
  
  public TicketTransferAction(Ticket ticket, User currentUser) {
    super("TRANSFER");
    this.ticket = ticket;
    this.currentUser = currentUser;
  }
  
  public TicketTransferAction(DataChangeListener listener) {
    super("TRANSFER", listener);
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void execute()
  {
    try {
      if (listener != null) {
        Object selectedObject = getSelectedObject();
        if (selectedObject == null) {
          return;
        }
        if ((selectedObject instanceof Ticket)) {
          ticket = ((Ticket)selectedObject);
        }
      }
      if (ticket == null) {
        return;
      }
      if (ticket.isVoided().booleanValue()) {
        POSMessageDialog.showMessage(Application.getPosWindow(), POSConstants.TICKET_IS_VOIDED);
        return;
      }
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Are you sure to transfer this ticket?", "Transfer");
      
      if (option != 0) {
        return;
      }
      
      ticket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
      
      if (currentUser == null) {
        currentUser = Application.getCurrentUser();
      }
      User owner = ticket.getOwner();
      
      if ((!currentUser.equals(owner)) && 
        (!currentUser.hasPermission(UserPermission.TRANSFER_TICKET))) {
        POSMessageDialog.showError(POSUtil.getFocusedWindow(), Messages.getString("OrderInfoDialog.4") + ticket.getId());
        return;
      }
      

      UserListDialog dialog = new UserListDialog();
      dialog.hideUser(owner);
      dialog.setTitle(Messages.getString("UserTransferDialog.0"));
      dialog.setCaption(Messages.getString("UserTransferDialog.1"));
      dialog.setSize(PosUIManager.getSize(400, 600));
      dialog.open();
      
      if (dialog.isCanceled())
        return;
      User selectedUser = dialog.getSelectedUser();
      doTransferTickets(selectedUser);
      POSMessageDialog.showMessage("Successfully transferred.");
    } catch (PosException e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  protected void doTransferTickets(User selectedUser) {
    if (selectedUser == null) {
      return;
    }
    ticket.setOwner(selectedUser);
    TicketDAO.getInstance().saveOrUpdate(ticket);
    isTransfered = true;
  }
  
  public boolean isTransfered() {
    return isTransfered;
  }
}
