package com.floreantpos.actions;

import com.floreantpos.IconFactory;
import com.floreantpos.main.Application;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.ProgressDialog;
import com.floreantpos.swing.ProgressObserver;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;



















public class CacheDataUpdateAction
  extends ViewChangeAction
{
  public CacheDataUpdateAction() {}
  
  public CacheDataUpdateAction(boolean showText, boolean showIcon)
  {
    if (showIcon) {
      putValue("SmallIcon", IconFactory.getIcon("refresh_24.png"));
    }
  }
  
  public void execute()
  {
    try {
      ProgressDialog dialog = new ProgressDialog()
      {
        public void execute(ProgressObserver progressObserver)
        {
          try {
            DataProvider.get().initialize();
          } catch (Exception e) {
            POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
          }
        }
      };
      dialog.setProgressLabelText("Refreshing data..");
      dialog.showCompleteMsg("Data refreshed.");
      dialog.setIndeterminate(true);
      dialog.setSize(PosUIManager.getSize(400, 180));
      dialog.open();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
}
