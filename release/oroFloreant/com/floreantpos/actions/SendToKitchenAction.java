package com.floreantpos.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.SendToKitchenOptionSelectionDialog;
import com.floreantpos.ui.views.order.actions.DataChangeListener;

public class SendToKitchenAction extends PosAction
{
  public SendToKitchenAction()
  {
    super("SEND");
  }
  
  public SendToKitchenAction(DataChangeListener listener) {
    super("SEND", listener);
  }
  
  public void execute()
  {
    try {
      Object selectedObject = getSelectedObject();
      if (selectedObject == null) {
        return;
      }
      Ticket ticket = null;
      if ((selectedObject instanceof Ticket)) {
        ticket = (Ticket)selectedObject;
      }
      else {
        ticket = com.floreantpos.services.TicketService.getTicket((String)selectedObject);
      }
      if (ticket.isVoided().booleanValue()) {
        POSMessageDialog.showMessage(Application.getPosWindow(), POSConstants.TICKET_IS_VOIDED);
        return;
      }
      TicketDAO.getInstance().loadFullTicket(ticket);
      
      SendToKitchenOptionSelectionDialog sendTicketDialog = new SendToKitchenOptionSelectionDialog(ticket);
      sendTicketDialog.pack();
      sendTicketDialog.open();
      if (sendTicketDialog.isCanceled())
        return;
    } catch (PosException e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
}
