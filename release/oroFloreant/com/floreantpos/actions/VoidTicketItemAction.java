package com.floreantpos.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.main.Application;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.ext.KitchenStatus;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.voidticket.VoidTicketItemDialog;

















public class VoidTicketItemAction
  extends PosAction
{
  private TicketItem ticketItem;
  private boolean dataChanged = false;
  private Ticket ticket;
  
  public VoidTicketItemAction(Ticket ticket, TicketItem ticketItem) {
    super(POSConstants.VOID);
    
    KitchenStatus kitchenStatusValue = ticketItem.getKitchenStatusValue();
    if ((ticketItem.isPrintedToKitchen().booleanValue()) && (kitchenStatusValue == KitchenStatus.BUMP)) {
      super.setRequiredPermission(UserPermission.VOID_COOKED_ITEM);
    }
    else if (((ticketItem.isPrintedToKitchen().booleanValue()) && (kitchenStatusValue == null)) || (kitchenStatusValue == KitchenStatus.UNKNOWN)) {
      super.setRequiredPermission(UserPermission.VOID_KITCHEN_SENT_ITEM);
    }
    
    this.ticketItem = ticketItem;
    this.ticket = ticket;
  }
  
  public void execute()
  {
    try {
      if ((ticketItem == null) || (!checkDrawerAssignment(Application.getInstance().getTerminal(), Application.getCurrentUser())))
        return;
      dataChanged = false;
      TicketDAO.getInstance().loadFullTicket(ticket);
      VoidTicketItemDialog dialog = new VoidTicketItemDialog(ticket, ticketItem);
      dialog.setSize(PosUIManager.getSize(400, 600));
      dialog.open();
      if (dialog.isCanceled())
        return;
      dataChanged = true;
    } catch (PosException e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public static boolean checkDrawerAssignment(Terminal terminal, User user) {
    if (terminal.isCashDrawerAssigned()) {
      return true;
    }
    if ((user.isStaffBank().booleanValue()) && (user.isStaffBankStarted().booleanValue())) {
      return true;
    }
    showUnableToVoid();
    return false;
  }
  
  private static void showUnableToVoid() {
    POSMessageDialog.showError(Application.getPosWindow(), "Unable to perform void. Drawer has not been assigned.\nEither assign drawer or start staff bank.");
  }
  
  public boolean isDataChanged()
  {
    return dataChanged;
  }
}
