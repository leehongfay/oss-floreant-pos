package com.floreantpos.actions;

import com.floreantpos.PosException;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.services.TicketService;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.VoidPaymentDialog;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import com.floreantpos.util.POSUtil;

public class VoidPaymentAction extends PosAction
{
  public VoidPaymentAction()
  {
    super("VOID PAYMENT");
    setRequiredPermission(UserPermission.PERFORM_MANAGER_TASK);
  }
  
  public VoidPaymentAction(DataChangeListener listener) {
    super("VOID PAYMENT", listener);
    setRequiredPermission(UserPermission.PERFORM_MANAGER_TASK);
  }
  
  public void execute()
  {
    try {
      Object selectedObject = getSelectedObject();
      if (selectedObject == null) {
        return;
      }
      Ticket ticket = null;
      if ((selectedObject instanceof Ticket)) {
        ticket = (Ticket)selectedObject;
      }
      else {
        ticket = TicketService.getTicket((String)selectedObject);
      }
      if (ticket == null) {
        return;
      }
      
      TicketDAO.getInstance().loadFullTicket(ticket);
      VoidPaymentDialog refundDialog = new VoidPaymentDialog(POSUtil.getFocusedWindow(), ticket);
      refundDialog.setSize(com.floreantpos.swing.PosUIManager.getSize(800, 600));
      refundDialog.open();
      
      if (!ticket.isPaid().booleanValue()) {
        new SettleTicketAction(ticket).actionPerformed(null);
      }
    }
    catch (PosException e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
}
