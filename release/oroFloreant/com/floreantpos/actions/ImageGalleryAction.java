package com.floreantpos.actions;

import com.floreantpos.ui.dialog.ImageGalleryDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

public class ImageGalleryAction
  extends AbstractAction
{
  public ImageGalleryAction()
  {
    super("Image Gallery");
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    try {
      ImageGalleryDialog dialog = ImageGalleryDialog.getInstance();
      dialog.setSelectBtnVisible(false);
      dialog.setTitle("Image Gallery");
      dialog.openFullScreen();
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    }
  }
}
