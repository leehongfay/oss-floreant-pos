package com.floreantpos.actions;

import com.floreantpos.model.Ticket;
import com.floreantpos.model.UserPermission;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.actions.DataChangeListener;

public class RefundAction extends PosAction
{
  public RefundAction(DataChangeListener listener)
  {
    super(com.floreantpos.Messages.getString("RefundAction.0"), listener);
    super.setRequiredPermission(UserPermission.REFUND);
  }
  
  public void execute()
  {
    try
    {
      if (listener == null)
        return;
      Ticket ticket = null;
      Object selectedObject = getSelectedObject();
      if (selectedObject == null) {
        return;
      }
      if ((selectedObject instanceof Ticket)) {
        ticket = (Ticket)selectedObject;
      }
      VoidTicketAction action = new VoidTicketAction(ticket);
      action.execute();
      

































      if (listener != null) {
        listener.dataChanged(ticket);
      }
    } catch (Exception e) {
      POSMessageDialog.showError(com.floreantpos.main.Application.getPosWindow(), e.getMessage(), e);
    }
  }
}
