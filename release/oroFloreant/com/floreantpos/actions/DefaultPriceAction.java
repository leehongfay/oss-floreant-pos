package com.floreantpos.actions;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.bo.ui.explorer.DefaultPriceExplorer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

















public class DefaultPriceAction
  extends AbstractAction
{
  public DefaultPriceAction()
  {
    super("Default Prices");
  }
  
  public DefaultPriceAction(String name) {
    super(name);
  }
  
  public DefaultPriceAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      DefaultPriceExplorer explorer = null;
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab("Default Price Explorer");
      if (index == -1) {
        explorer = new DefaultPriceExplorer();
        tabbedPane.addTab("Default Price Explorer", explorer);
      }
      else {
        explorer = (DefaultPriceExplorer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(explorer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
