package com.floreantpos.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.UserPermission;
import com.floreantpos.ui.dialog.PayoutDialog;
import com.floreantpos.util.POSUtil;
















public class PayoutAction
  extends PosAction
{
  public PayoutAction()
  {
    super(POSConstants.PAYOUT_BUTTON_TEXT, UserPermission.PAY_OUT);
  }
  
  public void execute()
  {
    if (!POSUtil.checkDrawerAssignment()) {
      return;
    }
    PayoutDialog dialog = new PayoutDialog(Application.getPosWindow(), Application.getCurrentUser(), Application.getInstance().refreshAndGetTerminal());
    dialog.open();
  }
}
