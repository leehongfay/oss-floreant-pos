package com.floreantpos.actions;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import java.awt.Window;



















public class LogoutAction
  extends ViewChangeAction
{
  public LogoutAction()
  {
    super(Messages.getString("Logout"));
  }
  
  public LogoutAction(boolean showText, boolean showIcon) {
    if (showText) {
      putValue("Name", Messages.getString("Logout"));
    }
    if (showIcon) {
      putValue("SmallIcon", IconFactory.getIcon("/ui_icons/", "logout.png"));
    }
  }
  
  public void execute()
  {
    Window[] windows = Window.getWindows();
    for (Window window : windows) {
      if (!(window instanceof PosWindow)) {
        window.setVisible(false);
        window.dispose();
      }
    }
    Application.getInstance().doLogout();
  }
}
