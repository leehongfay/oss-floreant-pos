package com.floreantpos.actions;

import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.UserPermission;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.views.payment.AuthorizableTicketBrowser;


















public class ShowTransactionsAuthorizationsAction
  extends PosAction
{
  public ShowTransactionsAuthorizationsAction()
  {
    super(POSConstants.AUTHORIZE_BUTTON_TEXT, UserPermission.AUTHORIZE_TICKETS);
  }
  

  public void execute()
  {
    AuthorizableTicketBrowser dialog = new AuthorizableTicketBrowser(Application.getPosWindow(), Application.getCurrentUser(), Application.getInstance().getTerminal());
    dialog.setDefaultCloseOperation(2);
    dialog.setSize(PosUIManager.getSize(800, 600));
    dialog.setLocationRelativeTo(Application.getPosWindow());
    dialog.setVisible(true);
  }
}
