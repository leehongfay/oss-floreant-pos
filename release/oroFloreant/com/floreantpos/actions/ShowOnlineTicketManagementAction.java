package com.floreantpos.actions;

import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.TicketImportPlugin;
import com.floreantpos.main.Application;
import com.floreantpos.model.UserPermission;
import com.floreantpos.ui.dialog.POSMessageDialog;
















public class ShowOnlineTicketManagementAction
  extends PosAction
{
  private TicketImportPlugin ticketImportPlugin;
  
  public ShowOnlineTicketManagementAction()
  {
    super("ONLINE TICKET STAT", UserPermission.DRAWER_PULL);
    ticketImportPlugin = ((TicketImportPlugin)ExtensionManager.getPlugin(TicketImportPlugin.class));
    setVisible(ticketImportPlugin != null);
  }
  
  public void execute()
  {
    try {
      if (ticketImportPlugin != null) {
        ticketImportPlugin.startService();
      }
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
}
