package com.floreantpos;

import com.floreantpos.model.Ticket;

public abstract interface ITicketList
{
  public abstract Ticket getSelectedTicket();
  
  public abstract void updateTicketList();
  
  public abstract void updateCustomerTicketList(String paramString);
}
