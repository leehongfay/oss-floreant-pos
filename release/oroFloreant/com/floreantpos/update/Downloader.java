package com.floreantpos.update;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Iterator;

public class Downloader
{
  private ProgressObserver observer;
  
  public Downloader() {}
  
  public void download(ProgressObserver observer, String link, String filesxml, String destinationdir, Modes mode) throws org.xml.sax.SAXException, FileNotFoundException, java.io.IOException, InterruptedException
  {
    this.observer = observer;
    UpdateXMLParser parser = new UpdateXMLParser();
    Iterator iterator = parser.parse(filesxml, mode).iterator();
    

    File dir = new File(destinationdir);
    if (!dir.exists()) {
      dir.mkdir();
    }
    URL url = new URL(filesxml);
    download(url, destinationdir + File.separator + new File(url.getFile()).getName());
    String findPathJar = Updater.findPathJar(observer.getParentComponent().getClass());
    if (findPathJar.contains(".jar")) {
      findPathJar = findPathJar.substring(0, findPathJar.lastIndexOf(File.separator));
    }
    while (iterator.hasNext()) {
      Information info = (Information)iterator.next();
      url = new URL(link + "/" + info.getFilename());
      try {
        download(url, destinationdir + File.separator + new File(url.getFile()).getName());
      } catch (FileNotFoundException e) {}
      if (!info.isPlugin().booleanValue())
      {

        throw e;
      }
    }
  }
  
  private void download(URL url, String destination) throws java.net.MalformedURLException, java.io.IOException {
    java.net.URLConnection conn = url.openConnection();
    InputStream in = conn.getInputStream();
    
    File dstfile = new File(destination);
    OutputStream out = new java.io.FileOutputStream(dstfile);
    
    byte[] buffer = new byte['Ȁ'];
    

    long totalBytesRead = 0L;
    int percentCompleted = 0;
    long fileSize = conn.getContentLength();
    int length;
    while ((length = in.read(buffer)) > 0) {
      out.write(buffer, 0, length);
      totalBytesRead += length;
      percentCompleted = (int)(totalBytesRead * 100L / fileSize);
      observer.progress(percentCompleted);
    }
    in.close();
    out.close();
  }
}
