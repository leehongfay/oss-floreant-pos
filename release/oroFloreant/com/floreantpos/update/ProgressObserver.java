package com.floreantpos.update;

import java.awt.Component;

public abstract interface ProgressObserver
{
  public abstract void progress(int paramInt);
  
  public abstract void progress(int paramInt, String paramString);
  
  public abstract Component getParentComponent();
}
