package com.floreantpos.update;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import net.miginfocom.swing.MigLayout;


public class UpdateChangesInfoDialog
  extends JDialog
{
  private boolean canceled;
  private JEditorPane taChanges;
  private String url;
  private Modes readMode;
  private Component parent;
  private boolean readFromResource;
  private URL changeLogURL;
  
  public UpdateChangesInfoDialog(Component parent, String url)
  {
    this(parent, url, Modes.URL);
  }
  
  public UpdateChangesInfoDialog(Component parent, URL url, boolean resourceFile) {
    this(parent, null, Modes.FILE);
    readFromResource = resourceFile;
    changeLogURL = url;
  }
  
  public UpdateChangesInfoDialog(Component parent, String url, Modes modes) {
    super((JFrame)parent, true);
    String title = ((JFrame)parent).getTitle();
    setTitle(title);
    this.url = url;
    readMode = modes;
    this.parent = parent;
    setIconImage(((JFrame)parent).getIconImage());
    setLayout(new BorderLayout());
    
    JPanel headerPanel = new JPanel(new MigLayout("fillx,ins 0", "", ""));
    headerPanel.setBackground(Color.white);
    
    JLabel lblUpdateTitle = new JLabel("Changes");
    lblUpdateTitle.setFont(new Font(null, 1, 16));
    lblUpdateTitle.setOpaque(false);
    headerPanel.add(lblUpdateTitle, "gapleft 10,growx,span");
    headerPanel.add(new JSeparator(), "growx,span");
    
    add(headerPanel, "North");
    
    JPanel contentPanel = new JPanel(new MigLayout("fill,ins 20"));
    
    taChanges = new JEditorPane("text/html", "");
    taChanges.setEditable(false);
    taChanges.setBackground(Color.white);
    taChanges.setBorder(BorderFactory.createCompoundBorder(null, BorderFactory.createEmptyBorder(10, 8, 5, 5)));
    JScrollPane scrollPane = new JScrollPane(taChanges);
    scrollPane.setBorder(BorderFactory.createLineBorder(new Color(200, 200, 200)));
    contentPanel.add(scrollPane, "grow,span");
    
    add(contentPanel);
    
    JPanel buttonPanel = new JPanel(new MigLayout("fillx,center,ins 0 0 5 0"));
    buttonPanel.add(new JSeparator(), "growx,span,gapbottom 5");
    
    JButton btnCancel = new JButton("OK ");
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        canceled = false;
        dispose();
      }
    });
    buttonPanel.add(btnCancel, "center,h 35!,w 90!");
    add(buttonPanel, "South");
  }
  
  public void setVisible(boolean b)
  {
    if (b) {
      SwingUtilities.invokeLater(new Runnable()
      {
        public void run()
        {
          if (readMode == Modes.FILE) {
            showCurrentChanges();
          }
          else {
            showNewChanges();
          }
        }
      });
    }
    super.setVisible(b);
  }
  
  protected void showNewChanges() {
    try {
      StringBuilder builder = new StringBuilder();
      UpdateXMLParser updateParser = new UpdateXMLParser();
      ArrayList<Information> versionInfos = updateParser.parse(url + "/" + "update.xml", Modes.URL);
      builder.append("<html>");
      if (versionInfos != null) {
        for (Iterator vIterator = versionInfos.iterator(); vIterator.hasNext();) {
          Information versionInfo = (Information)vIterator.next();
          String changesLogFile = versionInfo.getChangesLogFile();
          if ((changesLogFile != null) && (!changesLogFile.isEmpty()))
          {
            appendChanges(url, changesLogFile, builder); }
        }
        builder.append("</html>");
        taChanges.setText(builder.toString());
      }
    }
    catch (Exception localException) {}
  }
  
  protected void showCurrentChanges() {
    try {
      StringBuilder builder = new StringBuilder();
      builder.append("<html>");
      String changesLogFile = "change.log.xml";
      String findPathJar = "";
      if (!readFromResource) {
        findPathJar = Updater.findPathJar(parent.getClass());
        if (findPathJar.contains(".jar")) {
          findPathJar = findPathJar.substring(0, findPathJar.lastIndexOf(File.separator));
        }
        findPathJar = findPathJar.replace("/classes", "");
      }
      else {
        findPathJar = url;
      }
      appendChanges(findPathJar, changesLogFile, builder);
      builder.append("</html>");
      taChanges.setText(builder.toString());
    } catch (Exception localException) {}
  }
  
  private void appendChanges(String url, String changesLogFile, StringBuilder builder) {
    Iterator iterator;
    try {
      ChangesXMLParser parser = new ChangesXMLParser();
      ArrayList<Version> versions = null;
      if (readFromResource) {
        versions = parser.parse(changeLogURL);
      }
      else {
        versions = parser.parse(url + (readMode == Modes.URL ? "/" : File.separator) + changesLogFile, readMode);
      }
      for (iterator = versions.iterator(); iterator.hasNext();) {
        Version version = (Version)iterator.next();
        builder.append("<b>Version: " + version.getVersionNo() + "</b><br>");
        if (version.getChanges() != null) {
          for (String change : version.getChanges()) {
            builder.append("&nbsp;&nbsp;&nbsp;" + change + "<br>");
          }
          builder.append("<br>");
        }
      }
    }
    catch (Exception localException) {}
  }
  
  public void setTitle(String title, String labelText) {
    setTitle(title);
  }
  
  public boolean isCanceled() {
    return canceled;
  }
}
