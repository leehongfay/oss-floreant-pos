package com.floreantpos.update;

import java.awt.Component;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.xml.sax.SAXException;

public class UpdateService
{
  public static final String UPDATE_FILE = "update.xml";
  public static final String UP_TO_DATE = "up_to_date";
  private static UpdateListener listener;
  
  public UpdateService() {}
  
  public static void checkForUpdate(Component parent, UpdateListener updateListener, String url, String appName, String appVersion, int numVersion)
  {
    try
    {
      Information newUpdate = getNewVersionInfo(url, appVersion, numVersion);
      if (newUpdate == null)
        return;
      String newVersion = newUpdate.getPkgver();
      if (newVersion == null) {
        return;
      }
      appName = newUpdate.getAppName();
      if (appName == null) {
        appName = "";
      }
      listener = updateListener;
      propmtToUpdate(parent, url, newVersion, appName, appVersion, numVersion);
    }
    catch (Exception localException) {}
  }
  


  public static void propmtToUpdate(Component parent, String url, String newVersion, String productName, String appVersion, int numVersion)
  {
    UpdatePromptDialog promptDialog = new UpdatePromptDialog(parent, url, appVersion, productName, newVersion, numVersion);
    promptDialog.pack();
    promptDialog.setLocationRelativeTo(parent);
    promptDialog.setDefaultCloseOperation(0);
    promptDialog.setVisible(true);
    if (promptDialog.isCanceled())
      return;
    UpdateProgressDialog dialog = new UpdateProgressDialog(parent, false, listener, url);
    dialog.setSize(400, 150);
    dialog.setTitle("Downloading software in progress.");
    dialog.setProgressTitle("Downloading..");
    dialog.setLocationRelativeTo(parent);
    dialog.setVisible(true);
  }
  
  public static String getAvaiableUpdate(String url, String appVersion, int numVersion) throws Exception {
    try {
      Information serverVersionInfo = getLatestVersion(url);
      Information currentVersionInfo = new Information();
      currentVersionInfo.setPkgver(appVersion);
      currentVersionInfo.setPkgrel(numVersion);
      if (serverVersionInfo == null)
        return null;
      if (serverVersionInfo.compareTo(currentVersionInfo) > 0) {
        return serverVersionInfo.getPkgver();
      }
      return "up_to_date";
    } catch (Exception ex) {
      throw ex;
    }
  }
  
  public static Information getNewVersionInfo(String url, String appVersion, int numVersion) throws Exception {
    try {
      Information serverVersionInfo = getLatestVersion(url);
      Information currentVersionInfo = new Information();
      currentVersionInfo.setPkgver(appVersion);
      currentVersionInfo.setPkgrel(numVersion);
      if (serverVersionInfo == null)
        return null;
      if (serverVersionInfo.compareTo(currentVersionInfo) > 0) {
        return serverVersionInfo;
      }
    } catch (Exception ex) {
      throw ex;
    }
    return null;
  }
  
  public static Information getLatestVersion(String url) throws Exception {
    UpdateXMLParser parser = new UpdateXMLParser();
    if (url.endsWith("/")) {
      url = url + "update.xml";
    }
    else {
      url = url + "/update.xml";
    }
    Iterator<Information> iterator = parser.parse(url, Modes.URL).iterator();
    if (iterator.hasNext()) {
      return (Information)iterator.next();
    }
    return null;
  }
  
  public static boolean download(ProgressObserver observer, String url) {
    Component rootPane = observer.getParentComponent();
    try {
      observer.progress(0, "Downloading..");
      Downloader dl = new Downloader();
      dl.download(observer, url, url + "/" + "update.xml", System.getProperty("java.io.tmpdir") + "/oropos", Modes.URL);
      return true;
    } catch (SAXException ex) {
      JOptionPane.showMessageDialog(rootPane, "The xml wasn't loaded succesfully!\n", "Something went wrong!", 2);
      Logger.getLogger(UpdateService.class.getName()).log(Level.SEVERE, null, ex);
    } catch (FileNotFoundException ex) {
      JOptionPane.showMessageDialog(rootPane, "Files were unable to be read or created successfully!\nPlease be sure that you have the right permissions and internet connectivity!", "Something went wrong!", 2);
      
      Logger.getLogger(UpdateService.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
      JOptionPane.showMessageDialog(rootPane, "Please check your internet connectivity.", "Something went wrong!", 2);
      Logger.getLogger(UpdateService.class.getName()).log(Level.SEVERE, null, ex);
    } catch (InterruptedException ex) {
      JOptionPane.showMessageDialog(rootPane, "The connection has been lost!\nPlease check your internet connectivity!", "Something went wrong!", 2);
      
      Logger.getLogger(UpdateService.class.getName()).log(Level.SEVERE, null, ex);
    }
    return false;
  }
  

  public static boolean update(ProgressObserver observer, Component rootPane, String destination)
  {
    try
    {
      observer.progress(0, "Extracting...");
      Thread.sleep(500L);
      Updater update = new Updater();
      update.update(observer, "update.xml", System.getProperty("java.io.tmpdir") + "/oropos", Modes.FILE, destination);
      System.out.println("Update completed.......");
      
      PosOptionPane.showMessage(rootPane, "The update was completed successfully.\nPlease restart the application in order the changes take effect."); } catch (Exception ex) { File tmp;
      File[] arrayOfFile1;
      int i; int j; File file; JOptionPane.showMessageDialog(rootPane, "Something went wrong, failed to update..", "Error!", 2);
      Logger.getLogger(UpdateService.class.getName()).log(Level.SEVERE, null, ex);
      observer.progress(0, "Failed.");
      boolean bool = false;
      



      File tmp = new File(System.getProperty("java.io.tmpdir") + "/oropos");
      if (tmp.exists()) {
        File[] arrayOfFile2 = tmp.listFiles();file = arrayOfFile2.length; for (File localFile1 = 0; localFile1 < file; localFile1++) { File file = arrayOfFile2[localFile1];
          file.delete();
        }
        tmp.delete();
      }
      return bool;

    }
    finally
    {
      File tmp = new File(System.getProperty("java.io.tmpdir") + "/oropos");
      if (tmp.exists()) {
        for (File file : tmp.listFiles()) {
          file.delete();
        }
        tmp.delete();
      }
    }
    




    return false;
  }
  
  public static void restart() throws IOException, InterruptedException, java.net.URISyntaxException {
    String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
    String classPath = System.getProperty("java.class.path");
    String mainClass = System.getProperty("sun.java.command");
    ArrayList<String> command = new ArrayList();
    command.add(javaBin);
    command.add("-cp");
    command.add(classPath);
    command.add(mainClass);
    ProcessBuilder builder = new ProcessBuilder(command);
    builder.start();
    System.exit(0);
  }
}
