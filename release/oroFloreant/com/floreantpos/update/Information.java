package com.floreantpos.update;

public class Information implements Comparable {
  private Action action;
  private String filename;
  private String pubDate;
  private String pkgver;
  private int pkgrel;
  private String destination;
  private Boolean plugin;
  private String changesLogFile;
  private String appName;
  private boolean removeOldPlugins;
  
  public Information() {}
  
  public Action getAction() { return action; }
  
  public String getFilename()
  {
    return filename;
  }
  
  public void setFilename(String filename) {
    this.filename = filename;
  }
  
  public void setAction(Action action) {
    this.action = action;
  }
  
  public String getPubDate() {
    return pubDate;
  }
  
  public void setPubDate(String pubDate) {
    this.pubDate = pubDate;
  }
  
  public String getPkgver() {
    return pkgver;
  }
  
  public void setPkgver(String pkgver) {
    this.pkgver = pkgver;
  }
  
  public int getPkgrel() {
    return pkgrel;
  }
  
  public void setPkgrel(int pkgrel) {
    this.pkgrel = pkgrel;
  }
  
  public void setAction(String value) {
    if (value.equalsIgnoreCase("MOVE")) {
      action = Action.MOVE;
    }
    else if (value.equalsIgnoreCase("DELETE")) {
      action = Action.DELETE;
    }
    else if (value.equalsIgnoreCase("EXECUTE")) {
      action = Action.EXECUTE;
    }
  }
  
  public int compareTo(Object release)
  {
    return pkgrel - ((Information)release).getPkgrel();
  }
  














  public String getDestination()
  {
    return destination;
  }
  
  public void setDestination(String findPathJar) {
    destination = findPathJar;
  }
  
  public Boolean isPlugin() {
    return Boolean.valueOf(plugin == null ? false : plugin.booleanValue());
  }
  
  public void setPlugin(Boolean plugin) {
    this.plugin = plugin;
  }
  
  public String getChangesLogFile() {
    return changesLogFile;
  }
  
  public void setChangesLogFile(String changesLogFile) {
    this.changesLogFile = changesLogFile;
  }
  
  public void setAppName(String appName) {
    this.appName = appName;
  }
  
  public String getAppName() {
    return appName;
  }
  
  public boolean isRemoveOldPlugins() {
    return removeOldPlugins;
  }
  
  public void setRemoveOldPlugins(boolean removeOldPlugins) {
    this.removeOldPlugins = removeOldPlugins;
  }
}
