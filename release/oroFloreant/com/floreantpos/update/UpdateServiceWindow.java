package com.floreantpos.update;

import javax.swing.ImageIcon;

public class UpdateServiceWindow extends javax.swing.JFrame implements UpdateListener {
  public UpdateServiceWindow() {}
  
  public static void main(String[] args) throws Exception {
    System.out.println("Starting updates .......");
    
    UpdateServiceWindow window = new UpdateServiceWindow();
    ImageIcon icon = new ImageIcon("download.png");
    window.setIconImage(icon.getImage());
    String url = "";
    if (args.length > 0) {
      url = args[0];
    }
    if ((url == null) || (url.isEmpty())) {
      System.out.println("Error: Update url address not defined.");
    }
    UpdateProgressDialog dialog = new UpdateProgressDialog(window, true, window, url);
    dialog.setSize(400, 150);
    dialog.setTitle("Updating software in progress.");
    dialog.setProgressTitle("Updating..");
    dialog.setLocationRelativeTo(window);
    dialog.setIconImage(icon.getImage());
    dialog.setVisible(true);
  }
  

  public void downloadComplete() {}
  

  public void updateComplete()
  {
    System.exit(0);
  }
}
