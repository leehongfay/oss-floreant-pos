package com.floreantpos.update;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class ChangesXMLParser
{
  public ChangesXMLParser() {}
  
  public ArrayList<Version> parse(String filename, Modes mode) throws SAXException, FileNotFoundException, java.io.IOException, InterruptedException
  {
    XMLReader reader = XMLReaderFactory.createXMLReader();
    ChangesXMLParserHandler handler = new ChangesXMLParserHandler();
    reader.setContentHandler(handler);
    reader.setErrorHandler(handler);
    
    if (mode == Modes.FILE) {
      reader.parse(new InputSource(new java.io.FileReader(new java.io.File(filename))));
    }
    else {
      URL u = new URL(filename);
      URLConnection conn = u.openConnection();
      InputStream in = conn.getInputStream();
      reader.parse(new InputSource(in));
    }
    
    return handler.getVersions();
  }
  
  public ArrayList<Version> parse(URL uri) throws SAXException, FileNotFoundException, java.io.IOException, InterruptedException
  {
    InputStream inputStream = uri.openStream();
    try
    {
      XMLReader reader = XMLReaderFactory.createXMLReader();
      ChangesXMLParserHandler handler = new ChangesXMLParserHandler();
      reader.setContentHandler(handler);
      reader.setErrorHandler(handler);
      reader.parse(new InputSource(inputStream));
      return handler.getVersions();
    } finally {
      try {
        inputStream.close();
      }
      catch (Exception localException1) {}
    }
  }
}
