package com.floreantpos.update;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Properties;
import java.util.zip.ZipEntry;

public class ZipUtil
{
  java.util.List<String> fileList;
  private static final int DEFAULT_BUFFER_SIZE = 4096;
  private static final int EOF = -1;
  
  public ZipUtil() {}
  
  public static void unZip(ProgressObserver observer, String sourceZipFile, String outputFolder) throws Exception
  {
    outputFolder = outputFolder.replace("/classes", "");
    
    cleanUpData(outputFolder);
    java.util.zip.ZipFile zipFile = new java.util.zip.ZipFile(sourceZipFile);
    long totalBytesRead = 0L;
    int percentCompleted = 0;
    long fileSize = 0L;
    try
    {
      java.util.Enumeration<? extends ZipEntry> entryList = zipFile.entries();
      while (entryList.hasMoreElements()) {
        ZipEntry entry = (ZipEntry)entryList.nextElement();
        fileSize += entry.getSize();
      }
      System.out.println("Starting unzip.....");
      java.util.Enumeration<? extends ZipEntry> entries = zipFile.entries();
      while (entries.hasMoreElements()) {
        ZipEntry entry = (ZipEntry)entries.nextElement();
        int bytesRead = (int)entry.getSize();
        totalBytesRead += bytesRead;
        percentCompleted = (int)(totalBytesRead * 100L / fileSize);
        String name = entry.getName();
        File entryDestination = new File(outputFolder, name);
        if (entry.isDirectory()) {
          if ((!name.contains("i18n")) && (!name.contains("database")) && (!name.contains("config")))
          {
            entryDestination.mkdirs();
            observer.progress(percentCompleted, name);
          }
        }
        else if ((!name.contains("app.config")) && (!name.contains("database")) && (!name.contains("config")))
        {
          observer.progress(percentCompleted, name);
          entryDestination.getParentFile().mkdirs();
          InputStream in = zipFile.getInputStream(entry);
          if ((name.contains("messages")) && (name.endsWith(".properties"))) {
            try {
              java.io.FileInputStream inStream = new java.io.FileInputStream(entryDestination);
              Properties properties = new Properties();
              properties.load(inStream);
              Properties properties2 = new Properties();
              properties2.load(in);
              properties2.putAll(properties);
              FileOutputStream out = new FileOutputStream(entryDestination);
              properties2.save(out, "");
              try {
                in.close();
              }
              catch (Exception localException) {}
            }
            catch (java.io.FileNotFoundException localFileNotFoundException) {}
          }
          else
          {
            FileOutputStream out = new FileOutputStream(entryDestination);
            copy(in, out);
            System.out.println("Copying file... " + entryDestination.getPath() + File.pathSeparator + name);
            observer.progress(percentCompleted);
            try {
              in.close();
            }
            catch (IOException localIOException) {}
          }
        }
      }
    }
    finally {
      zipFile.close();
    }
  }
  
  public static void cleanUpData(String projectLocation) throws Exception {
    System.out.println("Deleting data ............." + projectLocation);
    File directory = new File(projectLocation);
    try {
      delete(directory);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  



















  public static void delete(File file)
    throws IOException
  {
    System.out.println("Checking directory : " + file.getAbsolutePath());
    if (file.isDirectory()) {
      if (file.list().length == 0) {
        file.delete();
        System.out.println("Directory is deleted : " + file.getAbsolutePath());
      }
      else {
        String[] files = file.list();
        for (String temp : files)
          if ((!temp.contains("i18n")) && (!temp.contains("database")) && (!temp.contains("config")) && (!temp.contains("updater.jar")))
          {
            File fileDelete = new File(file, temp);
            delete(fileDelete);
          }
        if (file.list().length == 0) {
          file.delete();
          System.out.println("Directory is deleted : " + file.getAbsolutePath());
        }
      }
    }
    else {
      try {
        if (file.delete()) {
          System.out.println("File is deleted : " + file.getAbsolutePath());

        }
        else if (file.renameTo(new File(System.getProperty("java.io.tmpdir") + "/" + file.getName()))) {
          System.out.println("File is moved successful!");
        }
        else {
          System.out.println("File is failed to move--!");
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
  
  public static int copy(InputStream input, java.io.OutputStream output) throws IOException {
    long count = copyLarge(input, output);
    if (count > 2147483647L) {
      return -1;
    }
    return (int)count;
  }
  
  public static long copyLarge(InputStream input, java.io.OutputStream output) throws IOException {
    return copyLarge(input, output, new byte['က']);
  }
  

  public static long copyLarge(InputStream input, java.io.OutputStream output, byte[] buffer)
    throws IOException
  {
    long count = 0L;
    int n = 0;
    while (-1 != (n = input.read(buffer))) {
      output.write(buffer, 0, n);
      count += n;
    }
    return count;
  }
  
  public static void unZipIt(String zipFile, String outputFolder)
  {
    byte[] buffer = new byte['Ѐ'];
    

    try
    {
      File folder = new File(outputFolder);
      if (!folder.exists()) {
        folder.mkdir();
      }
      

      java.util.zip.ZipInputStream zis = new java.util.zip.ZipInputStream(new java.io.FileInputStream(zipFile));
      
      ZipEntry ze = zis.getNextEntry();
      
      while (ze != null)
      {
        String fileName = ze.getName();
        File newFile = new File(outputFolder + File.separator + fileName);
        
        System.out.println("file unzip : " + newFile.getAbsoluteFile());
        


        new File(newFile.getParent()).mkdirs();
        
        FileOutputStream fos = new FileOutputStream(newFile);
        
        int len;
        while ((len = zis.read(buffer)) > 0) {
          fos.write(buffer, 0, len);
        }
        
        fos.close();
        ze = zis.getNextEntry();
      }
      
      zis.closeEntry();
      zis.close();
      
      System.out.println("Done");
    }
    catch (IOException ex) {
      ex.printStackTrace();
    }
  }
}
