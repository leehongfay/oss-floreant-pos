package com.floreantpos.update;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;

public class Updater
{
  private static final String EXCLUDE_FILE_LIST = "files.xml";
  
  public Updater() {}
  
  public void update(ProgressObserver observer, String instructionsxml, String tmp, Modes mode, String destination) throws Exception
  {
    UpdateXMLParser parser = new UpdateXMLParser();
    ArrayList<Information> updateList = parser.parse(tmp + File.separator + instructionsxml, mode);
    








    if ((updateList == null) || (updateList.isEmpty())) {
      throw new Exception("Not found.");
    }
    
    for (Iterator iterator = updateList.iterator(); iterator.hasNext();) {
      Information information = (Information)iterator.next();
      observer.progress(0);
      String findPathJar = destination;
      if ((findPathJar != null) && (findPathJar.contains(".jar"))) {
        try {
          findPathJar = findPathJar.substring(0, findPathJar.lastIndexOf("\\"));
        } catch (Exception e) {
          findPathJar = findPathJar.substring(0, findPathJar.lastIndexOf("/"));
        }
      }
      information.setDestination(findPathJar);
      if (information.isRemoveOldPlugins()) {
        File pluginsDir = new File(findPathJar, "plugins");
        if ((pluginsDir.exists()) && (pluginsDir.isDirectory())) {
          File[] files = pluginsDir.listFiles();
          for (File file : files) {
            file.delete();
          }
        }
      }
      
      switch (1.$SwitchMap$com$floreantpos$update$Action[information.getAction().ordinal()]) {
      case 1: 
        if (information.isPlugin().booleanValue()) {
          try {
            copy(tmp + File.separator + information.getFilename(), information.getDestination() + File.separator + "plugins");

          }
          catch (FileNotFoundException e) {}
        }
        else
          ZipUtil.unZip(observer, tmp + File.separator + information.getFilename(), information.getDestination());
        break;
      case 2: 
        delete(information.getDestination());
        break;
      case 3: 
        Runtime.getRuntime().exec("java -jar " + tmp + File.separator + information.getFilename());
      }
    }
  }
  
  private void copy(String source, String destination)
    throws FileNotFoundException, java.io.IOException
  {
    destination = destination.replace("/classes", "");
    File srcfile = new File(source);
    File dstfile = new File(destination);
    if (dstfile.isDirectory()) {
      dstfile = new File(destination + File.separator + srcfile.getName());
    }
    
    InputStream in = new java.io.FileInputStream(srcfile);
    OutputStream out = new java.io.FileOutputStream(dstfile);
    
    byte[] buffer = new byte['Ȁ'];
    
    int length;
    while ((length = in.read(buffer)) > 0) {
      out.write(buffer, 0, length);
    }
    
    in.close();
    out.close();
  }
  
  private void delete(String filename) {
    File file = new File(filename);
    file.delete();
  }
  
  public static String findPathJar(Class<?> context) throws IllegalStateException {
    if (context == null)
      context = Updater.class;
    String rawName = context.getName();
    

    int idx = rawName.lastIndexOf('.');
    String classFileName = (idx == -1 ? rawName : rawName.substring(idx + 1)) + ".class";
    

    String uri = context.getResource(classFileName).toString();
    if (uri.startsWith("file:"))
    {
      return context.getProtectionDomain().getCodeSource().getLocation().getPath(); }
    if (!uri.startsWith("jar:file:")) {
      int idx = uri.indexOf(':');
      String protocol = idx == -1 ? "(unknown)" : uri.substring(0, idx);
      throw new IllegalStateException("This class has been loaded remotely via the " + protocol + " protocol. Only loading from a jar on the local file system is supported.");
    }
    

    int idx = uri.indexOf('!');
    
    if (idx == -1) {
      throw new IllegalStateException("You appear to have loaded this class from a local jar file, but I can't make sense of the URL!");
    }
    try {
      String fileName = java.net.URLDecoder.decode(uri.substring("jar:file:".length(), idx), Charset.defaultCharset().name());
      return new File(fileName).getAbsolutePath();
    } catch (UnsupportedEncodingException e) {
      throw new InternalError("default charset doesn't exist. Your VM is borked.");
    }
  }
}
