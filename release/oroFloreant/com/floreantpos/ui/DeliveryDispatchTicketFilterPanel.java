package com.floreantpos.ui;

import com.floreantpos.POSConstants;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosUIManager;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXCollapsiblePane;


















public class DeliveryDispatchTicketFilterPanel
  extends JXCollapsiblePane
{
  private List<String> filters;
  private DeliveryDispatchTicketFilterListener listener;
  private JToggleButton toggleButton;
  
  public DeliveryDispatchTicketFilterPanel(List<String> filters)
  {
    this.filters = filters;
    getContentPane().setLayout(new MigLayout("inset 0 0 5 0,fillx", "[][][grow]", ""));
    setCollapsed(true);
    createFilterPanel();
  }
  
  public void addFilterListener(DeliveryDispatchTicketFilterListener listener) {
    this.listener = listener;
  }
  
  private void createFilterPanel() {
    JPanel panel = new JPanel(new MigLayout("inset 0,fillx", "", ""));
    
    JPanel datePanel = new JPanel(new MigLayout("fill"));
    datePanel.setBorder(BorderFactory.createTitledBorder(""));
    
    JPanel onlineOrderPanel = new JPanel(new MigLayout("fill"));
    onlineOrderPanel.setBorder(BorderFactory.createTitledBorder(""));
    
    JPanel orderPanel = new JPanel(new MigLayout("fill", "sg,fill", ""));
    orderPanel.setBorder(BorderFactory.createTitledBorder(""));
    
    ButtonGroup dateGroup = new ButtonGroup();
    
    for (String filter : filters) {
      FilterButton btnFilter = new FilterButton(filter);
      if (filter.equals(POSConstants.ALL)) {
        btnFilter.setSelected(true);
        setToggleButton(btnFilter);
        dateGroup.add(btnFilter);
        datePanel.add(btnFilter, "grow");
      }
      else if ((filter.equals(POSConstants.TODAY)) || (filter.equals(POSConstants.TOMORROW))) {
        dateGroup.add(btnFilter);
        datePanel.add(btnFilter, "grow");
      }
      else if (filter.equals("ONLINE")) {
        onlineOrderPanel.add(btnFilter, "grow");
      }
      else if (filter.equals("PICK UP")) {
        orderPanel.add(btnFilter, "grow");
      }
      else if (filter.equals("DELIVERY")) {
        orderPanel.add(btnFilter, "grow");
      }
    }
    
    panel.add(datePanel);
    panel.add(onlineOrderPanel, "w " + PosUIManager.getSize(150));
    panel.add(orderPanel);
    
    getContentPane().add(panel, "growx"); }
  
  public static abstract interface DeliveryDispatchTicketFilterListener { public abstract void filterSelected(JToggleButton paramJToggleButton);
    
    public abstract void filterUnSelected(JToggleButton paramJToggleButton); }
  
  private class FilterButton extends POSToggleButton implements ActionListener { public FilterButton(String name) { setText(name);
      addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e)
    {
      JToggleButton tBtn = (JToggleButton)e.getSource();
      if (tBtn.isSelected()) {
        setSelected(true);
        listener.filterSelected(tBtn);
      }
      else {
        setSelected(false);
        listener.filterUnSelected(tBtn);
      }
    }
  }
  
  public JToggleButton getToggleButton() {
    return toggleButton;
  }
  
  public void setToggleButton(JToggleButton toggleButton) {
    this.toggleButton = toggleButton;
  }
}
