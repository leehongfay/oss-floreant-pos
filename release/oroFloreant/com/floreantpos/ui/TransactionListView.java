package com.floreantpos.ui;

import com.floreantpos.Messages;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.CurrencyUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.ListSelectionModel;
import org.jdesktop.swingx.JXTable;



















public class TransactionListView
  extends JPanel
{
  private JXTable table;
  private TransactionListTableModel tableModel;
  
  public TransactionListView()
  {
    table = new TransactionListTable();
    table.setSortable(false);
    table.setModel(this.tableModel = new TransactionListTableModel());
    table.setRowHeight(PosUIManager.getSize(40));
    table.setAutoResizeMode(3);
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    table.setGridColor(Color.LIGHT_GRAY);
    






    PosScrollPane scrollPane = new PosScrollPane(table, 20, 31);
    JScrollBar scrollBar = scrollPane.getVerticalScrollBar();
    scrollBar.setPreferredSize(PosUIManager.getSize(30, 60));
    
    setLayout(new BorderLayout());
    
    add(scrollPane);
  }
  
  public void setTransactions(List<PosTransaction> transactions) {
    tableModel.setRows(transactions);
  }
  
  public void addTransaction(PosTransaction transaction) {
    tableModel.addItem(transaction);
  }
  
  public PosTransaction getSelectedTransaction() {
    int selectedRow = table.getSelectedRow();
    if (selectedRow < 0) {
      return null;
    }
    
    return (PosTransaction)tableModel.getRowData(selectedRow);
  }
  
  public List<PosTransaction> getAllTransactions() {
    return tableModel.getRows();
  }
  
  public List<PosTransaction> getSelectedTransactions() {
    int[] selectedRows = table.getSelectedRows();
    
    ArrayList<PosTransaction> transactions = new ArrayList(selectedRows.length);
    
    for (int i = 0; i < selectedRows.length; i++) {
      PosTransaction transaction = (PosTransaction)tableModel.getRowData(selectedRows[i]);
      transactions.add(transaction);
    }
    
    return transactions;
  }
  
  private class TransactionListTable extends JXTable
  {
    public TransactionListTable() {
      setColumnControlVisible(false);
    }
    
    public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend)
    {
      ListSelectionModel selectionModel = getSelectionModel();
      boolean selected = selectionModel.isSelectedIndex(rowIndex);
      if (selected) {
        selectionModel.removeSelectionInterval(rowIndex, rowIndex);
      }
      else {
        selectionModel.addSelectionInterval(rowIndex, rowIndex);
      }
    }
  }
  
  private class TransactionListTableModel extends ListTableModel<PosTransaction> {
    public TransactionListTableModel() {
      super();
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      PosTransaction transaction = (PosTransaction)rows.get(rowIndex);
      
      Ticket ticket = transaction.getTicket();
      switch (columnIndex) {
      case 0: 
        return transaction.getId();
      
      case 1: 
        return ticket.getId();
      
      case 2: 
        return transaction.getCardMerchantGateway();
      
      case 3: 
        User owner = ticket.getOwner();
        if (owner == null) {
          return null;
        }
        return owner.getFirstName();
      
      case 4: 
        return transaction.getCardType();
      
      case 5: 
        return transaction.getCardNumber();
      
      case 6: 
        return transaction.getCardHolderName();
      
      case 7: 
        return transaction.getTipsAmount();
      
      case 8: 
        return Double.valueOf(transaction.getAmount().doubleValue() - transaction.getTipsAmount().doubleValue());
      
      case 9: 
        return transaction.getAmount();
      }
      
      
      return null;
    }
  }
  
  public PosTransaction getFirstSelectedTransaction()
  {
    List<PosTransaction> selectedTickets = getSelectedTransactions();
    
    if ((selectedTickets.size() == 0) || (selectedTickets.size() > 1)) {
      POSMessageDialog.showMessage(Messages.getString("TransactionListView.7"));
      return null;
    }
    
    PosTransaction t = (PosTransaction)selectedTickets.get(0);
    
    return t;
  }
  
  public JXTable getTable() {
    return table;
  }
}
