package com.floreantpos.ui;

import com.floreantpos.IconFactory;
import com.floreantpos.model.Ticket;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.util.NumberUtil;
import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;






















public class PosTableRenderer
  extends DefaultTableCellRenderer
{
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, h:m a");
  


  private JCheckBox checkBox = new JCheckBox();
  private JLabel lblColor = new JLabel();
  
  public PosTableRenderer() {
    checkBox.setHorizontalAlignment(0);
    lblColor.setOpaque(true);
  }
  
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, false, row, column);
    label.setIcon(null);
    label.setHorizontalAlignment(10);
    if ((value instanceof Boolean)) {
      checkBox.setSelected(((Boolean)value).booleanValue());
      if (isSelected) {
        checkBox.setBackground(table.getSelectionBackground());
      }
      else {
        checkBox.setBackground(table.getBackground());
      }
      return checkBox;
    }
    if ((value instanceof Color)) {
      Color color = (Color)value;
      String rgb = Integer.toHexString(color.getRGB()).toUpperCase();
      rgb = rgb.substring(2, rgb.length());
      
      lblColor.setText(rgb);
      lblColor.setBackground(color);
      
      return lblColor;
    }
    if ((value instanceof ImageIcon))
    {
      ImageIcon image = (ImageIcon)value;
      image = new ImageIcon(image.getImage().getScaledInstance(48, 48, 4));
      if (image != null) {
        table.setRowHeight(PosUIManager.getSize(60));
      }
      JLabel l = new JLabel(image);
      Border unselectedBorder = BorderFactory.createMatteBorder(5, 5, 5, 5, table.getBackground());
      l.setBorder(unselectedBorder);
      return l;
    }
    if (column == 0) {
      Ticket ticket = null;
      TableModel tableModel = table.getModel();
      if ((tableModel instanceof ListTableModel)) {
        ListTableModel listTableModel = (ListTableModel)tableModel;
        Object rowData = listTableModel.getRowData(row);
        if ((rowData instanceof Ticket)) {
          ticket = (Ticket)rowData;
        }
      }
      
      if (ticket == null) {
        return label;
      }
      if (ticket.isSourceOnline()) {
        label.setIcon(IconFactory.getIcon("/ui_icons/", "cloud.png"));
      }
    }
    
    if ((value instanceof Double)) {
      label.setHorizontalAlignment(4);
    }
    
    return label;
  }
  
  protected void setValue(Object value)
  {
    if (value == null) {
      setText("");
      return;
    }
    
    String text = value.toString();
    
    if (((value instanceof Double)) || ((value instanceof Float))) {
      text = NumberUtil.formatNumber(Double.valueOf(((Number)value).doubleValue()), true);
      setHorizontalAlignment(4);
    }
    else if ((value instanceof Integer)) {
      setHorizontalAlignment(4);
    }
    else if ((value instanceof Date)) {
      text = dateFormat.format(value);
      setHorizontalAlignment(2);
    }
    else {
      setHorizontalAlignment(2);
    }
    
    setText(" " + text + " ");
  }
}
