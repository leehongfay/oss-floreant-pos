package com.floreantpos.ui.inv;

import com.floreantpos.model.IUnit;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.InventoryStock;
import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.InventoryTransactionType;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.PurchaseOrder;
import com.floreantpos.model.dao.InventoryLocationDAO;
import com.floreantpos.model.dao.InventoryStockDAO;
import com.floreantpos.model.dao.InventoryTransactionDAO;
import com.floreantpos.model.dao.InventoryVendorDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.CopyUtil;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.PosGuiUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class InventoryTransactionEntryForm
  extends BeanEditor<InventoryTransaction>
{
  private JButton btnParentLoc;
  private POSTextField tfParentLocation;
  private JLabel lblParentLoc;
  private POSTextField tfOrderId;
  private JLabel lblMenuItem;
  private JLabel lblVendor;
  private InventoryTransaction inventoryTransaction;
  private JComboBox<InventoryVendor> cbVendor;
  private JLabel lblInventoryTransactionType;
  private JComboBox cbTransactionType;
  private JLabel lblPurchaseUnitPrice;
  private DoubleTextField tfPurchaseUnitPrice;
  private JLabel lblPurchaseTotalPrice;
  private JLabel lblPurchaseQuantity;
  private DoubleTextField tfPurchaseTotalPrice;
  private POSTextField tfMenuItem;
  private DoubleTextField tfPurchaseQuantity;
  private POSTextField tfSku;
  private InventoryLocation defaultInLocation;
  private InventoryLocation defaultOutLocation;
  private JComboBox<IUnit> cbUnits;
  private POSTextField tfToLocation;
  private JButton btnToLoc;
  private JLabel lblToLocation;
  private JButton btnClearToLoc;
  private JComboBox cbInventoryReasons;
  private JLabel lblInventoryReasonType;
  
  public InventoryTransactionEntryForm(InventoryTransaction inventoryTransaction)
  {
    this.inventoryTransaction = inventoryTransaction;
    setLayout(new BorderLayout());
    createUI();
    initData();
    setBean(inventoryTransaction);
  }
  



  private void initData()
  {
    InventoryVendorDAO inventoryVendorDao = new InventoryVendorDAO();
    List<InventoryVendor> inventoryVendors = inventoryVendorDao.findAll();
    cbVendor.setModel(new ComboBoxModel(inventoryVendors));
    
    List<String> inventoryReasons = new ArrayList();
    for (int i = 0; i < InventoryTransaction.REASON_OUT.length; i++) {
      inventoryReasons.add(InventoryTransaction.REASON_OUT[i]);
    }
    cbInventoryReasons.setModel(new ComboBoxModel(inventoryReasons));
    
    cbTransactionType.setEnabled(false);
    cbUnits.setModel(new ComboBoxModel());
    loadUnits();
    
    if (inventoryTransaction.getTransactionType().equals(InventoryTransactionType.UNCHANGED)) {
      cbTransactionType.addItem(InventoryTransactionType.UNCHANGED);
      lblPurchaseUnitPrice.setVisible(false);
      lblPurchaseTotalPrice.setVisible(false);
      tfPurchaseUnitPrice.setVisible(false);
      tfPurchaseTotalPrice.setVisible(false);
      lblInventoryReasonType.setVisible(false);
      cbInventoryReasons.setVisible(false);
      lblParentLoc.setText("From Location");
      List<InventoryLocation> inventoryLocationList = InventoryLocationDAO.getInstance().findAll();
      for (InventoryLocation inventoryLocation : inventoryLocationList)
      {
        if (inventoryLocation.isDefaultInLocation().booleanValue()) {
          defaultOutLocation = inventoryLocation;
        }
        if (inventoryLocation.isDefaultOutLocation().booleanValue()) {
          defaultInLocation = inventoryLocation;
        }
      }
      
      if (defaultInLocation != null) {
        tfToLocation.setText(defaultInLocation.getName());
      }
      if (defaultOutLocation != null) {
        tfParentLocation.setText(defaultOutLocation.getName());
      }
    }
    else if (inventoryTransaction.getTransactionType().equals(InventoryTransactionType.OUT)) {
      cbTransactionType.addItem(InventoryTransactionType.OUT);
      
      List<InventoryLocation> inventoryLocationList = InventoryLocationDAO.getInstance().findAll();
      defaultOutLocation = null;
      if (inventoryLocationList == null) {
        return;
      }
      for (InventoryLocation inventoryLocation : inventoryLocationList) {
        if (inventoryLocation.isDefaultOutLocation().booleanValue()) {
          defaultOutLocation = inventoryLocation;
        }
      }
      if (defaultOutLocation != null) {
        tfParentLocation.setText(defaultOutLocation.getName());
      }
      
      lblToLocation.setVisible(false);
      tfToLocation.setVisible(false);
      btnToLoc.setVisible(false);
      btnClearToLoc.setVisible(false);
    }
    
    btnParentLoc.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        InventoryLocation selectedLocation = InventoryLocationSelector.openLocationSelector();
        if (selectedLocation == null) {
          return;
        }
        defaultOutLocation = selectedLocation;
        tfParentLocation.setText(selectedLocation.getName());
        tfParentLocation.setEditable(false);
      }
      
    });
    btnToLoc.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        InventoryLocation selectedLocation = InventoryLocationSelector.openLocationSelector();
        if (selectedLocation == null) {
          return;
        }
        defaultInLocation = selectedLocation;
        tfToLocation.setText(selectedLocation.getName());
        tfToLocation.setEditable(false);
      }
    });
  }
  
  public InventoryTransactionEntryForm() {
    setLayout(new BorderLayout());
    createUI();
  }
  
  private void loadUnits() {
    MenuItem menuItem = inventoryTransaction.getMenuItem();
    if (menuItem == null)
      return;
    ComboBoxModel model = (ComboBoxModel)cbUnits.getModel();
    model.removeAllElements();
    MenuItemDAO.getInstance().initializeUnits(menuItem);
    model.setDataList(menuItem.getUnits());
  }
  
  private void createUI() {
    JPanel itemInfoPanel = new JPanel();
    



    add(itemInfoPanel, "Center");
    












































    itemInfoPanel.setLayout(new MigLayout("hidemode 3", "[][grow][]", ""));
    
    JLabel lblOrderID = new JLabel("P.O. No");
    itemInfoPanel.add(lblOrderID, "cell 0 0,alignx trailing");
    
    tfOrderId = new POSTextField();
    itemInfoPanel.add(tfOrderId, "cell 1 0,growx");
    
    lblMenuItem = new JLabel("Inventory Item");
    itemInfoPanel.add(lblMenuItem, "cell 0 1,alignx trailing ");
    
    tfMenuItem = new POSTextField();
    itemInfoPanel.add(tfMenuItem, "cell 1 1,growx");
    tfMenuItem.setEditable(false);
    














    JLabel lblSku = new JLabel("SKU");
    itemInfoPanel.add(lblSku, "cell 0 2,alignx trailing");
    tfSku = new POSTextField();
    itemInfoPanel.add(tfSku, "cell 1 2,growx");
    tfSku.setEditable(false);
    
    lblVendor = new JLabel("Vendor");
    itemInfoPanel.add(lblVendor, "cell 0 3, alignx trailing");
    
    cbVendor = new JComboBox();
    itemInfoPanel.add(cbVendor, "cell 1 3, growx");
    
    lblInventoryReasonType = new JLabel("Reason Type");
    itemInfoPanel.add(lblInventoryReasonType, "cell 0 4, alignx trailing");
    
    cbInventoryReasons = new JComboBox();
    cbInventoryReasons.setMinimumSize(PosUIManager.getSize(100, 0));
    itemInfoPanel.add(cbInventoryReasons, "cell 1 4, growx");
    
    lblInventoryTransactionType = new JLabel("Transaction Type");
    itemInfoPanel.add(lblInventoryTransactionType, "cell 0 5, alignx trailing");
    
    cbTransactionType = new JComboBox();
    itemInfoPanel.add(cbTransactionType, "cell 1 5, growx");
    
    lblParentLoc = new JLabel("Location");
    itemInfoPanel.add(lblParentLoc, "cell 0 6, alignx trailing");
    
    tfParentLocation = new POSTextField();
    itemInfoPanel.add(tfParentLocation, "cell 1 6,split 3, growx");
    tfParentLocation.setEditable(false);
    
    btnParentLoc = new JButton("Select");
    itemInfoPanel.add(btnParentLoc);
    
    JButton btnClearLoc = new JButton("Clear");
    itemInfoPanel.add(btnClearLoc);
    
    btnClearLoc.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tfParentLocation.setText("");
        defaultOutLocation = null;
      }
      
    });
    lblToLocation = new JLabel("To Location");
    itemInfoPanel.add(lblToLocation, "cell 0 7, alignx trailing");
    
    tfToLocation = new POSTextField();
    itemInfoPanel.add(tfToLocation, "cell 1 7,split 3, growx");
    tfToLocation.setEditable(false);
    
    btnToLoc = new JButton("Select");
    itemInfoPanel.add(btnToLoc);
    
    btnClearToLoc = new JButton("Clear");
    itemInfoPanel.add(btnClearToLoc);
    
    btnClearToLoc.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tfToLocation.setText("");
        defaultInLocation = null;
      }
    });
    JLabel lblUnit = new JLabel("Unit");
    itemInfoPanel.add(lblUnit, "cell 0 8,alignx trailing");
    
    cbUnits = new JComboBox();
    cbUnits.setMinimumSize(PosUIManager.getSize(100, 0));
    itemInfoPanel.add(cbUnits, "cell 1 8");
    
    lblPurchaseUnitPrice = new JLabel("Unit Cost (" + CurrencyUtil.getCurrencySymbol() + ")");
    itemInfoPanel.add(lblPurchaseUnitPrice, "cell 0 9,alignx trailing");
    
    tfPurchaseUnitPrice = new DoubleTextField();
    itemInfoPanel.add(tfPurchaseUnitPrice, "cell 1 9,growx");
    
    lblPurchaseQuantity = new JLabel("Quantity");
    itemInfoPanel.add(lblPurchaseQuantity, "cell 0 10,alignx trailing");
    
    tfPurchaseQuantity = new DoubleTextField();
    itemInfoPanel.add(tfPurchaseQuantity, "cell 1 10,growx");
    
    lblPurchaseTotalPrice = new JLabel("Total Price");
    tfPurchaseTotalPrice = new DoubleTextField();
  }
  
  public boolean save() {
    try {
      if (!updateModel()) {
        return false;
      }
      
      InventoryTransaction modelTransaction = (InventoryTransaction)getBean();
      String orderId = tfOrderId.getText();
      
      PurchaseOrder purchaseOrder = new PurchaseOrder();
      purchaseOrder.setOrderId(orderId);
      
      modelTransaction.setReferenceNo(purchaseOrder.getId());
      Session session = null;
      Transaction tx = null;
      
      try
      {
        InventoryTransactionDAO inventoryTransactionDAO = InventoryTransactionDAO.getInstance();
        session = inventoryTransactionDAO.createNewSession();
        tx = session.beginTransaction();
        
        session.saveOrUpdate(purchaseOrder);
        if (modelTransaction.getTransactionType() == InventoryTransactionType.UNCHANGED) {
          InventoryTransaction outTrans = (InventoryTransaction)CopyUtil.deepCopy(modelTransaction);
          InventoryTransaction inTrans = modelTransaction;
          
          outTrans.setTransactionType(InventoryTransactionType.OUT);
          outTrans.setToInventoryLocation(null);
          inventoryTransactionDAO.adjustInventoryStock(outTrans, session);
          
          inTrans.setTransactionType(InventoryTransactionType.IN);
          inTrans.setFromInventoryLocation(null);
          inventoryTransactionDAO.adjustInventoryStock(inTrans, session);
        }
        else {
          inventoryTransactionDAO.adjustInventoryStock(modelTransaction, session);
        }
        tx.commit();
      }
      catch (Exception e) {
        e.printStackTrace();
        if (tx != null) {
          tx.rollback();
        }
      }
      finally {
        if (session != null) {
          session.close();
        }
      }
      
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(e.getMessage());
    }
    
    return false;
  }
  
  public void updateView() {
    InventoryTransaction model = (InventoryTransaction)getBean();
    
    if (model == null) {
      return;
    }
    
    if (model.getReferenceNo() != null) {
      tfOrderId.setText(model.getReferenceNo());
    }
    tfMenuItem.setText(model.getMenuItem().getName());
    tfSku.setText(model.getMenuItem().getSku());
    PosGuiUtil.selectComboItemById(cbVendor, model.getVendorId());
    cbTransactionType.setSelectedItem(model.getType());
    
    InventoryLocation fromInventoryLocation = DataProvider.get().getInventoryLocationById(model.getFromLocationId());
    InventoryLocation toInventoryLocation = DataProvider.get().getInventoryLocationById(model.getToLocationId());
    if (fromInventoryLocation != null) {
      tfParentLocation.setText(fromInventoryLocation.getName());
    }
    if (toInventoryLocation != null) {
      tfToLocation.setText(toInventoryLocation.getName());
    }
    
    tfPurchaseUnitPrice.setText(NumberUtil.formatNumber(model.getUnitPrice()));
    tfPurchaseQuantity.setText(String.valueOf(model.getQuantity()));
  }
  

  public boolean updateModel()
  {
    InventoryTransaction invTran = (InventoryTransaction)getBean();
    
    MenuItem inventoryItem = invTran.getMenuItem();
    
    if (inventoryItem == null) {
      POSMessageDialog.showMessage(getParent(), "Please select inventory item");
      return false;
    }
    
    invTran.setMenuItem(inventoryItem);
    
    IUnit unit = (IUnit)cbUnits.getSelectedItem();
    if (unit == null) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select item unit.");
      return false;
    }
    
    invTran.setUnit(unit.getUniqueCode());
    
    if (invTran.getTransactionType() == InventoryTransactionType.UNCHANGED) {
      if (defaultInLocation == null) {
        POSMessageDialog.showMessage(getParent(), "Please select location.");
        return false;
      }
      invTran.setToLocationId(defaultInLocation.getId());
      
      if (defaultOutLocation == null) {
        POSMessageDialog.showMessage(getParent(), "Please select location.");
        return false;
      }
      invTran.setFromLocationId(defaultOutLocation.getId());
      
      if (defaultInLocation.equals(defaultOutLocation)) {
        POSMessageDialog.showMessage(getParent(), "Please choose a different location.");
        return false;
      }
    }
    if (invTran.getTransactionType() == InventoryTransactionType.OUT) {
      String inventoryReasons = (String)cbInventoryReasons.getSelectedItem();
      invTran.setReason(inventoryReasons);
      if (defaultOutLocation == null) {
        POSMessageDialog.showMessage(getParent(), "Please select location");
        return false;
      }
      invTran.setFromLocationId(defaultOutLocation.getId());
    }
    
    InventoryStock inventoryStockForOutTransaction = InventoryStockDAO.getInstance().getInventoryStock(invTran.getMenuItem(), defaultOutLocation.getId(), invTran
      .getUnit());
    
    double quantity = tfPurchaseQuantity.getDouble();
    
    if (quantity == 0.0D) {
      POSMessageDialog.showMessage(getParent(), "Quantity must be greater than zero");
      return false;
    }
    
    if ((inventoryStockForOutTransaction == null) || (inventoryStockForOutTransaction.getQuantityInHand().doubleValue() <= 0.0D) || 
      (quantity > inventoryStockForOutTransaction.getQuantityInHand().doubleValue())) {
      POSMessageDialog.showMessage(getParent(), "Selected location does not contain enough items for out transaction!");
      return false;
    }
    Object selectedItem = cbVendor.getSelectedItem();
    if ((selectedItem instanceof InventoryVendor)) {
      invTran.setVendorId(((InventoryVendor)selectedItem).getId());
    }
    invTran.setTransactionDate(new Date());
    


    invTran.setQuantity(Double.valueOf(quantity));
    double cost = tfPurchaseUnitPrice.getDouble();
    invTran.setUnitCost(Double.valueOf(cost));
    invTran.setUnit(invTran.getMenuItem().getUnit().getUniqueCode());
    invTran.setTotal(Double.valueOf(quantity * cost));
    






    return true;
  }
  
  public String getDisplayText()
  {
    if (inventoryTransaction.getTransactionType().equals(InventoryTransactionType.UNCHANGED)) {
      return "Inventory Transfer";
    }
    if (inventoryTransaction.getTransactionType().equals(InventoryTransactionType.OUT)) {
      return "Inventory Out";
    }
    return "";
  }
}
