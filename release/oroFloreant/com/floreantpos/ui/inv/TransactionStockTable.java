package com.floreantpos.ui.inv;

import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.InventoryStock;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.dao.InventoryStockDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.TransparentPanel;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import org.jdesktop.swingx.JXTable;



public class TransactionStockTable
  extends TransparentPanel
{
  private static final long serialVersionUID = 1L;
  private JXTable table;
  private BeanTableModel<InventoryStock> tableModel;
  private MenuItem menuItem;
  
  public TransactionStockTable(MenuItem menuItem)
  {
    this.menuItem = menuItem;
    
    tableModel = new BeanTableModel(InventoryStock.class);
    
    tableModel.addColumn("LOCATION", "inventoryLocation");
    tableModel.addColumn("QUANTITY", "quantityInHand");
    tableModel.addColumn("UNIT", "unit");
    
    tableModel.addRows(InventoryStockDAO.getInstance().getInventoryStocks(menuItem));
    
    table = new JXTable(tableModel);
    table.setRowHeight(40);
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
  }
  
  public InventoryStock getSelectedItem()
  {
    int index = table.getSelectedRow();
    if (index < 0) {
      return null;
    }
    index = table.convertRowIndexToModel(index);
    return (InventoryStock)tableModel.getRow(index);
  }
}
