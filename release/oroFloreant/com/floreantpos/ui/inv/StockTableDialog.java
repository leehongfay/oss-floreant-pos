package com.floreantpos.ui.inv;

import com.floreantpos.model.InventoryStock;
import com.floreantpos.model.MenuItem;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import javax.swing.border.EmptyBorder;

public class StockTableDialog extends OkCancelOptionDialog
{
  private TransactionStockTable stockTable;
  private InventoryStock stockItem;
  private MenuItem menuItem;
  
  public StockTableDialog(MenuItem menuItem)
  {
    this.menuItem = menuItem;
    initComponents();
  }
  
  private void initComponents() {
    setDefaultCloseOperation(2);
    setCaption("Select Location");
    setTitle("Location");
    
    stockTable = new TransactionStockTable(menuItem);
    stockTable.setBorder(new EmptyBorder(10, 20, 10, 20));
    add(stockTable);
    pack();
  }
  

  public void doOk()
  {
    stockItem = stockTable.getSelectedItem();
    if (stockItem == null) {
      return;
    }
    setCanceled(false);
    dispose();
  }
  
  public String getInvUnit() {
    return stockItem.getUnit();
  }
  
  public Double getAvailableStockQuantity() {
    return stockItem.getQuantityInHand();
  }
}
