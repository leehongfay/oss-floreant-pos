package com.floreantpos.ui.inv;

import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.dao.InventoryLocationDAO;
import com.floreantpos.ui.dialog.TreeDisplayDialog;
import java.util.List;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

public class InventoryLocationSelector
{
  public InventoryLocationSelector() {}
  
  public static InventoryLocation openLocationSelector()
  {
    List<InventoryLocation> rootLocations = InventoryLocationDAO.getInstance().getRootLocations();
    DefaultMutableTreeNode root = new DefaultMutableTreeNode();
    for (InventoryLocation inventoryLocation : rootLocations) {
      DefaultMutableTreeNode node = new DefaultMutableTreeNode(inventoryLocation);
      root.add(node);
      buildTree(node);
    }
    
    JTree tree = new JTree(root);
    
    for (int i = 1; i < tree.getRowCount() * 2; i++) {
      tree.expandRow(i);
    }
    
    tree.setRootVisible(false);
    
    TreeDisplayDialog treeDisplayDialog = new TreeDisplayDialog(tree);
    treeDisplayDialog.setCaption("Select location");
    treeDisplayDialog.setSize(com.floreantpos.swing.PosUIManager.getSize(500, 500));
    treeDisplayDialog.open();
    if (treeDisplayDialog.isCanceled())
      return null;
    DefaultMutableTreeNode treeNode = treeDisplayDialog.getPath();
    return (InventoryLocation)treeNode.getUserObject();
  }
  
  private static void buildTree(DefaultMutableTreeNode node) {
    InventoryLocation location = (InventoryLocation)node.getUserObject();
    List<InventoryLocation> children = null;
    
    if (location != null) {
      children = location.getChildren();
    }
    

    if (children == null) {
      return;
    }
    
    for (InventoryLocation inventoryLocation : children) {
      DefaultMutableTreeNode newChild = new DefaultMutableTreeNode(inventoryLocation);
      node.add(newChild);
      buildTree(newChild);
    }
  }
}
