package com.floreantpos.ui.inv;

import com.floreantpos.PosLog;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.dao.InventoryLocationDAO;
import com.floreantpos.model.dao.InventoryTransactionDAO;
import com.floreantpos.model.dao.InventoryVendorDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.BeanTableModel.DataType;
import com.floreantpos.swing.BeanTableModel.EditMode;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.swing.PosTable;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.TreeDisplayDialog;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.tree.DefaultMutableTreeNode;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.swingx.JXTable;






public class InventoryStockInForm
  extends BeanEditor<InventoryTransaction>
{
  private JButton btnParentLoc;
  private POSTextField tfLocation;
  private JLabel lblLoc;
  private DefaultMutableTreeNode path;
  private JLabel lblVendor;
  private JComboBox<InventoryVendor> cbVendor;
  private POSTextField tfPONumber;
  private JLabel lblPONumber;
  private InventoryLocation location;
  private TitledBorder titleBorder;
  private JXTable table;
  private BeanTableModel<InventoryTransaction> tableModel;
  private DoubleTextField tfSubTotalAmount;
  private JButton btnAddItem;
  private JButton btnDeleteItem;
  private InventoryTransaction inventoryTransaction;
  private JButton btnSearch;
  private JComboBox<String> cbInventoryReasons;
  private List<InventoryTransaction> inventoryTransactions;
  
  public InventoryStockInForm()
  {
    this(null);
  }
  
  public InventoryStockInForm(InventoryTransaction inventoryTransaction) {
    this.inventoryTransaction = inventoryTransaction;
    
    initComponents();
    initData();
    setBean(inventoryTransaction);
  }
  
  private void initData() {
    if (inventoryTransaction.getMenuItem() != null) {
      tableModel.addRow(inventoryTransaction);
    }
    List<String> inventoryReasons = new ArrayList();
    for (int i = 0; i < InventoryTransaction.REASON_IN.length; i++) {
      inventoryReasons.add(InventoryTransaction.REASON_IN[i]);
    }
    cbInventoryReasons.setModel(new ComboBoxModel(inventoryReasons));
    InventoryVendorDAO inventoryVendorDao = new InventoryVendorDAO();
    List<InventoryVendor> inventoryVendors = inventoryVendorDao.findAll();
    cbVendor.setModel(new ComboBoxModel(inventoryVendors));
    
    tfLocation.setEditable(false);
    
    btnParentLoc.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        path = InventoryStockInForm.this.getLocationModelPath();
        if (path == null)
          return;
        location = ((InventoryLocation)path.getUserObject());
        tfLocation.setText(location.getName());
      }
      
    });
    List<InventoryLocation> inventoryLocationList = InventoryLocationDAO.getInstance().findAll();
    InventoryLocation defaultInLocation = null;
    if (inventoryLocationList != null) {
      for (InventoryLocation inventoryLocation : inventoryLocationList) {
        if (inventoryLocation.isDefaultInLocation().booleanValue()) {
          defaultInLocation = inventoryLocation;
        }
      }
      if (defaultInLocation != null) {
        location = defaultInLocation;
        tfLocation.setText(defaultInLocation.getName());
      }
    }
    updatePrice();
  }
  
  private DefaultMutableTreeNode getLocationModelPath() {
    List<InventoryLocation> rootLocations = InventoryLocationDAO.getInstance().getRootLocations();
    DefaultMutableTreeNode root = new DefaultMutableTreeNode();
    for (InventoryLocation inventoryLocation : rootLocations) {
      DefaultMutableTreeNode node = new DefaultMutableTreeNode(inventoryLocation);
      root.add(node);
      buildTree(node);
    }
    
    JTree tree = new JTree(root);
    
    for (int i = 1; i < tree.getRowCount() * 2; i++) {
      tree.expandRow(i);
    }
    
    tree.setRootVisible(false);
    
    TreeDisplayDialog treeDisplayDialog = new TreeDisplayDialog(tree);
    treeDisplayDialog.setPreferredSize(PosUIManager.getSize(500, 600));
    treeDisplayDialog.open();
    if (treeDisplayDialog.isCanceled())
      return null;
    return treeDisplayDialog.getPath();
  }
  
  private void updatePrice() {
    double subtotalAmount = 0.0D;
    List<InventoryTransaction> transactions = tableModel.getRows();
    if (transactions == null)
      return;
    for (InventoryTransaction item : transactions) {
      subtotalAmount += item.getQuantity().doubleValue() * item.getUnitCost().doubleValue();
    }
    tfSubTotalAmount.setText(NumberUtil.formatNumber(Double.valueOf(subtotalAmount)));
    table.repaint();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    setBorder(new EmptyBorder(10, 10, 10, 10));
    JPanel itemInfoPanel = new JPanel(new MigLayout("hidemode 3,wrap 2", "[][grow][]", ""));
    titleBorder = new TitledBorder("");
    itemInfoPanel.setBorder(titleBorder);
    add(itemInfoPanel, "North");
    
    lblPONumber = new JLabel("P.O. No");
    itemInfoPanel.add(lblPONumber, "alignx trailing");
    
    tfPONumber = new POSTextField();
    itemInfoPanel.add(tfPONumber, "growx, wrap");
    
    btnSearch = new JButton("..Search..");
    
    btnSearch.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        double quantity = 0.0D;
        double price = 0.0D;
        if (StringUtils.isNotEmpty(tfPONumber.getText())) {}








      }
      









    });
    lblVendor = new JLabel("Vendor");
    itemInfoPanel.add(lblVendor, "alignx trailing");
    
    cbVendor = new JComboBox();
    cbVendor.setMinimumSize(PosUIManager.getSize(100, 0));
    itemInfoPanel.add(cbVendor, "wrap");
    
    JLabel lblInventoryTransactionType = new JLabel("Reason Type");
    itemInfoPanel.add(lblInventoryTransactionType, "alignx trailing");
    
    cbInventoryReasons = new JComboBox();
    cbInventoryReasons.setMinimumSize(PosUIManager.getSize(100, 0));
    itemInfoPanel.add(cbInventoryReasons, "wrap");
    
    lblLoc = new JLabel("To Location");
    itemInfoPanel.add(lblLoc, "alignx trailing");
    
    tfLocation = new POSTextField();
    itemInfoPanel.add(tfLocation, "split 3,growx");
    tfLocation.setEditable(false);
    
    btnParentLoc = new JButton("Select");
    itemInfoPanel.add(btnParentLoc);
    
    JButton btnClearLoc = new JButton("Clear");
    itemInfoPanel.add(btnClearLoc);
    btnClearLoc.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tfLocation.setText("");
      }
      
    });
    tableModel = new BeanTableModel(InventoryTransaction.class);
    tableModel.addColumn("SKU", "sku");
    tableModel.addColumn("ITEM", "itemName");
    tableModel.addColumn("QTY", "quantity", BeanTableModel.EditMode.EDITABLE, 11, BeanTableModel.DataType.NUMBER);
    tableModel.addColumn("COST", "unitCost", BeanTableModel.EditMode.EDITABLE, 11, BeanTableModel.DataType.MONEY);
    tableModel.addColumn("UNIT", "unit");
    tableModel.addColumn("TOTAL", "total", 11, BeanTableModel.DataType.MONEY);
    
    table = new PosTable(tableModel)
    {
      public void setValueAt(Object value, int rowIndex, int columnIndex) {
        InventoryTransaction inventoryTransaction = (InventoryTransaction)tableModel.getRow(rowIndex);
        
        String receiveStr = value.toString();
        if (receiveStr.isEmpty())
          return;
        double quantity = Double.parseDouble(receiveStr);
        if (columnIndex == 2) {
          inventoryTransaction.setQuantity(Double.valueOf(quantity));
          inventoryTransaction.setTotal(Double.valueOf(inventoryTransaction.getQuantity().doubleValue() * inventoryTransaction.getUnitCost().doubleValue()));
          InventoryStockInForm.this.updatePrice();
        }
        if (columnIndex == 3) {
          inventoryTransaction.setUnitCost(Double.valueOf(quantity));
          inventoryTransaction.setTotal(Double.valueOf(inventoryTransaction.getQuantity().doubleValue() * inventoryTransaction.getUnitCost().doubleValue()));
          InventoryStockInForm.this.updatePrice();
        }
      }
    };
    add(new JScrollPane(table));
    
    JPanel southActionPanel = new JPanel(new MigLayout("fillx,hidemode 3"));
    
    btnAddItem = new JButton("Add Item");
    JButton btnEditItem = new JButton("Edit");
    btnDeleteItem = new JButton("Delete");
    
    btnAddItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        InventoryTransaction inventoryTransaction = new InventoryTransaction();
        inventoryTransaction.setTransactionType(((InventoryTransaction)getBean()).getTransactionType());
        InventoryItemEntryDialog dialog = new InventoryItemEntryDialog(inventoryTransaction);
        dialog.setSize(500, 400);
        dialog.open();
        if (dialog.isCanceled())
          return;
        tableModel.addRow(dialog.getInventoryTransaction());
        InventoryStockInForm.this.updatePrice();
      }
    });
    btnEditItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        int row = table.getSelectedRow();
        if (row == -1)
          return;
        int index = table.convertRowIndexToModel(row);
        
        InventoryTransaction inventoryTransaction = (InventoryTransaction)tableModel.getRow(index);
        if (inventoryTransaction == null) {
          return;
        }
        InventoryItemEntryDialog dialog = new InventoryItemEntryDialog(inventoryTransaction);
        dialog.setSize(500, 400);
        dialog.open();
        if (dialog.isCanceled())
          return;
        table.repaint();
        InventoryStockInForm.this.updatePrice();
      }
      
    });
    btnDeleteItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        int row = table.getSelectedRow();
        if (row == -1)
          return;
        int index = table.convertRowIndexToModel(row);
        
        InventoryTransaction item = (InventoryTransaction)tableModel.getRow(index);
        if (item == null) {
          return;
        }
        tableModel.removeRow(index);
      }
    });
    southActionPanel.add(btnAddItem, "split 3,left");
    southActionPanel.add(btnEditItem);
    southActionPanel.add(btnDeleteItem);
    
    tfSubTotalAmount = new DoubleTextField(10);
    tfSubTotalAmount.setHorizontalAlignment(4);
    tfSubTotalAmount.setEditable(false);
    southActionPanel.add(new JLabel("Sub-total Amount: (" + CurrencyUtil.getCurrencySymbol() + ")"), "split 2,right");
    southActionPanel.add(tfSubTotalAmount, "");
    
    add(southActionPanel, "South");
  }
  
  private void resizeTableColumns()
  {
    table.setAutoResizeMode(4);
    setColumnWidth(0, PosUIManager.getSize(100));
    setColumnWidth(2, PosUIManager.getSize(80));
    setColumnWidth(3, PosUIManager.getSize(100));
    setColumnWidth(4, PosUIManager.getSize(80));
    setColumnWidth(5, PosUIManager.getSize(120));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = table.getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  private void buildTree(DefaultMutableTreeNode node) {
    InventoryLocation location = (InventoryLocation)node.getUserObject();
    List<InventoryLocation> children = null;
    
    if (location != null) {
      children = location.getChildren();
    }
    

    if (children == null) {
      return;
    }
    
    for (InventoryLocation inventoryLocation : children) {
      DefaultMutableTreeNode newChild = new DefaultMutableTreeNode(inventoryLocation);
      node.add(newChild);
      buildTree(newChild);
    }
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      
      InventoryTransactionDAO.getInstance().saveInventoryTransactionList(inventoryTransactions);
      
      return true;
    } catch (Exception e) {
      PosLog.error(getClass(), e);
      POSMessageDialog.showError(e.getMessage());
    }
    return false;
  }
  
  public void updateView() {
    InventoryTransaction inventoryTransaction = (InventoryTransaction)getBean();
    if (inventoryTransaction == null) {
      return;
    }
    if (inventoryTransaction.getReason() != null) {
      cbInventoryReasons.setSelectedItem(inventoryTransaction.getReason());
    }
    updatePrice();
  }
  
  public boolean updateModel()
  {
    inventoryTransactions = tableModel.getRows();
    
    if ((inventoryTransactions == null) || (inventoryTransactions.size() == 0)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please add item.");
      return false;
    }
    if (location == null) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select location.");
      return false;
    }
    for (Iterator iterator = inventoryTransactions.iterator(); iterator.hasNext();) {
      InventoryTransaction inventoryTransaction = (InventoryTransaction)iterator.next();
      
      inventoryTransaction.setToInventoryLocation(location);
      if (inventoryTransaction.getQuantity().doubleValue() <= 0.0D) {
        POSMessageDialog.showMessage(getParent(), "Please enter quantity.");
        return false;
      }
      



      if (inventoryTransaction.getUnit().isEmpty()) {
        POSMessageDialog.showMessage(getParent(), "Please enter unit.");
        return false;
      }
      inventoryTransaction.setTransactionDate(new Date());
      String reasonType = (String)cbInventoryReasons.getSelectedItem();
      if (StringUtils.isEmpty(reasonType)) {
        POSMessageDialog.showMessage(getParent(), "Please enter reason type.");
        return false;
      }
      inventoryTransaction.setReason(reasonType);
      inventoryTransaction.setVendor((InventoryVendor)cbVendor.getSelectedItem());
    }
    

    return true;
  }
  
  public String getDisplayText()
  {
    return "Stock In";
  }
}
