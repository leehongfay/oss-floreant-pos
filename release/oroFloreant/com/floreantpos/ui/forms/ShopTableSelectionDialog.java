package com.floreantpos.ui.forms;

import com.floreantpos.Messages;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.dao.ShopTableDAO;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.ref.SoftReference;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import org.jdesktop.swingx.combobox.ListComboBoxModel;























public class ShopTableSelectionDialog
  extends POSDialog
{
  private JComboBox<ShopTable> cbTables;
  private SoftReference<ShopTableSelectionDialog> instance;
  private JSeparator jSeparator1;
  private JPanel buttonPanel;
  
  public ShopTableSelectionDialog()
  {
    super(POSUtil.getBackOfficeWindow(), Messages.getString("ShopTableSelectionDialog.0"), true);
    
    initModel();
  }
  
  protected void initUI()
  {
    getContentPane().setLayout(new BorderLayout(0, 0));
    GridLayout experimentLayout = new GridLayout(0, 3, 5, 0);
    buttonPanel = new JPanel(experimentLayout);
    buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    
    getContentPane().add(buttonPanel, "South");
    
    JButton btnOk = new JButton(Messages.getString("ShopTableSelectionDialog.1"));
    btnOk.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setCanceled(false);
        dispose();
      }
      
    });
    buttonPanel.add(btnOk);
    
    JButton btnCancel = new JButton(Messages.getString("ShopTableSelectionDialog.2"));
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
      
    });
    buttonPanel.add(btnCancel);
    
    JPanel contentPanel = new JPanel();
    contentPanel.setPreferredSize(new Dimension(132, 65));
    getContentPane().add(contentPanel, "North");
    
    JLabel lblSelectTable = new JLabel(Messages.getString("ShopTableSelectionDialog.3"));
    contentPanel.add(lblSelectTable);
    
    cbTables = new JComboBox();
    cbTables.setPreferredSize(new Dimension(132, 24));
    contentPanel.add(cbTables);
    
    jSeparator1 = new JSeparator();
    getContentPane().add(jSeparator1, "Center");
  }
  




















  private void initModel()
  {
    try
    {
      cbTables.setModel(new ListComboBoxModel(ShopTableDAO.getInstance().getAllUnassigned()));
    } catch (Exception e) {
      POSMessageDialog.showError(this, Messages.getString("ShopTableSelectionDialog.4"), e);
    }
  }
  
  public ShopTable getSelectedTable() {
    if (isCanceled()) {
      return null;
    }
    
    return (ShopTable)cbTables.getSelectedItem();
  }
  
  public ShopTableSelectionDialog getInstance() {
    if ((instance == null) || (instance.get() == null)) {
      instance = new SoftReference(new ShopTableSelectionDialog());
    }
    
    return (ShopTableSelectionDialog)instance.get();
  }
  
  public JPanel getButtonPanel() {
    return buttonPanel;
  }
  
  public void refresh() {
    try {
      cbTables.setModel(new ListComboBoxModel(ShopTableDAO.getInstance().getAllUnassigned()));
    } catch (Exception e) {
      POSMessageDialog.showError(this, Messages.getString("ShopTableSelectionDialog.4"), e);
    }
  }
}
