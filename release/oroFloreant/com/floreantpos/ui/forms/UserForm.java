package com.floreantpos.ui.forms;

import com.floreantpos.Messages;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.ImageResource;
import com.floreantpos.model.Outlet;
import com.floreantpos.model.Store;
import com.floreantpos.model.User;
import com.floreantpos.model.UserType;
import com.floreantpos.model.dao.ImageResourceDAO;
import com.floreantpos.model.dao.OutletDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.model.dao.UserTypeDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthDocument;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.ImageGalleryDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import net.miginfocom.swing.MigLayout;



































public class UserForm
  extends BeanEditor
{
  private FixedLengthTextField tfFirstName;
  private FixedLengthTextField tfId;
  private FixedLengthTextField tfLastName;
  private JPasswordField tfPassword1;
  private JPasswordField tfPassword2;
  private FixedLengthTextField tfSsn;
  private FixedLengthTextField tfPhone;
  private JComboBox cbUserType;
  private JLabel lblImagePreview;
  private DoubleTextField tfCostPerHour;
  private JCheckBox chkDriver;
  private JCheckBox chkStaffBank;
  private JCheckBox chkAutoStartStaffBank;
  private JCheckBox chkAllowReceiveTips;
  private JCheckBox chkBlindAccountableAmount;
  private ImageResource imageResource;
  private JCheckBox chkActive;
  private JComboBox cbOutlet = new JComboBox();
  private boolean editMode;
  private DoubleTextField tfOTRatePerHour;
  
  public UserForm()
  {
    initComponents();
    UserTypeDAO dao = new UserTypeDAO();
    List<UserType> userTypes = dao.findAll();
    cbUserType.setModel(new DefaultComboBoxModel(userTypes.toArray()));
    cbOutlet.setModel(getComboModel(OutletDAO.getInstance().findAll()));
  }
  
  private ComboBoxModel getComboModel(List items) {
    List dataList = new ArrayList();
    dataList.add("");
    dataList.addAll(items);
    return new ComboBoxModel(dataList);
  }
  
  private void initComponents() {
    setLayout(new MigLayout("hidemode 3", "[134px][204px,grow][][]", ""));
    
    JLabel lblUserType = new JLabel(Messages.getString("UserForm.31"));
    JLabel lblCostPerHour = new JLabel(Messages.getString("UserForm.28"));
    JLabel lblId = new JLabel(Messages.getString("UserForm.7"));
    JLabel lblSSN = new JLabel("SSN");
    JLabel lblFirstName = new JLabel(Messages.getString("UserForm.14"));
    JLabel lblLastName = new JLabel(Messages.getString("UserForm.16"));
    JLabel lblSecretKey = new JLabel(Messages.getString("UserForm.18"));
    JLabel lblConfirmSecretKey = new JLabel(Messages.getString("UserForm.20"));
    JLabel lblPhone = new JLabel(Messages.getString("UserForm.9"));
    
    tfPhone = new FixedLengthTextField();
    tfPhone.setLength(20);
    tfPhone.setColumns(20);
    tfPassword1 = new JPasswordField(new FixedLengthDocument(16), "", 10);
    tfPassword2 = new JPasswordField(new FixedLengthDocument(16), "", 10);
    tfId = new FixedLengthTextField(128);
    tfSsn = new FixedLengthTextField();
    tfSsn.setLength(30);
    tfSsn.setColumns(30);
    tfFirstName = new FixedLengthTextField();
    tfFirstName.setColumns(30);
    tfFirstName.setLength(30);
    tfLastName = new FixedLengthTextField();
    tfLastName.setLength(30);
    tfLastName.setColumns(30);
    tfCostPerHour = new DoubleTextField();
    tfOTRatePerHour = new DoubleTextField();
    cbUserType = new JComboBox();
    
    lblImagePreview = new JLabel("");
    lblImagePreview.setHorizontalAlignment(0);
    
    lblImagePreview.setBorder(BorderFactory.createTitledBorder(null, "Image", 2, 2));
    lblImagePreview.setPreferredSize(new Dimension(150, 150));
    
    JButton btnSelectImage = new JButton("...");
    JButton btnClearImage = new JButton(Messages.getString("MenuItemForm.34"));
    btnClearImage.setHorizontalAlignment(11);
    
    btnSelectImage.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doSelectImageFile();
      }
      

    });
    btnClearImage.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doClearImage();
      }
      
    });
    add(lblId, "cell 0 0,alignx trailing,aligny center");
    add(tfId, "cell 1 0,growx,aligny center");
    
    add(lblPhone, "cell 0 1,alignx trailing");
    add(tfPhone, "cell 1 1,growx");
    
    add(lblSSN, "cell 0 2,alignx trailing,aligny center");
    add(tfSsn, "cell 1 2,aligny center");
    
    add(lblFirstName, "cell 0 3,alignx trailing,aligny center");
    add(tfFirstName, "cell 1 3,growx,aligny center");
    
    add(lblLastName, "cell 0 4,alignx trailing,aligny center");
    add(tfLastName, "cell 1 4,growx,aligny center");
    
    add(lblSecretKey, "cell 0 5,alignx trailing,aligny center");
    add(tfPassword1, "cell 1 5,growx,aligny center");
    
    add(lblConfirmSecretKey, "cell 0 6,alignx trailing,aligny center");
    add(tfPassword2, "cell 1 6,growx,aligny center");
    
    add(lblCostPerHour, "cell 0 7,alignx trailing,aligny center");
    add(tfCostPerHour, "cell 1 7,growx,aligny center");
    
    add(new JLabel("Overtime Rate Per Hour"), "cell 0 8,alignx trailing,aligny center");
    add(tfOTRatePerHour, "cell 1 8,growx,aligny center");
    
    add(lblUserType, "cell 0 9,alignx trailing,aligny center");
    
    cbUserType.setModel(new DefaultComboBoxModel(new String[] { Messages.getString("UserForm.33"), Messages.getString("UserForm.34"), Messages.getString("UserForm.35") }));
    add(cbUserType, "cell 1 9,growx,aligny center");
    
    add(new JLabel("Outlet"), "cell 0 10,alignx trailing,aligny center");
    add(cbOutlet, "cell 1 10,growx,aligny center");
    
    chkDriver = new JCheckBox(Messages.getString("UserForm.0"));
    chkStaffBank = new JCheckBox("Staff Bank");
    chkAutoStartStaffBank = new JCheckBox("Auto start staff bank");
    chkAllowReceiveTips = new JCheckBox("Allow receive tips");
    chkBlindAccountableAmount = new JCheckBox("Blind Accountable Amount");
    
    chkStaffBank.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        chkAutoStartStaffBank.setEnabled(chkStaffBank.isSelected());
        chkBlindAccountableAmount.setEnabled(chkStaffBank.isSelected());
      }
    });
    chkAutoStartStaffBank.setEnabled(chkStaffBank.isSelected());
    chkBlindAccountableAmount.setEnabled(chkStaffBank.isSelected());
    
    add(chkDriver, "cell 1 10");
    add(chkStaffBank, "cell 1 11");
    add(chkAutoStartStaffBank, "cell 1 12");
    add(chkBlindAccountableAmount, "cell 1 13");
    add(chkAllowReceiveTips, "cell 1 14");
    
    chkActive = new JCheckBox("Active");
    add(chkActive, "cell 1 15");
    
    add(lblImagePreview, "cell 3 0 1 6");
    add(btnClearImage, "cell 3 6,center");
    add(btnSelectImage, "cell 3 6,center");
  }
  
  public String getDisplayText()
  {
    if (isEditMode()) {
      return Messages.getString("UserForm.37");
    }
    return Messages.getString("UserForm.38");
  }
  
  public boolean save()
  {
    try {
      updateModel();
    } catch (IllegalModelStateException e) {
      POSMessageDialog.showError(this, e.getMessage());
      return false;
    }
    
    User user = (User)getBean();
    UserDAO userDAO = UserDAO.getInstance();
    
    if ((!editMode) && 
      (userDAO.isUserExist(user.getId()))) {
      POSMessageDialog.showError(this, Messages.getString("UserForm.39") + user.getId() + " " + Messages.getString("UserForm.1"));
      return false;
    }
    
    try
    {
      userDAO.saveOrUpdate(user, editMode);
    } catch (PosException x) {
      POSMessageDialog.showError(this, x.getMessage(), x);
      PosLog.error(getClass(), x);
      return false;
    } catch (Exception x) {
      POSMessageDialog.showError(this, Messages.getString("UserForm.41"), x);
      PosLog.error(getClass(), x);
      return false;
    }
    
    return true;
  }
  
  protected boolean updateModel() throws IllegalModelStateException
  {
    User user = null;
    if (!(getBean() instanceof User)) {
      user = new User();
    }
    else {
      user = (User)getBean();
    }
    String id = tfId.getText();
    String ssn = tfSsn.getText();
    String firstName = tfFirstName.getText();
    String lastName = tfLastName.getText();
    String secretKey1 = new String(tfPassword1.getPassword());
    String secretKey2 = new String(tfPassword2.getPassword());
    
    if (POSUtil.isBlankOrNull(id)) {
      throw new IllegalModelStateException(Messages.getString("UserForm.50"));
    }
    if (POSUtil.isBlankOrNull(firstName)) {
      throw new IllegalModelStateException(Messages.getString("UserForm.43"));
    }
    if (POSUtil.isBlankOrNull(secretKey1)) {
      throw new IllegalModelStateException(Messages.getString("UserForm.45"));
    }
    if (POSUtil.isBlankOrNull(secretKey2)) {
      throw new IllegalModelStateException(Messages.getString("UserForm.46"));
    }
    if (!secretKey1.equals(secretKey2)) {
      throw new IllegalModelStateException(Messages.getString("UserForm.47"));
    }
    
    User userBySecretKey = UserDAO.getInstance().findUserBySecretKey(secretKey1);
    if ((userBySecretKey != null) && (!userBySecretKey.equals(user))) {
      throw new IllegalModelStateException(Messages.getString("UserForm.48"));
    }
    
    double costPerHour = tfCostPerHour.getDoubleOrZero();
    double overtimeRatePerHour = tfOTRatePerHour.getDoubleOrZero();
    if (overtimeRatePerHour == 0.0D) {
      overtimeRatePerHour = costPerHour;
    }
    Object selectedOutlet = cbOutlet.getSelectedItem();
    if ((selectedOutlet instanceof Outlet)) {
      user.setOutletId(((Outlet)selectedOutlet).getId());
    }
    else {
      user.setOutletId(null);
    }
    
    Store store = Application.getInstance().getStore();
    double overtimeMarkup = store.getOvertimeMarkup();
    if ((costPerHour > 0.0D) && (overtimeMarkup > 0.0D) && (overtimeRatePerHour == 0.0D)) {
      overtimeRatePerHour = costPerHour + costPerHour * (overtimeMarkup / 100.0D);
    }
    
    UserType selectedRole = (UserType)cbUserType.getSelectedItem();
    user.setType(selectedRole);
    user.setRoot(Boolean.valueOf(true));
    user.setCostPerHour(Double.valueOf(costPerHour));
    
    user.setSsn(ssn);
    user.setId(id);
    user.setFirstName(firstName);
    user.setLastName(lastName);
    user.setPhoneNo(tfPhone.getText());
    user.setEncryptedPassword(secretKey1);
    user.setDriver(Boolean.valueOf(chkDriver.isSelected()));
    user.setStaffBank(Boolean.valueOf(chkStaffBank.isSelected()));
    user.setActive(Boolean.valueOf(chkActive.isSelected()));
    user.setOvertimeRatePerHour(Double.valueOf(overtimeRatePerHour));
    user.setAllowReceiveTips(Boolean.valueOf(chkAllowReceiveTips.isSelected()));
    if (chkStaffBank.isSelected()) {
      user.setAutoStartStaffBank(Boolean.valueOf(chkAutoStartStaffBank.isSelected()));
      user.setBlindAccountableAmount(Boolean.valueOf(chkBlindAccountableAmount.isSelected()));
    }
    else {
      user.setAutoStartStaffBank(Boolean.valueOf(false));
      user.setBlindAccountableAmount(Boolean.valueOf(false));
    }
    try {
      List<User> linkedUserList = user.getLinkedUser();
      if ((linkedUserList == null) || (linkedUserList.isEmpty())) {
        user.setParentUser(user);
        user.setRoot(Boolean.valueOf(true));
      }
      if (linkedUserList != null) {
        for (User linkedUser : linkedUserList) {
          if ((user.getId() == null) && (linkedUser.getType().getName().equals(selectedRole.getName()))) {
            POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "This role already exists.");
            return false;
          }
        }
      }
    }
    catch (Exception localException) {}
    
    if (imageResource != null) {
      user.setImageId(imageResource.getId());
    }
    else {
      user.setImageId(null);
    }
    
    setBean(user);
    return true;
  }
  
  protected void updateView()
  {
    if (!(getBean() instanceof User)) {
      return;
    }
    User user = (User)getBean();
    setData(user);
  }
  
  private void setData(User user) {
    if (user.getId() != null) {
      tfId.setText(String.valueOf(user.getId()));
    }
    else {
      tfId.setText("");
    }
    if (user.getSsn() != null) {
      tfSsn.setText(user.getSsn());
    }
    else {
      tfSsn.setText("");
    }
    tfFirstName.setText(user.getFirstName());
    tfLastName.setText(user.getLastName());
    tfPassword1.setText(user.getPasswordAsPlainText());
    tfPassword2.setText(user.getPasswordAsPlainText());
    tfPhone.setText(user.getPhoneNo());
    cbUserType.setSelectedItem(user.getType());
    
    Double costPerHour = user.getCostPerHour();
    if (costPerHour == null) {
      costPerHour = Double.valueOf(0.0D);
    }
    
    tfCostPerHour.setText(String.valueOf(costPerHour));
    chkDriver.setSelected(user.isDriver().booleanValue());
    chkStaffBank.setSelected(user.isStaffBank().booleanValue());
    chkAutoStartStaffBank.setSelected(user.isAutoStartStaffBank().booleanValue());
    chkBlindAccountableAmount.setSelected(user.isBlindAccountableAmount().booleanValue());
    chkActive.setSelected(user.isActive().booleanValue());
    chkAllowReceiveTips.setSelected(user.isAllowReceiveTips().booleanValue());
    imageResource = ImageResourceDAO.getInstance().findById(user.getImageId());
    if (imageResource != null) {
      lblImagePreview.setIcon(imageResource.getAsIcon());
    }
    chkAutoStartStaffBank.setEnabled(chkStaffBank.isSelected());
    chkBlindAccountableAmount.setEnabled(chkStaffBank.isSelected());
    
    cbOutlet.setSelectedItem(user.getOutletId() == null ? "" : OutletDAO.getInstance().get(user.getOutletId()));
    if (user.getOvertimeRatePerHour() != null) {
      tfOTRatePerHour.setText(String.valueOf(user.getOvertimeRatePerHour()));
    }
  }
  
  public boolean isEditMode() {
    return editMode;
  }
  
  public void setEditMode(boolean editMode) {
    this.editMode = editMode;
    if (editMode) {
      tfId.setEditable(false);
    }
    else {
      tfId.setEditable(true);
    }
  }
  
  public void setId(Integer id) {
    if (id != null) {
      tfId.setText(String.valueOf(id.intValue()));
    }
  }
  
  protected void doSelectImageFile() {
    ImageGalleryDialog dialog = ImageGalleryDialog.getInstance();
    dialog.setSelectBtnVisible(true);
    dialog.setTitle("Image Gallery");
    dialog.setSize(PosUIManager.getSize(650, 600));
    dialog.setResizable(false);
    dialog.open();
    if (dialog.isCanceled()) {
      return;
    }
    imageResource = dialog.getImageResource();
    if (imageResource != null) {
      lblImagePreview.setIcon(imageResource.getAsIcon());
    }
  }
  
  protected void doClearImage() {
    lblImagePreview.setIcon(null);
    imageResource = null;
  }
}
