package com.floreantpos.ui.forms;

import com.floreantpos.Messages;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.Customer;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.model.util.ZipCodeUtil;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.QwertyKeyPad;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;






















public class QuickCustomerForm
  extends BeanEditor<Customer>
{
  private FixedLengthTextField tfAddress;
  private FixedLengthTextField tfCity;
  private FixedLengthTextField tfZip;
  private FixedLengthTextField tfName;
  private FixedLengthTextField tfState;
  private FixedLengthTextField tfPhone;
  private QwertyKeyPad qwertyKeyPad;
  public boolean isKeypad;
  private FixedLengthTextField tfPersonalEmail;
  private FixedLengthTextField tfBusinessEmail;
  
  public QuickCustomerForm()
  {
    createCustomerForm();
  }
  
  public QuickCustomerForm(Customer customer) {
    isKeypad = true;
    createCustomerForm();
    setBean(customer);
  }
  
  public QuickCustomerForm(boolean showKeyPad) {
    isKeypad = showKeyPad;
    createCustomerForm();
  }
  
  private void createCustomerForm() {
    setLayout(new BorderLayout(10, 10));
    setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    setOpaque(true);
    JPanel inputPanel = new JPanel();
    inputPanel.setLayout(new MigLayout("insets 10 10 10 10, fill", "[][fill,grow]", ""));
    inputPanel.setBorder(BorderFactory.createTitledBorder(Messages.getString("QuickCustomerForm.2")));
    
    JLabel lblAddress = new JLabel(Messages.getString("CustomerForm.18"));
    tfAddress = new FixedLengthTextField(220);
    tfAddress.setColumns(60);
    
    JLabel lblZip = new JLabel(Messages.getString("CustomerForm.21"));
    tfZip = new FixedLengthTextField(30);
    
    JLabel lblCitytown = new JLabel(Messages.getString("QuickCustomerForm.3"));
    tfCity = new FixedLengthTextField();
    
    JLabel lblState = new JLabel(Messages.getString("QuickCustomerForm.0"));
    tfState = new FixedLengthTextField(30);
    
    JLabel lblPhone = new JLabel(Messages.getString("QuickCustomerForm.4"));
    tfPhone = new FixedLengthTextField(30);
    
    JLabel lblPersonalEmail = new JLabel(Messages.getString("QuickCustomerForm.5"));
    tfPersonalEmail = new FixedLengthTextField(30);
    
    JLabel lblBusinessEmail = new JLabel(Messages.getString("QuickCustomerForm.6"));
    tfBusinessEmail = new FixedLengthTextField(30);
    
    JLabel lblName = new JLabel("Name");
    tfName = new FixedLengthTextField();
    tfName.setLength(120);
    
    inputPanel.add(lblPhone, "alignx right");
    inputPanel.add(tfPhone, "wrap");
    
    inputPanel.add(lblName, "alignx right");
    inputPanel.add(tfName, "wrap");
    
    inputPanel.add(lblPersonalEmail, "right");
    inputPanel.add(tfPersonalEmail, "wrap");
    
    inputPanel.add(lblBusinessEmail, "right");
    inputPanel.add(tfBusinessEmail, "wrap");
    
    inputPanel.add(lblAddress, "right");
    inputPanel.add(tfAddress, "wrap");
    
    inputPanel.add(lblZip, "right");
    inputPanel.add(tfZip, "wrap");
    
    inputPanel.add(lblCitytown, "right");
    inputPanel.add(tfCity, "wrap");
    
    inputPanel.add(lblState, "right");
    inputPanel.add(tfState, "wrap");
    
    qwertyKeyPad = new QwertyKeyPad();
    
    add(inputPanel, "Center");
    
    if (isKeypad) {
      add(qwertyKeyPad, "South");
    }
    
    tfZip.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        QuickCustomerForm.this.getStateAndCityByZipCode();
      }
      
    });
    tfZip.addFocusListener(new FocusListener()
    {
      public void focusLost(FocusEvent e)
      {
        QuickCustomerForm.this.getStateAndCityByZipCode();
      }
      




      public void focusGained(FocusEvent e) {}
    });
    enableCustomerFields(false);
    callOrderController();
  }
  
  public void callOrderController() {
    Vector<Component> order = new Vector();
    
    order.add(tfPhone);
    order.add(tfName);
    order.add(tfPersonalEmail);
    order.add(tfBusinessEmail);
    

    order.add(tfZip);
    order.add(tfCity);
    order.add(tfState);
    order.add(tfAddress);
  }
  




  public void enableCustomerFields(boolean enable)
  {
    tfName.setEnabled(enable);
    tfPersonalEmail.setEnabled(enable);
    tfBusinessEmail.setEnabled(enable);
    tfAddress.setEnabled(enable);
    tfCity.setEnabled(enable);
    tfZip.setEnabled(enable);
    tfPhone.setEnabled(enable);
  }
  
  public void setFieldsEnable(boolean enable)
  {
    tfName.setEnabled(enable);
    tfPersonalEmail.setEnabled(enable);
    tfBusinessEmail.setEnabled(enable);
    tfAddress.setEnabled(enable);
    tfCity.setEnabled(enable);
    tfZip.setEnabled(enable);
    tfPhone.setEnabled(enable);
  }
  
  public void setFieldsEditable(boolean editable) {
    tfName.setEditable(editable);
    tfPersonalEmail.setEditable(editable);
    tfBusinessEmail.setEditable(editable);
    tfAddress.setEditable(editable);
    tfCity.setEditable(editable);
    tfZip.setEditable(editable);
    tfPhone.setEditable(editable);
  }
  
  public void createNew()
  {
    setBean(new Customer());
    tfName.setText("");
    tfPersonalEmail.setText("");
    tfBusinessEmail.setText("");
    tfAddress.setText("");
    tfCity.setText("");
    tfZip.setText("");
    tfPhone.setText("");
  }
  
  public boolean save()
  {
    try {
      if (!updateModel())
        return false;
      Customer customer = (Customer)getBean();
      CustomerDAO.getInstance().saveOrUpdate(customer);
      updateView();
      return true;
    } catch (Exception e) {
      BOMessageDialog.showError(this, Messages.getString("CustomerForm.47"));
    }
    return false;
  }
  
  protected void updateView()
  {
    Customer customer = (Customer)getBean();
    if (customer == null) {
      return;
    }
    tfName.setText(customer.getName());
    tfPersonalEmail.setText(customer.getEmail());
    tfBusinessEmail.setText(customer.getEmail2());
    tfCity.setText(customer.getCity());
    tfZip.setText(customer.getZipCode());
    tfPhone.setText(customer.getMobileNo());
    tfState.setText(customer.getState());
    tfZip.setText(customer.getZipCode());
    tfAddress.setText(customer.getAddress());
  }
  
  protected boolean updateModel() throws IllegalModelStateException
  {
    String phone = tfPhone.getText();
    String name = tfName.getText();
    String[] fullName = name.split(" ");
    String fname = fullName[0];
    String lastName = name.substring(fname.length(), name.length());
    String personalEmail = tfPersonalEmail.getText();
    String businessEmail = tfBusinessEmail.getText();
    
    if (StringUtils.isEmpty(name)) {
      POSMessageDialog.showError(null, Messages.getString("QuickCustomerForm.1"));
      return false;
    }
    
    if ((StringUtils.isNotEmpty(personalEmail)) && 
      (!doCheckValidEmail(personalEmail))) {
      return false;
    }
    

    if ((StringUtils.isNotEmpty(businessEmail)) && 
      (!doCheckValidEmail(businessEmail))) {
      return false;
    }
    
    Customer customer = (Customer)getBean();
    
    if (customer == null) {
      customer = new Customer();
      setBean(customer, false);
    }
    customer.setName(name);
    customer.setFirstName(fname);
    customer.setLastName(lastName);
    customer.setEmail(personalEmail);
    customer.setEmail2(businessEmail);
    customer.setAddress(tfAddress.getText());
    customer.setCity(tfCity.getText());
    customer.setState(tfState.getText());
    customer.setZipCode(tfZip.getText());
    customer.setMobileNo(phone);
    
    return true;
  }
  
  private boolean doCheckValidEmail(String personalEmail) {
    boolean valid = EmailValidator.getInstance().isValid(personalEmail);
    if (!valid) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), Messages.getString("QuickCustomerForm.8"));
      return false;
    }
    return valid;
  }
  
  public boolean delete()
  {
    try {
      Customer bean2 = (Customer)getBean();
      if (bean2 == null) {
        return false;
      }
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), "Are you sure to delete selected table?", "Confirm");
      if (option != 0) {
        return false;
      }
      CustomerDAO.getInstance().delete(bean2);
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e);
    }
    return false;
  }
  
  public String getDisplayText()
  {
    return Messages.getString("CustomerForm.54");
  }
  
  public static class MyOwnFocusTraversalPolicy extends FocusTraversalPolicy {
    Vector<Component> order;
    
    public MyOwnFocusTraversalPolicy(Vector<Component> order) {
      this.order = new Vector(order.size());
      this.order.addAll(order);
    }
    
    public Component getComponentAfter(Container focusCycleRoot, Component aComponent) {
      int idx = (order.indexOf(aComponent) + 1) % order.size();
      return (Component)order.get(idx);
    }
    
    public Component getComponentBefore(Container focusCycleRoot, Component aComponent) {
      int idx = order.indexOf(aComponent) - 1;
      if (idx < 0) {
        idx = order.size() - 1;
      }
      return (Component)order.get(idx);
    }
    
    public Component getDefaultComponent(Container focusCycleRoot) {
      return (Component)order.get(0);
    }
    
    public Component getLastComponent(Container focusCycleRoot) {
      return (Component)order.lastElement();
    }
    
    public Component getFirstComponent(Container focusCycleRoot) {
      return (Component)order.get(0);
    }
  }
  
  private void getStateAndCityByZipCode()
  {
    String zipCode = tfZip.getText();
    
    if ((zipCode == null) || (zipCode.isEmpty()))
    {

      return;
    }
    
    String city = ZipCodeUtil.getCity(zipCode);
    String state = ZipCodeUtil.getState(zipCode);
    
    if (StringUtils.isNotEmpty(state)) {
      tfState.setText(state);
    }
    if (StringUtils.isNotEmpty(city)) {
      tfCity.setText(city);
    }
  }
  
  public void setPhoneNo(String phoneNo) {
    tfPhone.setText(phoneNo);
  }
}
