package com.floreantpos.ui.forms;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.UserType;
import com.floreantpos.model.dao.UserTypeDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.CheckBoxList;
import com.floreantpos.swing.CheckBoxList.Entry;
import com.floreantpos.swing.CheckBoxListModel;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import java.util.List;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import org.apache.commons.lang.StringUtils;
















public class UserTypeForm
  extends BeanEditor
{
  private JLabel jLabel1;
  private JLabel jLabel2;
  private JScrollPane jScrollPane1;
  private CheckBoxList<UserPermission> listPermissions;
  private JTextField tfTypeName;
  private JPanel headerPanel;
  private JPanel centerPanel;
  
  public UserTypeForm()
  {
    this(new UserType());
  }
  
  public UserTypeForm(UserType type) {
    initComponents();
    listPermissions.setModel(UserPermission.permissions);
    setBean(type);
  }
  





  private void initComponents()
  {
    jLabel1 = new JLabel();
    tfTypeName = new JTextField();
    jLabel2 = new JLabel();
    jScrollPane1 = new JScrollPane();
    listPermissions = new CheckBoxList();
    listPermissions.setTableHeaderHide(true);
    headerPanel = new JPanel(new BorderLayout(5, 5));
    headerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    centerPanel = new JPanel(new BorderLayout(5, 5));
    centerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    
    jLabel1.setText(POSConstants.TYPE_NAME + ":");
    
    jLabel2.setText(POSConstants.PERMISSIONS + ":");
    
    jScrollPane1.setViewportView(listPermissions);
    
    BorderLayout layout = new BorderLayout();
    setLayout(layout);
    
    headerPanel.add(jLabel1, "West");
    headerPanel.add(tfTypeName);
    

    centerPanel.add(jLabel2, "West");
    centerPanel.add(jScrollPane1, "Center");
    
    add(headerPanel, "North");
    add(centerPanel, "Center");
  }
  










  public boolean save()
  {
    try
    {
      if (!updateModel()) {
        return false;
      }
    } catch (IllegalModelStateException e) {
      POSMessageDialog.showError(e.getMessage());
      return false;
    }
    
    UserTypeDAO dao = new UserTypeDAO();
    dao.saveOrUpdate(getUserType());
    
    return true;
  }
  
  protected boolean updateModel() throws IllegalModelStateException
  {
    UserType userType = getUserType();
    if (userType == null) {
      userType = new UserType();
    }
    
    String name = tfTypeName.getText();
    if (StringUtils.isEmpty(name)) {
      throw new IllegalModelStateException(POSConstants.TYPE_NAME_CANNOT_BE_EMPTY);
    }
    
    userType.setName(name);
    userType.clearPermissions();
    
    List<UserPermission> checkedValues = listPermissions.getCheckedValues();
    for (int i = 0; i < checkedValues.size(); i++) {
      userType.addTopermissions((UserPermission)checkedValues.get(i));
    }
    
    setBean(userType);
    return true;
  }
  
  protected void updateView()
  {
    UserType userType = getUserType();
    if (userType.getId() == null) {
      listPermissions.clearSelection();
      return;
    }
    
    tfTypeName.setText(userType.getName());
    
    Set<UserPermission> permissions = userType.getPermissions();
    if (permissions == null) {
      listPermissions.clearSelection();
      return;
    }
    
    CheckBoxListModel model = (CheckBoxListModel)listPermissions.getModel();
    for (UserPermission permission : permissions) {
      for (int i = 0; i < model.getItems().size(); i++) {
        CheckBoxList.Entry entry = (CheckBoxList.Entry)model.getItems().get(i);
        if (entry.getValue().equals(permission)) {
          entry.setChecked(true);
        }
      }
    }
    model.fireTableDataChanged();
  }
  
  public UserType getUserType() {
    return (UserType)getBean();
  }
  
  public String getDisplayText()
  {
    UserType userType = (UserType)getBean();
    
    if ((userType == null) || (userType.getId() == null)) {
      return Messages.getString("UserTypeForm.0");
    }
    
    return Messages.getString("UserTypeForm.1");
  }
}
