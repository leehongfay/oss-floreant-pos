package com.floreantpos.ui.forms;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.Customer;
import com.floreantpos.model.ImageResource;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.dao.ImageResourceDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.PosSmallButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.QwertyKeyPad;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.ImageGalleryDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.PosGuiUtil;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FocusTraversalPolicy;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.hibernate.StaleObjectStateException;


























public class CustomerForm
  extends BeanEditor<Customer>
{
  private FixedLengthTextField tfFirstName;
  private FixedLengthTextField tfLastName;
  private FixedLengthTextField tfDoB;
  private FixedLengthTextField tfMobile;
  private FixedLengthTextField tfPersonalEmail;
  private JTextArea tfAddress;
  private FixedLengthTextField tfCity;
  private FixedLengthTextField tfState;
  private FixedLengthTextField tfZip;
  private FixedLengthTextField tfCountry;
  private FixedLengthTextField tfLoyaltyNo;
  private IntegerTextField tfLoyaltyPoint;
  private DoubleTextField tfCreditLimit;
  private DoubleTextField tfBalance;
  private JCheckBox cbVip;
  private JCheckBox cbTaxExempt;
  private JLabel lblPicture;
  private PosSmallButton btnSelectImage;
  private PosSmallButton btnClearImage;
  private JComboBox cbSalutation;
  private QwertyKeyPad qwertyKeyPad;
  public boolean isKeypad;
  private ImageResource imageResource;
  private MyOwnFocusTraversalPolicy newPolicy;
  private Image image;
  private FixedLengthTextField tfBusinessEmail;
  
  public CustomerForm()
  {
    createCustomerForm();
  }
  
  public CustomerForm(boolean enable) {
    isKeypad = enable;
    createCustomerForm();
  }
  
  private void createCustomerForm()
  {
    setOpaque(true);
    setLayout(new MigLayout("fill", "[][][]", ""));
    
    add(createPicturePanel(), "top 0");
    
    add(createCenterPanel(), "grow");
    
    add(createRightPanel(), "grow, wrap");
    qwertyKeyPad = new QwertyKeyPad();
    
    if (isKeypad) {
      add(qwertyKeyPad, "span, grow");
    }
    
    btnSelectImage.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        doSelectImageFile();
      }
    });
    btnClearImage.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        doClearImage();
      }
      
    });
    enableCustomerFields(false);
    callOrderController();
  }
  
  private JPanel createPicturePanel() {
    JPanel picturePanel = new JPanel(new MigLayout());
    
    lblPicture = new JLabel("");
    image = IconFactory.getIcon("/images/", "generic-profile-pic-v2.png").getImage();
    lblPicture = new JLabel("");
    lblPicture.setPreferredSize(new Dimension(200, 200));
    lblPicture.setIcon(new ImageIcon(image.getScaledInstance(200, 200, 1)));
    lblPicture.setIconTextGap(0);
    lblPicture.setHorizontalAlignment(0);
    picturePanel.setBorder(new TitledBorder(null, Messages.getString("CustomerForm.10"), 4, 2, null, null));
    picturePanel.add(lblPicture, "grow,wrap,center");
    
    btnSelectImage = new PosSmallButton();
    btnSelectImage.setText(Messages.getString("CustomerForm.44"));
    picturePanel.add(btnSelectImage, "split 2,center");
    
    btnClearImage = new PosSmallButton();
    btnClearImage.setText(Messages.getString("CustomerForm.45"));
    picturePanel.add(btnClearImage);
    return picturePanel;
  }
  
  private JPanel createCenterPanel() {
    JPanel centerPanel = new JPanel(new MigLayout("fillx", "[][grow]", ""));
    
    JLabel lblSalutation = new JLabel(Messages.getString("CustomerForm.0"));
    
    cbSalutation = new JComboBox();
    cbSalutation.addItem(Messages.getString("CustomerForm.2"));
    cbSalutation.addItem(Messages.getString("CustomerForm.4"));
    cbSalutation.addItem(Messages.getString("CustomerForm.5"));
    cbSalutation.addItem(Messages.getString("CustomerForm.6"));
    
    cbSalutation.setPreferredSize(new Dimension(100, 0));
    
    JLabel lblFirstName = new JLabel(Messages.getString("CustomerForm.3"));
    tfFirstName = new FixedLengthTextField(30);
    
    JLabel lblLastName = new JLabel(Messages.getString("CustomerForm.11"));
    tfLastName = new FixedLengthTextField();
    
    JLabel lblDob = new JLabel("DoB (MM-DD-YYYY)");
    tfDoB = new FixedLengthTextField();
    
    JLabel lblMobile = new JLabel(Messages.getString("CustomerForm.16"));
    tfMobile = new FixedLengthTextField(30);
    
    JLabel lblPersonalEmail = new JLabel(Messages.getString("CustomerForm.17"));
    tfPersonalEmail = new FixedLengthTextField();
    
    JLabel lblBusinessEmail = new JLabel(Messages.getString("CustomerForm.8"));
    tfBusinessEmail = new FixedLengthTextField();
    
    JLabel lblLoyaltyNo = new JLabel(Messages.getString("CustomerForm.19"));
    tfLoyaltyNo = new FixedLengthTextField();
    
    JLabel lblLoyaltyPoint = new JLabel(Messages.getString("CustomerForm.20"));
    tfLoyaltyPoint = new IntegerTextField();
    tfLoyaltyPoint.setEditable(false);
    
    JLabel lblCreditLimit = new JLabel(Messages.getString("CustomerForm.23"));
    tfCreditLimit = new DoubleTextField();
    
    tfCreditLimit.setText("500.00");
    
    JLabel lblBalance = new JLabel(Messages.getString("CustomerForm.25"));
    tfBalance = new DoubleTextField();
    
    centerPanel.add(lblSalutation, "right");
    centerPanel.add(cbSalutation, "wrap,grow");
    centerPanel.add(lblFirstName, "right");
    centerPanel.add(tfFirstName, "wrap,grow");
    centerPanel.add(lblLastName, "right");
    centerPanel.add(tfLastName, "wrap,grow");
    centerPanel.add(lblDob, "right");
    centerPanel.add(tfDoB, "wrap,grow");
    centerPanel.add(lblMobile, "right");
    centerPanel.add(tfMobile, "wrap,grow");
    centerPanel.add(lblPersonalEmail, "right");
    centerPanel.add(tfPersonalEmail, "wrap,grow");
    centerPanel.add(lblBusinessEmail, "right");
    centerPanel.add(tfBusinessEmail, "wrap,grow");
    centerPanel.add(lblLoyaltyNo, "right");
    centerPanel.add(tfLoyaltyNo, "wrap,grow");
    centerPanel.add(lblLoyaltyPoint, "right");
    centerPanel.add(tfLoyaltyPoint, "wrap,grow");
    centerPanel.add(lblCreditLimit, "right");
    centerPanel.add(tfCreditLimit, "wrap,grow");
    centerPanel.add(lblBalance, "right");
    centerPanel.add(tfBalance, "grow");
    
    return centerPanel;
  }
  
  private JPanel createRightPanel() {
    JPanel rightPanel = new JPanel(new MigLayout("fillx", "[][grow]"));
    
    JLabel lblAddress = new JLabel(Messages.getString("CustomerForm.51"));
    tfAddress = new JTextArea();
    tfAddress.setRows(3);
    tfAddress.setLineWrap(true);
    JScrollPane jScrollPane = new JScrollPane(tfAddress);
    
    JLabel lblCity = new JLabel(Messages.getString("CustomerForm.52"));
    tfCity = new FixedLengthTextField();
    
    JLabel lblState = new JLabel(Messages.getString("CustomerForm.53"));
    tfState = new FixedLengthTextField();
    
    JLabel lblZip = new JLabel(Messages.getString("CustomerForm.55"));
    tfZip = new FixedLengthTextField();
    
    JLabel lblCountry = new JLabel(Messages.getString("CustomerForm.56"));
    tfCountry = new FixedLengthTextField();
    tfCountry.setText(Messages.getString("CustomerForm.57"));
    
    cbVip = new JCheckBox(Messages.getString("CustomerForm.58"));
    cbVip.setFocusable(false);
    
    cbTaxExempt = new JCheckBox(Messages.getString("CustomerForm.59"));
    


    rightPanel.add(lblAddress, "right");
    rightPanel.add(jScrollPane, "wrap, span 0 1, grow");
    rightPanel.add(lblCity, "right");
    rightPanel.add(tfCity, "wrap");
    rightPanel.add(lblState, "right");
    rightPanel.add(tfState, "wrap");
    rightPanel.add(lblZip, "right");
    rightPanel.add(tfZip, "wrap");
    rightPanel.add(lblCountry, "right");
    rightPanel.add(tfCountry, "wrap");
    rightPanel.add(cbVip, "skip 1,wrap");
    rightPanel.add(cbTaxExempt, "skip 1");
    return rightPanel;
  }
  
  public void callOrderController() {
    Vector<Component> order = new Vector();
    order.add(tfFirstName);
    order.add(tfLastName);
    order.add(tfDoB);
    
    order.add(tfMobile);
    order.add(tfPersonalEmail);
    order.add(tfBusinessEmail);
    order.add(tfLoyaltyNo);
    order.add(tfLoyaltyPoint);
    order.add(tfCreditLimit);
    order.add(tfBalance);
    order.add(tfAddress);
    order.add(tfCity);
    order.add(tfState);
    order.add(tfZip);
    order.add(tfCountry);
    
    newPolicy = new MyOwnFocusTraversalPolicy(order);
    
    setFocusCycleRoot(true);
    setFocusTraversalPolicy(newPolicy);
  }
  










  public void enableCustomerFields(boolean enable)
  {
    cbSalutation.setEnabled(enable);
    tfLastName.setEnabled(enable);
    tfFirstName.setEnabled(enable);
    tfDoB.setEnabled(enable);
    
    tfMobile.setEnabled(enable);
    tfPersonalEmail.setEnabled(enable);
    tfBusinessEmail.setEnabled(enable);
    tfAddress.setEnabled(enable);
    tfCity.setEnabled(enable);
    tfState.setEditable(enable);
    tfZip.setEnabled(enable);
    tfCountry.setEnabled(enable);
    tfLoyaltyNo.setEnabled(enable);
    tfLoyaltyPoint.setEnabled(enable);
    tfCreditLimit.setEnabled(enable);
    tfBalance.setEnabled(enable);
    cbVip.setEnabled(enable);
    cbTaxExempt.setEnabled(enable);
    
    btnClearImage.setEnabled(enable);
    btnSelectImage.setEnabled(enable);
  }
  

  public void setFieldsEnable(boolean enable)
  {
    cbSalutation.setEnabled(enable);
    tfFirstName.setEnabled(enable);
    tfLastName.setEnabled(enable);
    tfPersonalEmail.setEnabled(enable);
    tfBusinessEmail.setEnabled(enable);
    tfLoyaltyNo.setEnabled(enable);
    tfAddress.setEnabled(enable);
    tfCity.setEnabled(enable);
    tfCreditLimit.setEnabled(enable);
    tfZip.setEnabled(enable);
    tfCountry.setEnabled(enable);
    cbVip.setEnabled(enable);
    cbTaxExempt.setEnabled(enable);
    tfDoB.setEnabled(enable);
    tfBalance.setEnabled(enable);
    btnClearImage.setEnabled(enable);
    btnSelectImage.setEnabled(enable);
    tfLoyaltyPoint.setEnabled(enable);
    


    tfMobile.setEnabled(enable);
  }
  
  public void setFieldsEditable(boolean editable)
  {
    cbSalutation.setEditable(editable);
    tfFirstName.setEditable(editable);
    tfLastName.setEditable(editable);
    tfPersonalEmail.setEditable(editable);
    tfBusinessEmail.setEditable(editable);
    tfLoyaltyNo.setEditable(editable);
    tfAddress.setEditable(editable);
    tfCity.setEditable(editable);
    tfCreditLimit.setEditable(editable);
    tfZip.setEditable(editable);
    tfCountry.setEditable(editable);
    cbVip.setEnabled(editable);
    cbTaxExempt.setEnabled(editable);
    tfDoB.setEditable(editable);
    tfBalance.setEditable(editable);
    btnClearImage.setEnabled(editable);
    btnSelectImage.setEnabled(editable);
    tfLoyaltyPoint.setEditable(editable);
    


    tfMobile.setEditable(editable);
  }
  

  public void createNew()
  {
    setBean(new Customer());
    tfFirstName.setText("");
    tfLastName.setText("");
    cbSalutation.setSelectedIndex(0);
    tfDoB.setText("");
    tfAddress.setText("");
    tfCity.setText("");
    tfCountry.setText("");
    tfCreditLimit.setText("");
    tfPersonalEmail.setText("");
    tfBusinessEmail.setText("");
    tfLoyaltyNo.setText("");
    tfLoyaltyPoint.setText("");
    tfBalance.setText("");
    
    tfZip.setText("");
    cbVip.setSelected(false);
    cbTaxExempt.setSelected(false);
    
    tfMobile.setText("");
  }
  
  public boolean save()
  {
    try
    {
      if (!updateModel())
        return false;
      Customer customer = (Customer)getBean();
      CustomerDAO.getInstance().saveOrUpdate(customer);
      updateView();
      return true;
    }
    catch (IllegalModelStateException localIllegalModelStateException) {}catch (StaleObjectStateException e) {
      BOMessageDialog.showError(this, Messages.getString("CustomerForm.47"));
    }
    return false;
  }
  
  protected void updateView()
  {
    Customer customer = (Customer)getBean();
    if (customer == null) {
      return;
    }
    cbSalutation.setSelectedItem(customer.getSalutation());
    tfFirstName.setText(customer.getFirstName());
    tfLastName.setText(customer.getLastName());
    tfDoB.setText(customer.getDob());
    tfAddress.setText(customer.getAddress());
    tfCity.setText(customer.getCity());
    tfCountry.setText(customer.getCountry());
    tfCreditLimit.setText(String.valueOf(customer.getCreditLimit()));
    tfPersonalEmail.setText(customer.getEmail());
    tfBusinessEmail.setText(customer.getEmail2());
    tfLoyaltyNo.setText(customer.getLoyaltyNo());
    tfLoyaltyPoint.setText(customer.getLoyaltyPoint().toString());
    tfBalance.setText(String.valueOf(customer.getBalance()));
    
    tfState.setText(customer.getState());
    tfZip.setText(customer.getZipCode());
    cbVip.setSelected(customer.isVip().booleanValue());
    cbTaxExempt.setSelected(customer.isTaxExempt().booleanValue());
    
    tfMobile.setText(customer.getMobileNo());
    if (customer.getSsn() != null) {}
    


    imageResource = ImageResourceDAO.getInstance().findById(customer.getImageId());
    if (imageResource != null) {
      lblPicture.setIcon(new ImageIcon(imageResource.getImage().getScaledInstance(200, 200, 1)));
    }
    else {
      lblPicture.setIcon(new ImageIcon(image.getScaledInstance(200, 200, 1)));
    }
  }
  





















  protected void doSelectImageFile()
  {
    ImageGalleryDialog dialog = ImageGalleryDialog.getInstance();
    dialog.setTitle("Image Gallery");
    dialog.setSelectBtnVisible(true);
    dialog.setSize(PosUIManager.getSize(650, 600));
    dialog.setResizable(false);
    dialog.open();
    if (dialog.isCanceled()) {
      return;
    }
    imageResource = dialog.getImageResource();
    if (imageResource != null) {
      lblPicture.setIcon(new ImageIcon(imageResource.getImage().getScaledInstance(200, 200, 1)));
    }
  }
  
  protected void doClearImage() {
    lblPicture.setIcon(new ImageIcon(image.getScaledInstance(200, 200, 1)));
  }
  
  protected boolean updateModel() throws IllegalModelStateException
  {
    String fname = tfFirstName.getText();
    String personalEmail = tfPersonalEmail.getText();
    String businessEmail = tfBusinessEmail.getText();
    
    if (StringUtils.isEmpty(fname)) {
      POSMessageDialog.showError(null, Messages.getString("CustomerForm.60"));
      return false;
    }
    
    if ((StringUtils.isNotEmpty(personalEmail)) && 
      (!doCheckValidEmail(personalEmail))) {
      return false;
    }
    

    if ((StringUtils.isNotEmpty(businessEmail)) && 
      (!doCheckValidEmail(businessEmail))) {
      return false;
    }
    
    Customer customer = (Customer)getBean();
    
    if (customer == null) {
      customer = new Customer();
      setBean(customer, false);
    }
    Object selectedItem = cbSalutation.getSelectedItem();
    if (selectedItem != null) {
      customer.setSalutation(selectedItem.toString());
    }
    
    customer.setFirstName(tfFirstName.getText());
    customer.setLastName(tfLastName.getText());
    customer.setDob(tfDoB.getText());
    customer.setAddress(tfAddress.getText());
    customer.setCity(tfCity.getText());
    customer.setCountry(tfCountry.getText());
    customer.setState(tfState.getText());
    customer.setCreditLimit(PosGuiUtil.parseDouble(tfCreditLimit));
    customer.setEmail(personalEmail);
    customer.setEmail2(tfBusinessEmail.getText());
    customer.setLoyaltyNo(tfLoyaltyNo.getText());
    customer.setLoyaltyPoint(Integer.valueOf(tfLoyaltyPoint.getInteger()));
    

    customer.setZipCode(tfZip.getText());
    customer.setVip(Boolean.valueOf(cbVip.isSelected()));
    customer.setTaxExempt(Boolean.valueOf(cbTaxExempt.isSelected()));
    customer.setMobileNo(tfMobile.getText());
    
    customer.setBalance(PosGuiUtil.parseDouble(tfBalance));
    

    if (imageResource != null) {
      customer.setImageId(imageResource.getId());
    }
    
    return true;
  }
  
  private boolean doCheckValidEmail(String personalEmail) {
    boolean valid = EmailValidator.getInstance().isValid(personalEmail);
    if (!valid) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), Messages.getString("CustomerForm.9"));
      return false;
    }
    return valid;
  }
  
  public boolean delete()
  {
    try {
      Customer bean2 = (Customer)getBean();
      if (bean2 == null) {
        return false;
      }
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), "Are you sure to delete selected table?", "Confirm");
      if (option != 0) {
        return false;
      }
      CustomerDAO.getInstance().delete(bean2);
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e);
    }
    return false;
  }
  
  public String getDisplayText()
  {
    if (getBean() == null) {
      return Messages.getString("CustomerForm.54");
    }
    
    return Messages.getString("CustomerForm.12");
  }
  
  public static class MyOwnFocusTraversalPolicy extends FocusTraversalPolicy
  {
    Vector<Component> order;
    
    public MyOwnFocusTraversalPolicy(Vector<Component> order) {
      this.order = new Vector(order.size());
      this.order.addAll(order);
    }
    
    public Component getComponentAfter(Container focusCycleRoot, Component aComponent) {
      int idx = (order.indexOf(aComponent) + 1) % order.size();
      return (Component)order.get(idx);
    }
    
    public Component getComponentBefore(Container focusCycleRoot, Component aComponent) {
      int idx = order.indexOf(aComponent) - 1;
      if (idx < 0) {
        idx = order.size() - 1;
      }
      return (Component)order.get(idx);
    }
    
    public Component getDefaultComponent(Container focusCycleRoot) {
      return (Component)order.get(0);
    }
    
    public Component getLastComponent(Container focusCycleRoot) {
      return (Component)order.lastElement();
    }
    
    public Component getFirstComponent(Container focusCycleRoot) {
      return (Component)order.get(0);
    }
  }
}
