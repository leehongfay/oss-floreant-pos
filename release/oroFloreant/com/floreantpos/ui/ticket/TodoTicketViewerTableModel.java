package com.floreantpos.ui.ticket;

import com.floreantpos.Messages;
import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemCookingInstruction;
import com.floreantpos.model.TicketItemModifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;




















public class TodoTicketViewerTableModel
  extends AbstractTableModel
{
  private JTable table;
  protected Ticket ticket;
  private List<ITicketItem> items = new ArrayList();
  
  protected String[] columnNames = {
    Messages.getString("TodoTicketViewerTableModel.0"), Messages.getString("TodoTicketViewerTableModel.1"), Messages.getString("TodoTicketViewerTableModel.2"), Messages.getString("TodoTicketViewerTableModel.3"), Messages.getString("TodoTicketViewerTableModel.4") };
  
  private boolean forReciptPrint;
  private boolean printCookingInstructions;
  
  public TodoTicketViewerTableModel() {}
  
  public TodoTicketViewerTableModel(Ticket ticket)
  {
    setTicket(ticket);
  }
  
  public int getItemCount() {
    return items.size();
  }
  
  public int getRowCount() {
    return items.size();
  }
  
  public int getActualRowCount() {
    return items.size();
  }
  
  public int getColumnCount() {
    return columnNames.length;
  }
  
  public String getColumnName(int column)
  {
    return columnNames[column];
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    ITicketItem ticketItem = (ITicketItem)items.get(rowIndex);
    
    if (ticketItem == null) {
      return null;
    }
    
    switch (columnIndex) {
    case 0: 
      return ticketItem.getNameDisplay();
    
    case 1: 
      return ticketItem.getUnitPriceDisplay();
    
    case 2: 
      return ticketItem.getItemQuantityDisplay();
    
    case 3: 
      return ticketItem.getTaxAmountWithoutModifiersDisplay();
    
    case 4: 
      return ticketItem.getTotalAmountWithoutModifiersDisplay();
    }
    
    return null;
  }
  
  private void calculateRows() {
    items.clear();
    
    if ((ticket == null) || (ticket.getTicketItems() == null)) {
      return;
    }
    List<TicketItem> ticketItems = ticket.getTicketItems();
    for (TicketItem ticketItem : ticketItems)
    {
      items.add(ticketItem);
      

      if (ticketItem.getTicketItemModifiers() != null) {
        List<TicketItemModifier> ticketItemModifiers = ticketItem.getTicketItemModifiers();
        if (ticketItemModifiers != null) {
          for (TicketItemModifier itemModifier : ticketItemModifiers) {
            items.add(itemModifier);
          }
        }
      }
      
      List<TicketItemCookingInstruction> cookingInstructions = ticketItem.getCookingInstructions();
      if (cookingInstructions != null) {
        for (TicketItemCookingInstruction ticketItemCookingInstruction : cookingInstructions) {
          items.add(ticketItemCookingInstruction);
        }
      }
    }
  }
  
  public int addTicketItem(TicketItem ticketItem)
  {
    if (ticketItem.isHasModifiers().booleanValue()) {
      return addTicketItemToTicket(ticketItem);
    }
    
    for (int row = 0; row < items.size(); row++) {
      ITicketItem iTicketItem = (ITicketItem)items.get(row);
      
      if ((iTicketItem instanceof TicketItem))
      {


        TicketItem t = (TicketItem)iTicketItem;
        
        if ((ticketItem.getName().equals(t.getName())) && (!t.isPrintedToKitchen().booleanValue())) {
          t.setQuantity(Double.valueOf(t.getQuantity().doubleValue() + 1.0D));
          
          table.repaint();
          
          return Integer.valueOf(row).intValue();
        }
      }
    }
    return addTicketItemToTicket(ticketItem);
  }
  
  private int addTicketItemToTicket(TicketItem ticketItem) {
    ticket.addToticketItems(ticketItem);
    calculateRows();
    fireTableDataChanged();
    
    return items.size() - 1;
  }
  
  public void addAllTicketItem(TicketItem ticketItem) {
    if (ticketItem.isHasModifiers().booleanValue()) {
      List<TicketItem> ticketItems = ticket.getTicketItems();
      ticketItems.add(ticketItem);
      
      calculateRows();
      fireTableDataChanged();
    }
    else {
      List<TicketItem> ticketItems = ticket.getTicketItems();
      boolean exists = false;
      for (TicketItem item : ticketItems) {
        if (item.getName().equals(ticketItem.getName())) {
          double itemCount = item.getQuantity().doubleValue();
          itemCount += ticketItem.getQuantity().doubleValue();
          item.setQuantity(Double.valueOf(itemCount));
          exists = true;
          table.repaint();
          return;
        }
      }
      if (!exists) {
        ticket.addToticketItems(ticketItem);
        calculateRows();
        fireTableDataChanged();
      }
    }
  }
  
  public boolean containsTicketItem(TicketItem ticketItem) {
    if (ticketItem.isHasModifiers().booleanValue()) {
      return false;
    }
    List<TicketItem> ticketItems = ticket.getTicketItems();
    for (TicketItem item : ticketItems) {
      if (item.getName().equals(ticketItem.getName())) {
        return true;
      }
    }
    return false;
  }
  
  public void removeModifier(TicketItem parent, TicketItemModifier modifierToDelete) {
    List<TicketItemModifier> ticketItemModifiers = parent.getTicketItemModifiers();
    
    for (Iterator iter = ticketItemModifiers.iterator(); iter.hasNext();) {
      TicketItemModifier modifier = (TicketItemModifier)iter.next();
      if (modifier.getItemId() == modifierToDelete.getItemId()) {
        iter.remove();
        
        if (modifier.isPrintedToKitchen().booleanValue()) {
          ticket.addDeletedItems(modifier);
        }
        
        calculateRows();
        fireTableDataChanged();
        return;
      }
    }
  }
  
  public Object delete(int index) {
    if ((index < 0) || (index >= items.size())) {
      return null;
    }
    ITicketItem iTicketItem = (ITicketItem)items.get(index);
    int rowNum;
    Iterator iter; TicketItemModifier itemModifier; Iterator iterator; if ((iTicketItem instanceof TicketItem)) {
      TicketItem ticketItem = (TicketItem)iTicketItem;
      rowNum = ticketItem.getTableRowNum();
      
      List<TicketItem> ticketItems = ticket.getTicketItems();
      for (iter = ticketItems.iterator(); iter.hasNext();) {
        TicketItem item = (TicketItem)iter.next();
        if (item.getTableRowNum() == rowNum) {
          iter.remove();
          
          if ((!item.isPrintedToKitchen().booleanValue()) && (!item.isInventoryAdjusted())) break;
          ticket.addDeletedItems(item); break;
        }
        
      }
      
    }
    else if ((iTicketItem instanceof TicketItemModifier)) {
      itemModifier = (TicketItemModifier)iTicketItem;
      TicketItem ticketItemModifierGroup = itemModifier.getTicketItem();
      List<TicketItemModifier> ticketItemModifiers = ticketItemModifierGroup.getTicketItemModifiers();
      
      if (ticketItemModifiers != null) {
        for (iterator = ticketItemModifiers.iterator(); iterator.hasNext();) {
          TicketItemModifier element = (TicketItemModifier)iterator.next();
          if (itemModifier.getTableRowNum() == element.getTableRowNum()) {
            iterator.remove();
            
            if (element.isPrintedToKitchen().booleanValue()) {
              ticket.addDeletedItems(element);
            }
          }
        }
      }
    }
    

















    calculateRows();
    fireTableDataChanged();
    return iTicketItem;
  }
  




  public Object get(int index)
  {
    return null;
  }
  
  public JTable getTable() {
    return table;
  }
  
  public void setTable(JTable table) {
    this.table = table;
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
    
    update();
  }
  
  public void update() {
    calculateRows();
    fireTableDataChanged();
  }
  
  public boolean isForReciptPrint() {
    return forReciptPrint;
  }
  
  public void setForReciptPrint(boolean forReciptPrint) {
    this.forReciptPrint = forReciptPrint;
  }
  
  public boolean isPrintCookingInstructions() {
    return printCookingInstructions;
  }
  
  public void setPrintCookingInstructions(boolean printCookingInstructions) {
    this.printCookingInstructions = printCookingInstructions;
  }
}
