package com.floreantpos.ui.ticket;

import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import java.awt.Color;
import java.awt.Component;
import java.awt.Rectangle;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;


public class VoidTicketItemViewerTable
  extends JTable
{
  private VoidTicketItemViewerTableModel model;
  private DefaultListSelectionModel selectionModel;
  private final DefaultTableCellRenderer cellRenderer;
  
  public VoidTicketItemViewerTable()
  {
    this(null);
  }
  
  public VoidTicketItemViewerTable(Ticket ticket) {
    model = new VoidTicketItemViewerTableModel(this);
    setModel(model);
    
    selectionModel = new DefaultListSelectionModel();
    selectionModel.setSelectionMode(0);
    
    setGridColor(Color.LIGHT_GRAY);
    setSelectionModel(selectionModel);
    setAutoscrolls(true);
    setShowGrid(true);
    setBorder(null);
    
    setFocusable(false);
    cellRenderer = new DefaultTableCellRenderer() {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component rendererComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        
        VoidTicketItemViewerTableModel model = (VoidTicketItemViewerTableModel)table.getModel();
        Object object = model.get(row);
        
        if (column == 0) {
          setHorizontalAlignment(2);
        } else {
          setHorizontalAlignment(4);
        }
        if (isSelected) {
          return rendererComponent;
        }
        rendererComponent.setBackground(table.getBackground());
        
        if ((object instanceof ITicketItem)) {
          ITicketItem ticketItem = (ITicketItem)object;
          if (ticketItem.isPrintedToKitchen().booleanValue()) {
            rendererComponent.setBackground(Color.YELLOW);
          }
        }
        return rendererComponent;
      }
      
    };
    setRowHeight(50);
    resizeTableColumns();
    
    setTicket(ticket);
  }
  
  private void resizeTableColumns() {
    setAutoResizeMode(4);
  }
  
  public void setTicket(Ticket ticket) {
    model.setTicket(ticket);
  }
  
  public TableCellRenderer getCellRenderer(int row, int column)
  {
    return cellRenderer;
  }
  
  public DefaultTableCellRenderer getRenderer() {
    return cellRenderer;
  }
  
  public Ticket getTicket() {
    return model.getTicket();
  }
  
  public void updateView() {
    int selectedRow = getSelectedRow();
    
    model.update();
    try
    {
      getSelectionModel().setSelectionInterval(selectedRow, selectedRow);
    }
    catch (Exception localException) {}
  }
  
  private boolean isTicketNull()
  {
    Ticket ticket = getTicket();
    if (ticket == null) {
      return true;
    }
    if (ticket.getTicketItems() == null) {
      return true;
    }
    return false;
  }
  
  public void scrollUp() {
    if (isTicketNull()) {
      return;
    }
    int selectedRow = getSelectedRow();
    int rowCount = model.getItemCount();
    
    if (selectedRow > rowCount - 1) {
      return;
    }
    
    selectedRow--;
    if (selectedRow < 0) {
      selectedRow = 0;
    }
    
    selectionModel.addSelectionInterval(selectedRow, selectedRow);
    Rectangle cellRect = getCellRect(selectedRow, 0, false);
    scrollRectToVisible(cellRect);
  }
  
  public void scrollDown() {
    if (isTicketNull()) {
      return;
    }
    int selectedRow = getSelectedRow();
    if (selectedRow >= model.getItemCount() - 1) {
      return;
    }
    
    selectedRow++;
    
    selectionModel.addSelectionInterval(selectedRow, selectedRow);
    Rectangle cellRect = getCellRect(selectedRow, 0, false);
    scrollRectToVisible(cellRect);
  }
  
  public ITicketItem get(int index) {
    return (ITicketItem)model.get(index);
  }
  
  public ITicketItem getSelected() {
    int index = getSelectedRow();
    
    return (ITicketItem)model.get(index);
  }
  
  public void addTicketItem(TicketItem ticketItem) {
    ticketItem.setTicket(getTicket());
    int addTicketItem = model.addTicketItem(ticketItem);
    
    int actualRowCount = addTicketItem;
    selectionModel.addSelectionInterval(actualRowCount, actualRowCount);
    Rectangle cellRect = getCellRect(actualRowCount, 0, false);
    scrollRectToVisible(cellRect);
  }
  
  public void delete(int index, boolean itemWasted) {
    model.delete(index, itemWasted);
  }
  
  public void deleteFromTicket(TicketItem ticketItem) {
    model.deleteFromTicket(ticketItem);
  }
}
