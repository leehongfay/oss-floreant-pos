package com.floreantpos.ui.ticket;

import com.floreantpos.Messages;
import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import org.apache.commons.lang.StringUtils;



















public class VoidTicketItemViewerTableModel
  extends AbstractTableModel
{
  private JTable table;
  protected Ticket ticket;
  private double previousFractionalItemQuantity;
  protected final HashMap<String, ITicketItem> tableRows = new LinkedHashMap();
  
  private boolean priceIncludesTax = false;
  
  protected String[] columnNames = { Messages.getString("TicketViewerTableModel.0"), Messages.getString("TicketViewerTableModel.2"), 
    Messages.getString("TicketViewerTableModel.3") };
  
  public VoidTicketItemViewerTableModel(JTable table) {
    this(table, null);
  }
  
  public VoidTicketItemViewerTableModel(JTable table, Ticket ticket) {
    this.table = table;
    setTicket(ticket);
  }
  
  public int getItemCount() {
    return tableRows.size();
  }
  
  public int getRowCount() {
    int size = tableRows.size();
    
    return size;
  }
  
  public int getActualRowCount() {
    return tableRows.size();
  }
  
  public int getColumnCount() {
    return columnNames.length;
  }
  
  public String getColumnName(int column)
  {
    return columnNames[column];
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    ITicketItem ticketItem = (ITicketItem)tableRows.get(String.valueOf(rowIndex));
    
    if (ticketItem == null) {
      return null;
    }
    
    switch (columnIndex) {
    case 0: 
      return ticketItem.getNameDisplay();
    
    case 1: 
      return ticketItem.getItemQuantityDisplay();
    
    case 2: 
      return ticketItem.getSubTotalAmountDisplay();
    }
    
    return null;
  }
  
  private void calculateRows() {
    TicketItemRowCreator.calculateTicketRows(ticket, tableRows, true);
    Collection<ITicketItem> items = tableRows.values();
    for (Iterator iterator = items.iterator(); iterator.hasNext();) {
      ITicketItem item = (ITicketItem)iterator.next();
      if ((!ticket.isPaid().booleanValue()) && (!ticket.isClosed().booleanValue()) && (!item.isPrintedToKitchen().booleanValue()))
        iterator.remove();
    }
  }
  
  public int addTicketItem(TicketItem ticketItem) {
    if (ticketItem.isHasModifiers().booleanValue()) {
      return addTicketItemToTicket(ticketItem);
    }
    
    Object[] values = tableRows.values().toArray();
    if ((values == null) || (values.length == 0)) {
      previousFractionalItemQuantity = ticketItem.getQuantity().doubleValue();
      return addTicketItemToTicket(ticketItem);
    }
    
    Object object = values[(values.length - 1)];
    if ((object instanceof TicketItem)) {
      TicketItem item = (TicketItem)object;
      
      if (StringUtils.isEmpty(ticketItem.getMenuItemId())) {
        return addTicketItemToTicket(ticketItem);
      }
      
      if (ticketItem.getMenuItemId().equals(item.getMenuItemId())) {
        previousFractionalItemQuantity = item.getQuantity().doubleValue();
        if (ticketItem.isFractionalUnit().booleanValue()) {
          item.setFractionalUnit(Boolean.valueOf(true));
          item.setQuantity(Double.valueOf(previousFractionalItemQuantity + ticketItem.getQuantity().doubleValue()));
          previousFractionalItemQuantity = item.getQuantity().doubleValue();
        }
        else {
          item.setQuantity(Double.valueOf(item.getQuantity().doubleValue() + 1.0D));
        }
        return values.length - 1;
      }
    }
    previousFractionalItemQuantity = ticketItem.getQuantity().doubleValue();
    return addTicketItemToTicket(ticketItem);
  }
  
  private int addTicketItemToTicket(TicketItem ticketItem) {
    ticket.addToticketItems(ticketItem);
    calculateRows();
    fireTableDataChanged();
    
    return tableRows.size() - 1;
  }
  
  public void addAllTicketItem(TicketItem ticketItem) {
    if (ticketItem.isHasModifiers().booleanValue()) {
      List<TicketItem> ticketItems = ticket.getTicketItems();
      ticketItems.add(ticketItem);
      
      calculateRows();
      fireTableDataChanged();
    }
    else {
      List<TicketItem> ticketItems = ticket.getTicketItems();
      boolean exists = false;
      for (TicketItem item : ticketItems) {
        if (item.getName().equals(ticketItem.getName())) {
          double itemCount = item.getQuantity().doubleValue();
          itemCount += ticketItem.getQuantity().doubleValue();
          item.setQuantity(Double.valueOf(itemCount));
          exists = true;
          table.repaint();
          return;
        }
      }
      if (!exists) {
        ticket.addToticketItems(ticketItem);
        calculateRows();
        fireTableDataChanged();
      }
    }
  }
  
  public boolean containsTicketItem(TicketItem ticketItem) {
    if (ticketItem.isHasModifiers().booleanValue()) {
      return false;
    }
    List<TicketItem> ticketItems = ticket.getTicketItems();
    for (TicketItem item : ticketItems) {
      if (item.getName().equals(ticketItem.getName())) {
        return true;
      }
    }
    return false;
  }
  
  public Object delete(int index, boolean itemWasted) {
    if ((index < 0) || (index >= tableRows.size())) {
      return null;
    }
    Object object = tableRows.get(String.valueOf(index));
    int rowNum; Iterator iter; if ((object instanceof TicketItem)) {
      TicketItem ticketItem = (TicketItem)object;
      rowNum = ticketItem.getTableRowNum();
      
      List<TicketItem> ticketItems = ticket.getTicketItems();
      for (iter = ticketItems.iterator(); iter.hasNext();) {
        TicketItem item = (TicketItem)iter.next();
        if (item.getTableRowNum() == rowNum) {
          iter.remove();
          if (itemWasted) break;
          ticket.addDeletedItems(item); break;
        }
      }
    }
    

    calculateRows();
    fireTableDataChanged();
    return object;
  }
  
  public Object get(int index) {
    if ((index < 0) || (index >= tableRows.size())) {
      return null;
    }
    return tableRows.get(String.valueOf(index));
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
    
    update();
  }
  
  public void update() {
    calculateRows();
    fireTableDataChanged();
  }
  
  public boolean isPriceIncludesTax() {
    return priceIncludesTax;
  }
  
  public void setPriceIncludesTax(boolean priceIncludesTax) {
    this.priceIncludesTax = priceIncludesTax;
  }
  
  public void deleteFromTicket(TicketItem ticketItem) {
    if (ticket != null) {
      ticket.addDeletedItems(ticketItem);
    }
  }
}
