package com.floreantpos.ui.ticket;

public abstract interface TicketViewerTableChangeListener
{
  public abstract void ticketDataChanged();
}
