package com.floreantpos.ui.ticket;

import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.ext.KitchenStatus;
import com.floreantpos.util.NumberUtil;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;



















public class TicketViewerTableCellRenderer
  extends DefaultTableCellRenderer
{
  public static final Color SAVED_ITEM_COLOR = Color.decode("#FFFACD");
  private boolean inTicketScreen = false;
  MultiLineTableCellRenderer multiLineTableCellRenderer = new MultiLineTableCellRenderer();
  

  public TicketViewerTableCellRenderer() {}
  

  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    Component rendererComponent = null;
    
    TicketViewerTableModel model = (TicketViewerTableModel)table.getModel();
    Object object = model.get(row);
    if (column == 0) {
      if ((model.isOrganizeTable()) && ((object instanceof TicketItem))) {
        TicketItem ticketItem = (TicketItem)object;
        rendererComponent = multiLineTableCellRenderer.getTableCellRendererLabel(ticketItem, table, value, isSelected, hasFocus, row, column);
      }
      else {
        rendererComponent = multiLineTableCellRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      }
    }
    else {
      rendererComponent = super.getTableCellRendererComponent(table, value, isSelected, false, row, column);
      setHorizontalAlignment(4);
    }
    
    if ((!inTicketScreen) || (isSelected)) {
      return rendererComponent;
    }
    
    rendererComponent.setBackground(table.getBackground());
    
    if ((object instanceof ITicketItem)) {
      ITicketItem iTicketItem = (ITicketItem)object;
      if (((iTicketItem instanceof TicketItem)) && (((TicketItem)iTicketItem).getKitchenStatusValue() == KitchenStatus.BUMP)) {
        rendererComponent.setBackground(Color.GREEN);
      }
      else if (iTicketItem.isPrintedToKitchen().booleanValue()) {
        rendererComponent.setBackground(Color.YELLOW);
      }
      else if (iTicketItem.isSaved()) {
        rendererComponent.setBackground(SAVED_ITEM_COLOR);
      }
    }
    return rendererComponent;
  }
  
  protected void setValue(Object value)
  {
    if (value == null) {
      setText("");
      return;
    }
    String text = value.toString();
    
    if (((value instanceof Double)) || ((value instanceof Float))) {
      text = NumberUtil.formatNumberAcceptNegative(Double.valueOf(((Number)value).doubleValue()));
    }
    setText(text);
  }
  
  public boolean isInTicketScreen() {
    return inTicketScreen;
  }
  
  public void setInTicketScreen(boolean inTicketScreen) {
    this.inTicketScreen = inTicketScreen;
  }
}
