package com.floreantpos.ui.ticket;

import com.floreantpos.Messages;
import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketDiscount;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemCookingInstruction;
import com.floreantpos.model.TicketItemDiscount;
import com.floreantpos.model.TicketItemModifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import org.apache.commons.lang.StringUtils;

















public class TicketViewerTableModel
  extends AbstractTableModel
{
  private final JTable table;
  protected Ticket ticket;
  private double previousFractionalItemQuantity;
  protected final HashMap<String, ITicketItem> tableRows = new LinkedHashMap();
  
  private boolean priceIncludesTax = false;
  
  private TicketViewerTableChangeListener listener;
  protected String[] columnNames = { Messages.getString("TicketViewerTableModel.0"), Messages.getString("TicketViewerTableModel.3") };
  private boolean forReciptPrint;
  private boolean printCookingInstructions;
  private boolean editable;
  private boolean organizeTable;
  
  public TicketViewerTableModel(JTable table)
  {
    this(table, null);
  }
  
  public TicketViewerTableModel(JTable table, Ticket ticket) {
    this.table = table;
    setTicket(ticket);
  }
  
  public int getItemCount() {
    return tableRows.size();
  }
  
  public int getRowCount()
  {
    int size = tableRows.size();
    return size;
  }
  
  public int getActualRowCount() {
    return tableRows.size();
  }
  
  public int getColumnCount()
  {
    return columnNames.length;
  }
  
  public String getColumnName(int column)
  {
    return columnNames[column];
  }
  
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    ITicketItem ticketItem = (ITicketItem)tableRows.get(String.valueOf(rowIndex));
    
    if (ticketItem == null) {
      return null;
    }
    
    switch (columnIndex) {
    case 0: 
      return ticketItem.getNameDisplay();
    





    case 1: 
      return ticketItem.getSubTotalAmountDisplay();
    }
    
    return null;
  }
  
  protected void calculateRows() {
    TicketItemRowCreator.calculateTicketRows(ticket, tableRows, true);
  }
  
  public int addTicketItem(TicketItem ticketItemToAdd) {
    if (ticketItemToAdd.isHasModifiers().booleanValue()) {
      return addTicketItemToTicket(ticketItemToAdd);
    }
    
    Object[] values = tableRows.values().toArray();
    if ((values == null) || (values.length == 0)) {
      previousFractionalItemQuantity = ticketItemToAdd.getQuantity().doubleValue();
      return addTicketItemToTicket(ticketItemToAdd);
    }
    
    Object object = values[(values.length - 1)];
    if ((object instanceof TicketItem)) {
      TicketItem item = (TicketItem)object;
      
      String menuItemId = ticketItemToAdd.getMenuItemId();
      if ((StringUtils.isEmpty(menuItemId)) || (menuItemId.equals("0"))) {
        return addTicketItemToTicket(ticketItemToAdd);
      }
      
      if ((menuItemId.equals(item.getMenuItemId())) && (item.getQuantity().doubleValue() > 0.0D) && (!item.isComboItem().booleanValue()) && 
        (!item.isPrintedToKitchen().booleanValue()) && 
        (item.getUnitPrice().equals(ticketItemToAdd.getUnitPrice())))
      {
        previousFractionalItemQuantity = item.getQuantity().doubleValue();
        if (ticketItemToAdd.isFractionalUnit().booleanValue()) {
          item.setFractionalUnit(Boolean.valueOf(true));
          item.setQuantity(Double.valueOf(previousFractionalItemQuantity + ticketItemToAdd.getQuantity().doubleValue()));
          
          previousFractionalItemQuantity = item.getQuantity().doubleValue();
        }
        else {
          item.setQuantity(Double.valueOf(item.getQuantity().doubleValue() + 1.0D));
        }
        fireTableRowsUpdated(values.length - 1, values.length - 1);
        return values.length - 1;
      }
    }
    previousFractionalItemQuantity = ticketItemToAdd.getQuantity().doubleValue();
    return addTicketItemToTicket(ticketItemToAdd);
  }
  
  private int addTicketItemToTicket(TicketItem ticketItem) {
    int oldRowCount = tableRows.size();
    ticket.addToticketItems(ticketItem);
    calculateRows();
    int newRowCount = tableRows.size();
    
    fireTableRowsInserted(oldRowCount, newRowCount);
    return newRowCount - 1;
  }
  
  public int addTicketDiscount(TicketDiscount ticketDiscount) {
    int oldRowCount = tableRows.size();
    ticket.addTodiscounts(ticketDiscount);
    calculateRows();
    int newRowCount = tableRows.size();
    
    fireTableRowsInserted(oldRowCount, newRowCount);
    return newRowCount - 1;
  }
  
  public void addAllTicketItem(TicketItem ticketItem) {
    if (ticketItem.isHasModifiers().booleanValue()) {
      List<TicketItem> ticketItems = ticket.getTicketItems();
      ticketItems.add(ticketItem);
      
      calculateRows();
      fireTableDataChanged();
    }
    else {
      List<TicketItem> ticketItems = ticket.getTicketItems();
      boolean exists = false;
      for (TicketItem item : ticketItems) {
        if (item.getName().equals(ticketItem.getName())) {
          double itemCount = item.getQuantity().doubleValue();
          itemCount += ticketItem.getQuantity().doubleValue();
          item.setQuantity(Double.valueOf(itemCount));
          exists = true;
          table.repaint();
          return;
        }
      }
      if (!exists) {
        ticket.addToticketItems(ticketItem);
        calculateRows();
        fireTableDataChanged();
      }
    }
  }
  
  public boolean containsTicketItem(TicketItem ticketItem) {
    if (ticketItem.isHasModifiers().booleanValue()) {
      return false;
    }
    List<TicketItem> ticketItems = ticket.getTicketItems();
    for (TicketItem item : ticketItems) {
      if (item.getName().equals(ticketItem.getName())) {
        return true;
      }
    }
    return false;
  }
  
  public void removeModifier(TicketItem parent, TicketItemModifier modifierToDelete) {
    List<TicketItemModifier> ticketItemModifiers = parent.getTicketItemModifiers();
    
    for (Iterator iter = ticketItemModifiers.iterator(); iter.hasNext();) {
      TicketItemModifier modifier = (TicketItemModifier)iter.next();
      if (modifier.getItemId() == modifierToDelete.getItemId()) {
        iter.remove();
        
        if (modifier.isPrintedToKitchen().booleanValue()) {
          ticket.addDeletedItems(modifier);
        }
        
        calculateRows();
        fireTableDataChanged();
        return;
      }
    }
  }
  
  public Object delete(int index) {
    if ((index < 0) || (index >= tableRows.size())) {
      return null;
    }
    Object object = tableRows.get(String.valueOf(index));
    int rowNum; Iterator iter; if ((object instanceof TicketItem)) {
      TicketItem ticketItem = (TicketItem)object;
      rowNum = ticketItem.getTableRowNum();
      
      List<TicketItem> ticketItems = ticket.getTicketItems();
      for (iter = ticketItems.iterator(); iter.hasNext();) {
        TicketItem item = (TicketItem)iter.next();
        if (item.getTableRowNum() == rowNum) {
          iter.remove();
          
          if ((!item.isPrintedToKitchen().booleanValue()) && (item.getInventoryAdjustQty().doubleValue() <= 0.0D)) break;
          ticket.addDeletedItems(item); break;
        }
      }
    }
    else {
      TicketItemModifier itemModifier;
      Iterator iterator;
      if ((object instanceof TicketItemModifier)) {
        itemModifier = (TicketItemModifier)object;
        TicketItem ticketItem = itemModifier.getTicketItem();
        List<TicketItemModifier> ticketItemModifiers = ticketItem.getTicketItemModifiers();
        
        if (ticketItemModifiers != null) {
          for (iterator = ticketItemModifiers.iterator(); iterator.hasNext();) {
            TicketItemModifier element = (TicketItemModifier)iterator.next();
            if (itemModifier.getTableRowNum() == element.getTableRowNum()) {
              iterator.remove();
              
              if (element.isPrintedToKitchen().booleanValue()) {
                ticket.addDeletedItems(element);
              }
            }
          }
        }
      }
      else if ((object instanceof TicketItemCookingInstruction)) {
        TicketItemCookingInstruction cookingInstruction = (TicketItemCookingInstruction)object;
        int tableRowNum = cookingInstruction.getTableRowNum();
        
        TicketItem ticketItem = null;
        while (tableRowNum > 0) {
          Object object2 = tableRows.get(String.valueOf(--tableRowNum));
          if ((object2 instanceof TicketItem)) {
            ticketItem = (TicketItem)object2;
            break;
          }
        }
        
        if (ticketItem != null) {
          ticketItem.removeCookingInstruction(cookingInstruction);
        }
      }
      else if ((object instanceof TicketItemDiscount)) {
        TicketItemDiscount ticketItemDiscount = (TicketItemDiscount)object;
        int tableRowNum = ticketItemDiscount.getTableRowNum();
        
        TicketItem ticketItem = null;
        while (tableRowNum > 0) {
          Object object2 = tableRows.get(String.valueOf(--tableRowNum));
          if ((object2 instanceof TicketItem)) {
            ticketItem = (TicketItem)object2;
            break;
          }
        }
        
        if (ticketItem != null) {
          ticketItem.removeTicketItemDiscount(ticketItemDiscount);
          if (listener != null)
            listener.ticketDataChanged();
        }
      }
    }
    calculateRows();
    fireTableRowsDeleted(index, index);
    return object;
  }
  
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    if ((editable) && (columnIndex == 0))
      return ((get(rowIndex) instanceof TicketItemDiscount)) || (organizeTable);
    return false;
  }
  
  public void removeAll() {
    if (tableRows.size() <= 0) {
      return;
    }
    ticket.getTicketItems().clear();
    tableRows.clear();
    fireTableDataChanged();
  }
  
  public Object get(int index)
  {
    if ((index < 0) || (index >= tableRows.size())) {
      return null;
    }
    return tableRows.get(String.valueOf(index));
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
    if ((ticket != null) && (ticket.getOrderType() != null))
      organizeTable = ticket.getOrderType().isEnableCourse().booleanValue();
    update();
  }
  
  public void update() {
    calculateRows();
    fireTableDataChanged();
  }
  
  public boolean isForReciptPrint() {
    return forReciptPrint;
  }
  
  public void setForReciptPrint(boolean forReciptPrint) {
    this.forReciptPrint = forReciptPrint;
  }
  
  public boolean isPrintCookingInstructions() {
    return printCookingInstructions;
  }
  
  public void setPrintCookingInstructions(boolean printCookingInstructions) {
    this.printCookingInstructions = printCookingInstructions;
  }
  
  public boolean isPriceIncludesTax() {
    return priceIncludesTax;
  }
  
  public void setPriceIncludesTax(boolean priceIncludesTax) {
    this.priceIncludesTax = priceIncludesTax;
  }
  
  public void setEditable(boolean editable) {
    this.editable = editable;
  }
  
  public void addTicketDataChangeListener(TicketViewerTableChangeListener listener) {
    this.listener = listener;
  }
  
  public boolean isOrganizeTable() {
    return organizeTable;
  }
}
