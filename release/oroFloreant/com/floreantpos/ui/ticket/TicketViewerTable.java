package com.floreantpos.ui.ticket;

import com.floreantpos.IconFactory;
import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketDiscount;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemDiscount;
import com.floreantpos.model.TicketItemModifier;
import com.floreantpos.swing.ButtonColumn;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.views.order.actions.TicketEditListener;
import java.awt.Color;
import java.awt.Component;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

















public class TicketViewerTable
  extends JTable
  implements ComponentListener
{
  private TicketViewerTableModel model;
  private final DefaultListSelectionModel selectionModel;
  private final TicketViewerTableCellRenderer cellRenderer;
  public int rowTobeSelectedOnResize = -1;
  private boolean editable;
  private ArrayList<TicketEditListener> ticketEditListenerList;
  
  public TicketViewerTable() {
    this(null);
    addComponentListener(this);
  }
  
  public TicketViewerTable(Ticket ticket) {
    model = new TicketViewerTableModel(this);
    setModel(model);
    
    selectionModel = new DefaultListSelectionModel();
    selectionModel.setSelectionMode(0);
    setGridColor(Color.LIGHT_GRAY);
    setSelectionModel(selectionModel);
    setCellSelectionEnabled(false);
    setColumnSelectionAllowed(false);
    setRowSelectionAllowed(true);
    
    setAutoscrolls(true);
    setShowGrid(true);
    setBorder(null);
    setFocusable(false);
    
    cellRenderer = new TicketViewerTableCellRenderer();
    resizeTableColumns();
    setTicket(ticket);
    
    ticketEditListenerList = new ArrayList();
  }
  
  public TableCellRenderer getCellRenderer(int row, int column)
  {
    if ((editable) && (column == 0) && ((model.get(row) instanceof TicketItemDiscount)))
      return super.getCellRenderer(row, column);
    return cellRenderer;
  }
  
  public TicketViewerTableCellRenderer getRenderer() {
    return cellRenderer;
  }
  
  private boolean isTicketNull() {
    Ticket ticket = getTicket();
    if (ticket == null) {
      return true;
    }
    if (ticket.getTicketItems() == null) {
      return true;
    }
    return false;
  }
  
  public void scrollUp() {
    if (isTicketNull()) {
      return;
    }
    int selectedRow = getSelectedRow();
    
    if (selectedRow == 0) {
      return;
    }
    
    selectedRow--;
    
    selectionModel.addSelectionInterval(selectedRow, selectedRow);
    Rectangle cellRect = getCellRect(selectedRow, 0, false);
    scrollRectToVisible(cellRect);
  }
  
  public void scrollUpTen() {
    if (isTicketNull()) {
      return;
    }
    




    int selectedRow = getSelectedRow();
    
    if (selectedRow == 0) {
      return;
    }
    if (selectedRow - 9 < 0) {
      selectedRow = 0;
    }
    else {
      selectedRow -= 9;
    }
    
    selectionModel.addSelectionInterval(selectedRow, selectedRow);
    Rectangle cellRect = getCellRect(selectedRow, 0, false);
    scrollRectToVisible(cellRect);
  }
  
  public void scrollDown() {
    if (isTicketNull()) {
      return;
    }
    int selectedRow = getSelectedRow();
    if (selectedRow >= model.getItemCount() - 1) {
      return;
    }
    
    selectedRow++;
    selectionModel.addSelectionInterval(selectedRow, selectedRow);
    Rectangle cellRect = getCellRect(selectedRow, 0, false);
    scrollRectToVisible(cellRect);
  }
  
  public void scrollDownTen() {
    if (isTicketNull())
      return;
    int selectedRow = getSelectedRow();
    if (selectedRow > model.getItemCount() - 9) {
      selectedRow = model.getRowCount() - 1;
    }
    else
    {
      selectedRow += 9;
    }
    selectionModel.addSelectionInterval(selectedRow, selectedRow);
    Rectangle cellRect = getCellRect(selectedRow, 0, false);
    scrollRectToVisible(cellRect);
  }
  
  public void increaseItemAmount(TicketItem ticketItem) {
    double itemCount = ticketItem.getQuantity().doubleValue();
    ticketItem.setQuantity(Double.valueOf(++itemCount));
    fireTicketItemUpdated(getTicket(), ticketItem);
    repaint();
  }
  
  public boolean increaseFractionalUnit(double selectedQuantity) {
    int selectedRow = getSelectedRow();
    if (selectedRow < 0) {
      return false;
    }
    if (selectedRow >= model.getItemCount()) {
      return false;
    }
    
    Object object = model.get(selectedRow);
    if ((object instanceof TicketItem)) {
      TicketItem ticketItem = (TicketItem)object;
      ticketItem.setQuantity(Double.valueOf(selectedQuantity));
      return true;
    }
    return false;
  }
  
  public boolean increaseItemAmount() {
    int selectedRow = getSelectedRow();
    if (selectedRow < 0) {
      return false;
    }
    if (selectedRow >= model.getItemCount()) {
      return false;
    }
    
    ITicketItem iTicketItem = (ITicketItem)model.get(selectedRow);
    if (iTicketItem.isPrintedToKitchen().booleanValue()) {
      return false;
    }
    
    if ((iTicketItem instanceof TicketItem)) {
      TicketItem ticketItem = (TicketItem)iTicketItem;
      double itemCount = ticketItem.getQuantity().doubleValue();
      ticketItem.setQuantity(Double.valueOf(++itemCount));
      if (ticketItem.isHasModifiers().booleanValue()) {
        List<TicketItemModifier> ticketItemModifiers = ticketItem.getTicketItemModifiers();
        for (TicketItemModifier ticketItemModifier : ticketItemModifiers) {
          ticketItemModifier.setItemQuantity(Double.valueOf(ticketItemModifier.getItemQuantity().doubleValue() + 1.0D));
        }
      }
      return true;
    }
    return false;
  }
  
  public boolean decreaseItemAmount() {
    int selectedRow = getSelectedRow();
    if (selectedRow < 0) {
      return false;
    }
    if (selectedRow >= model.getItemCount()) {
      return false;
    }
    
    ITicketItem iTicketItem = (ITicketItem)model.get(selectedRow);
    if (iTicketItem.isPrintedToKitchen().booleanValue()) {
      return false;
    }
    if ((iTicketItem instanceof TicketItem)) {
      TicketItem ticketItem = (TicketItem)iTicketItem;
      double itemCount = ticketItem.getQuantity().doubleValue();
      if (itemCount <= 1.0D) {
        return false;
      }
      
      ticketItem.setQuantity(Double.valueOf(--itemCount));
      fireTicketItemUpdated(getTicket(), ticketItem);
      if (ticketItem.isHasModifiers().booleanValue()) {
        List<TicketItemModifier> ticketItemModifiers = ticketItem.getTicketItemModifiers();
        for (TicketItemModifier ticketItemModifier : ticketItemModifiers) {
          ticketItemModifier.setItemQuantity(Double.valueOf(ticketItemModifier.getItemQuantity().doubleValue() - 1.0D));
        }
      }
      return true;
    }
    return false;
  }
  
  public void setTicket(Ticket ticket) {
    model.setTicket(ticket);
  }
  
  public Ticket getTicket() {
    return model.getTicket();
  }
  
  public void addTicketItem(TicketItem ticketItem) {
    ticketItem.setTicket(getTicket());
    rowTobeSelectedOnResize = model.addTicketItem(ticketItem);
    fireTicketItemUpdated(getTicket(), ticketItem);
  }
  
  public void addTicketDiscount(TicketDiscount ticketDiscount) {
    model.addTicketDiscount(ticketDiscount);
  }
  
  public Object deleteSelectedItem() {
    int selectedRow = getSelectedRow();
    Object delete = model.delete(selectedRow);
    rowTobeSelectedOnResize = -1;
    if ((delete instanceof TicketItem)) {
      fireTicketItemUpdated(getTicket(), (TicketItem)delete);
    }
    return delete;
  }
  
  public boolean containsTicketItem(TicketItem ticketItem) {
    return model.containsTicketItem(ticketItem);
  }
  
  public void delete(int index) {
    model.delete(index);
  }
  
  public ITicketItem get(int index) {
    return (ITicketItem)model.get(index);
  }
  
  public ITicketItem getSelected() {
    int index = getSelectedRow();
    
    return (ITicketItem)model.get(index);
  }
  
  public void addAllTicketItem(TicketItem ticketItem) {
    model.addAllTicketItem(ticketItem);
  }
  
  public void removeModifier(TicketItem parent, TicketItemModifier modifier) {
    model.removeModifier(parent, modifier);
  }
  
  public void updateView() {
    int selectedRow = getSelectedRow();
    
    model.update();
    try
    {
      getSelectionModel().setSelectionInterval(selectedRow, selectedRow);
    }
    catch (Exception localException) {}
  }
  
  public int getActualRowCount()
  {
    return model.getActualRowCount();
  }
  
  public void selectLast() {
    int actualRowCount = getActualRowCount() - 1;
    selectionModel.addSelectionInterval(actualRowCount, actualRowCount);
    Rectangle cellRect = getCellRect(actualRowCount, 0, true);
    scrollRectToVisible(cellRect);
  }
  
  public void selectRow(int index) {
    if ((index < 0) || (index >= getActualRowCount())) {
      index = 0;
    }
    selectionModel.addSelectionInterval(index, index);
    Rectangle cellRect = getCellRect(index, 0, true);
    scrollRectToVisible(cellRect);
  }
  
  public TicketViewerTableModel getModel()
  {
    return model;
  }
  
  private List<TicketItem> getRowByValue(TicketViewerTableModel model)
  {
    List<TicketItem> ticketItems = new ArrayList();
    for (int i = 0; i <= model.getRowCount(); i++) {
      Object value = model.get(i);
      if ((value instanceof TicketItem)) {
        TicketItem ticketItem = (TicketItem)value;
        ticketItems.add(ticketItem);
      }
    }
    
    return ticketItems;
  }
  
  public List<TicketItem> getTicketItems() {
    return getRowByValue(model);
  }
  
  public TicketItem getTicketItem() {
    return (TicketItem)getSelected();
  }
  
  public void setModel(TicketViewerTableModel dataModel) {
    super.setModel(dataModel);
    model = dataModel;
    revalidate();
  }
  
  private void resizeTableColumns() {
    setAutoResizeMode(4);
    
    setColumnWidth(1, PosUIManager.getSize(80));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  protected void resizeAndRepaint()
  {
    if (getIgnoreRepaint()) {
      return;
    }
    super.resizeAndRepaint();
  }
  
  public void componentResized(ComponentEvent e)
  {
    if (rowTobeSelectedOnResize != -1) {
      selectRow(rowTobeSelectedOnResize);
    }
  }
  


  public void componentMoved(ComponentEvent e) {}
  


  public void componentShown(ComponentEvent e) {}
  


  public void componentHidden(ComponentEvent e) {}
  


  public void setVisibleDeleteButton(int cloumn)
  {
    editable = true;
    model.setEditable(editable);
    AbstractAction action = new AbstractAction()
    {
      public void actionPerformed(ActionEvent e)
      {
        int row = Integer.parseInt(e.getActionCommand());
        ITicketItem item = (ITicketItem)model.get(row);
        if ((item instanceof TicketItemDiscount))
          model.delete(row);
      }
    };
    ButtonColumn coloum = new ButtonColumn(this, action, cloumn)
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JPanel panel = (JPanel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        int verticalGap = PosUIManager.getSize(15);
        int horizontalGap = PosUIManager.getSize(2);
        ITicketItem item = (ITicketItem)model.get(row);
        PosButton button = (PosButton)panel.getComponent(0);
        button.setOpaque(false);
        button.setBorder(new EmptyBorder(verticalGap, horizontalGap, verticalGap, horizontalGap));
        if ((item instanceof TicketItemDiscount)) {
          button.setIcon(IconFactory.getIcon("/ui_icons/", "delete-icon.png"));
        }
        else {
          button.setIcon(null);
        }
        table.setRowHeight(row, table.getRowHeight(0));
        return panel;
      }
      
      public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
      {
        return super.getTableCellEditorComponent(table, value, false, row, column);
      }
    };
    coloum.showColumnValueInLabel(true);
  }
  
  public void addTicketUpdateListener(TicketEditListener l) {
    ticketEditListenerList.add(l);
  }
  
  public void removeTicketUpdateListener(TicketEditListener l) {
    ticketEditListenerList.remove(l);
  }
  
  public void fireTicketItemUpdated(Ticket ticket, TicketItem ticketItem) {
    for (TicketEditListener listener : ticketEditListenerList) {
      listener.itemAdded(ticket, ticketItem);
    }
  }
}
