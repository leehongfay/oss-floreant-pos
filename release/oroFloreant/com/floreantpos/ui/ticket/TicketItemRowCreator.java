package com.floreantpos.ui.ticket;

import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemCookingInstruction;
import com.floreantpos.model.TicketItemDiscount;
import com.floreantpos.model.TicketItemModifier;
import java.util.List;
import java.util.Map;


















public class TicketItemRowCreator
{
  public TicketItemRowCreator() {}
  
  public static void calculateTicketRows(Ticket ticket, Map<String, ITicketItem> tableRows)
  {
    calculateTicketRows(ticket, tableRows, true, true, true);
  }
  
  public static void calculateTicketRows(Ticket ticket, Map<String, ITicketItem> tableRows, boolean includeVoidItems) {
    calculateTicketRows(ticket, tableRows, true, true, true, includeVoidItems);
  }
  
  public static void calculateTicketRows(Ticket ticket, Map<String, ITicketItem> tableRows, boolean includeModifiers, boolean includeCookingInstructions) {
    calculateTicketRows(ticket, tableRows, true, true, true);
  }
  
  public static void calculateTicketRows(Ticket ticket, Map<String, ITicketItem> tableRows, boolean includeModifiers, boolean includeCookingInstructions, boolean includeDiscounts)
  {
    calculateTicketRows(ticket, tableRows, includeModifiers, includeCookingInstructions, includeDiscounts, true);
  }
  
  public static void calculateTicketRows(Ticket ticket, Map<String, ITicketItem> tableRows, boolean includeModifiers, boolean includeCookingInstructions, boolean includeDiscounts, boolean includeVoidItems)
  {
    tableRows.clear();
    
    int rowNum = 0;
    
    if ((ticket == null) || (ticket.getTicketItems() == null)) {
      return;
    }
    List<TicketItem> ticketItems = ticket.getTicketItems();
    for (TicketItem ticketItem : ticketItems) {
      if ((includeVoidItems) || (!ticketItem.isVoided().booleanValue()))
      {

        ticketItem.setTableRowNum(rowNum);
        tableRows.put(String.valueOf(rowNum), ticketItem);
        rowNum++;
        
        if (includeModifiers) {
          rowNum = includeModifiers(ticketItem, tableRows, rowNum, false);
        }
        
        if (includeDiscounts) {
          rowNum = includeDiscounts(ticketItem, tableRows, rowNum);
        }
        
        if (includeCookingInstructions) {
          rowNum = includeCookintInstructions(ticketItem, tableRows, rowNum);
        }
      }
    }
  }
  


















  private static int includeCookintInstructions(TicketItem ticketItem, Map<String, ITicketItem> tableRows, int rowNum)
  {
    List<TicketItemCookingInstruction> cookingInstructions = ticketItem.getCookingInstructions();
    if (cookingInstructions != null) {
      for (TicketItemCookingInstruction ticketItemCookingInstruction : cookingInstructions) {
        ticketItemCookingInstruction.setTableRowNum(rowNum);
        tableRows.put(String.valueOf(rowNum), ticketItemCookingInstruction);
        rowNum++;
      }
    }
    return rowNum;
  }
  
  private static int includeDiscounts(TicketItem ticketItem, Map<String, ITicketItem> tableRows, int rowNum) {
    List<TicketItemDiscount> discounts = ticketItem.getDiscounts();
    if (discounts != null) {
      for (TicketItemDiscount ticketItemDiscount : discounts)
        if (ticketItemDiscount.getType().intValue() != 2)
        {

          ticketItemDiscount.setTableRowNum(rowNum);
          tableRows.put(String.valueOf(rowNum), ticketItemDiscount);
          rowNum++;
        }
    }
    return rowNum;
  }
  
  private static int includeModifiers(TicketItem ticketItem, Map<String, ITicketItem> tableRows, int rowNum, boolean kitchenPrint) {
    List<TicketItemModifier> ticketItemModifiers = ticketItem.getTicketItemModifiers();
    if ((ticketItemModifiers != null) && (ticketItemModifiers.size() > 0))
    {



      for (TicketItemModifier itemModifier : ticketItemModifiers)
      {
        if ((!kitchenPrint) || ((!itemModifier.isPrintedToKitchen().booleanValue()) && (itemModifier.isShouldPrintToKitchen().booleanValue())))
        {


          itemModifier.setTableRowNum(rowNum);
          tableRows.put(String.valueOf(rowNum), itemModifier);
          rowNum++;
        } }
    }
    return rowNum;
  }
}
