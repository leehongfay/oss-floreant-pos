package com.floreantpos.ui.ticket;

import com.floreantpos.model.Ticket;
import com.floreantpos.model.VoidItem;
import java.awt.Color;
import java.awt.Rectangle;
import java.util.List;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JTable;

public class VoidItemViewerTable extends JTable
{
  private VoidItemViewerTableModel model;
  private DefaultListSelectionModel selectionModel;
  
  public VoidItemViewerTable()
  {
    this(null);
  }
  
  public VoidItemViewerTable(Ticket ticket) {
    model = new VoidItemViewerTableModel(this);
    setModel(model);
    
    selectionModel = new DefaultListSelectionModel();
    selectionModel.setSelectionMode(0);
    
    setGridColor(Color.LIGHT_GRAY);
    setSelectionModel(selectionModel);
    setAutoscrolls(true);
    setShowGrid(true);
    setBorder(null);
    
    setFocusable(false);
    
    setRowHeight(50);
    resizeTableColumns();
  }
  
  private void resizeTableColumns()
  {
    setAutoResizeMode(4);
  }
  
  public List<VoidItem> getVoidItems() {
    return model.getItems();
  }
  
  public void addVoidItem(VoidItem voidItem) {
    model.add(voidItem);
  }
  
  public void deleteSelectedItem() {
    int selectedRow = getSelectedRow();
    model.remove(selectedRow);
  }
  
  public void updateView()
  {
    int selectedRow = getSelectedRow();
    
    model.update();
    try
    {
      getSelectionModel().setSelectionInterval(selectedRow, selectedRow);
    }
    catch (Exception localException) {}
  }
  
  private boolean isVoidItemNull() {
    List<VoidItem> itemList = getVoidItems();
    if (itemList == null) {
      return true;
    }
    return false;
  }
  
  public void scrollUp() {
    if (isVoidItemNull()) {
      return;
    }
    int selectedRow = getSelectedRow();
    int rowCount = model.getItems().size();
    
    if (selectedRow > rowCount - 1) {
      return;
    }
    
    selectedRow--;
    if (selectedRow < 0) {
      selectedRow = 0;
    }
    
    selectionModel.addSelectionInterval(selectedRow, selectedRow);
    Rectangle cellRect = getCellRect(selectedRow, 0, false);
    scrollRectToVisible(cellRect);
  }
  
  public void scrollDown() {
    if (isVoidItemNull()) {
      return;
    }
    int selectedRow = getSelectedRow();
    if (selectedRow >= model.getRowCount() - 1) {
      return;
    }
    
    selectedRow++;
    
    selectionModel.addSelectionInterval(selectedRow, selectedRow);
    Rectangle cellRect = getCellRect(selectedRow, 0, false);
    scrollRectToVisible(cellRect);
  }
  
  public VoidItem get(int index) {
    return model.get(index);
  }
  
  public VoidItem getSelected() {
    int index = getSelectedRow();
    
    return model.get(index);
  }
  
  public void delete(int index) {
    model.remove(index);
  }
  
  public void removeItem(VoidItem voidItem) {
    model.remove(voidItem);
  }
  
  public void updateVoidItem(VoidItem voidItem) {
    model.updateVoidItem(voidItem);
  }
  
  public boolean containsVoidItem(VoidItem voidItem) {
    return model.containsVoidItem(voidItem);
  }
}
