package com.floreantpos.ui.ticket;

import com.floreantpos.model.TicketItem;
import com.floreantpos.swing.PosUIManager;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class MultiLineTableCellRenderer extends javax.swing.JTextArea implements TableCellRenderer
{
  private JPanel panel = new JPanel(new BorderLayout());
  private JLabel lblIcon;
  
  public MultiLineTableCellRenderer() {
    setOpaque(true);
    setLineWrap(true);
    setWrapStyleWord(true);
    setSize(new Dimension(200, 20));
    setText("----");
    
    int verticalGap = PosUIManager.getSize(15);
    int horizontalGap = PosUIManager.getSize(2);
    setBorder(new EmptyBorder(verticalGap, horizontalGap, verticalGap, horizontalGap));
    
    panel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    panel.setOpaque(true);
    
    lblIcon = new JLabel();
    lblIcon.setHorizontalAlignment(2);
    lblIcon.setOpaque(false);
  }
  
  public Component getTableCellRendererLabel(TicketItem item, JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component c = getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    Icon icon = item.getCourseIcon();
    if (icon == null)
      return c;
    if (!isSelected) {
      if (item.isPrintedToKitchen().booleanValue()) {
        c.setBackground(Color.YELLOW);
      }
      else if (item.isSaved()) {
        c.setBackground(TicketViewerTableCellRenderer.SAVED_ITEM_COLOR);
      }
    }
    lblIcon.setIcon(icon);
    Dimension preferredSize = c.getPreferredSize();
    int padding = (table.getRowHeight() - icon.getIconHeight()) / 2;
    lblIcon.setBorder(BorderFactory.createEmptyBorder(padding, 5, padding, 5));
    panel.setBackground(c.getBackground());
    panel.setForeground(c.getForeground());
    panel.setOpaque(true);
    panel.add(lblIcon, "West");
    panel.setPreferredSize(preferredSize);
    panel.add(c);
    return panel;
  }
  
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    setFont(table.getFont());
    
    if (isSelected) {
      setBackground(table.getSelectionBackground());
      setForeground(table.getSelectionForeground());
    }
    else {
      setBackground(table.getBackground());
      setForeground(table.getForeground());
    }
    if (column == 0) {
      int colWidth = table.getColumnModel().getColumn(column).getWidth();
      Dimension size = getSize();
      if (colWidth != width) {
        width = colWidth;
        setSize(size);
      }
    }
    
    if (value != null) {
      setText(value.toString());
    }
    else {
      setText("");
    }
    
    if (column == 0)
    {
      int height = getPreferredSizeheight;
      if (table.getRowHeight(row) != height) {
        table.setRowHeight(row, height);
      }
    }
    

    return this;
  }
}
