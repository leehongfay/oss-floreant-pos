package com.floreantpos.ui.ticket;

import com.floreantpos.model.Ticket;
import com.floreantpos.model.VoidItem;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

public class VoidItemViewerTableModel
  extends AbstractTableModel
{
  protected VoidItem voidItem;
  protected final HashMap<String, VoidItem> tableRows = new LinkedHashMap();
  protected String[] columnNames = { "Item Name", "Void Reason", "Item Wasted", "Unit", "Tax", "Price" };
  List<VoidItem> voidItemList = new ArrayList();
  
  public VoidItemViewerTableModel(JTable table) {
    this(table, null);
  }
  
  public VoidItemViewerTableModel(JTable table, Ticket ticket) {}
  
  public VoidItem get(int index)
  {
    return (VoidItem)voidItemList.get(index);
  }
  
  public void add(VoidItem group) {
    if (voidItemList == null) {
      voidItemList = new ArrayList();
    }
    voidItemList.add(group);
    fireTableDataChanged();
  }
  
  public void remove(int index) {
    if (voidItemList == null) {
      return;
    }
    voidItemList.remove(index);
    fireTableDataChanged();
  }
  
  public List<VoidItem> getItems() {
    return voidItemList;
  }
  
  public void remove(VoidItem voidItem) {
    if (voidItemList == null) {
      return;
    }
    voidItemList.remove(voidItem);
    fireTableDataChanged();
  }
  
  public int getRowCount() {
    if (voidItemList == null) {
      return 0;
    }
    return voidItemList.size();
  }
  
  public int getColumnCount()
  {
    return columnNames.length;
  }
  
  public String getColumnName(int column)
  {
    return columnNames[column];
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    VoidItem voidItem = (VoidItem)voidItemList.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      return voidItem.getMenuItemName();
    
    case 1: 
      return voidItem.getVoidReason();
    
    case 2: 
      return voidItem.isItemWasted();
    
    case 3: 
      return voidItem.getQuantity();
    case 4: 
      return voidItem.getTaxAmount();
    case 5: 
      return voidItem.getTotalPrice();
    }
    return null;
  }
  
  public void updateVoidItem(VoidItem voidItem) {
    VoidItem voidItemData = null;
    for (Iterator iterator = voidItemList.iterator(); iterator.hasNext();) {
      voidItemData = (VoidItem)iterator.next();
      
      if (voidItemData.getMenuItemId() == voidItem.getMenuItemId()) {
        voidItemData.setQuantity(Double.valueOf(voidItemData.getQuantity().doubleValue() + voidItem.getQuantity().doubleValue()));
      }
    }
    
    if (voidItemData != null) {
      voidItemList.clear();
      voidItemList.add(voidItemData);
    }
    fireTableDataChanged();
  }
  
  public boolean containsVoidItem(VoidItem voidItem)
  {
    for (VoidItem item : voidItemList) {
      if (item.getId() == voidItem.getId()) {
        return true;
      }
    }
    return false;
  }
  
  public void update() {
    fireTableDataChanged();
  }
}
