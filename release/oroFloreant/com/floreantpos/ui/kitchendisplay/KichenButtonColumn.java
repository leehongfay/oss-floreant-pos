package com.floreantpos.ui.kitchendisplay;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.AbstractCellEditor;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class KichenButtonColumn extends AbstractCellEditor implements TableCellRenderer, TableCellEditor, ActionListener, MouseListener
{
  private JTable table;
  private Action action;
  private int mnemonic;
  private Border focusBorder;
  private KitchenActionButton renderButton;
  private KitchenActionButton editButton;
  private Object editorValue;
  private boolean isButtonColumnEditor;
  
  public KichenButtonColumn(JTable table, Action action, int column)
  {
    this(table, action, column, null, null);
  }
  
  public KichenButtonColumn(JTable table, Action action, int column, KitchenActionButton renderButton, KitchenActionButton editButton) {
    this.table = table;
    this.action = action;
    
    if (renderButton == null) {
      renderButton = new KitchenActionButton("", 1, Color.yellow, Color.black);
    }
    this.renderButton = renderButton;
    if (editButton == null) {
      editButton = new KitchenActionButton("", 1, Color.yellow, Color.black);
    }
    this.editButton = editButton;
    editButton.setFocusPainted(false);
    editButton.addActionListener(this);
    
    TableColumnModel columnModel = table.getColumnModel();
    columnModel.getColumn(column).setCellRenderer(this);
    columnModel.getColumn(column).setCellEditor(this);
    table.addMouseListener(this);
  }
  




  public Border getFocusBorder()
  {
    return focusBorder;
  }
  





  public int getMnemonic()
  {
    return mnemonic;
  }
  




  public void setMnemonic(int mnemonic)
  {
    this.mnemonic = mnemonic;
    renderButton.setMnemonic(mnemonic);
    editButton.setMnemonic(mnemonic);
  }
  
  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
  {
    if (value == null) {
      editButton.setText("");
      editButton.setIcon(null);
    }
    else if ((value instanceof Icon)) {
      editButton.setText("");
      editButton.setIcon((Icon)value);
    }
    else {
      editButton.setText(value.toString());
      editButton.setIcon(null);
    }
    
    editorValue = value;
    return editButton;
  }
  
  public Object getCellEditorValue()
  {
    return editorValue;
  }
  


  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    if (isSelected) {
      renderButton.setForeground(table.getSelectionForeground());
      renderButton.setBackground(table.getSelectionBackground());
      renderButton.setSelected(true);
    }
    else {
      renderButton.setSelected(false);
    }
    







    if (value == null) {
      renderButton.setText("");
      renderButton.setIcon(null);
    }
    else if ((value instanceof Icon)) {
      renderButton.setText("");
      renderButton.setIcon((Icon)value);
    }
    else {
      renderButton.setText(value.toString());
      renderButton.setIcon(null);
    }
    
    return renderButton;
  }
  
  public JButton getRenderButton() {
    return renderButton;
  }
  





  public void actionPerformed(ActionEvent e)
  {
    int row = table.convertRowIndexToModel(table.getEditingRow());
    fireEditingStopped();
    


    ActionEvent event = new ActionEvent(table, 1001, "" + row);
    action.actionPerformed(event);
  }
  







  public void mousePressed(MouseEvent e)
  {
    if ((table.isEditing()) && (table.getCellEditor() == this))
      isButtonColumnEditor = true;
  }
  
  public void mouseReleased(MouseEvent e) {
    if ((isButtonColumnEditor) && (table.isEditing())) {
      table.getCellEditor().stopCellEditing();
    }
    isButtonColumnEditor = false;
  }
  
  public void mouseClicked(MouseEvent e) {}
  
  public void mouseEntered(MouseEvent e) {}
  
  public void mouseExited(MouseEvent e) {}
}
