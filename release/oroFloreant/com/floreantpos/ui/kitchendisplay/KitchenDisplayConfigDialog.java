package com.floreantpos.ui.kitchendisplay;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.config.AppConfig;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.NumericKeypad;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;



public class KitchenDisplayConfigDialog
  extends OkCancelOptionDialog
{
  private IntegerTextField tfTicketsPerPage = new IntegerTextField(25);
  private IntegerTextField tfWarningTimout = new IntegerTextField(8);
  private IntegerTextField tfOverTimout = new IntegerTextField(8);
  
  private JButton btnContainerBgColor = new JButton(POSConstants.BACKGROUND);
  private JButton btnInitialBgColor = new JButton(POSConstants.BACKGROUND);
  private JButton btnWarningBgColor = new JButton(POSConstants.BACKGROUND);
  private JButton btnOverBgColor = new JButton(POSConstants.BACKGROUND);
  
  private JButton btnContainerFgColor = new JButton(POSConstants.TEXT_COLOR);
  private JButton btnInitialFgColor = new JButton(POSConstants.TEXT_COLOR);
  private JButton btnWarningFgColor = new JButton(POSConstants.TEXT_COLOR);
  private JButton btnOverFgColor = new JButton(POSConstants.TEXT_COLOR);
  private JCheckBox chkHideItemBumpButton = new JCheckBox(Messages.getString("KitchenDisplayConfigDialog.0"));
  
  private String[] labels = { Messages.getString("KitchenDisplayConfigDialog.2"), Messages.getString("KitchenDisplayConfigDialog.3") };
  private JComboBox cbInitialColorComponent = new JComboBox(labels);
  private JComboBox cbWarningColorComponent = new JComboBox(labels);
  private JComboBox cbOverColorComponent = new JComboBox(labels);
  
  private JCheckBox chkShowBorder = new JCheckBox(Messages.getString("KitchenDisplayConfigDialog.4"));
  private JCheckBox chkShowHorizontalLines = new JCheckBox(Messages.getString("KitchenDisplayConfigDialog.6"));
  
  public KitchenDisplayConfigDialog() {
    String title = Messages.getString("KitchenDisplayConfigDialog.1");
    setTitle(title);
    setCaption(title);
    initComponents();
    updateView();
  }
  
  private void initComponents() {
    JPanel contentPanel = getContentPanel();
    contentPanel.setLayout(new MigLayout("wrap 2,ins 0 10 0 10", "[][grow]", ""));
    
    contentPanel.add(createLabel(Messages.getString("KitchenDisplayConfigDialog.5")));
    contentPanel.add(tfTicketsPerPage, "growx");
    
    contentPanel.add(createLabel(Messages.getString("KitchenDisplayConfigDialog.7")));
    contentPanel.add(tfWarningTimout, "split 4");
    

    contentPanel.add(createLabel(Messages.getString("KitchenDisplayConfigDialog.10")));
    contentPanel.add(tfOverTimout);
    contentPanel.add(createLabel("sec"));
    
    addButtonRow(contentPanel, "Kitchen Display", btnContainerBgColor, btnContainerFgColor, null);
    addButtonRow(contentPanel, Messages.getString("KitchenDisplayConfigDialog.13"), btnInitialBgColor, btnInitialFgColor, cbInitialColorComponent);
    addButtonRow(contentPanel, Messages.getString("KitchenDisplayConfigDialog.14"), btnWarningBgColor, btnWarningFgColor, cbWarningColorComponent);
    addButtonRow(contentPanel, Messages.getString("KitchenDisplayConfigDialog.15"), btnOverBgColor, btnOverFgColor, cbOverColorComponent);
    
    contentPanel.add(chkHideItemBumpButton, "skip 1");
    contentPanel.add(chkShowBorder, "skip 1,split 2");
    contentPanel.add(chkShowHorizontalLines);
    
    NumericKeypad keypad = new NumericKeypad();
    contentPanel.add(keypad, "span 2,gaptop 10,grow");
  }
  
  private void addButtonRow(JPanel contentPanel, String caption, JButton btnBg, JButton btnTextColor, JComboBox cbColorComponent) {
    contentPanel.add(createLabel(caption));
    contentPanel.add(btnBg, "grow,split " + (cbColorComponent == null ? 2 : 3) + ",h " + PosUIManager.getSize(30));
    contentPanel.add(btnTextColor, "grow");
    if (cbColorComponent != null)
      contentPanel.add(cbColorComponent, "grow");
    ActionListener buttonbackgroundColorActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        JButton btnButtonColor = (JButton)e.getSource();
        Color color = JColorChooser.showDialog(POSUtil.getBackOfficeWindow(), "", btnButtonColor
          .getBackground());
        if (color == null)
          return;
        btnButtonColor.setBackground(color);
        JButton btnTextColor = (JButton)btnButtonColor.getClientProperty(POSConstants.TEXT_COLOR);
        btnTextColor.setBackground(color);
      }
    };
    btnBg.putClientProperty(POSConstants.TEXT_COLOR, btnTextColor);
    btnBg.addActionListener(buttonbackgroundColorActionListener);
    btnTextColor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        JButton btnTextColor = (JButton)e.getSource();
        Color color = JColorChooser.showDialog(POSUtil.getBackOfficeWindow(), "", btnTextColor.getForeground());
        if (color == null)
          return;
        btnTextColor.setForeground(color);
      }
    });
  }
  
  private Component createLabel(String text) {
    return new JLabel(text);
  }
  
  private void updateView() {
    String yellowTimeOut = AppConfig.getString("YellowTimeOut");
    String redTimeOut = AppConfig.getString("RedTimeOut");
    if (yellowTimeOut != null) {
      tfWarningTimout.setText(yellowTimeOut);
    }
    if (redTimeOut != null) {
      tfOverTimout.setText(redTimeOut);
    }
    tfTicketsPerPage.setText(String.valueOf(TerminalConfig.getKDSTicketsPerPage()));
    chkHideItemBumpButton.setSelected(AppConfig.getBoolean("kds.hide_item_bump", false));
    chkShowBorder.setSelected(AppConfig.getBoolean("kds.border", false));
    chkShowHorizontalLines.setSelected(AppConfig.getBoolean("kds.sep", true));
    
    Color containerBgColor = TerminalConfig.getColor("kds.background", Color.black);
    Color containerFgColor = TerminalConfig.getColor("kds.textcolor", Color.white);
    
    Color initialBgColor = TerminalConfig.getColor("kds.initial.bg", new Color(0, 135, 67));
    Color initialFgColor = TerminalConfig.getColor("kds.initial.fg", Color.white);
    Color warningBgColor = TerminalConfig.getColor("kds.warning.bg", new Color(247, 177, 55));
    Color warningFgColor = TerminalConfig.getColor("kds.warning.fg", Color.white);
    Color overBgColor = TerminalConfig.getColor("kds.over.bd", new Color(204, 0, 0));
    Color overFgColor = TerminalConfig.getColor("kds.over.fg", Color.white);
    
    updateColorApplicableCompoment(cbInitialColorComponent, "kds.initial.component");
    updateColorApplicableCompoment(cbWarningColorComponent, "kds.warning.component");
    updateColorApplicableCompoment(cbOverColorComponent, "kds.over.component");
    
    updateButtonColor(btnContainerBgColor, btnContainerFgColor, containerBgColor, containerFgColor);
    updateButtonColor(btnInitialBgColor, btnInitialFgColor, initialBgColor, initialFgColor);
    updateButtonColor(btnWarningBgColor, btnWarningFgColor, warningBgColor, warningFgColor);
    updateButtonColor(btnOverBgColor, btnOverFgColor, overBgColor, overFgColor);
  }
  
  private void updateColorApplicableCompoment(JComboBox cbOverColorComponent, String prop) {
    int componentIndex = AppConfig.getInt(prop, 0);
    cbOverColorComponent.setSelectedIndex(componentIndex);
  }
  
  public void doOk()
  {
    int ticketsPerPage = tfTicketsPerPage.getInteger();
    if (ticketsPerPage <= 0) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("KitchenDisplayConfigDialog.20"));
      return;
    }
    TerminalConfig.setKDSTicketsPerPage(ticketsPerPage);
    AppConfig.put("YellowTimeOut", tfWarningTimout.getText());
    AppConfig.put("RedTimeOut", tfOverTimout.getText());
    AppConfig.put("kds.hide_item_bump", chkHideItemBumpButton.isSelected());
    AppConfig.put("kds.border", chkShowBorder.isSelected());
    AppConfig.put("kds.sep", chkShowHorizontalLines.isSelected());
    
    AppConfig.putInt("kds.initial.component", cbInitialColorComponent.getSelectedIndex());
    AppConfig.putInt("kds.warning.component", cbWarningColorComponent.getSelectedIndex());
    AppConfig.putInt("kds.over.component", cbOverColorComponent.getSelectedIndex());
    
    updateButtonColorCode(btnContainerBgColor, btnContainerFgColor, "kds.background", "kds.textcolor");
    updateButtonColorCode(btnInitialBgColor, btnInitialFgColor, "kds.initial.bg", "kds.initial.fg");
    updateButtonColorCode(btnWarningBgColor, btnWarningFgColor, "kds.warning.bg", "kds.warning.fg");
    updateButtonColorCode(btnOverBgColor, btnOverFgColor, "kds.over.bd", "kds.over.fg");
    
    setCanceled(false);
    dispose();
  }
  
  private void updateButtonColorCode(JButton btnButtonColor, JButton btnTextColor, String bgProp, String fgProp) {
    AppConfig.put(bgProp, String.valueOf(btnButtonColor.getBackground().getRGB()));
    AppConfig.put(fgProp, String.valueOf(btnTextColor.getForeground().getRGB()));
  }
  
  private void updateButtonColor(JButton btnButtonColor, JButton btnTextColor, Color bgColor, Color fgColor) {
    btnButtonColor.setBackground(bgColor);
    btnTextColor.setBackground(bgColor);
    btnTextColor.setForeground(fgColor);
  }
}
