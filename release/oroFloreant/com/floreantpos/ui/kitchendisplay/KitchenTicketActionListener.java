package com.floreantpos.ui.kitchendisplay;

public abstract interface KitchenTicketActionListener
{
  public abstract void bump(KitchenTicketView paramKitchenTicketView);
  
  public abstract void dispatch(KitchenTicketView paramKitchenTicketView);
  
  public abstract boolean isDispatchMode();
}
