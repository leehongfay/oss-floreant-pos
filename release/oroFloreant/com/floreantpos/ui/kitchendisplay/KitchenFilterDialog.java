package com.floreantpos.ui.kitchendisplay;

import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PosPrinters;
import com.floreantpos.model.Printer;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.VirtualPrinter;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.ScrollableFlowPanel;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.plaf.metal.MetalToggleButtonUI;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;






























public class KitchenFilterDialog
  extends OkCancelOptionDialog
{
  private JPanel filterPanel;
  private Terminal terminal = Application.getInstance().getTerminal();
  
  private List<Printer> printerList = new ArrayList();
  private List<OrderType> orderTypeList = new ArrayList();
  private static final String ORDER_TYPE_ID = "KDS.ORDER.TYPE.ID";
  private static final String PRINTER_NAME = "KDS.PRINTER_ID";
  
  public KitchenFilterDialog()
  {
    initializeComponent();
    setResizable(true);
  }
  
  private void initializeComponent() {
    String title = Messages.getString("KitchenFilterDialog.0");
    setTitle(title);
    setCaption(title);
    filterPanel = new JPanel();
    
    PosPrinters printers = Application.getPrinters();
    List<Printer> kitchenPrinters = printers.getKitchenPrinters();
    List<OrderType> orderType = Application.getInstance().getOrderTypes();
    
    getContentPanel().setLayout(new BorderLayout());
    filterPanel.setLayout(new MigLayout("fill, ins 0", "[225][225]"));
    JPanel leftPanel = new JPanel(new MigLayout("center center, wrap, gapy 20"));
    leftPanel.setBorder(BorderFactory.createTitledBorder(null, "Printers", 2, 0));
    
    String printerIds = "";
    List<String> printerIdsInList = new ArrayList();
    if (StringUtils.isNotBlank(terminal.getProperty("KDS.PRINTER_ID"))) {
      printerIds = terminal.getProperty("KDS.PRINTER_ID");
      printerIdsInList = Arrays.asList(printerIds.split(","));
    }
    
    for (final Printer printer : kitchenPrinters) {
      POSToggleButton button = new POSToggleButton(printer.getVirtualPrinter().getName());
      
      button.setPreferredSize(PosUIManager.getSize(120, 50));
      button.setBackground(Color.WHITE);
      button.setUI(new MetalToggleButtonUI() {
        protected Color getSelectColor() {
          return Color.RED;
        }
      });
      if ((printerIdsInList != null) && 
        (printerIdsInList.contains(printer.getVirtualPrinter().getId()))) {
        button.setSelected(true);
        if (printerList.contains(printer)) {
          return;
        }
        
        printerList.add(printer);
      }
      

      button.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          addOrRemovePrinter(printer, e);
        }
        
        private void addOrRemovePrinter(Printer printer, ActionEvent e)
        {
          JToggleButton tb = (JToggleButton)e.getSource();
          if (tb.isSelected()) {
            if (!printerList.contains(printer)) {
              printerList.add(printer);
            }
          }
          else {
            printerList.remove(printer);
          }
        }
      });
      leftPanel.add(button);
    }
    
    JPanel rightPanel = new JPanel(new BorderLayout());
    ScrollableFlowPanel contentPanel = new ScrollableFlowPanel(1);
    contentPanel.setBorder(BorderFactory.createTitledBorder(null, "Order Types", 2, 0));
    
    String orderTypeID = "";
    List<String> orderTypeIdInList = new ArrayList();
    
    if (StringUtils.isNotBlank(terminal.getProperty("KDS.ORDER.TYPE.ID"))) {
      orderTypeID = terminal.getProperty("KDS.ORDER.TYPE.ID");
      orderTypeIdInList = Arrays.asList(orderTypeID.split(","));
    }
    
    for (final OrderType order : orderType) {
      POSToggleButton btnOrdertype = new POSToggleButton(order.getName());
      btnOrdertype.setPreferredSize(PosUIManager.getSize(120, 50));
      btnOrdertype.setBackground(Color.WHITE);
      btnOrdertype.setUI(new MetalToggleButtonUI() {
        protected Color getSelectColor() {
          return Color.RED;
        }
      });
      if ((orderTypeIdInList != null) && 
        (orderTypeIdInList.contains(order.getId()))) {
        btnOrdertype.setSelected(true);
        if (orderTypeList.contains(order)) {
          return;
        }
        
        orderTypeList.add(order);
      }
      

      btnOrdertype.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          addOrRemoveOrder(order, e);
        }
        
        private void addOrRemoveOrder(OrderType order, ActionEvent e)
        {
          JToggleButton tb = (JToggleButton)e.getSource();
          if (tb.isSelected()) {
            if (!orderTypeList.contains(order)) {
              orderTypeList.add(order);
            }
          }
          else {
            orderTypeList.remove(order);
          }
          
        }
      });
      contentPanel.add(btnOrdertype);
    }
    contentPanel.getContentPane().setPreferredSize(PosUIManager.getSize(0, 500));
    rightPanel.add(contentPanel);
    JScrollPane comp = new JScrollPane(leftPanel);
    comp.setBorder(null);
    filterPanel.add(comp, "grow");
    
    JScrollPane comp2 = new JScrollPane(rightPanel);
    comp2.setBorder(null);
    
    filterPanel.add(comp2, " grow");
    
    filterPanel.setOpaque(true);
    getContentPanel().add(filterPanel);
    setSize(550, 450);
  }
  

  public void doOk()
  {
    String printerIds = "";
    String orderTypeIds = "";
    
    for (Iterator<OrderType> iterator = orderTypeList.iterator(); iterator.hasNext();) {
      OrderType orderType = (OrderType)iterator.next();
      orderTypeIds = orderTypeIds + orderType.getId();
      if (iterator.hasNext()) {
        orderTypeIds = orderTypeIds + ",";
      }
    }
    
    for (Iterator<Printer> iterator = printerList.iterator(); iterator.hasNext();) {
      Printer printer = (Printer)iterator.next();
      printerIds = printerIds + printer.getVirtualPrinter().getId();
      if (iterator.hasNext()) {
        printerIds = printerIds + ",";
      }
    }
    
    terminal.addProperty("KDS.PRINTER_ID", printerIds);
    terminal.addProperty("KDS.ORDER.TYPE.ID", orderTypeIds);
    TerminalDAO.getInstance().update(terminal);
    setCanceled(false);
    dispose();
  }
  
  public List getPrinter() {
    if (printerList.isEmpty()) {
      return null;
    }
    
    return printerList;
  }
  
  public List getOrderType()
  {
    if (orderTypeList.isEmpty()) {
      return null;
    }
    
    return orderTypeList;
  }
}
