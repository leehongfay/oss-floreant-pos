package com.floreantpos.ui.kitchendisplay;

import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import com.floreantpos.model.KitchenTicket;
import com.floreantpos.model.KitchenTicketItem;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.dao.KitchenTicketItemDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.ext.KitchenStatus;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import org.hibernate.Session;
import org.hibernate.Transaction;


















public class KitchenTicketStatusSelector
  extends POSDialog
  implements ActionListener
{
  private PosButton btnVoid = new PosButton(KitchenStatus.VOID.name());
  private PosButton btnReady = new PosButton(Messages.getString("KitchenTicketView.11"));
  private KitchenTicket kitchenTicket;
  private KitchenTicketItem ticketItem;
  
  public KitchenTicketStatusSelector(Frame parent)
  {
    super(parent, true);
    initComponent();
  }
  
  public KitchenTicketStatusSelector(Frame parent, KitchenTicket kitchenTicket) {
    super(parent, true);
    this.kitchenTicket = kitchenTicket;
    initComponent();
  }
  
  private void initComponent() {
    setTitle(Messages.getString("KitchenTicketStatusSelector.0"));
    setIconImage(Application.getApplicationIcon().getImage());
    setDefaultCloseOperation(2);
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle(Messages.getString("KitchenTicketStatusSelector.1"));
    add(titlePanel, "North");
    
    JPanel panel = new JPanel(new GridLayout(1, 0, 10, 10));
    panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    
    panel.add(btnReady);
    
    add(panel);
    
    btnVoid.setActionCommand(KitchenStatus.BUMP.name());
    btnReady.setActionCommand(KitchenStatus.BUMP.name());
    btnVoid.addActionListener(this);
    btnReady.addActionListener(this);
  }
  
  public void actionPerformed(ActionEvent e)
  {
    try {
      KitchenStatus status = KitchenStatus.fromString(e.getActionCommand());
      ticketItem.setKitchenStatusValue(status);
      
      double itemQuantity = ticketItem.getQuantity().doubleValue();
      
      Ticket ticket = TicketDAO.getInstance().loadFullTicket(kitchenTicket.getTicketId());
      for (TicketItem item : ticket.getTicketItems()) {
        if ((ticketItem.getMenuItemCode() != null) && (ticketItem.getMenuItemCode().equals(item.getItemCode())))
          if (item.getKitchenStatusValue() != KitchenStatus.BUMP)
          {

            if (itemQuantity == 0.0D) {
              break;
            }
            item.setKitchenStatusValue(status);
            itemQuantity -= item.getQuantity().doubleValue();
          }
      }
      Transaction tx = null;
      Session session = null;
      try
      {
        session = KitchenTicketItemDAO.getInstance().createNewSession();
        tx = session.beginTransaction();
        session.saveOrUpdate(ticket);
        session.saveOrUpdate(ticketItem);
        tx.commit();
      }
      catch (Exception ex) {
        tx.rollback();
      } finally {
        session.close();
      }
      dispose();
    } catch (Exception e2) {
      POSMessageDialog.showError(this, e2.getMessage(), e2);
    }
  }
  
  public KitchenTicketItem getTicketItem() {
    return ticketItem;
  }
  
  public void setTicketItem(KitchenTicketItem ticketItem) {
    this.ticketItem = ticketItem;
  }
}
