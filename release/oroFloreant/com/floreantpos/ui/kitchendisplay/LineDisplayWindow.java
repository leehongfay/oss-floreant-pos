package com.floreantpos.ui.kitchendisplay;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.config.AppConfig;
import com.floreantpos.main.Application;
import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.ImageResource;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.dao.ImageResourceDAO;
import com.floreantpos.swing.ImageComponent;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.ticket.TicketViewerTable;
import com.floreantpos.ui.ticket.TicketViewerTableModel;
import com.floreantpos.ui.views.order.OrderController;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.actions.TicketEditListener;
import com.floreantpos.util.NumberUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;


public class LineDisplayWindow
  extends JFrame
  implements TicketEditListener, WindowListener
{
  private static final String POSY = "bwy";
  private static final String POSX = "bwx";
  private static final String WINDOW_HEIGHT = "bwheight";
  private static final String WINDOW_WIDTH = "bwwidth";
  public static final String VIEW_NAME = "KD";
  private ImageComponent lblTicketImage;
  private JLabel lblTicketNameValue;
  private JLabel lblTicketPrice;
  private JLabel lblTicketPriceValue;
  private JLabel lblTotalTicketPrice;
  private JLabel lblTotalTicketPriceValue;
  private static LineDisplayWindow instance;
  private JPanel rightTicketInfoPanel;
  private JLabel lblQuantity;
  private JLabel lblQuantityValue;
  private JLabel lblDot;
  private JLabel lblUnitPrice;
  private JLabel lblUnitPriceValue;
  private JPanel rightPanel;
  private ImageIcon defaultImage;
  private TicketViewerTable ticketViewerTable = new TicketViewerTable();
  
  public LineDisplayWindow() {
    setIconImage(Application.getApplicationIcon().getImage());
    try {
      defaultImage = IconFactory.getIcon("offer-image.png");
    }
    catch (Exception localException) {}
    ticketViewerTable.setModel(new LineDisplayTableModel(ticketViewerTable));
    ticketViewerTable.setShowGrid(false);
    ticketViewerTable.setFont(ticketViewerTable.getFont().deriveFont(PosUIManager.getFontSize(24)));
    TableColumnModel columnModel = ticketViewerTable.getColumnModel();
    columnModel.getColumn(0).setPreferredWidth(PosUIManager.getSize(250));
    
    initComponents();
    positionWindow();
    addWindowListener(this);
    
    setDefaultCloseOperation(0);
    
    setTitle(Application.getTitle() + "- " + Messages.getString("LineDisplayWindow.1"));
    applyComponentOrientation(ComponentOrientation.getOrientation(Locale.getDefault()));
    
    OrderView.getInstance().getOrderController().addTicketUpdateListener(this);
  }
  
  public static LineDisplayWindow getInstance() {
    if (instance == null) {
      instance = new LineDisplayWindow();
    }
    return instance;
  }
  
  private void initComponents()
  {
    JPanel centerPanel = new JPanel(new BorderLayout(5, 5));
    getContentPane().add(centerPanel);
    
    JPanel leftImagePanel = new JPanel(new BorderLayout());
    
    lblTicketImage = new ImageComponent(defaultImage.getImage());
    

    leftImagePanel.add(lblTicketImage, "Center");
    
    centerPanel.add(leftImagePanel, "Center");
    
    rightPanel = new JPanel(new MigLayout("fill"));
    rightPanel.setOpaque(true);
    rightPanel.setBackground(Color.WHITE);
    rightTicketInfoPanel = new JPanel(new MigLayout("fill"));
    rightTicketInfoPanel.setOpaque(false);
    rightTicketInfoPanel.setPreferredSize(PosUIManager.getSize(500, 300));
    
    rightPanel.add(rightTicketInfoPanel, "grow, aligny top");
    lblTicketNameValue = new JLabel();
    

    Font fontTicketNameValue = lblTicketNameValue.getFont();
    int titleFontSizeToUse = 48;
    int otherFontSizeToUse = 14;
    int totalFontSizeToUse = 32;
    
    lblTicketNameValue.setFont(new Font(fontTicketNameValue.getName(), 1, titleFontSizeToUse));
    

    lblTicketPrice = new JLabel(Messages.getString("LineDisplayWindow.6"));
    Font fontTicketPrice = lblTicketPrice.getFont();
    lblTicketPrice.setFont(new Font(fontTicketPrice.getName(), 0, otherFontSizeToUse));
    
    lblUnitPrice = new JLabel(Messages.getString("LineDisplayWindow.7"));
    lblUnitPrice.setFont(new Font(fontTicketPrice.getName(), 0, otherFontSizeToUse));
    lblUnitPriceValue = new JLabel();
    lblUnitPriceValue.setFont(new Font(fontTicketPrice.getName(), 0, otherFontSizeToUse));
    
    lblTicketPriceValue = new JLabel();
    lblTicketPriceValue.setFont(new Font(fontTicketPrice.getName(), 0, otherFontSizeToUse));
    
    lblQuantity = new JLabel(Messages.getString("LineDisplayWindow.8"));
    lblQuantity.setFont(new Font(fontTicketPrice.getName(), 0, otherFontSizeToUse));
    
    lblQuantityValue = new JLabel();
    lblQuantityValue.setFont(new Font(fontTicketPrice.getName(), 0, otherFontSizeToUse));
    
    lblDot = new JLabel("---------------------------------------------------------------------------------");
    lblDot.setFont(new Font(fontTicketPrice.getName(), 1, otherFontSizeToUse));
    
    lblTotalTicketPrice = new JLabel(Messages.getString("LineDisplayWindow.10"));
    lblTotalTicketPrice.setFont(new Font(fontTicketPrice.getName(), 1, totalFontSizeToUse));
    
    lblTotalTicketPriceValue = new JLabel();
    lblTotalTicketPriceValue.setFont(new Font(fontTicketPrice.getName(), 1, totalFontSizeToUse));
    
















    rightTicketInfoPanel.add(ticketViewerTable, "grow");
    

    centerPanel.add(rightPanel, "East");
    
    if (defaultImage != null) {}
  }
  


  private void saveSizeAndLocation()
  {
    AppConfig.putInt("bwwidth", getWidth());
    AppConfig.putInt("bwheight", getHeight());
    AppConfig.putInt("bwx", getX());
    AppConfig.putInt("bwy", getY());
  }
  
  private void positionWindow() {
    int width = AppConfig.getInt("bwwidth", 900);
    int height = AppConfig.getInt("bwheight", 650);
    setSize(width, height);
    
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    int x = width - width >> 1;
    int y = height - height >> 1;
    
    x = AppConfig.getInt("bwx", x);
    y = AppConfig.getInt("bwy", y);
    
    setLocation(x, y);
  }
  
  public void itemAdded(Ticket ticket, TicketItem item)
  {
    MenuItem menuItem = item.getMenuItem();
    ImageIcon icon = menuItem.getImage();
    if (icon != null) {
      String imageId = menuItem.getImageId();
      ImageResource imageResource = ImageResourceDAO.getInstance().get(imageId);
      imageResource.getImage();
      lblTicketImage.setImage(imageResource.getImage());
    } else if (defaultImage != null) {
      lblTicketImage.setImage(defaultImage.getImage());
    }
    
    ticketViewerTable.setTicket(ticket);
    
    revalidate();
    repaint();
  }
  



  public void itemRemoved(TicketItem item) {}
  


  public void windowOpened(WindowEvent e) {}
  


  public void windowClosing(WindowEvent e)
  {
    OrderView.getInstance().getOrderController().removeTicketUpdateListener(this);
    saveSizeAndLocation();
    setVisible(false);
  }
  

  public void windowClosed(WindowEvent e)
  {
    setVisible(false);
  }
  



  public void windowIconified(WindowEvent e) {}
  



  public void windowDeiconified(WindowEvent e) {}
  


  public void windowActivated(WindowEvent e) {}
  


  public void windowDeactivated(WindowEvent e) {}
  


  public void openFullScreen()
  {
    setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
    setResizable(true);
    setVisible(true);
  }
  
  class LineDisplayTableModel extends TicketViewerTableModel
  {
    public LineDisplayTableModel(JTable table) {
      super();
    }
    
    public int getColumnCount()
    {
      return 2;
    }
    
    protected void calculateRows()
    {
      if (ticket == null) {
        return;
      }
      List<TicketItem> ticketItems = ticket.getTicketItems();
      if (ticketItems != null) {
        int row = 0;
        tableRows.clear();
        for (int i = ticketItems.size() - 1; i >= 0; i--) {
          tableRows.put(String.valueOf(row++), ticketItems.get(i));
        }
      }
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      ITicketItem ticketItem = (ITicketItem)tableRows.get(String.valueOf(rowIndex));
      
      if (ticketItem == null) {
        return null;
      }
      
      switch (columnIndex) {
      case 0: 
        return ticketItem.getNameDisplay();
      
      case 1: 
        return NumberUtil.getCurrencyFormat(ticketItem.getSubTotalAmountDisplay());
      }
      
      return null;
    }
  }
}
