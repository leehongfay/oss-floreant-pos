package com.floreantpos.ui.kitchendisplay;

import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import com.floreantpos.model.Store;
import com.floreantpos.versioning.VersionInfo;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

















public class KitchenDisplayWindow
  extends JFrame
{
  private KitchenDisplayView view = new KitchenDisplayView();
  
  public KitchenDisplayWindow() {
    setTitle(VersionInfo.getAppName() + "-" + Application.getInstance().getStore().getName() + "-" + Messages.getString("KitchenDisplayWindow.0"));
    setIconImage(Application.getApplicationIcon().getImage());
    
    add(view);
    
    setSize(Toolkit.getDefaultToolkit().getScreenSize());
    setDefaultCloseOperation(2);
  }
  
  public void setVisible(boolean b)
  {
    super.setVisible(b);
    view.setVisible(b);
  }
  
  public void dispose()
  {
    view.cleanup();
    super.dispose();
  }
}
