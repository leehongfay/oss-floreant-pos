package com.floreantpos.ui.kitchendisplay;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.config.AppConfig;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.model.Course;
import com.floreantpos.model.KitchenTicket;
import com.floreantpos.model.KitchenTicketItem;
import com.floreantpos.model.ext.KitchenStatus;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.util.NumberUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;


















public class KitchenTicketView
  extends JPanel
  implements ChangeListener
{
  private static Border normalBorder;
  private static Border selectedBorder = BorderFactory.createLineBorder(Color.green, 2);
  private ImageIcon checkMark = IconFactory.getIcon("/ui_icons/", "check_mark.png");
  
  private TimerWatch timerWatch;
  
  private JScrollPane scrollPane;
  
  private JPanel headerPanel;
  private JLabel ticketInfo;
  private JLabel tableInfo;
  private JLabel serverInfo;
  private BumpButton btnDone;
  private KitchenTicketStatusSelector statusSelector;
  private KitchenTicketActionListener kitchenTicketContainerView;
  private JTable table;
  private KitchenTicketTableModel tableModel;
  private KitchenTicket ticket;
  private boolean showItemBump;
  private Color cellDefaultBackgroundColor = Color.black;
  private Color cellDefaultTextColor = Color.white;
  
  public KitchenTicketView(KitchenTicket ticket, KitchenTicketActionListener kitchenTicketContainerView) {
    this.ticket = ticket;
    this.kitchenTicketContainerView = kitchenTicketContainerView;
    
    cellDefaultBackgroundColor = TerminalConfig.getColor("kds.background", Color.black);
    cellDefaultTextColor = TerminalConfig.getColor("kds.textcolor", Color.white);
    boolean showBorder = AppConfig.getBoolean("kds.border", false);
    
    normalBorder = BorderFactory.createLineBorder(showBorder ? Color.GRAY : KitchenDisplayView.getInstance().getBackground());
    
    setLayout(new BorderLayout(1, 1));
    setOpaque(false);
    
    String dispatchText = Messages.getString("KitchenTicketView.DISPATCH");
    String bumpText = Messages.getString("KitchenTicketView.11");
    btnDone = new BumpButton(kitchenTicketContainerView.isDispatchMode() ? dispatchText : bumpText);
    
    createHeader(ticket);
    createTable(ticket);
    createButtonPanel();
    
    setBorder(BorderFactory.createLineBorder(Color.GRAY));
    
    statusSelector = new KitchenTicketStatusSelector((Frame)SwingUtilities.getWindowAncestor(this), ticket);
    setPreferredSize(PosUIManager.getSize(350, 240));
    
    timerWatch.start();
    
    addAncestorListener(new AncestorListener()
    {
      public void ancestorRemoved(AncestorEvent event) {
        timerWatch.stop();
      }
      


      public void ancestorMoved(AncestorEvent event) {}
      


      public void ancestorAdded(AncestorEvent event) {}
    });
    setSelectedBorder(Boolean.valueOf(false));
  }
  
  public void stopTimer() {
    timerWatch.stop();
  }
  
  private void createHeader(KitchenTicket kitchenTicket) {
    String printerName = kitchenTicket.getPrinterName();
    if (StringUtils.isEmpty(printerName)) {
      printerName = kitchenTicket.getPrinters().toString();
    }
    ticketInfo = new JLabel(POSConstants.TOKEN + " " + kitchenTicket.getTokenNo() + " (" + kitchenTicket.getSequenceNumber() + ") ");
    
    tableInfo = new JLabel();
    if ((kitchenTicket.getTableNumbers() != null) && (kitchenTicket.getTableNumbers().size() > 0)) {
      String tableNumbers = kitchenTicket.getTableNumbers().toString();
      tableNumbers = tableNumbers.replace("[", "").replace("]", "");
      tableInfo.setText(POSConstants.TABLE + " " + tableNumbers);
    }
    serverInfo = new JLabel();
    if (kitchenTicket.getServerName() != null) {
      serverInfo.setText(POSConstants.RECEIPT_REPORT_SERVER_LABEL + " " + kitchenTicket.getServerName());
    }
    Font font = new Font("Tahoma", 1, PosUIManager.getDefaultFontSize() + 2);
    
    ticketInfo.setFont(font);
    tableInfo.setFont(font);
    serverInfo.setFont(font);
    
    headerPanel = new JPanel(new MigLayout("fill,ins 0", "sg, fill", ""));
    timerWatch = new TimerWatch(ticket.getCreateDate(), this);
    headerPanel.setBackground(Color.black);
    
    headerPanel.add(ticketInfo, "split 2");
    headerPanel.add(timerWatch, "right,wrap, span");
    headerPanel.add(tableInfo, "split 2, grow");
    headerPanel.add(serverInfo, "right,span");
    
    if (AppConfig.getBoolean("kds.sep", true)) {
      JSeparator sep = new JSeparator();
      sep.setBackground(Color.gray);
      headerPanel.add(sep, "newline,h 1!,span");
    }
    add(headerPanel, "North");
    updateHeaderView();
  }
  
  private void createTable(KitchenTicket kitchenTicket) {
    tableModel = new KitchenTicketTableModel(kitchenTicket.getTicketItems());
    table = new JTable(tableModel);
    table.setRowSelectionAllowed(false);
    table.setCellSelectionEnabled(false);
    table.setRowHeight(PosUIManager.getSize(30));
    table.setTableHeader(null);
    table.setOpaque(false);
    table.setIntercellSpacing(new Dimension(0, 0));
    table.setShowGrid(false);
    table.setFocusable(false);
    table.setDefaultRenderer(Object.class, new DefaultTableCellRenderer()
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JLabel rendererComponent = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        KitchenTicketItem ticketItem = (KitchenTicketItem)tableModel.getRowData(row);
        Icon icon = null;
        if ((column == 0) && 
          (StringUtils.isNotEmpty(ticketItem.getCourseId()))) {
          Course course = DataProvider.get().getCourse(ticketItem.getCourseId());
          icon = course == null ? null : course.getIcon();
        }
        
        rendererComponent.setIcon(icon);
        rendererComponent.setHorizontalAlignment(2);
        rendererComponent.setBackground(cellDefaultBackgroundColor);
        rendererComponent.setForeground(cellDefaultTextColor);
        Font font = new Font("Arial", 1, PosUIManager.getDefaultFontSize() + 4);
        rendererComponent.setFont(font);
        if (ticketItem.isVoided().booleanValue()) {
          JLabel label = rendererComponent;
          if (column == 0) {
            label.setText("<html><strike>" + NumberUtil.trimDecilamIfNotNeeded(ticketItem.getQuantity(), true) + "x " + ticketItem.getMenuItemName() + "</strike></html>");
          }
          rendererComponent.setBackground(Color.red);
          rendererComponent.setForeground(Color.white);
          return label;
        }
        if ((ticketItem != null) && (ticketItem.getStatus() != null)) {
          if (ticketItem.getStatus().equalsIgnoreCase(KitchenStatus.BUMP.name())) {
            rendererComponent.setBackground(Color.green);
            rendererComponent.setForeground(Color.black);
          } else {
            if ((ticketItem.isVoided().booleanValue()) || (ticketItem.getStatus().equalsIgnoreCase(KitchenStatus.VOID.name()))) {
              JLabel label = rendererComponent;
              if (column == 0)
                label.setText("<html><strike>" + ticketItem.getMenuItemName() + "</strike></html>");
              label.setBackground(new Color(128, 0, 128));
              label.setForeground(Color.white);
              return label;
            }
            
            rendererComponent.setBackground(cellDefaultBackgroundColor);
            rendererComponent.setForeground(cellDefaultTextColor);
          }
        }
        JLabel jLabel = new JLabel();
        jLabel.setOpaque(false);
        if ((column == 2) && (
          (ticketItem.isVoided().booleanValue()) || (KitchenStatus.BUMP.name().equals(ticketItem.getStatus())))) {
          return jLabel;
        }
        
        if ((column == 1) && 
          (ticketItem.getQuantity().doubleValue() <= 0.0D)) {
          return jLabel;
        }
        
        return rendererComponent;
      }
    });
    resizeTableColumns();
    
    AbstractAction action = new AbstractAction()
    {
      public void actionPerformed(ActionEvent e)
      {
        int row = Integer.parseInt(e.getActionCommand());
        KitchenTicketItem ticketItem = (KitchenTicketItem)tableModel.getRowData(row);
        if (!ticketItem.isCookable().booleanValue()) {
          return;
        }
        if ((ticketItem.isVoided().booleanValue()) || (ticketItem.getStatus().equals(KitchenStatus.BUMP.name()))) {
          return;
        }
        statusSelector.setTicketItem(ticketItem);
        statusSelector.setSize(PosUIManager.getSize(350, 150));
        statusSelector.setLocationRelativeTo(KitchenTicketView.this);
        statusSelector.setVisible(true);
        table.repaint();
      }
    };
    showItemBump = (!AppConfig.getBoolean("kds.hide_item_bump", false));
    if (showItemBump) {
      new KichenButtonColumn(table, action, 2)
      {
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
        {
          KitchenTicketItem ticketItem = (KitchenTicketItem)tableModel.getRowData(row);
          Font font = getFont().deriveFont(1, PosUIManager.getDefaultFontSize() + 3);
          if (ticketItem.isVoided().booleanValue()) {
            JLabel jLabel = new JLabel("X", 0);
            jLabel.setFont(font);
            return jLabel;
          }
          if (KitchenStatus.BUMP.name().equals(ticketItem.getStatus())) {
            JLabel label = new JLabel();
            label.setHorizontalAlignment(0);
            label.setBackground(Color.green);
            label.setFont(font);
            label.setIcon(checkMark);
            return label;
          }
          if (ticketItem.getQuantity().doubleValue() <= 0.0D) {
            return new JLabel();
          }
          Component rendererComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
          rendererComponent.setFont(font);
          return rendererComponent;
        }
        
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
        {
          KitchenTicketItem ticketItem = (KitchenTicketItem)tableModel.getRowData(row);
          if (ticketItem.isVoided().booleanValue()) {
            return new JLabel("X", 0);
          }
          if (ticketItem.getStatus().equals(KitchenStatus.BUMP.name())) {
            JLabel label = new JLabel();
            label.setHorizontalAlignment(0);
            label.setOpaque(false);
            label.setIcon(checkMark);
            return label;
          }
          if (ticketItem.getQuantity().doubleValue() <= 0.0D) {
            return new JLabel();
          }
          return super.getTableCellEditorComponent(table, value, isSelected, row, column);
        }
      };
    }
    
    scrollPane = new JScrollPane(table);
    scrollPane.getViewport().setOpaque(false);
    scrollPane.setOpaque(false);
    scrollPane.setBorder(BorderFactory.createLineBorder(KitchenDisplayView.getInstance().getBackground()));
    add(scrollPane);
  }
  
  private void updateHeaderView() {
    if (timerWatch.applicableColorComponentIndex.intValue() == 0) {
      timerWatch.setBackgroundColor(timerWatch.backColor);
      timerWatch.setForegroundColor(timerWatch.textColor);
    }
    else {
      headerPanel.setBackground(timerWatch.backColor);
    }
    ticketInfo.setForeground(timerWatch.textColor);
    tableInfo.setForeground(timerWatch.textColor);
    serverInfo.setForeground(timerWatch.textColor);
  }
  
  private void createButtonPanel() {
    JPanel buttonPanel = new JPanel(new BorderLayout(0, 0));
    buttonPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    buttonPanel.setOpaque(false);
    
    btnDone.setFont(btnDone.getFont().deriveFont(1, PosUIManager.getDefaultFontSize() + 3));
    btnDone.setPreferredSize(PosUIManager.getSize(100, 40));
    
    if (AppConfig.getBoolean("kds.sep", true)) {
      JSeparator sep = new JSeparator();
      sep.setBackground(Color.gray);
      sep.setPreferredSize(new Dimension(0, 1));
      buttonPanel.add(sep, "North");
    }
    buttonPanel.add(btnDone);
    add(buttonPanel, "South");
  }
  
  protected String getKey() {
    return String.valueOf(getClientProperty("key"));
  }
  
  private void resizeTableColumns() {
    table.setAutoResizeMode(4);
    setColumnWidth(1, PosUIManager.getSize(40));
    setColumnWidth(2, PosUIManager.getSize(65));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = table.getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  class KitchenTicketTableModel extends ListTableModel<KitchenTicketItem>
  {
    KitchenTicketTableModel() {
      super(list);
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      if ((columnIndex == 2) && (showItemBump)) {
        return true;
      }
      
      return false;
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      KitchenTicketItem ticketItem = (KitchenTicketItem)getRowData(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return NumberUtil.trimDecilamIfNotNeeded(ticketItem.getQuantity(), true) + "x " + ticketItem.getMenuItemName();
      
      case 1: 
        return "";
      case 2: 
        return (ticketItem.isVoided().booleanValue()) || (!showItemBump) ? "" : POSConstants.BUMP;
      }
      return null;
    }
  }
  
  public KitchenTicket getTicket() {
    return ticket;
  }
  
  public void setTicket(KitchenTicket ticket) {
    this.ticket = ticket;
  }
  
  public void bump() {
    timerWatch.stop();
    kitchenTicketContainerView.bump(this);
  }
  
  private void setSelectedBorder(Boolean keySelected) {
    setBorder(!keySelected.booleanValue() ? normalBorder : selectedBorder);
  }
  
  private class BumpButton extends PosButton implements ActionListener {
    private Boolean keySelected = Boolean.valueOf(false);
    
    public BumpButton(String string) {
      super();
      setOpaque(false);
      addActionListener(this);
      setForeground(Color.white);
      setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    }
    
    protected void paintComponent(Graphics g)
    {
      g.setColor(getBackground());
      g.fillRect(0, 0, getWidth(), getHeight());
      
      Color color1 = timerWatch != null ? new Color(45, 45, 45) : getBackground();
      Color color2 = color1;
      
      int buttonX = 0;
      int buttonY = 0;
      int width = getWidth();
      int height = getHeight();
      
      GradientPaint gp = new GradientPaint(buttonX, buttonY, color2, width - 2, height - 2, color1, true);
      Graphics2D g2 = (Graphics2D)g;
      g2.setPaint(gp);
      g2.fillRect(buttonX, buttonY, width, height);
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      if (keySelected.booleanValue()) {
        g2.setColor(new Color(0, 160, 0));
      }
      else {
        g2.setColor(Color.black);
      }
      int h = getHeight() - 10;
      g2.fillOval(10, 4, h, h);
      g2.setColor(Color.WHITE);
      g2.setFont(new Font(getFont().getName(), 1, PosUIManager.getDefaultFontSize() + 8));
      FontMetrics fm = g2.getFontMetrics();
      Rectangle2D r = fm.getStringBounds(getKey(), g2);
      int x = (h + 10 - (int)r.getWidth() + 10) / 2;
      int y = (h - (int)r.getHeight()) / 2 + fm.getAscent();
      g2.drawString(getKey(), x, y + 2);
      g2.setFont(getFont());
      g2.setBackground(color1);
      super.paintComponent(g2);
    }
    
    public Boolean isKeySelected() {
      return keySelected;
    }
    
    public void setKeySelected(Boolean keySelected) {
      this.keySelected = keySelected;
      KitchenTicketView.this.setSelectedBorder(keySelected);
      repaint();
    }
    
    public void actionPerformed(ActionEvent e)
    {
      if (kitchenTicketContainerView.isDispatchMode()) {
        kitchenTicketContainerView.dispatch(KitchenTicketView.this);
      }
      else {
        kitchenTicketContainerView.bump(KitchenTicketView.this);
      }
    }
  }
  
  public boolean keySelected(int key) {
    String keyString = KeyEvent.getKeyText(key);
    if (keyString.contains("NumPad-")) {
      keyString = keyString.split("-")[1];
    }
    Object kitchenKey = getClientProperty("key");
    String keyValue = String.valueOf(kitchenKey);
    boolean keySelected = keyValue.equals(keyString);
    btnDone.setKeySelected(Boolean.valueOf(keySelected));
    return keySelected;
  }
  
  public void setSelected(boolean b) {
    btnDone.setKeySelected(Boolean.valueOf(b));
  }
  
  public boolean isKeySelected() {
    return btnDone.isKeySelected().booleanValue();
  }
  
  public void fireBumpSelected() {
    btnDone.actionPerformed(null);
  }
  
  public void stateChanged(ChangeEvent e)
  {
    if (timerWatch == null)
      return;
    updateHeaderView();
  }
}
