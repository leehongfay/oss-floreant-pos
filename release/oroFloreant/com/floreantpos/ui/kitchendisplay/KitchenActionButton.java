package com.floreantpos.ui.kitchendisplay;

import com.floreantpos.swing.PosButton;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class KitchenActionButton
  extends PosButton implements MouseListener, MouseMotionListener
{
  public static final int TYPE_ROUNDED_RECTANGLE = 1;
  public static final int RECTANGLE = 2;
  private int shape;
  private boolean buttonSelected;
  private Color selectedBackgoundColor;
  private Color originalBackground;
  private Color originalForeground;
  private Color selectedBorderColor;
  private int gapTop = 2;
  private int gapLeft = 5;
  private int arcWidth = 10;
  private int arcHeight = 10;
  private boolean cellButton = true;
  private boolean btnToogle;
  
  public KitchenActionButton(String text) {
    this(text, 1, Color.black, Color.white);
    selectedBackgoundColor = new Color(203, 33, 55);
    setCellButton(false);
    setGap(2, 2);
  }
  
  public KitchenActionButton(String text, Color bg) {
    this(text, bg, Color.black);
  }
  
  public KitchenActionButton(String text, Color bg, Color fg) {
    this(text, 2, bg, fg);
    selectedBackgoundColor = new Color(203, 33, 55);
    setFont(getFont().deriveFont(1));
    setCellButton(false);
    setGap(0, 0);
  }
  
  public KitchenActionButton(String text, int shape, Color bg, Color fg) {
    super(text);
    this.shape = shape;
    originalBackground = bg;
    originalForeground = fg;
    setOpaque(false);
    setFocusPainted(false);
    setContentAreaFilled(false);
    setFocusable(false);
    setBackground(bg);
    setForeground(fg);
    setBorderPainted(false);
    addMouseListener(this);
  }
  
  public void setCellButton(boolean b) {
    cellButton = b;
  }
  
  public void setBtnToogle(boolean btnToogle) {
    this.btnToogle = btnToogle;
  }
  
  public void setGap(int gapTop, int gapLeft) {
    this.gapTop = gapTop;
    this.gapLeft = gapLeft;
  }
  
  public void setRoundCorner(int arcWidth, int arcHeight) {
    this.arcWidth = arcWidth;
    this.arcHeight = arcHeight;
  }
  
  public void setSelectedBackground(Color selectedBackground) {
    selectedBackgoundColor = selectedBackground;
  }
  

  public KitchenActionButton() {}
  
  public void setSelected(boolean b)
  {
    buttonSelected = b;
  }
  
  public boolean isSelected()
  {
    return buttonSelected;
  }
  
  protected void paintComponent(Graphics g)
  {
    Graphics2D g2 = (Graphics2D)g;
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    if (selectedBorderColor != null) {
      g.setColor(selectedBorderColor);
      g.fillRect(0, 0, getWidth(), getHeight());
    }
    if (buttonSelected) {
      g.setColor(selectedBackgoundColor);
      if (shape == 1) {
        g.fillRoundRect(gapLeft, gapTop, getWidth() - gapLeft * 2, getHeight() - gapTop * 2, arcWidth, arcHeight);
      } else {
        g.fillRect(0, 0, getWidth(), getHeight());
      }
      setForeground(Color.WHITE);
    }
    else {
      g.setColor(originalBackground);
      if (shape == 1) {
        g.fillRoundRect(gapLeft, gapTop, getWidth() - gapLeft * 2, getHeight() - gapTop * 2, arcWidth, arcHeight);
      } else {
        g.fillRect(0, 0, getWidth(), getHeight());
        g.drawLine(getX(), getY(), 1, getHeight());
      }
      setForeground(originalForeground);
    }
    super.paintComponent(g);
  }
  
  public void setSelectedBorderColor(Color color) {
    selectedBorderColor = color;
    repaint();
  }
  


  public void mouseClicked(MouseEvent e) {}
  

  public void mousePressed(MouseEvent e)
  {
    if (btnToogle)
      return;
    if (!cellButton) {
      setSelected(true);
    }
  }
  
  public void mouseReleased(MouseEvent e) {
    if (btnToogle)
      return;
    if (!cellButton) {
      setSelected(false);
      repaint();
    }
  }
  
  public void mouseEntered(MouseEvent e) {}
  
  public void mouseExited(MouseEvent e) {}
  
  public void mouseDragged(MouseEvent e) {}
  
  public void mouseMoved(MouseEvent e) {}
}
