package com.floreantpos.ui.kitchendisplay;

import com.floreantpos.config.TerminalConfig;
import com.floreantpos.ui.HeaderPanel;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.ui.views.order.ViewPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import javax.swing.border.EmptyBorder;


















public class KitchenDisplayView
  extends ViewPanel
{
  public static final String VIEW_NAME = "KD";
  private KeyboardDispatcher dispatcher;
  private KitchenTicketListPanel ticketPanel;
  private static KitchenDisplayView instance;
  
  public KitchenDisplayView()
  {
    setLayout(new BorderLayout(5, 5));
    
    ticketPanel = new KitchenTicketListPanel();
    ticketPanel.setOpaque(false);
    ticketPanel.setBorder(new EmptyBorder(5, 0, 5, 0));
    
    add(ticketPanel);
    dispatcher = new KeyboardDispatcher(null);
  }
  
  public Color getBackground()
  {
    return TerminalConfig.getColor("kds.background", Color.black);
  }
  
  public void setVisible(boolean b)
  {
    super.setVisible(b);
    
    if (b) {
      RootView.getInstance().getHeaderPanel().setVisible(false);
      String currentView = TerminalConfig.getDefaultView();
      ticketPanel.setBackButtonVisible((currentView != null) && (!currentView.equals("KD")));
      ticketPanel.updateKDSView();
      KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
      manager.addKeyEventDispatcher(dispatcher);
    }
    else {
      KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
      manager.removeKeyEventDispatcher(dispatcher);
      cleanup();
    }
  }
  
  private class KeyboardDispatcher implements KeyEventDispatcher {
    private KeyboardDispatcher() {}
    
    public boolean dispatchKeyEvent(KeyEvent e) { if (e.getID() == 402) {
        ticketPanel.setSelectedKey(e.getKeyCode());
      }
      return false;
    }
  }
  
  public synchronized void cleanup() {
    ticketPanel.stopTimer();
    ticketPanel.reset();
  }
  
  public void updateView() {
    revalidate();
    repaint();
  }
  
  public String getViewName()
  {
    return "KD";
  }
  
  public static synchronized KitchenDisplayView getInstance() {
    if (instance == null) {
      instance = new KitchenDisplayView();
    }
    return instance;
  }
}
