package com.floreantpos.ui.kitchendisplay;

import com.floreantpos.config.AppConfig;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.swing.PosUIManager;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.event.ChangeListener;
import net.miginfocom.swing.MigLayout;
import org.joda.time.Duration;
import org.joda.time.Instant;
import org.joda.time.Interval;






















public class TimerWatch
  extends JPanel
  implements ActionListener
{
  private Timer updateTimer = new Timer(1000, this);
  private JLabel timerLabel = new JLabel("");
  
  private final Date date;
  
  private final Color initialBgColor;
  
  private final Color initialFgColor;
  private final Color warningBgColor;
  private final Color warningFgColor;
  private final Color overBgColor;
  private final Color overFgColor;
  public Color backColor;
  public Color textColor;
  public Integer applicableColorComponentIndex;
  private ChangeListener colorChangeListener;
  int timeOutValueYellow = 300;
  int timeOutValueRed = 600;
  private final int initialColorComponent;
  private final int warningColorComponent;
  private final int overColorComponent;
  
  public TimerWatch(Date date)
  {
    this(date, null);
  }
  
  public TimerWatch(Date date, ChangeListener colorChangeListener) {
    this.date = date;
    this.colorChangeListener = colorChangeListener;
    setOpaque(false);
    setLayout(new MigLayout("right,ins 0"));
    
    initialBgColor = TerminalConfig.getColor("kds.initial.bg", new Color(0, 135, 67));
    initialFgColor = TerminalConfig.getColor("kds.initial.fg", Color.white);
    warningBgColor = TerminalConfig.getColor("kds.warning.bg", new Color(247, 177, 55));
    warningFgColor = TerminalConfig.getColor("kds.warning.fg", Color.white);
    overBgColor = TerminalConfig.getColor("kds.over.bd", new Color(204, 0, 0));
    overFgColor = TerminalConfig.getColor("kds.over.fg", Color.white);
    
    initialColorComponent = AppConfig.getInt("kds.initial.component", 0);
    warningColorComponent = AppConfig.getInt("kds.warning.component", 0);
    overColorComponent = AppConfig.getInt("kds.over.component", 0);
    
    backColor = initialBgColor;
    textColor = initialFgColor;
    applicableColorComponentIndex = Integer.valueOf(initialColorComponent);
    
    timerLabel.setFont(timerLabel.getFont().deriveFont(1, PosUIManager.getDefaultFontSize() + 2));
    timerLabel.setHorizontalAlignment(4);
    timerLabel.setOpaque(false);
    timerLabel.setForeground(Color.black);
    timerLabel.setBorder(BorderFactory.createEmptyBorder(2, 8, 2, 8));
    
    if (AppConfig.getString("YellowTimeOut") != null) {
      timeOutValueYellow = Integer.parseInt(AppConfig.getString("YellowTimeOut"));
    }
    
    if (AppConfig.getString("RedTimeOut") != null) {
      timeOutValueRed = Integer.parseInt(AppConfig.getString("RedTimeOut"));
    }
    actionPerformed(null);
    add(timerLabel);
  }
  
  public void actionPerformed(ActionEvent e)
  {
    long currentTimeMillis = new Instant().getMillis();
    long createTimeMillis = date.getTime();
    long diff = createTimeMillis - currentTimeMillis;
    
    Interval interval = null;
    if (diff > 0L) {
      interval = new Interval(currentTimeMillis, date.getTime());
    }
    else {
      interval = new Interval(date.getTime(), currentTimeMillis);
    }
    Duration duration = interval.toDuration();
    
    int oldColorCode = backColor != null ? backColor.getRGB() : -1;
    if ((timeOutValueYellow < duration.getStandardSeconds()) && (timeOutValueRed > duration.getStandardSeconds())) {
      backColor = warningBgColor;
      textColor = warningFgColor;
      applicableColorComponentIndex = Integer.valueOf(warningColorComponent);
    }
    else if (timeOutValueRed < duration.getStandardSeconds()) {
      backColor = overBgColor;
      textColor = overFgColor;
      applicableColorComponentIndex = Integer.valueOf(overColorComponent);
    }
    else {
      backColor = initialBgColor;
      textColor = initialFgColor;
      applicableColorComponentIndex = Integer.valueOf(initialColorComponent);
    }
    timerLabel.setForeground(textColor);
    timerLabel.setText(format(duration.getStandardHours()) + ":" + 
      format(duration.getStandardMinutes() % 60L) + ":" + format(duration.getStandardSeconds() % 60L));
    
    boolean colorChanged = (backColor != null) && (backColor.getRGB() != oldColorCode);
    if ((colorChangeListener != null) && (colorChanged)) {
      colorChangeListener.stateChanged(null);
    }
  }
  
  public void setBackgroundColor(Color background) {
    timerLabel.setOpaque(true);
    timerLabel.setBackground(background);
  }
  
  public void setForegroundColor(Color foreground) {
    timerLabel.setForeground(foreground);
  }
  
  public Color getBackgroundColor() {
    return applicableColorComponentIndex.intValue() == 0 ? KitchenDisplayView.getInstance().getBackground() : backColor;
  }
  
  private String format(long value) {
    String stringValue = String.valueOf(value);
    return "0" + value;
  }
  
  public void start() {
    updateTimer.start();
  }
  
  public void stop() {
    updateTimer.stop();
  }
}
