package com.floreantpos.ui.kitchendisplay;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.actions.LogoutAction;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.model.KitchenTicket;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Printer;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.dao.KitchenTicketDAO;
import com.floreantpos.model.ext.KitchenStatus;
import com.floreantpos.swing.PaginatedListModel;
import com.floreantpos.swing.PosBlinkButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.PosGuiUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;
import java.util.Stack;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import jiconfont.icons.FontAwesome;
import jiconfont.swing.IconFontSwing;
import net.miginfocom.swing.MigLayout;
import org.hibernate.StaleStateException;





public class KitchenTicketListPanel
  extends JPanel
  implements ActionListener, KitchenTicketActionListener
{
  private static final int HORIZONTAL_GAP = 0;
  private static final int VERTICAL_GAP = 0;
  private PaginatedListModel dataModel = new PaginatedListModel();
  
  private JPanel selectionTicketViewsPanel;
  
  private TitledBorder border;
  private KitchenActionButton btnLogout;
  private KitchenActionButton btnFilter;
  private KitchenActionButton btnKitchenDispatch;
  private KitchenActionButton btnBack;
  private KitchenActionButton btnNext;
  private KitchenActionButton btnPrev;
  private KitchenActionButton btnBumpAll;
  private int wrapCount = 4;
  
  private List<Printer> selectedPrinter;
  
  private List<OrderType> selectedTicketType;
  private PosBlinkButton btnRefresh;
  private KitchenActionButton btnUndo;
  private Stack<KitchenTicket> stack = new Stack();
  
  private Timer viewUpdateTimer;
  private Date lastUpdateTime;
  private static final Color defaultButtonBg = Color.white;
  private static final Color defaultButtonTextColor = Color.black;
  private KitchenActionButton btnOption;
  
  public KitchenTicketListPanel()
  {
    border = new TitledBorder("");
    border.setTitleJustification(2);
    
    setLayout(new BorderLayout(0, 0));
    setBorder(new CompoundBorder(border, new EmptyBorder(0, 0, 0, 0)));
    
    selectionTicketViewsPanel = new JPanel(new MigLayout("fillx"));
    selectionTicketViewsPanel.setOpaque(false);
    add(selectionTicketViewsPanel);
    
    Dimension size = PosUIManager.getSize(90, 40);
    
    Color undoBtnColor = Color.white;
    Color optionBtnColor = Color.white;
    Color filterBtnColor = Color.white;
    
    btnLogout = new KitchenActionButton("", defaultButtonBg, defaultButtonTextColor);
    btnBack = new KitchenActionButton(POSConstants.CLOSE.toUpperCase(), defaultButtonBg, defaultButtonTextColor);
    btnKitchenDispatch = new KitchenActionButton(Messages.getString("KitchenTicketListPanel.2").toUpperCase(), defaultButtonBg, defaultButtonTextColor);
    btnFilter = new KitchenActionButton(POSConstants.FILTER.toUpperCase(), filterBtnColor);
    btnOption = new KitchenActionButton(Messages.getString("OPTION").toUpperCase(), optionBtnColor);
    btnUndo = new KitchenActionButton(Messages.getString("KitchenTicketListPanel.1").toUpperCase(), undoBtnColor);
    btnPrev = new KitchenActionButton(POSConstants.CAPITAL_PREV, defaultButtonBg, defaultButtonTextColor);
    btnNext = new KitchenActionButton(POSConstants.CAPITAL_NEXT, defaultButtonBg, defaultButtonTextColor);
    btnBumpAll = new KitchenActionButton(Messages.getString("BUMP_ALL"), defaultButtonBg, defaultButtonTextColor);
    
    btnRefresh = new PosBlinkButton()
    {
      public Color getBackground() {
        if (btnRefresh == null)
          return Color.white;
        return super.getBackground();
      }
      
      public Color getForeground()
      {
        if ((btnRefresh == null) || (!btnRefresh.isBlinking()))
          return Color.black;
        return Color.white;
      }
    };
    btnRefresh.setFont(btnRefresh.getFont().deriveFont(1));
    btnBack.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        KitchenTicketListPanel.this.close();
      }
    });
    btnKitchenDispatch.setBtnToogle(true);
    btnKitchenDispatch.setCellButton(false);
    btnKitchenDispatch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        btnKitchenDispatch.setSelected(!btnKitchenDispatch.isSelected());
        getDataModel().setCurrentRowIndex(0);
        if (btnKitchenDispatch.isSelected()) {
          btnBumpAll.setText(Messages.getString("DISPATCH_ALL"));
        }
        else {
          btnBumpAll.setText(Messages.getString("BUMP_ALL"));
        }
        updateKDSView();
      }
    });
    btnOption.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        KitchenDisplayConfigDialog dialog = new KitchenDisplayConfigDialog();
        dialog.pack();
        dialog.open();
        if (dialog.isCanceled())
          return;
        dataModel.setCurrentRowIndex(0);
        updateKDSView();
        KitchenDisplayView.getInstance().updateView();
      }
      
    });
    btnBumpAll.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        KitchenTicketListPanel.this.bumpOrDispatchAll();
      }
      

    });
    btnLogout.setAction(new LogoutAction(true, false));
    btnLogout.setText(POSConstants.LOGOUT.toUpperCase());
    
    btnUndo.setEnabled(false);
    btnUndo.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        unbump();
      }
    });
    btnRefresh.setText(POSConstants.REFRESH.toUpperCase());
    btnRefresh.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dataModel.setCurrentRowIndex(0);
        updateKDSView();
      }
    });
    btnRefresh.setBorder(null);
    btnRefresh.setOpaque(true);
    
    ScrollAction action = new ScrollAction(null);
    btnPrev.addActionListener(action);
    btnNext.addActionListener(action);
    
    btnNext.setEnabled(false);
    btnPrev.setEnabled(false);
    btnFilter.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        KitchenTicketListPanel.this.doFilter();
      }
      
    });
    int iconSize = PosUIManager.getSize(16);
    btnPrev.setIcon(IconFontSwing.buildIcon(FontAwesome.BACKWARD, iconSize));
    btnNext.setIcon(IconFontSwing.buildIcon(FontAwesome.FORWARD, iconSize));
    btnNext.setHorizontalTextPosition(2);
    btnRefresh.setIcon(IconFontSwing.buildIcon(FontAwesome.REFRESH, iconSize));
    btnFilter.setIcon(IconFontSwing.buildIcon(FontAwesome.FILTER, iconSize));
    btnUndo.setIcon(IconFontSwing.buildIcon(FontAwesome.UNDO, iconSize));
    btnBack.setIcon(IconFontSwing.buildIcon(FontAwesome.WINDOW_CLOSE, iconSize));
    btnLogout.setIcon(IconFontSwing.buildIcon(FontAwesome.ARROW_CIRCLE_O_LEFT, iconSize));
    
    JPanel optionPanel = new JPanel(new MigLayout("ins 0,hidemode 3", "[]2px[]", ""));
    optionPanel.setOpaque(false);
    String constraints = "w " + (width + 7) + "!, growy,gap 1";
    
    optionPanel.add(btnLogout, constraints);
    optionPanel.add(btnBack, constraints);
    
    JPanel actionButtonPanel = new JPanel(new MigLayout("fill,hidemode 3, ins 0 2 3 2", "[][grow][]", "[]"));
    
    JSeparator sep = new JSeparator();
    sep.setBackground(Color.white);
    sep.setForeground(Color.white);
    sep.setPreferredSize(PosUIManager.getSize(0, 5));
    
    actionButtonPanel.add(sep, "growx,span");
    actionButtonPanel.add(optionPanel, "growy");
    
    actionButtonPanel.add(btnBumpAll, "split 6,center," + constraints);
    actionButtonPanel.add(btnKitchenDispatch, constraints);
    actionButtonPanel.add(btnFilter, constraints);
    actionButtonPanel.add(btnOption, constraints);
    actionButtonPanel.add(btnUndo, constraints);
    actionButtonPanel.add(btnRefresh, constraints + "");
    
    actionButtonPanel.add(btnPrev, "split 3,right," + constraints);
    actionButtonPanel.add(btnNext, constraints + ",gapleft 1");
    
    add(actionButtonPanel, "South");
    actionButtonPanel.setBackground(Color.black);
    
    viewUpdateTimer = new Timer(5000, this);
    viewUpdateTimer.setRepeats(true);
  }
  
  public void setBackButtonVisible(boolean b) {
    btnBack.setVisible(b);
    btnLogout.setVisible(!b);
  }
  
  private void doFilter() {
    KitchenFilterDialog dialog = new KitchenFilterDialog();
    dialog.open();
    
    if (dialog.isCanceled())
      return;
    selectedPrinter = dialog.getPrinter();
    selectedTicketType = dialog.getOrderType();
    getDataModel().setCurrentRowIndex(0);
    updateKDSView();
  }
  
  public void updateKDSView() {
    stopTimer();
    reset();
    try {
      Terminal terminal = Application.getInstance().getTerminal();
      int pageSize = TerminalConfig.getKDSTicketsPerPage();
      wrapCount = ((int)Math.round(pageSize / 2.0D));
      dataModel.setPageSize(pageSize);
      
      KitchenTicketDAO.getInstance().loadItems(terminal, selectedPrinter, selectedTicketType, btnKitchenDispatch.isSelected(), dataModel);
      
      btnRefresh.setBlinking(false);
      Date dbLastUpdateTime = KitchenTicketDAO.getInstance().getLastUpdateDate();
      if (dbLastUpdateTime != null) {
        lastUpdateTime = new Date(dbLastUpdateTime.getTime());
      }
      setDataModel(dataModel);
      stack.clear();
      refreshUndoRedo();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    } finally {
      restartTimer();
    }
  }
  
  protected void setDataModel(PaginatedListModel listModel) {
    dataModel = listModel;
    renderItems();
  }
  
  protected void updateButton() {
    boolean hasNext = dataModel.hasNext();
    boolean hasPrevious = dataModel.hasPrevious();
    btnNext.setVisible((hasNext) || (hasPrevious));
    btnPrev.setVisible((hasNext) || (hasPrevious));
    btnPrev.setEnabled(hasPrevious);
    btnNext.setEnabled(hasNext);
  }
  
  public PaginatedListModel getDataModel() {
    return dataModel;
  }
  
  public void reset() {
    Component[] components = selectionTicketViewsPanel.getComponents();
    for (Component component : components) {
      if ((component instanceof KitchenTicketView)) {
        KitchenTicketView kitchenTicketView = (KitchenTicketView)component;
        kitchenTicketView.stopTimer();
      }
    }
    selectionTicketViewsPanel.removeAll();
    selectionTicketViewsPanel.repaint();
    btnNext.setEnabled(false);
    btnPrev.setEnabled(false);
  }
  
  protected void renderItems() {
    try {
      reset();
      
      if ((dataModel == null) || (dataModel.getSize() == 0)) {
        btnNext.setEnabled(dataModel.hasNext());
        btnPrev.setEnabled(dataModel.hasPrevious());
        return;
      }
      int numOfTickets = TerminalConfig.getKDSTicketsPerPage();
      if (numOfTickets <= 5) {
        selectionTicketViewsPanel.setLayout(new GridLayout(1, numOfTickets, 5, 5));
      }
      else {
        int colCount = (int)Math.ceil(numOfTickets / 2.0D);
        selectionTicketViewsPanel.setLayout(new GridLayout(2, colCount, 5, 5));
      }
      selectionTicketViewsPanel.setOpaque(false);
      
      for (int i = 0; i < numOfTickets; i++)
        if (i >= dataModel.getSize()) {
          TransparentPanel transparentPanel = new TransparentPanel();
          selectionTicketViewsPanel.add(transparentPanel);
        }
        else {
          Object item = dataModel.getElementAt(i);
          JPanel itemPanel = createKitchenTicket(item, i);
          if (itemPanel != null)
          {

            selectionTicketViewsPanel.add(itemPanel); }
        }
      btnNext.setEnabled(dataModel.hasNext());
      btnPrev.setEnabled(dataModel.hasPrevious());
      
      refreshUndoRedo();
      revalidate();
      repaint();
    } catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage(), e);
    }
  }
  
  private class ScrollAction implements ActionListener {
    private ScrollAction() {}
    
    public void actionPerformed(ActionEvent e) {
      try { Object source = e.getSource();
        if (source == btnPrev) {
          scrollUp();
        }
        else if (source == btnNext) {
          scrollDown();
        }
      } catch (Exception e2) {
        POSMessageDialog.showError(Application.getPosWindow(), e2.getMessage(), e2);
      }
    }
  }
  
  protected void scrollDown()
  {
    if (!dataModel.hasNext())
      return;
    stack.clear();
    dataModel.setCurrentRowIndex(dataModel.getNextRowIndex());
    updateKDSView();
  }
  
  protected void scrollUp() {
    if (!dataModel.hasPrevious())
      return;
    stack.clear();
    dataModel.setCurrentRowIndex(dataModel.getPreviousRowIndex());
    updateKDSView();
  }
  
  protected JPanel createKitchenTicket(Object item, int index) {
    KitchenTicket kitchenTicket = (KitchenTicket)item;
    kitchenTicket.setSortOrder(Integer.valueOf(index));
    if (kitchenTicket.getId() == null) {
      JPanel panel = new JPanel();
      panel.setOpaque(false);
      return panel;
    }
    KitchenTicketView kitchenTicketView = new KitchenTicketView(kitchenTicket, this);
    kitchenTicketView.putClientProperty("key", Integer.valueOf(index + 1));
    return kitchenTicketView;
  }
  
  public void setSelectedKey(int key) {
    if (PosGuiUtil.isModalDialogShowing()) {
      return;
    }
    if (key == 10) {
      updateKDSView();
      return;
    }
    if (key == 81) {
      Application.getInstance().doLogout();
      return;
    }
    if ((key == 85) && (!btnKitchenDispatch.isSelected())) {
      unbump();
      return;
    }
    Component[] components = selectionTicketViewsPanel.getComponents();
    if (components.length == 0)
      return;
    if (key == 78) {
      scrollDown();
      return;
    }
    if (key == 80) {
      scrollUp();
      return;
    }
    int previousSelectedIndex = 0;
    int selectedIndex = 0;
    int count = 0;
    
    boolean active = false;
    boolean numberSelected = false;
    
    for (Component component : components) {
      if ((component instanceof KitchenTicketView)) {
        KitchenTicketView kitchenTicketView = (KitchenTicketView)component;
        if ((kitchenTicketView.isKeySelected()) && ((key == 66) || (key == 32))) {
          kitchenTicketView.fireBumpSelected();
          return;
        }
        if ((key != 37) && (key != 39) && (key != 40) && (key != 38)) {
          boolean selected = kitchenTicketView.keySelected(key);
          if (selected) {
            numberSelected = true;
          }
        }
        if (kitchenTicketView.isKeySelected()) {
          active = true;
          selectedIndex = count;
          previousSelectedIndex = count;
        }
      }
      count++;
    }
    if (numberSelected) {
      return;
    }
    
    boolean scrollSelected = true;
    if ((active) && 
      (key != 32)) {
      switch (key) {
      case 39: 
        selectedIndex++;
        if (selectedIndex >= wrapCount * 2) {
          selectedIndex = 0;
        }
        break;
      case 37: 
        selectedIndex--;
        if (selectedIndex < 0)
          return;
        break;
      case 40: 
        selectedIndex += wrapCount;
        if (selectedIndex >= wrapCount * 2)
          return;
        break;
      case 38: 
        selectedIndex -= wrapCount;
        if (selectedIndex >= wrapCount * 2)
          return;
        break;
      default: 
        scrollSelected = false;
      }
      
    }
    
    if (scrollSelected) {
      if ((selectedIndex < 0) || (components.length < selectedIndex + 1)) {
        return;
      }
      Component component = components[selectedIndex];
      if ((component == null) || (!(component instanceof KitchenTicketView)))
        return;
      KitchenTicketView kitchenTicketView = (KitchenTicketView)component;
      kitchenTicketView.setSelected(true);
      
      if (selectedIndex != previousSelectedIndex) {
        KitchenTicketView previousTicketView = (KitchenTicketView)components[previousSelectedIndex];
        if (previousTicketView == null)
          return;
        previousTicketView.setSelected(false);
      }
    }
  }
  
  public PaginatedListModel getModel() {
    return dataModel;
  }
  
  public void refreshUndoRedo() {
    btnUndo.setEnabled(!stack.isEmpty());
  }
  
  public boolean isDispatchMode()
  {
    if (btnKitchenDispatch != null) {
      return btnKitchenDispatch.isSelected();
    }
    return false;
  }
  
  public void dispatch(KitchenTicketView view)
  {
    KitchenTicket kitchenTicket = view.getTicket();
    KitchenTicketDAO.getInstance().bumpOrUnbump(kitchenTicket, KitchenStatus.DISPATCHED, KitchenStatus.DISPATCHED, false);
    KitchenTicketDAO.getInstance().delete(kitchenTicket);
    updateKDSView();
  }
  
  public void bump(KitchenTicketView view)
  {
    try {
      KitchenTicket kitchenTicket = view.getTicket();
      KitchenTicketDAO.getInstance().bumpOrUnbump(kitchenTicket, KitchenStatus.BUMP, KitchenStatus.BUMP, true);
      lastUpdateTime = kitchenTicket.getLastUpdateDate();
      stack.push(kitchenTicket);
      dataModel.getDataList().set(kitchenTicket.getSortOrder().intValue(), new KitchenTicket());
      renderItems();
      refreshUndoRedo();
    } catch (StaleStateException e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), Messages.getString("KitchenTicketListPanel.0"), e);
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    }
  }
  
  public void bumpAll() {
    try {
      List<KitchenTicket> findAllOpen = KitchenTicketDAO.getInstance().findAllOpen();
      
      if (findAllOpen.isEmpty()) {
        POSMessageDialog.showError(this, "No data to bump");
        return;
      }
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Do you want to bump all tickets?", "Confirm");
      if (option != 0) {
        return;
      }
      for (KitchenTicket kitchenTicket : findAllOpen) {
        KitchenTicketDAO.getInstance().bumpOrUnbump(kitchenTicket, KitchenStatus.BUMP, KitchenStatus.BUMP, true);
      }
      dataModel.getDataList().clear();
      lastUpdateTime = new Date();
      renderItems();
      refreshUndoRedo();
      
      dataModel.getDataList().clear();
      lastUpdateTime = new Date();
      renderItems();
      refreshUndoRedo();
    }
    catch (StaleStateException e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), Messages.getString("KitchenTicketListPanel.0"), e);
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    }
  }
  
  private void bumpOrDispatchAll() {
    try {
      if (btnKitchenDispatch.isSelected()) {
        int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Do you want to dispatch all tickets?", "Confirm");
        if (option != 0) {
          return;
        }
        KitchenTicketDAO.getInstance().dispatchAll();
        updateKDSView();
      }
      else {
        bumpAll();
        updateKDSView();
      }
    } catch (PosException e) {
      POSMessageDialog.showError(this, e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage(), e);
    }
  }
  
  public void unbump() {
    if (stack.isEmpty())
      return;
    KitchenTicket kitchenTicket = (KitchenTicket)stack.pop();
    try {
      KitchenTicketDAO.getInstance().bumpOrUnbump(kitchenTicket, KitchenStatus.WAITING, KitchenStatus.WAITING, false);
      lastUpdateTime = kitchenTicket.getLastUpdateDate();
      dataModel.getDataList().set(kitchenTicket.getSortOrder().intValue(), kitchenTicket);
      renderItems();
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    }
  }
  
  public boolean isBlinkingRefreshButton() {
    return btnRefresh.isBlinking();
  }
  
  public void setBlinkingRefreshButton(boolean b) {
    btnRefresh.setBlinking(b);
  }
  
  private void close() {
    Window window = getWindow();
    if (window == null) {
      RootView.getInstance().showDefaultView();
    }
    else {
      window.dispose();
    }
  }
  
  private Window getWindow() {
    Window windowAncestor = SwingUtilities.getWindowAncestor(this);
    if ((windowAncestor instanceof JFrame)) {
      return windowAncestor;
    }
    return null;
  }
  
  public void startTimer() {
    if (!viewUpdateTimer.isRunning())
      viewUpdateTimer.start();
  }
  
  public void stopTimer() {
    if (viewUpdateTimer.isRunning()) {
      viewUpdateTimer.stop();
    }
  }
  
  public void restartTimer() {
    viewUpdateTimer.restart();
  }
  
  public void actionPerformed(ActionEvent e)
  {
    try {
      if (PosGuiUtil.isModalDialogShowing()) {
        return;
      }
      viewUpdateTimer.stop();
      selectionTicketViewsPanel.repaint();
      if (isBlinkingRefreshButton()) {
        return;
      }
      Date dbLastUpdatedTime = KitchenTicketDAO.getInstance().getLastUpdateDate();
      if (dbLastUpdatedTime == null) {
        return;
      }
      if ((lastUpdateTime != null) && (new Date(dbLastUpdatedTime.getTime()).after(lastUpdateTime))) {
        setBlinkingRefreshButton(true);
      }
    } finally {
      viewUpdateTimer.restart();
    }
  }
}
