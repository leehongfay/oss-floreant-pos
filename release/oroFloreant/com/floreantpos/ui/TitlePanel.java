package com.floreantpos.ui;

import com.floreantpos.POSConstants;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.UIManager;






















public class TitlePanel
  extends TransparentPanel
{
  protected JSeparator jSeparator1;
  protected JLabel lblTitle;
  
  public TitlePanel()
  {
    initComponents();
  }
  





  private void initComponents()
  {
    lblTitle = new JLabel();
    lblTitle.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    jSeparator1 = new JSeparator();
    
    lblTitle.setFont(getTitleFont());
    lblTitle.setForeground(getTitleColor());
    lblTitle.setText(POSConstants.TITLE);
    
    setLayout(new BorderLayout(5, 5));
    add(lblTitle);
    add(jSeparator1, "South");
    
    setPreferredSize(PosUIManager.getSize(300, 60));
  }
  
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    
    Graphics2D g2 = (Graphics2D)g;
    int x = 0;int y = 0;
    float width = getWidth();
    float height = getHeight();
    
    Color color1 = Color.WHITE;
    Color color2 = getBackground();
    g2.setPaint(new GradientPaint(x, y, color1, width, height, color2));
    g2.fillRect(x, y, (int)width, (int)height);
  }
  





  private Font getTitleFont()
  {
    Font f = lblTitle.getFont();
    f = f.deriveFont(1, PosUIManager.getTitleFontSize());
    return f;
  }
  
  private Color getTitleColor() {
    return UIManager.getColor("TitledBorder.titleColor");
  }
  
  public String getTitle() {
    return lblTitle.getText();
  }
  
  public void setTitle(String title) {
    lblTitle.setText(title);
  }
  
  public void setTextAlignment(int alignment) {
    lblTitle.setHorizontalAlignment(alignment);
  }
}
