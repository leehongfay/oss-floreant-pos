package com.floreantpos.ui.order;

import com.floreantpos.swing.ListTableModel;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DragSource;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.activation.DataHandler;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTable.DropLocation;
import javax.swing.TransferHandler;
import javax.swing.TransferHandler.DropLocation;
import javax.swing.TransferHandler.TransferSupport;

class TableDataTransferHandler extends TransferHandler
{
  private final DataFlavor localObjectFlavor;
  private int[] indices;
  private int addIndex = -1;
  private int addCount;
  private JComponent source;
  private Object[] transferedObjects;
  private Cursor cursor = DragSource.DefaultMoveNoDrop;
  
  protected TableDataTransferHandler()
  {
    localObjectFlavor = new javax.activation.ActivationDataFlavor([Ljava.lang.Object.class, "application/x-java-jvm-local-objectref", "Array of items");
  }
  
  protected Transferable createTransferable(JComponent c)
  {
    source = c;
    JTable table = (JTable)c;
    ListTableModel model = (ListTableModel)table.getModel();
    List<Object> list = new ArrayList();
    indices = table.getSelectedRows();
    for (int i : indices) {
      list.add(model.getRowData(i));
    }
    transferedObjects = list.toArray();
    return new DataHandler(transferedObjects, localObjectFlavor.getMimeType());
  }
  
  public boolean canImport(TransferHandler.TransferSupport info)
  {
    JTable table = (JTable)info.getComponent();
    boolean isDroppable = (info.isDrop()) && (info.isDataFlavorSupported(localObjectFlavor));
    table.setCursor(isDroppable ? DragSource.DefaultMoveDrop : cursor);
    return isDroppable;
  }
  
  public int getSourceActions(JComponent c)
  {
    return 2;
  }
  
  public boolean importData(TransferHandler.TransferSupport info)
  {
    if (!canImport(info)) {
      return false;
    }
    TransferHandler.DropLocation tdl = info.getDropLocation();
    if (!(tdl instanceof JTable.DropLocation)) {
      return false;
    }
    JTable.DropLocation dl = (JTable.DropLocation)tdl;
    JTable target = (JTable)info.getComponent();
    CourseOrganizeTableModel model = (CourseOrganizeTableModel)target.getModel();
    
    int index = dl.getRow();
    int max = model.getRowCount();
    if ((index < 0) || (index > max)) {
      index = max;
    }
    addIndex = index;
    target.setCursor(Cursor.getPredefinedCursor(0));
    try {
      Object[] values = (Object[])info.getTransferable().getTransferData(localObjectFlavor);
      if (java.util.Objects.equals(source, target)) {
        addCount = values.length;
      }
      for (int i = 0; i < values.length; i++) {
        int idx = index++;
        model.insertRow(idx, values[i]);
        target.getSelectionModel().addSelectionInterval(idx, idx);
      }
      return true;
    } catch (UnsupportedFlavorException|IOException ex) {
      ex.printStackTrace();
    }
    return false;
  }
  
  protected void exportDone(JComponent c, Transferable data, int action)
  {
    cleanup(c, action == 2);
  }
  
  private void cleanup(JComponent c, boolean remove) {
    if ((remove) && (indices != null)) {
      c.setCursor(Cursor.getPredefinedCursor(0));
      CourseOrganizeTableModel model = (CourseOrganizeTableModel)((JTable)c).getModel();
      if (addCount > 0) {
        for (int i = 0; i < indices.length; i++) {
          if (indices[i] >= addIndex) {
            indices[i] += addCount;
          }
        }
      }
      for (int i = indices.length - 1; i >= 0; i--) {
        model.delete(indices[i]);
      }
    }
    indices = null;
    addCount = 0;
    addIndex = -1;
  }
  
  public BufferedImage createImage(Component panel, int w, int h) {
    BufferedImage bi = new BufferedImage(w, h, 1);
    Graphics2D g = bi.createGraphics();
    panel.print(g);
    return bi;
  }
}
