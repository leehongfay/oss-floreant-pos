package com.floreantpos.ui.order;

import com.floreantpos.Messages;
import com.floreantpos.model.ITicketItem;
import com.floreantpos.swing.ListTableModel;
import java.util.List;

















public class CourseOrganizeTableModel
  extends ListTableModel<ITicketItem>
{
  protected String[] columnNames = { Messages.getString("TicketViewerTableModel.0") };
  
  public CourseOrganizeTableModel() {}
  
  public void setItems(List<ITicketItem> items)
  {
    if (items == null) {
      super.setRows(null);
      return;
    }
    setRows(items);
    fireTableDataChanged();
  }
  
  public int getColumnCount()
  {
    return columnNames.length;
  }
  
  public String getColumnName(int column)
  {
    return columnNames[column];
  }
  
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    ITicketItem ticketItem = (ITicketItem)getRowData(rowIndex);
    
    if (ticketItem == null) {
      return null;
    }
    
    switch (columnIndex) {
    case 0: 
      return ticketItem.getNameDisplay();
    case 1: 
      return ticketItem.getSubTotalAmountDisplay();
    }
    
    return null;
  }
  
  public Object delete(int index) {
    if ((index < 0) || (index >= getRowCount())) {
      return null;
    }
    Object object = getRowData(index);
    super.deleteItem(index);
    fireTableRowsDeleted(index, index);
    return object;
  }
  
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    return false;
  }
  
  public void update() {
    fireTableDataChanged();
  }
  
  public void insertRow(int idx, Object value) {
    ITicketItem item = (ITicketItem)value;
    getRows().add(idx, item);
    fireTableDataChanged();
  }
}
