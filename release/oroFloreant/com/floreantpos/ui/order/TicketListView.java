package com.floreantpos.ui.order;

import com.floreantpos.ITicketList;
import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.model.DataUpdateInfo;
import com.floreantpos.model.PaymentStatusFilter;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.DataUpdateInfoDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.services.TicketService;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosBlinkButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.OrderFilterPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.TicketListUpdateListener;
import com.floreantpos.ui.dialog.GlobalInputDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import com.floreantpos.util.PosGuiUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.ListSelectionModel;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.plaf.basic.BasicScrollBarUI;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.ColumnControlButton;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;





















public class TicketListView
  extends JPanel
  implements DataChangeListener, ITicketList
{
  private JXTable ticketListTable;
  private TicketListTableModel ticketListTableModel;
  private OrderFilterPanel orderFiltersPanel;
  private PosBlinkButton btnRefresh;
  private PosButton btnPrevious;
  private PosButton btnNext;
  private TableColumnModelExt columnModel;
  private ArrayList<TicketListUpdateListener> ticketUpdateListenerList = new ArrayList();
  
  private boolean isCustomerHistoryOpen;
  private Date lastUpdateTime;
  private Timer lastUpateCheckTimer = new Timer(5000, new TaskLastUpdateCheck(null));
  private String customerId;
  private POSToggleButton btnOrderFilters;
  
  public TicketListView()
  {
    setLayout(new BorderLayout());
    orderFiltersPanel = new OrderFilterPanel(this);
    add(orderFiltersPanel, "North");
    createTicketTable();
    updateTicketList();
  }
  
  public TicketListView(String customerId, boolean customerHistory) {
    isCustomerHistoryOpen = customerHistory;
    setLayout(new BorderLayout());
    
    createTicketTable();
    updateTicketList();
  }
  
  private void createTicketTable() {
    ticketListTable = new JXTable();
    ticketListTable.setSortable(true);
    ticketListTable.setSelectionMode(0);
    ticketListTable.setColumnControlVisible(true);
    ticketListTable.setModel(this.ticketListTableModel = new TicketListTableModel());
    ticketListTableModel.setPageSize(TerminalConfig.getOrderViewPageSize());
    ticketListTable.setRowHeight(PosUIManager.getSize(60));
    ticketListTable.setAutoResizeMode(3);
    ticketListTable.setDefaultRenderer(Object.class, new PosTableRenderer());
    ticketListTable.setGridColor(Color.LIGHT_GRAY);
    ticketListTable.getTableHeader().setPreferredSize(new Dimension(100, PosUIManager.getSize(40)));
    
    columnModel = ((TableColumnModelExt)ticketListTable.getColumnModel());
    columnModel.getColumn(0).setPreferredWidth(65);
    columnModel.getColumn(1).setPreferredWidth(30);
    columnModel.getColumn(2).setPreferredWidth(20);
    columnModel.getColumn(3).setPreferredWidth(100);
    columnModel.getColumn(4).setPreferredWidth(100);
    
    if (isCustomerHistoryOpen) {
      columnModel.getColumnExt(1).setVisible(false);
      columnModel.getColumnExt(1).setVisible(false);
      columnModel.getColumnExt(6).setVisible(false);
      columnModel.getColumnExt(8).setVisible(false);
      createScrollPane();
      return;
    }
    
    restoreTableColumnsVisibility();
    addTableColumnListener();
    createScrollPane();
  }
  
  private void addTableColumnListener()
  {
    columnModel.addColumnModelListener(new TableColumnModelListener()
    {
      public void columnSelectionChanged(ListSelectionEvent e) {}
      


      public void columnRemoved(TableColumnModelEvent e)
      {
        TicketListView.this.saveHiddenColumns();
      }
      


      public void columnMoved(TableColumnModelEvent e) {}
      


      public void columnMarginChanged(ChangeEvent e) {}
      


      public void columnAdded(TableColumnModelEvent e)
      {
        TicketListView.this.saveHiddenColumns();
      }
    });
  }
  
  private void restoreTableColumnsVisibility() {
    String recordedSelectedColumns = TerminalConfig.getTicketListViewHiddenColumns();
    TableColumnModelExt columnModel = (TableColumnModelExt)ticketListTable.getColumnModel();
    
    if (recordedSelectedColumns.isEmpty()) {
      return;
    }
    String[] str = recordedSelectedColumns.split("\\*");
    for (int i = 0; i < str.length; i++) {
      Integer columnIndex = Integer.valueOf(Integer.parseInt(str[i]));
      columnModel.getColumnExt(columnIndex.intValue() - i).setVisible(false);
    }
  }
  
  private void saveHiddenColumns() {
    List<TableColumn> columns = columnModel.getColumns(true);
    List<Integer> indices = new ArrayList();
    for (TableColumn tableColumn : columns) {
      TableColumnExt c = (TableColumnExt)tableColumn;
      if (!c.isVisible()) {
        indices.add(Integer.valueOf(c.getModelIndex()));
      }
    }
    saveTableColumnsVisibility(indices);
  }
  
  private void createScrollPane()
  {
    if (!isCustomerHistoryOpen) {
      btnOrderFilters = new POSToggleButton();
      btnOrderFilters.setIcon(IconFactory.getIcon("/ui_icons/", "filter-16.png"));
      updateOrderFilterPanel(TerminalConfig.isOrderFilterPanelVisible());
    }
    

    btnRefresh = new PosBlinkButton(IconFactory.getIcon("/ui_icons/", "refresh-16.png"));
    btnPrevious = new PosButton(IconFactory.getIcon("/ui_icons/", "previous-24.png"));
    btnNext = new PosButton(IconFactory.getIcon("/ui_icons/", "next-24.png"));
    
    createActionHandlers();
    
    PosScrollPane scrollPane = new PosScrollPane(ticketListTable, 20, 31);
    scrollPane.getVerticalScrollBar().setUI(new CustomScrollbarUI());
    
    int height = PosUIManager.getSize(40);
    
    JPanel topButtonPanel = new JPanel(new MigLayout("ins 0", "grow", ""));
    ColumnControlButton controlButton = new ColumnControlButton(ticketListTable);
    if (!isCustomerHistoryOpen) {
      topButtonPanel.add(controlButton, "h " + height + "!, grow, wrap");
    }
    topButtonPanel.add(btnRefresh, "h " + height + "!, grow, wrap");
    topButtonPanel.add(btnPrevious, "h " + height + "!, grow, wrap");
    
    JPanel downButtonPanel = new JPanel(new MigLayout("ins 0", "grow", ""));
    downButtonPanel.add(btnNext, "h " + height + "!, grow,gaptop 5, wrap");
    
    if (!isCustomerHistoryOpen) {
      downButtonPanel.add(btnOrderFilters, "h " + height + "!,grow, wrap");
    }
    
    JPanel tableButtonPanel = new JPanel(new BorderLayout());
    tableButtonPanel.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));
    tableButtonPanel.setPreferredSize(PosUIManager.getSize(52, 0));
    tableButtonPanel.add(topButtonPanel, "North");
    tableButtonPanel.add(downButtonPanel, "South");
    JScrollBar verticalScrollBar = scrollPane.getVerticalScrollBar();
    tableButtonPanel.add(verticalScrollBar);
    
    add(scrollPane);
    add(tableButtonPanel, "East");
  }
  
  public void createActionHandlers()
  {
    btnPrevious.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        if (ticketListTableModel.hasPrevious()) {
          ticketListTableModel.setCurrentRowIndex(ticketListTableModel.getPreviousRowIndex());
          TicketDAO.getInstance().loadTickets(ticketListTableModel);
        }
        updateButtonStatus();
      }
      
    });
    btnNext.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        if (ticketListTableModel.hasNext()) {
          ticketListTableModel.setCurrentRowIndex(ticketListTableModel.getNextRowIndex());
          TicketDAO.getInstance().loadTickets(ticketListTableModel);
        }
        updateButtonStatus();
      }
      
    });
    btnRefresh.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        getTableModel().setCurrentRowIndex(0);
        if (customerId != null) {
          updateCustomerTicketList(customerId);
        }
        else {
          updateTicketList();
        }
      }
    });
    
    if (!isCustomerHistoryOpen) {
      btnOrderFilters.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          orderFiltersPanel.setCollapsed(!orderFiltersPanel.isCollapsed());
          TerminalConfig.setOrderFilterPanelVisible(btnOrderFilters.isSelected());
        }
      });
    }
  }
  
  public void updateButtonStatus() {
    btnNext.setEnabled(ticketListTableModel.hasNext());
    btnPrevious.setEnabled(ticketListTableModel.hasPrevious());
  }
  
  public synchronized void updateTicketList() {
    lastUpateCheckTimer.stop();
    try
    {
      ticketListTableModel.setCurrentRowIndex(0);
      ticketListTableModel.setNumRows(TicketDAO.getInstance().getNumTickets());
      TicketDAO.getInstance().loadTickets(ticketListTableModel);
      btnRefresh.setBlinking(false);
      updateButtonStatus();
      for (int i = 0; i < ticketUpdateListenerList.size(); i++) {
        TicketListUpdateListener listener = (TicketListUpdateListener)ticketUpdateListenerList.get(i);
        listener.ticketListUpdated();
      }
    } catch (Exception e) {
      POSMessageDialog.showError(this, Messages.getString("SwitchboardView.19"), e);
    }
    
    try
    {
      DataUpdateInfo lastUpdateInfo = DataUpdateInfoDAO.getLastUpdateInfo();
      
      if (lastUpdateInfo != null) {
        lastUpdateTime = new Date(lastUpdateInfo.getLastUpdateTime().getTime());
      }
    }
    catch (Exception e) {
      POSMessageDialog.showError(this, Messages.getString("SwitchboardView.20"), e);
    }
  }
  

  public synchronized void updateCustomerTicketList(String memberId)
  {
    lastUpateCheckTimer.stop();
    customerId = memberId;
    try
    {
      Application.getPosWindow().setGlassPaneVisible(true);
      
      TicketListTableModel ticketListTableModel = getTableModel();
      
      List<Ticket> tickets = TicketDAO.getInstance().findCustomerTickets(memberId, ticketListTableModel);
      
      setTickets(tickets);
      
      btnRefresh.setBlinking(false);
      
      for (int i = 0; i < ticketUpdateListenerList.size(); i++) {
        TicketListUpdateListener listener = (TicketListUpdateListener)ticketUpdateListenerList.get(i);
        listener.ticketListUpdated();
      }
    }
    catch (Exception e) {
      POSMessageDialog.showError(this, Messages.getString("SwitchboardView.19"), e);
    } finally {
      Application.getPosWindow().setGlassPaneVisible(false);
    }
    
    try
    {
      DataUpdateInfo lastUpdateInfo = DataUpdateInfoDAO.getLastUpdateInfo();
      
      if (lastUpdateInfo != null) {
        lastUpdateTime = new Date(lastUpdateInfo.getLastUpdateTime().getTime());
      }
    }
    catch (Exception e) {
      POSMessageDialog.showError(this, Messages.getString("SwitchboardView.20"), e);
    }
  }
  

  public void addTicketListUpateListener(TicketListUpdateListener l)
  {
    ticketUpdateListenerList.add(l);
  }
  
  private class TaskLastUpdateCheck implements ActionListener {
    private TaskLastUpdateCheck() {}
    
    public void actionPerformed(ActionEvent e) {
      if (PosGuiUtil.isModalDialogShowing()) {
        return;
      }
      
      lastUpateCheckTimer.stop();
      
      if (btnRefresh.isBlinking()) {
        return;
      }
      
      DataUpdateInfo lastUpdateInfo = DataUpdateInfoDAO.getLastUpdateInfo();
      if (lastUpdateInfo == null) {
        return;
      }
      if (lastUpdateInfo.getLastUpdateTime().after(lastUpdateTime)) {
        btnRefresh.setBlinking(true);
      }
    }
  }
  




  public void setTickets(List<Ticket> tickets)
  {
    ticketListTableModel.setRows(tickets);
  }
  
  public void addTicket(Ticket ticket)
  {
    ticketListTableModel.addItem(ticket);
  }
  
  public Ticket getSelectedTicket() {
    int selectedRow = ticketListTable.getSelectedRow();
    if (selectedRow < 0) {
      return null;
    }
    
    return (Ticket)ticketListTableModel.getRowData(ticketListTable.convertRowIndexToModel(selectedRow));
  }
  
  public List<Ticket> getSelectedTickets() {
    ArrayList<Ticket> tickets = new ArrayList();
    Ticket selectedTicket = getSelectedTicket();
    if (selectedTicket == null)
      return null;
    tickets.add(selectedTicket);
    return tickets;
  }
  
  public Ticket getFirstSelectedTicket() {
    List<Ticket> selectedTickets = getSelectedTickets();
    
    if ((selectedTickets.size() == 0) || (selectedTickets.size() > 1)) {
      POSMessageDialog.showMessage(Messages.getString("TicketListView.14"));
      return null;
    }
    
    Ticket ticket = (Ticket)selectedTickets.get(0);
    
    return ticket;
  }
  
  public String getFirstSelectedTicketId() {
    Ticket ticket = getFirstSelectedTicket();
    if (ticket == null) {
      return null;
    }
    
    return ticket.getId();
  }
  
  public JXTable getTable() {
    return ticketListTable;
  }
  
  public TicketListTableModel getTableModel() {
    return ticketListTableModel;
  }
  
  public void setCurrentRowIndexZero() {
    getTableModel().setCurrentRowIndex(0);
  }
  
  public void setAutoUpdateCheck(boolean check)
  {
    if (!check)
    {


      lastUpateCheckTimer.stop();
    }
  }
  
  private void updateOrderFilterPanel(boolean isVisible) {
    if (isVisible) {
      orderFiltersPanel.setCollapsed(!TerminalConfig.isOrderFilterPanelVisible());
      btnOrderFilters.setSelected(!orderFiltersPanel.isCollapsed());
    }
  }
  
  private void saveTableColumnsVisibility(List indices)
  {
    String selectedColumns = "";
    for (Iterator iterator = indices.iterator(); iterator.hasNext();) {
      String newSelectedColumn = String.valueOf(iterator.next());
      selectedColumns = selectedColumns + newSelectedColumn;
      
      if (iterator.hasNext()) {
        selectedColumns = selectedColumns + "*";
      }
    }
    TerminalConfig.setTicketListViewHiddenColumns(selectedColumns);
  }
  
  class CustomScrollbarUI extends BasicScrollBarUI {
    private ImageIcon downArrow;
    private ImageIcon upArrow;
    
    public CustomScrollbarUI() {
      upArrow = IconFactory.getIcon("/ui_icons/", "up.png");
      downArrow = IconFactory.getIcon("/ui_icons/", "down.png");
    }
    
    protected JButton createDecreaseButton(int orientation)
    {
      PosButton decreaseButton = new PosButton(getAppropriateIcon(orientation))
      {
        public Dimension getPreferredSize() {
          return PosUIManager.getSize(40, 40);
        }
      };
      return decreaseButton;
    }
    
    protected JButton createIncreaseButton(int orientation)
    {
      PosButton increaseButton = new PosButton(getAppropriateIcon(orientation))
      {
        public Dimension getPreferredSize() {
          return PosUIManager.getSize(40, 40);
        }
      };
      return increaseButton;
    }
    
    private ImageIcon getAppropriateIcon(int orientation) {
      switch (orientation) {
      case 5: 
        return downArrow;
      case 1: 
        return upArrow;
      }
      return upArrow;
    }
  }
  


  public void dataAdded(Object object)
  {
    if (object == null)
      return;
    TicketListTableModel ticketListTableModel = getTableModel();
    List<Ticket> filteredTicketList = new ArrayList();
    filteredTicketList.add((Ticket)object);
    filteredTicketList.addAll(ticketListTableModel.getRows());
    ticketListTableModel.setRows(filteredTicketList);
    ticketListTableModel.setNumRows(ticketListTableModel.getNumRows() + 1);
    updateButtonStatus();
  }
  
  public void dataChanged(Object object)
  {
    if (object == null) {
      return;
    }
    Ticket updatedTicket = (Ticket)object;
    
    PaymentStatusFilter paymentStatusFilter = TerminalConfig.getPaymentStatusFilter();
    int row = getTicketRowNum(updatedTicket);
    if ((!updatedTicket.isClosed().booleanValue()) && (paymentStatusFilter == PaymentStatusFilter.OPEN)) {
      if (row == -1) {
        dataAdded(updatedTicket);
      }
      
    }
    else if ((updatedTicket.isClosed().booleanValue()) && (paymentStatusFilter != PaymentStatusFilter.CLOSED)) {
      dataRemoved(updatedTicket);
    }
    else {
      ticketListTableModel.updateItem(row);
      ticketListTable.getSelectionModel().addSelectionInterval(row, row);
    }
  }
  
  private int getTicketRowNum(Ticket updatedTicket)
  {
    List rows = ticketListTableModel.getRows();
    for (int row = 0; row < rows.size(); row++) {
      Ticket modelTicket = (Ticket)rows.get(row);
      if (modelTicket.getId().equals(updatedTicket.getId())) {
        rows.set(row, updatedTicket);
        return row;
      }
    }
    return -1;
  }
  
  public void dataRemoved(Object object)
  {
    if (object == null)
      return;
    ticketListTableModel.deleteItem(object);
    ticketListTableModel.setNumRows(ticketListTableModel.getNumRows() - 1);
    ticketListTableModel.fireTableDataChanged();
  }
  
  public Object getSelectedData()
  {
    Ticket ticket = getSelectedTicket();
    if (ticket == null) {
      GlobalInputDialog inputDialog = new GlobalInputDialog(Application.getPosWindow());
      inputDialog.setCaption("Enter ticket ID");
      inputDialog.open();
      if (inputDialog.isCanceled()) {
        return null;
      }
      String ticketId = inputDialog.getInput();
      ticket = TicketService.getTicket(ticketId);
    }
    return ticket;
  }
  
  public void dataSetUpdated()
  {
    updateTicketList();
  }
  
  public void dataChangeCanceled(Object object)
  {
    if (object == null) {
      return;
    }
    Ticket updatedTicket = (Ticket)object;
    if (updatedTicket.getId() == null)
      return;
    updatedTicket = TicketDAO.getInstance().get(updatedTicket.getId());
    dataChanged(updatedTicket);
  }
}
