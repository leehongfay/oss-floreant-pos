package com.floreantpos.ui.order;

import com.floreantpos.POSConstants;
import com.floreantpos.model.Course;
import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;


public class CourseOrganizeDialog
  extends OkCancelOptionDialog
{
  private final TableDataTransferHandler handler = new TableDataTransferHandler();
  private JPanel coursePanel;
  
  public CourseOrganizeDialog(Ticket ticket) {
    setCaption(POSConstants.ORGANIZE_COURSE);
    setOkButtonText(POSConstants.SAVE_BUTTON_TEXT.toUpperCase());
    
    Map<String, List<ITicketItem>> courseItemMap = new LinkedHashMap();
    List<Course> courses = DataProvider.get().getCourses();
    int size = courses.size();
    if ((courses != null) && (size > 0)) {
      Collections.sort(courses, new Comparator()
      {
        public int compare(Course o1, Course o2)
        {
          return o1.getSortOrder().compareTo(o2.getSortOrder());
        }
      });
      for (Course course : courses) {
        courseItemMap.put(course.getId(), new ArrayList());
      }
    }
    for (??? = ticket.getTicketItems().iterator(); ???.hasNext();) { ticketItem = (TicketItem)???.next();
      String courseId = ticketItem.getCourseId();
      if (StringUtils.isEmpty(courseId)) {
        List<ITicketItem> nonCourseItems = (List)courseItemMap.get(((Course)courses.get(0)).getId());
        nonCourseItems.add(ticketItem);
      }
      else {
        List<ITicketItem> courseItems = (List)courseItemMap.get(courseId);
        if (courseItems != null)
          courseItems.add(ticketItem);
      } }
    TicketItem ticketItem;
    int wrap = 5;
    if ((size == 6) || (size == 5)) {
      wrap = 3;
    }
    else if ((size == 8) || (size == 7)) {
      wrap = 4;
    }
    coursePanel = new JPanel(new MigLayout("fill,wrap " + wrap, "sg,fill", ""));
    for (String course : courseItemMap.keySet()) {
      CourseOrganizeTableView tableView = new CourseOrganizeTableView(DataProvider.get().getCourse(course), (List)courseItemMap.get(course));
      tableView.setDataTransferHandler(handler);
      tableView.setTicket(ticket);
      coursePanel.add(tableView, "grow");
    }
    JPanel contentPanel = getContentPanel();
    contentPanel.setLayout(new BorderLayout());
    contentPanel.add(coursePanel);
  }
  
  public void doOk()
  {
    for (Component c : coursePanel.getComponents()) {
      CourseOrganizeTableView tableView = (CourseOrganizeTableView)c;
      tableView.updateModel();
    }
    setCanceled(false);
    dispose();
  }
  
  static Image iconToImage(Icon icon) {
    if ((icon instanceof ImageIcon)) {
      return ((ImageIcon)icon).getImage();
    }
    
    int w = icon.getIconWidth();
    int h = icon.getIconHeight();
    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    GraphicsDevice gd = ge.getDefaultScreenDevice();
    GraphicsConfiguration gc = gd.getDefaultConfiguration();
    BufferedImage image = gc.createCompatibleImage(w, h);
    Graphics2D g = image.createGraphics();
    icon.paintIcon(null, g, 0, 0);
    g.dispose();
    return image;
  }
}
