package com.floreantpos.ui.order;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.PaginatedTableModel;
import java.util.Date;

public class TicketListTableModel extends PaginatedTableModel
{
  public TicketListTableModel()
  {
    super(new String[] { POSConstants.TICKET_LIST_COLUMN_ID, "TOKEN", POSConstants.TICKET_LIST_COLUMN_TABLE, POSConstants.TICKET_LIST_COLUMN_SERVER, POSConstants.TICKET_LIST_COLUMN_CREATE_DATE, POSConstants.TICKET_LIST_COLUMN_CUSTOMER, POSConstants.TICKET_LIST_COLUMN_DELIVERY_ADDRESS, POSConstants.TICKET_LIST_COLUMN_DELIVERY_DATE, POSConstants.TICKET_LIST_COLUMN_TICKET_TYPE, POSConstants.TICKET_LIST_COLUMN_STATUS, POSConstants.TICKET_LIST_COLUMN_TOTAL, POSConstants.TICKET_LIST_COLUMN_DUE });
  }
  



  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Ticket ticket = (Ticket)rows.get(rowIndex);
    com.floreantpos.model.Terminal terminal = Application.getInstance().getTerminal();
    
    switch (columnIndex) {
    case 0: 
      return ticket.getId();
    case 1: 
      return ticket.getTokenNo();
    
    case 2: 
      if (!terminal.isShowTableNumber()) {
        return ticket.getTableNames();
      }
      
      return ticket.getTableNumbers();
    
    case 3: 
      User owner = ticket.getOwner();
      if (owner == null)
        return "";
      return owner.getFirstName();
    
    case 4: 
      Date createDate = ticket.getCreateDate();
      if (createDate == null) {
        return "";
      }
      if (DateUtil.isToday(createDate)) {
        return DateUtil.formatAsTodayDate(createDate);
      }
      return DateUtil.formatFullDateAndTimeAsString(createDate);
    
    case 5: 
      String customerName = ticket.getProperty("CUSTOMER_NAME");
      
      if ((customerName != null) && (!customerName.equals(""))) {
        return customerName;
      }
      
      String customerMobile = ticket.getProperty("CUSTOMER_MOBILE");
      
      if (customerMobile != null) {
        return customerMobile;
      }
      
      return Messages.getString("TicketListView.6");
    

    case 6: 
      return ticket.getDeliveryAddress();
    
    case 7: 
      Date deliveryDate = ticket.getDeliveryDate();
      if (deliveryDate == null) {
        return "";
      }
      if (DateUtil.isToday(deliveryDate)) {
        return DateUtil.formatAsTodayDate(deliveryDate);
      }
      return deliveryDate;
    
    case 8: 
      return ticket.getOrderType();
    
    case 9: 
      String status = "";
      if (ticket.isPaid().booleanValue()) {
        status = Messages.getString("TicketListView.8");
      }
      else {
        status = Messages.getString("TicketListView.9");
      }
      








      if (ticket.isVoided().booleanValue()) {
        status = Messages.getString("TicketListView.12");
      }
      else if (ticket.isClosed().booleanValue()) {
        status = Messages.getString("TicketListView.13");
      }
      
      return status;
    
    case 10: 
      return ticket.getTotalAmountWithTips();
    
    case 11: 
      return ticket.getDueAmount();
    }
    
    
    return null;
  }
}
