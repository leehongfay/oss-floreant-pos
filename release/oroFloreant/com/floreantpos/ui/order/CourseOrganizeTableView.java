package com.floreantpos.ui.order;

import com.floreantpos.model.Course;
import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.ticket.TicketViewerTableCellRenderer;
import com.floreantpos.ui.views.order.OrderController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.DropMode;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.TransferHandler;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;

public class CourseOrganizeTableView extends TransparentPanel
{
  private JTable table;
  private CourseOrganizeTableModel model;
  private Course course;
  protected boolean pressed;
  private Ticket ticket;
  
  public CourseOrganizeTableView(Course course, List<ITicketItem> items)
  {
    this.course = course;
    initComponents();
    model.setItems(items);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    table = new JTable();
    model = new CourseOrganizeTableModel();
    table.setModel(model);
    table.getSelectionModel().setSelectionMode(2);
    table.setDropMode(DropMode.INSERT_ROWS);
    table.setDragEnabled(true);
    table.setRowSelectionAllowed(true);
    table.setFillsViewportHeight(true);
    table.setFocusable(false);
    table.setAutoResizeMode(4);
    table.setAutoCreateRowSorter(false);
    table.setRowHeight(40);
    table.getTableHeader().setPreferredSize(new Dimension(0, 35));
    table.setAutoscrolls(true);
    table.addMouseListener(new MouseAdapter()
    {
      public void mousePressed(MouseEvent e)
      {
        pressed = true;
      }
      
      public void mouseReleased(MouseEvent e)
      {
        pressed = false;
        table.setCursor(Cursor.getPredefinedCursor(0));
        super.mouseReleased(e);
      }
    });
    PosScrollPane comp = new PosScrollPane(table);
    
    add(comp);
    
    PosButton btnSendToKit = new PosButton("Send");
    btnSendToKit.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (model.getRows() == null) {
          return;
        }
        updateModel();
        List<TicketItem> ticketItems = new ArrayList();
        for (Iterator iterator = model.getRows().iterator(); iterator.hasNext();) {
          TicketItem ticketItem = (TicketItem)iterator.next();
          ticketItems.add(ticketItem);
        }
        
        OrderController.saveOrder(ticket);
        ReceiptPrintService.printItemsToKitchen(ticket, ticketItems);
      }
      
    });
    JPanel bottomPanel = new JPanel(new MigLayout("center"));
    bottomPanel.add(btnSendToKit);
    add(bottomPanel, "South");
  }
  
  public void setDataTransferHandler(TableDataTransferHandler handler) {
    table.setTransferHandler(handler);
    table.setDefaultRenderer(Object.class, new DefaultTableCellRenderer()
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component rendererComponent = null;
        ListTableModel model = (ListTableModel)table.getModel();
        Object object = model.getRowData(row);
        rendererComponent = super.getTableCellRendererComponent(table, value, isSelected, false, row, column);
        if (isSelected) {
          return rendererComponent;
        }
        rendererComponent.setBackground(table.getBackground());
        if ((object instanceof ITicketItem)) {
          ITicketItem ticketItem = (ITicketItem)object;
          if (ticketItem.isPrintedToKitchen().booleanValue()) {
            rendererComponent.setBackground(Color.YELLOW);
          }
          else if (ticketItem.isSaved()) {
            rendererComponent.setBackground(TicketViewerTableCellRenderer.SAVED_ITEM_COLOR);
          }
        }
        return rendererComponent;
      }
    });
    ActionMap map = table.getActionMap();
    Action action = new AbstractAction()
    {
      public void actionPerformed(ActionEvent e) {}

    };
    map.put(TransferHandler.getCutAction().getValue("Name"), action);
    map.put(TransferHandler.getCopyAction().getValue("Name"), action);
    map.put(TransferHandler.getPasteAction().getValue("Name"), action);
    TableColumn col = table.getColumnModel().getColumn(0);
    col.setHeaderRenderer(new TableCellRenderer()
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int rowIndex, int vColIndex) {
        JLabel lblCourse = new JLabel(course.getName(), course.getIcon(), 0);
        lblCourse.setBorder(table.getBorder());
        return lblCourse;
      }
    });
  }
  
  public void updateModel() {
    for (ITicketItem item : model.getRows()) {
      TicketItem ticketItem = (TicketItem)item;
      ticketItem.setCourseId(course == null ? null : course.getId());
      ticketItem.setSortOrder(course == null ? null : course.getSortOrder());
    }
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }
}
