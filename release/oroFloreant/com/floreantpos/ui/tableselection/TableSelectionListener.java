package com.floreantpos.ui.tableselection;

import com.floreantpos.model.ShopTable;
import com.floreantpos.model.Ticket;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;

public abstract interface TableSelectionListener
{
  public static final String GROUP = "GROUP";
  public static final String UNGROUP = "UNGROUP";
  
  public abstract void tableSelected(ShopTable paramShopTable);
  
  public abstract AbstractButton getTableActionButton();
  
  public abstract void tableActionSelected(ShopTable paramShopTable, AbstractAction paramAbstractAction, Ticket paramTicket);
  
  public abstract List getAddedTableList();
  
  public abstract List getReleasesTableList();
  
  public abstract Ticket getTicket(Integer paramInteger);
}
