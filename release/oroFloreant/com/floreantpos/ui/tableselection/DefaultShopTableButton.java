package com.floreantpos.ui.tableselection;

import com.floreantpos.Messages;
import com.floreantpos.actions.PosAction;
import com.floreantpos.main.Application;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.ShopTableStatus;
import com.floreantpos.model.TableStatus;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.ShopTableStatusDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.services.TicketService;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.PasswordEntryDialog;
import com.floreantpos.util.POSUtil;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang.StringUtils;
















public class DefaultShopTableButton
  extends PosButton
  implements ActionListener
{
  private ShopTable shopTable;
  private User user;
  private Ticket ticket;
  private TableSelectionListener listener;
  
  public DefaultShopTableButton(ShopTable shopTable)
  {
    this.shopTable = shopTable;
    setPreferredSize(PosUIManager.getSize(157, 138));
    setBorder(null);
    addActionListener(this);
  }
  
  public void actionPerformed(ActionEvent e)
  {
    ShopTableStatus shopTaleStatus = shopTable.getStatus();
    if (!TerminalDAO.getInstance().isVersionEqual(ShopTableStatus.class, shopTaleStatus.getId(), shopTaleStatus.getVersion())) {
      POSMessageDialog.showMessage("Table has been changed in other terminal. Please reload this window and try again.");
      return;
    }
    if (listener == null)
      return;
    Object selectedActionButton = listener.getTableActionButton();
    if (selectedActionButton == null) {
      shopTaleStatus = ShopTableStatusDAO.getInstance().get(shopTaleStatus.getId());
      listener.tableSelected(shopTable);
    }
    else {
      POSToggleButton toggleActionButton = (POSToggleButton)selectedActionButton;
      Object ticketAction = toggleActionButton.getClientProperty("ticketAction");
      if (ticketAction == null) {
        doGroupOrUngroupTable(toggleActionButton);
      }
      else {
        PosAction action = (PosAction)toggleActionButton.getClientProperty("ticketAction");
        if (shopTaleStatus.getTicketId() == null) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("DefaultTableSelectionView.2"));
        }
        else {
          listener.tableActionSelected(shopTable, action, initializeTicket());
        }
      }
    }
    updateView();
  }
  
  private void doGroupOrUngroupTable(POSToggleButton toggleActionButton) {
    ShopTableStatus shopTaleStatus = shopTable.getStatus();
    String buttonAction = (String)toggleActionButton.getClientProperty("buttonAction");
    if (buttonAction.equals("GROUP")) {
      if (shopTaleStatus.getTicketId() != null)
        return;
      List list = listener.getAddedTableList();
      if (list.contains(shopTaleStatus.getId()))
        return;
      list.add(shopTaleStatus.getId());
    }
    else if (buttonAction.equals("UNGROUP")) {
      if (shopTaleStatus.getTicketId() == null)
        return;
      List list = listener.getReleasesTableList();
      for (Iterator iterator = list.iterator(); iterator.hasNext();) {
        Integer tableId = (Integer)iterator.next();
        Ticket ticket = listener.getTicket(tableId);
        if (!shopTaleStatus.getTicketId().equals(ticket.getId())) {
          return;
        }
      }
      
      if (list.size() >= getTicket().getTableNumbers().size() - 1) {
        return;
      }
      list.add(shopTaleStatus.getId());
    }
  }
  
  public void setTableSelectionListener(TableSelectionListener listener) {
    this.listener = listener;
  }
  
  public int getId() {
    return shopTable.getId().intValue();
  }
  
  public void setShopTable(ShopTable shopTable) {
    this.shopTable = shopTable;
  }
  
  public ShopTable getShopTable() {
    return shopTable;
  }
  
  public boolean equals(Object obj)
  {
    if (!(obj instanceof DefaultShopTableButton)) {
      return false;
    }
    
    DefaultShopTableButton that = (DefaultShopTableButton)obj;
    return shopTable.equals(shopTable);
  }
  
  public int hashCode()
  {
    return shopTable.hashCode();
  }
  
  public String toString()
  {
    return shopTable.toString();
  }
  
  public void updateView() {
    ShopTableStatus shopTaleStatus = shopTable.getStatus();
    TableStatus tableStatus = shopTaleStatus.getTableStatus();
    setBackground(tableStatus == null ? Color.WHITE : tableStatus.getBgColor());
    setForeground(tableStatus == null ? Color.BLACK : tableStatus.getTextColor());
    
    if ((listener != null) && (
      (listener.getAddedTableList().contains(shopTaleStatus.getId())) || (listener.getReleasesTableList().contains(shopTaleStatus.getId())))) {
      setBackground(Color.GREEN);
      setForeground(Color.BLACK);
    }
    
    String text = "<html><center><font size='36'>";
    text = text + shopTable.getTableNumber();
    if ((shopTaleStatus.getTicketId() != null) && (StringUtils.isNotEmpty(shopTaleStatus.getUserId()))) {
      text = text + "</font><strong>" + shopTaleStatus.getUserName();
      if (!shopTaleStatus.getUserId().toString().equals(Application.getCurrentUser().getId().toString())) {
        setBackground(new Color(139, 0, 139));
        setForeground(Color.WHITE);
      }
    }
    if (StringUtils.isNotEmpty(shopTaleStatus.getTicketId())) {
      text = text + "<br>Token# " + shopTaleStatus.getTokenNo();
    }
    text = text + "<strong></center></html>";
    setText(text);
  }
  
  public void setUser(User user) {
    if (user != null) {
      this.user = user;
    }
  }
  
  public User getUser() {
    ShopTableStatus shopTaleStatus = shopTable.getStatus();
    User currentUser = Application.getCurrentUser();
    
    String currentUserId = currentUser.getId();
    String ticketUserId = shopTaleStatus.getUserId();
    
    if (currentUserId.equals(ticketUserId)) {
      user = currentUser;
    }
    if (user == null) {
      user = UserDAO.getInstance().get(user.getId());
    }
    return user;
  }
  
  public boolean hasUserAccess() {
    User ticketUser = getUser();
    if (ticketUser == null) {
      return false;
    }
    User currentUser = Application.getCurrentUser();
    if ((currentUser.hasPermission(UserPermission.PERFORM_MANAGER_TASK)) || (currentUser.hasPermission(UserPermission.PERFORM_ADMINISTRATIVE_TASK))) {
      return true;
    }
    String password = PasswordEntryDialog.show(Application.getPosWindow(), Messages.getString("PosAction.0"));
    if (StringUtils.isEmpty(password)) {
      return false;
    }
    String inputUserId = UserDAO.getInstance().findUserBySecretKey(password).getId();
    if (!inputUserId.equals(ticketUser.getId())) {
      POSMessageDialog.showError(Application.getPosWindow(), "Incorrect password");
      return false;
    }
    return true;
  }
  
  public void initializeUser() {
    ShopTableStatus shopTaleStatus = shopTable.getStatus();
    String userId = shopTaleStatus.getUserId();
    if (StringUtils.isEmpty(userId)) {
      return;
    }
    if ((user != null) && (user.getId().equals(userId))) {
      return;
    }
    user = UserDAO.getInstance().get(shopTaleStatus.getUserId());
  }
  
  private Ticket initializeTicket() {
    ShopTableStatus shopTaleStatus = shopTable.getStatus();
    String ticketId = shopTaleStatus.getTicketId();
    if (StringUtils.isEmpty(ticketId)) {
      ticket = null;
    }
    if ((ticket != null) && (ticket.getId().equals(shopTaleStatus.getTicketId()))) {
      return ticket;
    }
    return this.ticket = TicketService.getTicket(shopTaleStatus.getTicketId());
  }
  
  public Ticket getTicket() {
    return initializeTicket();
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
    ShopTableStatus shopTaleStatus = shopTable.getStatus();
    if (ticket == null) {
      shopTaleStatus.setTableStatus(TableStatus.Available);
      shopTaleStatus.setTicketId(null);
    }
    else {
      shopTaleStatus.setTableStatus(TableStatus.Seat);
      shopTaleStatus.setTableTicket(ticket.getId(), ticket.getTokenNo(), ticket.getOwner().getId(), ticket.getOwner().getFirstName());
    }
    updateView();
  }
}
