package com.floreantpos.ui.tableselection;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.actions.GroupSettleTicketAction;
import com.floreantpos.actions.NewBarTabAction;
import com.floreantpos.actions.ReorderTicketAction;
import com.floreantpos.actions.SendToKitchenAction;
import com.floreantpos.actions.SettleTicketAction;
import com.floreantpos.actions.ShowOrderInfoAction;
import com.floreantpos.actions.ShowTransactionsAuthorizationsAction;
import com.floreantpos.actions.SplitTicketAction;
import com.floreantpos.actions.TicketEditAction;
import com.floreantpos.actions.TicketKitchenSentAction;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.extension.OrderServiceFactory;
import com.floreantpos.main.Application;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.ShopTableStatus;
import com.floreantpos.model.TableStatus;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.ShopTableDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.services.TicketService;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.ScrollableFlowPanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import com.floreantpos.ui.views.payment.SplitedTicketSelectionDialog;
import com.floreantpos.util.TicketAlreadyExistsException;
import com.jidesoft.swing.JideScrollPane;
import java.awt.BorderLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;



















public class DefaultTableSelectionView
  extends TableSelector
  implements ActionListener, DataChangeListener, TableSelectionListener
{
  private ScrollableFlowPanel buttonsPanel;
  private static PosButton btnCancelDialog;
  private PosButton btnDone = new PosButton(POSConstants.SAVE_BUTTON_TEXT);
  private PosButton btnCancel = new PosButton(POSConstants.CANCEL);
  private PosButton btnAuthorize = new PosButton(Messages.getString("DefaultTableSelectionView.5"));
  
  private PosButton btnRefresh;
  private ButtonGroup btnGroups = new ButtonGroup();
  private POSToggleButton btnGroup = new POSToggleButton(POSConstants.GROUP);
  private POSToggleButton btnUnGroup = new POSToggleButton(POSConstants.UNGROUP);
  private POSToggleButton btnHoldFire = new POSToggleButton(Messages.getString("DefaultTableSelectionView.0"));
  private POSToggleButton btnGuestCheck = new POSToggleButton(Messages.getString("DefaultTableSelectionView.1"));
  private POSToggleButton btnSplitCheck = new POSToggleButton(Messages.getString("DefaultTableSelectionView.3"));
  private POSToggleButton btnSettle = new POSToggleButton(Messages.getString("DefaultTableSelectionView.4"));
  private POSToggleButton btnReorder = new POSToggleButton(Messages.getString("DefaultTableSelectionView.8"));
  private PosButton btnGroupSettle = new PosButton(Messages.getString("DefaultTableSelectionView.7"));
  
  private POSToggleButton selectedActionButton;
  
  private Ticket selectedTicket;
  private Map<Integer, DefaultShopTableButton> tableButtonMap = new HashMap();
  private List<Integer> addedTableList = new ArrayList();
  private List<Integer> releasesTableList = new ArrayList();
  private boolean initialized = false;
  private BarTabSelectionView barTabSelectionView;
  private JPanel tableSelectionPanel;
  private PosButton btnNewBarTab;
  private JTabbedPane tabbedPane;
  
  public DefaultTableSelectionView() {
    initComponents();
    initActions();
  }
  
  private void initActions() {
    btnGroup.putClientProperty("buttonAction", "GROUP");
    btnUnGroup.putClientProperty("buttonAction", "UNGROUP");
    
    btnSettle.putClientProperty("ticketAction", new SettleTicketAction(this));
    btnSplitCheck.putClientProperty("ticketAction", new SplitTicketAction(this));
    btnGuestCheck.putClientProperty("ticketAction", new ShowOrderInfoAction(this));
    btnHoldFire.putClientProperty("ticketAction", new TicketKitchenSentAction(this));
    btnReorder.putClientProperty("ticketAction", new ReorderTicketAction(this));
    
    btnGroup.addActionListener(this);
    btnUnGroup.addActionListener(this);
    btnDone.addActionListener(this);
    btnCancel.addActionListener(this);
    btnHoldFire.addActionListener(this);
    btnGuestCheck.addActionListener(this);
    btnSplitCheck.addActionListener(this);
    btnSettle.addActionListener(this);
    btnReorder.addActionListener(this);
    btnDone.setVisible(false);
    btnCancel.setVisible(false);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    setBorder(BorderFactory.createEmptyBorder(7, 0, 0, 0));
    
    TitledBorder titledBorder1 = BorderFactory.createTitledBorder(null, POSConstants.TABLES, 2, 0);
    tableSelectionPanel = new JPanel(new BorderLayout(5, 5));
    tableSelectionPanel.setBorder(new CompoundBorder(titledBorder1, new EmptyBorder(2, 2, 2, 2)));
    
    buttonsPanel = new ScrollableFlowPanel(1);
    
    JideScrollPane scrollPane = new JideScrollPane(buttonsPanel, 20, 31);
    scrollPane.getVerticalScrollBar().setPreferredSize(PosUIManager.getSize(60, 0));
    
    tableSelectionPanel.add(scrollPane, "Center");
    
    tabbedPane = new JTabbedPane();
    
    tabbedPane.addTab(Messages.getString("DefaultTableSelectionView.9"), tableSelectionPanel);
    
    barTabSelectionView = new BarTabSelectionView();
    
    add(tabbedPane, "Center");
    
    add(createButtonActionPanel(), "East");
  }
  
  private JPanel createButtonActionPanel() {
    TitledBorder titledBorder2 = BorderFactory.createTitledBorder(null, "-", 2, 0);
    
    JPanel rightPanel = new JPanel(new BorderLayout(20, 20));
    rightPanel.setPreferredSize(PosUIManager.getSize(125, 0));
    rightPanel.setBorder(new CompoundBorder(titledBorder2, new EmptyBorder(2, 2, 6, 2)));
    
    JPanel actionBtnPanel = new JPanel(new MigLayout("ins 2 2 0 2, hidemode 3, flowy", "sg fill, grow", ""));
    
    btnGroup.setIcon(new ImageIcon(getClass().getResource("/images/plus.png")));
    btnUnGroup.setIcon(new ImageIcon(getClass().getResource("/images/minus2.png")));
    
    btnNewBarTab = new PosButton(Messages.getString("DefaultTableSelectionView.10"));
    btnNewBarTab.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        List<ShopTable> selectedTables = getSelectedTables();
        new NewBarTabAction(orderType, selectedTables, Application.getPosWindow()).actionPerformed(e);
      }
      
    });
    btnGroups.add(btnGroup);
    btnGroups.add(btnUnGroup);
    btnGroups.add(btnHoldFire);
    btnGroups.add(btnGuestCheck);
    btnGroups.add(btnSplitCheck);
    btnGroups.add(btnSettle);
    btnGroups.add(btnReorder);
    
    actionBtnPanel.add(btnGroup, "grow");
    actionBtnPanel.add(btnUnGroup, "grow");
    actionBtnPanel.add(btnNewBarTab, "grow");
    actionBtnPanel.add(btnHoldFire, "grow");
    actionBtnPanel.add(btnGuestCheck, "grow");
    actionBtnPanel.add(btnSplitCheck, "grow");
    actionBtnPanel.add(btnSettle, "grow");
    actionBtnPanel.add(btnGroupSettle, "grow");
    actionBtnPanel.add(btnReorder, "grow");
    actionBtnPanel.add(btnAuthorize, "grow");
    
    actionBtnPanel.add(btnDone, "grow");
    actionBtnPanel.add(btnCancel, "grow");
    
    rightPanel.add(actionBtnPanel);
    
    JPanel southbuttonPanel = new JPanel(new MigLayout("ins 2 2 0 2, hidemode 3, flowy", "grow", ""));
    btnRefresh = new PosButton(POSConstants.REFRESH);
    btnRefresh.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        initialized = false;
        rendererTables();
      }
      
    });
    btnAuthorize.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        new ShowTransactionsAuthorizationsAction().execute();
      }
      

    });
    btnGroupSettle.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        new GroupSettleTicketAction(Application.getCurrentUser()).actionPerformed(null);
        rendererTables();
      }
    });
    southbuttonPanel.add(btnRefresh, "grow");
    
    btnCancelDialog = new PosButton(POSConstants.CANCEL);
    btnCancelDialog.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        DefaultTableSelectionView.this.closeDialog(true);
      }
      
    });
    southbuttonPanel.add(btnCancelDialog, "grow");
    rightPanel.add(southbuttonPanel, "South");
    
    return rightPanel;
  }
  
  public synchronized void rendererTables() {
    clearSelection();
    List<ShopTable> tables = ShopTableDAO.getInstance().findAll();
    for (ShopTable shopTable : tables) {
      DefaultShopTableButton tableButton = (DefaultShopTableButton)tableButtonMap.get(shopTable.getId());
      if (tableButton == null) {
        tableButton = new DefaultShopTableButton(shopTable);
        tableButton.setTableSelectionListener(this);
        tableButton.updateView();
        tableButtonMap.put(shopTable.getId(), tableButton);
        buttonsPanel.add(tableButton);
      }
      else {
        tableButton.setShopTable(shopTable);
        tableButton.updateView();
      }
    }
    barTabSelectionView.updateView(orderType);
    initialized = true;
  }
  
  private void closeDialog(boolean canceled) {
    Window windowAncestor = SwingUtilities.getWindowAncestor(this);
    if ((windowAncestor instanceof POSDialog)) {
      ((POSDialog)windowAncestor).setCanceled(false);
      windowAncestor.dispose();
    }
  }
  
  public List<ShopTable> getSelectedTables()
  {
    List<ShopTable> tables = new ArrayList();
    
    for (Integer shopTableId : addedTableList) {
      DefaultShopTableButton defaultShopTableButton = (DefaultShopTableButton)tableButtonMap.get(shopTableId);
      tables.add(defaultShopTableButton.getShopTable());
    }
    return tables;
  }
  
  private void clearSelection() {
    if (isCreateNewTicket()) {
      addedTableList.clear();
    }
    releasesTableList.clear();
    btnGroups.clearSelection();
    btnGroup.setVisible(true);
    btnUnGroup.setVisible(true);
    btnHoldFire.setVisible(true);
    btnGuestCheck.setVisible(true);
    btnSplitCheck.setVisible(true);
    btnSettle.setVisible(true);
    btnGroupSettle.setVisible(true);
    btnAuthorize.setVisible(true);
    btnDone.setVisible(false);
    btnCancel.setVisible(false);
    selectedActionButton = null;
    if (getOrderType() != null) {
      if (getOrderType().isEnableReorder().booleanValue()) {
        btnReorder.setVisible(true);
      }
      else {
        btnReorder.setVisible(false);
      }
    }
  }
  
  public void actionPerformed(ActionEvent e)
  {
    Object object = e.getSource();
    if ((object instanceof POSToggleButton)) {
      selectedActionButton = ((POSToggleButton)object);
    }
    if (object == btnGroup) {
      if (isCreateNewTicket()) {
        addedTableList.clear();
      }
      btnUnGroup.setVisible(false);
      btnHoldFire.setVisible(false);
      btnGuestCheck.setVisible(false);
      btnSplitCheck.setVisible(false);
      btnSettle.setVisible(false);
      btnGroupSettle.setVisible(false);
      btnReorder.setVisible(false);
      btnAuthorize.setVisible(false);
      btnDone.setVisible(true);
      btnCancel.setVisible(true);
    }
    else if (object == btnUnGroup) {
      releasesTableList.clear();
      btnGroup.setVisible(false);
      btnHoldFire.setVisible(false);
      btnGuestCheck.setVisible(false);
      btnSplitCheck.setVisible(false);
      btnSettle.setVisible(false);
      btnGroupSettle.setVisible(false);
      btnReorder.setVisible(false);
      btnAuthorize.setVisible(false);
      btnDone.setVisible(true);
      btnCancel.setVisible(true);
    }
    else if (object == btnDone) {
      if (btnGroup.isSelected()) {
        doGroupAction();
        clearSelection();
      }
      else if (btnUnGroup.isSelected()) {
        doUnGroupAction();
        clearSelection();
      }
    }
    else if (object == btnCancel) {
      clearSelection();
      rendererTables();
    }
  }
  
  private void doCreateNewTicket() {
    try {
      List<ShopTable> selectedTables = getSelectedTables();
      if (selectedTables.isEmpty()) {
        clearSelection();
        return;
      }
      OrderServiceFactory.getOrderService().createNewTicket(getOrderType(), selectedTables, null);
      ShopTableDAO.getInstance().updateTableStatus(addedTableList, Integer.valueOf(TableStatus.Booked.getValue()), null, false);
      clearSelection();
    } catch (TicketAlreadyExistsException e) {
      PosLog.error(getClass(), e);
    }
  }
  
  private boolean editTicket(Integer tableNumber) {
    closeDialog(false);
    
    Ticket ticketToEdit = ((DefaultShopTableButton)tableButtonMap.get(tableNumber)).getTicket();
    TicketDAO.getInstance().loadFullTicket(ticketToEdit);
    OrderView.getInstance().setCurrentTicket(ticketToEdit);
    RootView.getInstance().showView("ORDER_VIEW");
    
    return true;
  }
  
  private void doGroupAction() {
    if (isCreateNewTicket()) {
      doCreateNewTicket();
    }
    closeDialog(false);
  }
  
  private void doUnGroupAction() {
    if ((releasesTableList == null) || (releasesTableList.isEmpty())) {
      return;
    }
    
    DefaultShopTableButton firstButton = (DefaultShopTableButton)tableButtonMap.get(releasesTableList.get(0));
    if (!firstButton.hasUserAccess()) {
      return;
    }
    doReleaseTables();
    
    if (!isCreateNewTicket()) {
      closeDialog(false);
    }
  }
  
  private void doReleaseTables()
  {
    for (Iterator iterator = releasesTableList.iterator(); iterator.hasNext();) {
      Integer removedTableId = (Integer)iterator.next();
      DefaultShopTableButton button = (DefaultShopTableButton)tableButtonMap.get(removedTableId);
      ShopTable shopTable = button.getShopTable();
      if (addedTableList.contains(shopTable.getId())) {
        addedTableList.remove(shopTable.getId());
      }
      iterator.remove();
      button.setTicket(null);
    }
    clearSelection();
  }
  
  public void setTicket(Ticket ticket) {
    if (ticket == null) {
      return;
    }
    this.ticket = ticket;
    
    List<ShopTable> tables = ShopTableDAO.getInstance().getTables(ticket);
    if (tables == null) {
      return;
    }
    addedTableList.clear();
    
    for (ShopTable shopTable : tables) {
      DefaultShopTableButton defaultShopTableButton = (DefaultShopTableButton)tableButtonMap.get(shopTable.getId());
      


      addedTableList.add(defaultShopTableButton.getShopTable().getId());
      defaultShopTableButton.updateView();
    }
  }
  
  public void updateView(boolean update)
  {
    btnCancelDialog.setVisible(update);
  }
  
  public void tableSelected(ShopTable table)
  {
    if (table == null)
      return;
    ShopTableStatus shopTableStatus = table.getShopTableStatus();
    DefaultShopTableButton button = (DefaultShopTableButton)tableButtonMap.get(table.getId());
    if (shopTableStatus.hasMultipleTickets()) {
      showSplitTickets(new TicketEditAction(), button.getTicket(), shopTableStatus);

    }
    else if (table.getTicketId() != null) {
      editTicket(table.getId());
    }
    else {
      if ((!isCreateNewTicket()) && (!btnGroup.isSelected()) && (addedTableList.size() >= 1)) {
        releasesTableList.addAll(addedTableList);
        doReleaseTables();
      }
      addedTableList.add(table.getId());
      if (isCreateNewTicket()) {
        doCreateNewTicket();
      }
    }
    
    closeDialog(false);
  }
  
  public AbstractButton getTableActionButton()
  {
    return selectedActionButton;
  }
  
  public List getAddedTableList()
  {
    return addedTableList;
  }
  
  public List getReleasesTableList()
  {
    return releasesTableList;
  }
  
  public void dataAdded(Object object)
  {
    updateView(object);
  }
  
  private void updateView(Object object) {
    if (object == null)
      return;
    Ticket updatedTicket = (Ticket)object;
    List<Integer> tableNumbers = updatedTicket.getTableNumbers();
    if ((tableNumbers == null) || (tableNumbers.isEmpty()))
      return;
    for (Integer tableNumber : tableNumbers) {
      DefaultShopTableButton button = (DefaultShopTableButton)tableButtonMap.get(tableNumber);
      if (updatedTicket.isClosed().booleanValue()) {
        button.setTicket(null);
      }
      else {
        button.setTicket(updatedTicket);
      }
    }
  }
  
  public void dataChanged(Object object)
  {
    updateView(object);
  }
  
  public void dataRemoved(Object object)
  {
    updateView(object);
  }
  
  public Object getSelectedData()
  {
    return selectedTicket;
  }
  
  public void dataSetUpdated()
  {
    initialized = false;
    rendererTables();
  }
  
  public void tableActionSelected(ShopTable shopTable, AbstractAction action, Ticket ticket)
  {
    selectedTicket = ticket;
    ShopTableStatus shopTableStatus = shopTable.getShopTableStatus();
    DefaultShopTableButton button = (DefaultShopTableButton)tableButtonMap.get(shopTable.getId());
    if (shopTableStatus.hasMultipleTickets()) {
      showSplitTickets(action, button.getTicket(), shopTableStatus);
    }
    else {
      if (isCreateNewTicket()) {
        action.actionPerformed(null);
        closeDialog(false);
      }
      clearSelection();
      ticket = TicketService.getTicket(ticket.getId());
    }
  }
  
  private void showSplitTickets(AbstractAction selectedAction, Ticket selectedTicket, ShopTableStatus shopTaleStatus) {
    List<Ticket> splitTickets = new ArrayList();
    for (String ticketId : shopTaleStatus.getListOfTicketNumbers()) {
      if ((selectedTicket != null) && (ticketId.equals(selectedTicket.getId()))) {
        splitTickets.add(selectedTicket);
      } else
        splitTickets.add(TicketDAO.getInstance().loadFullTicket(ticketId));
    }
    String action = "";
    if ((selectedAction instanceof SendToKitchenAction)) {
      action = POSConstants.SEND_TO_KITCHEN;
    }
    else if ((selectedAction instanceof ShowOrderInfoAction)) {
      action = POSConstants.ORDER_INFO;
    }
    else if ((selectedAction instanceof SettleTicketAction)) {
      action = POSConstants.SETTLE;
    }
    else if ((selectedAction instanceof SplitTicketAction)) {
      action = POSConstants.SPLIT_TICKET;
    }
    else if ((selectedAction instanceof ReorderTicketAction)) {
      action = POSConstants.REORDER_TICKET_BUTTON_TEXT;
    }
    else {
      action = POSConstants.EDIT;
    }
    SplitedTicketSelectionDialog posDialog = new SplitedTicketSelectionDialog(splitTickets);
    posDialog.setDefaultCloseOperation(2);
    posDialog.setSelectedAction(action);
    posDialog.setSize(730, 470);
    posDialog.open();
    rendererTables();
    if (posDialog.isCanceled())
      return;
    if (action.equals(POSConstants.EDIT)) {
      closeDialog(false);
    }
  }
  
  public Ticket getTicket(Integer tableId) {
    return ((DefaultShopTableButton)tableButtonMap.get(tableId)).getTicket();
  }
  
  public void dataChangeCanceled(Object object)
  {
    if (object == null)
      return;
    Ticket updatedTicket = (Ticket)object;
    List<Integer> tableNumbers = updatedTicket.getTableNumbers();
    if ((tableNumbers == null) || (tableNumbers.isEmpty()))
      return;
    for (Iterator iterator = tableNumbers.iterator(); iterator.hasNext();) {
      Integer removedTableId = (Integer)iterator.next();
      DefaultShopTableButton button = (DefaultShopTableButton)tableButtonMap.get(removedTableId);
      button.setTicket(null);
    }
  }
  
  public void setOrderType(OrderType orderType)
  {
    super.setOrderType(orderType);
    if (orderType == null)
      return;
    btnNewBarTab.setVisible(orderType.isBarTab().booleanValue());
    if (orderType.isBarTab().booleanValue()) {
      tabbedPane.addTab(Messages.getString("DefaultTableSelectionView.11"), barTabSelectionView);
    }
  }
}
