package com.floreantpos.ui.tableselection;

import com.floreantpos.extension.FloorLayoutPlugin;

public class TableSelectorFactory {
  private static TableSelector tableSelector;
  
  public TableSelectorFactory() {}
  
  public static TableSelectorDialog createTableSelectorDialog(com.floreantpos.model.OrderType orderType) {
    FloorLayoutPlugin floorLayoutPlugin = (FloorLayoutPlugin)com.floreantpos.extension.ExtensionManager.getPlugin(FloorLayoutPlugin.class);
    if (tableSelector == null) {
      if (floorLayoutPlugin == null) {
        tableSelector = new DefaultTableSelectionView();
      }
      else {
        tableSelector = floorLayoutPlugin.createTableSelector();
      }
    }
    tableSelector.setOrderType(orderType);
    tableSelector.rendererTables();
    
    return new TableSelectorDialog(tableSelector);
  }
}
