package com.floreantpos.ui.tableselection;

import com.floreantpos.PosLog;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.extension.OrderServiceFactory;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.Ticket;
import com.floreantpos.util.TicketAlreadyExistsException;
import java.util.List;
import javax.swing.JPanel;

public abstract class TableSelector
  extends JPanel
{
  protected OrderType orderType;
  protected Ticket ticket;
  private boolean createNewTicket = true;
  
  public TableSelector() {}
  
  public void tablesSelected(OrderType orderType, List<ShopTable> selectedTables)
  {
    try {
      OrderServiceFactory.getOrderService().createNewTicket(orderType, selectedTables, null);
    } catch (TicketAlreadyExistsException e) {
      PosLog.error(getClass(), e);
    } }
  
  public abstract void rendererTables();
  
  public abstract List<ShopTable> getSelectedTables();
  
  public abstract void updateView(boolean paramBoolean);
  
  public OrderType getOrderType() { return orderType; }
  
  public void setOrderType(OrderType orderType)
  {
    this.orderType = orderType;
  }
  
  public boolean isCreateNewTicket() {
    return createNewTicket;
  }
  
  public void setCreateNewTicket(boolean createNewTicket) {
    this.createNewTicket = createNewTicket;
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }
  
  public Ticket getTicket() {
    return ticket;
  }
}
