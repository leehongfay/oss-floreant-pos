package com.floreantpos.ui;

public abstract interface TicketListUpdateListener
{
  public abstract void ticketListUpdated();
}
