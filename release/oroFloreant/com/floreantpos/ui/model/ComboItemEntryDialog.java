package com.floreantpos.ui.model;

import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.IntegerDocument;
import com.floreantpos.swing.ListComboBoxModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.PosGuiUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;
















public class ComboItemEntryDialog
  extends POSDialog
  implements ActionListener
{
  private JComboBox cbGroup;
  private JComboBox cbMenuItem;
  private DoubleTextField tfQuantity;
  private JLabel lblMenuItem;
  private JLabel lblQuantity;
  
  public ComboItemEntryDialog(MenuItem menuItem)
  {
    super(POSUtil.getFocusedWindow(), "");
    init();
    
    rendererMenuItems();
  }
  
  private void init() {
    setLayout(new BorderLayout(10, 10));
    setIconImage(Application.getPosWindow().getIconImage());
    
    JPanel contentPane = new JPanel(new MigLayout("fill,hidemode 3,inset 0 10 0 10"));
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Select item");
    add(titlePanel, "North");
    
    cbGroup = new JComboBox();
    cbMenuItem = new JComboBox();
    
    tfQuantity = new DoubleTextField(10);
    
    List<MenuGroup> menuGroupList = MenuGroupDAO.getInstance().findAll();
    for (MenuGroup s : menuGroupList) {
      cbGroup.addItem(s);
    }
    cbGroup.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        ComboItemEntryDialog.this.rendererMenuItems();
      }
    });
    contentPane.add(new JLabel("Group"));
    contentPane.add(cbGroup, "grow,wrap");
    lblMenuItem = new JLabel("Menu Item");
    contentPane.add(lblMenuItem);
    contentPane.add(cbMenuItem, "grow,wrap");
    lblQuantity = new JLabel("Quantity");
    contentPane.add(lblQuantity);
    contentPane.add(tfQuantity, "grow,wrap");
    contentPane.add(new JSeparator(), "gaptop 10,span 2,grow");
    
    PosButton btnOk = new PosButton(POSConstants.OK);
    btnOk.setFocusable(false);
    btnOk.addActionListener(this);
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL);
    btnCancel.setFocusable(false);
    btnCancel.addActionListener(this);
    
    JPanel footerPanel = new JPanel(new MigLayout("center,ins 0 5 5 5", "", ""));
    footerPanel.add(btnOk);
    footerPanel.add(btnCancel);
    
    add(footerPanel, "South");
    
    add(contentPane);
    pack();
  }
  
  private void rendererMenuItems() {
    MenuGroup selectedGroup = (MenuGroup)cbGroup.getSelectedItem();
    List<MenuItem> menuItems = MenuItemDAO.getInstance().findByParent(null, selectedGroup, false);
    for (Iterator iterator = menuItems.iterator(); iterator.hasNext();) {
      MenuItem menuItem = (MenuItem)iterator.next();
      if (menuItem.isComboItem().booleanValue()) {
        iterator.remove();
      }
    }
    cbMenuItem.setModel(new ListComboBoxModel(menuItems));
  }
  
  public void setSelectedItem(MenuItem item) {
    cbGroup.setSelectedItem(null);
    if ((item != null) && (item.getMenuGroupId() != null)) {
      PosGuiUtil.selectComboItemById(cbGroup, item.getMenuGroupId());
    }
    rendererMenuItems();
    cbMenuItem.setSelectedItem(item);
  }
  
  public void setSelectedGroup(MenuGroup menuGroup) {
    lblQuantity.setText("Max Quantity");
    lblMenuItem.setVisible(false);
    cbMenuItem.setVisible(false);
    if (menuGroup != null)
      cbGroup.setSelectedItem(menuGroup);
  }
  
  public void setQuantity(Double quantity) {
    tfQuantity.setText(String.valueOf(quantity));
  }
  
  public void setQuantity(Integer quantity) {
    tfQuantity.setText(String.valueOf(quantity));
  }
  
  private void doOk() {
    setCanceled(false);
    dispose();
  }
  
  public MenuItem getItem() {
    return (MenuItem)cbMenuItem.getSelectedItem();
  }
  
  public MenuGroup getSelectedGroup() {
    return (MenuGroup)cbGroup.getSelectedItem();
  }
  
  private void doCancel() {
    setCanceled(true);
    dispose();
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    
    if (POSConstants.CANCEL.equalsIgnoreCase(actionCommand)) {
      doCancel();
    }
    else if (POSConstants.OK.equalsIgnoreCase(actionCommand)) {
      doOk();
    }
  }
  
  public Double getQuantity() {
    if (Double.isNaN(tfQuantity.getDouble())) {
      return Double.valueOf(1.0D);
    }
    return Double.valueOf(tfQuantity.getDouble());
  }
  
  public void setFloatingPoint(boolean b) {
    if (!b) {
      tfQuantity.setDocument(new IntegerDocument());
    }
  }
  
  public int getIntegerValue() {
    try {
      return Integer.parseInt(tfQuantity.getText());
    } catch (Exception e) {}
    return 0;
  }
}
