package com.floreantpos.ui.model;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.PizzaCrust;
import com.floreantpos.model.dao.PizzaCrustDAO;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.util.POSUtil;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;




























public class PizzaCrustForm
  extends BeanEditor
{
  private FixedLengthTextField tfName;
  private FixedLengthTextField tfDescription;
  private FixedLengthTextField tfTranslatedName;
  private FixedLengthTextField tfSortOrder;
  
  public PizzaCrustForm()
  {
    this(new PizzaCrust());
  }
  
  public PizzaCrustForm(PizzaCrust pizzaCrust) {
    initComponents();
    
    setBean(pizzaCrust);
  }
  
  private void initComponents() {
    JPanel contentPanel = new JPanel(new MigLayout("fill"));
    
    JLabel lblName = new JLabel(POSConstants.NAME + ":");
    JLabel lblDescription = new JLabel(Messages.getString("PizzaCrustForm.2"));
    JLabel lblTranslatedName = new JLabel(Messages.getString("PizzaCrustForm.3"));
    JLabel lblSortOrder = new JLabel(Messages.getString("PizzaCrustForm.4"));
    
    tfName = new FixedLengthTextField();
    tfDescription = new FixedLengthTextField();
    tfTranslatedName = new FixedLengthTextField();
    tfSortOrder = new FixedLengthTextField();
    
    contentPanel.add(lblName, "cell 0 0");
    contentPanel.add(tfName, "cell 1 0");
    contentPanel.add(lblTranslatedName, "cell 0 1");
    contentPanel.add(tfTranslatedName, "cell 1 1");
    contentPanel.add(lblDescription, "cell 0 2");
    contentPanel.add(tfDescription, "cell 1 2");
    contentPanel.add(lblSortOrder, "cell 0 3");
    contentPanel.add(tfSortOrder, "cell 1 3");
    
    add(contentPanel);
  }
  
  public boolean save()
  {
    try
    {
      if (!updateModel()) {
        return false;
      }
      PizzaCrust pizzaCrust = (PizzaCrust)getBean();
      PizzaCrustDAO dao = new PizzaCrustDAO();
      dao.saveOrUpdate(pizzaCrust);
    } catch (Exception e) {
      MessageDialog.showError(e);
      return false;
    }
    
    return true;
  }
  
  protected void updateView()
  {
    PizzaCrust pizzaCrust = (PizzaCrust)getBean();
    if (pizzaCrust == null) {
      return;
    }
    tfName.setText(pizzaCrust.getName());
    tfDescription.setText(pizzaCrust.getDescription());
    tfTranslatedName.setText(pizzaCrust.getTranslatedName());
    tfSortOrder.setText(String.valueOf(pizzaCrust.getSortOrder()));
  }
  

  protected boolean updateModel()
  {
    PizzaCrust pizzaCrust = (PizzaCrust)getBean();
    
    String name = tfName.getText();
    String description = tfDescription.getText();
    String translatedName = tfTranslatedName.getText();
    String sortOrder = tfSortOrder.getText();
    
    if (POSUtil.isBlankOrNull(name)) {
      MessageDialog.showError(Messages.getString("PizzaCrustForm.13"));
      return false;
    }
    











    pizzaCrust.setName(name);
    pizzaCrust.setDescription(description);
    pizzaCrust.setTranslatedName(translatedName);
    pizzaCrust.setSortOrder(Integer.valueOf(sortOrder));
    
    PizzaCrustDAO dao = new PizzaCrustDAO();
    dao.saveOrUpdate(pizzaCrust);
    
    return true;
  }
  
  public String getDisplayText() {
    PizzaCrust pizzaCrust = (PizzaCrust)getBean();
    if (pizzaCrust.getId() == null) {
      return Messages.getString("PizzaCrustForm.14");
    }
    return Messages.getString("PizzaCrustForm.15");
  }
}
