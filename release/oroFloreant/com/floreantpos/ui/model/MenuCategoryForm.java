package com.floreantpos.ui.model;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.Department;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuShift;
import com.floreantpos.model.dao.DepartmentDAO;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.model.dao.MenuShiftDAO;
import com.floreantpos.model.dao.OrderTypeDAO;
import com.floreantpos.swing.CheckBoxList;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.util.POSUtil;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;















public class MenuCategoryForm
  extends BeanEditor
{
  private CheckBoxList chkbDeptList;
  private CheckBoxList orderTypeCheckBoxList;
  private CheckBoxList chkbMenuShiftList;
  private JCheckBox chkBeverage;
  private JCheckBox chkVisible;
  private JLabel jLabel1;
  private FixedLengthTextField tfName;
  private IntegerTextField tfSortOrder;
  private JButton btnButtonColor;
  private JLabel lblSortOrder;
  private JLabel lblButtonColor;
  private JLabel lblTranslatedName;
  private FixedLengthTextField tfTranslatedName;
  private JLabel lblTextColor;
  private JButton btnTextColor;
  
  public MenuCategoryForm()
    throws Exception
  {
    this(new MenuCategory());
  }
  
  public MenuCategoryForm(MenuCategory category) throws Exception {
    initComponents();
    
    setBean(category);
  }
  
  public String getDisplayText() {
    MenuCategory foodCategory = (MenuCategory)getBean();
    if (foodCategory.getId() == null) {
      return POSConstants.NEW_MENU_CATEGORY;
    }
    return POSConstants.EDIT_MENU_CATEGORY;
  }
  






  private void initComponents()
  {
    jLabel1 = new JLabel();
    chkVisible = new JCheckBox();
    tfName = new FixedLengthTextField();
    tfName.setLength(120);
    chkBeverage = new JCheckBox();
    
    jLabel1.setText(POSConstants.NAME + ":");
    
    chkVisible.setText(POSConstants.VISIBLE);
    chkVisible.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    chkVisible.setMargin(new Insets(0, 0, 0, 0));
    
    chkBeverage.setText(POSConstants.BEVERAGE);
    chkBeverage.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    chkBeverage.setMargin(new Insets(0, 0, 0, 0));
    
    lblSortOrder = new JLabel(Messages.getString("MenuCategoryForm.1"));
    
    tfSortOrder = new IntegerTextField();
    tfSortOrder.setColumns(10);
    
    lblButtonColor = new JLabel(Messages.getString("MenuCategoryForm.2"));
    
    btnButtonColor = new JButton();
    btnButtonColor.setPreferredSize(new Dimension(140, 40));
    
    chkbDeptList = new CheckBoxList();
    
    List<Department> deptList = DepartmentDAO.getInstance().findAll();
    chkbDeptList.setModel(deptList);
    TitledBorder titledBorderOrderTypes = new TitledBorder(Messages.getString("MenuCategoryForm.0"));
    JScrollPane deptCheckBoxList = new JScrollPane(chkbDeptList);
    deptCheckBoxList.setBorder(titledBorderOrderTypes);
    deptCheckBoxList.setPreferredSize(new Dimension(228, 100));
    

    orderTypeCheckBoxList = new CheckBoxList();
    orderTypeCheckBoxList.setModel(OrderTypeDAO.getInstance().findAll());
    JScrollPane orderTypeScrollPane = new JScrollPane(orderTypeCheckBoxList);
    orderTypeScrollPane.setBorder(BorderFactory.createTitledBorder("Order Types"));
    orderTypeScrollPane.setPreferredSize(new Dimension(228, 120));
    
    chkbMenuShiftList = new CheckBoxList();
    List<MenuShift> menuShiftList = MenuShiftDAO.getInstance().findAll();
    chkbMenuShiftList.setModel(menuShiftList);
    TitledBorder titledBorderCategoryShifts = new TitledBorder("Menu shifts");
    JScrollPane spMenuShift = new JScrollPane(chkbMenuShiftList);
    spMenuShift.setBorder(titledBorderCategoryShifts);
    spMenuShift.setPreferredSize(new Dimension(228, 100));
    
    lblTranslatedName = new JLabel(Messages.getString("MenuCategoryForm.7"));
    tfTranslatedName = new FixedLengthTextField();
    tfTranslatedName.setLength(120);
    
    lblTextColor = new JLabel("Text color");
    
    btnTextColor = new JButton();
    btnTextColor.setText(Messages.getString("MenuCategoryForm.16"));
    btnTextColor.setPreferredSize(new Dimension(140, 40));
    
    setLayout(new MigLayout("hidemode 3", "[87px][327px,grow]", ""));
    add(jLabel1, "cell 0 0,alignx left,aligny center");
    add(tfName, "cell 1 0,growx,aligny top");
    
    add(lblTranslatedName, "cell 0 1,alignx trailing");
    add(tfTranslatedName, "cell 1 1,growx");
    add(lblSortOrder, "cell 0 2,alignx left,aligny center");
    add(tfSortOrder, "cell 1 2,alignx left,aligny top");
    


    add(lblButtonColor, "cell 0 3,h 25!,alignx left,growy");
    add(btnButtonColor, "cell 1 3,h 25!,alignx left,growy");
    add(lblTextColor, "cell 0 4,h 25!");
    add(btnTextColor, "cell 1 4,h 25!,growy");
    
    add(chkBeverage, "cell 1 5,alignx left,growy");
    add(chkVisible, "cell 1 6,alignx left,gapbottom 5,aligny top");
    
    add(orderTypeScrollPane, "cell 1 7, growy, split 2");
    add(spMenuShift, "cell 1 7, growy");
    add(deptCheckBoxList, "cell 1 8, growy");
    
    btnButtonColor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        Color color = JColorChooser.showDialog(POSUtil.getBackOfficeWindow(), Messages.getString("MenuCategoryForm.21"), 
          btnButtonColor.getBackground());
        btnButtonColor.setBackground(color);
        btnTextColor.setBackground(color);
      }
      
    });
    btnTextColor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        Color color = JColorChooser.showDialog(POSUtil.getBackOfficeWindow(), Messages.getString("MenuCategoryForm.22"), btnTextColor.getForeground());
        btnTextColor.setForeground(color);
      }
    });
  }
  
  protected void updateView() {
    MenuCategory menuCategory = (MenuCategory)getBean();
    MenuCategoryDAO.getInstance().initialize(menuCategory);
    if (menuCategory == null) {
      tfName.setText("");
      tfTranslatedName.setText("");
      tfSortOrder.setText("0");
      chkVisible.setSelected(false);
      return;
    }
    
    tfName.setText(menuCategory.getName());
    tfTranslatedName.setText(menuCategory.getTranslatedName());
    
    if (menuCategory.getSortOrder() != null) {
      tfSortOrder.setText(menuCategory.getSortOrder().toString());
    }
    
    Color buttonColor = menuCategory.getButtonColor();
    if (buttonColor != null) {
      btnButtonColor.setBackground(buttonColor);
      btnTextColor.setBackground(buttonColor);
    }
    
    if (menuCategory.getTextColor() != null) {
      btnTextColor.setForeground(menuCategory.getTextColor());
    }
    
    chkBeverage.setSelected(menuCategory.isBeverage().booleanValue());
    if (menuCategory.getId() == null) {
      chkVisible.setSelected(true);
    }
    else {
      chkVisible.setSelected(menuCategory.isVisible().booleanValue());
    }
    
    if (menuCategory.getDepartments() != null) {
      ArrayList<Department> selectedDeptList = new ArrayList(menuCategory.getDepartments());
      chkbDeptList.selectItems(selectedDeptList);
    }
    orderTypeCheckBoxList.selectItems(menuCategory.getOrderTypes());
    
    chkbMenuShiftList.selectItems(menuCategory.getMenuShifts());
  }
  
  protected boolean updateModel() {
    MenuCategory menuCategory = (MenuCategory)getBean();
    if (menuCategory == null) {
      return false;
    }
    
    String categoryName = tfName.getText();
    if (POSUtil.isBlankOrNull(categoryName)) {
      MessageDialog.showError(Messages.getString("MenuCategoryForm.26"));
      return false;
    }
    
    menuCategory.setName(categoryName);
    menuCategory.setTranslatedName(tfTranslatedName.getText());
    menuCategory.setSortOrder(Integer.valueOf(tfSortOrder.getInteger()));
    
    menuCategory.setButtonColor(btnButtonColor.getBackground());
    menuCategory.setTextColor(btnTextColor.getForeground());
    
    menuCategory.setButtonColorCode(Integer.valueOf(btnButtonColor.getBackground().getRGB()));
    menuCategory.setTextColorCode(Integer.valueOf(btnTextColor.getForeground().getRGB()));
    
    menuCategory.setBeverage(Boolean.valueOf(chkBeverage.isSelected()));
    menuCategory.setVisible(Boolean.valueOf(chkVisible.isSelected()));
    
    menuCategory.setOrderTypes(POSUtil.copySelectedValues(menuCategory.getOrderTypes(), orderTypeCheckBoxList.getCheckedValuesAsSet()));
    menuCategory.setDepartments(POSUtil.copySelectedValues(menuCategory.getDepartments(), chkbDeptList.getCheckedValuesAsSet()));
    menuCategory.setMenuShifts(POSUtil.copySelectedValues(menuCategory.getMenuShifts(), chkbMenuShiftList.getCheckedValuesAsSet()));
    


    return true;
  }
  














  public boolean save()
  {
    try
    {
      if (!updateModel()) {
        return false;
      }
      MenuCategory menuCategory = (MenuCategory)getBean();
      MenuCategoryDAO.getInstance().saveOrUpdate(menuCategory);
      return true;
    } catch (Exception x) {
      MessageDialog.showError(x); }
    return false;
  }
}
