package com.floreantpos.ui.model;

import com.floreantpos.Messages;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.model.Course;
import com.floreantpos.model.ImageResource;
import com.floreantpos.model.ImageResource.IMAGE_CATEGORY;
import com.floreantpos.model.dao.CourseDAO;
import com.floreantpos.model.dao.ImageResourceDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.ImageGalleryDialog;
import com.floreantpos.ui.dialog.ImageUploaderDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;



























public class CourseForm
  extends BeanEditor
{
  private FixedLengthTextField tfName;
  private FixedLengthTextField tfShortName;
  private IntegerTextField tfSortOrder;
  private JLabel lblImagePreview;
  private ImageResource imageResource;
  private List<Course> existingCourses;
  
  public CourseForm()
  {
    this(new Course());
  }
  
  public CourseForm(Course course) {
    initComponents();
    setBean(course);
  }
  
  private void initComponents() {
    setLayout(new MigLayout("hidemode 3,wrap 2", "[134px][204px,grow]", ""));
    
    tfSortOrder = new IntegerTextField();
    tfName = new FixedLengthTextField();
    tfName.setLength(128);
    tfShortName = new FixedLengthTextField();
    tfShortName.setLength(3);
    
    lblImagePreview = new JLabel("");
    lblImagePreview.setText("Icon");
    lblImagePreview.setForeground(Color.gray);
    
    JButton btnSelectImage = new JButton("...");
    JButton btnClearImage = new JButton(Messages.getString("MenuItemForm.34"));
    
    btnSelectImage.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doSelectImageFile();
      }
      

    });
    btnClearImage.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doClearImage();
      }
    });
    add(new JLabel("Name"), "alignx trailing,aligny center");
    add(tfName, "growx,aligny center");
    
    add(new JLabel("Short Name"), "alignx trailing,aligny center");
    add(tfShortName, "growx,aligny center");
    
    add(new JLabel("Sort Order"), "alignx trailing,aligny center");
    add(tfSortOrder, "w 100!,aligny center");
    
    JPanel imagePanel = new JPanel(new MigLayout("center", "", ""));
    
    JPanel imgPreviewPanel = new JPanel(new MigLayout("center"));
    lblImagePreview.setHorizontalAlignment(0);
    imgPreviewPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(null, "", 2, 2), 
      BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    imgPreviewPanel.setPreferredSize(new Dimension(70, 50));
    imgPreviewPanel.add(lblImagePreview, "w 24!,h 24!,aligny center");
    
    imagePanel.add(imgPreviewPanel, "wrap");
    imagePanel.add(btnClearImage, "split 2,grow,center");
    imagePanel.add(btnSelectImage, "center,grow");
    
    add(imagePanel, "skip 1");
  }
  
  public String getDisplayText()
  {
    if (((Course)getBean()).getId() == null) {
      return "New Course";
    }
    return "Edit Course";
  }
  
  public boolean save()
  {
    try {
      if (!updateModel())
        return false;
      Course course = (Course)getBean();
      CourseDAO courseDAO = CourseDAO.getInstance();
      courseDAO.saveOrUpdate(course);
    } catch (PosException x) {
      POSMessageDialog.showError(this, x.getMessage(), x);
      PosLog.error(getClass(), x);
      return false;
    } catch (Exception x) {
      POSMessageDialog.showError(this, "Error", x);
      PosLog.error(getClass(), x);
      return false;
    }
    return true;
  }
  
  protected boolean updateModel()
    throws IllegalModelStateException
  {
    String courseName = tfName.getText();
    String shortName = tfShortName.getText();
    int sortOrder = tfSortOrder.getInteger();
    
    if (StringUtils.isEmpty(courseName)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please enter course name.");
      return false;
    }
    if (StringUtils.isEmpty(shortName)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please enter short name.");
      return false;
    }
    if (sortOrder <= 0) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please enter sort order.");
      return false;
    }
    if (existingCourses != null) {
      for (Course course : existingCourses) {
        if (course.getSortOrder().intValue() == sortOrder) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Duplicate sort order cannot be allowed.");
          return false;
        }
      }
    }
    



    Course course = (Course)getBean();
    course.setName(courseName);
    course.setShortName(shortName);
    course.setSortOrder(Integer.valueOf(sortOrder));
    
    if (imageResource != null) {
      course.setIconId(imageResource.getId());
    }
    else {
      saveShortNameAsImage(course);
    }
    return true;
  }
  
  private void saveShortNameAsImage(Course course) {
    try {
      ImageResource imageResource = null;
      if (course.getId() != null)
        imageResource = ImageResourceDAO.getInstance().findById(course.getId());
      if (imageResource == null) {
        imageResource = new ImageResource();
      }
      String shortName = course.getShortName();
      imageResource.setDescription(shortName);
      Rectangle rect = new Rectangle(16, 16);
      BufferedImage bufferedImage = new BufferedImage(16, 16, 1);
      Graphics g = bufferedImage.getGraphics();
      g.setColor(new Color(29, 96, 124));
      g.fillRect(0, 0, 16, 16);
      g.setColor(Color.white);
      int fontSize = 10;
      if (shortName.length() == 1) {
        fontSize = 12;
      }
      else if (shortName.length() == 3) {
        fontSize = 7;
      }
      Font font = new Font(null, 1, fontSize);
      FontMetrics metrics = g.getFontMetrics(font);
      int x = x + (width - metrics.stringWidth(shortName)) / 2;
      int y = y + (height - metrics.getHeight()) / 2 + metrics.getAscent();
      g.setFont(font);
      g.drawString(shortName, x, y);
      if (bufferedImage != null) {
        imageResource.setImageData(POSUtil.convertImageToBlob(bufferedImage, "png"));
      }
      imageResource.setImageCategoryNum(Integer.valueOf(ImageResource.IMAGE_CATEGORY.UNLISTED.getType()));
      ImageResourceDAO.getInstance().saveOrUpdate(imageResource);
      course.setIconId(imageResource.getId());
    } catch (Exception e) {
      PosLog.error(ImageUploaderDialog.class, e.getMessage(), e);
    }
  }
  
  protected void updateView()
  {
    if (!(getBean() instanceof Course)) {
      return;
    }
    Course course = (Course)getBean();
    setData(course);
  }
  
  private void setData(Course course) {
    tfSortOrder.setText(String.valueOf(course.getSortOrder()));
    tfName.setText(course.getName());
    tfShortName.setText(course.getShortName());
    
    imageResource = ImageResourceDAO.getInstance().findById(course.getIconId());
    if (imageResource != null) {
      lblImagePreview.setIcon(imageResource.getScaledImage(16, 16));
      lblImagePreview.setText("");
    }
    else {
      lblImagePreview.setText("Icon");
    }
  }
  
  protected void doSelectImageFile() {
    ImageGalleryDialog dialog = ImageGalleryDialog.getInstance();
    dialog.setSelectBtnVisible(true);
    dialog.setTitle("Image Gallery");
    dialog.setSize(PosUIManager.getSize(650, 600));
    dialog.setResizable(false);
    dialog.open();
    if (dialog.isCanceled()) {
      return;
    }
    imageResource = dialog.getImageResource();
    if (imageResource != null) {
      lblImagePreview.setIcon(imageResource.getScaledImage(16, 16));
      lblImagePreview.setText("");
    }
    else {
      lblImagePreview.setText("Icon");
    }
  }
  
  protected void doClearImage() {
    lblImagePreview.setIcon(null);
    imageResource = null;
  }
  
  public List<Course> getExistingCourses() {
    return existingCourses;
  }
  
  public void setExistingCourses(List<Course> courses) {
    existingCourses = new ArrayList();
    Course course = (Course)getBean();
    int nextSortOrder = 1;
    for (Course c : courses)
      if ((course.getId() == null) || (!c.getId().equals(course.getId())))
      {
        if (c.getSortOrder().intValue() > nextSortOrder) {
          nextSortOrder = c.getSortOrder().intValue();
        }
        existingCourses.add(c);
      }
    if (tfSortOrder.getInteger() == 0) {
      tfSortOrder.setText(String.valueOf(nextSortOrder + 1));
    }
  }
}
