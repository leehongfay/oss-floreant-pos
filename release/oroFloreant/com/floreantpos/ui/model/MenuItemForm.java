package com.floreantpos.ui.model;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.explorer.ComboItemExplorer;
import com.floreantpos.bo.ui.explorer.MenuItemVariantExplorer;
import com.floreantpos.bo.ui.modifierdesigner.ModifierPageDesigner;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.InventoryPlugin;
import com.floreantpos.model.Course;
import com.floreantpos.model.ImageResource;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.InventoryVendorItems;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuItemModifierPage;
import com.floreantpos.model.MenuItemModifierPageItem;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.MenuItemShift;
import com.floreantpos.model.PrinterGroup;
import com.floreantpos.model.Recepie;
import com.floreantpos.model.ReportGroup;
import com.floreantpos.model.Shift;
import com.floreantpos.model.TaxGroup;
import com.floreantpos.model.dao.CourseDAO;
import com.floreantpos.model.dao.ImageResourceDAO;
import com.floreantpos.model.dao.InventoryUnitDAO;
import com.floreantpos.model.dao.InventoryVendorItemsDAO;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.model.dao.PrinterGroupDAO;
import com.floreantpos.model.dao.RecepieDAO;
import com.floreantpos.model.dao.ReportGroupDAO;
import com.floreantpos.model.dao.TaxGroupDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthDocument;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IUpdatebleView;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.menuitem.ButtonStylePanel;
import com.floreantpos.ui.menuitem.InventoryPanel;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.PosGuiUtil;
import com.floreantpos.util.ShiftUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.hibernate.Hibernate;
import org.hibernate.Session;






















public class MenuItemForm
  extends BeanEditor<MenuItem>
  implements ActionListener, ChangeListener
{
  private MenuItem menuItem;
  private JButton btnDeleteModifierGroup = new JButton();
  private JButton btnEditModifierGroup = new JButton();
  private JButton btnNewModifierGroup = new JButton();
  private JComboBox cbReportGroup = new JComboBox();
  private JComboBox cbTaxGroup = new JComboBox();
  private JCheckBox chkVisible = new JCheckBox();
  private JLabel lfname = new JLabel();
  private JLabel lblPrice = new JLabel();
  private JLabel lTax = new JLabel();
  private JPanel tabGeneral = new JPanel();
  private JPanel tabModifier = new JPanel();
  private JScrollPane modifiertabScrollpane = new JScrollPane();
  private JTabbedPane tabbedPane = new JTabbedPane();
  private JTable modifierSpecTable = new JTable();
  private DoubleTextField tfDiscountRate = new DoubleTextField(18);
  private FixedLengthTextField tfName = new FixedLengthTextField(20, 255);
  private FixedLengthTextField tfTranslatedName = new FixedLengthTextField(20, 255);
  private DoubleTextField tfPrice = new DoubleTextField(10);
  private DoubleTextField tfCost = new DoubleTextField(10);
  private JTextArea tfDescription = new JTextArea(new FixedLengthDocument(512));
  
  private List<MenuItemModifierSpec> menuItemModifierSpecList;
  private MenuItemMGListModel menuItemMGListModel = new MenuItemMGListModel();
  private JLabel lblKitchenPrinter = new JLabel(Messages.getString("MenuItemForm.27"));
  private JComboBox<PrinterGroup> cbPrinterGroup = new JComboBox(new DefaultComboBoxModel(PrinterGroupDAO.getInstance().findAll()
    .toArray(new PrinterGroup[0])));
  private JLabel lblBarcode = new JLabel(Messages.getString("MenuItemForm.lblBarcode.text"));
  private FixedLengthTextField tfBarcode = new FixedLengthTextField(25, 128);
  private JLabel lblUnitName = new JLabel(Messages.getString("MenuItemForm.23"));
  private JLabel lblTranslatedName = new JLabel(Messages.getString("MenuItemForm.lblTranslatedName.text"));
  
  private FixedLengthTextField tfUnitName = new FixedLengthTextField(10, 20);
  
  private JCheckBox cbFractionalUnit = new JCheckBox(Messages.getString("MenuItemForm.24"));
  private DoubleTextField tfStockCount = new DoubleTextField(1);
  private JCheckBox cbRawMaterial = new JCheckBox(Messages.getString("MenuItemForm.31"));
  private JCheckBox cbIsInventory = new JCheckBox(Messages.getString("MenuItemForm.44"));
  private JLabel lblReportGroup = new JLabel();
  private JButton btnNewReportGroup = new JButton();
  private JButton btnNewMenuGroup = new JButton("+");
  
  private boolean inventoryItemMode = false;
  
  private ModifierPageDesigner modifierDesigner;
  
  private JCheckBox chkComboItem;
  private JCheckBox chkHasVariant = new JCheckBox("Has variant");
  private MenuItemVariantExplorer variantTab;
  private ComboItemExplorer tabComboItems;
  private JComboBox cbUnits = new JComboBox();
  private JComboBox<MenuGroup> cbMenuGroup = new JComboBox();
  private JComboBox<Course> cbCourse = new JComboBox();
  private JComboBox<Recepie> cbRecipe = new JComboBox();
  private InventoryPanel inventoryPanel;
  private ButtonStylePanel buttonStylePanel;
  private JCheckBox chkPrintToKitchen;
  private JCheckBox chkPrintKitchenSticker;
  
  public MenuItemForm() throws Exception
  {
    this(new MenuItem(), false);
  }
  
  public MenuItemForm(MenuItem menuItem) throws Exception {
    this(menuItem, false);
  }
  
  public MenuItemForm(MenuItem menuItem, boolean inventoryMode) throws Exception {
    this.menuItem = menuItem;
    inventoryItemMode = inventoryMode;
    
    initComponents();
    initData();
    setBean(menuItem);
  }
  
  public void initData() {
    MenuGroupDAO foodGroupDAO = new MenuGroupDAO();
    List<MenuGroup> foodGroups = new ArrayList();
    foodGroups.add(null);
    if (foodGroupDAO.findAll() != null) {
      foodGroups.addAll(foodGroupDAO.findAll());
    }
    cbMenuGroup.setModel(new ComboBoxModel(foodGroups));
    
    CourseDAO courseDAO = new CourseDAO();
    List<Course> courses = new ArrayList();
    courses.add(null);
    List<Course> courseList = courseDAO.findAll();
    if ((courseList != null) && (courseList.size() > 0))
      courses.addAll(courseList);
    ComboBoxModel aModel = new ComboBoxModel(courses);
    cbCourse.setModel(aModel);
    cbCourse.setSelectedItem(null);
    
    ComboBoxModel recipeModel = new ComboBoxModel();
    recipeModel.addElement(null);
    for (Recepie recipe : RecepieDAO.getInstance().findAll()) {
      recipeModel.addElement(recipe);
    }
    cbRecipe.setModel(recipeModel);
    cbRecipe.setSelectedItem(null);
    
    ReportGroupDAO reportGroupDao = new ReportGroupDAO();
    List<ReportGroup> reportGroups = reportGroupDao.findAll();
    cbReportGroup.setModel(new ComboBoxModel(reportGroups));
    
    ComboBoxModel comboBoxModel = new ComboBoxModel();
    comboBoxModel.addElement(null);
    List<TaxGroup> taxGroups = TaxGroupDAO.getInstance().findAll();
    for (TaxGroup taxGroup : taxGroups) {
      comboBoxModel.addElement(taxGroup);
    }
    cbTaxGroup.setModel(comboBoxModel);
    
    menuItemModifierSpecList = menuItem.getMenuItemModiferSpecs();
    tabComboItems.setMenuItem(menuItem);
    variantTab.setParentMenuItem(menuItem);
  }
  
  public void addRecepieExtension() {
    InventoryPlugin plugin = (InventoryPlugin)ExtensionManager.getPlugin(InventoryPlugin.class);
    if (plugin == null) {
      return;
    }
    
    plugin.addRecepieView(tabbedPane);
    if (cbUnits.isVisible()) {
      plugin.addStockUnitView(tabbedPane, menuItem);
      cbUnits.addItemListener(new ItemListener()
      {
        public void itemStateChanged(ItemEvent e)
        {
          menuItem.setUnit((InventoryUnit)cbUnits.getSelectedItem());
        }
      });
    }
  }
  
  private Component getUnitField()
  {
    InventoryPlugin plugin = (InventoryPlugin)ExtensionManager.getPlugin(InventoryPlugin.class);
    if (plugin == null) {
      cbUnits.setVisible(false);
      return tfUnitName;
    }
    lblUnitName.setText("Unit");
    ComboBoxModel model = new ComboBoxModel();
    model.addElement(null);
    for (InventoryUnit inventoryUnit : InventoryUnitDAO.getInstance().findAll()) {
      model.addElement(inventoryUnit);
    }
    cbUnits.setModel(model);
    return cbUnits;
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    
    initGeneralTab();
    initModifierGroupTab();
    addRecepieExtension();
    initComboTab();
    initAttributeTab();
    initBtnstyleTab();
    initInverntoryTab();
  }
  
  private void initComboTab()
  {
    tabComboItems = new ComboItemExplorer();
  }
  
  private void initAttributeTab()
  {
    variantTab = new MenuItemVariantExplorer();
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(200));
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(50));
    
    return columnWidth;
  }
  
  private void btnNewModifierGroupActionPerformed(ActionEvent evt) {}
  
  private void addMenuItemModifierGroup()
  {
    try
    {
      MenuItemModifierSpec modifierSpec = new MenuItemModifierSpec();
      MenuItemModifierSpecForm form = new MenuItemModifierSpecForm(modifierSpec, menuItem, false);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), form);
      dialog.getButtonPanel().add(form.getAutoBuildButton(), 0);
      dialog.openWithScale(500, 500);
      if (dialog.isCanceled()) {
        return;
      }
      menuItemMGListModel.add(form.getBean());
      modifierSpecTable.getSelectionModel().setSelectionInterval(menuItemMGListModel.getRowCount() - 1, menuItemMGListModel.getRowCount() - 1);
      if (form.isAutoBuildSelected()) {
        modifierDesigner.doGenenateMenuItemModifierPageItems();
      }
    } catch (Exception x) {
      MessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void editMenuItemModifierGroup() {
    editSelectedRow();
  }
  
  private void editSelectedRow() {
    try {
      int index = modifierSpecTable.getSelectedRow();
      if (index < 0) {
        return;
      }
      MenuItemModifierSpec menuItemModifierGroup = menuItemMGListModel.get(index);
      MenuItemModifierSpecForm form = new MenuItemModifierSpecForm(menuItemModifierGroup, menuItem, true);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), form);
      dialog.openWithScale(500, 500);
      if (!dialog.isCanceled()) {
        menuItemMGListModel.fireTableRowsUpdated(index, index);
      }
    } catch (Exception x) {
      MessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void deleteMenuItemModifierGroup() {
    try {
      int index = modifierSpecTable.getSelectedRow();
      if (index < 0) {
        return;
      }
      if (ConfirmDeleteDialog.showMessage(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.CONFIRM) == 0)
      {
        menuItemMGListModel.remove(index);
        if (menuItemMGListModel.getRowCount() > 0) {
          modifierSpecTable.getSelectionModel().setSelectionInterval(menuItemMGListModel.getRowCount() - 1, menuItemMGListModel.getRowCount() - 1);
        }
        else {
          modifierDesigner.reset();
        }
      }
    } catch (Exception x) {
      MessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public boolean save()
  {
    try
    {
      if (!updateModel()) {
        return false;
      }
      MenuItem menuItem = (MenuItem)getBean();
      MenuItemDAO.getInstance().saveOrUpdate(menuItem);
      List<InventoryVendorItems> vendorItems = inventoryPanel.getVendorItems();
      for (InventoryVendorItems inventoryVendorItems : vendorItems) {
        InventoryVendorItemsDAO.getInstance().saveOrUpdate(inventoryVendorItems);
      }
    } catch (Exception e) {
      MessageDialog.showError(POSConstants.ERROR_MESSAGE, e);
      return false;
    }
    return true;
  }
  
  protected void updateView()
  {
    MenuItem menuItem = (MenuItem)getBean();
    
    if ((menuItem.getId() != null) && (!Hibernate.isInitialized(menuItem.getMenuItemModiferSpecs())))
    {
      MenuItemDAO dao = new MenuItemDAO();
      Session session = dao.getSession();
      menuItem = (MenuItem)session.merge(menuItem);
      Hibernate.initialize(menuItem.getMenuItemModiferSpecs());
      session.close();
    }
    
    tfName.setText(menuItem.getName());
    tfDescription.setText(menuItem.getDescription());
    tfTranslatedName.setText(menuItem.getTranslatedName());
    tfBarcode.setText(menuItem.getBarcode());
    tfCost.setText(NumberUtil.trimDecilamIfNotNeeded(menuItem.getCost()));
    tfPrice.setText(NumberUtil.formatNumber(menuItem.getPrice()));
    if (menuItem.getUnit() != null) {
      cbUnits.setSelectedItem(menuItem.getUnit());
      tfUnitName.setText(menuItem.getUnit().getCode());
    }
    tfDiscountRate.setText(String.valueOf(menuItem.getDiscountRate()));
    tfStockCount.setText(String.valueOf(menuItem.getAvailableUnit()));
    chkVisible.setSelected(menuItem.isVisible().booleanValue());
    buttonStylePanel.setShowTextWithImage(menuItem.isShowImageOnly().booleanValue());
    cbRawMaterial.setSelected(menuItem.isRawMaterial().booleanValue());
    cbIsInventory.setSelected(menuItem.isInventoryItem().booleanValue());
    
    if (!menuItem.isInventoryItem().booleanValue()) {
      tabbedPane.remove(inventoryPanel);
    }
    tabbedPane.addChangeListener(this);
    ImageResource imageResource = ImageResourceDAO.getInstance().findById(menuItem.getImageId());
    if (imageResource != null) {
      buttonStylePanel.setImageResource(imageResource);
    }
    
    cbTaxGroup.setSelectedItem(menuItem.getTaxGroup());
    
    if (menuItem.getMenuGroupId() != null) {
      PosGuiUtil.selectComboItemById(cbMenuGroup, menuItem.getMenuGroupId());
    }
    if (menuItem.getCourseId() != null) {
      PosGuiUtil.selectComboItemById(cbCourse, menuItem.getCourseId());
    }
    if (menuItem.getDefaultRecipeId() != null) {
      PosGuiUtil.selectComboItemById(cbRecipe, menuItem.getDefaultRecipeId());
    }
    cbPrinterGroup.setSelectedItem(menuItem.getPrinterGroup());
    
    buttonStylePanel.setSortOrder(menuItem.getSortOrder().intValue());
    
    Color buttonColor = menuItem.getButtonColor();
    if (buttonColor != null) {
      buttonStylePanel.setButtonColor(buttonColor);
      buttonStylePanel.setTextColor(buttonColor);
    }
    
    if (menuItem.getTextColor() != null) {
      buttonStylePanel.setTextForegroundColor(menuItem.getTextColor());
    }
    cbFractionalUnit.setSelected(menuItem.isFractionalUnit().booleanValue());
    chkComboItem.setSelected(menuItem.isComboItem().booleanValue());
    chkHasVariant.setSelected(menuItem.isHasVariant().booleanValue());
    
    inventoryPanel.setSku(menuItem.getSku());
    inventoryPanel.setReOrderLevel(menuItem.getReorderLevel().doubleValue());
    inventoryPanel.setReplenishLevel(menuItem.getReplenishLevel().doubleValue());
    inventoryPanel.setCbDisableStockCount(menuItem.isDisableWhenStockAmountIsZero().booleanValue());
    
    menuItemModifierSpecList = menuItem.getMenuItemModiferSpecs();
    
    chkPrintToKitchen.setSelected(menuItem.isShouldPrintToKitchen().booleanValue());
    chkPrintKitchenSticker.setSelected(menuItem.isPrintKitchenSticker().booleanValue());
  }
  
  public boolean updateModel()
  {
    String itemName = tfName.getText();
    if (POSUtil.isBlankOrNull(itemName)) {
      MessageDialog.showError(POSConstants.NAME_REQUIRED);
      return false;
    }
    
    MenuItem menuItem = (MenuItem)getBean();
    menuItem.setName(itemName);
    menuItem.setDescription(tfDescription.getText());
    menuItem.setBarcode(tfBarcode.getText());
    menuItem.setCost(Double.valueOf(tfCost.getDoubleOrZero()));
    menuItem.setPrice(Double.valueOf(tfPrice.getText()));
    if (cbUnits.isVisible()) {
      InventoryUnit unit = (InventoryUnit)cbUnits.getSelectedItem();
      if (unit == null) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select unit!");
        return false;
      }
      
      menuItem.setUnit(unit);
      menuItem.setUnitName(unit.getCode());
    }
    else
    {
      menuItem.setUnitName(tfUnitName.getText()); }
    menuItem.setTaxGroup((TaxGroup)cbTaxGroup.getSelectedItem());
    menuItem.setAvailableUnit(Double.valueOf(Double.parseDouble(tfStockCount.getText())));
    menuItem.setVisible(Boolean.valueOf(chkVisible.isSelected()));
    menuItem.setShowImageOnly(Boolean.valueOf(buttonStylePanel.isShowTextWithImage()));
    menuItem.setFractionalUnit(Boolean.valueOf(cbFractionalUnit.isSelected()));
    menuItem.setRawMaterial(Boolean.valueOf(cbRawMaterial.isSelected()));
    menuItem.setInventoryItem(Boolean.valueOf(cbIsInventory.isSelected()));
    
    menuItem.setTranslatedName(tfTranslatedName.getText());
    
    menuItem.setSortOrder(buttonStylePanel.getSortOrder());
    menuItem.setButtonColorCode(Integer.valueOf(buttonStylePanel.getButtonColorCode()));
    menuItem.setTextColorCode(Integer.valueOf(buttonStylePanel.getTextColorCode()));
    try
    {
      menuItem.setDiscountRate(Double.valueOf(Double.parseDouble(tfDiscountRate.getText())));
    }
    catch (Exception localException) {}
    
    if ((menuItemModifierSpecList != null) && (menuItemModifierSpecList.size() > 0)) {
      for (MenuItemModifierSpec menuItemModifierSpec : menuItemModifierSpecList) {
        int minQuantity = menuItemModifierSpec.getMinQuantity().intValue();
        int itemCount = 0;
        for (Iterator localIterator1 = menuItemModifierSpec.getModifierPages().iterator(); localIterator1.hasNext();) { itemModifierPage = (MenuItemModifierPage)localIterator1.next();
          if (menuItemModifierSpec.getId() != null) {
            itemModifierPage.setModifierSpecId(menuItemModifierSpec.getId());
          }
          List<MenuItemModifierPageItem> pageItems = itemModifierPage.getPageItems();
          if (pageItems != null) {
            itemCount += pageItems.size();
            for (MenuItemModifierPageItem menuItemModifierPageItem : pageItems) {
              if (itemModifierPage.getId() != null)
                menuItemModifierPageItem.setParentPage(itemModifierPage);
            }
          }
        }
        MenuItemModifierPage itemModifierPage;
        if (itemCount < minQuantity) {
          Component componentAt = tabbedPane.getSelectedComponent();
          if (componentAt != tabModifier) {
            tabbedPane.setSelectedComponent(tabModifier);
          }
          int quaDiff = minQuantity - itemCount;
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), " Please add " + quaDiff + " more " + (quaDiff > 1 ? "items" : "item") + " in " + menuItemModifierSpec
            .getName() + " group \n Minimum quantity required " + minQuantity + ".");
          return false;
        }
      }
    }
    menuItem.setMenuItemModiferSpecs(menuItemModifierSpecList);
    menuItem.setHasModifiers(Boolean.valueOf((menuItemModifierSpecList != null) && (menuItemModifierSpecList.size() > 0)));
    menuItem.setHasMandatoryModifiers(Boolean.valueOf(false));
    if (menuItem.hasModifiers()) {
      for (MenuItemModifierSpec menuItemModifierGroup : menuItemModifierSpecList) {
        if ((menuItemModifierGroup.getMinQuantity().intValue() > 0) || (menuItemModifierGroup.isAutoShow().booleanValue())) {
          menuItem.setHasMandatoryModifiers(Boolean.valueOf(true));
          break;
        }
      }
    }
    int tabCount = tabbedPane.getTabCount();
    Component componentAt; for (int i = 0; i < tabCount; i++) {
      componentAt = tabbedPane.getComponent(i);
      if ((componentAt instanceof IUpdatebleView)) {
        IUpdatebleView view = (IUpdatebleView)componentAt;
        if (!view.updateModel(menuItem)) {
          return false;
        }
      }
    }
    
    menuItem.setMenuGroup((MenuGroup)cbMenuGroup.getSelectedItem());
    menuItem.setCourse((Course)cbCourse.getSelectedItem());
    menuItem.setDefaultRecipeId(null);
    if ((cbRecipe.getSelectedItem() instanceof Recepie)) {
      String defaultRecipeId = ((Recepie)cbRecipe.getSelectedItem()).getId();
      menuItem.setDefaultRecipeId(defaultRecipeId);
    }
    menuItem.setPrinterGroup((PrinterGroup)cbPrinterGroup.getSelectedItem());
    menuItem.setComboItem(Boolean.valueOf(chkComboItem.isSelected()));
    menuItem.setHasVariant(Boolean.valueOf(chkHasVariant.isSelected()));
    if (menuItem.isComboItem().booleanValue())
    {






      menuItem.setComboItems(tabComboItems.getSelectedComboItems());
      menuItem.setComboGroups(tabComboItems.getSelectedComboGroups());
    }
    if (menuItem.isHasVariant().booleanValue()) {
      List<MenuItem> newVariants = variantTab.getVariants();
      if (newVariants != null) {
        for (MenuItem menuItem2 : newVariants) {
          menuItem2.setInventoryItem(Boolean.valueOf(cbIsInventory.isSelected()));
          menuItem2.setParentMenuItem(menuItem);
        }
      }
      menuItem.setVariants(POSUtil.copySelectedValues(menuItem.getVariants(), newVariants));
    }
    
    menuItem.setSku(inventoryPanel.getSku());
    menuItem.setReorderLevel(Double.valueOf(inventoryPanel.getReOrderLevel()));
    menuItem.setReplenishLevel(Double.valueOf(inventoryPanel.getReplenishLevel()));
    menuItem.setDisableWhenStockAmountIsZero(Boolean.valueOf(inventoryPanel.isCbDisableStockCount()));
    
    menuItem.setImageId(buttonStylePanel.getImageResourceId());
    
    menuItem.setShouldPrintToKitchen(Boolean.valueOf(chkPrintToKitchen.isSelected()));
    menuItem.setPrintKitchenSticker(Boolean.valueOf(chkPrintKitchenSticker.isSelected()));
    
    return true;
  }
  
  public String getDisplayText() {
    MenuItem foodItem = (MenuItem)getBean();
    if (foodItem.getId() == null) {
      return POSConstants.NEW_MENU_ITEM;
    }
    return POSConstants.EDIT_MENU_ITEM;
  }
  
  class MenuItemMGListModel extends AbstractTableModel {
    String[] cn = { POSConstants.GROUP_NAME, "MIN", "MAX" };
    

    MenuItemMGListModel() {}
    
    public MenuItemModifierSpec get(int index)
    {
      return (MenuItemModifierSpec)menuItemModifierSpecList.get(index);
    }
    
    public void add(MenuItemModifierSpec group) {
      if (menuItemModifierSpecList == null) {
        menuItemModifierSpecList = new ArrayList();
      }
      menuItemModifierSpecList.add(group);
      fireTableDataChanged();
    }
    
    public void remove(int index) {
      if (menuItemModifierSpecList == null) {
        return;
      }
      menuItemModifierSpecList.remove(index);
      fireTableDataChanged();
    }
    
    public List<MenuItemModifierSpec> getItems() {
      return menuItemModifierSpecList;
    }
    
    public void remove(MenuItemModifierSpec group) {
      if (menuItemModifierSpecList == null) {
        return;
      }
      menuItemModifierSpecList.remove(group);
      fireTableDataChanged();
    }
    
    public int getRowCount() {
      if (menuItemModifierSpecList == null) {
        return 0;
      }
      return menuItemModifierSpecList.size();
    }
    
    public int getColumnCount()
    {
      return cn.length;
    }
    
    public String getColumnName(int column)
    {
      return cn[column];
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      MenuItemModifierSpec menuItemModifierGroup = (MenuItemModifierSpec)menuItemModifierSpecList.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return menuItemModifierGroup.getName();
      






      case 1: 
        return Integer.valueOf(menuItemModifierGroup.getMinQuantity().intValue());
      
      case 2: 
        return Integer.valueOf(menuItemModifierGroup.getMaxQuantity().intValue());
      }
      return null;
    }
  }
  
  class ShiftTableModel extends AbstractTableModel {
    List<MenuItemShift> shifts;
    String[] cn = { POSConstants.START_TIME, POSConstants.END_TIME, POSConstants.PRICE };
    Calendar calendar = Calendar.getInstance();
    
    ShiftTableModel() {
      if (shifts == null) {
        this.shifts = new ArrayList();
      }
      else {
        this.shifts = new ArrayList(shifts);
      }
    }
    
    public MenuItemShift get(int index) {
      return (MenuItemShift)shifts.get(index);
    }
    
    public void add(MenuItemShift group) {
      if (shifts == null) {
        shifts = new ArrayList();
      }
      shifts.add(group);
      fireTableDataChanged();
    }
    
    public void remove(int index) {
      if (shifts == null) {
        return;
      }
      shifts.remove(index);
      fireTableDataChanged();
    }
    
    public void remove(MenuItemShift group) {
      if (shifts == null) {
        return;
      }
      shifts.remove(group);
      fireTableDataChanged();
    }
    
    public int getRowCount() {
      if (shifts == null) {
        return 0;
      }
      return shifts.size();
    }
    
    public int getColumnCount()
    {
      return cn.length;
    }
    
    public String getColumnName(int column)
    {
      return cn[column];
    }
    
    public List<MenuItemShift> getShifts() {
      return shifts;
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      MenuItemShift shift = (MenuItemShift)shifts.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return ShiftUtil.buildShiftTimeRepresentation(shift.getShift().getStartTime());
      
      case 1: 
        return ShiftUtil.buildShiftTimeRepresentation(shift.getShift().getEndTime());
      
      case 2: 
        return String.valueOf(shift.getShiftPrice());
      }
      return null;
    }
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    if (actionCommand.equals("AddModifierGroup")) {
      addMenuItemModifierGroup();
    }
    else if (actionCommand.equals("EditModifierGroup")) {
      editMenuItemModifierGroup();
    }
    else if (actionCommand.equals("DeleteModifierGroup")) {
      deleteMenuItemModifierGroup();
    }
  }
  
  public void stateChanged(ChangeEvent e)
  {
    Component selectedComponent = tabbedPane.getSelectedComponent();
    if (!(selectedComponent instanceof IUpdatebleView)) {
      return;
    }
    
    IUpdatebleView view = (IUpdatebleView)selectedComponent;
    
    MenuItem menuItem = (MenuItem)getBean();
    view.initView(menuItem);
  }
  
  private void doCreateNewGroup() {
    MenuGroupForm editor = new MenuGroupForm();
    BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
    dialog.open();
    
    if (!dialog.isCanceled()) {
      MenuGroup foodGroup = (MenuGroup)editor.getBean();
      ComboBoxModel model = (ComboBoxModel)cbMenuGroup.getModel();
      model.addElement(foodGroup);
      model.setSelectedItem(foodGroup);
    }
  }
  
  private void doCreateReportGroup(ActionEvent evt) {
    ReportGroupForm editor = new ReportGroupForm();
    BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
    dialog.open();
    
    if (!dialog.isCanceled()) {
      ReportGroup reportGroup = (ReportGroup)editor.getBean();
      ComboBoxModel model = (ComboBoxModel)cbReportGroup.getModel();
      model.addElement(reportGroup);
      model.setSelectedItem(reportGroup);
    }
  }
  
  public void initModifierGroupTab()
  {
    modifierDesigner = new ModifierPageDesigner(menuItem);
    
    btnDeleteModifierGroup.setText(POSConstants.DELETE);
    btnEditModifierGroup.setText(POSConstants.EDIT);
    btnNewModifierGroup.setText(POSConstants.ADD);
    btnDeleteModifierGroup.setActionCommand("DeleteModifierGroup");
    

    btnEditModifierGroup.setActionCommand("EditModifierGroup");
    
    modifierSpecTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        if (e.getValueIsAdjusting()) {
          return;
        }
        int index = modifierSpecTable.getSelectedRow();
        if (index < 0) {
          return;
        }
        MenuItemModifierSpec menuItemModifierSpec = menuItemMGListModel.get(index);
        modifierDesigner.setMenuItemModifierSpec(menuItemModifierSpec);
      }
      

    });
    modifierSpecTable.setModel(menuItemMGListModel);
    modifierSpecTable.setRowHeight(PosUIManager.getSize(30));
    modifierSpecTable.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          MenuItemForm.this.editSelectedRow();
        }
      }
    });
    btnNewModifierGroup.addActionListener(this);
    btnEditModifierGroup.addActionListener(this);
    btnDeleteModifierGroup.addActionListener(this);
    
    btnNewModifierGroup.setActionCommand("AddModifierGroup");
    btnNewModifierGroup.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        MenuItemForm.this.btnNewModifierGroupActionPerformed(evt);
      }
    });
    tabModifier.setLayout(new BorderLayout(10, 10));
    
    JPanel groupsPanel = new JPanel(new BorderLayout());
    TitledBorder groupsPanelBorder = BorderFactory.createTitledBorder(new EmptyBorder(5, 5, 0, 5), "Groups", 2, 2);
    groupsPanelBorder.setTitleJustification(2);
    groupsPanel.setBorder(groupsPanelBorder);
    
    groupsPanel.add(modifiertabScrollpane);
    
    JPanel btmpanel = new JPanel();
    btmpanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    groupsPanel.add(btmpanel, "South");
    
    btmpanel.add(btnNewModifierGroup);
    btmpanel.add(btnEditModifierGroup);
    btmpanel.add(btnDeleteModifierGroup);
    
    tabModifier.add(groupsPanel, "West");
    tabModifier.add(modifierDesigner);
    
    tabbedPane.addTab(POSConstants.MODIFIER_GROUPS, tabModifier);
    
    modifiertabScrollpane.setViewportView(modifierSpecTable);
    modifierSpecTable.getColumnModel().getColumn(0).setPreferredWidth(200);
    modifierSpecTable.getColumnModel().getColumn(1).setPreferredWidth(45);
    modifierSpecTable.getColumnModel().getColumn(2).setPreferredWidth(45);
    modifierSpecTable.setAutoResizeMode(3);
  }
  
  public void initGeneralTab()
  {
    int comboWidth = PosUIManager.getSize(200);
    chkComboItem = new JCheckBox("Combo Item");
    lfname.setText(Messages.getString("LABEL_NAME"));
    lfname.setHorizontalAlignment(11);
    lblPrice.setHorizontalAlignment(11);
    lblPrice.setText("Sales Price (" + CurrencyUtil.getCurrencySymbol() + ")");
    lTax.setHorizontalAlignment(11);
    lTax.setText("Tax Group");
    tfPrice.setHorizontalAlignment(4);
    tfCost.setHorizontalAlignment(4);
    
    lblReportGroup.setHorizontalAlignment(11);
    lblReportGroup.setText(Messages.getString("MenuItemForm.45"));
    btnNewReportGroup.setText("+");
    
    JPanel column1 = new JPanel(new MigLayout("fillx", "[][grow]", ""));
    JPanel column2 = new JPanel(new MigLayout("fillx", "[][grow]", ""));
    
    tabGeneral.setLayout(new GridLayout(1, 2, 40, 5));
    
    chkPrintToKitchen = new JCheckBox("Print to kitchen");
    chkPrintKitchenSticker = new JCheckBox("Print kitchen sticker");
    
    column1.add(lfname, "right");
    column1.add(tfName, "grow, wrap");
    column1.add(lblTranslatedName, "right");
    column1.add(tfTranslatedName, "grow, wrap");
    column1.add(lblUnitName, "right");
    column1.add(getUnitField(), "width 150!, wrap");
    column1.add(lblPrice, "right");
    column1.add(tfPrice, "wrap");
    column1.add(new JLabel("Cost (" + CurrencyUtil.getCurrencySymbol() + ")"), "right");
    column1.add(tfCost, "wrap");
    column1.add(new JLabel("Group"), "right");
    column1.add(cbMenuGroup, "split 2, w " + comboWidth);
    column1.add(btnNewMenuGroup, "wrap");
    column1.add(lblReportGroup, "right");
    column1.add(cbReportGroup, "split 2, w " + comboWidth);
    column1.add(btnNewReportGroup, "wrap");
    column1.add(lblBarcode, "right");
    column1.add(tfBarcode, "wrap");
    

    column1.add(chkVisible, "skip 1,wrap");
    column1.add(cbFractionalUnit, "growx 8,skip 1,wrap");
    column1.add(cbRawMaterial, "skip 1,wrap");
    column1.add(cbIsInventory, "skip 1,wrap");
    column1.add(chkComboItem, "skip 1,wrap");
    column1.add(chkHasVariant, "skip 1,wrap");
    column1.add(chkPrintToKitchen, "skip 1,wrap");
    column1.add(chkPrintKitchenSticker, "skip 1,wrap");
    
    chkVisible.setText(POSConstants.VISIBLE);
    chkVisible.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    chkVisible.setMargin(new Insets(0, 0, 0, 0));
    
    column2.add(lblKitchenPrinter, "right");
    column2.add(cbPrinterGroup, "wrap, w " + comboWidth + "!");
    column2.add(lTax, "right");
    column2.add(cbTaxGroup, "wrap, w " + comboWidth + "!");
    column2.add(new JLabel("Course"), "right");
    column2.add(cbCourse, "wrap, w " + comboWidth + "!");
    column2.add(new JLabel("Default recipe"), "right");
    column2.add(cbRecipe, "wrap, w " + comboWidth + "!");
    JLabel descLabel = new JLabel("Description");
    descLabel.setVerticalAlignment(1);
    descLabel.setHorizontalAlignment(11);
    tfDescription.setLineWrap(true);
    JScrollPane scrlDescription = new JScrollPane(tfDescription, 20, 30);
    column2.add(descLabel, "align right, grow");
    column2.add(scrlDescription, "h 50%,grow,wrap");
    
    tabGeneral.add(column1, "grow");
    tabGeneral.add(column2, "grow");
    tabbedPane.addTab(POSConstants.GENERAL, tabGeneral);
    tabbedPane.setPreferredSize(new Dimension(750, 470));
    
    add(tabbedPane);
    
    chkComboItem.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    chkComboItem.setMargin(new Insets(0, 0, 0, 0));
    chkComboItem.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if (e.getStateChange() == 1) {
          tabbedPane.addTab("Combo Items", tabComboItems);
        }
        else {
          tabbedPane.remove(tabComboItems);
        }
        
      }
    });
    chkHasVariant.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    chkHasVariant.setMargin(new Insets(0, 0, 0, 0));
    chkHasVariant.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == 1) {
          if (tfName.getText().isEmpty()) {
            POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please enter item name.");
            chkHasVariant.setSelected(false);
            return;
          }
          if (cbMenuGroup.getSelectedItem() == null) {
            POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select group.");
            chkHasVariant.setSelected(false);
            return;
          }
          tabbedPane.addTab("Variant", variantTab);
        }
        else {
          tabbedPane.remove(variantTab);
        }
        
      }
    });
    btnNewMenuGroup.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        MenuItemForm.this.doCreateNewGroup();
      }
    });
    btnNewReportGroup.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        MenuItemForm.this.doCreateReportGroup(evt);
      }
    });
    tfName.addFocusListener(new FocusListener()
    {
      public void focusLost(FocusEvent e) {
        menuItem.setName(tfName.getText());
      }
      

      public void focusGained(FocusEvent e) {}
    });
  }
  

  public void initBtnstyleTab()
  {
    buttonStylePanel = new ButtonStylePanel();
    tabbedPane.addTab(Messages.getString("MenuItemForm.26"), buttonStylePanel);
  }
  
  public void initInverntoryTab() {
    inventoryPanel = new InventoryPanel(menuItem);
    
    tabbedPane.addTab(Messages.getString("MenuItemForm.64"), inventoryPanel);
    if (inventoryItemMode) {
      cbIsInventory.setVisible(false);
      inventoryPanel.setVisible(true);
    }
  }
}
