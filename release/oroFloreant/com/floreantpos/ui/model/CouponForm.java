package com.floreantpos.ui.model;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.Discount;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.dao.DiscountDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.MenuItemSelectionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import net.authorize.util.StringUtils;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXDatePicker;




















public class CouponForm
  extends BeanEditor
  implements ItemListener
{
  private JPanel contentPane;
  private JPanel itemPanel;
  private FixedLengthTextField tfCouponName;
  private FixedLengthTextField tfBarcode;
  private JComboBox cbQualificationType;
  private JComboBox cbCouponType;
  private DoubleTextField tfCouponValue;
  private JCheckBox chkEnabled;
  private JCheckBox chkModifiable;
  private JCheckBox chkAutoApply;
  private JCheckBox chkNeverExpire;
  private JXDatePicker dpExperation;
  private JLabel lblMinimum;
  private DoubleTextField tfMinimumQua;
  private DoubleTextField tfMaxUnit = new DoubleTextField();
  
  private JScrollPane itemScrollPane;
  private List<MenuItem> addedItems = new ArrayList();
  private JTextField tfSearch;
  private JPanel itemSearchPanel;
  private BeanTableModel itemModel;
  
  public CouponForm() {
    this(new Discount());
  }
  
  public CouponForm(Discount coupon) {
    initializeComponent();
    
    cbCouponType.setModel(new DefaultComboBoxModel(Discount.COUPON_TYPE_NAMES));
    cbCouponType.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if (cbCouponType.getSelectedItem() == Discount.COUPON_TYPE_NAMES[2]) {
          cbQualificationType.setEnabled(false);
          tfMinimumQua.setEnabled(false);
          tfMaxUnit.setEnabled(false);
        }
        else {
          cbQualificationType.setEnabled(true);
          tfMinimumQua.setEnabled(true);
          tfMaxUnit.setEnabled(true);
        }
        
      }
    });
    cbQualificationType.setModel(new DefaultComboBoxModel(Discount.COUPON_QUALIFICATION_NAMES));
    cbQualificationType.addItemListener(this);
    cbCouponType.addItemListener(this);
    
    setBean(coupon);
  }
  
  private void initializeComponent() {
    setLayout(new BorderLayout(10, 10));
    
    contentPane = new JPanel();
    contentPane.setLayout(new MigLayout());
    contentPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5), null));
    contentPane.setPreferredSize(new Dimension(400, 0));
    
    JLabel label1 = new JLabel(Messages.getString("CouponForm.0") + ":");
    JLabel label2 = new JLabel(Messages.getString("CouponForm.9") + ":");
    JLabel label3 = new JLabel(Messages.getString("CouponForm.11") + ":");
    JLabel label4 = new JLabel(Messages.getString("CouponForm.13") + ":");
    JLabel label6 = new JLabel(Messages.getString("CouponForm.12"));
    JLabel label5 = new JLabel(Messages.getString("CouponForm.7"));
    lblMinimum = new JLabel(Messages.getString("CouponForm.5"));
    
    tfCouponName = new FixedLengthTextField(120);
    tfBarcode = new FixedLengthTextField(120);
    cbCouponType = new JComboBox();
    cbQualificationType = new JComboBox();
    dpExperation = new JXDatePicker();
    tfCouponValue = new DoubleTextField();
    tfMinimumQua = new DoubleTextField();
    chkEnabled = new JCheckBox(POSConstants.ENABLED);
    chkModifiable = new JCheckBox("Modifiable Amount");
    chkAutoApply = new JCheckBox(Messages.getString("CouponForm.6"));
    chkNeverExpire = new JCheckBox(Messages.getString("CouponForm.16"));
    
    contentPane.add(label1);
    contentPane.add(tfCouponName, "grow, wrap");
    contentPane.add(label2);
    contentPane.add(dpExperation, "grow, wrap");
    contentPane.add(label6);
    contentPane.add(tfBarcode, "grow, wrap");
    contentPane.add(label5);
    contentPane.add(cbQualificationType, "grow, wrap");
    contentPane.add(lblMinimum);
    contentPane.add(tfMinimumQua, "grow, wrap");
    contentPane.add(new JLabel("Maximum Unit:"));
    contentPane.add(tfMaxUnit, "grow, wrap");
    contentPane.add(label3);
    contentPane.add(cbCouponType, "grow, wrap");
    contentPane.add(label4);
    contentPane.add(tfCouponValue, "grow, wrap");
    contentPane.add(new JLabel(""));
    contentPane.add(chkEnabled, "wrap");
    contentPane.add(new JLabel(""));
    contentPane.add(chkAutoApply, "wrap");
    contentPane.add(new JLabel(""));
    contentPane.add(chkNeverExpire, "wrap");
    contentPane.add(new JLabel(""));
    contentPane.add(chkModifiable, "wrap");
    
    createItemSearchPanel();
    
    itemPanel = new JPanel(new BorderLayout(10, 10));
    itemPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5), null));
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Discounted items");
    
    itemPanel.add(titlePanel, "North");
    tfSearch = new JTextField();
    tfSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        CouponForm.this.doSearchItem();
      }
    });
    JButton btnSearch = new JButton("Search");
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ev)
      {
        CouponForm.this.doSearchItem();
      }
      
    });
    JTable itemTable = new JTable();
    itemModel = new BeanTableModel(MenuItem.class, 2);
    itemModel.addColumn("Item name", MenuItem.PROP_NAME);
    itemModel.addColumn("Item price", MenuItem.PROP_PRICE);
    itemTable.setModel(itemModel);
    itemTable.setRowHeight(30);
    
    itemPanel.add(itemSearchPanel, "South");
    itemScrollPane = new JScrollPane(itemTable);
    
    JPanel centerPanel = new JPanel(new MigLayout("fill, ins 0"));
    centerPanel.add(new JLabel("Search by name"), "split 3");
    centerPanel.add(tfSearch, "grow");
    centerPanel.add(btnSearch, "wrap");
    centerPanel.add(itemScrollPane, "grow");
    itemPanel.add(centerPanel, "Center");
    
    add(contentPane, "West");
    add(itemPanel, "Center");
    
    setPreferredSize(new Dimension(700, 350));
  }
  
  private void createItemSearchPanel() {
    itemSearchPanel = new JPanel();
    itemSearchPanel.setLayout(new MigLayout("ins 0, center"));
    
    JButton btnAdd = new JButton("ADD/EDIT ITEMS");
    btnAdd.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        CouponForm.this.doOpenItemSelectionDialog();
      }
    });
    itemSearchPanel.add(btnAdd);
  }
  
  public void itemStateChanged(ItemEvent event)
  {
    if (event.getItem() == Discount.COUPON_QUALIFICATION_NAMES[0]) {
      itemPanel.setVisible(true);
    }
    else if (event.getItem() == Discount.COUPON_TYPE_NAMES[0]) {
      chkModifiable.setVisible(true);
    }
    else if (event.getItem() == Discount.COUPON_TYPE_NAMES[1]) {
      chkModifiable.setVisible(false);




    }
    else
    {



      itemPanel.setVisible(false);
    }
  }
  
  public boolean save()
  {
    try
    {
      if (!updateModel()) {
        return false;
      }
      Discount coupon = (Discount)getBean();
      DiscountDAO.getInstance().saveOrUpdate(coupon);
    }
    catch (Exception e) {
      MessageDialog.showError(POSConstants.SAVE_ERROR, e);
      return false;
    }
    return true;
  }
  
  protected void updateView()
  {
    Discount coupon = (Discount)getBean();
    if (coupon.getId() == null) {
      chkEnabled.setSelected(true);
      tfMinimumQua.setText("0");
      tfMaxUnit.setText("0");
      cbCouponType.setSelectedIndex(1);
      return;
    }
    
    tfCouponName.setText(coupon.getName());
    tfMinimumQua.setText(coupon.getMinimumBuy().toString());
    tfMaxUnit.setText(coupon.getMaximumOff().toString());
    tfCouponValue.setText(String.valueOf(coupon.getValue()));
    cbCouponType.setSelectedIndex(coupon.getType().intValue());
    cbQualificationType.setSelectedIndex(coupon.getQualificationType().intValue());
    dpExperation.setDate(coupon.getExpiryDate());
    tfBarcode.setText(coupon.getBarcode());
    chkEnabled.setSelected(coupon.isEnabled().booleanValue());
    chkModifiable.setSelected(coupon.isModifiable().booleanValue());
    chkAutoApply.setSelected(coupon.isAutoApply().booleanValue());
    chkNeverExpire.setSelected(coupon.isNeverExpire().booleanValue());
    
    if ((coupon.getQualificationType().intValue() == 0) && 
      (coupon.getMenuItems() != null)) {
      itemModel.setRows(coupon.getMenuItems());
      addedItems.addAll(itemModel.getRows());
    }
  }
  









  protected boolean updateModel()
  {
    String name = tfCouponName.getText();
    String barcode = tfBarcode.getText();
    double couponValue = 0.0D;
    couponValue = tfCouponValue.getDoubleOrZero();
    double couponMinimumQua = tfMinimumQua.getDoubleOrZero();
    double couponMaximunUnit = tfMaxUnit.getDoubleOrZero();
    int couponType = cbCouponType.getSelectedIndex();
    Date expiryDate = dpExperation.getDate();
    boolean enabled = chkEnabled.isSelected();
    boolean modifiable = chkModifiable.isSelected();
    boolean autoApply = chkAutoApply.isSelected();
    boolean neverExpire = chkNeverExpire.isSelected();
    int qualificationType = cbQualificationType.getSelectedIndex();
    
    if ((name == null) || (name.trim().equals(""))) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), Messages.getString("CouponForm.1"));
      return false;
    }
    if (couponValue <= 0.0D) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), Messages.getString("CouponForm.2"));
      return false;
    }
    if ((qualificationType == 0) && (couponValueOverflow())) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), Messages.getString("CouponForm.10"));
      return false;
    }
    
    Discount coupon = (Discount)getBean();
    coupon.setName(name);
    coupon.setMinimumBuy(Double.valueOf(couponMinimumQua));
    coupon.setMaximumOff(Double.valueOf(couponMaximunUnit));
    coupon.setValue(Double.valueOf(couponValue));
    coupon.setExpiryDate(expiryDate);
    coupon.setBarcode(barcode);
    coupon.setType(Integer.valueOf(couponType));
    coupon.setQualificationType(Integer.valueOf(qualificationType));
    coupon.setEnabled(Boolean.valueOf(enabled));
    coupon.setModifiable(Boolean.valueOf(modifiable));
    coupon.setAutoApply(Boolean.valueOf(autoApply));
    coupon.setNeverExpire(Boolean.valueOf(neverExpire));
    
    if (qualificationType == 0) {
      tfSearch.setText("");
      doSearchItem();
      if ((itemModel.getRows() != null) && (itemModel.getRows().size() > 0)) {
        coupon.setMenuItems(itemModel.getRows());
        coupon.setApplyToAll(Boolean.valueOf(false));
      }
      else {
        coupon.setMenuItems(null);
        coupon.setApplyToAll(Boolean.valueOf(true));
      }
    }
    






    return true;
  }
  
  private boolean couponValueOverflow() {
    List<MenuItem> menuItems = itemModel.getRows();
    double couponValue = tfCouponValue.getDoubleOrZero();
    if (cbCouponType.getSelectedIndex() == 1) {
      couponValue /= 100.0D;
    }
    double minimumQua = tfMinimumQua.getDouble();
    if (minimumQua > 0.0D) {
      for (MenuItem menuItem : menuItems) {
        if (couponValue > menuItem.getPrice().doubleValue() * minimumQua) {
          return true;
        }
        
      }
    } else {
      for (MenuItem menuItem : menuItems) {
        if (couponValue > menuItem.getPrice().doubleValue()) {
          return true;
        }
      }
    }
    return false;
  }
  
  public String getDisplayText()
  {
    Discount coupon = (Discount)getBean();
    if (coupon.getId() == null) {
      return Messages.getString("CouponForm.3");
    }
    return Messages.getString("CouponForm.4");
  }
  
  private void doSearchItem() {
    try {
      String searchTxt = tfSearch.getText();
      if (StringUtils.isEmpty(searchTxt)) {
        itemModel.setRows(addedItems);
        return;
      }
      
      List<MenuItem> searchItems = new ArrayList();
      for (MenuItem entry : addedItems) {
        String itemName = entry.getName();
        if ((itemName.contains(searchTxt)) || (itemName.toLowerCase().contains(searchTxt)) || (itemName.toLowerCase().contains(searchTxt.toLowerCase()))) {
          searchItems.add(entry);
        }
      }
      itemModel.setRows(searchItems);
      itemModel.fireTableDataChanged();
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    }
  }
  
  private void doOpenItemSelectionDialog() {
    try {
      tfSearch.setText("");
      doSearchItem();
      
      MenuItemSelectionDialog dialog = new MenuItemSelectionDialog(itemModel.getRows());
      dialog.setSelectionMode(1);
      dialog.setSize(PosUIManager.getSize(750, 515));
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      List<MenuItem> menuItemList = dialog.getSelectedItems();
      itemModel.setRows(menuItemList);
      addedItems.clear();
      addedItems.addAll(menuItemList);
    }
    catch (Exception ex) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), ex.getMessage(), ex);
    }
  }
}
