package com.floreantpos.ui.model;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.MenuItemSize;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.Multiplier;
import com.floreantpos.model.PizzaModifierPrice;
import com.floreantpos.model.TaxGroup;
import com.floreantpos.model.dao.MenuItemSizeDAO;
import com.floreantpos.model.dao.MenuModifierDAO;
import com.floreantpos.model.dao.ModifierDAO;
import com.floreantpos.model.dao.MultiplierDAO;
import com.floreantpos.model.dao.TaxGroupDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.views.order.multipart.PizzaPriceTableModel;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import net.miginfocom.swing.MigLayout;































public class PizzaModifierForm
  extends BeanEditor
{
  private MenuModifier modifier;
  private JCheckBox chkPrintToKitchen;
  private JComboBox cbTaxGroup;
  private JFormattedTextField tfName;
  private FixedLengthTextField tfTranslatedName;
  private DoubleTextField tfNormalPrice;
  private DoubleTextField tfExtraPrice;
  private IntegerTextField tfSortOrder;
  private JButton btnButtonColor;
  private JButton btnTextColor;
  private JTable priceTable;
  private JTabbedPane jTabbedPane1;
  private JTable pizzaModifierPriceTable;
  private PizzaPriceTableModel pizzaModifierPriceTableModel;
  private JCheckBox chkUseFixedPrice;
  private JCheckBox chkSectionWisePrice;
  
  public PizzaModifierForm()
    throws Exception
  {
    this(new MenuModifier());
  }
  
  public PizzaModifierForm(MenuModifier modifier) throws Exception {
    this.modifier = modifier;
    MenuModifierDAO.getInstance().initialize(modifier);
    checkRegularMultiplier();
    initComponents();
    initData();
    
    setBean(modifier);
  }
  
  public void initData()
  {
    ComboBoxModel comboBoxModel = new ComboBoxModel();
    comboBoxModel.addElement(null);
    TaxGroupDAO taxGroupDAO = new TaxGroupDAO();
    List<TaxGroup> taxGroups = taxGroupDAO.findAll();
    for (TaxGroup taxGroup : taxGroups) {
      comboBoxModel.addElement(taxGroup);
    }
    
    cbTaxGroup.setModel(comboBoxModel);
  }
  
  private void initComponents()
  {
    setLayout(new BorderLayout(0, 0));
    
    jTabbedPane1 = new JTabbedPane();
    
    tfName = new JFormattedTextField();
    tfTranslatedName = new FixedLengthTextField();
    tfNormalPrice = new DoubleTextField();
    tfExtraPrice = new DoubleTextField();
    tfSortOrder = new IntegerTextField();
    cbTaxGroup = new JComboBox();
    JButton btnNewTax = new JButton();
    chkPrintToKitchen = new JCheckBox();
    chkSectionWisePrice = new JCheckBox();
    
    JScrollPane jScrollPane3 = new JScrollPane();
    priceTable = new JTable();
    
    JLabel lblName = new JLabel(POSConstants.NAME + ":");
    JLabel lblTranslatedName = new JLabel(Messages.getString("MenuModifierForm.0"));
    JLabel lblSortOrder = new JLabel(Messages.getString("MenuModifierForm.15"));
    JLabel lblTaxRate = new JLabel("Tax Group:");
    JLabel lblPercentage = new JLabel();
    
    tfExtraPrice.setText("0");
    lblPercentage.setText("");
    tfNormalPrice.setText("0");
    
    btnNewTax.setText("...");
    btnNewTax.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        PizzaModifierForm.this.btnNewTaxActionPerformed(evt);
      }
      
    });
    chkPrintToKitchen.setText(POSConstants.PRINT_TO_KITCHEN);
    chkPrintToKitchen.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    chkPrintToKitchen.setMargin(new Insets(0, 0, 0, 0));
    
    chkSectionWisePrice.setText("Sectionwise price");
    chkSectionWisePrice.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    chkSectionWisePrice.setMargin(new Insets(0, 0, 0, 0));
    
    chkUseFixedPrice = new JCheckBox("Use fixed price");
    
    JPanel generalTabPanel = new JPanel(new BorderLayout());
    jTabbedPane1.addTab(POSConstants.GENERAL, generalTabPanel);
    
    TransparentPanel inputPanel = new TransparentPanel();
    inputPanel.setLayout(new MigLayout("fill", "[60%][40%]", ""));
    
    TransparentPanel lelfInputPanel = new TransparentPanel();
    lelfInputPanel.setLayout(new MigLayout("wrap 2,hidemode 3", "[90px][grow]", ""));
    
    lelfInputPanel.add(lblName, "alignx left,aligny center");
    lelfInputPanel.add(tfName, "growx,aligny top");
    
    lelfInputPanel.add(lblTranslatedName, "alignx left,aligny center");
    lelfInputPanel.add(tfTranslatedName, "growx");
    


    JPanel rightInputPanel = new JPanel(new MigLayout("wrap 2", "[86px][grow]"));
    
    rightInputPanel.add(lblTaxRate, "alignx left,aligny center,split 2");
    rightInputPanel.add(lblPercentage, "alignx left,aligny center");
    rightInputPanel.add(cbTaxGroup, "growx,aligny top,split 2");
    rightInputPanel.add(btnNewTax, "alignx left,aligny top");
    
    rightInputPanel.add(lblSortOrder, "alignx left,aligny center");
    rightInputPanel.add(tfSortOrder, "growx,aligny top");
    
    rightInputPanel.add(chkPrintToKitchen, "skip 1,alignx left,aligny top");
    rightInputPanel.add(chkSectionWisePrice, "skip 1");
    
    inputPanel.add(lelfInputPanel, "grow");
    inputPanel.add(rightInputPanel, "grow");
    
    generalTabPanel.add(inputPanel, "North");
    
    JLabel lblButtonColor = new JLabel(Messages.getString("MenuModifierForm.1"));
    btnButtonColor = new JButton("");
    btnButtonColor.setPreferredSize(new Dimension(140, 40));
    
    JLabel lblTextColor = new JLabel(Messages.getString("MenuModifierForm.27"));
    btnTextColor = new JButton(Messages.getString("MenuModifierForm.29"));
    btnTextColor.setPreferredSize(new Dimension(140, 40));
    
    JPanel tabButtonStyle = new JPanel(new MigLayout("hidemode 3,wrap 2"));
    tabButtonStyle.add(lblButtonColor);
    tabButtonStyle.add(btnButtonColor);
    tabButtonStyle.add(lblTextColor);
    tabButtonStyle.add(btnTextColor);
    
    jTabbedPane1.addTab("Button Style", tabButtonStyle);
    
    btnButtonColor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        Color color = JColorChooser.showDialog(PizzaModifierForm.this, Messages.getString("MenuModifierForm.39"), btnButtonColor.getBackground());
        btnButtonColor.setBackground(color);
        btnTextColor.setBackground(color);
      }
      
    });
    btnTextColor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        Color color = JColorChooser.showDialog(PizzaModifierForm.this, Messages.getString("MenuModifierForm.40"), btnTextColor.getForeground());
        btnTextColor.setForeground(color);
      }
      
    });
    priceTable.setModel(new DefaultTableModel(new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null } }, new String[] { "Title 1", "Title 2", "Title 3", "Title 4" }));
    


    jScrollPane3.setViewportView(priceTable);
    createPizzaModifierPricePanel(generalTabPanel);
    
    add(jTabbedPane1);
  }
  
  private void checkRegularMultiplier() {
    Multiplier multiplier = MultiplierDAO.getInstance().get("Regular");
    if ((multiplier != null) && (multiplier.isMain().booleanValue())) {
      return;
    }
    if (multiplier == null) {
      multiplier = new Multiplier("Regular");
      multiplier.setRate(Double.valueOf(0.0D));
      multiplier.setSortOrder(Integer.valueOf(0));
      multiplier.setTicketPrefix("");
      multiplier.setDefaultMultiplier(Boolean.valueOf(true));
      multiplier.setMain(Boolean.valueOf(true));
      MultiplierDAO.getInstance().save(multiplier);
    }
    else {
      multiplier.setMain(Boolean.valueOf(true));
      MultiplierDAO.getInstance().update(multiplier);
    }
  }
  
  private void createPizzaModifierPricePanel(JPanel generalTabPanel) {
    List<PizzaModifierPrice> pizzaModifierPriceList = modifier.getPizzaModifierPriceList();
    pizzaModifierPriceList = generatePossibleModifierPriceList(pizzaModifierPriceList);
    pizzaModifierPriceTable = new JTable() {
      public void changeSelection(int row, int column, boolean toggle, boolean extend) {
        super.changeSelection(row, column, toggle, extend);
        pizzaModifierPriceTable.editCellAt(row, column);
        pizzaModifierPriceTable.transferFocus();
        DefaultCellEditor editor = (DefaultCellEditor)pizzaModifierPriceTable.getCellEditor(row, column);
        if (column != 0) {
          DoubleTextField textField = (DoubleTextField)editor.getComponent();
          textField.requestFocus();
          textField.selectAll();
        }
      }
    };
    pizzaModifierPriceTable.setRowHeight(PosUIManager.getSize(22));
    pizzaModifierPriceTable.setCellSelectionEnabled(true);
    pizzaModifierPriceTable.setSelectionMode(0);
    pizzaModifierPriceTable.setSurrendersFocusOnKeystroke(true);
    
    pizzaModifierPriceTableModel = new PizzaPriceTableModel(pizzaModifierPriceList, MultiplierDAO.getInstance().findAll());
    pizzaModifierPriceTable.setModel(pizzaModifierPriceTableModel);
    







    JScrollPane pizzaModifierPriceTabScrollPane = new JScrollPane();
    pizzaModifierPriceTabScrollPane.setViewportView(pizzaModifierPriceTable);
    
    JPanel pizzaModifierPricePanel = new JPanel();
    pizzaModifierPricePanel.setLayout(new BorderLayout());
    pizzaModifierPricePanel.setPreferredSize(PosUIManager.getSize(600, 250));
    pizzaModifierPricePanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    pizzaModifierPricePanel.add(pizzaModifierPriceTabScrollPane, "Center");
    
    generalTabPanel.add(pizzaModifierPricePanel);
    
    DoubleTextField tfPrice = new DoubleTextField();
    tfPrice.setAllowNegativeValue(true);
    tfPrice.setHorizontalAlignment(4);
    DefaultCellEditor editorPrice = new DefaultCellEditor(tfPrice);
    editorPrice.setClickCountToStart(1);
    pizzaModifierPriceTable.setDefaultEditor(pizzaModifierPriceTable.getColumnClass(1), editorPrice);
  }
  
  private List<PizzaModifierPrice> generatePossibleModifierPriceList(List<PizzaModifierPrice> pizzaModifierPriceList) {
    List<MenuItemSize> menuItemSizeList = MenuItemSizeDAO.getInstance().findAll();
    List<MenuItemSize> menuItemSizeTempList = new ArrayList();
    int i;
    if ((pizzaModifierPriceList == null) || (pizzaModifierPriceList.isEmpty())) {
      pizzaModifierPriceList = new ArrayList();
      for (i = 0; i < menuItemSizeList.size(); i++) {
        PizzaModifierPrice pizzaModifierPrice = new PizzaModifierPrice();
        pizzaModifierPrice.setSize((MenuItemSize)menuItemSizeList.get(i));
        pizzaModifierPrice.setPrice(0.0D);
        pizzaModifierPrice.setExtraPrice(0.0D);
        
        pizzaModifierPriceList.add(pizzaModifierPrice);
      }
      return pizzaModifierPriceList;
    }
    

    for (PizzaModifierPrice pizzaModifierPrice : pizzaModifierPriceList)
    {
      menuItemSizeTempList.add(pizzaModifierPrice.getSize());
    }
    
    menuItemSizeList.removeAll(menuItemSizeTempList);
    
    for (int i = 0; i < menuItemSizeList.size(); i++) {
      PizzaModifierPrice pizzaModifierPrice = new PizzaModifierPrice();
      pizzaModifierPrice.setSize((MenuItemSize)menuItemSizeList.get(i));
      pizzaModifierPrice.setPrice(0.0D);
      pizzaModifierPrice.setExtraPrice(0.0D);
      pizzaModifierPriceList.add(pizzaModifierPrice);
    }
    return pizzaModifierPriceList;
  }
  
  private void btnNewTaxActionPerformed(ActionEvent evt)
  {
    try {
      TaxForm editor = new TaxForm();
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      ComboBoxModel model = (ComboBoxModel)cbTaxGroup.getModel();
      model.setDataList(TaxGroupDAO.getInstance().findAll());
    } catch (Exception x) {
      MessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      MenuModifier modifier = (MenuModifier)getBean();
      ModifierDAO dao = new ModifierDAO();
      dao.saveOrUpdate(modifier);
    } catch (Exception e) {
      MessageDialog.showError(POSConstants.SAVE_ERROR, e);
      return false;
    }
    return true;
  }
  
  protected void updateView()
  {
    MenuModifier modifier = (MenuModifier)getBean();
    
    if (modifier == null) {
      tfName.setText("");
      tfNormalPrice.setText("0");
      tfExtraPrice.setText("0");
      return;
    }
    
    tfName.setText(modifier.getName());
    tfTranslatedName.setText(modifier.getTranslatedName());
    tfNormalPrice.setText(String.valueOf(modifier.getPrice()));
    tfExtraPrice.setText(String.valueOf(modifier.getExtraPrice()));
    chkPrintToKitchen.setSelected(modifier.isShouldPrintToKitchen().booleanValue());
    chkSectionWisePrice.setSelected(modifier.isShouldSectionWisePrice().booleanValue());
    chkUseFixedPrice.setSelected(modifier.isFixedPrice().booleanValue());
    
    if (modifier.getSortOrder() != null) {
      tfSortOrder.setText(modifier.getSortOrder().toString());
    }
    
    if (modifier.getButtonColor() != null) {
      Color color = new Color(modifier.getButtonColor().intValue());
      btnButtonColor.setBackground(color);
      btnTextColor.setBackground(color);
    }
    
    if (modifier.getTextColor() != null) {
      Color color = new Color(modifier.getTextColor().intValue());
      btnTextColor.setForeground(color);
    }
    
    if (modifier.getTaxGroup() != null) {
      cbTaxGroup.setSelectedItem(modifier.getTaxGroup());
    }
  }
  
  protected boolean updateModel()
  {
    MenuModifier modifier = (MenuModifier)getBean();
    
    String name = tfName.getText();
    if (POSUtil.isBlankOrNull(name)) {
      MessageDialog.showError(Messages.getString("MenuModifierForm.44"));
      return false;
    }
    
    modifier.setName(name);
    modifier.setPrice(Double.valueOf(tfNormalPrice.getDouble()));
    modifier.setExtraPrice(Double.valueOf(tfExtraPrice.getDouble()));
    modifier.setTaxGroup((TaxGroup)cbTaxGroup.getSelectedItem());
    modifier.setShouldPrintToKitchen(Boolean.valueOf(chkPrintToKitchen.isSelected()));
    modifier.setShouldSectionWisePrice(Boolean.valueOf(chkSectionWisePrice.isSelected()));
    
    modifier.setTranslatedName(tfTranslatedName.getText());
    modifier.setButtonColor(Integer.valueOf(btnButtonColor.getBackground().getRGB()));
    modifier.setTextColor(Integer.valueOf(btnTextColor.getForeground().getRGB()));
    modifier.setSortOrder(Integer.valueOf(tfSortOrder.getInteger()));
    modifier.setFixedPrice(Boolean.valueOf(chkUseFixedPrice.isSelected()));
    modifier.setMultiplierPriceList(null);
    
    modifier.setPizzaModifier(Boolean.valueOf(true));
    List<PizzaModifierPrice> rows = pizzaModifierPriceTableModel.getRows(modifier);
    modifier.setPizzaModifierPriceList(rows);
    return true;
  }
  
  public String getDisplayText() {
    MenuModifier modifier = (MenuModifier)getBean();
    if (modifier.getId() == null) {
      return Messages.getString("MenuModifierForm.45");
    }
    return Messages.getString("MenuModifierForm.46");
  }
}
