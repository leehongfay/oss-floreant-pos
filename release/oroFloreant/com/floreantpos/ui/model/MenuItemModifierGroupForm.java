package com.floreantpos.ui.model;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.Tag;
import com.floreantpos.model.dao.MenuModifierDAO;
import com.floreantpos.model.dao.TagDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Set;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;





























public class MenuItemModifierGroupForm
  extends BeanEditor
{
  private JLabel lblName;
  private JLabel lblTranslatedName;
  private JLabel lblMin;
  private JLabel lblMax;
  private JLabel lblTag;
  private JLabel lblEnabled;
  private JLabel lblSortOrder;
  private FixedLengthTextField tfName = new FixedLengthTextField(60);
  private FixedLengthTextField tfTranslatedName = new FixedLengthTextField(60);
  private IntegerTextField tfMaxQuantity = new IntegerTextField();
  private IntegerTextField tfMinQuantity = new IntegerTextField();
  private IntegerTextField tfSortOrder = new IntegerTextField();
  
  private JCheckBox ckbEnable;
  private List<MenuItemModifierSpec> menuItemModifierGroupsList;
  private JXTable modifierSelectionTable;
  private ModifierTableModel modifierTableModel;
  private JComboBox cbTag;
  private JCheckBox ckbJump;
  private JLabel lblJump;
  
  public MenuItemModifierGroupForm(List<MenuItemModifierSpec> menuItemModifierGroupsList)
  {
    this(new MenuItemModifierSpec(), menuItemModifierGroupsList);
  }
  
  public MenuItemModifierGroupForm(MenuItemModifierSpec modifierGroup, List<MenuItemModifierSpec> menuItemModifierGroupsList) {
    initComponents();
    
    this.menuItemModifierGroupsList = menuItemModifierGroupsList;
    initList();
    setBean(modifierGroup);
  }
  
  private void initList()
  {
    TagDAO tagDao = new TagDAO();
    List<Tag> tagList = tagDao.findAll();
    cbTag.setModel(new ComboBoxModel(tagList));
    











    cbTag.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        searchItem();
      }
      
      private void searchItem()
      {
        Tag selectedType = (Tag)cbTag.getSelectedItem();
        















        List<MenuModifier> menuModifierList = MenuModifierDAO.getInstance().getModifierList(selectedType.getName());
        
        modifierTableModel.setRows(menuModifierList);
      }
      

    });
    List<MenuModifier> findAll = MenuModifierDAO.getInstance().findAll();
    modifierTableModel = new ModifierTableModel();
    modifierTableModel.setRows(findAll);
    
    modifierSelectionTable = new JXTable(modifierTableModel);
    




    modifierSelectionTable.setRowHeight(PosUIManager.getSize(30));
    
    add(new JScrollPane(modifierSelectionTable), "span 2, grow,newline");
  }
  














  private void initComponents()
  {
    setLayout(new MigLayout("fillx", "[grow][grow]", ""));
    
    lblName = new JLabel(Messages.getString("MenuItemModifierGroupForm.4"));
    lblTranslatedName = new JLabel(Messages.getString("MenuItemModifierGroupForm.5"));
    lblMin = new JLabel(Messages.getString("MenuItemModifierGroupForm.6"));
    lblMax = new JLabel(Messages.getString("MenuItemModifierGroupForm.7"));
    lblTag = new JLabel(Messages.getString("MenuItemModifierGroupForm.8"));
    lblEnabled = new JLabel(Messages.getString("MenuItemModifierGroupForm.10"));
    lblSortOrder = new JLabel(Messages.getString("MenuItemModifierGroupForm.11"));
    lblJump = new JLabel(Messages.getString("MenuItemModifierGroupForm.12"));
    
    ckbEnable = new JCheckBox();
    ckbJump = new JCheckBox();
    
    cbTag = new JComboBox();
    cbTag.setPreferredSize(new Dimension(198, 0));
    
    tfName.setColumns(20);
    tfTranslatedName.setColumns(20);
    tfMinQuantity.setColumns(10);
    tfMaxQuantity.setColumns(10);
    tfSortOrder.setColumns(10);
    
    add(lblName);
    add(tfName);
    add(lblTranslatedName, "newline");
    add(tfTranslatedName);
    add(lblMin, "newline");
    add(tfMinQuantity);
    add(lblMax, "newline");
    add(tfMaxQuantity);
    add(lblEnabled, "newline");
    add(ckbEnable);
    add(lblSortOrder, "newline");
    add(tfSortOrder);
    add(lblJump, "newline");
    add(ckbJump);
    add(lblTag, "newline");
    add(cbTag);
  }
  
















  public boolean save()
  {
    return updateModel();
  }
  
  protected void updateView()
  {
    MenuItemModifierSpec menuItemModifierGroup = getBean();
    if (menuItemModifierGroup == null) {
      return;
    }
    














    tfName.setText(menuItemModifierGroup.getName());
    tfTranslatedName.setText(menuItemModifierGroup.getTranslatedName());
    tfMinQuantity.setText(String.valueOf(menuItemModifierGroup.getMinQuantity()));
    tfMaxQuantity.setText(String.valueOf(menuItemModifierGroup.getMaxQuantity()));
    ckbEnable.setSelected(menuItemModifierGroup.isEnable().booleanValue());
    tfSortOrder.setText(String.valueOf(menuItemModifierGroup.getSortOrder()));
    ckbJump.setSelected(menuItemModifierGroup.isJumpGroup().booleanValue());
  }
  
  protected boolean updateModel()
  {
    String name = tfName.getText();
    String translatedName = tfTranslatedName.getText();
    int minQuantity = tfMinQuantity.getInteger();
    int maxQuantity = tfMaxQuantity.getInteger();
    boolean selected = ckbEnable.isSelected();
    int sortOrder = tfSortOrder.getInteger();
    boolean jumpSelected = ckbJump.isSelected();
    
    MenuItemModifierSpec modifierGroup = getBean();
    
    if (menuItemModifierGroupsList != null) {
      for (MenuItemModifierSpec pc : menuItemModifierGroupsList) {
        if (modifierGroup != pc)
        {

          if (pc.getName().equalsIgnoreCase(name)) {
            POSMessageDialog.showMessage(this, Messages.getString("MenuItemModifierGroupForm.20"));
            return false;
          }
        }
      }
    }
    
    modifierGroup.setName(name);
    modifierGroup.setTranslatedName(translatedName);
    modifierGroup.setMinQuantity(Integer.valueOf(minQuantity));
    modifierGroup.setMaxQuantity(Integer.valueOf(maxQuantity));
    modifierGroup.setEnable(Boolean.valueOf(selected));
    modifierGroup.setSortOrder(Integer.valueOf(sortOrder));
    modifierGroup.setJumpGroup(Boolean.valueOf(jumpSelected));
    





































    return true;
  }
  
  public String getDisplayText()
  {
    MenuItemModifierSpec modifierGroup = getBean();
    if (modifierGroup.getId() == null) {
      return POSConstants.ADD_NEW_MODIFIER_GROUP_IN_MENU_ITEM_;
    }
    
    return POSConstants.EDIT_MODIFIER_GROUP_IN_MENU_ITEM_;
  }
  
  public MenuItemModifierSpec getBean()
  {
    MenuItemModifierSpec modifierGroup = (MenuItemModifierSpec)super.getBean();
    if (modifierGroup == null) {
      modifierGroup = new MenuItemModifierSpec();
      setBean(modifierGroup);
    }
    
    return modifierGroup;
  }
  
  public class ModifierTableModel extends ListTableModel {
    private String[] columns = { "MODIFIER_NAME", "ACTIVE", "DEFAULT" };
    
    public ModifierTableModel() {}
    
    public int getColumnCount() { return columns.length; }
    

    public String[] getColumnNames()
    {
      return columns;
    }
    
    public Class<?> getColumnClass(int columnIndex)
    {
      switch (columnIndex) {
      case 1: 
      case 2: 
        return Boolean.class;
      }
      
      return String.class;
    }
    

    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      if ((columnIndex == 1) || (columnIndex == 2)) {
        return true;
      }
      return false;
    }
    
    public String getColumnName(int column)
    {
      return columns[column];
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      MenuModifier menuModifier = (MenuModifier)getRowData(rowIndex);
      MenuItemModifierSpec modifierGroup = getBean();
      
      switch (columnIndex)
      {
      case 0: 
        return menuModifier.getName();
      


      case 1: 
        return "";
      
      case 2: 
        return Boolean.valueOf((modifierGroup.getDefaultModifiers() != null) && (modifierGroup.getDefaultModifiers().contains(menuModifier)));
      }
      
      return null;
    }
    
    public void setValueAt(Object aValue, int rowIndex, int columnIndex)
    {
      MenuModifier menuModifier = (MenuModifier)getRowData(rowIndex);
      
      MenuItemModifierSpec modifierGroup = getBean();
      boolean selected = ((Boolean)aValue).booleanValue();
      
      switch (columnIndex)
      {
      case 1: 
        break;
      











      case 2: 
        if (selected)
        {
          if (tfMaxQuantity.getInteger() == 0) {
            POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("MenuItemModifierGroupForm.24"));
          }
          else if ((modifierGroup.getDefaultModifiers() != null) && (modifierGroup.getDefaultModifiers().size() >= tfMaxQuantity.getInteger())) {
            POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("MenuItemModifierGroupForm.25"));
          }
          

        }
        else
        {

          Set<MenuModifier> modifiers = modifierGroup.getDefaultModifiers();
          if (modifiers != null) {
            modifiers.remove(menuModifier);
          }
        }
        break;
      }
      
      super.setValueAt(aValue, rowIndex, columnIndex);
    }
  }
}
