package com.floreantpos.ui.model;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.MenuItemSize;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PizzaCrust;
import com.floreantpos.model.PizzaPrice;
import com.floreantpos.model.dao.MenuItemSizeDAO;
import com.floreantpos.model.dao.OrderTypeDAO;
import com.floreantpos.model.dao.PizzaCrustDAO;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.combobox.ListComboBoxModel;




public class PizzaItemPriceDialog
  extends POSDialog
{
  private JPanel contentPane;
  private JButton btnOK;
  private JButton btnCancel;
  private JComboBox cbCrust;
  private JComboBox cbSize;
  private JTextField tfPrice;
  private PizzaPrice pizzaPrice;
  List<PizzaPrice> pizzaPriceList;
  
  public PizzaItemPriceDialog(Frame owner, PizzaPrice pizzaPrice, List<PizzaPrice> pizzaPriceList)
  {
    super(owner, true);
    this.pizzaPrice = pizzaPrice;
    this.pizzaPriceList = pizzaPriceList;
    init();
    updateView();
  }
  
  private void init() {
    createView();
    
    setModal(true);
    getRootPane().setDefaultButton(btnOK);
    
    btnOK.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        PizzaItemPriceDialog.this.onOK();
      }
      
    });
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        PizzaItemPriceDialog.this.onCancel();
      }
      

    });
    setDefaultCloseOperation(0);
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        PizzaItemPriceDialog.this.onCancel();
      }
      

    });
    contentPane.registerKeyboardAction(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        PizzaItemPriceDialog.this.onCancel();
      }
    }, KeyStroke.getKeyStroke(27, 0), 1);
  }
  
  private void onOK()
  {
    if (!updateModel()) {
      return;
    }
    try {
      setCanceled(false);
      dispose();
    } catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  private void onCancel() {
    setCanceled(true);
    dispose();
  }
  
  private void updateView() {
    if (pizzaPrice == null) {
      return;
    }
    
    cbSize.setSelectedItem(pizzaPrice.getSize());
    cbCrust.setSelectedItem(pizzaPrice.getCrust());
    
    tfPrice.setText(String.valueOf(pizzaPrice.getPrice()));
  }
  
  public boolean updateModel() {
    if (pizzaPrice == null) {
      pizzaPrice = new PizzaPrice();
    }
    
    if (cbSize.getSelectedItem() == null) {
      POSMessageDialog.showError(this, Messages.getString("PizzaItemPriceDialog.0"));
      return false;
    }
    if (cbCrust.getSelectedItem() == null) {
      POSMessageDialog.showError(this, Messages.getString("PizzaItemPriceDialog.1"));
      return false;
    }
    if (tfPrice.getText() == null) {
      POSMessageDialog.showError(this, Messages.getString("PizzaItemPriceDialog.2"));
      return false;
    }
    
    double price = 0.0D;
    try {
      price = Double.parseDouble(tfPrice.getText());
    } catch (Exception x) {
      POSMessageDialog.showError(this, POSConstants.PRICE_IS_NOT_VALID_);
      return false;
    }
    
    if (pizzaPriceList != null) {
      for (PizzaPrice pc : pizzaPriceList) {
        if ((pc.getSize().equals(cbSize.getSelectedItem())) && (pc.getCrust().equals(cbCrust.getSelectedItem())) && 
          (pc != pizzaPrice)) {
          POSMessageDialog.showMessage(this, Messages.getString("PizzaItemPriceDialog.3"));
          return false;
        }
      }
    }
    

    pizzaPrice.setSize((MenuItemSize)cbSize.getSelectedItem());
    pizzaPrice.setCrust((PizzaCrust)cbCrust.getSelectedItem());
    
    pizzaPrice.setPrice(Double.valueOf(price));
    return true;
  }
  
  private void createView() {
    contentPane = new JPanel(new BorderLayout());
    
    List<MenuItemSize> menuItemSizeList = MenuItemSizeDAO.getInstance().findAll();
    List<PizzaCrust> crustList = PizzaCrustDAO.getInstance().findAll();
    List<OrderType> orderTypeList = OrderTypeDAO.getInstance().findAll();
    orderTypeList.add(0, null);
    
    JLabel sizeLabel = new JLabel();
    sizeLabel.setText(Messages.getString("PizzaItemPriceDialog.4"));
    cbSize = new JComboBox(new ListComboBoxModel(menuItemSizeList));
    
    JLabel crustLabel = new JLabel();
    crustLabel.setText(Messages.getString("PizzaItemPriceDialog.5"));
    cbCrust = new JComboBox(new ListComboBoxModel(crustList));
    






    JLabel priceLabel = new JLabel();
    priceLabel.setText(POSConstants.PRICE + ":");
    tfPrice = new JTextField();
    
    JPanel panel = new JPanel(new MigLayout("", "grow", ""));
    
    panel.add(sizeLabel, "right");
    panel.add(cbSize, "grow,wrap");
    panel.add(crustLabel, "right");
    panel.add(cbCrust, "grow,wrap");
    

    panel.add(priceLabel, "right");
    panel.add(tfPrice, "grow");
    
    contentPane.add(panel, "Center");
    
    JPanel buttonPanel = new JPanel(new MigLayout("al center center", "sg", ""));
    btnOK = new JButton(Messages.getString("MenuItemPriceByOrderTypeDialog.0"));
    btnCancel = new JButton(Messages.getString("MenuItemPriceByOrderTypeDialog.21"));
    
    buttonPanel.add(btnOK, "grow");
    buttonPanel.add(btnCancel, "grow");
    contentPane.add(buttonPanel, "South");
    add(contentPane);
  }
  
  public PizzaPrice getPizzaPrice() {
    return pizzaPrice;
  }
}
