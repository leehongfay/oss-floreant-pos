package com.floreantpos.ui.model;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.bo.ui.explorer.ExplorerButtonPanel;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.ModifierGroup;
import com.floreantpos.model.dao.MenuModifierDAO;
import com.floreantpos.model.dao.ModifierGroupDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;


















public class MultipleModifierSelectionView
  extends JPanel
{
  private JComboBox cbGroup;
  private JXTable table;
  private BeanTableModel<MenuModifier> tableModel;
  private JTextField tfName;
  private MenuModifier parentMenuModifier;
  private PosButton btnNext;
  private PosButton btnPrev;
  private ModifierGroup selectedGroup;
  private JLabel lblNumberOfItem = new JLabel();
  private JLabel lblName;
  private JButton searchBttn;
  private JPanel searchPanel;
  private Map<String, MenuModifier> addedMenuModifierMap = new HashMap();
  private JCheckBox chkShowSelected;
  private JCheckBox chkSelectAll;
  private boolean pizzaModifierGroup;
  private boolean singleSelectionEnable;
  private JLabel lblGroupName;
  
  public MultipleModifierSelectionView(List<MenuModifier> addedMenuModifiers, boolean pizzaModifierGroup) {
    this.pizzaModifierGroup = pizzaModifierGroup;
    init();
    tableModel.setCurrentRowIndex(0);
    cbGroup.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        chkSelectAll.setEnabled(true);
        tableModel.setCurrentRowIndex(0);
        setSelectedModifierGroup(cbGroup.getSelectedItem());
      }
    });
    setMenuModifiers(addedMenuModifiers);
  }
  
  private void init() {
    setLayout(new BorderLayout(5, 5));
    tableModel = new BeanTableModel(MenuModifier.class);
    tableModel.addColumn("", "enable");
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "name");
    tableModel.addColumn(POSConstants.PRICE.toUpperCase() + " (" + CurrencyUtil.getCurrencySymbol() + ")", "price");
    tableModel.setPageSize(10);
    table = new JXTable(tableModel);
    table.setSelectionMode(2);
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.setRowHeight(PosUIManager.getSize(40));
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          MultipleModifierSelectionView.this.editSelectedRow();
        }
        else {
          MultipleModifierSelectionView.this.selectItem();
        }
        
      }
    });
    JPanel contentPanel = new JPanel(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(10, 5, 10, 5));
    JScrollPane scroll = new JScrollPane(table);
    scroll.setPreferredSize(PosUIManager.getSize(500, 250));
    contentPanel.add(scroll);
    contentPanel.add(buildSearchForm(), "North");
    
    add(contentPanel);
    resizeColumnWidth(table);
    
    JPanel paginationButtonPanel = new JPanel(new MigLayout("ins 5 0 0 0,fillx", "[left,grow][][][]", ""));
    paginationButtonPanel.add(createButtonPanel(), "left,split 2");
    
    chkShowSelected = new JCheckBox("Show selected");
    chkShowSelected.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (chkShowSelected.isSelected())
        {
          tableModel.setRows(new ArrayList(addedMenuModifierMap.values()));
          
          MultipleModifierSelectionView.this.updateMenuModifierSelection();
          chkShowSelected.setText("Show Selected (" + addedMenuModifierMap.values().size() + ")");
          lblNumberOfItem.setText("");
          btnPrev.setEnabled(false);
          btnNext.setEnabled(false);
          chkSelectAll.setEnabled(false);
          cbGroup.setEnabled(false);
          
          table.repaint();
        }
        else {
          if (cbGroup.getSelectedItem() != null) {
            MultipleModifierSelectionView.this.searchItem();
          }
          cbGroup.setEnabled(true);
        }
      }
    });
    paginationButtonPanel.add(chkShowSelected);
    paginationButtonPanel.add(lblNumberOfItem, "split 3,center");
    
    btnPrev = new PosButton();
    btnPrev.setIcon(IconFactory.getIcon("/ui_icons/", "previous.png"));
    paginationButtonPanel.add(btnPrev, "center");
    
    PosButton btnDot = new PosButton();
    btnDot.setBorder(null);
    btnDot.setOpaque(false);
    btnDot.setContentAreaFilled(false);
    btnDot.setIcon(IconFactory.getIcon("/ui_icons/", "dot.png"));
    
    btnNext = new PosButton();
    btnNext.setIcon(IconFactory.getIcon("/ui_icons/", "next.png"));
    paginationButtonPanel.add(btnNext);
    paginationButtonPanel.add(new JSeparator(), "newline,span,grow");
    
    contentPanel.add(paginationButtonPanel, "South");
    
    ActionListener action = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          Object source = e.getSource();
          if (source == btnPrev) {
            MultipleModifierSelectionView.this.scrollUp();
          }
          else if (source == btnNext) {
            MultipleModifierSelectionView.this.scrollDown();
          }
        } catch (Exception e2) {
          POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e2.getMessage(), e2);
        }
        
      }
    };
    btnPrev.addActionListener(action);
    btnNext.addActionListener(action);
    
    btnNext.setEnabled(false);
    btnPrev.setEnabled(false);
  }
  
  private JPanel buildSearchForm() {
    searchPanel = new JPanel();
    
    searchPanel.setLayout(new MigLayout("inset 0,fillx,hidemode 3", "", "[][]"));
    lblName = new JLabel(POSConstants.NAME);
    tfName = new JTextField();
    searchBttn = new JButton(POSConstants.SEARCH_ITEM_BUTTON_TEXT);
    searchPanel.add(lblName, "align label,split 5");
    searchPanel.add(tfName, "grow");
    
    JButton btnSearch = new JButton("Search");
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tableModel.setCurrentRowIndex(0);
        MultipleModifierSelectionView.this.searchItem();
      }
    });
    searchPanel.add(btnSearch);
    
    chkSelectAll = new JCheckBox("Select All");
    chkSelectAll.setEnabled(false);
    chkSelectAll.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MultipleModifierSelectionView.this.selectGroupItems();
      }
      
    });
    cbGroup = new JComboBox();
    
    List groups = new ArrayList();
    groups.add("<All>");
    
    List<ModifierGroup> menuModifierGroupList = null;
    if (pizzaModifierGroup) {
      menuModifierGroupList = ModifierGroupDAO.getInstance().findPizzaModifierGroups();
    }
    else {
      menuModifierGroupList = ModifierGroupDAO.getInstance().findNormalModifierGroups();
    }
    
    if (menuModifierGroupList != null) {
      groups.addAll(menuModifierGroupList);
    }
    
    ComboBoxModel model = new ComboBoxModel(groups);
    cbGroup.setModel(model);
    cbGroup.setSelectedItem("<All>");
    lblGroupName = new JLabel("Group");
    searchPanel.add(lblGroupName);
    searchPanel.add(cbGroup, "wrap");
    
    searchPanel.add(chkSelectAll, "left");
    

    searchBttn.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        tableModel.setCurrentRowIndex(0);
        MultipleModifierSelectionView.this.searchItem();
      }
      
    });
    tfName.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tableModel.setCurrentRowIndex(0);
        MultipleModifierSelectionView.this.searchItem();
      }
    });
    return searchPanel;
  }
  
  private void searchItem() {
    Object selectedGroupObject = cbGroup.getSelectedItem();
    
    if ((selectedGroupObject instanceof ModifierGroup)) {
      ModifierGroup group = (ModifierGroup)selectedGroupObject;
      List<MenuModifier> modifierList = group.getModifiers();
      if (modifierList == null) {
        modifierList = new ArrayList();
      }
      tableModel.setCurrentRowIndex(0);
      tableModel.setNumRows(modifierList.size());
      tableModel.setRows(modifierList);
      btnPrev.setEnabled(false);
      btnNext.setEnabled(false);
      lblNumberOfItem.setText("");
    }
    else {
      tableModel.setNumRows(MenuModifierDAO.getInstance().getRowCount(tfName.getText(), pizzaModifierGroup));
      MenuModifierDAO.getInstance().loadItems(tfName.getText(), true, pizzaModifierGroup, tableModel);
    }
    doSetEnableCheckAll();
    updateButton();
    updateMenuModifierSelection();
    table.repaint();
    chkShowSelected.setSelected(false);
  }
  
  private void selectGroupItems() {
    Object selectedGroup = cbGroup.getSelectedItem();
    if ((selectedGroup instanceof ModifierGroup)) {
      List<MenuModifier> menuItems = tableModel.getRows();
      if ((menuItems != null) && (menuItems.size() > 0)) {
        for (MenuModifier item : menuItems) {
          if ((parentMenuModifier == null) || (parentMenuModifier.getId() == null) || (!parentMenuModifier.getId().equals(item.getId())))
          {

            item.setEnable(Boolean.valueOf(chkSelectAll.isSelected()));
            if (item.isEnable().booleanValue()) {
              addedMenuModifierMap.put(item.getId(), item);
            }
            else {
              addedMenuModifierMap.remove(item.getId());
            }
          }
        }
      } else {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "No items found!");
        chkSelectAll.setSelected(false);
      }
    }
    
    table.repaint();
  }
  
  private void updateMenuModifierSelection() {
    List<MenuModifier> menuItems = tableModel.getRows();
    if (menuItems == null)
      return;
    for (MenuModifier menuItem : menuItems) {
      MenuModifier item = (MenuModifier)addedMenuModifierMap.get(menuItem.getId());
      menuItem.setEnable(Boolean.valueOf(item != null));
    }
  }
  
  private void updateButton() {
    int startNumber = tableModel.getCurrentRowIndex() + 1;
    int endNumber = tableModel.getNextRowIndex();
    int totalNumber = tableModel.getNumRows();
    if (endNumber > totalNumber) {
      endNumber = totalNumber;
    }
    lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
    btnPrev.setEnabled(tableModel.hasPrevious());
    btnNext.setEnabled(tableModel.hasNext());
    
    if (tableModel.getRowCount() > 0) {
      table.setRowSelectionInterval(0, 0);
    }
    chkShowSelected.setText("Show Selected (" + addedMenuModifierMap.values().size() + ")");
  }
  
  private TransparentPanel createButtonPanel() {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton btnEdit = explorerButton.getEditButton();
    JButton btnAdd = explorerButton.getAddButton();
    btnAdd.setText(POSConstants.ADD);
    btnEdit.setText(POSConstants.EDIT);
    
    btnEdit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MultipleModifierSelectionView.this.editSelectedRow();
      }
    });
    btnAdd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          MenuModifier menuItem = new MenuModifier();
          MenuModifierForm editor = new MenuModifierForm(menuItem);
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          MenuModifier foodItem = (MenuModifier)editor.getBean();
          tableModel.addRow(foodItem);
          tableModel.setNumRows(tableModel.getNumRows() + 1);
          MultipleModifierSelectionView.this.updateButton();
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel(new MigLayout("center,ins 0", "sg,fill", ""));
    int w = PosUIManager.getSize(50);
    int h = PosUIManager.getSize(40);
    

    return panel;
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(70));
    
    columnWidth.add(Integer.valueOf(250));
    columnWidth.add(Integer.valueOf(70));
    
    return columnWidth;
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      MenuModifier menuItem = (MenuModifier)tableModel.getRow(index);
      tableModel.setRow(index, menuItem);
      
      MenuModifierForm editor = new MenuModifierForm(menuItem);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public List<MenuModifier> getSelectedMenuModifierList() {
    return new ArrayList(addedMenuModifierMap.values());
  }
  
  public void setMenuModifiers(List<MenuModifier> menuItems) {
    if (menuItems != null) {
      for (MenuModifier item : menuItems) {
        addedMenuModifierMap.put(item.getId(), item);
      }
    }
    

    searchItem();
  }
  
  public void setParentMenuModifier(MenuModifier selectedMenuModifier) {
    parentMenuModifier = selectedMenuModifier;
  }
  
  private void scrollDown() {
    tableModel.setCurrentRowIndex(tableModel.getNextRowIndex());
    searchItem();
  }
  
  private void scrollUp() {
    tableModel.setCurrentRowIndex(tableModel.getPreviousRowIndex());
    searchItem();
  }
  
  public void setSelectedModifierGroup(Object selectedItem) {
    if ((selectedItem instanceof ModifierGroup)) {
      selectedGroup = ((ModifierGroup)selectedItem);
    }
    else {
      selectedGroup = null;
    }
    searchItem();
  }
  
  private void selectItem() {
    if (table.getSelectedRow() < 0) {
      return;
    }
    if (singleSelectionEnable) {
      return;
    }
    int selectedRow = table.getSelectedRow();
    selectedRow = table.convertRowIndexToModel(selectedRow);
    MenuModifier item = (MenuModifier)tableModel.getRow(selectedRow);
    if ((parentMenuModifier != null) && (parentMenuModifier.getId() != null) && (parentMenuModifier.getId().equals(item.getId()))) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Parent item cannot be selected");
      return;
    }
    item.setEnable(Boolean.valueOf(!item.isEnable().booleanValue()));
    if (item.isEnable().booleanValue()) {
      addedMenuModifierMap.put(item.getId(), item);
    }
    else {
      addedMenuModifierMap.remove(item.getId());
    }
    chkShowSelected.setText("Show Selected (" + addedMenuModifierMap.values().size() + ")");
    table.repaint();
  }
  
  private void doSetEnableCheckAll() {
    List<Boolean> checkVisibilities = new ArrayList();
    List<MenuModifier> menuItems = tableModel.getRows();
    for (MenuModifier menuItem : menuItems) {
      if (addedMenuModifierMap.containsKey(menuItem.getId())) {
        checkVisibilities.add(menuItem.isEnable());
      }
    }
    
    if ((checkVisibilities.size() != 0) && (!checkVisibilities.contains(Boolean.valueOf(false)))) {
      chkSelectAll.setSelected(true);
    }
    else {
      chkSelectAll.setSelected(false);
    }
    chkSelectAll.setEnabled(cbGroup.getSelectedItem() instanceof ModifierGroup);
  }
  
  public BeanTableModel<MenuModifier> getModel() {
    return tableModel;
  }
  
  public int getSelectedRow() {
    int index = table.getSelectedRow();
    if (index < 0)
      return -1;
    return table.convertRowIndexToModel(index);
  }
  
  public void repaintTable() {
    table.repaint();
  }
  
  public void setSingleSelectionEnable(boolean enable) {
    singleSelectionEnable = enable;
    if (enable) {
      chkShowSelected.setVisible(false);
      cbGroup.setVisible(false);
      lblGroupName.setVisible(false);
      chkSelectAll.setVisible(false);
      lblNumberOfItem.setVisible(false);
      btnNext.setVisible(false);
      btnPrev.setVisible(false);
      TableColumnModelExt columnModel = (TableColumnModelExt)table.getColumnModel();
      columnModel.getColumnExt(0).setVisible(false);
      searchItem();
    }
  }
  
  public void setSelectedGroup(ModifierGroup modifierGroup) {
    if ((selectedGroup != null) && (selectedGroup.getId().equals(modifierGroup.getId())))
      return;
    selectedGroup = modifierGroup;
    cbGroup.setSelectedItem(modifierGroup);
    searchItem();
  }
  
  public void setModifiers(List<MenuModifier> modifiers) {
    searchPanel.setVisible(false);
    tableModel.setPageSize(modifiers.size());
    tableModel.setNumRows(modifiers.size());
    tableModel.setRows(modifiers);
    doSetEnableCheckAll();
    updateButton();
    updateMenuModifierSelection();
    table.repaint();
    chkShowSelected.setSelected(false);
  }
}
