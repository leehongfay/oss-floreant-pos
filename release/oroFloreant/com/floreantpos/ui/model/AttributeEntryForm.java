package com.floreantpos.ui.model;

import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.Attribute;
import com.floreantpos.model.AttributeGroup;
import com.floreantpos.model.dao.AttributeDAO;
import com.floreantpos.model.dao.AttributeGroupDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import net.miginfocom.swing.MigLayout;



public class AttributeEntryForm
  extends BeanEditor<Attribute>
{
  private FixedLengthTextField tfName = new FixedLengthTextField(30);
  private IntegerTextField tfFactor = new IntegerTextField(10);
  
  private JComboBox<AttributeGroup> cbGroups = new JComboBox();
  private List<AttributeGroup> groups;
  private JButton btnAddNew;
  private JCheckBox chkDefaultAttribute;
  
  public AttributeEntryForm() {
    this(new Attribute());
  }
  
  public AttributeEntryForm(Attribute unit) {
    createUI();
    groups = AttributeGroupDAO.getInstance().findAll();
    if (groups != null)
      cbGroups.setModel(new ComboBoxModel(groups));
    setBean(unit);
  }
  
  private void createUI() {
    setLayout(new MigLayout("fillx"));
    
    add(new JLabel("Name"));
    add(tfName, "grow, wrap");
    
    add(new JLabel("Sort Order"));
    add(tfFactor, "wrap,grow");
    
    add(new JLabel("Attribute Group"));
    add(cbGroups, "split 2,grow");
    
    btnAddNew = new JButton("New");
    btnAddNew.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        String groupName = JOptionPane.showInputDialog(POSUtil.getFocusedWindow(), "Enter group name");
        if (groupName == null) {
          return;
        }
        if (groupName.equals("")) {
          BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Group name cannot be empty.");
          return;
        }
        if (groupName.length() > 30) {
          BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Group name too long.");
          return;
        }
        for (AttributeGroup group : groups) {
          if (group.getName().equals(groupName)) {
            BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Group name already exists.");
            return;
          }
        }
        AttributeGroup group = new AttributeGroup();
        group.setName(groupName);
        AttributeGroupDAO.getInstance().saveOrUpdate(group);
        groups.add(group);
        cbGroups.setModel(new ComboBoxModel(groups));
        cbGroups.setSelectedItem(group);
      }
    });
    add(btnAddNew, "wrap");
    
    chkDefaultAttribute = new JCheckBox("Default");
    add(chkDefaultAttribute, "skip 1,wrap");
  }
  
  public boolean save()
  {
    try
    {
      if (!updateModel()) {
        return false;
      }
      Attribute selectedUnit = (Attribute)getBean();
      boolean baseUnit = selectedUnit.isDefaultAttribute().booleanValue();
      AttributeGroup unitGroup = selectedUnit.getGroup();
      if (baseUnit) {
        if (unitGroup.getAttributes() != null) {
          for (Attribute unit : unitGroup.getAttributes()) {
            unit.setDefaultAttribute(Boolean.valueOf(false));
          }
        }
        selectedUnit.setDefaultAttribute(Boolean.valueOf(true));
      }
      Object newAttributes = new ArrayList();
      List<Attribute> units = unitGroup.getAttributes();
      if (units != null)
      {
        if (units.contains(selectedUnit)) {
          for (Attribute attribute : units) {
            if (attribute.getId().equals(selectedUnit.getId())) {
              attribute = selectedUnit;
            }
            ((List)newAttributes).add(attribute);
          }
          unitGroup.setAttributes((List)newAttributes);
        }
        else {
          unitGroup.addToattributes(selectedUnit);
        }
      }
      else {
        unitGroup.addToattributes(selectedUnit);
      }
      
      AttributeGroupDAO.getInstance().saveOrUpdate(unitGroup);
      
      return true;
    }
    catch (IllegalModelStateException e) {
      POSMessageDialog.showError(this, e.getMessage());
    }
    
    return false;
  }
  
  public void createNew()
  {
    setBean(new Attribute());
    clearFields();
  }
  
  public void setFieldsEnable(boolean enable)
  {
    tfName.setEnabled(enable);
    tfFactor.setEnabled(enable);
    cbGroups.setEnabled(enable);
    btnAddNew.setEnabled(enable);
    chkDefaultAttribute.setEnabled(enable);
  }
  
  public void clearFields()
  {
    tfName.setText("");
    tfFactor.setText("");
  }
  
  protected void updateView()
  {
    Attribute attribute = (Attribute)getBean();
    if (attribute == null) {
      return;
    }
    
    tfName.setText(attribute.getName());
    tfFactor.setText(attribute.getSortOrder() + "");
    cbGroups.setSelectedItem(attribute.getGroup());
    chkDefaultAttribute.setSelected(attribute.isDefaultAttribute().booleanValue());
  }
  
  protected boolean updateModel() throws IllegalModelStateException
  {
    Attribute attribute = (Attribute)getBean();
    String name = tfName.getText();
    Integer conversionRate = Integer.valueOf(tfFactor.getInteger());
    AttributeGroup group = (AttributeGroup)cbGroups.getSelectedItem();
    
    if (AttributeDAO.getInstance().nameExists(attribute, name)) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), "An attribute with that name already exists");
      return false;
    }
    
    if (group == null) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), "Please select a group");
      return false;
    }
    
    attribute.setName(name);
    attribute.setSortOrder(conversionRate);
    attribute.setGroup(group);
    attribute.setDefaultAttribute(Boolean.valueOf(chkDefaultAttribute.isSelected()));
    
    return true;
  }
  
  public boolean delete()
  {
    try
    {
      Attribute attribute = (Attribute)getBean();
      if (attribute == null) {
        return false;
      }
      
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), "Are you sure to delete selected item ?", "Confirm");
      if (option != 0) {
        return false;
      }
      
      AttributeDAO.getInstance().delete(attribute);
      clearFields();
      return true;
    }
    catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e); }
    return false;
  }
  


  public String getDisplayText()
  {
    return "Add/Edit Attribute";
  }
}
