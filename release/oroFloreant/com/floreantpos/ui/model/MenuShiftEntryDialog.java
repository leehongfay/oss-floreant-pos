package com.floreantpos.ui.model;

import com.floreantpos.POSConstants;
import com.floreantpos.model.DayOfWeek;
import com.floreantpos.model.MenuShift;
import com.floreantpos.model.dao.MenuShiftDAO;
import com.floreantpos.swing.CheckBoxList;
import com.floreantpos.swing.CheckBoxListModel;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.ShiftUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;



















public class MenuShiftEntryDialog
  extends OkCancelOptionDialog
{
  private JComboBox cbStartHour;
  private JComboBox cbStartMin;
  private JComboBox cbStartAmPm;
  private JComboBox cbEndHour;
  private JComboBox cbEndAmPm;
  private JComboBox cbEndMin;
  private FixedLengthTextField tfMenuShiftName;
  private FixedLengthTextField tfMenuShiftDescription = new FixedLengthTextField();
  private JCheckBox chkAnyDay = new JCheckBox("Any Day");
  
  private CheckBoxList<DayOfWeek> chkDays;
  private Vector<Integer> hours;
  private Vector<Integer> mins;
  private MenuShift menuShift;
  private Date shiftStart;
  private Date shiftEnd;
  private JCheckBox chkActive = new JCheckBox("Active");
  private IntegerTextField tfPriority = new IntegerTextField();
  
  public MenuShiftEntryDialog() {
    this(new MenuShift());
  }
  
  public MenuShiftEntryDialog(MenuShift shift) {
    super(POSUtil.getBackOfficeWindow(), shift.getId() == null ? "New menu shift" : "Edit menu shift");
    
    initComponents();
    
    hours = new Vector();
    for (int i = 0; i <= 23; i++) {
      hours.add(Integer.valueOf(i));
    }
    mins = new Vector();
    int min = 0;
    for (int i = 0; i < 5; i++) {
      mins.add(Integer.valueOf(min));
      if (i == 3) {
        min += 14;
      } else
        min += 15;
    }
    cbStartHour.setModel(new DefaultComboBoxModel(hours));
    cbEndHour.setModel(new DefaultComboBoxModel(hours));
    
    cbStartMin.setModel(new DefaultComboBoxModel(mins));
    cbEndMin.setModel(new DefaultComboBoxModel(mins));
    
    setDefaultCloseOperation(0);
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        MenuShiftEntryDialog.this.onCancel();
      }
      
    });
    setMenuShift(shift);
  }
  
  private boolean calculatePriceShifts()
  {
    int hour1 = ((Integer)cbStartHour.getSelectedItem()).intValue();
    int hour2 = ((Integer)cbEndHour.getSelectedItem()).intValue();
    int min1 = ((Integer)cbStartMin.getSelectedItem()).intValue();
    int min2 = ((Integer)cbEndMin.getSelectedItem()).intValue();
    
    shiftStart = ShiftUtil.buildStartTime(hour1, min1, hour2, min2);
    shiftEnd = ShiftUtil.buildEndTime(hour1, min1, hour2, min2);
    
    if (!shiftEnd.after(shiftStart)) {
      POSMessageDialog.showError(this, POSConstants.SHIFT_END_TIME_MUST_BE_GREATER_THAN_SHIFT_START_TIME);
      return false;
    }
    return true;
  }
  
  private void onCancel() {
    setCanceled(true);
    dispose();
  }
  
  public Date getPriceShiftStart() {
    return shiftStart;
  }
  
  public Date getPriceShiftEnd() {
    return shiftEnd;
  }
  
  public void updateView() {
    if (menuShift.getId() == null) {
      chkActive.setSelected(true);
      return;
    }
    
    tfMenuShiftName.setText(menuShift.getName());
    tfMenuShiftDescription.setText(menuShift.getDescription());
    chkAnyDay.setSelected(menuShift.isAnyDay());
    chkActive.setSelected(menuShift.isEnable().booleanValue());
    tfPriority.setText(String.valueOf(menuShift.getPriority()));
    
    if (menuShift.isAnyDay()) {
      chkDays.setEnabled(false);
      chkDays.selectAll();
    }
    else {
      String days = menuShift.getDaysOfWeek();
      List<DayOfWeek> items = new ArrayList();
      CheckBoxListModel model = (CheckBoxListModel)chkDays.getModel();
      if (StringUtils.isNotEmpty(days)) {
        for (int i = 0; i < model.getRowCount(); i++) {
          DayOfWeek item = (DayOfWeek)model.getValueAt(i, 1);
          if (days.contains(String.valueOf(item.getValue())))
            items.add(item);
        }
      }
      chkDays.selectItems(items);
    }
    
    Date startTime = menuShift.getStartTime();
    Date endTime = menuShift.getEndTime();
    
    Calendar c = Calendar.getInstance();
    c.setTime(startTime);
    
    cbStartHour.setSelectedIndex(c.get(11) - 1);
    cbStartMin.setSelectedItem(Integer.valueOf(c.get(12)));
    

    c.setTime(endTime);
    cbEndHour.setSelectedIndex(c.get(11));
    cbEndMin.setSelectedItem(Integer.valueOf(c.get(12)));
  }
  
  public boolean updateModel()
  {
    if (!calculatePriceShifts()) {
      return false;
    }
    if (menuShift == null) {
      menuShift = new MenuShift();
    }
    menuShift.setName(tfMenuShiftName.getText());
    menuShift.setDescription(tfMenuShiftDescription.getText());
    menuShift.setPriority(Integer.valueOf(tfPriority.getInteger()));
    menuShift.setEnable(Boolean.valueOf(chkActive.isSelected()));
    
    menuShift.setStartTime(shiftStart);
    menuShift.setEndTime(shiftEnd);
    long length = Math.abs(shiftStart.getTime() - shiftEnd.getTime());
    menuShift.setShiftLength(Long.valueOf(length));
    
    if (chkAnyDay.isSelected()) {
      menuShift.setAnyDay(true);
    }
    else {
      String dayString = "";
      List<DayOfWeek> days = chkDays.getCheckedValues();
      for (Iterator iterator = days.iterator(); iterator.hasNext();) {
        DayOfWeek dayOfWeek = (DayOfWeek)iterator.next();
        dayString = dayString + dayOfWeek.getValue();
        if (iterator.hasNext())
          dayString = dayString + ",";
      }
      menuShift.setDaysOfWeek(dayString);
    }
    Calendar c = Calendar.getInstance();
    c.setTime(shiftStart);
    
    return true;
  }
  
  public MenuShift getCategoryShift() {
    return menuShift;
  }
  
  public void setMenuShift(MenuShift shift) {
    menuShift = shift;
    updateView();
  }
  
  private void initComponents() {
    JPanel contentPane = getContentPanel();
    contentPane.setLayout(new BorderLayout());
    JPanel centerPanel = new JPanel(new MigLayout("fillx", "[][grow]", ""));
    
    JLabel lblStartTime = new JLabel("From:");
    cbStartHour = new JComboBox();
    JLabel lblMin = new JLabel(POSConstants.MIN);
    
    cbStartMin = new JComboBox();
    cbStartAmPm = new JComboBox();
    
    DefaultComboBoxModel defaultComboBoxModel1 = new DefaultComboBoxModel();
    defaultComboBoxModel1.addElement(POSConstants.AM);
    defaultComboBoxModel1.addElement(POSConstants.PM);
    cbStartAmPm.setModel(defaultComboBoxModel1);
    
    JLabel lblEndTime = new JLabel("To:");
    cbEndHour = new JComboBox();
    JLabel lblEndMin = new JLabel(POSConstants.MIN);
    cbEndMin = new JComboBox();
    cbEndAmPm = new JComboBox();
    DefaultComboBoxModel defaultComboBoxModel2 = new DefaultComboBoxModel();
    defaultComboBoxModel2.addElement(POSConstants.AM);
    defaultComboBoxModel2.addElement(POSConstants.PM);
    cbEndAmPm.setModel(defaultComboBoxModel2);
    JLabel label7 = new JLabel();
    label7.setText(POSConstants.SHIFT_NAME + ":");
    tfMenuShiftName = new FixedLengthTextField();
    chkDays = new CheckBoxList(DayOfWeek.values());
    
    tfMenuShiftName.setLength(30);
    tfMenuShiftDescription.setLength(255);
    
    centerPanel.add(new JLabel("Name"));
    centerPanel.add(tfMenuShiftName, "grow,wrap");
    
    centerPanel.add(new JLabel("Description"));
    centerPanel.add(tfMenuShiftDescription, "grow,wrap");
    
    centerPanel.add(new JLabel("Priority"));
    centerPanel.add(tfPriority, "grow,wrap");
    
    chkAnyDay.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (chkAnyDay.isSelected())
          chkDays.selectAll();
        chkDays.setEnabled(!chkAnyDay.isSelected());
      }
    });
    cbStartHour.setMinimumSize(PosUIManager.getSize(60, 0));
    cbStartMin.setMinimumSize(PosUIManager.getSize(60, 0));
    cbEndHour.setMinimumSize(PosUIManager.getSize(60, 0));
    cbEndMin.setMinimumSize(PosUIManager.getSize(60, 0));
    
    centerPanel.add(lblStartTime);
    
    centerPanel.add(cbStartHour, "split 4,h 25!");
    centerPanel.add(new JLabel("Hour:"));
    centerPanel.add(cbStartMin);
    centerPanel.add(lblMin, "wrap");
    

    centerPanel.add(lblEndTime);
    
    centerPanel.add(cbEndHour, "split 4,h 25!");
    centerPanel.add(new JLabel("Hour:"));
    centerPanel.add(cbEndMin);
    centerPanel.add(lblEndMin);
    
    centerPanel.add(chkActive, "skip 1,gapleft 5,newline");
    centerPanel.add(chkAnyDay, "newline,gapleft 5,skip 1,wrap");
    
    JScrollPane scrollPane = new JScrollPane(chkDays);
    scrollPane.setBorder(new TitledBorder("Days of week"));
    centerPanel.add(scrollPane, "skip 1,grow,wrap");
    
    contentPane.add(centerPanel);
    setSize(PosUIManager.getSize(500, 550));
  }
  
  public void doOk()
  {
    try {
      if (!updateModel()) {
        return;
      }
      MenuShiftDAO.getInstance().saveOrUpdate(menuShift);
      ShiftUtil.reInitialize();
      setCanceled(false);
      dispose();
    } catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_SAVING_SHIFT_STATE, e);
    }
  }
}
