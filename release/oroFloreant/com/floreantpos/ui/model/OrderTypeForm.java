package com.floreantpos.ui.model;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.model.Department;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.TaxGroup;
import com.floreantpos.model.TerminalType;
import com.floreantpos.model.dao.DepartmentDAO;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.model.dao.OrderTypeDAO;
import com.floreantpos.model.dao.TaxGroupDAO;
import com.floreantpos.model.dao.TerminalTypeDAO;
import com.floreantpos.swing.CheckBoxList;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.exception.ConstraintViolationException;































public class OrderTypeForm
  extends BeanEditor
  implements ItemListener
{
  private FixedLengthTextField tfName;
  private JCheckBox chkEnabled;
  private JCheckBox chkShowTableSelection;
  private JCheckBox chkShowGuestSelection;
  private JCheckBox chkShouldPrintToKitchen;
  private JCheckBox chkCloseOnPaid;
  private JCheckBox chkPrepaid;
  private JCheckBox chkDelivery;
  private JCheckBox chkRequiredCustomerData;
  private JCheckBox chkShowInLoginScreen;
  private JCheckBox chkConsolidateItemsInReceipt;
  private JCheckBox chkAllowSeatBasedOrder;
  private JCheckBox chkHideItemWithEmptyInventory;
  private JCheckBox chkHasForHereAndToGo;
  private JCheckBox chkBarTab;
  private JCheckBox chkPreAuthCreditCard;
  private JCheckBox chkRequiredDeliveryData;
  private JCheckBox chkAssignDriver;
  private IntegerTextField tfSortOrder;
  private JLabel lblImagePreview;
  private JButton btnTextColorChooser = new JButton(Messages.getString("OrderTypeForm.15"));
  private JButton btnColorChooser = new JButton(Messages.getString("OrderTypeForm.16"));
  private JCheckBox showImageOnly;
  private Log logger = LogFactory.getLog(OrderTypeForm.class);
  JList<String> list;
  DefaultListModel<String> listModel;
  private JCheckBox chkEnableReorder;
  private JCheckBox chkEnableCourse;
  private JCheckBox chkPickup;
  private JCheckBox chkRetailOrder;
  private CheckBoxList<Department> chkbDeptList = new CheckBoxList();
  private CheckBoxList<TerminalType> chkbTerminalTypeList = new CheckBoxList();
  private CheckBoxList<MenuCategory> chkbCategoryList = new CheckBoxList();
  private ArrayList<Department> selectedDeptList;
  private List<Department> deptList;
  private JTabbedPane tabbedPane;
  private JComboBox cbForHereTG;
  private JComboBox cbToGoTG;
  private JLabel lblForHereTG;
  private JLabel lblToGoTG;
  private JComboBox cbDefaultTG;
  private List<TerminalType> terminalTypeList;
  private List<MenuCategory> categoryList;
  private ArrayList<TerminalType> selectedTerminalTypes;
  private ArrayList<MenuCategory> selectedCategories;
  
  public OrderTypeForm() throws Exception {
    this(new OrderType());
    initHandler();
  }
  
  public OrderTypeForm(OrderType orderType) throws Exception {
    initComponents();
    setBean(orderType);
    initHandler();
  }
  
  public String getDisplayText() {
    OrderType orderType = (OrderType)getBean();
    if (orderType.getId() == null) {
      return POSConstants.ORDER_TYPE;
    }
    return POSConstants.ORDER_TYPE;
  }
  
  private void initHandler() {
    chkDelivery.addItemListener(this);
    chkPickup.addItemListener(this);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    tabbedPane = new JTabbedPane();
    tabbedPane.setPreferredSize(new Dimension(750, 470));
    
    initGeneralTab();
    initGeneralData();
    initStyleTab();
    
    add(tabbedPane);
  }
  
  private void initGeneralTab()
  {
    TransparentPanel generalPanel = new TransparentPanel();
    
    JLabel lblName = new JLabel(POSConstants.NAME + ":");
    tfName = new FixedLengthTextField();
    tfName.setLength(120);
    
    JLabel lblSortOrder = new JLabel("Sort Order:");
    tfSortOrder = new IntegerTextField();
    
    JLabel lblTaxGroup = new JLabel("Tax Group:");
    cbDefaultTG = new JComboBox();
    
    chkEnabled = new JCheckBox(POSConstants.ENABLED);
    chkShowTableSelection = new JCheckBox(Messages.getString("OrderTypeForm.1"));
    chkShowGuestSelection = new JCheckBox(Messages.getString("OrderTypeForm.2"));
    chkShouldPrintToKitchen = new JCheckBox(Messages.getString("OrderTypeForm.3"));
    chkCloseOnPaid = new JCheckBox(Messages.getString("OrderTypeForm.4"));
    chkPrepaid = new JCheckBox(Messages.getString("OrderTypeForm.5"));
    chkDelivery = new JCheckBox("Delivery");
    chkRequiredCustomerData = new JCheckBox(Messages.getString("OrderTypeForm.6"));
    chkShowInLoginScreen = new JCheckBox(Messages.getString("OrderTypeForm.10"));
    chkConsolidateItemsInReceipt = new JCheckBox(Messages.getString("OrderTypeForm.11"));
    chkAllowSeatBasedOrder = new JCheckBox("Allow seat based order");
    chkHideItemWithEmptyInventory = new JCheckBox(Messages.getString("OrderTypeForm.12"));
    
    chkHasForHereAndToGo = new JCheckBox(Messages.getString("OrderTypeForm.13"));
    
    chkBarTab = new JCheckBox(Messages.getString("OrderTypeForm.14"));
    chkPreAuthCreditCard = new JCheckBox(Messages.getString("OrderTypeForm.0"));
    chkRequiredDeliveryData = new JCheckBox(Messages.getString("OrderTypeForm.17"));
    chkAssignDriver = new JCheckBox(Messages.getString("OrderTypeForm.18"));
    chkEnableReorder = new JCheckBox("Enable reorder");
    chkEnableCourse = new JCheckBox(Messages.getString("ENABLE_COURSE"));
    chkPickup = new JCheckBox("Pickup");
    chkRetailOrder = new JCheckBox("Retail");
    chkBarTab.addItemListener(this);
    chkShowTableSelection.addItemListener(this);
    
    lblForHereTG = new JLabel("For Here tax group");
    lblToGoTG = new JLabel("To Go tax group");
    
    cbForHereTG = new JComboBox();
    cbToGoTG = new JComboBox();
    
    generalPanel.setLayout(new MigLayout("hidemode 3", "[][grow][grow]", ""));
    
    generalPanel.add(lblName, "align trailing");
    generalPanel.add(tfName, "growx");
    
    TitledBorder titledBorderOrderTypes = new TitledBorder("Departments");
    JScrollPane deptScrollPanel = new JScrollPane(chkbDeptList);
    deptScrollPanel.setBorder(titledBorderOrderTypes);
    
    generalPanel.add(deptScrollPanel, "span 0 6, w 200!,h 150!, wrap");
    
    generalPanel.add(lblSortOrder, "trailing");
    generalPanel.add(tfSortOrder, "growx, wrap");
    
    generalPanel.add(lblTaxGroup, "trailing");
    generalPanel.add(cbDefaultTG, "growx, wrap");
    
    generalPanel.add(chkEnabled, "skip 1, wrap");
    generalPanel.add(chkShowTableSelection, "skip 1, wrap");
    generalPanel.add(chkShowGuestSelection, "skip 1, wrap");
    generalPanel.add(chkShouldPrintToKitchen, "skip 1");
    
    TitledBorder titledBorderTerminalTypes = new TitledBorder("Terminal types");
    JScrollPane terminalTypesScrollPanel = new JScrollPane(chkbTerminalTypeList);
    terminalTypesScrollPanel.setBorder(titledBorderTerminalTypes);
    generalPanel.add(terminalTypesScrollPanel, "span 0 7, w 200!,h 150!, wrap");
    
    generalPanel.add(chkPrepaid, "skip 1, wrap");
    generalPanel.add(chkCloseOnPaid, "skip 1, wrap");
    
    OrderServiceExtension orderServiceExtension = (OrderServiceExtension)ExtensionManager.getPlugin(OrderServiceExtension.class);
    generalPanel.add(chkRequiredCustomerData, "skip 1, wrap");
    if (orderServiceExtension != null) {
      generalPanel.add(chkRequiredDeliveryData, "skip 1, wrap");
      generalPanel.add(chkAssignDriver, "skip 1, wrap");
    }
    generalPanel.add(chkShowInLoginScreen, "skip 1,wrap");
    
    generalPanel.add(chkConsolidateItemsInReceipt, "skip 1, wrap");
    generalPanel.add(chkEnableCourse, "skip 1, wrap");
    generalPanel.add(chkAllowSeatBasedOrder, "skip 1");
    
    TitledBorder titledBorderCategories = new TitledBorder("Categories");
    JScrollPane categoriesScrollPanel = new JScrollPane(chkbCategoryList);
    categoriesScrollPanel.setBorder(titledBorderCategories);
    generalPanel.add(categoriesScrollPanel, "span 0 6, w 200!,h 150!, wrap");
    
    generalPanel.add(chkHideItemWithEmptyInventory, "skip 1,wrap");
    
    generalPanel.add(chkHasForHereAndToGo, "skip 1, wrap");
    
    generalPanel.add(lblForHereTG, "gapleft 20, skip 1, split 2");
    generalPanel.add(cbForHereTG, "gapleft 20, wrap");
    
    generalPanel.add(lblToGoTG, "gapleft 20, skip 1, split 2");
    generalPanel.add(cbToGoTG, "gapleft 35, skip 1, wrap");
    
    generalPanel.add(chkBarTab, "skip 1, wrap");
    generalPanel.add(chkPreAuthCreditCard, "skip 1, wrap");
    generalPanel.add(chkEnableReorder, "skip 1, wrap");
    generalPanel.add(chkDelivery, "skip 1, wrap");
    generalPanel.add(chkPickup, "skip 1, wrap");
    generalPanel.add(chkRetailOrder, "skip 1, wrap");
    
    chkHasForHereAndToGo.addItemListener(this);
    
    lblForHereTG.setVisible(false);
    lblToGoTG.setVisible(false);
    cbForHereTG.setVisible(false);
    cbToGoTG.setVisible(false);
    
    tabbedPane.add(Messages.getString("OrderTypeForm.32"), new JScrollPane(generalPanel));
  }
  
  private void initGeneralData() {
    deptList = DepartmentDAO.getInstance().findAll();
    if (deptList == null) {
      deptList = new ArrayList();
    }
    chkbDeptList.setModel(deptList);
    
    terminalTypeList = TerminalTypeDAO.getInstance().findAll();
    if (terminalTypeList == null) {
      terminalTypeList = new ArrayList();
    }
    chkbTerminalTypeList.setModel(terminalTypeList);
    
    categoryList = MenuCategoryDAO.getInstance().findAll();
    if (categoryList == null) {
      categoryList = new ArrayList();
    }
    chkbCategoryList.setModel(categoryList);
    
    List<TaxGroup> taxGroups = new ArrayList();
    taxGroups.add(null);
    taxGroups.addAll(TaxGroupDAO.getInstance().findAll());
    
    cbDefaultTG.setModel(new ComboBoxModel(taxGroups));
    cbForHereTG.setModel(new ComboBoxModel(taxGroups));
    cbToGoTG.setModel(new ComboBoxModel(taxGroups));
  }
  
  private void initStyleTab() {
    JPanel btnPanel = new JPanel(new MigLayout("insets 10", "[][]100[][][][]", "[][][center][][][]"));
    
    JLabel lblImage = new JLabel(Messages.getString("MenuItemForm.28"));
    lblImage.setHorizontalAlignment(11);
    btnPanel.add(lblImage, "cell 0 0,right");
    
    lblImagePreview = new JLabel("");
    lblImagePreview.setHorizontalAlignment(0);
    lblImagePreview.setBorder(new EtchedBorder(1, null, null));
    lblImagePreview.setPreferredSize(new Dimension(100, 100));
    btnPanel.add(lblImagePreview, "cell 1 0");
    
    JButton btnSelectImage = new JButton("...");
    btnSelectImage.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        doSelectImageFile();
      }
      
    });
    JButton btnClearImage = new JButton(Messages.getString("MenuItemForm.34"));
    btnClearImage.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        doClearImage();
      }
    });
    btnPanel.add(btnSelectImage, "cell 1 0");
    btnPanel.add(btnClearImage, "cell  1 0");
    
    JLabel lblButtonColor = new JLabel(Messages.getString("OrderTypeForm.22"));
    btnColorChooser.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        Color color = JColorChooser.showDialog(POSUtil.getBackOfficeWindow(), Messages.getString("OrderTypeForm.23"), Color.WHITE);
        btnColorChooser.setBackground(color);
        btnTextColorChooser.setBackground(color);
      }
      
    });
    btnColorChooser.setPreferredSize(new Dimension(228, 40));
    btnPanel.add(lblButtonColor, "cell 0 2");
    btnPanel.add(btnColorChooser, "cell 1 2 ,grow");
    
    JLabel lblTxtColor = new JLabel(Messages.getString("OrderTypeForm.26"));
    btnTextColorChooser.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        Color color = JColorChooser.showDialog(POSUtil.getBackOfficeWindow(), Messages.getString("OrderTypeForm.27"), Color.WHITE);
        btnTextColorChooser.setForeground(color);
      }
      
    });
    btnTextColorChooser.setPreferredSize(new Dimension(228, 40));
    btnPanel.add(lblTxtColor, "cell 0 3");
    btnPanel.add(btnTextColorChooser, "cell 1 3 ,grow");
    showImageOnly = new JCheckBox(Messages.getString("OrderTypeForm.30"));
    btnPanel.add(showImageOnly, "cell 1 4");
    
    tabbedPane.add(Messages.getString("OrderTypeForm.33"), btnPanel);
  }
  
  protected void doClearImage() {
    lblImagePreview.setIcon(null);
    lblImagePreview.putClientProperty("image", null);
  }
  
  protected void updateView() {
    OrderType ordersType = (OrderType)getBean();
    
    if (ordersType == null) {
      tfName.setText("");
      chkEnabled.setSelected(false);
      return;
    }
    
    tfName.setText(ordersType.getName());
    if (ordersType.getId() == null) {
      chkEnabled.setSelected(true);
    }
    else {
      OrderTypeDAO.getInstance().initialize(ordersType);
      chkEnabled.setSelected(ordersType.isEnabled().booleanValue());
      chkShowTableSelection.setSelected(ordersType.isShowTableSelection().booleanValue());
      chkShowGuestSelection.setSelected(ordersType.isShowGuestSelection().booleanValue());
      chkShouldPrintToKitchen.setSelected(ordersType.isShouldPrintToKitchen().booleanValue());
      chkPrepaid.setSelected(ordersType.isPrepaid().booleanValue());
      chkCloseOnPaid.setSelected(ordersType.isCloseOnPaid().booleanValue());
      chkRequiredCustomerData.setSelected(ordersType.isRequiredCustomerData().booleanValue());
      chkShowInLoginScreen.setSelected(ordersType.isShowInLoginScreen().booleanValue());
      chkConsolidateItemsInReceipt.setSelected(ordersType.isConsolidateItemsInReceipt().booleanValue());
      chkAllowSeatBasedOrder.setSelected(ordersType.isAllowSeatBasedOrder().booleanValue());
      chkHideItemWithEmptyInventory.setSelected(ordersType.isHideItemWithEmptyInventory().booleanValue());
      chkHasForHereAndToGo.setSelected(ordersType.isHasForHereAndToGo().booleanValue());
      chkBarTab.setSelected(ordersType.isBarTab().booleanValue());
      chkPreAuthCreditCard.setSelected(ordersType.isPreAuthCreditCard().booleanValue());
      chkEnableReorder.setSelected(ordersType.isEnableReorder().booleanValue());
      chkEnableCourse.setSelected(ordersType.isEnableCourse().booleanValue());
      btnTextColorChooser.setForeground(ordersType.getTextColor());
      btnColorChooser.setBackground(ordersType.getButtonColor());
      showImageOnly.setSelected(ordersType.isShowImageOnly().booleanValue());
      chkDelivery.setSelected(ordersType.isDelivery().booleanValue());
      chkPickup.setSelected(ordersType.isPickup().booleanValue());
      chkRetailOrder.setSelected(ordersType.isRetailOrder().booleanValue());
      chkRequiredDeliveryData.setSelected(ordersType.isRequiredDeliveryData().booleanValue());
      chkAssignDriver.setSelected(ordersType.isAssignDriver().booleanValue());
      
      ImageIcon orderTypeImage = ordersType.getScaledImage(80, 60);
      if (orderTypeImage != null) {
        lblImagePreview.setIcon(orderTypeImage);
        lblImagePreview.putClientProperty("image", convertImageAsFile(orderTypeImage));
      }
      
      selectedDeptList = new ArrayList(ordersType.getDepartments());
      chkbDeptList.selectItems(selectedDeptList);
      
      selectedTerminalTypes = new ArrayList(ordersType.getTerminalTypes());
      chkbTerminalTypeList.selectItems(selectedTerminalTypes);
      
      selectedCategories = new ArrayList(ordersType.getCategories());
      chkbCategoryList.selectItems(selectedCategories);
      
      cbDefaultTG.setSelectedItem(ordersType.getDefaultTaxGroup());
      cbForHereTG.setSelectedItem(ordersType.getForHereTaxGroup());
      cbToGoTG.setSelectedItem(ordersType.getToGoTaxGroup());
    }
  }
  
  private File convertImageAsFile(ImageIcon orderTypeImage) {
    Image rImage = orderTypeImage.getImage();
    BufferedImage bufferedImage = new BufferedImage(rImage.getWidth(null), rImage.getHeight(null), 2);
    
    Graphics2D bGr = bufferedImage.createGraphics();
    bGr.drawImage(rImage, 0, 0, null);
    bGr.dispose();
    File outputfile = new File("saved.png");
    try {
      ImageIO.write(bufferedImage, "png", outputfile);
    } catch (IOException e) {
      PosLog.error(getClass(), e);
    }
    return outputfile;
  }
  
  protected boolean updateModel()
  {
    try {
      OrderType ordersType = (OrderType)getBean();
      if (ordersType == null) {
        return false;
      }
      
      String categoryName = tfName.getText();
      if (POSUtil.isBlankOrNull(categoryName)) {
        MessageDialog.showError(Messages.getString("MenuCategoryForm.26"));
        return false;
      }
      
      ordersType.setName(categoryName);
      ordersType.setEnabled(Boolean.valueOf(chkEnabled.isSelected()));
      ordersType.setShowTableSelection(Boolean.valueOf(chkShowTableSelection.isSelected()));
      ordersType.setShowGuestSelection(Boolean.valueOf(chkShowGuestSelection.isSelected()));
      ordersType.setShouldPrintToKitchen(Boolean.valueOf(chkShouldPrintToKitchen.isSelected()));
      ordersType.setPrepaid(Boolean.valueOf(chkPrepaid.isSelected()));
      ordersType.setCloseOnPaid(Boolean.valueOf(chkCloseOnPaid.isSelected()));
      ordersType.setDelivery(Boolean.valueOf(chkDelivery.isSelected()));
      ordersType.setPickup(Boolean.valueOf(chkPickup.isSelected()));
      ordersType.setRetailOrder(Boolean.valueOf(chkRetailOrder.isSelected()));
      ordersType.setRequiredCustomerData(Boolean.valueOf(chkRequiredCustomerData.isSelected()));
      ordersType.setRequiredDeliveryData(Boolean.valueOf(chkRequiredDeliveryData.isSelected()));
      ordersType.setAssignDriver(Boolean.valueOf(chkAssignDriver.isSelected()));
      ordersType.setShowInLoginScreen(Boolean.valueOf(chkShowInLoginScreen.isSelected()));
      ordersType.setConsolidateItemsInReceipt(Boolean.valueOf(chkConsolidateItemsInReceipt.isSelected()));
      ordersType.setAllowSeatBasedOrder(Boolean.valueOf(chkAllowSeatBasedOrder.isSelected()));
      ordersType.setHideItemWithEmptyInventory(Boolean.valueOf(chkHideItemWithEmptyInventory.isSelected()));
      ordersType.setHasForHereAndToGo(Boolean.valueOf(chkHasForHereAndToGo.isSelected()));
      ordersType.setPreAuthCreditCard(Boolean.valueOf(chkPreAuthCreditCard.isSelected()));
      ordersType.setSortOrder(Integer.valueOf(tfSortOrder.getInteger()));
      ordersType.setTextColorCode(Integer.valueOf(btnTextColorChooser.getForeground().getRGB()));
      ordersType.setButtonColorCode(Integer.valueOf(btnColorChooser.getBackground().getRGB()));
      ordersType.setShowImageOnly(Boolean.valueOf(showImageOnly.isSelected()));
      ordersType.setEnableReorder(Boolean.valueOf(chkEnableReorder.isSelected()));
      ordersType.setEnableCourse(Boolean.valueOf(chkEnableCourse.isSelected()));
      File imageFile = (File)lblImagePreview.getClientProperty("image");
      if (imageFile != null) {
        ordersType.setImageData(POSUtil.convertImageToBlob(imageFile));
      }
      else {
        ordersType.setImageData(null);
      }
      ordersType.setDefaultTaxGroup((TaxGroup)cbDefaultTG.getSelectedItem());
      
      if (chkHasForHereAndToGo.isSelected()) {
        ordersType.setForHereTaxGroup((TaxGroup)cbForHereTG.getSelectedItem());
        ordersType.setToGoTaxGroup((TaxGroup)cbToGoTG.getSelectedItem());
      }
      else {
        ordersType.setForHereTaxGroup(null);
        ordersType.setToGoTaxGroup(null);
      }
      ordersType.setBarTab(Boolean.valueOf(chkBarTab.isSelected()));
    }
    catch (Exception e) {
      logger.error(e);
      return false;
    }
    return true;
  }
  
  public boolean save()
  {
    OrderType ordersType = null;
    try {
      if (!updateModel()) {
        return false;
      }
      ordersType = (OrderType)getBean();
      






















      ordersType.setDepartments(POSUtil.copySelectedValues(ordersType.getDepartments(), chkbDeptList.getCheckedValuesAsSet()));
      ordersType.setTerminalTypes(POSUtil.copySelectedValues(ordersType.getTerminalTypes(), chkbTerminalTypeList.getCheckedValuesAsSet()));
      ordersType.setCategories(POSUtil.copySelectedValues(ordersType.getCategories(), chkbCategoryList.getCheckedValuesAsSet()));
      
      OrderTypeDAO.getInstance().saveOrUpdate(ordersType);
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("TerminalConfigurationView.40"));
      return true;
    } catch (ConstraintViolationException e) {
      MessageDialog.showError("Order type with same name already exists.");
      ordersType.setId(null);
      return false;
    } catch (Exception x) {
      MessageDialog.showError(x); }
    return false;
  }
  

  public void itemStateChanged(ItemEvent e)
  {
    JCheckBox chkBox = (JCheckBox)e.getItem();
    if (chkBox == chkDelivery) {
      if (chkDelivery.isSelected()) {
        chkRequiredCustomerData.setSelected(true);
        chkRequiredDeliveryData.setSelected(true);
        chkAssignDriver.setSelected(true);
      }
    }
    else if (chkBox == chkPickup) {
      if (chkPickup.isSelected()) {
        chkRequiredCustomerData.setSelected(true);
      }
    }
    else if (chkBox == chkHasForHereAndToGo) {
      if (chkHasForHereAndToGo.isSelected()) {
        lblForHereTG.setVisible(true);
        lblToGoTG.setVisible(true);
        cbForHereTG.setVisible(true);
        cbToGoTG.setVisible(true);
      }
      else {
        lblForHereTG.setVisible(false);
        lblToGoTG.setVisible(false);
        cbForHereTG.setVisible(false);
        cbToGoTG.setVisible(false);
      }
    }
    else if (chkBox == chkBarTab) {
      if ((chkBarTab.isSelected()) && (!chkShowTableSelection.isSelected())) {
        chkShowTableSelection.setSelected(chkBarTab.isSelected());
      }
    }
    else if ((chkBox == chkShowTableSelection) && 
      (chkBarTab.isSelected()) && (!chkShowTableSelection.isSelected())) {
      chkBarTab.setSelected(false);
    }
  }
  


  protected void doSelectImageFile()
  {
    FileNameExtensionFilter filter = new FileNameExtensionFilter("*.jpg , *.png , *.gif , *.jpeg", new String[] { "jpg", "png", "gif", "jpeg" });
    JFileChooser fileChooser = new JFileChooser();
    fileChooser.setFileFilter(filter);
    fileChooser.setMultiSelectionEnabled(false);
    fileChooser.setFileFilter(filter);
    fileChooser.setAcceptAllFileFilterUsed(false);
    fileChooser.setFileSelectionMode(0);
    int option = fileChooser.showOpenDialog(POSUtil.getBackOfficeWindow());
    
    if (option == 0) {
      File imageFile = fileChooser.getSelectedFile();
      long imageSizeChk = imageFile.length();
      
      if (imageSizeChk >= 10485760L) {
        POSMessageDialog.showError(Messages.getString("OrderTypeForm.40"));
        doSelectImageFile();
      }
      try {
        Image resizedImg = ImageIO.read(imageFile);
        Image scaledImage = POSUtil.getScaledImage(resizedImg, 100, 100);
        lblImagePreview.putClientProperty("image", imageFile);
        lblImagePreview.setIcon(new ImageIcon(scaledImage));
      }
      catch (Exception e) {
        logger.error(e);
      }
    }
  }
}
