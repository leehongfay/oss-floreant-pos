package com.floreantpos.ui.model;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.bo.ui.explorer.ExplorerButtonPanel;
import com.floreantpos.model.Customer;
import com.floreantpos.model.CustomerGroup;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.forms.CustomerForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;


















public class MultipleCustomerSelectionView
  extends JPanel
{
  private JComboBox cbGroup;
  private JXTable table;
  private BeanTableModel<Customer> tableModel;
  private JTextField tfName;
  private Customer parentCustomer;
  private PosButton btnNext;
  private PosButton btnPrev;
  private CustomerGroup selectedGroup;
  private JLabel lblNumberOfItem = new JLabel();
  private JLabel lblName;
  private JButton searchBttn;
  private JPanel searchPanel;
  private Map<String, Customer> addedCustomerMap = new HashMap();
  private JCheckBox chkShowSelected;
  private JCheckBox chkSelectAll;
  private boolean singleSelectionEnable;
  private JLabel lblGroupName;
  
  public MultipleCustomerSelectionView(List<Customer> customerList) {
    init();
    tableModel.setCurrentRowIndex(0);
    cbGroup.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        chkSelectAll.setEnabled(true);
        tableModel.setCurrentRowIndex(0);
        setSelectedCustomerGroup(cbGroup.getSelectedItem());
      }
      
    });
    setCustomer(customerList);
  }
  
  private void init() {
    setLayout(new BorderLayout(5, 5));
    tableModel = new BeanTableModel(Customer.class);
    tableModel.addColumn("", "selected");
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "name");
    tableModel.setPageSize(10);
    table = new JXTable(tableModel);
    table.setSelectionMode(2);
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.setRowHeight(PosUIManager.getSize(40));
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          MultipleCustomerSelectionView.this.editSelectedRow();
        }
        else {
          MultipleCustomerSelectionView.this.selectItem();
        }
        
      }
    });
    JPanel contentPanel = new JPanel(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(10, 5, 10, 5));
    JScrollPane scroll = new JScrollPane(table);
    scroll.setPreferredSize(PosUIManager.getSize(500, 250));
    contentPanel.add(scroll);
    contentPanel.add(buildSearchForm(), "North");
    
    add(contentPanel);
    resizeColumnWidth(table);
    
    JPanel paginationButtonPanel = new JPanel(new MigLayout("ins 5 0 0 0,fillx", "[left,grow][][][]", ""));
    paginationButtonPanel.add(createButtonPanel(), "left,split 2");
    
    chkShowSelected = new JCheckBox("Show selected");
    chkShowSelected.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (chkShowSelected.isSelected())
        {
          tableModel.setRows(new ArrayList(addedCustomerMap.values()));
          
          MultipleCustomerSelectionView.this.updateCustomerSelection();
          chkShowSelected.setText("Show Selected (" + addedCustomerMap.values().size() + ")");
          lblNumberOfItem.setText("");
          btnPrev.setEnabled(false);
          btnNext.setEnabled(false);
          cbGroup.setEnabled(false);
          table.repaint();
        }
        else {
          if (cbGroup.getSelectedItem() != null) {
            MultipleCustomerSelectionView.this.searchItem();
          }
          cbGroup.setEnabled(true);
        }
        
      }
    });
    paginationButtonPanel.add(chkShowSelected);
    paginationButtonPanel.add(lblNumberOfItem, "split 3,center");
    
    btnPrev = new PosButton();
    btnPrev.setIcon(IconFactory.getIcon("/ui_icons/", "previous.png"));
    paginationButtonPanel.add(btnPrev, "center");
    
    PosButton btnDot = new PosButton();
    btnDot.setBorder(null);
    btnDot.setOpaque(false);
    btnDot.setContentAreaFilled(false);
    btnDot.setIcon(IconFactory.getIcon("/ui_icons/", "dot.png"));
    
    btnNext = new PosButton();
    btnNext.setIcon(IconFactory.getIcon("/ui_icons/", "next.png"));
    paginationButtonPanel.add(btnNext);
    paginationButtonPanel.add(new JSeparator(), "newline,span,grow");
    
    contentPanel.add(paginationButtonPanel, "South");
    
    ActionListener action = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          Object source = e.getSource();
          if (source == btnPrev) {
            MultipleCustomerSelectionView.this.scrollUp();
          }
          else if (source == btnNext) {
            MultipleCustomerSelectionView.this.scrollDown();
          }
        } catch (Exception e2) {
          POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e2.getMessage(), e2);
        }
        
      }
    };
    btnPrev.addActionListener(action);
    btnNext.addActionListener(action);
    
    btnNext.setEnabled(false);
    btnPrev.setEnabled(false);
  }
  
  private JPanel buildSearchForm() {
    searchPanel = new JPanel();
    
    searchPanel.setLayout(new MigLayout("inset 0,fillx,hidemode 3", "", "[][]"));
    lblName = new JLabel(POSConstants.NAME);
    tfName = new JTextField();
    searchBttn = new JButton(POSConstants.SEARCH_ITEM_BUTTON_TEXT);
    searchPanel.add(lblName, "align label,split 5");
    searchPanel.add(tfName, "grow");
    
    JButton btnSearch = new JButton("Search");
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tableModel.setCurrentRowIndex(0);
        MultipleCustomerSelectionView.this.searchItem();
      }
    });
    searchPanel.add(btnSearch, "wrap");
    
    chkSelectAll = new JCheckBox("Select All");
    chkSelectAll.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MultipleCustomerSelectionView.this.selectGroupItems();
      }
      
    });
    cbGroup = new JComboBox();
    
    List groups = new ArrayList();
    groups.add("<All>");
    
    ComboBoxModel model = new ComboBoxModel(groups);
    cbGroup.setModel(model);
    cbGroup.setSelectedItem("<All>");
    lblGroupName = new JLabel("Group");
    


    searchPanel.add(chkSelectAll, "left");
    

    searchBttn.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        tableModel.setCurrentRowIndex(0);
        MultipleCustomerSelectionView.this.searchItem();
      }
      
    });
    tfName.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tableModel.setCurrentRowIndex(0);
        MultipleCustomerSelectionView.this.searchItem();
      }
    });
    return searchPanel;
  }
  
  private void searchItem() {
    Object selectedGroupObject = cbGroup.getSelectedItem();
    
    if ((selectedGroupObject instanceof CustomerGroup)) {
      CustomerGroup customerGroup = (CustomerGroup)selectedGroupObject;
      List<Customer> customerList = customerGroup.getCustomers();
      if (customerList == null) {
        customerList = new ArrayList();
      }
      tableModel.setCurrentRowIndex(0);
      tableModel.setNumRows(customerList.size());
      tableModel.setRows(customerList);
      btnPrev.setEnabled(false);
      btnNext.setEnabled(false);
      lblNumberOfItem.setText("");
    }
    else {
      tableModel.setNumRows(CustomerDAO.getInstance().getRowCount(tfName.getText()));
      CustomerDAO.getInstance().loadCustomers(tfName.getText(), tableModel);
    }
    doSetEnableCheckAll();
    updateButton();
    updateCustomerSelection();
    table.repaint();
    chkShowSelected.setSelected(false);
  }
  
  private void selectGroupItems() {
    Object selectedGroup = cbGroup.getSelectedItem();
    if ((selectedGroup instanceof CustomerGroup)) {
      List<Customer> customers = tableModel.getRows();
      if ((customers != null) && (customers.size() > 0)) {
        for (Customer customer : customers) {
          if ((parentCustomer == null) || (parentCustomer.getId() == null) || (!parentCustomer.getId().equals(customer.getId())))
          {

            customer.setSelected(Boolean.valueOf(chkSelectAll.isSelected()));
            
            if (customer.isSelected().booleanValue()) {
              addedCustomerMap.put(customer.getId(), customer);
            }
            else {
              addedCustomerMap.remove(customer.getId());
            }
          }
        }
      } else {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "No items found!");
        chkSelectAll.setSelected(false);
      }
    }
    
    table.repaint();
  }
  
  private void updateCustomerSelection() {
    List<Customer> customers = tableModel.getRows();
    if (customers == null) {
      return;
    }
    for (Customer customer : customers) {
      Customer existingCustomer = (Customer)addedCustomerMap.get(customer.getId());
      customer.setSelected(Boolean.valueOf(existingCustomer != null));
    }
  }
  
  private void updateButton()
  {
    int startNumber = tableModel.getCurrentRowIndex() + 1;
    int endNumber = tableModel.getNextRowIndex();
    int totalNumber = tableModel.getNumRows();
    if (endNumber > totalNumber) {
      endNumber = totalNumber;
    }
    lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
    btnPrev.setEnabled(tableModel.hasPrevious());
    btnNext.setEnabled(tableModel.hasNext());
    
    if (tableModel.getRowCount() > 0) {
      table.setRowSelectionInterval(0, 0);
    }
    chkShowSelected.setText("Show Selected (" + addedCustomerMap.values().size() + ")");
  }
  
  private TransparentPanel createButtonPanel() {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton btnEdit = explorerButton.getEditButton();
    JButton btnAdd = explorerButton.getAddButton();
    btnAdd.setText(POSConstants.ADD);
    btnEdit.setText(POSConstants.EDIT);
    
    btnEdit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MultipleCustomerSelectionView.this.editSelectedRow();
      }
    });
    btnAdd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          CustomerForm editor = new CustomerForm();
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          Customer foodItem = (Customer)editor.getBean();
          tableModel.addRow(foodItem);
          tableModel.setNumRows(tableModel.getNumRows() + 1);
          MultipleCustomerSelectionView.this.updateButton();
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel(new MigLayout("center,ins 0", "sg,fill", ""));
    return panel;
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(70));
    
    columnWidth.add(Integer.valueOf(250));
    columnWidth.add(Integer.valueOf(70));
    
    return columnWidth;
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      Customer customer = (Customer)tableModel.getRow(index);
      tableModel.setRow(index, customer);
      
      CustomerForm editor = new CustomerForm();
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public List<Customer> getSelectedCustomerList() {
    return new ArrayList(addedCustomerMap.values());
  }
  
  public void setCustomer(List<Customer> customers) {
    if (customers != null) {
      for (Customer item : customers) {
        addedCustomerMap.put(item.getId(), item);
      }
    }
    

    searchItem();
  }
  
  public void setParentCustomer(Customer selectedCustomer) {
    parentCustomer = selectedCustomer;
  }
  
  private void scrollDown() {
    tableModel.setCurrentRowIndex(tableModel.getNextRowIndex());
    searchItem();
  }
  
  private void scrollUp() {
    tableModel.setCurrentRowIndex(tableModel.getPreviousRowIndex());
    searchItem();
  }
  
  public void setSelectedCustomerGroup(Object selectedItem) {
    if ((selectedItem instanceof CustomerGroup)) {
      selectedGroup = ((CustomerGroup)selectedItem);
    }
    else {
      selectedGroup = null;
    }
    searchItem();
  }
  
  private void selectItem() {
    if (table.getSelectedRow() < 0) {
      return;
    }
    if (singleSelectionEnable) {
      return;
    }
    int selectedRow = table.getSelectedRow();
    selectedRow = table.convertRowIndexToModel(selectedRow);
    Customer customer = (Customer)tableModel.getRow(selectedRow);
    if ((parentCustomer != null) && (parentCustomer.getId() != null) && (parentCustomer.getId().equals(customer.getId()))) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Parent item cannot be selected");
      return;
    }
    customer.setSelected(Boolean.valueOf(!customer.isSelected().booleanValue()));
    if (customer.isSelected().booleanValue()) {
      addedCustomerMap.put(customer.getId(), customer);
    }
    else {
      addedCustomerMap.remove(customer.getId());
    }
    chkShowSelected.setText("Show Selected (" + addedCustomerMap.values().size() + ")");
    table.repaint();
  }
  








  private void doSetEnableCheckAll() {}
  







  public BeanTableModel<Customer> getModel()
  {
    return tableModel;
  }
  
  public int getSelectedRow() {
    int index = table.getSelectedRow();
    if (index < 0)
      return -1;
    return table.convertRowIndexToModel(index);
  }
  
  public void repaintTable() {
    table.repaint();
  }
  
  public void setSingleSelectionEnable(boolean enable) {
    singleSelectionEnable = enable;
    if (enable)
    {
      chkShowSelected.setVisible(false);
      cbGroup.setVisible(false);
      lblGroupName.setVisible(false);
      chkSelectAll.setVisible(false);
      lblNumberOfItem.setVisible(false);
      btnNext.setVisible(false);
      btnPrev.setVisible(false);
      TableColumnModelExt columnModel = (TableColumnModelExt)table.getColumnModel();
      columnModel.getColumnExt(0).setVisible(false);
      searchItem();
    }
  }
  
  public void setSelectedGroup(CustomerGroup customerGroup) {
    if ((selectedGroup != null) && (selectedGroup.getId().equals(customerGroup.getId())))
      return;
    selectedGroup = customerGroup;
    cbGroup.setSelectedItem(customerGroup);
    searchItem();
  }
}
