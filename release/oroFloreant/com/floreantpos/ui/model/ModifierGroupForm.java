package com.floreantpos.ui.model;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.modifierdesigner.ModifierSelectionDialog;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.ModifierGroup;
import com.floreantpos.model.dao.ModifierDAO;
import com.floreantpos.model.dao.ModifierGroupDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.hibernate.Hibernate;
import org.hibernate.Session;






























public class ModifierGroupForm
  extends BeanEditor
{
  private JLabel jLabel1;
  private FixedLengthTextField tfName;
  private FixedLengthTextField tfTranslatedName;
  private JList listModifiers;
  private ComboBoxModel modifierListModel;
  private Boolean pizzaModifierGroup;
  
  public ModifierGroupForm()
    throws Exception
  {
    this(new ModifierGroup());
  }
  
  public ModifierGroupForm(ModifierGroup group) throws Exception {
    this(group, Boolean.valueOf(false));
  }
  
  public ModifierGroupForm(ModifierGroup group, Boolean pizzaModifierGroup) throws Exception {
    initComponents();
    setBean(group);
    this.pizzaModifierGroup = pizzaModifierGroup;
  }
  
  private void initComponents()
  {
    setLayout(new BorderLayout(5, 5));
    JPanel formPanel = new JPanel();
    formPanel.setLayout(new MigLayout("", "[45px][369px,grow]", "[19px][]"));
    
    jLabel1 = new JLabel();
    tfName = new FixedLengthTextField();
    tfName.setLength(60);
    
    jLabel1.setText(POSConstants.NAME);
    
    formPanel.add(jLabel1, "cell 0 0,alignx left,aligny center");
    formPanel.add(tfName, "cell 1 0,growx,aligny top");
    
    JLabel lblTranslatedName = new JLabel(POSConstants.TRANSLATED_NAME);
    formPanel.add(lblTranslatedName, "cell 0 1,alignx trailing");
    
    tfTranslatedName = new FixedLengthTextField();
    tfTranslatedName.setLength(60);
    formPanel.add(tfTranslatedName, "cell 1 1,growx");
    
    add(formPanel, "North");
    
    JPanel modifierPanel = new JPanel(new MigLayout("fill"));
    JPanel listPanel = new JPanel(new MigLayout("fill"));
    JPanel buttonPanel = new JPanel(new MigLayout());
    
    Border loweredetched = BorderFactory.createEtchedBorder(1);
    TitledBorder title = BorderFactory.createTitledBorder(loweredetched, "Modifier List");
    title.setTitleJustification(1);
    
    JButton btnAddModifier = new JButton("Add Modifier");
    btnAddModifier.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ModifierGroupForm.this.doAddModifiers();
      }
      
    });
    JButton btnRemoveModifier = new JButton("Remove");
    btnRemoveModifier.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuModifier modifier = (MenuModifier)listModifiers.getSelectedValue();
        if (modifier == null)
          return;
        modifierListModel.removeElement(modifier);
      }
      
    });
    listModifiers = new JList();
    
    JScrollPane scrollPane = new JScrollPane(listModifiers);
    listPanel.setBorder(title);
    listPanel.add(scrollPane, "grow");
    
    buttonPanel.add(btnAddModifier);
    buttonPanel.add(btnRemoveModifier);
    
    modifierPanel.add(listPanel, "grow");
    modifierPanel.add(buttonPanel, "South");
    
    add(modifierPanel);
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      ModifierGroup group = (ModifierGroup)getBean();
      
      ModifierGroupDAO dao = new ModifierGroupDAO();
      dao.saveOrUpdate(group);
    } catch (Exception e) {
      MessageDialog.showError(e);
      return false;
    }
    return true;
  }
  
  protected void updateView()
  {
    ModifierGroup group = (ModifierGroup)getBean();
    
    if ((group.getId() != null) && (!Hibernate.isInitialized(group.getModifiers()))) {
      ModifierDAO dao = new ModifierDAO();
      Session session = dao.getSession();
      group = (ModifierGroup)session.merge(group);
      Hibernate.initialize(group.getModifiers());
      session.close();
    }
    
    tfName.setText(group.getName());
    tfTranslatedName.setText(group.getTranslatedName());
    modifierListModel = new ComboBoxModel();
    if (group.getModifiers() != null) {
      modifierListModel.setDataList(group.getModifiers());
    }
    else {
      modifierListModel.setDataList(new ArrayList());
    }
    listModifiers.setModel(modifierListModel);
  }
  
  protected boolean updateModel()
  {
    ModifierGroup group = (ModifierGroup)getBean();
    
    String name = tfName.getText();
    if (POSUtil.isBlankOrNull(name)) {
      MessageDialog.showError(POSConstants.NAME_REQUIRED);
      return false;
    }
    
    group.setName(name);
    group.setTranslatedName(tfTranslatedName.getText());
    group.setModifiers(modifierListModel.getDataList());
    group.setPizzaModifierGroup(pizzaModifierGroup);
    
    return true;
  }
  
  public String getDisplayText() {
    ModifierGroup modifierGroup = (ModifierGroup)getBean();
    if (modifierGroup.getId() == null) {
      return POSConstants.NEW_MODIFIER_GROUP;
    }
    return POSConstants.EDIT_MODIFIER_GROUP;
  }
  
  private void doAddModifiers() {
    ModifierSelectionDialog modifierSelectionDialog = new ModifierSelectionDialog(modifierListModel.getDataList(), pizzaModifierGroup.booleanValue());
    modifierSelectionDialog.setSize(PosUIManager.getSize(600, 500));
    modifierSelectionDialog.open();
    
    if (modifierSelectionDialog.isCanceled()) {
      return;
    }
    List<MenuModifier> selectedMenuModifierList = modifierSelectionDialog.getSelectedMenuModifierList();
    modifierListModel.removeAllElements();
    for (MenuModifier menuModifier : selectedMenuModifierList) {
      modifierListModel.addElement(menuModifier);
    }
  }
}
