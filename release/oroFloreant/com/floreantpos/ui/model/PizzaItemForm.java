package com.floreantpos.ui.model;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.modifierdesigner.ModifierPageDesigner;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.InventoryPlugin;
import com.floreantpos.main.Application;
import com.floreantpos.model.ImageResource;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.MenuItemShift;
import com.floreantpos.model.MenuItemSize;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PizzaCrust;
import com.floreantpos.model.PizzaPrice;
import com.floreantpos.model.PrinterGroup;
import com.floreantpos.model.Shift;
import com.floreantpos.model.TaxGroup;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.model.dao.MenuItemSizeDAO;
import com.floreantpos.model.dao.PizzaCrustDAO;
import com.floreantpos.model.dao.PrinterGroupDAO;
import com.floreantpos.model.dao.TaxGroupDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.CheckBoxList;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthDocument;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IUpdatebleView;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.dialog.ImageGalleryDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.PosGuiUtil;
import com.floreantpos.util.ShiftUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.hibernate.Hibernate;
import org.hibernate.Session;































public class PizzaItemForm
  extends BeanEditor<MenuItem>
  implements ActionListener, ChangeListener
{
  private JTabbedPane tabbedPane;
  private JTable priceTable;
  private FixedLengthTextField tfName;
  private FixedLengthTextField tfTranslatedName;
  private FixedLengthTextField tfBarcode;
  private DoubleTextField tfStockCount;
  private JCheckBox chkVisible;
  private JCheckBox cbDisableStockCount;
  private JButton btnDeleteModifierGroup;
  private JButton btnEditModifierGroup;
  private JButton btnNewModifierGroup;
  private IntegerTextField tfDefaultSellPortion;
  private JComboBox<PrinterGroup> cbPrinterGroup;
  private JComboBox cbTaxGroup;
  private CheckBoxList orderList;
  private JTextArea tfDescription;
  private JTable modifierSpecTable;
  private List<MenuItemModifierSpec> menuItemModifierGroups;
  private MenuItemMGListModel menuItemMGListModel;
  private JLabel lblImagePreview;
  private JButton btnClearImage;
  private JCheckBox cbShowTextWithImage;
  private JLabel lblKitchenPrinter;
  private JButton btnButtonColor;
  private JButton btnTextColor;
  private IntegerTextField tfSortOrder;
  private BeanTableModel<PizzaPrice> priceTableModel;
  private final MenuItem menuItem;
  private ModifierPageDesigner modifierDesigner;
  private JPanel tabModifier;
  private JScrollPane jScrollPane1;
  private ImageResource imageResource;
  private JComboBox<MenuGroup> cbGroup;
  
  public PizzaItemForm()
    throws Exception
  {
    this(new MenuItem());
  }
  
  public PizzaItemForm(MenuItem menuItem) throws Exception {
    this.menuItem = menuItem;
    menuItem.setPizzaType(Boolean.valueOf(true));
    initComponents();
    initData();
  }
  
  private void initData() {
    MenuGroupDAO menuGroupDao = new MenuGroupDAO();
    List<MenuGroup> menuGroups = menuGroupDao.findAll();
    cbGroup.setModel(new ComboBoxModel(menuGroups));
    
    TaxGroupDAO taxDAO = new TaxGroupDAO();
    List<TaxGroup> taxGroups = taxDAO.findAll();
    cbTaxGroup.setModel(new ComboBoxModel(taxGroups));
    
    menuItemModifierGroups = menuItem.getMenuItemModiferSpecs();
    priceTableModel = new BeanTableModel(PizzaPrice.class)
    {
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 2) {
          return true;
        }
        return false;
      }
      
      public Class<?> getColumnClass(int columnIndex)
      {
        if (columnIndex == 2) {
          return Double.class;
        }
        return super.getColumnClass(columnIndex);
      }
      
      public void setValueAt(Object value, int rowIndex, int columnIndex)
      {
        PizzaPrice price = (PizzaPrice)priceTableModel.getRow(rowIndex);
        if (columnIndex == 2) {
          String priceString = (String)value;
          if (priceString.isEmpty())
            return;
          double priceValue = Double.parseDouble(priceString);
          price.setPrice(Double.valueOf(priceValue));
        }
        else {
          super.setValueAt(value, rowIndex, columnIndex);
        }
      } };
    priceTableModel.addColumn("SIZE", "size");
    priceTableModel.addColumn("CRUST", "crust");
    priceTableModel.addColumn("PRICE", "price");
    
    List<PizzaPrice> pizzaPriceList = menuItem.getPizzaPriceList();
    
    if ((pizzaPriceList == null) || (pizzaPriceList.isEmpty())) {
      priceTableModel.addRows(generatedPossiblePizzaItemSizeAndPriceList());
    } else {
      priceTableModel.addRows(pizzaPriceList);
    }
    priceTable.setModel(priceTableModel);
    setBean(menuItem);
    
    priceTable.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          PizzaItemForm.this.editEvent();
        }
        
      }
    });
    DoubleTextField tfPrice = new DoubleTextField();
    tfPrice.setAllowNegativeValue(true);
    tfPrice.setHorizontalAlignment(4);
    DefaultCellEditor editorPrice = new DefaultCellEditor(tfPrice);
    editorPrice.setClickCountToStart(1);
    priceTable.setDefaultEditor(priceTable.getColumnClass(2), editorPrice);
  }
  
  protected void doSelectImageFile()
  {
    ImageGalleryDialog dialog = ImageGalleryDialog.getInstance();
    dialog.setSelectBtnVisible(true);
    dialog.setTitle("Image Gallery");
    dialog.setSize(PosUIManager.getSize(650, 600));
    dialog.setResizable(false);
    dialog.open();
    if (dialog.isCanceled()) {
      return;
    }
    imageResource = dialog.getImageResource();
    if (imageResource != null) {
      lblImagePreview.setIcon(imageResource.getAsIcon());
    }
  }
  
  protected void doClearImage() {
    lblImagePreview.setIcon(null);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    JLabel lblButtonColor = new JLabel(Messages.getString("MenuItemForm.19"));
    tabbedPane = new JTabbedPane();
    
    JPanel tabGeneral = new JPanel();
    
    JLabel lblName = new JLabel();
    lblName.setHorizontalAlignment(11);
    
    tfName = new FixedLengthTextField(20);
    tfDescription = new JTextArea(new FixedLengthDocument(120));
    
    JLabel lblTaxGroup = new JLabel();
    lblTaxGroup.setHorizontalAlignment(11);
    
    cbTaxGroup = new JComboBox();
    JButton btnNewTax = new JButton();
    
    tabModifier = new JPanel();
    btnNewModifierGroup = new JButton();
    btnDeleteModifierGroup = new JButton();
    btnEditModifierGroup = new JButton();
    jScrollPane1 = new JScrollPane();
    modifierSpecTable = new JTable();
    JPanel tabPrice = new JPanel();
    
    JPanel tabButtonStyle = new JPanel();
    JButton btnDeleteShift = new JButton();
    JButton btnAddShift = new JButton();
    
    JButton btnNewPrice = new JButton();
    JButton btnUpdatePrice = new JButton();
    JButton btnDeletePrice = new JButton();
    JButton btnDeleteAll = new JButton();
    JButton btnDefaultValue = new JButton();
    JButton btnAutoGenerate = new JButton();
    
    JScrollPane priceTabScrollPane = new JScrollPane();
    priceTable = new JTable() {
      public void changeSelection(int row, int column, boolean toggle, boolean extend) {
        super.changeSelection(row, column, toggle, extend);
        priceTable.editCellAt(row, column);
        priceTable.transferFocus();
        DefaultCellEditor editor = (DefaultCellEditor)priceTable.getCellEditor(row, column);
        if (column == 2) {
          DoubleTextField textField = (DoubleTextField)editor.getComponent();
          textField.requestFocus();
          textField.selectAll();
        }
      }
    };
    priceTable.setRowHeight(PosUIManager.getSize(22));
    priceTable.setCellSelectionEnabled(true);
    priceTable.setSelectionMode(0);
    priceTable.setSurrendersFocusOnKeystroke(true);
    
    cbPrinterGroup = new JComboBox(new DefaultComboBoxModel(PrinterGroupDAO.getInstance().findAll().toArray(new PrinterGroup[0])));
    cbPrinterGroup.setPreferredSize(new Dimension(226, 0));
    
    tfDefaultSellPortion = new IntegerTextField(10);
    tfTranslatedName = new FixedLengthTextField(20);
    tfTranslatedName.setLength(120);
    lblKitchenPrinter = new JLabel(Messages.getString("MenuItemForm.27"));
    lblName.setText(Messages.getString("LABEL_NAME"));
    tfName.setLength(120);
    JLabel lblTranslatedName = new JLabel(Messages.getString("MenuItemForm.lblTranslatedName.text"));
    tfSortOrder = new IntegerTextField(20);
    tfSortOrder.setText("");
    cbTaxGroup.setPreferredSize(new Dimension(198, 0));
    btnButtonColor = new JButton();
    btnButtonColor.setPreferredSize(new Dimension(228, 40));
    JLabel lblTextColor = new JLabel(Messages.getString("MenuItemForm.lblTextColor.text"));
    btnTextColor = new JButton(Messages.getString("MenuItemForm.SAMPLE_TEXT"));
    cbShowTextWithImage = new JCheckBox(Messages.getString("MenuItemForm.40"));
    cbShowTextWithImage.setActionCommand(Messages.getString("MenuItemForm.41"));
    lblTaxGroup.setText("Tax Group");
    btnNewTax.setText("...");
    
    btnNewTax.setText("...");
    btnNewTax.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent evt) {
        PizzaItemForm.this.btnNewTaxdoCreateNewTax(evt);
      }
      
    });
    tabbedPane.addTab(POSConstants.GENERAL, tabGeneral);
    tabbedPane.setPreferredSize(new Dimension(750, 470));
    
    btnNewModifierGroup.setText(POSConstants.ADD);
    btnNewModifierGroup.setActionCommand("AddModifierGroup");
    
    btnDeleteModifierGroup.setText(POSConstants.DELETE);
    btnDeleteModifierGroup.setActionCommand("DeleteModifierGroup");
    
    btnEditModifierGroup.setText(POSConstants.EDIT);
    btnEditModifierGroup.setActionCommand("EditModifierGroup");
    
    menuItemMGListModel = new MenuItemMGListModel();
    modifierSpecTable.setModel(menuItemMGListModel);
    modifierSpecTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        if (e.getValueIsAdjusting()) {
          return;
        }
        int index = modifierSpecTable.getSelectedRow();
        if (index < 0) {
          return;
        }
        MenuItemModifierSpec menuItemModifierSpec = menuItemMGListModel.get(index);
        modifierDesigner.setMenuItemModifierSpec(menuItemModifierSpec);
      }
    });
    modifierSpecTable.setAutoResizeMode(0);
    modifierSpecTable.setModel(menuItemMGListModel);
    modifierSpecTable.setRowHeight(PosUIManager.getSize(30));
    
    btnNewModifierGroup.addActionListener(this);
    btnEditModifierGroup.addActionListener(this);
    btnDeleteModifierGroup.addActionListener(this);
    
    btnAddShift.addActionListener(this);
    btnDeleteShift.addActionListener(this);
    
    tabGeneral.setLayout(new MigLayout("insets 20,center,hidemode 3", "[][]20px[][]", "[][][][][][][][][][][][][]"));
    tabGeneral.setBorder(BorderFactory.createTitledBorder("-"));
    
    tabGeneral.add(lblName, "cell 0 1 ,right");
    tabGeneral.add(tfName, "cell 1 1,grow");
    
    tabGeneral.add(lblTranslatedName, "cell 0 2,right");
    tabGeneral.add(tfTranslatedName, "cell 1 2,grow");
    
    JLabel lgroup = new JLabel();
    lgroup.setHorizontalAlignment(11);
    lgroup.setText(Messages.getString("LABEL_GROUP"));
    tabGeneral.add(lgroup, "cell 0 3,alignx right");
    
    JLabel lblBarcode = new JLabel(Messages.getString("MenuItemForm.lblBarcode.text"));
    tabGeneral.add(lblBarcode, "cell 0 4,alignx right");
    tfBarcode = new FixedLengthTextField(20);
    tabGeneral.add(tfBarcode, "cell 1 4,grow");
    JLabel lblStockCount = new JLabel(Messages.getString("MenuItemForm.17"));
    
    tabGeneral.add(lblStockCount, "cell 0 5,alignx right");
    tfStockCount = new DoubleTextField(1);
    tabGeneral.add(tfStockCount, "cell 1 5,growx");
    chkVisible = new JCheckBox();
    
    tabGeneral.add(new JLabel("Default sell portion (%)"), "cell 0 6");
    tabGeneral.add(tfDefaultSellPortion, "cell 1 6,grow");
    
    chkVisible.setText(POSConstants.VISIBLE);
    chkVisible.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    chkVisible.setMargin(new Insets(0, 0, 0, 0));
    
    tabGeneral.add(chkVisible, "cell 1 7");
    tabGeneral.add(lblKitchenPrinter, "cell 2 1,right");
    tabGeneral.add(cbPrinterGroup, "cell 3 1,grow");
    
    tabGeneral.add(lblTaxGroup, "cell 2 2,right");
    tabGeneral.add(cbTaxGroup, "cell 3 2");
    tabGeneral.add(btnNewTax, "cell 3 2,grow");
    cbGroup = new JComboBox();
    cbGroup.setPreferredSize(new Dimension(198, 0));
    
    tabGeneral.add(cbGroup, "flowx,cell 1 3");
    JButton btnNewGroup = new JButton();
    
    btnNewGroup.setText("...");
    btnNewGroup.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        PizzaItemForm.this.doCreateNewGroup(evt);
      }
    });
    tabGeneral.add(btnNewGroup, "cell 1 3");
    
    tabGeneral.add(new JLabel(Messages.getString("MenuItemForm.25")), "cell 2 3,right");
    orderList = new CheckBoxList();
    
    List<OrderType> orderTypes = Application.getInstance().getOrderTypes();
    orderList.setModel(orderTypes);
    
    JScrollPane orderCheckBoxList = new JScrollPane(orderList);
    orderCheckBoxList.setPreferredSize(new Dimension(228, 100));
    tabGeneral.add(orderCheckBoxList, "cell 3 3 3 4, grow");
    cbDisableStockCount = new JCheckBox(Messages.getString("MenuItemForm.18"));
    tabGeneral.add(cbDisableStockCount, "cell 1 8");
    
    tabGeneral.add(new JLabel(Messages.getString("MenuItemForm.29")), "cell 2 7,alignx right");
    JScrollPane scrlDescription = new JScrollPane(tfDescription, 20, 30);
    scrlDescription.setPreferredSize(new Dimension(228, 70));
    tfDescription.setLineWrap(true);
    tabGeneral.add(scrlDescription, "cell 3 7 3 3, grow");
    
    add(tabbedPane);
    addRecepieExtension();
    
    jScrollPane1.setViewportView(modifierSpecTable);
    resizeColumnWidth(modifierSpecTable);
    
    tabModifier.setLayout(new BorderLayout(10, 10));
    JPanel newpanel1 = new JPanel(new BorderLayout());
    newpanel1.setPreferredSize(PosUIManager.getSize(350, 0));
    
    TitledBorder titledBorder = BorderFactory.createTitledBorder("Pizza Groups");
    titledBorder.setTitleJustification(2);
    Border titel = BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(0, 10, 10, 0), titledBorder);
    newpanel1.setBorder(titel);
    
    newpanel1.add(jScrollPane1);
    
    JPanel btmpanel = new JPanel();
    
    newpanel1.add(btmpanel, "South");
    
    btmpanel.add(btnNewModifierGroup);
    btmpanel.add(btnEditModifierGroup);
    btmpanel.add(btnDeleteModifierGroup);
    
    tabModifier.add(newpanel1, "West");
    modifierDesigner = new ModifierPageDesigner(menuItem);
    
    tabModifier.add(modifierDesigner);
    
    tabbedPane.addTab(POSConstants.MODIFIER_GROUPS, tabModifier);
    

    modifierSpecTable.getColumnModel().getColumn(0).setPreferredWidth(150);
    modifierSpecTable.getColumnModel().getColumn(1).setPreferredWidth(100);
    modifierSpecTable.getColumnModel().getColumn(2).setPreferredWidth(100);
    modifierSpecTable.setAutoResizeMode(3);
    









    btnDeleteShift.setText(POSConstants.DELETE_SHIFT);
    btnAddShift.setText(POSConstants.ADD_SHIFT);
    
    btnNewPrice.setText(Messages.getString("MenuItemForm.9"));
    btnNewPrice.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        PizzaItemForm.this.addNewPrice();
      }
    });
    btnUpdatePrice.setText(Messages.getString("MenuItemForm.13"));
    btnUpdatePrice.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PizzaItemForm.this.updatePrice();
      }
    });
    btnDeletePrice.setText(Messages.getString("MenuItemForm.14"));
    btnDeletePrice.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PizzaItemForm.this.deletePrice();
      }
      
    });
    btnAutoGenerate.setText("Auto Generate");
    btnAutoGenerate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        PizzaItemForm.this.autoGeneratePizzaItemSizeAndPrice();
      }
      
    });
    btnDeleteAll.setText(Messages.getString("MenuItemForm.15"));
    btnDeleteAll.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PizzaItemForm.this.deleteAll();
      }
      
    });
    btnDefaultValue.setText(Messages.getString("MenuItemForm.7"));
    priceTabScrollPane.setViewportView(priceTable);
    
    tabPrice.setLayout(new BorderLayout());
    tabPrice.add(priceTabScrollPane, "Center");
    
    JPanel buttonPanel = new JPanel();
    
    buttonPanel.add(btnNewPrice);
    buttonPanel.add(btnUpdatePrice);
    buttonPanel.add(btnDeletePrice);
    buttonPanel.add(btnAutoGenerate);
    
    tabPrice.add(buttonPanel, "South");
    tabGeneral.add(tabPrice, "cell 0 10,grow,span");
    tabbedPane.addChangeListener(this);
    
    tabButtonStyle.setLayout(new MigLayout("insets 10", "[][]100[][][][]", "[][][center][][][]"));
    
    JLabel lblImage = new JLabel(Messages.getString("MenuItemForm.28"));
    lblImage.setHorizontalAlignment(11);
    tabButtonStyle.add(lblImage, "cell 0 0,right");
    
    lblImagePreview = new JLabel("");
    lblImagePreview.setHorizontalAlignment(0);
    lblImagePreview.setBorder(new EtchedBorder(1, null, null));
    lblImagePreview.setPreferredSize(new Dimension(100, 100));
    tabButtonStyle.add(lblImagePreview, "cell 1 0");
    
    JButton btnSelectImage = new JButton("...");
    btnClearImage = new JButton(Messages.getString("MenuItemForm.34"));
    tabButtonStyle.add(btnClearImage, "cell  1 0");
    tabButtonStyle.add(btnSelectImage, "cell 1 0");
    
    tabButtonStyle.add(lblButtonColor, "cell 0 2,right");
    tabButtonStyle.add(btnButtonColor, "cell 1 2,grow");
    tabButtonStyle.add(lblTextColor, "cell 0 3,right");
    tabButtonStyle.add(btnTextColor, "cell 1 3");
    tabButtonStyle.add(cbShowTextWithImage, "cell 1 4");
    
    btnTextColor.setPreferredSize(new Dimension(228, 50));
    
    btnSelectImage.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        doSelectImageFile();
      }
      
    });
    btnClearImage.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        doClearImage();
      }
      
    });
    btnButtonColor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        Color color = JColorChooser.showDialog(POSUtil.getBackOfficeWindow(), Messages.getString("MenuItemForm.42"), btnButtonColor.getBackground());
        btnButtonColor.setBackground(color);
        btnTextColor.setBackground(color);
      }
      
    });
    btnTextColor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        Color color = JColorChooser.showDialog(POSUtil.getBackOfficeWindow(), Messages.getString("MenuItemForm.43"), btnTextColor.getForeground());
        btnTextColor.setForeground(color);
      }
      
    });
    tabbedPane.addTab(Messages.getString("MenuItemForm.26"), tabButtonStyle);
  }
  
  private void autoGeneratePizzaItemSizeAndPrice() {
    List<PizzaPrice> pizzaPriceList = generatedPossiblePizzaItemSizeAndPriceList();
    filterDuplicateItemSizesAndPrices(pizzaPriceList);
    priceTableModel.addRows(pizzaPriceList);
    priceTable.repaint();
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(50));
    
    return columnWidth;
  }
  
  public void addRecepieExtension() {
    InventoryPlugin plugin = (InventoryPlugin)ExtensionManager.getPlugin(InventoryPlugin.class);
    if (plugin == null) {
      return;
    }
    
    plugin.addRecepieView(tabbedPane);
  }
  
  private void btnNewTaxdoCreateNewTax(ActionEvent evt) {
    TaxForm editor = new TaxForm();
    BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
    dialog.open();
    if (dialog.isCanceled()) {
      return;
    }
    ComboBoxModel model = (ComboBoxModel)cbTaxGroup.getModel();
    model.setDataList(TaxGroupDAO.getInstance().findAll());
  }
  
  private void doCreateNewGroup(ActionEvent evt) {
    MenuGroupForm editor = new MenuGroupForm();
    BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
    dialog.open();
    
    if (!dialog.isCanceled()) {
      MenuGroup menuGroup = (MenuGroup)editor.getBean();
      ComboBoxModel model = (ComboBoxModel)cbGroup.getModel();
      model.addElement(menuGroup);
      model.setSelectedItem(menuGroup);
    }
  }
  
  private void addMenuItemModifierGroup()
  {
    try {
      MenuItemModifierSpecForm form = new MenuItemModifierSpecForm(new MenuItemModifierSpec(), menuItem, false);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), form);
      dialog.getButtonPanel().add(form.getAutoBuildButton(), 0);
      dialog.openWithScale(500, 500);
      if (dialog.isCanceled()) {
        return;
      }
      menuItemMGListModel.add(form.getBean());
      modifierSpecTable.getSelectionModel().setSelectionInterval(menuItemMGListModel.getRowCount() - 1, menuItemMGListModel.getRowCount() - 1);
      if (form.isAutoBuildSelected()) {
        modifierDesigner.doGenenateMenuItemModifierPageItems();
      }
    } catch (Exception x) {
      MessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void editMenuItemModifierGroup() {
    try {
      int index = modifierSpecTable.getSelectedRow();
      if (index < 0) {
        return;
      }
      MenuItemModifierSpec menuItemModifierGroup = menuItemMGListModel.get(index);
      MenuItemModifierSpecForm form = new MenuItemModifierSpecForm(menuItemModifierGroup, menuItem, true);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), form);
      dialog.openWithScale(500, 500);
      if (!dialog.isCanceled()) {
        menuItemMGListModel.fireTableRowsUpdated(index, index);
      }
    } catch (Exception x) {
      MessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void deleteMenuItemModifierGroup() {
    try {
      int index = modifierSpecTable.getSelectedRow();
      if (index < 0) {
        return;
      }
      if (ConfirmDeleteDialog.showMessage(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.CONFIRM) == 0)
      {
        menuItemMGListModel.remove(index);
        if (menuItemMGListModel.getRowCount() > 0) {
          modifierSpecTable.getSelectionModel().setSelectionInterval(menuItemMGListModel.getRowCount() - 1, menuItemMGListModel.getRowCount() - 1);
        }
        else {
          modifierDesigner.reset();
        }
      }
    } catch (Exception x) {
      MessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      MenuItem menuItem = (MenuItem)getBean();
      MenuItemDAO menuItemDAO = new MenuItemDAO();
      menuItemDAO.saveOrUpdate(menuItem);
    } catch (Exception e) {
      MessageDialog.showError(POSConstants.ERROR_MESSAGE, e);
      return false;
    }
    return true;
  }
  
  protected void updateView()
  {
    MenuItem menuItem = (MenuItem)getBean();
    
    if ((menuItem.getId() != null) && (!Hibernate.isInitialized(menuItem.getMenuItemModiferSpecs())))
    {
      MenuItemDAO dao = new MenuItemDAO();
      Session session = dao.getSession();
      menuItem = (MenuItem)session.merge(menuItem);
      Hibernate.initialize(menuItem.getMenuItemModiferSpecs());
      session.close();
    }
    


    tfName.setText(menuItem.getName());
    tfDescription.setText(menuItem.getDescription());
    tfTranslatedName.setText(menuItem.getTranslatedName());
    tfBarcode.setText(menuItem.getBarcode());
    tfStockCount.setText(String.valueOf(menuItem.getAvailableUnit()));
    chkVisible.setSelected(menuItem.isVisible().booleanValue());
    cbShowTextWithImage.setSelected(menuItem.isShowImageOnly().booleanValue());
    cbDisableStockCount.setSelected(menuItem.isDisableWhenStockAmountIsZero().booleanValue());
    ImageIcon menuItemImage = menuItem.getImage();
    if (menuItemImage != null) {
      lblImagePreview.setIcon(menuItemImage);
    }
    if (menuItem.getId() == null) {
      tfDefaultSellPortion.setText(String.valueOf(100));
    } else {
      tfDefaultSellPortion.setText(String.valueOf(menuItem.getDefaultSellPortion()));
    }
    PosGuiUtil.selectComboItemById(cbGroup, menuItem.getMenuGroupId());
    cbTaxGroup.setSelectedItem(menuItem.getTaxGroup());
    cbPrinterGroup.setSelectedItem(menuItem.getPrinterGroup());
    
    if (menuItem.getSortOrder() != null) {
      tfSortOrder.setText(menuItem.getSortOrder().toString());
    }
    
    Color buttonColor = menuItem.getButtonColor();
    if (buttonColor != null) {
      btnButtonColor.setBackground(buttonColor);
      btnTextColor.setBackground(buttonColor);
    }
    
    if (menuItem.getTextColor() != null) {
      btnTextColor.setForeground(menuItem.getTextColor());
    }
  }
  

  protected boolean updateModel()
  {
    String itemName = tfName.getText();
    if (POSUtil.isBlankOrNull(itemName)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.NAME_REQUIRED);
      return false;
    }
    
    MenuItem menuItem = (MenuItem)getBean();
    menuItem.setName(itemName);
    menuItem.setDescription(tfDescription.getText());
    menuItem.setBarcode(tfBarcode.getText());
    menuItem.setMenuGroup((MenuGroup)cbGroup.getSelectedItem());
    menuItem.setTaxGroup((TaxGroup)cbTaxGroup.getSelectedItem());
    menuItem.setAvailableUnit(Double.valueOf(Double.parseDouble(tfStockCount.getText())));
    menuItem.setVisible(Boolean.valueOf(chkVisible.isSelected()));
    menuItem.setShowImageOnly(Boolean.valueOf(cbShowTextWithImage.isSelected()));
    menuItem.setDisableWhenStockAmountIsZero(Boolean.valueOf(cbDisableStockCount.isSelected()));
    menuItem.setDefaultSellPortion(Integer.valueOf(tfDefaultSellPortion.getInteger()));
    
    menuItem.setTranslatedName(tfTranslatedName.getText());
    menuItem.setSortOrder(Integer.valueOf(tfSortOrder.getInteger()));
    
    menuItem.setButtonColorCode(Integer.valueOf(btnButtonColor.getBackground().getRGB()));
    menuItem.setTextColorCode(Integer.valueOf(btnTextColor.getForeground().getRGB()));
    
    if (orderList.getCheckedValues().isEmpty()) {
      menuItem.setOrderTypeList(null);
    }
    else {
      menuItem.setOrderTypeList(orderList.getCheckedValues());
    }
    menuItem.setMenuItemModiferSpecs(menuItemModifierGroups);
    menuItem.setHasModifiers(Boolean.valueOf((menuItemModifierGroups != null) && (menuItemModifierGroups.size() > 0)));
    menuItem.setHasMandatoryModifiers(Boolean.valueOf(false));
    Iterator localIterator; if (menuItem.hasModifiers()) {
      for (localIterator = menuItemModifierGroups.iterator(); localIterator.hasNext();) { menuItemModifierGroup = (MenuItemModifierSpec)localIterator.next();
        if ((menuItemModifierGroup.getMinQuantity().intValue() > 0) || (menuItemModifierGroup.isAutoShow().booleanValue())) {
          menuItem.setHasMandatoryModifiers(Boolean.valueOf(true));
          break;
        }
      }
    }
    MenuItemModifierSpec menuItemModifierGroup;
    Object pizzaPriceList = priceTableModel.getRows();
    if (menuItem.getPizzaPriceList() != null) {
      menuItem.getPizzaPriceList().clear();
    }
    for (PizzaPrice pizzaPrice : (List)pizzaPriceList) {
      menuItem.addTopizzaPriceList(pizzaPrice);
    }
    
    int tabCount = tabbedPane.getTabCount();
    for (int i = 0; i < tabCount; i++) {
      Component componentAt = tabbedPane.getComponent(i);
      if ((componentAt instanceof IUpdatebleView)) {
        IUpdatebleView view = (IUpdatebleView)componentAt;
        if (!view.updateModel(menuItem)) {
          return false;
        }
      }
    }
    
    menuItem.setPrinterGroup((PrinterGroup)cbPrinterGroup.getSelectedItem());
    menuItem.setPizzaType(Boolean.valueOf(true));
    
    if (imageResource != null) {
      menuItem.setImageId(imageResource.getId());
    }
    
    return true;
  }
  
  public String getDisplayText()
  {
    MenuItem foodItem = (MenuItem)getBean();
    if (foodItem.getId() == null) {
      return "New pizza item";
    }
    return "Edit pizza item";
  }
  
  class MenuItemMGListModel extends AbstractTableModel {
    String[] cn = { POSConstants.GROUP_NAME, POSConstants.MIN_QUANTITY, POSConstants.MAX_QUANTITY };
    
    MenuItemMGListModel() {}
    
    public MenuItemModifierSpec get(int index)
    {
      return (MenuItemModifierSpec)menuItemModifierGroups.get(index);
    }
    
    public void add(MenuItemModifierSpec group) {
      if (menuItemModifierGroups == null) {
        menuItemModifierGroups = new ArrayList();
      }
      menuItemModifierGroups.add(group);
      fireTableDataChanged();
    }
    
    public void remove(int index) {
      if (menuItemModifierGroups == null) {
        return;
      }
      menuItemModifierGroups.remove(index);
      fireTableDataChanged();
    }
    
    public void remove(MenuItemModifierSpec group) {
      if (menuItemModifierGroups == null) {
        return;
      }
      menuItemModifierGroups.remove(group);
      fireTableDataChanged();
    }
    
    public int getRowCount()
    {
      if (menuItemModifierGroups == null) {
        return 0;
      }
      return menuItemModifierGroups.size();
    }
    

    public int getColumnCount()
    {
      return cn.length;
    }
    
    public String getColumnName(int column)
    {
      return cn[column];
    }
    
    public List<MenuItemModifierSpec> getItems() {
      return menuItemModifierGroups;
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      MenuItemModifierSpec menuItemModifierGroup = (MenuItemModifierSpec)menuItemModifierGroups.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return menuItemModifierGroup.getName();
      
      case 1: 
        return Integer.valueOf(menuItemModifierGroup.getMinQuantity().intValue());
      
      case 2: 
        return Integer.valueOf(menuItemModifierGroup.getMaxQuantity().intValue());
      }
      return null;
    }
  }
  
  class ShiftTableModel extends AbstractTableModel {
    List<MenuItemShift> shifts;
    String[] cn = { POSConstants.START_TIME, POSConstants.END_TIME, POSConstants.PRICE };
    Calendar calendar = Calendar.getInstance();
    
    ShiftTableModel() {
      if (shifts == null) {
        this.shifts = new ArrayList();
      }
      else {
        this.shifts = new ArrayList(shifts);
      }
    }
    
    public MenuItemShift get(int index) {
      return (MenuItemShift)shifts.get(index);
    }
    
    public void add(MenuItemShift group) {
      if (shifts == null) {
        shifts = new ArrayList();
      }
      shifts.add(group);
      fireTableDataChanged();
    }
    
    public void remove(int index) {
      if (shifts == null) {
        return;
      }
      shifts.remove(index);
      fireTableDataChanged();
    }
    
    public void remove(MenuItemShift group) {
      if (shifts == null) {
        return;
      }
      shifts.remove(group);
      fireTableDataChanged();
    }
    
    public int getRowCount()
    {
      if (shifts == null) {
        return 0;
      }
      return shifts.size();
    }
    

    public int getColumnCount()
    {
      return cn.length;
    }
    
    public String getColumnName(int column)
    {
      return cn[column];
    }
    
    public List<MenuItemShift> getShifts() {
      return shifts;
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      MenuItemShift shift = (MenuItemShift)shifts.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return ShiftUtil.buildShiftTimeRepresentation(shift.getShift().getStartTime());
      
      case 1: 
        return ShiftUtil.buildShiftTimeRepresentation(shift.getShift().getEndTime());
      
      case 2: 
        return String.valueOf(shift.getShiftPrice());
      }
      return null;
    }
  }
  

  private void addNewPrice()
  {
    List<PizzaPrice> pizzaPriceList = priceTableModel.getRows();
    PizzaItemPriceDialog dialog = new PizzaItemPriceDialog(getParentFrame(), null, pizzaPriceList);
    dialog.setTitle("Add New Price");
    dialog.setSize(PosUIManager.getSize(350, 220));
    dialog.open();
    if (dialog.isCanceled()) {
      return;
    }
    PizzaPrice pizzaPrice = dialog.getPizzaPrice();
    priceTableModel.addRow(pizzaPrice);
  }
  
  private void deletePrice() {
    int selectedRow = priceTable.getSelectedRow();
    if (selectedRow == -1) {
      POSMessageDialog.showMessage(getParentFrame(), Messages.getString("MenuItemForm.32"));
      return;
    }
    int option = POSMessageDialog.showYesNoQuestionDialog(getParentFrame(), 
      Messages.getString("MenuItemForm.33"), Messages.getString("MenuItemForm.35"));
    if (option != 0) {
      return;
    }
    
    priceTableModel.removeRow(selectedRow);
  }
  
  private void deleteAll()
  {
    int option = POSMessageDialog.showYesNoQuestionDialog(getParentFrame(), 
      Messages.getString("MenuItemForm.36"), Messages.getString("MenuItemForm.37"));
    if (option != 0) {
      return;
    }
    priceTableModel.removeAll();
  }
  
  private void updatePrice() {
    editEvent();
  }
  
  private void editEvent() {
    List<PizzaPrice> pizzaPriceList = priceTableModel.getRows();
    int selectedRow = priceTable.getSelectedRow();
    if (selectedRow == -1) {
      POSMessageDialog.showMessage(getParentFrame(), Messages.getString("MenuItemForm.38"));
      return;
    }
    PizzaPrice pizzaPrice = (PizzaPrice)priceTableModel.getRow(selectedRow);
    PizzaItemPriceDialog pizzaItemPriceDialog = new PizzaItemPriceDialog(getParentFrame(), pizzaPrice, pizzaPriceList);
    pizzaItemPriceDialog.setTitle("Edit Pizza Price");
    pizzaItemPriceDialog.setSize(PosUIManager.getSize(350, 220));
    pizzaItemPriceDialog.open();
    
    if (pizzaItemPriceDialog.isCanceled()) {
      return;
    }
    priceTableModel.fireTableRowsUpdated(selectedRow, selectedRow);
  }
  
  public void actionPerformed(ActionEvent e)
  {
    String actionCommand = e.getActionCommand();
    if (actionCommand.equals("AddModifierGroup")) {
      addMenuItemModifierGroup();
    }
    else if (actionCommand.equals("EditModifierGroup")) {
      editMenuItemModifierGroup();
    }
    else if (actionCommand.equals("DeleteModifierGroup")) {
      deleteMenuItemModifierGroup();
    }
  }
  
  public void stateChanged(ChangeEvent e)
  {
    Component selectedComponent = tabbedPane.getSelectedComponent();
    if (!(selectedComponent instanceof IUpdatebleView)) {
      return;
    }
    
    IUpdatebleView view = (IUpdatebleView)selectedComponent;
    
    MenuItem menuItem = (MenuItem)getBean();
    view.initView(menuItem);
  }
  
  private void filterDuplicateItemSizesAndPrices(List<PizzaPrice> pizzaPriceList) {
    List<PizzaPrice> existedPizzaPriceValueList = priceTableModel.getRows();
    Iterator iterator;
    if (existedPizzaPriceValueList != null)
      for (iterator = existedPizzaPriceValueList.iterator(); iterator.hasNext();) {
        existingPizzaPrice = (PizzaPrice)iterator.next();
        
        for (iterator2 = pizzaPriceList.iterator(); iterator2.hasNext();) {
          PizzaPrice pizzaPrice = (PizzaPrice)iterator2.next();
          if ((existingPizzaPrice.getSize().equals(pizzaPrice.getSize())) && (existingPizzaPrice.getCrust().equals(pizzaPrice.getCrust())))
            iterator2.remove();
        }
      }
    PizzaPrice existingPizzaPrice;
    Iterator iterator2;
  }
  
  private List<PizzaPrice> generatedPossiblePizzaItemSizeAndPriceList() {
    List<MenuItemSize> menuItemSizeList = MenuItemSizeDAO.getInstance().findAll();
    List<PizzaCrust> crustList = PizzaCrustDAO.getInstance().findAll();
    List<PizzaPrice> pizzaPriceList = new ArrayList();
    
    for (int i = 0; i < menuItemSizeList.size(); i++) {
      for (int j = 0; j < crustList.size(); j++) {
        PizzaPrice pizzaPrice = new PizzaPrice();
        pizzaPrice.setSize((MenuItemSize)menuItemSizeList.get(i));
        pizzaPrice.setCrust((PizzaCrust)crustList.get(j));
        pizzaPrice.setPrice(Double.valueOf(0.0D));
        
        pizzaPriceList.add(pizzaPrice);
      }
    }
    return pizzaPriceList;
  }
}
