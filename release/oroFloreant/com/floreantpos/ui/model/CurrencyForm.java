package com.floreantpos.ui.model;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.dao.CurrencyDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.jidesoft.swing.AutoCompletionComboBox;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;



























public class CurrencyForm
  extends BeanEditor
{
  private FixedLengthTextField tfCode;
  private FixedLengthTextField tfName;
  private JTextField tfSymbol;
  private DoubleTextField tfExchangeRate;
  private DoubleTextField tfTolerance;
  private JCheckBox chkMain;
  private AutoCompletionComboBox cbCurrency;
  
  public CurrencyForm()
  {
    this(new com.floreantpos.model.Currency());
  }
  
  public CurrencyForm(com.floreantpos.model.Currency currency) {
    initComponents();
    initData();
    setBean(currency);
  }
  
  private void initData() {
    ComboBoxModel<com.floreantpos.model.Currency> currencyModel = new ComboBoxModel(getAllCurrencies());
    cbCurrency.setModel(currencyModel);
  }
  
  public static List<com.floreantpos.model.Currency> getAllCurrencies() {
    Map<java.util.Currency, Locale> currencyMap = new HashMap();
    Locale[] locs = Locale.getAvailableLocales();
    
    for (Locale loc : locs) {
      try {
        java.util.Currency utilCurrency = java.util.Currency.getInstance(loc);
        

        if (utilCurrency != null) {
          currencyMap.put(utilCurrency, loc);
        }
      }
      catch (Exception localException) {}
    }
    

    Object currencyList = new ArrayList();
    Iterator it = currencyMap.entrySet().iterator();
    while (it.hasNext()) {
      Map.Entry pair = (Map.Entry)it.next();
      java.util.Currency currencyUtil = (java.util.Currency)pair.getKey();
      Locale locale = (Locale)pair.getValue();
      
      com.floreantpos.model.Currency currency = new com.floreantpos.model.Currency();
      currency.setName(currencyUtil.getDisplayName());
      currency.setCode(currencyUtil.getCurrencyCode());
      String symbol = java.util.Currency.getInstance(currencyUtil.getCurrencyCode()).getSymbol(locale);
      currency.setSymbol(symbol);
      ((List)currencyList).add(currency);
    }
    
    Collections.sort((List)currencyList, new Comparator()
    {
      public int compare(com.floreantpos.model.Currency o1, com.floreantpos.model.Currency o2) {
        return o1.getName().compareTo(o2.getName());
      }
      
    });
    return currencyList;
  }
  
  private void initComponents() {
    JPanel contentPanel = new JPanel(new MigLayout("fill"));
    
    JLabel lblCode = new JLabel(Messages.getString("CurrencyForm.1"));
    tfCode = new FixedLengthTextField();
    tfCode.setEnabled(false);
    
    JLabel lblName = new JLabel(POSConstants.NAME + ":");
    tfName = new FixedLengthTextField();
    tfName.setEnabled(false);
    
    JLabel lblExchangeRate = new JLabel(Messages.getString("CurrencyForm.3"));
    tfExchangeRate = new DoubleTextField();
    
    JLabel lblTolerance = new JLabel(Messages.getString("CurrencyForm.4"));
    tfTolerance = new DoubleTextField();
    
    JLabel lblSymbol = new JLabel(Messages.getString("CurrencyForm.5"));
    tfSymbol = new JTextField();
    

    chkMain = new JCheckBox(Messages.getString("CurrencyForm.6"));
    
    cbCurrency = new AutoCompletionComboBox();
    cbCurrency.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        JComboBox cbCurrency = (JComboBox)e.getSource();
        com.floreantpos.model.Currency selectedCurrency = (com.floreantpos.model.Currency)cbCurrency.getSelectedItem();
        tfName.setText(selectedCurrency.getName());
        tfSymbol.setText(selectedCurrency.getSymbol());
        tfCode.setText(selectedCurrency.getCode());
      }
      
    });
    contentPanel.add(new JLabel("Currency"), "");
    contentPanel.add(cbCurrency, "grow, wrap");
    contentPanel.add(lblName, "");
    contentPanel.add(tfName, "grow, wrap");
    contentPanel.add(lblCode, "");
    contentPanel.add(tfCode, "grow, wrap");
    contentPanel.add(lblSymbol, "");
    contentPanel.add(tfSymbol, "grow, wrap");
    contentPanel.add(lblExchangeRate, "");
    contentPanel.add(tfExchangeRate, "grow, wrap");
    contentPanel.add(lblTolerance, "");
    contentPanel.add(tfTolerance, "grow, wrap");
    contentPanel.add(chkMain, "");
    
    add(contentPanel);
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      com.floreantpos.model.Currency currency = (com.floreantpos.model.Currency)getBean();
      CurrencyDAO dao = new CurrencyDAO();
      dao.saveOrUpdate(currency);
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
      return false;
    }
    
    return true;
  }
  
  protected void updateView()
  {
    com.floreantpos.model.Currency currency = (com.floreantpos.model.Currency)getBean();
    if (currency == null) {
      return;
    }
    ComboBoxModel<com.floreantpos.model.Currency> model = (ComboBoxModel)cbCurrency.getModel();
    List<com.floreantpos.model.Currency> dataList = model.getDataList();
    for (com.floreantpos.model.Currency currencyOfList : dataList) {
      if (currencyOfList.getCode().equals(currency.getCode())) {
        cbCurrency.setSelectedItem(currencyOfList);
      }
    }
    tfCode.setText(currency.getCode());
    tfName.setText(currency.getName());
    tfSymbol.setText(currency.getSymbol());
    tfExchangeRate.setText("" + currency.getExchangeRate());
    tfTolerance.setText("" + currency.getTolerance());
    chkMain.setSelected(currency.isMain().booleanValue());
  }
  
  protected boolean updateModel()
  {
    com.floreantpos.model.Currency currency = (com.floreantpos.model.Currency)getBean();
    
    String code = tfCode.getText();
    String name = tfName.getText();
    if (POSUtil.isBlankOrNull(code)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), Messages.getString("CurrencyForm.0"));
      return false;
    }
    
    double exchangeRate = tfExchangeRate.getDouble();
    if ((chkMain.isSelected()) && 
      (exchangeRate != 1.0D)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("CurrencyForm.2"));
      return false;
    }
    

    currency.setCode(code);
    currency.setName(name);
    currency.setSymbol(tfSymbol.getText());
    currency.setMain(Boolean.valueOf(chkMain.isSelected()));
    currency.setExchangeRate(Double.valueOf(exchangeRate));
    currency.setTolerance(Double.valueOf(tfTolerance.getDouble()));
    
    return true;
  }
  
  public String getDisplayText() {
    com.floreantpos.model.Currency currency = (com.floreantpos.model.Currency)getBean();
    if (currency.getId() == null) {
      return Messages.getString("CurrencyForm.20");
    }
    return Messages.getString("CurrencyForm.21");
  }
}
