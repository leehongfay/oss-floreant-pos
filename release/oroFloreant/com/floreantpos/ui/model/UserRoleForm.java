package com.floreantpos.ui.model;

import com.floreantpos.model.User;
import com.floreantpos.model.UserType;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.model.dao.UserTypeDAO;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.GlobalIdGenerator;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;























public class UserRoleForm
  extends BeanEditor
{
  private JComboBox cbUserType;
  private DoubleTextField tfCostPerHour;
  private JCheckBox chkAllowReceiveTips;
  private JCheckBox chkUserInDriver;
  private JCheckBox chkStaffBank;
  private JCheckBox chkAutoStartStaffBank;
  private JCheckBox chkBlindAccountableAmount;
  private JCheckBox chkActive;
  
  public UserRoleForm(User user)
  {
    initComponents();
    
    UserTypeDAO dao = new UserTypeDAO();
    List<UserType> userTypes = dao.findAll();
    cbUserType.setModel(new DefaultComboBoxModel(userTypes.toArray()));
    setBean(user);
  }
  
  private void initComponents() {
    JPanel contentPanel = new JPanel(new MigLayout());
    
    tfCostPerHour = new DoubleTextField(10);
    
    cbUserType = new JComboBox();
    contentPanel.add(new JLabel("User Role"));
    contentPanel.add(cbUserType, "wrap,grow");
    cbUserType.setSelectedItem(null);
    
    contentPanel.add(new JLabel("Cost per hour"));
    contentPanel.add(tfCostPerHour, "wrap");
    
    chkAllowReceiveTips = new JCheckBox("Can receive tips");
    contentPanel.add(chkAllowReceiveTips, "skip 1,newline,grow");
    
    chkUserInDriver = new JCheckBox("User is driver");
    contentPanel.add(chkUserInDriver, "skip 1,newline,grow");
    
    chkStaffBank = new JCheckBox("Staff Bank");
    contentPanel.add(chkStaffBank, "skip 1,newline,grow");
    
    chkAutoStartStaffBank = new JCheckBox("Auto start staff bank");
    contentPanel.add(chkAutoStartStaffBank, "skip 1,newline,grow");
    
    chkBlindAccountableAmount = new JCheckBox("Blind Accountable Amount");
    contentPanel.add(chkBlindAccountableAmount, "skip 1,newline,grow");
    
    chkActive = new JCheckBox("Active");
    contentPanel.add(chkActive, "skip 1,newline,grow");
    
    chkStaffBank.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        chkAutoStartStaffBank.setEnabled(chkStaffBank.isSelected());
        chkBlindAccountableAmount.setEnabled(chkStaffBank.isSelected());
      }
    });
    add(contentPanel);
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      User user = (User)getBean();
      UserDAO dao = new UserDAO();
      dao.saveOrUpdate(user);
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage(), e); }
    return false;
  }
  

  protected void updateView()
  {
    User user = (User)getBean();
    tfCostPerHour.setText(String.valueOf(user.getCostPerHour()));
    chkAllowReceiveTips.setSelected(user.isAllowReceiveTips().booleanValue());
    cbUserType.setSelectedItem(user.getType());
    chkStaffBank.setSelected(user.isStaffBank().booleanValue());
    chkAutoStartStaffBank.setSelected(user.isAutoStartStaffBank().booleanValue());
    chkBlindAccountableAmount.setSelected(user.isBlindAccountableAmount().booleanValue());
    chkUserInDriver.setSelected(user.isDriver().booleanValue());
    
    chkAutoStartStaffBank.setEnabled(chkStaffBank.isSelected());
    chkBlindAccountableAmount.setEnabled(chkStaffBank.isSelected());
    chkActive.setSelected(user.isActive().booleanValue());
  }
  
  protected boolean updateModel()
  {
    User user = (User)getBean();
    UserType selectedRole = (UserType)cbUserType.getSelectedItem();
    if (selectedRole == null) {
      return false;
    }
    double costPerHour = tfCostPerHour.getDouble();
    if (user.getId() == null)
      user.setId(GlobalIdGenerator.generateGlobalId().toString());
    user.setCostPerHour(Double.valueOf(costPerHour));
    user.setAllowReceiveTips(Boolean.valueOf(chkAllowReceiveTips.isSelected()));
    user.setType(selectedRole);
    user.setStaffBank(Boolean.valueOf(chkStaffBank.isSelected()));
    user.setActive(Boolean.valueOf(chkActive.isSelected()));
    
    if (chkStaffBank.isSelected()) {
      user.setAutoStartStaffBank(Boolean.valueOf(chkAutoStartStaffBank.isSelected()));
      user.setBlindAccountableAmount(Boolean.valueOf(chkBlindAccountableAmount.isSelected()));
    }
    else {
      user.setAutoStartStaffBank(Boolean.valueOf(false));
      user.setBlindAccountableAmount(Boolean.valueOf(false));
    }
    user.setDriver(Boolean.valueOf(chkUserInDriver.isSelected()));
    
    User parentUser = user.getParentUser();
    user.setFirstName(parentUser.getFirstName());
    user.setLastName(parentUser.getLastName());
    user.setActive(parentUser.isActive());
    user.setPhoneNo(parentUser.getPhoneNo());
    user.setAvailableForDelivery(parentUser.isAvailableForDelivery());
    user.setImageId(parentUser.getImageId());
    user.setSsn(parentUser.getSsn());
    user.setPassword(null);
    user.setLinkedUser(null);
    user.setRoot(Boolean.valueOf(false));
    
    List<User> linkedUserList = parentUser.getLinkedUser();
    if (linkedUserList != null) {
      for (User linkedUser : linkedUserList) {
        if ((user.getId() == null) && (linkedUser.getType().getName().equals(selectedRole.getName()))) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "This role already exists.");
          return false;
        }
      }
    }
    return true;
  }
  
  public String getDisplayText() {
    return "User role";
  }
}
