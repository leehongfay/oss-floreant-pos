package com.floreantpos.ui.model;

import com.floreantpos.POSConstants;
import com.floreantpos.model.Department;
import com.floreantpos.model.SalesArea;
import com.floreantpos.model.dao.DepartmentDAO;
import com.floreantpos.model.dao.SalesAreaDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.ui.BeanEditor;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;



public class SalesAreaEntryForm
  extends BeanEditor<SalesArea>
{
  private SalesArea salesArea;
  private FixedLengthTextField txtName;
  JLabel lblDeptName = new JLabel("Department");
  JComboBox cbDeptName = new JComboBox();
  
  public SalesAreaEntryForm(SalesArea salesArea) {
    this.salesArea = salesArea;
    
    DepartmentDAO deptDAO = new DepartmentDAO();
    List<Department> deptList = deptDAO.findAll();
    cbDeptName.setModel(new ComboBoxModel(deptList));
    
    initComponents();
    setBean(salesArea);
  }
  
  public void initComponents()
  {
    JPanel mainPanel = new JPanel(new MigLayout("fill,alignx center", "[][]", ""));
    JLabel lblName = new JLabel("Name");
    txtName = new FixedLengthTextField(20);
    
    mainPanel.add(lblName);
    mainPanel.add(txtName, "wrap");
    mainPanel.add(lblDeptName);
    mainPanel.add(cbDeptName, "grow");
    
    add(mainPanel);
  }
  
  public boolean save()
  {
    try
    {
      if (!updateModel()) {
        return false;
      }
      SalesArea item = (SalesArea)getBean();
      SalesAreaDAO salesAreaDAO = new SalesAreaDAO();
      salesAreaDAO.saveOrUpdate(item);
    } catch (Exception e) {
      MessageDialog.showError(POSConstants.ERROR_MESSAGE, e);
      return false;
    }
    return true;
  }
  
  protected void updateView()
  {
    SalesArea item = (SalesArea)getBean();
    
    txtName.setText(item.getName());
    cbDeptName.setSelectedItem(item.getDepartment());
  }
  
  protected boolean updateModel()
    throws IllegalModelStateException
  {
    SalesArea term = (SalesArea)getBean();
    String name = txtName.getText();
    
    if (name == null) {
      MessageDialog.showError("Please enter name");
      return false;
    }
    
    salesArea.setName(name);
    
    term.setDepartment((Department)cbDeptName.getSelectedItem());
    
    return true;
  }
  
  public String getDisplayText()
  {
    SalesArea item = (SalesArea)getBean();
    if (item.getId() == null) {
      return "Add New Sales Area";
    }
    return "Edit Sales Area";
  }
}
