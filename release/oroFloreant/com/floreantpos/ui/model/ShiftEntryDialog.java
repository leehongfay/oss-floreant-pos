package com.floreantpos.ui.model;

import com.floreantpos.POSConstants;
import com.floreantpos.model.DayPart;
import com.floreantpos.model.dao.ShiftDAO;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.ShiftUtil;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.KeyStroke;



















public class ShiftEntryDialog
  extends POSDialog
{
  private JPanel contentPane;
  private JButton buttonOK;
  private JButton buttonCancel;
  private JComboBox startHour;
  private JComboBox startMin;
  private JComboBox endHour;
  private JComboBox endMin;
  private JTextField tfShiftName;
  private Vector<Integer> hours;
  private Vector<Integer> mins;
  private DayPart dayPart;
  private Date shiftStart;
  private Date shiftEnd;
  
  public ShiftEntryDialog()
  {
    this(null);
  }
  
  public ShiftEntryDialog(DayPart shift) {
    super(POSUtil.getBackOfficeWindow(), true);
    

































































































































































    $$$setupUI$$$();setTitle(POSConstants.NEW_SHIFT);setContentPane(contentPane);getRootPane().setDefaultButton(buttonOK);hours = new Vector();
    for (int i = 0; i <= 23; i++) {
      hours.add(Integer.valueOf(i));
    }
    
    mins = new Vector();
    for (int i = 0; i < 60; i++) {
      mins.add(Integer.valueOf(i));
    }
    
    startHour.setModel(new DefaultComboBoxModel(hours));
    endHour.setModel(new DefaultComboBoxModel(hours));
    
    startMin.setModel(new DefaultComboBoxModel(mins));
    endMin.setModel(new DefaultComboBoxModel(mins));
    
    buttonOK.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        ShiftEntryDialog.this.onOK();
      }
      
    });
    buttonCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        ShiftEntryDialog.this.onCancel();
      }
      

    });
    setDefaultCloseOperation(0);
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        ShiftEntryDialog.this.onCancel();
      }
      

    });
    contentPane.registerKeyboardAction(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        ShiftEntryDialog.this.onCancel();
      }
    }, KeyStroke.getKeyStroke(27, 0), 1);
    
    setSize(350, 250);
    
    setShift(shift);
  }
  
  private boolean calculateShifts() {
    int hour1 = ((Integer)startHour.getSelectedItem()).intValue();
    int hour2 = ((Integer)endHour.getSelectedItem()).intValue();
    int min1 = ((Integer)startMin.getSelectedItem()).intValue();
    int min2 = ((Integer)endMin.getSelectedItem()).intValue();
    


    shiftStart = ShiftUtil.buildShiftWithoutAmPm(hour1, min1, 0);
    shiftEnd = ShiftUtil.buildShiftWithoutAmPm(hour2, min2, 59);
    
    if (!shiftEnd.after(shiftStart)) {
      POSMessageDialog.showError(this, POSConstants.SHIFT_END_TIME_MUST_BE_GREATER_THAN_SHIFT_START_TIME);
      return false;
    }
    return true;
  }
  
  private void onOK() {
    try {
      if (!updateModel()) {
        return;
      }
      ShiftDAO.getInstance().saveOrUpdate(dayPart);
      setCanceled(false);
      
      dispose();
    } catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_SAVING_SHIFT_STATE, e);
    }
  }
  
  private void onCancel() {
    setCanceled(true);
    dispose();
  }
  
  public Date getShiftStart() {
    return shiftStart;
  }
  
  public Date getShiftEnd() {
    return shiftEnd;
  }
  
  public void updateView() {
    if (dayPart == null) {
      return;
    }
    
    tfShiftName.setText(dayPart.getName());
    
    Date startTime = dayPart.getStartTime();
    Date endTime = dayPart.getEndTime();
    
    Calendar c = Calendar.getInstance();
    c.setTime(startTime);
    
    int ampm1 = c.get(9);
    if (ampm1 == 0) {
      startHour.setSelectedItem(Integer.valueOf(c.get(11)));
    } else
      startHour.setSelectedItem(Integer.valueOf(c.get(11)));
    startMin.setSelectedItem(Integer.valueOf(c.get(12)));
    
    c.setTime(endTime);
    int ampm2 = c.get(9);
    if (ampm2 == 0) {
      endHour.setSelectedItem(Integer.valueOf(c.get(11)));
    } else
      endHour.setSelectedItem(Integer.valueOf(c.get(11)));
    endMin.setSelectedItem(Integer.valueOf(c.get(12)));
  }
  
  public boolean updateModel() {
    if (!calculateShifts()) {
      return false;
    }
    if (dayPart == null) {
      dayPart = new DayPart();
    }
    dayPart.setName(tfShiftName.getText());
    dayPart.setStartTime(shiftStart);
    dayPart.setEndTime(shiftEnd);
    
    Calendar c = Calendar.getInstance();
    c.setTime(shiftStart);
    
    long length = Math.abs(shiftStart.getTime() - shiftEnd.getTime());
    
    dayPart.setShiftLength(Long.valueOf(length));
    
    return true;
  }
  
  public DayPart getShift() {
    return dayPart;
  }
  
  public void setShift(DayPart dayPart) {
    this.dayPart = dayPart;
    
    updateView();
  }
  













  private void $$$setupUI$$$()
  {
    contentPane = new JPanel();
    contentPane.setLayout(new GridLayoutManager(3, 1, new Insets(10, 10, 10, 10), -1, -1));
    JPanel panel1 = new JPanel();
    panel1.setLayout(new GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
    contentPane.add(panel1, new GridConstraints(2, 0, 1, 1, 0, 3, 3, 1, null, null, null, 0, false));
    Spacer spacer1 = new Spacer();
    panel1.add(spacer1, new GridConstraints(1, 0, 1, 1, 0, 1, 4, 1, null, null, null, 0, false));
    JPanel panel2 = new JPanel();
    panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, true, false));
    panel1.add(panel2, new GridConstraints(1, 1, 1, 1, 0, 3, 3, 3, null, null, null, 0, false));
    buttonOK = new JButton();
    buttonOK.setText(POSConstants.OK);
    panel2.add(buttonOK, new GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null, 0, false));
    buttonCancel = new JButton();
    buttonCancel.setText(POSConstants.CANCEL);
    panel2.add(buttonCancel, new GridConstraints(0, 1, 1, 1, 0, 1, 3, 0, null, null, null, 0, false));
    JSeparator separator1 = new JSeparator();
    panel1.add(separator1, new GridConstraints(0, 0, 1, 2, 2, 1, 4, 4, null, null, null, 0, false));
    JPanel panel3 = new JPanel();
    panel3.setLayout(new GridLayoutManager(6, 5, new Insets(0, 0, 0, 0), -1, -1));
    contentPane.add(panel3, new GridConstraints(0, 0, 1, 1, 0, 3, 3, 3, null, null, null, 0, false));
    
    JLabel label1 = new JLabel();
    label1.setText(POSConstants.START_TIME + ":");
    panel3.add(label1, new GridConstraints(1, 0, 1, 1, 6, 0, 0, 0, null, null, null, 0, false));
    JSeparator separator2 = new JSeparator();
    panel3.add(separator2, new GridConstraints(1, 1, 1, 4, 2, 1, 2, 4, null, null, null, 0, false));
    JLabel label2 = new JLabel();
    label2.setText(POSConstants.HOUR + ":");
    panel3.add(label2, new GridConstraints(2, 0, 1, 1, 4, 0, 0, 0, null, null, null, 0, false));
    startHour = new JComboBox();
    panel3.add(startHour, new GridConstraints(2, 1, 1, 1, 8, 1, 2, 0, null, new Dimension(75, 22), null, 0, false));
    JLabel label3 = new JLabel();
    label3.setText(POSConstants.MIN + ":");
    panel3.add(label3, new GridConstraints(2, 2, 1, 1, 8, 0, 0, 0, null, null, null, 0, false));
    startMin = new JComboBox();
    panel3.add(startMin, new GridConstraints(2, 3, 1, 1, 8, 1, 4, 0, null, null, null, 0, false));
    






    JLabel label4 = new JLabel();
    label4.setText(POSConstants.END_TIME + ":");
    panel3.add(label4, new GridConstraints(4, 0, 1, 1, 6, 0, 0, 0, null, null, null, 0, false));
    JSeparator separator3 = new JSeparator();
    panel3.add(separator3, new GridConstraints(4, 1, 1, 4, 2, 1, 2, 4, null, null, new Dimension(-1, 2), 0, false));
    JLabel label5 = new JLabel();
    label5.setText(POSConstants.HOUR + ":");
    panel3.add(label5, new GridConstraints(5, 0, 1, 1, 4, 0, 0, 0, null, null, null, 0, false));
    endHour = new JComboBox();
    panel3.add(endHour, new GridConstraints(5, 1, 1, 1, 8, 1, 2, 0, null, new Dimension(75, 22), null, 0, false));
    JLabel label6 = new JLabel();
    label6.setText(POSConstants.MIN + ":");
    panel3.add(label6, new GridConstraints(5, 2, 1, 1, 8, 0, 0, 0, null, null, null, 0, false));
    endMin = new JComboBox();
    panel3.add(endMin, new GridConstraints(5, 3, 1, 1, 8, 1, 2, 0, null, null, null, 0, false));
    






    Spacer spacer2 = new Spacer();
    panel3.add(spacer2, new GridConstraints(3, 0, 1, 1, 0, 2, 1, 4, null, null, null, 0, false));
    JLabel label7 = new JLabel();
    label7.setText(POSConstants.SHIFT_NAME + ":");
    panel3.add(label7, new GridConstraints(0, 0, 1, 1, 8, 0, 0, 0, null, null, null, 0, false));
    tfShiftName = new JTextField();
    panel3.add(tfShiftName, new GridConstraints(0, 1, 1, 4, 8, 1, 4, 0, null, new Dimension(150, -1), null, 0, false));
    Spacer spacer3 = new Spacer();
    contentPane.add(spacer3, new GridConstraints(1, 0, 1, 1, 0, 2, 1, 4, null, null, null, 0, false));
  }
  


  public JComponent $$$getRootComponent$$$()
  {
    return contentPane;
  }
}
