package com.floreantpos.ui.model;

import com.floreantpos.POSConstants;
import com.floreantpos.model.MenuItemShift;
import com.floreantpos.model.Shift;
import com.floreantpos.model.dao.ShiftDAO;
import com.floreantpos.swing.ListComboBoxModel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

















public class MenuItemShiftDialog
  extends POSDialog
{
  private JPanel contentPane;
  private JButton buttonOK;
  private JButton buttonCancel;
  private JComboBox cbShifts;
  private JTextField tfPrice;
  private MenuItemShift menuItemShift;
  
  public MenuItemShiftDialog(Frame owner)
  {
    super(owner, true);
    



































































































    $$$setupUI$$$();setContentPane(contentPane);ShiftDAO dao = new ShiftDAO();List<Shift> shifts = dao.findAll();cbShifts.setModel(new ListComboBoxModel(shifts));
    if (shifts.size() == 0) {
      buttonOK.setEnabled(false);
    }
    
    setModal(true);
    getRootPane().setDefaultButton(buttonOK);
    
    buttonOK.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MenuItemShiftDialog.this.onOK();
      }
      
    });
    buttonCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MenuItemShiftDialog.this.onCancel();
      }
      

    });
    setDefaultCloseOperation(0);
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        MenuItemShiftDialog.this.onCancel();
      }
      

    });
    contentPane.registerKeyboardAction(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MenuItemShiftDialog.this.onCancel();
      }
    }, KeyStroke.getKeyStroke(27, 0), 1);
    

    setMenuItemShift(menuItemShift);
  }
  
  private void onOK() {
    if (!updateModel()) { return;
    }
    
    try
    {
      setCanceled(false);
      dispose();
    } catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  private void onCancel() {
    setCanceled(true);
    dispose();
  }
  
  private void updateView()
  {
    if (menuItemShift == null) { return;
    }
    cbShifts.setSelectedItem(menuItemShift.getShift());
    tfPrice.setText(String.valueOf(menuItemShift.getShiftPrice()));
  }
  
  public boolean updateModel() {
    double price = 0.0D;
    try {
      price = Double.parseDouble(tfPrice.getText());
    } catch (Exception x) {
      POSMessageDialog.showError(this, POSConstants.PRICE_IS_NOT_VALID_);
      return false;
    }
    if (menuItemShift == null) {
      menuItemShift = new MenuItemShift();
    }
    menuItemShift.setShift((Shift)cbShifts.getSelectedItem());
    menuItemShift.setShiftPrice(Double.valueOf(price));
    
    return true;
  }
  
  public MenuItemShift getMenuItemShift() {
    return menuItemShift;
  }
  
  public void setMenuItemShift(MenuItemShift menuItemShift) {
    this.menuItemShift = menuItemShift;
    
    updateView();
  }
  













  private void $$$setupUI$$$()
  {
    contentPane = new JPanel();
    contentPane.setLayout(new GridLayoutManager(3, 1, new Insets(10, 10, 10, 10), -1, -1));
    JPanel panel1 = new JPanel();
    panel1.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
    contentPane.add(panel1, new GridConstraints(2, 0, 1, 1, 0, 3, 3, 1, null, null, null, 0, false));
    Spacer spacer1 = new Spacer();
    panel1.add(spacer1, new GridConstraints(0, 0, 1, 1, 0, 1, 4, 1, null, null, null, 0, false));
    JPanel panel2 = new JPanel();
    panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, true, false));
    panel1.add(panel2, new GridConstraints(0, 1, 1, 1, 0, 3, 3, 3, null, null, null, 0, false));
    buttonOK = new JButton();
    buttonOK.setText(POSConstants.OK);
    panel2.add(buttonOK, new GridConstraints(0, 0, 1, 1, 0, 1, 3, 0, null, null, null, 0, false));
    buttonCancel = new JButton();
    buttonCancel.setText(POSConstants.CANCEL);
    panel2.add(buttonCancel, new GridConstraints(0, 1, 1, 1, 0, 1, 3, 0, null, null, null, 0, false));
    JPanel panel3 = new JPanel();
    panel3.setLayout(new GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
    contentPane.add(panel3, new GridConstraints(0, 0, 1, 1, 0, 3, 3, 3, null, null, null, 0, false));
    JLabel label1 = new JLabel();
    label1.setText(POSConstants.SELECT_SHIFT + ":");
    panel3.add(label1, new GridConstraints(0, 0, 1, 1, 8, 0, 0, 0, null, null, null, 0, false));
    cbShifts = new JComboBox();
    panel3.add(cbShifts, new GridConstraints(0, 1, 1, 1, 8, 1, 2, 0, null, null, null, 0, false));
    JLabel label2 = new JLabel();
    label2.setText(POSConstants.PRICE + ":");
    panel3.add(label2, new GridConstraints(1, 0, 1, 1, 4, 0, 0, 0, null, null, null, 0, false));
    tfPrice = new JTextField();
    panel3.add(tfPrice, new GridConstraints(1, 1, 1, 1, 8, 1, 4, 0, null, new Dimension(150, -1), null, 0, false));
    Spacer spacer2 = new Spacer();
    contentPane.add(spacer2, new GridConstraints(1, 0, 1, 1, 0, 2, 1, 4, null, new Dimension(-1, 15), null, 0, false));
  }
  


  public JComponent $$$getRootComponent$$$()
  {
    return contentPane;
  }
}
