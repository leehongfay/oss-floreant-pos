package com.floreantpos.ui.model;

import com.floreantpos.Messages;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.ModifierGroup;
import com.floreantpos.model.dao.ModifierGroupDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.FixedLengthDocument;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import net.miginfocom.swing.MigLayout;































public class MenuItemModifierSpecForm
  extends BeanEditor
{
  private JLabel lblName;
  private JLabel lblMin;
  private JLabel lblMax;
  private JLabel lblSortOrder;
  private IntegerTextField tfSortOrder = new IntegerTextField();
  private FixedLengthTextField tfName = new FixedLengthTextField(60);
  private JTextArea taInstruction = new JTextArea(new FixedLengthDocument(220));
  private IntegerTextField tfMaxQuantity = new IntegerTextField();
  private IntegerTextField tfMinQuantity = new IntegerTextField();
  private JCheckBox chkVisible;
  private JComboBox cbMenuModifierGroup;
  private JCheckBox chkAutoShow;
  private JCheckBox chkJumbToNextGroup;
  private PosButton btnSaveAndAutoBuild;
  private boolean autoBuildSelected;
  private MenuItem menuItem;
  private MenuItemModifierSpec modifierSpec;
  private boolean editMode;
  
  public MenuItemModifierSpecForm(MenuItemModifierSpec modifierSpec, MenuItem menuItem, boolean editMode) {
    this.menuItem = menuItem;
    this.modifierSpec = modifierSpec;
    this.editMode = editMode;
    initComponents();
    initList();
    setBean(modifierSpec);
  }
  
  private void initList() {
    List<ModifierGroup> modifierGroups = null;
    if (menuItem.isPizzaType().booleanValue()) {
      modifierGroups = ModifierGroupDAO.getInstance().findPizzaModifierGroups();
    }
    else {
      modifierGroups = ModifierGroupDAO.getInstance().findNormalModifierGroups();
    }
    ComboBoxModel model = new ComboBoxModel(modifierGroups);
    cbMenuModifierGroup.setModel(model);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    JPanel contentPanel = new JPanel();
    contentPanel.setLayout(new MigLayout("fillx,inset 5 15 5 15", "[][grow]", ""));
    
    lblName = new JLabel(Messages.getString("MenuItemModifierGroupForm.4"));
    lblMin = new JLabel(Messages.getString("MenuItemModifierGroupForm.6"));
    lblMax = new JLabel(Messages.getString("MenuItemModifierGroupForm.7"));
    lblSortOrder = new JLabel("Sort Order");
    chkVisible = new JCheckBox("Visible");
    chkAutoShow = new JCheckBox("Auto show");
    chkJumbToNextGroup = new JCheckBox("Jump to next group");
    cbMenuModifierGroup = new JComboBox();
    
    tfName.setColumns(20);
    tfMinQuantity.setColumns(10);
    tfMaxQuantity.setColumns(10);
    tfSortOrder.setColumns(10);
    taInstruction.setLineWrap(true);
    taInstruction.setRows(4);
    
    tfMinQuantity.setText("1");
    tfMaxQuantity.setText("1");
    chkVisible.setSelected(true);
    
    btnSaveAndAutoBuild = new PosButton("SAVE and AUTO BUILD PAGES");
    btnSaveAndAutoBuild.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (!save())
          return;
        autoBuildSelected = true;
        editorDialog.dispose();
      }
      

    });
    JButton btnAddNewGroup = new JButton("...");
    btnAddNewGroup.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuItemModifierSpecForm.this.doAddNewModifierGroup();
      }
      

    });
    contentPanel.add(new JLabel("Group"));
    contentPanel.add(cbMenuModifierGroup, "grow");
    
    contentPanel.add(new JLabel("Instruction"), "newline,aligny top");
    contentPanel.add(new JScrollPane(taInstruction), "grow");
    contentPanel.add(lblMin, "newline");
    contentPanel.add(tfMinQuantity);
    contentPanel.add(lblMax, "newline");
    contentPanel.add(tfMaxQuantity);
    contentPanel.add(lblSortOrder, "newline");
    contentPanel.add(tfSortOrder);
    contentPanel.add(chkJumbToNextGroup, "skip 1,newline");
    contentPanel.add(chkAutoShow, "skip 1,newline");
    contentPanel.add(chkVisible, "skip 1,newline");
    
    add(contentPanel);
  }
  
  private void doAddNewModifierGroup()
  {
    try {
      ModifierGroupForm form = new ModifierGroupForm(new ModifierGroup());
      BeanEditorDialog dialog = new BeanEditorDialog(form);
      dialog.pack();
      dialog.open();
      if (dialog.isCanceled())
        return;
      ComboBoxModel model = (ComboBoxModel)cbMenuModifierGroup.getModel();
      Object modifierGroup = form.getBean();
      model.addElement(modifierGroup);
      cbMenuModifierGroup.setSelectedItem(modifierGroup);
    } catch (Exception e2) {
      BOMessageDialog.showError(POSUtil.getFocusedWindow(), e2.getMessage(), e2);
    }
  }
  
  public PosButton getAutoBuildButton() {
    return btnSaveAndAutoBuild;
  }
  
  public boolean save()
  {
    return updateModel();
  }
  
  protected void updateView()
  {
    MenuItemModifierSpec menuItemModifierGroup = getBean();
    if ((menuItemModifierGroup == null) || (menuItemModifierGroup.getId() == null))
      return;
    tfName.setText(menuItemModifierGroup.getName());
    tfMinQuantity.setText(String.valueOf(menuItemModifierGroup.getMinQuantity()));
    tfMaxQuantity.setText(String.valueOf(menuItemModifierGroup.getMaxQuantity()));
    tfSortOrder.setText(String.valueOf(menuItemModifierGroup.getSortOrder()));
    chkVisible.setSelected(menuItemModifierGroup.isEnable().booleanValue());
    cbMenuModifierGroup.setSelectedItem(menuItemModifierGroup.getModifierGroup());
    taInstruction.setText(menuItemModifierGroup.getInstruction());
    chkJumbToNextGroup.setSelected(menuItemModifierGroup.isJumpGroup().booleanValue());
    chkAutoShow.setSelected(menuItemModifierGroup.isAutoShow().booleanValue());
  }
  
  protected boolean updateModel()
  {
    MenuItemModifierSpec modifierGroup = getBean();
    
    ModifierGroup selectedGroup = (ModifierGroup)cbMenuModifierGroup.getSelectedItem();
    if (selectedGroup == null) {
      return false;
    }
    if (hasSimilerGroupSpec(selectedGroup)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Selected group already exists.");
      return false;
    }
    modifierGroup.setModifierGroup(selectedGroup);
    modifierGroup.setName(selectedGroup.getName());
    int minQuantity = tfMinQuantity.getInteger();
    int maxQuantity = tfMaxQuantity.getInteger();
    int sortOrder = tfSortOrder.getInteger();
    boolean selected = chkVisible.isSelected();
    
    modifierGroup.setMinQuantity(Integer.valueOf(minQuantity));
    modifierGroup.setMaxQuantity(Integer.valueOf(maxQuantity));
    modifierGroup.setSortOrder(Integer.valueOf(sortOrder));
    modifierGroup.setEnable(Boolean.valueOf(selected));
    
    modifierGroup.setInstruction(taInstruction.getText());
    modifierGroup.setJumpGroup(Boolean.valueOf(chkJumbToNextGroup.isSelected()));
    modifierGroup.setAutoShow(Boolean.valueOf(chkAutoShow.isSelected()));
    return true;
  }
  
  private boolean hasSimilerGroupSpec(ModifierGroup selectedGroup) {
    if ((editMode) && (selectedGroup.getName().equals(modifierSpec.getName()))) {
      return false;
    }
    List<MenuItemModifierSpec> menuItemModiferSpecs = menuItem.getMenuItemModiferSpecs();
    if ((menuItemModiferSpecs == null) || (menuItemModiferSpecs.isEmpty()))
      return false;
    for (Iterator iterator = menuItemModiferSpecs.iterator(); iterator.hasNext();) {
      MenuItemModifierSpec menuItemModifierSpec = (MenuItemModifierSpec)iterator.next();
      if (menuItemModifierSpec.getModifierGroup().getName().equals(selectedGroup.getName())) {
        return true;
      }
    }
    return false;
  }
  
  public String getDisplayText()
  {
    MenuItemModifierSpec modifierGroup = getBean();
    if (modifierGroup.getId() == null) {
      return "New modifier group";
    }
    
    return "Edit modifier group";
  }
  
  public MenuItemModifierSpec getBean()
  {
    MenuItemModifierSpec modifierGroup = (MenuItemModifierSpec)super.getBean();
    if (modifierGroup == null) {
      modifierGroup = new MenuItemModifierSpec();
      setBean(modifierGroup);
    }
    
    return modifierGroup;
  }
  
  public boolean isAutoBuildSelected() {
    return autoBuildSelected;
  }
}
