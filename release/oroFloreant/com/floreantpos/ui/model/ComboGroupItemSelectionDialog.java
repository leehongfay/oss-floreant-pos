package com.floreantpos.ui.model;

import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.model.ComboGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang3.StringUtils;


















public class ComboGroupItemSelectionDialog
  extends POSDialog
  implements ActionListener
{
  private MenuItemSelectionView menuSelectorPanel;
  private IntegerTextField tfMaxQuantity = new IntegerTextField(10);
  private JLabel lblMinQuantity = new JLabel("Min Quantity");
  private JLabel lblMaxQuantity = new JLabel("Max Quantity");
  private IntegerTextField tfMinQuantity = new IntegerTextField();
  private FixedLengthTextField tfComboGroupName = new FixedLengthTextField();
  private ComboGroup comboGroup;
  private MenuItem menuItem;
  private boolean editMode;
  
  public ComboGroupItemSelectionDialog(MenuItem menuItem, ComboGroup comboGroup) {
    this(menuItem, comboGroup, false);
  }
  
  public ComboGroupItemSelectionDialog(MenuItem menuItem, ComboGroup comboGroup, boolean editMode) {
    super(POSUtil.getFocusedWindow(), "");
    this.comboGroup = comboGroup;
    this.menuItem = menuItem;
    this.editMode = editMode;
    init();
    tfComboGroupName.setText(comboGroup.getName());
    tfMaxQuantity.setText(String.valueOf(comboGroup.getMaxQuantity()));
    tfMinQuantity.setText(String.valueOf(comboGroup.getMinQuantity()));
  }
  
  private void init()
  {
    setLayout(new BorderLayout(10, 10));
    setIconImage(Application.getPosWindow().getIconImage());
    
    JPanel contentPane = new JPanel(new MigLayout("fill,hidemode 3,inset 0 10 0 10"));
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Select item");
    add(titlePanel, "North");
    
    menuSelectorPanel = new MenuItemSelectionView(comboGroup.getItems(), false);
    menuSelectorPanel.setParentMenuItem(menuItem, editMode);
    menuSelectorPanel.setEnableSearch(false);
    contentPane.add(new JLabel("Group Name"));
    contentPane.add(tfComboGroupName, "grow,wrap");
    contentPane.add(lblMinQuantity);
    contentPane.add(tfMinQuantity, "grow,wrap");
    contentPane.add(lblMaxQuantity);
    contentPane.add(tfMaxQuantity, "grow,wrap");
    
    contentPane.add(menuSelectorPanel, "grow,span,wrap");
    
    PosButton btnOk = new PosButton(POSConstants.OK);
    btnOk.setFocusable(false);
    btnOk.addActionListener(this);
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL);
    btnCancel.setFocusable(false);
    btnCancel.addActionListener(this);
    
    JPanel footerPanel = new JPanel(new MigLayout("center,ins 0 5 5 5", "", ""));
    footerPanel.add(btnOk);
    footerPanel.add(btnCancel);
    
    add(footerPanel, "South");
    
    add(contentPane);
    pack();
  }
  
  public void setQuantity(Integer quantity) {
    tfMaxQuantity.setText(String.valueOf(quantity));
  }
  
  private void doOk() {
    List<MenuItem> menuItems = menuSelectorPanel.getSelectedMenuItemList();
    if ((menuItems == null) || (menuItems.isEmpty())) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select menu item");
      return;
    }
    String groupName = tfComboGroupName.getText();
    if (StringUtils.isEmpty(groupName)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please enter group name.");
      return;
    }
    int minQuantity = tfMinQuantity.getInteger();
    int maxQuantity = tfMaxQuantity.getInteger();
    if (minQuantity > maxQuantity) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Min quantity cannot be greater than max.");
      return;
    }
    if (maxQuantity == 0) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Max quantity cannot be zero!");
      return;
    }
    if (menuItem.getComboGroups() != null) {
      for (ComboGroup comboGroup : menuItem.getComboGroups()) {
        if (!comboGroup.getName().equals(groupName))
        {
          if (comboGroup.getName().equals(groupName)) {
            POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Group name already exists.");
            return;
          } }
      }
    }
    this.comboGroup.setName(groupName);
    this.comboGroup.setMinQuantity(Integer.valueOf(minQuantity));
    this.comboGroup.setMaxQuantity(Integer.valueOf(maxQuantity));
    this.comboGroup.setItems(menuItems);
    setCanceled(false);
    dispose();
  }
  
  private void doCancel() {
    setCanceled(true);
    dispose();
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    
    if (POSConstants.CANCEL.equalsIgnoreCase(actionCommand)) {
      doCancel();
    }
    else if (POSConstants.OK.equalsIgnoreCase(actionCommand)) {
      doOk();
    }
  }
}
