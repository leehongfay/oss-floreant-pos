package com.floreantpos.ui.model;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.Multiplier;
import com.floreantpos.model.dao.MultiplierDAO;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;



























public class MultiplierForm
  extends BeanEditor
{
  private FixedLengthTextField tfName;
  private FixedLengthTextField tfTicketPrefix;
  private DoubleTextField tfRate;
  private IntegerTextField tfSortOrder;
  private JButton btnButtonColor;
  private JButton btnTextColor;
  
  public MultiplierForm()
  {
    this(new Multiplier());
  }
  
  public MultiplierForm(Multiplier multiplier) {
    initComponents();
    setBean(multiplier);
  }
  
  private void initComponents() {
    tfName = new FixedLengthTextField();
    tfName.setLength(20);
    tfRate = new DoubleTextField();
    
    tfTicketPrefix = new FixedLengthTextField();
    tfTicketPrefix.setLength(20);
    
    tfRate.setHorizontalAlignment(4);
    
    tfSortOrder = new IntegerTextField();
    
    JLabel lblButtonColor = new JLabel(Messages.getString("MenuModifierForm.1"));
    JLabel lblTextColor = new JLabel(Messages.getString("MenuModifierForm.27"));
    
    btnButtonColor = new JButton("");
    btnButtonColor.setPreferredSize(new Dimension(140, 40));
    
    btnTextColor = new JButton(Messages.getString("MenuModifierForm.29"));
    btnTextColor.setPreferredSize(new Dimension(140, 40));
    
    btnButtonColor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        Color color = JColorChooser.showDialog(POSUtil.getBackOfficeWindow(), Messages.getString("MenuModifierForm.39"), btnButtonColor.getBackground());
        btnButtonColor.setBackground(color);
        btnTextColor.setBackground(color);
      }
      
    });
    btnTextColor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        Color color = JColorChooser.showDialog(POSUtil.getBackOfficeWindow(), Messages.getString("MenuModifierForm.40"), btnTextColor.getForeground());
        btnTextColor.setForeground(color);
      }
      
    });
    JPanel contentPanel = new JPanel(new MigLayout("fillx"));
    
    contentPanel.add(new JLabel("Name"));
    contentPanel.add(tfName, "grow,wrap");
    
    contentPanel.add(new JLabel("Ticket prefix"));
    contentPanel.add(tfTicketPrefix, "grow,wrap");
    
    contentPanel.add(new JLabel("Percentage (%)"));
    contentPanel.add(tfRate, "grow,wrap");
    
    contentPanel.add(new JLabel("Sort Order"));
    contentPanel.add(tfSortOrder, "grow,wrap");
    
    contentPanel.add(lblButtonColor);
    contentPanel.add(btnButtonColor, "wrap");
    
    contentPanel.add(lblTextColor);
    contentPanel.add(btnTextColor, "wrap");
    
    add(contentPanel);
  }
  
  public boolean save()
  {
    try
    {
      if (!updateModel()) {
        return false;
      }
      Multiplier multiplier = (Multiplier)getBean();
      MultiplierDAO dao = new MultiplierDAO();
      if (dao.get(multiplier.getId()) == null) {
        dao.save(multiplier);
      }
      else
        dao.update(multiplier);
    } catch (Exception e) {
      MessageDialog.showError(e);
      return false;
    }
    
    return true;
  }
  
  protected void updateView()
  {
    Multiplier multiplier = (Multiplier)getBean();
    tfName.setText(multiplier.getId());
    tfTicketPrefix.setText(multiplier.getTicketPrefix());
    tfRate.setText("" + multiplier.getRate());
    tfSortOrder.setText(String.valueOf(multiplier.getSortOrder()));
    
    tfName.setEnabled(multiplier.getId() == null);
    
    if (multiplier.getButtonColor() != null) {
      Color color = new Color(multiplier.getButtonColor().intValue());
      btnButtonColor.setBackground(color);
      btnTextColor.setBackground(color);
    }
    
    if (multiplier.getTextColor() != null) {
      Color color = new Color(multiplier.getTextColor().intValue());
      btnTextColor.setForeground(color);
    }
  }
  
  protected boolean updateModel()
  {
    String name = tfName.getText();
    Multiplier multiplier = (Multiplier)getBean();
    
    if (POSUtil.isBlankOrNull(name)) {
      MessageDialog.showError(POSConstants.NAME_REQUIRED);
      return false;
    }
    if ((multiplier.getId() == null) && (MultiplierDAO.getInstance().nameExists(name))) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), "An multiplier with that name already exists");
      return false;
    }
    multiplier.setId(name);
    multiplier.setRate(Double.valueOf(tfRate.getDouble()));
    multiplier.setTicketPrefix(tfTicketPrefix.getText());
    multiplier.setSortOrder(Integer.valueOf(tfSortOrder.getInteger()));
    multiplier.setButtonColor(Integer.valueOf(btnButtonColor.getBackground().getRGB()));
    multiplier.setTextColor(Integer.valueOf(btnTextColor.getForeground().getRGB()));
    
    return true;
  }
  
  public String getDisplayText() {
    Multiplier multiplier = (Multiplier)getBean();
    if (multiplier.getId() == null) {
      return "New multiplier";
    }
    return "Edit multiplier";
  }
}
