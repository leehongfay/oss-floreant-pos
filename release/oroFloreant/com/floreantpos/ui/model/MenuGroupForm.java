package com.floreantpos.ui.model;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.util.POSUtil;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import net.miginfocom.swing.MigLayout;










public class MenuGroupForm
  extends BeanEditor
{
  private JButton btnNewCategory;
  private JComboBox cbCategory;
  private JCheckBox chkVisible;
  private JLabel jLabel1;
  private JLabel jLabel2;
  private FixedLengthTextField tfName;
  private JLabel lblTranslatedName;
  private FixedLengthTextField tfTranslatedName;
  private JLabel lblSortOrder;
  private JLabel lblButtonColor;
  private IntegerTextField tfSortOrder;
  private JButton btnButtonColor;
  private JLabel lblTextColor;
  private JButton btnTextColor;
  
  public MenuGroupForm()
  {
    this(new MenuGroup());
  }
  
  public MenuGroupForm(MenuGroup foodGroup) {
    initComponents();
    
    setBean(foodGroup);
  }
  






  private void initComponents()
  {
    setLayout(new MigLayout("", "[70px][289px,grow][6px][49px]", "[19px][][25px][][][][15px]"));
    
    setPreferredSize(new Dimension(600, 250));
    jLabel1 = new JLabel();
    tfName = new FixedLengthTextField(120);
    jLabel2 = new JLabel();
    cbCategory = new JComboBox();
    chkVisible = new JCheckBox();
    btnNewCategory = new JButton();
    
    jLabel1.setText(POSConstants.NAME + ":");
    
    jLabel2.setText(POSConstants.CATEGORY + ":");
    
    chkVisible.setText(POSConstants.VISIBLE);
    chkVisible.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    
    chkVisible.setMargin(new Insets(0, 0, 0, 0));
    
    btnNewCategory.setText("...");
    btnNewCategory.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        MenuGroupForm.this.doNewCategory(evt);
      }
      
    });
    lblTranslatedName = new JLabel(Messages.getString("MenuGroupForm.6"));
    add(lblTranslatedName, "cell 0 1,alignx trailing");
    
    tfTranslatedName = new FixedLengthTextField(120);
    add(tfTranslatedName, "cell 1 1,growx");
    add(jLabel2, "cell 0 2,alignx left,aligny center");
    add(jLabel1, "cell 0 0,alignx left,aligny center");
    add(tfName, "cell 1 0,growx");
    
    lblSortOrder = new JLabel(Messages.getString("MenuGroupForm.12"));
    add(lblSortOrder, "cell 0 3,alignx leading");
    
    tfSortOrder = new IntegerTextField();
    tfSortOrder.setColumns(10);
    add(tfSortOrder, "cell 1 3");
    
    lblButtonColor = new JLabel(Messages.getString("MenuGroupForm.15"));
    add(lblButtonColor, "cell 0 4");
    
    btnButtonColor = new JButton("");
    btnButtonColor.setPreferredSize(new Dimension(140, 40));
    add(btnButtonColor, "cell 1 4,growy");
    
    lblTextColor = new JLabel(Messages.getString("MenuGroupForm.19"));
    add(lblTextColor, "cell 0 5");
    
    btnTextColor = new JButton(Messages.getString("MenuGroupForm.21"));
    btnTextColor.setPreferredSize(new Dimension(140, 40));
    add(btnTextColor, "cell 1 5");
    add(chkVisible, "cell 1 6,alignx left,aligny top");
    add(cbCategory, "cell 1 2,growx,aligny top");
    add(btnNewCategory, "cell 3 2,alignx left,aligny top");
    
    btnButtonColor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        Color color = JColorChooser.showDialog(
          POSUtil.getBackOfficeWindow(), 
          Messages.getString("MenuGroupForm.26"), btnButtonColor.getBackground());
        btnButtonColor.setBackground(color);
        btnTextColor.setBackground(color);
      }
      
    });
    btnTextColor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        Color color = JColorChooser.showDialog(
          POSUtil.getBackOfficeWindow(), 
          Messages.getString("MenuGroupForm.27"), btnTextColor.getForeground());
        btnTextColor.setForeground(color);
      }
      
    });
    MenuCategoryDAO categoryDAO = new MenuCategoryDAO();
    List<MenuCategory> foodCategories = categoryDAO.findAll();
    cbCategory.setModel(new ComboBoxModel(foodCategories));
    cbCategory.setSelectedItem(null);
  }
  
  private void doNewCategory(ActionEvent evt) {
    try {
      MenuCategoryForm editor = new MenuCategoryForm();
      
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (!dialog.isCanceled()) {
        MenuCategory foodCategory = (MenuCategory)editor.getBean();
        ComboBoxModel model = (ComboBoxModel)cbCategory.getModel();
        model.addElement(foodCategory);
        model.setSelectedItem(foodCategory);
      }
    } catch (Exception x) {
      MessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  


















  public boolean save()
  {
    if (!updateModel()) {
      return false;
    }
    MenuGroup menuGroup = (MenuGroup)getBean();
    try
    {
      MenuGroupDAO menuGroupDAO = new MenuGroupDAO();
      menuGroupDAO.saveOrUpdate(menuGroup);
    } catch (Exception e) {
      MessageDialog.showError(e);
      return false;
    }
    return true;
  }
  
  protected void updateView()
  {
    MenuGroup menuGroup = (MenuGroup)getBean();
    if (menuGroup == null) {
      tfName.setText("");
      cbCategory.setSelectedItem(null);
      chkVisible.setSelected(false);
      return;
    }
    tfName.setText(menuGroup.getName());
    tfTranslatedName.setText(menuGroup.getTranslatedName());
    
    if (menuGroup.getSortOrder() != null) {
      tfSortOrder.setText(menuGroup.getSortOrder().toString());
    }
    
    Color buttonColor = menuGroup.getButtonColor();
    if (buttonColor != null) {
      btnButtonColor.setBackground(buttonColor);
      btnTextColor.setBackground(buttonColor);
    }
    
    if (menuGroup.getTextColor() != null) {
      btnTextColor.setForeground(menuGroup.getTextColor());
    }
    
    chkVisible.setSelected(menuGroup.isVisible().booleanValue());
    
    for (int i = 0; i < cbCategory.getItemCount(); i++) {
      MenuCategory menuCategory = (MenuCategory)cbCategory.getItemAt(i);
      if ((menuCategory != null) && (menuCategory.getId() != null) && 
        (menuCategory.getId().equals(menuGroup.getMenuCategoryId()))) {
        cbCategory.setSelectedIndex(i);
        break;
      }
    }
  }
  

  protected boolean updateModel()
  {
    MenuGroup menuGroup = (MenuGroup)getBean();
    if (menuGroup == null) {
      return false;
    }
    
    String name = tfName.getText();
    if (POSUtil.isBlankOrNull(name)) {
      MessageDialog.showError(Messages.getString("MenuGroupForm.29"));
      return false;
    }
    
    MenuCategory category = (MenuCategory)cbCategory.getSelectedItem();
    if (category == null) {
      MessageDialog.showError(Messages.getString("MenuGroupForm.30"));
      return false;
    }
    
    menuGroup.setName(tfName.getText());
    
    menuGroup.setTranslatedName(tfTranslatedName.getText());
    menuGroup.setSortOrder(Integer.valueOf(tfSortOrder.getInteger()));
    
    menuGroup.setButtonColor(btnButtonColor.getBackground());
    menuGroup.setTextColor(btnTextColor.getForeground());
    
    menuGroup.setButtonColorCode(Integer.valueOf(btnButtonColor.getBackground().getRGB()));
    menuGroup.setTextColorCode(Integer.valueOf(btnTextColor.getForeground().getRGB()));
    MenuCategory menuCategory = (MenuCategory)cbCategory.getSelectedItem();
    if (menuCategory != null) {
      menuGroup.setMenuCategoryId(menuCategory.getId());
      menuGroup.setCategoryName(menuCategory.getDisplayName());
    }
    menuGroup.setVisible(Boolean.valueOf(chkVisible.isSelected()));
    
    return true;
  }
  
  public String getDisplayText() {
    MenuGroup foodGroup = (MenuGroup)getBean();
    if (foodGroup.getId() == null) {
      return Messages.getString("MenuGroupForm.31");
    }
    return Messages.getString("MenuGroupForm.32");
  }
}
