package com.floreantpos.ui.model;

import com.floreantpos.model.ReportGroup;
import com.floreantpos.model.dao.ReportGroupDAO;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.ui.BeanEditor;
import java.awt.Dimension;
import javax.swing.JLabel;
import net.miginfocom.swing.MigLayout;




























public class ReportGroupForm
  extends BeanEditor
{
  private JLabel lblDescription;
  private FixedLengthTextField tfDescription;
  private JLabel jLabel1;
  private FixedLengthTextField tfName;
  
  public ReportGroupForm()
  {
    initComponents();
    setBean(new ReportGroup());
  }
  
  public ReportGroupForm(ReportGroup dept) {
    initComponents();
    setBean(dept);
  }
  





  private void initComponents()
  {
    setLayout(new MigLayout("", "[70px][289px,grow][6px][49px]", ""));
    setPreferredSize(new Dimension(600, 250));
    jLabel1 = new JLabel();
    tfName = new FixedLengthTextField(120);
    
    jLabel1.setText("ReportGroup Name:");
    
    lblDescription = new JLabel("Report Description:");
    add(lblDescription, "cell 0 1,alignx trailing");
    
    tfDescription = new FixedLengthTextField(120);
    add(tfDescription, "cell 1 1,growx");
    add(jLabel1, "cell 0 0,alignx left,aligny center");
    add(tfName, "cell 1 0,growx");
  }
  







  public boolean save()
  {
    if (!updateModel()) {
      return false;
    }
    ReportGroup reportGroup = (ReportGroup)getBean();
    try
    {
      ReportGroupDAO reportGroupDAO = new ReportGroupDAO();
      reportGroupDAO.saveOrUpdate(reportGroup);
    } catch (Exception e) {
      MessageDialog.showError(e);
      return false;
    }
    return true;
  }
  


  protected void updateView() {}
  

  protected boolean updateModel()
  {
    ReportGroup reportGroupModel = (ReportGroup)getBean();
    reportGroupModel.setName(tfName.getText());
    reportGroupModel.setDescription(tfDescription.getText());
    return true;
  }
  
  public String getDisplayText()
  {
    return "Add Report Group";
  }
}
