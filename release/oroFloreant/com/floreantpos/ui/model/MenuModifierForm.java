package com.floreantpos.ui.model;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.ModifierMultiplierPrice;
import com.floreantpos.model.Multiplier;
import com.floreantpos.model.TaxGroup;
import com.floreantpos.model.dao.ModifierDAO;
import com.floreantpos.model.dao.MultiplierDAO;
import com.floreantpos.model.dao.TaxGroupDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import net.miginfocom.swing.MigLayout;































public class MenuModifierForm
  extends BeanEditor
{
  private MenuModifier modifier;
  private JCheckBox chkPrintToKitchen;
  private JComboBox cbTaxGroup;
  private JFormattedTextField tfName;
  private FixedLengthTextField tfTranslatedName;
  private DoubleTextField tfNormalPrice;
  private DoubleTextField tfExtraPrice;
  private IntegerTextField tfSortOrder;
  private JButton btnButtonColor;
  private JButton btnTextColor;
  private JTable priceTable;
  private JTabbedPane jTabbedPane1;
  private Map<String, MultiplierPricePanel> itemMap = new HashMap();
  private JCheckBox chkUseFixedPrice;
  private JCheckBox chkComboModifier;
  
  public MenuModifierForm() throws Exception {
    this(new MenuModifier());
  }
  
  public MenuModifierForm(MenuModifier modifier) throws Exception {
    this.modifier = modifier;
    
    checkRegularMultiplier();
    initComponents();
    initData();
    
    add(jTabbedPane1);
    
    setBean(modifier);
  }
  
  private void initData() { ComboBoxModel comboBoxModel = new ComboBoxModel();
    comboBoxModel.addElement(null);
    List<TaxGroup> taxGroups = TaxGroupDAO.getInstance().findAll();
    for (TaxGroup taxGroup : taxGroups) {
      comboBoxModel.addElement(taxGroup);
    }
    cbTaxGroup.setModel(comboBoxModel);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(0, 0));
    jTabbedPane1 = new JTabbedPane();
    tfName = new JFormattedTextField();
    tfTranslatedName = new FixedLengthTextField();
    tfNormalPrice = new DoubleTextField();
    tfExtraPrice = new DoubleTextField();
    tfSortOrder = new IntegerTextField();
    cbTaxGroup = new JComboBox();
    JButton btnNewTax = new JButton();
    chkPrintToKitchen = new JCheckBox();
    chkUseFixedPrice = new JCheckBox("Use fixed price");
    
    JPanel tabPrice = new JPanel();
    JScrollPane jScrollPane3 = new JScrollPane();
    priceTable = new JTable();
    
    JLabel lblName = new JLabel(POSConstants.NAME + ":");
    JLabel lblTranslatedName = new JLabel(Messages.getString("MenuModifierForm.0"));
    JLabel lblSortOrder = new JLabel(Messages.getString("MenuModifierForm.15"));
    JLabel lblTaxRate = new JLabel("Tax Group:");
    JLabel lblPercentage = new JLabel();
    
    tfExtraPrice.setText("0");
    lblPercentage.setText("");
    tfNormalPrice.setText("0");
    
    btnNewTax.setText("...");
    btnNewTax.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        MenuModifierForm.this.btnNewTaxActionPerformed(evt);
      }
      
    });
    chkPrintToKitchen.setText(POSConstants.PRINT_TO_KITCHEN);
    chkPrintToKitchen.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    chkPrintToKitchen.setMargin(new Insets(0, 0, 0, 0));
    
    JPanel generalTabPanel = new JPanel(new BorderLayout());
    jTabbedPane1.addTab(POSConstants.GENERAL, generalTabPanel);
    
    TransparentPanel lelfInputPanel = new TransparentPanel();
    lelfInputPanel.setLayout(new MigLayout("wrap 2,hidemode 3", "[90px][grow]", ""));
    
    lelfInputPanel.add(lblName, "alignx left,aligny center");
    lelfInputPanel.add(tfName, "growx,aligny top");
    
    lelfInputPanel.add(lblTranslatedName, "alignx left,aligny center");
    lelfInputPanel.add(tfTranslatedName, "growx");
    
    JPanel rightInputPanel = new JPanel(new MigLayout("wrap 2", "[86px][grow]"));
    
    rightInputPanel.add(lblTaxRate, "alignx left,aligny center,split 2");
    rightInputPanel.add(lblPercentage, "alignx left,aligny center");
    rightInputPanel.add(cbTaxGroup, "growx,aligny top,split 2");
    rightInputPanel.add(btnNewTax, "alignx left,aligny top");
    
    rightInputPanel.add(lblSortOrder, "alignx left,aligny center");
    rightInputPanel.add(tfSortOrder, "growx,aligny top");
    
    rightInputPanel.add(chkPrintToKitchen, "skip 1,alignx left,aligny top");
    
    chkComboModifier = new JCheckBox("Combo Modifier");
    rightInputPanel.add(chkComboModifier, "skip 1,alignx left,aligny top");
    
    generalTabPanel.add(lelfInputPanel);
    generalTabPanel.add(rightInputPanel, "East");
    
    JLabel lblButtonColor = new JLabel(Messages.getString("MenuModifierForm.1"));
    btnButtonColor = new JButton("");
    btnButtonColor.setPreferredSize(new Dimension(140, 40));
    
    JLabel lblTextColor = new JLabel(Messages.getString("MenuModifierForm.27"));
    btnTextColor = new JButton(Messages.getString("MenuModifierForm.29"));
    btnTextColor.setPreferredSize(new Dimension(140, 40));
    
    JPanel tabButtonStyle = new JPanel(new MigLayout("hidemode 3,wrap 2"));
    tabButtonStyle.add(lblButtonColor);
    tabButtonStyle.add(btnButtonColor);
    tabButtonStyle.add(lblTextColor);
    tabButtonStyle.add(btnTextColor);
    
    jTabbedPane1.addTab("Button Style", tabButtonStyle);
    
    btnButtonColor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        Color color = JColorChooser.showDialog(MenuModifierForm.this, Messages.getString("MenuModifierForm.39"), btnButtonColor.getBackground());
        btnButtonColor.setBackground(color);
        btnTextColor.setBackground(color);
      }
      
    });
    btnTextColor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        Color color = JColorChooser.showDialog(MenuModifierForm.this, Messages.getString("MenuModifierForm.40"), btnTextColor.getForeground());
        btnTextColor.setForeground(color);
      }
      
    });
    priceTable.setModel(new DefaultTableModel(new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null } }, new String[] { "Title 1", "Title 2", "Title 3", "Title 4" }));
    
    jScrollPane3.setViewportView(priceTable);
    
    tabPrice.setLayout(new BorderLayout());
    tabPrice.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    tabPrice.add(jScrollPane3, "Center");
    
    JPanel buttonPanel = new JPanel();
    
    JPanel multiplierPanel = new JPanel(new MigLayout("fillx,wrap 1"));
    List<Multiplier> multipliers = MultiplierDAO.getInstance().findAll();
    
    if (multipliers != null) {
      for (Multiplier multiplier : multipliers) {
        MultiplierPricePanel multiplierPricePanel = new MultiplierPricePanel(multiplier);
        multiplierPanel.add(multiplierPricePanel, "grow");
        itemMap.put(multiplier.getId(), multiplierPricePanel);
      }
    }
    JScrollPane scrollPane = new JScrollPane(multiplierPanel);
    scrollPane.setBorder(new TitledBorder("Multiplier price"));
    lelfInputPanel.add(scrollPane, "newline,skip 1,grow");
    
    tabPrice.add(buttonPanel, "South");
    if (TerminalConfig.isMultipleOrderSupported()) {}
  }
  

  private void checkRegularMultiplier()
  {
    Multiplier multiplier = MultiplierDAO.getInstance().get("Regular");
    if ((multiplier != null) && (multiplier.isMain().booleanValue())) {
      return;
    }
    if (multiplier == null) {
      multiplier = new Multiplier("Regular");
      multiplier.setRate(Double.valueOf(0.0D));
      multiplier.setSortOrder(Integer.valueOf(0));
      multiplier.setTicketPrefix("");
      multiplier.setDefaultMultiplier(Boolean.valueOf(true));
      multiplier.setMain(Boolean.valueOf(true));
      MultiplierDAO.getInstance().save(multiplier);
    }
    else {
      multiplier.setMain(Boolean.valueOf(true));
      MultiplierDAO.getInstance().update(multiplier);
    }
  }
  
  private void btnNewTaxActionPerformed(ActionEvent evt) {
    try {
      TaxForm editor = new TaxForm();
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      ComboBoxModel model = (ComboBoxModel)cbTaxGroup.getModel();
      model.setDataList(TaxGroupDAO.getInstance().findAll());
    } catch (Exception x) {
      MessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      MenuModifier modifier = (MenuModifier)getBean();
      ModifierDAO dao = new ModifierDAO();
      dao.saveMenuModifierFormData(modifier);
    } catch (Exception e) {
      MessageDialog.showError(POSConstants.SAVE_ERROR, e);
      return false;
    }
    return true;
  }
  
  protected void updateView()
  {
    MenuModifier modifier = (MenuModifier)getBean();
    
    if (modifier == null) {
      tfName.setText("");
      tfNormalPrice.setText("0");
      tfExtraPrice.setText("0");
      return;
    }
    
    tfName.setText(modifier.getName());
    tfTranslatedName.setText(modifier.getTranslatedName());
    tfNormalPrice.setText(String.valueOf(modifier.getPrice()));
    tfExtraPrice.setText(String.valueOf(modifier.getExtraPrice()));
    chkPrintToKitchen.setSelected(modifier.isShouldPrintToKitchen().booleanValue());
    chkComboModifier.setSelected(modifier.isComboModifier().booleanValue());
    chkUseFixedPrice.setSelected(modifier.isFixedPrice().booleanValue());
    
    if (modifier.getSortOrder() != null) {
      tfSortOrder.setText(modifier.getSortOrder().toString());
    }
    
    if (modifier.getButtonColor() != null) {
      Color color = new Color(modifier.getButtonColor().intValue());
      btnButtonColor.setBackground(color);
      btnTextColor.setBackground(color);
    }
    
    if (modifier.getTextColor() != null) {
      Color color = new Color(modifier.getTextColor().intValue());
      btnTextColor.setForeground(color);
    }
    
    if (modifier.getTaxGroup() != null) {
      cbTaxGroup.setSelectedItem(modifier.getTaxGroup());
    }
    List<ModifierMultiplierPrice> multiplierPriceList = modifier.getMultiplierPriceList();
    if (multiplierPriceList != null) {
      for (ModifierMultiplierPrice multiplierPrice : multiplierPriceList) {
        MultiplierPricePanel pricePanel = (MultiplierPricePanel)itemMap.get(multiplierPrice.getMultiplier().getId());
        if (pricePanel != null)
        {
          pricePanel.setModifierMultiplierPrice(multiplierPrice); }
      }
    }
    itemMap.get("Regular")).tfAditionalPrice.setText(String.valueOf(modifier.getPrice()));
  }
  
  protected boolean updateModel()
  {
    MenuModifier modifier = (MenuModifier)getBean();
    
    String name = tfName.getText();
    if (POSUtil.isBlankOrNull(name)) {
      MessageDialog.showError(Messages.getString("MenuModifierForm.44"));
      return false;
    }
    
    modifier.setName(name);
    modifier.setExtraPrice(Double.valueOf(tfExtraPrice.getDouble()));
    modifier.setTaxGroup((TaxGroup)cbTaxGroup.getSelectedItem());
    modifier.setShouldPrintToKitchen(Boolean.valueOf(chkPrintToKitchen.isSelected()));
    modifier.setComboModifier(Boolean.valueOf(chkComboModifier.isSelected()));
    
    modifier.setTranslatedName(tfTranslatedName.getText());
    modifier.setButtonColor(Integer.valueOf(btnButtonColor.getBackground().getRGB()));
    modifier.setTextColor(Integer.valueOf(btnTextColor.getForeground().getRGB()));
    modifier.setSortOrder(Integer.valueOf(tfSortOrder.getInteger()));
    modifier.setFixedPrice(Boolean.valueOf(chkUseFixedPrice.isSelected()));
    
    List<ModifierMultiplierPrice> mulplierPriceList = new ArrayList();
    for (MultiplierPricePanel panel : itemMap.values()) {
      if (panel.isSelected()) {
        ModifierMultiplierPrice multiplierPrice = panel.getMultiplierPrice();
        if (multiplierPrice == null) {
          multiplierPrice = new ModifierMultiplierPrice();
          multiplierPrice.setMultiplier(panel.getMultiplier());
          multiplierPrice.setModifierId(modifier.getId());
        }
        multiplierPrice.setPrice(panel.getPrice());
        mulplierPriceList.add(multiplierPrice);
      }
    }
    modifier.setPrice(((MultiplierPricePanel)itemMap.get("Regular")).getPrice());
    modifier.setMultiplierPriceList(mulplierPriceList);
    return true;
  }
  
  public String getDisplayText() {
    MenuModifier modifier = (MenuModifier)getBean();
    if (modifier.getId() == null) {
      return Messages.getString("MenuModifierForm.45");
    }
    return Messages.getString("MenuModifierForm.46");
  }
  
  protected void doCalculateMultiplierPrice() {
    MultiplierPricePanel regularPricePanel = (MultiplierPricePanel)itemMap.get("Regular");
    if (regularPricePanel == null)
      return;
    for (MultiplierPricePanel panel : itemMap.values()) {
      panel.calculatePrice(getDoubleOrZero(tfAditionalPrice.getText()));
    }
  }
  
  public double getDouble(String text) {
    try {
      return Double.parseDouble(text);
    } catch (Exception e) {}
    return NaN.0D;
  }
  
  public double getDoubleOrZero(String text)
  {
    try {
      return Double.parseDouble(text);
    } catch (Exception e) {}
    return 0.0D;
  }
  
  private class MultiplierPricePanel extends JPanel
  {
    ModifierMultiplierPrice multiplierPrice;
    Multiplier multiplier;
    JTextField tfAditionalPrice;
    
    public MultiplierPricePanel(Multiplier multiplier) {
      this.multiplier = multiplier;
      setLayout(new MigLayout("inset 0,fillx", "[100px][grow][100px]", ""));
      
      tfAditionalPrice = new JTextField(multiplier.isMain().booleanValue() ? 6 : 9);
      tfAditionalPrice.setHorizontalAlignment(4);
      JLabel lblName = new JLabel(multiplier.getId());
      if (multiplier.isMain().booleanValue()) {
        lblName.setFont(new Font(null, 1, tfName.getFont().getSize()));
      }
      add(lblName);
      add(new JLabel(multiplier.isMain().booleanValue() ? "Price" : "Additional price", 11), "grow, gapright 10px");
      add(tfAditionalPrice, "split 2,grow");
      if (multiplier.isMain().booleanValue()) {
        JButton btnCalculateMultilierPrice = new JButton("Calc");
        btnCalculateMultilierPrice.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            doCalculateMultiplierPrice();
          }
        });
        add(btnCalculateMultilierPrice);
      }
    }
    
    public void calculatePrice(double regPrice) {
      if (multiplier.isMain().booleanValue())
        return;
      tfAditionalPrice.setText(String.valueOf(regPrice * multiplier.getRate().doubleValue() / 100.0D));
    }
    
    public Double getPrice() {
      return Double.valueOf(getDoubleOrZero(tfAditionalPrice.getText()));
    }
    
    public Multiplier getMultiplier() {
      return multiplier;
    }
    
    public boolean isSelected() {
      Double value = Double.valueOf(getDouble(tfAditionalPrice.getText()));
      return !value.isNaN();
    }
    
    private void update() {
      if (multiplierPrice == null)
        return;
      tfAditionalPrice.setText(String.valueOf(multiplierPrice.getPrice()));
    }
    
    public void setModifierMultiplierPrice(ModifierMultiplierPrice price) {
      multiplierPrice = price;
      update();
    }
    
    public ModifierMultiplierPrice getMultiplierPrice() {
      return multiplierPrice;
    }
  }
}
