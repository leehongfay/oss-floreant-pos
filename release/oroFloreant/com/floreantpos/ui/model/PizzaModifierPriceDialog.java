package com.floreantpos.ui.model;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.MenuItemSize;
import com.floreantpos.model.PizzaModifierPrice;
import com.floreantpos.model.dao.MenuItemSizeDAO;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.combobox.ListComboBoxModel;




public class PizzaModifierPriceDialog
  extends POSDialog
{
  private JPanel contentPane;
  private JButton btnOK;
  private JButton btnCancel;
  private JComboBox cbSize;
  private DoubleTextField tfPrice;
  private DoubleTextField tfExtraPrice;
  private PizzaModifierPrice modifierPrice;
  private final List<PizzaModifierPrice> existingPriceList;
  
  public PizzaModifierPriceDialog(Frame owner, PizzaModifierPrice modifierPrice, List<PizzaModifierPrice> existingPriceList)
  {
    super(owner, true);
    this.modifierPrice = modifierPrice;
    this.existingPriceList = existingPriceList;
    
    init();
    updateView();
  }
  
  private void init() {
    createView();
    
    btnOK.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        PizzaModifierPriceDialog.this.onOK();
      }
      
    });
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        PizzaModifierPriceDialog.this.onCancel();
      }
      

    });
    setDefaultCloseOperation(0);
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        PizzaModifierPriceDialog.this.onCancel();
      }
      

    });
    contentPane.registerKeyboardAction(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        PizzaModifierPriceDialog.this.onCancel();
      }
    }, KeyStroke.getKeyStroke(27, 0), 1);
  }
  
  private void onOK()
  {
    if (!updateModel()) {
      return;
    }
    try {
      setCanceled(false);
      dispose();
    } catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  private void onCancel() {
    setCanceled(true);
    dispose();
  }
  
  private void updateView() {
    if (modifierPrice == null) {
      return;
    }
    cbSize.setSelectedItem(modifierPrice.getSize());
    tfPrice.setText(String.valueOf(modifierPrice.getPrice()));
    tfExtraPrice.setText(String.valueOf(modifierPrice.getExtraPrice()));
  }
  
  public boolean updateModel() {
    double price = tfPrice.getDoubleOrZero();
    double extraPrice = tfExtraPrice.getDoubleOrZero();
    
    MenuItemSize selectedSize = (MenuItemSize)cbSize.getSelectedItem();
    
    if (selectedSize == null) {
      POSMessageDialog.showError(this, Messages.getString("PizzaModifierPriceDialog.0"));
      return false;
    }
    
    if (existingPriceList != null) {
      for (PizzaModifierPrice mp : existingPriceList) {
        if ((selectedSize.equals(mp.getSize())) && (mp != modifierPrice)) {
          POSMessageDialog.showError(this, Messages.getString("PizzaModifierPriceDialog.1"));
          return false;
        }
      }
    }
    

    if (modifierPrice == null) {
      modifierPrice = new PizzaModifierPrice();
    }
    
    modifierPrice.setSize(selectedSize);
    modifierPrice.setPrice(price);
    modifierPrice.setExtraPrice(extraPrice);
    
    return true;
  }
  
  public PizzaModifierPrice getModifierPrice()
  {
    return modifierPrice;
  }
  
  private void createView() {
    contentPane = new JPanel(new BorderLayout());
    
    List<MenuItemSize> menuItemSizeList = MenuItemSizeDAO.getInstance().findAll();
    JLabel label3 = new JLabel();
    label3.setText("Size:");
    cbSize = new JComboBox(new ListComboBoxModel(menuItemSizeList));
    
    JLabel label2 = new JLabel();
    label2.setText(POSConstants.PRICE + ":");
    tfPrice = new DoubleTextField();
    
    JLabel lblExtraPrice = new JLabel();
    lblExtraPrice.setText(Messages.getString("PizzaModifierPriceDialog.2"));
    tfExtraPrice = new DoubleTextField();
    
    JPanel panel = new JPanel(new MigLayout("", "[][fill, grow]", ""));
    
    panel.add(label3, "left");
    panel.add(cbSize, "grow,wrap");
    panel.add(label2, "left");
    panel.add(tfPrice, "wrap");
    panel.add(lblExtraPrice, "left");
    panel.add(tfExtraPrice, "grow,wrap");
    
    contentPane.add(panel, "Center");
    
    JPanel buttonPanel = new JPanel(new MigLayout("al center center", "sg", ""));
    btnOK = new JButton(Messages.getString("ModifierPriceByOrderTypeDialog.0"));
    btnCancel = new JButton(Messages.getString("ModifierPriceByOrderTypeDialog.19"));
    
    buttonPanel.add(btnOK, "grow");
    buttonPanel.add(btnCancel, "grow");
    contentPane.add(buttonPanel, "South");
    add(contentPane);
  }
}
