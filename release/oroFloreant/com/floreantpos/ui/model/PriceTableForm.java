package com.floreantpos.ui.model;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.main.Application;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.PriceTable;
import com.floreantpos.model.PriceTableItem;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.PriceTableItemDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthDocument;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PaginatedListModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.dialog.MenuItemSelectionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;


















public class PriceTableForm
  extends BeanEditor<PriceTable>
  implements ActionListener
{
  private JTable priceTableItemTable;
  private PriceTableItemExplorerTableModel priceTableItemModel;
  private FixedLengthTextField tfName = new FixedLengthTextField();
  private JTextArea tfDescription = new JTextArea();
  
  private JButton addButton;
  private JButton deleteButton;
  private boolean viewMode;
  private JLabel lblNumberOfItem = new JLabel();
  private PosButton btnNext;
  private PosButton btnPrev;
  protected PaginatedListModel dataModel = new PaginatedListModel();
  private boolean edit = false;
  
  public PriceTableForm(PriceTable priceTable) {
    initComponents();
    
    priceTableItemModel = new PriceTableItemExplorerTableModel();
    priceTableItemTable.setModel(priceTableItemModel);
    setBean(priceTable);
    resizeTableColumns();
    priceTableItemTable.getInputMap().put(KeyStroke.getKeyStroke(32, 0), "startEditing");
    
    DoubleTextField tfPrice = new DoubleTextField();
    tfPrice.setAllowNegativeValue(true);
    tfPrice.setHorizontalAlignment(4);
    DefaultCellEditor editorPrice = new DefaultCellEditor(tfPrice);
    editorPrice.setClickCountToStart(1);
    
    priceTableItemTable.setDefaultEditor(priceTableItemTable.getColumnClass(4), editorPrice);
  }
  
  private void initComponents() {
    setLayout(new MigLayout("fill", "[][grow]", ""));
    setBorder(new EmptyBorder(10, 10, 10, 10));
    
    priceTableItemTable = new JTable() {
      public void changeSelection(int row, int column, boolean toggle, boolean extend) {
        super.changeSelection(row, column, toggle, extend);
        if (column != 4)
          return;
        priceTableItemTable.editCellAt(row, column);
        priceTableItemTable.transferFocus();
        DefaultCellEditor editor = (DefaultCellEditor)priceTableItemTable.getCellEditor(row, column);
        if (column == 4) {
          DoubleTextField textField = (DoubleTextField)editor.getComponent();
          textField.requestFocus();
          textField.selectAll();
          edit = true;
        }
      }
    };
    priceTableItemTable.setRowHeight(PosUIManager.getSize(30));
    
    tfName.setLength(30);
    
    add(new JLabel("Name"));
    add(tfName, "growx,wrap");
    
    tfDescription.setLineWrap(true);
    tfDescription.setDocument(new FixedLengthDocument(255));
    
    add(new JLabel("Description"), "growx,aligny top");
    JScrollPane scrollPane = new JScrollPane(tfDescription);
    add(scrollPane, "growx,h 70!,top,wrap");
    
    add(new JScrollPane(priceTableItemTable), "span,grow");
    addButtonPanel();
  }
  
  private void scrollDown() {
    PriceTable priceTable = (PriceTable)getBean();
    if (edit) {
      PriceTableItemDAO.getInstance().saveOrUpdateItems(priceTable, dataModel.getDataList());
      edit = false;
    }
    dataModel.setCurrentRowIndex(dataModel.getNextRowIndex());
    updateData();
  }
  
  private void updateData() {
    PriceTable priceTable = (PriceTable)getBean();
    dataModel.setNumRows(PriceTableItemDAO.getInstance().rowCount(priceTable));
    PriceTableItemDAO.getInstance().loadItems(priceTable, dataModel);
    priceTableItemModel.setItems(dataModel.getDataList());
    updateButton();
    priceTableItemTable.revalidate();
    priceTableItemTable.repaint();
  }
  
  private void scrollUp() {
    PriceTable priceTable = (PriceTable)getBean();
    if (edit) {
      PriceTableItemDAO.getInstance().saveOrUpdateItems(priceTable, dataModel.getDataList());
      edit = false;
    }
    dataModel.setCurrentRowIndex(dataModel.getPreviousRowIndex());
    updateData();
  }
  
  private void updateButton() {
    int startNumber = dataModel.getCurrentRowIndex() + 1;
    int endNumber = dataModel.getNextRowIndex();
    int totalNumber = dataModel.getNumRows();
    if (endNumber > totalNumber) {
      endNumber = totalNumber;
    }
    lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
    btnPrev.setEnabled(dataModel.hasPrevious());
    btnNext.setEnabled(dataModel.hasNext());
  }
  
  private void addButtonPanel() {
    addButton = new JButton("Add Item");
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          if (!updateModel()) {
            return;
          }
          MenuItemSelectionDialog dialog = new MenuItemSelectionDialog(new ArrayList());
          dialog.setSelectionMode(1);
          dialog.setSize(PosUIManager.getSize(600, 515));
          dialog.open();
          if (dialog.isCanceled()) {
            return;
          }
          List<PriceTableItem> priceTableItems = new ArrayList();
          PriceTable priceTable = (PriceTable)getBean();
          List<MenuItem> menuItemList = dialog.getSelectedItems();
          for (Iterator iterator = menuItemList.iterator(); iterator.hasNext();) {
            MenuItem menuItem = (MenuItem)iterator.next();
            PriceTableItem existingItem = null;
            if (priceTable.getId() != null) {
              existingItem = PriceTableItemDAO.getInstance().getItem(priceTable, menuItem);
            }
            if (existingItem == null) {
              PriceTableItem item = new PriceTableItem();
              item.setMenuItem(menuItem);
              item.setMenuItemId(menuItem.getId());
              item.setItemBarcode(menuItem.getBarcode());
              item.setPrice(menuItem.getPrice());
              priceTableItems.add(item);
            }
          }
          PriceTableItemDAO.getInstance().saveOrUpdateItems(priceTable, priceTableItems);
          dataModel.setNumRows(PriceTableItemDAO.getInstance().rowCount(priceTable));
          dataModel.setCurrentRowIndex(0);
          PriceTableItemDAO.getInstance().loadItems(priceTable, dataModel);
          PriceTableForm.this.updateButton();
          priceTableItemModel.setItems(dataModel.getDataList());
          priceTableItemTable.revalidate();
          priceTableItemTable.repaint();
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = priceTableItemTable.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = priceTableItemTable.convertRowIndexToModel(index);
          PriceTableItem priceTableItem = priceTableItemModel.getPriceTableItem(index);
          PriceTableItemDAO.getInstance().delete(priceTableItem);
          dataModel.setNumRows(dataModel.getNumRows() - 1);
          dataModel.removeElement(priceTableItem);
          priceTableItemModel.deleteItem(priceTableItem, index);
          PriceTableForm.this.updateButton();
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    dataModel.setPageSize(20);
    
    JPanel paginationButtonPanel = new JPanel(new MigLayout("ins 5 0 0 0,fillx", "[left,grow][]", ""));
    
    paginationButtonPanel.add(addButton, "split 2,top");
    paginationButtonPanel.add(deleteButton, "top");
    
    paginationButtonPanel.add(lblNumberOfItem, "split 3,span");
    
    btnPrev = new PosButton();
    btnPrev.setIcon(IconFactory.getIcon("/ui_icons/", "previous.png"));
    paginationButtonPanel.add(btnPrev, "center");
    
    btnNext = new PosButton();
    btnNext.setIcon(IconFactory.getIcon("/ui_icons/", "next.png"));
    paginationButtonPanel.add(btnNext);
    
    add(paginationButtonPanel, "growx,span 2");
    
    ActionListener action = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          Object source = e.getSource();
          if (source == btnPrev) {
            PriceTableForm.this.scrollUp();
          }
          else if (source == btnNext) {
            PriceTableForm.this.scrollDown();
          }
        } catch (Exception e2) {
          POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e2.getMessage(), e2);
        }
        
      }
    };
    btnPrev.addActionListener(action);
    btnNext.addActionListener(action);
    
    btnNext.setEnabled(false);
    btnPrev.setEnabled(false);
  }
  
  private void resizeTableColumns()
  {
    priceTableItemTable.setAutoResizeMode(4);
    setColumnWidth(0, PosUIManager.getSize(100));
    setColumnWidth(1, PosUIManager.getSize(100));
    setColumnWidth(3, PosUIManager.getSize(100));
    setColumnWidth(4, PosUIManager.getSize(100));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = priceTableItemTable.getColumnModel().getColumn(columnNumber);
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  public void actionPerformed(ActionEvent e)
  {
    String actionCommand = e.getActionCommand();
    if (POSConstants.DELETE.equals(actionCommand)) {
      int index = priceTableItemTable.getSelectedRow();
      if (index < 0) {
        BOMessageDialog.showError(POSConstants.SELECT_ITEM_TO_DELETE);
        return;
      }
      PriceTableItem menuItem = priceTableItemModel.getPriceTableItem(index);
      deletePriceTableItem(index, menuItem);
    }
  }
  
  private void deletePriceTableItem(int index, PriceTableItem priceTableItem) {
    try {
      if (ConfirmDeleteDialog.showMessage(this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0) {
        priceTableItemModel.deleteItem(priceTableItem, index);
      }
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private class PriceTableItemExplorerTableModel extends ListTableModel<PriceTableItem> {
    String[] columnNames = { "ID", "BARCODE", "NAME", "DEFAULT PRICE", "NEW PRICE" };
    List<PriceTableItem> priceTableItemList;
    
    public PriceTableItemExplorerTableModel() {
      priceTableItemList = new ArrayList();
    }
    
    public void setItems(List<PriceTableItem> priceTableItems) {
      if (priceTableItems == null)
        return;
      priceTableItemList.clear();
      priceTableItemList.addAll(priceTableItems);
    }
    
    public List<PriceTableItem> getItems() {
      return priceTableItemList;
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      if (columnIndex == 4)
        return true;
      return false;
    }
    
    public int getRowCount()
    {
      if (priceTableItemList == null) {
        return 0;
      }
      return priceTableItemList.size();
    }
    
    public int getColumnCount()
    {
      return columnNames.length;
    }
    
    public String getColumnName(int index)
    {
      return columnNames[index];
    }
    
    public Class<?> getColumnClass(int columnIndex)
    {
      if ((columnIndex == 4) || (columnIndex == 3)) {
        return Double.class;
      }
      return String.class;
    }
    
    public Object getValueAt(int row, int column)
    {
      if (priceTableItemList == null) {
        return "";
      }
      PriceTableItem priceTableItem = (PriceTableItem)priceTableItemList.get(row);
      if (priceTableItem == null) {
        return "";
      }
      
      switch (column) {
      case 0: 
        return priceTableItem.getMenuItem().getId();
      case 1: 
        return priceTableItem.getMenuItem().getBarcode();
      case 2: 
        return priceTableItem.getMenuItem().getDisplayName();
      case 3: 
        return priceTableItem.getMenuItem().getPrice();
      case 4: 
        return priceTableItem.getPrice();
      }
      return null;
    }
    
    public void setValueAt(Object aValue, int rowIndex, int columnIndex)
    {
      PriceTableItem priceTableItem = (PriceTableItem)priceTableItemList.get(rowIndex);
      if (priceTableItem == null) {
        return;
      }
      if (columnIndex == 4) {
        String priceString = (String)aValue;
        if (priceString.isEmpty())
          return;
        double price = Double.parseDouble(priceString);
        priceTableItem.setPrice(Double.valueOf(price));
      }
      else {
        super.setValueAt(aValue, rowIndex, columnIndex);
      }
    }
    
    public void deleteItem(PriceTableItem priceTableItem, int index) { for (Iterator iterator = priceTableItemList.iterator(); iterator.hasNext();) {
        PriceTableItem item = (PriceTableItem)iterator.next();
        if (priceTableItem == item) {
          iterator.remove();
        }
      }
      fireTableRowsDeleted(index, index);
    }
    
    public PriceTableItem getPriceTableItem(int index) {
      return (PriceTableItem)priceTableItemList.get(index);
    }
  }
  
  public boolean save()
  {
    try {
      if (!updateModel())
        return false;
      PriceTable priceTable = (PriceTable)getBean();
      PriceTableItemDAO.getInstance().saveOrUpdateItems(priceTable, dataModel.getDataList());
      return true;
    } catch (IllegalModelStateException e) {
      e.printStackTrace();
    }
    return false;
  }
  
  protected void updateView()
  {
    PriceTable priceTable = (PriceTable)getBean();
    tfName.setText(priceTable.getName());
    tfDescription.setText(priceTable.getDescription());
    
    if (priceTable.getId() == null) {
      return;
    }
    dataModel.setCurrentRowIndex(0);
    updateData();
  }
  
  protected boolean updateModel() throws IllegalModelStateException
  {
    PriceTable priceTable = (PriceTable)getBean();
    String name = tfName.getText();
    if (StringUtils.isEmpty(name)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Name cannot be empty.");
      return false;
    }
    priceTable.setName(name);
    priceTable.setDescription(tfDescription.getText());
    priceTable.setLastUpdatedTime(new Date());
    priceTable.setLastUpdatedBy(Application.getCurrentUser().getId());
    return true;
  }
  
  public String getDisplayText()
  {
    if (viewMode) {
      return "Price List";
    }
    if (((PriceTable)getBean()).getId() == null) {
      return "New Price List";
    }
    return "Edit Price List";
  }
  
  public void setEditable(boolean b) {
    viewMode = (!b);
    if (viewMode) {
      addButton.setVisible(false);
      deleteButton.setVisible(false);
      tfDescription.setEnabled(false);
      tfName.setEnabled(false);
      priceTableItemTable.setEnabled(false);
    }
  }
}
