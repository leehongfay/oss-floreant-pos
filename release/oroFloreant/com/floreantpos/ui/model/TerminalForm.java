package com.floreantpos.ui.model;

import com.floreantpos.Messages;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.ui.BeanEditor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import net.miginfocom.swing.MigLayout;



























public class TerminalForm
  extends BeanEditor
{
  private int terminalId;
  private JButton btnTerminalIDGenerate;
  private JLabel lblTerminalName;
  private POSTextField tfTerminalName;
  private JLabel lblOpeningBlnc;
  private DoubleTextField tfOpeningBlnc;
  private JLabel lblCurrentBlnc;
  private DoubleTextField tfCurrentBlnc;
  private JLabel lblLocation;
  private POSTextField tfLocation;
  private JCheckBox chbHasCashDrawer;
  private JLabel lblTerminalNumber;
  private FixedLengthTextField tfTerminalNumber;
  
  public TerminalForm()
  {
    initComponents();
    setBean(new Terminal());
  }
  
  public TerminalForm(Terminal term) {
    initComponents();
    setBean(term);
  }
  
  private void createTerminal() {
    Random random = new Random();
    terminalId = (random.nextInt(10000) + 1);
    tfTerminalNumber.setText(String.valueOf(terminalId));
  }
  
  public int getTerminalId()
  {
    return Integer.valueOf(tfTerminalNumber.getText()).intValue();
  }
  





  private void initComponents()
  {
    setLayout(new MigLayout("", "[70px][289px,grow][6px][49px]", ""));
    setPreferredSize(new Dimension(600, 250));
    lblTerminalNumber = new JLabel();
    tfTerminalNumber = new FixedLengthTextField(120);
    
    lblTerminalNumber.setText("Terminal Number:");
    
    btnTerminalIDGenerate = new JButton(Messages.getString("TerminalForm.0"));
    
    lblTerminalName = new JLabel(Messages.getString("TerminalForm.1"));
    tfTerminalName = new POSTextField();
    
    lblOpeningBlnc = new JLabel(Messages.getString("TerminalForm.2"));
    tfOpeningBlnc = new DoubleTextField();
    
    lblCurrentBlnc = new JLabel(Messages.getString("TerminalForm.3"));
    tfCurrentBlnc = new DoubleTextField();
    
    lblLocation = new JLabel(Messages.getString("TerminalForm.4"));
    tfLocation = new POSTextField();
    

    chbHasCashDrawer = new JCheckBox(Messages.getString("TerminalForm.5"));
    
    add(lblTerminalNumber, "cell 0 1,alignx right");
    add(tfTerminalNumber, "cell 1 1,growx");
    add(btnTerminalIDGenerate, "cell 2 1,growx");
    
    add(lblTerminalName, "cell 0 2,alignx right");
    add(tfTerminalName, "cell 1 2,growx");
    
    add(lblOpeningBlnc, "cell 0 3,alignx right");
    add(tfOpeningBlnc, "cell 1 3,growx");
    
    add(lblCurrentBlnc, "cell 0 4,alignx right");
    add(tfCurrentBlnc, "cell 1 4,growx");
    
    add(lblLocation, "cell 0 5,alignx right");
    add(tfLocation, "cell 1 5,growx");
    
    add(chbHasCashDrawer, "cell 1 6,growx");
    
    btnTerminalIDGenerate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TerminalForm.this.createTerminal();
      }
    });
  }
  







  public boolean save()
  {
    if (!updateModel()) {
      return false;
    }
    Terminal term = (Terminal)getBean();
    try
    {
      TerminalDAO termDAO = new TerminalDAO();
      termDAO.saveOrUpdate(term);
    } catch (Exception e) {
      MessageDialog.showError(e);
      return false;
    }
    return true;
  }
  


  protected void updateView() {}
  

  protected boolean updateModel()
  {
    Terminal termModel = (Terminal)getBean();
    termModel.setName(tfTerminalNumber.getText());
    termModel.setId(Integer.valueOf(tfTerminalNumber.getText()));
    return true;
  }
  
  public String getDisplayText()
  {
    return "Terminal Generate";
  }
}
