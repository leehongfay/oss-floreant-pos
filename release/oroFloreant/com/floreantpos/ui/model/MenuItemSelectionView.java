package com.floreantpos.ui.model;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.explorer.ExplorerButtonPanel;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.PaginatedTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosTable;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;



















public class MenuItemSelectionView
  extends JPanel
{
  private JComboBox cbGroup;
  private PosTable table;
  private MenuItemTableModel tableModel;
  private JTextField tfName;
  private MenuItem parentMenuItem;
  private JButton btnNext;
  private JButton btnPrev;
  private MenuGroup selectedGroup;
  private JLabel lblNumberOfItem = new JLabel();
  private JLabel lblName;
  private JButton btnSearch;
  private JPanel searchPanel;
  private Map<String, MenuItem> addedMenuItemMap = new HashMap();
  
  private JCheckBox chkShowSelected;
  private JCheckBox chkSelectAll;
  private JLabel lblGroup;
  private boolean inventoryItemOnly;
  public static final int SINGLE_SELECTION = 0;
  public static final int MULTIPLE_SELECTION = 1;
  
  public MenuItemSelectionView(List<MenuItem> addedMenuItems, boolean inventoryItemOnly)
  {
    this.inventoryItemOnly = inventoryItemOnly;
    initComponents();
    tableModel.setCurrentRowIndex(0);
    cbGroup.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        tableModel.setCurrentRowIndex(0);
        setSelectedMenuGroup(cbGroup.getSelectedItem());
      }
    });
    setSelectedItems(addedMenuItems);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    tableModel = new MenuItemTableModel();
    tableModel.setColumnNames(new String[] { "-", "Item name", "Item price" });
    tableModel.setPageSize(10);
    table = new PosTable();
    table.setModel(tableModel);
    table.setSelectionMode(2);
    table.setRowHeight(PosUIManager.getSize(40));
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          MenuItemSelectionView.this.editSelectedRow();
        }
        else {
          MenuItemSelectionView.this.selectItem();
        }
        
      }
    });
    JPanel contentPanel = new JPanel(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(10, 5, 10, 5));
    JScrollPane scroll = new JScrollPane(table);
    scroll.setPreferredSize(PosUIManager.getSize(500, 250));
    contentPanel.add(scroll);
    contentPanel.add(buildSearchForm(), "North");
    
    add(contentPanel);
    resizeColumnWidth(table);
    
    JPanel paginationButtonPanel = new JPanel(new MigLayout("ins 5 0 0 0,fillx", "[left,grow][][][]", ""));
    paginationButtonPanel.add(createButtonPanel(), "left,split 2");
    
    chkShowSelected = new JCheckBox("Show selected");
    chkShowSelected.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuItemSelectionView.this.updateView();
      }
    });
    paginationButtonPanel.add(chkShowSelected);
    paginationButtonPanel.add(lblNumberOfItem, "split 3,center");
    
    btnPrev = new JButton("Previous");
    paginationButtonPanel.add(btnPrev, "center");
    
    PosButton btnDot = new PosButton();
    btnDot.setBorder(null);
    btnDot.setOpaque(false);
    btnDot.setContentAreaFilled(false);
    btnDot.setIcon(IconFactory.getIcon("/ui_icons/", "dot.png"));
    
    btnNext = new JButton("Next");
    paginationButtonPanel.add(btnNext);
    paginationButtonPanel.add(new JSeparator(), "newline,span,grow");
    
    contentPanel.add(paginationButtonPanel, "South");
    
    ActionListener action = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          Object source = e.getSource();
          if (source == btnPrev) {
            MenuItemSelectionView.this.scrollUp();
          }
          else if (source == btnNext) {
            MenuItemSelectionView.this.scrollDown();
          }
        } catch (Exception e2) {
          POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e2.getMessage(), e2);
        }
        
      }
    };
    btnPrev.addActionListener(action);
    btnNext.addActionListener(action);
    
    btnNext.setEnabled(false);
    btnPrev.setEnabled(false);
  }
  
  private void updateView() {
    if (chkShowSelected.isSelected()) {
      tableModel.setRows(new ArrayList(addedMenuItemMap.values()));
      chkShowSelected.setText("Show Selected (" + addedMenuItemMap.values().size() + ")");
      lblNumberOfItem.setText("");
      btnPrev.setEnabled(false);
      btnNext.setEnabled(false);
      cbGroup.setEnabled(false);
      table.repaint();
    }
    else {
      if (cbGroup.getSelectedItem() != null) {
        searchItem();
      }
      cbGroup.setEnabled(true);
    }
  }
  
  private JPanel buildSearchForm() {
    searchPanel = new JPanel();
    searchPanel.setLayout(new MigLayout("inset 0,fillx,hidemode 3", "", "[]10[]"));
    lblName = new JLabel(POSConstants.NAME + " / Barcode: ");
    tfName = new JTextField(15);
    btnSearch = new JButton(POSConstants.SEARCH_ITEM_BUTTON_TEXT);
    searchPanel.add(lblName, "align label,split 5");
    searchPanel.add(tfName, "growx");
    
    chkSelectAll = new JCheckBox("Select All");
    chkSelectAll.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuItemSelectionView.this.selectGroupItems();
      }
      
    });
    cbGroup = new JComboBox();
    List groups = new ArrayList();
    groups.add("<All>");
    
    List<MenuGroup> menuGroupList = MenuGroupDAO.getInstance().findAll();
    groups.addAll(menuGroupList);
    
    ComboBoxModel model = new ComboBoxModel(groups);
    cbGroup.setModel(model);
    cbGroup.setSelectedItem("<All>");
    cbGroup.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        MenuItemSelectionView.this.searchItem();
      }
    });
    searchPanel.add(btnSearch);
    lblGroup = new JLabel("Group");
    searchPanel.add(lblGroup, "split 2,right");
    searchPanel.add(cbGroup, "wrap");
    searchPanel.add(chkSelectAll, "left, split 2");
    
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        MenuItemSelectionView.this.searchItem();
      }
      
    });
    tfName.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuItemSelectionView.this.searchItem();
      }
    });
    return searchPanel;
  }
  
  private void searchItem() {
    try {
      if ((cbGroup.getSelectedItem() instanceof MenuGroup)) {
        selectedGroup = ((MenuGroup)cbGroup.getSelectedItem());
      }
      else {
        selectedGroup = null;
      }
      tableModel.setCurrentRowIndex(0);
      
      MenuItemDAO.getInstance().findByBarcodeOrName(tableModel, Boolean.valueOf(isInventoryItemOnly()), selectedGroup, tfName.getText(), null, null, new String[0]);
      
      updateButton();
      tableModel.fireTableDataChanged();
      table.repaint();
      chkShowSelected.setSelected(false);
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    }
  }
  
  private void selectGroupItems() {
    try {
      List<MenuItem> menuItems = tableModel.getRows();
      if (menuItems == null)
        return;
      for (MenuItem item : menuItems)
        if ((parentMenuItem == null) || (parentMenuItem.getId() == null) || (!parentMenuItem.getId().equals(item.getId())))
        {

          if (chkSelectAll.isSelected()) {
            if (!addedMenuItemMap.containsKey(item.getId()))
              addedMenuItemMap.put(item.getId(), item);
          } else {
            addedMenuItemMap.clear();
          }
          chkShowSelected.setText("Show Selected (" + addedMenuItemMap.values().size() + ")");
          table.repaint();
        }
      table.repaint();
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    }
  }
  
  private void updateButton() {
    int startNumber = tableModel.getCurrentRowIndex() + 1;
    int endNumber = tableModel.getNextRowIndex();
    int totalNumber = tableModel.getNumRows();
    if (endNumber > totalNumber) {
      endNumber = totalNumber;
    }
    lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
    btnPrev.setEnabled(tableModel.hasPrevious());
    btnNext.setEnabled(tableModel.hasNext());
    
    if (tableModel.getRowCount() > 0) {
      table.setRowSelectionInterval(0, 0);
    }
    chkShowSelected.setText("Show Selected (" + addedMenuItemMap.values().size() + ")");
  }
  
  private TransparentPanel createButtonPanel() {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton btnEdit = explorerButton.getEditButton();
    JButton btnAdd = explorerButton.getAddButton();
    btnAdd.setText(POSConstants.ADD);
    btnEdit.setText(POSConstants.EDIT);
    
    btnEdit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MenuItemSelectionView.this.editSelectedRow();
      }
    });
    btnAdd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MenuItemSelectionView.this.doAddItem();
      }
      

    });
    TransparentPanel panel = new TransparentPanel(new MigLayout("center,ins 0", "sg,fill", ""));
    return panel;
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(250));
    columnWidth.add(Integer.valueOf(70));
    
    return columnWidth;
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0)
        return;
      index = table.convertRowIndexToModel(index);
      
      MenuItem menuItem = (MenuItem)tableModel.getRowData(index);
      MenuItemDAO.getInstance().initialize(menuItem);
      tableModel.addItem(menuItem);
      
      BeanEditor<MenuItem> editor = null;
      if (menuItem.isPizzaType().booleanValue()) {
        editor = new PizzaItemForm(menuItem);
      }
      else {
        editor = new MenuItemForm(menuItem);
      }
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public List<MenuItem> getSelectedMenuItemList() {
    return new ArrayList(addedMenuItemMap.values());
  }
  
  public void setSelectedItems(List<MenuItem> menuItems) {
    if (menuItems != null) {
      for (MenuItem item : menuItems) {
        addedMenuItemMap.put(item.getId(), item);
      }
    }
  }
  
  public void setParentMenuItem(MenuItem selectedMenuItem, boolean editMode) {
    parentMenuItem = selectedMenuItem;
    if (editMode) {
      chkShowSelected.setSelected(true);
      updateView();
    }
    else {
      searchItem();
    }
  }
  
  private void scrollDown() { tableModel.setCurrentRowIndex(tableModel.getNextRowIndex());
    MenuItemDAO.getInstance().findByBarcodeOrName(tableModel, Boolean.valueOf(isInventoryItemOnly()), selectedGroup, tfName.getText(), null, null, new String[0]);
    updateButton();
    table.repaint();
    chkShowSelected.setSelected(false);
  }
  
  private void scrollUp() {
    tableModel.setCurrentRowIndex(tableModel.getPreviousRowIndex());
    MenuItemDAO.getInstance().findByBarcodeOrName(tableModel, Boolean.valueOf(isInventoryItemOnly()), selectedGroup, tfName.getText(), null, null, new String[0]);
    updateButton();
    table.repaint();
    chkShowSelected.setSelected(false);
  }
  
  public void setSelectedMenuGroup(Object selectedItem) {
    if ((selectedItem instanceof MenuGroup)) {
      selectedGroup = ((MenuGroup)selectedItem);
    }
    else {
      selectedGroup = null;
    }
    searchItem();
  }
  
  private void selectItem() {
    if (table.getSelectedRow() < 0) {
      return;
    }
    int selectedRow = table.getSelectedRow();
    selectedRow = table.convertRowIndexToModel(selectedRow);
    MenuItem item = (MenuItem)tableModel.getRowData(selectedRow);
    if ((parentMenuItem != null) && (parentMenuItem.getId() != null) && (parentMenuItem.getId().equals(item.getId()))) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Parent item cannot be selected");
      return;
    }
    if (addedMenuItemMap.containsKey(item.getId())) {
      addedMenuItemMap.remove(item.getId());
    }
    else {
      addedMenuItemMap.put(item.getId(), item);
    }
    
    chkShowSelected.setText("Show Selected (" + addedMenuItemMap.values().size() + ")");
    table.repaint();
  }
  
  public int getSelectedRow() {
    int index = table.getSelectedRow();
    if (index < 0)
      return -1;
    return table.convertRowIndexToModel(index);
  }
  
  public void repaintTable() {
    table.repaint();
  }
  
  public void setSelectionMode(int selectionMode) {
    chkShowSelected.setVisible(selectionMode == 1);
    chkSelectAll.setVisible(selectionMode == 1);
    table.getSelectionModel().setSelectionMode(selectionMode);
    if (selectionMode == 0) {
      hideCheckboxColumn();
    }
    searchItem();
  }
  
  private void hideCheckboxColumn() {
    TableColumnModel tcm = table.getColumnModel();
    if (tcm.getColumnCount() == 3) {
      tcm.removeColumn(tcm.getColumn(0));
    }
  }
  
  public void setSelectedGroup(MenuGroup menuGroup) {
    if ((selectedGroup != null) && (selectedGroup.getId().equals(menuGroup.getId())))
      return;
    selectedGroup = menuGroup;
    cbGroup.setSelectedItem(menuGroup);
    searchItem();
  }
  
  public void setEnableSearch(boolean enableSearch) {
    tfName.setVisible(enableSearch);
    lblName.setVisible(enableSearch);
    btnSearch.setVisible(enableSearch);
  }
  
  public boolean isInventoryItemOnly() {
    return inventoryItemOnly;
  }
  
  public void setInventoryItemOnly(boolean inventoryItemOnly) {
    this.inventoryItemOnly = inventoryItemOnly;
  }
  
  public MenuItemTableModel getModel() {
    return tableModel;
  }
  
  private void doAddItem() {
    try {
      MenuItem menuItem = new MenuItem();
      MenuItemForm editor = new MenuItemForm(menuItem);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      
      if (dialog.isCanceled()) {
        return;
      }
      MenuItem foodItem = (MenuItem)editor.getBean();
      tableModel.addItem(foodItem);
      tableModel.setNumRows(tableModel.getNumRows() + 1);
      updateButton();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public class MenuItemTableModel extends PaginatedTableModel<MenuItem> {
    public MenuItemTableModel() {}
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      MenuItem menuItem = (MenuItem)getRowData(rowIndex);
      MenuItem storedItem = (MenuItem)addedMenuItemMap.get(menuItem.getId());
      switch (columnIndex) {
      case 0: 
        return Boolean.valueOf(storedItem != null);
      case 1: 
        return menuItem.getName();
      case 2: 
        return menuItem.getPrice();
      }
      
      
      return null;
    }
    
    public boolean isCellEditable(int row, int col)
    {
      return col == 0;
    }
    
    public Class getColumnClass(int col) {
      switch (col) {
      case 0: 
        return Boolean.class;
      case 1: 
        return String.class;
      case 2: 
        return Double.class;
      }
      throw new InternalError();
    }
  }
}
