package com.floreantpos.ui.model;

import com.floreantpos.POSConstants;
import com.floreantpos.model.MenuPage;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;























public class MenuPageForm
  extends BeanEditor
{
  private FixedLengthTextField tfName;
  private IntegerTextField tfSortOrder;
  private IntegerTextField tfNumberOfColumns;
  private IntegerTextField tfNumberOfRows;
  private IntegerTextField tfButtonWidth;
  private IntegerTextField tfButtonHeight;
  private JCheckBox cbFlexibleButtonSize;
  private boolean isInfo;
  
  public MenuPageForm()
  {
    this(new MenuPage(), false);
  }
  
  public MenuPageForm(MenuPage menuPage, boolean isInfo) {
    this.isInfo = isInfo;
    initComponents();
    setBean(menuPage);
  }
  
  private void initComponents()
  {
    JPanel contentPanel = new JPanel(new MigLayout());
    
    tfName = new FixedLengthTextField();
    tfSortOrder = new IntegerTextField(10);
    
    tfNumberOfColumns = new IntegerTextField(10);
    tfNumberOfRows = new IntegerTextField(10);
    tfButtonWidth = new IntegerTextField(6);
    tfButtonHeight = new IntegerTextField(6);
    
    tfNumberOfColumns.setText("4");
    tfNumberOfRows.setText("4");
    
    JLabel lblName = new JLabel(POSConstants.NAME);
    contentPanel.add(lblName);
    contentPanel.add(tfName, "wrap");
    
    JLabel lblSortOrder = new JLabel(POSConstants.SORT_ORDER);
    contentPanel.add(lblSortOrder);
    contentPanel.add(tfSortOrder, "grow,wrap");
    
    contentPanel.add(new JLabel("Number of columns"));
    contentPanel.add(tfNumberOfColumns, "grow,wrap");
    
    contentPanel.add(new JLabel("Number of rows"));
    contentPanel.add(tfNumberOfRows, "grow");
    
    contentPanel.add(new JLabel("Button width"), "newline");
    contentPanel.add(tfButtonWidth, "grow");
    
    contentPanel.add(new JLabel("Button height"), "newline");
    contentPanel.add(tfButtonHeight, "grow");
    
    cbFlexibleButtonSize = new JCheckBox("Flexible button size");
    contentPanel.add(cbFlexibleButtonSize, "skip 1,newline,grow");
    
    if (isInfo)
    {

      contentPanel.remove(lblName);
      contentPanel.remove(tfName);
      contentPanel.remove(lblSortOrder);
      contentPanel.remove(tfSortOrder);
      

      tfButtonHeight.setText("100");
      tfButtonWidth.setText("100");
    }
    add(contentPanel);
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      


      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage(), e); }
    return false;
  }
  

  protected void updateView()
  {
    MenuPage menuPage = (MenuPage)getBean();
    if (menuPage.getId() == null)
      return;
    tfName.setText(menuPage.getName());
    tfSortOrder.setText(String.valueOf(menuPage.getSortOrder()));
    tfNumberOfColumns.setText(String.valueOf(menuPage.getCols()));
    tfNumberOfRows.setText(String.valueOf(menuPage.getRows()));
    cbFlexibleButtonSize.setSelected(menuPage.isFlixibleButtonSize().booleanValue());
    tfButtonWidth.setText(String.valueOf(menuPage.getButtonWidth()));
    tfButtonHeight.setText(String.valueOf(menuPage.getButtonHeight()));
  }
  
  protected boolean updateModel()
  {
    MenuPage menuPage = (MenuPage)getBean();
    
    String name = tfName.getText();
    if ((POSUtil.isBlankOrNull(name)) && (!isInfo)) {
      POSMessageDialog.showError(this, POSConstants.NAME_REQUIRED);
      return false;
    }
    int buttonWidth = tfButtonWidth.getInteger();
    if (buttonWidth < 30) {
      POSMessageDialog.showError(this, "Button width must be greater than or equal to 30");
      return false;
    }
    int buttonHeight = tfButtonHeight.getInteger();
    if (buttonHeight < 30) {
      POSMessageDialog.showError(this, "Button height must be greater than or equal to 30");
      return false;
    }
    if (!isInfo) {
      menuPage.setName(name);
      menuPage.setSortOrder(Integer.valueOf(tfSortOrder.getInteger()));
    }
    else {
      menuPage.setName("auto");
    }
    
    menuPage.setCols(Integer.valueOf(tfNumberOfColumns.getInteger()));
    menuPage.setRows(Integer.valueOf(tfNumberOfRows.getInteger()));
    menuPage.setFlixibleButtonSize(Boolean.valueOf(cbFlexibleButtonSize.isSelected()));
    menuPage.setButtonWidth(Integer.valueOf(buttonWidth));
    menuPage.setButtonHeight(Integer.valueOf(buttonHeight));
    
    return true;
  }
  
  public String getDisplayText() {
    MenuPage menuPage = (MenuPage)getBean();
    if (menuPage.getId() == null) {
      return "New menu page";
    }
    return "Edit menu gage";
  }
}
