package com.floreantpos.ui.model;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.CustomerGroup;
import com.floreantpos.model.Department;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Outlet;
import com.floreantpos.model.PriceRule;
import com.floreantpos.model.PriceShift;
import com.floreantpos.model.PriceTable;
import com.floreantpos.model.SalesArea;
import com.floreantpos.model.dao.CustomerGroupDAO;
import com.floreantpos.model.dao.DepartmentDAO;
import com.floreantpos.model.dao.OrderTypeDAO;
import com.floreantpos.model.dao.OutletDAO;
import com.floreantpos.model.dao.PriceRuleDAO;
import com.floreantpos.model.dao.PriceShiftDAO;
import com.floreantpos.model.dao.PriceTableDAO;
import com.floreantpos.model.dao.SalesAreaDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;






















public class PriceRuleForm
  extends BeanEditor<PriceRule>
{
  private FixedLengthTextField tfName = new FixedLengthTextField();
  private FixedLengthTextField tfDescription = new FixedLengthTextField();
  private FixedLengthTextField tfCode = new FixedLengthTextField();
  
  private JComboBox cbOutlet = new JComboBox();
  private JComboBox cbDepartment = new JComboBox();
  private JComboBox cbSalesArea = new JComboBox();
  private JComboBox cbOrderType = new JComboBox();
  private JComboBox cbCustomerGroup = new JComboBox();
  private JComboBox cbPriceShift = new JComboBox();
  private JComboBox cbPriceList = new JComboBox();
  
  private JCheckBox chkActive = new JCheckBox("Active");
  
  public PriceRuleForm(PriceRule priceTable) {
    initComponents();
    initData();
    setBean(priceTable);
  }
  
  private void initData() {
    cbOutlet.setModel(getComboModel(OutletDAO.getInstance().findAll()));
    cbDepartment.setModel(getComboModel(DepartmentDAO.getInstance().findAll()));
    cbSalesArea.setModel(getComboModel(SalesAreaDAO.getInstance().findAll()));
    cbOrderType.setModel(getComboModel(OrderTypeDAO.getInstance().findAll()));
    cbCustomerGroup.setModel(getComboModel(CustomerGroupDAO.getInstance().findAll()));
    cbPriceShift.setModel(getComboModel(PriceShiftDAO.getInstance().findAll()));
    cbPriceList.setModel(new com.floreantpos.swing.ComboBoxModel(PriceTableDAO.getInstance().findAll()));
    
    cbOutlet.setSelectedIndex(0);
    cbDepartment.setSelectedIndex(0);
    cbSalesArea.setSelectedIndex(0);
    cbOrderType.setSelectedIndex(0);
    cbCustomerGroup.setSelectedIndex(0);
    cbPriceShift.setSelectedIndex(0);
    if (cbPriceList.getModel().getSize() > 0)
      cbPriceList.setSelectedIndex(0);
  }
  
  private com.floreantpos.swing.ComboBoxModel getComboModel(List items) {
    List dataList = new ArrayList();
    dataList.add("<All>");
    dataList.addAll(items);
    return new com.floreantpos.swing.ComboBoxModel(dataList);
  }
  
  private void initComponents() {
    setLayout(new MigLayout("fillx"));
    setBorder(new EmptyBorder(10, 10, 10, 10));
    
    JButton btnNewPriceShift = new JButton("...");
    JButton btnNewPriceList = new JButton("...");
    
    btnNewPriceShift.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          PriceShiftEntryDialog dialog = new PriceShiftEntryDialog();
          dialog.open();
          if (dialog.isCanceled())
            return;
          PriceShift shift = dialog.getPriceShift();
          cbPriceShift.addItem(shift);
          cbPriceShift.setSelectedItem(shift);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    btnNewPriceList.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try
        {
          PriceTableForm editor = new PriceTableForm(new PriceTable());
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.openWithScale(650, 600);
          
          if (dialog.isCanceled()) {
            return;
          }
          PriceTable priceTable = (PriceTable)editor.getBean();
          cbPriceList.addItem(priceTable);
          cbPriceList.setSelectedItem(priceTable);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    tfName.setLength(30);
    tfDescription.setLength(255);
    tfCode.setLength(6);
    
    add(new JLabel("Name"));
    add(tfName, "grow,wrap");
    add(new JLabel("Description"));
    add(tfDescription, "grow,wrap");
    
    add(new JLabel("Code"));
    add(tfCode, "grow,wrap");
    
    add(new JLabel("Outlet"));
    add(cbOutlet, "grow,wrap");
    
    add(new JLabel("Department"));
    add(cbDepartment, "grow,wrap");
    
    add(new JLabel("Sales Area"));
    add(cbSalesArea, "grow,wrap");
    
    add(new JLabel("Order Type"));
    add(cbOrderType, "grow,wrap");
    
    add(new JLabel("Customer Group"));
    add(cbCustomerGroup, "grow,wrap");
    
    add(new JLabel("Price Shift"));
    add(cbPriceShift, "grow,split 2");
    add(btnNewPriceShift, "wrap");
    
    add(new JLabel("Price List"));
    add(cbPriceList, "grow,split 2");
    add(btnNewPriceList, "wrap");
    
    add(chkActive, "skip 1,grow,wrap");
  }
  
  public boolean save()
  {
    try {
      if (!updateModel())
        return false;
      PriceRule priceTable = (PriceRule)getBean();
      PriceRuleDAO.getInstance().saveOrUpdate(priceTable);
      return true;
    } catch (IllegalModelStateException e) {
      e.printStackTrace();
    }
    return false;
  }
  
  protected void updateView()
  {
    PriceRule priceTable = (PriceRule)getBean();
    tfName.setText(priceTable.getName());
    tfDescription.setText(priceTable.getDescription());
    tfCode.setText(priceTable.getCode());
    chkActive.setSelected(priceTable.getId() == null ? true : priceTable.isActive().booleanValue());
    
    String all = "<All>";
    cbOutlet.setSelectedItem(priceTable.getOutlet() == null ? all : priceTable.getOutlet());
    cbDepartment.setSelectedItem(priceTable.getDepartment() == null ? all : priceTable.getDepartment());
    cbSalesArea.setSelectedItem(priceTable.getSalesArea() == null ? all : priceTable.getSalesArea());
    cbOrderType.setSelectedItem(priceTable.getOrderType() == null ? all : priceTable.getOrderType());
    cbCustomerGroup.setSelectedItem(priceTable.getCustomerGroup() == null ? all : priceTable.getCustomerGroup());
    cbPriceShift.setSelectedItem(priceTable.getPriceShift() == null ? all : priceTable.getPriceShift());
    cbPriceList.setSelectedItem(priceTable.getPriceTable());
  }
  
  protected boolean updateModel() throws IllegalModelStateException
  {
    PriceRule priceTable = (PriceRule)getBean();
    String name = tfName.getText();
    if (StringUtils.isEmpty(name)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Name cannot be empty.");
      return false;
    }
    Object selectedPriceList = cbPriceList.getSelectedItem();
    if (selectedPriceList == null) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select price list.");
      return false;
    }
    priceTable.setName(name);
    priceTable.setDescription(tfDescription.getText());
    priceTable.setCode(tfCode.getText());
    priceTable.setActive(Boolean.valueOf(chkActive.isSelected()));
    
    priceTable.setOutlet((Outlet)getValue(cbOutlet.getSelectedItem()));
    priceTable.setDepartment((Department)getValue(cbDepartment.getSelectedItem()));
    priceTable.setSalesArea((SalesArea)getValue(cbSalesArea.getSelectedItem()));
    priceTable.setCustomerGroup((CustomerGroup)getValue(cbCustomerGroup.getSelectedItem()));
    priceTable.setPriceShift((PriceShift)getValue(cbPriceShift.getSelectedItem()));
    priceTable.setOrderType((OrderType)getValue(cbOrderType.getSelectedItem()));
    
    priceTable.setPriceTable((PriceTable)selectedPriceList);
    return true;
  }
  
  private Object getValue(Object selectedItem) {
    if ((selectedItem instanceof String))
      return null;
    return selectedItem;
  }
  
  public String getDisplayText()
  {
    if (((PriceRule)getBean()).getId() == null) {
      return "New Price Rule";
    }
    return "Edit Price Rule";
  }
}
