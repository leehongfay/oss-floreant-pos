package com.floreantpos.ui.model;

import com.floreantpos.POSConstants;
import com.floreantpos.model.Tax;
import com.floreantpos.model.dao.TaxDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import javax.swing.JLabel;
import net.miginfocom.swing.MigLayout;


public class TaxForm
  extends BeanEditor<Tax>
{
  private FixedLengthTextField tfName = new FixedLengthTextField();
  private DoubleTextField tfRate = new DoubleTextField(10);
  
  public TaxForm() {
    this(new Tax());
  }
  
  public TaxForm(Tax unit) {
    createUI();
    setBean(unit);
  }
  
  private void createUI() {
    setLayout(new MigLayout("fillx"));
    
    add(new JLabel(POSConstants.NAME + ":"));
    add(tfName, "grow, wrap");
    
    add(new JLabel(POSConstants.RATE + ":"));
    tfRate.setHorizontalAlignment(4);
    add(tfRate, "split 2,grow");
    add(new JLabel("%"), "wrap");
  }
  

  public boolean save()
  {
    try
    {
      if (!updateModel()) {
        return false;
      }
      Tax item = (Tax)getBean();
      TaxDAO.getInstance().saveOrUpdate(item);
      return true;
    }
    catch (IllegalModelStateException e) {
      POSMessageDialog.showError(this, e.getMessage());
    }
    
    return false;
  }
  
  public void createNew()
  {
    setBean(new Tax());
    clearFields();
  }
  
  public void setFieldsEnable(boolean enable)
  {
    tfName.setEnabled(enable);
    tfRate.setEnabled(enable);
  }
  
  public void clearFields()
  {
    tfName.setText("");
    tfRate.setText("");
  }
  
  protected void updateView()
  {
    Tax tax = (Tax)getBean();
    if (tax == null) {
      return;
    }
    
    tfName.setText(tax.getName());
    tfRate.setText(String.valueOf(tax.getRate()));
  }
  
  protected boolean updateModel() throws IllegalModelStateException
  {
    Tax tax = (Tax)getBean();
    String name = tfName.getText();
    
    if (POSUtil.isBlankOrNull(name)) {
      MessageDialog.showError(POSConstants.NAME_REQUIRED);
      return false;
    }
    if (TaxDAO.getInstance().nameExists(tax, name)) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), "An tax with that name already exists");
      return false;
    }
    Double conversionRate = Double.valueOf(tfRate.getDoubleOrZero());
    
    tax.setName(name);
    tax.setRate(conversionRate);
    return true;
  }
  
  public boolean delete()
  {
    try
    {
      Tax tax = (Tax)getBean();
      if (tax == null) {
        return false;
      }
      
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), "Are you sure to delete selected item ?", "Confirm");
      if (option != 0) {
        return false;
      }
      
      TaxDAO.getInstance().delete(tax);
      clearFields();
      return true;
    }
    catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e); }
    return false;
  }
  


  public String getDisplayText()
  {
    Tax tax = (Tax)getBean();
    if (tax.getId() == null) {
      return POSConstants.NEW_TAX_RATE;
    }
    return POSConstants.EDIT_TAX_RATE;
  }
}
