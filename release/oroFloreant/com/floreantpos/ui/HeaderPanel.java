package com.floreantpos.ui;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.actions.CacheDataUpdateAction;
import com.floreantpos.actions.ClockInOutAction;
import com.floreantpos.actions.HomeScreenViewAction;
import com.floreantpos.actions.LogoutAction;
import com.floreantpos.actions.ShowBackofficeAction;
import com.floreantpos.actions.ShowSpecialFunctionsAction;
import com.floreantpos.actions.ShutDownAction;
import com.floreantpos.actions.SwithboardViewAction;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.views.order.OrderView;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;






















public class HeaderPanel
  extends JPanel
{
  private JLabel statusLabel;
  private JLabel logoffLabel;
  private PosButton btnHomeScreen;
  private PosButton btnReloadCacheData;
  private PosButton btnShowBackoffice;
  private PosButton btnSpecialFunctions;
  private PosButton btnSwithboardView;
  private PosButton btnLogout;
  private PosButton btnClockOUt;
  private PosButton btnShutdown;
  private POSToggleButton btn_86 = new POSToggleButton("86");
  private JPanel buttonPanel;
  private int btnSize;
  
  public HeaderPanel()
  {
    super(new BorderLayout());
    setOpaque(true);
    setBackground(Color.white);
    
    buttonPanel = new JPanel(new MigLayout("hidemode 3", "sg,fill", ""));
    buttonPanel.setBackground(Color.white);
    
    JLabel logoLabel = new JLabel(IconFactory.getIcon("/ui_icons/", "header_logo.png"));
    add(logoLabel, "West");
    
    TransparentPanel statusPanel = new TransparentPanel(new MigLayout("hidemode 3, fill, ins 0, gap 0"));
    statusLabel = new JLabel();
    statusLabel.setFont(statusLabel.getFont().deriveFont(1));
    statusLabel.setHorizontalAlignment(0);
    statusLabel.setVerticalAlignment(3);
    statusPanel.add(statusLabel, "grow");
    logoffLabel = new JLabel();
    logoffLabel.setFont(statusLabel.getFont().deriveFont(1));
    logoffLabel.setHorizontalAlignment(0);
    logoffLabel.setVerticalAlignment(1);
    statusPanel.add(logoffLabel, "newline, growx");
    
    add(statusPanel, "Center");
    
    btnSize = PosUIManager.getSize(50);
    
    btn_86.setFont(new Font(btn_86.getFont().getName(), 1, 20));
    

    buttonPanel.add(btn_86, "w " + btnSize + "!, h " + btnSize + "!");
    btn_86.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        OrderView.getInstance().set_86Mode(btn_86.isSelected());
      }
      
    });
    btnHomeScreen = new PosButton(new HomeScreenViewAction(false, true));
    buttonPanel.add(btnHomeScreen, "w " + btnSize + "!, h " + btnSize + "!");
    
    btnReloadCacheData = new PosButton(new CacheDataUpdateAction(false, true));
    buttonPanel.add(btnReloadCacheData, "w " + btnSize + "!, h " + btnSize + "!");
    
    btnSwithboardView = new PosButton(new SwithboardViewAction(false, true));
    buttonPanel.add(btnSwithboardView, "w " + btnSize + "!, h " + btnSize + "!");
    
    btnSpecialFunctions = new PosButton(new ShowSpecialFunctionsAction(false, true));
    buttonPanel.add(btnSpecialFunctions, "w " + btnSize + "!, h " + btnSize + "!");
    
    btnShowBackoffice = new PosButton(new ShowBackofficeAction(false, true));
    buttonPanel.add(btnShowBackoffice, "w " + btnSize + "!, h " + btnSize + "!");
    
    btnClockOUt = new PosButton(new ClockInOutAction(false, true));
    buttonPanel.add(btnClockOUt, "w " + btnSize + "!, h " + btnSize + "!");
    
    btnLogout = new PosButton(new LogoutAction(false, true));
    btnLogout.setToolTipText(Messages.getString("Logout"));
    buttonPanel.add(btnLogout, "w " + btnSize + "!, h " + btnSize + "!");
    
    btnShutdown = new PosButton(new ShutDownAction(false, true));
    btnShutdown.setIcon(IconFactory.getIcon("/ui_icons/", "shutdown.png"));
    btnShutdown.setToolTipText(Messages.getString("Shutdown"));
    buttonPanel.add(btnShutdown, "w " + btnSize + "!, h " + btnSize + "!");
    
    add(buttonPanel, "East");
    
    add(new JSeparator(0), "South");
    setPreferredSize(PosUIManager.getSize(100, 62));
  }
  
  public void updateOthersFunctionsView(boolean enable) {
    buttonPanel.removeAll();
    
    buttonPanel.add(btn_86, "w " + btnSize + "!, h " + btnSize + "!");
    buttonPanel.add(btnHomeScreen, "w " + btnSize + "!, h " + btnSize + "!");
    buttonPanel.add(btnReloadCacheData, "w " + btnSize + "!, h " + btnSize + "!");
    buttonPanel.add(btnSpecialFunctions, "w " + btnSize + "!, h " + btnSize + "!");
    buttonPanel.add(btnSwithboardView, "w " + btnSize + "!, h " + btnSize + "!");
    buttonPanel.add(btnShowBackoffice, "w " + btnSize + "!, h " + btnSize + "!");
    buttonPanel.add(btnClockOUt, "w " + btnSize + "!, h " + btnSize + "!");
    buttonPanel.add(btnLogout, "w " + btnSize + "!, h " + btnSize + "!");
    buttonPanel.add(btnShutdown, "w " + btnSize + "!, h " + btnSize + "!");
    btnSpecialFunctions.setVisible(enable);
  }
  
  public void updateSwitchBoardView(boolean enable) {
    buttonPanel.removeAll();
    
    buttonPanel.add(btn_86, "w " + btnSize + "!, h " + btnSize + "!");
    buttonPanel.add(btnHomeScreen, "w " + btnSize + "!, h " + btnSize + "!");
    buttonPanel.add(btnReloadCacheData, "w " + btnSize + "!, h " + btnSize + "!");
    buttonPanel.add(btnSpecialFunctions, "w " + btnSize + "!, h " + btnSize + "!");
    buttonPanel.add(btnSwithboardView, "w " + btnSize + "!, h " + btnSize + "!");
    buttonPanel.add(btnShowBackoffice, "w " + btnSize + "!, h " + btnSize + "!");
    buttonPanel.add(btnClockOUt, "w " + btnSize + "!, h " + btnSize + "!");
    buttonPanel.add(btnLogout, "w " + btnSize + "!, h " + btnSize + "!");
    buttonPanel.add(btnShutdown, "w " + btnSize + "!, h " + btnSize + "!");
    btnSwithboardView.setVisible(enable);
  }
  
  public void updateHomeView(boolean enable) {
    btnHomeScreen.setVisible(enable);
    btn_86.setVisible(OrderView.getInstance().isVisible());
  }
}
