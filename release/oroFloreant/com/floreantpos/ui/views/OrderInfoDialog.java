package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.PosLog;
import com.floreantpos.actions.EmailSendAction;
import com.floreantpos.actions.TicketReorderAction;
import com.floreantpos.actions.TicketTransferAction;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JPanel;


















public class OrderInfoDialog
  extends POSDialog
{
  private OrderInfoView view;
  private boolean reorder = false;
  private PosButton btnReOrder;
  private PosButton btnTransferUser;
  private PosButton btnPrint;
  private PosButton btnPrintDriverCopy;
  private PosButton btnEmail;
  
  public OrderInfoDialog(OrderInfoView view) {
    this.view = view;
    setTitle(Messages.getString("OrderInfoDialog.0"));
    
    createUI();
  }
  
  public void createUI() {
    add(view);
    
    JPanel panel = new JPanel();
    getContentPane().add(panel, "South");
    
    btnReOrder = new PosButton(Messages.getString("OrderInfoDialog.5"));
    
    btnReOrder.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TicketReorderAction reorderAction = new TicketReorderAction((Ticket)view.getTickets().get(0));
        reorderAction.execute();
        if (!reorderAction.isReorder()) {
          return;
        }
        reorder = true;
        setCanceled(false);
        dispose();
      }
      
    });
    btnEmail = new PosButton("EMAIL");
    btnEmail.setAction(new EmailSendAction()
    {
      public Ticket getTicket()
      {
        return (Ticket)view.getTickets().get(0);
      }
    });
    panel.add(btnEmail);
    panel.add(btnReOrder);
    
    btnTransferUser = new PosButton();
    btnTransferUser.setText(Messages.getString("OrderInfoDialog.3"));
    btnTransferUser.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          TicketTransferAction transferAction = new TicketTransferAction((Ticket)view.getTickets().get(0), Application.getCurrentUser());
          transferAction.execute();
          if (!transferAction.isTransfered()) {
            return;
          }
          view.getReportPanel().removeAll();
          view.createReport();
          view.revalidate();
          view.repaint();
          setCanceled(false);
          dispose();
        } catch (Exception e1) {
          POSMessageDialog.showError(Messages.getString("UserTransferDialog.4"));
          PosLog.error(getClass(), e1);
        }
        
      }
      
    });
    panel.add(btnTransferUser);
    
    btnPrint = new PosButton();
    btnPrint.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doPrint();
      }
    });
    btnPrint.setText(Messages.getString("OrderInfoDialog.1"));
    panel.add(btnPrint);
    
    btnPrintDriverCopy = new PosButton();
    btnPrintDriverCopy.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        OrderInfoDialog.this.doPrintDriverCopy();
      }
    });
    btnPrintDriverCopy.setText("Print (Driver Copy)");
    btnPrintDriverCopy.setVisible(false);
    panel.add(btnPrintDriverCopy);
    
    PosButton btnClose = new PosButton();
    btnClose.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        dispose();
      }
    });
    btnClose.setText(Messages.getString("OrderInfoDialog.2"));
    panel.add(btnClose);
  }
  
  private void doPrintDriverCopy() {
    try {
      view.printCopy("Driver Copy");
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    }
  }
  
  public void updateView() {
    btnTransferUser.setVisible(false);
    btnReOrder.setVisible(false);
    btnPrintDriverCopy.setVisible(true);
    btnPrint.setText("Print (Customer Copy)");
  }
  
  protected void doPrint() {
    try {
      view.printCopy("Customer Copy");
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    }
  }
  
  public void setReorder(boolean reorder) {
    this.reorder = reorder;
  }
  
  public boolean isReorder() {
    return reorder;
  }
  
  public void showOnlyPrintButton() {
    btnTransferUser.setVisible(false);
    btnReOrder.setVisible(false);
    btnPrintDriverCopy.setVisible(false);
  }
  
  public OrderInfoView getOrderInfoView() {
    return view;
  }
}
