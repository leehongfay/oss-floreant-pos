package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.ActionHistory;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.dao.ActionHistoryDAO;
import com.floreantpos.model.dao.ShopTableDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.POSTitleLabel;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.TicketForSplitView;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;
import org.hibernate.Session;
import org.hibernate.Transaction;









public class SplitTicketDialog
  extends POSDialog
{
  private Ticket ticket;
  private POSToggleButton btnSplitBySeat;
  private PosButton btnCancel;
  private PosButton btnFinish;
  private POSToggleButton btnNumSplit2;
  private POSToggleButton btnNumSplit3;
  private POSToggleButton btnNumSplit4;
  private POSTitleLabel lblTicketId;
  private TicketForSplitView mainTicketView;
  private TicketForSplitView ticketView2;
  private TicketForSplitView ticketView3;
  private TicketForSplitView ticketView4;
  private TitlePanel titlePanel;
  private TransparentPanel actionButtonPanel;
  private TransparentPanel centerPanel;
  private TransparentPanel toolbarPanel;
  private TransparentPanel ticketPanel;
  
  public SplitTicketDialog()
  {
    initComponents();
    
    mainTicketView.setViewNumber(1);
    ticketView2.setViewNumber(2);
    ticketView3.setViewNumber(3);
    ticketView4.setViewNumber(4);
    
    mainTicketView.setTicketView1(ticketView2);
    mainTicketView.setTicketView2(ticketView3);
    mainTicketView.setTicketView3(ticketView4);
    
    ticketView2.setTicketView1(mainTicketView);
    ticketView2.setTicketView2(ticketView3);
    ticketView2.setTicketView3(ticketView4);
    
    ticketView3.setTicketView1(mainTicketView);
    ticketView3.setTicketView2(ticketView2);
    ticketView3.setTicketView3(ticketView4);
    
    ticketView4.setTicketView1(mainTicketView);
    ticketView4.setTicketView2(ticketView2);
    ticketView4.setTicketView3(ticketView3);
    
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    setSize(screenSize);
    setDefaultCloseOperation(2);
    setResizable(true);
  }
  






  private void initComponents()
  {
    createTitlePanel();
    createActionButtonPanel();
    createToolbarPanel();
    createTicketViewPanel();
    
    centerPanel = new TransparentPanel(new BorderLayout());
    centerPanel.add(toolbarPanel, "North");
    centerPanel.add(ticketPanel, "Center");
    
    getContentPane().add(centerPanel, "Center");
  }
  
  private void createToolbarPanel()
  {
    toolbarPanel = new TransparentPanel();
    
    ButtonGroup buttonGroup = new ButtonGroup();
    
    btnNumSplit2 = new POSToggleButton();
    btnNumSplit3 = new POSToggleButton();
    btnNumSplit4 = new POSToggleButton();
    btnSplitBySeat = new POSToggleButton();
    lblTicketId = new POSTitleLabel();
    
    lblTicketId.setText(Messages.getString("SplitTicketDialog.0"));
    toolbarPanel.add(lblTicketId);
    JSeparator separator = new JSeparator(1);
    separator.setPreferredSize(new Dimension(5, 20));
    toolbarPanel.add(separator);
    toolbarPanel.add(new JLabel(POSConstants.NUMBER_OF_SPLITS));
    
    buttonGroup.add(btnNumSplit2);
    btnNumSplit2.setSelected(true);
    btnNumSplit2.setText("2");
    btnNumSplit2.setPreferredSize(new Dimension(60, 40));
    btnNumSplit2.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        SplitTicketDialog.this.btnNumSplit2ActionPerformed(evt);
      }
      
    });
    toolbarPanel.add(btnNumSplit2);
    
    buttonGroup.add(btnNumSplit3);
    btnNumSplit3.setText("3");
    btnNumSplit3.setPreferredSize(new Dimension(60, 40));
    btnNumSplit3.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        SplitTicketDialog.this.btnNumSplit3ActionPerformed(evt);
      }
      
    });
    toolbarPanel.add(btnNumSplit3);
    
    buttonGroup.add(btnNumSplit4);
    btnNumSplit4.setText("4");
    btnNumSplit4.setPreferredSize(new Dimension(60, 40));
    btnNumSplit4.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        SplitTicketDialog.this.btnNumSplit4ActionPerformed(evt);
      }
      
    });
    toolbarPanel.add(btnNumSplit4);
    
    buttonGroup.add(btnSplitBySeat);
    btnSplitBySeat.setText("Split by seat number");
    btnSplitBySeat.setPreferredSize(new Dimension(120, 40));
    btnSplitBySeat.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        SplitTicketDialog.this.doSplitBySeatNumber(evt);
      }
      
    });
    toolbarPanel.add(btnSplitBySeat);
  }
  
  private void createTicketViewPanel() {
    ticketPanel = new TransparentPanel(new MigLayout("fill, hidemode 3"));
    
    mainTicketView = new TicketForSplitView();
    ticketView2 = new TicketForSplitView();
    ticketView3 = new TicketForSplitView();
    ticketView4 = new TicketForSplitView();
    
    ticketView3.setVisible(false);
    ticketView4.setVisible(false);
    
    ticketPanel.add(mainTicketView, "grow");
    ticketPanel.add(ticketView2, "grow");
    ticketPanel.add(ticketView3, "grow");
    ticketPanel.add(ticketView4, "grow");
  }
  
  private void createActionButtonPanel()
  {
    actionButtonPanel = new TransparentPanel();
    
    btnFinish = new PosButton();
    btnFinish.setText(POSConstants.FINISH);
    btnFinish.setPreferredSize(new Dimension(140, 50));
    btnFinish.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        SplitTicketDialog.this.btnFinishActionPerformed(evt);
      }
      
    });
    actionButtonPanel.add(btnFinish);
    
    btnCancel = new PosButton();
    btnCancel.setText(POSConstants.CANCEL);
    btnCancel.setPreferredSize(new Dimension(140, 50));
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        SplitTicketDialog.this.btnCancelActionPerformed(evt);
      }
      
    });
    actionButtonPanel.add(btnCancel);
    
    getContentPane().add(actionButtonPanel, "South");
  }
  
  private void createTitlePanel() {
    titlePanel = new TitlePanel();
    titlePanel.setTitle(POSConstants.SPLIT_TICKET);
    getContentPane().add(titlePanel, "North");
  }
  
  private void btnCancelActionPerformed(ActionEvent evt) {
    setTicket(null);
    dispose();
  }
  
  private synchronized void btnFinishActionPerformed(ActionEvent evt) {
    Session session = null;
    Transaction tx = null;
    try
    {
      TicketDAO dao = new TicketDAO();
      session = dao.createNewSession();
      tx = session.beginTransaction();
      
      saveTicket(mainTicketView, session);
      saveTicket(ticketView2, session);
      saveTicket(ticketView3, session);
      saveTicket(ticketView4, session);
      
      tx.commit();
      

      ActionHistoryDAO.getInstance().saveHistory(Application.getCurrentUser(), ActionHistory.SPLIT_CHECK, POSConstants.RECEIPT_REPORT_TICKET_NO_LABEL + ":" + mainTicketView.getTicket().getId());
      
      dispose(); return;
    } catch (Exception e) {
      try {
        tx.rollback();
      }
      catch (Exception localException2) {}
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, e);
    } finally {
      try {
        session.close();
      }
      catch (Exception localException4) {}
    }
  }
  
  private void doSplitBySeatNumber(ActionEvent evt) {
    List<TicketItem> ticketItems = ticket.getTicketItems();
    Map<Integer, List<TicketItem>> itemMap = new HashMap();
    for (Iterator localIterator = ticketItems.iterator(); localIterator.hasNext();) { ticketItem = (TicketItem)localIterator.next();
      List<TicketItem> ticketItemList = (List)itemMap.get(ticketItem.getSeatNumber());
      if (ticketItemList == null) {
        ticketItemList = new ArrayList();
        ticketItemList.add(ticketItem);
        itemMap.put(ticketItem.getSeatNumber(), ticketItemList);
      }
      else {
        ticketItemList.add(ticketItem);
      }
    }
    TicketItem ticketItem;
    int splitViewNumber = 1;
    
    for (Integer seatNumber : itemMap.keySet()) {
      List<TicketItem> splitTicketItems = (List)itemMap.get(seatNumber);
      transferTicketItemsToSplitView(splitTicketItems, splitViewNumber);
      splitViewNumber++;
    }
  }
  
  private void transferTicketItemsToSplitView(List<TicketItem> splitTicketItems, int splitViewNumber) {
    if (splitViewNumber == 2) {
      for (TicketItem ticketItem : splitTicketItems) {
        mainTicketView.transferTicketItem(ticketItem, ticketView2, true);
      }
    }
    else if (splitViewNumber == 3) {
      ticketView3.setVisible(true);
      for (TicketItem ticketItem : splitTicketItems) {
        mainTicketView.transferTicketItem(ticketItem, ticketView3, true);
      }
    }
    else if (splitViewNumber == 4) {
      ticketView4.setVisible(true);
      for (TicketItem ticketItem : splitTicketItems) {
        mainTicketView.transferTicketItem(ticketItem, ticketView4, true);
      }
    }
  }
  
  private void btnNumSplit4ActionPerformed(ActionEvent evt)
  {
    ticketView3.setVisible(true);
    ticketView4.setVisible(true);
  }
  
  private void btnNumSplit3ActionPerformed(ActionEvent evt) {
    ticketView3.setVisible(true);
    
    if (ticketView4.isVisible()) {
      Ticket ticket4 = ticketView4.getTicket();
      
      List<TicketItem> ticketItems = ticket4.getTicketItems();
      for (TicketItem item : ticketItems) {
        ticketView4.transferAllTicketItem(item, mainTicketView);
      }
      ticketView4.setVisible(false);
    }
  }
  
  private void btnNumSplit2ActionPerformed(ActionEvent evt) {
    if (ticketView3.isVisible()) {
      Ticket ticket3 = ticketView3.getTicket();
      
      List<TicketItem> ticketItems = new ArrayList(ticket3.getTicketItems());
      for (TicketItem item : ticketItems) {
        ticketView3.transferAllTicketItem(item, mainTicketView);
      }
      ticketView3.setVisible(false);
    }
    
    if (ticketView4.isVisible()) {
      Ticket ticket4 = ticketView4.getTicket();
      
      List<TicketItem> ticketItems = ticket4.getTicketItems();
      for (TicketItem item : ticketItems) {
        ticketView4.transferAllTicketItem(item, mainTicketView);
      }
      ticketView4.setVisible(false);
    }
  }
  
  public void saveTicket(TicketForSplitView view, Session session) {
    if (!view.isVisible()) {
      return;
    }
    view.getTicket().setOrderType(mainTicketView.getTicket().getOrderType());
    view.updateModel();
    
    Ticket ticket = view.getTicket();
    if (ticket.getTicketItems().size() <= 0) {
      return;
    }
    List<ShopTable> tables = ShopTableDAO.getInstance().getTables(mainTicketView.getTicket());
    if (tables != null) {
      for (ShopTable shopTable : tables) {
        ticket.addTable(shopTable.getTableNumber().intValue());
      }
    }
    
    TicketDAO.getInstance().saveOrUpdate(ticket, session);
  }
  

















  public Ticket getTicket()
  {
    return ticket;
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
    if (ticket != null) {
      lblTicketId.setText(POSConstants.ORIGINAL_TICKET_ID + ": " + ticket.getId());
      mainTicketView.setTicket(ticket);
      btnSplitBySeat.setVisible(ticket.getOrderType().isAllowSeatBasedOrder().booleanValue());
    }
  }
}
