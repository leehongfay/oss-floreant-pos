package com.floreantpos.ui.views;

import com.floreantpos.main.Application;
import com.floreantpos.model.Customer;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import java.awt.Color;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;




public class TicketInfoButton
  extends JPanel
{
  Ticket ticket;
  boolean selected;
  private JLabel lblPaidStatus;
  private JLabel lblTitle;
  private JLabel lblInfo;
  private JLabel lblTotalAmount;
  private boolean isAllowCustomerSelection;
  
  public TicketInfoButton(Ticket ticket)
  {
    this.ticket = ticket;
    setLayout(new MigLayout("hidemode 3, inset 0,fill"));
    
    lblTitle = new JLabel();
    lblTitle.setBackground(new Color(66, 155, 207));
    lblTitle.setForeground(Color.white);
    lblTitle.setHorizontalAlignment(0);
    lblTitle.setFont(lblTitle.getFont().deriveFont(1, 20.0F));
    lblTitle.setOpaque(true);
    
    add(lblTitle, "grow,wrap");
    setBackground(Color.white);
    setBorder(BorderFactory.createLineBorder(Color.GRAY));
    
    lblInfo = new JLabel();
    lblInfo.setHorizontalAlignment(0);
    lblInfo.setFont(lblTitle.getFont().deriveFont(1, 14.0F));
    lblInfo.setBackground(Color.white);
    lblInfo.setOpaque(true);
    add(lblInfo, "grow,wrap");
    
    lblTotalAmount = new JLabel();
    lblTotalAmount.setHorizontalAlignment(0);
    lblTotalAmount.setFont(lblTitle.getFont().deriveFont(1, 18.0F));
    lblTotalAmount.setBackground(Color.white);
    lblTotalAmount.setOpaque(true);
    add(lblTotalAmount, "grow,wrap");
    
    lblPaidStatus = new JLabel();
    lblPaidStatus.setHorizontalAlignment(0);
    lblPaidStatus.setBackground(Color.white);
    lblPaidStatus.setForeground(Color.red);
    lblPaidStatus.setFont(lblTitle.getFont().deriveFont(1, 10.0F));
    lblPaidStatus.setOpaque(true);
    add(lblPaidStatus, "grow,wrap");
    
    updateView();
  }
  
  public void updateView() {
    String currencySymbol = CurrencyUtil.getCurrencySymbol();
    String title = "";
    

    Terminal terminal = Application.getInstance().getTerminal();
    if (ticket == null) {
      title = "[New Ticket]";
      lblPaidStatus.setText(String.valueOf(title));
      lblTotalAmount.setVisible(false);
    }
    else {
      Integer tokenNo = ticket.getTokenNo();
      String memberId = ticket.getCustomer() == null ? "" : ticket.getCustomer().getName();
      String server = ticket.getOwner().getFirstName();
      
      if (tokenNo.intValue() > 0) {
        title = "Token# " + tokenNo;
      }
      else
        title = "[New Ticket]";
      String tableText = terminal.isShowTableNumber() ? ticket.getTableNumbersDisplay() : ticket.getTableNames();
      
      String info = "<html><center>";
      if (StringUtils.isNotEmpty(memberId)) {
        info = info + "Member: " + memberId;
      }
      
      if (StringUtils.isNotEmpty(tableText)) {
        info = info + "<br/>Table# " + tableText;
      }
      
      info = info + "<br/>Server: " + server + "</center></html>";
      
      lblTitle.setText("<html><center>" + String.valueOf(title) + "</center></html>");
      lblInfo.setText(info);
      lblTotalAmount.setText("DUE: " + currencySymbol + NumberUtil.formatNumber(ticket.getTotalAmountWithTips()));
      if (isAllowCustomerSelection) {
        String memId = ticket.getCustomerId();
        if (memId != null) {
          Customer customer = CustomerDAO.getInstance().get(memId);
          if (customer != null) {
            lblTitle.setText("<html><center>" + lblTitle.getText() + "<small>-" + customer.getName() + "</small></center></html>");
          }
        }
      }
    }
  }
  
  public boolean isAllowCustomerSelection() {
    return isAllowCustomerSelection;
  }
  
  public void setAllowCustomerSelection(boolean isAllowCustomerSelection) {
    this.isAllowCustomerSelection = isAllowCustomerSelection;
  }
  
  public void setSelected(boolean selected) {
    this.selected = selected;
  }
  
  public boolean isSelected() {
    return selected;
  }
  
  public Ticket getTicket() {
    return ticket;
  }
}
