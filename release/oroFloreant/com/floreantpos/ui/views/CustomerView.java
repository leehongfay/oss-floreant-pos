package com.floreantpos.ui.views;

import com.floreantpos.customer.CustomerSelector;
import com.floreantpos.customer.DefaultCustomerListView;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.model.OrderType;
import com.floreantpos.ui.views.order.ViewPanel;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.util.Locale;





























public class CustomerView
  extends ViewPanel
{
  public static final String VIEW_NAME = "CUSTOMER_ACTIVITY";
  private CustomerSelector customerSelector = null;
  private static CustomerView instance;
  
  private CustomerView(OrderType orderType) {
    setLayout(new BorderLayout());
    
    OrderServiceExtension orderServicePlugin = (OrderServiceExtension)ExtensionManager.getPlugin(OrderServiceExtension.class);
    if (orderServicePlugin == null) {
      customerSelector = new DefaultCustomerListView();
    }
    else
    {
      customerSelector = orderServicePlugin.createCustomerSelectorView();
    }
    customerSelector.setCreateNewTicket(true);
    customerSelector.updateView(false);
    add(customerSelector, "Center");
    
    applyComponentOrientation(ComponentOrientation.getOrientation(Locale.getDefault()));
  }
  
  public void updateView() {
    customerSelector.redererCustomers();
  }
  
  public static CustomerView getInstance(OrderType orderType) {
    if (instance == null) {
      instance = new CustomerView(orderType);
    }
    instancecustomerSelector.setOrderType(orderType);
    
    return instance;
  }
  
  public String getViewName()
  {
    return "CUSTOMER_ACTIVITY";
  }
}
