package com.floreantpos.ui.views;

import com.floreantpos.main.Application;
import com.floreantpos.model.GuestCheckPrint;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.GuestCheckPrintDAO;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.report.TicketPrintProperties;
import com.floreantpos.swing.PosScrollPane;
import java.awt.BorderLayout;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JasperPrint;



















public class OrderInfoView
  extends JPanel
{
  private List<Ticket> tickets;
  private JPanel reportPanel;
  
  public OrderInfoView(List<Ticket> tickets)
    throws Exception
  {
    this.tickets = tickets;
    
    createUI();
  }
  
  public void setTickets(List<Ticket> tickets) {
    this.tickets = tickets;
  }
  
  public void createUI() throws Exception
  {
    reportPanel = new JPanel(new MigLayout("wrap 1, ax 50%", "", ""));
    PosScrollPane scrollPane = new PosScrollPane(reportPanel);
    scrollPane.getVerticalScrollBar().setUnitIncrement(20);
    
    createReport();
    
    setLayout(new BorderLayout());
    add(scrollPane);
  }
  
  public void createReport() throws Exception
  {
    for (int i = 0; i < tickets.size(); i++) {
      Ticket ticket = (Ticket)tickets.get(i);
      
      TicketPrintProperties printProperties = new TicketPrintProperties(null, false, true, true);
      HashMap map = ReceiptPrintService.populateTicketProperties(ticket, printProperties, null);
      map.put("IS_IGNORE_PAGINATION", Boolean.valueOf(true));
      JasperPrint jasperPrint = ReceiptPrintService.createPrint(ticket, map, null);
      TicketReceiptView receiptView = new TicketReceiptView(jasperPrint);
      reportPanel.add(receiptView.getReportPanel());
    }
  }
  
  public void createReport(PosTransaction transaction) throws Exception {
    JasperPrint jasperPrint = ReceiptPrintService.getTransactionReceipt(transaction);
    TicketReceiptView receiptView = new TicketReceiptView(jasperPrint);
    reportPanel.add(receiptView.getReportPanel());
  }
  
  public void print() throws Exception {
    for (Iterator iter = tickets.iterator(); iter.hasNext();) {
      Ticket ticket = (Ticket)iter.next();
      ReceiptPrintService.printTicket(ticket);
    }
  }
  
  public void printCopy(String copyType) throws Exception {
    for (Iterator iter = tickets.iterator(); iter.hasNext();) {
      Ticket ticket = (Ticket)iter.next();
      doCreateGuestCheck(ticket);
      ReceiptPrintService.printTicket(ticket, copyType);
    }
  }
  
  public List<Ticket> getTickets() {
    return tickets;
  }
  
  public JPanel getReportPanel() {
    return reportPanel;
  }
  
  private void doCreateGuestCheck(Ticket ticket) {
    GuestCheckPrint guestCheckPrint = new GuestCheckPrint();
    guestCheckPrint.setTicketId(ticket.getId());
    guestCheckPrint.setPrintTime(new Date());
    guestCheckPrint.setTicketTotal(ticket.getTotalAmountWithTips());
    guestCheckPrint.setUser(Application.getCurrentUser());
    
    Terminal terminal = Application.getInstance().getTerminal();
    String strTableNumbers = "";
    List<Integer> tableNumbers; int count; if (!terminal.isShowTableNumber()) {
      strTableNumbers = ticket.getTableNames();
    }
    else {
      tableNumbers = ticket.getTableNumbers();
      if ((tableNumbers != null) && (tableNumbers.size() > 0)) {
        count = 0;
        for (Integer tableNumber : tableNumbers) {
          strTableNumbers = strTableNumbers + tableNumber;
          if (count < tableNumbers.size() - 1) {
            strTableNumbers = strTableNumbers + ", ";
          }
          count++;
        }
      }
    }
    guestCheckPrint.setTableNo(strTableNumbers);
    
    GuestCheckPrintDAO.getInstance().saveOrUpdate(guestCheckPrint);
  }
}
