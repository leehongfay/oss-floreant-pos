package com.floreantpos.ui.views;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.ticket.TicketViewerTable;
import com.floreantpos.util.NumberUtil;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;


























public class TicketTransferView
  extends TransparentPanel
  implements ActionListener
{
  private PosButton btnScrollDown;
  private PosButton btnScrollUp;
  private PosButton btnTransferOneByOne;
  private PosButton btnTransferAll;
  private JScrollPane scrollPane;
  private JTextField tfDiscount;
  private JTextField tfSubtotal;
  private JTextField tfTax;
  private JTextField tfTotal;
  private TicketViewerTable ticketViewerTable;
  private int viewNumber = 1;
  
  protected int splitNumber;
  
  private List<Integer> selectedTicketNumbers;
  private List<Ticket> tickets;
  private Ticket ticket;
  private TicketTransferView otherTicketView;
  private ButtonGroup btnGroup;
  private ArrayList<POSToggleButton> addedButtonList;
  private final String TICKET_PROP = "ticket";
  private POSToggleButton currentSelectedButton;
  private ORIENTATION orientation;
  
  public static enum ORIENTATION { LEFT,  RIGHT;
    
    private ORIENTATION() {}
  }
  
  public TicketTransferView(List<Ticket> tickets, ORIENTATION orientation) {
    this.tickets = tickets;
    this.orientation = orientation;
    initComponents();
    setOpaque(true);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    setBorder(BorderFactory.createTitledBorder(null, "*", 2, 0));
    add(createTopPanel(), "North");
    add(createTicketTablePanel(), "Center");
    add(createRightButtonPanel(), "East");
    add(createTicketTotalPanel(), "South");
  }
  
  private JPanel createTopPanel()
  {
    JPanel topPanel = new JPanel(new MigLayout());
    btnGroup = new ButtonGroup();
    addedButtonList = new ArrayList();
    for (Ticket ticket : tickets) {
      POSToggleButton btnTicket = new POSToggleButton("Token#: " + ticket.getTokenNo());
      btnTicket.putClientProperty("ticket", ticket);
      btnTicket.addActionListener(this);
      addedButtonList.add(btnTicket);
      btnGroup.add(btnTicket);
      topPanel.add(btnTicket);
    }
    return topPanel;
  }
  
  private JPanel createTicketTablePanel() {
    TransparentPanel ticketTableViewPanel = new TransparentPanel(new BorderLayout());
    ticketTableViewPanel.setBorder(new EmptyBorder(0, 5, 0, 0));
    
    scrollPane = new JScrollPane();
    ticketViewerTable = new TicketViewerTable();
    
    scrollPane.setHorizontalScrollBarPolicy(31);
    scrollPane.setVerticalScrollBarPolicy(21);
    scrollPane.setViewportView(ticketViewerTable);
    
    ticketTableViewPanel.add(scrollPane, "Center");
    return ticketTableViewPanel;
  }
  
  private JPanel createTicketTotalPanel() {
    int rightGap = PosUIManager.getSize(60) + 10;
    TransparentPanel ticketTotalPanel = new TransparentPanel(new MigLayout("wrap 2,right,inset 5 5 5 " + rightGap + " "));
    JLabel lblSubtotal = new JLabel(POSConstants.SUBTOTAL + ":");
    JLabel lblTotal = new JLabel(POSConstants.TOTAL + ":");
    JLabel lblDiscount = new JLabel(POSConstants.DISCOUNT + ":");
    JLabel lblTax = new JLabel(POSConstants.TAX + ":");
    
    lblSubtotal.setHorizontalAlignment(4);
    lblTotal.setHorizontalAlignment(4);
    lblDiscount.setHorizontalAlignment(4);
    lblTax.setHorizontalAlignment(4);
    
    tfSubtotal = new JTextField();
    tfSubtotal.setHorizontalAlignment(11);
    tfSubtotal.setColumns(10);
    
    tfTax = new JTextField();
    tfTax.setHorizontalAlignment(11);
    tfTax.setColumns(10);
    
    tfDiscount = new JTextField();
    tfDiscount.setHorizontalAlignment(11);
    tfDiscount.setColumns(10);
    
    tfTotal = new JTextField();
    tfTotal.setHorizontalAlignment(11);
    tfTotal.setColumns(10);
    
    tfSubtotal.setEditable(false);
    tfTotal.setEditable(false);
    tfDiscount.setEditable(false);
    tfTax.setEditable(false);
    
    ticketTotalPanel.add(lblSubtotal);
    ticketTotalPanel.add(tfSubtotal);
    
    ticketTotalPanel.add(lblDiscount);
    ticketTotalPanel.add(tfDiscount);
    
    ticketTotalPanel.add(lblTax);
    ticketTotalPanel.add(tfTax);
    
    ticketTotalPanel.add(lblTotal);
    ticketTotalPanel.add(tfTotal);
    
    return ticketTotalPanel;
  }
  
  private JPanel createRightButtonPanel() {
    TransparentPanel rightButtonPanel = new TransparentPanel(new MigLayout("wrap 1,fill,hidemode 3,inset 1"));
    
    btnScrollUp = new PosButton();
    btnScrollUp.setIcon(IconFactory.getIcon("/ui_icons/", "up.png"));
    btnScrollUp.setPreferredSize(PosUIManager.getSize(60, 50));
    btnScrollUp.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketTransferView.this.doScrollUp(evt);
      }
      
    });
    rightButtonPanel.add(btnScrollUp, "grow");
    
    btnScrollDown = new PosButton();
    btnScrollDown.setIcon(IconFactory.getIcon("/ui_icons/", "down.png"));
    btnScrollDown.setPreferredSize(PosUIManager.getSize(60, 50));
    btnScrollDown.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketTransferView.this.doScrollDown(evt);
      }
      
    });
    rightButtonPanel.add(btnScrollDown, "grow");
    
    btnTransferOneByOne = new PosButton();
    btnTransferOneByOne.setPreferredSize(PosUIManager.getSize(60, 50));
    btnTransferOneByOne.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketTransferView.this.btnTransferToTicket1ActionPerformed();
      }
      
    });
    rightButtonPanel.add(btnTransferOneByOne, "grow");
    
    btnTransferAll = new PosButton(">>>");
    btnTransferAll.setFont(new Font("Tahoma", 1, 18));
    btnTransferAll.setPreferredSize(PosUIManager.getSize(60, 50));
    btnTransferAll.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketTransferView.this.doTransferAll();
      }
      
    });
    rightButtonPanel.add(btnTransferAll, "grow");
    
    if (orientation.equals(ORIENTATION.LEFT)) {
      btnTransferOneByOne.setIcon(IconFactory.getIcon("next.png"));
      btnTransferAll.setText(">>>");
    }
    else {
      btnTransferOneByOne.setIcon(IconFactory.getIcon("previous.png"));
      btnTransferAll.setText("<<<");
    }
    
    return rightButtonPanel;
  }
  
  private void btnTransferToTicket1ActionPerformed() {
    try {
      int selectedRow = ticketViewerTable.getSelectedRow();
      Object object = ticketViewerTable.get(selectedRow);
      
      if ((object instanceof TicketItem)) {
        TicketItem ticketItem = (TicketItem)object;
        double ticketItemQuantity = ticketItem.getQuantity().doubleValue();
        
        transferTicketItem(ticketItem, Double.valueOf(ticketItemQuantity));
      }
    } catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  private void doTransferAll() {
    try {
      List<TicketItem> ticketItems = ticketViewerTable.getTicketItems();
      for (TicketItem ticketItem : ticketItems) {
        transferTicketItem(ticketItem, ticketItem.getQuantity());
      }
    } catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  public void transferTicketItem(TicketItem ticketItem, Double quantity) {
    TicketItem newTicketItem = ticketItem.cloneAsNew();
    double ticketItemQuantity = NumberUtil.roundToTwoDigit(ticketItem.getQuantity().doubleValue());
    
    if (Double.isInfinite(quantity.doubleValue())) {
      return;
    }
    if (quantity.doubleValue() % 1.0D != 0.0D) {
      newTicketItem.setFractionalUnit(Boolean.valueOf(true));
    }
    quantity = Double.valueOf(NumberUtil.roundToTwoDigit(quantity.doubleValue()));
    newTicketItem.setQuantity(quantity);
    newTicketItem.setInventoryAdjustQty(newTicketItem.getQuantity());
    
    otherTicketView.addTicketItem(newTicketItem);
    otherTicketView.updateView();
    
    if (ticketItemQuantity > quantity.doubleValue()) {
      double itemQuantity = ticketItem.getQuantity().doubleValue();
      ticketItem.setQuantity(Double.valueOf(itemQuantity - quantity.doubleValue()));
      if (ticketItem.getQuantity().doubleValue() % 1.0D != 0.0D) {
        ticketItem.setFractionalUnit(Boolean.valueOf(true));
      }
    }
    else {
      ticketViewerTable.delete(ticketItem.getTableRowNum());
    }
    ticket.getTicketItems().remove(ticketItem);
    updateView();
  }
  
  public void addTicketItem(TicketItem ticketItem) {
    getTicketViewerTable().addTicketItem(ticketItem);
  }
  
  public List<Integer> getViewNumbers() {
    return selectedTicketNumbers;
  }
  
  private void doScrollDown(ActionEvent evt) {
    ticketViewerTable.scrollDown();
  }
  
  private void doScrollUp(ActionEvent evt) {
    ticketViewerTable.scrollUp();
  }
  
  public void updateModel() {
    ticket.calculatePrice();
  }
  
  public void updateView() {
    if ((ticket == null) || (ticket.getTicketItems() == null) || (ticket.getTicketItems().size() <= 0)) {
      tfSubtotal.setText("");
      tfDiscount.setText("");
      tfTax.setText("");
      tfTotal.setText("");
      return;
    }
    
    ticket.calculatePrice();
    tfSubtotal.setText(NumberUtil.formatNumber(ticket.getSubtotalAmount()));
    tfDiscount.setText(NumberUtil.formatNumber(ticket.getDiscountAmount()));
    tfTax.setText(NumberUtil.formatNumber(ticket.getTaxAmount()));
    tfTotal.setText(NumberUtil.formatNumber(ticket.getTotalAmountWithTips()));
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public TicketTransferView getOtherTicketView() {
    return otherTicketView;
  }
  
  public void setOtherTicketView(TicketTransferView ticketView) {
    otherTicketView = ticketView;
  }
  
  public int getViewNumber() {
    return viewNumber;
  }
  
  private TicketViewerTable getTicketViewerTable() {
    return ticketViewerTable;
  }
  
  public void setSelectTicketButton(int index) {
    currentSelectedButton = ((POSToggleButton)addedButtonList.get(index));
    currentSelectedButton.setSelected(true);
    ticket = ((Ticket)currentSelectedButton.getClientProperty("ticket"));
    ticketViewerTable.setTicket(ticket);
    updateView();
  }
  
  public void actionPerformed(ActionEvent e)
  {
    try {
      POSToggleButton btnTgl = (POSToggleButton)e.getSource();
      if (btnTgl.isSelected()) {
        Ticket ticket = (Ticket)btnTgl.getClientProperty("ticket");
        
        if ((otherTicketView.getTicket() != null) && (otherTicketView.getTicket().equals(ticket))) {
          btnTgl.setSelected(false);
          currentSelectedButton.setSelected(true);
          return;
        }
        currentSelectedButton = ((POSToggleButton)e.getSource());
        setCurrentTicket(ticket);
      }
    } catch (Exception e2) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e2);
    }
  }
  
  public void setCurrentTicket(Ticket ticket)
  {
    this.ticket = ticket;
    ticketViewerTable.setTicket(ticket);
    updateView();
  }
  
  public List<Ticket> getTickets() {
    List<Ticket> tickets = new ArrayList();
    for (POSToggleButton posToggleButton : addedButtonList) {
      tickets.add((Ticket)posToggleButton.getClientProperty("ticket"));
    }
    return tickets;
  }
}
