package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.actions.PosAction;
import com.floreantpos.config.GiftCardConfig;
import com.floreantpos.extension.GiftCardPaymentPlugin;
import com.floreantpos.model.GiftCard;
import com.floreantpos.model.UserPermission;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.GiftCardInfo;
import com.floreantpos.ui.dialog.GlobalInputDialog;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.payment.GiftCardProcessor;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;





public class GiftCardManagementView
  extends POSDialog
{
  private PosButton btnActivate;
  private PosButton btnDeActivate;
  private PosButton btnBalance;
  private PosButton btnDisable;
  private PosButton btnCardInfo;
  private PosButton btnEditPinNumber;
  private PosButton btnTransaction;
  private GiftCardProcessor giftCardProcessor;
  
  public GiftCardManagementView()
  {
    super(POSUtil.getBackOfficeWindow(), "");
    
    init();
    pluginChecking();
  }
  
  private void pluginChecking() {
    GiftCardPaymentPlugin paymentGateway = GiftCardConfig.getPaymentGateway();
    giftCardProcessor = paymentGateway.getProcessor();
    if (paymentGateway != null) {
      if (!giftCardProcessor.supportActivation()) {
        btnActivate.setEnabled(false);
      }
      
      if (!giftCardProcessor.supportDeActivation()) {
        btnDeActivate.setEnabled(false);
      }
      
      if (!giftCardProcessor.supportDisable()) {
        btnDisable.setEnabled(false);
      }
      
      if (!giftCardProcessor.supportPinNumberChange()) {
        btnEditPinNumber.setEnabled(false);
      }
      
      if (!giftCardProcessor.supportShowTransaction()) {
        btnTransaction.setEnabled(false);
      }
    }
  }
  

  public GiftCardManagementView(JFrame parent)
  {
    init();
    pluginChecking();
  }
  
  private void init() {
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Gift Card Management");
    add(titlePanel, "North");
    
    JPanel centerPanel = new JPanel(new MigLayout("fillx,aligny center", "[]20px[]", ""));
    
    centerPanel.setBorder(new EmptyBorder(0, 10, 0, 10));
    
    GiftCardProcessAction action = new GiftCardProcessAction();
    btnActivate = new PosButton(Messages.getString("GiftCardManagementView.4"), action);
    btnDeActivate = new PosButton(Messages.getString("GiftCardManagementView.5"), action);
    btnBalance = new PosButton(Messages.getString("GiftCardManagementView.6"), action);
    btnDisable = new PosButton(Messages.getString("GiftCardManagementView.7"), action);
    btnCardInfo = new PosButton(Messages.getString("GiftCardManagementView.8"), action);
    btnEditPinNumber = new PosButton(Messages.getString("GiftCardManagementView.9"), action);
    btnTransaction = new PosButton(Messages.getString("GiftCardManagementView.10"), action);
    
    centerPanel.add(btnActivate, "cell 0 0, growx");
    centerPanel.add(btnBalance, "cell 1 0, growx");
    centerPanel.add(btnDeActivate, "cell 0 1, growx");
    centerPanel.add(btnDisable, "cell 1 1, growx");
    centerPanel.add(btnCardInfo, "cell 0 2, growx");
    centerPanel.add(btnEditPinNumber, "cell 1 2, growx");
    centerPanel.add(btnTransaction, "cell 0 3, growx");
    
    add(centerPanel, "Center");
    
    PosButton closeButton = new PosButton(new CloseDialogAction(this, Messages.getString("GiftCardManagementView.18")));
    
    JPanel buttonsPanel = new JPanel(new MigLayout("fill"));
    
    buttonsPanel.add(closeButton, "alignx center");
    
    add(buttonsPanel, "South");
    

    btnActivate.setAction(action);
    
    btnBalance.setAction(action);
    
    btnDeActivate.setAction(action);
    
    btnDisable.setAction(action);
    
    btnCardInfo.setAction(action);
    
    btnEditPinNumber.setAction(action);
    
    btnTransaction.setAction(action);
  }
  
  private class GiftCardProcessAction extends PosAction
  {
    public GiftCardProcessAction()
    {
      super.setRequiredPermission(UserPermission.MANAGE_GIFT_CARD);
    }
    
    public void execute()
    {
      ActionEvent e = super.getActionEvent();
      String actionCommand = e.getActionCommand();
      
      if (actionCommand.equals(Messages.getString("GiftCardManagementView.4")))
      {
        GiftCardManagementView.this.doActivateGiftCard();
      }
      else if (actionCommand.equals(Messages.getString("GiftCardManagementView.5")))
      {
        GiftCardManagementView.this.doDeActivate();
      }
      else if (actionCommand.equals(Messages.getString("GiftCardManagementView.6")))
      {
        if (!POSUtil.checkDrawerAssignment()) {
          return;
        }
        GiftCardManagementView.this.doAddBalanceGiftCard();

      }
      else if (actionCommand.equals(Messages.getString("GiftCardManagementView.7")))
      {
        GiftCardManagementView.this.doDisableGiftCard();
      }
      else if (actionCommand.equals(Messages.getString("GiftCardManagementView.9")))
      {
        GiftCardManagementView.this.doEditPinNumberGiftCard();
      }
      else if (actionCommand.equals(Messages.getString("GiftCardManagementView.8")))
      {
        GiftCardManagementView.this.doShowCardInfo();
      }
      else if (actionCommand.equals(Messages.getString("GiftCardManagementView.34")))
      {
        GiftCardManagementView.this.doShowTransactionInfo();
      }
    }
  }
  
  private void doActivateGiftCard()
  {
    GiftCardActivateView dialog = new GiftCardActivateView(giftCardProcessor);
    dialog.setTitle(Messages.getString("GiftCardManagementView.28"));
    dialog.setDefaultCloseOperation(2);
    dialog.setSize(PosUIManager.getSize(600, 400));
    dialog.setLocationRelativeTo(POSUtil.getFocusedWindow());
    dialog.setVisible(true);
  }
  
  private void doEditPinNumberGiftCard() {
    GiftCardEditPinNumberView dialog = new GiftCardEditPinNumberView(giftCardProcessor);
    dialog.setTitle(Messages.getString("GiftCardManagementView.29"));
    dialog.setDefaultCloseOperation(2);
    dialog.setSize(PosUIManager.getSize(600, 400));
    dialog.setLocationRelativeTo(POSUtil.getFocusedWindow());
    dialog.setVisible(true);
  }
  
  private void doShowCardInfo() {
    try {
      GiftCard giftCard = getGiftCard();
      if (giftCard == null) {
        return;
      }
      GiftCardInfo dialog = new GiftCardInfo(giftCard);
      dialog.setDefaultCloseOperation(2);
      dialog.setSize(PosUIManager.getSize(470, 400));
      dialog.setLocationRelativeTo(POSUtil.getFocusedWindow());
      dialog.setVisible(true);
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    }
  }
  
  private void doDisableGiftCard() {
    try {
      GiftCard giftCard = getGiftCard();
      if (giftCard == null) {
        return;
      }
      int value = POSMessageDialog.showYesNoQuestionDialog(this, Messages.getString("GiftCardDisableView.13"), Messages.getString("GiftCardDisableView.14"));
      
      if (value == 0) {
        if (giftCard.isDisable().booleanValue()) {
          POSMessageDialog.showMessage(this, Messages.getString("GiftCardDisableView.15"));
          return;
        }
        giftCard.setDisable(Boolean.valueOf(true));
        giftCard.setActive(Boolean.valueOf(false));
        giftCard.setDeActivationDate(new Date());
        
        giftCardProcessor.disable(giftCard);
        
        POSMessageDialog.showMessage(this, Messages.getString("GiftCardDisableView.16"));
        dispose();
      }
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    }
  }
  
  private void doAddBalanceGiftCard() {
    GiftCardAddBalanceView dialog = new GiftCardAddBalanceView(giftCardProcessor);
    dialog.setTitle(Messages.getString("GiftCardManagementView.32"));
    dialog.setDefaultCloseOperation(2);
    dialog.setSize(PosUIManager.getSize(600, 400));
    dialog.setLocationRelativeTo(POSUtil.getFocusedWindow());
    dialog.setVisible(true);
  }
  
  private void doDeActivate() {
    try {
      GiftCard giftCard = getGiftCard();
      if (giftCard == null) {
        return;
      }
      int value = POSMessageDialog.showYesNoQuestionDialog(this, Messages.getString("GiftCardDeActiveView.14"), Messages.getString("GiftCardDeActiveView.15"));
      
      if (value == 0) {
        if (!giftCard.isActive().booleanValue()) {
          POSMessageDialog.showMessage(this, Messages.getString("GiftCardDeActiveView.16"));
          return;
        }
        giftCard.setActive(Boolean.valueOf(false));
        giftCard.setDeActivationDate(new Date());
        
        giftCardProcessor.deactivate(giftCard);
        
        POSMessageDialog.showMessage(this, Messages.getString("GiftCardDeActiveView.17"));
        dispose();
      }
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    }
  }
  
  private GiftCard getGiftCard() {
    String cardNo = getGiftCardNumber();
    if (StringUtils.isEmpty(cardNo)) {
      return null;
    }
    
    GiftCard giftCard = giftCardProcessor.getCard(cardNo);
    
    if (giftCard == null) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("GiftCardDeActiveView.12"));
      return null;
    }
    if (StringUtils.isEmpty(cardNo)) {
      MessageDialog.showError(Messages.getString("GiftCardDeActiveView.13"));
      return null;
    }
    return giftCard;
  }
  
  private void doShowTransactionInfo() {
    try {
      GiftCard giftCard = getGiftCard();
      if (giftCard == null) {
        return;
      }
      GiftCardTransactionInfoView dialog = new GiftCardTransactionInfoView(giftCard, giftCardProcessor);
      dialog.setTitle(Messages.getString("GiftCardTransactionDialog.16"));
      dialog.setDefaultCloseOperation(2);
      dialog.setSize(PosUIManager.getSize(900, 600));
      dialog.setLocationRelativeTo(POSUtil.getFocusedWindow());
      dialog.setVisible(true);
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    }
  }
  
  private String getGiftCardNumber() {
    GlobalInputDialog inputDialog = new GlobalInputDialog();
    inputDialog.setCaption("Enter gift card number ");
    inputDialog.open();
    if (inputDialog.isCanceled()) {
      return null;
    }
    String giftCardNumber = inputDialog.getInput();
    return giftCardNumber;
  }
}
