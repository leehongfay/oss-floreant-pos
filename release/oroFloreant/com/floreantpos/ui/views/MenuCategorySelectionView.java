package com.floreantpos.ui.views;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.bo.ui.explorer.ExplorerButtonPanel;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.model.dao.OrderTypeDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.model.MenuCategoryForm;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;



















public class MenuCategorySelectionView
  extends JPanel
{
  private JComboBox cbOrderTypes;
  private JXTable table;
  private BeanTableModel<MenuCategory> tableModel;
  private JTextField tfName;
  private MenuCategory parentMenuCategory;
  private PosButton btnNext;
  private PosButton btnPrev;
  private OrderType orderType;
  private JLabel lblNumberOfItem = new JLabel();
  private JLabel lblName;
  private JButton btnSearch;
  private JPanel searchPanel;
  private Map<String, MenuCategory> addedMenuCategoryMap = new HashMap();
  private JCheckBox chkShowSelected;
  private JCheckBox chkSelectAll;
  private JLabel lblOrderType;
  public static final int SINGLE_SELECTION = 0;
  public static final int MULTIPLE_SELECTION = 1;
  
  public MenuCategorySelectionView(List<MenuCategory> addedMenuCategories)
  {
    initComponents();
    tableModel.setCurrentRowIndex(0);
    cbOrderTypes.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        chkSelectAll.setEnabled(true);
        tableModel.setCurrentRowIndex(0);
        setSelectedOrderTypes(cbOrderTypes.getSelectedItem());
      }
    });
    setMenuCategories(addedMenuCategories);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    tableModel = new BeanTableModel(MenuCategory.class);
    tableModel.addColumn("", "visible");
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "name");
    tableModel.addColumn(POSConstants.PRICE.toUpperCase() + " (" + CurrencyUtil.getCurrencySymbol() + ")", "price");
    tableModel.setPageSize(10);
    table = new JXTable(tableModel);
    table.setSelectionMode(2);
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.setRowHeight(PosUIManager.getSize(40));
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          MenuCategorySelectionView.this.editSelectedRow();
        }
        else {
          MenuCategorySelectionView.this.selectItem();
        }
        
      }
    });
    JPanel contentPanel = new JPanel(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(10, 5, 10, 5));
    JScrollPane scroll = new JScrollPane(table);
    scroll.setPreferredSize(PosUIManager.getSize(500, 250));
    contentPanel.add(scroll);
    contentPanel.add(buildSearchForm(), "North");
    
    add(contentPanel);
    resizeColumnWidth(table);
    
    JPanel paginationButtonPanel = new JPanel(new MigLayout("ins 5 0 0 0,fillx", "[left,grow][][][]", ""));
    paginationButtonPanel.add(createButtonPanel(), "left,split 2");
    
    chkShowSelected = new JCheckBox("Show selected");
    chkShowSelected.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuCategorySelectionView.this.updateView();
      }
    });
    paginationButtonPanel.add(chkShowSelected);
    paginationButtonPanel.add(lblNumberOfItem, "split 3,center");
    
    btnPrev = new PosButton();
    btnPrev.setIcon(IconFactory.getIcon("/ui_icons/", "previous.png"));
    paginationButtonPanel.add(btnPrev, "center");
    
    PosButton btnDot = new PosButton();
    btnDot.setBorder(null);
    btnDot.setOpaque(false);
    btnDot.setContentAreaFilled(false);
    btnDot.setIcon(IconFactory.getIcon("/ui_icons/", "dot.png"));
    
    btnNext = new PosButton();
    btnNext.setIcon(IconFactory.getIcon("/ui_icons/", "next.png"));
    paginationButtonPanel.add(btnNext);
    paginationButtonPanel.add(new JSeparator(), "newline,span,grow");
    
    contentPanel.add(paginationButtonPanel, "South");
    
    ActionListener action = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          Object source = e.getSource();
          if (source == btnPrev) {
            MenuCategorySelectionView.this.scrollUp();
          }
          else if (source == btnNext) {
            MenuCategorySelectionView.this.scrollDown();
          }
        } catch (Exception e2) {
          POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e2.getMessage(), e2);
        }
        
      }
    };
    btnPrev.addActionListener(action);
    btnNext.addActionListener(action);
    
    btnNext.setEnabled(false);
    btnPrev.setEnabled(false);
  }
  
  private void updateView() {
    if (chkShowSelected.isSelected())
    {
      tableModel.setRows(new ArrayList(addedMenuCategoryMap.values()));
      
      updateMenuCategorySelection();
      chkShowSelected.setText("Show Selected (" + addedMenuCategoryMap.values().size() + ")");
      lblNumberOfItem.setText("");
      btnPrev.setEnabled(false);
      btnNext.setEnabled(false);
      cbOrderTypes.setEnabled(false);
      table.repaint();
    }
    else {
      if (cbOrderTypes.getSelectedItem() != null) {
        searchItem();
      }
      cbOrderTypes.setEnabled(true);
    }
  }
  
  private JPanel buildSearchForm() {
    searchPanel = new JPanel();
    
    searchPanel.setLayout(new MigLayout("inset 0,fillx,hidemode 3", "", "[]10[]"));
    lblName = new JLabel(POSConstants.NAME + ":");
    tfName = new JTextField(15);
    btnSearch = new JButton(POSConstants.SEARCH_ITEM_BUTTON_TEXT);
    searchPanel.add(lblName, "align label,split 5");
    searchPanel.add(tfName, "growx");
    
    chkSelectAll = new JCheckBox("Select All");
    chkSelectAll.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuCategorySelectionView.this.selectOrderTypes();
      }
      
    });
    cbOrderTypes = new JComboBox();
    List groups = new ArrayList();
    groups.add("<All>");
    
    List<OrderType> orderTypeList = OrderTypeDAO.getInstance().findAll();
    groups.addAll(orderTypeList);
    
    ComboBoxModel model = new ComboBoxModel(groups);
    cbOrderTypes.setModel(model);
    cbOrderTypes.setSelectedItem("<All>");
    cbOrderTypes.addItemListener(new ItemListener()
    {

      public void itemStateChanged(ItemEvent e)
      {
        MenuCategorySelectionView.this.searchItem();
      }
    });
    searchPanel.add(btnSearch);
    lblOrderType = new JLabel("Ordertype");
    searchPanel.add(lblOrderType, "split 2,right");
    searchPanel.add(cbOrderTypes, "wrap");
    searchPanel.add(chkSelectAll, "left");
    
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        MenuCategorySelectionView.this.searchItem();
      }
      
    });
    tfName.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuCategorySelectionView.this.searchItem();
      }
    });
    return searchPanel;
  }
  
  private void searchItem() {
    if ((cbOrderTypes.getSelectedItem() instanceof OrderType)) {
      orderType = ((OrderType)cbOrderTypes.getSelectedItem());
    }
    else {
      orderType = null;
    }
    tableModel.setCurrentRowIndex(0);
    
    MenuCategoryDAO.getInstance().findCategories(tableModel, true, new String[] { MenuCategory.PROP_NAME });
    
    doSetVisibleCheckAll();
    updateButton();
    updateMenuCategorySelection();
    table.repaint();
    chkShowSelected.setSelected(false);
  }
  
  private void selectOrderTypes() {
    Object selectedOrderType = cbOrderTypes.getSelectedItem();
    if ((selectedOrderType instanceof OrderType)) {
      List<MenuCategory> categories = tableModel.getRows();
      if ((categories != null) && (categories.size() > 0)) {
        for (MenuCategory item : categories) {
          if ((parentMenuCategory == null) || (parentMenuCategory.getId() == null) || (!parentMenuCategory.getId().equals(item.getId())))
          {

            item.setVisible(Boolean.valueOf(chkSelectAll.isSelected()));
            if (item.isVisible().booleanValue()) {
              addedMenuCategoryMap.put(item.getId(), item);
            }
            else {
              addedMenuCategoryMap.remove(item.getId());
            }
          }
        }
      } else {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "No items found!");
        chkSelectAll.setSelected(false);
      }
    }
    else {
      List<MenuCategory> menuItems = tableModel.getRows();
      if (menuItems == null)
        return;
      for (MenuCategory item : menuItems) {
        if ((parentMenuCategory == null) || (parentMenuCategory.getId() == null) || (!parentMenuCategory.getId().equals(item.getId())))
        {

          item.setVisible(Boolean.valueOf(chkSelectAll.isSelected()));
          if (item.isVisible().booleanValue()) {
            addedMenuCategoryMap.put(item.getId(), item);
          }
          else {
            addedMenuCategoryMap.remove(item.getId());
          }
          chkShowSelected.setText("Show Selected (" + addedMenuCategoryMap.values().size() + ")");
          table.repaint();
        }
      }
    }
    table.repaint();
  }
  
  private void updateMenuCategorySelection() {
    List<MenuCategory> menuCategories = tableModel.getRows();
    if (menuCategories == null)
      return;
    for (MenuCategory category : menuCategories) {
      MenuCategory item = (MenuCategory)addedMenuCategoryMap.get(category.getId());
      category.setVisible(Boolean.valueOf(item != null));
    }
  }
  
  private void updateButton() {
    int startNumber = tableModel.getCurrentRowIndex() + 1;
    int endNumber = tableModel.getNextRowIndex();
    int totalNumber = tableModel.getNumRows();
    if (endNumber > totalNumber) {
      endNumber = totalNumber;
    }
    lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
    btnPrev.setEnabled(tableModel.hasPrevious());
    btnNext.setEnabled(tableModel.hasNext());
    
    if (tableModel.getRowCount() > 0) {
      table.setRowSelectionInterval(0, 0);
    }
    chkShowSelected.setText("Show Selected (" + addedMenuCategoryMap.values().size() + ")");
  }
  
  private TransparentPanel createButtonPanel() {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton btnEdit = explorerButton.getEditButton();
    JButton btnAdd = explorerButton.getAddButton();
    btnAdd.setText(POSConstants.ADD);
    btnEdit.setText(POSConstants.EDIT);
    
    btnEdit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MenuCategorySelectionView.this.editSelectedRow();
      }
    });
    btnAdd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          MenuCategory menuCategory = new MenuCategory();
          MenuCategoryForm editor = new MenuCategoryForm(menuCategory);
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          MenuCategory category = (MenuCategory)editor.getBean();
          tableModel.addRow(category);
          tableModel.setNumRows(tableModel.getNumRows() + 1);
          MenuCategorySelectionView.this.updateButton();
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel(new MigLayout("center,ins 0", "sg,fill", ""));
    return panel;
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(70));
    
    columnWidth.add(Integer.valueOf(250));
    columnWidth.add(Integer.valueOf(70));
    
    return columnWidth;
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      MenuCategory category = (MenuCategory)tableModel.getRow(index);
      MenuCategoryDAO.getInstance().initialize(category);
      tableModel.setRow(index, category);
      
      BeanEditor<MenuCategory> editor = new MenuCategoryForm(category);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public List<MenuCategory> getSelectedMenuCategoryList() {
    return new ArrayList(addedMenuCategoryMap.values());
  }
  
  public void setMenuCategories(List<MenuCategory> menuCategories) {
    if (menuCategories != null) {
      for (MenuCategory item : menuCategories) {
        addedMenuCategoryMap.put(item.getId(), item);
        tableModel.addRow(item);
      }
    }
  }
  
  public void setParentMenuItem(MenuCategory selectedMenuItem, boolean editMode) {
    parentMenuCategory = selectedMenuItem;
    if (editMode) {
      chkShowSelected.setSelected(true);
      updateView();
    }
    else {
      searchItem();
    }
  }
  
  private void scrollDown() { tableModel.setCurrentRowIndex(tableModel.getNextRowIndex());
    MenuCategoryDAO.getInstance().findCategories(tableModel, true, new String[] { MenuCategory.PROP_NAME });
    doSetVisibleCheckAll();
    updateButton();
    updateMenuCategorySelection();
    table.repaint();
    chkShowSelected.setSelected(false);
  }
  
  private void scrollUp() {
    tableModel.setCurrentRowIndex(tableModel.getPreviousRowIndex());
    MenuCategoryDAO.getInstance().findCategories(tableModel, true, new String[] { MenuCategory.PROP_NAME });
    doSetVisibleCheckAll();
    updateButton();
    updateMenuCategorySelection();
    table.repaint();
    chkShowSelected.setSelected(false);
  }
  
  public void setSelectedOrderTypes(Object selectedItem) {
    if ((selectedItem instanceof OrderType)) {
      orderType = ((OrderType)selectedItem);
    }
    else {
      orderType = null;
    }
    searchItem();
  }
  
  private void selectItem() {
    if (table.getSelectedRow() < 0) {
      return;
    }
    int selectedRow = table.getSelectedRow();
    selectedRow = table.convertRowIndexToModel(selectedRow);
    MenuCategory item = (MenuCategory)tableModel.getRow(selectedRow);
    if ((parentMenuCategory != null) && (parentMenuCategory.getId() != null) && (parentMenuCategory.getId().equals(item.getId()))) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Parent item cannot be selected");
      return;
    }
    item.setVisible(Boolean.valueOf(!item.isVisible().booleanValue()));
    if (item.isVisible().booleanValue()) {
      addedMenuCategoryMap.put(item.getId(), item);
    }
    else {
      addedMenuCategoryMap.remove(item.getId());
    }
    chkShowSelected.setText("Show Selected (" + addedMenuCategoryMap.values().size() + ")");
    table.repaint();
  }
  
  private void doSetVisibleCheckAll() {
    List<Boolean> checkVisibilities = new ArrayList();
    List<MenuCategory> menuItems = tableModel.getRows();
    for (MenuCategory menuItem : menuItems) {
      if (addedMenuCategoryMap.containsKey(menuItem.getId())) {
        checkVisibilities.add(menuItem.isVisible());
      }
    }
    
    if ((checkVisibilities.size() != 0) && (!checkVisibilities.contains(Boolean.valueOf(false)))) {
      chkSelectAll.setSelected(true);
    }
    else {
      chkSelectAll.setSelected(false);
    }
  }
  
  public BeanTableModel<MenuCategory> getModel() {
    return tableModel;
  }
  
  public int getSelectedRow() {
    int index = table.getSelectedRow();
    if (index < 0)
      return -1;
    return table.convertRowIndexToModel(index);
  }
  
  public void repaintTable() {
    table.repaint();
  }
  
  public void setSelectionMode(int selectionMode) {
    chkShowSelected.setVisible(selectionMode == 1);
    chkSelectAll.setVisible(selectionMode == 1);
    TableColumnModelExt columnModel = (TableColumnModelExt)table.getColumnModel();
    columnModel.getColumnExt(0).setVisible(selectionMode == 1);
    searchItem();
  }
  
  public void setSelectedOrdertype(OrderType orderType) {
    if ((orderType != null) && (orderType.getId().equals(orderType.getId())))
      return;
    this.orderType = orderType;
    cbOrderTypes.setSelectedItem(orderType);
    searchItem();
  }
  
  public void setEnableSearch(boolean enableSearch) {
    tfName.setVisible(enableSearch);
    lblName.setVisible(enableSearch);
    btnSearch.setVisible(enableSearch);
  }
}
