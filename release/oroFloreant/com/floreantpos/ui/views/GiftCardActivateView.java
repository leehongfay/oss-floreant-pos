package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.model.GiftCard;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.payment.GiftCardProcessor;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;





public class GiftCardActivateView
  extends POSDialog
{
  private JTextField txtCardNumber;
  private JTextField txtHolder;
  private GiftCard giftCard;
  private FixedLengthTextField txtPinNumber;
  private GiftCardProcessor giftCardProcessor;
  
  public GiftCardActivateView(GiftCardProcessor giftCardProcessor)
  {
    this.giftCardProcessor = giftCardProcessor;
    init();
  }
  

  public GiftCardActivateView(JFrame parent)
  {
    init();
  }
  
  private void init() {
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Gift Card Activation");
    add(titlePanel, "North");
    
    JPanel centerPanel = new JPanel(new MigLayout("fillx,aligny center", "[][]", ""));
    
    JLabel lblNumberOfCard = new JLabel(Messages.getString("GiftCardActivateView.3"));
    
    txtCardNumber = new JTextField(20);
    
    JLabel lblHolder = new JLabel(Messages.getString("GiftCardActivateView.4"));
    
    txtHolder = new JTextField(20);
    
    txtPinNumber = new FixedLengthTextField(20);
    txtPinNumber.setLength(8);
    
    centerPanel.add(lblNumberOfCard, "cell 0 0, alignx right");
    centerPanel.add(txtCardNumber, "cell 1 0");
    centerPanel.add(lblHolder, "cell 0 1,alignx right");
    centerPanel.add(txtHolder, "cell 1 1");
    centerPanel.add(new JLabel(Messages.getString("GiftCardActivateView.9")), "cell 0 2,alignx right");
    centerPanel.add(txtPinNumber, "cell 1 2");
    
    add(centerPanel, "Center");
    
    JPanel buttonPanel = new JPanel(new MigLayout("al center", "sg, fill", ""));
    
    PosButton btnGenerate = new PosButton(Messages.getString("GiftCardActivateView.15"));
    buttonPanel.add(btnGenerate, "grow");
    
    btnGenerate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doGenerate();

      }
      

    });
    buttonPanel.add(new PosButton(new CloseDialogAction(this, Messages.getString("UserListDialog.5"))));
    
    add(buttonPanel, "South");
    
    txtCardNumber.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (StringUtils.isEmpty(txtCardNumber.getText())) {
          MessageDialog.showError(Messages.getString("GiftCardActivateView.18"));
        }
        else {
          giftCard = giftCardProcessor.getCard(txtCardNumber.getText());
          if (giftCard == null) {
            POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("GiftCardActivateView.19"));
            return;
          }
          if (giftCard != null) {
            String holder = giftCard.getOwnerName();
            if (StringUtils.isEmpty(holder)) {
              txtHolder.setText("");
            }
            else {
              txtHolder.setText(holder);
            }
            
            String pinNumber = giftCard.getPinNumber();
            if (StringUtils.isEmpty(pinNumber)) {
              txtPinNumber.setText("");
            }
            else {
              txtPinNumber.setText(pinNumber);
            }
          }
        }
      }
    });
  }
  


  public void doGenerate()
  {
    if (save())
    {
      giftCardProcessor.activate(giftCard);
      POSMessageDialog.showMessage(this, Messages.getString("GiftCardActivateView.22"));
      dispose();
    }
  }
  
  private boolean save() {
    String cardNo = txtCardNumber.getText();
    String holder = txtHolder.getText();
    String pinNumber = txtPinNumber.getText();
    
    if (StringUtils.isEmpty(cardNo)) {
      MessageDialog.showError(Messages.getString("GiftCardActivateView.23"));
      return false;
    }
    
    giftCard = giftCardProcessor.getCard(cardNo);
    
    if (giftCard == null) {
      POSMessageDialog.showMessage(this, Messages.getString("GiftCardActivateView.24"));
      return false;
    }
    
    if (giftCard.isActive().booleanValue()) {
      POSMessageDialog.showMessage(this, Messages.getString("GiftCardActivateView.25"));
      return false;
    }
    
    if (StringUtils.isEmpty(holder)) {
      MessageDialog.showError(Messages.getString("GiftCardActivateView.26"));
      return false;
    }
    
    if (StringUtils.isEmpty(pinNumber)) {
      MessageDialog.showError(Messages.getString("GiftCardActivateView.27"));
      return false;
    }
    
    Calendar c = Calendar.getInstance();
    Date activationDate = c.getTime();
    if (giftCard.getDurationType().equals("DAY")) {
      c.add(5, giftCard.getDuration().intValue());
    }
    else if (giftCard.getDurationType().equals("MONTH")) {
      c.add(2, giftCard.getDuration().intValue());
    }
    else {
      c.add(1, giftCard.getDuration().intValue());
    }
    Date expiryDate = c.getTime();
    
    giftCard.setOwnerName(holder);
    giftCard.setActivationDate(activationDate);
    giftCard.setDeActivationDate(null);
    giftCard.setExpiryDate(expiryDate);
    giftCard.setActive(Boolean.valueOf(true));
    giftCard.setDisable(Boolean.valueOf(false));
    giftCard.setPinNumber(pinNumber);
    
    return true;
  }
}
