package com.floreantpos.ui.views;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.StoreAlreadyOpenException;
import com.floreantpos.actions.DrawerAssignmentAction;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.model.AttendenceHistory;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.DeclaredTips;
import com.floreantpos.model.DrawerType;
import com.floreantpos.model.EmployeeInOutHistory;
import com.floreantpos.model.Shift;
import com.floreantpos.model.Store;
import com.floreantpos.model.StoreSession;
import com.floreantpos.model.StoreSessionControl;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.UserType;
import com.floreantpos.model.dao.AttendenceHistoryDAO;
import com.floreantpos.model.dao.DeclaredTipsDAO;
import com.floreantpos.model.dao.EmployeeInOutHistoryDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.CashDrawerInfoDialog;
import com.floreantpos.ui.dialog.ClockedInUserListDialog;
import com.floreantpos.ui.dialog.DrawerAndStaffBankReportDialog;
import com.floreantpos.ui.dialog.GratuityDialog;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.ServerOpenTicketListDialog;
import com.floreantpos.ui.dialog.TipsDeclarationDialog;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.ui.views.payment.AuthorizableTicketBrowser;
import com.floreantpos.util.DrawerUtil;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.ShiftUtil;
import com.floreantpos.util.StoreUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.Timer;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;






























public class ClockInOutDialog
  extends POSDialog
{
  private static final SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm:ss aaa");
  private static final SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd MMM, hh:mm aaa");
  private static final SimpleDateFormat dateFormat3 = new SimpleDateFormat("hh:mm aaa");
  private Timer clockTimer = new Timer(1000, new ClockTimerHandler(null));
  
  private JLabel lblPicture;
  
  private JLabel lblUserName;
  
  private JLabel lblUserType;
  
  private JLabel lblCurrentTimeInfo;
  
  private JLabel lblTime;
  private JLabel lblRestaurantName;
  private JLabel lblUser;
  private JLabel lblTerminal;
  private JLabel lblOpenStoreStatus;
  private PosButton btnOpenStore;
  private PosButton btnStoreStatus;
  private PosButton btnClockIn;
  private PosButton btnClockOut;
  private PosButton btnDriverIn;
  private PosButton btnNoSale = new PosButton(Messages.getString("ManagerDialog.1"));
  
  private PosButton btnCancel;
  
  private PosButton btnStaffBankStatus;
  private PosButton btnShowClockedInUsers;
  private PosButton btnAssignDrawer;
  private PosButton btnTerminalStatus;
  private PosButton btnStartStaffBank;
  private PosButton btnOpenTickets;
  private JPanel loginRoleSelectionPanel;
  private PosButton btnTipsManagement;
  private PosButton btnDeclareTips;
  private PosButton btnAuthorize = new PosButton(POSConstants.AUTHORIZE_BUTTON_TEXT);
  private User currentUser;
  private boolean loginMode;
  private static ClockInOutDialog instance;
  private JLabel lblCurrentShift;
  private JLabel lblUserClockedStatus;
  private PosButton btnExit;
  private JPanel cashDrawerButtonPanel;
  
  public ClockInOutDialog()
  {
    super(Application.getPosWindow());
    setLayout(new BorderLayout(5, 5));
    
    int width = PosUIManager.getSize(250);
    Dimension size = Application.getPosWindow().getSize();
    
    String wrap = "";
    if (width < 1390) {
      wrap = ",wrap";
      width = 190;
    }
    
    JPanel contentPanel = new JPanel(new MigLayout("fill,ins 10 20 10 20,hidemode 3", "[][grow][]", ""));
    JPanel leftPictureAndInfoPanel = new JPanel(new MigLayout("wrap 1,filly,ins 0", "", "[grow][]"));
    
    JPanel userProfilePanel = new JPanel(new MigLayout("wrap 1,center"));
    userProfilePanel.setBorder(BorderFactory.createTitledBorder(null, "User Profile", 2, 2));
    
    lblPicture = new JLabel("");
    lblPicture.setBorder(BorderFactory.createLineBorder(new Color(0.0F, 0.0F, 0.0F, 0.1F), 5));
    
    lblUserName = new JLabel("");
    lblUserType = new JLabel("");
    
    Font f = lblUserName.getFont().deriveFont(1);
    lblUserName.setFont(f);
    lblUserType.setFont(f);
    
    userProfilePanel.add(lblPicture, "center");
    userProfilePanel.add(lblUserName, "center");
    userProfilePanel.add(lblUserType, "center");
    
    JPanel messagePanel = new JPanel(new BorderLayout());
    JLabel lblMessage = new JLabel();
    messagePanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(null, "Message", 2, 2), 
      BorderFactory.createEmptyBorder(1, 2, 1, 2)));
    leftPictureAndInfoPanel.add(userProfilePanel, "aligny top,w " + width + "!,grow");
    
    JTextArea txtMessage = new JTextArea(4, 4);
    txtMessage.setPreferredSize(new Dimension(0, 50));
    txtMessage.setLineWrap(true);
    
    messagePanel.add(lblMessage);
    


    contentPanel.add(leftPictureAndInfoPanel, "growy");
    
    JPanel clockInOutActionPanel = new JPanel(new BorderLayout());
    clockInOutActionPanel.setBorder(new EmptyBorder(0, 5, 5, 5));
    lblCurrentTimeInfo = new JLabel();
    
    btnOpenStore = new PosButton("<html><center><h4>OPEN STORE & </h4>" + Messages.getString("ClockInOutAction.5") + "</center></html>");
    btnStoreStatus = new PosButton("STORE STATUS");
    btnOpenTickets = new PosButton("OPEN TICKETS");
    btnClockIn = new PosButton(Messages.getString("ClockInOutAction.5"));
    btnClockOut = new PosButton(Messages.getString("ClockInOutAction.6"));
    btnDriverIn = new PosButton("DRIVER IN");
    
    btnExit = new PosButton("EXIT");
    
    lblTime = new JLabel("");
    StringBuilder sb = new StringBuilder();
    sb.append(timeFormat.format(Calendar.getInstance().getTime()));
    lblTime.setText(sb.toString());
    lblTime.setHorizontalAlignment(0);
    
    JPanel centerPanel = new JPanel(new BorderLayout());
    
    contentPanel.add(centerPanel, "grow");
    
    JPanel attendancePanel = new JPanel(new MigLayout("wrap 1,hidemode 3,fill", "", "[][grow][][][]"));
    attendancePanel.setBorder(BorderFactory.createTitledBorder(null, "Attendance", 2, 2));
    
    Dimension btnSize = PosUIManager.getSize(130, 120);
    btnClockIn.setPreferredSize(btnSize);
    btnClockOut.setPreferredSize(btnSize);
    btnDriverIn.setPreferredSize(btnSize);
    
    btnOpenStore.setPreferredSize(btnSize);
    
    btnClockIn.setIcon(IconFactory.getIcon("/ui_icons/", "clock_out.png"));
    btnClockOut.setIcon(IconFactory.getIcon("/ui_icons/", "clock_out.png"));
    btnOpenStore.setIcon(IconFactory.getIcon("/ui_icons/", "clock_out.png"));
    
    btnClockIn.setVerticalTextPosition(3);
    btnClockIn.setHorizontalTextPosition(0);
    
    btnOpenStore.setHorizontalTextPosition(0);
    btnOpenStore.setVerticalTextPosition(3);
    
    btnClockOut.setVerticalTextPosition(3);
    btnClockOut.setHorizontalTextPosition(0);
    
    Font font = new Font(btnClockIn.getFont().getName(), 1, 20);
    btnClockIn.setFont(font);
    btnClockOut.setFont(font);
    btnDriverIn.setFont(font);
    
    btnOpenStore.setFont(font);
    
    lblOpenStoreStatus = new JLabel(" STORE IS NOT OPEN ");
    lblOpenStoreStatus.setFont(font);
    lblOpenStoreStatus.setForeground(Color.GRAY);
    lblOpenStoreStatus.setVisible(false);
    
    lblCurrentShift = new JLabel("<html><b>Current Shift</b></html>");
    lblCurrentTimeInfo.setHorizontalAlignment(4);
    
    lblUserClockedStatus = new JLabel();
    lblUserClockedStatus.setHorizontalAlignment(0);
    
    JPanel shiftInfoPanel = new JPanel(new BorderLayout());
    shiftInfoPanel.add(lblCurrentShift, "West");
    shiftInfoPanel.add(lblUserClockedStatus, "South");
    shiftInfoPanel.add(lblCurrentTimeInfo, "East");
    
    attendancePanel.add(shiftInfoPanel, "growx");
    attendancePanel.add(lblOpenStoreStatus, "center");
    attendancePanel.add(btnOpenStore, "center");
    attendancePanel.add(btnClockIn, "center");
    attendancePanel.add(btnClockOut, "center");
    


    loginRoleSelectionPanel = new JPanel(new MigLayout("hidemode 3,fillx,ins 0 5 5 5", "sg,fill", ""));
    
    clockInOutActionPanel.add(loginRoleSelectionPanel, "North");
    clockInOutActionPanel.add(attendancePanel);
    
    btnAssignDrawer = new PosButton("ASSIGN DRAWER");
    btnTerminalStatus = new PosButton("THIS TERMINAL STATUS");
    btnShowClockedInUsers = new PosButton("CLOCKED IN USERS");
    btnStaffBankStatus = new PosButton("STAFF BANK STATUS");
    btnStartStaffBank = new PosButton("START STAFF BANK");
    btnTipsManagement = new PosButton(POSConstants.SERVER_TIPS);
    btnDeclareTips = new PosButton("DECLARE TIPS");
    btnTipsManagement.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ClockInOutDialog.this.doManageGratuity();
      }
    });
    btnDeclareTips.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ClockInOutDialog.this.doDeclareTips();
      }
      
    });
    btnAuthorize.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ClockInOutDialog.this.doAuthorizeTickets();
      }
    });
    btnNoSale.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e) {}


    });
    btnOpenTickets.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (currentUser.hasPermission(UserPermission.EDIT_OTHER_USERS_TICKETS)) {
          ServerOpenTicketListDialog dialog = new ServerOpenTicketListDialog(currentUser, null, false);
          dialog.setTitle("Open tickets..");
          dialog.setSize(PosUIManager.getSize(830, 550));
          dialog.open();
        }
        else {
          ServerOpenTicketListDialog dialog = new ServerOpenTicketListDialog(currentUser, currentUser, true);
          dialog.setTitle("Open tickets..");
          dialog.setSize(PosUIManager.getSize(830, 550));
          dialog.open();
        }
        
      }
    });
    JPanel buttonUserActionPanel = new JPanel(new MigLayout("center,ins 5 5 5 5,hidemode 3,fillx", "", ""));
    
    btnCancel = new PosButton("EXIT");
    btnCancel.setHorizontalTextPosition(2);
    
    JPanel panel1 = new JPanel(new MigLayout("center,ins 0,hidemode 3,fillx", "", ""));
    cashDrawerButtonPanel = new JPanel(new MigLayout("center,ins 0,hidemode 3,fillx", "", ""));
    
    panel1.add(btnNoSale, "growx");
    panel1.add(btnStoreStatus, "growx");
    panel1.add(btnOpenTickets, "growx");
    panel1.add(btnAuthorize, "growx");
    panel1.add(btnShowClockedInUsers, "growx");
    
    cashDrawerButtonPanel.add(btnTipsManagement, "growx");
    cashDrawerButtonPanel.add(btnDeclareTips, "growx");
    cashDrawerButtonPanel.add(btnAssignDrawer, "growx");
    cashDrawerButtonPanel.add(btnTerminalStatus, "growx");
    cashDrawerButtonPanel.add(btnStaffBankStatus, "growx");
    cashDrawerButtonPanel.add(btnStartStaffBank, "growx");
    
    buttonUserActionPanel.add(panel1, "growx" + wrap);
    buttonUserActionPanel.add(cashDrawerButtonPanel, "growx");
    
    buttonUserActionPanel.add(btnCancel, "newline,span 9,center,growx");
    
    centerPanel.add(buttonUserActionPanel, "South");
    centerPanel.add(clockInOutActionPanel, "Center");
    contentPanel.add(messagePanel, "aligny top,w " + width + "!,grow");
    
    JPanel headerPanel = new JPanel(new BorderLayout());
    headerPanel.setBorder(BorderFactory.createLineBorder(getBackground(), 3));
    headerPanel.setBackground(Color.white);
    
    JLabel logoLabel = new JLabel(IconFactory.getIcon("/icons/", "header_logo.png"));
    JSeparator sep = new JSeparator();
    sep.setBackground(Color.white);
    
    headerPanel.add(logoLabel, "West");
    headerPanel.add(sep, "South");
    
    add(headerPanel, "North");
    add(contentPanel);
    
    createFooterStatusPanel();
    
    initializeActions();
    
    showFooterTimer();
    clockTimer.start();
  }
  
  private void doDeclareTips() {
    StoreSession currentSession = StoreUtil.getCurrentStoreOperation().getCurrentData();
    List<DeclaredTips> declaredTips = DeclaredTipsDAO.getInstance().findBy(currentSession, currentUser);
    TipsDeclarationDialog dialog = new TipsDeclarationDialog(currentUser, currentSession, declaredTips);
    dialog.setTitle("DECLARE TIPS");
    dialog.setSize(PosUIManager.getSize(630, 550));
    dialog.open();
  }
  
  private void doAuthorizeTickets() {
    AuthorizableTicketBrowser dialog = new AuthorizableTicketBrowser(Application.getPosWindow(), currentUser, null);
    dialog.setDefaultCloseOperation(2);
    dialog.setLocationRelativeTo(Application.getPosWindow());
    dialog.openFullScreen();
  }
  
  private void doManageGratuity() {
    if ((!currentUser.hasPermission(UserPermission.PERFORM_ADMINISTRATIVE_TASK)) && (!currentUser.hasPermission(UserPermission.PERFORM_MANAGER_TASK))) {
      POSMessageDialog.showMessage(Application.getPosWindow(), "You have no permission to manage tips.");
      return;
    }
    GratuityDialog dialog = new GratuityDialog(this, currentUser);
    dialog.setCaption("Tips Management");
    dialog.setOkButtonText("PAY");
    dialog.setSize(PosUIManager.getSize(780, 550));
    if (dialog.isCanceled()) {
      dialog.open();
    }
  }
  
  protected void doShowClockedInUsers() {
    try {
      ClockedInUserListDialog userListDialog = new ClockedInUserListDialog(currentUser);
      userListDialog.pack();
      userListDialog.open();
      if (userListDialog.isDataUpdated()) {
        currentUser = UserDAO.getInstance().get(currentUser.getId());
        updateClockInOutButton(currentUser);
      }
    } catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage(), e);
    }
  }
  
  private void initializeActions() {
    btnOpenStore.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          try {
            StoreUtil.openStore(currentUser);
          } catch (StoreAlreadyOpenException e2) {
            POSMessageDialog.showError(ClockInOutDialog.this, e2.getMessage());
          }
          ClockInOutDialog.this.performClockIn(currentUser);
          ClockInOutDialog.this.updateClockInOutButton(currentUser);
        } catch (Exception e2) {
          POSMessageDialog.showError(ClockInOutDialog.this, e2.getMessage(), e2);
        }
        ClockInOutDialog.this.updateButtonStatus();
      }
    });
    btnStoreStatus.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        ClockInOutDialog.this.showDrawerAndStaffBankReportList();
      }
    });
    btnClockIn.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Are you sure you want to clock in?", "Confirm") != 0) {
          return;
        }
        ClockInOutDialog.this.performClockIn(currentUser);
        ClockInOutDialog.this.updateClockInOutButton(currentUser);
      }
    });
    btnClockOut.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        ClockInOutDialog.this.performClockOut();
      }
    });
    btnDriverIn.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        performDriverIn(currentUser);
        ClockInOutDialog.this.updateClockInOutButton(currentUser);



      }
      



    });
    btnExit.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        RootView.getInstance().showView(LoginView.getInstance());
      }
    });
    btnAssignDrawer.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (!currentUser.isClockedIn().booleanValue()) {
          POSMessageDialog.showMessage("You are not clock in.\nPlease clock in to assign drawer.");
          return;
        }
        DrawerAssignmentAction drawerAction = new DrawerAssignmentAction(Application.getInstance().getTerminal(), currentUser);
        drawerAction.execute();
        ClockInOutDialog.this.updateButtonStatus();
      }
    });
    btnTerminalStatus.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          Terminal terminal = Application.getInstance().getTerminal();
          CashDrawerInfoDialog statusDialog = new CashDrawerInfoDialog(terminal.getAssignedUser(), terminal.getCurrentCashDrawer());
          statusDialog.setTitle("DRAWER STATUS");
          statusDialog.refreshReport();
          statusDialog.setDefaultCloseOperation(2);
          statusDialog.openFullScreen();
          Application.getInstance().refreshAndGetTerminal();
          ClockInOutDialog.this.updateButtonStatus();
        } catch (Exception ex) {
          PosLog.error(getClass(), ex);
        }
      }
    });
    btnShowClockedInUsers.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        doShowClockedInUsers();
      }
    });
    btnStaffBankStatus.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        ClockInOutDialog.this.showStaffBankStatus();
      }
    });
    btnStartStaffBank.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        if (!currentUser.isClockedIn().booleanValue()) {
          POSMessageDialog.showError("Can't start staff bank. You are not clocked in.");
          return;
        }
        int option = POSMessageDialog.showYesNoQuestionDialog(ClockInOutDialog.this, "Are you sure to start staff bank?", "Confirm");
        if (option != 0) {
          return;
        }
        performStartStaffBank(currentUser);
      }
    });
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
    });
  }
  
  private void createFooterStatusPanel() {
    JPanel statusBarContainer = new JPanel(new BorderLayout());
    statusBarContainer.add(new JSeparator(0), "North");
    
    JPanel infoPanel = new JPanel(new MigLayout("fillx, ins 5", "[]20px[]20px[]", "[]"));
    infoPanel.setOpaque(true);
    
    lblRestaurantName = new JLabel();
    Font f = lblRestaurantName.getFont().deriveFont(1, PosUIManager.getFontSize(10));
    lblTerminal = new JLabel("Terminal: " + TerminalConfig.getTerminalId());
    lblUser = new JLabel();
    
    lblTime = new JLabel("");
    lblTime.setBorder(new EmptyBorder(5, 5, 5, 5));
    
    lblRestaurantName.setFont(f);
    lblUser.setFont(f);
    lblTerminal.setFont(f);
    lblTime.setFont(f);
    
    infoPanel.add(lblRestaurantName);
    infoPanel.add(lblTerminal);
    infoPanel.add(lblUser);
    
    statusBarContainer.add(infoPanel, "West");
    statusBarContainer.add(lblTime, "East");
    add(statusBarContainer, "South");
    if (TerminalConfig.isKioskMode()) {
      statusBarContainer.setVisible(false);
    }
  }
  
  public void rendererFooterInfo() {
    User currentUser = Application.getCurrentUser();
    if (currentUser != null) {
      lblUser.setText("");
    }
    else {
      lblUser.setText("");
    }
    lblRestaurantName.setText(Application.getInstance().getStore().getName());
  }
  
  public static ClockInOutDialog getInstance(User user, boolean loginMode) {
    if (instance == null) {
      instance = new ClockInOutDialog();
    }
    instance.updateView(user, loginMode);
    instance.rendererUserRole();
    return instance;
  }
  
  private void updateView(User selectedUser, boolean isLoginMode) {
    currentUser = selectedUser;
    loginMode = isLoginMode;
    if (selectedUser == null)
      return;
    lblUserName.setText(selectedUser.getFullName());
    
    String userOtherIno = "<html><center>" + selectedUser.getType().getName();
    userOtherIno = userOtherIno + "<br>Has staff bank: " + (selectedUser.isStaffBank().booleanValue() ? "yes" : "no");
    if (selectedUser.isStaffBank().booleanValue()) {
      userOtherIno = userOtherIno + "<br>Auto start staff bank: " + (selectedUser.isAutoStartStaffBank().booleanValue() ? "yes" : "no");
    }
    userOtherIno = userOtherIno + "</center></html>";
    lblUserType.setText(userOtherIno);
    ImageIcon memberImage = selectedUser.getImage();
    if (memberImage != null) {
      lblPicture.setIcon(memberImage);
    }
    else {
      setDefaultUserPicture();
    }
    updateClockInOutButton(selectedUser);
    rendererFooterInfo();
  }
  
  private void rendererUserRole() {
    loginRoleSelectionPanel.removeAll();
    loginRoleSelectionPanel.setBorder(null);
    
    if ((currentUser.isClockedIn().booleanValue()) || (!currentUser.hasLinkedUser())) {
      loginRoleSelectionPanel.setVisible(false);
      return;
    }
    List<User> linkedUserList = currentUser.getLinkedUsersIfExists();
    loginRoleSelectionPanel.setVisible(true);
    Border emptyBorder = new EmptyBorder(2, 5, 5, 5);
    loginRoleSelectionPanel.setBorder(BorderFactory.createCompoundBorder(
      BorderFactory.createTitledBorder(null, "CLOCK IN AS", 2, 2), emptyBorder));
    ButtonGroup roleButtonGroup = new ButtonGroup();
    for (User roleUser : linkedUserList) {
      POSToggleButton btnRole = new POSToggleButton(roleUser.getType().getName());
      btnRole.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          POSToggleButton btnRoleUser = (POSToggleButton)e.getSource();
          currentUser = ((User)btnRoleUser.getClientProperty("roleUser"));
          ClockInOutDialog.this.updateView(currentUser, loginMode);
        }
      });
      btnRole.putClientProperty("roleUser", roleUser);
      loginRoleSelectionPanel.add(btnRole, "growx");
      roleButtonGroup.add(btnRole);
      if (roleUser.getType().getName().equals(currentUser.getType().getName())) {
        btnRole.setSelected(true);
      }
      btnRole.setEnabled(!currentUser.isClockedIn().booleanValue());
    }
  }
  
  private void updateClockInOutButton(User user) {
    boolean storeOpen = StoreUtil.isStoreOpen();
    
    btnClockOut.setVisible((storeOpen) && (user.isClockedIn().booleanValue()));
    btnClockIn.setVisible((storeOpen) && (!user.isClockedIn().booleanValue()));
    btnDriverIn.setVisible(false);
    

    Date date = new Date();
    String currentDate = new SimpleDateFormat("dd MMMM, yyyy").format(date);
    
    if (user.isClockedIn().booleanValue()) {
      lblUserClockedStatus.setText("Status: Clocked in as " + user.getType().getName());
      lblCurrentTimeInfo.setText("<html><center><b>Clock in time</b></center>" + dateFormat2.format(user.getLastClockInTime()) + "</html>");
    }
    else {
      lblUserClockedStatus.setText("Status: Clocked out");
      lblCurrentTimeInfo.setText("<html><center>" + currentDate + "</center></html>");
    }
    
    if ((user.isDriver().booleanValue()) && (storeOpen) && 
      (user.isClockedIn().booleanValue()) && 
      (!user.isAvailableForDelivery().booleanValue()))
    {


      btnDriverIn.setVisible(true);
    }
    

    updateButtonStatus();
  }
  
  private void setDefaultUserPicture() {
    try {
      InputStream stream = getClass().getResourceAsStream("/images/generic-profile-pic-v2.png");
      byte[] picture2 = IOUtils.toByteArray(stream);
      IOUtils.closeQuietly(stream);
      lblPicture.setIcon(new ImageIcon(new ImageIcon(picture2).getImage().getScaledInstance(150, 160, 4)));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  private void showDrawerAndStaffBankReportList() {
    try {
      DrawerAndStaffBankReportDialog dialog = new DrawerAndStaffBankReportDialog(this.currentUser);
      dialog.setInfo("Cash drawer and staff bank status");
      dialog.open();
      updateButtonStatus();
      if (StoreUtil.isStoreOpen())
        return;
      User currentUser = Application.getCurrentUser();
      if ((currentUser == null) || (currentUser.getId().equals(currentUser.getId()))) {
        Application.getInstance().setCurrentUser(null);
        Application.getPosWindow().rendererUserInfo();
        RootView.getInstance().showView("LOGIN_VIEW");
        dispose();
      }
    } catch (Exception ex) {
      POSMessageDialog.showError(this, ex.getMessage(), ex);
    }
  }
  
  private void showStaffBankStatus() {
    try {
      CashDrawerInfoDialog dialog = new CashDrawerInfoDialog(currentUser, currentUser.getActiveDrawerPullReport());
      dialog.setTitle("BANK STATUS");
      dialog.refreshReport();
      dialog.setDefaultCloseOperation(2);
      dialog.openFullScreen();
      currentUser = UserDAO.getInstance().get(currentUser.getId());
      updateClockInOutButton(currentUser);
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public void setVisible(boolean aFlag)
  {
    super.setVisible(aFlag);
    
    if (aFlag) {
      startTimer();
    }
    else {
      stopTimer();
    }
  }
  
  private void startTimer() {
    clockTimer.start();
  }
  
  private void stopTimer() {
    clockTimer.stop();
  }
  

  private class ClockTimerHandler
    implements ActionListener
  {
    private ClockTimerHandler() {}
    
    public void actionPerformed(ActionEvent e)
    {
      ClockInOutDialog.this.showFooterTimer();
    }
  }
  
  private void showFooterTimer() {
    StringBuilder sb = new StringBuilder();
    sb.append(timeFormat.format(Calendar.getInstance().getTime()));
    lblTime.setText(sb.toString());
  }
  

  private void performClockOut()
  {
    try
    {
      if (TicketDAO.getInstance().hasOpenTickets(currentUser)) {
        ServerOpenTicketListDialog dialog = new ServerOpenTicketListDialog(currentUser, currentUser, true);
        dialog.setTitle("You have open tickets. Please review them.");
        dialog.setSize(PosUIManager.getSize(830, 550));
        dialog.open();
      }
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Are you sure you want to clock out?", "Confirm") != 0) {
        return;
      }
      CashDrawer activeDrawerPullReport = currentUser.getActiveDrawerPullReport();
      if ((activeDrawerPullReport != null) && (activeDrawerPullReport.getDrawerType() == DrawerType.STAFF_BANK) && (activeDrawerPullReport.isOpen())) {
        POSMessageDialog.showMessage(this, "Please close staff bank first");
        return;
      }
      doClockedOutUser(currentUser);
      updateClockInOutButton(currentUser);
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private void performClockIn(User clockInuser) {
    try {
      if (clockInuser == null) {
        return;
      }
      if ((clockInuser.isClockedIn() != null) && (clockInuser.isClockedIn().booleanValue())) {
        POSMessageDialog.showMessage(Application.getPosWindow(), clockInuser.getFirstName() + " " + clockInuser.getLastName() + 
          Messages.getString("ClockInOutAction.13"));
        return;
      }
      if (clockInuser.isAutoStartStaffBank().booleanValue()) {
        doClockedInUser(true);
      }
      else if (clockInuser.isStaffBank().booleanValue()) {
        final POSDialog dialog = new POSDialog(Application.getPosWindow(), true);
        dialog.setTitle(Messages.getString("ClockInOutAction.4"));
        
        PosButton btnStartStaffBank = new PosButton("START STAFF BANK");
        btnStartStaffBank.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e) {
            ClockInOutDialog.this.doClockedInUser(true);
            dialog.dispose();
          }
          
        });
        PosButton btnClose = new PosButton("CLOSE");
        btnClose.setPreferredSize(new Dimension(150, 120));
        btnClose.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e) {
            ClockInOutDialog.this.doClockedInUser(false);
            dialog.dispose();
          }
          
        });
        JPanel contentPane = (JPanel)dialog.getContentPane();
        contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
        contentPane.setLayout(new GridLayout(1, 0, 10, 10));
        contentPane.add(btnStartStaffBank);
        contentPane.add(btnClose);
        dialog.pack();
        dialog.open();
      }
      else {
        doClockedInUser(false);
      }
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private void doClockedInUser(boolean startStaffBank) {
    try {
      Shift currentShift = ShiftUtil.getCurrentShift();
      Calendar currentTime = Calendar.getInstance();
      
      currentUser.doClockIn(Application.getInstance().getTerminal(), currentShift, currentTime);
      

      String msg = currentUser.getFirstName() + " " + currentUser.getLastName() + Messages.getString("ClockInOutAction.16");
      if (POSMessageDialog.showMessageAndPromtToPrint(msg)) {
        ReceiptPrintService.printClockInOutReceipt(currentUser);
      }
      if (loginMode) {
        Application.getInstance().doLogin(currentUser);
      }
      if (startStaffBank) {
        performStartStaffBank(currentUser);
      }
      close();
      rendererUserRole();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private void close() {}
  
  private void doClockedOutUser(User user)
    throws Exception
  {
    try
    {
      if (user == null) {
        return;
      }
      boolean clockedIn = UserDAO.getInstance().isClockedIn(user);
      if (!clockedIn) {
        throw new PosException("User is not cloked in.");
      }
      AttendenceHistoryDAO attendenceHistoryDAO = new AttendenceHistoryDAO();
      AttendenceHistory attendenceHistory = attendenceHistoryDAO.findHistoryByClockedInTime(user);
      if (attendenceHistory == null) {
        attendenceHistory = new AttendenceHistory();
        Date lastClockInTime = user.getLastClockInTime();
        Calendar c = Calendar.getInstance();
        c.setTime(lastClockInTime);
        attendenceHistory.setClockInTime(lastClockInTime);
        attendenceHistory.setClockInHour(Short.valueOf((short)c.get(10)));
        attendenceHistory.setUser(user);
        attendenceHistory.setTerminal(Application.getInstance().getTerminal());
        attendenceHistory.setShift(user.getCurrentShift());
      }
      
      Shift shift = user.getCurrentShift();
      Calendar calendar = Calendar.getInstance();
      
      user.doClockOut(attendenceHistory, shift, calendar);
      
      String msg = user.getFirstName() + " " + user.getLastName() + Messages.getString("ClockInOutAction.10");
      if (POSMessageDialog.showMessageAndPromtToPrint(msg)) {
        ReceiptPrintService.printClockInOutReceipt(user);
      }
      User currentUser = Application.getCurrentUser();
      if ((currentUser != null) && (currentUser.getId().equals(user.getId()))) {
        Application.getInstance().setCurrentUser(null);
        Application.getPosWindow().rendererUserInfo();
        RootView.getInstance().showView(LoginView.getInstance());
        close();
      }
      rendererUserRole();
      close();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public void performDriverOut(User user) {
    try {
      if (user == null) {
        return;
      }
      
      Shift currentShift = ShiftUtil.getCurrentShift();
      Terminal terminal = Application.getInstance().getTerminal();
      
      Calendar currentTime = Calendar.getInstance();
      user.setAvailableForDelivery(Boolean.valueOf(false));
      user.setLastClockOutTime(currentTime.getTime());
      
      LogFactory.getLog(Application.class).info("terminal id befor saving clockIn=" + terminal.getId());
      
      EmployeeInOutHistory attendenceHistory = new EmployeeInOutHistory();
      attendenceHistory.setOutTime(currentTime.getTime());
      attendenceHistory.setOutHour(Short.valueOf((short)currentTime.get(11)));
      attendenceHistory.setClockOut(Boolean.valueOf(true));
      attendenceHistory.setUser(user);
      attendenceHistory.setTerminal(terminal);
      attendenceHistory.setShift(currentShift);
      
      UserDAO.getInstance().saveDriverOut(user, attendenceHistory, currentShift, currentTime);
      
      POSMessageDialog.showMessage("Driver " + user.getFirstName() + " " + user.getLastName() + " is out for delivery.");
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public void performDriverIn(User user) {
    try {
      if (user == null) {
        return;
      }
      if (!user.isClockedIn().booleanValue()) {
        POSMessageDialog.showMessage(this, Messages.getString("ClockInOutAction.2"));
        return;
      }
      
      EmployeeInOutHistoryDAO attendenceHistoryDAO = new EmployeeInOutHistoryDAO();
      EmployeeInOutHistory attendenceHistory = attendenceHistoryDAO.findDriverHistoryByClockedInTime(user);
      if (attendenceHistory == null) {
        attendenceHistory = new EmployeeInOutHistory();
        Date lastClockOutTime = user.getLastClockOutTime();
        Calendar c = Calendar.getInstance();
        c.setTime(lastClockOutTime);
        attendenceHistory.setOutTime(lastClockOutTime);
        attendenceHistory.setOutHour(Short.valueOf((short)c.get(10)));
        attendenceHistory.setUser(user);
        attendenceHistory.setTerminal(Application.getInstance().getTerminal());
        attendenceHistory.setShift(user.getCurrentShift());
      }
      
      Shift shift = user.getCurrentShift();
      Calendar calendar = Calendar.getInstance();
      
      user.setAvailableForDelivery(Boolean.valueOf(true));
      
      attendenceHistory.setClockOut(Boolean.valueOf(false));
      attendenceHistory.setInTime(calendar.getTime());
      attendenceHistory.setInHour(Short.valueOf((short)calendar.get(11)));
      
      UserDAO.getInstance().saveDriverIn(user, attendenceHistory, shift, calendar);
      
      POSMessageDialog.showMessage("Driver " + user.getFirstName() + " " + user.getLastName() + " is in after delivery");
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public boolean isLoginMode() {
    return loginMode;
  }
  
  public void setLoginMode(boolean loginMode) {
    this.loginMode = loginMode;
  }
  
  public void performStartStaffBank(User bankUser) {
    try {
      CashDrawer staffBankReport = new CashDrawer();
      staffBankReport.setStartTime(new Date());
      staffBankReport.setAssignedUser(bankUser);
      staffBankReport.setTerminal(Application.getInstance().getTerminal());
      staffBankReport.setStoreOperationData(StoreUtil.getCurrentStoreOperation().getCurrentData());
      staffBankReport.setDrawerType(DrawerType.STAFF_BANK);
      
      staffBankReport.setAssignedBy(bankUser);
      
      bankUser.setStaffBankStarted(Boolean.valueOf(true));
      bankUser.setCurrentCashDrawer(staffBankReport);
      
      TerminalDAO.getInstance().performBatchSave(new Object[] { staffBankReport, bankUser });
      POSMessageDialog.showMessage("Your staff bank has been started successfully");
      updateClockInOutButton(currentUser);
      Application.getInstance().refreshCurrentUser();
    } catch (Exception e) {
      POSMessageDialog.showError(this, "An error occured while starting staff bank", e);
    }
  }
  
  private void updateButtonStatus() {
    Terminal currentTerminal = Application.getInstance().getTerminal();
    
    boolean hasStoreAccess = currentUser.hasPermission(UserPermission.OPEN_CLOSE_STORE);
    boolean storeOpen = StoreUtil.isStoreOpen();
    boolean userClockedIn = currentUser.isClockedIn().booleanValue();
    
    btnStoreStatus.setVisible((storeOpen) && (hasStoreAccess) && (userClockedIn));
    btnOpenStore.setVisible((!storeOpen) && (hasStoreAccess));
    lblOpenStoreStatus.setVisible((!storeOpen) && (!hasStoreAccess));
    btnOpenTickets.setVisible((storeOpen) && (userClockedIn));
    btnShowClockedInUsers.setVisible(storeOpen);
    btnTipsManagement.setVisible((storeOpen) && (userClockedIn) && (currentUser.isManager()));
    btnDeclareTips.setVisible((storeOpen) && (userClockedIn));
    btnAuthorize.setVisible((storeOpen) && (userClockedIn) && (currentUser.hasPermission(UserPermission.AUTHORIZE_TICKETS)));
    
    btnStartStaffBank.setVisible((userClockedIn) && (currentUser.isStaffBank().booleanValue()) && (!currentUser.isStaffBankStarted().booleanValue()));
    btnStaffBankStatus.setVisible((userClockedIn) && (currentUser.isStaffBankStarted().booleanValue()));
    
    if (((currentUser.isAdministrator()) || (currentUser.isManager())) && (userClockedIn) && (currentTerminal.isHasCashDrawer().booleanValue())) {
      if (currentTerminal.isCashDrawerAssigned()) {
        btnAssignDrawer.setVisible(false);
        btnTerminalStatus.setVisible(true);
      }
      else {
        btnAssignDrawer.setVisible(true);
        btnTerminalStatus.setVisible(false);
      }
    }
    else {
      btnAssignDrawer.setVisible(false);
      btnTerminalStatus.setVisible(false);
    }
    
    cashDrawerButtonPanel.setVisible((btnTipsManagement.isVisible()) || (btnDeclareTips.isVisible()) || (btnAssignDrawer.isVisible()) || 
      (btnTerminalStatus.isVisible()) || (btnStartStaffBank.isVisible()) || (btnStaffBankStatus.isVisible()));
  }
}
