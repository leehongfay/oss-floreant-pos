package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.actions.ClockInOutAction;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.config.ui.DatabaseConfigurationDialog;
import com.floreantpos.main.Application;
import com.floreantpos.model.User;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.PasswordEntryDialog;
import com.floreantpos.util.ShiftException;
import com.floreantpos.util.UserNotFoundException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


























class LoginPasswordEntryView
  extends JPanel
{
  private PosButton btnConfigureDatabase;
  private PosButton btnShutdown;
  private JPanel buttonPanel;
  private JPanel bottomPanel;
  
  LoginPasswordEntryView()
  {
    initComponents();
    
    applyComponentOrientation(ComponentOrientation.getOrientation(Locale.getDefault()));
  }
  






  private void initComponents()
  {
    buttonPanel = new JPanel();
    bottomPanel = new JPanel();
    
    btnShutdown = new PosButton();
    
    setPreferredSize(new Dimension(320, 593));
    setLayout(new BorderLayout());
    
    buttonPanel.setOpaque(false);
    buttonPanel.setPreferredSize(new Dimension(200, 180));
    buttonPanel.setLayout(new MigLayout("", "[111px][111px][111px,grow]", "[60px][60px][60px][60px]"));
    
    lblTerminalId = new JLabel("TERMINAL ID:");
    lblTerminalId.setForeground(Color.BLACK);
    lblTerminalId.setFont(new Font("Dialog", 1, 18));
    lblTerminalId.setHorizontalAlignment(0);
    add(lblTerminalId, "North");
    
    bottomPanel.setLayout(new MigLayout("hidemode 3, fill"));
    bottomPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
    
    ButtonGroup buttonGroup = new ButtonGroup();
    buttonGroup.add(btnRegularMode);
    
    buttonGroup.add(btnKitchenMode);
    
    btnRegularMode.setFocusable(false);
    
    btnKitchenMode.setFocusable(false);
    
    ModeSelectionListener l = new ModeSelectionListener();
    btnRegularMode.addActionListener(l);
    
    btnKitchenMode.addActionListener(l);
    
    btnRegularMode.setSelected(TerminalConfig.isRegularMode());
    
    btnKitchenMode.setSelected(TerminalConfig.isKitchenMode());
    
    if ((!btnRegularMode.isSelected()) && (!btnKitchenMode.isSelected())) {
      btnRegularMode.setSelected(true);
    }
    
    JPanel modePanel = new JPanel(new GridLayout(1, 0, 2, 2));
    modePanel.add(btnRegularMode);
    
    modePanel.add(btnKitchenMode);
    
    bottomPanel.add(modePanel, "h 60!, grow, wrap");
    
    psbtnLogin = new PosButton();
    psbtnLogin.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doLogin();
      }
    });
    psbtnLogin.setText("LOGIN");
    bottomPanel.add(psbtnLogin, "grow, wrap, gapbottom 20px");
    
    PosButton btnClockOUt = new PosButton(new ClockInOutAction(false, true));
    bottomPanel.add(btnClockOUt, "grow, wrap, h 60!");
    
    if (TerminalConfig.isShowDbConfigureButton()) {
      btnConfigureDatabase = new PosButton();
      btnConfigureDatabase.setAction(goAction);
      btnConfigureDatabase.setText(POSConstants.CONFIGURE_DATABASE);
      btnConfigureDatabase.setFocusable(false);
      btnConfigureDatabase.setActionCommand("DBCONFIG");
      bottomPanel.add(btnConfigureDatabase, "grow, wrap, h 60!");
    }
    
    btnShutdown.setAction(goAction);
    btnShutdown.setText(POSConstants.SHUTDOWN);
    btnShutdown.setFocusable(false);
    
    if (TerminalConfig.isKioskMode()) {
      if (btnConfigureDatabase != null) {
        btnConfigureDatabase.setVisible(false);
      }
      if (btnShutdown != null) {
        btnShutdown.setVisible(false);
      }
    }
    
    bottomPanel.add(btnShutdown, "grow, wrap, h 60!");
    add(bottomPanel, "South");
    
    lblTerminalId.setText("");
  }
  
  public synchronized void doLogin() {
    try {
      User user = PasswordEntryDialog.getUser(Application.getPosWindow(), "", "");
      if (user == null) {
        return;
      }
      Application application = Application.getInstance();
      application.doLogin(user);
    }
    catch (UserNotFoundException e) {
      LogFactory.getLog(Application.class).error(e);
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("LoginPasswordEntryView.15"));
    } catch (ShiftException e) {
      LogFactory.getLog(Application.class).error(e);
      MessageDialog.showError(e.getMessage());
    } catch (Exception e1) {
      LogFactory.getLog(Application.class).error(e1);
      String message = e1.getMessage();
      
      if ((message != null) && (message.contains("Cannot open connection"))) {
        MessageDialog.showError("", e1);
        DatabaseConfigurationDialog.show(Application.getPosWindow());
      }
      else {
        MessageDialog.showError("", e1);
      }
    }
  }
  

  public void setTerminalId(int terminalId)
  {
    lblTerminalId.setText(Messages.getString("LoginPasswordEntryView.19") + terminalId);
  }
  








  Action goAction = new AbstractAction()
  {
    public void actionPerformed(ActionEvent e) {
      String command = e.getActionCommand();
      if (POSConstants.LOGIN.equals(command)) {
        doLogin();
      }
      else if (POSConstants.SHUTDOWN.equals(command)) {
        Application.getInstance().shutdownPOS();
      }
      else if ("DBCONFIG".equalsIgnoreCase(command)) {
        DatabaseConfigurationDialog.show(Application.getPosWindow());
      }
    }
  };
  
  private PosButton psbtnLogin;
  
  private JLabel lblTerminalId;
  private JToggleButton btnRegularMode = new JToggleButton("<html><center>REGULAR<br/>MODE</center></html>");
  
  private JToggleButton btnKitchenMode = new JToggleButton("<html><center>KITCHEN<br/>MODE</center></html>");
  
  class ModeSelectionListener implements ActionListener {
    ModeSelectionListener() {}
    
    public void actionPerformed(ActionEvent e) {
      TerminalConfig.setRegularMode(btnRegularMode.isSelected());
      
      TerminalConfig.setKitchenMode(btnKitchenMode.isSelected());
    }
  }
}
