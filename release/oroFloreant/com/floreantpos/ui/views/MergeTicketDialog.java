package com.floreantpos.ui.views;

import com.floreantpos.POSConstants;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.ticket.TicketViewerTable;
import com.floreantpos.ui.ticket.TicketViewerTableModel;
import com.floreantpos.util.NumberUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;





public class MergeTicketDialog
  extends POSDialog
{
  private List<Ticket> tickets;
  private Ticket mainTicket;
  private JTextField tfSubtotal;
  private JTextField tfTax;
  private JTextField tfDiscount;
  private JTextField tfTotal;
  
  public MergeTicketDialog(List<Ticket> tickets, Ticket mainTicket)
  {
    this.tickets = tickets;
    this.mainTicket = mainTicket;
    doMergeTickets();
    initComponents();
    updateView();
  }
  
  private void doMergeTickets() {
    try {
      mainTicket.getDiscounts().clear();
      for (iterator = tickets.iterator(); iterator.hasNext();) {
        Ticket ticket = (Ticket)iterator.next();
        
        if (ticket.equals(mainTicket)) {
          iterator.remove();
        }
        else
        {
          List<TicketItem> ticketItems = ticket.getTicketItems();
          if ((ticketItems != null) && (ticketItems.size() > 0)) {
            for (TicketItem ticketItem : ticketItems) {
              cloneTicketItem = ticketItem.cloneAsNew();
              cloneTicketItem.setTicket(mainTicket);
              mainTicket.addToticketItems(cloneTicketItem);
            }
          }
          
          Object discounts = ticket.getDiscounts();
          if ((discounts != null) && (((List)discounts).size() > 0)) {
            ticket.getDiscounts().clear();
          }
          
          List<Integer> tableNumbers = ticket.getTableNumbers();
          for (Integer table : tableNumbers)
            if (!mainTicket.getTableNumbers().contains(table))
            {

              mainTicket.getTableNumbers().add(table); }
        }
      }
    } catch (Exception e) { Iterator iterator;
      TicketItem cloneTicketItem;
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(10, 10));
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Merge preview");
    add(titlePanel, "North");
    TransparentPanel ticketTableViewPanel = new TransparentPanel(new BorderLayout(5, 5));
    ticketTableViewPanel.setBorder(new EmptyBorder(0, 10, 0, 10));
    
    TicketViewerTable ticketViewerTable = new TicketViewerTable();
    ticketViewerTable.getModel().setTicket(mainTicket);
    
    PosScrollPane scrollPane = new PosScrollPane();
    scrollPane.setHorizontalScrollBarPolicy(31);
    scrollPane.setVerticalScrollBarPolicy(20);
    scrollPane.setViewportView(ticketViewerTable);
    ticketTableViewPanel.add(scrollPane, "Center");
    
    TransparentPanel ticketTotalPanel = new TransparentPanel(new MigLayout("wrap 2,right,inset 5 5 5 "));
    
    JLabel lblSubtotal = new JLabel(POSConstants.SUBTOTAL + ":");
    JLabel lblDiscount = new JLabel(POSConstants.DISCOUNT + ":");
    JLabel lblTax = new JLabel(POSConstants.TAX + ":");
    JLabel lblTotal = new JLabel(POSConstants.TOTAL + ":");
    
    tfSubtotal = new JTextField();
    tfSubtotal.setHorizontalAlignment(11);
    tfSubtotal.setColumns(10);
    
    tfTax = new JTextField();
    tfTax.setHorizontalAlignment(11);
    tfTax.setColumns(10);
    
    tfDiscount = new JTextField();
    tfDiscount.setHorizontalAlignment(11);
    tfDiscount.setColumns(10);
    
    tfTotal = new JTextField();
    tfTotal.setHorizontalAlignment(11);
    tfTotal.setColumns(10);
    
    lblSubtotal.setHorizontalAlignment(4);
    ticketTotalPanel.add(lblSubtotal);
    
    tfSubtotal.setEditable(false);
    ticketTotalPanel.add(tfSubtotal);
    
    lblDiscount.setHorizontalAlignment(4);
    ticketTotalPanel.add(lblDiscount);
    
    tfDiscount.setEditable(false);
    ticketTotalPanel.add(tfDiscount);
    
    lblTax.setHorizontalAlignment(4);
    ticketTotalPanel.add(lblTax);
    
    tfTax.setEditable(false);
    ticketTotalPanel.add(tfTax);
    
    lblTotal.setHorizontalAlignment(4);
    ticketTotalPanel.add(lblTotal);
    
    tfTotal.setEditable(false);
    ticketTotalPanel.add(tfTotal);
    
    ticketTableViewPanel.add(ticketTotalPanel, "South");
    add(ticketTableViewPanel, "Center");
    
    JPanel bottomPanel = new JPanel(new MigLayout("center"));
    PosButton btnOk = new PosButton("Done");
    btnOk.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MergeTicketDialog.this.doMergeTicket();
      }
    });
    PosButton btnCancel = new PosButton("Cancel");
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setCanceled(true);
        dispose();
      }
    });
    bottomPanel.add(btnOk);
    bottomPanel.add(btnCancel);
    
    add(bottomPanel, "South");
    resizeColumnWidth(ticketViewerTable);
  }
  
  private void doMergeTicket()
  {
    try {
      TicketDAO.getInstance().saveMergedTickets(tickets, mainTicket);
      setCanceled(false);
      dispose();
    } catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  public void updateView() {
    if ((mainTicket == null) || (mainTicket.getTicketItems() == null) || (mainTicket.getTicketItems().size() <= 0)) {
      tfSubtotal.setText("");
      tfDiscount.setText("");
      tfTax.setText("");
      tfTotal.setText("");
      return;
    }
    
    mainTicket.calculatePrice();
    tfSubtotal.setText(NumberUtil.formatNumber(mainTicket.getSubtotalAmount()));
    tfDiscount.setText(NumberUtil.formatNumber(mainTicket.getDiscountAmount()));
    tfTax.setText(NumberUtil.formatNumber(mainTicket.getTaxAmount()));
    tfTotal.setText(NumberUtil.formatNumber(mainTicket.getTotalAmountWithTips()));
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(200));
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(100));
    
    return columnWidth;
  }
  
  public Ticket getMainTicket() {
    return mainTicket;
  }
}
