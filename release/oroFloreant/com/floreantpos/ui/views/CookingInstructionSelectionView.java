package com.floreantpos.ui.views;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.CookingInstruction;
import com.floreantpos.model.TicketItemCookingInstruction;
import com.floreantpos.model.dao.CookingInstructionDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.QwertyKeyPad;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang3.StringUtils;




















public class CookingInstructionSelectionView
  extends OkCancelOptionDialog
{
  private JTable table;
  private List<TicketItemCookingInstruction> ticketItemCookingInstructions;
  private JTextField tfCookingInstruction = new JTextField();
  private List<CookingInstruction> cookingInstructions;
  
  public CookingInstructionSelectionView() {
    super(Application.getPosWindow(), true);
    createUI();
    updateView();
  }
  
  private void createUI() {
    setTitle(Messages.getString("CookingInstructionSelectionView.1"));
    setCaption(Messages.getString("CookingInstructionSelectionView.1"));
    getContentPanel().setBorder(new EmptyBorder(10, 20, 0, 20));
    
    JScrollPane scrollPane = new JScrollPane();
    table = new JTable();
    table.setRowHeight(35);
    scrollPane.setViewportView(table);
    
    table.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        int index = table.getSelectedRow();
        if (index < 0)
          return;
        CookingInstructionSelectionView.CookingInstructionTableModel model = (CookingInstructionSelectionView.CookingInstructionTableModel)table.getModel();
        CookingInstruction cookingInstruction = (CookingInstruction)rowsList.get(index);
        tfCookingInstruction.setText(cookingInstruction.getDescription());
      }
      
    });
    tfCookingInstruction.addKeyListener(new KeyListener()
    {
      public void keyTyped(KeyEvent e)
      {
        CookingInstructionSelectionView.this.doFilter();
      }
      
      public void keyReleased(KeyEvent e)
      {
        CookingInstructionSelectionView.this.doFilter();
      }
      




      public void keyPressed(KeyEvent e) {}
    });
    PosButton btnSave = new PosButton(IconFactory.getIcon("save.png"));
    btnSave.setText(POSConstants.SAVE.toUpperCase());
    btnSave.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        String instruction = tfCookingInstruction.getText();
        if ((instruction == null) || (instruction.isEmpty())) {
          POSMessageDialog.showMessage(Application.getPosWindow(), "Instruction cannot be empty.");
          return;
        }
        if (instruction.length() > 60) {
          POSMessageDialog.showMessage(Application.getPosWindow(), "Instruction length must be less than 60 characters.");
          return;
        }
        CookingInstruction cookingInstruction = new CookingInstruction();
        cookingInstruction.setDescription(instruction);
        CookingInstructionDAO.getInstance().save(cookingInstruction);
        updateView();
        CookingInstructionSelectionView.CookingInstructionTableModel model = (CookingInstructionSelectionView.CookingInstructionTableModel)table.getModel();
        table.getSelectionModel().addSelectionInterval(model.getRowCount() - 1, model.getRowCount() - 1);
      }
    });
    JPanel contentPanel = new JPanel(new MigLayout("fill,wrap 1,inset 0"));
    contentPanel.add(scrollPane, "grow");
    contentPanel.add(tfCookingInstruction, "h 35!,split 2,grow");
    contentPanel.add(btnSave, "h 35!,w 90!");
    QwertyKeyPad keyPad = new QwertyKeyPad();
    contentPanel.add(keyPad, "grow");
    getContentPanel().add(contentPanel);
  }
  
  private void doFilter() {
    String text = tfCookingInstruction.getText().toLowerCase();
    List<CookingInstruction> filteredInstructions = new ArrayList();
    for (CookingInstruction i : cookingInstructions) {
      String description = i.getDescription().toLowerCase();
      if (description.contains(text)) {
        filteredInstructions.add(i);
      }
    }
    CookingInstructionTableModel model = (CookingInstructionTableModel)table.getModel();
    rowsList = filteredInstructions;
    model.fireTableDataChanged();
  }
  
  public void doOk()
  {
    int[] selectedRows = table.getSelectedRows();
    





    if (ticketItemCookingInstructions == null) {
      ticketItemCookingInstructions = new ArrayList(selectedRows.length);
    }
    
    CookingInstructionTableModel model = (CookingInstructionTableModel)table.getModel();
    String inputInstruction = tfCookingInstruction.getText();
    if (selectedRows.length == 1) {
      if ((inputInstruction == null) || (inputInstruction.isEmpty())) {
        POSMessageDialog.showMessage(Application.getPosWindow(), "Instruction cannot be empty.");
        return;
      }
      CookingInstruction ci = (CookingInstruction)rowsList.get(selectedRows[0]);
      if (!inputInstruction.equals(ci.getDescription())) {
        TicketItemCookingInstruction cookingInstruction = new TicketItemCookingInstruction();
        cookingInstruction.setDescription(inputInstruction);
        ticketItemCookingInstructions.add(cookingInstruction);
      }
      else {
        TicketItemCookingInstruction cookingInstruction = new TicketItemCookingInstruction();
        cookingInstruction.setDescription(ci.getDescription());
        ticketItemCookingInstructions.add(cookingInstruction);
      }
    }
    else if (selectedRows.length == 0) {
      if ((inputInstruction == null) || (inputInstruction.isEmpty())) {
        POSMessageDialog.showMessage(Application.getPosWindow(), "Instruction cannot be empty.");
        return;
      }
      if (inputInstruction.length() > 60) {
        POSMessageDialog.showMessage(Application.getPosWindow(), "Instruction length must be less than 60 characters.");
        return;
      }
      TicketItemCookingInstruction cookingInstruction = new TicketItemCookingInstruction();
      cookingInstruction.setDescription(inputInstruction);
      ticketItemCookingInstructions.add(cookingInstruction);
    }
    else {
      boolean newInstruction = false;
      for (int i = 0; i < selectedRows.length; i++) {
        CookingInstruction ci = (CookingInstruction)rowsList.get(selectedRows[i]);
        TicketItemCookingInstruction cookingInstruction = new TicketItemCookingInstruction();
        cookingInstruction.setDescription(ci.getDescription());
        if ((StringUtils.isNotEmpty(inputInstruction)) && (!ci.getDescription().equals(inputInstruction))) {
          newInstruction = true;
        }
        ticketItemCookingInstructions.add(cookingInstruction);
      }
      if (newInstruction) {
        TicketItemCookingInstruction cookingInstruction = new TicketItemCookingInstruction();
        cookingInstruction.setDescription(inputInstruction);
        ticketItemCookingInstructions.add(cookingInstruction);
      }
    }
    setCanceled(false);
    dispose();
  }
  
  protected void updateView() {
    cookingInstructions = CookingInstructionDAO.getInstance().findAll();
    table.setModel(new CookingInstructionTableModel(cookingInstructions));
  }
  
  public List<TicketItemCookingInstruction> getTicketItemCookingInstructions() {
    return ticketItemCookingInstructions;
  }
  
  class CookingInstructionTableModel extends AbstractTableModel {
    private final String[] columns = { Messages.getString("CookingInstructionSelectionView.2") };
    
    private List<CookingInstruction> rowsList;
    
    public CookingInstructionTableModel() {}
    
    public CookingInstructionTableModel()
    {
      rowsList = rows;
    }
    
    public int getRowCount()
    {
      if (rowsList == null) {
        return 0;
      }
      
      return rowsList.size();
    }
    
    public int getColumnCount()
    {
      return columns.length;
    }
    
    public String getColumnName(int column)
    {
      return columns[column];
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      if (rowsList == null) {
        return null;
      }
      
      CookingInstruction row = (CookingInstruction)rowsList.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return row.getDescription();
      }
      return null;
    }
  }
}
