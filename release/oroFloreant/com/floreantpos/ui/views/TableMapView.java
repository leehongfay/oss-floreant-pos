package com.floreantpos.ui.views;

import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.FloorLayoutPlugin;
import com.floreantpos.model.OrderType;
import com.floreantpos.ui.tableselection.DefaultTableSelectionView;
import com.floreantpos.ui.tableselection.TableSelector;
import com.floreantpos.ui.views.order.ViewPanel;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.util.Locale;





























public class TableMapView
  extends ViewPanel
{
  public static final String VIEW_NAME = "TABLE_MAP";
  private TableSelector tableSelector = null;
  private static TableMapView instance;
  
  private TableMapView() {
    initComponents();
    
    applyComponentOrientation(ComponentOrientation.getOrientation(Locale.getDefault()));
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    
    FloorLayoutPlugin floorLayoutPlugin = (FloorLayoutPlugin)ExtensionManager.getPlugin(FloorLayoutPlugin.class);
    if (floorLayoutPlugin == null) {
      tableSelector = new DefaultTableSelectionView();
    }
    else
    {
      tableSelector = floorLayoutPlugin.createTableSelector();
    }
    tableSelector.setCreateNewTicket(true);
    tableSelector.updateView(false);
    add(tableSelector, "Center");
  }
  
  public DataChangeListener getDataChangeListener()
  {
    return (DataChangeListener)tableSelector;
  }
  
  public void updateView() {
    tableSelector.rendererTables();
  }
  
  private static TableMapView getInstance() {
    if (instance == null) {
      instance = new TableMapView();
    }
    
    return instance;
  }
  
  public static TableMapView getInstance(OrderType orderType) {
    TableMapView instance2 = getInstance();
    tableSelector.setOrderType(orderType);
    
    return instance2;
  }
  
  public String getViewName()
  {
    return "TABLE_MAP";
  }
}
