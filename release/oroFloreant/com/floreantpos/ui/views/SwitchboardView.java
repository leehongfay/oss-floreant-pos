package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.actions.GroupSettleTicketAction;
import com.floreantpos.actions.MergeTicketsAction;
import com.floreantpos.actions.RefundAction;
import com.floreantpos.actions.SendToKitchenAction;
import com.floreantpos.actions.SettleTicketAction;
import com.floreantpos.actions.ShowOrderInfoAction;
import com.floreantpos.actions.SplitTicketAction;
import com.floreantpos.actions.TicketCloseAction;
import com.floreantpos.actions.TicketEditAction;
import com.floreantpos.actions.TicketReorderAction;
import com.floreantpos.actions.TicketTransferAction;
import com.floreantpos.actions.TransferTicketItemsAction;
import com.floreantpos.actions.VoidPaymentAction;
import com.floreantpos.actions.VoidTicketAction;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.main.Application;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PaymentStatusFilter;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.UserType;
import com.floreantpos.swing.OrderTypeButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TicketListUpdateListener;
import com.floreantpos.ui.order.TicketListView;
import com.floreantpos.ui.views.order.DefaultOrderServiceExtension;
import com.floreantpos.ui.views.order.ViewPanel;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXCollapsiblePane;



























public class SwitchboardView
  extends ViewPanel
  implements TicketListUpdateListener
{
  public static final String VIEW_NAME = POSConstants.ORDERS;
  
  private PosButton btnEditTicket = new PosButton(POSConstants.EDIT_TICKET_BUTTON_TEXT);
  private PosButton btnSendTicket = new PosButton("SEND");
  private PosButton btnGroupSettle = new PosButton(POSConstants.GROUP_SETTLE_BUTTON_TEXT);
  private PosButton btnOrderInfo = new PosButton(POSConstants.ORDER_INFO_BUTTON_TEXT);
  
  private PosButton btnReorder = new PosButton("REORDER");
  private PosButton btnTransferServer = new PosButton("TRANSFER SERVER");
  private PosButton btnSettleTicket = new PosButton(POSConstants.SETTLE_TICKET_BUTTON_TEXT);
  private PosButton btnSplitTicket = new PosButton(POSConstants.SPLIT_TICKET_BUTTON_TEXT);
  private PosButton btnMergeTickets = new PosButton("MERGE");
  private PosButton btnVoidTicket = new PosButton(POSConstants.VOID_TICKET_BUTTON_TEXT);
  private PosButton btnRefundTicket = new PosButton(POSConstants.REFUND_BUTTON_TEXT);
  private PosButton btnReversePayment = new PosButton();
  private PosButton btnCloseOrder = new PosButton(POSConstants.CLOSE_ORDER_BUTTON_TEXT);
  private PosButton btnTransferItems = new PosButton("TRANSFER ITEMS");
  
  private TicketListView ticketList = new TicketListView();
  private TitledBorder ticketsListPanelBorder;
  private static SwitchboardView instance;
  
  private SwitchboardView()
  {
    initComponents();
    initActions();
    applyComponentOrientation(ComponentOrientation.getOrientation(Locale.getDefault()));
  }
  
  private void initActions() {
    btnEditTicket.setAction(new TicketEditAction(ticketList));
    btnGroupSettle.addActionListener(new GroupSettleTicketAction(ticketList));
    btnOrderInfo.addActionListener(new ShowOrderInfoAction(ticketList));
    
    btnReorder.addActionListener(new TicketReorderAction(ticketList));
    btnTransferServer.addActionListener(new TicketTransferAction(ticketList));
    btnSettleTicket.setAction(new SettleTicketAction(ticketList));
    btnSplitTicket.addActionListener(new SplitTicketAction(ticketList));
    btnMergeTickets.addActionListener(new MergeTicketsAction());
    btnVoidTicket.setAction(new VoidTicketAction(ticketList));
    btnRefundTicket.setAction(new RefundAction(ticketList));
    btnCloseOrder.setAction(new TicketCloseAction(ticketList));
    btnReversePayment.setAction(new VoidPaymentAction(ticketList));
    btnSendTicket.setAction(new SendToKitchenAction(ticketList));
    btnTransferItems.addActionListener(new TransferTicketItemsAction());
  }
  
  public static SwitchboardView getInstance() {
    if (instance == null) {
      instance = new SwitchboardView();
    }
    return instance;
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(10, 10));
    JPanel centerPanel = new JPanel(new BorderLayout(5, 5));
    centerPanel.add(createActivityPanel(), "Center");
    centerPanel.add(createRightOrdersPanel(), "East");
    add(centerPanel, "Center");
  }
  
  private JPanel createRightOrdersPanel() {
    JPanel rightOrderButtonPanel = new JPanel(new BorderLayout(20, 20));
    TitledBorder titledBorder2 = BorderFactory.createTitledBorder(null, "-", 2, 0);
    rightOrderButtonPanel.setBorder(new CompoundBorder(titledBorder2, new EmptyBorder(0, 2, 2, 2)));
    
    JPanel orderPanel = new JPanel(new MigLayout("ins 2 2 0 2, fill, hidemode 3, flowy", "fill, grow", ""));
    List<OrderType> orderTypes = Application.getInstance().getOrderTypes();
    for (OrderType orderType : orderTypes) {
      orderPanel.add(new OrderTypeButton(orderType), "grow");
    }
    rightOrderButtonPanel.add(orderPanel);
    rightOrderButtonPanel.setMinimumSize(PosUIManager.getSize(120, 0));
    return rightOrderButtonPanel;
  }
  
  private JPanel createActivityPanel() {
    JPanel ticketsAndActivityPanel = new JPanel(new BorderLayout(5, 5));
    ticketsListPanelBorder = BorderFactory.createTitledBorder(null, POSConstants.OPEN_TICKETS_AND_ACTIVITY, 2, 0);
    

    ticketsAndActivityPanel.setBorder(new CompoundBorder(ticketsListPanelBorder, new EmptyBorder(2, 1, 2, 1)));
    ticketsAndActivityPanel.add(ticketList, "Center");
    
    JPanel activityPanel = new JPanel(new BorderLayout(2, 5));
    JPanel innerActivityPanel = new JPanel(new MigLayout("hidemode 3, fill, ins 0", "fill, grow", ""));
    
    JPanel firstRowButtonPanel = new JPanel(new GridLayout(1, 0, 5, 5));
    final JXCollapsiblePane secondRowButtonPanel = new JXCollapsiblePane();
    secondRowButtonPanel.setAnimated(false);
    secondRowButtonPanel.setCollapsed(true);
    secondRowButtonPanel.setVisible(false);
    secondRowButtonPanel.getContentPane().setLayout(new GridLayout(1, 0, 5, 5));
    
    if (Application.getInstance().getTerminal().isHasCashDrawer().booleanValue()) {
      firstRowButtonPanel.add(btnOrderInfo);
      firstRowButtonPanel.add(btnEditTicket);
      firstRowButtonPanel.add(btnSendTicket);
      
      firstRowButtonPanel.add(btnSettleTicket);
      firstRowButtonPanel.add(btnSplitTicket);
      firstRowButtonPanel.add(btnCloseOrder);
      
      secondRowButtonPanel.getContentPane().add(btnMergeTickets);
      secondRowButtonPanel.getContentPane().add(btnTransferServer);
      secondRowButtonPanel.getContentPane().add(btnTransferItems);
      secondRowButtonPanel.getContentPane().add(btnGroupSettle);
      secondRowButtonPanel.getContentPane().add(btnReorder);

    }
    else
    {

      firstRowButtonPanel.add(btnOrderInfo);
      firstRowButtonPanel.add(btnEditTicket);
      firstRowButtonPanel.add(btnSendTicket);
      firstRowButtonPanel.add(btnTransferServer);
      firstRowButtonPanel.add(btnCloseOrder);
      firstRowButtonPanel.add(btnSplitTicket);
      firstRowButtonPanel.add(btnMergeTickets);
      

      secondRowButtonPanel.getContentPane().add(btnTransferItems);
      secondRowButtonPanel.getContentPane().add(btnReorder);
    }
    



    innerActivityPanel.add(firstRowButtonPanel);
    innerActivityPanel.add(secondRowButtonPanel, "newline");
    
    ticketList.addTicketListUpateListener(this);
    final PosButton btnMore = new PosButton(POSConstants.MORE_ACTIVITY_BUTTON_TEXT);
    btnMore.setPreferredSize(new Dimension(PosUIManager.getSize(50), 0));
    
    btnMore.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        boolean collapsed = secondRowButtonPanel.isCollapsed();
        secondRowButtonPanel.setVisible(collapsed);
        secondRowButtonPanel.setCollapsed(!collapsed);
        if (collapsed) {
          btnMore.setText(POSConstants.LESS_ACTIVITY_BUTTON_TEXT);
        }
        else {
          btnMore.setText(POSConstants.MORE_ACTIVITY_BUTTON_TEXT);
        }
      }
    });
    OrderServiceExtension orderServiceExtension = (OrderServiceExtension)ExtensionManager.getPlugin(OrderServiceExtension.class);
    if (orderServiceExtension == null) {
      btnReversePayment.setEnabled(false);
      orderServiceExtension = new DefaultOrderServiceExtension();
    }
    
    activityPanel.add(innerActivityPanel);
    activityPanel.add(btnMore, "East");
    
    ticketsAndActivityPanel.add(activityPanel, "South");
    return ticketsAndActivityPanel;
  }
  
  public void updateView() {
    User user = Application.getCurrentUser();
    UserType userType = user.getType();
    if (userType != null) {
      Set<UserPermission> permissions = userType.getPermissions();
      if (permissions != null) {
        btnEditTicket.setEnabled(false);
        btnGroupSettle.setEnabled(false);
        
        btnReorder.setEnabled(false);
        btnTransferServer.setEnabled(false);
        btnSettleTicket.setEnabled(false);
        btnSplitTicket.setEnabled(false);
        

        for (UserPermission permission : permissions) {
          if (permission.equals(UserPermission.VOID_TICKET)) {
            btnVoidTicket.setEnabled(true);
          }
          else if (permission.equals(UserPermission.SETTLE_TICKET)) {
            btnSettleTicket.setEnabled(true);
            btnGroupSettle.setEnabled(true);



          }
          else if (permission.equals(UserPermission.SPLIT_TICKET)) {
            btnSplitTicket.setEnabled(true);
          }
          else if (permission.equals(UserPermission.CREATE_TICKET)) {
            btnEditTicket.setEnabled(true);
          }
          else if (permission.equals(UserPermission.REOPEN_TICKET)) {
            btnReorder.setEnabled(true);
          }
          else if (permission.equals(UserPermission.TRANSFER_TICKET)) {
            btnTransferServer.setEnabled(true);
          }
        }
      }
    }
    

    if (!isDataInitialized()) {
      updateTicketList();
      setDataInitialized(true);
      setDataInitializedForUser(Application.getCurrentUser());
    }
    else if (!Application.getCurrentUser().equals(getDataInitializedForUser()))
    {

      updateTicketList();
      setDataInitialized(true);
      setDataInitializedForUser(Application.getCurrentUser());
    }
  }
  
  public synchronized void updateTicketList() {
    ticketList.updateTicketList();
  }
  
  public void setVisible(boolean visible)
  {
    super.setVisible(visible);
    
    if (visible) {
      updateView();
      ticketList.setAutoUpdateCheck(true);
    }
    else {
      ticketList.setAutoUpdateCheck(false);
    }
  }
  
  public String getViewName()
  {
    return VIEW_NAME;
  }
  
  public void refresh()
  {
    setDataInitialized(false);
    updateView();
  }
  

  public void ticketListUpdated()
  {
    String paymentStatusFilter = TerminalConfig.getPaymentStatusFilter().getDisplayString();
    
    String orderTypeFilter = TerminalConfig.getOrderTypeFilter();
    String title = POSConstants.OPEN_TICKETS_AND_ACTIVITY + " [" + Messages.getString("SwitchboardView.27") + ": " + paymentStatusFilter + ", " + orderTypeFilter + " ]";
    
    ticketsListPanelBorder.setTitle(title);
  }
  
  public DataChangeListener getDataChangeListener()
  {
    return ticketList;
  }
}
