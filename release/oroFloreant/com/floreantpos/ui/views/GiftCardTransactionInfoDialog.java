package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.PosException;
import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.swing.JRViewer;


public class GiftCardTransactionInfoDialog
  extends POSDialog
{
  private JasperPrint print;
  
  public GiftCardTransactionInfoDialog(JasperPrint print)
  {
    this.print = print;
    initComponent();
  }
  
  public JRViewer createReport() {
    try {
      return new JRViewer(print);
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
      throw new PosException(e.getMessage(), e);
    }
  }
  
  private void initComponent()
  {
    setTitle(Messages.getString("GiftCardTransactionInfoDialog.4"));
    setLayout(new BorderLayout(10, 10));
    
    JPanel mainJPanel = new JPanel();
    mainJPanel.setLayout(new BorderLayout());
    mainJPanel.add(createReport(), "Center");
    
    add(mainJPanel);
    
    JPanel buttonPanel = new JPanel(new MigLayout("al center", "sg, fill", Messages.getString("GiftCardTransactionInfoDialog.7")));
    
    PosButton btnPrint = new PosButton(Messages.getString("GiftCardTransactionInfoDialog.8"));
    buttonPanel.add(btnPrint, "grow");
    
    btnPrint.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          print.setName(Messages.getString("GiftCardTransactionInfoDialog.10"));
          ReceiptPrintService.printQuitely(print);
        } catch (Exception e1) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), e1.getMessage(), e1);
        }
        
      }
    });
    buttonPanel.add(new PosButton(new CloseDialogAction(this, Messages.getString("GiftCardTransactionInfoDialog.11"))));
    
    add(buttonPanel, "South");
  }
}
