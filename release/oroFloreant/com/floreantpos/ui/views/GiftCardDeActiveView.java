package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.model.GiftCard;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.payment.GiftCardProcessor;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;































public class GiftCardDeActiveView
  extends POSDialog
{
  private JTextField txtCardNumber;
  private GiftCard giftCard;
  private GiftCardProcessor giftCardProcessor;
  
  public GiftCardDeActiveView(GiftCardProcessor giftCardProcessor)
  {
    this.giftCardProcessor = giftCardProcessor;
    init();
  }
  

  public GiftCardDeActiveView(JFrame parent)
  {
    init();
  }
  
  private void init() {
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Card Deactivate");
    add(titlePanel, "North");
    
    JPanel centerPanel = new JPanel(new MigLayout("fillx,aligny center", "[]20px[]", ""));
    
    JLabel lblNumberOfCard = new JLabel(Messages.getString("GiftCardDeActiveView.3"));
    
    txtCardNumber = new JTextField(20);
    
    centerPanel.add(lblNumberOfCard, "cell 0 0, alignx right");
    centerPanel.add(txtCardNumber, "cell 1 0");
    
    add(centerPanel, "Center");
    
    JPanel buttonPanel = new JPanel(new MigLayout("al center", "sg, fill", ""));
    
    PosButton btnGenerate = new PosButton(Messages.getString("GiftCardDeActiveView.9"));
    buttonPanel.add(btnGenerate, "grow");
    
    btnGenerate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doGenerate();
      }
      

    });
    buttonPanel.add(new PosButton(new CloseDialogAction(this, Messages.getString("GiftCardDeActiveView.11"))));
    
    add(buttonPanel, "South");
  }
  














  public void doGenerate()
  {
    String cardNo = txtCardNumber.getText();
    
    giftCard = giftCardProcessor.getCard(cardNo);
    
    if (giftCard == null) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("GiftCardDeActiveView.12"));
      return;
    }
    if (StringUtils.isEmpty(cardNo)) {
      MessageDialog.showError(Messages.getString("GiftCardDeActiveView.13"));
      return;
    }
    
    int value = POSMessageDialog.showYesNoQuestionDialog(this, Messages.getString("GiftCardDeActiveView.14"), Messages.getString("GiftCardDeActiveView.15"));
    
    if (value == 0) {
      if (!giftCard.isActive().booleanValue()) {
        POSMessageDialog.showMessage(this, Messages.getString("GiftCardDeActiveView.16"));
        return;
      }
      giftCard.setActive(Boolean.valueOf(false));
      giftCard.setDeActivationDate(new Date());
      
      giftCardProcessor.deactivate(giftCard);
      
      POSMessageDialog.showMessage(this, Messages.getString("GiftCardDeActiveView.17"));
      dispose();
    }
  }
}
