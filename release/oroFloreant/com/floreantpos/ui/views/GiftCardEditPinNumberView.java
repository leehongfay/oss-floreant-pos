package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.model.GiftCard;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.payment.GiftCardProcessor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;



































public class GiftCardEditPinNumberView
  extends POSDialog
{
  private FixedLengthTextField txtCardNumber;
  private FixedLengthTextField txtOldPinNumber;
  private FixedLengthTextField txtNewPinNumber;
  private FixedLengthTextField txtConfirmPinNumber;
  private GiftCard giftCard;
  private GiftCardProcessor giftCardProcessor;
  
  public GiftCardEditPinNumberView(GiftCardProcessor giftCardProcessor)
  {
    this.giftCardProcessor = giftCardProcessor;
    init();
  }
  

  public GiftCardEditPinNumberView(JFrame parent)
  {
    init();
  }
  
  private void init() {
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Pin Card Update");
    add(titlePanel, "North");
    
    JPanel centerPanel = new JPanel(new MigLayout("fillx,aligny center", "[]20px[]", ""));
    
    JLabel lblNumberOfCard = new JLabel(Messages.getString("GiftCardEditPinNumberView.3"));
    
    txtCardNumber = new FixedLengthTextField(20);
    txtOldPinNumber = new FixedLengthTextField(20);
    txtOldPinNumber.setLength(8);
    txtNewPinNumber = new FixedLengthTextField(20);
    txtNewPinNumber.setLength(8);
    txtConfirmPinNumber = new FixedLengthTextField(20);
    txtConfirmPinNumber.setLength(8);
    
    centerPanel.add(lblNumberOfCard, "cell 0 0, alignx right");
    centerPanel.add(txtCardNumber, "cell 1 0");
    centerPanel.add(new JLabel(Messages.getString("GiftCardEditPinNumberView.6")), "cell 0 1, alignx right");
    centerPanel.add(txtOldPinNumber, "cell 1 1");
    centerPanel.add(new JLabel(Messages.getString("GiftCardEditPinNumberView.9")), "cell 0 2, alignx right");
    centerPanel.add(txtNewPinNumber, "cell 1 2");
    centerPanel.add(new JLabel(Messages.getString("GiftCardEditPinNumberView.12")), "cell 0 3, alignx right");
    centerPanel.add(txtConfirmPinNumber, "cell 1 3");
    add(centerPanel, "Center");
    
    JPanel buttonPanel = new JPanel(new MigLayout("al center", "sg, fill", ""));
    
    PosButton btnGenerate = new PosButton(Messages.getString("GiftCardEditPinNumberView.18"));
    buttonPanel.add(btnGenerate, "grow");
    

























    btnGenerate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doGenerate();

      }
      

    });
    buttonPanel.add(new PosButton(new CloseDialogAction(this, Messages.getString("GiftCardEditPinNumberView.20"))));
    
    add(buttonPanel, "South");
  }
  

  public void doGenerate()
  {
    if (save())
    {
      giftCardProcessor.changePinNumber(giftCard);
      
      POSMessageDialog.showMessage(this, Messages.getString("GiftCardEditPinNumberView.21"));
      dispose();
    }
  }
  
  private boolean save() {
    String cardNo = txtCardNumber.getText();
    
    giftCard = giftCardProcessor.getCard(cardNo);
    
    if (StringUtils.isEmpty(cardNo)) {
      MessageDialog.showError(Messages.getString("GiftCardEditPinNumberView.22"));
      return false;
    }
    
    String oldPinNumber = txtOldPinNumber.getText();
    
    if (StringUtils.isEmpty(oldPinNumber)) {
      MessageDialog.showError(Messages.getString("GiftCardEditPinNumberView.23"));
      return false;
    }
    
    if (!oldPinNumber.equals(giftCard.getPinNumber())) {
      MessageDialog.showError(Messages.getString("GiftCardEditPinNumberView.24"));
      return false;
    }
    
    String newPinNumber = txtNewPinNumber.getText();
    
    if (StringUtils.isEmpty(newPinNumber)) {
      MessageDialog.showError(Messages.getString("GiftCardEditPinNumberView.25"));
      return false;
    }
    
    String repeatPinNumber = txtConfirmPinNumber.getText();
    
    if (StringUtils.isEmpty(repeatPinNumber)) {
      MessageDialog.showError(Messages.getString("GiftCardEditPinNumberView.26"));
      return false;
    }
    
    if (!newPinNumber.equals(repeatPinNumber)) {
      MessageDialog.showError(Messages.getString("GiftCardEditPinNumberView.27"));
      return false;
    }
    
    giftCard.setPinNumber(repeatPinNumber);
    return true;
  }
}
