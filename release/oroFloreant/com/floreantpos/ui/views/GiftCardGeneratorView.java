package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.model.GiftCard;
import com.floreantpos.model.dao.GiftCardDAO;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;




public class GiftCardGeneratorView
  extends POSDialog
{
  private IntegerTextField txtCardNumber;
  private DoubleTextField txtFaceValue;
  private IntegerTextField txtValidity;
  private JComboBox cbDuration;
  private FixedLengthTextField txtPinNumber;
  private List<GiftCard> giftCardList;
  private FixedLengthTextField tfBatchNumber;
  
  public GiftCardGeneratorView()
  {
    super(POSUtil.getBackOfficeWindow(), "");
    
    init();
  }
  

  public GiftCardGeneratorView(JFrame parent)
  {
    init();
  }
  
  private void init() {
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Gift Card Generation");
    add(titlePanel, "North");
    
    JPanel centerPanel = new JPanel(new MigLayout("fillx,aligny center", "[][]", ""));
    
    JLabel lblNumberOfCard = new JLabel(Messages.getString("GiftCardGeneratorView.4"));
    
    txtCardNumber = new IntegerTextField();
    
    JLabel lblFaceValue = new JLabel(Messages.getString("GiftCardGeneratorView.5"));
    
    txtFaceValue = new DoubleTextField();
    
    JLabel lblValidity = new JLabel(Messages.getString("GiftCardGeneratorView.6"));
    
    txtValidity = new IntegerTextField();
    
    cbDuration = new JComboBox();
    cbDuration.addItem("DAY");
    cbDuration.addItem("MONTH");
    cbDuration.addItem("YEAR");
    cbDuration.setSelectedItem("YEAR");
    
    txtPinNumber = new FixedLengthTextField(10);
    txtPinNumber.setLength(8);
    
    tfBatchNumber = new FixedLengthTextField();
    tfBatchNumber.setLength(32);
    
    txtValidity.setText("5");
    txtFaceValue.setText("10");
    
    String randomBatchNumber = generateBatchNumber();
    
    tfBatchNumber.setText("" + randomBatchNumber);
    
    centerPanel.add(lblNumberOfCard, "cell 0 0,alignx right");
    centerPanel.add(txtCardNumber, "cell 1 0,growx");
    centerPanel.add(lblFaceValue, "cell 0 1,alignx right");
    centerPanel.add(txtFaceValue, "cell 1 1,growx");
    centerPanel.add(new JLabel(Messages.getString("GiftCardGeneratorView.15")), "cell 0 2,alignx right");
    centerPanel.add(txtPinNumber, "cell 1 2,growx");
    centerPanel.add(new JLabel(Messages.getString("GiftCardGeneratorView.18")), "cell 0 3,alignx right");
    centerPanel.add(tfBatchNumber, "cell 1 3,growx");
    centerPanel.add(lblValidity, "cell 0 4,alignx right");
    centerPanel.add(txtValidity, "cell 1 4,growx");
    centerPanel.add(cbDuration, "cell 2 4");
    
    add(centerPanel, "Center");
    
    JPanel buttonPanel = new JPanel(new MigLayout("al center", "sg, fill", ""));
    
    PosButton btnGenerate = new PosButton(Messages.getString("GiftCardGeneratorView.27"));
    buttonPanel.add(btnGenerate, "grow");
    
    btnGenerate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doGenerate();
      }
      

    });
    buttonPanel.add(new PosButton(new CloseDialogAction(this)));
    
    add(buttonPanel, "South");
  }
  
  public static String generateBatchNumber()
  {
    String randomBatchNumber = String.valueOf((Math.random() * 1.0E8D));
    String pinPad = "00000000";
    randomBatchNumber = pinPad.substring(randomBatchNumber.length()) + randomBatchNumber;
    if (GiftCardDAO.getInstance().hasBatchNo(randomBatchNumber)) {
      return generateBatchNumber();
    }
    return randomBatchNumber;
  }
  
  public void doGenerate()
  {
    if (save())
    {
      GiftCardDAO.getInstance().saveAsList(giftCardList);
      
      POSMessageDialog.showMessage(this, Messages.getString("GiftCardGeneratorView.29"));
      dispose();
    }
  }
  
  private boolean save() {
    if ((txtCardNumber.getText() == null) || (txtCardNumber.getInteger() == 0)) {
      POSMessageDialog.showError(this, Messages.getString("GiftCardGeneratorView.30"));
      return false;
    }
    
    if (StringUtils.isEmpty(txtFaceValue.getText())) {
      POSMessageDialog.showError(this, Messages.getString("GiftCardGeneratorView.31"));
      return false;
    }
    
    if (StringUtils.isEmpty(txtValidity.getText())) {
      POSMessageDialog.showError(this, Messages.getString("GiftCardGeneratorView.32"));
      return false;
    }
    
    int validity = txtValidity.getInteger();
    if (validity == 0) {
      POSMessageDialog.showError(this, Messages.getString("GiftCardGeneratorView.33"));
      return false;
    }
    
    String durationType = (String)cbDuration.getSelectedItem();
    if (StringUtils.isEmpty(durationType)) {
      POSMessageDialog.showError(this, Messages.getString("GiftCardGeneratorView.34"));
      return false;
    }
    
    if (StringUtils.isEmpty(txtPinNumber.getText())) {
      POSMessageDialog.showError(this, Messages.getString("GiftCardGeneratorView.35"));
      return false;
    }
    
    if (StringUtils.isEmpty(tfBatchNumber.getText())) {
      POSMessageDialog.showError(this, Messages.getString("GiftCardGeneratorView.36"));
      return false;
    }
    
    int cardNumber = txtCardNumber.getInteger();
    giftCardList = new ArrayList();
    
    for (int i = 0; i < cardNumber; i++) {
      GiftCard giftCard = new GiftCard();
      String randomNumber = String.valueOf((Math.random() * 1.0E16D));
      String pad = "0000000000000000";
      randomNumber = pad.substring(randomNumber.length()) + randomNumber;
      giftCard.setCardNumber(randomNumber);
      giftCard.setBalance(Double.valueOf(txtFaceValue.getDouble()));
      
      giftCard.setPinNumber(txtPinNumber.getText());
      giftCard.setIssueDate(new Date());
      giftCard.setDuration(Integer.valueOf(txtValidity.getInteger()));
      giftCard.setDurationType((String)cbDuration.getSelectedItem());
      giftCard.setBatchNo(tfBatchNumber.getText());
      giftCardList.add(giftCard);
    }
    
    return true;
  }
}
