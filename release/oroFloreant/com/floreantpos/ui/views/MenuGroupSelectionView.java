package com.floreantpos.ui.views;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.bo.ui.explorer.ExplorerButtonPanel;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.model.MenuGroupForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;



















public class MenuGroupSelectionView
  extends JPanel
{
  private JComboBox cbMenuCategory;
  private JXTable table;
  private BeanTableModel<MenuGroup> tableModel;
  private JTextField tfName;
  private MenuGroup parentMenuGroup;
  private PosButton btnNext;
  private PosButton btnPrev;
  private MenuCategory selectedCategory;
  private JLabel lblNumberOfItem = new JLabel();
  private JLabel lblName;
  private JButton btnSearch;
  private JPanel searchPanel;
  private Map<String, MenuGroup> addedMenuGroupMap = new HashMap();
  private JCheckBox chkShowSelected;
  private JCheckBox chkSelectAll;
  private JLabel lblMenuCategory;
  public static final int SINGLE_SELECTION = 0;
  public static final int MULTIPLE_SELECTION = 1;
  
  public MenuGroupSelectionView(List<MenuGroup> addedMenuGroups)
  {
    initComponents();
    tableModel.setCurrentRowIndex(0);
    cbMenuCategory.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        chkSelectAll.setEnabled(true);
        tableModel.setCurrentRowIndex(0);
        setSelectedMenuGroup(cbMenuCategory.getSelectedItem());
      }
    });
    setMenuGroups(addedMenuGroups);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    tableModel = new BeanTableModel(MenuGroup.class);
    tableModel.addColumn("", "visible");
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "name");
    tableModel.setPageSize(10);
    table = new JXTable(tableModel);
    table.setSelectionMode(2);
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.setRowHeight(PosUIManager.getSize(40));
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          MenuGroupSelectionView.this.editSelectedRow();
        }
        else {
          MenuGroupSelectionView.this.selectItem();
        }
        
      }
    });
    JPanel contentPanel = new JPanel(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(10, 5, 10, 5));
    JScrollPane scroll = new JScrollPane(table);
    scroll.setPreferredSize(PosUIManager.getSize(500, 250));
    contentPanel.add(scroll);
    contentPanel.add(buildSearchForm(), "North");
    
    add(contentPanel);
    resizeColumnWidth(table);
    
    JPanel paginationButtonPanel = new JPanel(new MigLayout("ins 5 0 0 0,fillx", "[left,grow][][][]", ""));
    paginationButtonPanel.add(createButtonPanel(), "left,split 2");
    
    chkShowSelected = new JCheckBox("Show selected");
    chkShowSelected.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuGroupSelectionView.this.updateView();
      }
    });
    paginationButtonPanel.add(chkShowSelected);
    paginationButtonPanel.add(lblNumberOfItem, "split 3,center");
    
    btnPrev = new PosButton();
    btnPrev.setIcon(IconFactory.getIcon("/ui_icons/", "previous.png"));
    paginationButtonPanel.add(btnPrev, "center");
    
    PosButton btnDot = new PosButton();
    btnDot.setBorder(null);
    btnDot.setOpaque(false);
    btnDot.setContentAreaFilled(false);
    btnDot.setIcon(IconFactory.getIcon("/ui_icons/", "dot.png"));
    
    btnNext = new PosButton();
    btnNext.setIcon(IconFactory.getIcon("/ui_icons/", "next.png"));
    paginationButtonPanel.add(btnNext);
    paginationButtonPanel.add(new JSeparator(), "newline,span,grow");
    
    contentPanel.add(paginationButtonPanel, "South");
    
    ActionListener action = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          Object source = e.getSource();
          if (source == btnPrev) {
            MenuGroupSelectionView.this.scrollUp();
          }
          else if (source == btnNext) {
            MenuGroupSelectionView.this.scrollDown();
          }
        } catch (Exception e2) {
          POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e2.getMessage(), e2);
        }
        
      }
    };
    btnPrev.addActionListener(action);
    btnNext.addActionListener(action);
    
    btnNext.setEnabled(false);
    btnPrev.setEnabled(false);
  }
  
  private void updateView() {
    if (chkShowSelected.isSelected())
    {
      tableModel.setRows(new ArrayList(addedMenuGroupMap.values()));
      
      updateMenuGroupSelection();
      chkShowSelected.setText("Show Selected (" + addedMenuGroupMap.values().size() + ")");
      lblNumberOfItem.setText("");
      btnPrev.setEnabled(false);
      btnNext.setEnabled(false);
      cbMenuCategory.setEnabled(false);
      table.repaint();
    }
    else {
      if (cbMenuCategory.getSelectedItem() != null) {
        searchItem();
      }
      cbMenuCategory.setEnabled(true);
    }
  }
  
  private JPanel buildSearchForm() {
    searchPanel = new JPanel();
    
    searchPanel.setLayout(new MigLayout("inset 0,fillx,hidemode 3", "", "[]10[]"));
    lblName = new JLabel(POSConstants.NAME + ":");
    tfName = new JTextField(15);
    btnSearch = new JButton(POSConstants.SEARCH_ITEM_BUTTON_TEXT);
    searchPanel.add(lblName, "align label,split 5");
    searchPanel.add(tfName, "growx");
    
    chkSelectAll = new JCheckBox("Select All");
    chkSelectAll.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuGroupSelectionView.this.selectGroupItems();
      }
      
    });
    cbMenuCategory = new JComboBox();
    List groups = new ArrayList();
    groups.add("<All>");
    
    List<MenuCategory> menuCategoryList = MenuCategoryDAO.getInstance().findAll();
    groups.addAll(menuCategoryList);
    
    ComboBoxModel model = new ComboBoxModel(groups);
    cbMenuCategory.setModel(model);
    cbMenuCategory.setSelectedItem("<All>");
    cbMenuCategory.addItemListener(new ItemListener()
    {

      public void itemStateChanged(ItemEvent e)
      {
        MenuGroupSelectionView.this.searchItem();
      }
    });
    searchPanel.add(btnSearch);
    lblMenuCategory = new JLabel("Category");
    searchPanel.add(lblMenuCategory, "split 2,right");
    searchPanel.add(cbMenuCategory, "wrap");
    searchPanel.add(chkSelectAll, "left");
    
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        MenuGroupSelectionView.this.searchItem();
      }
      
    });
    tfName.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuGroupSelectionView.this.searchItem();
      }
    });
    return searchPanel;
  }
  
  private void searchItem() {
    if ((cbMenuCategory.getSelectedItem() instanceof MenuCategory)) {
      selectedCategory = ((MenuCategory)cbMenuCategory.getSelectedItem());
    }
    else {
      selectedCategory = null;
    }
    tableModel.setCurrentRowIndex(0);
    
    MenuGroupDAO.getInstance().loadMenuGroups(tableModel, tfName.getText(), selectedCategory, new String[] { MenuCategory.PROP_NAME });
    doSetVisibleCheckAll();
    updateButton();
    updateMenuGroupSelection();
    table.repaint();
    chkShowSelected.setSelected(false);
  }
  
  private void selectGroupItems() {
    Object selectedGroup = cbMenuCategory.getSelectedItem();
    if ((selectedGroup instanceof MenuCategory)) {
      List<MenuGroup> menuGroups = tableModel.getRows();
      if ((menuGroups != null) && (menuGroups.size() > 0)) {
        for (MenuGroup group : menuGroups) {
          if ((parentMenuGroup == null) || (parentMenuGroup.getId() == null) || (!parentMenuGroup.getId().equals(group.getId())))
          {

            group.setVisible(Boolean.valueOf(chkSelectAll.isSelected()));
            if (group.isVisible().booleanValue()) {
              addedMenuGroupMap.put(group.getId(), group);
            }
            else {
              addedMenuGroupMap.remove(group.getId());
            }
          }
        }
      } else {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "No groups found!");
        chkSelectAll.setSelected(false);
      }
    }
    else {
      List<MenuGroup> menuGroups = tableModel.getRows();
      if (menuGroups == null)
        return;
      for (MenuGroup group : menuGroups) {
        if ((parentMenuGroup == null) || (parentMenuGroup.getId() == null) || (!parentMenuGroup.getId().equals(group.getId())))
        {

          group.setVisible(Boolean.valueOf(chkSelectAll.isSelected()));
          if (group.isVisible().booleanValue()) {
            addedMenuGroupMap.put(group.getId(), group);
          }
          else {
            addedMenuGroupMap.remove(group.getId());
          }
          chkShowSelected.setText("Show Selected (" + addedMenuGroupMap.values().size() + ")");
          table.repaint();
        }
      }
    }
    table.repaint();
  }
  
  private void updateMenuGroupSelection() {
    List<MenuGroup> menuGroups = tableModel.getRows();
    if (menuGroups == null)
      return;
    for (MenuGroup menuGroup : menuGroups) {
      MenuGroup group = (MenuGroup)addedMenuGroupMap.get(menuGroup.getId());
      menuGroup.setVisible(Boolean.valueOf(group != null));
    }
  }
  
  private void updateButton() {
    int startNumber = tableModel.getCurrentRowIndex() + 1;
    int endNumber = tableModel.getNextRowIndex();
    int totalNumber = tableModel.getNumRows();
    if (endNumber > totalNumber) {
      endNumber = totalNumber;
    }
    lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
    btnPrev.setEnabled(tableModel.hasPrevious());
    btnNext.setEnabled(tableModel.hasNext());
    
    if (tableModel.getRowCount() > 0) {
      table.setRowSelectionInterval(0, 0);
    }
    chkShowSelected.setText("Show Selected (" + addedMenuGroupMap.values().size() + ")");
  }
  
  private TransparentPanel createButtonPanel() {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton btnEdit = explorerButton.getEditButton();
    JButton btnAdd = explorerButton.getAddButton();
    btnAdd.setText(POSConstants.ADD);
    btnEdit.setText(POSConstants.EDIT);
    
    btnEdit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MenuGroupSelectionView.this.editSelectedRow();
      }
    });
    btnAdd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          MenuGroup menuGroup = new MenuGroup();
          MenuGroupForm editor = new MenuGroupForm(menuGroup);
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          MenuGroup group = (MenuGroup)editor.getBean();
          tableModel.addRow(group);
          tableModel.setNumRows(tableModel.getNumRows() + 1);
          MenuGroupSelectionView.this.updateButton();
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel(new MigLayout("center,ins 0", "sg,fill", ""));
    return panel;
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(70));
    
    columnWidth.add(Integer.valueOf(250));
    columnWidth.add(Integer.valueOf(70));
    
    return columnWidth;
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      MenuGroup menuGroup = (MenuGroup)tableModel.getRow(index);
      MenuGroupDAO.getInstance().initialize(menuGroup);
      tableModel.setRow(index, menuGroup);
      
      BeanEditor<MenuGroup> editor = new MenuGroupForm(menuGroup);
      
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public List<MenuGroup> getSelectedMenuGroupList() {
    return new ArrayList(addedMenuGroupMap.values());
  }
  
  public void setMenuGroups(List<MenuGroup> menuGroups) {
    if (menuGroups != null) {
      for (MenuGroup group : menuGroups) {
        addedMenuGroupMap.put(group.getId(), group);
        tableModel.addRow(group);
      }
    }
  }
  
  private void scrollDown() {
    tableModel.setCurrentRowIndex(tableModel.getNextRowIndex());
    MenuGroupDAO.getInstance().loadMenuGroups(tableModel, tfName.getText(), selectedCategory, new String[] { MenuCategory.PROP_NAME });
    doSetVisibleCheckAll();
    updateButton();
    updateMenuGroupSelection();
    table.repaint();
    chkShowSelected.setSelected(false);
  }
  
  private void scrollUp() {
    tableModel.setCurrentRowIndex(tableModel.getPreviousRowIndex());
    MenuGroupDAO.getInstance().loadMenuGroups(tableModel, tfName.getText(), selectedCategory, new String[] { MenuCategory.PROP_NAME });
    doSetVisibleCheckAll();
    updateButton();
    updateMenuGroupSelection();
    table.repaint();
    chkShowSelected.setSelected(false);
  }
  
  public void setSelectedMenuGroup(Object selectedItem) {
    if ((selectedItem instanceof MenuCategory)) {
      selectedCategory = ((MenuCategory)selectedItem);
    }
    else {
      selectedCategory = null;
    }
    searchItem();
  }
  
  private void selectItem() {
    if (table.getSelectedRow() < 0) {
      return;
    }
    int selectedRow = table.getSelectedRow();
    selectedRow = table.convertRowIndexToModel(selectedRow);
    MenuGroup item = (MenuGroup)tableModel.getRow(selectedRow);
    if ((parentMenuGroup != null) && (parentMenuGroup.getId() != null) && (parentMenuGroup.getId().equals(item.getId()))) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Parent item cannot be selected");
      return;
    }
    item.setVisible(Boolean.valueOf(!item.isVisible().booleanValue()));
    if (item.isVisible().booleanValue()) {
      addedMenuGroupMap.put(item.getId(), item);
    }
    else {
      addedMenuGroupMap.remove(item.getId());
    }
    chkShowSelected.setText("Show Selected (" + addedMenuGroupMap.values().size() + ")");
    table.repaint();
  }
  
  private void doSetVisibleCheckAll() {
    List<Boolean> checkVisibilities = new ArrayList();
    List<MenuGroup> menuItems = tableModel.getRows();
    for (MenuGroup menuItem : menuItems) {
      if (addedMenuGroupMap.containsKey(menuItem.getId())) {
        checkVisibilities.add(menuItem.isVisible());
      }
    }
    
    if ((checkVisibilities.size() != 0) && (!checkVisibilities.contains(Boolean.valueOf(false)))) {
      chkSelectAll.setSelected(true);
    }
    else {
      chkSelectAll.setSelected(false);
    }
  }
  
  public BeanTableModel<MenuGroup> getModel() {
    return tableModel;
  }
  
  public int getSelectedRow() {
    int index = table.getSelectedRow();
    if (index < 0)
      return -1;
    return table.convertRowIndexToModel(index);
  }
  
  public void repaintTable() {
    table.repaint();
  }
  
  public void setSelectionMode(int selectionMode) {
    chkShowSelected.setVisible(selectionMode == 1);
    chkSelectAll.setVisible(selectionMode == 1);
    TableColumnModelExt columnModel = (TableColumnModelExt)table.getColumnModel();
    columnModel.getColumnExt(0).setVisible(selectionMode == 1);
    searchItem();
  }
  
  public void setSelectedCategory(MenuCategory menuCategory) {
    if ((selectedCategory != null) && (selectedCategory.getId().equals(menuCategory.getId())))
      return;
    selectedCategory = menuCategory;
    cbMenuCategory.setSelectedItem(menuCategory);
    searchItem();
  }
  
  public void setEnableSearch(boolean enableSearch) {
    tfName.setVisible(enableSearch);
    lblName.setVisible(enableSearch);
    btnSearch.setVisible(enableSearch);
  }
}
