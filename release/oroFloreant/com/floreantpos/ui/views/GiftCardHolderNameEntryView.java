package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;


































public class GiftCardHolderNameEntryView
  extends POSDialog
{
  private JTextField txtHolderName;
  private String cardHolderName;
  
  public String getCardHolderName()
  {
    return cardHolderName;
  }
  
  public GiftCardHolderNameEntryView() {
    super(POSUtil.getBackOfficeWindow(), "");
    init();
  }
  

  public GiftCardHolderNameEntryView(JFrame parent)
  {
    init();
  }
  
  private void init() {
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Card Activation");
    add(titlePanel, "North");
    
    JPanel centerPanel = new JPanel(new MigLayout("fillx,aligny center", "[]20px[]", ""));
    
    JLabel lblNumberOfCard = new JLabel(Messages.getString("GiftCardHolderNameEntryView.4"));
    
    txtHolderName = new JTextField(20);
    
    centerPanel.add(lblNumberOfCard, "cell 0 0, alignx right");
    centerPanel.add(txtHolderName, "cell 1 0");
    
    add(centerPanel, "Center");
    
    JPanel buttonPanel = new JPanel(new MigLayout("al center", "sg, fill", ""));
    
    PosButton btnGenerate = new PosButton(Messages.getString("GiftCardHolderNameEntryView.10"));
    buttonPanel.add(btnGenerate, "grow");
    
    btnGenerate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doGenerate();
      }
      

    });
    buttonPanel.add(new PosButton(new CloseDialogAction(this, Messages.getString("GiftCardHolderNameEntryView.12"))));
    
    add(buttonPanel, "South");
  }
  
  public void doGenerate()
  {
    String cardOwnerName = txtHolderName.getText();
    if (StringUtils.isEmpty(cardOwnerName)) {
      MessageDialog.showError(Messages.getString("GiftCardHolderNameEntryView.13"));
      return;
    }
    cardHolderName = cardOwnerName;
    setCanceled(false);
    dispose();
  }
}
