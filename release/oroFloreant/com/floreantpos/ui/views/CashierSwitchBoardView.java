package com.floreantpos.ui.views;

import com.floreantpos.POSConstants;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.views.order.ViewPanel;
import com.floreantpos.util.PosGuiUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;



















public class CashierSwitchBoardView
  extends ViewPanel
  implements ActionListener
{
  public static final String VIEW_NAME = "csbv";
  private PosButton btnNewOrder = new PosButton(POSConstants.NEW_ORDER_BUTTON_TEXT);
  private PosButton btnEditOrder = new PosButton(POSConstants.EDIT_TICKET_BUTTON_TEXT);
  private PosButton btnSettleOrder = new PosButton(POSConstants.SETTLE_TICKET_BUTTON_TEXT);
  
  public CashierSwitchBoardView() {
    setLayout(new MigLayout("align 50% 50%"));
    
    JPanel orderPanel = new JPanel(new MigLayout());
    orderPanel.setBorder(PosGuiUtil.createTitledBorder(POSConstants.CashierSwitchBoardView_LABEL_ORDER));
    
    orderPanel.add(btnNewOrder, "w 160!, h 160!");
    orderPanel.add(btnEditOrder, "w 160!, h 160!");
    orderPanel.add(btnSettleOrder, "w 160!, h 160!");
    
    btnNewOrder.addActionListener(this);
    btnEditOrder.addActionListener(this);
    btnSettleOrder.addActionListener(this);
    
    add(orderPanel);
  }
  
  public String getViewName()
  {
    return "csbv";
  }
  
  public void actionPerformed(ActionEvent e)
  {
    Object source = e.getSource();
    
    if (source != btnNewOrder)
    {


      if (source != btnEditOrder)
      {

        if (source != btnSettleOrder) {}
      }
    }
  }
}
