package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.model.GiftCard;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.GiftCardInfo;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.views.payment.GiftCardProcessor;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;






public class GiftCardInfoDialog
  extends POSDialog
{
  private JTextField txtCardNumber;
  private GiftCard giftCard;
  private GiftCardProcessor giftCardProcessor;
  
  public GiftCardInfoDialog(GiftCardProcessor giftCardProcessor)
  {
    this.giftCardProcessor = giftCardProcessor;
    init();
  }
  

  public GiftCardInfoDialog(JFrame parent)
  {
    init();
  }
  
  private void init() {
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Card Info");
    add(titlePanel, "North");
    
    JPanel centerPanel = new JPanel(new MigLayout("fillx,aligny center", "[]20px[]", ""));
    
    JLabel lblNumberOfCard = new JLabel(Messages.getString("GiftCardInfoDialog.3"));
    
    txtCardNumber = new JTextField(20);
    
    centerPanel.add(lblNumberOfCard, "cell 0 0, alignx right");
    centerPanel.add(txtCardNumber, "cell 1 0");
    add(centerPanel, "Center");
    
    JPanel buttonPanel = new JPanel(new MigLayout("al center", "sg, fill", ""));
    
    PosButton btnGenerate = new PosButton(Messages.getString("GiftCardInfoDialog.9"));
    buttonPanel.add(btnGenerate, "grow");
    
    btnGenerate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doGenerate();

      }
      

    });
    buttonPanel.add(new PosButton(new CloseDialogAction(this, Messages.getString("GiftCardInfoDialog.11"))));
    
    add(buttonPanel, "South");
  }
  

  public void doGenerate()
  {
    String cardNo = txtCardNumber.getText();
    
    giftCard = giftCardProcessor.getCard(cardNo);
    
    if (StringUtils.isEmpty(cardNo)) {
      MessageDialog.showError(Messages.getString("GiftCardInfoDialog.12"));
      return;
    }
    
    if (!giftCard.isActive().booleanValue()) {
      MessageDialog.showError(Messages.getString("GiftCardInfoDialog.13"));
      return;
    }
    
    GiftCardInfo dialog = new GiftCardInfo(giftCard);
    dialog.setDefaultCloseOperation(2);
    dialog.setSize(PosUIManager.getSize(470, 400));
    dialog.setLocationRelativeTo(POSUtil.getFocusedWindow());
    dialog.setVisible(true);
  }
}
