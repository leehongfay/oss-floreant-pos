package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.Ticket;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;


















public class SplitTypeSelectionDialog
  extends POSDialog
{
  Ticket ticket;
  private int selectedType;
  private PosButton btnSplitEqually;
  private PosButton btnSplitBySeat;
  private PosButton btnSplitManually;
  
  public SplitTypeSelectionDialog(Ticket ticket)
    throws HeadlessException
  {
    this.ticket = ticket;
    initializeComponent();
  }
  
  private void initializeComponent() {
    setTitle("Select split type");
    setResizable(false);
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle(Messages.getString("SplitTypeSelectionDialog.0"));
    
    add(titlePanel, "North");
    
    JPanel splitTypePanel = new JPanel(new MigLayout("fill,wrap 1,inset 10", "sg", ""));
    
    Font f = new Font("Verdana", 1, PosUIManager.getFontSize(20));
    int width = PosUIManager.getSize(300);
    int height = PosUIManager.getSize(70);
    
    btnSplitEqually = new PosButton("Split Equally");
    btnSplitEqually.setFont(f);
    btnSplitEqually.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        selectedType = 0;
        setCanceled(false);
        dispose();
      }
    });
    splitTypePanel.add(btnSplitEqually, "w " + width + "!,h " + height + "!,grow");
    
    btnSplitBySeat = new PosButton(Messages.getString("SplitTypeSelectionDialog.1"));
    btnSplitBySeat.setFont(f);
    btnSplitBySeat.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        selectedType = 1;
        setCanceled(false);
        dispose();
      }
    });
    splitTypePanel.add(btnSplitBySeat, "w " + width + "!,h " + height + "!,grow");
    
    btnSplitManually = new PosButton(Messages.getString("SplitTypeSelectionDialog.13"));
    btnSplitManually.setFont(f);
    btnSplitManually.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        selectedType = 2;
        setCanceled(false);
        dispose();
      }
    });
    splitTypePanel.add(btnSplitManually, "w " + width + "!,h " + height + "!,grow");
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL_BUTTON_TEXT);
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
      
    });
    JPanel actionPanel = new JPanel(new MigLayout("fill"));
    actionPanel.add(btnCancel, "w " + width + "!,h " + height + "!,grow");
    
    add(splitTypePanel, "Center");
    add(actionPanel, "South");
  }
  
  public int getSelectedSplitType() {
    return selectedType;
  }
}
