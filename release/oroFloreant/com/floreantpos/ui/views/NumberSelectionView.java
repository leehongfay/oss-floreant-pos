package com.floreantpos.ui.views;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.NumberUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

























public class NumberSelectionView
  extends TransparentPanel
  implements ActionListener
{
  private TitledBorder titledBorder;
  private boolean decimalAllowed;
  private JTextField tfNumber;
  private PosButton btnIncrementQuantity;
  private PosButton btnDecrementQuantity;
  
  public NumberSelectionView()
  {
    initComponents();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    
    tfNumber = new JTextField();
    tfNumber.setText("0");
    tfNumber.setFont(tfNumber.getFont().deriveFont(1, PosUIManager.getNumberFieldFontSize()));
    tfNumber.setEditable(false);
    tfNumber.setBackground(Color.WHITE);
    tfNumber.setHorizontalAlignment(4);
    
    JPanel northPanel = new JPanel(new BorderLayout(5, 5));
    northPanel.add(tfNumber, "Center");
    
    btnIncrementQuantity = new PosButton("+");
    btnIncrementQuantity.setFont(new Font("Arial", 0, 30));
    btnIncrementQuantity.setFocusable(false);
    
    northPanel.add(btnIncrementQuantity, "East");
    btnIncrementQuantity.setPreferredSize(PosUIManager.getSize(60, 45));
    btnDecrementQuantity = new PosButton("-");
    btnDecrementQuantity.setPreferredSize(PosUIManager.getSize(60, 45));
    btnDecrementQuantity.setFont(new Font("Arial", 1, 30));
    btnDecrementQuantity.setFocusable(false);
    
    northPanel.add(btnDecrementQuantity, "West");
    







    add(northPanel, "North");
    
    String[][] numbers = { { "7", "8", "9" }, { "4", "5", "6" }, { "1", "2", "3" }, { ".", "0", POSConstants.CLEAR_ALL } };
    



    String[][] iconNames = { { "7.png", "8.png", "9.png" }, { "4.png", "5.png", "6.png" }, { "1.png", "2.png", "3.png" }, { "dot.png", "0.png", "clear.png" } };
    




    JPanel centerPanel = new JPanel(new GridLayout(4, 3, 5, 5));
    Dimension preferredSize = PosUIManager.getSize(100, 70);
    
    for (int i = 0; i < numbers.length; i++) {
      for (int j = 0; j < numbers[i].length; j++) {
        PosButton posButton = new PosButton();
        ImageIcon icon = IconFactory.getIcon("/ui_icons/", iconNames[i][j]);
        String buttonText = String.valueOf(numbers[i][j]);
        
        if (icon == null) {
          posButton.setText(buttonText);
        }
        else {
          posButton.setIcon(icon);
          if (POSConstants.CLEAR_ALL.equals(buttonText)) {
            posButton.setText(buttonText);
          }
        }
        
        posButton.setActionCommand(buttonText);
        posButton.setPreferredSize(preferredSize);
        posButton.addActionListener(this);
        centerPanel.add(posButton);
      }
    }
    add(centerPanel, "Center");
    
    titledBorder = new TitledBorder("");
    titledBorder.setTitleJustification(2);
    
    setBorder(titledBorder);
    
    buttonAction();
    setVisibleControlsButton(false);
  }
  
  public void setVisibleControlsButton(boolean visible) {
    btnIncrementQuantity.setVisible(visible);
    btnDecrementQuantity.setVisible(visible);
  }
  
  public void buttonAction() {
    try {
      btnIncrementQuantity.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          double total = Double.parseDouble(tfNumber.getText());
          if ((total == Math.floor(total)) || (!Double.isInfinite(total))) {
            total += 1.0D;
            tfNumber.setText("" + NumberUtil.trimDecilamIfNotNeeded(Double.valueOf(total)));
          }
          
        }
      });
      btnDecrementQuantity.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          double total = Double.parseDouble(tfNumber.getText());
          if ((total == Math.floor(total)) || (!Double.isInfinite(total)))
          {
            total -= 1.0D;
            if (total >= 0.0D) {
              tfNumber.setText("" + NumberUtil.trimDecilamIfNotNeeded(Double.valueOf(total)));
            }
          }
        }
      });
    } catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    if (actionCommand.equals(POSConstants.CLEAR_ALL)) {
      tfNumber.setText("0");
    }
    else if (actionCommand.equals(POSConstants.CLEAR)) {
      String s = tfNumber.getText();
      if (s.length() > 1) {
        s = s.substring(0, s.length() - 1);
      }
      else {
        s = "0";
      }
      tfNumber.setText(s);
    }
    else if (actionCommand.equals(".")) {
      if ((isDecimalAllowed()) && (tfNumber.getText().indexOf('.') < 0)) {
        String string = tfNumber.getText() + ".";
        if (!validate(string)) {
          POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("NumberSelectionView.1"));
          return;
        }
        tfNumber.setText(string);
      }
    }
    else {
      String s = tfNumber.getText();
      if (s.equals("0")) {
        tfNumber.setText(actionCommand);
        return;
      }
      
      s = actionCommand;
      if (!validate(s)) {
        POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("NumberSelectionView.0"));
        return;
      }
      tfNumber.replaceSelection(s);
    }
  }
  
  private boolean validate(String str)
  {
    if (isDecimalAllowed()) {
      try {
        Double.parseDouble(str);
      } catch (Exception x) {
        return false;
      }
    } else {
      try
      {
        Integer.parseInt(str);
      } catch (Exception x) {
        return false;
      }
    }
    return true;
  }
  
  public void setTitle(String title) {
    titledBorder.setTitle(title);
  }
  
  public double getValue() {
    return Double.parseDouble(tfNumber.getText());
  }
  
  public String getText() {
    return tfNumber.getText();
  }
  
  public void setValue(double value) {
    if (isDecimalAllowed()) {
      tfNumber.setText(String.valueOf(value));
    }
    else {
      tfNumber.setText(String.valueOf((int)value));
    }
  }
  
  public boolean isDecimalAllowed() {
    return decimalAllowed;
  }
  
  public void setDecimalAllowed(boolean decimalAllowed) {
    this.decimalAllowed = decimalAllowed;
  }
}
