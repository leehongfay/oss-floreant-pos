package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.PayoutReason;
import com.floreantpos.model.PayoutRecepient;
import com.floreantpos.model.dao.PayoutReasonDAO;
import com.floreantpos.model.dao.PayoutRecepientDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.NotesDialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;




























public class PayOutView
  extends TransparentPanel
{
  private JComboBox cbReason;
  private JComboBox cbRecepient;
  private NumberSelectionView numberSelectionView;
  private JTextArea tfNote;
  
  public PayOutView()
  {
    init();
  }
  
  public void initialize() {
    PayoutReasonDAO reasonDAO = new PayoutReasonDAO();
    List<PayoutReason> reasons = reasonDAO.findAll();
    cbReason.setModel(new DefaultComboBoxModel(reasons.toArray()));
    
    PayoutRecepientDAO recepientDAO = new PayoutRecepientDAO();
    List<PayoutRecepient> recepients = recepientDAO.findAll();
    cbRecepient.setModel(new DefaultComboBoxModel(new Vector(recepients)));
  }
  
  private void init() {
    setLayout(new MigLayout("inset 0,fill"));
    numberSelectionView = new NumberSelectionView();
    numberSelectionView.setTitle(POSConstants.AMOUNT_PAID_OUT);
    numberSelectionView.setBorder(BorderFactory.createCompoundBorder(numberSelectionView.getBorder(), new EmptyBorder(5, 5, 5, 5)));
    numberSelectionView.setDecimalAllowed(true);
    
    Font font1 = new Font("Tahoma", 1, PosUIManager.getFontSize(12));
    Font font2 = new Font("Tahoma", 1, PosUIManager.getFontSize(18));
    
    JLabel lblPayoutReason = new JLabel(POSConstants.PAY_OUT_REASON);
    JLabel lblPayOutReceipient = new JLabel(POSConstants.SELECT_PAY_OUT_RECEPIENT);
    JLabel lblNote = new JLabel(Messages.getString("PayOutView.5"));
    




    Dimension size = PosUIManager.getSize(300, 40);
    cbReason = new JComboBox();
    cbReason.setPreferredSize(size);
    cbRecepient = new JComboBox();
    cbRecepient.setPreferredSize(size);
    tfNote = new JTextArea();
    
    cbReason.setFont(font2);
    cbRecepient.setFont(font2);
    
    JScrollPane jScrollPane1 = new JScrollPane();
    tfNote.setColumns(20);
    tfNote.setEditable(false);
    tfNote.setLineWrap(true);
    tfNote.setRows(4);
    tfNote.setWrapStyleWord(true);
    jScrollPane1.setViewportView(tfNote);
    
    PosButton btnAddNote = new PosButton("...");
    btnAddNote.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        PayOutView.this.btnAddNoteActionPerformed(evt);
      }
      
    });
    PosButton btnNewReason = new PosButton("...");
    btnNewReason.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doNewReason();
      }
      
    });
    PosButton btnNewRecepient = new PosButton("...");
    btnNewRecepient.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doNewRecepient();
      }
      
    });
    int width = PosUIManager.getSize(70);
    
    JPanel inputPanel = new JPanel(new MigLayout("fill", "grow, fill", ""));
    inputPanel.add(lblPayoutReason, "grow,wrap");
    inputPanel.add(cbReason, "grow");
    inputPanel.add(btnNewReason, "grow,wrap,w " + width + "!");
    
    inputPanel.add(lblPayOutReceipient, "grow,gaptop 30,wrap");
    inputPanel.add(cbRecepient, "grow");
    inputPanel.add(btnNewRecepient, "grow,wrap,w " + width + "!");
    
    inputPanel.add(lblNote, "grow,gaptop 30,wrap");
    inputPanel.add(jScrollPane1, "grow,spany,h " + PosUIManager.getSize(120) + "!");
    inputPanel.add(btnAddNote, "grow, wrap,w " + width + "!");
    
    add(numberSelectionView, "grow");
    add(inputPanel, "grow");
  }
  
  protected void doNewRecepient() {
    NotesDialog dialog = new NotesDialog();
    dialog.setTitle(Messages.getString("PayOutView.0"));
    dialog.pack();
    dialog.open();
    
    if (dialog.isCanceled()) {
      return;
    }
    
    PayoutRecepient recepient = new PayoutRecepient();
    recepient.setName(dialog.getNote());
    
    PayoutRecepientDAO.getInstance().saveOrUpdate(recepient);
    DefaultComboBoxModel<PayoutRecepient> model = (DefaultComboBoxModel)cbRecepient.getModel();
    model.addElement(recepient);
  }
  
  protected void doNewReason() {
    NotesDialog dialog = new NotesDialog();
    dialog.setTitle(Messages.getString("PayOutView.10"));
    dialog.pack();
    dialog.open();
    
    if (dialog.isCanceled()) {
      return;
    }
    
    PayoutReason reason = new PayoutReason();
    reason.setReason(dialog.getNote());
    
    PayoutReasonDAO.getInstance().saveOrUpdate(reason);
    DefaultComboBoxModel<PayoutReason> model = (DefaultComboBoxModel)cbReason.getModel();
    model.addElement(reason);
  }
  
  private void btnAddNoteActionPerformed(ActionEvent evt) {
    NotesDialog dialog = new NotesDialog();
    dialog.setTitle(POSConstants.ENTER_PAYOUT_NOTE);
    dialog.pack();
    dialog.open();
    
    if (!dialog.isCanceled()) {
      tfNote.setText(dialog.getNote());
    }
  }
  
  public double getPayoutAmount() {
    return numberSelectionView.getValue();
  }
  
  public String getNote() {
    return tfNote.getText();
  }
  
  public PayoutReason getReason() {
    return (PayoutReason)cbReason.getSelectedItem();
  }
  
  public PayoutRecepient getRecepient() {
    return (PayoutRecepient)cbRecepient.getSelectedItem();
  }
}
