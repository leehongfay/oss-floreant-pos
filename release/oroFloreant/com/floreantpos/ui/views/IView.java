package com.floreantpos.ui.views;

import com.floreantpos.ui.views.order.actions.DataChangeListener;
import java.awt.Component;

public abstract interface IView
{
  public abstract String getViewName();
  
  public abstract Component getViewComponent();
  
  public abstract void refresh();
  
  public abstract DataChangeListener getDataChangeListener();
}
