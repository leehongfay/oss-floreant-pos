package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.actions.DrawerAssignmentAction;
import com.floreantpos.actions.ManageTableLayoutAction;
import com.floreantpos.actions.PosAction;
import com.floreantpos.actions.ShowGiftCardAction;
import com.floreantpos.actions.ShowKitchenDisplayAction;
import com.floreantpos.actions.ShowLineDisplayAction;
import com.floreantpos.actions.ShowOnlineTicketManagementAction;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.FloreantPlugin;
import com.floreantpos.main.Application;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.UserType;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.ui.views.order.ViewPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import net.miginfocom.swing.MigLayout;






















public class SwitchboardOtherFunctionsView
  extends ViewPanel
{
  public static final String VIEW_NAME = "ALL FUNCTIONS";
  private static SwitchboardOtherFunctionsView instance;
  private Set<UserPermission> permissions;
  private JPanel contentPanel;
  private DrawerAssignmentAction drawerAssignAction;
  
  public SwitchboardOtherFunctionsView()
  {
    setLayout(new BorderLayout(5, 5));
    PosButton btnBack = new PosButton(Messages.getString("SwitchboardOtherFunctionsView.1"));
    btnBack.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        RootView.getInstance().showDefaultView();
      }
    });
    add(btnBack, "South");
    
    contentPanel = new JPanel(new MigLayout("hidemode 3,align 50% 50%, wrap 3", "sg fill", ""));
    
    User user = Application.getCurrentUser();
    if (user != null) {
      UserType newUserType = user.getType();
      
      if (newUserType != null) {
        permissions = newUserType.getPermissions();
      }
    }
    
    List<PosAction> actions = new ArrayList();
    drawerAssignAction = new DrawerAssignmentAction(Application.getInstance().getTerminal(), Application.getCurrentUser());
    






    if (TerminalConfig.isKDSenabled()) {
      actions.add(new ShowKitchenDisplayAction());
    }
    
    actions.add(new ManageTableLayoutAction());
    actions.add(new ShowOnlineTicketManagementAction());
    actions.add(new ShowLineDisplayAction());
    actions.add(new ShowGiftCardAction());
    
    List<FloreantPlugin> plugins = ExtensionManager.getPlugins();
    Iterator localIterator1; if (plugins != null) {
      for (localIterator1 = plugins.iterator(); localIterator1.hasNext();) { plugin = (FloreantPlugin)localIterator1.next();
        List<AbstractAction> posActions = plugin.getSpecialFunctionActions();
        if (posActions != null) {
          for (AbstractAction action : posActions) {
            actions.add((PosAction)action);
          }
        }
      }
    }
    FloreantPlugin plugin;
    Dimension size = PosUIManager.getSize(150, 150);
    for (PosAction action : actions) {
      if ((!(action instanceof DrawerAssignmentAction)) || 
        (Application.getInstance().getTerminal().isHasCashDrawer().booleanValue()))
      {



        PosButton button = new PosButton(action);
        contentPanel.add(button, "w " + width + "!, h " + height + "!");
      }
    }
    JScrollPane scrollPane = new JScrollPane(contentPanel);
    scrollPane.setBorder(null);
    add(scrollPane);
  }
  
  public static SwitchboardOtherFunctionsView getInstance() {
    if (instance == null) {
      instance = new SwitchboardOtherFunctionsView();
    }
    instance.updateView();
    return instance;
  }
  
  private void updateView() {
    drawerAssignAction.updateActionText();
  }
  
  public String getViewName()
  {
    return "ALL FUNCTIONS";
  }
  
  public JPanel getContentPanel() {
    return contentPanel;
  }
}
