package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.actions.SettleTicketAction;
import com.floreantpos.main.Application;
import com.floreantpos.model.GiftCard;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.OrderController;
import com.floreantpos.ui.views.payment.GiftCardProcessor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;








public class GiftCardAddBalanceView
  extends POSDialog
{
  private JTextField txtCardNumber;
  private DoubleTextField txtBalance;
  private GiftCard giftCard;
  private GiftCardProcessor giftCardProcessor;
  
  public GiftCardAddBalanceView(GiftCardProcessor giftCardProcessor)
  {
    this.giftCardProcessor = giftCardProcessor;
    init();
  }
  

  public GiftCardAddBalanceView(JFrame parent)
  {
    init();
  }
  
  private void init() {
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Add Balance");
    add(titlePanel, "North");
    
    JPanel centerPanel = new JPanel(new MigLayout("fillx,aligny center", "[][]", ""));
    
    JLabel lblNumberOfCard = new JLabel(Messages.getString("GiftCardAddBalanceView.3"));
    
    txtCardNumber = new JTextField(20);
    
    JLabel lblBalance = new JLabel(Messages.getString("GiftCardAddBalanceView.4"));
    
    txtBalance = new DoubleTextField(20);
    
    centerPanel.add(lblNumberOfCard, "cell 0 0, alignx right");
    centerPanel.add(txtCardNumber, "cell 1 0");
    centerPanel.add(lblBalance, "cell 0 1,alignx right");
    centerPanel.add(txtBalance, "cell 1 1");
    
    add(centerPanel, "Center");
    
    JPanel buttonPanel = new JPanel(new MigLayout("al center", "sg, fill", ""));
    
    PosButton btnGenerate = new PosButton(Messages.getString("GiftCardAddBalanceView.12"));
    buttonPanel.add(btnGenerate, "grow");
    
    btnGenerate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doGenerate();
      }
      

    });
    buttonPanel.add(new PosButton(new CloseDialogAction(this, "CANCEL")));
    
    add(buttonPanel, "South");
  }
  


















  public void doGenerate()
  {
    if (save())
    {


      POSMessageDialog.showMessage(this, Messages.getString("GiftCardAddBalanceView.15"));
      dispose();
    }
  }
  




  private boolean save()
  {
    String cardNo = txtCardNumber.getText();
    double balance = txtBalance.getDouble();
    giftCard = giftCardProcessor.getCard(cardNo);
    
    if (giftCard == null) {
      POSMessageDialog.showMessage(this, Messages.getString("GiftCardAddBalanceView.16"));
      return false;
    }
    if (StringUtils.isEmpty(cardNo)) {
      MessageDialog.showError(Messages.getString("GiftCardAddBalanceView.17"));
      return false;
    }
    if (balance == 0.0D) {
      MessageDialog.showError(Messages.getString("GiftCardAddBalanceView.18"));
      return false;
    }
    
    if (!giftCard.isActive().booleanValue()) {
      MessageDialog.showError(Messages.getString("GiftCardAddBalanceView.19"));
      return false;
    }
    


    Ticket ticket = new Ticket();
    ticket.setTaxIncluded(Boolean.valueOf(Application.getInstance().isPriceIncludesTax()));
    


    ticket.setTerminal(Application.getInstance().getTerminal());
    Calendar currentTime = DateUtil.getServerTimeCalendar();
    ticket.setCreateDate(currentTime.getTime());
    ticket.setOrderType(Application.getInstance().getCurrentOrderType());
    ticket.setCreationHour(Integer.valueOf(currentTime.get(11)));
    ticket.setOwner(Application.getCurrentUser());
    ticket.addTable(0);
    ticket.addProperty("cardNumber", cardNo);
    ticket.setShouldIncludeInSales(Boolean.valueOf(false));
    ticket.setRevenue_purpose("GIFT_CARD_ADD_BALANCE");
    TicketItem ticketItem = new TicketItem();
    ticketItem.setQuantity(Double.valueOf(1.0D));
    ticketItem.setUnitPrice(Double.valueOf(balance));
    ticketItem.setName(Messages.getString("GiftCardAddBalanceView.22"));
    ticketItem.setCategoryName(Messages.getString("GiftCardAddBalanceView.23"));
    ticketItem.setGroupName(Messages.getString("GiftCardAddBalanceView.24"));
    
    ticketItem.setTicket(ticket);
    ticket.addToticketItems(ticketItem);
    ticket.calculatePrice();
    
    OrderController.saveOrder(ticket);
    return new SettleTicketAction(ticket).performSettle();
  }
}
