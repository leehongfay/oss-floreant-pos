package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;




public class GiftCardDialog
  extends POSDialog
{
  public GiftCardDialog()
  {
    super(POSUtil.getBackOfficeWindow(), "");
    
    init();
  }
  

  public GiftCardDialog(JFrame parent)
  {
    init();
  }
  
  private void init()
  {
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Gift Card Dialog");
    add(titlePanel, "North");
    
    JPanel centerPanel = new JPanel(new MigLayout("fillx,aligny center", "[]20px[]", ""));
    
    centerPanel.setBorder(new EmptyBorder(0, 10, 0, 10));
    
    PosButton btnGenerate = new PosButton(Messages.getString("GiftCardDialog.4"));
    PosButton btnCardInfo = new PosButton(Messages.getString("GiftCardDialog.5"));
    
    centerPanel.add(btnGenerate, "cell 0 0, growx");
    centerPanel.add(btnCardInfo, "cell 1 0, growx");
    
    add(centerPanel, "Center");
    
    JButton closeButton = new JButton(new CloseDialogAction(this, Messages.getString("GiftCardDialog.8")));
    
    JPanel buttonsPanel = new JPanel(new MigLayout("fill"));
    
    buttonsPanel.add(closeButton, "alignx right");
    
    add(buttonsPanel, "South");
    
    btnGenerate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        GiftCardGeneratorView dialog = new GiftCardGeneratorView();
        dialog.setTitle(Messages.getString("GiftCardDialog.11"));
        dialog.setDefaultCloseOperation(2);
        dialog.setSize(PosUIManager.getSize(600, 400));
        dialog.setLocationRelativeTo(POSUtil.getBackOfficeWindow());
        dialog.setVisible(true);
      }
      

    });
    btnCardInfo.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {}
    });
  }
}
