package com.floreantpos.ui.views;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.customer.CustomerSelectorDialog;
import com.floreantpos.customer.CustomerSelectorFactory;
import com.floreantpos.model.Customer;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.MultipleNumberSelectionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.ticket.TicketViewerTable;
import com.floreantpos.ui.views.order.actions.SplitItemSelectionListener;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import net.miginfocom.swing.MigLayout;



































public class TicketSplitView
  extends TransparentPanel
  implements TableModelListener
{
  private Ticket ticket;
  public static final String VIEW_NAME = "TICKET_FOR_SPLIT_VIEW";
  private TicketSplitView ticketView1;
  private PosButton btnScrollDown;
  private PosButton btnScrollUp;
  private PosButton btnTransferToTicket1;
  private PosButton btnHalf;
  private PosButton btnShared;
  private JScrollPane scrollPane;
  private JTextField tfDiscount;
  private JTextField tfSubtotal;
  private JTextField tfTax;
  private JTextField tfTotal;
  private TicketViewerTable ticketViewerTable;
  private int viewNumber = 1;
  
  protected int splitNumber;
  private SplitItemSelectionListener listener;
  private PosButton btnCustomQty;
  private int noOfCustomSplit;
  private int totalTicketQuantity;
  private List<Integer> selectedTicketNumbers;
  private JPanel customerInfoPanel;
  private JLabel lblCustomerName;
  private PosButton btnChangeMember;
  
  public TicketSplitView(SplitItemSelectionListener listener)
  {
    this.listener = listener;
    initComponents();
    setOpaque(true);
    setTicket(ticket);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    setBorder(BorderFactory.createTitledBorder(null, POSConstants.TICKET, 2, 0));
    setPreferredSize(PosUIManager.getSize(280, 463));
    
    TransparentPanel ticketTableViewPanel = new TransparentPanel(new BorderLayout());
    ticketTableViewPanel.setBorder(new EmptyBorder(0, 5, 0, 0));
    
    int rightGap = PosUIManager.getSize(60) + 10;
    TransparentPanel ticketTotalPanel = new TransparentPanel(new MigLayout("wrap 2,right,inset 5 5 5 " + rightGap + " "));
    TransparentPanel rightButtonPanel = new TransparentPanel(new MigLayout("wrap 1,fill,hidemode 3,inset 1"));
    
    JLabel lblSubtotal = new JLabel(POSConstants.SUBTOTAL + ":");
    JLabel lblTotal = new JLabel(POSConstants.TOTAL + ":");
    JLabel lblDiscount = new JLabel(POSConstants.DISCOUNT + ":");
    JLabel lblTax = new JLabel(POSConstants.TAX + ":");
    
    tfSubtotal = new JTextField();
    tfSubtotal.setHorizontalAlignment(11);
    tfSubtotal.setColumns(10);
    
    tfTax = new JTextField();
    tfTax.setHorizontalAlignment(11);
    tfTax.setColumns(10);
    
    tfDiscount = new JTextField();
    tfDiscount.setHorizontalAlignment(11);
    tfDiscount.setColumns(10);
    
    tfTotal = new JTextField();
    tfTotal.setHorizontalAlignment(11);
    tfTotal.setColumns(10);
    
    btnScrollUp = new PosButton();
    btnScrollDown = new PosButton();
    btnTransferToTicket1 = new PosButton();
    scrollPane = new JScrollPane();
    ticketViewerTable = new TicketViewerTable();
    
    lblSubtotal.setHorizontalAlignment(4);
    ticketTotalPanel.add(lblSubtotal);
    
    tfSubtotal.setEditable(false);
    ticketTotalPanel.add(tfSubtotal);
    
    lblTotal.setHorizontalAlignment(4);
    ticketTotalPanel.add(lblTotal);
    
    tfTotal.setEditable(false);
    ticketTotalPanel.add(tfTotal);
    
    lblDiscount.setHorizontalAlignment(4);
    ticketTotalPanel.add(lblDiscount);
    
    tfDiscount.setEditable(false);
    ticketTotalPanel.add(tfDiscount);
    
    lblTax.setHorizontalAlignment(4);
    ticketTotalPanel.add(lblTax);
    
    tfTax.setEditable(false);
    ticketTotalPanel.add(tfTax);
    
    btnScrollUp.setIcon(IconFactory.getIcon("/ui_icons/", "up.png"));
    btnScrollUp.setPreferredSize(PosUIManager.getSize(60, 50));
    btnScrollUp.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketSplitView.this.doScrollUp(evt);
      }
      
    });
    rightButtonPanel.add(btnScrollUp, "grow");
    
    btnScrollDown.setIcon(IconFactory.getIcon("/ui_icons/", "down.png"));
    btnScrollDown.setPreferredSize(PosUIManager.getSize(60, 50));
    btnScrollDown.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketSplitView.this.doScrollDown(evt);
      }
      
    });
    rightButtonPanel.add(btnScrollDown, "grow");
    
    btnTransferToTicket1.setPreferredSize(PosUIManager.getSize(60, 50));
    btnTransferToTicket1.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketSplitView.this.btnTransferToTicket1ActionPerformed(1);
      }
      
    });
    rightButtonPanel.add(btnTransferToTicket1, "grow");
    
    btnHalf = new PosButton();
    btnHalf.setText("1/2");
    btnHalf.setPreferredSize(PosUIManager.getSize(60, 50));
    btnHalf.setFont(new Font("Tahoma", 1, 18));
    btnHalf.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketSplitView.this.btnTransferToTicket1ActionPerformed(0);
      }
      
    });
    rightButtonPanel.add(btnHalf, "grow");
    
    btnShared = new PosButton(">>>");
    btnShared.setFont(new Font("Tahoma", 1, 18));
    btnShared.setPreferredSize(PosUIManager.getSize(60, 50));
    btnShared.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketSplitView.this.btnTransferToTicket1ActionPerformed(2);
      }
      
    });
    rightButtonPanel.add(btnShared, "grow");
    
    btnCustomQty = new PosButton("...");
    btnCustomQty.setFont(new Font("Tahoma", 1, 18));
    btnCustomQty.setPreferredSize(PosUIManager.getSize(60, 50));
    btnCustomQty.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketSplitView.this.btnTransferToTicket1ActionPerformed(3);
      }
      
    });
    rightButtonPanel.add(btnCustomQty, "grow");
    
    scrollPane.setHorizontalScrollBarPolicy(31);
    scrollPane.setVerticalScrollBarPolicy(21);
    scrollPane.setViewportView(ticketViewerTable);
    
    ticketTableViewPanel.add(scrollPane, "Center");
    
    add(rightButtonPanel, "East");
    add(ticketTotalPanel, "South");
    add(ticketTableViewPanel, "Center");
  }
  
  private void createCustomerInfoPanel() {
    JPanel memberInfoPanel = new JPanel(new BorderLayout());
    memberInfoPanel.setBackground(Color.red);
    btnChangeMember = new PosButton("Change Member");
    lblCustomerName = new JLabel();
    
    customerInfoPanel = new JPanel(new MigLayout("", "", "grow"));
    JLabel lblMemName = new JLabel("MEMBER:");
    customerInfoPanel.add(lblMemName);
    customerInfoPanel.add(lblCustomerName, "grow");
    
    memberInfoPanel.add(customerInfoPanel, "Center");
    memberInfoPanel.add(btnChangeMember, "East");
    
    btnChangeMember.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TicketSplitView.this.doChangeMember();
      }
    });
    add(memberInfoPanel, "North");
  }
  
  private void btnTransferToTicket1ActionPerformed(int itemSplitType) {
    try {
      if ((ticketView1 != null) && (ticketView1.isVisible())) {
        int selectedRow = ticketViewerTable.getSelectedRow();
        Object object = ticketViewerTable.get(selectedRow);
        
        if ((object instanceof TicketItem)) {
          TicketItem ticketItem = (TicketItem)object;
          double ticketItemQuantity = ticketItem.getQuantity().doubleValue();
          double splitQuantity = 1.0D;
          if (itemSplitType == 1) {
            if (ticketItemQuantity < 1.0D) {
              splitQuantity = ticketItemQuantity;
            }
          }
          else if (itemSplitType == 0) {
            splitQuantity = ticketItemQuantity / 2.0D;
          }
          else if (itemSplitType == 3) {
            List<Integer> viewNumbers = new ArrayList();
            for (int i = 0; i < totalTicketQuantity; i++) {
              viewNumbers.add(Integer.valueOf(i + 1));
            }
            if (viewNumbers.size() < 2) {
              POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please choose more than two split.");
              return;
            }
            MultipleNumberSelectionDialog dialog = new MultipleNumberSelectionDialog(viewNumbers);
            dialog.pack();
            dialog.open();
            if (dialog.isCanceled())
              return;
            selectedTicketNumbers = dialog.getViewNumbers();
            if ((selectedTicketNumbers == null) || (selectedTicketNumbers.isEmpty()))
              return;
          }
          listener.itemSelected((TicketItem)object, this, ticketView1, itemSplitType, Double.valueOf(splitQuantity));
        }
      }
    } catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  public void allowCustomerSelection(boolean b) {
    if (b) {
      createCustomerInfoPanel();
    }
  }
  
  public int getNoOfCustomSplit() {
    return noOfCustomSplit;
  }
  
  public List<Integer> getViewNumbers() {
    return selectedTicketNumbers;
  }
  
  private void doScrollDown(ActionEvent evt) {
    ticketViewerTable.scrollDown();
  }
  
  private void doScrollUp(ActionEvent evt) {
    ticketViewerTable.scrollUp();
  }
  
  public void updateModel() {
    ticket.calculatePrice();
  }
  
  public void updateView() {
    if ((ticket == null) || (ticket.getTicketItems() == null) || (ticket.getTicketItems().size() <= 0)) {
      tfSubtotal.setText("");
      tfDiscount.setText("");
      tfTax.setText("");
      tfTotal.setText("");
      if ((ticket != null) && (customerInfoPanel != null)) {
        lblCustomerName.setText(ticket.getProperty("CUSTOMER_NAME"));
      }
      return;
    }
    
    ticket.calculatePrice();
    if (customerInfoPanel != null)
      lblCustomerName.setText(ticket.getProperty("CUSTOMER_NAME"));
    tfSubtotal.setText(NumberUtil.formatNumber(ticket.getSubtotalAmount()));
    tfDiscount.setText(NumberUtil.formatNumber(ticket.getDiscountAmount()));
    tfTax.setText(NumberUtil.formatNumber(ticket.getTaxAmount()));
    tfTotal.setText(NumberUtil.formatNumber(ticket.getTotalAmountWithTips()));
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void setTicket(Ticket _ticket) {
    ticket = _ticket;
    ticketViewerTable.setTicket(_ticket);
    
    updateView();
  }
  
  public void tableChanged(TableModelEvent e) {
    if ((ticket == null) || (ticket.getTicketItems() == null) || (ticket.getTicketItems().size() <= 0)) {
      tfSubtotal.setText("");
      tfDiscount.setText("");
      tfTax.setText("");
      tfTotal.setText("");
      
      return;
    }
    
    ticket.calculatePrice();
    
    tfSubtotal.setText(NumberUtil.formatNumber(ticket.getSubtotalAmount()));
    tfDiscount.setText(NumberUtil.formatNumber(ticket.getDiscountAmount()));
    tfTax.setText(NumberUtil.formatNumber(ticket.getTaxAmount()));
    tfTotal.setText(NumberUtil.formatNumber(ticket.getTotalAmountWithTips()));
  }
  
  public TicketSplitView getTicketView1() {
    return ticketView1;
  }
  
  public void setTicketView1(TicketSplitView ticketView1) {
    this.ticketView1 = ticketView1;
  }
  
  public int getViewNumber() {
    return viewNumber;
  }
  
  public TicketViewerTable getTicketViewerTable() {
    return ticketViewerTable;
  }
  
  public void setViewNumber(int viewNumber) {
    this.viewNumber = viewNumber;
    
    if (viewNumber != 1) {
      btnShared.setVisible(false);
      btnHalf.setVisible(false);
      btnCustomQty.setVisible(false);
    }
    String title = Messages.getString("TicketForSplitView.1") + viewNumber;
    if (viewNumber == 1) {
      title = Messages.getString("TicketSplitView.14");
    }
    TitledBorder titledBorder = new TitledBorder(title);
    titledBorder.setTitleJustification(2);
    
    setBorder(titledBorder);
    
    switch (viewNumber) {
    case 1: 
      btnTransferToTicket1.setIcon(IconFactory.getIcon("next.png"));
      break;
    
    case 2: 
      btnTransferToTicket1.setIcon(IconFactory.getIcon("previous.png"));
    }
    
  }
  
  public void setTotalTicketQuantity(int size)
  {
    totalTicketQuantity = size;
  }
  
  private void doChangeMember() {
    try {
      CustomerSelectorDialog dialog = CustomerSelectorFactory.createCustomerSelectorDialog(ticket.getOrderType());
      dialog.setCreateNewTicket(false);
      if (ticket != null) {
        dialog.setTicket(ticket);
      }
      dialog.openUndecoratedFullScreen();
      
      if (!dialog.isCanceled()) {
        Customer selectedCustomer = dialog.getSelectedCustomer();
        ticket.setCustomer(selectedCustomer);
        lblCustomerName.setText(selectedCustomer.getFirstName());
      }
    } catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
}
