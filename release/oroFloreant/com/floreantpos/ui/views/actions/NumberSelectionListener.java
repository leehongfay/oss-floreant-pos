package com.floreantpos.ui.views.actions;

public abstract interface NumberSelectionListener
{
  public abstract void numberSelected(double paramDouble);
}
