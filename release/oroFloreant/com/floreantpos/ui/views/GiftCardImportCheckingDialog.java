package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.PosLog;
import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.bo.ui.explorer.GiftCardExplorer;
import com.floreantpos.config.AppProperties;
import com.floreantpos.model.GiftCard;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.BeanTableModel.EditMode;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.DateTimePicker;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.swingx.JXTable;

public class GiftCardImportCheckingDialog extends POSDialog
{
  private BeanTableModel<GiftCard> tableModel;
  private JXTable table;
  private File file;
  private List<GiftCard> giftCards;
  private TitlePanel titlePanel;
  
  public GiftCardImportCheckingDialog(File file)
  {
    this.file = file;
    init();
    initData();
  }
  
  private void initData() {
    try {
      List<GiftCard> giftCards = importData(file.toURI().toURL());
      if (giftCards == null) {
        return;
      }
      tableModel.setRows(giftCards);
      updateTitle(giftCards.size());
    }
    catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Error in importing file!");
      PosLog.error(GiftCardImportCheckingDialog.class, e.getMessage(), e);
    }
  }
  
  private void updateTitle(int length)
  {
    titlePanel.setTitle("Imported " + length + " gift cards, please review and then click save to make it permanent.");
  }
  
  private void init() {
    setTitle(AppProperties.getAppName());
    titlePanel = new TitlePanel();
    tableModel = new BeanTableModel(GiftCard.class);
    tableModel.addColumn(Messages.getString("GiftCardExplorer.42"), "customCardNumber");
    tableModel.addColumn(Messages.getString("GiftCardExplorer.44"), "ownerName", BeanTableModel.EditMode.EDITABLE);
    tableModel.addColumn(Messages.getString("GiftCardExplorer.46"), "pinNumber", BeanTableModel.EditMode.EDITABLE);
    tableModel.addColumn(Messages.getString("GiftCardExplorer.48"), "batchNo", BeanTableModel.EditMode.EDITABLE);
    tableModel.addColumn(Messages.getString("GiftCardExplorer.50"), "balance", BeanTableModel.EditMode.EDITABLE);
    tableModel.addColumn(Messages.getString("GiftCardExplorer.52"), "issueDate", BeanTableModel.EditMode.EDITABLE);
    tableModel.addColumn(Messages.getString("GiftCardExplorer.54"), "activationDate", BeanTableModel.EditMode.EDITABLE);
    tableModel.addColumn(Messages.getString("GiftCardExplorer.56"), "deActivationDate", BeanTableModel.EditMode.EDITABLE);
    tableModel.addColumn(Messages.getString("GiftCardExplorer.58"), "expiryDate", BeanTableModel.EditMode.EDITABLE);
    tableModel.addColumn(Messages.getString("GiftCardExplorer.60"), "active", BeanTableModel.EditMode.EDITABLE);
    tableModel.addColumn(Messages.getString("GiftCardExplorer.62"), "disable", BeanTableModel.EditMode.EDITABLE);
    tableModel.addColumn(Messages.getString("GiftCardExplorer.64"), "duration", BeanTableModel.EditMode.EDITABLE);
    tableModel.addColumn(Messages.getString("GiftCardExplorer.66"), "durationType", BeanTableModel.EditMode.EDITABLE);
    tableModel.addColumn(Messages.getString("GiftCardExplorer.68"), "point", BeanTableModel.EditMode.EDITABLE);
    
    table = new JXTable(tableModel) {
      public void changeSelection(int row, int column, boolean toggle, boolean extend) {
        super.changeSelection(row, column, toggle, extend);
        table.editCellAt(row, column);
        DefaultCellEditor editor = (DefaultCellEditor)table.getCellEditor(row, column);
        if ((editor.getComponent() instanceof JTextField)) {
          JTextField textField = (JTextField)editor.getComponent();
          textField.requestFocus();
          textField.selectAll();
        }
        
      }
    };
    table.getColumnModel().getColumn(5).setCellEditor(new JDateChooserEditor(new JCheckBox()));
    table.getColumnModel().getColumn(6).setCellEditor(new JDateChooserEditor(new JCheckBox()));
    table.getColumnModel().getColumn(7).setCellEditor(new JDateChooserEditor(new JCheckBox()));
    table.getColumnModel().getColumn(8).setCellEditor(new JDateChooserEditor(new JCheckBox()));
    


    JComboBox cbDuration = new JComboBox();
    cbDuration.addItem("DAY");
    cbDuration.addItem("MONTH");
    cbDuration.addItem("YEAR");
    
    table.getColumnModel().getColumn(12).setCellEditor(new JComboBoxEditor(cbDuration));
    
    table.getInputMap().put(KeyStroke.getKeyStroke(32, 0), "startEditing");
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.setRowHeight(PosUIManager.getSize(30));
    
    resizeColumnWidth(table);
    
    setLayout(new BorderLayout(5, 5));
    add(titlePanel, "North");
    add(new PosScrollPane(table), "Center");
    
    JPanel buttonPanel = new JPanel(new MigLayout("al center", "sg, fill", ""));
    
    PosButton btnSetBatchNumber = new PosButton("SET BATCH NO");
    buttonPanel.add(btnSetBatchNumber, "grow");
    btnSetBatchNumber.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent ev) {
        try {
          String batchNumber = JOptionPane.showInputDialog(GiftCardImportCheckingDialog.this, "Enter batch number:");
          if (StringUtils.isEmpty(batchNumber)) {
            return;
          }
          int option = JOptionPane.showConfirmDialog(GiftCardImportCheckingDialog.this, "This will set batch no. to all imported gift cards. Proceed?");
          if (option != 0) {
            return;
          }
          List<GiftCard> rows = new ArrayList(tableModel.getRows());
          for (GiftCard giftCard : rows) {
            giftCard.setBatchNo(batchNumber);
          }
          tableModel.fireTableRowsUpdated(0, table.getRowCount() - 1);
        } catch (Exception e) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
        }
      }
    });
    PosButton btnSave = new PosButton("SAVE");
    buttonPanel.add(btnSave, "grow");
    
    btnSave.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        giftCards = new ArrayList();
        giftCards.addAll(tableModel.getRows());
        setCanceled(false);
        dispose();
      }
      
    });
    PosButton btnDelete = new PosButton("DELETE");
    buttonPanel.add(btnDelete, "grow");
    
    btnDelete.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        GiftCardImportCheckingDialog.this.doDeleteItem();
      }
      
    });
    buttonPanel.add(new PosButton(new CloseDialogAction(this, Messages.getString("UserListDialog.5"))));
    
    add(buttonPanel, "South");
  }
  
  private void doDeleteItem()
  {
    int index = table.getSelectedRow();
    if (index < 0)
      return;
    int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Do you want to delete selected item?", "Delete item?");
    
    if (option != 0) {
      return;
    }
    tableModel.removeRow(index);
    updateTitle(tableModel.getRows().size());
  }
  
  public void resizeColumnWidth(JTable table)
  {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(80));
    columnWidth.add(Integer.valueOf(180));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(85));
    columnWidth.add(Integer.valueOf(90));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(50));
    
    return columnWidth;
  }
  
  private List<GiftCard> importData(URL url)
  {
    return doImportCSVFile(url);
  }
  
  private List<GiftCard> doImportCSVFile(URL url)
  {
    InputStream in = null;
    try {
      in = url.openStream();
      
      BufferedReader streamReader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
      List<GiftCard> giftCards = new ArrayList();
      
      Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader(new String[0]).parse(streamReader);
      String batchNumber = GiftCardGeneratorView.generateBatchNumber();
      for (Object localObject1 = records.iterator(); ((Iterator)localObject1).hasNext();) { CSVRecord record = (CSVRecord)((Iterator)localObject1).next();
        String cardNumber = getStringValue("Card Number", record);
        String ownerName = getStringValue("Owner Name", record);
        Date issueDate = getDateValue("Issue Date", record);
        Date expirationDate = getDateValue("Expiration Date", record);
        Date activationDate = getDateValue("Activation Date", record);
        Date deActivationDate = getDateValue("Deactivation Date", record);
        Double balance = getDoubleValue("Balance", record);
        Boolean isActive = getBooleanValue("Active", record);
        Boolean isDisable = getBooleanValue("Disable", record);
        String durationType = getStringValue("Duration Type", record);
        Integer duration = getIntegerValue("Duration", record);
        String pinNumber = getStringValue("Pin Number", record);
        Integer point = getIntegerValue("Point", record);
        String email = getStringValue("Email", record);
        String type = getStringValue("Type", record);
        
        GiftCard giftCard = new GiftCard();
        giftCard.setCardNumber(cardNumber);
        giftCard.setOwnerName(ownerName);
        giftCard.setIssueDate(issueDate);
        giftCard.setExpiryDate(expirationDate);
        giftCard.setActivationDate(activationDate);
        giftCard.setDeActivationDate(deActivationDate);
        giftCard.setBalance(balance);
        giftCard.setActive(isActive);
        giftCard.setDisable(isDisable);
        giftCard.setDurationType(durationType);
        giftCard.setDuration(duration);
        giftCard.setPinNumber(pinNumber);
        giftCard.setPoint(point);
        giftCard.setBatchNo(batchNumber);
        giftCard.setEmail(email);
        giftCard.setType(type);
        giftCards.add(giftCard);
      }
      

      return giftCards;
    }
    catch (Exception e) {
      PosLog.error(GiftCardExplorer.class, "Error: " + e);
      e.printStackTrace();
    } finally {
      IOUtils.closeQuietly(in);
    }
    return null;
  }
  
  private Date getDateValue(String value, CSVRecord record) {
    try {
      if (StringUtils.isEmpty(value)) {
        return null;
      }
      String txtValue = record.get(value);
      if (StringUtils.isNotEmpty(txtValue)) {
        return DateUtil.parseByFullDate(txtValue);
      }
    }
    catch (Exception localException) {}
    
    return null;
  }
  
  private String getStringValue(String value, CSVRecord record) {
    try {
      String txtValue = record.get(value);
      if (StringUtils.isNotEmpty(txtValue)) {
        return txtValue;
      }
    }
    catch (Exception localException) {}
    
    return null;
  }
  
  private Double getDoubleValue(String value, CSVRecord record) {
    try {
      String txtValue = record.get(value);
      if (StringUtils.isNotEmpty(txtValue)) {
        return Double.valueOf(Double.parseDouble(txtValue));
      }
    }
    catch (Exception localException) {}
    
    return null;
  }
  
  private Integer getIntegerValue(String value, CSVRecord record) {
    try {
      String txtValue = record.get(value);
      if (StringUtils.isNotEmpty(txtValue)) {
        return Integer.valueOf(Integer.parseInt(txtValue));
      }
    }
    catch (Exception localException) {}
    
    return null;
  }
  
  private Boolean getBooleanValue(String value, CSVRecord record) {
    try {
      String txtValue = record.get(value);
      if (StringUtils.isNotEmpty(txtValue)) {
        return Boolean.valueOf(Boolean.parseBoolean(txtValue));
      }
    }
    catch (Exception localException) {}
    
    return null;
  }
  
  public List<GiftCard> getGiftCards() {
    return giftCards;
  }
  

  public void setGiftCards(List<GiftCard> giftCards) { this.giftCards = giftCards; }
  
  class JDateChooserEditor extends DefaultCellEditor {
    DateTimePicker datePicker;
    
    public JDateChooserEditor(JCheckBox checkBox) { super(); }
    




    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
      datePicker = new DateTimePicker();
      GiftCard giftCard = (GiftCard)tableModel.getRow(row);
      if (column == 5) {
        datePicker.setDate(giftCard.getIssueDate());
      }
      else if (column == 6) {
        datePicker.setDate(giftCard.getActivationDate());
      }
      else if (column == 7) {
        datePicker.setDate(giftCard.getDeActivationDate());
      }
      else if (column == 8) {
        datePicker.setDate(giftCard.getExpiryDate());
      }
      
      return datePicker;
    }
    
    public Object getCellEditorValue() {
      return datePicker.getDate();
    }
    
    public boolean stopCellEditing() {
      return super.stopCellEditing();
    }
    
    protected void fireEditingStopped() {
      super.fireEditingStopped();
    }
  }
  
  class JComboBoxEditor extends DefaultCellEditor {
    JComboBox combobox;
    
    public JComboBoxEditor(JComboBox comboBox) {
      super();
    }
    
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
      GiftCard giftCard = (GiftCard)tableModel.getRow(row);
      combobox = ((JComboBox)getComponent());
      if ((column == 12) && 
        (giftCard.getDurationType() != null)) {
        combobox.setSelectedItem(giftCard.getDurationType());
      }
      
      return combobox;
    }
    
    public Object getCellEditorValue() {
      return combobox.getSelectedItem();
    }
    
    public boolean stopCellEditing() {
      return super.stopCellEditing();
    }
    
    protected void fireEditingStopped() {
      super.fireEditingStopped();
    }
  }
}
