package com.floreantpos.ui.views.voidticket;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.VoidItem;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.ticket.VoidTicketItemViewerTable;
import com.floreantpos.ui.views.VoidItemFormDialog;
import com.floreantpos.ui.views.order.actions.VoidTicketItemSplitListener;
import com.floreantpos.util.NumberUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;





public class VoidTicketItemView
  extends TransparentPanel
  implements TableModelListener
{
  private Ticket ticket;
  private VoidItemSplitView voidItemSplitView;
  private PosButton btnScrollDown;
  private PosButton btnScrollUp;
  private PosButton btnTransferToTicket1;
  private JScrollPane scrollPane;
  private JTextField tfDiscount;
  private JTextField tfSubtotal;
  private JTextField tfTax;
  private JTextField tfTotal;
  private VoidTicketItemViewerTable ticketViewerTable;
  private VoidTicketItemSplitListener listener;
  private VoidItem voidItem;
  
  public VoidTicketItemView(VoidTicketItemSplitListener listener)
  {
    this.listener = listener;
    initComponents();
    setOpaque(true);
    setTicket(ticket);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    setBorder(BorderFactory.createTitledBorder(null, "Ticket Item", 2, 0));
    setPreferredSize(PosUIManager.getSize(280, 463));
    
    TransparentPanel ticketTableViewPanel = new TransparentPanel(new BorderLayout(5, 5));
    ticketTableViewPanel.setBorder(new EmptyBorder(0, 5, 0, 0));
    
    int rightGap = PosUIManager.getSize(60) + 10;
    TransparentPanel ticketTotalPanel = new TransparentPanel(new MigLayout("wrap 2,ins 5 0 5 0,right", "[][]" + rightGap + "", ""));
    TransparentPanel rightButtonPanel = new TransparentPanel(new MigLayout("wrap 1,fill,hidemode 3,inset 1"));
    
    JLabel lblSubtotal = new JLabel(POSConstants.SUBTOTAL + ":");
    JLabel lblTotal = new JLabel(POSConstants.TOTAL + ":");
    JLabel lblDiscount = new JLabel(POSConstants.DISCOUNT + ":");
    JLabel lblTax = new JLabel(POSConstants.TAX + ":");
    
    tfSubtotal = new JTextField();
    tfSubtotal.setHorizontalAlignment(11);
    tfSubtotal.setColumns(10);
    
    tfTax = new JTextField();
    tfTax.setHorizontalAlignment(11);
    tfTax.setColumns(10);
    
    tfDiscount = new JTextField();
    tfDiscount.setHorizontalAlignment(11);
    tfDiscount.setColumns(10);
    
    tfTotal = new JTextField();
    tfTotal.setHorizontalAlignment(11);
    tfTotal.setColumns(10);
    
    btnScrollUp = new PosButton();
    btnScrollDown = new PosButton();
    btnTransferToTicket1 = new PosButton();
    btnTransferToTicket1.setIcon(IconFactory.getIcon("next.png"));
    scrollPane = new JScrollPane();
    ticketViewerTable = new VoidTicketItemViewerTable();
    
    lblSubtotal.setHorizontalAlignment(4);
    ticketTotalPanel.add(lblSubtotal, "right");
    tfSubtotal.setEditable(false);
    ticketTotalPanel.add(tfSubtotal);
    
    lblTax.setHorizontalAlignment(4);
    ticketTotalPanel.add(lblTax, "right");
    tfTax.setEditable(false);
    ticketTotalPanel.add(tfTax);
    
    lblDiscount.setHorizontalAlignment(4);
    ticketTotalPanel.add(lblDiscount, "right");
    tfDiscount.setEditable(false);
    ticketTotalPanel.add(tfDiscount);
    
    lblTotal.setHorizontalAlignment(4);
    ticketTotalPanel.add(lblTotal, "right");
    tfTotal.setEditable(false);
    ticketTotalPanel.add(tfTotal, "wrap");
    
    btnScrollUp.setIcon(IconFactory.getIcon("/ui_icons/", "up.png"));
    btnScrollUp.setPreferredSize(PosUIManager.getSize(60, 50));
    btnScrollUp.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        VoidTicketItemView.this.doScrollUp(evt);
      }
      
    });
    rightButtonPanel.add(btnScrollUp, "grow");
    
    btnScrollDown.setIcon(IconFactory.getIcon("/ui_icons/", "down.png"));
    btnScrollDown.setPreferredSize(PosUIManager.getSize(60, 50));
    btnScrollDown.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        VoidTicketItemView.this.doScrollDown(evt);
      }
      
    });
    rightButtonPanel.add(btnScrollDown, "grow");
    
    btnTransferToTicket1.setPreferredSize(PosUIManager.getSize(60, 50));
    btnTransferToTicket1.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent evt) {
        VoidTicketItemView.this.btnTransferToTicket1ActionPerformed();
      }
      
    });
    rightButtonPanel.add(btnTransferToTicket1, "grow");
    
    scrollPane.setHorizontalScrollBarPolicy(31);
    scrollPane.setVerticalScrollBarPolicy(21);
    scrollPane.setViewportView(ticketViewerTable);
    
    ticketTableViewPanel.add(scrollPane, "Center");
    
    ticketTableViewPanel.add(rightButtonPanel, "East");
    ticketTableViewPanel.add(ticketTotalPanel, "South");
    add(ticketTableViewPanel, "Center");
    
    ticketViewerTable.setDefaultRenderer(Object.class, new PosTableRenderer());
    ticketViewerTable.getTableHeader().setPreferredSize(PosUIManager.getSize(0, 35));
    resizeTableColumns();
  }
  
  private void resizeTableColumns() {
    ticketViewerTable.setAutoResizeMode(4);
    setColumnWidth(1, PosUIManager.getSize(50));
    setColumnWidth(2, PosUIManager.getSize(50));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = ticketViewerTable.getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  private void btnTransferToTicket1ActionPerformed() {
    if ((voidItemSplitView != null) && (voidItemSplitView.isVisible())) {
      int selectedRow = ticketViewerTable.getSelectedRow();
      Object selectedItem = ticketViewerTable.get(selectedRow);
      
      if ((selectedItem != null) && ((selectedItem instanceof TicketItem))) {
        VoidItemFormDialog dialog = new VoidItemFormDialog((TicketItem)selectedItem);
        dialog.pack();
        dialog.open();
        
        if (dialog.isCanceled()) {
          return;
        }
        voidItem = dialog.getVoidItem();
        if (voidItem != null) {
          listener.voidTicketItemSelected((TicketItem)selectedItem, this, voidItemSplitView, voidItem);
        }
      }
    }
  }
  
  private void doScrollDown(ActionEvent evt) {
    ticketViewerTable.scrollDown();
  }
  
  private void doScrollUp(ActionEvent evt) {
    ticketViewerTable.scrollUp();
  }
  
  public void updateView() {
    if ((ticket == null) || (ticket.getTicketItems() == null) || (ticket.getTicketItems().size() <= 0)) {
      tfSubtotal.setText("");
      tfDiscount.setText("");
      tfTax.setText("");
      tfTotal.setText("");
      
      return;
    }
    
    ticket.calculatePrice();
    
    tfSubtotal.setText(NumberUtil.formatNumber(ticket.getSubtotalAmount()));
    tfDiscount.setText(NumberUtil.formatNumber(ticket.getDiscountAmount()));
    tfTax.setText(NumberUtil.formatNumber(ticket.getTaxAmount()));
    tfTotal.setText(NumberUtil.formatNumber(ticket.getTotalAmountWithTips()));
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void setTicket(Ticket _ticket) {
    ticket = _ticket;
    ticketViewerTable.setTicket(_ticket);
    
    updateView();
  }
  
  public void tableChanged(TableModelEvent e) {
    if ((ticket == null) || (ticket.getTicketItems() == null) || (ticket.getTicketItems().size() <= 0)) {
      tfSubtotal.setText("");
      tfDiscount.setText("");
      tfTax.setText("");
      tfTotal.setText("");
      
      return;
    }
    
    ticket.calculatePrice();
    
    tfSubtotal.setText(NumberUtil.formatNumber(ticket.getSubtotalAmount()));
    tfDiscount.setText(NumberUtil.formatNumber(ticket.getDiscountAmount()));
    tfTax.setText(NumberUtil.formatNumber(ticket.getTaxAmount()));
    tfTotal.setText(NumberUtil.formatNumber(ticket.getTotalAmountWithTips()));
  }
  
  public void setVoidItemSplitView(VoidItemSplitView voidItemSplitView) {
    this.voidItemSplitView = voidItemSplitView;
  }
  
  public VoidTicketItemViewerTable getTicketViewerTable() {
    return ticketViewerTable;
  }
}
