package com.floreantpos.ui.views.voidticket;

import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.VoidReason;
import com.floreantpos.model.dao.VoidReasonDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.POSComboBox;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.NotesDialog;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.NumberSelectionView;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;


public class VoidTicketItemDialog
  extends OkCancelOptionDialog
{
  private TicketItem ticketItem;
  private POSComboBox cbVoidItemReason;
  private POSToggleButton tbItemWasted;
  private VoidReason voidReason;
  private ComboBoxModel comboBoxModel;
  private NumberSelectionView numberSelectionView;
  private Ticket ticket;
  
  public VoidTicketItemDialog(Ticket ticket, TicketItem ticketItem)
  {
    super(POSUtil.getFocusedWindow(), "VOID ITEM");
    this.ticket = ticket;
    this.ticketItem = ticketItem;
    inItComponents();
    
    updateView();
  }
  
  public void inItComponents() {
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Void " + ticketItem.getName());
    add(titlePanel, "North");
    JPanel centerPanel = new JPanel(new MigLayout("fill", "[]", "[][][grow]"));
    numberSelectionView = new NumberSelectionView();
    numberSelectionView.setDecimalAllowed(ticketItem.isFractionalUnit().booleanValue());
    numberSelectionView.setBorder(null);
    numberSelectionView.setVisibleControlsButton(true);
    
    JLabel lblReason = new JLabel("Void Reason :");
    
    comboBoxModel = new ComboBoxModel();
    
    cbVoidItemReason = new POSComboBox();
    
    cbVoidItemReason.setModel(comboBoxModel);
    
    PosButton addReasonBtn = new PosButton("+");
    
    addReasonBtn.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        VoidTicketItemDialog.this.doAddNewVoidReason();
      }
      
    });
    tbItemWasted = new POSToggleButton("Item Wasted");
    centerPanel.add(lblReason, "wrap");
    centerPanel.add(cbVoidItemReason, "growx,split 3");
    centerPanel.add(addReasonBtn, "w 50!");
    centerPanel.add(tbItemWasted);
    

    add(centerPanel, "Center");
    centerPanel.add(numberSelectionView, "newline,span,grow,center,gaptop 5");
  }
  
  private void updateView() {
    comboBoxModel.setDataList(VoidReasonDAO.getInstance().findAll());
    if (cbVoidItemReason.getMaximumRowCount() > 5) {
      cbVoidItemReason.setMaximumRowCount(5);
    }
    numberSelectionView.setValue(Math.abs(ticketItem.getQuantity().doubleValue()));
    if (comboBoxModel.getSize() > 0) {
      cbVoidItemReason.setSelectedIndex(0);
    }
  }
  
  private boolean updateModel() {
    try {
      double quantity = Math.abs(numberSelectionView.getValue());
      String voidReasonText = null;
      voidReason = ((VoidReason)cbVoidItemReason.getSelectedItem());
      if (voidReason != null) {
        voidReasonText = voidReason.getReasonText();
      }
      boolean itemWasted = tbItemWasted.isSelected();
      
      ticket.voidItem(ticketItem, voidReasonText, itemWasted, quantity);
      ticketItem.calculatePrice();
      return true;
    } catch (PosException e) {
      POSMessageDialog.showError(this, e.getMessage());
      return false;
    } catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e); }
    return false;
  }
  

  public void doOk()
  {
    if (!updateModel()) {
      return;
    }
    setCanceled(false);
    dispose();
  }
  
  private void doAddNewVoidReason()
  {
    try {
      NotesDialog noteDialogue = new NotesDialog();
      noteDialogue.setTitle("Add Void Reason");
      noteDialogue.open();
      if (noteDialogue.isCanceled()) {
        return;
      }
      String note = noteDialogue.getNote();
      
      for (Iterator iterator = comboBoxModel.getDataList().iterator(); iterator.hasNext();) {
        VoidReason voidReason = (VoidReason)iterator.next();
        if (voidReason.getReasonText().equalsIgnoreCase(note)) {
          comboBoxModel.setSelectedItem(voidReason);
          return;
        }
      }
      

      this.voidReason = new VoidReason();
      this.voidReason.setReasonText(note);
      VoidReasonDAO.getInstance().saveOrUpdate(this.voidReason);
      comboBoxModel.addElement(this.voidReason);
      comboBoxModel.setSelectedItem(this.voidReason);
    }
    catch (Exception e) {
      POSMessageDialog.showError(this, "Error while creating void reason.", e);
    }
  }
}
