package com.floreantpos.ui.views.voidticket;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.VoidItem;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.ticket.VoidItemViewerTable;
import com.floreantpos.ui.views.order.actions.VoidItemSplitViewsListener;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;




public class VoidItemSplitView
  extends TransparentPanel
  implements TableModelListener
{
  private Ticket ticket;
  private VoidTicketItemView voidTicketItemView;
  private PosButton btnScrollDown;
  private PosButton btnScrollUp;
  private PosButton btnTransferToTicket1;
  private JScrollPane scrollPane;
  private VoidItemViewerTable voidItemViewerTable;
  protected int splitNumber;
  private VoidItemSplitViewsListener listener;
  private int noOfCustomSplit;
  private List<Integer> selectedTicketNumbers;
  
  public VoidItemSplitView(VoidItemSplitViewsListener listener)
  {
    this.listener = listener;
    initComponents();
    setOpaque(true);
    setTicket(ticket);
  }
  
  public List<VoidItem> getAllVoidItems() {
    return voidItemViewerTable.getVoidItems();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    setBorder(BorderFactory.createTitledBorder(null, Messages.getString("VoidItemSplitView.0"), 2, 0));
    setPreferredSize(PosUIManager.getSize(280, 463));
    
    TransparentPanel ticketTableViewPanel = new TransparentPanel(new BorderLayout());
    ticketTableViewPanel.setBorder(new EmptyBorder(0, 5, 0, 0));
    
    int rightGap = PosUIManager.getSize(60) + 10;
    TransparentPanel ticketTotalPanel = new TransparentPanel(new MigLayout("wrap 2,right,inset 5 5 5 " + rightGap + " "));
    TransparentPanel rightButtonPanel = new TransparentPanel(new MigLayout("wrap 1,fill,hidemode 3,inset 1"));
    
    btnScrollUp = new PosButton();
    btnScrollDown = new PosButton();
    btnTransferToTicket1 = new PosButton();
    btnTransferToTicket1.setIcon(IconFactory.getIcon("previous.png"));
    scrollPane = new JScrollPane();
    voidItemViewerTable = new VoidItemViewerTable();
    voidItemViewerTable.setDefaultRenderer(Object.class, new PosTableRenderer());
    
    btnScrollUp.setIcon(IconFactory.getIcon("/ui_icons/", "up.png"));
    btnScrollUp.setPreferredSize(PosUIManager.getSize(60, 50));
    btnScrollUp.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        VoidItemSplitView.this.doScrollUp(evt);
      }
      
    });
    rightButtonPanel.add(btnScrollUp, "grow");
    
    btnScrollDown.setIcon(IconFactory.getIcon("/ui_icons/", "down.png"));
    btnScrollDown.setPreferredSize(PosUIManager.getSize(60, 50));
    btnScrollDown.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        VoidItemSplitView.this.doScrollDown(evt);
      }
      
    });
    rightButtonPanel.add(btnScrollDown, "grow");
    
    btnTransferToTicket1.setPreferredSize(PosUIManager.getSize(60, 50));
    btnTransferToTicket1.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent evt) {
        VoidItemSplitView.this.btnTransferToTicket1ActionPerformed();

      }
      

    });
    scrollPane.setHorizontalScrollBarPolicy(31);
    scrollPane.setVerticalScrollBarPolicy(21);
    scrollPane.setViewportView(voidItemViewerTable);
    
    ticketTableViewPanel.add(scrollPane, "Center");
    
    add(rightButtonPanel, "East");
    add(ticketTotalPanel, "South");
    add(ticketTableViewPanel, "Center");
    
    voidItemViewerTable.getTableHeader().setPreferredSize(PosUIManager.getSize(0, 35));
    resizeTableColumns();
  }
  
  private void resizeTableColumns() {
    voidItemViewerTable.setAutoResizeMode(4);
    setColumnWidth(2, PosUIManager.getSize(100));
    setColumnWidth(3, PosUIManager.getSize(100));
    setColumnWidth(4, PosUIManager.getSize(100));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = voidItemViewerTable.getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  private void btnTransferToTicket1ActionPerformed() {
    if ((voidTicketItemView != null) && (voidTicketItemView.isVisible()) && 
      (voidItemViewerTable.getSelectedRow() != -1)) {
      int selectedRow = voidItemViewerTable.getSelectedRow();
      VoidItem voidItem = voidItemViewerTable.get(selectedRow);
      TicketItem ticketItemto = null;
      if (voidItem != null) {
        List<TicketItem> ticketItems = ticket.getDeletedItems();
        Iterator iterator; if (ticketItems != null) {
          for (iterator = ticketItems.iterator(); iterator.hasNext();) {
            TicketItem ticketItem = (TicketItem)iterator.next();
            
            if (ticketItem.getMenuItemId().equals(voidItem.getMenuItemId())) {
              ticketItemto = ticketItem;
            }
          }
        }
        if (ticketItemto != null) {
          listener.itemSelected(ticketItemto, this, voidTicketItemView, voidItem);
        }
      }
    }
  }
  
  public int getNoOfCustomSplit()
  {
    return noOfCustomSplit;
  }
  
  public List<Integer> getViewNumbers() {
    return selectedTicketNumbers;
  }
  
  private void doScrollDown(ActionEvent evt) {
    voidItemViewerTable.scrollDown();
  }
  
  private void doScrollUp(ActionEvent evt) {
    voidItemViewerTable.scrollUp();
  }
  
  public void updateModel() {
    ticket.calculatePrice();
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void setTicket(Ticket _ticket) {
    ticket = _ticket;
  }
  
  public void setVoidTicketItemView(VoidTicketItemView voidTicketItemView) {
    this.voidTicketItemView = voidTicketItemView;
  }
  
  public VoidItemViewerTable getVoidItemViewerTable() {
    return voidItemViewerTable;
  }
  
  public void tableChanged(TableModelEvent e) {}
}
