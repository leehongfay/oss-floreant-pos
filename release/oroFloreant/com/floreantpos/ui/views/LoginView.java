package com.floreantpos.ui.views;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.actions.ClockInOutAction;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.config.ui.DatabaseConfigurationDialog;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.extension.OrderServiceFactory;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Store;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.OrderTypeLoginButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.PasswordEntryDialog;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.ui.views.order.ViewPanel;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.ShiftException;
import com.floreantpos.util.UserNotFoundException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;































public class LoginView
  extends ViewPanel
{
  public static final String VIEW_NAME = "LOGIN_VIEW";
  private PosButton btnSwitchBoard;
  private PosButton btnKitchenDisplay;
  private PosButton btnDriverView;
  private PosButton btnConfigureDatabase;
  private PosButton btnBackOffice;
  private PosButton btnShutdown;
  private PosButton btnClockOUt;
  private JPanel centerPanel = new JPanel(new MigLayout("al center center", "sg", "100"));
  
  private static LoginView instance;
  private JPanel mainPanel;
  private JPanel panel1 = new JPanel(new MigLayout("fill, ins 0, hidemode 3", "sg, fill", ""));
  private JPanel panel2 = new JPanel(new MigLayout("fill, ins 0, hidemode 3", "sg, fill", ""));
  private int width;
  private int height;
  private JPanel panel3;
  private JPanel panel4;
  
  private LoginView()
  {
    setLayout(new BorderLayout(5, 5));
    setOpaque(false);
    
    width = PosUIManager.getSize(600);
    height = PosUIManager.getSize(100);
    
    centerPanel.setLayout(new MigLayout("al center center", "sg fill", String.valueOf(height)));
    centerPanel.setOpaque(false);
    
    JLabel titleLabel = new JLabel(IconFactory.getIcon("/icons/", "header_logo.png"));
    titleLabel.setOpaque(true);
    titleLabel.setBackground(Color.WHITE);
    
    JPanel panel = new JPanel(new BorderLayout());
    panel.setOpaque(false);
    panel.add(titleLabel, "Center");
    panel.add(new JSeparator(0), "South");
    
    add(panel, "North");
    add(createCenterPanel(), "Center");
  }
  
  protected void paintComponent(Graphics g)
  {
    super.paintComponent(g);
    Store store = Application.getInstance().getStore();
    if (store == null)
      return;
    ImageIcon icon = store.getLoginScreenBackground();
    if (icon == null) {
      return;
    }
    Image bgImage = icon.getImage();
    Dimension dimension = Application.getPosWindow().getSize();
    bgImage = POSUtil.getScaledImage(bgImage, width, height);
    g.drawImage(bgImage, 0, 0, width, height, null);
  }
  
  private JPanel createCenterPanel() {
    mainPanel = new JPanel(new BorderLayout());
    mainPanel.setOpaque(false);
    
    btnSwitchBoard = new PosButton(POSConstants.ORDERS);
    btnKitchenDisplay = new PosButton(POSConstants.KITCHEN_DISPLAY_BUTTON_TEXT);
    btnDriverView = new PosButton(Messages.getString("LoginView.7"));
    btnConfigureDatabase = new PosButton(POSConstants.CONFIGURE_DATABASE);
    btnBackOffice = new PosButton(POSConstants.BACK_OFFICE_BUTTON_TEXT);
    
    btnShutdown = new PosButton(POSConstants.SHUTDOWN);
    btnClockOUt = new PosButton(new ClockInOutAction(false, true));
    
    btnBackOffice.setVisible(false);
    btnSwitchBoard.setVisible(false);
    btnKitchenDisplay.setVisible(false);
    btnClockOUt.setVisible(false);
    
    panel3 = new JPanel(new GridLayout(1, 0, 5, 5));
    panel4 = new JPanel(new MigLayout("fill, ins 0, hidemode 3", "sg, fill", ""));
    
    centerPanel.add(panel1, "cell 0 0, wrap, w " + width + "px, h " + height + "px, grow");
    
    panel3.add(btnSwitchBoard);
    panel3.add(btnBackOffice);
    if ((TerminalConfig.isShowKDSOnLogInScreen()) && (TerminalConfig.isKDSenabled())) {
      panel3.add(btnKitchenDisplay);
    }
    OrderServiceExtension orderService = (OrderServiceExtension)ExtensionManager.getPlugin(OrderServiceExtension.class);
    if (orderService != null) {
      panel3.add(btnDriverView);
      btnDriverView.setVisible(false);
    }
    centerPanel.add(panel3, "cell 0 2, wrap, w " + width + "px, h " + height + "px, grow");
    
    panel4.add(btnClockOUt, "grow");
    panel4.add(btnConfigureDatabase, "grow");
    panel4.add(btnShutdown, "grow");
    
    centerPanel.add(panel4, "cell 0 3, wrap, w " + width + "px, h " + height + "px, grow");
    
    if (TerminalConfig.isKioskMode()) {
      if (btnConfigureDatabase != null) {
        btnConfigureDatabase.setVisible(false);

      }
      


    }
    else if (!TerminalConfig.isShowDbConfigureButton()) {
      btnConfigureDatabase.setVisible(false);
    }
    
    panel3.setOpaque(false);
    panel4.setOpaque(false);
    panel1.setOpaque(false);
    panel2.setOpaque(false);
    
    initActionHandlers();
    
    mainPanel.add(centerPanel, "Center");
    return mainPanel;
  }
  
  public void initializeOrderButtonPanel() {
    panel1.removeAll();
    panel2.removeAll();
    
    List<OrderType> orderTypes = Application.getInstance().getOrderTypes();
    int buttonCount = 0;
    
    for (OrderType orderType : orderTypes) {
      if ((orderType.isShowInLoginScreen().booleanValue()) && (orderType.isEnabled().booleanValue()))
      {

        if (buttonCount < 3) {
          panel1.add(new OrderTypeLoginButton(orderType), "grow");
        }
        else {
          panel2.add(new OrderTypeLoginButton(orderType), "grow");
        }
        buttonCount++;
      }
    }
    int row = 1;
    centerPanel.add(panel1, "cell 0 " + row++ + ", wrap, w " + width + "px, h " + height + "px, grow");
    if (buttonCount > 3) {
      centerPanel.add(panel2, "cell 0 " + row++ + ", wrap,w " + width + "px, h " + height + "px, grow");
    }
    btnSwitchBoard.setVisible(true);
    btnKitchenDisplay.setVisible(true);
    btnBackOffice.setVisible(true);
    btnClockOUt.setVisible(true);
    btnDriverView.setVisible(true);
    
    centerPanel.add(panel3, "cell 0 " + row++ + ", wrap, w " + width + "px, h " + height + "px, grow");
    centerPanel.add(panel4, "cell 0 " + row++ + ", wrap, w " + width + "px, h " + height + "px, grow");
    
    centerPanel.repaint();
  }
  
  public void updateView() {
    mainPanel.repaint();
  }
  
  void initActionHandlers() {
    btnConfigureDatabase.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        DatabaseConfigurationDialog.show(Application.getPosWindow());
      }
      
    });
    btnBackOffice.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        doBackofficeLogin();
      }
      
    });
    btnKitchenDisplay.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        TerminalConfig.setDefaultView("KD");
        doLogin();
      }
      
    });
    btnDriverView.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        IView view = OrderServiceFactory.getOrderService().getDriverView();
        if (view == null) {
          return;
        }
        RootView.getInstance().setAndShowHomeScreen(view);
      }
      
    });
    btnShutdown.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          if (Application.getInstance().isSystemInitialized()) {
            User user = PasswordEntryDialog.getUser(Application.getPosWindow(), "SHUTDOWN", Messages.getString("Application.1"));
            if (user == null) {
              return;
            }
            if (!user.hasPermission(UserPermission.SHUT_DOWN)) {
              POSMessageDialog.showMessage(Application.getPosWindow(), "You have no permission to shutdown");
              return;
            }
          }
        } catch (Exception ex) {
          PosLog.error(getClass(), ex);
        }
        Application.getInstance().shutdownPOS();
      }
      
    });
    btnSwitchBoard.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TerminalConfig.setDefaultView(SwitchboardView.VIEW_NAME);
        doLogin();
      }
    });
  }
  
  public synchronized void doLogin() {
    try {
      User user = PasswordEntryDialog.getUser(Application.getPosWindow(), Messages.getString("LoginView.1"), Messages.getString("LoginView.2"));
      if (user == null) {
        return;
      }
      Application application = Application.getInstance();
      application.doLogin(user);
    }
    catch (UserNotFoundException e) {
      LogFactory.getLog(Application.class).error(e);
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("LoginView.3"));
    } catch (ShiftException e) {
      LogFactory.getLog(Application.class).error(e);
      MessageDialog.showError(e.getMessage());
    } catch (Exception e1) {
      LogFactory.getLog(Application.class).error(e1);
      String message = e1.getMessage();
      
      if ((message != null) && (message.contains("Cannot open connection"))) {
        MessageDialog.showError(Messages.getString("LoginView.4"), e1);
        DatabaseConfigurationDialog.show(Application.getPosWindow());
      }
      else {
        MessageDialog.showError(Messages.getString("LoginView.5"), e1);
      }
    }
  }
  
  public synchronized void doBackofficeLogin() {
    User user = PasswordEntryDialog.getUser(Application.getPosWindow(), Messages.getString("LoginView.1"), Messages.getString("LoginView.2"));
    if (user == null) {
      return;
    }
    if (!user.hasPermission(UserPermission.VIEW_BACK_OFFICE)) {
      POSMessageDialog.showError(Messages.getString("PasswordEntryDialog.4"));
      return;
    }
    Application.getInstance().setCurrentUser(user);
    PosWindow posWindow = Application.getPosWindow();
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    if (window == null) {
      window = new BackOfficeWindow(user);
    }
    window.addWindowListener(new BackofficeWindowCloseListener(window));
    posWindow.setVisible(false);
    window.setVisible(true);
    window.toFront();
  }
  

  public void setTerminalId(int terminalId) {}
  
  public String getViewName()
  {
    return "LOGIN_VIEW";
  }
  
  public static LoginView getInstance() {
    if (instance == null) {
      instance = new LoginView();
    }
    
    return instance;
  }
  
  public JPanel getCenterPanel() {
    return centerPanel;
  }
  
  public JPanel getMainPanel() {
    return mainPanel;
  }
  
  class BackofficeWindowCloseListener extends WindowAdapter {
    private BackOfficeWindow backOfficeWindow;
    
    public BackofficeWindowCloseListener(BackOfficeWindow backOfficeWindow) {
      this.backOfficeWindow = backOfficeWindow;
    }
    
    public void windowClosing(WindowEvent e)
    {
      super.windowClosing(e);
      backOfficeWindow.removeWindowListener(this);
      
      PosWindow posWindow = Application.getPosWindow();
      posWindow.setVisible(true);
    }
  }
}
