package com.floreantpos.ui.views.payment;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.config.CardConfig;
import com.floreantpos.extension.PaymentGatewayPlugin;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.model.CardReader;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.dao.PosTransactionDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSDialog;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;













class AuthorizationDialog
  extends POSDialog
  implements Runnable
{
  private JLabel label;
  private JTextArea txtStatus;
  private PosButton btnFinish;
  private List<PosTransaction> transactions;
  
  public AuthorizationDialog(POSDialog parent, List<PosTransaction> transactions)
  {
    this.transactions = transactions;
    
    initComponents();
    setTitle(Messages.getString("PaymentProcessWaitDialog.0"));
    setIconImage(Application.getPosWindow().getIconImage());
    setLocationRelativeTo(parent);
  }
  
  public void setVisible(boolean b)
  {
    if (b) {
      Thread authorizationThread = new Thread(this);
      authorizationThread.start();
    }
    
    super.setVisible(b);
  }
  
  public void run()
  {
    for (Iterator iterator = transactions.iterator(); iterator.hasNext();) {
      PosTransaction transaction = (PosTransaction)iterator.next();
      try {
        String cardEntryType = transaction.getCardReader();
        CardReader cardReader = CardReader.fromString(cardEntryType);
        
        if (cardReader == CardReader.EXTERNAL_TERMINAL) {
          transaction.setCaptured(Boolean.valueOf(true));
          PosTransactionDAO.getInstance().saveOrUpdate(transaction);
          txtStatus.append(Messages.getString("AuthorizationDialog.1") + transaction.getId() + Messages.getString("AuthorizationDialog.2"));
        }
        else
        {
          CardProcessor cardProcessor = CardConfig.getPaymentGateway().getProcessor();
          cardProcessor.captureAuthAmount(transaction);
          transaction.setCaptured(Boolean.valueOf(true));
          
          PosTransactionDAO.getInstance().saveOrUpdate(transaction);
          txtStatus.append(Messages.getString("AuthorizationDialog.1") + transaction.getId() + Messages.getString("AuthorizationDialog.2"));
          
          if (iterator.hasNext()) {
            Thread.sleep(6000L);
          }
          
        }
      }
      catch (InterruptedException localInterruptedException) {}catch (Exception e)
      {
        txtStatus.append(Messages.getString("AuthorizationDialog.1") + transaction.getId() + Messages.getString("AuthorizationDialog.4") + e.getMessage() + "\n");
      }
    }
    
    label.setText(Messages.getString("AuthorizationDialog.0"));
    btnFinish.setVisible(true);
  }
  
  private void initComponents() {
    TransparentPanel transparentPanel1 = new TransparentPanel();
    transparentPanel1.setLayout(new BorderLayout());
    transparentPanel1.setOpaque(true);
    
    label = new JLabel(Messages.getString("PaymentProcessWaitDialog.1"));
    label.setHorizontalAlignment(0);
    label.setFont(label.getFont().deriveFont(24).deriveFont(1));
    
    JLabel labelGateway = new JLabel(Messages.getString("PaymentProcessWaitDialog.8") + CardConfig.getPaymentGateway().getProductName());
    labelGateway.setHorizontalAlignment(0);
    labelGateway.setFont(label.getFont().deriveFont(24).deriveFont(1));
    
    txtStatus = new JTextArea();
    txtStatus.setEditable(false);
    txtStatus.setLineWrap(true);
    
    transparentPanel1.add(labelGateway, "North");
    JScrollPane scrollPane = new JScrollPane(txtStatus);
    scrollPane.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(20, 30, 20, 30), scrollPane.getBorder()));
    transparentPanel1.add(scrollPane, "Center");
    
    TransparentPanel transparentPanel2 = new TransparentPanel();
    transparentPanel2.setLayout(new FlowLayout(1, 10, 5));
    
    btnFinish = new PosButton();
    btnFinish.setText(POSConstants.FINISH);
    btnFinish.setPreferredSize(new Dimension(140, 50));
    btnFinish.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        AuthorizationDialog.this.doFinish(evt);
      }
    });
    btnFinish.setVisible(false);
    
    transparentPanel2.add(btnFinish);
    
    transparentPanel1.add(transparentPanel2, "South");
    
    getContentPane().add(label, "North");
    getContentPane().add(transparentPanel1, "Center");
    
    setSize(500, 400);
    setDefaultCloseOperation(0);
    setResizable(false);
  }
  
  private void doFinish(ActionEvent evt) {
    dispose();
  }
}
