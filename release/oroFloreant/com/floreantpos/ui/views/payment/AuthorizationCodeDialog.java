package com.floreantpos.ui.views.payment;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import com.floreantpos.model.PaymentType;
import com.floreantpos.swing.FocusedTextField;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.QwertyKeyPad;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
















public class AuthorizationCodeDialog
  extends POSDialog
  implements CardInputProcessor
{
  private CardInputListener cardInputListener;
  private FocusedTextField tfAuthorizationCode;
  private POSToggleButton btnVisaCard;
  private POSToggleButton btnMasterCard;
  private POSToggleButton btnAmericanExpress;
  private POSToggleButton btnDiscoverCard;
  private POSToggleButton btnDebitVisaCard;
  private POSToggleButton btnDebitMasterCard;
  
  public AuthorizationCodeDialog(CardInputListener cardInputListener)
  {
    super(Application.getPosWindow(), true);
    
    this.cardInputListener = cardInputListener;
    
    setDefaultCloseOperation(2);
    setResizable(false);
    
    createUI();
    btnVisaCard.setSelected(true);
  }
  
  private void createUI()
  {
    setPreferredSize(new Dimension(PosUIManager.getSize(1000), PosUIManager.getSize(600)));
    btnVisaCard = new POSToggleButton();
    btnVisaCard.setIcon(IconFactory.getIcon("/ui_icons/", "visa_card.png"));
    
    btnMasterCard = new POSToggleButton("");
    btnMasterCard.setIcon(IconFactory.getIcon("/ui_icons/", "master_card.png"));
    
    btnAmericanExpress = new POSToggleButton();
    btnAmericanExpress.setIcon(IconFactory.getIcon("/ui_icons/", "am_ex_card.png"));
    
    btnDiscoverCard = new POSToggleButton();
    btnDiscoverCard.setIcon(IconFactory.getIcon("/ui_icons/", "discover_card.png"));
    
    ButtonGroup group = new ButtonGroup();
    group.add(btnVisaCard);
    group.add(btnMasterCard);
    group.add(btnAmericanExpress);
    group.add(btnDiscoverCard);
    
    JPanel creditCardPanel = new JPanel(new GridLayout(1, 0, 10, 10));
    
    creditCardPanel.add(btnVisaCard);
    creditCardPanel.add(btnMasterCard);
    creditCardPanel.add(btnAmericanExpress);
    creditCardPanel.add(btnDiscoverCard);
    
    creditCardPanel.setBorder(new CompoundBorder(new TitledBorder(Messages.getString("PaymentTypeSelectionDialog.4")), new EmptyBorder(10, 10, 10, 10)));
    
    JPanel debitCardPanel = new JPanel(new GridLayout(1, 0, 10, 10));
    
    btnDebitVisaCard = new POSToggleButton();
    btnDebitVisaCard.setIcon(IconFactory.getIcon("/ui_icons/", "visa_card.png"));
    
    btnDebitMasterCard = new POSToggleButton();
    btnDebitMasterCard.setIcon(IconFactory.getIcon("/ui_icons/", "master_card.png"));
    
    group.add(btnDebitVisaCard);
    group.add(btnDebitMasterCard);
    
    debitCardPanel.add(btnDebitVisaCard);
    debitCardPanel.add(btnDebitMasterCard);
    
    debitCardPanel.setBorder(new CompoundBorder(new TitledBorder(Messages.getString("PaymentTypeSelectionDialog.6")), new EmptyBorder(10, 10, 10, 10)));
    
    JPanel panel = new JPanel();
    
    JPanel centralPanel = new JPanel(new BorderLayout());
    centralPanel.add(panel, "Center");
    
    JPanel cardPanel = new JPanel(new MigLayout("fill, ins 2", "sg, fill", ""));
    cardPanel.add(creditCardPanel);
    cardPanel.add(debitCardPanel);
    centralPanel.add(cardPanel, "North");
    
    getContentPane().add(centralPanel, "Center");
    panel.setLayout(new MigLayout("", "[][grow]", "[50][grow]"));
    
    JLabel lblAuthorizationCode = new JLabel(Messages.getString("AuthorizationCodeDialog.3"));
    panel.add(lblAuthorizationCode, "cell 0 0,alignx trailing");
    
    tfAuthorizationCode = new FocusedTextField();
    tfAuthorizationCode.setColumns(12);
    panel.add(tfAuthorizationCode, "cell 1 0,growx");
    
    QwertyKeyPad qwertyKeyPad = new QwertyKeyPad();
    panel.add(qwertyKeyPad, "cell 0 1 2 1,grow");
    
    JPanel panel_1 = new JPanel();
    getContentPane().add(panel_1, "South");
    panel_1.setLayout(new BorderLayout(0, 0));
    
    JPanel panel_2 = new JPanel();
    panel_1.add(panel_2);
    
    PosButton btnSubmit = new PosButton();
    panel_2.add(btnSubmit);
    btnSubmit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setCanceled(false);
        dispose();
        cardInputListener.cardInputted(AuthorizationCodeDialog.this, AuthorizationCodeDialog.this.getPaymentType());
      }
    });
    btnSubmit.setText(Messages.getString("AuthorizationCodeDialog.7"));
    
    PosButton btnCancel = new PosButton();
    panel_2.add(btnCancel);
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
    });
    btnCancel.setText(Messages.getString("AuthorizationCodeDialog.8"));
    
    JSeparator separator = new JSeparator();
    panel_1.add(separator, "North");
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle(Messages.getString("AuthorizationCodeDialog.9"));
    getContentPane().add(titlePanel, "North");
  }
  
  private PaymentType getPaymentType() {
    if (btnVisaCard.isSelected()) {
      return PaymentType.CREDIT_VISA;
    }
    if (btnMasterCard.isSelected()) {
      return PaymentType.CREDIT_MASTER_CARD;
    }
    if (btnAmericanExpress.isSelected()) {
      return PaymentType.CREDIT_AMEX;
    }
    if (btnDiscoverCard.isSelected()) {
      return PaymentType.CREDIT_DISCOVERY;
    }
    if (btnDebitMasterCard.isSelected()) {
      return PaymentType.DEBIT_MASTER_CARD;
    }
    if (btnDebitVisaCard.isSelected()) {
      return PaymentType.DEBIT_VISA;
    }
    
    return PaymentType.CREDIT_VISA;
  }
  
  public String getAuthorizationCode()
  {
    return tfAuthorizationCode.getText();
  }
}
