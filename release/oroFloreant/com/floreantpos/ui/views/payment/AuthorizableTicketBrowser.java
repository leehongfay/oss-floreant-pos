package com.floreantpos.ui.views.payment;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.actions.ActionCommand;
import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.config.CardConfig;
import com.floreantpos.extension.PaymentGatewayPlugin;
import com.floreantpos.model.Gratuity;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.PosTransactionDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.TransactionListView;
import com.floreantpos.ui.dialog.NumberSelectionDialog2;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;

















public class AuthorizableTicketBrowser
  extends POSDialog
{
  private TransactionListView authClosedListView = new TransactionListView();
  private TransactionListView authWaitingListView = new TransactionListView();
  private JPanel tabbedPane;
  private POSToggleButton btnShowCaptureWaiting = new POSToggleButton(Messages.getString("AuthorizableTicketBrowser.12"));
  private POSToggleButton btnShowCaptured = new POSToggleButton(Messages.getString("AuthorizableTicketBrowser.13"));
  private User currentUser;
  private Terminal drawerTerminal;
  
  public AuthorizableTicketBrowser(JFrame parent, User currentUser, Terminal drawerTerminal)
  {
    this.currentUser = currentUser;
    this.drawerTerminal = drawerTerminal;
    init();
  }
  
  private void init() {
    setTitle(POSConstants.AUTHORIZE_BUTTON_TEXT);
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle(Messages.getString("TicketAuthorizationDialog.0"));
    add(titlePanel, "North");
    
    tabbedPane = new JPanel(new BorderLayout());
    
    JPanel toggleButtonActionPanel = new JPanel(new MigLayout("", "sg,fill", ""));
    
    ButtonGroup group = new ButtonGroup();
    group.add(btnShowCaptureWaiting);
    group.add(btnShowCaptured);
    
    btnShowCaptureWaiting.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tabbedPane.remove(authClosedListView);
        tabbedPane.add(authWaitingListView);
        tabbedPane.revalidate();
        tabbedPane.repaint();
      }
      
    });
    btnShowCaptured.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tabbedPane.remove(authWaitingListView);
        tabbedPane.add(authClosedListView);
        tabbedPane.revalidate();
        tabbedPane.repaint();
      }
    });
    toggleButtonActionPanel.add(btnShowCaptureWaiting);
    toggleButtonActionPanel.add(btnShowCaptured);
    
    btnShowCaptureWaiting.setSelected(true);
    
    tabbedPane.add(toggleButtonActionPanel, "North");
    tabbedPane.add(authWaitingListView);
    
    authWaitingListView.setBorder(BorderFactory.createEmptyBorder(5, 8, 5, 8));
    authClosedListView.setBorder(BorderFactory.createEmptyBorder(5, 8, 5, 8));
    
    JPanel buttonPanel = new JPanel(new MigLayout("al center", "sg, fill", ""));
    ActionHandler actionHandler = new ActionHandler();
    buttonPanel.add(new PosButton(ActionCommand.EDIT_TIPS, actionHandler), "grow");
    buttonPanel.add(new PosButton(ActionCommand.CAPTURE, actionHandler), "grow");
    buttonPanel.add(new PosButton(ActionCommand.CAPTURE_ALL, actionHandler), "grow");
    buttonPanel.add(new PosButton(ActionCommand.VOID_TRANS, actionHandler), "grow");
    buttonPanel.add(new PosButton(new CloseDialogAction(this)));
    
    tabbedPane.add(buttonPanel, "South");
    
    JPanel authClosedTab = new JPanel(new BorderLayout());
    JPanel buttonPanel2 = new JPanel(new MigLayout("al center", "sg, fill", ""));
    buttonPanel2.add(new PosButton(ActionCommand.VOID_TRANS, actionHandler), "grow");
    buttonPanel2.add(new PosButton(new CloseDialogAction(this)));
    authClosedTab.add(authClosedListView);
    authClosedTab.add(buttonPanel2, "South");
    
    add(tabbedPane);
    
    updateTransactiontList();
  }
  
  public void updateTransactiontList() {
    User owner = null;
    if ((!currentUser.isAdministrator()) && (!currentUser.isManager())) {
      owner = currentUser;
    }
    authWaitingListView.setTransactions(PosTransactionDAO.getInstance().findUnauthorizedTransactions(owner));
    authClosedListView.setTransactions(PosTransactionDAO.getInstance().findCapturedTransactions(owner));
  }
  
  private boolean confirmAuthorize(String message) {
    int option = JOptionPane.showConfirmDialog(this, message, Messages.getString("TicketAuthorizationDialog.1"), 2);
    
    if (option == 0) {
      return true;
    }
    
    return false;
  }
  
  private void doAuthorize() {
    try {
      List<PosTransaction> transactions = authWaitingListView.getSelectedTransactions();
      
      if ((transactions == null) || (transactions.size() == 0)) {
        POSMessageDialog.showMessage(this, Messages.getString("TicketAuthorizationDialog.2"));
        return;
      }
      
      if (!confirmAuthorize(Messages.getString("TicketAuthorizationDialog.3"))) {
        return;
      }
      
      AuthorizationDialog authorizingDialog = new AuthorizationDialog(this, transactions);
      authorizingDialog.setVisible(true);
      
      updateTransactiontList();
    } catch (Exception e) {
      POSMessageDialog.showError(this, "We are sorry, an unexpected error has occured.");
      PosLog.error(getClass(), e);
    }
  }
  
  public void doAuthorizeAll() {
    try {
      List<PosTransaction> transactions = authWaitingListView.getAllTransactions();
      
      if ((transactions == null) || (transactions.size() == 0)) {
        POSMessageDialog.showMessage(this, Messages.getString("TicketAuthorizationDialog.5"));
        return;
      }
      
      if (!confirmAuthorize(Messages.getString("TicketAuthorizationDialog.6"))) {
        return;
      }
      
      AuthorizationDialog authorizingDialog = new AuthorizationDialog(this, transactions);
      authorizingDialog.setVisible(true);
      
      updateTransactiontList();
    } catch (Exception e) {
      POSMessageDialog.showError(this, "We are sorry, an unexpected error has occured.");
      PosLog.error(getClass(), e);
    }
  }
  
  private void doEditTips() {
    try {
      PosTransaction transaction = authWaitingListView.getFirstSelectedTransaction();
      
      if (transaction == null) {
        return;
      }
      
      Ticket ticket = TicketDAO.getInstance().loadFullTicket(transaction.getTicket().getId());
      Set<PosTransaction> transactions = ticket.getTransactions();
      for (PosTransaction posTransaction : transactions) {
        if (transaction.getId().equals(posTransaction.getId())) {
          transaction = posTransaction;
          break;
        }
      }
      
      double oldTipsAmount = transaction.getTipsAmount().doubleValue();
      double newTipsAmount = NumberSelectionDialog2.show(this, Messages.getString("TicketAuthorizationDialog.8"), oldTipsAmount);
      

      if (Double.isNaN(newTipsAmount)) {
        return;
      }
      transaction.setTipsAmount(Double.valueOf(newTipsAmount));
      transaction.setAmount(Double.valueOf(transaction.getAmount().doubleValue() - oldTipsAmount + newTipsAmount));
      
      if (ticket.hasGratuity()) {
        double ticketTipsAmount = ticket.getGratuity().getAmount().doubleValue();
        double ticketPaidAmount = ticket.getPaidAmount().doubleValue();
        
        double newTicketTipsAmount = ticketTipsAmount - oldTipsAmount + newTipsAmount;
        double newTicketPaidAmount = ticketPaidAmount - oldTipsAmount + newTipsAmount;
        
        ticket.setGratuityAmount(newTicketTipsAmount);
        ticket.setPaidAmount(Double.valueOf(newTicketPaidAmount));
      }
      else {
        ticket.setGratuityAmount(newTipsAmount);
        ticket.setPaidAmount(Double.valueOf(ticket.getPaidAmount().doubleValue() + newTipsAmount));
      }
      
      ticket.calculatePrice();
      
      TicketDAO.getInstance().saveOrUpdate(ticket);
      updateTransactiontList();
    } catch (Exception e) {
      POSMessageDialog.showError(this, "We are sorry, an unexpected error has occured.");
      PosLog.error(getClass(), e);
    }
  }
  
  private void doVoidTransaction() {
    try {
      PosTransaction transaction = authWaitingListView.getSelectedTransaction();
      if (btnShowCaptured.isSelected()) {
        transaction = authClosedListView.getSelectedTransaction();
      }
      
      if (transaction == null) {
        POSMessageDialog.showMessage(this, "Please select transaction to void");
        return;
      }
      
      int option = POSMessageDialog.showYesNoQuestionDialog(this, Messages.getString("AuthorizableTicketBrowser.14"), 
        Messages.getString("AuthorizableTicketBrowser.15"));
      if (option != 0) {
        return;
      }
      CardProcessor cardProcessor = CardConfig.getPaymentGateway().getProcessor();
      cardProcessor.voidTransaction(transaction);
      PosTransactionDAO.getInstance().saveOrUpdate(transaction);
      POSMessageDialog.showMessage(this, "Transaction voided");
      updateTransactiontList();
    } catch (PosException e) {
      POSMessageDialog.showError(this, e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(this, "We are sorry, voiding transaction failed because of an unexpected error.");
      PosLog.error(getClass(), e);
    }
  }
  
  class ActionHandler implements ActionListener {
    ActionHandler() {}
    
    public void actionPerformed(ActionEvent e) {
      ActionCommand command = ActionCommand.valueOf(e.getActionCommand());
      try
      {
        switch (AuthorizableTicketBrowser.3.$SwitchMap$com$floreantpos$actions$ActionCommand[command.ordinal()])
        {
        case 1: 
          AuthorizableTicketBrowser.this.doEditTips();
          break;
        
        case 2: 
          AuthorizableTicketBrowser.this.doAuthorize();
          break;
        
        case 3: 
          doAuthorizeAll();
          break;
        
        case 4: 
          AuthorizableTicketBrowser.this.doVoidTransaction();
        
        }
        
      }
      catch (Exception e2)
      {
        POSMessageDialog.showError(AuthorizableTicketBrowser.this, e2.getMessage(), e2);
      }
    }
  }
}
