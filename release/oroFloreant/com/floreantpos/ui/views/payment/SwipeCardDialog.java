package com.floreantpos.ui.views.payment;

import com.floreantpos.Messages;
import com.floreantpos.config.CardConfig;
import com.floreantpos.main.Application;
import com.floreantpos.model.PaymentType;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;















public class SwipeCardDialog
  extends POSDialog
  implements CardInputProcessor
{
  private CardInputListener cardInputListener;
  private JPasswordField passwordField;
  private String cardString;
  private PosButton btnEnterAuthorizationCode;
  private PosButton btnManualEntry;
  
  public SwipeCardDialog(CardInputListener cardInputListener)
  {
    this.cardInputListener = cardInputListener;
    
    setDefaultCloseOperation(2);
    setResizable(false);
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle(Messages.getString("SwipeCardDialog.0"));
    getContentPane().add(titlePanel, "North");
    
    JPanel panel = new JPanel();
    getContentPane().add(panel, "South");
    panel.setLayout(new BorderLayout(0, 0));
    
    JPanel panel_2 = new JPanel(new MigLayout("", "grow", ""));
    panel.add(panel_2);
    
    btnManualEntry = new PosButton();
    panel_2.add(btnManualEntry);
    btnManualEntry.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        openManualEntry();
      }
    });
    btnManualEntry.setText(Messages.getString("SwipeCardDialog.1"));
    
    btnEnterAuthorizationCode = new PosButton();
    panel_2.add(btnEnterAuthorizationCode);
    btnEnterAuthorizationCode.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        openAuthorizationEntryDialog();
      }
    });
    btnEnterAuthorizationCode.setText(Messages.getString("SwipeCardDialog.2"));
    
    PosButton btnSubmit = new PosButton();
    panel_2.add(btnSubmit);
    btnSubmit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        SwipeCardDialog.this.submitCard();
      }
    });
    btnSubmit.setText(Messages.getString("SwipeCardDialog.3"));
    
    PosButton psbtnCancel = new PosButton();
    panel_2.add(psbtnCancel);
    psbtnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
    });
    psbtnCancel.setText(Messages.getString("SwipeCardDialog.4"));
    
    JSeparator separator = new JSeparator();
    panel.add(separator, "North");
    
    JPanel panel_1 = new JPanel();
    panel_1.setBorder(new EmptyBorder(20, 10, 20, 10));
    getContentPane().add(panel_1, "Center");
    panel_1.setLayout(new BorderLayout(0, 0));
    
    passwordField = new JPasswordField();
    passwordField.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        SwipeCardDialog.this.submitCard();
      }
    });
    passwordField.setColumns(30);
    panel_1.add(passwordField);
    
    if (Application.getInstance().isDevelopmentMode())
    {
      passwordField.setText("%B4111111111111111^SHAH/RIAR^1803101000000000020000831000000?;4111111111111111=1803101000020000831?");
    }
    
    if (!CardConfig.isManualEntrySupported()) {
      btnManualEntry.setEnabled(false);
    }
    if (!CardConfig.isExtTerminalSupported()) {
      btnEnterAuthorizationCode.setEnabled(false);
    }
  }
  
  protected void openAuthorizationEntryDialog() {
    setCanceled(true);
    dispose();
    
    AuthorizationCodeDialog dialog = new AuthorizationCodeDialog(cardInputListener);
    dialog.setLocationRelativeTo(this);
    dialog.pack();
    dialog.open();
  }
  
  protected void openManualEntry() {
    setCanceled(true);
    dispose();
    
    ManualCardEntryDialog dialog = new ManualCardEntryDialog(cardInputListener);
    dialog.setLocationRelativeTo(this);
    dialog.pack();
    dialog.open();
  }
  
  public String getCardString() {
    return cardString;
  }
  
  public void setCardString(String cardString) {
    this.cardString = cardString;
  }
  
  private void submitCard() {
    cardString = new String(passwordField.getPassword());
    setCanceled(false);
    dispose();
    cardInputListener.cardInputted(this, PaymentType.CREDIT_CARD);
  }
  
  public void setManualEntryVisible(boolean visible) {
    btnManualEntry.setVisible(visible);
  }
  
  public void setAuthorizationEntryVisible(boolean visible) {
    btnEnterAuthorizationCode.setVisible(visible);
  }
}
