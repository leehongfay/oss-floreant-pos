package com.floreantpos.ui.views.payment;

import com.floreantpos.Messages;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.config.CardConfig;
import com.floreantpos.model.PosTransaction;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.authorize.DeviceType;
import net.authorize.Environment;
import net.authorize.MarketType;
import net.authorize.Merchant;
import net.authorize.ResponseReasonCode;
import net.authorize.TransactionType;
import net.authorize.aim.Transaction;
import net.authorize.aim.cardpresent.Result;
import net.authorize.api.contract.v1.CreateTransactionRequest;
import net.authorize.api.contract.v1.CreateTransactionResponse;
import net.authorize.api.contract.v1.CreditCardType;
import net.authorize.api.contract.v1.MerchantAuthenticationType;
import net.authorize.api.contract.v1.MessageTypeEnum;
import net.authorize.api.contract.v1.MessagesType;
import net.authorize.api.contract.v1.MessagesType.Message;
import net.authorize.api.contract.v1.PaymentType;
import net.authorize.api.contract.v1.TransactionRequestType;
import net.authorize.api.contract.v1.TransactionResponse;
import net.authorize.api.contract.v1.TransactionResponse.Errors;
import net.authorize.api.contract.v1.TransactionResponse.Errors.Error;
import net.authorize.api.contract.v1.TransactionResponse.Messages;
import net.authorize.api.contract.v1.TransactionResponse.Messages.Message;
import net.authorize.api.contract.v1.TransactionTypeEnum;
import net.authorize.api.controller.CreateTransactionController;
import net.authorize.api.controller.base.ApiOperationBase;
import net.authorize.data.creditcard.CardType;
import net.authorize.data.creditcard.CreditCard;
import org.apache.commons.lang.StringUtils;
import us.fatehi.magnetictrack.bankcard.BankCardMagneticTrack;
import us.fatehi.magnetictrack.bankcard.Name;
import us.fatehi.magnetictrack.bankcard.Track1FormatB;



public class AuthorizeDotNetProcessor
  implements CardProcessor
{
  public AuthorizeDotNetProcessor() {}
  
  public void preAuth(PosTransaction transaction)
    throws Exception
  {
    Environment environment = createEnvironment();
    Merchant merchant = createMerchant(environment);
    
    CreditCard creditCard = createCard(transaction);
    

    Transaction authCaptureTransaction = merchant.createAIMTransaction(TransactionType.AUTH_ONLY, new BigDecimal(transaction.calculateAuthorizeAmount().doubleValue()));
    authCaptureTransaction.setCreditCard(creditCard);
    
    Result<Transaction> result = (Result)merchant.postTransaction(authCaptureTransaction);
    
    if (result.isApproved()) {
      transaction.setCardTransactionId(result.getTransId());
      transaction.setCardAuthCode(result.getAuthCode());
      
      populateCardInformation(transaction, result);
    } else {
      if (result.isDeclined()) {
        throw new Exception(Messages.getString("AuthorizeDotNetProcessor.2") + ((ResponseReasonCode)result.getResponseReasonCodes().get(0)).getReasonText());
      }
      
      throw new Exception(Messages.getString("AuthorizeDotNetProcessor.3") + ((ResponseReasonCode)result.getResponseReasonCodes().get(0)).getReasonText());
    }
  }
  
  public void chargeAmount(PosTransaction transaction) throws Exception
  {
    Environment environment = createEnvironment();
    Merchant merchant = createMerchant(environment);
    
    CreditCard creditCard = createCard(transaction);
    

    Transaction authCaptureTransaction = merchant.createAIMTransaction(TransactionType.AUTH_CAPTURE, new BigDecimal(transaction.getAmount().doubleValue()));
    authCaptureTransaction.setCreditCard(creditCard);
    
    Result<Transaction> result = (Result)merchant.postTransaction(authCaptureTransaction);
    if (result.isApproved()) {
      transaction.setCaptured(Boolean.valueOf(true));
      transaction.setAuthorizable(Boolean.valueOf(false));
      transaction.setCardTransactionId(result.getTransId());
      transaction.setCardAuthCode(result.getAuthCode());
      
      populateCardInformation(transaction, result);
    } else {
      if (result.isDeclined()) {
        throw new Exception(Messages.getString("AuthorizeDotNetProcessor.2") + ((ResponseReasonCode)result.getResponseReasonCodes().get(0)).getReasonText());
      }
      
      throw new Exception(Messages.getString("AuthorizeDotNetProcessor.3") + ((ResponseReasonCode)result.getResponseReasonCodes().get(0)).getReasonText());
    }
  }
  
  public void captureAuthAmount(PosTransaction transaction) throws Exception {
    Environment environment = createEnvironment();
    Merchant merchant = createMerchant(environment);
    

    Transaction authCaptureTransaction = merchant.createAIMTransaction(TransactionType.PRIOR_AUTH_CAPTURE, new BigDecimal(transaction.getAmount().doubleValue()));
    authCaptureTransaction.setTransactionId(transaction.getCardTransactionId());
    
    Result<Transaction> result = (Result)merchant.postTransaction(authCaptureTransaction);
    if (result.isApproved()) {
      transaction.setCardTransactionId(result.getTransId());
      transaction.setCardAuthCode(result.getAuthCode());
      
      populateCardInformation(transaction, result);
    } else {
      if (result.isDeclined()) {
        throw new Exception(Messages.getString("AuthorizeDotNetProcessor.6") + ((ResponseReasonCode)result.getResponseReasonCodes().get(0)).getReasonText());
      }
      
      throw new Exception(Messages.getString("AuthorizeDotNetProcessor.7") + ((ResponseReasonCode)result.getResponseReasonCodes().get(0)).getReasonText());
    }
  }
  
  public void captureNewAmount(PosTransaction transaction) throws Exception {
    Environment environment = createEnvironment();
    Merchant merchant = createMerchant(environment);
    
    CreditCard creditCard = createCard(transaction);
    

    Transaction authCaptureTransaction = merchant.createAIMTransaction(TransactionType.AUTH_CAPTURE, new BigDecimal(transaction.getAmount().doubleValue()));
    authCaptureTransaction.setCreditCard(creditCard);
    
    Result<Transaction> result = (Result)merchant.postTransaction(authCaptureTransaction);
    if (result.isApproved()) {
      transaction.setCardTransactionId(result.getTransId());
      transaction.setCardAuthCode(result.getAuthCode());
      
      populateCardInformation(transaction, result);
    } else {
      if (result.isDeclined()) {
        throw new Exception(Messages.getString("AuthorizeDotNetProcessor.8") + ((ResponseReasonCode)result.getResponseReasonCodes().get(0)).getReasonText());
      }
      
      throw new Exception(Messages.getString("AuthorizeDotNetProcessor.9") + ((ResponseReasonCode)result.getResponseReasonCodes().get(0)).getReasonText());
    }
  }
  
  public void voidAmount(PosTransaction transaction) throws Exception {
    Environment environment = createEnvironment();
    Merchant merchant = createMerchant(environment);
    

    Transaction authCaptureTransaction = merchant.createAIMTransaction(TransactionType.VOID, new BigDecimal(transaction.getAmount().doubleValue()));
    authCaptureTransaction.setTransactionId(transaction.getCardTransactionId());
    
    Result<Transaction> result = (Result)merchant.postTransaction(authCaptureTransaction);
    if (result.isApproved()) {
      transaction.setCardTransactionId(result.getTransId());
      transaction.setCardAuthCode(result.getAuthCode());
      populateCardInformation(transaction, result);
    } else {
      if (result.isDeclined()) {
        throw new Exception(Messages.getString("AuthorizeDotNetProcessor.10") + ((ResponseReasonCode)result.getResponseReasonCodes().get(0)).getReasonText());
      }
      
      throw new Exception(Messages.getString("AuthorizeDotNetProcessor.11") + ((ResponseReasonCode)result.getResponseReasonCodes().get(0)).getReasonText());
    }
  }
  
  private CreditCard createCard(PosTransaction transaction) {
    CreditCard creditCard = CreditCard.createCreditCard();
    String cardType = transaction.getCardType();
    creditCard.setCardType(CardType.findByValue(cardType));
    
    if (StringUtils.isNotEmpty(transaction.getCardTrack())) {
      return createCard(transaction.getCardTrack(), cardType);
    }
    

    String cardNumber = transaction.getCardNumber();
    String cardExpMonth = transaction.getCardExpMonth();
    String cardExpYear = transaction.getCardExpYear();
    
    validateCreditCardInfo(cardNumber, cardExpMonth, cardExpYear);
    
    return createCard(cardNumber, cardExpMonth, cardExpYear, cardType);
  }
  
  private void validateCreditCardInfo(String cardNumber, String cardExpMonth, String cardExpYear)
  {
    List<String> inputList = Arrays.asList(new String[] { cardNumber, cardExpMonth, cardExpYear });
    for (String input : inputList) {
      if (StringUtils.isEmpty(input)) {
        throw new PosException("Invalid credit card information.");
      }
    }
  }
  
  private CreditCard createCard(String cardTrack, String cardType) {
    CreditCard creditCard = CreditCard.createCreditCard();
    creditCard.setCardType(CardType.findByValue(cardType));
    

    String[] tracks = cardTrack.split(";");
    
    creditCard.setTrack1(tracks[0]);
    if (tracks.length > 1) {
      creditCard.setTrack2(";" + tracks[1]);
    }
    
    return creditCard;
  }
  
  private CreditCard createCard(String cardNumber, String expMonth, String expYear, String cardType) {
    CreditCard creditCard = CreditCard.createCreditCard();
    creditCard.setCardType(CardType.findByValue(cardType));
    
    creditCard.setExpirationYear(expYear);
    creditCard.setExpirationMonth(expMonth);
    creditCard.setCreditCardNumber(cardNumber);
    
    return creditCard;
  }
  
  private Merchant createMerchant(Environment environment) throws Exception {
    String apiLoginID = CardConfig.getMerchantAccount();
    String transactionKey = CardConfig.getMerchantPass();
    

    Merchant merchant = Merchant.createMerchant(environment, apiLoginID, transactionKey);
    merchant.setDeviceType(DeviceType.VIRTUAL_TERMINAL);
    merchant.setMarketType(MarketType.RETAIL);
    


    ApiOperationBase.setEnvironment(environment);
    MerchantAuthenticationType merchantAuthenticationType = new MerchantAuthenticationType();
    merchantAuthenticationType.setName(apiLoginID);
    merchantAuthenticationType.setTransactionKey(transactionKey);
    ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);
    
    return merchant;
  }
  
  private Environment createEnvironment() {
    Environment environment = Environment.PRODUCTION;
    if (CardConfig.isSandboxMode()) {
      environment = Environment.SANDBOX;
    }
    return environment;
  }
  





















































  public static void main(String[] args)
    throws Exception
  {
    BankCardMagneticTrack track = BankCardMagneticTrack.from("%B4111111111111111^SHAH/RIAR^1803101000000000020000831000000?;4111111111111111=1803101000020000831?");
    PosLog.info(AuthorizeDotNetProcessor.class, "" + track.getTrack1());
  }
  
  public void voidTransaction(PosTransaction transaction)
    throws Exception
  {
    Environment environment = createEnvironment();
    Merchant merchant = createMerchant(environment);
    

    Transaction voidTransaction = merchant.createAIMTransaction(TransactionType.VOID, new BigDecimal(transaction.getAmount().doubleValue()));
    voidTransaction.setTransactionId(transaction.getCardTransactionId());
    
    Result<Transaction> result = (Result)merchant.postTransaction(voidTransaction);
    if (result.isApproved()) {
      transaction.setCardTransactionId(result.getTransId());
      transaction.setCardAuthCode(result.getAuthCode());
      transaction.setVoided(Boolean.valueOf(true));
      populateCardInformation(transaction, result);
    } else {
      if (result.isDeclined()) {
        throw new Exception(Messages.getString("AuthorizeDotNetProcessor.10") + ((ResponseReasonCode)result.getResponseReasonCodes().get(0)).getReasonText());
      }
      
      throw new Exception(Messages.getString("AuthorizeDotNetProcessor.11") + ((ResponseReasonCode)result.getResponseReasonCodes().get(0)).getReasonText());
    }
  }
  
  public String getCardInformationForReceipt(PosTransaction transaction)
  {
    return null;
  }
  
  private void populateCardInformation(PosTransaction transaction, Result<Transaction> result) {
    if (result.getTarget() != null) {
      CreditCard card = ((Transaction)result.getTarget()).getCreditCard();
      if (card != null) {
        transaction.setCardType(card.getCardType().name());
        transaction.setCardNumber(card.getCreditCardNumber());
        
        if (StringUtils.isNotEmpty(card.getTrack1())) {
          String rawTrackData = "%" + card.getTrack1() + "?;" + card.getTrack2() + "?";
          BankCardMagneticTrack track = BankCardMagneticTrack.from(rawTrackData);
          transaction.setCardHolderName(track.getTrack1().getName().toString());
          transaction.setCardExpYear(card.getExpirationYear());
          transaction.setCardExpMonth(card.getExpirationMonth());
        }
      }
    }
  }
  

  public void cancelTransaction() {}
  
  public void refundTransaction(PosTransaction transaction, double refundAmount)
    throws Exception
  {
    createMerchant(createEnvironment());
    
    PaymentType paymentType = new PaymentType();
    CreditCardType creditCard = new CreditCardType();
    creditCard.setCardNumber(transaction.getCardNumber().replaceAll("X", ""));
    creditCard.setExpirationDate("XXXX");
    paymentType.setCreditCard(creditCard);
    
    TransactionRequestType txnRequest = new TransactionRequestType();
    txnRequest.setTransactionType(TransactionTypeEnum.REFUND_TRANSACTION.value());
    txnRequest.setRefTransId(transaction.getCardTransactionId());
    txnRequest.setAmount(new BigDecimal(String.valueOf(refundAmount)));
    txnRequest.setPayment(paymentType);
    

    CreateTransactionRequest apiRequest = new CreateTransactionRequest();
    apiRequest.setTransactionRequest(txnRequest);
    CreateTransactionController controller = new CreateTransactionController(apiRequest);
    controller.execute();
    
    CreateTransactionResponse response = (CreateTransactionResponse)controller.getApiResponse();
    
    if (response != null)
    {
      String errMsg = "";
      if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {
        TransactionResponse result = response.getTransactionResponse();
        if (result.getMessages() != null) {
          transaction.setRefunded(true);
          PosLog.info(getClass(), "Successfully created transaction with Transaction ID: " + result.getTransId());
          PosLog.info(getClass(), "Response Code: " + result.getResponseCode());
          PosLog.info(getClass(), "Message Code: " + ((TransactionResponse.Messages.Message)result.getMessages().getMessage().get(0)).getCode());
          PosLog.info(getClass(), "Description: " + ((TransactionResponse.Messages.Message)result.getMessages().getMessage().get(0)).getDescription());
          PosLog.info(getClass(), "Auth Code: " + result.getAuthCode());
        }
        else {
          PosLog.info(getClass(), "Failed Transaction.");
          if (response.getTransactionResponse().getErrors() != null) {
            PosLog.info(getClass(), "Error Code: " + ((TransactionResponse.Errors.Error)response.getTransactionResponse().getErrors().getError().get(0)).getErrorCode());
            errMsg = ((TransactionResponse.Errors.Error)response.getTransactionResponse().getErrors().getError().get(0)).getErrorText();
            PosLog.info(getClass(), "Error message: " + errMsg);
            throw new Exception(Messages.getString("AuthorizeDotNetProcessor.11") + errMsg);
          }
        }
      }
      else {
        PosLog.info(getClass(), "Failed Transaction.");
        if ((response.getTransactionResponse() != null) && (response.getTransactionResponse().getErrors() != null)) {
          errMsg = ((TransactionResponse.Errors.Error)response.getTransactionResponse().getErrors().getError().get(0)).getErrorText();
          PosLog.info(getClass(), "Error Code: " + ((TransactionResponse.Errors.Error)response.getTransactionResponse().getErrors().getError().get(0)).getErrorCode());
          PosLog.info(getClass(), "Error message: " + errMsg);
          throw new Exception(Messages.getString("AuthorizeDotNetProcessor.11") + errMsg);
        }
        
        errMsg = ((MessagesType.Message)response.getMessages().getMessage().get(0)).getText();
        PosLog.info(getClass(), "Error Code: " + ((MessagesType.Message)response.getMessages().getMessage().get(0)).getCode());
        PosLog.info(getClass(), "Error message: " + errMsg);
        throw new Exception(Messages.getString("AuthorizeDotNetProcessor.11") + errMsg);
      }
    }
    else
    {
      throw new Exception(Messages.getString("AuthorizeDotNetProcessor.11") + "No Response");
    }
  }
}
