package com.floreantpos.ui.views.payment;

import com.floreantpos.Messages;
import java.awt.Font;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;


















public class PaymentProcessWaitDialog
  extends JDialog
{
  public PaymentProcessWaitDialog(JDialog parent)
  {
    super(parent, false);
    setTitle(Messages.getString("PaymentProcessWaitDialog.0"));
    
    JLabel label = new JLabel(Messages.getString("PaymentProcessWaitDialog.1"));
    label.setHorizontalAlignment(0);
    label.setFont(label.getFont().deriveFont(24).deriveFont(1));
    add(label);
    
    setSize(500, 400);
    setDefaultCloseOperation(0);
    setLocationRelativeTo(parent);
  }
  
  public PaymentProcessWaitDialog(JFrame parent) {
    super(parent, false);
    setTitle(Messages.getString("PaymentProcessWaitDialog.2"));
    
    JLabel label = new JLabel(Messages.getString("PaymentProcessWaitDialog.3"));
    label.setHorizontalAlignment(0);
    label.setFont(label.getFont().deriveFont(24).deriveFont(1));
    add(label);
    
    setSize(500, 400);
    setDefaultCloseOperation(0);
    setLocationRelativeTo(parent);
  }
}
