package com.floreantpos.ui.views.payment;

import com.floreantpos.Messages;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.NumericKeypad;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.util.POSUtil;
import java.awt.Font;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;



















public class GratuityInputDialog
  extends OkCancelOptionDialog
{
  private DoubleTextField doubleTextField;
  
  public GratuityInputDialog()
  {
    super(POSUtil.getFocusedWindow());
    setCaption(Messages.getString("GratuityInputDialog.0"));
    
    JPanel panel = new JPanel();
    panel.setLayout(new MigLayout("inset 0", "[grow,fill]", "[grow,fill]"));
    
    doubleTextField = new DoubleTextField();
    doubleTextField.setHorizontalAlignment(11);
    doubleTextField.setFocusCycleRoot(true);
    doubleTextField.setFont(doubleTextField.getFont().deriveFont(1, PosUIManager.getNumberFieldFontSize()));
    
    panel.add(doubleTextField, "cell 0 0,alignx left,height 40px,aligny top");
    
    NumericKeypad numericKeypad = new NumericKeypad();
    panel.add(numericKeypad, "cell 0 1");
    
    getContentPanel().add(panel);
  }
  
  public void doOk() {
    setCanceled(false);
    dispose();
  }
  
  public double getGratuityAmount() {
    return doubleTextField.getDouble();
  }
}
