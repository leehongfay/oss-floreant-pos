package com.floreantpos.ui.views.payment;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.actions.PosAction;
import com.floreantpos.main.Application;
import com.floreantpos.model.Gratuity;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.RefundTransaction;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketDiscount;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.ButtonColumn;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.NumberSelectionDialog2;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.ticket.TicketViewerTable;
import com.floreantpos.ui.ticket.TicketViewerTableChangeListener;
import com.floreantpos.ui.ticket.TicketViewerTableModel;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import com.jidesoft.swing.TitledSeparator;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;













public class SettleTicketDialog
  extends POSDialog
  implements PaymentListener, TicketViewerTableChangeListener
{
  private PaymentView paymentView;
  private TicketViewerTable ticketViewerTable;
  private JScrollPane ticketScrollPane;
  private Ticket ticket;
  private JTextField tfSubtotal;
  private JTextField tfDiscount;
  private JTextField tfRefundAmount = new JTextField();
  private JTextField tfPaidAmount = new JTextField();
  
  private JTextField tfDeliveryCharge;
  
  private JTextField tfTax;
  
  private JTextField tfServiceCharge;
  private JTextField tfTotal;
  private JTextField tfGratuity;
  private JPanel bottomPanel;
  private JPanel discountInfoPanel;
  private JScrollPane scrollPane;
  private JPanel centerPanel;
  private SettleTicketProcessor ticketProcessor = null;
  private JTable discountTable;
  private TicketDiscountTableModel discountTableModel;
  private JPanel transactionListPanel;
  private JTable transactionsTable;
  private TransactionDataTableModel transactionTableModel;
  private JLabel lblRefundAmount;
  private JLabel labelTicketNumber;
  
  public SettleTicketDialog(Ticket ticket, User currentUser)
  {
    this.ticket = ticket;
    
    ticketProcessor = new SettleTicketProcessor(currentUser);
    if (ticket.getOrderType().isConsolidateItemsInReceipt().booleanValue()) {
      ticket.consolidateTicketItems();
    }
    
    setTitle(Messages.getString("SettleTicketDialog.6"));
    getContentPane().setLayout(new BorderLayout());
    
    centerPanel = new JPanel(new BorderLayout(5, 5));
    centerPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 3, 5));
    
    ticketViewerTable = new TicketViewerTable(ticket);
    ticketViewerTable.getModel().addTicketDataChangeListener(this);
    ticketViewerTable.setVisibleDeleteButton(0);
    ticketViewerTable.getModel().setEditable(true);
    ticketScrollPane = new PosScrollPane(ticketViewerTable);
    TitledSeparator ticketInfoTitled = new TitledSeparator(createTicketInfoPanel(), 0);
    ticketScrollPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(""), BorderFactory.createEmptyBorder(0, 0, 0, 0)));
    ticketViewerTable.getTableHeader().setPreferredSize(PosUIManager.getSize(0, 35));
    
    bottomPanel = new JPanel(new BorderLayout());
    
    JPanel summaryPanel = new JPanel(new BorderLayout());
    summaryPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    summaryPanel.add(createTotalViewerPanel(), "East");
    
    bottomPanel.add(summaryPanel, "Center");
    
    discountInfoPanel = new JPanel(new MigLayout("fill"));
    
    scrollPane = new JScrollPane(discountInfoPanel);
    scrollPane.setPreferredSize(PosUIManager.getSize(0, 70));
    scrollPane.setVisible(false);
    bottomPanel.add(scrollPane, "North");
    
    centerPanel.add(ticketInfoTitled, "North");
    centerPanel.add(ticketScrollPane, "Center");
    centerPanel.add(bottomPanel, "South");
    
    paymentView = new PaymentView(ticketProcessor);
    


    getContentPane().add(centerPanel, "Center");
    getContentPane().add(paymentView, "East");
    createTicketDiscountPanel();
    createTransactionListPanel();
    summaryPanel.add(transactionListPanel, "Center");
    
    ticketProcessor.addPaymentListener(this);
    
    updateView();
    paymentView.setTicket(ticket);
    ticketProcessor.setTicket(ticket);
    paymentView.setDefaultFocus();
    paymentView.updateView();
  }
  
  public void updateView() {
    if (ticket == null) {
      tfSubtotal.setText("");
      tfDiscount.setText("");
      tfDeliveryCharge.setText("");
      tfTax.setText("");
      tfServiceCharge.setText("");
      tfRefundAmount.setText("");
      tfPaidAmount.setText("");
      tfTotal.setText("");
      tfGratuity.setText("");
      return;
    }
    updateTicketTokenText();
    tfSubtotal.setText(NumberUtil.formatNumber(ticket.getSubtotalAmount(), true));
    tfDiscount.setText(NumberUtil.formatNumber(ticket.getDiscountAmount()));
    tfDeliveryCharge.setText(NumberUtil.formatNumber(ticket.getDeliveryCharge()));
    tfServiceCharge.setText(NumberUtil.formatNumber(ticket.getServiceCharge()));
    if (Application.getInstance().isPriceIncludesTax()) {
      tfTax.setText(Messages.getString("TicketView.35"));
    }
    else {
      tfTax.setText(NumberUtil.formatNumber(ticket.getTaxAmount(), true));
    }
    if (ticket.getGratuity() != null) {
      tfGratuity.setText(NumberUtil.formatNumber(ticket.getGratuity().getAmount()));
    }
    else {
      tfGratuity.setText("0.00");
    }
    Double refundAmount = ticket.getRefundAmount();
    tfRefundAmount.setText(NumberUtil.formatNumber(refundAmount));
    tfPaidAmount.setText(NumberUtil.formatNumber(ticket.getPaidAmount()));
    tfTotal.setText(NumberUtil.formatNumber(ticket.getTotalAmount(), true));
    lblRefundAmount.setVisible(refundAmount.doubleValue() > 0.0D);
    tfRefundAmount.setVisible(refundAmount.doubleValue() > 0.0D);
    rendererTicketDiscounts();
    rendererTransactions();
  }
  
  private JPanel createTicketInfoPanel() {
    JLabel lblTable = new JLabel();
    Font bigBoldFont = lblTable.getFont().deriveFont(1);
    
    labelTicketNumber = new JLabel();
    Font bigFont = labelTicketNumber.getFont().deriveFont(1);
    labelTicketNumber.setFont(bigFont);
    
    lblTable.setFont(bigBoldFont);
    lblTable.setText(" " + Messages.getString("SettleTicketDialog.3"));
    
    JLabel labelTableNumber = new JLabel();
    labelTableNumber.setFont(bigFont);
    if (!getTerminal().isShowTableNumber()) {
      labelTableNumber.setText(" " + ticket.getTableNames());
    }
    else {
      labelTableNumber.setText(" " + getTableNumbers(ticket.getTableNumbers()));
      
      if ((ticket.getTableNumbers() == null) || (ticket.getTableNumbers().isEmpty())) {
        labelTableNumber.setVisible(false);
        lblTable.setVisible(false);
      }
    }
    
    JLabel lblCustomer = new JLabel();
    lblCustomer.setFont(bigBoldFont);
    lblCustomer.setText(", " + Messages.getString("SettleTicketDialog.10") + ": ");
    
    JLabel labelCustomer = new JLabel();
    labelCustomer.setFont(bigFont);
    labelCustomer.setText(ticket.getProperty("CUSTOMER_NAME"));
    
    if (ticket.getProperty("CUSTOMER_NAME") == null) {
      labelCustomer.setVisible(false);
      lblCustomer.setVisible(false);
    }
    
    JPanel ticketInfoPanel = new TransparentPanel(new MigLayout("hidemode 3,ins 0", "[]0[]0[]0[]0[]0[]", "[]"));
    
    ticketInfoPanel.add(labelTicketNumber);
    ticketInfoPanel.add(lblTable);
    ticketInfoPanel.add(labelTableNumber);
    ticketInfoPanel.add(lblCustomer);
    ticketInfoPanel.add(labelCustomer);
    
    Color color = UIManager.getColor("TitledBorder.titleColor");
    labelTicketNumber.setForeground(color);
    lblTable.setForeground(color);
    labelTableNumber.setForeground(color);
    lblCustomer.setForeground(color);
    labelCustomer.setForeground(color);
    
    return ticketInfoPanel;
  }
  
  private void updateTicketTokenText() {
    if (ticket.getTokenNo().intValue() > 0) {
      labelTicketNumber.setText(String.valueOf(Messages.getString("SettleTicketDialog.0") + "# " + ticket.getTokenNo()));
    }
    else {
      labelTicketNumber.setText("[New Ticket]");
    }
  }
  
  public void createTransactionListPanel() {
    transactionListPanel = new JPanel(new BorderLayout());
    
    transactionsTable = new JTable();
    transactionsTable.setGridColor(Color.LIGHT_GRAY);
    transactionsTable.setCellSelectionEnabled(false);
    transactionsTable.setColumnSelectionAllowed(false);
    transactionsTable.setRowSelectionAllowed(false);
    
    transactionsTable.setAutoscrolls(true);
    transactionsTable.setRowHeight(PosUIManager.getSize(40));
    transactionsTable.setShowVerticalLines(false);
    transactionsTable.setIntercellSpacing(new Dimension(0, 2));
    transactionsTable.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
    transactionsTable.setFocusable(false);
    transactionsTable.getTableHeader().setPreferredSize(new Dimension(0, 0));
    transactionsTable.getTableHeader().setVisible(false);
    transactionsTable.setAutoResizeMode(4);
    transactionTableModel = new TransactionDataTableModel();
    transactionsTable.setModel(transactionTableModel);
    transactionsTable.getColumnModel().getColumn(2).setCellRenderer(new DefaultTableCellRenderer()
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component rendererComponent = super.getTableCellRendererComponent(table, value, isSelected, false, row, column);
        setHorizontalAlignment(4);
        if (isSelected) {
          return rendererComponent;
        }
        rendererComponent.setBackground(table.getBackground());
        return rendererComponent;
      }
      
      protected void setValue(Object value)
      {
        if (value == null) {
          setText("");
          return;
        }
        String text = value.toString();
        if (((value instanceof Double)) || ((value instanceof Float))) {
          text = NumberUtil.formatNumberAcceptNegative(Double.valueOf(((Number)value).doubleValue()));
        }
        setText(text);
      }
    });
    setColumnWidth(1, PosUIManager.getSize(50), transactionsTable);
    setColumnWidth(2, PosUIManager.getSize(70), transactionsTable);
    setColumnWidth(3, PosUIManager.getSize(50), transactionsTable);
    
    PosAction captureAction = new PosAction() {
      int selectedRow = -1;
      
      public void actionPerformed(ActionEvent e)
      {
        selectedRow = Integer.parseInt(e.getActionCommand());
        super.actionPerformed(e);
      }
      
      public void execute()
      {
        if (selectedRow == -1)
          return;
        PosTransaction item = (PosTransaction)transactionTableModel.getRowData(selectedRow);
        if (item == null)
          return;
        SettleTicketDialog.this.doAuthorize(item);
      }
    };
    captureAction.setRequiredPermission(UserPermission.AUTHORIZE_TICKETS);
    PosAction deleteAction = new PosAction() {
      int selectedRow = -1;
      
      public void actionPerformed(ActionEvent e)
      {
        selectedRow = Integer.parseInt(e.getActionCommand());
        super.actionPerformed(e);
      }
      
      public void execute()
      {
        if (selectedRow == -1)
          return;
        PosTransaction transaction = (PosTransaction)transactionTableModel.getRowData(selectedRow);
        if (transaction == null)
          return;
        SettleTicketDialog.this.doReversePayment(transaction, false);
      }
    };
    deleteAction.setRequiredPermission(UserPermission.VOID_PAYMENTS);
    ButtonColumn btnDelete = new ButtonColumn(transactionsTable, deleteAction, 3)
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        PosButton button = (PosButton)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        button.setText("X");
        button.setHorizontalAlignment(0);
        table.setRowHeight(row, table.getRowHeight(0));
        return button;
      }
      
      public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
      {
        return super.getTableCellEditorComponent(table, value, false, row, column);
      }
    };
    ButtonColumn btnCapture = new ButtonColumn(transactionsTable, captureAction, 2)
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        PosButton button = (PosButton)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        PosTransaction transaction = (PosTransaction)transactionTableModel.getRowData(row);
        if ((transaction.isCard()) && (transaction.isAuthorizable().booleanValue()) && (!transaction.isCaptured().booleanValue())) {
          button.setText("Capture");
          button.setHorizontalAlignment(0);
          table.setRowHeight(row, table.getRowHeight(0));
          return button;
        }
        return new JLabel("");
      }
      
      public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
      {
        PosTransaction transaction = (PosTransaction)transactionTableModel.getRowData(row);
        if ((transaction.isCard()) && (transaction.isAuthorizable().booleanValue()) && (!transaction.isCaptured().booleanValue())) {
          return super.getTableCellEditorComponent(table, value, false, row, column);
        }
        return new JLabel("");
      }
    };
    MatteBorder selectedBorder = BorderFactory.createMatteBorder(2, 2, 2, 2, transactionsTable.getBackground());
    MatteBorder unselectedBorder = BorderFactory.createMatteBorder(2, 2, 2, 2, transactionsTable.getBackground());
    
    PosButton sampleButton = new PosButton();
    Border border1 = new CompoundBorder(selectedBorder, sampleButton.getBorder());
    Border border2 = new CompoundBorder(unselectedBorder, sampleButton.getBorder());
    
    btnCapture.setUnselectedBorder(border1);
    btnCapture.setFocusBorder(border2);
    
    btnDelete.setUnselectedBorder(border1);
    btnDelete.setFocusBorder(border2);
    
    PosScrollPane scrollPane = new PosScrollPane(transactionsTable);
    scrollPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Payments"), BorderFactory.createEmptyBorder(0, 5, 0, 5)));
    transactionListPanel.add(scrollPane);
    scrollPane.setPreferredSize(PosUIManager.getSize(0, 130));
    transactionListPanel.setVisible(false);
    rendererTransactions();
  }
  
  private void doReversePayment(PosTransaction transaction, boolean forceVoid) {
    try {
      if (ticket.isRefunded().booleanValue()) {
        POSMessageDialog.showError(this, "Payments cannot be reversed after refund.");
        return;
      }
      if ((!forceVoid) && 
        (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Are you sure to void this payment?", POSConstants.CONFIRM) != 0)) {
        return;
      }
      TicketDAO.getInstance().reversePayment(ticket, transaction, forceVoid);
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Successfully voided.");
      ticketDataChanged();
    } catch (Exception e) {
      if (transaction.isCard()) {
        if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "System could not automatically VOID the payment. Do you want to force void?", POSConstants.CONFIRM) == 0)
        {
          doReversePayment(transaction, true);
        }
      }
      else {
        POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
      }
    }
  }
  
  private void doAuthorize(PosTransaction transaction) {
    try {
      if (!confirmTips(transaction))
        return;
      List<PosTransaction> transactions = new ArrayList();
      transactions.add(transaction);
      AuthorizationDialog authorizingDialog = new AuthorizationDialog(this, transactions);
      authorizingDialog.setVisible(true);
      updateView();
    } catch (Exception e) {
      ticket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
      updateView();
      POSMessageDialog.showError(this, e.getMessage());
      PosLog.error(getClass(), e);
    }
  }
  
  private boolean confirmTips(PosTransaction transaction) {
    double oldTipsAmount = transaction.getTipsAmount().doubleValue();
    double newTipsAmount = NumberSelectionDialog2.takeDoubleInput(Messages.getString("TicketAuthorizationDialog.8"), oldTipsAmount, true);
    

    if (newTipsAmount == -1.0D) {
      return false;
    }
    transaction.setTipsAmount(Double.valueOf(newTipsAmount));
    transaction.setAmount(Double.valueOf(transaction.getAmount().doubleValue() - oldTipsAmount + newTipsAmount));
    
    if (ticket.hasGratuity()) {
      double ticketTipsAmount = ticket.getGratuity().getAmount().doubleValue();
      double ticketPaidAmount = ticket.getPaidAmount().doubleValue();
      
      double newTicketTipsAmount = ticketTipsAmount - oldTipsAmount + newTipsAmount;
      double newTicketPaidAmount = ticketPaidAmount - oldTipsAmount + newTipsAmount;
      
      ticket.setGratuityAmount(newTicketTipsAmount);
      ticket.setPaidAmount(Double.valueOf(newTicketPaidAmount));
    }
    else {
      ticket.setGratuityAmount(newTipsAmount);
      ticket.setPaidAmount(Double.valueOf(ticket.getPaidAmount().doubleValue() + newTipsAmount));
    }
    ticket.calculatePrice();
    return true;
  }
  
  public void createTicketDiscountPanel() {
    discountInfoPanel = new JPanel(new MigLayout("fill,hidemode 3,ins 2 0 0 0"));
    
    discountTable = new JTable();
    discountTable.setGridColor(Color.LIGHT_GRAY);
    discountTable.setCellSelectionEnabled(false);
    discountTable.setColumnSelectionAllowed(false);
    discountTable.setRowSelectionAllowed(false);
    
    discountTable.setAutoscrolls(true);
    discountTable.setRowHeight(PosUIManager.getSize(40));
    discountTable.setShowGrid(true);
    discountTable.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
    discountTable.setFocusable(false);
    discountTable.getTableHeader().setPreferredSize(new Dimension(0, 0));
    discountTable.getTableHeader().setVisible(false);
    discountTable.setAutoResizeMode(4);
    discountTableModel = new TicketDiscountTableModel();
    discountTable.setModel(discountTableModel);
    discountTable.getColumnModel().getColumn(2).setCellRenderer(new DefaultTableCellRenderer()
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component rendererComponent = super.getTableCellRendererComponent(table, value, isSelected, false, row, column);
        setHorizontalAlignment(4);
        if (isSelected) {
          return rendererComponent;
        }
        rendererComponent.setBackground(table.getBackground());
        return rendererComponent;
      }
      
      protected void setValue(Object value)
      {
        if (value == null) {
          setText("");
          return;
        }
        String text = value.toString();
        if (((value instanceof Double)) || ((value instanceof Float))) {
          text = NumberUtil.formatNumberAcceptNegative(Double.valueOf(((Number)value).doubleValue()));
        }
        setText(text);
      }
    });
    setColumnWidth(1, PosUIManager.getSize(50), discountTable);
    setColumnWidth(2, PosUIManager.getSize(50), discountTable);
    
    JLabel lblDiscountTitle = new JLabel("Ticket discounts: ");
    discountInfoPanel.add(lblDiscountTitle, "wrap");
    
    AbstractAction action = new AbstractAction()
    {
      public void actionPerformed(ActionEvent e) {
        int row = Integer.parseInt(e.getActionCommand());
        TicketDiscount item = (TicketDiscount)discountTableModel.getRowData(row);
        List<TicketDiscount> discounts = ticket.getDiscounts();
        if (discounts != null) {
          for (Iterator iterator = discounts.iterator(); iterator.hasNext();) {
            TicketDiscount ticketDiscount = (TicketDiscount)iterator.next();
            if (ticketDiscount == item) {
              iterator.remove();
              break;
            }
          }
          ticketDataChanged();
        }
        ticket.setDiscounts(discounts);
      }
    };
    ButtonColumn coloum = new ButtonColumn(discountTable, action, 0)
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JPanel panel = (JPanel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        int verticalGap = PosUIManager.getSize(15);
        int horizontalGap = PosUIManager.getSize(2);
        PosButton button = (PosButton)panel.getComponent(0);
        button.setOpaque(false);
        button.setBorder(new EmptyBorder(verticalGap, horizontalGap, verticalGap, horizontalGap));
        button.setIcon(IconFactory.getIcon("/ui_icons/", "delete-icon.png"));
        table.setRowHeight(row, table.getRowHeight(0));
        return panel;
      }
      
      public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
      {
        return super.getTableCellEditorComponent(table, value, false, row, column);
      }
    };
    coloum.showColumnValueInLabel(true);
    JScrollPane scrollPane = new JScrollPane(discountTable);
    discountInfoPanel.add(scrollPane, "newline,grow,span");
    scrollPane.setPreferredSize(PosUIManager.getSize(0, 70));
    discountInfoPanel.setVisible(false);
    bottomPanel.add(discountInfoPanel, "North");
    rendererTicketDiscounts();
  }
  
  private void refreshOrderView() {
    if (OrderView.getInstance().isVisible())
      OrderView.getInstance().setCurrentTicket(ticket);
  }
  
  private void setColumnWidth(int columnNumber, int width, JTable table) {
    TableColumn column = table.getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  private void rendererTicketDiscounts() {
    List<TicketDiscount> discounts = ticket.getDiscounts();
    discountTableModel.setRows(discounts);
    discountInfoPanel.setVisible((discounts != null) && (discounts.size() > 0));
    centerPanel.revalidate();
    centerPanel.repaint();
    repaint();
  }
  
  private void rendererTransactions() {
    Set<PosTransaction> transactions = ticket.getTransactions();
    if ((transactions == null) || (transactions.size() == 0)) {
      return;
    }
    List<PosTransaction> rows = new ArrayList();
    for (PosTransaction posTransaction : transactions) {
      if ((!posTransaction.isVoided().booleanValue()) && (!(posTransaction instanceof RefundTransaction)))
      {
        rows.add(posTransaction); }
    }
    Collections.sort(rows, new Comparator()
    {
      public int compare(PosTransaction o1, PosTransaction o2)
      {
        return o1.getTransactionTime().compareTo(o2.getTransactionTime());
      }
    });
    transactionTableModel.setRows(rows);
    transactionListPanel.setVisible(rows.size() > 0);
    centerPanel.revalidate();
    centerPanel.repaint();
    repaint();
  }
  
  private String getTableNumbers(List<Integer> numbers) {
    if (numbers == null) {
      return null;
    }
    String tableNumbers = numbers.toString().replaceAll("[\\[\\]]", "");
    return tableNumbers;
  }
  
  private JPanel createTotalViewerPanel()
  {
    JLabel lblSubtotal = new JLabel();
    lblSubtotal.setHorizontalAlignment(4);
    lblSubtotal.setText(POSConstants.SUBTOTAL + ":" + " " + CurrencyUtil.getCurrencySymbol());
    
    tfSubtotal = new JTextField(10);
    tfSubtotal.setHorizontalAlignment(11);
    tfSubtotal.setEditable(false);
    
    JLabel lblDiscount = new JLabel();
    lblDiscount.setHorizontalAlignment(4);
    lblDiscount.setText(Messages.getString("TicketView.9") + " " + CurrencyUtil.getCurrencySymbol());
    
    tfDiscount = new JTextField(10);
    
    tfDiscount.setHorizontalAlignment(11);
    tfDiscount.setEditable(false);
    tfDiscount.setText(ticket.getDiscountAmount().toString());
    
    JLabel lblDeliveryCharge = new JLabel();
    lblDeliveryCharge.setHorizontalAlignment(4);
    lblDeliveryCharge.setText("Delivery Charge: " + CurrencyUtil.getCurrencySymbol());
    
    tfDeliveryCharge = new JTextField(10);
    tfDeliveryCharge.setHorizontalAlignment(11);
    tfDeliveryCharge.setEditable(false);
    
    JLabel lblTax = new JLabel();
    lblTax.setHorizontalAlignment(4);
    lblTax.setText(POSConstants.TAX + ":" + " " + CurrencyUtil.getCurrencySymbol());
    
    tfTax = new JTextField(10);
    
    tfTax.setEditable(false);
    tfTax.setHorizontalAlignment(11);
    
    JLabel lblServiceCharge = new JLabel();
    lblServiceCharge.setHorizontalAlignment(4);
    lblServiceCharge.setText("Service charge: " + CurrencyUtil.getCurrencySymbol());
    
    tfServiceCharge = new JTextField(10);
    
    tfServiceCharge.setEditable(false);
    tfServiceCharge.setHorizontalAlignment(11);
    
    tfRefundAmount.setEditable(false);
    tfRefundAmount.setHorizontalAlignment(11);
    
    tfPaidAmount.setEditable(false);
    tfPaidAmount.setHorizontalAlignment(11);
    
    JLabel lblGratuity = new JLabel();
    lblGratuity.setHorizontalAlignment(4);
    lblGratuity.setText(Messages.getString("SettleTicketDialog.5") + ":" + " " + CurrencyUtil.getCurrencySymbol());
    
    tfGratuity = new JTextField(10);
    tfGratuity.setEditable(false);
    tfGratuity.setHorizontalAlignment(11);
    
    JLabel lblTotal = new JLabel();
    lblTotal.setFont(lblTotal.getFont().deriveFont(1, PosUIManager.getFontSize(18)));
    lblTotal.setHorizontalAlignment(4);
    lblTotal.setText(POSConstants.TOTAL + ":" + " " + CurrencyUtil.getCurrencySymbol());
    
    tfTotal = new JTextField(10);
    tfTotal.setFont(tfTotal.getFont().deriveFont(1, PosUIManager.getFontSize(18)));
    tfTotal.setHorizontalAlignment(11);
    tfTotal.setEditable(false);
    
    JPanel ticketAmountPanel = new TransparentPanel(new MigLayout("fillx,hidemode 3,ins 2 10 3 2,alignx trailing", "[grow]2[]", ""));
    
    ticketAmountPanel.add(lblSubtotal, "growx,aligny center");
    ticketAmountPanel.add(tfSubtotal, "growx,aligny center");
    ticketAmountPanel.add(lblDiscount, "newline,growx,aligny center");
    ticketAmountPanel.add(tfDiscount, "growx,aligny center");
    ticketAmountPanel.add(lblTax, "newline,growx,aligny center");
    ticketAmountPanel.add(tfTax, "growx,aligny center");
    ticketAmountPanel.add(lblServiceCharge, "newline,growx,aligny center");
    ticketAmountPanel.add(tfServiceCharge, "growx,aligny center");
    if ((ticket.getOrderType().isDelivery().booleanValue()) && (!ticket.isCustomerWillPickup().booleanValue())) {
      ticketAmountPanel.add(lblDeliveryCharge, "newline,growx,aligny center");
      ticketAmountPanel.add(tfDeliveryCharge, "growx,aligny center");
    }
    

    ticketAmountPanel.add(lblTotal, "newline,growx,aligny center");
    ticketAmountPanel.add(tfTotal, "growx,aligny center");
    ticketAmountPanel.add(new JLabel("Paid: " + CurrencyUtil.getCurrencySymbol(), 11), "newline,growx,aligny center");
    ticketAmountPanel.add(tfPaidAmount, "growx,aligny center");
    lblRefundAmount = new JLabel("Refund: " + CurrencyUtil.getCurrencySymbol(), 11);
    ticketAmountPanel.add(lblRefundAmount, "newline,growx,aligny center");
    ticketAmountPanel.add(tfRefundAmount, "growx,aligny center");
    return ticketAmountPanel;
  }
  
  public void open()
  {
    super.open();
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
    ticketProcessor.setTicket(ticket);
    paymentView.setTicket(ticket);
    paymentView.updateView();
  }
  
  public void paymentCanceled()
  {
    setCanceled(true);
    dispose();
  }
  
  public void paymentDone()
  {
    setCanceled(false);
    dispose();
  }
  
  public void paymentDataChanged()
  {
    updateView();
    paymentView.updateView();
    ticketViewerTable.updateView();
  }
  
  public SettleTicketProcessor getTicketProcessor() {
    return ticketProcessor;
  }
  
  public class TicketDiscountTableModel extends ListTableModel<TicketDiscount>
  {
    public TicketDiscountTableModel() {
      super();
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      if (columnIndex == 0)
        return true;
      return false;
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      TicketDiscount discount = (TicketDiscount)rows.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return discount.getNameDisplay();
      
      case 1: 
        return "";
      
      case 2: 
        return Double.valueOf(-discount.getTotalDiscountAmount().doubleValue());
      }
      
      
      return null;
    }
  }
  
  public class TransactionDataTableModel extends ListTableModel<PosTransaction>
  {
    public TransactionDataTableModel() {
      super();
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      if ((columnIndex == 2) || (columnIndex == 3))
        return true;
      return false;
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      PosTransaction transaction = (PosTransaction)rows.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        String cardType = transaction.getCardType();
        if (StringUtils.isEmpty(cardType))
          return " " + transaction.getPaymentType();
        if (StringUtils.isNotEmpty(transaction.getCardNumber())) {
          cardType = cardType + " [" + transaction.getCardNumber().replace("X", "") + "]";
        }
        return " " + cardType;
      case 1: 
        if ((transaction.isVoided().booleanValue()) || ((transaction instanceof RefundTransaction)))
          return "-" + NumberUtil.formatNumber(transaction.getAmount()) + " ";
        return NumberUtil.formatNumber(transaction.getAmount()) + " ";
      case 2: 
        return "Capture";
      case 3: 
        return "";
      }
      return null;
    }
  }
  
  public void ticketDataChanged()
  {
    ticket.calculatePrice();
    updateView();
    paymentView.updateView();
    refreshOrderView();
  }
  
  private Terminal getTerminal() {
    return Application.getInstance().getTerminal();
  }
}
