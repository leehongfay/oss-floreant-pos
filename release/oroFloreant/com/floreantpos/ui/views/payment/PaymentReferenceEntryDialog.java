package com.floreantpos.ui.views.payment;

import com.floreantpos.Messages;
import com.floreantpos.model.CustomPayment;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.QwertyKeyPad;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;
















public class PaymentReferenceEntryDialog
  extends POSDialog
{
  private JLabel lblRef;
  private FixedLengthTextField txtRef;
  private String paymentRef;
  private CustomPayment payment;
  
  public PaymentReferenceEntryDialog(CustomPayment payment)
  {
    super(POSUtil.getFocusedWindow(), Dialog.ModalityType.APPLICATION_MODAL);
    setDefaultCloseOperation(2);
    setResizable(false);
    this.payment = payment;
    setTitle(Messages.getString("PaymentReferenceEntryDialog.0"));
    
    createUI();
  }
  
  private void createUI()
  {
    lblRef = new JLabel(Messages.getString("PaymentReferenceEntryDialog.1") + payment.getRefNumberFieldName());
    txtRef = new FixedLengthTextField(120);
    
    setPreferredSize(new Dimension(900, 500));
    
    JPanel customTypePanel = new JPanel(new MigLayout("", "grow", "grow"));
    customTypePanel.add(lblRef);
    customTypePanel.add(txtRef);
    
    QwertyKeyPad qwertyKeyPad = new QwertyKeyPad();
    
    JPanel centerPanel = new JPanel(new BorderLayout());
    centerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    
    centerPanel.add(customTypePanel, "North");
    centerPanel.add(qwertyKeyPad, "Center");
    
    getContentPane().add(centerPanel, "Center");
    
    JPanel panel_1 = new JPanel();
    getContentPane().add(panel_1, "South");
    panel_1.setLayout(new BorderLayout(0, 0));
    
    JPanel panel_2 = new JPanel();
    panel_1.add(panel_2);
    
    PosButton btnSubmit = new PosButton();
    panel_2.add(btnSubmit);
    btnSubmit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        submitCard();
      }
      
    });
    btnSubmit.setText(Messages.getString("PaymentReferenceEntryDialog.5"));
    
    PosButton btnCancel = new PosButton();
    panel_2.add(btnCancel);
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
      
    });
    btnCancel.setText(Messages.getString("PaymentReferenceEntryDialog.2"));
    
    JSeparator separator = new JSeparator();
    panel_1.add(separator, "North");
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle(Messages.getString("PaymentReferenceEntryDialog.6"));
    getContentPane().add(titlePanel, "North");
  }
  
  protected void submitCard()
  {
    String ref = txtRef.getText();
    if (ref.equals("")) {
      POSMessageDialog.showMessage(Messages.getString("PaymentReferenceEntryDialog.8"));
      return;
    }
    setPaymentRef(ref);
    setCanceled(false);
    dispose();
  }
  


  public String getPaymentRef()
  {
    return paymentRef;
  }
  


  public void setPaymentRef(String paymentRef)
  {
    this.paymentRef = paymentRef;
  }
}
