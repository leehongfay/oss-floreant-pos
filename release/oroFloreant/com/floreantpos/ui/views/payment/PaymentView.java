package com.floreantpos.ui.views.payment;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.customer.CustomerSelector;
import com.floreantpos.customer.CustomerSelectorDialog;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.FloreantPlugin;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.main.Application;
import com.floreantpos.model.CashBreakdown;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.Currency;
import com.floreantpos.model.CustomPayment;
import com.floreantpos.model.Customer;
import com.floreantpos.model.Gratuity;
import com.floreantpos.model.PaymentType;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.CustomPaymentDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.payment.PaymentPlugin;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.MultiCurrencyTenderDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.DrawerUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;



























public class PaymentView
  extends JPanel
{
  private static final String ZERO = "0";
  private static final String REMOVE = "1";
  private PosButton btnGratuity;
  private PosButton btnCancel;
  private PosButton btnCash;
  private PosButton btnPrint;
  private PosButton btnCreditCard;
  private PosButton btnGift;
  private PosButton btnOther;
  private PosButton btnCustomerPayment;
  private TransparentPanel calcButtonPanel;
  private JLabel labelDueAmount;
  private JLabel labelTipsAmount;
  private JLabel labelTenderedAmount;
  private TransparentPanel actionButtonPanel;
  private PosButton btn7;
  private PosButton btnDot;
  private PosButton btn0;
  private PosButton btnClear;
  private PosButton btn8;
  private PosButton btn9;
  private PosButton btn4;
  private PosButton btn5;
  private PosButton btn6;
  private PosButton btn3;
  private PosButton btn2;
  private PosButton btn1;
  private PosButton btn00;
  private PosButton btnNextAmount;
  private PosButton btnAmount1;
  private PosButton btnAmount2;
  private PosButton btnAmount5;
  private PosButton btnAmount10;
  private PosButton btnAmount20;
  private PosButton btnAmount50;
  private PosButton btnAmount100;
  private PosButton btnExactAmount;
  private JTextField txtTenderedAmount;
  private JTextField txtDueAmount;
  private JTextField txtTipsAmount;
  private PosButton btnRefund;
  private PosButton btnVoid;
  private PosButton btnTicketDiscount;
  private PosButton btnTicketItemDiscount;
  private Ticket ticket;
  private SettleTicketProcessor paymentProcessor;
  private boolean clearPreviousAmount = true;
  
  private Terminal terminal;
  
  private int width = PosUIManager.getSize(100);
  private PosButton btnSplitTicket;
  
  public PaymentView(SettleTicketProcessor ticketProcessor)
  {
    paymentProcessor = ticketProcessor;
    terminal = Application.getInstance().getTerminal();
    initComponents();
    setVisibilityBtnByConfig();
  }
  
  private void initComponents() {
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    setLayout(new MigLayout("fill, ins 0", "", ""));
    
    JPanel leftPanel = new JPanel(new BorderLayout());
    
    TransparentPanel transparentPanel1 = new TransparentPanel(new BorderLayout());
    
    labelDueAmount = new JLabel();
    labelTipsAmount = new JLabel();
    labelTenderedAmount = new JLabel();
    txtDueAmount = new JTextField();
    txtTipsAmount = new JTextField();
    txtTenderedAmount = new JTextField();
    
    Font font1 = new Font("Tahoma", 1, PosUIManager.getFontSize(20));
    Font font2 = new Font("Arial", 0, PosUIManager.getFontSize(34));
    
    labelTipsAmount.setFont(font1);
    labelTipsAmount.setText("Tips: " + CurrencyUtil.getCurrencySymbol());
    labelTipsAmount.setForeground(Color.gray);
    
    labelTipsAmount.setFocusable(false);
    txtTipsAmount.setFocusable(false);
    
    labelTenderedAmount.setFont(font1);
    labelTenderedAmount.setText(Messages.getString("PaymentView.54") + " " + CurrencyUtil.getCurrencySymbol());
    labelTenderedAmount.setForeground(Color.gray);
    
    txtTipsAmount.setHorizontalAlignment(4);
    txtTipsAmount.setEditable(false);
    txtTipsAmount.setFont(font1);
    
    txtTenderedAmount.setHorizontalAlignment(4);
    txtTenderedAmount.setFont(font1);
    
    labelDueAmount.setFont(font1);
    labelDueAmount.setText(Messages.getString("PaymentView.52") + " " + CurrencyUtil.getCurrencySymbol());
    labelDueAmount.setForeground(Color.gray);
    
    txtDueAmount.setFont(font1);
    txtDueAmount.setEditable(false);
    txtDueAmount.setHorizontalAlignment(4);
    
    transparentPanel1.setLayout(new MigLayout("hidemode 3,ins 0 0 5 0", "[][grow,fill]", "[grow][][grow]"));
    
    transparentPanel1.add(labelDueAmount, "cell 0 0,alignx right,aligny center");
    transparentPanel1.add(labelTipsAmount, "cell 0 1,alignx right,aligny center");
    transparentPanel1.add(labelTenderedAmount, "cell 0 2,alignx left,aligny center");
    
    transparentPanel1.add(txtDueAmount, "cell 1 0,growx,aligny top");
    transparentPanel1.add(txtTipsAmount, "cell 1 1,alignx left,aligny center");
    transparentPanel1.add(txtTenderedAmount, "cell 1 2,growx,aligny top");
    
    leftPanel.add(transparentPanel1, "North");
    
    calcButtonPanel = new TransparentPanel();
    calcButtonPanel.setLayout(new MigLayout("wrap 4,fill, ins 0", "sg, fill", "sg, fill"));
    
    btnNextAmount = new PosButton();
    btnAmount1 = new PosButton();
    btnAmount1.setFont(font2);
    
    btnAmount2 = new PosButton();
    btnAmount2.setFont(font2);
    
    btnAmount5 = new PosButton();
    btnAmount5.setFont(font2);
    
    btnAmount10 = new PosButton();
    btnAmount10.setFont(font2);
    
    btnAmount20 = new PosButton();
    btnAmount20.setFont(font2);
    
    btnAmount50 = new PosButton();
    btnAmount50.setFont(font2);
    
    btnAmount100 = new PosButton();
    btnAmount100.setFont(font2);
    
    btnExactAmount = new PosButton();
    
    btn7 = new PosButton();
    btn8 = new PosButton();
    btn9 = new PosButton();
    btn4 = new PosButton();
    btn5 = new PosButton();
    btn6 = new PosButton();
    btn1 = new PosButton();
    btn2 = new PosButton();
    btn3 = new PosButton();
    btn0 = new PosButton();
    btnDot = new PosButton();
    btnClear = new PosButton();
    btn00 = new PosButton();
    
    btnAmount1.setForeground(Color.blue);
    btnAmount1.setAction(nextButtonAction);
    btnAmount1.setText(Messages.getString("PaymentView.1"));
    btnAmount1.setActionCommand("1");
    btnAmount1.setFocusable(false);
    calcButtonPanel.add(btnAmount1);
    
    btn7.setAction(calAction);
    btn7.setText("7");
    btn7.setFont(font2);
    
    btn7.setActionCommand("7");
    btn7.setFocusable(false);
    calcButtonPanel.add(btn7);
    
    btn8.setAction(calAction);
    btn8.setText("8");
    btn8.setFont(font2);
    
    btn8.setActionCommand("8");
    btn8.setFocusable(false);
    calcButtonPanel.add(btn8);
    
    btn9.setAction(calAction);
    btn9.setText("9");
    btn9.setFont(font2);
    
    btn9.setActionCommand("9");
    btn9.setFocusable(false);
    calcButtonPanel.add(btn9);
    
    btnAmount2.setForeground(Color.blue);
    btnAmount2.setAction(nextButtonAction);
    btnAmount2.setText(Messages.getString("PaymentView.10"));
    btnAmount2.setActionCommand("2");
    btnAmount2.setFocusable(false);
    
    calcButtonPanel.add(btnAmount2);
    
    btn4.setAction(calAction);
    btn4.setText("4");
    btn4.setFont(font2);
    
    btn4.setActionCommand("4");
    btn4.setFocusable(false);
    calcButtonPanel.add(btn4);
    
    btn5.setAction(calAction);
    btn5.setText("5");
    btn5.setFont(font2);
    
    btn5.setActionCommand("5");
    btn5.setFocusable(false);
    calcButtonPanel.add(btn5);
    
    btn6.setAction(calAction);
    btn6.setText("6");
    btn6.setFont(font2);
    
    btn6.setActionCommand("6");
    btn6.setFocusable(false);
    calcButtonPanel.add(btn6);
    
    btnAmount5.setForeground(Color.blue);
    btnAmount5.setAction(nextButtonAction);
    btnAmount5.setText(Messages.getString("PaymentView.12"));
    btnAmount5.setActionCommand("5");
    btnAmount5.setFocusable(false);
    
    calcButtonPanel.add(btnAmount5);
    
    btn1.setAction(calAction);
    btn1.setText("1");
    btn1.setFont(font2);
    
    btn1.setActionCommand("1");
    btn1.setFocusable(false);
    calcButtonPanel.add(btn1);
    
    btn2.setAction(calAction);
    btn2.setText("2");
    btn2.setFont(font2);
    
    btn2.setActionCommand("2");
    btn2.setFocusable(false);
    calcButtonPanel.add(btn2);
    
    btn3.setAction(calAction);
    btn3.setText("3");
    btn3.setFont(font2);
    
    btn3.setActionCommand("3");
    btn3.setFocusable(false);
    calcButtonPanel.add(btn3);
    
    btnAmount10.setForeground(Color.blue);
    btnAmount10.setAction(nextButtonAction);
    btnAmount10.setText(Messages.getString("PaymentView.14"));
    btnAmount10.setActionCommand("10");
    btnAmount10.setFocusable(false);
    calcButtonPanel.add(btnAmount10, "grow");
    
    btn0.setAction(calAction);
    btn0.setText("0");
    btn0.setFont(font2);
    
    btn0.setActionCommand("0");
    btn0.setFocusable(false);
    calcButtonPanel.add(btn0);
    
    btn00.setFont(new Font("Arial", 0, 30));
    btn00.setAction(calAction);
    btn00.setText(Messages.getString("PaymentView.18"));
    btn00.setActionCommand("00");
    btn00.setFocusable(false);
    calcButtonPanel.add(btn00);
    
    btnDot.setAction(calAction);
    btnDot.setIcon(IconFactory.getIcon("/ui_icons/", "dot.png"));
    btnDot.setActionCommand(".");
    btnDot.setFocusable(false);
    calcButtonPanel.add(btnDot);
    
    btnAmount20.setForeground(Color.BLUE);
    btnAmount20.setAction(nextButtonAction);
    btnAmount20.setText("20");
    btnAmount20.setActionCommand("20");
    btnAmount20.setFocusable(false);
    calcButtonPanel.add(btnAmount20, "grow");
    
    btnAmount50.setForeground(Color.blue);
    btnAmount50.setAction(nextButtonAction);
    btnAmount50.setText("50");
    btnAmount50.setActionCommand("50");
    btnAmount50.setFocusable(false);
    calcButtonPanel.add(btnAmount50, "grow");
    
    btnAmount100.setForeground(Color.blue);
    btnAmount100.setAction(nextButtonAction);
    btnAmount100.setText("100");
    btnAmount100.setActionCommand("100");
    btnAmount100.setFocusable(false);
    calcButtonPanel.add(btnAmount100, "grow");
    
    btnClear.setAction(calAction);
    
    btnClear.setText(Messages.getString("PaymentView.38"));
    btnClear.setFocusable(false);
    calcButtonPanel.add(btnClear);
    
    btnExactAmount.setAction(nextButtonAction);
    btnExactAmount.setText(Messages.getString("PaymentView.20"));
    btnExactAmount.setActionCommand("exactAmount");
    btnExactAmount.setFocusable(false);
    calcButtonPanel.add(btnExactAmount, "span 2,grow");
    
    btnNextAmount.setAction(nextButtonAction);
    btnNextAmount.setText(Messages.getString("PaymentView.23"));
    btnNextAmount.setActionCommand("nextAmount");
    btnNextAmount.setFocusable(false);
    calcButtonPanel.add(btnNextAmount, "span 2,grow");
    
    btnGratuity = new PosButton(POSConstants.ADD_GRATUITY_TEXT);
    btnGratuity.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doSetGratuity();
      }
      
    });
    btnVoid = new PosButton(POSConstants.VOID.toUpperCase());
    btnVoid.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        paymentProcessor.doVoidTicket();
      }
      
    });
    btnTicketDiscount = new PosButton("<html><center>TICKET <br>DISCOUNT</center></html>");
    btnTicketDiscount.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        paymentProcessor.doApplyCoupon(1);
      }
      
    });
    btnTicketItemDiscount = new PosButton("<html><center>ITEM <br>DISCOUNT</center></html>");
    btnTicketItemDiscount.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        paymentProcessor.doApplyCoupon(0);
      }
      
    });
    btnPrint = new PosButton(POSConstants.PRINT_TICKET);
    btnPrint.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        paymentProcessor.printTicket(ticket);
      }
      
    });
    btnSplitTicket = new PosButton("SPLIT");
    btnSplitTicket.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        paymentProcessor.doSplitTicket();
      }
      
    });
    PosButton btnNoSale = new PosButton("NO SALE");
    btnNoSale.setFocusable(false);
    btnNoSale.addActionListener(new ActionListener()
    {


      public void actionPerformed(ActionEvent e) {}


    });
    JPanel actionButtonPanelThird = new JPanel(new GridLayout(1, 0, 5, 5));
    JPanel actionButtonPanelFirst = new JPanel(new GridLayout(1, 0, 5, 5));
    JPanel actionButtonPanelSecond = new JPanel(new GridLayout(1, 0, 5, 5));
    
    actionButtonPanelFirst.add(btnGratuity);
    actionButtonPanelFirst.add(btnExactAmount);
    actionButtonPanelFirst.add(btnNextAmount);
    

    actionButtonPanelSecond.add(btnVoid);
    actionButtonPanelSecond.add(btnTicketDiscount);
    actionButtonPanelSecond.add(btnTicketItemDiscount);
    
    actionButtonPanelThird.add(btnSplitTicket);
    if (terminal.isShowPrntBtn().booleanValue()) {
      actionButtonPanelThird.add(btnPrint);
    }
    actionButtonPanelThird.add(btnNoSale);
    
    calcButtonPanel.add(actionButtonPanelFirst, "span 4,growx");
    calcButtonPanel.add(actionButtonPanelSecond, "span 4,growx");
    calcButtonPanel.add(actionButtonPanelThird, "span 4,growx");
    leftPanel.add(calcButtonPanel, "Center");
    
    actionButtonPanel = new TransparentPanel();
    actionButtonPanel.setOpaque(true);
    actionButtonPanel.setLayout(new MigLayout("hidemode 3,wrap 1, ins 0 5 0 0, fill", "sg, fill", ""));
    
    btnCash = new PosButton(Messages.getString("PaymentView.31"));
    actionButtonPanel.add(btnCash, "grow,w " + width + "!");
    btnCash.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        PaymentView.this.doPayByCash();
      }
      
    });
    PosButton btnMultiCurrencyCash = new PosButton("MULTI CURRENCY CASH");
    actionButtonPanel.add(btnMultiCurrencyCash, "grow,w " + width + "!");
    btnMultiCurrencyCash.setVisible(terminal.isEnableMultiCurrency().booleanValue());
    btnMultiCurrencyCash.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        PaymentView.this.doPayByMulticurrencyCash();
      }
      
    });
    btnCreditCard = new PosButton(Messages.getString("PaymentView.33"));
    actionButtonPanel.add(btnCreditCard, "grow,w " + width + "!");
    btnCreditCard.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        PaymentView.this.doPayBy(PaymentType.CREDIT_CARD);
      }
      
    });
    btnGift = new PosButton(Messages.getString("PaymentView.35"));
    actionButtonPanel.add(btnGift, "grow,w " + width + "!");
    btnGift.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        PaymentView.this.doPayBy(PaymentType.GIFT_CERTIFICATE);
      }
      
    });
    List<FloreantPlugin> paymentPlugins = ExtensionManager.getPlugins(PaymentPlugin.class);
    for (FloreantPlugin floreantPlugin : paymentPlugins) {
      final PaymentPlugin paymentPlugin = (PaymentPlugin)floreantPlugin;
      PosButton btnPay = new PosButton(paymentPlugin.getName());
      actionButtonPanel.add(btnPay, "grow,w " + width + "!");
      btnPay.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent ev)
        {
          try {
            paymentPlugin.pay(ticket, PaymentView.this.getTenderAmountFieldValue(), paymentProcessor);
          } catch (PosException e) {
            POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
          } catch (Exception e) {
            POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
          }
        }
      });
    }
    
    btnOther = new PosButton("OTHER");
    actionButtonPanel.add(btnOther, "grow,w " + width + "!");
    btnOther.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        PaymentView.this.doPayBy(PaymentType.CUSTOM_PAYMENT);
      }
      
    });
    btnCustomerPayment = new PosButton("CUST. ACCT.");
    actionButtonPanel.add(btnCustomerPayment, "grow,w " + width + "!");
    btnCustomerPayment.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        PaymentView.this.doPaymentFromCustomerBlnce();
      }
    });
    btnRefund = new PosButton("REFUND");
    btnRefund.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        paymentProcessor.doRefund();
      }
    });
    actionButtonPanel.add(btnRefund, "grow,w " + width + "!");
    


    add(leftPanel, "cell 0 0,grow");
    add(actionButtonPanel, "cell 1 0,grow");
  }
  
  private void setVisibilityBtnByConfig()
  {
    if (terminal == null) {
      return;
    }
    Boolean isHideCashPayment = Boolean.valueOf(terminal.getProperty("hideCashPayment"));
    Boolean isHideCardPayment = Boolean.valueOf(terminal.getProperty("hideCardPayment"));
    Boolean isHideGiftCardPayment = Boolean.valueOf(terminal.getProperty("hideGiftCardPayment"));
    Boolean isHideCustomPayment = Boolean.valueOf(terminal.getProperty("hideCustomPayment"));
    Boolean isShowIndivBtn = Boolean.valueOf(terminal.getProperty("showIndivBtn"));
    Boolean isHideCustBlncePayment = Boolean.valueOf(terminal.getProperty("hideCustBlncPayment"));
    
    OrderServiceExtension orderServiceExtension = (OrderServiceExtension)ExtensionManager.getPlugin(OrderServiceExtension.class);
    if (orderServiceExtension == null) {
      btnCustomerPayment.setVisible(false);
    }
    else {
      btnCustomerPayment.setVisible(!isHideCustBlncePayment.booleanValue());
    }
    
    btnCash.setVisible(!isHideCashPayment.booleanValue());
    btnCreditCard.setVisible(!isHideCardPayment.booleanValue());
    btnGift.setVisible(!isHideGiftCardPayment.booleanValue());
    Iterator iter; if ((!isHideCustomPayment.booleanValue()) && (isShowIndivBtn.booleanValue())) {
      btnOther.setVisible(false);
      
      List<CustomPayment> custompPayments = CustomPaymentDAO.getInstance().findAll();
      
      for (iter = custompPayments.iterator(); iter.hasNext();) {
        final CustomPayment customPayment = (CustomPayment)iter.next();
        if (customPayment.isEnable().booleanValue())
        {


          PosButton button = new PosButton("<html><body><center>" + customPayment.getName() + "</center></body></html>");
          button.setPreferredSize(new Dimension(120, 80));
          button.addActionListener(new ActionListener()
          {
            public void actionPerformed(ActionEvent e) {
              paymentProcessor.payByCustomPayment(customPayment);
            }
          });
          actionButtonPanel.add(button, "grow,w " + width + "!");
        }
      }
    } else if (!isHideCustomPayment.booleanValue()) {
      btnOther.setVisible(!isHideCustomPayment.booleanValue());
    }
    else {
      btnOther.setVisible(!isHideCustomPayment.booleanValue());
    }
    
    doAddCancelButton();
  }
  
  private void doAddCancelButton() {
    PosButton btnHold = new PosButton(POSConstants.HOLD_BUTTON_TEXT.toUpperCase());
    actionButtonPanel.add(btnHold, "grow,w " + width + "!");
    btnHold.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        paymentProcessor.doHoldTicket();
      }
      
    });
    btnCancel = new PosButton(POSConstants.CANCEL.toUpperCase());
    actionButtonPanel.add(btnCancel, "grow,w " + width + "!");
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        PaymentView.this.btnCancelActionPerformed(evt);
      }
    });
  }
  
  protected boolean adjustCashDrawerBalance(List<Currency> currencyList) {
    MultiCurrencyTenderDialog dialog = new MultiCurrencyTenderDialog(getTicket(), currencyList);
    dialog.pack();
    dialog.open();
    
    if (dialog.isCanceled()) {
      return false;
    }
    txtTenderedAmount.setText(NumberUtil.format3DigitNumber(Double.valueOf(dialog.getTenderedAmount())));
    
    return true;
  }
  
  protected void doSetGratuity() {
    paymentProcessor.doSetGratuity();
  }
  
  protected void doTaxExempt() {}
  
  private void btnCancelActionPerformed(ActionEvent evt)
  {
    paymentProcessor.cancelPayment();
  }
  


  Action calAction = new AbstractAction()
  {
    public void actionPerformed(ActionEvent e) {
      JTextField textField = txtTenderedAmount;
      
      PosButton button = (PosButton)e.getSource();
      String command = button.getActionCommand();
      if (command.equals(Messages.getString("PaymentView.66"))) {
        textField.setText("0");
      }
      else if (command.equals(".")) {
        if (textField.getText().indexOf('.') < 0) {
          textField.setText(textField.getText() + ".");
        }
      }
      else {
        if (clearPreviousAmount) {
          textField.setText("");
          clearPreviousAmount = false;
        }
        String string = textField.getText();
        int index = string.indexOf('.');
        if (index < 0) {
          double value = 0.0D;
          try {
            value = Double.parseDouble(string);
          } catch (NumberFormatException x) {
            Toolkit.getDefaultToolkit().beep();
          }
          if (value == 0.0D) {
            textField.setText(command);
          }
          else {
            textField.setText(string + command);
          }
        }
        else {
          textField.setText(string + command);
        }
      }
    }
  };
  
  Action nextButtonAction = new AbstractAction() {
    public void actionPerformed(ActionEvent e) {
      PaymentView.this.fillPredefinedAmount(e);
    }
  };
  
  public void updateView() {
    double tipsAmount = ticket.getGratuityAmount();
    double dueAmount = getDueAmount();
    
    txtDueAmount.setText(NumberUtil.formatNumber(Double.valueOf(dueAmount - tipsAmount), true));
    txtTipsAmount.setText(NumberUtil.formatNumber(Double.valueOf(tipsAmount), true));
    txtTenderedAmount.setText(NumberUtil.formatNumber(Double.valueOf(dueAmount), true));
    
    labelTipsAmount.setVisible(tipsAmount > 0.0D);
    txtTipsAmount.setVisible(tipsAmount > 0.0D);
  }
  
  public double getTenderedAmount() throws ParseException
  {
    double doubleValue = NumberUtil.parse(txtTenderedAmount.getText()).doubleValue();
    return doubleValue;
  }
  
  protected double getPaidAmount() {
    return getTicket().getPaidAmount().doubleValue();
  }
  
  protected double getDueAmount() {
    return getTicket().getDueAmount().doubleValue();
  }
  
  protected double getAdvanceAmount() {
    Gratuity gratuity = getTicket().getGratuity();
    return gratuity != null ? gratuity.getAmount().doubleValue() : 0.0D;
  }
  
  protected double getTotalGratuity() {
    return getTicket().getPaidAmount().doubleValue();
  }
  
  public void setDefaultFocus() {
    txtTenderedAmount.requestFocus();
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
    paymentProcessor.setTicket(ticket);
    updateView();
  }
  
  public SettleTicketProcessor getTicketProcessor() {
    return paymentProcessor;
  }
  
  public void setTicketProcessor(SettleTicketProcessor ticketProcessor) {
    paymentProcessor = ticketProcessor;
  }
  
  private double getTenderAmountFieldValue() {
    try {
      String tenderedAmountString = txtTenderedAmount.getText();
      NumberFormat format = NumberFormat.getNumberInstance();
      Number number = format.parse(tenderedAmountString);
      double tenderAmount = number.doubleValue();
      
      if (tenderAmount < 0.0D) {
        throw new PosException("Invalid tender amount");
      }
      return tenderAmount;
    } catch (Exception e) {
      throw new PosException("Invalid tender amount");
    }
  }
  
  private void doPayByCash() {
    try {
      double terderAmount = getTenderAmountFieldValue();
      if (Application.getInstance().getTerminal().isEnableMultiCurrency().booleanValue()) {
        User currentUser = Application.getCurrentUser();
        CashDrawer cashDrawer = currentUser.getActiveDrawerPullReport();
        Currency mainCurrency = CurrencyUtil.getMainCurrency();
        CashBreakdown item = cashDrawer.getCurrencyBalance(mainCurrency);
        if (item == null) {
          item = new CashBreakdown();
          item.setCurrency(mainCurrency);
          cashDrawer.addTocashBreakdownList(item);
          item.setCashDrawer(cashDrawer);
        }
        double amount = 0.0D;
        Ticket ticket = getTicket();
        if (terderAmount >= ticket.getDueAmount().doubleValue()) {
          amount = ticket.getDueAmount().doubleValue();
        }
        else {
          amount = terderAmount;
        }
        item.setBalance(Double.valueOf(NumberUtil.roundToThreeDigit(item.getBalance().doubleValue() + amount)));
        if (terderAmount > 0.0D) {
          ticket.addProperty(mainCurrency.getName(), String.valueOf(terderAmount));
        }
        if (terderAmount - amount > 0.0D) {
          ticket.addProperty(mainCurrency.getName() + "_CASH_BACK", String.valueOf(terderAmount - ticket.getDueAmount().doubleValue()));
        }
      }
      paymentProcessor.doSettle(PaymentType.CASH, terderAmount);
    } catch (PosException e) {
      POSMessageDialog.showError(getParent(), e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(getParent(), e.getMessage(), e);
    }
  }
  
  private void doPayByMulticurrencyCash() {
    try {
      List<Currency> currencyList = CurrencyUtil.getAllCurrency();
      if ((currencyList.size() > 1) && 
        (!adjustCashDrawerBalance(currencyList))) {
        return;
      }
      
      double terderAmount = getTenderAmountFieldValue();
      paymentProcessor.doSettle(PaymentType.CASH, terderAmount);
    } catch (PosException e) {
      POSMessageDialog.showError(getParent(), e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(getParent(), e.getMessage(), e);
    }
  }
  
  private void doPayBy(PaymentType paymentType) {
    try {
      if (!validAmount())
        return;
      TicketDAO.getInstance().saveOrUpdate(ticket);
      double terderAmount = getTenderAmountFieldValue();
      paymentProcessor.doSettle(paymentType, terderAmount);
    } catch (PosException e) {
      POSMessageDialog.showError(getParent(), e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(getParent(), e.getMessage(), e);
    }
  }
  
  private boolean validAmount() {
    if (getDueAmount() == 0.0D) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Invalid amount");
      return false;
    }
    return true;
  }
  
  private void fillPredefinedAmount(ActionEvent e) {
    try {
      DecimalFormat format = new DecimalFormat("##.00");
      
      PosButton button = (PosButton)e.getSource();
      
      String command = button.getActionCommand();
      
      if (command.equals("exactAmount")) {
        double dueAmount = getDueAmount();
        txtTenderedAmount.setText(NumberUtil.formatNumber(Double.valueOf(dueAmount)));
      }
      else if (command.equals("nextAmount")) {
        double amount = Math.ceil(getDueAmount());
        txtTenderedAmount.setText(String.valueOf(format.format(amount)));
      }
      else {
        if (clearPreviousAmount) {
          txtTenderedAmount.setText("0");
          clearPreviousAmount = false;
        }
        
        double tenderAmount = getTenderAmountFieldValue();
        double inputValue = Double.parseDouble(command);
        double newAmount = tenderAmount + inputValue;
        txtTenderedAmount.setText(String.valueOf(format.format(newAmount)));
      }
    } catch (PosException ex) {
      POSMessageDialog.showError(getParent(), ex.getMessage());
    } catch (Exception ex) {
      POSMessageDialog.showError(getParent(), ex.getMessage(), ex);
    }
  }
  
  private void doPaymentFromCustomerBlnce() {
    if (!validAmount())
      return;
    Customer customer = null;
    try {
      if (StringUtils.isEmpty(ticket.getCustomerId())) {
        OrderServiceExtension orderServicePlugin = (OrderServiceExtension)ExtensionManager.getPlugin(OrderServiceExtension.class);
        if (orderServicePlugin == null) {
          return;
        }
        
        CustomerSelector customerSelector = orderServicePlugin.createNewCustomerSelector();
        
        if (customerSelector == null) {
          return;
        }
        customerSelector.redererCustomers();
        customerSelector.setVisibleOnlySelectionButtons(false);
        
        CustomerSelectorDialog customerSelectorDialog = new CustomerSelectorDialog(customerSelector);
        
        customerSelectorDialog.setCreateNewTicket(false);
        if (ticket != null) {
          customerSelectorDialog.setTicket(ticket);
        }
        customerSelectorDialog.openUndecoratedFullScreen();
        
        if (customerSelectorDialog.isCanceled()) {
          return;
        }
        customer = customerSelectorDialog.getSelectedCustomer();
        Double customerBalance = customer.getBalance();
        if (customerBalance.doubleValue() >= getTenderedAmount())
        {

          String msg = "<html><body>" + CurrencyUtil.getCurrencySymbol() + getTenderedAmount() + " will be deducted from customer balance.<br><br><b>Customer id:</b> " + customer.getId() + "<br><b>Customer name:</b> " + customer.getName() + "<br><b>Available balance:</b> " + CurrencyUtil.getCurrencySymbol() + customerBalance + "<br><br>Will you proceed for transaction?<br></body></html>";
          

          int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), msg, "Confirmation message:");
          if (option != 0) {
            return;
          }
          ticket.setCustomer(customer);
        }
        else {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Sorry, customer does not have enough balance for payment!");
          return;
        }
      }
      

      doPayBy(PaymentType.CUSTOMER_ACCOUNT);
    } catch (Exception e) {
      PosLog.error(PaymentView.class, e.getMessage(), e);
    }
  }
}
