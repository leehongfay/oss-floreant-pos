package com.floreantpos.ui.views.payment;

import com.floreantpos.config.AppProperties;
import com.floreantpos.main.Application;
import com.floreantpos.model.PaymentType;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.ReversalTransaction;
import com.floreantpos.model.StoreSession;
import com.floreantpos.model.StoreSessionControl;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.PosTransactionDAO;
import com.floreantpos.model.dao.StoreSessionControlDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;



















public class SessionBasedTransactionBrowser
  extends POSDialog
{
  private JTable table;
  private TransactionListTableModel tableModel;
  
  public SessionBasedTransactionBrowser()
  {
    setTitle(AppProperties.getAppName());
    init();
    initData();
  }
  
  private void initData() {
    StoreSessionControl storeSessionControl = StoreSessionControlDAO.getInstance().getCurrent();
    StoreSession storeSession = storeSessionControl.getCurrentData();
    List<PosTransaction> transactionList = PosTransactionDAO.getInstance().getStoreSessionTransactions(storeSession);
    tableModel.setRows(transactionList);
  }
  
  private void init() {
    setLayout(new BorderLayout(5, 5));
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Transaction List Of Current Session");
    add(titlePanel, "North");
    
    table = new JTable();
    table.setModel(this.tableModel = new TransactionListTableModel());
    table.setRowHeight(PosUIManager.getSize(40));
    table.setAutoResizeMode(3);
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    table.setGridColor(Color.LIGHT_GRAY);
    
    PosScrollPane scrollPane = new PosScrollPane(table, 20, 31);
    JScrollBar scrollBar = scrollPane.getVerticalScrollBar();
    scrollBar.setPreferredSize(PosUIManager.getSize(30, 60));
    
    JPanel centerPanel = new JPanel(new MigLayout("fill"));
    TitledBorder border = BorderFactory.createTitledBorder(null, "TRANSACTIONS", 2, 0);
    centerPanel.setBorder(border);
    centerPanel.add(scrollPane, "grow");
    add(centerPanel, "Center");
    
    JPanel buttonPanel = new JPanel(new MigLayout("center"));
    PosButton btnReverse = new PosButton("DELETE");
    btnReverse.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        SessionBasedTransactionBrowser.this.doRevertTransaction();
      }
    });
    PosButton btnClose = new PosButton("CLOSE");
    btnClose.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
      
    });
    buttonPanel.add(btnReverse);
    buttonPanel.add(btnClose);
    add(buttonPanel, "South");
  }
  
  private void doRevertTransaction() {
    int index = table.getSelectedRow();
    if (index < 0) {
      return;
    }
    int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Are you sure to delete this transaction?", "Delete Confirmation:");
    
    if (option != 0) {
      return;
    }
    index = table.convertRowIndexToModel(index);
    
    PosTransaction transaction = (PosTransaction)tableModel.getRowData(index);
    Ticket ticket = transaction.getTicket();
    TicketDAO.getInstance().loadFullTicket(ticket);
    double dueAmount = ticket.getDueAmount().doubleValue();
    double transactionAmount = transaction.getAmount().doubleValue();
    ticket.setDueAmount(Double.valueOf(dueAmount + transactionAmount));
    ticket.setPaidAmount(Double.valueOf(0.0D));
    ticket.setPaid(Boolean.valueOf(false));
    ticket.setClosed(Boolean.valueOf(false));
    ticket.setClosingDate(null);
    ticket.getTransactions().remove(transaction);
    
    ReversalTransaction reversalTransaction = new ReversalTransaction();
    
    reversalTransaction.setPaymentType(PaymentType.CASH.getDisplayString());
    reversalTransaction.setTicket(ticket);
    reversalTransaction.setCaptured(Boolean.valueOf(true));
    reversalTransaction.setAmount(Double.valueOf(transactionAmount));
    reversalTransaction.setTransactionTime(new Date());
    reversalTransaction.setTerminal(Application.getInstance().getTerminal());
    reversalTransaction.setUser(Application.getCurrentUser());
    reversalTransaction.setNote("Reversed transaction type: " + transaction.getPaymentType());
    
    PosTransactionDAO.getInstance().saveReversalTransaction(ticket, transaction, reversalTransaction);
    tableModel.deleteItem(transaction);
    tableModel.fireTableDataChanged();
    table.repaint();
    table.revalidate();
  }
  
  private class TransactionListTableModel extends ListTableModel<PosTransaction> {
    public TransactionListTableModel() {
      super();
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      PosTransaction transaction = (PosTransaction)rows.get(rowIndex);
      
      Ticket ticket = transaction.getTicket();
      switch (columnIndex) {
      case 0: 
        return transaction.getId();
      
      case 1: 
        return ticket.getId();
      
      case 2: 
        User owner = ticket.getOwner();
        if (owner == null) {
          return null;
        }
        return owner.getFirstName();
      
      case 3: 
        return transaction.getTransactionTime();
      
      case 4: 
        return transaction.getPaymentType();
      
      case 5: 
        return transaction.getTipsAmount();
      
      case 6: 
        return Double.valueOf(transaction.getAmount().doubleValue() - transaction.getTipsAmount().doubleValue());
      
      case 7: 
        return transaction.getAmount();
      }
      
      
      return null;
    }
  }
}
