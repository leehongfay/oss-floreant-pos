package com.floreantpos.ui.views.payment;

public abstract interface PaymentListener
{
  public abstract void paymentDone();
  
  public abstract void paymentCanceled();
  
  public abstract void paymentDataChanged();
}
