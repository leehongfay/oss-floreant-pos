package com.floreantpos.ui.views.payment;

import com.floreantpos.Messages;
import com.floreantpos.ui.dialog.POSDialog;
import java.awt.Font;
import javax.swing.JLabel;


















public class PosPaymentWaitDialog
  extends POSDialog
{
  private JLabel label;
  
  public PosPaymentWaitDialog()
  {
    setTitle(Messages.getString("PaymentProcessWaitDialog.0"));
    
    label = new JLabel("Waiting for response from credit card Device..........");
    label.setHorizontalAlignment(0);
    label.setFont(label.getFont().deriveFont(1, 20.0F));
    add(label);
    
    setSize(500, 400);
    setLocationRelativeTo(null);
  }
  
  public void setMessage(String msg) {
    label.setText(msg);
  }
}
