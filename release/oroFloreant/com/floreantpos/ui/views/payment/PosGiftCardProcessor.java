package com.floreantpos.ui.views.payment;

import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.model.GiftCard;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.dao.GiftCardDAO;
import com.floreantpos.model.dao.PosTransactionDAO;
import java.util.Date;
import java.util.List;
import us.fatehi.magnetictrack.bankcard.BankCardMagneticTrack;



































































public class PosGiftCardProcessor
  implements GiftCardProcessor
{
  private GiftCard giftCard;
  
  public PosGiftCardProcessor() {}
  
  public static void main(String[] args)
    throws Exception
  {
    BankCardMagneticTrack track = BankCardMagneticTrack.from("%B4111111111111111^SHAH/RIAR^1803101000000000020000831000000?;4111111111111111=1803101000020000831?");
    PosLog.info(PosGiftCardProcessor.class, "" + track.getTrack1());
  }
  



  public void addBalance(GiftCard giftCard)
  {
    GiftCardDAO.getInstance().saveOrUpdate(giftCard);
  }
  
  public void activate(GiftCard giftCard)
  {
    GiftCardDAO.getInstance().saveOrUpdate(giftCard);
  }
  
  public void deactivate(GiftCard giftCard)
  {
    GiftCardDAO.getInstance().saveOrUpdate(giftCard);
  }
  
  public boolean supportActivation()
  {
    return true;
  }
  
  public boolean supportDeActivation()
  {
    return true;
  }
  
  public boolean supportPinNumberChange()
  {
    return true;
  }
  
  public boolean supportDisable()
  {
    return true;
  }
  
  public boolean supportAddBalance()
  {
    return true;
  }
  
  public boolean isValid(String cardNo) {
    giftCard = GiftCardDAO.getInstance().findByCardNumber(cardNo);
    if (giftCard == null) {
      throw new PosException("Sorry, card is not valid.");
    }
    if (!giftCard.isActive().booleanValue()) {
      throw new PosException("Sorry, card is not active.");
    }
    Date expDate = giftCard.getExpiryDate();
    Date today = new Date();
    if (today.compareTo(expDate) > 0) {
      throw new PosException("Sorry, card validity has expired!");
    }
    return true;
  }
  
  public void chargeAmount(PosTransaction transaction) throws Exception {
    Double totalAmount = transaction.getTenderAmount();
    String cardNo = transaction.getGiftCertNumber();
    giftCard = GiftCardDAO.getInstance().findByCardNumber(cardNo);
    Double cardBalance = giftCard.getBalance();
    
    if (!giftCard.isActive().booleanValue()) {
      throw new PosException("Sorry, card is not active.");
    }
    if (cardBalance.doubleValue() <= 0.0D) {
      throw new PosException("Gift card balance is 0.");
    }
    Date expDate = giftCard.getExpiryDate();
    Date today = new Date();
    if (today.compareTo(expDate) > 0) {
      throw new PosException("Sorry, card validity has expired!");
    }
    
    if (cardBalance.doubleValue() < totalAmount.doubleValue()) {
      totalAmount = cardBalance;
      double paidAmount = cardBalance.doubleValue();
      transaction.setAmount(Double.valueOf(paidAmount));
      transaction.setTenderAmount(Double.valueOf(paidAmount));
    }
    else {
      transaction.setAmount(totalAmount);
    }
    
    Double updatedAmount = Double.valueOf(cardBalance.doubleValue() - totalAmount.doubleValue());
    giftCard.setBalance(updatedAmount);
    GiftCardDAO.getInstance().saveOrUpdate(giftCard);
  }
  
  public GiftCard getCard(String strGiftCardNumber)
  {
    GiftCard findByCardNumber = GiftCardDAO.getInstance().findByCardNumber(strGiftCardNumber);
    return findByCardNumber;
  }
  
  public void disable(GiftCard giftCard)
  {
    GiftCardDAO.getInstance().saveOrUpdate(giftCard);
  }
  

  public void changePinNumber(GiftCard giftCard)
  {
    GiftCardDAO.getInstance().saveOrUpdate(giftCard);
  }
  

  public boolean supportShowTransaction()
  {
    return true;
  }
  
  public List<PosTransaction> getTransactionList(String cardNumber, Date fromDate, Date toDate)
  {
    List<PosTransaction> findTransactionListByGiftCardNumber = PosTransactionDAO.getInstance().findTransactionListByGiftCardNumber(cardNumber, fromDate, toDate);
    return findTransactionListByGiftCardNumber;
  }
  
  public void refund(String cardNo, double amount) throws Exception
  {
    if (!isValid(cardNo)) {
      return;
    }
    giftCard.setBalance(Double.valueOf(giftCard.getBalance().doubleValue() + amount));
    GiftCardDAO.getInstance().saveOrUpdate(giftCard);
  }
}
