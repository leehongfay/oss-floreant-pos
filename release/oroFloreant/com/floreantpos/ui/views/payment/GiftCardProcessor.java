package com.floreantpos.ui.views.payment;

import com.floreantpos.model.GiftCard;
import com.floreantpos.model.PosTransaction;
import java.util.Date;
import java.util.List;

public abstract interface GiftCardProcessor
{
  public abstract void chargeAmount(PosTransaction paramPosTransaction)
    throws Exception;
  
  public abstract void addBalance(GiftCard paramGiftCard);
  
  public abstract void refund(String paramString, double paramDouble)
    throws Exception;
  
  public abstract void activate(GiftCard paramGiftCard);
  
  public abstract void deactivate(GiftCard paramGiftCard);
  
  public abstract void disable(GiftCard paramGiftCard);
  
  public abstract void changePinNumber(GiftCard paramGiftCard);
  
  public abstract boolean supportActivation();
  
  public abstract boolean supportDeActivation();
  
  public abstract boolean supportPinNumberChange();
  
  public abstract boolean supportDisable();
  
  public abstract boolean supportAddBalance();
  
  public abstract boolean supportShowTransaction();
  
  public abstract GiftCard getCard(String paramString);
  
  public abstract List<PosTransaction> getTransactionList(String paramString, Date paramDate1, Date paramDate2);
}
