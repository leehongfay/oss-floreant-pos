package com.floreantpos.ui.views.payment;

import com.floreantpos.Messages;
import com.floreantpos.config.CardConfig;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.QwertyKeyPad;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;


















public class ManualGiftCardEntryDialog
  extends POSDialog
  implements CardInputProcessor
{
  private CardInputListener cardInputListener;
  private POSTextField tfCardNumber;
  
  public ManualGiftCardEntryDialog(CardInputListener cardInputListener)
  {
    this.cardInputListener = cardInputListener;
    setDefaultCloseOperation(2);
    setResizable(false);
    
    createUI();
  }
  
  private void createUI()
  {
    setPreferredSize(new Dimension(PosUIManager.getSize(900), PosUIManager.getSize(500)));
    JPanel panel = new JPanel();
    panel.setLayout(new MigLayout("", "[][grow]", "[][][][][][][][grow]"));
    
    JLabel lblCardNumber = new JLabel(Messages.getString("ManualCardEntryDialog.3"));
    panel.add(lblCardNumber, "cell 0 0,alignx trailing");
    
    tfCardNumber = new POSTextField();
    tfCardNumber.setColumns(20);
    panel.add(tfCardNumber, "cell 1 0");
    
    JLabel lblExpieryMonth = new JLabel(Messages.getString("ManualCardEntryDialog.6"));
    panel.add(lblExpieryMonth, "cell 0 1,alignx trailing");
    




    JLabel lblExpieryYear = new JLabel(Messages.getString("ManualCardEntryDialog.9"));
    panel.add(lblExpieryYear, "cell 0 2,alignx trailing");
    




    QwertyKeyPad qwertyKeyPad = new QwertyKeyPad();
    JPanel centerPanel = new JPanel(new BorderLayout());
    centerPanel.setBorder(BorderFactory.createEmptyBorder(5, 3, 5, 5));
    centerPanel.add(panel, "North");
    centerPanel.add(qwertyKeyPad, "Center");
    
    getContentPane().add(centerPanel, "Center");
    
    JPanel panel_1 = new JPanel();
    getContentPane().add(panel_1, "South");
    panel_1.setLayout(new BorderLayout(0, 0));
    
    JPanel panel_2 = new JPanel(new MigLayout("al center"));
    panel_1.add(panel_2);
    
    PosButton btnSwipeCard = new PosButton();
    panel_2.add(btnSwipeCard, "grow");
    btnSwipeCard.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        ManualGiftCardEntryDialog.this.openSwipeCardDialog();
      }
    });
    btnSwipeCard.setText(Messages.getString("ManualCardEntryDialog.13"));
    

    PosButton btnSubmit = new PosButton();
    panel_2.add(btnSubmit, "grow");
    btnSubmit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        submitCard();
      }
    });
    btnSubmit.setText(Messages.getString("ManualCardEntryDialog.15"));
    
    PosButton btnCancel = new PosButton();
    panel_2.add(btnCancel, "grow");
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
    });
    btnCancel.setText(Messages.getString("ManualCardEntryDialog.16"));
    
    JSeparator separator = new JSeparator();
    panel_1.add(separator, "North");
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle(Messages.getString("ManualCardEntryDialog.17"));
    getContentPane().add(titlePanel, "North");
    
    if (!CardConfig.isSwipeCardSupported()) {
      btnSwipeCard.setEnabled(false);
    }
  }
  
  protected void submitCard() {
    setCanceled(false);
    dispose();
    cardInputListener.cardInputted(this, null);
  }
  
  private void openSwipeCardDialog() {
    setCanceled(true);
    dispose();
    
    SwipeGiftCardDialog swipeGiftCardDialog = new SwipeGiftCardDialog(cardInputListener);
    swipeGiftCardDialog.pack();
    swipeGiftCardDialog.open();
  }
  
  public String getCardNumber() {
    return tfCardNumber.getText();
  }
}
