package com.floreantpos.ui.views.payment;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.config.CardConfig;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.extension.InginicoPlugin;
import com.floreantpos.extension.PaymentGatewayPlugin;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.model.CardReader;
import com.floreantpos.model.CashTransaction;
import com.floreantpos.model.CustomPaymentTransaction;
import com.floreantpos.model.GiftCertificateTransaction;
import com.floreantpos.model.Gratuity;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PaymentType;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.services.PosTransactionService;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.TransactionCompletionDialog;
import com.floreantpos.ui.views.TicketDetailView;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.DrawerUtil;
import com.floreantpos.util.NumberUtil;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Font;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.StringUtils;


















public class GroupSettleTicketDialog
  extends POSDialog
  implements CardInputListener
{
  public static final String LOYALTY_DISCOUNT_PERCENTAGE = "loyalty_discount_percentage";
  public static final String LOYALTY_POINT = "loyalty_point";
  public static final String LOYALTY_COUPON = "loyalty_coupon";
  public static final String LOYALTY_DISCOUNT = "loyalty_discount";
  public static final String LOYALTY_ID = "loyalty_id";
  public static final String VIEW_NAME = "PAYMENT_VIEW";
  private GroupPaymentView paymentView;
  private List<Ticket> tickets;
  private TicketDetailView ticketDetailView;
  private JScrollPane ticketScrollPane;
  private double totalTenderAmount;
  private PaymentType paymentType;
  private String cardName;
  private JTextField tfSubtotal;
  private JTextField tfDiscount;
  private JTextField tfDeliveryCharge;
  private JTextField tfTax;
  private JTextField tfTotal;
  private JTextField tfGratuity;
  private String ticketNumbers = "";
  private List<Integer> tableNumbers = new ArrayList();
  
  private String customerName;
  
  private double totalDueAmount;
  
  private JLabel lblCustomer;
  private JLabel lblTable;
  private JLabel labelTicketNumber;
  private JLabel labelTableNumber;
  private JLabel labelCustomer;
  public static PosPaymentWaitDialog waitDialog = new PosPaymentWaitDialog();
  private User currentUser;
  
  public GroupSettleTicketDialog(List<Ticket> tickets, User currentUser)
  {
    this.tickets = tickets;
    this.currentUser = currentUser;
    
    for (Ticket ticket : tickets) {
      if (ticket.getOrderType().isConsolidateItemsInReceipt().booleanValue()) {
        ticket.consolidateTicketItems();
      }
    }
    
    setTitle(Messages.getString("SettleTicketDialog.6"));
    getContentPane().setLayout(new BorderLayout());
    
    ticketDetailView = new TicketDetailView();
    ticketScrollPane = new PosScrollPane(ticketDetailView);
    
    JPanel centerPanel = new JPanel(new BorderLayout(5, 5));
    centerPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 0));
    
    centerPanel.add(createTicketInfoPanel(), "North");
    centerPanel.add(ticketScrollPane, "Center");
    centerPanel.add(createTotalViewerPanel(), "South");
    
    paymentView = new GroupPaymentView(this);
    paymentView.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
    
    getContentPane().add(centerPanel, "Center");
    getContentPane().add(paymentView, "East");
    
    setSize(Application.getPosWindow().getSize());
    updateView();
    paymentView.updateView();
    paymentView.setDefaultFocus();
  }
  
  public void updateView() {
    if ((tickets == null) && (!tickets.isEmpty())) {
      tfSubtotal.setText("");
      tfDiscount.setText("");
      tfDeliveryCharge.setText("");
      tfTax.setText("");
      tfTotal.setText("");
      tfGratuity.setText("");
      return;
    }
    double subtotalAmount = 0.0D;
    double discountAmount = 0.0D;
    double deliveryCharge = 0.0D;
    double taxAmount = 0.0D;
    double gratuityAmount = 0.0D;
    double totalAmount = 0.0D;
    
    for (Ticket ticket : tickets) {
      subtotalAmount += ticket.getSubtotalAmount().doubleValue();
      discountAmount += ticket.getDiscountAmount().doubleValue();
      deliveryCharge += ticket.getDeliveryCharge().doubleValue();
      taxAmount += ticket.getTaxAmount().doubleValue();
      if (ticket.getGratuity() != null) {
        gratuityAmount = ticket.getGratuity().getAmount().doubleValue();
      }
      totalAmount += ticket.getTotalAmountWithTips().doubleValue();
      
      totalDueAmount += ticket.getDueAmount().doubleValue();
      
      ticketNumbers = (ticketNumbers + "[" + (ticket.getId() == null ? "New Ticket" : ticket.getId()) + "], ");
      List<Integer> tableNumbers = ticket.getTableNumbers();
      if ((tableNumbers != null) && (tableNumbers.size() > 0)) {
        for (Integer tableNumber : tableNumbers) {
          if (!tableNumbers.contains(tableNumber)) {
            tableNumbers.add(tableNumber);
          }
        }
      }
      customerName = ticket.getProperty("CUSTOMER_NAME");
    }
    
    tfSubtotal.setText(NumberUtil.formatNumber(Double.valueOf(subtotalAmount)));
    tfDiscount.setText(NumberUtil.formatNumber(Double.valueOf(discountAmount)));
    
    tfDeliveryCharge.setText(NumberUtil.formatNumber(Double.valueOf(deliveryCharge)));
    
    if (Application.getInstance().isPriceIncludesTax()) {
      tfTax.setText(Messages.getString("TicketView.35"));
    }
    else {
      tfTax.setText(NumberUtil.formatNumber(Double.valueOf(taxAmount)));
    }
    if (gratuityAmount > 0.0D) {
      tfGratuity.setText(NumberUtil.formatNumber(Double.valueOf(gratuityAmount)));
    }
    else {
      tfGratuity.setText("0.00");
    }
    tfTotal.setText(NumberUtil.formatNumber(Double.valueOf(totalAmount)));
    
    labelTicketNumber.setText(ticketNumbers.substring(0, ticketNumbers.length() - 2));
    labelTableNumber.setText(this.tableNumbers.toString());
    
    if (this.tableNumbers.isEmpty()) {
      labelTableNumber.setVisible(false);
      lblTable.setVisible(false);
    }
    
    labelCustomer.setText(customerName);
    
    if (customerName == null) {
      labelCustomer.setVisible(false);
      lblCustomer.setVisible(false);
    }
    
    ticketDetailView.setTickets(tickets);
  }
  

  private JPanel createTicketInfoPanel()
  {
    JLabel lblTicket = new JLabel();
    lblTicket.setText(Messages.getString("SettleTicketDialog.0"));
    
    labelTicketNumber = new JLabel();
    
    lblTable = new JLabel();
    lblTable.setText(POSConstants.TABLES);
    
    labelTableNumber = new JLabel();
    
    lblCustomer = new JLabel();
    lblCustomer.setText("Customer:");
    
    labelCustomer = new JLabel();
    
    JPanel ticketInfoPanel = new TransparentPanel(new MigLayout("wrap 2,fill, hidemode 3", "[][grow]", ""));
    
    ticketInfoPanel.add(lblTicket);
    ticketInfoPanel.add(labelTicketNumber, "grow");
    ticketInfoPanel.add(lblTable);
    ticketInfoPanel.add(labelTableNumber, "grow");
    ticketInfoPanel.add(lblCustomer);
    ticketInfoPanel.add(labelCustomer, "grow");
    
    return ticketInfoPanel;
  }
  
  private JPanel createTotalViewerPanel()
  {
    JLabel lblSubtotal = new JLabel();
    lblSubtotal.setHorizontalAlignment(4);
    lblSubtotal.setText(POSConstants.SUBTOTAL + ":" + " " + CurrencyUtil.getCurrencySymbol());
    
    tfSubtotal = new JTextField(10);
    tfSubtotal.setHorizontalAlignment(11);
    tfSubtotal.setEditable(false);
    
    JLabel lblDiscount = new JLabel();
    lblDiscount.setHorizontalAlignment(4);
    lblDiscount.setText(Messages.getString("TicketView.9") + " " + CurrencyUtil.getCurrencySymbol());
    
    tfDiscount = new JTextField(10);
    tfDiscount.setHorizontalAlignment(11);
    tfDiscount.setEditable(false);
    
    JLabel lblDeliveryCharge = new JLabel();
    lblDeliveryCharge.setHorizontalAlignment(4);
    lblDeliveryCharge.setText("Delivery Charge: " + CurrencyUtil.getCurrencySymbol());
    
    tfDeliveryCharge = new JTextField(10);
    tfDeliveryCharge.setHorizontalAlignment(11);
    tfDeliveryCharge.setEditable(false);
    
    JLabel lblTax = new JLabel();
    lblTax.setHorizontalAlignment(4);
    lblTax.setText(POSConstants.TAX + ":" + " " + CurrencyUtil.getCurrencySymbol());
    
    tfTax = new JTextField();
    tfTax.setEditable(false);
    tfTax.setHorizontalAlignment(11);
    
    JLabel lblGratuity = new JLabel();
    lblGratuity.setHorizontalAlignment(4);
    lblGratuity.setText(Messages.getString("SettleTicketDialog.5") + ":" + " " + CurrencyUtil.getCurrencySymbol());
    
    tfGratuity = new JTextField();
    tfGratuity.setEditable(false);
    tfGratuity.setHorizontalAlignment(11);
    
    JLabel lblTotal = new JLabel();
    lblTotal.setFont(lblTotal.getFont().deriveFont(1, 18.0F));
    lblTotal.setHorizontalAlignment(4);
    lblTotal.setText(POSConstants.TOTAL + ":" + " " + CurrencyUtil.getCurrencySymbol());
    
    tfTotal = new JTextField(10);
    tfTotal.setFont(tfTotal.getFont().deriveFont(1, 18.0F));
    tfTotal.setHorizontalAlignment(11);
    tfTotal.setEditable(false);
    
    JPanel ticketAmountPanel = new TransparentPanel(new MigLayout("hidemode 3,ins 2 2 3 2,alignx trailing,fill", "[grow][]", ""));
    
    ticketAmountPanel.add(lblSubtotal, "growx,aligny center");
    ticketAmountPanel.add(tfSubtotal, "growx,aligny center");
    ticketAmountPanel.add(lblDiscount, "newline,growx,aligny center");
    ticketAmountPanel.add(tfDiscount, "growx,aligny center");
    ticketAmountPanel.add(lblTax, "newline,growx,aligny center");
    ticketAmountPanel.add(tfTax, "growx,aligny center");
    ticketAmountPanel.add(lblDeliveryCharge, "newline,growx,aligny center");
    ticketAmountPanel.add(tfDeliveryCharge, "growx,aligny center");
    ticketAmountPanel.add(lblGratuity, "newline,growx,aligny center");
    ticketAmountPanel.add(tfGratuity, "growx,aligny center");
    ticketAmountPanel.add(lblTotal, "newline,growx,aligny center");
    ticketAmountPanel.add(tfTotal, "growx,aligny center");
    
    return ticketAmountPanel;
  }
  
  public void doGroupSettle(PaymentType paymentType) {
    try {
      if (tickets == null)
        return;
      this.paymentType = paymentType;
      totalTenderAmount = paymentView.getTenderedAmount();
      totalDueAmount = NumberUtil.roundToTwoDigit(totalDueAmount);
      
      if (totalTenderAmount < totalDueAmount) {
        POSMessageDialog.showMessage("Partial payment not allowed.");
        return;
      }
      





      cardName = paymentType.getDisplayString();
      PosTransaction transaction = null;
      
      switch (1.$SwitchMap$com$floreantpos$model$PaymentType[paymentType.ordinal()]) {
      case 1: 
        if (!confirmPayment()) {
          return;
        }
        
        transaction = paymentType.createTransaction();
        transaction.setCaptured(Boolean.valueOf(true));
        
        settleTicket(transaction);
        break;
      

      case 2: 
        CustomPaymentSelectionDialog customPaymentDialog = new CustomPaymentSelectionDialog();
        customPaymentDialog.setTitle(Messages.getString("SettleTicketDialog.8"));
        customPaymentDialog.pack();
        customPaymentDialog.open();
        
        if (customPaymentDialog.isCanceled()) {
          return;
        }
        if (!confirmPayment()) {
          return;
        }
        
        transaction = paymentType.createTransaction();
        CustomPaymentTransaction customPaymentTransaction = (CustomPaymentTransaction)transaction;
        customPaymentTransaction.setCustomPaymentFieldName(customPaymentDialog.getPaymentFieldName());
        customPaymentTransaction.setCustomPaymentName(customPaymentDialog.getPaymentName());
        customPaymentTransaction.setCustomPaymentRef(customPaymentDialog.getPaymentRef());
        customPaymentTransaction.setCaptured(Boolean.valueOf(true));
        settleTicket(customPaymentTransaction);
        break;
      
      case 3: 
      case 4: 
      case 5: 
      case 6: 
      case 7: 
        payUsingCard(cardName, totalTenderAmount);
        break;
      
      case 8: 
      case 9: 
        payUsingCard(cardName, totalTenderAmount);
        break;
      
      case 10: 
        GiftCertDialog giftCertDialog = new GiftCertDialog(this);
        giftCertDialog.pack();
        giftCertDialog.open();
        
        if (giftCertDialog.isCanceled()) {
          return;
        }
        transaction = new GiftCertificateTransaction();
        transaction.setPaymentType(PaymentType.GIFT_CERTIFICATE.name());
        transaction.setCaptured(Boolean.valueOf(true));
        
        double giftCertFaceValue = giftCertDialog.getGiftCertFaceValue();
        double giftCertCashBackAmount = 0.0D;
        transaction.setTenderAmount(Double.valueOf(giftCertFaceValue));
        
        if (giftCertFaceValue >= totalDueAmount) {
          transaction.setAmount(Double.valueOf(totalDueAmount));
          giftCertCashBackAmount = giftCertFaceValue - totalDueAmount;
        }
        else {
          transaction.setAmount(Double.valueOf(giftCertFaceValue));
        }
        
        transaction.setGiftCertNumber(giftCertDialog.getGiftCertNumber());
        transaction.setGiftCertFaceValue(Double.valueOf(giftCertFaceValue));
        transaction.setGiftCertPaidAmount(transaction.getAmount());
        transaction.setGiftCertCashBackAmount(Double.valueOf(giftCertCashBackAmount));
        
        settleTicket(transaction);
      

      }
      
    }
    catch (Exception e)
    {
      PosLog.error(getClass(), e);
    }
  }
  
  private boolean confirmPayment() {
    if (!TerminalConfig.isUseSettlementPrompt()) {
      return true;
    }
    
    ConfirmPayDialog confirmPayDialog = new ConfirmPayDialog();
    confirmPayDialog.setAmount(totalTenderAmount);
    confirmPayDialog.open();
    
    if (confirmPayDialog.isCanceled()) {
      return false;
    }
    
    return true;
  }
  
  public void settleTicket(PosTransaction posTransaction) {
    try {
      List<PosTransaction> transactionList = new ArrayList();
      totalTenderAmount = paymentView.getTenderedAmount();
      
      for (Ticket ticket : tickets) {
        PosTransaction transaction = null;
        if (totalTenderAmount <= 0.0D) {
          break;
        }
        
        transaction = (PosTransaction)SerializationUtils.clone(posTransaction);
        
        transaction.setTicket(ticket);
        setTransactionAmounts(transaction);
        
        PosTransactionService transactionService = PosTransactionService.getInstance();
        transactionService.settleTicket(ticket, transaction, currentUser);
        
        transactionList.add(transaction);
        if (TerminalConfig.isAutoPrintReceipt()) {
          printTicket(ticket, transaction);
        }
      }
      
      showTransactionCompleteMsg(totalDueAmount, totalTenderAmount, tickets, transactionList);
      
      setCanceled(false);
      dispose();
    }
    catch (UnknownHostException e) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("SettleTicketDialog.12"));
    } catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  public void showTransactionCompleteMsg(double dueAmount, double tenderedAmount, List<Ticket> tickets, List<PosTransaction> transactions) {
    TransactionCompletionDialog dialog = new TransactionCompletionDialog(transactions);
    dialog.setTickets(tickets);
    
    double paidAmount = 0.0D;
    double ticketsDueAmount = 0.0D;
    for (PosTransaction transaction : transactions) {
      paidAmount += transaction.getAmount().doubleValue();
      dialog.setCard(transaction.isCard());
    }
    dialog.setTenderedAmount(tenderedAmount);
    dialog.setTotalAmount(dueAmount);
    dialog.setPaidAmount(paidAmount);
    for (Ticket tTicket : tickets) {
      ticketsDueAmount += tTicket.getDueAmount().doubleValue();
    }
    dialog.setDueAmount(ticketsDueAmount);
    if (tenderedAmount > paidAmount) {
      dialog.setChangeAmount(tenderedAmount - paidAmount);
    }
    else {
      dialog.setChangeAmount(0.0D);
    }
    
    dialog.updateView();
    dialog.pack();
    dialog.open();
  }
  
  public static void printTicket(Ticket ticket, PosTransaction transaction) {
    try {
      if (ticket.needsKitchenPrint()) {
        ReceiptPrintService.printToKitchen(ticket);
      }
      
      ReceiptPrintService.printTransaction(transaction);
      
      if ((transaction instanceof CashTransaction)) {
        DrawerUtil.kickDrawer();
      }
    } catch (Exception ee) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.PRINT_ERROR, ee);
    }
  }
  
  private void payUsingCard(String cardName, double tenderedAmount) throws Exception {
    try {
      PaymentGatewayPlugin paymentGateway = CardConfig.getPaymentGateway();
      
      if ((paymentGateway instanceof InginicoPlugin)) {
        waitDialog.setVisible(true);
        if (!waitDialog.isCanceled()) {
          dispose();
        }
        return;
      }
      if (!paymentGateway.shouldShowCardInputProcessor())
      {
        PosTransaction transaction = paymentType.createTransaction();
        
        if (!confirmPayment()) {
          return;
        }
        Ticket ticket = (Ticket)tickets.get(0);
        transaction.setTenderAmount(Double.valueOf(totalTenderAmount));
        if (totalTenderAmount >= getDueAmount()) {
          transaction.setAmount(Double.valueOf(getDueAmount()));
        }
        else {
          transaction.setAmount(Double.valueOf(totalTenderAmount));
        }
        transaction.setTicket(ticket);
        
        transaction.setCaptured(Boolean.valueOf(false));
        transaction.setCardMerchantGateway(paymentGateway.getProductName());
        
        if (ticket.getOrderType().isPreAuthCreditCard().booleanValue()) {
          paymentGateway.getProcessor().preAuth(transaction);
        }
        else {
          paymentGateway.getProcessor().chargeAmount(transaction);
        }
        
        settleTicket(transaction);
        
        return;
      }
      
      CardReader cardReader = CardConfig.getCardReader();
      switch (1.$SwitchMap$com$floreantpos$model$CardReader[cardReader.ordinal()]) {
      case 1: 
        SwipeCardDialog swipeCardDialog = new SwipeCardDialog(this);
        swipeCardDialog.pack();
        swipeCardDialog.open();
        break;
      
      case 2: 
        ManualCardEntryDialog dialog = new ManualCardEntryDialog(this);
        dialog.pack();
        dialog.open();
        break;
      
      case 3: 
        AuthorizationCodeDialog authorizationCodeDialog = new AuthorizationCodeDialog(this);
        authorizationCodeDialog.pack();
        authorizationCodeDialog.open();
      
      }
      
    }
    catch (Exception e)
    {
      POSMessageDialog.showError(this, e.getMessage(), e);
    }
  }
  

  public void open()
  {
    super.open();
  }
  

  public void cardInputted(CardInputProcessor inputter, PaymentType selectedPaymentType)
  {
    PaymentProcessWaitDialog waitDialog = new PaymentProcessWaitDialog(this);
    try
    {
      waitDialog.setVisible(true);
      
      PosTransaction transaction = paymentType.createTransaction();
      
      PaymentGatewayPlugin paymentGateway = CardConfig.getPaymentGateway();
      CardProcessor cardProcessor = paymentGateway.getProcessor();
      
      if ((inputter instanceof SwipeCardDialog))
      {
        SwipeCardDialog swipeCardDialog = (SwipeCardDialog)inputter;
        String cardString = swipeCardDialog.getCardString();
        
        if ((StringUtils.isEmpty(cardString)) || (cardString.length() < 16)) {
          throw new RuntimeException(Messages.getString("SettleTicketDialog.16"));
        }
        
        if (!confirmPayment()) {
          return;
        }
        transaction.setCardType(paymentType.getDisplayString());
        transaction.setCardTrack(cardString);
        transaction.setCaptured(Boolean.valueOf(false));
        transaction.setCardMerchantGateway(paymentGateway.getProductName());
        transaction.setCardReader(CardReader.SWIPE.name());
        
        settleTicket(cardProcessor, transaction);
      }
      else if ((inputter instanceof ManualCardEntryDialog))
      {
        ManualCardEntryDialog mDialog = (ManualCardEntryDialog)inputter;
        
        transaction.setCaptured(Boolean.valueOf(false));
        transaction.setCardMerchantGateway(paymentGateway.getProductName());
        transaction.setCardReader(CardReader.MANUAL.name());
        transaction.setCardNumber(mDialog.getCardNumber());
        transaction.setCardExpMonth(mDialog.getExpMonth());
        transaction.setCardExpYear(mDialog.getExpYear());
        
        settleTicket(cardProcessor, transaction);
      }
      else if ((inputter instanceof AuthorizationCodeDialog))
      {
        PosTransaction selectedTransaction = selectedPaymentType.createTransaction();
        
        AuthorizationCodeDialog authDialog = (AuthorizationCodeDialog)inputter;
        String authorizationCode = authDialog.getAuthorizationCode();
        if (StringUtils.isEmpty(authorizationCode)) {
          throw new PosException(Messages.getString("SettleTicketDialog.17"));
        }
        
        selectedTransaction.setCardType(selectedPaymentType.getDisplayString());
        selectedTransaction.setCaptured(Boolean.valueOf(true));
        selectedTransaction.setAuthorizable(Boolean.valueOf(false));
        selectedTransaction.setCardReader(CardReader.EXTERNAL_TERMINAL.name());
        selectedTransaction.setCardAuthCode(authorizationCode);
        
        settleTicket(selectedTransaction);
      }
    } catch (Exception e) {
      PosLog.error(getClass(), e);
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    } finally {
      waitDialog.setVisible(false);
    }
  }
  
  private void settleTicket(CardProcessor cardProcessor, PosTransaction transaction) {
    try {
      List<PosTransaction> transactionList = new ArrayList();
      totalTenderAmount = paymentView.getTenderedAmount();
      
      for (Ticket ticket : tickets) {
        PosTransaction cardTransaction = new PosTransaction();
        if (totalTenderAmount <= 0.0D) {
          break;
        }
        
        cardTransaction = (PosTransaction)SerializationUtils.clone(transaction);
        cardTransaction.setId(null);
        
        cardTransaction.setTicket(ticket);
        setTransactionAmounts(cardTransaction);
        
        if (ticket.getOrderType().isPreAuthCreditCard().booleanValue()) {
          cardProcessor.preAuth(transaction);
        }
        else {
          cardProcessor.chargeAmount(transaction);
        }
        
        PosTransactionService transactionService = PosTransactionService.getInstance();
        transactionService.settleTicket(ticket, cardTransaction, currentUser);
        
        transactionList.add(cardTransaction);
        printTicket(ticket, cardTransaction);
      }
      

      showTransactionCompleteMsg(totalDueAmount, totalTenderAmount, tickets, transactionList);
      
      setCanceled(false);
      dispose();
    } catch (UnknownHostException e) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("SettleTicketDialog.12"));
    } catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  private void setTransactionAmounts(PosTransaction transaction) {
    if (((Ticket)tickets.get(tickets.size() - 1)).getId() == transaction.getTicket().getId()) {
      if (totalTenderAmount > totalDueAmount) {
        transaction.setTenderAmount(Double.valueOf(totalTenderAmount - totalDueAmount + transaction.getTicket().getDueAmount().doubleValue()));
        transaction.setAmount(transaction.getTicket().getDueAmount());
      }
      else {
        transaction.setTenderAmount(transaction.getTicket().getDueAmount());
        transaction.setAmount(transaction.getTicket().getDueAmount());
      }
      String ticketNumbers = "";
      for (Ticket ticket : tickets) {
        ticketNumbers = ticketNumbers + "[" + ticket.getId() + "]";
      }
      ticketNumbers = ticketNumbers.substring(0, ticketNumbers.length() > 215 ? 215 : ticketNumbers.length());
      ticketNumbers = ticketNumbers + (ticketNumbers.length() == 215 ? "...." : "");
      transaction.getTicket().addProperty("GROUP_SETTLE_TICKETS", "#CHK " + ticketNumbers);
    }
    else {
      transaction.setTenderAmount(transaction.getTicket().getDueAmount());
      transaction.setAmount(transaction.getTicket().getDueAmount());
    }
  }
  
  public List<Ticket> getTickets() {
    return tickets;
  }
  
  public double getDueAmount() {
    return totalDueAmount;
  }
}
