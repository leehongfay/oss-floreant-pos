package com.floreantpos.ui.views.payment;

import com.floreantpos.Messages;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.QwertyKeyPad;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.Container;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;





















public class GiftCertDialog
  extends OkCancelOptionDialog
{
  private FixedLengthTextField tfGiftCertNumber;
  private DoubleTextField tfFaceValue;
  private QwertyKeyPad qwertyKeyPad;
  
  public GiftCertDialog(JDialog parent)
  {
    setTitle(Messages.getString("GiftCertDialog.0"));
    setCaption(Messages.getString("GiftCertDialog.1"));
    
    JPanel panel = getContentPanel();
    getContentPane().add(panel, "Center");
    panel.setLayout(new MigLayout("", "[][grow]", "[][]"));
    
    JLabel lblGiftCertificateNumber = new JLabel(Messages.getString("GiftCertDialog.5"));
    panel.add(lblGiftCertificateNumber, "cell 0 0,alignx trailing");
    
    tfGiftCertNumber = new FixedLengthTextField();
    tfGiftCertNumber.setLength(64);
    panel.add(tfGiftCertNumber, "cell 1 0,growx");
    
    JLabel lblFaceValue = new JLabel(Messages.getString("GiftCertDialog.8"));
    panel.add(lblFaceValue, "cell 0 1,alignx trailing");
    
    tfFaceValue = new DoubleTextField();
    tfFaceValue.setText("50");
    panel.add(tfFaceValue, "cell 1 1,growx");
    
    qwertyKeyPad = new QwertyKeyPad();
    panel.add(qwertyKeyPad, "newline, gaptop 10px, span");
  }
  
  public void doOk()
  {
    if (StringUtils.isEmpty(getGiftCertNumber())) {
      POSMessageDialog.showMessage(Messages.getString("GiftCertDialog.14"));
      return;
    }
    
    if (getGiftCertFaceValue() <= 0.0D) {
      POSMessageDialog.showMessage(Messages.getString("GiftCertDialog.15"));
      return;
    }
    
    setCanceled(false);
    dispose();
  }
  
  public String getGiftCertNumber() {
    return tfGiftCertNumber.getText();
  }
  
  public double getGiftCertFaceValue() {
    return tfFaceValue.getDouble();
  }
}
