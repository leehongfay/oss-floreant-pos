package com.floreantpos.ui.views.payment;

import com.floreantpos.model.PosTransaction;

public abstract interface CardProcessor
{
  public abstract void preAuth(PosTransaction paramPosTransaction)
    throws Exception;
  
  public abstract void captureAuthAmount(PosTransaction paramPosTransaction)
    throws Exception;
  
  public abstract void chargeAmount(PosTransaction paramPosTransaction)
    throws Exception;
  
  public abstract void voidTransaction(PosTransaction paramPosTransaction)
    throws Exception;
  
  public abstract void refundTransaction(PosTransaction paramPosTransaction, double paramDouble)
    throws Exception;
  
  public abstract String getCardInformationForReceipt(PosTransaction paramPosTransaction);
  
  public abstract void cancelTransaction();
}
