package com.floreantpos.ui.views.payment;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.CashBreakdown;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.Currency;
import com.floreantpos.model.PaymentType;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.MultiCurrencyTenderDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;






















public class GroupPaymentView
  extends JPanel
{
  private static final String ZERO = "0";
  private static final String REMOVE = "1";
  protected GroupSettleTicketDialog groupSettleTicketView;
  private PosButton btnCancel;
  private PosButton btnCash;
  private PosButton btnCreditCard;
  private PosButton btnGift;
  private PosButton btnOther;
  private TransparentPanel calcButtonPanel;
  private JLabel labelDueAmount;
  private JLabel labelTenderedAmount;
  private TransparentPanel actionButtonPanel;
  private PosButton btn7;
  private PosButton btnDot;
  private PosButton btn0;
  private PosButton btnClear;
  private PosButton btn8;
  private PosButton btn9;
  private PosButton btn4;
  private PosButton btn5;
  private PosButton btn6;
  private PosButton btn3;
  private PosButton btn2;
  private PosButton btn1;
  private PosButton btn00;
  private PosButton btnNextAmount;
  private PosButton btnAmount1;
  private PosButton btnAmount2;
  private PosButton btnAmount5;
  private PosButton btnAmount10;
  private PosButton btnAmount20;
  private PosButton btnAmount50;
  private PosButton btnAmount100;
  private PosButton btnExactAmount;
  private JTextField txtTenderedAmount;
  private JTextField txtDueAmount;
  private boolean clearPreviousAmount = true;
  
  public GroupPaymentView(GroupSettleTicketDialog groupSettleTicketView) {
    this.groupSettleTicketView = groupSettleTicketView;
    
    initComponents();
  }
  
  private void initComponents() {
    setLayout(new MigLayout("fill,hidemode 3", "[grow][grow]", ""));
    
    JPanel centerPanel = new JPanel(new BorderLayout(5, 5));
    
    TransparentPanel transparentPanel1 = new TransparentPanel(new BorderLayout());
    
    labelDueAmount = new JLabel();
    labelTenderedAmount = new JLabel();
    txtDueAmount = new JTextField();
    txtTenderedAmount = new JTextField();
    
    Font font1 = new Font("Tahoma", 1, PosUIManager.getFontSize(20));
    Font font2 = new Font("Arial", 0, PosUIManager.getFontSize(34));
    
    labelTenderedAmount.setFont(font1);
    labelTenderedAmount.setText(Messages.getString("PaymentView.54") + " " + CurrencyUtil.getCurrencySymbol());
    labelTenderedAmount.setForeground(Color.gray);
    
    txtTenderedAmount.setHorizontalAlignment(4);
    txtTenderedAmount.setFont(font1);
    
    labelDueAmount.setFont(font1);
    labelDueAmount.setText(Messages.getString("PaymentView.52") + " " + CurrencyUtil.getCurrencySymbol());
    labelDueAmount.setForeground(Color.gray);
    
    txtDueAmount.setFont(font1);
    txtDueAmount.setEditable(false);
    txtDueAmount.setHorizontalAlignment(4);
    
    transparentPanel1.setLayout(new MigLayout("", "[][grow,fill]", "[19px][][19px]"));
    transparentPanel1.add(labelDueAmount, "cell 0 0,alignx right,aligny center");
    
    transparentPanel1.add(labelTenderedAmount, "cell 0 2,alignx left,aligny center");
    transparentPanel1.add(txtDueAmount, "cell 1 0,growx,aligny top");
    transparentPanel1.add(txtTenderedAmount, "cell 1 2,growx,aligny top");
    
    centerPanel.add(transparentPanel1, "North");
    
    calcButtonPanel = new TransparentPanel();
    calcButtonPanel.setLayout(new MigLayout("wrap 4,fill, ins 0", "sg, fill", "sg, fill"));
    
    btnNextAmount = new PosButton();
    btnAmount1 = new PosButton();
    btnAmount1.setFont(font2);
    
    btnAmount2 = new PosButton();
    btnAmount2.setFont(font2);
    
    btnAmount5 = new PosButton();
    btnAmount5.setFont(font2);
    
    btnAmount10 = new PosButton();
    btnAmount10.setFont(font2);
    
    btnAmount20 = new PosButton();
    btnAmount20.setFont(font2);
    
    btnAmount50 = new PosButton();
    btnAmount50.setFont(font2);
    
    btnAmount100 = new PosButton();
    btnAmount100.setFont(font2);
    
    btnExactAmount = new PosButton();
    
    btn7 = new PosButton();
    btn8 = new PosButton();
    btn9 = new PosButton();
    btn4 = new PosButton();
    btn5 = new PosButton();
    btn6 = new PosButton();
    btn1 = new PosButton();
    btn2 = new PosButton();
    btn3 = new PosButton();
    btn0 = new PosButton();
    btnDot = new PosButton();
    btnClear = new PosButton();
    btn00 = new PosButton();
    
    btnAmount1.setForeground(Color.blue);
    btnAmount1.setAction(nextButtonAction);
    btnAmount1.setText(Messages.getString("PaymentView.1"));
    btnAmount1.setActionCommand("1");
    btnAmount1.setFocusable(false);
    calcButtonPanel.add(btnAmount1);
    
    btn7.setAction(calAction);
    btn7.setText("7");
    btn7.setFont(font2);
    
    btn7.setActionCommand("7");
    btn7.setFocusable(false);
    calcButtonPanel.add(btn7);
    
    btn8.setAction(calAction);
    btn8.setText("8");
    btn8.setFont(font2);
    
    btn8.setActionCommand("8");
    btn8.setFocusable(false);
    calcButtonPanel.add(btn8);
    
    btn9.setAction(calAction);
    btn9.setText("9");
    btn9.setFont(font2);
    
    btn9.setActionCommand("9");
    btn9.setFocusable(false);
    calcButtonPanel.add(btn9);
    
    btnAmount2.setForeground(Color.blue);
    btnAmount2.setAction(nextButtonAction);
    btnAmount2.setText(Messages.getString("PaymentView.10"));
    btnAmount2.setActionCommand("2");
    btnAmount2.setFocusable(false);
    
    calcButtonPanel.add(btnAmount2);
    
    btn4.setAction(calAction);
    btn4.setText("4");
    btn4.setFont(font2);
    
    btn4.setActionCommand("4");
    btn4.setFocusable(false);
    calcButtonPanel.add(btn4);
    
    btn5.setAction(calAction);
    btn5.setText("5");
    btn5.setFont(font2);
    
    btn5.setActionCommand("5");
    btn5.setFocusable(false);
    calcButtonPanel.add(btn5);
    
    btn6.setAction(calAction);
    btn6.setText("6");
    btn6.setFont(font2);
    
    btn6.setActionCommand("6");
    btn6.setFocusable(false);
    calcButtonPanel.add(btn6);
    
    btnAmount5.setForeground(Color.blue);
    btnAmount5.setAction(nextButtonAction);
    btnAmount5.setText(Messages.getString("PaymentView.12"));
    btnAmount5.setActionCommand("5");
    btnAmount5.setFocusable(false);
    
    calcButtonPanel.add(btnAmount5);
    
    btn1.setAction(calAction);
    btn1.setText("1");
    btn1.setFont(font2);
    
    btn1.setActionCommand("1");
    btn1.setFocusable(false);
    calcButtonPanel.add(btn1);
    
    btn2.setAction(calAction);
    btn2.setText("2");
    btn2.setFont(font2);
    
    btn2.setActionCommand("2");
    btn2.setFocusable(false);
    calcButtonPanel.add(btn2);
    
    btn3.setAction(calAction);
    btn3.setText("3");
    btn3.setFont(font2);
    
    btn3.setActionCommand("3");
    btn3.setFocusable(false);
    calcButtonPanel.add(btn3);
    
    btnAmount10.setForeground(Color.blue);
    btnAmount10.setAction(nextButtonAction);
    btnAmount10.setText(Messages.getString("PaymentView.14"));
    btnAmount10.setActionCommand("10");
    btnAmount10.setFocusable(false);
    calcButtonPanel.add(btnAmount10, "grow");
    
    btn0.setAction(calAction);
    btn0.setText("0");
    btn0.setFont(font2);
    
    btn0.setActionCommand("0");
    btn0.setFocusable(false);
    calcButtonPanel.add(btn0);
    
    btn00.setFont(new Font("Arial", 0, 30));
    btn00.setAction(calAction);
    btn00.setText(Messages.getString("PaymentView.18"));
    btn00.setActionCommand("00");
    btn00.setFocusable(false);
    calcButtonPanel.add(btn00);
    
    btnDot.setAction(calAction);
    btnDot.setIcon(IconFactory.getIcon("/ui_icons/", "dot.png"));
    btnDot.setActionCommand(".");
    btnDot.setFocusable(false);
    calcButtonPanel.add(btnDot);
    
    btnAmount20.setForeground(Color.BLUE);
    btnAmount20.setAction(nextButtonAction);
    btnAmount20.setText("20");
    btnAmount20.setActionCommand("20");
    btnAmount20.setFocusable(false);
    calcButtonPanel.add(btnAmount20, "grow");
    
    btnAmount50.setForeground(Color.blue);
    btnAmount50.setAction(nextButtonAction);
    btnAmount50.setText("50");
    btnAmount50.setActionCommand("50");
    btnAmount50.setFocusable(false);
    calcButtonPanel.add(btnAmount50, "grow");
    
    btnAmount100.setForeground(Color.blue);
    btnAmount100.setAction(nextButtonAction);
    btnAmount100.setText("100");
    btnAmount100.setActionCommand("100");
    btnAmount100.setFocusable(false);
    calcButtonPanel.add(btnAmount100, "grow");
    
    btnClear.setAction(calAction);
    btnClear.setIcon(IconFactory.getIcon("/ui_icons/", "clear.png"));
    btnClear.setText(Messages.getString("PaymentView.38"));
    btnClear.setFocusable(false);
    calcButtonPanel.add(btnClear);
    
    btnExactAmount.setAction(nextButtonAction);
    btnExactAmount.setText(Messages.getString("PaymentView.20"));
    btnExactAmount.setActionCommand("exactAmount");
    btnExactAmount.setFocusable(false);
    calcButtonPanel.add(btnExactAmount, "span 2,grow");
    
    btnNextAmount.setAction(nextButtonAction);
    btnNextAmount.setText(Messages.getString("PaymentView.23"));
    btnNextAmount.setActionCommand("nextAmount");
    btnNextAmount.setFocusable(false);
    calcButtonPanel.add(btnNextAmount, "span 2,grow");
    
    PosButton btnPrint = new PosButton(POSConstants.PRINT_TICKET);
    btnPrint.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        for (Ticket ticket : groupSettleTicketView.getTickets()) {
          ReceiptPrintService.printTicket(ticket);
        }
        
      }
    });
    calcButtonPanel.add(btnPrint, "span 4,grow");
    
    centerPanel.add(calcButtonPanel, "Center");
    
    actionButtonPanel = new TransparentPanel();
    actionButtonPanel.setLayout(new MigLayout("wrap 1,hidemode 3, ins 0 20 0 0, fill", "sg, fill", ""));
    

    int width = PosUIManager.getSize(160);
    
    btnCash = new PosButton(Messages.getString("PaymentView.31"));
    actionButtonPanel.add(btnCash, "grow,w " + width + "!");
    btnCash.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        try {
          double x = NumberUtil.parse(txtTenderedAmount.getText()).doubleValue();
          if (x <= 0.0D) {
            POSMessageDialog.showError(Messages.getString("PaymentView.32"));
            return;
          }
          if (Application.getInstance().getTerminal().isEnableMultiCurrency().booleanValue()) {
            User currentUser = Application.getCurrentUser();
            CashDrawer cashDrawer = currentUser.getActiveDrawerPullReport();
            Currency mainCurrency = CurrencyUtil.getMainCurrency();
            CashBreakdown item = cashDrawer.getCurrencyBalance(mainCurrency);
            if (item == null) {
              item = new CashBreakdown();
              item.setCurrency(mainCurrency);
              cashDrawer.addTocashBreakdownList(item);
              item.setCashDrawer(cashDrawer);
            }
            double amount = 0.0D;
            if (x >= groupSettleTicketView.getDueAmount()) {
              amount = groupSettleTicketView.getDueAmount();
            }
            else {
              amount = x;
            }
            item.setBalance(Double.valueOf(NumberUtil.roundToThreeDigit(item.getBalance().doubleValue() + amount)));
          }
          groupSettleTicketView.doGroupSettle(PaymentType.CASH);
        } catch (Exception e) {
          LogFactory.getLog(getClass()).error(e);
        }
        
      }
    });
    PosButton btnMultiCurrencyCash = new PosButton("MULTI CURRENCY CASH");
    actionButtonPanel.add(btnMultiCurrencyCash, "grow,w " + width + "!");
    btnMultiCurrencyCash.setVisible(Application.getInstance().getTerminal().isEnableMultiCurrency().booleanValue());
    btnMultiCurrencyCash.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        try {
          List<Currency> currencyList = CurrencyUtil.getAllCurrency();
          if ((currencyList.size() > 1) && 
            (!adjustCashDrawerBalance(currencyList))) {
            return;
          }
          
          double x = NumberUtil.parse(txtTenderedAmount.getText()).doubleValue();
          
          if (x <= 0.0D) {
            POSMessageDialog.showError(Messages.getString("PaymentView.32"));
            return;
          }
          groupSettleTicketView.doGroupSettle(PaymentType.CASH);
        } catch (Exception e) {
          LogFactory.getLog(getClass()).error(e);
        }
        
      }
    });
    btnCreditCard = new PosButton(Messages.getString("PaymentView.33"));
    actionButtonPanel.add(btnCreditCard, "grow,w " + width + "!");
    btnCreditCard.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        groupSettleTicketView.doGroupSettle(PaymentType.CREDIT_CARD);
      }
      
    });
    btnGift = new PosButton(Messages.getString("PaymentView.35"));
    actionButtonPanel.add(btnGift, "grow,w " + width + "!");
    btnGift.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        groupSettleTicketView.doGroupSettle(PaymentType.GIFT_CERTIFICATE);
      }
      
    });
    btnOther = new PosButton("OTHER");
    actionButtonPanel.add(btnOther, "grow,w " + width + "!");
    btnOther.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        groupSettleTicketView.doGroupSettle(PaymentType.CUSTOM_PAYMENT);
      }
      
    });
    btnCancel = new PosButton(POSConstants.CANCEL.toUpperCase());
    actionButtonPanel.add(btnCancel, "grow,w " + width + "!");
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        GroupPaymentView.this.btnCancelActionPerformed(evt);

      }
      

    });
    add(centerPanel, "cell 0 0,grow");
    add(actionButtonPanel, "cell 1 0,grow");
  }
  
  protected boolean adjustCashDrawerBalance(List<Currency> currencyList) {
    MultiCurrencyTenderDialog dialog = new MultiCurrencyTenderDialog(groupSettleTicketView.getTickets(), currencyList);
    dialog.pack();
    dialog.open();
    
    if (dialog.isCanceled()) {
      return false;
    }
    txtTenderedAmount.setText(NumberUtil.format3DigitNumber(Double.valueOf(dialog.getTenderedAmount())));
    return true;
  }
  
  protected void doTaxExempt() {}
  
  private void btnCancelActionPerformed(ActionEvent evt)
  {
    groupSettleTicketView.setCanceled(true);
    groupSettleTicketView.dispose();
  }
  


  Action calAction = new AbstractAction()
  {
    public void actionPerformed(ActionEvent e) {
      JTextField textField = txtTenderedAmount;
      
      PosButton button = (PosButton)e.getSource();
      String command = button.getActionCommand();
      if (command.equals(Messages.getString("PaymentView.66"))) {
        textField.setText("0");
      }
      else if (command.equals(".")) {
        if (textField.getText().indexOf('.') < 0) {
          textField.setText(textField.getText() + ".");
        }
      }
      else {
        if (clearPreviousAmount) {
          textField.setText("");
          clearPreviousAmount = false;
        }
        String string = textField.getText();
        int index = string.indexOf('.');
        if (index < 0) {
          double value = 0.0D;
          try {
            value = Double.parseDouble(string);
          } catch (NumberFormatException x) {
            Toolkit.getDefaultToolkit().beep();
          }
          if (value == 0.0D) {
            textField.setText(command);
          }
          else {
            textField.setText(string + command);
          }
        }
        else {
          textField.setText(string + command);
        }
      }
    }
  };
  
  Action nextButtonAction = new AbstractAction()
  {
    public void actionPerformed(ActionEvent e) {
      try {
        DecimalFormat format = new DecimalFormat("##.00");
        
        PosButton button = (PosButton)e.getSource();
        
        String command = button.getActionCommand();
        
        if (command.equals("exactAmount")) {
          double dueAmount = getDueAmount();
          txtTenderedAmount.setText(NumberUtil.formatNumber(Double.valueOf(dueAmount)));
        }
        else if (command.equals("nextAmount"))
        {
          double dd = NumberUtil.parse(txtDueAmount.getText()).doubleValue();
          double amount = Math.ceil(dd);
          
          txtTenderedAmount.setText(String.valueOf(format.format(amount)));
        }
        else {
          if (clearPreviousAmount) {
            txtTenderedAmount.setText("0");
            clearPreviousAmount = false;
          }
          
          double x = NumberUtil.parse(txtTenderedAmount.getText()).doubleValue();
          double y = Double.parseDouble(command);
          double z = x + y;
          txtTenderedAmount.setText(String.valueOf(format.format(z)));
        }
      } catch (Exception e2) {
        LogFactory.getLog(getClass()).error(e2);
      }
    }
  };
  
  public void updateView() {
    double dueAmount = getDueAmount();
    
    txtDueAmount.setText(NumberUtil.formatNumber(Double.valueOf(dueAmount)));
    txtTenderedAmount.setText(NumberUtil.formatNumber(Double.valueOf(dueAmount)));
  }
  
  public double getTenderedAmount() throws ParseException {
    double doubleValue = NumberUtil.parse(txtTenderedAmount.getText()).doubleValue();
    return doubleValue;
  }
  
  public GroupSettleTicketDialog getSettleTicketView() {
    return groupSettleTicketView;
  }
  
  public void setSettleTicketView(GroupSettleTicketDialog groupSettleTicketView) {
    this.groupSettleTicketView = groupSettleTicketView;
  }
  
  protected double getDueAmount() {
    return groupSettleTicketView.getDueAmount();
  }
  
  public void setDefaultFocus() {
    txtTenderedAmount.requestFocus();
  }
}
