package com.floreantpos.ui.views.payment;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.actions.GroupSettleTicketAction;
import com.floreantpos.actions.ReorderTicketAction;
import com.floreantpos.actions.SendToKitchenAction;
import com.floreantpos.actions.SettleTicketAction;
import com.floreantpos.actions.ShowOrderInfoAction;
import com.floreantpos.actions.SplitTicketAction;
import com.floreantpos.actions.TicketEditAction;
import com.floreantpos.customer.CustomerSelectorDialog;
import com.floreantpos.customer.CustomerSelectorFactory;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.extension.OrderServiceFactory;
import com.floreantpos.main.Application;
import com.floreantpos.model.Customer;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.OrderInfoDialog;
import com.floreantpos.ui.views.OrderInfoView;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.TicketAlreadyExistsException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;



















public class SplitedTicketSelectionDialog
  extends POSDialog
{
  private List<Ticket> splitTickets;
  private JPanel splitTicketsPanel;
  private PosButton btnPay;
  private PosButton btnPayAll;
  private PosButton btnOrderInfo;
  private PosButton btnSelectCustomer;
  private boolean allowCustomerSelection = false;
  private String action;
  private PosButton btnHold;
  private PosButton btnPrint;
  private PosButton btnPrintAll;
  private boolean confirmSpliting;
  private List<ShopTable> selectedTables;
  private OrderType orderType;
  
  public SplitedTicketSelectionDialog(List<Ticket> tickets) {
    super(POSUtil.getFocusedWindow(), POSConstants.TICKETS.toUpperCase());
    splitTickets = tickets;
    setLayout(new BorderLayout());
    
    JPanel centerPanel = new JPanel(new BorderLayout(5, 5));
    centerPanel.setBorder(BorderFactory.createEmptyBorder(10, 15, 10, 15));
    splitTicketsPanel = new JPanel(new MigLayout("wrap 4", "sg", ""));
    splitTicketsPanel.setBorder(new TitledBorder(POSConstants.TICKETS.toUpperCase().replace(".", "")));
    JScrollPane scrollPane = new JScrollPane(splitTicketsPanel);
    scrollPane.setBorder(null);
    centerPanel.add(scrollPane, "Center");
    
    add(centerPanel, "Center");
    createButtonPanel();
    allowCustomerSelection(false);
  }
  
  public void setSelectedAction(String action) {
    this.action = action;
    if (!StringUtils.isEmpty(action)) {
      btnSelectCustomer.setVisible(false);
      btnPay.setVisible(false);
      btnPayAll.setVisible(false);
      btnHold.setVisible(false);
      btnPrint.setVisible(false);
      btnPrintAll.setVisible(false);
      btnOrderInfo.setVisible(false);
    }
    setSelectedSection(null);
  }
  
  public void allowCustomerSelection(boolean b) {
    allowCustomerSelection = b;
    btnSelectCustomer.setVisible(b);
    rendererTickets();
  }
  
  public void createButtonPanel() {
    TransparentPanel buttonPanel = new TransparentPanel();
    buttonPanel.setLayout(new MigLayout("fill,hidemode 3, ins 0 17 10 17", "fill", ""));
    
    btnPayAll = new PosButton(Messages.getString("SplitedTicketSelectionDialog.7"));
    btnPayAll.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent evt)
      {
        for (Ticket ticket : splitTickets) {
          if (ticket.getId() != null)
            TicketDAO.getInstance().loadFullTicket(ticket);
        }
        GroupSettleTicketAction action = new GroupSettleTicketAction(splitTickets, null);
        action.actionPerformed(evt);
        updateView();
        if (getDueAmount() <= 0.0D) {
          dispose();
        }
        else {
          setCanceled(false);
        }
        
      }
    });
    btnPay = new PosButton(Messages.getString("SplitedTicketSelectionDialog.8"));
    btnPay.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        SplitedTicketSelectionDialog.TicketSection selectedSection = getSelectedSection();
        if (selectedSection == null) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select ticket.");
          return;
        }
        Ticket splitTicket = selectedSection.getTicket();
        if (splitTicket == null) {
          return;
        }
        
        if (splitTicket.isPaid().booleanValue()) {
          POSMessageDialog.showMessage(Messages.getString("SplitedTicketSelectionDialog.9"));
          return;
        }
        SettleTicketAction action = new SettleTicketAction(splitTicket);
        action.actionPerformed(evt);
        if (StringUtils.isNotEmpty(splitTicket.getId())) {
          List<Integer> tableNumbers = ((Ticket)splitTickets.get(0)).getTableNumbers();
          TicketDAO.getInstance().saveOrUpdateSplitTickets(splitTickets, tableNumbers);
        }
        updateView();
        if (getDueAmount() <= 0.0D) {
          dispose();
        }
        else {
          setCanceled(false);
        }
        
      }
    });
    btnPrintAll = new PosButton(Messages.getString("SplitedTicketSelectionDialog.17"));
    btnPrintAll.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        if (!SplitedTicketSelectionDialog.this.confirmSpliting(true))
          return;
        for (Ticket splitTicket : splitTickets) {
          ReceiptPrintService.printTicket(splitTicket);
        }
        
      }
    });
    btnPrint = new PosButton(Messages.getString("SplitedTicketSelectionDialog.20"));
    btnPrint.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        SplitedTicketSelectionDialog.TicketSection selectedSection = getSelectedSection();
        if (selectedSection == null) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select ticket.");
          return;
        }
        Ticket splitTicket = selectedSection.getTicket();
        if (splitTicket == null)
          return;
        if (!SplitedTicketSelectionDialog.this.confirmSpliting(true))
          return;
        ReceiptPrintService.printTicket(splitTicket);
      }
      
    });
    btnHold = new PosButton(Messages.getString("SplitedTicketSelectionDialog.23"));
    btnHold.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        if (!SplitedTicketSelectionDialog.this.confirmSpliting(false))
          return;
        setCanceled(OrderView.getInstance().isVisible());
        dispose();
      }
      
    });
    btnOrderInfo = new PosButton(Messages.getString("SplitedTicketSelectionDialog.24"));
    btnOrderInfo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        SplitedTicketSelectionDialog.this.showOrderInfo();
      }
      
    });
    PosButton btnCancel = new PosButton(POSConstants.CANCEL.toUpperCase());
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        setCanceled(true);
        dispose();
      }
      
    });
    btnSelectCustomer = new PosButton("SELECT MEMBER");
    btnSelectCustomer.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        Ticket splitTicket = getSelectedSection().getTicket();
        if (splitTicket == null) {
          return;
        }
        CustomerSelectorDialog dialog = CustomerSelectorFactory.createCustomerSelectorDialog(splitTicket.getOrderType());
        dialog.setCreateNewTicket(false);
        dialog.openUndecoratedFullScreen();
        
        if (!dialog.isCanceled()) {
          Customer selectedCustomer = dialog.getSelectedCustomer();
          splitTicket.setCustomerId(selectedCustomer.getId());
          splitTicket.setCustomer(selectedCustomer);
          updateView();
          splitTicketsPanel.revalidate();
          splitTicketsPanel.repaint();
        }
      }
    });
    btnSelectCustomer.setVisible(false);
    
    buttonPanel.add(btnSelectCustomer);
    buttonPanel.add(btnOrderInfo);
    buttonPanel.add(btnPrintAll);
    buttonPanel.add(btnPrint);
    buttonPanel.add(btnPayAll);
    buttonPanel.add(btnPay);
    buttonPanel.add(btnHold);
    buttonPanel.add(btnCancel);
    
    add(buttonPanel, "South");
  }
  
  private void showOrderInfo() {
    List<Ticket> ticketsToShow = new ArrayList();
    TicketSection selectedSection = getSelectedSection();
    if (selectedSection == null) {
      ticketsToShow.addAll(splitTickets);
    }
    else {
      ticketsToShow.add(selectedSection.getTicket());
    }
    try
    {
      OrderInfoView view = new OrderInfoView(ticketsToShow);
      OrderInfoDialog dialog = new OrderInfoDialog(view);
      dialog.showOnlyPrintButton();
      dialog.setSize(PosUIManager.getSize(400), PosUIManager.getSize(600));
      dialog.setDefaultCloseOperation(2);
      dialog.setLocationRelativeTo(Application.getPosWindow());
      dialog.open();
    }
    catch (Exception localException) {}
  }
  
  protected void updateTicketView(Ticket splitTicket) {
    for (int row = 0; row < splitTickets.size(); row++) {
      Ticket modelTicket = (Ticket)splitTickets.get(row);
      if (modelTicket.getShortId().equals(splitTicket.getShortId())) {
        splitTickets.set(row, splitTicket);
      }
    }
  }
  
  private boolean confirmSpliting(boolean updateView) {
    boolean saveRequired = false;
    for (Iterator iterator = splitTickets.iterator(); iterator.hasNext();) {
      Ticket ticket = (Ticket)iterator.next();
      if (ticket.getId() == null) {
        saveRequired = true;
        break;
      }
    }
    if (!saveRequired) {
      return true;
    }
    boolean continueSpliting = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Operation cannot be undone.Do you want to continue?", "Confirm") == 0;
    
    if (continueSpliting) {
      List<Integer> tableNumbers = ((Ticket)splitTickets.get(0)).getTableNumbers();
      TicketDAO.getInstance().saveOrUpdateSplitTickets(splitTickets, tableNumbers);
      confirmSpliting = true;
      if (updateView)
        updateView();
    }
    return continueSpliting;
  }
  
  public boolean isConfirmSpliting() {
    return confirmSpliting;
  }
  
  protected double getDueAmount() {
    double dueAmount = 0.0D;
    for (Ticket ticket : splitTickets) {
      dueAmount += ticket.getDueAmount().doubleValue();
    }
    return dueAmount;
  }
  
  public void setRefundMode(boolean refundMode) {
    btnPay.setVisible(!refundMode);
    btnPayAll.setVisible(!refundMode);
  }
  
  public void updateView() {
    if (splitTickets == null) {
      return;
    }
    for (Component c : splitTicketsPanel.getComponents()) {
      TicketSection section = (TicketSection)c;
      section.updateView();
    }
  }
  
  public void rendererTickets() {
    if (splitTickets == null) {
      return;
    }
    splitTicketsPanel.removeAll();
    for (Ticket splitTicket : splitTickets) {
      TicketSection button = new TicketSection(splitTicket);
      Dimension size = PosUIManager.getSize(180, 170);
      splitTicketsPanel.add(button, "w " + width + "!,h " + height + "!");
    }
    setSelectedSection(getFirstSection());
    splitTicketsPanel.revalidate();
    splitTicketsPanel.repaint();
  }
  
  private class TicketSection extends JPanel implements MouseListener
  {
    Ticket splitTicket;
    boolean selected;
    private JLabel lblPaidStatus;
    private JLabel lblTitle;
    private JLabel lblTotalAmount;
    private JSeparator separator;
    
    public TicketSection(Ticket splitTicket) {
      this.splitTicket = splitTicket;
      setLayout(new MigLayout("hidemode 3, inset 0,fill"));
      
      lblTitle = new JLabel();
      lblTitle.setBackground(new Color(66, 155, 207));
      lblTitle.setForeground(Color.white);
      lblTitle.setHorizontalAlignment(0);
      lblTitle.setFont(lblTitle.getFont().deriveFont(1, 20.0F));
      lblTitle.setOpaque(true);
      
      add(lblTitle, "grow,wrap");
      setBackground(Color.white);
      setBorder(BorderFactory.createLineBorder(Color.GRAY));
      
      lblTotalAmount = new JLabel();
      lblTotalAmount.setHorizontalAlignment(0);
      lblTotalAmount.setFont(lblTitle.getFont().deriveFont(1, 30.0F));
      lblTotalAmount.setBackground(Color.white);
      lblTotalAmount.setOpaque(true);
      add(lblTotalAmount, "grow,wrap");
      
      separator = new JSeparator();
      add(separator, "gapleft 20,gapright 20,grow,wrap");
      
      lblPaidStatus = new JLabel();
      lblPaidStatus.setHorizontalAlignment(0);
      lblPaidStatus.setBackground(Color.white);
      lblPaidStatus.setForeground(Color.red);
      lblPaidStatus.setFont(lblTitle.getFont().deriveFont(1, 20.0F));
      lblPaidStatus.setOpaque(true);
      add(lblPaidStatus, "grow,wrap");
      
      addMouseListener(this);
      updateView();
    }
    
    public void updateView() {
      String currencySymbol = CurrencyUtil.getCurrencySymbol();
      String title = "";
      if (splitTicket == null) {
        title = "[New Ticket]";
        lblPaidStatus.setText(String.valueOf(title));
        separator.setVisible(false);
        lblTotalAmount.setVisible(false);
      }
      else {
        Integer tokenNo = splitTicket.getTokenNo();
        
        if (tokenNo.intValue() > 0) {
          title = "Token# " + tokenNo;
        }
        else {
          title = "[New Ticket]";
        }
        User owner = splitTicket.getOwner();
        lblTitle.setText("<html><center><small>" + String.valueOf(title) + "</small><br>" + owner.getFirstName() + "</center></html>");
        lblTotalAmount.setText(currencySymbol + NumberUtil.formatNumber(splitTicket.getDueAmount()));
        String dueStatusText = "";
        if (splitTicket.isRefunded().booleanValue()) {
          dueStatusText = Messages.getString("SplitedTicketSelectionDialog.35");
        }
        else if (splitTicket.getDueAmount().doubleValue() <= 0.0D) {
          dueStatusText = Messages.getString("SplitedTicketSelectionDialog.36");
        }
        else {
          dueStatusText = currencySymbol + NumberUtil.formatNumber(splitTicket.getDueAmount());
        }
        lblPaidStatus.setText(dueStatusText);
        if (allowCustomerSelection) {
          String memId = splitTicket.getCustomerId();
          if (memId != null) {
            Customer customer = CustomerDAO.getInstance().get(memId);
            if (customer != null) {
              String customerInfo = customer.getMemberId();
              if (!StringUtils.isEmpty(customerInfo)) {
                customerInfo = customerInfo + "/" + customer.getName();
              }
              else {
                customerInfo = customer.getName();
              }
              lblTitle.setText("<html><center><small>" + String.valueOf(title) + "/" + owner.getFirstName() + "</small><br><h2>" + customerInfo + "</h2></center></html>");
            }
          }
        }
      }
    }
    
    public void setSelected(boolean selected)
    {
      this.selected = selected;
    }
    
    public boolean isSelected() {
      return selected;
    }
    
    public void mouseClicked(MouseEvent e)
    {
      if (SplitedTicketSelectionDialog.this.action != null) {
        setCanceled(false);
        dispose();
        DataChangeListener listener = new DataChangeListener()
        {
          public Object getSelectedData()
          {
            return splitTicket;
          }
          



          public void dataSetUpdated() {}
          



          public void dataRemoved(Object object) {}
          



          public void dataChanged(Object object) {}
          


          public void dataChangeCanceled(Object object) {}
          


          public void dataAdded(Object object) {}
        };
        

        if (splitTicket == null) {
          doCreateNewTicket();
        }
        else if (SplitedTicketSelectionDialog.this.action.equals(POSConstants.SETTLE)) {
          SettleTicketAction action = new SettleTicketAction(listener);
          action.actionPerformed(null);
        }
        else if (SplitedTicketSelectionDialog.this.action.equals(POSConstants.SEND_TO_KITCHEN)) {
          List<Ticket> tickets = new ArrayList();
          tickets.add(splitTicket);
          SendToKitchenAction action = new SendToKitchenAction(listener);
          action.execute();

        }
        else if (SplitedTicketSelectionDialog.this.action.equals(POSConstants.ORDER_INFO)) {
          ShowOrderInfoAction action = new ShowOrderInfoAction(listener);
          action.execute();
        }
        else if (SplitedTicketSelectionDialog.this.action.equals(POSConstants.SPLIT_TICKET)) {
          SplitTicketAction action = new SplitTicketAction(listener);
          action.execute();
        }
        else if (SplitedTicketSelectionDialog.this.action.equals(POSConstants.REORDER_TICKET_BUTTON_TEXT)) {
          ReorderTicketAction action = new ReorderTicketAction(listener);
          action.execute();
        }
        else if (SplitedTicketSelectionDialog.this.action.equals(POSConstants.EDIT)) {
          TicketEditAction action = new TicketEditAction(listener);
          action.execute();
        }
      }
    }
    
    private void doCreateNewTicket() {
      try {
        List<ShopTable> selectedTables = getSelectedTables();
        if (selectedTables.isEmpty()) {
          return;
        }
        OrderServiceFactory.getOrderService().createNewTicket(getOrderType(), selectedTables, null);
      } catch (TicketAlreadyExistsException e) {
        PosLog.error(getClass(), e);
      }
    }
    
    public void mousePressed(MouseEvent e)
    {
      setSelectedSection(this);
    }
    


    public void mouseReleased(MouseEvent e) {}
    


    public void mouseEntered(MouseEvent e) {}
    


    public void mouseExited(MouseEvent e) {}
    


    public Ticket getTicket()
    {
      return splitTicket;
    }
  }
  
  public void setSelectedSection(TicketSection section) {
    for (Component c : splitTicketsPanel.getComponents()) {
      TicketSection sec = (TicketSection)c;
      if (sec == section) {
        sec.setBorder(BorderFactory.createLineBorder(Color.blue, 4));
        sec.setSelected(true);
      }
      else {
        sec.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        sec.setSelected(false);
      }
    }
  }
  
  public TicketSection getSelectedSection() {
    for (Component c : splitTicketsPanel.getComponents()) {
      TicketSection sec = (TicketSection)c;
      if (sec.isSelected()) {
        return sec;
      }
    }
    return null;
  }
  
  public TicketSection getFirstSection() {
    Component[] arrayOfComponent = splitTicketsPanel.getComponents();int i = arrayOfComponent.length;int j = 0; if (j < i) { Component c = arrayOfComponent[j];
      TicketSection sec = (TicketSection)c;
      return sec;
    }
    return null;
  }
  
  public Ticket getTicket() {
    if (getSelectedSection() != null) {
      return getSelectedSection().getTicket();
    }
    return null;
  }
  
  public void hidePayButtons(boolean b) {
    btnPay.setVisible(!b);
    btnPayAll.setVisible(!b);
  }
  
  public List<ShopTable> getSelectedTables() {
    return selectedTables;
  }
  
  public void setSelectedTables(List<ShopTable> selectedTables) {
    this.selectedTables = selectedTables;
  }
  
  public OrderType getOrderType() {
    return orderType;
  }
  
  public void setOrderType(OrderType orderType) {
    this.orderType = orderType;
  }
}
