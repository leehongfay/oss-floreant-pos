package com.floreantpos.ui.views.payment;

import com.floreantpos.Messages;
import com.floreantpos.model.CustomPayment;
import com.floreantpos.model.dao.CustomPaymentDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;



















public class CustomPaymentSelectionDialog
  extends POSDialog
{
  private String paymentName;
  private String paymentRef;
  private String paymentFieldName;
  
  public CustomPaymentSelectionDialog()
  {
    setDefaultCloseOperation(2);
    setResizable(false);
    
    createUI();
  }
  
  private void createUI()
  {
    setPreferredSize(new Dimension(675, 529));
    
    JPanel customTypePanel = new JPanel(new MigLayout("wrap 5,center", "", ""));
    PosScrollPane pane = new PosScrollPane(customTypePanel, 20, 31);
    
    List<CustomPayment> custompPayments = CustomPaymentDAO.getInstance().findAll();
    
    for (Iterator iter = custompPayments.iterator(); iter.hasNext();) {
      CustomPayment customPayment = (CustomPayment)iter.next();
      if (customPayment.isEnable().booleanValue())
      {

        PosButton button = new PosButton(customPayment.getName());
        button.setPreferredSize(new Dimension(120, 80));
        button.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            dispose();
            
            String paymentName = e.getActionCommand();
            setPaymentName(paymentName);
            
            CustomPayment payment = CustomPaymentDAO.getInstance().getByName(paymentName);
            setPaymentFieldName(payment.getRefNumberFieldName());
            
            if (payment.isRequiredRefNumber().booleanValue())
            {
              PaymentReferenceEntryDialog dialog = new PaymentReferenceEntryDialog(payment);
              dialog.pack();
              dialog.open();
              
              if (dialog.isCanceled())
                return;
              setPaymentRef(dialog.getPaymentRef());
            }
            setCanceled(false);
          }
        });
        customTypePanel.add(button);
      }
    }
    getContentPane().add(pane, "Center");
    
    JPanel panel_1 = new JPanel();
    getContentPane().add(panel_1, "South");
    panel_1.setLayout(new BorderLayout(0, 0));
    
    JPanel panel_2 = new JPanel();
    panel_1.add(panel_2);
    
    PosButton btnCancel = new PosButton();
    panel_2.add(btnCancel);
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
    });
    btnCancel.setText(Messages.getString("CustomPaymentSelectionDialog.3"));
    
    JSeparator separator = new JSeparator();
    panel_1.add(separator, "North");
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle(Messages.getString("CustomPaymentSelectionDialog.4"));
    getContentPane().add(titlePanel, "North");
  }
  


  public String getPaymentName()
  {
    return paymentName;
  }
  


  public void setPaymentName(String paymentName)
  {
    this.paymentName = paymentName;
  }
  


  public String getPaymentRef()
  {
    return paymentRef;
  }
  


  public void setPaymentRef(String paymentRef)
  {
    this.paymentRef = paymentRef;
  }
  


  public String getPaymentFieldName()
  {
    return paymentFieldName;
  }
  


  public void setPaymentFieldName(String paymentFieldName)
  {
    this.paymentFieldName = paymentFieldName;
  }
}
