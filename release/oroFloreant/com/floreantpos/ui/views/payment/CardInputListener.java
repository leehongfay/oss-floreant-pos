package com.floreantpos.ui.views.payment;

import com.floreantpos.model.PaymentType;

public abstract interface CardInputListener
{
  public abstract void cardInputted(CardInputProcessor paramCardInputProcessor, PaymentType paramPaymentType);
}
