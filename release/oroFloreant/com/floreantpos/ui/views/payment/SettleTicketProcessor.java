package com.floreantpos.ui.views.payment;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.actions.SplitTicketAction;
import com.floreantpos.actions.VoidTicketAction;
import com.floreantpos.config.CardConfig;
import com.floreantpos.config.GiftCardConfig;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.extension.FloreantPlugin;
import com.floreantpos.extension.GiftCardPaymentPlugin;
import com.floreantpos.extension.InginicoPlugin;
import com.floreantpos.extension.PaymentGatewayPlugin;
import com.floreantpos.main.Application;
import com.floreantpos.model.CardReader;
import com.floreantpos.model.CashTransaction;
import com.floreantpos.model.CustomPayment;
import com.floreantpos.model.CustomPaymentTransaction;
import com.floreantpos.model.CustomerAccountTransaction;
import com.floreantpos.model.Gratuity;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PaymentType;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TransactionType;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.services.PosTransactionService;
import com.floreantpos.services.PostPaymentProcessor;
import com.floreantpos.ui.dialog.NumberSelectionDialog2;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.RefundDialog;
import com.floreantpos.ui.dialog.TicketDiscountSelectionDialog;
import com.floreantpos.ui.dialog.TicketItemDiscountSelectionDialog;
import com.floreantpos.ui.dialog.TransactionCompletionDialog;
import com.floreantpos.ui.views.order.OrderController;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.DrawerUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import org.apache.commons.lang.StringUtils;




















public class SettleTicketProcessor
  implements CardInputListener
{
  public static PosPaymentWaitDialog waitDialog = new PosPaymentWaitDialog();
  
  private Vector<PaymentListener> paymentListeners = new Vector(3);
  
  private Ticket ticket;
  
  private double tenderAmount;
  private String cardName;
  private PaymentType paymentType;
  private User currentUser;
  
  public SettleTicketProcessor(User currentUser)
  {
    this.currentUser = currentUser;
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }
  
  public double getTenderAmount() {
    return tenderAmount;
  }
  
  public void addPaymentListener(PaymentListener paymentListener) {
    paymentListeners.add(paymentListener);
  }
  
  public void removePaymentListener(PaymentListener paymentListener) {
    paymentListeners.remove(paymentListener);
  }
  
  public void doSettle(PaymentType paymentType, double tenderAmount) {
    try {
      this.tenderAmount = tenderAmount;
      if (ticket == null) {
        return;
      }
      if (!POSUtil.checkDrawerAssignment()) {
        return;
      }
      this.paymentType = paymentType;
      cardName = paymentType.getDisplayString();
      PosTransaction transaction = null;
      
      switch (2.$SwitchMap$com$floreantpos$model$PaymentType[paymentType.ordinal()]) {
      case 1: 
        if (!confirmPayment()) {
          return;
        }
        try
        {
          if ((transaction instanceof CashTransaction)) {
            DrawerUtil.kickDrawer();
          }
        }
        catch (Exception localException1) {}
        
        transaction = doPayByCash(paymentType);
        break;
      

      case 2: 
        CustomPaymentSelectionDialog customPaymentDialog = new CustomPaymentSelectionDialog();
        customPaymentDialog.setTitle(Messages.getString("SettleTicketDialog.8"));
        customPaymentDialog.pack();
        customPaymentDialog.open();
        
        if (customPaymentDialog.isCanceled()) {
          return;
        }
        if (!confirmPayment()) {
          return;
        }
        transaction = paymentType.createTransaction();
        CustomPaymentTransaction customPaymentTransaction = (CustomPaymentTransaction)transaction;
        customPaymentTransaction.setCustomPaymentFieldName(customPaymentDialog.getPaymentFieldName());
        customPaymentTransaction.setCustomPaymentName(customPaymentDialog.getPaymentName());
        customPaymentTransaction.setCustomPaymentRef(customPaymentDialog.getPaymentRef());
        
        doPayByCustomPayment(customPaymentTransaction);
        break;
      
      case 3: 
      case 4: 
      case 5: 
      case 6: 
      case 7: 
        doPayByCard(cardName, tenderAmount);
        break;
      
      case 8: 
      case 9: 
        doPayByCard(cardName, tenderAmount);
        break;
      
      case 10: 
        doPayByGiftCard(cardName, tenderAmount);
        break;
      
      case 11: 
        if (!confirmPayment()) {
          return;
        }
        
        CustomerAccountTransaction customerAccountTransaction = (CustomerAccountTransaction)paymentType.createTransaction();
        customerAccountTransaction.setTicket(ticket);
        customerAccountTransaction.setCaptured(Boolean.valueOf(true));
        setTransactionAmounts(customerAccountTransaction);
        customerAccountTransaction.setCustomerId(ticket.getCustomerId());
        settleTicket(customerAccountTransaction);
      

      }
      
    }
    catch (Exception e)
    {
      PosLog.error(getClass(), e);
    }
  }
  
  public PosTransaction doPayByCash(PaymentType paymentType)
  {
    PosTransaction transaction = paymentType.createTransaction();
    transaction.setTicket(ticket);
    transaction.setCaptured(Boolean.valueOf(true));
    setTransactionAmounts(transaction);
    settleTicket(transaction);
    return transaction;
  }
  
  public void payByCustomPayment(CustomPayment customPayment) {
    CustomPaymentTransaction customPaymentTransaction = new CustomPaymentTransaction();
    customPaymentTransaction.setCustomPaymentFieldName(customPayment.getRefNumberFieldName());
    customPaymentTransaction.setCustomPaymentName(customPayment.getName());
    
    if ((customPayment.isRequiredRefNumber().booleanValue()) && (StringUtils.isEmpty(customPaymentTransaction.getCustomPaymentRef()))) {
      PaymentReferenceEntryDialog dialog = new PaymentReferenceEntryDialog(customPayment);
      dialog.pack();
      dialog.open();
      
      if (dialog.isCanceled()) {
        return;
      }
      customPaymentTransaction.setCustomPaymentRef(dialog.getPaymentRef());
    }
    
    doPayByCustomPayment(customPaymentTransaction);
  }
  
  public void doPayByCustomPayment(CustomPaymentTransaction customPaymentTransaction) {
    customPaymentTransaction.setTicket(ticket);
    customPaymentTransaction.setCaptured(Boolean.valueOf(true));
    setTransactionAmounts(customPaymentTransaction);
    
    settleTicket(customPaymentTransaction);
  }
  
  private void doPayByGiftCard(String cardName, double tenderedAmount) throws Exception {
    try {
      FloreantPlugin paymentPlugin = GiftCardConfig.getPaymentGateway();
      GiftCardPaymentPlugin giftCardPaymentPlugin = null;
      if ((paymentPlugin instanceof InginicoPlugin)) {
        waitDialog.setVisible(true);
        if (!waitDialog.isCanceled()) {
          doInformListenerPaymentDone();
        }
        return;
      }
      if ((giftCardPaymentPlugin instanceof GiftCardPaymentPlugin)) {
        giftCardPaymentPlugin = (GiftCardPaymentPlugin)paymentPlugin;
      }
      if ((giftCardPaymentPlugin != null) && (!giftCardPaymentPlugin.shouldShowCardInputProcessor()))
      {
        PosTransaction transaction = paymentType.createTransaction();
        transaction.setTicket(ticket);
        
        if (!confirmPayment()) {
          return;
        }
        

        transaction.setCaptured(Boolean.valueOf(false));
        transaction.setCardMerchantGateway(giftCardPaymentPlugin.getProductName());
        
        setTransactionAmounts(transaction);
        settleTicket(transaction);
        
        return;
      }
      
      CardReader cardReader = CardConfig.getCardReader();
      switch (cardReader) {
      case SWIPE: 
        SwipeGiftCardDialog swipeGiftCardDialog = new SwipeGiftCardDialog(this);
        swipeGiftCardDialog.pack();
        swipeGiftCardDialog.open();
        break;
      
      case MANUAL: 
        ManualGiftCardEntryDialog dialog = new ManualGiftCardEntryDialog(this);
        dialog.pack();
        dialog.open();
      
      }
      
    }
    catch (Exception e)
    {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    }
  }
  
  private boolean confirmPayment()
  {
    if (!TerminalConfig.isUseSettlementPrompt()) {
      return true;
    }
    
    ConfirmPayDialog confirmPayDialog = new ConfirmPayDialog();
    confirmPayDialog.setAmount(tenderAmount);
    confirmPayDialog.open();
    
    if (confirmPayDialog.isCanceled()) {
      return false;
    }
    
    return true;
  }
  
  public void doSettleBarTabTicket(Ticket ticket, User currentUser) {
    try {
      String msg = "Do you want to settle ticket?";
      int option1 = POSMessageDialog.showYesNoQuestionDialog(null, msg, Messages.getString("NewBarTabAction.4"));
      if (option1 != 0) {
        return;
      }
      
      for (PosTransaction barTabTransaction : ticket.getTransactions()) {
        barTabTransaction.setAmount(ticket.getDueAmount());
        barTabTransaction.setTenderAmount(ticket.getDueAmount());
        barTabTransaction.setAuthorizable(Boolean.valueOf(true));
        settleTicket(barTabTransaction);
      }
    }
    catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private void settleTicket(PosTransaction transaction) {
    settleTicket(transaction, null);
  }
  
  public void settleTicket(PosTransaction transaction, PostPaymentProcessor postPaymentProcessor) {
    try {
      validateGratuity(transaction);
      double dueAmount = ticket.getDueAmount().doubleValue();
      PosTransactionService transactionService = PosTransactionService.getInstance();
      if (postPaymentProcessor == null) {
        transactionService.settleTicket(ticket, transaction, currentUser);
      }
      else {
        transactionService.settleTicket(ticket, transaction, currentUser, postPaymentProcessor);
      }
      

      if (TerminalConfig.isAutoPrintReceipt()) {
        printTicket(ticket, transaction);
      }
      showTransactionCompleteMsg(dueAmount, transaction.getTenderAmount().doubleValue(), ticket, transaction);
      if (NumberUtil.isZero(ticket.getDueAmount())) {
        doInformListenerPaymentDone();
      }
      else {
        setTicket(ticket);
        doInformListenerPaymentUpdate();
      }
    } catch (UnknownHostException e) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("SettleTicketDialog.12"));
    } catch (PosException e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, e);
    }
  }
  

  public void cardInputted(CardInputProcessor inputter, PaymentType selectedPaymentType)
  {
    PaymentProcessWaitDialog waitDialog = new PaymentProcessWaitDialog(Application.getPosWindow());
    try
    {
      waitDialog.setVisible(true);
      
      PosTransaction transaction = paymentType.createTransaction();
      transaction.setTicket(ticket);
      
      PaymentGatewayPlugin paymentGateway = CardConfig.getPaymentGateway();
      CardProcessor cardProcessor = paymentGateway.getProcessor();
      
      GiftCardPaymentPlugin giftCardPaymentGateway = GiftCardConfig.getPaymentGateway();
      GiftCardProcessor giftCardProcessor = giftCardPaymentGateway.getProcessor();
      
      if ((inputter instanceof SwipeCardDialog))
      {
        SwipeCardDialog swipeCardDialog = (SwipeCardDialog)inputter;
        String cardString = swipeCardDialog.getCardString();
        
        if ((StringUtils.isEmpty(cardString)) || (cardString.length() < 16)) {
          throw new RuntimeException(Messages.getString("SettleTicketDialog.16"));
        }
        
        if (!confirmPayment()) {
          return;
        }
        transaction.setCardType(paymentType.getDisplayString());
        transaction.setCardTrack(cardString);
        transaction.setCaptured(Boolean.valueOf(false));
        transaction.setCardMerchantGateway(paymentGateway.getProductName());
        transaction.setCardReader(CardReader.SWIPE.name());
        setTransactionAmounts(transaction);
        
        if (ticket.getOrderType().isPreAuthCreditCard().booleanValue()) {
          cardProcessor.preAuth(transaction);
        }
        else {
          cardProcessor.chargeAmount(transaction);
        }
        
        settleTicket(transaction);
      }
      else if ((inputter instanceof SwipeGiftCardDialog))
      {
        SwipeGiftCardDialog swipeGiftCardDialog = (SwipeGiftCardDialog)inputter;
        String cardString = swipeGiftCardDialog.getCardString();
        
        if ((StringUtils.isEmpty(cardString)) || (cardString.length() < 16)) {
          throw new RuntimeException(Messages.getString("SettleTicketDialog.16"));
        }
        
        if (!confirmPayment()) {
          return;
        }
        transaction.setCardType(paymentType.getDisplayString());
        
        transaction.setGiftCertNumber(cardString);
        transaction.setCaptured(Boolean.valueOf(false));
        transaction.setCardMerchantGateway(giftCardPaymentGateway.getProductName());
        transaction.setCardReader(CardReader.SWIPE.name());
        setTransactionAmounts(transaction);
        
        giftCardProcessor.chargeAmount(transaction);
        
        settleTicket(transaction);
      }
      else if ((inputter instanceof ManualCardEntryDialog)) {
        ManualCardEntryDialog mDialog = (ManualCardEntryDialog)inputter;
        transaction.setCaptured(Boolean.valueOf(false));
        transaction.setCardMerchantGateway(paymentGateway.getProductName());
        transaction.setCardReader(CardReader.MANUAL.name());
        transaction.setCardHolderName(mDialog.getCardHolderName());
        transaction.setCardNumber(mDialog.getCardNumber());
        transaction.setCardExpMonth(mDialog.getExpMonth());
        transaction.setCardExpYear(mDialog.getExpYear());
        setTransactionAmounts(transaction);
        
        cardProcessor.preAuth(transaction);
        settleTicket(transaction);
      }
      else if ((inputter instanceof ManualGiftCardEntryDialog)) {
        ManualGiftCardEntryDialog mGCDialog = (ManualGiftCardEntryDialog)inputter;
        
        transaction.setCaptured(Boolean.valueOf(false));
        transaction.setCardMerchantGateway(giftCardPaymentGateway.getProductName());
        transaction.setCardReader(CardReader.MANUAL.name());
        transaction.setGiftCertNumber(mGCDialog.getCardNumber());
        

        setTransactionAmounts(transaction);
        
        giftCardProcessor.chargeAmount(transaction);
        
        settleTicket(transaction);
      }
      else if ((inputter instanceof AuthorizationCodeDialog))
      {
        PosTransaction selectedTransaction = selectedPaymentType.createTransaction();
        selectedTransaction.setTicket(ticket);
        
        AuthorizationCodeDialog authDialog = (AuthorizationCodeDialog)inputter;
        String authorizationCode = authDialog.getAuthorizationCode();
        if (StringUtils.isEmpty(authorizationCode)) {
          throw new PosException(Messages.getString("SettleTicketDialog.17"));
        }
        
        selectedTransaction.setCardType(selectedPaymentType.getDisplayString());
        selectedTransaction.setCaptured(Boolean.valueOf(true));
        selectedTransaction.setAuthorizable(Boolean.valueOf(false));
        selectedTransaction.setCardReader(CardReader.EXTERNAL_TERMINAL.name());
        selectedTransaction.setCardAuthCode(authorizationCode);
        setTransactionAmounts(selectedTransaction);
        
        settleTicket(selectedTransaction);
      }
    } catch (PosException e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Could not make payment. Please try again or consult system administrator.", e);
    } finally {
      waitDialog.setVisible(false);
    }
  }
  
  private void doPayByCard(String cardName, double tenderedAmount) throws Exception {
    try {
      PaymentGatewayPlugin paymentGateway = CardConfig.getPaymentGateway();
      
      if ((paymentGateway instanceof InginicoPlugin)) {
        waitDialog.setVisible(true);
        if (!waitDialog.isCanceled()) {
          doInformListenerPaymentDone();
        }
        return;
      }
      if (!paymentGateway.shouldShowCardInputProcessor())
      {
        PosTransaction transaction = paymentType.createTransaction();
        transaction.setTicket(ticket);
        
        if (!confirmPayment()) {
          return;
        }
        

        transaction.setCaptured(Boolean.valueOf(false));
        transaction.setCardMerchantGateway(paymentGateway.getProductName());
        
        setTransactionAmounts(transaction);
        
        if (ticket.getOrderType().isPreAuthCreditCard().booleanValue()) {
          paymentGateway.getProcessor().preAuth(transaction);
        }
        else {
          paymentGateway.getProcessor().chargeAmount(transaction);
        }
        
        settleTicket(transaction);
        
        return;
      }
      
      CardReader cardReader = CardConfig.getCardReader();
      switch (2.$SwitchMap$com$floreantpos$model$CardReader[cardReader.ordinal()]) {
      case 1: 
        SwipeCardDialog swipeCardDialog = new SwipeCardDialog(this);
        swipeCardDialog.pack();
        swipeCardDialog.open();
        break;
      
      case 2: 
        ManualCardEntryDialog dialog = new ManualCardEntryDialog(this);
        dialog.pack();
        dialog.open();
        break;
      
      case 3: 
        AuthorizationCodeDialog authorizationCodeDialog = new AuthorizationCodeDialog(this);
        authorizationCodeDialog.pack();
        authorizationCodeDialog.open();
      
      }
      
    }
    catch (PosException e)
    {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private void updateModel()
  {
    if (ticket == null) {
      return;
    }
    ticket.calculatePrice();
  }
  
  public void doApplyCoupon(int couponType) {
    try {
      if (ticket == null) {
        return;
      }
      if (!Application.getCurrentUser().hasPermission(UserPermission.ADD_DISCOUNT)) {
        POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("SettleTicketDialog.7"));
        return;
      }
      
      if (couponType == 1) {
        TicketDiscountSelectionDialog dialog = new TicketDiscountSelectionDialog(ticket);
        dialog.openFullScreen();
        
        if (dialog.isCanceled()) {
          return;
        }
      }
      else {
        TicketItemDiscountSelectionDialog dialog = new TicketItemDiscountSelectionDialog(ticket);
        dialog.openFullScreen();
        
        if (dialog.isCanceled()) {
          return;
        }
      }
      updateModel();
      doInformListenerPaymentUpdate();
      


      if (OrderView.getInstance().isVisible())
        OrderView.getInstance().setCurrentTicket(ticket);
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  public void doSetGratuity() {
    if (ticket == null) {
      return;
    }
    GratuityInputDialog d = new GratuityInputDialog();
    d.pack();
    d.setResizable(false);
    d.open();
    
    if (d.isCanceled()) {
      return;
    }
    
    double gratuityAmount = d.getGratuityAmount();
    Gratuity gratuity = ticket.createGratuity();
    gratuity.setAmount(Double.valueOf(gratuityAmount));
    
    ticket.setGratuity(gratuity);
    ticket.calculatePrice();
    
    doInformListenerPaymentUpdate();
  }
  
  public void doInformListenerPaymentUpdate() {
    for (PaymentListener paymentListener : paymentListeners) {
      paymentListener.paymentDataChanged();
    }
  }
  
  private void setTransactionAmounts(PosTransaction transaction) {
    transaction.setTenderAmount(Double.valueOf(tenderAmount));
    
    if (tenderAmount >= ticket.getDueAmount().doubleValue()) {
      transaction.setAmount(ticket.getDueAmount());
    }
    else {
      transaction.setAmount(Double.valueOf(tenderAmount));
    }
  }
  
  public void doHoldTicket() {
    if ((ticket.isReOpened().booleanValue()) && (ticket.getDueAmount().doubleValue() == 0.0D)) {
      ticket.setClosed(Boolean.valueOf(true));
    }
    TicketDAO.getInstance().saveOrUpdate(ticket);
    for (PaymentListener paymentListener : paymentListeners) {
      paymentListener.paymentDone();
    }
  }
  
  public void printTicket(Ticket ticket) {
    if (StringUtils.isEmpty(ticket.getId())) {
      OrderController.saveOrder(ticket);
    }
    ReceiptPrintService.printTicket(ticket);
    for (PaymentListener paymentListener : paymentListeners) {
      paymentListener.paymentDataChanged();
    }
  }
  
  public void doSplitTicket() {
    DataChangeListener listener = new DataChangeListener()
    {
      public Object getSelectedData()
      {
        return ticket;
      }
      


      public void dataSetUpdated() {}
      

      public void dataRemoved(Object object) {}
      

      public void dataChanged(Object object)
      {
        if (object == null)
          return;
        Ticket ticket = (Ticket)object;
        String id = ticket.getId();
        if (StringUtils.isNotEmpty(id)) {
          ticket = TicketDAO.getInstance().loadFullTicket(id);
          SettleTicketProcessor.this.doInformListenerPaymentDone();
        }
        else {
          doInformListenerPaymentUpdate();
        }
      }
      


      public void dataChangeCanceled(Object object) {}
      


      public void dataAdded(Object object) {}
    };
    SplitTicketAction splitAction = new SplitTicketAction();
    splitAction.setDataChangedListener(listener);
    splitAction.execute();
  }
  




  public void cancelPayment()
  {
    for (PaymentListener paymentListener : paymentListeners) {
      paymentListener.paymentCanceled();
    }
  }
  
  public static void showTransactionCompleteMsg(double dueAmount, double tenderedAmount, Ticket ticket, PosTransaction transaction) {
    TransactionCompletionDialog dialog = new TransactionCompletionDialog(transaction);
    dialog.setCompletedTransaction(transaction);
    dialog.setTenderedAmount(tenderedAmount);
    dialog.setTotalAmount(dueAmount);
    dialog.setPaidAmount(transaction.getAmount().doubleValue());
    dialog.setDueAmount(ticket.getDueAmount().doubleValue());
    dialog.setGratuityAmount(transaction.getTipsAmount().doubleValue());
    if (tenderedAmount > transaction.getAmount().doubleValue()) {
      dialog.setChangeAmount(tenderedAmount - transaction.getAmount().doubleValue());
    }
    else {
      dialog.setChangeAmount(0.0D);
    }
    
    dialog.updateView();
    dialog.pack();
    dialog.open();
  }
  
  public static void printTicket(Ticket ticket, PosTransaction transaction) {
    try {
      if ((ticket.getOrderType().isShouldPrintToKitchen().booleanValue()) && 
        (ticket.needsKitchenPrint())) {
        ReceiptPrintService.printToKitchen(ticket);
      }
      

      ReceiptPrintService.printTransaction(transaction);
    } catch (Exception ee) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.PRINT_ERROR, ee);
    }
  }
  
  public void doRefund() {
    if (ticket.getPaidAmount().doubleValue() <= 0.0D) {
      if (ticket.getDueAmount().doubleValue() < 0.0D) {
        doForceRefund();
      }
      else {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "No payment was made.");
      }
      return;
    }
    double refundedAmount = ticket.getDueAmount().doubleValue();
    if (refundedAmount >= 0.0D) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "No valid refund amount found.");
      return;
    }
    
    RefundDialog dialog = new RefundDialog(POSUtil.getFocusedWindow(), ticket);
    dialog.setSize(800, 600);
    dialog.open();
    ticket.calculatePrice();
    if (NumberUtil.isZero(ticket.getDueAmount())) {
      doInformListenerPaymentDone();
    }
    else {
      doInformListenerPaymentUpdate();
    }
  }
  
  private void doForceRefund() {
    double paidAmount = ticket.getPaidAmount().doubleValue();
    try {
      double refundAmount = NumberSelectionDialog2.takeDoubleInput("Enter refund amount", Math.abs(ticket.getDueAmount().doubleValue()));
      if (refundAmount <= 0.0D)
        return;
      if (refundAmount > Math.abs(ticket.getDueAmount().doubleValue())) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Refund amount cannot be greater than paid amount.");
        return;
      }
      PosTransaction transaction = PaymentType.CASH.createTransaction();
      transaction.setTicket(ticket);
      transaction.setCaptured(Boolean.valueOf(true));
      transaction.setAmount(Double.valueOf(refundAmount));
      transaction.setTenderAmount(Double.valueOf(refundAmount));
      transaction.setTransactionType(TransactionType.CREDIT.name());
      transaction.setPaymentType(transaction.getPaymentType());
      transaction.setTerminal(Application.getInstance().getTerminal());
      transaction.setUser(currentUser);
      transaction.setCashDrawer(currentUser.getActiveDrawerPullReport());
      transaction.setTransactionTime(new Date());
      
      List<PosTransaction> transactions = new ArrayList();
      transactions.add(transaction);
      
      double totalTransactionTaxAmount = Math.abs(ticket.getTaxAmount().doubleValue());
      Double refundTaxAmount = Double.valueOf(NumberUtil.roundToTwoDigit(totalTransactionTaxAmount * refundAmount / Math.abs(ticket.getDueAmount().doubleValue())));
      double refundedAmount = PosTransactionService.getInstance().refundTicket(ticket, refundAmount, refundTaxAmount, Application.getCurrentUser(), transactions, true);
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Refunded " + CurrencyUtil.getCurrencySymbol() + refundedAmount);
      doInformListenerPaymentDone();
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage());
      ticket.setPaidAmount(Double.valueOf(paidAmount));
    }
  }
  
  public void doVoidTicket() {
    if (ticket.getId() == null) {
      doInformListenerPaymentDone();
      return;
    }
    ticket.calculatePrice();
    



    VoidTicketAction action = new VoidTicketAction(ticket);
    action.actionPerformed(null);
    if (ticket.isVoided().booleanValue()) {
      if (ticket.getPaidAmount().doubleValue() > 0.0D) {
        doInformListenerPaymentUpdate();
      } else
        doInformListenerPaymentDone();
    }
  }
  
  private void validateGratuity(PosTransaction transaction) {
    if (!ticket.hasGratuity()) {
      return;
    }
    if ((ticket.getDueAmount() != transaction.getAmount()) && (ticket.getPaidAmount().doubleValue() + transaction.getTenderAmount().doubleValue() >= ticket.getTotalAmount().doubleValue())) {
      Gratuity gratuity = ticket.getGratuity();
      gratuity.setAmount(Double.valueOf(gratuity.getAmount().doubleValue() - (ticket.getDueAmount().doubleValue() - transaction.getAmount().doubleValue())));
      ticket.calculatePrice();
    }
  }
  
  private void doInformListenerPaymentDone() {
    for (PaymentListener paymentListener : paymentListeners) {
      paymentListener.paymentDone();
    }
  }
}
