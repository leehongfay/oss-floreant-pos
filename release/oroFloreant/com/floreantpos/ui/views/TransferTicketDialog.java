package com.floreantpos.ui.views;

import com.floreantpos.POSConstants;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;




public class TransferTicketDialog
  extends POSDialog
{
  private List<Ticket> tickets;
  private TicketTransferView leftTicketView;
  private TicketTransferView rightTicketView;
  
  public TransferTicketDialog(List<Ticket> tickets)
  {
    this.tickets = tickets;
    initComponents();
    initData();
  }
  
  private void initData() {
    leftTicketView.setSelectTicketButton(0);
    rightTicketView.setSelectTicketButton(1);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Transfer ticket items");
    add(titlePanel, "North");
    JPanel centerPanel = new JPanel();
    centerPanel.setLayout(new MigLayout("fill", "[fill, grow][fill, grow]", ""));
    

    leftTicketView = new TicketTransferView(tickets, TicketTransferView.ORIENTATION.LEFT);
    rightTicketView = new TicketTransferView(tickets, TicketTransferView.ORIENTATION.RIGHT);
    
    leftTicketView.setOtherTicketView(rightTicketView);
    rightTicketView.setOtherTicketView(leftTicketView);
    
    JPanel leftPanel = new JPanel(new BorderLayout());
    leftPanel.add(leftTicketView, "Center");
    
    JPanel rightPanel = new JPanel(new BorderLayout());
    rightPanel.add(rightTicketView, "Center");
    
    centerPanel.add(leftPanel, "");
    centerPanel.add(rightPanel, "wrap");
    
    add(centerPanel, "Center");
    
    JPanel bottomPanel = new JPanel(new MigLayout("center"));
    PosButton btnOk = new PosButton("Done");
    btnOk.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TransferTicketDialog.this.doOk();
      }
    });
    PosButton btnCancel = new PosButton("Cancel");
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setCanceled(true);
        dispose();
      }
      
    });
    bottomPanel.add(btnOk);
    bottomPanel.add(btnCancel);
    
    add(bottomPanel, "South");
  }
  
  private void doOk() {
    try {
      List<Ticket> renderedTickets = rightTicketView.getTickets();
      
      List<Ticket> deleteTickets = new ArrayList();
      for (Iterator iterator = renderedTickets.iterator(); iterator.hasNext();) {
        Ticket ticket = (Ticket)iterator.next();
        if (ticket.getTicketItems().size() <= 0) {
          deleteTickets.add(ticket);
          iterator.remove();
        }
      }
      
      if (deleteTickets.size() > 0) {
        int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Some ticket contains no items. Do you want to delete those tickets?", "No items");
        
        if (option != 0) {
          return;
        }
      }
      

      TicketDAO.getInstance().saveOrUpdateTransferedTicketsList(deleteTickets, renderedTickets);
      setCanceled(false);
      dispose();
    } catch (Exception e2) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e2);
    }
  }
  
  public List<Ticket> getTickets() {
    return tickets;
  }
  
  public void setTickets(List<Ticket> tickets) {
    this.tickets = tickets;
  }
}
