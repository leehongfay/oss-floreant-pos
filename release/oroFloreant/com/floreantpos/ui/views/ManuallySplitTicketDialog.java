package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.model.ModifiableTicketItem;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketDiscount;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemModifier;
import com.floreantpos.swing.POSTitleLabel;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.ticket.TicketViewerTable;
import com.floreantpos.ui.views.order.actions.SplitItemSelectionListener;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.beanutils.PropertyUtils;

























public class ManuallySplitTicketDialog
  extends POSDialog
  implements SplitItemSelectionListener
{
  private Ticket ticket;
  private PosButton btnCancel;
  private PosButton btnFinish;
  private POSTitleLabel lblTicketId;
  private TicketSplitView mainTicketView;
  private TicketSplitView ticketView2;
  private TransparentPanel actionButtonPanel;
  private TransparentPanel centerPanel;
  private TransparentPanel toolbarPanel;
  private TransparentPanel ticketPanel;
  private List<Ticket> splitTickets = new ArrayList();
  private Map<Integer, TicketButton> ticketMap = new HashMap();
  private int splitQuantity;
  private ButtonGroup group;
  private JPanel topPanel;
  
  public ManuallySplitTicketDialog()
  {
    super(POSUtil.getFocusedWindow(), POSConstants.SPLIT_TICKET);
    initComponents();
    
    mainTicketView.setTicketView1(ticketView2);
    ticketView2.setTicketView1(mainTicketView);
    
    mainTicketView.setViewNumber(1);
    ticketView2.setViewNumber(2);
    
    setBounds(Application.getPosWindow().getBounds());
  }
  
  private void initComponents() {
    centerPanel = new TransparentPanel(new BorderLayout());
    createActionButtonPanel();
    createToolbarPanel();
    createTicketViewPanel();
    
    centerPanel.add(ticketPanel, "Center");
    getContentPane().add(centerPanel, "Center");
  }
  
  public void allowCustomerSelection(boolean b)
  {
    mainTicketView.allowCustomerSelection(b);
    ticketView2.allowCustomerSelection(b);
  }
  
  private void createToolbarPanel() {
    topPanel = new JPanel(new MigLayout("center"));
    toolbarPanel = new TransparentPanel();
    
    lblTicketId = new POSTitleLabel();
    
    lblTicketId.setText(Messages.getString("SplitTicketDialog.0"));
    group = new ButtonGroup();
    toolbarPanel.add(lblTicketId);
    JSeparator separator = new JSeparator(1);
    separator.setPreferredSize(new Dimension(5, 50));
    toolbarPanel.add(separator);
    
    PosButton btnNewTicket = new PosButton(Messages.getString("ManuallySplitTicketDialog.1"));
    btnNewTicket.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ManuallySplitTicketDialog.this.doAddNewTicketButton();
      }
      
    });
    PosButton btnRemoveTicket = new PosButton("");
    btnRemoveTicket.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ManuallySplitTicketDialog.this.doRemoveTicketButton();
      }
      
    });
    btnNewTicket.setIcon(new ImageIcon(getClass().getResource("/images/plus.png")));
    btnRemoveTicket.setIcon(new ImageIcon(getClass().getResource("/images/minus2.png")));
    
    topPanel.add(toolbarPanel, "split 2");
    topPanel.add(btnNewTicket);
    topPanel.add(btnRemoveTicket);
    centerPanel.add(topPanel, "North");
  }
  
  private void createTicketViewPanel() {
    ticketPanel = new TransparentPanel(new MigLayout("fill,inset 0 20 0 20, hidemode 3"));
    
    mainTicketView = new TicketSplitView(this);
    ticketView2 = new TicketSplitView(this);
    
    ticketPanel.add(mainTicketView, "grow");
    ticketPanel.add(ticketView2, "grow");
  }
  
  private void createActionButtonPanel() {
    actionButtonPanel = new TransparentPanel();
    
    btnFinish = new PosButton();
    btnFinish.setText(POSConstants.FINISH);
    
    btnFinish.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        ManuallySplitTicketDialog.this.btnFinishActionPerformed(evt);
      }
      
    });
    actionButtonPanel.add(btnFinish);
    
    btnCancel = new PosButton();
    btnCancel.setText(POSConstants.CANCEL);
    
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        ManuallySplitTicketDialog.this.btnCancelActionPerformed(evt);
      }
      
    });
    actionButtonPanel.add(btnCancel);
    
    getContentPane().add(actionButtonPanel, "South");
  }
  
  private void btnCancelActionPerformed(ActionEvent evt) {
    setTicket(null, 0);
    dispose();
  }
  







  private synchronized void btnFinishActionPerformed(ActionEvent evt)
  {
    splitTickets.add(ticket);
    ticket.setSplitOrder(1);
    for (int i = 1; i < ticketMap.size(); i++) {
      Ticket newSplitTicket = ((TicketButton)ticketMap.get(Integer.valueOf(i + 1))).getTicket();
      List<TicketItem> ticketItems = newSplitTicket.getTicketItems();
      if ((newSplitTicket != null) && (!ticketItems.isEmpty())) {
        newSplitTicket.setOrderType(ticket.getOrderType());
        transferTicketDiscounts(newSplitTicket);
        newSplitTicket.setSplitOrder(i + 1);
        newSplitTicket.calculatePrice();
        List deletedItems = newSplitTicket.getDeletedItems();
        if (deletedItems != null)
          deletedItems.clear();
        splitTickets.add(newSplitTicket);
      }
    }
    updateTicketDiscounts();
    List deletedItems = ticket.getDeletedItems();
    if (deletedItems != null)
      deletedItems.clear();
    ticket.calculatePrice();
    setCanceled(false);
    dispose();
  }
  
  private void transferTicketDiscounts(Ticket newTicket) {
    List<TicketDiscount> newTicketDiscounts = new ArrayList();
    try {
      List<TicketDiscount> ticketDiscounts = ticket.getDiscounts();
      for (TicketDiscount ticketDiscount : ticketDiscounts) {
        TicketDiscount newTicketDiscount = new TicketDiscount();
        PropertyUtils.copyProperties(newTicketDiscount, ticketDiscount);
        newTicketDiscount.setId(null);
        newTicketDiscount.setCouponQuantity(Double.valueOf(ticketDiscount.getCouponQuantity().doubleValue() / splitQuantity));
        newTicketDiscount.setTicket(newTicket);
        newTicketDiscounts.add(newTicketDiscount);
      }
      newTicket.setDiscounts(newTicketDiscounts);
    }
    catch (Exception localException) {}
  }
  
  private List<TicketDiscount> updateTicketDiscounts() {
    List<TicketDiscount> newTicketDiscounts = new ArrayList();
    try {
      List<TicketDiscount> ticketDiscounts = ticket.getDiscounts();
      for (TicketDiscount ticketDiscount : ticketDiscounts) {
        ticketDiscount.setCouponQuantity(Double.valueOf(ticketDiscount.getCouponQuantity().doubleValue() / splitQuantity));
      }
    }
    catch (Exception localException) {}
    return newTicketDiscounts;
  }
  
































  public Ticket getTicket()
  {
    return ticket;
  }
  
  public void setTicket(Ticket ticket, int splitQuantity) {
    ticketMap.clear();
    this.ticket = ticket;
    this.splitQuantity = splitQuantity;
    if (ticket != null) {
      lblTicketId.setText(POSConstants.ORIGINAL_TICKET_ID + ": " + (ticket.getId() == null ? "[New Ticket]" : ticket.getId()));
      mainTicketView.setTicket(ticket);
      mainTicketView.setTotalTicketQuantity(splitQuantity);
      for (int i = 0; i < splitQuantity; i++) {
        TicketButton btnTicket = new TicketButton(createNewTicket(), i + 1);
        ticketMap.put(Integer.valueOf(i + 1), btnTicket);
        if (viewNumber == 2)
          btnTicket.setSelected(true);
        if (viewNumber == 1)
          btnTicket.setVisible(false);
        toolbarPanel.add(btnTicket);
        group.add(btnTicket);
      }
      ticketView2.setTicket(((TicketButton)ticketMap.get(Integer.valueOf(2))).getTicket());
    }
  }
  
  private void doAddNewTicketButton() {
    splitQuantity = (ticketMap.values().size() + 1);
    TicketButton btnTicket = new TicketButton(createNewTicket(), splitQuantity);
    ticketMap.put(Integer.valueOf(splitQuantity), btnTicket);
    group.add(btnTicket);
    toolbarPanel.add(btnTicket);
    ticketView2.setTicket(btnTicket.getTicket());
    ticketView2.setViewNumber(splitQuantity);
    toolbarPanel.revalidate();
    toolbarPanel.repaint();
    mainTicketView.setTotalTicketQuantity(ticketMap.values().size());
    btnTicket.setSelected(true);
  }
  
  private void doRemoveTicketButton() {
    TicketButton btnTicket = (TicketButton)ticketMap.get(Integer.valueOf(ticketView2.getViewNumber()));
    if (btnTicket.getTicket().getTicketItems().size() > 0) {
      POSMessageDialog.showMessage(Messages.getString("ManuallySplitTicketDialog.4"));
      return;
    }
    if (viewNumber == 2) {
      return;
    }
    int previousTicketNumber = viewNumber - 1;
    toolbarPanel.remove(btnTicket);
    ticketMap.remove(Integer.valueOf(viewNumber));
    
    List<TicketButton> addedTicketButtons = new ArrayList();
    addedTicketButtons.addAll(ticketMap.values());
    ticketMap.clear();
    
    int viewNumber = 1;
    for (TicketButton ticketButton : addedTicketButtons) {
      ticketButton.setViewNumber(viewNumber);
      ticketMap.put(Integer.valueOf(viewNumber), ticketButton);
      viewNumber++;
    }
    toolbarPanel.revalidate();
    toolbarPanel.repaint();
    
    TicketButton btnPrevious = (TicketButton)ticketMap.get(Integer.valueOf(previousTicketNumber));
    btnPrevious.setSelected(true);
    ticketView2.setTicket(btnPrevious.getTicket());
    ticketView2.setViewNumber(previousTicketNumber);
    splitQuantity -= 1;
  }
  
  private Ticket createNewTicket() {
    List<Integer> tableNumbers = new ArrayList();
    List<Integer> ticketTableNumbers = ticket.getTableNumbers();
    if (ticketTableNumbers != null)
      tableNumbers.addAll(ticketTableNumbers);
    Ticket splitTicket = ticket.clone(ticket);
    splitTicket.setId(null);
    splitTicket.setTokenNo(null);
    splitTicket.setShortId(null);
    splitTicket.setTicketItems(null);
    splitTicket.setTransactions(null);
    splitTicket.setProperties(null);
    splitTicket.setDiscounts(null);
    splitTicket.setTableNumbers(tableNumbers);
    splitTicket.setGratuity(null);
    splitTicket.calculatePrice();
    return splitTicket;
  }
  
  public List<Ticket> getSplitTickets() {
    return splitTickets;
  }
  
  private class TicketButton extends POSToggleButton implements ActionListener {
    private Ticket ticket;
    private int viewNumber;
    
    TicketButton(Ticket ticket, int viewNumber) {
      this.ticket = ticket;
      this.viewNumber = viewNumber;
      setFont(getFont().deriveFont(1, PosUIManager.getFontSize(18)));
      setText("<html><body><center><h1>" + viewNumber + "</h1></center></body></html>");
      addActionListener(this);
    }
    
    public void setViewNumber(int viewNumber) {
      this.viewNumber = viewNumber;
      setText("<html><body><center><h1>" + viewNumber + "</h1></center></body></html>");
    }
    
    public Ticket getTicket() {
      return ticket;
    }
    
    public void actionPerformed(ActionEvent e) {
      TicketButton ticketButton = (TicketButton)ticketMap.get(Integer.valueOf(viewNumber));
      Ticket ticket = ticketButton.getTicket();
      if (ticket != null) {
        ticketView2.setTicket(ticket);
        ticketView2.setViewNumber(viewNumber);
      }
    }
  }
  
  public void itemSelected(TicketItem ticketItem, TicketSplitView fromView, TicketSplitView toView, int itemSplitType, Double quantity)
  {
    splitQuantity = ticketMap.size();
    if (itemSplitType == 2) {
      shareTicketItemToAll(ticketItem);
    }
    else if (itemSplitType == 3) {
      List<Integer> viewNumbers = fromView.getViewNumbers();
      splitQuantity = viewNumbers.size();
      shareTicketItemToAll(ticketItem, viewNumbers);
    }
    else if (itemSplitType == 0) {
      splitQuantity = 2;
      List<Integer> viewNumbers = new ArrayList();
      viewNumbers.add(Integer.valueOf(fromView.getViewNumber()));
      viewNumbers.add(Integer.valueOf(toView.getViewNumber()));
      shareTicketItemToAll(ticketItem, viewNumbers);
      shareModifiersToAll(ticketItem, splitQuantity);
    }
    else {
      splitQuantity = 1;
      if ((ticketItem.isHasModifiers().booleanValue()) && (splitQuantity < 1)) {
        return;
      }
      if (ticketItem.isTreatAsSeat().booleanValue()) {
        transferSeatTicketItems(ticketItem, fromView, toView);
      }
      else {
        TicketItem seatTicketItem = getSelectedSeatTicketItem(ticketItem);
        if (seatTicketItem != null) {
          List<TicketItem> viewToticketItems = toView.getTicket().getTicketItems();
          if ((viewToticketItems.isEmpty()) || 
            (seatTicketItem.getSeatNumber().intValue() != ((TicketItem)viewToticketItems.get(viewToticketItems.size() - 1)).getSeatNumber().intValue())) {
            TicketItem newSeat = createNewTicketItem(seatTicketItem);
            newSeat.setTicket(toView.getTicket());
            toView.getTicket().addToticketItems(newSeat);
          }
        }
        transferTicketItem(ticketItem, fromView, toView, false, quantity);
        doRemoveEmptySeat(seatTicketItem, fromView.getTicket());
      }
    }
    fromView.updateModel();
    fromView.getTicketViewerTable().updateView();
    fromView.updateView();
    toView.updateModel();
    toView.getTicketViewerTable().updateView();
    toView.updateView();
  }
  
  private void doRemoveEmptySeat(TicketItem ticketItem, Ticket fromTicket) {
    boolean itemFound = false;
    boolean removeSeat = true;
    for (TicketItem item : fromTicket.getTicketItems()) {
      if (item == ticketItem) {
        itemFound = true;

      }
      else if (itemFound) {
        if (item.isTreatAsSeat().booleanValue()) break;
        removeSeat = false; break;
      }
    }
    

    if (removeSeat) {
      fromTicket.getTicketItems().remove(ticketItem);
    }
  }
  
  protected TicketItem getSelectedSeatTicketItem(TicketItem item) {
    for (TicketItem ticketItem : item.getTicket().getTicketItems()) {
      if (ticketItem.isTreatAsSeat().booleanValue())
      {
        if (ticketItem.getSeatNumber().intValue() == item.getSeatNumber().intValue())
          return ticketItem;
      }
    }
    return null;
  }
  
  private void transferSeatTicketItems(TicketItem ticketItem, TicketSplitView fromView, TicketSplitView toView) {
    boolean itemFound = false;
    for (Iterator iterator = fromView.getTicket().getTicketItems().iterator(); iterator.hasNext();) {
      TicketItem item = (TicketItem)iterator.next();
      if (item == ticketItem) {
        itemFound = true;
        TicketItem newTicketItem = createNewTicketItem(item);
        newTicketItem.setTicket(toView.getTicket());
        toView.getTicket().addToticketItems(newTicketItem);
        iterator.remove();

      }
      else if (itemFound) {
        if (item.isTreatAsSeat().booleanValue()) break;
        TicketItem newTicketItem = createNewTicketItem(item);
        newTicketItem.setTicket(toView.getTicket());
        toView.getTicket().addToticketItems(newTicketItem);
        iterator.remove();
      }
    }
  }
  



  private void shareTicketItemToAll(TicketItem ticketItem)
  {
    shareTicketItemToAll(ticketItem, null);
  }
  
  private void shareTicketItemToAll(TicketItem ticketItem, List<Integer> viewNumbers) {
    Double quantityValue = null;
    if (viewNumbers != null) {
      quantityValue = Double.valueOf(NumberUtil.roundToTwoDigit(ticketItem.getQuantity().doubleValue() / viewNumbers.size()));
    }
    else {
      quantityValue = Double.valueOf(NumberUtil.roundToTwoDigit(ticketItem.getQuantity().doubleValue() / splitQuantity));
    }
    for (TicketButton ticketButton : ticketMap.values())
      if ((viewNumbers != null) && (viewNumber == 1) && (!viewNumbers.contains(Integer.valueOf(1)))) {
        mainTicketView.getTicket().getTicketItems().remove(ticketItem);

      }
      else if (((viewNumbers == null) || (viewNumbers.contains(Integer.valueOf(viewNumber)))) && 
      
        (viewNumber != 1))
      {

        TicketItem newTicketItem = createNewTicketItem(ticketItem);
        newTicketItem.setFractionalUnit(Boolean.valueOf(true));
        ticketButton.getTicket().addToticketItems(newTicketItem);
        newTicketItem.setTicket(ticketButton.getTicket());
      }
    ticketItem.setQuantity(Double.valueOf(quantityValue.doubleValue() + (ticketItem.getQuantity().doubleValue() - NumberUtil.roundToTwoDigit(quantityValue.doubleValue() * splitQuantity))));
    ticketItem.setInventoryAdjustQty(quantityValue);
    ticketItem.setFractionalUnit(Boolean.valueOf(true));
  }
  



  private void shareModifiersToAll(TicketItem ticketItem, double splitQuantity) {}
  



  public void transferTicketItem(TicketItem ticketItem, TicketSplitView fromTicketView, TicketSplitView toTicketView, boolean fullTicketItem, Double quantity)
  {
    TicketItem newTicketItem = createNewTicketItem(ticketItem);
    double ticketItemQuantity = NumberUtil.roundToTwoDigit(ticketItem.getQuantity().doubleValue());
    
    if (Double.isInfinite(quantity.doubleValue())) {
      return;
    }
    if (quantity.doubleValue() % 1.0D != 0.0D) {
      newTicketItem.setFractionalUnit(Boolean.valueOf(true));
    }
    


    quantity = Double.valueOf(NumberUtil.roundToTwoDigit(quantity.doubleValue()));
    newTicketItem.setMenuItemId(ticketItem.getMenuItemId());
    newTicketItem.setQuantity(quantity);
    toTicketView.getTicketViewerTable().addTicketItem(newTicketItem);
    if ((ticketItemQuantity > quantity.doubleValue()) && (!fullTicketItem)) {
      double itemQuantity = ticketItem.getQuantity().doubleValue();
      ticketItem.setQuantity(Double.valueOf(itemQuantity - quantity.doubleValue()));
      if (ticketItem.getQuantity().doubleValue() % 1.0D != 0.0D) {
        ticketItem.setFractionalUnit(Boolean.valueOf(true));
      }
    }
    else {
      fromTicketView.getTicketViewerTable().delete(ticketItem.getTableRowNum());
    }
    newTicketItem.setInventoryAdjustQty(newTicketItem.getQuantity());
  }
  
  private TicketItem createNewTicketItem(TicketItem ticketItem) {
    try {
      TicketItem newTicketItem = null;
      if ((ticketItem instanceof ModifiableTicketItem)) {
        newTicketItem = new ModifiableTicketItem();
      }
      else {
        newTicketItem = new TicketItem();
      }
      newTicketItem.setTaxIncluded(ticketItem.isTaxIncluded());
      newTicketItem.setMenuItemId(ticketItem.getMenuItemId());
      newTicketItem.setName(ticketItem.getName());
      newTicketItem.setGroupName(ticketItem.getGroupName());
      newTicketItem.setCategoryName(ticketItem.getCategoryName());
      newTicketItem.setUnitName(ticketItem.getUnitName());
      newTicketItem.setUnitPrice(ticketItem.getUnitPrice());
      newTicketItem.setTreatAsSeat(ticketItem.isTreatAsSeat());
      newTicketItem.setSeatNumber(ticketItem.getSeatNumber());
      newTicketItem.setQuantity(Double.valueOf(NumberUtil.roundToTwoDigit(ticketItem.getQuantity().doubleValue() / splitQuantity)));
      newTicketItem.setInventoryAdjustQty(newTicketItem.getQuantity());
      newTicketItem.setFractionalUnit(Boolean.valueOf(true));
      newTicketItem.setDiscountsProperty(ticketItem.getDiscountsProperty());
      












      if (ticketItem.getTicketItemModifiers() != null) {
        for (TicketItemModifier ticketItemModifier : ticketItem.getTicketItemModifiers()) {
          TicketItemModifier newModifier = new TicketItemModifier();
          
          newModifier.setTaxIncluded(ticketItemModifier.isTaxIncluded());
          newModifier.setItemId(ticketItemModifier.getItemId());
          newModifier.setGroupId(ticketItemModifier.getGroupId());
          newModifier.setItemQuantity(ticketItemModifier.getItemQuantity());
          newModifier.setName(ticketItemModifier.getName());
          newModifier.setUnitPrice(ticketItemModifier.getUnitPrice());
          newModifier.setTaxesProperty(ticketItemModifier.getTaxesProperty());
          










          newModifier.setModifierType(ticketItemModifier.getModifierType());
          newModifier.setPrintedToKitchen(ticketItemModifier.isPrintedToKitchen());
          newModifier.setShouldPrintToKitchen(ticketItemModifier.isShouldPrintToKitchen());
          newModifier.setTicketItem(newTicketItem);
          newTicketItem.addToticketItemModifiers(newModifier);
        }
      }
      











      newTicketItem.setTaxesProperty(ticketItem.getTaxesProperty());
      newTicketItem.setBeverage(ticketItem.isBeverage());
      newTicketItem.setShouldPrintToKitchen(ticketItem.isShouldPrintToKitchen());
      newTicketItem.setPrinterGroup(ticketItem.getPrinterGroup());
      newTicketItem.setPrintedToKitchen(ticketItem.isPrintedToKitchen());
      return newTicketItem;
    }
    catch (Exception localException) {}
    return ticketItem;
  }
}
