package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.mailservices.MailService;
import com.floreantpos.model.GiftCard;
import com.floreantpos.model.GiftCertificateTransaction;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.report.GiftCardTransactionInfoModel;
import com.floreantpos.report.ReportUtil;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosOptionPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.payment.GiftCardProcessor;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.util.ByteArrayDataSource;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;







public class GiftCardTransactionInfoView
  extends POSDialog
{
  private JXTable table;
  private BeanTableModel<PosTransaction> tableModel;
  private Pattern pattern;
  private Matcher matcher;
  private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
  private GiftCardProcessor giftCardProcessor;
  private GiftCard giftCard;
  private JXDatePicker fromDatePicker;
  private JXDatePicker toDatePicker;
  private List<PosTransaction> transactionList;
  
  public GiftCardTransactionInfoView(GiftCard giftCard, GiftCardProcessor giftCardProcessor)
  {
    this.giftCardProcessor = giftCardProcessor;
    this.giftCard = giftCard;
    init();
  }
  
  private void init() {
    JPanel searchPanel = new JPanel(new MigLayout("fill"));
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Transaction Info");
    searchPanel.add(titlePanel, "grow,span");
    JLabel lblFromDate = new JLabel("From date: ");
    fromDatePicker = new JXDatePicker(new Date());
    JLabel lblToDate = new JLabel("To date: ");
    toDatePicker = new JXDatePicker(new Date());
    JButton btnSearch = new JButton("Search");
    searchPanel.add(lblFromDate, "split 5");
    searchPanel.add(fromDatePicker, "gapright 10");
    searchPanel.add(lblToDate);
    searchPanel.add(toDatePicker, "gapright 10");
    searchPanel.add(btnSearch);
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        GiftCardTransactionInfoView.this.doShowTransactions();
      }
      
    });
    add(searchPanel, "North");
    pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    JPanel centerPanel = new JPanel(new MigLayout("fill"));
    tableModel = new BeanTableModel(PosTransaction.class);
    tableModel.addColumn("TRANSACTION TIME", "transactionTime");
    tableModel.addColumn("AMOUNT", "amount");
    
    table = new JXTable(tableModel);
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.setRowHeight(30);
    
    table.getColumnModel().getColumn(0).setCellRenderer(new DefaultTableCellRenderer()
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if ((value instanceof Date)) {
          return super.getTableCellRendererComponent(table, DateUtil.formatReportDateAsString((Date)value), isSelected, hasFocus, row, column);
        }
        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      }
      
    });
    table.getColumnModel().getColumn(1).setCellRenderer(new DefaultTableCellRenderer()
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if ((value instanceof Double)) {
          PosTransaction transaction = (PosTransaction)tableModel.getRow(row);
          if ((transaction instanceof GiftCertificateTransaction)) {
            double amount = ((Double)value).doubleValue();
            return super.getTableCellRendererComponent(table, NumberUtil.formatNumberAcceptNegative(Double.valueOf(amount * -1.0D)), isSelected, hasFocus, row, column);
          }
        }
        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      }
      
    });
    centerPanel.add(new JScrollPane(table), "grow");
    
    add(centerPanel, "Center");
    
    resizeColumnWidth(table);
    
    JPanel buttonPanel = new JPanel(new MigLayout("al center", "sg, fill", ""));
    
    PosButton btnPrint = new PosButton(Messages.getString("GiftCardTransactionInfoView.9"));
    buttonPanel.add(btnPrint, "grow");
    
    btnPrint.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        doPrint();
      }
      
    });
    PosButton btnEmail = new PosButton(Messages.getString("GiftCardTransactionInfoView.11"));
    buttonPanel.add(btnEmail, "grow");
    
    btnEmail.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        doEmail();
      }
      
    });
    buttonPanel.add(new PosButton(new CloseDialogAction(this, Messages.getString("GiftCardTransactionInfoView.13"))));
    add(buttonPanel, "South");
  }
  
  private void doShowTransactions() {
    Date fromDate = fromDatePicker.getDate();
    Date toDate = toDatePicker.getDate();
    
    if (fromDate.after(toDate)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      return;
    }
    transactionList = giftCardProcessor.getTransactionList(giftCard.getCardNumber(), fromDate, toDate);
    
    if ((transactionList == null) || (transactionList.isEmpty())) {
      tableModel.removeAll();
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("GiftCardTransactionDialog.15"));
      return;
    }
    
    tableModel.setRows(transactionList);
  }
  
  public void doPrint() {
    JasperPrint reportCreation = reportCreation();
    if (reportCreation == null) {
      return;
    }
    GiftCardTransactionInfoDialog reportDialog = new GiftCardTransactionInfoDialog(reportCreation);
    reportDialog.setTitle(Messages.getString("GiftCardTransactionInfoView.14"));
    reportDialog.setDefaultCloseOperation(2);
    reportDialog.setSize(PosUIManager.getSize(900, 600));
    reportDialog.setLocationRelativeTo(POSUtil.getFocusedWindow());
    reportDialog.setVisible(true);
  }
  
  public void doEmail()
  {
    String email = PosOptionPane.showInputDialog(Messages.getString("GiftCardTransactionInfoView.15"));
    if (email == null) {
      return;
    }
    
    matcher = pattern.matcher(email);
    if (matcher.matches())
    {
      JasperPrint reportCreation = reportCreation();
      if (reportCreation == null) {
        return;
      }
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      
      JRPdfExporter exporter = new JRPdfExporter();
      exporter.setParameter(JRPdfExporterParameter.JASPER_PRINT, reportCreation);
      exporter.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
      try {
        exporter.exportReport();
      } catch (JRException e) {
        PosLog.error(getClass(), e);
      }
      

      byte[] bytes = byteArrayOutputStream.toByteArray();
      ByteArrayDataSource bads = new ByteArrayDataSource(bytes, "application/pdf");
      MailService.sendMail(email, Messages.getString("GiftCardTransactionInfoView.0"), Messages.getString("GiftCardTransactionInfoView.18"), "GiftCardTransactionReport.pdf", bads);
      
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("GiftCardTransactionInfoView.20"));
    }
    else {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), Messages.getString("GiftCardTransactionInfoView.21"));
      doEmail();
    }
  }
  





  public void resizeColumnWidth(JTable table)
  {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(200));
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(150));
    columnWidth.add(Integer.valueOf(180));
    columnWidth.add(Integer.valueOf(200));
    columnWidth.add(Integer.valueOf(300));
    
    return columnWidth;
  }
  
  public JasperPrint reportCreation() {
    JasperPrint print = null;
    try {
      if ((transactionList == null) || (transactionList.isEmpty())) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "No transaction found!");
        return null;
      }
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(new Date());
      Map<String, Object> parameters = new HashMap();
      parameters.put("cardNo", giftCard.getCardNumber() != null ? giftCard.getCardNumber() : "");
      parameters.put("cardOwner", giftCard.getOwnerName() != null ? giftCard.getOwnerName() : "");
      parameters.put("endBalance", giftCard.getBalance() != null ? NumberUtil.formatNumber(giftCard.getBalance()) : "");
      parameters.put("reportTime", calendar.getTime());
      parameters.put("terminalName", String.valueOf(TerminalConfig.getTerminalId()));
      parameters.put("currency", CurrencyUtil.getCurrencySymbol());
      JasperReport report = ReportUtil.getReport("gift_card_transaction_report");
      GiftCardTransactionInfoModel reportModel = new GiftCardTransactionInfoModel();
      reportModel.setRows(transactionList);
      print = JasperFillManager.fillReport(report, parameters, new JRTableModelDataSource(reportModel));
    } catch (Exception e) {
      PosLog.info(getClass(), "" + e);
    }
    return print;
  }
}
