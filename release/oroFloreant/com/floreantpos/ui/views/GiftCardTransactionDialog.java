package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.model.GiftCard;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.views.payment.GiftCardProcessor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;




public class GiftCardTransactionDialog
  extends POSDialog
{
  private JTextField txtCardNumber;
  private GiftCard giftCard;
  private GiftCardProcessor giftCardProcessor;
  
  public GiftCardTransactionDialog(GiftCardProcessor giftCardProcessor)
  {
    this.giftCardProcessor = giftCardProcessor;
    init();
  }
  

  public GiftCardTransactionDialog(JFrame parent)
  {
    init();
  }
  
  private void init() {
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Enter Card Number");
    add(titlePanel, "North");
    
    JPanel centerPanel = new JPanel(new MigLayout("fillx,aligny center", "[]20px[]", ""));
    
    JLabel lblNumberOfCard = new JLabel(Messages.getString("GiftCardTransactionDialog.3"));
    
    txtCardNumber = new JTextField(20);
    
    centerPanel.add(lblNumberOfCard, "cell 0 0, alignx right");
    centerPanel.add(txtCardNumber, "cell 1 0");
    
    add(centerPanel, "Center");
    
    JPanel buttonPanel = new JPanel(new MigLayout("al center", "sg, fill", ""));
    
    PosButton btnGenerate = new PosButton(Messages.getString("GiftCardTransactionDialog.9"));
    buttonPanel.add(btnGenerate, "grow");
    
    btnGenerate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doGenerate();
      }
      

    });
    buttonPanel.add(new PosButton(new CloseDialogAction(this, Messages.getString("GiftCardTransactionDialog.11"))));
    
    add(buttonPanel, "South");
  }
  
  public void doGenerate() {}
}
