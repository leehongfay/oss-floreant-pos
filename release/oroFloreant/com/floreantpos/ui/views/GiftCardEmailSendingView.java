package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.main.Application;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;




































public class GiftCardEmailSendingView
  extends POSDialog
{
  private FixedLengthTextField txtEmail;
  
  public GiftCardEmailSendingView()
  {
    super(Application.getPosWindow(), "");
    init();
  }
  

  public GiftCardEmailSendingView(JFrame parent)
  {
    init();
  }
  
  private void init() {
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Send Email");
    add(titlePanel, "North");
    
    JPanel centerPanel = new JPanel(new MigLayout("fillx,aligny center", "[]20px[]", ""));
    
    JLabel lblEmail = new JLabel(Messages.getString("GiftCardEmailSendingView.4"));
    
    txtEmail = new FixedLengthTextField(20);
    
    centerPanel.add(lblEmail, "cell 0 0, alignx right");
    centerPanel.add(txtEmail, "cell 1 0");
    
    add(centerPanel, "Center");
    
    JPanel buttonPanel = new JPanel(new MigLayout("al center", "sg, fill", ""));
    
    PosButton btnGenerate = new PosButton(Messages.getString("GiftCardEmailSendingView.10"));
    buttonPanel.add(btnGenerate, "grow");
    
    btnGenerate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doGenerate();
      }
      

    });
    buttonPanel.add(new PosButton(new CloseDialogAction(this, Messages.getString("GiftCardEmailSendingView.12"))));
    
    add(buttonPanel, "South");
  }
  
  public void doGenerate()
  {
    String email = txtEmail.getText();
    if (StringUtils.isEmpty(email)) {
      MessageDialog.showError(Messages.getString("GiftCardEmailSendingView.13"));
      return;
    }
    setCanceled(false);
    dispose();
  }
}
