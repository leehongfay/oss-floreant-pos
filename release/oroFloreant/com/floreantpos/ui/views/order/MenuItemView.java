package com.floreantpos.ui.views.order;

import com.floreantpos.POSConstants;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuPage;
import com.floreantpos.model.MenuPageItem;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.model.dao.MenuPageDAO;
import com.floreantpos.swing.PaginatedListModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.ScrollableFlowPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.actions.ItemSelectionListener;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.hibernate.exception.JDBCConnectionException;





























public class MenuItemView
  extends SelectionView
{
  public static final String VIEW_NAME = "ITEM_VIEW";
  private Vector<ItemSelectionListener> listenerList = new Vector();
  
  private MenuGroup menuGroup;
  
  public MenuItemView()
  {
    super(POSConstants.ITEMS, new FlowLayout(), PosUIManager.getSize(TerminalConfig.getMenuItemButtonWidth()), 
      PosUIManager.getSize(TerminalConfig.getMenuItemButtonHeight()));
    dataModel = new PaginatedListModel();
    
    btnPrev.setText("<< PREVIOUS");
    btnNext.setText("NEXT >>");
  }
  
  public MenuGroup getMenuGroup() {
    return menuGroup;
  }
  
  public void setMenuGroup(MenuGroup menuGroup) {
    this.menuGroup = menuGroup;
    reset();
    if (menuGroup == null) {
      return;
    }
    try {
      Terminal terminal = Application.getInstance().getTerminal();
      OrderType orderType = OrderView.getInstance().getCurrentTicket().getOrderType();
      dataModel.setPageSize(1);
      dataModel.setCurrentRowIndex(0);
      dataModel.setNumRows(MenuPageDAO.getInstance().getRowCount(terminal, menuGroup, orderType));
      MenuPageDAO.getInstance().loadItems(terminal, menuGroup, orderType, null, dataModel);
      setDataModel(dataModel);
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  protected void renderItems()
  {
    reset();
    if (dataModel.getSize() == 0) {
      setTitle(POSConstants.ITEMS);
      revalidate();
      repaint();
      return;
    }
    btnPrev.setText("<< " + POSConstants.CAPITAL_PREV);
    
    MenuPage menuPage = (MenuPage)dataModel.getElementAt(0);
    setTitle(menuPage.getName());
    
    int rows = menuPage.getRows().intValue();
    int cols = menuPage.getCols().intValue();
    
    double widthPercentage = 100.0D / rows;
    double heightPercentage = 100.0D / cols;
    
    MigLayout migLayout = new MigLayout("hidemode 0, center,wrap " + cols);
    if (menuPage.isFlixibleButtonSize().booleanValue()) {
      migLayout.setLayoutConstraints("fill");
      migLayout.setColumnConstraints("fill,grow");
      migLayout.setRowConstraints("fill,grow");
    }
    selectionButtonsPanel.setLayout(migLayout);
    for (int row = 0; row < rows; row++) {
      for (int col = 0; col < cols; col++) {
        String constraint = String.format("cell %s %s", new Object[] { Integer.valueOf(col), Integer.valueOf(row) });
        if (menuPage.isFlixibleButtonSize().booleanValue()) {
          constraint = constraint + String.format(", w %s, h %s", new Object[] { Double.valueOf(widthPercentage), Double.valueOf(heightPercentage) });
        }
        else {
          constraint = constraint + String.format(", w %s!, h %s!", new Object[] { menuPage.getButtonWidth(), menuPage.getButtonHeight() });
        }
        MenuPageItem itemForCell = menuPage.getItemForCell(col, row);
        if (itemForCell == null) {
          selectionButtonsPanel.add(new JLabel(), constraint);


        }
        else if (itemForCell.getMenuItemId() != null)
        {

          if (itemForCell.getMenuItem().isVisible().booleanValue())
          {

            AbstractButton itemButton = createItemButton(itemForCell);
            selectionButtonsPanel.add(itemButton, constraint);
          }
        }
      }
    }
    
    revalidate();
    repaint();
    btnNext.setVisible(dataModel.hasNext());
    btnPrev.setVisible(dataModel.hasPrevious());
  }
  
  public void rendererVariants(MenuItem parentItem)
  {
    reset();
    List<MenuItem> variants = parentItem.getVariants();
    if ((variants == null) || (variants.size() == 0)) {
      setTitle("Variants");
      revalidate();
      repaint();
      return;
    }
    ScrollableFlowPanel flowPanel = new ScrollableFlowPanel();
    selectionButtonsPanel.setLayout(new MigLayout("fill"));
    for (MenuItem variant : variants) {
      AbstractButton itemButton = createItemButton(variant);
      if (itemButton != null) {
        flowPanel.add(itemButton);
      }
    }
    PosScrollPane scrollPane = new PosScrollPane(flowPanel);
    scrollPane.setBorder(null);
    selectionButtonsPanel.add(scrollPane, "span,grow");
    revalidate();
    repaint();
    btnNext.setVisible(false);
    btnPrev.setVisible(true);
    btnPrev.setText("BACK");
    setTitle(parentItem.getName());
  }
  
  protected AbstractButton createItemButton(Object item)
  {
    if ((item instanceof MenuPageItem)) {
      return new PageItemButton((MenuPageItem)item);
    }
    if ((item instanceof MenuItem)) {
      MenuItem menuItem = (MenuItem)item;
      if (!menuItem.isVisible().booleanValue()) {
        return null;
      }
      menuItem.setMenuGroup(menuGroup);
      ItemButton itemButton = new ItemButton(menuItem);
      return itemButton;
    }
    

    return null;
  }
  
  public void addItemSelectionListener(ItemSelectionListener listener) {
    listenerList.add(listener);
  }
  
  public void removeItemSelectionListener(ItemSelectionListener listener) {
    listenerList.remove(listener);
  }
  
  private void fireItemSelected(MenuItem foodItem) {
    for (ItemSelectionListener listener : listenerList) {
      listener.itemSelected(foodItem);
    }
  }
  
  protected void scrollDown()
  {
    dataModel.setCurrentRowIndex(dataModel.getNextRowIndex());
    OrderType orderType = OrderView.getInstance().getCurrentTicket().getOrderType();
    MenuPageDAO.getInstance().loadItems(Application.getInstance().getTerminal(), menuGroup, orderType, Boolean.valueOf(false), dataModel);
    setDataModel(dataModel);
  }
  
  protected void scrollUp()
  {
    if (btnPrev.getText().equals("BACK")) {
      renderItems();
      return;
    }
    dataModel.setCurrentRowIndex(dataModel.getPreviousRowIndex());
    OrderType orderType = OrderView.getInstance().getCurrentTicket().getOrderType();
    MenuPageDAO.getInstance().loadItems(Application.getInstance().getTerminal(), menuGroup, orderType, Boolean.valueOf(false), dataModel);
    setDataModel(dataModel);
  }
  
  private class ItemButton extends PosButton implements ActionListener, MouseListener {
    MenuItem menuItem;
    
    ItemButton(MenuItem menuItem) {
      this.menuItem = menuItem;
      if (menuItem == null) {
        setVisible(false);
        return;
      }
      setVerticalTextPosition(3);
      setHorizontalTextPosition(0);
      setPreferredSize(getButtonSize());
      
      ImageIcon image = menuItem.getImage(getButtonSize().width, menuItem.isShowImageOnly().booleanValue() ? getButtonSize().height : 100);
      String buttonText = "<html><body><center>" + menuItem.getDisplayName() + "</center></body></html>";
      if (image != null) {
        if (menuItem.isShowImageOnly().booleanValue()) {
          setIcon(image);
        }
        else {
          setIcon(image);
          setText(buttonText);
        }
      }
      else {
        setText(buttonText);
      }
      
      Color buttonColor = menuItem.getButtonColor();
      if (buttonColor != null) {
        setBackground(buttonColor);
      }
      Color textColor = menuItem.getTextColor();
      if (textColor != null) {
        setForeground(textColor);
      }
      
      addActionListener(this);
      addMouseListener(this);
    }
    
    public void actionPerformed(ActionEvent e) {
      if (OrderView.getInstance().is_86Mode()) {
        return;
      }
      try {
        MenuItemView.this.fireItemSelected(menuItem);
      } catch (JDBCConnectionException x) {
        POSMessageDialog.showError("Database connection lost. Please try again.");
      }
    }
    
    public void mouseClicked(MouseEvent e)
    {
      if (OrderView.getInstance().is_86Mode()) {
        menuItem.setEnable(Boolean.valueOf(!menuItem.isEnable().booleanValue()));
        setEnabled(menuItem.isEnable().booleanValue());
        MenuItemDAO.getInstance().update(menuItem);
        return;
      }
    }
    

    public void mousePressed(MouseEvent e) {}
    

    public void mouseReleased(MouseEvent e) {}
    

    public void mouseEntered(MouseEvent e) {}
    

    public void mouseExited(MouseEvent e) {}
  }
  
  private class PageItemButton
    extends PosButton
    implements ActionListener, MouseListener
  {
    MenuItem menuItem;
    MenuPageItem pageItem;
    
    PageItemButton(MenuPageItem pageItem)
    {
      this.pageItem = pageItem;
      if (pageItem == null) {
        setVisible(false);
        return;
      }
      setVerticalTextPosition(3);
      setHorizontalTextPosition(0);
      setPreferredSize(getButtonSize());
      
      ImageIcon image = pageItem.getImage(getButtonSize().width, pageItem.isShowImageOnly().booleanValue() ? getButtonSize().height : 100);
      String buttonText = "<html><body><center>" + pageItem.getMenuItemName() + "</center></body></html>";
      if (image != null) {
        if (pageItem.isShowImageOnly().booleanValue()) {
          setIcon(image);
        }
        else
        {
          setIcon(image);
          setText(buttonText);
        }
      }
      else {
        setText(buttonText);
      }
      
      Color buttonColor = pageItem.getButtonColor();
      if (buttonColor != null) {
        setBackground(buttonColor);
      }
      Color textColor = pageItem.getTextColor();
      if (textColor != null) {
        setForeground(textColor);
      }
      
      addActionListener(this);
      addMouseListener(this);
    }
    
    public void actionPerformed(ActionEvent e) {
      if (OrderView.getInstance().is_86Mode()) {
        return;
      }
      try {
        if (menuItem == null) {
          menuItem = new MenuItem(pageItem.getMenuItemId());
        }
        MenuItemView.this.fireItemSelected(menuItem);
      } catch (JDBCConnectionException x) {
        POSMessageDialog.showError("Database connection lost. Please try again.");
      }
    }
    
    public void mouseClicked(MouseEvent e)
    {
      if (OrderView.getInstance().is_86Mode()) {
        MenuItem menuItem = pageItem.getMenuItem();
        menuItem.setEnable(Boolean.valueOf(!menuItem.isEnable().booleanValue()));
        setEnabled(menuItem.isEnable().booleanValue());
        MenuItemDAO.getInstance().update(menuItem);
        return;
      }
    }
    
    public void mousePressed(MouseEvent e) {}
    
    public void mouseReleased(MouseEvent e) {}
    
    public void mouseEntered(MouseEvent e) {}
    
    public void mouseExited(MouseEvent e) {}
  }
}
