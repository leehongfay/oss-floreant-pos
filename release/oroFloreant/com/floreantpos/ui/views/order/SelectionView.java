package com.floreantpos.ui.views.order;

import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.swing.PaginatedListModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
























public abstract class SelectionView
  extends JPanel
{
  private static final int HORIZONTAL_GAP = 5;
  private static final int VERTICAL_GAP = 5;
  protected PaginatedListModel dataModel;
  protected Dimension buttonSize;
  protected JPanel selectionButtonsPanel;
  protected TitledBorder border;
  protected JPanel actionButtonPanel = new JPanel(new MigLayout("fill,hidemode 3, ins 2", "sg, fill", ""));
  
  protected PosButton btnNext;
  
  protected PosButton btnPrev;
  private boolean initialized = false;
  
  public SelectionView(String title, LayoutManager buttonPanelLayout, int buttonWidth, int buttonHeight) {
    selectionButtonsPanel = new JPanel(buttonPanelLayout);
    buttonSize = new Dimension(buttonWidth, buttonHeight);
    
    setLayout(new BorderLayout(5, 5));
    
    border = new TitledBorder(title);
    border.setTitleJustification(2);
    setBorder(new CompoundBorder(border, new EmptyBorder(2, 2, 2, 2)));
    
    add(selectionButtonsPanel);
    
    btnPrev = new PosButton();
    btnPrev.setText(POSConstants.CAPITAL_PREV + "<<");
    actionButtonPanel.add(btnPrev, "grow, align center");
    
    btnNext = new PosButton();
    btnNext.setText(POSConstants.CAPITAL_NEXT + ">>");
    actionButtonPanel.add(btnNext, "grow, align center");
    
    add(actionButtonPanel, "South");
    
    ScrollAction action = new ScrollAction(null);
    btnPrev.addActionListener(action);
    btnNext.addActionListener(action);
    
    btnNext.setVisible(false);
    btnPrev.setVisible(false);
  }
  
  public void setTitle(String title) {
    border.setTitle(title);
  }
  
  public void setDataModel(PaginatedListModel items) {
    dataModel = items;
    renderItems();
  }
  
  protected void updateButton() {
    boolean hasNext = dataModel.hasNext();
    boolean hasPrevious = dataModel.hasPrevious();
    btnNext.setVisible((hasNext) || (hasPrevious));
    btnPrev.setVisible((hasNext) || (hasPrevious));
    btnPrev.setEnabled(hasPrevious);
    btnNext.setEnabled(hasNext);
  }
  
  public PaginatedListModel getDataModel() {
    return dataModel;
  }
  
  public Dimension getButtonSize() {
    return buttonSize;
  }
  
  public void setButtonSize(Dimension buttonSize) {
    this.buttonSize = buttonSize;
  }
  
  protected abstract AbstractButton createItemButton(Object paramObject);
  
  public void reset() {
    Component[] components = selectionButtonsPanel.getComponents();
    for (Component component : components) {
      if ((component instanceof AbstractButton)) {
        AbstractButton button = (AbstractButton)component;
        ActionListener[] actionListeners = button.getActionListeners();
        for (ActionListener actionListener : actionListeners) {
          button.removeActionListener(actionListener);
        }
      }
    }
    selectionButtonsPanel.removeAll();
    selectionButtonsPanel.repaint();
    btnNext.setVisible(false);
    btnPrev.setVisible(false);
  }
  
  protected int getHorizontalButtonCount() {
    Dimension size = selectionButtonsPanel.getSize();
    Dimension itemButtonSize = getButtonSize();
    
    return getButtonCount(width, width);
  }
  
  protected int getFitableButtonCount() {
    Dimension size = selectionButtonsPanel.getSize();
    Insets insets = selectionButtonsPanel.getInsets();
    Dimension itemButtonSize = getButtonSize();
    
    int horizontalButtonCount = getButtonCount(width - left - right, width);
    int verticalButtonCount = getButtonCount(height - 50 - top - bottom, height);
    
    int totalItem = horizontalButtonCount * verticalButtonCount;
    
    return totalItem;
  }
  
  protected void renderItems() {
    try {
      reset();
      
      if ((dataModel == null) || (dataModel.getSize() == 0)) {
        return;
      }
      
      for (int i = 0; i < dataModel.getSize(); i++) {
        Object item = dataModel.getElementAt(i);
        AbstractButton itemButton = createItemButton(item);
        if (itemButton != null)
        {


          selectionButtonsPanel.add(itemButton); }
      }
      btnNext.setVisible(dataModel.hasNext());
      btnPrev.setVisible(dataModel.hasPrevious());
      revalidate();
      repaint();
    } catch (Exception e) {
      initialized = false;
      POSMessageDialog.showError(this, e.getMessage(), e);
    }
  }
  


  protected void scrollDown() {}
  


  protected void scrollUp() {}
  


  private class ScrollAction
    implements ActionListener
  {
    private ScrollAction() {}
    

    public void actionPerformed(ActionEvent e)
    {
      try
      {
        Object source = e.getSource();
        if (source == btnPrev) {
          scrollUp();
        }
        else if (source == btnNext) {
          scrollDown();
        }
      } catch (Exception e2) {
        POSMessageDialog.showError(Application.getPosWindow(), e2.getMessage(), e2);
      }
    }
  }
  
  public JPanel getButtonsPanel()
  {
    return selectionButtonsPanel;
  }
  
  public AbstractButton getFirstItemButton() {
    int componentCount = selectionButtonsPanel.getComponentCount();
    if (componentCount == 0) {
      return null;
    }
    return (AbstractButton)selectionButtonsPanel.getComponent(0);
  }
  
  protected int getButtonCount(int containerSize, int itemSize) {
    int buttonCount = containerSize / (itemSize + 10);
    return buttonCount;
  }
  
  public boolean isInitialized() {
    return initialized;
  }
  
  public void setInitialized(boolean initialized) {
    this.initialized = initialized;
  }
}
