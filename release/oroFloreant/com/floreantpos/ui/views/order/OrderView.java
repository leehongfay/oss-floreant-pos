package com.floreantpos.ui.views.order;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.actions.SeatSelectionAction;
import com.floreantpos.actions.VoidTicketAction;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.customer.CustomerSelectorDialog;
import com.floreantpos.customer.CustomerSelectorFactory;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.extension.OrderServiceFactory;
import com.floreantpos.main.Application;
import com.floreantpos.model.Customer;
import com.floreantpos.model.Gratuity;
import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.SalesArea;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemSeat;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.dao.ShopTableStatusDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.MiscTicketItemDialog;
import com.floreantpos.ui.dialog.NumberSelectionDialog2;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.PasswordEntryDialog;
import com.floreantpos.ui.dialog.ReorderDialog;
import com.floreantpos.ui.dialog.SalesAreaSelectionDialog;
import com.floreantpos.ui.order.CourseOrganizeDialog;
import com.floreantpos.ui.tableselection.TableSelectorDialog;
import com.floreantpos.ui.tableselection.TableSelectorFactory;
import com.floreantpos.ui.ticket.TicketViewerTable;
import com.floreantpos.ui.views.order.actions.TicketEditListener;
import com.floreantpos.ui.views.payment.PaymentListener;
import com.floreantpos.ui.views.payment.PaymentView;
import com.floreantpos.ui.views.payment.SettleTicketProcessor;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.DrawerUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;
import org.hibernate.StaleStateException;





























public class OrderView
  extends ViewPanel
  implements PaymentListener, TicketEditListener
{
  private HashMap<String, JComponent> views = new HashMap();
  private SettleTicketProcessor ticketProcessor = new SettleTicketProcessor(Application.getCurrentUser());
  
  public static final String VIEW_NAME = "ORDER_VIEW";
  
  private static OrderView instance;
  private Ticket currentTicket;
  private CategoryView categoryView = new CategoryView();
  private GroupView groupView = new GroupView();
  private MenuItemView itemView = new MenuItemView();
  private TicketView ticketView = new TicketView();
  
  private TransparentPanel midContainer = new TransparentPanel(new BorderLayout(5, 5));
  private JPanel ticketViewContainer = new JPanel(new BorderLayout());
  private JPanel ticketSummaryView = createTicketSummeryPanel();
  
  private OrderController orderController = new OrderController(this);
  
  private JPanel actionButtonPanel = new JPanel(new MigLayout("fill, ins 2, hidemode 3", "fill", "fill"));
  
  private PosButton btnHold = new PosButton(POSConstants.HOLD_BUTTON_TEXT);
  private PosButton btnDone = new PosButton(POSConstants.SAVE_BUTTON_TEXT);
  private PosButton btnSend = new PosButton(POSConstants.SEND_TO_KITCHEN);
  private PosButton btnCancel = new PosButton(POSConstants.CANCEL_BUTTON_TEXT);
  private PosButton btnGuestNo = new PosButton(POSConstants.GUEST_NO_BUTTON_TEXT);
  private PosButton btnSeatNo = new PosButton(Messages.getString("OrderView.3"));
  private PosButton btnMisc = new PosButton(POSConstants.MISC_BUTTON_TEXT);
  private PosButton btnOrderType = new PosButton(POSConstants.ORDER_TYPE_BUTTON_TEXT);
  private PosButton btnTableNumber = new PosButton(POSConstants.TABLE_NO_BUTTON_TEXT);
  private PosButton btnCustomer = new PosButton(POSConstants.CUSTOMER_SELECTION_BUTTON_TEXT);
  private PosButton btnReorder = new PosButton("REORDER");
  private PosButton btnOrganizeCourse = new PosButton(POSConstants.ORGANIZE_COURSE.toUpperCase());
  
  private PosButton btnDeliveryInfo = new PosButton("DELIVERY INFO");
  private PosButton btnSalesArea = new PosButton("SALES AREA");
  private SeatSelectionAction seatAction = new SeatSelectionAction();
  

  private PosButton btnDiscount = new PosButton(Messages.getString("TicketView.43"));
  
  private boolean _86Mode;
  private boolean returnMode;
  private JTextField tfSubtotal;
  private JTextField tfDiscount;
  private JTextField tfDeliveryCharge;
  private JTextField tfTax;
  private JTextField tfGratuity;
  private JTextField tfTotal;
  private PaymentView paymentView;
  
  private OrderView()
  {
    initComponents();
    ticketView.getTicketViewerTable().addTicketUpdateListener(this);
    orderController.addTicketUpdateListener(this);
    ticketProcessor.addPaymentListener(this);
  }
  
  public void addView(String viewName, JComponent view) {
    JComponent oldView = (JComponent)views.get(viewName);
    if (oldView != null) {
      return;
    }
    midContainer.add(view, viewName);
  }
  





  private void initComponents()
  {
    setOpaque(false);
    setLayout(new BorderLayout(2, 1));
    
    JPanel ticketViewInnerCon = new JPanel(new BorderLayout());
    ticketViewInnerCon.add(ticketView);
    ticketViewInnerCon.add(ticketSummaryView, "South");
    ticketViewContainer.add(ticketViewInnerCon);
    
    midContainer.setOpaque(false);
    midContainer.setBorder(null);
    midContainer.add(groupView, "North");
    midContainer.add(itemView);
    









    addActionButtonPanel();
    btnOrderType.setVisible(false);
    showView("VIEW_EMPTY");
  }
  
  private void handleTicketItemSelection() {
    ITicketItem selectedItem = ticketView.getTicketViewerTable().getSelected();
    



















    if (selectedItem == null) {
      btnDiscount.setEnabled(false);
    }
    else
    {
      btnDiscount.setEnabled(selectedItem.canAddDiscount());
    }
  }
  
  private void addActionButtonPanel()
  {
    ticketView.getTicketViewerTable().getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
          OrderView.this.handleTicketItemSelection();
        }
      }
    });
    btnSalesArea.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        doAddSalesArea();
      }
      
    });
    btnDone.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          ticketView.doFinishOrder();
        }
        catch (StaleStateException x) {
          POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("OrderView.0"));
          return;
        } catch (PosException x) {
          POSMessageDialog.showError(x.getMessage());
        } catch (Exception x) {
          POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent evt) {
        if (ticketView.isCancelable()) {
          ticketView.doCancelOrder();
          return;
        }
        
        int result = POSMessageDialog.showYesNoQuestionDialog(null, Messages.getString("OrderView.4"), Messages.getString("OrderView.7"));
        if (result != 0) {
          return;
        }
        VoidTicketAction action = new VoidTicketAction(currentTicket);
        action.execute();
        if (currentTicket.isVoided().booleanValue()) {
          ticketView.doCancelOrder();
          ticketView.setAllowToLogOut(true);
        }
        
      }
    });
    btnSend.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        OrderView.this.doSendTicketToKitchen();
      }
      

    });
    btnReorder.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        OrderView.this.doReorder();
      }
      
    });
    btnOrganizeCourse.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        OrderView.this.doOrganizeTicketItemsByCourse();
      }
      
    });
    btnOrderType.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent evt) {}


    });
    btnCustomer.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        doAddEditCustomer();
      }
      
    });
    btnMisc.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        doInsertMisc(evt);
      }
      
    });
    btnGuestNo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        btnCustomerNumberActionPerformed();
      }
      
    });
    seatAction.setSource(btnSeatNo);
    btnSeatNo.setAction(seatAction);
    
    btnTableNumber.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        doSetTables();
      }
      
    });
    btnHold.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        OrderView.this.doHoldOrder();



      }
      




    });
    btnDiscount.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        addDiscount();
      }
      
    });
    btnDeliveryInfo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        doShowDeliveryDialog();
      }
      
    });
    actionButtonPanel.add(btnOrderType);
    actionButtonPanel.add(btnCustomer);
    actionButtonPanel.add(btnDeliveryInfo);
    actionButtonPanel.add(btnTableNumber);
    actionButtonPanel.add(btnGuestNo);
    actionButtonPanel.add(btnSeatNo);
    actionButtonPanel.add(btnSalesArea);
    actionButtonPanel.add(btnOrganizeCourse);
    actionButtonPanel.add(btnMisc);
    actionButtonPanel.add(btnHold);
    actionButtonPanel.add(btnSend);
    actionButtonPanel.add(btnReorder);
    actionButtonPanel.add(btnCancel);
    actionButtonPanel.add(btnDone);
    
    btnDeliveryInfo.setVisible(false);
  }
  
  private void doOrganizeTicketItemsByCourse() {
    if (DataProvider.get().getCourses().isEmpty()) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Course not found.");
      return;
    }
    CourseOrganizeDialog dialog = new CourseOrganizeDialog(currentTicket);
    dialog.setSize(950, 650);
    dialog.open();
    
    if (dialog.isCanceled())
      return;
    ticketView.getTicketViewerTable().updateView();
  }
  
  private JPanel createTicketSummeryPanel() {
    JLabel lblSubtotal = new JLabel();
    lblSubtotal.setHorizontalAlignment(4);
    lblSubtotal.setText(POSConstants.SUBTOTAL + ":" + " " + CurrencyUtil.getCurrencySymbol());
    
    tfSubtotal = new JTextField(10);
    tfSubtotal.setHorizontalAlignment(11);
    tfSubtotal.setEditable(false);
    
    JLabel lblDiscount = new JLabel();
    lblDiscount.setHorizontalAlignment(4);
    lblDiscount.setText(Messages.getString("TicketView.9") + " " + CurrencyUtil.getCurrencySymbol());
    
    tfDiscount = new JTextField(10);
    
    tfDiscount.setHorizontalAlignment(11);
    tfDiscount.setEditable(false);
    

    JLabel lblDeliveryCharge = new JLabel();
    lblDeliveryCharge.setHorizontalAlignment(4);
    lblDeliveryCharge.setText("Delivery Charge: " + CurrencyUtil.getCurrencySymbol());
    
    tfDeliveryCharge = new JTextField(10);
    tfDeliveryCharge.setHorizontalAlignment(11);
    tfDeliveryCharge.setEditable(false);
    
    JLabel lblTax = new JLabel();
    lblTax.setHorizontalAlignment(4);
    lblTax.setText(POSConstants.TAX + ":" + " " + CurrencyUtil.getCurrencySymbol());
    
    tfTax = new JTextField(10);
    
    tfTax.setEditable(false);
    tfTax.setHorizontalAlignment(11);
    
    JLabel lblGratuity = new JLabel();
    lblGratuity.setHorizontalAlignment(4);
    lblGratuity.setText(Messages.getString("SettleTicketDialog.5") + ":" + " " + CurrencyUtil.getCurrencySymbol());
    
    tfGratuity = new JTextField(10);
    tfGratuity.setEditable(false);
    tfGratuity.setHorizontalAlignment(11);
    
    JLabel lblTotal = new JLabel();
    lblTotal.setFont(lblTotal.getFont().deriveFont(1, PosUIManager.getFontSize(18)));
    lblTotal.setHorizontalAlignment(4);
    lblTotal.setText(POSConstants.TOTAL + ":" + " " + CurrencyUtil.getCurrencySymbol());
    
    tfTotal = new JTextField(10);
    tfTotal.setFont(tfTotal.getFont().deriveFont(1, PosUIManager.getFontSize(18)));
    tfTotal.setHorizontalAlignment(11);
    tfTotal.setEditable(false);
    
    JPanel ticketAmountPanel = new TransparentPanel(new MigLayout("hidemode 3,ins 2 2 3 2,alignx trailing,fill", "[grow]2[]", ""));
    
    ticketAmountPanel.add(lblSubtotal, "growx,aligny center");
    ticketAmountPanel.add(tfSubtotal, "growx,aligny center");
    ticketAmountPanel.add(lblDiscount, "newline,growx,aligny center");
    ticketAmountPanel.add(tfDiscount, "growx,aligny center");
    ticketAmountPanel.add(lblTax, "newline,growx,aligny center");
    ticketAmountPanel.add(tfTax, "growx,aligny center");
    



    ticketAmountPanel.add(lblGratuity, "newline,growx,aligny center");
    ticketAmountPanel.add(tfGratuity, "growx,aligny center");
    ticketAmountPanel.add(lblTotal, "newline,growx,aligny center");
    ticketAmountPanel.add(tfTotal, "growx,aligny center");
    
    return ticketAmountPanel;
  }
  








  public void updateTicketSummeryView()
  {
    Ticket ticket = ticketView.getTicket();
    if (ticket == null) {
      tfSubtotal.setText("");
      tfDiscount.setText("");
      tfDeliveryCharge.setText("");
      tfTax.setText("");
      tfTotal.setText("");
      tfGratuity.setText("");
      return;
    }
    tfSubtotal.setText(NumberUtil.formatNumber(ticket.getSubtotalAmount()));
    tfDiscount.setText(NumberUtil.formatNumber(ticket.getDiscountAmount()));
    tfDeliveryCharge.setText(NumberUtil.formatNumber(ticket.getDeliveryCharge()));
    
    if (Application.getInstance().isPriceIncludesTax()) {
      tfTax.setText(Messages.getString("TicketView.35"));
    }
    else {
      tfTax.setText(NumberUtil.formatNumber(ticket.getTaxAmount()));
    }
    if (ticket.getGratuity() != null) {
      tfGratuity.setText(NumberUtil.formatNumber(ticket.getGratuity().getAmount()));
    }
    else {
      tfGratuity.setText("0.00");
    }
    tfTotal.setText(NumberUtil.formatNumber(ticket.getTotalAmountWithTips()));
  }
  
  protected void doShowDeliveryDialog()
  {
    try
    {
      Customer customer = CustomerDAO.getInstance().findById(currentTicket.getCustomerId());
      OrderServiceFactory.getOrderService().showDeliveryInfo(currentTicket, currentTicket.getOrderType(), customer);
    } catch (StaleStateException x) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("OrderView.0"));
      return;
    } catch (PosException x) {
      POSMessageDialog.showError(x.getMessage());
    } catch (Exception x) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public void doSetTables() {
    try {
      TableSelectorDialog dialog = TableSelectorFactory.createTableSelectorDialog(currentTicket.getOrderType());
      dialog.setCreateNewTicket(false);
      if (currentTicket != null) {
        dialog.setTicket(currentTicket);
      }
      dialog.openUndecoratedFullScreen();
      
      if (dialog.isCanceled()) {
        return;
      }
      List<ShopTable> tables = dialog.getSelectedTables();
      List<Integer> tableNumbers = new ArrayList();
      for (ShopTable shopTable : tables) {
        SalesArea salesArea = shopTable.getSalesArea();
        if (salesArea != null) {
          currentTicket.setSalesArea(salesArea);
          currentTicket.setSalesAreaId(salesArea.getId());
        }
        tableNumbers.add(shopTable.getId());
      }
      ShopTableStatusDAO.getInstance().removeTicketFromShopTableStatus(currentTicket, null);
      currentTicket.setTableNumbers(tableNumbers);
      
      updateView();
    } catch (StaleStateException x) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("OrderView.0"));
      return;
    } catch (PosException x) {
      POSMessageDialog.showError(x.getMessage());
    } catch (Exception x) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  protected void doAddSalesArea() {
    try {
      SalesAreaSelectionDialog dialog = new SalesAreaSelectionDialog();
      dialog.open();
      
      if (dialog.isCanceled()) {
        return;
      }
      SalesArea selectedSalesArea = dialog.getSelectedSalesArea();
      setFocusable(true);
      
      currentTicket.setSalesArea(selectedSalesArea);
      currentTicket.setSalesAreaId(selectedSalesArea.getId());
      btnSalesArea.setText("<html><center>SALES AREA<br/>" + selectedSalesArea.getName() + "</center></html>");
    } catch (StaleStateException x) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("OrderView.0"));
      return;
    } catch (PosException x) {
      POSMessageDialog.showError(x.getMessage());
    } catch (Exception x) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  protected void btnCustomerNumberActionPerformed()
  {
    try {
      Ticket thisTicket = currentTicket;
      int guestNumber = thisTicket.getNumberOfGuests().intValue();
      
      NumberSelectionDialog2 dialog = new NumberSelectionDialog2();
      dialog.setTitle(POSConstants.NUMBER_OF_GUESTS);
      dialog.setValue(guestNumber);
      dialog.pack();
      dialog.open();
      
      if (dialog.isCanceled()) {
        return;
      }
      
      guestNumber = (int)dialog.getValue();
      if (guestNumber == 0) {
        POSMessageDialog.showError(Application.getPosWindow(), POSConstants.GUEST_NUMBER_CANNOT_BE_0);
        return;
      }
      
      thisTicket.setNumberOfGuests(Integer.valueOf(guestNumber));
      updateView();
    } catch (StaleStateException x) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("OrderView.0"));
      return;
    } catch (PosException x) {
      POSMessageDialog.showError(x.getMessage());
    } catch (Exception x) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  protected void doInsertMisc(ActionEvent evt) {
    try {
      MiscTicketItemDialog dialog = new MiscTicketItemDialog();
      
      dialog.setOrderType(currentTicket.getOrderType());
      dialog.pack();
      dialog.open();
      
      if (!dialog.isCanceled()) {
        TicketItem ticketItem = dialog.getTicketItem();
        ticketItem.setTicket(currentTicket);
        ticketItem.calculatePrice();
        ticketView.addTicketItem(ticketItem);
      }
    } catch (StaleStateException x) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("OrderView.0"));
      return;
    } catch (PosException x) {
      POSMessageDialog.showError(x.getMessage());
    } catch (Exception x) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  protected void doAddEditCustomer() {
    try {
      CustomerSelectorDialog dialog = CustomerSelectorFactory.createCustomerSelectorDialog(currentTicket.getOrderType());
      dialog.setCreateNewTicket(false);
      if (currentTicket != null) {
        dialog.setTicket(currentTicket);
      }
      dialog.openUndecoratedFullScreen();
      
      if (!dialog.isCanceled()) {
        Customer selectedCustomer = dialog.getSelectedCustomer();
        currentTicket.setCustomer(selectedCustomer);
        if (selectedCustomer != null) {
          btnCustomer.setText("<html><center>" + POSConstants.CUSTOMER_SELECTION_BUTTON_TEXT + "<br>" + "<small>" + selectedCustomer.getName() + "</small></center></html>");
        }
      }
    } catch (StaleStateException x) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("OrderView.0"));
      return;
    } catch (PosException x) {
      POSMessageDialog.showError(x.getMessage());
    } catch (Exception x) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  protected void addDiscount() {
    try {
      Object selectedObject = ticketView.getTicketViewerTable().getSelected();
      
      if (!(selectedObject instanceof TicketItem)) {
        POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("TicketView.20"));
        return;



      }
      



    }
    catch (StaleStateException x)
    {



      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("OrderView.0"));
      return;
    } catch (PosException x) {
      POSMessageDialog.showError(x.getMessage());
    } catch (Exception x) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public void updateView() {
    Terminal terminal = Application.getInstance().getTerminal();
    if (currentTicket != null) {
      OrderType type = currentTicket.getOrderType();
      
      if (type.isPrepaid().booleanValue()) {
        btnDone.setVisible(false);
      }
      else {
        btnDone.setVisible(true);
      }
      
      if (!type.isShouldPrintToKitchen().booleanValue()) {
        btnSend.setEnabled(false);
      }
      else {
        btnSend.setEnabled(true);
      }
      
      if (!type.isAllowSeatBasedOrder().booleanValue()) {
        btnSeatNo.setVisible(false);
      }
      else {
        btnSeatNo.setVisible(true);
        Integer lastSeatNumber = null;
        Object lastSeat = seatAction.getLastSeat(currentTicket);
        if ((lastSeat instanceof Integer)) {
          lastSeatNumber = (Integer)lastSeat;
          btnSeatNo.putClientProperty("SEAT_NO", lastSeatNumber);
          if ((lastSeatNumber != null) && (lastSeatNumber.intValue() > 0)) {
            btnSeatNo.setText(Messages.getString("OrderView.20") + lastSeatNumber);
          } else {
            btnSeatNo.setText(Messages.getString("OrderView.21"));
          }
        } else if ((lastSeat instanceof TicketItemSeat)) {
          TicketItemSeat ticketItemSeat = (TicketItemSeat)seatAction.getLastSeat(currentTicket);
          btnSeatNo.putClientProperty("SEAT_NO", ticketItemSeat);
          btnSeatNo.setText("SEAT:");
        }
      }
      
      if (!type.isShowTableSelection().booleanValue()) {
        btnGuestNo.setVisible(false);
        btnTableNumber.setVisible(false);
      }
      else {
        btnGuestNo.setVisible(true);
        btnTableNumber.setVisible(true);
        
        if (!terminal.isShowTableNumber()) {
          btnTableNumber.setText("<html><center>" + Messages.getString("OrderView.23") + ": " + currentTicket.getTableNames() + "</center><html/>");
        }
        else {
          List<Integer> tableNumbers = currentTicket.getTableNumbers();
          if (tableNumbers != null) {
            String tables = getTableNumbers(currentTicket.getTableNumbers());
            btnTableNumber.setText("<html><center>" + Messages.getString("OrderView.23") + ": " + tables + "</center><html/>");
          }
          else {
            btnTableNumber.setText(Messages.getString("OrderView.26"));
          }
        }
        
        btnGuestNo.setText(Messages.getString("OrderView.27") + ": " + String.valueOf(currentTicket.getNumberOfGuests()));
      }
      
      if (type.isEnableReorder().booleanValue()) {
        btnReorder.setVisible(true);
      }
      else {
        btnReorder.setVisible(false);
      }
      btnOrganizeCourse.setVisible(type.isEnableCourse().booleanValue());
      
      OrderServiceExtension orderService = (OrderServiceExtension)ExtensionManager.getPlugin(OrderServiceExtension.class);
      if ((orderService != null) && 
        (type.isDelivery().booleanValue()) && (type.isRequiredCustomerData().booleanValue())) {
        btnDeliveryInfo.setVisible(true);
      }
      
      if (type.isRetailOrder().booleanValue()) {
        ticketView.updateView();
        updateTicketSummeryView();
        paymentView.setTicket(currentTicket);
        
        setHideButtonForRetailView();
      }
      
      if (currentTicket.getSalesArea() != null) {
        if (currentTicket.getSalesArea() != null) {
          String salesAreaString = currentTicket.getSalesArea().toString();
          btnSalesArea.setText("<html><center>SALES AREA <br>" + salesAreaString + "</center><html/>");
        }
      }
      else if (currentTicket.getId() != null) {
        btnSalesArea.setText("SALES AREA");
      }
      else {
        SalesArea salesArea = terminal.getSalesArea();
        if (salesArea != null) {
          String salesAreaString = salesArea.toString();
          btnSalesArea.setText("<html><center>SALES AREA <br>" + salesAreaString + "</center><html/>");
        }
        
        if (terminal.isFixedSalesArea().booleanValue()) {
          btnSalesArea.setEnabled(false);
        }
        else {
          btnSalesArea.setEnabled(true);
        }
      }
      if (currentTicket.getCustomer() != null) {
        btnCustomer.setText("<html><center>" + POSConstants.CUSTOMER_SELECTION_BUTTON_TEXT + "<br>" + currentTicket.getCustomer().getName());
      }
      else {
        btnCustomer.setText(POSConstants.CUSTOMER_SELECTION_BUTTON_TEXT);
      }
    }
  }
  

  private String getTableNumbers(List<Integer> numbers)
  {
    String tableNumbers = "";
    
    if ((numbers != null) && (!numbers.isEmpty())) {
      for (Iterator iterator = numbers.iterator(); iterator.hasNext();) {
        Integer n = (Integer)iterator.next();
        tableNumbers = tableNumbers + n;
        
        if (iterator.hasNext()) {
          tableNumbers = tableNumbers + ", ";
        }
      }
      return tableNumbers;
    }
    return tableNumbers;
  }
  


  public void showView(String viewName) {}
  

  public CategoryView getCategoryView()
  {
    return categoryView;
  }
  
  public void setCategoryView(CategoryView categoryView) {
    this.categoryView = categoryView;
  }
  
  public GroupView getGroupView() {
    return groupView;
  }
  
  public void setGroupView(GroupView groupView) {
    this.groupView = groupView;
  }
  
  public MenuItemView getItemView() {
    return itemView;
  }
  
  public void setItemView(MenuItemView itemView) {
    this.itemView = itemView;
  }
  
  public TicketView getTicketView() {
    return ticketView;
  }
  
  public void setTicketView(TicketView ticketView) {
    this.ticketView = ticketView;
  }
  
  public OrderController getOrderController() {
    return orderController;
  }
  
  public Ticket getCurrentTicket() {
    return currentTicket;
  }
  
  private void changeViewForOrderType(OrderType orderType) {
    if (orderType.isRetailOrder().booleanValue()) {
      showRetailView();
    }
    else {
      showDefaultView();
    }
    revalidate();
    repaint();
  }
  
  private void setHideButtonForRetailView() {
    btnDone.setVisible(false);
    btnCancel.setVisible(false);
    btnDeliveryInfo.setVisible(false);
    btnDiscount.setVisible(false);
    btnGuestNo.setVisible(false);
    btnOrderType.setVisible(false);
    btnReorder.setVisible(false);
    btnSalesArea.setVisible(false);
    btnSeatNo.setVisible(false);
    btnTableNumber.setVisible(false);
    btnSend.setVisible(false);
  }
  
  private void setVisibleButtonForOrderView() {
    btnDone.setVisible(true);
    btnCancel.setVisible(true);
    btnDiscount.setVisible(true);
    btnGuestNo.setVisible(true);
    btnHold.setVisible(true);
    btnMisc.setVisible(true);
    btnSalesArea.setVisible(true);
    btnSeatNo.setVisible(true);
    btnSend.setVisible(true);
    btnTableNumber.setVisible(true);
  }
  
  private void showRetailView() {
    removeAll();
    if (paymentView == null) {
      paymentView = new PaymentView(ticketProcessor);
    }
    
    ticketView.showSettleButton(false);
    ticketSummaryView.setVisible(true);
    
    ticketViewContainer.add(actionButtonPanel, "South");
    add(ticketViewContainer, "Center");
    add(paymentView, "East");
  }
  
  private void showDefaultView() {
    removeAll();
    setVisibleButtonForOrderView();
    ticketSummaryView.setVisible(false);
    ticketView.showSettleButton(true);
    add(categoryView, "East");
    add(ticketViewContainer, "West");
    add(midContainer, "Center");
    add(actionButtonPanel, "South");
  }
  
  public void setCurrentTicket(Ticket newTicket) {
    if ((currentTicket != null) && (newTicket != null)) {
      OrderType currentOrderType = currentTicket.getOrderType();
      OrderType newOrderType = newTicket.getOrderType();
      if (!currentOrderType.equals(newOrderType)) {
        currentTicket = newTicket;
        changeViewForOrderType(newOrderType);
      }
    }
    else if ((currentTicket == null) && (newTicket != null)) {
      OrderType newOrderType = newTicket.getOrderType();
      changeViewForOrderType(newOrderType);
    }
    
    currentTicket = newTicket;
    ticketView.setTicket(newTicket);
    if (paymentView != null) {
      paymentView.setTicket(newTicket);
    }
    updateView();
    resetView();
  }
  
  public static synchronized OrderView getInstance() {
    if (instance == null) {
      instance = new OrderView();
    }
    return instance;
  }
  

  public void resetView() {}
  
  public void setVisible(boolean visible)
  {
    super.setVisible(visible);
    if (visible) {
      initializeCategoryView();
      ticketView.setVisible(true);
    }
    else {
      ticketView.setVisible(false);
      if (TerminalConfig.isActiveCustomerDisplay()) {
        DrawerUtil.setCustomerDisplayMessage(TerminalConfig.getCustomerDisplayPort(), "Thank You");
      }
    }
    setReturnMode(false);
  }
  
  public String getViewName()
  {
    return "ORDER_VIEW";
  }
  
  private void initializeCategoryView() {
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run() {
        try {
          categoryView.initialize();
        } catch (Throwable t) {
          POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, t);
        }
      }
    });
  }
  
  private void doHoldOrder()
  {
    try {
      long currentTimeMillis = System.currentTimeMillis();
      if ((currentTicket.getOrderType().isShowTableSelection().booleanValue()) && (currentTicket.getOrderType().isRequiredCustomerData().booleanValue()) && 
        (!Application.getCurrentUser().hasPermission(UserPermission.HOLD_TICKET)))
      {

        String password = PasswordEntryDialog.show(Application.getPosWindow(), Messages.getString("OrderView.9"));
        if (StringUtils.isEmpty(password)) {
          return;
        }
        
        User user2 = UserDAO.getInstance().findUserBySecretKey(password);
        if (user2 == null) {
          POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("OrderView.10"));
          return;
        }
        
        if (!user2.hasPermission(UserPermission.HOLD_TICKET)) {
          POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("OrderView.11"));
          return;
        }
      }
      

      if ((ticketView.getTicket().getTicketItems() == null) || (ticketView.getTicket().getTicketItems().size() == 0)) {
        POSMessageDialog.showError(POSConstants.TICKET_IS_EMPTY_);
        return;
      }
      ticketView.doHoldOrder();
      ticketView.setAllowToLogOut(true);
      long endTime = System.currentTimeMillis();
      long total = endTime - currentTimeMillis;
      PosLog.info(getClass(), "total time to save ticket: " + total);
    } catch (StaleStateException x) {
      if (POSMessageDialog.showErrorWithOption(Application.getPosWindow(), Messages.getString("OrderView.0"), Messages.getString("RELOAD"))) {
        setCurrentTicket(TicketDAO.getInstance().loadFullTicket(currentTicket.getId()));
      }
      return;
    } catch (PosException x) {
      POSMessageDialog.showError(x.getMessage());
    } catch (Exception x) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doReorder() {
    try {
      Ticket ticket = ticketView.getTicket();
      Ticket cloneTicket = ticket.clone(ticket);
      List<TicketItem> ticketItems = cloneTicket.getTicketItems();
      if ((ticketItems == null) || (ticketItems.size() == 0)) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "No items to reorder.");
        return;
      }
      for (Iterator iterator = ticketItems.iterator(); iterator.hasNext();) {
        TicketItem ticketItem = (TicketItem)iterator.next();
        if (!ticketItem.isPrintedToKitchen().booleanValue()) {
          iterator.remove();
        }
      }
      if (cloneTicket.getTicketItems().size() == 0) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "No items to reorder.");
        return;
      }
      ReorderDialog dialog = new ReorderDialog(cloneTicket);
      dialog.setTitle("OROPOS");
      dialog.setDefaultCloseOperation(2);
      

      dialog.setResizable(false);
      dialog.openFullScreen();
      if (!dialog.isCanceled()) {
        List<TicketItem> ticketItemList = dialog.getTicketItems();
        if (ticketItemList == null) {
          return;
        }
        for (Iterator iterator = ticketItemList.iterator(); iterator.hasNext();) {
          TicketItem ticketItem = (TicketItem)iterator.next();
          ticketView.addTicketItem(ticketItem);
        }
        OrderController.saveOrder(ticketView.getTicket());
        ticketView.sendTicketToKitchen();
        ticketView.getTicketViewerTable().repaint();
        POSMessageDialog.showMessage(Messages.getString("OrderView.8"));
      }
    } catch (StaleStateException x) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("OrderView.0"));
      return;
    } catch (PosException x) {
      POSMessageDialog.showError(x.getMessage());
    } catch (Exception x) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doSendTicketToKitchen()
  {
    try {
      ticketView.sendTicketToKitchenByOption();
      ticketView.updateTicketTitle();
      ticketView.getTicketViewerTable().updateView();
    }
    catch (StaleStateException x) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("OrderView.0"));
      return;
    } catch (PosException x) {
      POSMessageDialog.showError(x.getMessage());
    } catch (Exception x) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public Object getSelectedSeatNumber() {
    return seatAction.getSelectedSeatNumber();
  }
  
  public boolean updateSeat(TicketItem ticketItem) {
    return seatAction.updateSeatNumber(ticketItem);
  }
  
  public boolean is_86Mode() {
    return _86Mode;
  }
  
  public void set_86Mode(boolean _86Mode) {
    this._86Mode = _86Mode;
  }
  




  public void paymentDone()
  {
    ticketView.doFinishOrder();
    updateView();
  }
  
  public void paymentCanceled()
  {
    if (currentTicket == null) {
      return;
    }
    if ((StringUtils.isNotEmpty(currentTicket.getId())) && (currentTicket.getDueAmount().doubleValue() > 0.0D)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Payment is not fully completed, ticket can not be cancelled!");
      return;
    }
    
    if (StringUtils.isNotEmpty(currentTicket.getId())) {
      TicketDAO.getInstance().delete(currentTicket);
    }
    currentTicket.getTicketItems().clear();
    
    ticketView.doCancelOrder();
    updateView();
  }
  
  public void paymentDataChanged()
  {
    updateView();
  }
  
  public PaymentView getPaymentView()
  {
    return paymentView;
  }
  
  public SettleTicketProcessor getTicketProcessor() {
    return ticketProcessor;
  }
  
  public void itemAdded(Ticket ticket, TicketItem item)
  {
    if (ticket.getOrderType().isRetailOrder().booleanValue()) {
      updateView();
      paymentView.updateView();
    }
  }
  
  public void itemRemoved(TicketItem item)
  {
    if (currentTicket.getOrderType().isRetailOrder().booleanValue()) {
      updateView();
      paymentView.updateView();
    }
  }
  
  public boolean isReturnMode() {
    return returnMode;
  }
  
  public void setReturnMode(boolean returnMode) {
    this.returnMode = returnMode;
  }
}
