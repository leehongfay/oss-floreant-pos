package com.floreantpos.ui.views.order;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.actions.SettleTicketAction;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.model.ActionHistory;
import com.floreantpos.model.ComboItem;
import com.floreantpos.model.ComboTicketItem;
import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemModifier;
import com.floreantpos.model.TicketItemSeat;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.ActionHistoryDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.model.dao.StoreDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.AutomatedWeightInputDialog;
import com.floreantpos.ui.dialog.ComboTicketItemSelectionDialog;
import com.floreantpos.ui.dialog.NumberSelectionDialog2;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.ticket.TicketViewerTable;
import com.floreantpos.ui.views.order.actions.CategorySelectionListener;
import com.floreantpos.ui.views.order.actions.GroupSelectionListener;
import com.floreantpos.ui.views.order.actions.ItemSelectionListener;
import com.floreantpos.ui.views.order.actions.OrderListener;
import com.floreantpos.ui.views.order.actions.TicketEditListener;
import com.floreantpos.ui.views.order.modifier.ModifierSelectionDialog;
import com.floreantpos.ui.views.order.modifier.ModifierSelectionModel;
import com.floreantpos.ui.views.order.multipart.PizzaModifierSelectionDialog;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;

















public class OrderController
  implements OrderListener, CategorySelectionListener, GroupSelectionListener, ItemSelectionListener
{
  protected OrderView orderView;
  List<TicketEditListener> ticketEditListenerList;
  
  public OrderController(OrderView orderView)
  {
    this.orderView = orderView;
    
    orderView.getCategoryView().addCategorySelectionListener(this);
    orderView.getGroupView().addGroupSelectionListener(this);
    orderView.getItemView().addItemSelectionListener(this);
    orderView.getTicketView().addOrderListener(this);
    ticketEditListenerList = new ArrayList();
  }
  
  public void categorySelected(MenuCategory foodCategory) {
    try {
      orderView.showView("GROUP_VIEW");
      orderView.getGroupView().setMenuCategory(foodCategory);
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public void groupSelected(MenuGroup foodGroup) {
    try {
      orderView.showView("ITEM_VIEW");
      orderView.getItemView().setMenuGroup(foodGroup);
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public void itemSelected(MenuItem menuItem) {
    Session session = null;
    try {
      session = MenuItemDAO.getInstance().createNewSession();
      if (menuItem.getId() != null)
      {

        menuItem = (MenuItem)session.get(MenuItem.class, menuItem.getId());
      }
      if (!hasAvailableStock(menuItem, 1.0D)) {
        return;
      }
      if (menuItem.isVariant().booleanValue()) {
        MenuItem parentMenuItem = MenuItemDAO.getInstance().get(menuItem.getParentMenuItemId(), session);
        menuItem.setParentMenuItem(parentMenuItem);
      }
      if (menuItem.isHasVariant().booleanValue()) {
        orderView.getItemView().rendererVariants(menuItem);
        return;
      }
      
      TicketView ticketView = orderView.getTicketView();
      Ticket ticket = orderView.getCurrentTicket();
      double itemQuantity = 1.0D;
      if (menuItem.isFractionalUnit().booleanValue()) {
        if (TerminalConfig.getScaleActivationValue().equals("cas10")) {
          itemQuantity = AutomatedWeightInputDialog.takeDoubleInput(menuItem.getName(), 1.0D);
        }
        else {
          itemQuantity = NumberSelectionDialog2.takeDoubleInput(Messages.getString("OrderController.2"), 1.0D);
        }
        if (itemQuantity <= -1.0D) {
          return;
        }
        
        if (itemQuantity == 0.0D) {
          POSMessageDialog.showError(Messages.getString("OrderController.3"));
          return;
        }
      }
      double itemPrice = 0.0D;
      if (menuItem.isEditablePrice().booleanValue()) {
        itemPrice = NumberSelectionDialog2.takeDoubleInput("Please enter price!", menuItem.getPrice().doubleValue());
        if (itemPrice <= -1.0D) {
          return;
        }
        menuItem.setPrice(Double.valueOf(itemPrice));
      }
      

      TicketItem ticketItem = menuItem.convertToTicketItem(ticket, itemQuantity);
      if (orderView.isReturnMode())
      {


        ticketItem.markVoided("Return", false, ticketItem.getQuantity().doubleValue());
      }
      



      if (menuItem.isVariant().booleanValue()) {
        menuItem = menuItem.getParentMenuItem();
      }
      if ((menuItem.isComboItem().booleanValue()) && 
        (!addComboItemsToTicketItem(menuItem, ticketItem))) {
        return;
      }
      
      ticketItem.setTicket(ticket);
      
      Object selectedSeat = orderView.getSelectedSeatNumber();
      if ((selectedSeat instanceof Integer)) {
        ticketItem.setSeatNumber((Integer)selectedSeat);
      }
      else if ((selectedSeat instanceof TicketItemSeat)) {
        TicketItemSeat seat = (TicketItemSeat)orderView.getSelectedSeatNumber();
        ticketItem.setSeat(seat);
        ticketItem.setSeatNumber(seat.getSeatNumber());
      }
      if (menuItem.isPizzaType().booleanValue()) {
        PizzaModifierSelectionDialog dialog = new PizzaModifierSelectionDialog(ticket, ticketItem, menuItem, false);
        dialog.setResizable(false);
        if (TerminalConfig.isKioskMode()) {
          dialog.openUndecoratedFullScreen();
        }
        else {
          dialog.openFullScreen();
        }
        
        if (dialog.isCanceled()) {
          return;
        }
        fireTicketItemUpdated(ticket, ticketItem);
      }
      else if (menuItem.hasMandatoryModifiers()) {
        ModifierSelectionDialog dialog = new ModifierSelectionDialog(new ModifierSelectionModel(ticket, ticketItem, menuItem));
        dialog.open();
        if (!dialog.isCanceled()) {
          ticketView.addTicketItem(ticketItem);
          fireTicketItemUpdated(ticket, ticketItem);
        }
      }
      else {
        ticketView.addTicketItem(ticketItem);
        fireTicketItemUpdated(ticket, ticketItem);
      }
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    } finally {
      MenuItemDAO.getInstance().closeSession(session);
    }
  }
  
  private boolean hasAvailableStock(MenuItem menuItem, double selectedQty) {
    if ((menuItem == null) || (!menuItem.isDisableWhenStockAmountIsZero().booleanValue()) || (menuItem.isHasVariant().booleanValue()))
      return true;
    Ticket ticket = orderView.getCurrentTicket();
    double qty = 0.0D;
    if (ticket != null) {
      List<TicketItem> ticketItems = ticket.getTicketItems();
      if (ticketItems != null) {
        for (TicketItem item : ticketItems) {
          if (item.getMenuItemId().equals(menuItem.getId())) {
            qty += item.getQuantity().doubleValue() - item.getInventoryAdjustQty().doubleValue();
          }
        }
      }
    }
    qty += selectedQty;
    if ((menuItem.getAvailableUnit().doubleValue() < qty) && 
      (POSMessageDialog.showYesNoQuestionDialog(Application.getPosWindow(), "Item is not in stock, what do you want?", "Confirm", "ADD ANYWAY", "CLOSE") != 0))
    {
      return false;
    }
    
    return true;
  }
  
  public void doEditTicketItemQuantity(Object selectedObject) {
    TicketItem ticketItem = (TicketItem)selectedObject;
    
    double quantity = 0.0D;
    if (ticketItem.isFractionalUnit().booleanValue()) {
      quantity = NumberSelectionDialog2.takeDoubleInput("Enter quantity", Math.abs(ticketItem.getQuantity().doubleValue()));
    } else {
      quantity = NumberSelectionDialog2.takeIntInput("Enter quantity", Math.abs(ticketItem.getQuantity().doubleValue()));
    }
    if (quantity < 1.0D)
      return;
    MenuItem menuItem = ticketItem.getMenuItem();
    if ((quantity > ticketItem.getQuantity().doubleValue()) && (!hasAvailableStock(menuItem, quantity - ticketItem.getQuantity().doubleValue()))) {
      return;
    }
    if (orderView.isReturnMode()) {
      ticketItem.markVoided("Return", false, ticketItem.getQuantity().doubleValue());
    }
    else {
      if ((menuItem != null) && (menuItem.isComboItem().booleanValue())) {
        updateComboTicketItemQuantity(ticketItem, quantity);
      }
      ticketItem.setQuantity(Double.valueOf(quantity));
    }
  }
  
  public void updateComboTicketItemQuantity(TicketItem ticketItem, double quantity) {
    List<TicketItem> comboticketItems = ticketItem.getComboItems();
    double ticketItemQuantity = ticketItem.getQuantity().doubleValue();
    double adjustQuantity = quantity - ticketItemQuantity;
    if (comboticketItems != null) {
      for (TicketItem comboTicketItem : comboticketItems) {
        comboTicketItem.setQuantity(Double.valueOf(comboTicketItem.getQuantity().doubleValue() + adjustQuantity * comboTicketItem.getQuantity().doubleValue() / ticketItemQuantity));
      }
    }
  }
  
  private boolean addComboItemsToTicketItem(MenuItem menuItem, TicketItem ticketItemObj) {
    ComboTicketItem ticketItem = (ComboTicketItem)ticketItemObj;
    Ticket currentTicket; if ((menuItem.getComboItems() != null) && (!menuItem.getComboItems().isEmpty())) {
      currentTicket = orderView.getCurrentTicket();
      for (ComboItem item : menuItem.getComboItems())
        if (item.getQuantity().doubleValue() != 0.0D)
        {
          MenuItem comboMenuItem = item.getMenuItem();
          



          TicketItem comboTicketItem = comboMenuItem.convertToTicketItem(currentTicket, item.getQuantity().doubleValue());
          comboTicketItem.setMenuItem(comboMenuItem);
          if (comboMenuItem.hasMandatoryModifiers()) {
            ModifierSelectionDialog dialog = new ModifierSelectionDialog(new ModifierSelectionModel(currentTicket, comboTicketItem, comboMenuItem));
            dialog.open();
            if (dialog.isCanceled()) {
              return false;
            }
          }
          else if (menuItem.isPizzaType().booleanValue()) {
            PizzaModifierSelectionDialog dialog = new PizzaModifierSelectionDialog(currentTicket, comboTicketItem, comboMenuItem, false);
            dialog.openFullScreen();
            
            if (dialog.isCanceled()) {
              return false;
            }
          }
          comboTicketItem.setMenuItemId(comboMenuItem.getId());
          comboTicketItem.setName(comboMenuItem.getName());
          comboTicketItem.setQuantity(item.getQuantity());
          comboTicketItem.setParentTicketItem(ticketItem);
          comboTicketItem.setTicket(null);
          comboTicketItem.setGroupId(null);
          ticketItem.addTocomboItems(comboTicketItem);
        }
    }
    if ((menuItem.getComboGroups() != null) && (!menuItem.getComboGroups().isEmpty())) {
      ComboTicketItemSelectionDialog dialog = new ComboTicketItemSelectionDialog(menuItem, ticketItem);
      dialog.setTitle("SELECT COMBO ITEMS");
      dialog.setSize(PosUIManager.getSize(1024, 700));
      dialog.open();
      
      if (dialog.isCanceled()) {
        return false;
      }
    }
    return true;
  }
  
  public void itemSelectionFinished(MenuGroup parent) {
    MenuCategory menuCategory = parent.getParent();
    GroupView groupView = orderView.getGroupView();
    if (!menuCategory.equals(groupView.getMenuCategory())) {
      groupView.setMenuCategory(menuCategory);
    }
    orderView.showView("GROUP_VIEW");
  }
  
  public void payOrderSelected(Ticket ticket) {
    try {
      if (!new SettleTicketAction(ticket).performSettle()) {
        return;
      }
      RootView.getInstance().showDefaultView();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public static void openModifierDialog(ITicketItem ticketItemObject) {
    try {
      Ticket ticket = OrderView.getInstance().getCurrentTicket();
      ticketItem = null;
      if ((ticketItemObject instanceof TicketItem)) {
        ticketItem = (TicketItem)ticketItemObject;
      }
      else if ((ticketItemObject instanceof TicketItemModifier)) {
        TicketItemModifier ticketItemModifier = (TicketItemModifier)ticketItemObject;
        ticketItem = ticketItemModifier.getTicketItem();
        if (ticketItem == null) {
          ticketItem = ticketItemModifier.getTicketItem();
        }
      }
      MenuItem menuItem = ticketItem.getMenuItem();
      MenuItemDAO.getInstance().initialize(menuItem);
      if (menuItem.isVariant().booleanValue()) {
        menuItem = menuItem.getParentMenuItem();
      }
      List<TicketItemModifier> ticketItemModifiers = ticketItem.getTicketItemModifiers();
      if (ticketItemModifiers == null) {
        ticketItemModifiers = new ArrayList();
      }
      TicketItem cloneTicketItem = ticketItem.clone();
      
      boolean pizzaType = ticketItem.isPizzaType().booleanValue();
      if (pizzaType) {
        PizzaModifierSelectionDialog dialog = new PizzaModifierSelectionDialog(ticket, cloneTicketItem, menuItem, true);
        dialog.openFullScreen();
        
        if (dialog.isCanceled()) {
          return;
        }
        sizeModifier = cloneTicketItem.getSizeModifier();
        sizeModifier.setTicketItem(ticketItem);
        ticketItem.setSizeModifier(sizeModifier);
        ticketItem.setQuantity(cloneTicketItem.getQuantity());
        ticketItem.setUnitPrice(cloneTicketItem.getUnitPrice());
      }
      else {
        ModifierSelectionDialog dialog = new ModifierSelectionDialog(new ModifierSelectionModel(ticket, cloneTicketItem, menuItem));
        dialog.open();
        
        if (dialog.isCanceled()) {
          return;
        }
      }
      List<TicketItemModifier> addedTicketItemModifiers = cloneTicketItem.getTicketItemModifiers();
      if (addedTicketItemModifiers == null) {
        addedTicketItemModifiers = new ArrayList();
      }
      
      ticketItemModifiers.clear();
      for (TicketItemModifier ticketItemModifier : addedTicketItemModifiers) {
        ticketItemModifier.setTicketItem(ticketItem);
        


        ticketItem.addToticketItemModifiers(ticketItemModifier);
      } } catch (Exception e) { TicketItem ticketItem;
      TicketItemModifier sizeModifier;
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
    OrderView.getInstance().getTicketView().getTicketViewerTable().updateView();
  }
  
  public static synchronized void saveOrder(Ticket ticket)
  {
    if (ticket == null) {
      return;
    }
    TicketDAO.getInstance().saveOrUpdate(ticket);
    

    boolean newTicket = ticket.getId() == null;
    ActionHistoryDAO actionHistoryDAO = ActionHistoryDAO.getInstance();
    User user = Application.getCurrentUser();
    
    if (newTicket) {
      actionHistoryDAO.saveHistory(user, ActionHistory.NEW_CHECK, POSConstants.RECEIPT_REPORT_TICKET_NO_LABEL + ":" + ticket.getId());
    }
    else {
      actionHistoryDAO.saveHistory(user, ActionHistory.EDIT_CHECK, POSConstants.RECEIPT_REPORT_TICKET_NO_LABEL + ":" + ticket.getId());
    }
  }
  
  public static synchronized void closeOrder(Ticket ticket) {
    if ((ticket.getOrderType().isCloseOnPaid().booleanValue()) || (ticket.isPaid().booleanValue())) {
      ticket.setClosed(Boolean.valueOf(true));
      ticket.setClosingDate(StoreDAO.geServerTimestamp());
    }
    
    TicketDAO ticketDAO = new TicketDAO();
    ticketDAO.saveOrUpdate(ticket);
    
    User driver = ticket.getAssignedDriver();
    if (driver != null) {
      driver.setAvailableForDelivery(Boolean.valueOf(true));
      UserDAO.getInstance().saveOrUpdate(driver);
    }
  }
  
  public void addTicketUpdateListener(TicketEditListener l) {
    ticketEditListenerList.add(l);
  }
  
  public void removeTicketUpdateListener(TicketEditListener l) {
    ticketEditListenerList.remove(l);
  }
  
  private void fireTicketItemUpdated(Ticket ticket, TicketItem ticketItem) {
    for (TicketEditListener listener : ticketEditListenerList) {
      listener.itemAdded(ticket, ticketItem);
    }
  }
}
