package com.floreantpos.ui.views.order;

import com.floreantpos.model.User;
import com.floreantpos.ui.views.IView;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.JPanel;






















public abstract class ViewPanel
  extends JPanel
  implements IView
{
  private boolean dataInitialized;
  private User dataInitializedForUser;
  
  public ViewPanel() {}
  
  public ViewPanel(boolean isDoubleBuffered)
  {
    super(isDoubleBuffered);
  }
  
  public ViewPanel(LayoutManager layout, boolean isDoubleBuffered) {
    super(layout, isDoubleBuffered);
  }
  
  public ViewPanel(LayoutManager layout) {
    super(layout);
  }
  
  public Component getViewComponent()
  {
    return this;
  }
  


  public void refresh() {}
  


  public boolean isDataInitialized()
  {
    return dataInitialized;
  }
  
  public void setDataInitialized(boolean dataInitialized) {
    this.dataInitialized = dataInitialized;
  }
  
  public User getDataInitializedForUser() {
    return dataInitializedForUser;
  }
  
  public void setDataInitializedForUser(User dataInitializedFor) {
    dataInitializedForUser = dataInitializedFor;
  }
  
  public DataChangeListener getDataChangeListener()
  {
    return null;
  }
}
