package com.floreantpos.ui.views.order;

import com.floreantpos.PosLog;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.extension.OrderServiceFactory;
import com.floreantpos.main.Application;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.OrderTypeDAO;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.HeaderPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.kitchendisplay.KitchenDisplayView;
import com.floreantpos.ui.views.CustomerView;
import com.floreantpos.ui.views.IView;
import com.floreantpos.ui.views.LoginView;
import com.floreantpos.ui.views.SwitchboardOtherFunctionsView;
import com.floreantpos.ui.views.SwitchboardView;
import com.floreantpos.ui.views.TableMapView;
import com.floreantpos.ui.views.payment.SettleTicketDialog;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.TicketAlreadyExistsException;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
















public class RootView
  extends TransparentPanel
{
  private CardLayout cards = new CardLayout();
  
  private HeaderPanel headerPanel = new HeaderPanel();
  private JPanel contentPanel = new JPanel(cards);
  
  private LoginView loginScreen;
  private SettleTicketDialog paymentView;
  private String currentViewName;
  private IView homeView;
  private Map<String, IView> views = new HashMap();
  private static RootView instance;
  
  private RootView()
  {
    setLayout(new BorderLayout());
    setBorder(new EmptyBorder(3, 3, 3, 3));
    add(contentPanel);
  }
  
  public void initDefaultViews() {
    headerPanel.setVisible(false);
    add(headerPanel, "North");
    
    loginScreen = LoginView.getInstance();
    addView(loginScreen);
  }
  
  public void addView(IView iView) {
    views.put(iView.getViewName(), iView);
    contentPanel.add(iView.getViewName(), iView.getViewComponent());
  }
  
  public void showView(String viewName) {
    if ("LOGIN_VIEW".equals(viewName)) {
      headerPanel.setVisible(false);
    }
    else {
      headerPanel.setVisible(true);
    }
    
    currentViewName = viewName;
    cards.show(contentPanel, viewName);
    
    if (homeView != null) {
      headerPanel.updateHomeView(!homeView.getViewName().equals(currentViewName));
    }
    headerPanel.updateOthersFunctionsView(!currentViewName.equals("ALL FUNCTIONS"));
    headerPanel.updateSwitchBoardView(!currentViewName.equals(SwitchboardView.VIEW_NAME));
  }
  
  public void showView(IView view) {
    if (!views.containsKey(view.getViewName())) {
      addView(view);
    }
    currentViewName = view.getViewName();
    showView(currentViewName);
  }
  
  public boolean hasView(String viewName) {
    return views.containsKey(viewName);
  }
  
  public boolean hasView(IView view) {
    return views.containsKey(view.getViewName());
  }
  
  public OrderView getOrderView() {
    return (OrderView)views.get("ORDER_VIEW");
  }
  
  public LoginView getLoginScreen() {
    return loginScreen;
  }
  







  public static synchronized RootView getInstance()
  {
    if (instance == null) {
      instance = new RootView();
    }
    return instance;
  }
  
  public SettleTicketDialog getPaymentView() {
    return paymentView;
  }
  
  public HeaderPanel getHeaderPanel() {
    return headerPanel;
  }
  
  public String getCurrentViewName() {
    return currentViewName;
  }
  
  public IView getCurrentView() {
    return (IView)views.get(currentViewName);
  }
  
  public void showDefaultView()
  {
    String defaultViewName = TerminalConfig.getDefaultView();
    if (defaultViewName == null) {
      defaultViewName = "";
    }
    
    if (defaultViewName.equals("ALL FUNCTIONS")) {
      setAndShowHomeScreen(SwitchboardOtherFunctionsView.getInstance());
    }
    else if (defaultViewName.equals("KD")) {
      if (!hasView(KitchenDisplayView.getInstance())) {
        addView(KitchenDisplayView.getInstance());
      }
      
      headerPanel.setVisible(false);
      setAndShowHomeScreen(KitchenDisplayView.getInstance());
    }
    else if (defaultViewName.equals(SwitchboardView.VIEW_NAME)) {
      setAndShowHomeScreen(SwitchboardView.getInstance());
    }
    else {
      OrderType orderType = OrderTypeDAO.getInstance().findByName(defaultViewName);
      
      if (orderType == null) {
        getInstance().showHomeScreen();
        return;
      }
      if (orderType.isRetailOrder().booleanValue()) {
        try {
          homeView = OrderView.getInstance();
          if (!hasView(homeView)) {
            addView(homeView);
          }
          OrderServiceFactory.getOrderService().createNewTicket(orderType, null, null);
          return;
        } catch (TicketAlreadyExistsException e1) {
          PosLog.error(RootView.class, e1);
        }
      }
      if (orderType.isShowTableSelection().booleanValue()) {
        TableMapView tableMapView = TableMapView.getInstance(orderType);
        tableMapView.updateView();
        setAndShowHomeScreen(tableMapView);
      }
      else if ((orderType.isRequiredCustomerData().booleanValue()) || (orderType.isDelivery().booleanValue())) {
        OrderServiceExtension orderServicePlugin = (OrderServiceExtension)ExtensionManager.getPlugin(OrderServiceExtension.class);
        if (orderServicePlugin != null) {
          if (orderType.isDelivery().booleanValue()) {
            setAndShowHomeScreen(orderServicePlugin.getDeliveryDispatchView(orderType));
          }
          else {
            CustomerView customerView = CustomerView.getInstance(orderType);
            customerView.updateView();
            setAndShowHomeScreen(customerView);
          }
        }
        else {
          CustomerView customerView = CustomerView.getInstance(orderType);
          customerView.updateView();
          setAndShowHomeScreen(customerView);
        }
      }
      else {
        try {
          homeView = OrderView.getInstance();
          OrderServiceFactory.getOrderService().createNewTicket(orderType, null, null);
        } catch (TicketAlreadyExistsException e1) {
          POSMessageDialog.showError(this, "An unexpected error has occured. Please try again.", e1);
        }
      }
    }
  }
  



  public IView getHomeView()
  {
    return homeView;
  }
  
  public void setAndShowHomeScreen(IView homeScreen) {
    homeView = homeScreen;
    showHomeScreen();
  }
  
  public void showHomeScreen()
  {
    showView(getHomeView());
  }
  
  public void showBackOffice() {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    if (window == null) {
      window = new BackOfficeWindow(Application.getCurrentUser());
    }
    window.setVisible(true);
    window.toFront();
  }
  
  public void showBackOffice(User user) {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    if (window == null) {
      window = new BackOfficeWindow(user);
    }
    window.setVisible(true);
    window.toFront();
  }
}
