package com.floreantpos.ui.views.order;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemModifier;
import com.floreantpos.model.dao.StoreDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.ticket.TicketViewerTable;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.ShiftUtil;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
















public class TicketForSplitView
  extends TransparentPanel
  implements TableModelListener
{
  private Ticket ticket;
  public static final String VIEW_NAME = "TICKET_FOR_SPLIT_VIEW";
  private TicketForSplitView ticketView1;
  private TicketForSplitView ticketView2;
  private TicketForSplitView ticketView3;
  private int viewNumber = 1;
  private PosButton btnScrollDown;
  
  public TicketForSplitView() { initComponents();
    
    ticket = new Ticket();
    ticket.setTaxIncluded(Boolean.valueOf(Application.getInstance().isPriceIncludesTax()));
    ticket.setTerminal(Application.getInstance().getTerminal());
    ticket.setOwner(Application.getCurrentUser());
    ticket.setShift(ShiftUtil.getCurrentShift());
    
    Calendar currentTime = Calendar.getInstance();
    currentTime.setTime(StoreDAO.geServerTimestamp());
    ticket.setCreateDate(DateUtil.getServerTimeCalendar().getTime());
    ticket.setCreationHour(Integer.valueOf(currentTime.get(11)));
    
    setOpaque(true);
    setTicket(ticket);
  }
  
  private PosButton btnScrollUp;
  private PosButton btnTransferToTicket1;
  private PosButton btnTransferToTicket2;
  private PosButton btnTransferToTicket3;
  private JLabel jLabel1;
  private JLabel jLabel2;
  
  private void initComponents()
  {
    jPanel1 = new TransparentPanel();
    jSeparator1 = new JSeparator();
    jPanel3 = new TransparentPanel();
    jLabel5 = new JLabel();
    jLabel6 = new JLabel();
    jLabel1 = new JLabel();
    jLabel2 = new JLabel();
    jSeparator2 = new JSeparator();
    jSeparator3 = new JSeparator();
    tfSubtotal = new JTextField();
    tfSubtotal.setMinimumSize(new Dimension(90, 19));
    tfSubtotal.setHorizontalAlignment(11);
    tfSubtotal.setColumns(10);
    tfTax = new JTextField();
    tfTax.setMinimumSize(new Dimension(90, 19));
    tfTax.setHorizontalAlignment(11);
    tfTax.setColumns(10);
    tfDiscount = new JTextField();
    tfDiscount.setMinimumSize(new Dimension(90, 19));
    tfDiscount.setHorizontalAlignment(11);
    tfDiscount.setColumns(10);
    tfTotal = new JTextField();
    tfTotal.setMinimumSize(new Dimension(90, 19));
    tfTotal.setHorizontalAlignment(11);
    tfTotal.setColumns(10);
    jPanel5 = new TransparentPanel();
    btnScrollUp = new PosButton();
    btnScrollDown = new PosButton();
    btnTransferToTicket1 = new PosButton();
    btnTransferToTicket2 = new PosButton();
    btnTransferToTicket3 = new PosButton();
    jPanel2 = new TransparentPanel();
    jScrollPane1 = new JScrollPane();
    ticketViewerTable = new TicketViewerTable();
    
    setLayout(new BorderLayout(5, 5));
    
    setBorder(BorderFactory.createTitledBorder(null, POSConstants.TICKET, 2, 0));
    
    setPreferredSize(new Dimension(280, 463));
    jPanel1.setLayout(new BorderLayout(5, 5));
    
    jPanel1.add(jSeparator1, "Center");
    
    jPanel3.setLayout(new GridBagLayout());
    
    jLabel5.setFont(new Font("Tahoma", 1, 12));
    jLabel5.setHorizontalAlignment(4);
    jLabel5.setText(POSConstants.SUBTOTAL + ":");
    GridBagConstraints gridBagConstraints = new GridBagConstraints();
    weightx = 1.0D;
    gridx = 0;
    gridy = 1;
    fill = 2;
    insets = new Insets(3, 5, 0, 0);
    jPanel3.add(jLabel5, gridBagConstraints);
    
    jLabel6.setFont(new Font("Tahoma", 1, 12));
    jLabel6.setHorizontalAlignment(4);
    jLabel6.setText(POSConstants.TOTAL + ":");
    gridBagConstraints = new GridBagConstraints();
    gridx = 0;
    gridy = 4;
    fill = 2;
    insets = new Insets(0, 5, 3, 0);
    jPanel3.add(jLabel6, gridBagConstraints);
    
    jLabel1.setFont(new Font("Tahoma", 1, 12));
    jLabel1.setHorizontalAlignment(4);
    jLabel1.setText(POSConstants.DISCOUNT + ":");
    gridBagConstraints = new GridBagConstraints();
    gridx = 0;
    gridy = 2;
    fill = 2;
    insets = new Insets(0, 5, 0, 0);
    jPanel3.add(jLabel1, gridBagConstraints);
    
    jLabel2.setFont(new Font("Tahoma", 1, 12));
    jLabel2.setHorizontalAlignment(4);
    jLabel2.setText(POSConstants.TAX + ":");
    gridBagConstraints = new GridBagConstraints();
    gridx = 0;
    gridy = 3;
    fill = 2;
    insets = new Insets(0, 5, 0, 0);
    jPanel3.add(jLabel2, gridBagConstraints);
    
    gridBagConstraints = new GridBagConstraints();
    gridx = 0;
    gridy = 6;
    gridwidth = 0;
    fill = 2;
    weightx = 1.0D;
    jPanel3.add(jSeparator2, gridBagConstraints);
    
    gridBagConstraints = new GridBagConstraints();
    gridx = 0;
    gridy = 0;
    gridwidth = 0;
    fill = 2;
    weightx = 1.0D;
    jPanel3.add(jSeparator3, gridBagConstraints);
    
    tfSubtotal.setEditable(false);
    tfSubtotal.setFont(new Font("Tahoma", 1, 12));
    gridBagConstraints_1 = new GridBagConstraints();
    gridBagConstraints_1.anchor = 17;
    gridBagConstraints_1.gridwidth = 0;
    gridBagConstraints_1.insets = new Insets(3, 5, 0, 5);
    jPanel3.add(tfSubtotal, gridBagConstraints_1);
    
    tfTax.setEditable(false);
    tfTax.setFont(new Font("Tahoma", 1, 12));
    gridBagConstraints_3 = new GridBagConstraints();
    gridBagConstraints_3.anchor = 17;
    gridBagConstraints_3.gridx = 1;
    gridBagConstraints_3.gridy = 3;
    gridBagConstraints_3.insets = new Insets(3, 5, 0, 5);
    jPanel3.add(tfTax, gridBagConstraints_3);
    
    tfDiscount.setEditable(false);
    tfDiscount.setFont(new Font("Tahoma", 1, 12));
    gridBagConstraints_2 = new GridBagConstraints();
    gridBagConstraints_2.anchor = 17;
    gridBagConstraints_2.gridx = 1;
    gridBagConstraints_2.gridy = 2;
    gridBagConstraints_2.insets = new Insets(3, 5, 0, 5);
    jPanel3.add(tfDiscount, gridBagConstraints_2);
    
    tfTotal.setEditable(false);
    tfTotal.setFont(new Font("Tahoma", 1, 12));
    gridBagConstraints_4 = new GridBagConstraints();
    gridBagConstraints_4.anchor = 17;
    gridBagConstraints_4.gridx = 1;
    gridBagConstraints_4.gridy = 4;
    gridBagConstraints_4.gridwidth = 0;
    gridBagConstraints_4.insets = new Insets(3, 5, 3, 5);
    jPanel3.add(tfTotal, gridBagConstraints_4);
    
    jPanel1.add(jPanel3, "North");
    
    jPanel5.setLayout(new GridBagLayout());
    
    btnScrollUp.setIcon(IconFactory.getIcon("/ui_icons/", "up.png"));
    btnScrollUp.setPreferredSize(new Dimension(50, 45));
    btnScrollUp.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketForSplitView.this.doScrollUp(evt);
      }
      
    });
    gridBagConstraints = new GridBagConstraints();
    gridx = 2;
    gridy = 0;
    fill = 1;
    weightx = 1.0D;
    insets = new Insets(0, 2, 0, 0);
    jPanel5.add(btnScrollUp, gridBagConstraints);
    
    btnScrollDown.setIcon(IconFactory.getIcon("/ui_icons/", "down.png"));
    btnScrollDown.setPreferredSize(new Dimension(50, 45));
    btnScrollDown.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketForSplitView.this.doScrollDown(evt);
      }
      
    });
    gridBagConstraints = new GridBagConstraints();
    gridx = 2;
    gridy = 1;
    fill = 1;
    weightx = 1.0D;
    insets = new Insets(2, 2, 0, 0);
    jPanel5.add(btnScrollDown, gridBagConstraints);
    
    btnTransferToTicket1.setText("1");
    btnTransferToTicket1.setPreferredSize(new Dimension(60, 45));
    btnTransferToTicket1.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketForSplitView.this.btnTransferToTicket1ActionPerformed(evt);
      }
      
    });
    gridBagConstraints = new GridBagConstraints();
    gridx = 0;
    gridy = 0;
    fill = 1;
    weightx = 1.0D;
    insets = new Insets(0, 0, 0, 2);
    jPanel5.add(btnTransferToTicket1, gridBagConstraints);
    
    btnTransferToTicket2.setText("2");
    btnTransferToTicket2.setPreferredSize(new Dimension(60, 45));
    btnTransferToTicket2.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketForSplitView.this.btnTransferToTicket2ActionPerformed(evt);
      }
      
    });
    gridBagConstraints = new GridBagConstraints();
    fill = 1;
    weightx = 1.0D;
    jPanel5.add(btnTransferToTicket2, gridBagConstraints);
    
    btnTransferToTicket3.setText("3");
    btnTransferToTicket3.setPreferredSize(new Dimension(60, 45));
    btnTransferToTicket3.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketForSplitView.this.btnTransferToTicket3ActionPerformed(evt);
      }
      
    });
    gridBagConstraints = new GridBagConstraints();
    gridx = 0;
    gridy = 1;
    gridwidth = 2;
    fill = 1;
    weightx = 1.0D;
    insets = new Insets(2, 2, 0, 0);
    jPanel5.add(btnTransferToTicket3, gridBagConstraints);
    
    jPanel1.add(jPanel5, "Center");
    
    add(jPanel1, "South");
    
    jPanel2.setLayout(new BorderLayout());
    
    jScrollPane1.setHorizontalScrollBarPolicy(31);
    jScrollPane1.setVerticalScrollBarPolicy(21);
    jScrollPane1.setViewportView(ticketViewerTable);
    
    jPanel2.add(jScrollPane1, "Center");
    
    add(jPanel2, "Center");
  }
  
  private void btnTransferToTicket3ActionPerformed(ActionEvent evt)
  {
    if ((ticketView3 != null) && (ticketView3.isVisible())) {
      int selectedRow = ticketViewerTable.getSelectedRow();
      Object object = ticketViewerTable.get(selectedRow);
      
      if ((object instanceof TicketItem)) {
        transferTicketItem((TicketItem)object, ticketView3);
      }
    }
  }
  
  private void btnTransferToTicket2ActionPerformed(ActionEvent evt) {
    if ((ticketView2 != null) && (ticketView2.isVisible())) {
      int selectedRow = ticketViewerTable.getSelectedRow();
      Object object = ticketViewerTable.get(selectedRow);
      
      if ((object instanceof TicketItem)) {
        transferTicketItem((TicketItem)object, ticketView2);
      }
    }
  }
  
  private void btnTransferToTicket1ActionPerformed(ActionEvent evt) {
    if ((ticketView1 != null) && (ticketView1.isVisible())) {
      int selectedRow = ticketViewerTable.getSelectedRow();
      Object object = ticketViewerTable.get(selectedRow);
      
      if ((object instanceof TicketItem)) {
        transferTicketItem((TicketItem)object, ticketView1);
      }
    }
  }
  
  private void doScrollDown(ActionEvent evt) {
    ticketViewerTable.scrollDown();
  }
  
  private void doScrollUp(ActionEvent evt) {
    ticketViewerTable.scrollUp();
  }
  

  private JLabel jLabel5;
  
  private JLabel jLabel6;
  
  private TransparentPanel jPanel1;
  
  private TransparentPanel jPanel2;
  
  private TransparentPanel jPanel3;
  
  private TransparentPanel jPanel5;
  
  private JScrollPane jScrollPane1;
  
  private JSeparator jSeparator1;
  
  private JSeparator jSeparator2;
  
  private JSeparator jSeparator3;
  private JTextField tfDiscount;
  private JTextField tfSubtotal;
  private JTextField tfTax;
  private JTextField tfTotal;
  private TicketViewerTable ticketViewerTable;
  private GridBagConstraints gridBagConstraints_1;
  private GridBagConstraints gridBagConstraints_2;
  private GridBagConstraints gridBagConstraints_3;
  private GridBagConstraints gridBagConstraints_4;
  public void updateModel()
  {
    ticket.calculatePrice();
  }
  









  public void updateView()
  {
    if ((ticket == null) || (ticket.getTicketItems() == null) || (ticket.getTicketItems().size() <= 0)) {
      tfSubtotal.setText("");
      tfDiscount.setText("");
      tfTax.setText("");
      tfTotal.setText("");
      
      return;
    }
    
    ticket.calculatePrice();
    
    tfSubtotal.setText(NumberUtil.formatNumber(ticket.getSubtotalAmount()));
    tfDiscount.setText(NumberUtil.formatNumber(ticket.getDiscountAmount()));
    tfTax.setText(NumberUtil.formatNumber(ticket.getTaxAmount()));
    tfTotal.setText(NumberUtil.formatNumber(ticket.getTotalAmountWithTips()));
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void setTicket(Ticket _ticket) {
    ticket = _ticket;
    
    ticketViewerTable.setTicket(_ticket);
    
    updateView();
  }
  
  public void transferTicketItem(TicketItem ticketItem, TicketForSplitView toTicketView) {
    transferTicketItem(ticketItem, toTicketView, false);
  }
  
  public void transferTicketItem(TicketItem ticketItem, TicketForSplitView toTicketView, boolean fullTicketItem) {
    TicketItem newTicketItem = ticketItem.createNew();
    newTicketItem.setTaxIncluded(ticketItem.isTaxIncluded());
    if (!fullTicketItem) {
      newTicketItem.setQuantity(Double.valueOf(1.0D));
    } else {
      newTicketItem.setQuantity(ticketItem.getQuantity());
    }
    newTicketItem.setMenuItemId(ticketItem.getMenuItemId());
    
    newTicketItem.setName(ticketItem.getName());
    newTicketItem.setGroupName(ticketItem.getGroupName());
    newTicketItem.setCategoryName(ticketItem.getCategoryName());
    newTicketItem.setUnitPrice(ticketItem.getUnitPrice());
    newTicketItem.setTreatAsSeat(ticketItem.isTreatAsSeat());
    newTicketItem.setSeatNumber(ticketItem.getSeatNumber());
    










    newTicketItem.setDiscountsProperty(ticketItem.getDiscountsProperty());
    
    List<TicketItemModifier> ticketItemModifiers = ticketItem.getTicketItemModifiers();
    if (ticketItemModifiers != null) {
      for (TicketItemModifier ticketItemModifier : ticketItemModifiers) {
        TicketItemModifier newModifier = new TicketItemModifier();
        
        newModifier.setTaxIncluded(ticketItemModifier.isTaxIncluded());
        newModifier.setItemId(ticketItemModifier.getItemId());
        newModifier.setGroupId(ticketItemModifier.getGroupId());
        newModifier.setItemQuantity(ticketItemModifier.getItemQuantity());
        newModifier.setName(ticketItemModifier.getName());
        newModifier.setUnitPrice(ticketItemModifier.getUnitPrice());
        newModifier.setTaxes(ticketItemModifier.getTaxes());
        newModifier.setModifierType(ticketItemModifier.getModifierType());
        newModifier.setPrintedToKitchen(ticketItemModifier.isPrintedToKitchen());
        newModifier.setShouldPrintToKitchen(ticketItemModifier.isShouldPrintToKitchen());
        newModifier.setTicketItem(newTicketItem);
        newTicketItem.addToticketItemModifiers(newModifier);
      }
    }
    
    newTicketItem.setTaxes(ticketItem.getTaxes());
    newTicketItem.setBeverage(ticketItem.isBeverage());
    newTicketItem.setShouldPrintToKitchen(ticketItem.isShouldPrintToKitchen());
    newTicketItem.setPrinterGroup(ticketItem.getPrinterGroup());
    newTicketItem.setPrintedToKitchen(ticketItem.isPrintedToKitchen());
    
    double itemCount = ticketItem.getQuantity().doubleValue();
    
    ticketViewerTable.addTicketItem(newTicketItem);
    if ((itemCount > 1.0D) && (!fullTicketItem) && (!ticketItem.isHasModifiers().booleanValue())) {
      ticketItem.setQuantity(Double.valueOf(--itemCount));
    }
    else {
      ticketViewerTable.delete(ticketItem.getTableRowNum());
    }
    repaint();
    toTicketView.repaint();
    updateView();
    toTicketView.updateView();
  }
  
  public void transferAllTicketItem(TicketItem ticketItem, TicketForSplitView toTicketView) {
    TicketItem newTicketItem = ticketItem.createNew();
    newTicketItem.setTaxIncluded(ticketItem.isTaxIncluded());
    newTicketItem.setMenuItemId(ticketItem.getMenuItemId());
    newTicketItem.setQuantity(ticketItem.getQuantity());
    newTicketItem.setTicketItemModifiers(new ArrayList(ticketItem.getTicketItemModifiers()));
    newTicketItem.setName(ticketItem.getName());
    newTicketItem.setGroupName(ticketItem.getGroupName());
    newTicketItem.setCategoryName(ticketItem.getCategoryName());
    newTicketItem.setUnitPrice(ticketItem.getUnitPrice());
    









    newTicketItem.setDiscountsProperty(ticketItem.getDiscountsProperty());
    newTicketItem.setTaxes(ticketItem.getTaxes());
    newTicketItem.setBeverage(ticketItem.isBeverage());
    newTicketItem.setShouldPrintToKitchen(ticketItem.isShouldPrintToKitchen());
    newTicketItem.setPrinterGroup(ticketItem.getPrinterGroup());
    newTicketItem.setPrintedToKitchen(ticketItem.isPrintedToKitchen());
    
    ticketViewerTable.addAllTicketItem(newTicketItem);
    ticketViewerTable.delete(ticketItem.getTableRowNum());
    repaint();
    toTicketView.repaint();
    updateView();
    toTicketView.updateView();
  }
  
  public void tableChanged(TableModelEvent e) {
    if ((ticket == null) || (ticket.getTicketItems() == null) || (ticket.getTicketItems().size() <= 0)) {
      tfSubtotal.setText("");
      tfDiscount.setText("");
      tfTax.setText("");
      tfTotal.setText("");
      
      return;
    }
    
    ticket.calculatePrice();
    
    tfSubtotal.setText(NumberUtil.formatNumber(ticket.getSubtotalAmount()));
    tfDiscount.setText(NumberUtil.formatNumber(ticket.getDiscountAmount()));
    tfTax.setText(NumberUtil.formatNumber(ticket.getTaxAmount()));
    tfTotal.setText(NumberUtil.formatNumber(ticket.getTotalAmountWithTips()));
  }
  
  public TicketForSplitView getTicketView1() {
    return ticketView1;
  }
  
  public void setTicketView1(TicketForSplitView ticketView1) {
    this.ticketView1 = ticketView1;
  }
  
  public TicketForSplitView getTicketView2() {
    return ticketView2;
  }
  
  public void setTicketView2(TicketForSplitView ticketView2) {
    this.ticketView2 = ticketView2;
  }
  
  public TicketForSplitView getTicketView3() {
    return ticketView3;
  }
  
  public void setTicketView3(TicketForSplitView ticketView3) {
    this.ticketView3 = ticketView3;
  }
  
  public int getViewNumber() {
    return viewNumber;
  }
  
  public void setViewNumber(int viewNumber) {
    this.viewNumber = viewNumber;
    
    TitledBorder titledBorder = new TitledBorder(Messages.getString("TicketForSplitView.1") + viewNumber);
    titledBorder.setTitleJustification(2);
    
    setBorder(titledBorder);
    
    switch (viewNumber) {
    case 1: 
      btnTransferToTicket1.setIcon(IconFactory.getIcon("next.png"));
      btnTransferToTicket1.setText("2");
      btnTransferToTicket2.setIcon(IconFactory.getIcon("next.png"));
      btnTransferToTicket2.setText("3");
      btnTransferToTicket3.setIcon(IconFactory.getIcon("next.png"));
      btnTransferToTicket3.setText("4");
      break;
    
    case 2: 
      btnTransferToTicket1.setIcon(IconFactory.getIcon("previous.png"));
      btnTransferToTicket1.setText("1");
      btnTransferToTicket2.setIcon(IconFactory.getIcon("next.png"));
      btnTransferToTicket2.setText("3");
      btnTransferToTicket3.setIcon(IconFactory.getIcon("next.png"));
      btnTransferToTicket3.setText("4");
      break;
    
    case 3: 
      btnTransferToTicket1.setIcon(IconFactory.getIcon("previous.png"));
      btnTransferToTicket1.setText("1");
      btnTransferToTicket2.setIcon(IconFactory.getIcon("previous.png"));
      btnTransferToTicket2.setText("2");
      btnTransferToTicket3.setIcon(IconFactory.getIcon("next.png"));
      btnTransferToTicket3.setText("4");
      
      break;
    
    case 4: 
      btnTransferToTicket1.setIcon(IconFactory.getIcon("previous.png"));
      btnTransferToTicket1.setText("1");
      btnTransferToTicket2.setIcon(IconFactory.getIcon("previous.png"));
      btnTransferToTicket2.setText("2");
      btnTransferToTicket3.setIcon(IconFactory.getIcon("previous.png"));
      btnTransferToTicket3.setText("3");
      break;
    
    default: 
      throw new RuntimeException(Messages.getString("TicketForSplitView.2"));
    }
  }
}
