package com.floreantpos.ui.views.order;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.dialog.POSDialog;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;


















public class OrderTypeSelectionDialog2
  extends POSDialog
{
  private String selectedOrderType;
  private PosButton btnForHere;
  private PosButton btnToGo;
  
  public OrderTypeSelectionDialog2()
    throws HeadlessException
  {
    initializeComponent();
  }
  
  private void initializeComponent() {
    setTitle(Messages.getString("OrderTypeSelectionDialog.0"));
    setResizable(false);
    setLayout(new BorderLayout(5, 5));
    
    JPanel orderTypePanel = new JPanel(new GridLayout(1, 0, 10, 10));
    orderTypePanel.setBorder(new EmptyBorder(10, 10, 10, 10));
    
    btnForHere = new PosButton("FOR HERE");
    btnForHere.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        selectedOrderType = "FOR HERE";
        setCanceled(false);
        dispose();
      }
    });
    orderTypePanel.add(btnForHere);
    
    btnToGo = new PosButton("TO GO");
    btnToGo.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        selectedOrderType = "TO GO";
        setCanceled(false);
        dispose();
      }
    });
    orderTypePanel.add(btnToGo);
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL_BUTTON_TEXT);
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
      
    });
    JPanel actionPanel = new JPanel(new MigLayout("fill"));
    actionPanel.add(btnCancel, "growx, span");
    
    add(orderTypePanel);
    add(actionPanel, "South");
    
    setSize(400, 250);
  }
  
  public String getSelectedOrderType() {
    return selectedOrderType;
  }
}
