package com.floreantpos.ui.views.order;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.actions.VoidTicketItemAction;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.ModifiableTicketItem;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemCookingInstruction;
import com.floreantpos.model.TicketItemModifier;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.ComboTicketItemSelectionDialog;
import com.floreantpos.ui.dialog.ItemSearchDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.SendToKitchenOptionSelectionDialog;
import com.floreantpos.ui.ticket.TicketViewerTable;
import com.floreantpos.ui.ticket.TicketViewerTableCellRenderer;
import com.floreantpos.ui.ticket.TicketViewerTableModel;
import com.floreantpos.ui.views.CookingInstructionSelectionView;
import com.floreantpos.ui.views.IView;
import com.floreantpos.ui.views.order.actions.DataChangeListener;
import com.floreantpos.ui.views.order.actions.OrderListener;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.DrawerUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import net.miginfocom.swing.MigLayout;
import org.hibernate.StaleStateException;





















public class TicketView
  extends JPanel
{
  private final Vector<OrderListener> orderListeners = new Vector();
  private Ticket ticket;
  private PosButton btnDecreaseAmount;
  private final PosButton btnTenIncrease = new PosButton(IconFactory.getIcon("/ui_icons/", "page-up.png"));
  private final PosButton btnTenDecrease = new PosButton(IconFactory.getIcon("/ui_icons/", "page-down.png"));
  private final PosButton btnDelete = new PosButton();
  private final PosButton btnIncreaseAmount = new PosButton();
  private final PosButton btnEdit = new PosButton(IconFactory.getIcon("/ui_icons/", "edit.png"));
  private final PosButton btnEditItemQuantity = new PosButton(IconFactory.getIcon("/ui_icons/", "add-multiple.png"));
  private PosButton btnScrollDown;
  private final PosButton btnScrollUp = new PosButton();
  private TransparentPanel ticketItemActionPanel;
  private JScrollPane ticketScrollPane;
  private PosButton btnTotal;
  private TicketViewerTable ticketViewerTable;
  private ItemSearchDialog itemSearchDialog = new ItemSearchDialog(Application.getPosWindow());
  private JPanel itemSearchPanel;
  private JTextField txtSearchItem;
  private final PosButton btnCookingInstruction = new PosButton(IconFactory.getIcon("/ui_icons/", "cooking-instruction.png"));
  private final TitledBorder titledBorder = new TitledBorder("");
  private final Border border = new CompoundBorder(titledBorder, new EmptyBorder(2, 2, 2, 2));
  private boolean cancelable;
  private boolean allowToLogOut;
  public static final String VIEW_NAME = "TICKET_VIEW";
  
  public TicketView() {
    initComponents();
  }
  






  private void initComponents()
  {
    titledBorder.setTitleJustification(2);
    setBorder(border);
    setLayout(new BorderLayout(5, 5));
    itemSearchPanel = new JPanel();
    
    ticketItemActionPanel = new TransparentPanel();
    btnDecreaseAmount = new PosButton();
    

    btnScrollDown = new PosButton();
    ticketViewerTable = new TicketViewerTable();
    ticketScrollPane = new PosScrollPane(ticketViewerTable);
    ticketScrollPane.setHorizontalScrollBarPolicy(31);
    ticketScrollPane.setVerticalScrollBarPolicy(21);
    ticketScrollPane.setPreferredSize(PosUIManager.getSize(180, 200));
    
    btnEdit.setEnabled(false);
    btnCookingInstruction.setEnabled(false);
    createPayButton();
    
    createTicketItemControlPanel();
    createItemSearchPanel();
    
    JPanel centerPanel = new JPanel(new BorderLayout(5, 5));
    centerPanel.add(ticketScrollPane);
    
    add(itemSearchPanel, "North");
    add(centerPanel);
    add(ticketItemActionPanel, "East");
    ticketViewerTable.getRenderer().setInTicketScreen(true);
    ticketViewerTable.getSelectionModel().addListSelectionListener(new TicketItemSelectionListener(null));
    setPreferredSize(PosUIManager.getSize(320, 463));
  }
  
  private void createItemSearchPanel() {
    itemSearchPanel.setLayout(new BorderLayout(5, 5));
    PosButton btnSearch = new PosButton(IconFactory.getIcon("/ui_icons/", "search.png"));
    btnSearch.setPreferredSize(PosUIManager.getSize(50, 40));
    txtSearchItem = new JTextField();
    txtSearchItem.requestFocusInWindow();
    txtSearchItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        String searchString = txtSearchItem.getText();
        if (searchString.equals("")) {
          POSMessageDialog.showMessage(Messages.getString("TicketView.5"));
          return;
        }
        MenuItem menuItem = MenuItemDAO.getInstance().getMenuItemByBarcodeOrSKU(searchString);
        if (menuItem != null) {
          OrderView.getInstance().getOrderController().itemSelected(menuItem);
        }
        txtSearchItem.setText("");
      }
      
    });
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        itemSearchDialog.setTitle(Messages.getString("TicketView.7"));
        itemSearchDialog.updateFilterPanel(ticket.getOrderType());
        itemSearchDialog.pack();
        itemSearchDialog.openFullScreen();
        if (itemSearchDialog.isCanceled()) {
          return;
        }
        
        txtSearchItem.requestFocus();
        
        if (itemSearchDialog.getMenuItem() == null) {
          POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("TicketView.8"));
          return;
        }
        OrderView.getInstance().getOrderController().itemSelected(itemSearchDialog.getMenuItem());
      }
    });
    itemSearchPanel.add(txtSearchItem);
    itemSearchPanel.add(btnSearch, "East");
  }
  
  private void createPayButton() {
    btnTotal = new PosButton(POSConstants.TOTAL.toUpperCase());
    btnTotal.setFont(btnTotal.getFont().deriveFont(1));
    
    if (!Application.getInstance().getTerminal().isHasCashDrawer().booleanValue()) {
      btnTotal.setEnabled(false);
    }
    
    btnTotal.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        TicketView.this.doPayNow();
      }
      
    });
    add(btnTotal, "South");
  }
  
  private void createTicketItemControlPanel() {
    ticketItemActionPanel.setLayout(new MigLayout("fill, hidemode 3, ins 0 0 0 0", "sg, fill", ""));
    btnScrollUp.setIcon(IconFactory.getIcon("/ui_icons/", "up.png"));
    btnScrollUp.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent evt) {
        TicketView.this.doScrollUp();
      }
    });
    btnTenIncrease.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TicketView.this.scrollPageUp();















      }
      















    });
    btnScrollDown.setIcon(IconFactory.getIcon("/ui_icons/", "down.png"));
    btnScrollDown.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent evt) {
        TicketView.this.doScrollDown();
      }
    });
    btnTenDecrease.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TicketView.this.scrollPageDown();
      }
      
    });
    btnDelete.setText("");
    btnDelete.setIcon(IconFactory.getIcon("/ui_icons/", "delete.png"));
    btnDelete.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent evt) {
        TicketView.this.doDeleteSelection();
      }
      
    });
    btnEdit.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent evt) {
        TicketView.this.doEditSelection();
      }
    });
    btnEditItemQuantity.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent evt) {
        TicketView.this.doEditItemQuantity();
      }
    });
    btnCookingInstruction.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        doAddCookingInstruction();
      }
    });
    ticketItemActionPanel.add(btnTenIncrease, "grow, wrap");
    ticketItemActionPanel.add(btnScrollUp, "grow, wrap");
    

    ticketItemActionPanel.add(btnEditItemQuantity, "grow, wrap");
    ticketItemActionPanel.add(btnDelete, "grow, wrap");
    ticketItemActionPanel.add(btnEdit, "grow, wrap");
    ticketItemActionPanel.add(btnCookingInstruction, "grow, wrap");
    ticketItemActionPanel.add(btnScrollDown, "grow, wrap");
    ticketItemActionPanel.add(btnTenDecrease, "grow");
    
    ticketItemActionPanel.setPreferredSize(PosUIManager.getSize(50, 0));
  }
  
  private void doEditItemQuantity() {
    ITicketItem object = ticketViewerTable.getSelected();
    
    if (object == null) {
      return;
    }
    if ((!(object instanceof TicketItem)) || (((TicketItem)object).isTreatAsSeat().booleanValue())) {
      return;
    }
    TicketItem ticketItem = (TicketItem)object;
    OrderView.getInstance().getOrderController().doEditTicketItemQuantity(ticketItem);
    updateView();
    int selectedRow = ticketViewerTable.getSelectedRow();
    ticketViewerTable.getModel().fireTableRowsUpdated(selectedRow, selectedRow);
    ticketViewerTable.fireTicketItemUpdated(getTicket(), ticketItem);
  }
  
  public synchronized void doFinishOrder() {
    saveTicketIfNeeded();
    sendTicketToKitchen();
    closeView(false);
  }
  
  public synchronized void sendTicketToKitchenByOption()
  {
    SendToKitchenOptionSelectionDialog sendTicketDialog = new SendToKitchenOptionSelectionDialog(ticket);
    sendTicketDialog.pack();
    sendTicketDialog.open();
    if (sendTicketDialog.isCanceled()) {
      return;
    }
    saveTicketIfNeeded();
    setCancelable(false);
    setAllowToLogOut(false);
  }
  
  public synchronized void sendTicketToKitchen()
  {
    if ((ticket.getOrderType().isShouldPrintToKitchen().booleanValue()) && 
      (ticket.needsKitchenPrint())) {
      ReceiptPrintService.printToKitchen(ticket);
      setCancelable(false);
      setAllowToLogOut(false);
    }
  }
  
  public synchronized void doHoldOrder()
  {
    updateModel();
    OrderController.saveOrder(ticket);
    closeView(false);
  }
  
  public void saveTicketIfNeeded() {
    updateModel();
    

    OrderController.saveOrder(ticket);
  }
  
  private void closeView(boolean orderCanceled) {
    DataChangeListener listener = RootView.getInstance().getHomeView().getDataChangeListener();
    if (((!orderCanceled) || (ticket.getId() != null) || (!ticket.getOrderType().isShowTableSelection().booleanValue())) || 
    

      (listener != null)) {
      if (orderCanceled) {
        listener.dataChangeCanceled(ticket);
      }
      else
        listener.dataChanged(ticket);
    }
    RootView.getInstance().showDefaultView();
  }
  
  public void doCancelOrder() {
    closeView(true);
  }
  
  private synchronized void updateModel() {
    if ((ticket.getTicketItems() == null) || (ticket.getTicketItems().size() == 0)) {
      throw new PosException(POSConstants.TICKET_IS_EMPTY_);
    }
    ticket.calculatePrice();
  }
  
  private void doPayNow() {
    try {
      firePayOrderSelected();
    } catch (PosException e) {
      POSMessageDialog.showError(e.getMessage());
    } catch (StaleStateException x) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("OrderView.0"));
      return;
    }
  }
  
  private void doDeleteSelection() {
    ITicketItem selected = ticketViewerTable.getSelected();
    if (selected == null)
      return;
    boolean inventoryAdjusted = false;
    if ((selected instanceof TicketItem)) {
      inventoryAdjusted = ((TicketItem)selected).getInventoryAdjustQty().doubleValue() > 0.0D;
    }
    if ((selected.isPrintedToKitchen().booleanValue()) || (inventoryAdjusted)) {
      TicketItem ticketItem = (TicketItem)selected;
      if (isExceedVoidQuantity(ticketItem)) {
        POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Void item quantity limit exceed.");
        return;
      }
      VoidTicketItemAction action = new VoidTicketItemAction(ticket, ticketItem);
      action.actionPerformed(null);
      if (action.isDataChanged()) {
        ticketViewerTable.updateView();
        updateView();
      }
    }
    else {
      ticketViewerTable.deleteSelectedItem();
      updateView();
    }
  }
  
  private boolean isExceedVoidQuantity(TicketItem selectedTicketItem)
  {
    double quantity = 0.0D;
    for (TicketItem ticketItem : ticket.getTicketItems()) {
      String s1 = selectedTicketItem.getMenuItemId().equals("0") ? selectedTicketItem.getName() : selectedTicketItem.getMenuItemId();
      String s2 = ticketItem.getMenuItemId().equals("0") ? ticketItem.getName() : ticketItem.getMenuItemId();
      
      if (s1.equals(s2)) {
        quantity += ticketItem.getQuantity().doubleValue();
      }
    }
    return quantity <= 0.0D;
  }
  
  private void doEditSelection() {
    Object object = ticketViewerTable.getSelected();
    if (object == null) {
      return;
    }
    if ((object instanceof TicketItem)) {
      TicketItem ticketItem = (TicketItem)object;
      
      if (ticketItem.isTreatAsSeat().booleanValue()) {
        if (OrderView.getInstance().updateSeat(ticketItem)) {}

      }
      else if (ticketItem.isComboItem().booleanValue()) {
        ComboTicketItemSelectionDialog dialog = new ComboTicketItemSelectionDialog(ticketItem.getMenuItem(), ticketItem);
        dialog.setTitle("SELECT COMBO ITEMS");
        dialog.setSize(PosUIManager.getSize(800, 600));
        dialog.open();
        
        if (dialog.isCanceled()) {
          return;
        }
      }
      else {
        OrderController.openModifierDialog((ITicketItem)object);
      }
    }
    else {
      OrderController.openModifierDialog((ITicketItem)object);
    }
    ticketViewerTable.rowTobeSelectedOnResize = -1;
    updateView();
  }
  
  private void doScrollDown() {
    ticketViewerTable.scrollDown();
  }
  
  private void doScrollUp() {
    ticketViewerTable.scrollUp();
  }
  
  private void scrollPageUp() {
    ticketViewerTable.scrollUpTen();
  }
  
  private void scrollPageDown()
  {
    ticketViewerTable.scrollDownTen();
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void setTicket(Ticket _ticket) {
    ticket = _ticket;
    
    ticketViewerTable.setTicket(_ticket);
    updateView();
    setCancelable(true);
    setAllowToLogOut(true);
  }
  
  public void addTicketItem(TicketItem ticketItem) {
    ticketViewerTable.addTicketItem(ticketItem);
    updateView();
  }
  
  public void removeModifier(TicketItem parent, TicketItemModifier modifier) {
    modifier.setItemQuantity(Double.valueOf(0.0D));
    
    ticketViewerTable.removeModifier(parent, modifier);
  }
  
  public void selectRow(int rowIndex) {
    ticketViewerTable.selectRow(rowIndex);
  }
  
  public void updateView() {
    if (ticket == null) {
      btnTotal.setText(POSConstants.TOTAL.toUpperCase() + " " + CurrencyUtil.getCurrencySymbol() + "0.00");
      titledBorder.setTitle(ticket.getOrderType().toString() + "[New Ticket]");
      return;
    }
    ticket.calculatePrice();
    if (ticket.getDueAmount().doubleValue() < 0.0D) {
      String dueAmountString = NumberUtil.formatNumber(ticket.getDueAmount(), true);
      String textToDisplay = "DUE: -" + CurrencyUtil.getCurrencySymbol() + dueAmountString.replaceFirst("-", "");
      btnTotal.setText(textToDisplay);
    }
    else {
      String dueAmountString = NumberUtil.formatNumber(ticket.getDueAmount(), true);
      btnTotal.setText("DUE: " + CurrencyUtil.getCurrencySymbol() + dueAmountString);
    }
    updateTicketTitle();
    showCustomerDisplayInfo(ticketViewerTable.getSelected());
  }
  
  public void updateTicketTitle() {
    String ticketType = ticket.getOrderType().toString();
    if (ticket.getId() == null) {
      titledBorder.setTitle(ticketType + " [New Ticket]");
    }
    else {
      StringBuilder titleBuilder = new StringBuilder();
      String ticketName = Messages.getString("TicketView.37");
      titleBuilder.append(ticketType);
      titleBuilder.append(" [ " + ticketName + " " + ticket.getTokenNo() + " ]");
      titledBorder.setTitle(titleBuilder.toString());
    }
    repaint();
  }
  
  public void addOrderListener(OrderListener listenre) {
    orderListeners.add(listenre);
  }
  
  public void removeOrderListener(OrderListener listenre) {
    orderListeners.remove(listenre);
  }
  
  public void firePayOrderSelected() {
    for (OrderListener listener : orderListeners) {
      listener.payOrderSelected(getTicket());
    }
  }
  
  public void setControlsVisible(boolean visible) {
    if (visible) {
      btnIncreaseAmount.setEnabled(true);
      btnDecreaseAmount.setEnabled(true);
      btnDelete.setEnabled(true);
    }
    else {
      btnIncreaseAmount.setEnabled(false);
      btnDecreaseAmount.setEnabled(false);
      btnDelete.setEnabled(false);
    }
  }
  

  public TicketViewerTable getTicketViewerTable() { return ticketViewerTable; }
  
  private class TicketItemSelectionListener implements ListSelectionListener {
    private TicketItemSelectionListener() {}
    
    public void valueChanged(ListSelectionEvent e) {
      Object selected = ticketViewerTable.getSelected();
      if (!(selected instanceof ITicketItem)) {
        return;
      }
      

      btnEdit.setEnabled(true);
      btnDecreaseAmount.setEnabled(true);
      btnIncreaseAmount.setEnabled(true);
      btnDelete.setEnabled(true);
      btnCookingInstruction.setEnabled(true);
      btnEditItemQuantity.setEnabled(true);
      
      ITicketItem iTicketItem = (ITicketItem)selected;
      btnCookingInstruction.setEnabled(iTicketItem.canAddCookingInstruction());
      if (iTicketItem.isPrintedToKitchen().booleanValue()) {
        btnIncreaseAmount.setEnabled(false);
        btnDecreaseAmount.setEnabled(false);
        btnCookingInstruction.setEnabled(false);
        btnEditItemQuantity.setEnabled(false);
      }
      else if ((selected instanceof TicketItemModifier))
      {

        btnEdit.setEnabled(true);
        btnDelete.setEnabled(false);

      }
      else
      {
        btnDelete.setEnabled(true);
        btnEdit.setEnabled(false);
        
        if ((selected instanceof TicketItem)) {
          TicketItem ticketItem = (TicketItem)selected;
          btnEditItemQuantity.setEnabled((ticketItem.getInventoryAdjustQty().doubleValue() == 0.0D) && (!ticketItem.isVoided().booleanValue()));
          btnDelete.setEnabled((ticketItem.getId() == null) || (!ticketItem.isVoided().booleanValue()));
          if ((!ticketItem.isPrintedToKitchen().booleanValue()) || (
          



            (ticketItem.isTreatAsSeat().booleanValue()) || (ticketItem.isComboItem().booleanValue()))) {
            btnEdit.setEnabled(!ticketItem.isPrintedToKitchen().booleanValue());
          }
          else if ((ticketItem instanceof ModifiableTicketItem))
          {

            btnEdit.setEnabled(true);
          }
          else if (ticketItem.isFractionalUnit().booleanValue())
          {

            btnDelete.setEnabled(true);
          }
        }
      }
      
      if (ticket.isSourceOnline()) {
        btnEdit.setEnabled(false);
        btnDecreaseAmount.setEnabled(false);
        btnIncreaseAmount.setEnabled(false);
        btnDelete.setEnabled(false);
        btnCookingInstruction.setEnabled(false);
        btnEditItemQuantity.setEnabled(false);
      }
      TicketView.this.showCustomerDisplayInfo(selected);
    }
  }
  
  private String getDisplayMessage(ITicketItem item, String totalPrice) {
    String itemName = item.getNameDisplay();
    if (itemName.length() > 10) {
      itemName = item.getNameDisplay().substring(0, 10);
    }
    else {
      itemName = item.getNameDisplay();
    }
    double itemPrice = item.getSubTotalAmountDisplay() == null ? 0.0D : item.getSubTotalAmountDisplay().doubleValue();
    String line = String.format("%-10s %9s", new Object[] { itemName, NumberUtil.formatNumber(Double.valueOf(itemPrice), true) });
    if (line.length() > 20) {
      line = line.substring(0, 20);
    }
    
    String total = Messages.getString("TicketView.29") + CurrencyUtil.getCurrencySymbol();
    String line2 = String.format("%-8s %11s", new Object[] { total, totalPrice });
    if (line2.length() > 20) {
      line2 = line2.substring(0, 20);
    }
    return line + line2;
  }
  


  public boolean isCancelable()
  {
    return cancelable;
  }
  


  public void setCancelable(boolean cancelable)
  {
    this.cancelable = cancelable;
  }
  


  public boolean isAllowToLogOut()
  {
    return allowToLogOut;
  }
  


  public void setAllowToLogOut(boolean allowToLogOut)
  {
    this.allowToLogOut = allowToLogOut;
  }
  
  public boolean isStockAvailable(MenuItem menuItem, TicketItem selectedTicketItem, double selectedItemQuantity)
  {
    if (!menuItem.isDisableWhenStockAmountIsZero().booleanValue()) {
      return true;
    }
    
    List<TicketItem> ticketItems = ticketViewerTable.getTicketItems();
    
    if (menuItem.isFractionalUnit().booleanValue())
    {
      if ((ticketItems == null) || (ticketItems.isEmpty())) {
        if (menuItem.getAvailableUnit().doubleValue() < selectedTicketItem.getQuantity().doubleValue()) {
          return false;
        }
        return true;
      }
      
      double totalItemQuantity = 0.0D;
      
      for (TicketItem tItem : ticketItems)
      {
        if (menuItem.getName().equals(tItem.getName()))
        {
          totalItemQuantity += tItem.getQuantity().doubleValue();
          
          if (menuItem.getAvailableUnit().doubleValue() < totalItemQuantity) {
            return false;
          }
        }
      }
      
      if (selectedItemQuantity != -1.0D)
      {
        totalItemQuantity -= selectedTicketItem.getQuantity().doubleValue();
        
        totalItemQuantity += selectedItemQuantity;
      }
      else {
        totalItemQuantity += selectedTicketItem.getQuantity().doubleValue();
      }
      
      if (menuItem.getAvailableUnit().doubleValue() < totalItemQuantity) {
        return false;
      }
      return true;
    }
    
    if ((ticketItems == null) || (ticketItems.isEmpty())) {
      if (menuItem.getAvailableUnit().doubleValue() < selectedTicketItem.getQuantity().doubleValue()) {
        return false;
      }
      return true;
    }
    
    int totalItemCount = 0;
    
    for (TicketItem tItem : ticketItems)
    {
      if (tItem.getName().equals(menuItem.getName()))
      {
        totalItemCount = (int)(totalItemCount + tItem.getQuantity().doubleValue());
        
        if (menuItem.getAvailableUnit().doubleValue() <= totalItemCount) {
          return false;
        }
      }
    }
    return true;
  }
  
  protected void doAddCookingInstruction() {
    try {
      Object object = ticketViewerTable.getSelected();
      if (!(object instanceof TicketItem)) {
        POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("TicketView.20"));
        return;
      }
      
      TicketItem ticketItem = (TicketItem)object;
      
      if (ticketItem.isPrintedToKitchen().booleanValue()) {
        POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("TicketView.21"));
        return;
      }
      
      CookingInstructionSelectionView dialog = new CookingInstructionSelectionView();
      dialog.setSize(800, 600);
      dialog.setLocationRelativeTo(Application.getPosWindow());
      dialog.setVisible(true);
      
      if (dialog.isCanceled()) {
        return;
      }
      
      List<TicketItemCookingInstruction> instructions = dialog.getTicketItemCookingInstructions();
      ticketItem.addCookingInstructions(instructions);
      ticketViewerTable.updateView();
    } catch (Exception e) {
      PosLog.error(getClass(), e);
      POSMessageDialog.showError(e.getMessage());
    }
  }
  
  public void showSettleButton(boolean show) {
    btnTotal.setVisible(show);
    btnEdit.setVisible(show);
    btnCookingInstruction.setVisible(show);
  }
  
  private void showCustomerDisplayInfo(Object selected) {
    if (TerminalConfig.isActiveCustomerDisplay()) {
      TicketItem ticketItem = null;
      if ((selected instanceof TicketItemModifier)) {
        ticketItem = ((TicketItemModifier)selected).getTicketItem();
      }
      else if ((selected instanceof TicketItem)) {
        ticketItem = (TicketItem)selected;
      }
      if (ticketItem != null) {
        String sendMessageToCustomerDisplay = getDisplayMessage(ticketItem, NumberUtil.formatNumber(ticket.getTotalAmountWithTips(), true));
        DrawerUtil.setItemDisplay(TerminalConfig.getCustomerDisplayPort(), sendMessageToCustomerDisplay);
      }
    }
  }
  
  public void setVisible(boolean visible)
  {
    super.setVisible(visible);
    if (visible) {
      if (txtSearchItem.getParent() == null) {
        itemSearchPanel.add(txtSearchItem);
      }
      txtSearchItem.requestFocus();
    }
    else {
      itemSearchPanel.remove(txtSearchItem);
    }
  }
}
