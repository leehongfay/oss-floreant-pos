package com.floreantpos.ui.views.order;

import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.dialog.OpenTicketsListDialog;
import com.floreantpos.ui.dialog.POSDialog;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
















public class CashierModeNextActionDialog
  extends POSDialog
  implements ActionListener
{
  PosButton btnNew = new PosButton(Messages.getString("CashierModeNextActionDialog.0"));
  PosButton btnOpen = new PosButton(Messages.getString("CashierModeNextActionDialog.1"));
  PosButton btnLogout = new PosButton(Messages.getString("CashierModeNextActionDialog.2"));
  
  JLabel messageLabel = new JLabel("", 0);
  
  public CashierModeNextActionDialog(String message) {
    super(Application.getPosWindow(), true);
    
    setTitle(Messages.getString("CashierModeNextActionDialog.4"));
    
    JPanel contentPane = new JPanel(new BorderLayout(10, 20));
    contentPane.setBorder(BorderFactory.createEmptyBorder(20, 10, 20, 10));
    contentPane.add(messageLabel, "North");
    
    JPanel buttonPanel = new JPanel(new GridLayout(1, 0, 5, 5));
    
    buttonPanel.add(btnNew);
    buttonPanel.add(btnOpen);
    buttonPanel.add(btnLogout);
    contentPane.add(buttonPanel);
    
    setContentPane(contentPane);
    
    btnNew.addActionListener(this);
    btnOpen.addActionListener(this);
    btnLogout.addActionListener(this);
    
    messageLabel.setFont(messageLabel.getFont().deriveFont(1, 16.0F));
    messageLabel.setText(message);
    
    setSize(550, 180);
    setResizable(false);
    
    Application.getPosWindow().setGlassPaneVisible(true);
  }
  

  public void actionPerformed(ActionEvent e)
  {
    if (e.getSource() != btnNew)
    {


      if (e.getSource() == btnLogout) {
        Application.getInstance().doLogout();
      }
      else if (e.getSource() == btnOpen) {
        OpenTicketsListDialog dialog = new OpenTicketsListDialog();
        dialog.open();
      }
    }
    Application.getPosWindow().setGlassPaneVisible(false);
    dispose();
  }
}
