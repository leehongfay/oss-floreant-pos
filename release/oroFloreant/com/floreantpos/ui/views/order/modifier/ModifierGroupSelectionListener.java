package com.floreantpos.ui.views.order.modifier;

import com.floreantpos.model.MenuItemModifierSpec;

public abstract interface ModifierGroupSelectionListener
{
  public abstract void modifierGroupSelected(MenuItemModifierSpec paramMenuItemModifierSpec);
}
