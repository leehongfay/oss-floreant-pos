package com.floreantpos.ui.views.order.modifier;

import com.floreantpos.IconFactory;
import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemModifier;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.NumberSelectionDialog2;
import com.floreantpos.ui.views.order.actions.OrderListener;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang3.StringUtils;

























public class TicketItemModifierTableView
  extends JPanel
{
  private Vector<OrderListener> orderListeners = new Vector();
  
  public static final String VIEW_NAME = "TICKET_MODIFIER_VIEW";
  
  private Vector<ModifierSelectionListener> listenerList = new Vector();
  
  private ModifierSelectionModel modifierSelectionModel;
  
  private TransparentPanel ticketActionPanel = new TransparentPanel();
  

  private PosButton btnEditItemQuantity = new PosButton(IconFactory.getIcon("/ui_icons/", "add-multiple.png"));
  private PosButton btnDelete = new PosButton(IconFactory.getIcon("/ui_icons/", "delete.png"));
  
  private PosButton btnScrollDown;
  private PosButton btnScrollUp = new PosButton(IconFactory.getIcon("/ui_icons/", "up.png"));
  
  private TransparentPanel ticketItemActionPanel;
  
  private JScrollPane ticketScrollPane;
  
  private ModifierViewerTable modifierViewerTable;
  private TitledBorder titledBorder = new TitledBorder("");
  private Border border = new CompoundBorder(titledBorder, new EmptyBorder(5, 5, 5, 5));
  
  public TicketItemModifierTableView(ModifierSelectionModel modifierSelectionModel) {
    this.modifierSelectionModel = modifierSelectionModel;
    
    initComponents();
  }
  
  private void initComponents() {
    titledBorder.setTitle(modifierSelectionModel.getTicketItem().getName());
    titledBorder.setTitleJustification(2);
    setBorder(border);
    setLayout(new BorderLayout(5, 5));
    
    ticketItemActionPanel = new TransparentPanel();
    
    btnScrollDown = new PosButton();
    modifierViewerTable = new ModifierViewerTable(modifierSelectionModel.getTicketItem());
    ticketScrollPane = new PosScrollPane(modifierViewerTable);
    ticketScrollPane.setHorizontalScrollBarPolicy(31);
    ticketScrollPane.setVerticalScrollBarPolicy(21);
    ticketScrollPane.setPreferredSize(PosUIManager.getSize(180, 200));
    
    createTicketActionPanel();
    
    createTicketItemControlPanel();
    
    JPanel centerPanel = new JPanel(new BorderLayout(5, 5));
    centerPanel.add(ticketScrollPane);
    
    centerPanel.add(createItemDescriptionPanel(), "North");
    add(centerPanel);
    add(ticketActionPanel, "South");
    centerPanel.add(ticketItemActionPanel, "East");
    
    modifierViewerTable.getRenderer().setInTicketScreen(true);
    modifierViewerTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
          TicketItemModifierTableView.this.updateSelectionView();
        }
        
      }
    });
    modifierViewerTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        Object selected = modifierViewerTable.getSelected();
        if (!(selected instanceof ITicketItem)) {
          return;
        }
        
        ITicketItem item = (ITicketItem)selected;
        
        Boolean printedToKitchen = item.isPrintedToKitchen();
        btnDelete.setEnabled(!printedToKitchen.booleanValue());
      }
      

    });
    setPreferredSize(PosUIManager.getSize(360, 463));
  }
  
  private JPanel createItemDescriptionPanel()
  {
    MenuItem menuItem = modifierSelectionModel.getMenuItem();
    JPanel itemDescriptionPanel = new JPanel(new MigLayout("inset 0,center"));
    String description = menuItem.getDescription();
    if ((StringUtils.isEmpty(description)) && (menuItem.getImage() == null)) {
      return itemDescriptionPanel;
    }
    itemDescriptionPanel.setBorder(BorderFactory.createTitledBorder("-"));
    JLabel lblDescription = new JLabel();
    lblDescription.setText("<html><body>" + description + "</body></html>");
    JLabel pictureLabel = new JLabel(menuItem.getImage());
    
    itemDescriptionPanel.add(pictureLabel);
    itemDescriptionPanel.add(lblDescription);
    
    return itemDescriptionPanel;
  }
  
  private void createTicketActionPanel() {
    ticketActionPanel.setLayout(new GridLayout(1, 0, 5, 5));
  }
  
  private void createTicketItemControlPanel() {
    ticketItemActionPanel.setLayout(new GridLayout(0, 1, 5, 5));
    
    btnScrollUp.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketItemModifierTableView.this.doScrollUp(evt);






      }
      







    });
    btnScrollDown.setIcon(IconFactory.getIcon("/ui_icons/", "down.png"));
    btnScrollDown.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketItemModifierTableView.this.doScrollDown(evt);
      }
      
    });
    btnDelete.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketItemModifierTableView.this.doDeleteSelection(evt);
      }
      
    });
    btnEditItemQuantity.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketItemModifierTableView.this.doEditItemQuantity();
      }
      
    });
    ticketItemActionPanel.add(btnScrollUp);
    

    ticketItemActionPanel.add(btnEditItemQuantity);
    ticketItemActionPanel.add(btnDelete);
    ticketItemActionPanel.add(btnScrollDown);
    
    ticketItemActionPanel.setPreferredSize(new Dimension(60, 360));
  }
  
  private void doEditItemQuantity() {
    TicketItemModifier ticketItemModifier = (TicketItemModifier)modifierViewerTable.getSelected();
    
    if (ticketItemModifier == null) {
      return;
    }
    double quantity = 0.0D;
    
    if (ticketItemModifier.isFractionalUnit().booleanValue()) {
      quantity = NumberSelectionDialog2.takeDoubleInput("Enter quantity", ticketItemModifier.getItemQuantity().doubleValue());
    } else {
      quantity = NumberSelectionDialog2.takeIntInput("Enter quantity", ticketItemModifier.getItemQuantity().doubleValue());
    }
    if (quantity < 1.0D) {
      return;
    }
    ticketItemModifier.setItemQuantity(Double.valueOf(quantity));
    updateView();
  }
  
  public void addModifierSelectionListener(ModifierSelectionListener listener) {
    listenerList.add(listener);
  }
  
  public void removeModifierSelectionListener(ModifierSelectionListener listener) {
    listenerList.remove(listener);
  }
  
  private void doDeleteSelection(ActionEvent evt) {
    Object object = modifierViewerTable.deleteSelectedItem();
    if (object != null) {
      updateView();
      for (ModifierSelectionListener listener : listenerList) {
        listener.modifierRemoved((TicketItemModifier)object);
      }
    }
  }
  
  private void doScrollDown(ActionEvent evt)
  {
    modifierViewerTable.scrollDown();
  }
  
  private void doScrollUp(ActionEvent evt) {
    modifierViewerTable.scrollUp();
  }
  
  public void removeModifier(TicketItem parent, TicketItemModifier modifier) {
    modifier.setItemQuantity(Double.valueOf(0.0D));
    
    modifierViewerTable.removeModifier(parent, modifier);
  }
  
  public void updateAllView() {
    modifierViewerTable.updateView();
    updateView();
  }
  
  public void selectRow(int rowIndex) {
    modifierViewerTable.selectRow(rowIndex);
  }
  
  public void updateView()
  {
    modifierViewerTable.updateView();
  }
  





















  public void addOrderListener(OrderListener listenre)
  {
    orderListeners.add(listenre);
  }
  
  public void removeOrderListener(OrderListener listenre) {
    orderListeners.remove(listenre);
  }
  
  public void setControlsVisible(boolean visible) {
    if (visible) {
      ticketActionPanel.setVisible(true);
      

      btnDelete.setEnabled(true);
    }
    else {
      ticketActionPanel.setVisible(false);
      

      btnDelete.setEnabled(false);
    }
  }
  







  private void updateSelectionView() {}
  







  public ModifierViewerTable getTicketViewerTable()
  {
    return modifierViewerTable;
  }
}
