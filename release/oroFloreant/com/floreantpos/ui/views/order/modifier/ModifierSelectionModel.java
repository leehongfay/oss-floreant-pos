package com.floreantpos.ui.views.order.modifier;

import com.floreantpos.model.MenuItem;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;




public class ModifierSelectionModel
{
  private Ticket ticket;
  private TicketItem ticketItem;
  private MenuItem menuItem;
  
  public ModifierSelectionModel(Ticket ticket, TicketItem ticketItem, MenuItem menuItem)
  {
    this.ticket = ticket;
    this.ticketItem = ticketItem;
    this.menuItem = menuItem;
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }
  
  public TicketItem getTicketItem() {
    return ticketItem;
  }
  
  public void setTicketItem(TicketItem ticketItem) {
    this.ticketItem = ticketItem;
  }
  
  public MenuItem getMenuItem() {
    return menuItem;
  }
  
  public void setMenuItem(MenuItem menuItem) {
    this.menuItem = menuItem;
  }
}
