package com.floreantpos.ui.views.order.modifier;

import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.TicketItemModifier;
import com.floreantpos.util.NumberUtil;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;



















public class ModifierViewerTableCellRenderer
  extends DefaultTableCellRenderer
{
  private boolean inTicketScreen = false;
  
  public ModifierViewerTableCellRenderer() {}
  
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) { Component rendererComponent = super.getTableCellRendererComponent(table, value, isSelected, false, row, column);
    
    ModifierViewerTableModel model = (ModifierViewerTableModel)table.getModel();
    Object object = model.get(row);
    
    if ((!inTicketScreen) || (isSelected)) {
      return rendererComponent;
    }
    
    rendererComponent.setBackground(Color.WHITE);
    
    if ((object instanceof TicketItemModifier)) {
      TicketItemModifier modifier = (TicketItemModifier)object;
      if (modifier.getModifierType().intValue() == 3) {
        rendererComponent.setForeground(Color.red);
      }
      else {
        rendererComponent.setForeground(Color.black);
      }
    }
    
    if ((object instanceof ITicketItem)) {
      ITicketItem ticketItem = (ITicketItem)object;
      if (ticketItem.isPrintedToKitchen().booleanValue()) {
        rendererComponent.setBackground(Color.YELLOW);
      }
    }
    
    return rendererComponent;
  }
  
  protected void setValue(Object value)
  {
    if (value == null) {
      setText("");
      return;
    }
    
    String text = value.toString();
    
    if (((value instanceof Double)) || ((value instanceof Float))) {
      text = NumberUtil.formatNumberAcceptNegative(Double.valueOf(((Number)value).doubleValue()));
      setHorizontalAlignment(4);
    }
    else if ((value instanceof Integer)) {
      setHorizontalAlignment(4);
    }
    else {
      setHorizontalAlignment(2);
    }
    

    setText(text);
  }
  
  public boolean isInTicketScreen() {
    return inTicketScreen;
  }
  
  public void setInTicketScreen(boolean inTicketScreen) {
    this.inTicketScreen = inTicketScreen;
  }
}
