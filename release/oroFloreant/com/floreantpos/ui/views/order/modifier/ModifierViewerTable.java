package com.floreantpos.ui.views.order.modifier;

import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemModifier;
import com.floreantpos.swing.PosUIManager;
import java.awt.Color;
import java.awt.Rectangle;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;





















public class ModifierViewerTable
  extends JTable
{
  private ModifierViewerTableModel model;
  private DefaultListSelectionModel selectionModel;
  private ModifierViewerTableCellRenderer cellRenderer;
  
  public ModifierViewerTable()
  {
    this(null);
  }
  

  public ModifierViewerTable(TicketItem ticketItem)
  {
    model = new ModifierViewerTableModel(ticketItem);
    setModel(model);
    
    selectionModel = new DefaultListSelectionModel();
    selectionModel.setSelectionMode(0);
    
    cellRenderer = new ModifierViewerTableCellRenderer();
    
    setGridColor(Color.LIGHT_GRAY);
    setSelectionModel(selectionModel);
    setAutoscrolls(true);
    setShowGrid(true);
    setBorder(null);
    
    setFocusable(false);
    
    setRowHeight(PosUIManager.getSize(60));
    resizeTableColumns();
  }
  
  private void resizeTableColumns() {
    setAutoResizeMode(4);
    

    setColumnWidth(1, PosUIManager.getSize(60));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  public TableCellRenderer getCellRenderer(int row, int column)
  {
    return cellRenderer;
  }
  
  public ModifierViewerTableCellRenderer getRenderer() {
    return cellRenderer;
  }
  






  private boolean isTicketNull()
  {
    return false;
  }
  
  public void scrollUp() {
    if (isTicketNull()) {
      return;
    }
    int selectedRow = getSelectedRow();
    int rowCount = model.getItemCount();
    
    if (selectedRow > rowCount - 1) {
      return;
    }
    
    selectedRow--;
    
    selectionModel.addSelectionInterval(selectedRow, selectedRow);
    Rectangle cellRect = getCellRect(selectedRow, 0, false);
    scrollRectToVisible(cellRect);
  }
  
  public void scrollDown() {
    if (isTicketNull()) {
      return;
    }
    int selectedRow = getSelectedRow();
    if (selectedRow >= model.getItemCount() - 1) {
      return;
    }
    
    selectedRow++;
    
    selectionModel.addSelectionInterval(selectedRow, selectedRow);
    Rectangle cellRect = getCellRect(selectedRow, 0, false);
    scrollRectToVisible(cellRect);
  }
  
  public void increaseItemAmount(TicketItem ticketItem) {
    double itemCount = ticketItem.getQuantity().doubleValue();
    ticketItem.setQuantity(Double.valueOf(++itemCount));
    repaint();
  }
  
  public boolean increaseItemAmount() {
    int selectedRow = getSelectedRow();
    if (selectedRow < 0) {
      return false;
    }
    if (selectedRow >= model.getItemCount()) {
      return false;
    }
    
    Object object = model.get(selectedRow);
    if ((object instanceof TicketItem)) {
      TicketItem ticketItem = (TicketItem)object;
      double itemCount = ticketItem.getQuantity().doubleValue();
      ticketItem.setQuantity(Double.valueOf(++itemCount));
      repaint();
      
      return true;
    }
    if ((object instanceof TicketItemModifier)) {
      TicketItemModifier modifier = (TicketItemModifier)object;
      double itemCount = modifier.getItemQuantity().doubleValue();
      





      modifier.setItemQuantity(Double.valueOf(++itemCount));
      repaint();
      
      return true;
    }
    return false;
  }
  
  public boolean decreaseItemAmount() {
    int selectedRow = getSelectedRow();
    if (selectedRow < 0) {
      return false;
    }
    if (selectedRow >= model.getItemCount()) {
      return false;
    }
    
    Object object = model.get(selectedRow);
    if ((object instanceof TicketItem)) {
      TicketItem ticketItem = (TicketItem)object;
      double itemCount = ticketItem.getQuantity().doubleValue();
      if (itemCount == 1.0D) {
        return false;
      }
      ticketItem.setQuantity(Double.valueOf(--itemCount));
      repaint();
      
      return true;
    }
    if ((object instanceof TicketItemModifier)) {
      TicketItemModifier modifier = (TicketItemModifier)object;
      double itemCount = modifier.getItemQuantity().doubleValue();
      if (itemCount == 1.0D) {
        return false;
      }
      modifier.setItemQuantity(Double.valueOf(--itemCount));
      repaint();
      
      return true;
    }
    return false;
  }
  
  public Object deleteSelectedItem() {
    int selectedRow = getSelectedRow();
    Object delete = model.delete(selectedRow);
    return delete;
  }
  
  public void delete(int index) {
    model.delete(index);
  }
  
  public Object get(int index) {
    return model.get(index);
  }
  
  public Object getSelected() {
    int index = getSelectedRow();
    
    return model.get(index);
  }
  
  public void removeModifier(TicketItem parent, TicketItemModifier modifier) {
    model.removeModifier(parent, modifier);
  }
  
  public void updateView()
  {
    model.update();
    try
    {
      int actualRowCount = model.getRowCount() - 1;
      selectionModel.addSelectionInterval(actualRowCount, actualRowCount);
      Rectangle cellRect = getCellRect(actualRowCount, 0, false);
      scrollRectToVisible(cellRect);
    }
    catch (Exception localException) {}
  }
  

  public int getActualRowCount()
  {
    return model.getActualRowCount();
  }
  
  public void selectLast() {
    int actualRowCount = getActualRowCount() - 1;
    selectionModel.addSelectionInterval(actualRowCount, actualRowCount);
    Rectangle cellRect = getCellRect(actualRowCount, 0, false);
    scrollRectToVisible(cellRect);
  }
  
  public void selectRow(int index) {
    if ((index < 0) || (index >= getActualRowCount())) {
      index = 0;
    }
    selectionModel.addSelectionInterval(index, index);
    Rectangle cellRect = getCellRect(index, 0, false);
    scrollRectToVisible(cellRect);
  }
  
  public boolean isAddOnMode() {
    return false;
  }
}
