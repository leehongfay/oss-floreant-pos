package com.floreantpos.ui.views.order.modifier;

import com.floreantpos.Messages;
import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemModifier;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;



















public class ModifierViewerTableModel
  extends AbstractTableModel
{
  protected TicketItem ticketItem;
  private final List<ITicketItem> tableRows = new ArrayList();
  private boolean priceIncludesTax = false;
  
  protected String[] columnNames = { Messages.getString("TicketViewerTableModel.0"), Messages.getString("TicketViewerTableModel.3") };
  private boolean forReciptPrint;
  private boolean printCookingInstructions;
  
  public ModifierViewerTableModel(TicketItem ticketItem)
  {
    setTicketItem(ticketItem);
  }
  
  public int getItemCount() {
    return tableRows.size();
  }
  
  public int getRowCount() {
    int size = tableRows.size();
    
    return size;
  }
  
  public int getActualRowCount() {
    return tableRows.size();
  }
  
  public int getColumnCount() {
    return columnNames.length;
  }
  
  public String getColumnName(int column)
  {
    return columnNames[column];
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    TicketItemModifier ticketItem = (TicketItemModifier)tableRows.get(rowIndex);
    
    if (ticketItem == null) {
      return null;
    }
    
    switch (columnIndex) {
    case 0: 
      return ticketItem.getNameDisplay();
    






    case 1: 
      return Double.valueOf(ticketItem.getUnitPrice().doubleValue() * ticketItem.getItemQuantity().doubleValue());
    }
    
    return null;
  }
  
  private void calculateRows() {
    tableRows.clear();
    calculateRowsForModifiers();
  }
  
  private void calculateRowsForModifiers() {
    List<TicketItemModifier> ticketItemModifiers = ticketItem.getTicketItemModifiers();
    if (ticketItemModifiers != null) {
      for (TicketItemModifier ticketItemModifier : ticketItemModifiers) {
        tableRows.add(ticketItemModifier);
      }
    }
  }
  









  public void removeModifier(TicketItem parent, TicketItemModifier modifierToDelete) {}
  








  public Object delete(int index)
  {
    if ((index < 0) || (index >= tableRows.size())) {
      return null;
    }
    TicketItemModifier ticketItemModifier = (TicketItemModifier)tableRows.remove(index);
    ticketItemModifier.getTicketItem().removeTicketItemModifier(ticketItemModifier);
    fireTableRowsDeleted(index, index);
    return ticketItemModifier;
  }
  
  public Object get(int index) {
    if ((index < 0) || (index >= tableRows.size())) {
      return null;
    }
    return tableRows.get(index);
  }
  
  public TicketItem getTicketItem() {
    return ticketItem;
  }
  
  public void setTicketItem(TicketItem ticketItem) {
    this.ticketItem = ticketItem;
    
    update();
  }
  
  public void update() {
    calculateRows();
    fireTableDataChanged();
  }
  
  public boolean isForReciptPrint() {
    return forReciptPrint;
  }
  
  public void setForReciptPrint(boolean forReciptPrint) {
    this.forReciptPrint = forReciptPrint;
  }
  
  public boolean isPrintCookingInstructions() {
    return printCookingInstructions;
  }
  
  public void setPrintCookingInstructions(boolean printCookingInstructions) {
    this.printCookingInstructions = printCookingInstructions;
  }
  
  public boolean isPriceIncludesTax() {
    return priceIncludesTax;
  }
  
  public void setPriceIncludesTax(boolean priceIncludesTax) {
    this.priceIncludesTax = priceIncludesTax;
  }
}
