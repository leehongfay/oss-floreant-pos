package com.floreantpos.ui.views.order.modifier;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.model.MenuItemModifierPage;
import com.floreantpos.model.MenuItemModifierPageItem;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.Multiplier;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemModifier;
import com.floreantpos.model.dao.MenuItemModifierPageDAO;
import com.floreantpos.model.dao.MenuModifierDAO;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PaginatedListModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.SelectionView;
import com.floreantpos.util.POSUtil;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import net.miginfocom.swing.MigLayout;





























public class ModifierView
  extends SelectionView
{
  private Vector<ModifierSelectionListener> listenerList = new Vector();
  
  private ModifierSelectionModel modifierSelectionModel;
  
  private MenuItemModifierSpec modifierGroup;
  private PosButton btnClear = new PosButton(POSConstants.CLEAR);
  private PosButton btnDone = new PosButton(POSConstants.GROUP.toUpperCase() + " DONE");
  private PosButton btnCancel = new PosButton(POSConstants.CANCEL);
  
  private Multiplier selectedMultiplier;
  private MultiplierButton defaultMultiplierButton;
  
  public ModifierView(ModifierSelectionModel modifierSelectionModel)
  {
    super(POSConstants.MODIFIERS, new FlowLayout(), PosUIManager.getSize(120), PosUIManager.getSize(80));
    this.modifierSelectionModel = modifierSelectionModel;
    dataModel = new PaginatedListModel();
    actionButtonPanel.add(btnPrev, "grow,split 2,span");
    actionButtonPanel.add(btnNext, "grow");
    addMultiplierButtons();
    addActionButtons();
  }
  
  private void addMultiplierButtons() {
    JPanel multiplierPanel = new JPanel(new MigLayout("ins 0,fillx,center"));
    List<Multiplier> multiplierList = DataProvider.get().getMultiplierList();
    ButtonGroup group = new ButtonGroup();
    if (multiplierList != null) {
      for (Multiplier multiplier : multiplierList) {
        MultiplierButton btnMultiplier = new MultiplierButton(multiplier);
        if (multiplier.isDefaultMultiplier().booleanValue()) {
          selectedMultiplier = multiplier;
          defaultMultiplierButton = btnMultiplier;
          btnMultiplier.setSelected(true);
        }
        multiplierPanel.add(btnMultiplier, "grow");
        group.add(btnMultiplier);
      }
    }
    actionButtonPanel.add(multiplierPanel, "newline,span");
  }
  
  private void addActionButtons() {
    actionButtonPanel.add(btnClear, "newline,grow");
    actionButtonPanel.add(btnDone, "grow");
    actionButtonPanel.add(btnCancel, "grow");
    
    btnDone.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        for (ModifierSelectionListener listener : listenerList) {
          listener.finishModifierSelection();
        }
      }
    });
    btnClear.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        for (ModifierSelectionListener listener : listenerList) {
          listener.clearModifiers(modifierGroup);
        }
        
      }
    });
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ModifierView.this.closeDialog(true);
      }
    });
  }
  
  private void closeDialog(boolean canceled) {
    Window windowAncestor = SwingUtilities.getWindowAncestor(this);
    if ((windowAncestor instanceof POSDialog)) {
      ((POSDialog)windowAncestor).setCanceled(canceled);
      windowAncestor.dispose();
    }
  }
  
  public void setModifierGroup(MenuItemModifierSpec modifierGroup) {
    this.modifierGroup = modifierGroup;
    

    if (modifierGroup == null) {
      return;
    }
    
    renderTitle();
    







    try
    {
      dataModel.setPageSize(1);
      dataModel.setCurrentRowIndex(0);
      dataModel.setNumRows(MenuItemModifierPageDAO.getInstance().getRowCount(modifierGroup.getId()));
      MenuItemModifierPageDAO.getInstance().loadItems(modifierGroup, dataModel);
      setDataModel(dataModel);
    } catch (PosException e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  public void setDataModel(PaginatedListModel items)
  {
    populateItems();
    super.setDataModel(items);
    updateButton();
  }
  
  protected void renderItems()
  {
    updateView();
  }
  
  protected void populateItems() {
    reset();
    if (dataModel.getSize() == 0) {
      return;
    }
    MenuItemModifierPage modifierPage = (MenuItemModifierPage)dataModel.getElementAt(0);
    setButtonSize(new Dimension(modifierPage.getButtonWidth().intValue(), modifierPage.getButtonHeight().intValue()));
    Integer cols = modifierPage.getCols();
    MigLayout migLayout = new MigLayout("center,wrap " + cols);
    if (modifierPage.isFlixibleButtonSize().booleanValue()) {
      migLayout.setLayoutConstraints("fill");
      migLayout.setColumnConstraints("fill,grow");
      migLayout.setRowConstraints("fill,grow");
    }
    selectionButtonsPanel.setLayout(migLayout);
    for (int row = 0; row < modifierPage.getRows().intValue(); row++) {
      for (int col = 0; col < modifierPage.getCols().intValue(); col++) {
        String constraint = String.format("cell %s %s", new Object[] { Integer.valueOf(col), Integer.valueOf(row) });
        if (!modifierPage.isFlixibleButtonSize().booleanValue()) {
          constraint = constraint + String.format(", w %s!, h %s!", new Object[] { modifierPage.getButtonWidth(), modifierPage.getButtonHeight() });
        }
        MenuItemModifierPageItem itemForCell = modifierPage.getItemForCell(col, row);
        if (itemForCell == null) {
          selectionButtonsPanel.add(new JLabel(), constraint);
        }
        else {
          AbstractButton itemButton = createItemButton(itemForCell);
          if (itemButton != null) {
            selectionButtonsPanel.add(itemButton, constraint);
          }
        }
      }
    }
    revalidate();
    repaint();
    btnNext.setVisible(dataModel.hasNext());
    btnPrev.setVisible(dataModel.hasPrevious());
  }
  
  private void renderTitle() {
    String displayName = modifierGroup.getName();
    String instruction = modifierGroup.getInstruction();
    int minQuantity = modifierGroup.getMinQuantity().intValue();
    int maxQuantity = modifierGroup.getMaxQuantity().intValue();
    setTitle(displayName + Messages.getString("ModifierView.2") + minQuantity + Messages.getString("ModifierView.3") + maxQuantity + (instruction == null ? "" : new StringBuilder().append(" ").append(instruction).toString()));
  }
  




  protected AbstractButton createItemButton(Object item)
  {
    MenuItemModifierPageItem menuItemModifierPageItem = (MenuItemModifierPageItem)item;
    ModifierButton modifierButton = new ModifierButton(menuItemModifierPageItem);
    


    return modifierButton;
  }
  
  public void addModifierSelectionListener(ModifierSelectionListener listener) {
    listenerList.add(listener);
  }
  
  public void removeModifierSelectionListener(ModifierSelectionListener listener) {
    listenerList.remove(listener);
  }
  
  public void updateView() {
    Component[] components = selectionButtonsPanel.getComponents();
    if ((components == null) || (components.length == 0)) {
      return;
    }
    TicketItem ticketItem = modifierSelectionModel.getTicketItem();
    
    Map<String, TicketItemModifier> ticketItemModifierMap = new HashMap();
    
    if ((ticketItem.getTicketItemModifiers() != null) && (ticketItem.getTicketItemModifiers().size() > 0)) {
      for (localObject = ticketItem.getTicketItemModifiers().iterator(); ((Iterator)localObject).hasNext();) { ticketItemModifier = (TicketItemModifier)((Iterator)localObject).next();
        ticketItemModifierMap.put(ticketItemModifier.getItemId(), ticketItemModifier);
      }
    }
    
    Object localObject = components;TicketItemModifier ticketItemModifier = localObject.length; for (TicketItemModifier localTicketItemModifier1 = 0; localTicketItemModifier1 < ticketItemModifier; localTicketItemModifier1++) { Component component = localObject[localTicketItemModifier1];
      if (!(component instanceof JLabel))
      {
        ModifierButton modifierButton = (ModifierButton)component;
        MenuItemModifierPageItem modifierPageItem = menuItemModifierPageItem;
        
        TicketItemModifier ticketItemModifier = (TicketItemModifier)ticketItemModifierMap.get(modifierPageItem.getMenuModifierId());
        if (ticketItemModifier != null) {
          modifierButton.setText("<html><center>" + modifierPageItem.getMenuModifierName() + "<br/><span style='color:green;" + "'>(" + ticketItemModifier
            .getItemQuantity() + ")</span></center></html>");
        }
        else {
          modifierButton.setText("<html><center>" + modifierPageItem.getMenuModifierName() + "</center></html>");
        }
        if (ModifierSelectionDialog.isRequiredModifiersAdded(ticketItem, modifierGroup)) {
          btnDone.setBackground(Color.green);
        }
        else
          btnDone.setBackground(UIManager.getColor("Control"));
      }
    }
  }
  
  private class ModifierButton extends PosButton implements ActionListener {
    private int BUTTON_SIZE = 100;
    private MenuItemModifierPageItem menuItemModifierPageItem;
    
    public ModifierButton(MenuItemModifierPageItem menuItemModifierPageItem) {
      this.menuItemModifierPageItem = menuItemModifierPageItem;
      setFocusable(true);
      setVerticalTextPosition(3);
      setHorizontalTextPosition(0);
      BUTTON_SIZE = PosUIManager.getSize(100);
      
      setText("<html><center>" + menuItemModifierPageItem.getMenuModifierName() + "</center></html>");
      int w;
      if (menuItemModifierPageItem.getImage() != null) {
        w = BUTTON_SIZE - PosUIManager.getSize(0);
        int h = BUTTON_SIZE - PosUIManager.getSize(0);
        
        if (menuItemModifierPageItem.isShowImageOnly().booleanValue()) {
          setIcon(new ImageIcon(menuItemModifierPageItem.getImage().getImage().getScaledInstance(w, h, 0)));
        }
        else
        {
          w = PosUIManager.getSize(80);
          h = PosUIManager.getSize(40);
          
          setIcon(new ImageIcon(menuItemModifierPageItem.getImage().getImage().getScaledInstance(w, h, 0)));
          setText("<html><body><center>" + menuItemModifierPageItem.getMenuModifierName() + "</center></body></html>");
        }
      }
      else
      {
        setText("<html><body><center>" + menuItemModifierPageItem.getMenuModifierName() + "</center></body></html>");
      }
      
      if (menuItemModifierPageItem.getTextColor() != null) {
        setForeground(menuItemModifierPageItem.getTextColor());
      }
      
      if (menuItemModifierPageItem.getButtonColor() != null) {
        setBackground(menuItemModifierPageItem.getButtonColor());
      }
      
      setPreferredSize(new Dimension(BUTTON_SIZE, BUTTON_SIZE));
      
      addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e) {
      try {
        for (ModifierSelectionListener listener : listenerList) {
          String menuModifierId = menuItemModifierPageItem.getMenuModifierId();
          MenuModifier modifier = MenuModifierDAO.getInstance().get(menuModifierId);
          MenuModifierDAO.getInstance().initialize(modifier);
          modifier.setMenuItemModifierGroup(modifierGroup);
          listener.modifierSelected(modifier, selectedMultiplier);
        }
        if (defaultMultiplierButton != null) {
          defaultMultiplierButton.setSelected(true);
          selectedMultiplier = defaultMultiplierButton.getMultiplier();
        }
      } catch (Exception ex) {
        POSMessageDialog.showError(POSUtil.getFocusedWindow(), ex.getMessage(), ex);
      }
    }
  }
  
  private class MultiplierButton extends POSToggleButton implements ActionListener {
    private Multiplier multiplier;
    
    public MultiplierButton(Multiplier multiplier) {
      this.multiplier = multiplier;
      setText(multiplier.getId());
      Integer buttonColor = multiplier.getButtonColor();
      if (buttonColor != null) {
        setBackground(new Color(buttonColor.intValue()));
      }
      Integer textColor = multiplier.getTextColor();
      if (textColor != null) {
        setForeground(new Color(textColor.intValue()));
      }
      setBorder(BorderFactory.createLineBorder(Color.GRAY));
      addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e)
    {
      selectedMultiplier = multiplier;
    }
    
    public Multiplier getMultiplier() {
      return multiplier;
    }
    
    protected void paintComponent(Graphics g)
    {
      super.paintComponent(g);
      
      if (isSelected()) {
        setBorder(BorderFactory.createLineBorder(new Color(255, 128, 0), 1));
      } else {
        setBorder(BorderFactory.createLineBorder(Color.GRAY));
      }
    }
  }
  
  protected void scrollDown()
  {
    dataModel.setCurrentRowIndex(dataModel.getNextRowIndex());
    MenuItemModifierPageDAO.getInstance().loadItems(modifierGroup, dataModel);
    setDataModel(dataModel);
  }
  
  protected void scrollUp()
  {
    dataModel.setCurrentRowIndex(dataModel.getPreviousRowIndex());
    MenuItemModifierPageDAO.getInstance().loadItems(modifierGroup, dataModel);
    setDataModel(dataModel);
  }
}
