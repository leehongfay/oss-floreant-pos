package com.floreantpos.ui.views.order.modifier;

import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.Multiplier;
import com.floreantpos.model.TicketItemModifier;

public abstract interface ModifierSelectionListener
{
  public abstract void modifierSelected(MenuModifier paramMenuModifier, Multiplier paramMultiplier);
  
  public abstract void modifierRemoved(TicketItemModifier paramTicketItemModifier);
  
  public abstract void clearModifiers(MenuItemModifierSpec paramMenuItemModifierSpec);
  
  public abstract void modifierGroupSelectionDone(MenuItemModifierSpec paramMenuItemModifierSpec);
  
  public abstract void finishModifierSelection();
}
