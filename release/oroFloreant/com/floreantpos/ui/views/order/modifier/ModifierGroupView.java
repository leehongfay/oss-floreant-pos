package com.floreantpos.ui.views.order.modifier;

import com.floreantpos.POSConstants;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.ScrollableFlowPanel;
import com.jidesoft.swing.SimpleScrollPane;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.List;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JViewport;
import javax.swing.border.TitledBorder;

























public class ModifierGroupView
  extends JPanel
  implements ComponentListener
{
  private Vector<ModifierGroupSelectionListener> listenerList = new Vector();
  
  private static Dimension buttonSize = PosUIManager.getSize(100, 80);
  
  private ModifierSelectionModel modifierSelectionModel;
  
  private ButtonGroup modifierGroupButtonGroup;
  
  private SimpleScrollPane simpleScrollPane;
  
  private ScrollableFlowPanel contentPanel;
  public static final String VIEW_NAME = "MODIFIER_GROUP_VIEW";
  
  public ModifierGroupView(ModifierSelectionModel modifierSelectionModel)
  {
    this.modifierSelectionModel = modifierSelectionModel;
    
    setLayout(new BorderLayout());
    TitledBorder border = new TitledBorder(POSConstants.GROUPS);
    border.setTitleJustification(2);
    setBorder(border);
    
    contentPanel = new ScrollableFlowPanel();
    simpleScrollPane = new SimpleScrollPane(contentPanel);
    simpleScrollPane.setBorder(null);
    simpleScrollPane.setAutoscrolls(false);
    simpleScrollPane.setScrollOnRollover(false);
    simpleScrollPane.setVerticalUnitIncrement(TerminalConfig.getTouchScreenButtonHeight());
    simpleScrollPane.getScrollUpButton().setPreferredSize(PosUIManager.getSize(100, TerminalConfig.getTouchScreenButtonHeight()));
    simpleScrollPane.getScrollDownButton().setPreferredSize(PosUIManager.getSize(100, TerminalConfig.getTouchScreenButtonHeight()));
    
    add(simpleScrollPane);
    
    modifierGroupButtonGroup = new ButtonGroup();
    setPreferredSize(PosUIManager.getSize(120, 120));
    
    init();
    
    addComponentListener(this);
  }
  
  public void reset() {
    modifierGroupButtonGroup = new ButtonGroup();
  }
  
  private void init() {
    List<MenuItemModifierSpec> modifierSpecs = modifierSelectionModel.getMenuItem().getMenuItemModiferSpecs();
    for (MenuItemModifierSpec menuItemModifierSpec : modifierSpecs) {
      if (menuItemModifierSpec.isEnable().booleanValue())
        contentPanel.add(createItemButton(menuItemModifierSpec));
    }
  }
  
  protected AbstractButton createItemButton(Object item) {
    MenuItemModifierSpec menuModifierGroup = (MenuItemModifierSpec)item;
    
    ModifierGroupButton button = new ModifierGroupButton(menuModifierGroup);
    button.setPreferredSize(buttonSize);
    modifierGroupButtonGroup.add(button);
    
    return button;
  }
  
  public void addModifierGroupSelectionListener(ModifierGroupSelectionListener listener) {
    listenerList.add(listener);
  }
  
  public void removeModifierGroupSelectionListener(ModifierGroupSelectionListener listener) {
    listenerList.remove(listener);
  }
  
  private void fireModifierGroupSelected(MenuItemModifierSpec foodModifierGroup) {
    for (ModifierGroupSelectionListener listener : listenerList) {
      listener.modifierGroupSelected(foodModifierGroup);
    }
  }
  
  public void setSelectedModifierGroup(MenuItemModifierSpec modifierGroup) {
    Component[] components = contentPanel.getContentPane().getComponents();
    if ((components != null) && (components.length > 0)) {
      for (Component component : components) {
        ModifierGroupButton button = (ModifierGroupButton)component;
        if (menuModifierGroup.getId() == modifierGroup.getId()) {
          button.setSelected(true);
          Rectangle bounds = button.getBounds();
          height *= 2;
          simpleScrollPane.scrollRectToVisible(bounds);
          fireModifierGroupSelected(menuModifierGroup);
          break;
        }
      }
    }
  }
  
  public void selectFirst() {
    Component[] components = contentPanel.getContentPane().getComponents();
    if ((components != null) && (components.length > 0)) {
      ModifierGroupButton button = (ModifierGroupButton)components[0];
      button.setSelected(true);
      fireModifierGroupSelected(menuModifierGroup);
    }
  }
  
  public void selectNextGroup() {
    if (hasNextMandatoryGroup()) {
      ModifierGroupButton button = getNextMandatoryGroup();
      button.setSelected(true);
      fireModifierGroupSelected(menuModifierGroup);
    }
  }
  
  public boolean hasNextMandatoryGroup() {
    return getNextMandatoryGroup() != null;
  }
  
  private ModifierGroupButton getNextMandatoryGroup() {
    Component[] components = contentPanel.getContentPane().getComponents();
    if ((components != null) && (components.length > 0)) {
      for (int i = 0; i < components.length; i++) {
        ModifierGroupButton button = (ModifierGroupButton)components[i];
        if ((button.isSelected()) && (i < components.length - 1)) {
          ModifierGroupButton modifierGroupButton = (ModifierGroupButton)components[(i + 1)];
          

          return modifierGroupButton;
        }
      }
    }
    

    return null;
  }
  
  private class ModifierGroupButton extends POSToggleButton implements ActionListener {
    MenuItemModifierSpec menuModifierGroup;
    
    ModifierGroupButton(MenuItemModifierSpec menuModifierGroup) {
      this.menuModifierGroup = menuModifierGroup;
      updateButtonText();
      addActionListener(this);
    }
    
    private void updateButtonText() {
      String string = "";
      





      string = "<html><body><center>" + menuModifierGroup.getName() + "<br/>" + "(" + menuModifierGroup.getMinQuantity() + "*)</center></body></html>";
      

      setText(string);
    }
    
    public void actionPerformed(ActionEvent e)
    {
      ModifierGroupView.this.fireModifierGroupSelected(menuModifierGroup);
    }
  }
  
  public void componentResized(ComponentEvent e)
  {
    int verticalUnitIncrement = simpleScrollPane.getViewport().getVisibleRect().height - TerminalConfig.getTouchScreenButtonHeight();
    if (verticalUnitIncrement < TerminalConfig.getTouchScreenButtonHeight()) {
      verticalUnitIncrement = TerminalConfig.getTouchScreenButtonHeight();
    }
    simpleScrollPane.setVerticalUnitIncrement(verticalUnitIncrement);
  }
  
  public void componentMoved(ComponentEvent e) {}
  
  public void componentShown(ComponentEvent e) {}
  
  public void componentHidden(ComponentEvent e) {}
}
