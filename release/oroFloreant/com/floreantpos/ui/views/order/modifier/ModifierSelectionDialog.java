package com.floreantpos.ui.views.order.modifier;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.model.DefaultMenuModifier;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.Multiplier;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemModifier;
import com.floreantpos.model.dao.MenuItemModifierSpecDAO;
import com.floreantpos.model.dao.MultiplierDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;

























public class ModifierSelectionDialog
  extends POSDialog
  implements ModifierGroupSelectionListener, ModifierSelectionListener
{
  private ModifierSelectionModel modifierSelectionModel;
  private ModifierGroupView modifierGroupView;
  private ModifierView modifierView;
  private TicketItemModifierTableView ticketItemModifierView;
  private JPanel westPanel = new JPanel(new BorderLayout(5, 5));
  
  private TransparentPanel buttonPanel;
  private PosButton btnSave;
  private PosButton btnCancel;
  
  public ModifierSelectionDialog(ModifierSelectionModel modifierSelectionModel)
  {
    super(Application.getPosWindow(), true);
    this.modifierSelectionModel = modifierSelectionModel;
    
    initComponents();
    initData();
  }
  
  private void initData() {
    MenuItem menuItem = modifierSelectionModel.getMenuItem();
    Multiplier defaultMultiplier;
    Iterator localIterator1; if (modifierSelectionModel.getTicketItem().getTicketItemModifiers() == null) {
      List<MenuItemModifierSpec> groupList = menuItem.getMenuItemModiferSpecs();
      defaultMultiplier = MultiplierDAO.getInstance().getDefaultMutltiplier();
      for (localIterator1 = groupList.iterator(); localIterator1.hasNext();) { group = (MenuItemModifierSpec)localIterator1.next();
        
        if (group.isEnable().booleanValue()) {
          List<DefaultMenuModifier> defaultModifierList = group.getDefaultModifierList();
          if (defaultModifierList != null)
            for (DefaultMenuModifier defaultModifier : defaultModifierList) {
              MenuModifier modifier = defaultModifier.getModifier();
              Multiplier multiplier = defaultModifier.getMultiplier();
              if (multiplier == null)
                multiplier = defaultMultiplier;
              modifier.setMenuItemModifierGroup(group);
              addToTicket(modifier, multiplier, defaultModifier.getQuantity());
            }
        }
      }
    }
    MenuItemModifierSpec group;
  }
  
  private void initComponents() {
    setTitle(Messages.getString("ModifierSelectionDialog.0"));
    
    setLayout(new BorderLayout(PosUIManager.getSize(5), PosUIManager.getSize(5)));
    
    modifierGroupView = new ModifierGroupView(modifierSelectionModel);
    modifierView = new ModifierView(modifierSelectionModel);
    ticketItemModifierView = new TicketItemModifierTableView(modifierSelectionModel);
    buttonPanel = new TransparentPanel();
    buttonPanel.setLayout(new MigLayout("fill, ins 4", "fill", ""));
    
    westPanel.add(ticketItemModifierView);
    add(modifierGroupView, "East");
    add(modifierView);
    add(westPanel, "West");
    
    createButtonPanel();
    
    setBounds(Application.getPosWindow().getBounds());
    
    ticketItemModifierView.addModifierSelectionListener(this);
    modifierGroupView.addModifierGroupSelectionListener(this);
    modifierView.addModifierSelectionListener(this);
    
    modifierGroupView.selectFirst();
  }
  
  public void createButtonPanel() {
    btnSave = new PosButton(Messages.getString("ModifierSelectionDialog.4"));
    btnSave.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        ModifierSelectionDialog.this.doFinishModifierSelection();
      }
      
    });
    btnCancel = new PosButton(POSConstants.CANCEL.toUpperCase());
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        setCanceled(true);
        dispose();
      }
      
    });
    buttonPanel.add(btnCancel);
    buttonPanel.add(btnSave);
  }
  

  public ModifierGroupView getModifierGroupView()
  {
    return modifierGroupView;
  }
  
  public void setModifierGroupView(ModifierGroupView modifierGroupView) {
    this.modifierGroupView = modifierGroupView;
  }
  
  public ModifierView getModifierView() {
    return modifierView;
  }
  
  public void setModifierView(ModifierView modifierView) {
    this.modifierView = modifierView;
  }
  
  private void doFinishModifierSelection() {
    List<MenuItemModifierSpec> menuItemModiferGroups = modifierSelectionModel.getMenuItem().getMenuItemModiferSpecs();
    if (menuItemModiferGroups == null) {
      dispose();
      return;
    }
    
    boolean finishGroupSelection = true;
    for (MenuItemModifierSpec menuItemModifierGroup : menuItemModiferGroups) {
      if ((menuItemModifierGroup.isEnable().booleanValue()) && 
        (!isRequiredModifiersAdded(modifierSelectionModel.getTicketItem(), menuItemModifierGroup))) {
        showModifierSelectionMessage(menuItemModifierGroup);
        modifierGroupView.setSelectedModifierGroup(menuItemModifierGroup);
        finishGroupSelection = false;
      }
    }
  }
  







  public void modifierGroupSelected(MenuItemModifierSpec menuModifierGroup)
  {
    modifierView.setModifierGroup(menuModifierGroup);
  }
  














  public void modifierSelected(MenuModifier modifier, Multiplier multiplier)
  {
    addToTicket(modifier, multiplier);
  }
  


  private void jumpToNextGroup(MenuModifier modifier)
  {
    TicketItem ticketItem = modifierSelectionModel.getTicketItem();
    MenuItemModifierSpec menuItemModifierGroup = modifier.getMenuItemModifierGroup();
    
    int freeModifiers = ticketItem.countModifierFromGroup(menuItemModifierGroup);
    int minQuantity = menuItemModifierGroup.getMinQuantity().intValue();
    int maxQuantity = menuItemModifierGroup.getMaxQuantity().intValue();
    
    if (maxQuantity < minQuantity) {
      maxQuantity = minQuantity;
    }
    
    if ((menuItemModifierGroup.isJumpGroup().booleanValue()) && 
      (freeModifiers == maxQuantity)) {
      if (modifierGroupView.hasNextMandatoryGroup()) {
        modifierGroupView.selectNextGroup();
        updateView();
        
        return;
      }
      
      doFinishModifierSelection();
    }
  }
  

  private void addToTicket(MenuModifier modifier, Multiplier multiplier)
  {
    addToTicket(modifier, multiplier, Double.valueOf(1.0D));
  }
  

  private void addToTicket(MenuModifier modifier, Multiplier multiplier, Double quantity)
  {
    TicketItem ticketItem = modifierSelectionModel.getTicketItem();
    MenuItemModifierSpec menuItemModifierGroup = modifier.getMenuItemModifierGroup();
    
    int numOfModifiers = ticketItem.countModifierFromGroup(menuItemModifierGroup);
    int minQuantity = menuItemModifierGroup.getMinQuantity().intValue();
    int maxQuantity = menuItemModifierGroup.getMaxQuantity().intValue();
    
    if (maxQuantity < minQuantity) {
      maxQuantity = minQuantity;
    }
    
    if (numOfModifiers >= maxQuantity) {
      POSMessageDialog.showError("You have added maximum number of allowed modifiers from group " + modifier.getMenuItemModifierGroup().getDisplayName());
      






      return;
    }
    
    TicketItemModifier ticketItemModifier = ticketItem.findTicketItemModifierFor(modifier, multiplier);
    if (ticketItemModifier == null) {
      OrderType type = ticketItem.getTicket().getOrderType();
      TicketItemModifier ticketitemModifier = ticketItem.addTicketItemModifier(modifier, 1, type, multiplier);
      ticketitemModifier.setItemQuantity(quantity);
    }
    else {
      ticketItemModifier.setItemCount(Integer.valueOf(ticketItemModifier.getItemCount().intValue() + 1));
    }
    updateView();
    if (numOfModifiers + 1 == maxQuantity) {
      modifierGroupSelectionDone(modifier.getMenuItemModifierGroup());
    }
  }
  
  private void updateView()
  {
    modifierSelectionModel.getTicketItem().calculatePrice();
    modifierView.updateView();
    ticketItemModifierView.updateView();
  }
  
  public void clearModifiers(MenuItemModifierSpec modifierGroup)
  {
    TicketItem ticketItem = modifierSelectionModel.getTicketItem();
    List<TicketItemModifier> ticketItemModifiers = ticketItem.getTicketItemModifiers();
    Iterator iterator; if (ticketItemModifiers != null) {
      for (iterator = ticketItemModifiers.iterator(); iterator.hasNext();) {
        TicketItemModifier ticketItemModifier = (TicketItemModifier)iterator.next();
        if (!ticketItemModifier.isPrintedToKitchen().booleanValue()) {
          iterator.remove();
        }
      }
    }
    updateView();
  }
  
  public void modifierGroupSelectionDone(MenuItemModifierSpec modifierGroup)
  {
    if (!isRequiredModifiersAdded(modifierSelectionModel.getTicketItem(), modifierGroup)) {
      showModifierSelectionMessage(modifierGroup);
      modifierGroupView.setSelectedModifierGroup(modifierGroup);
      return;
    }
    
    if ((modifierGroup.isJumpGroup().booleanValue()) && (modifierGroupView.hasNextMandatoryGroup())) {
      modifierGroupView.selectNextGroup();
    }
  }
  






  public void finishModifierSelection()
  {
    TicketItem ticketItem = modifierSelectionModel.getTicketItem();
    List<MenuItemModifierSpec> menuItemModiferGroups = modifierSelectionModel.getMenuItem().getMenuItemModiferSpecs();
    if (menuItemModiferGroups == null) {
      setCanceled(false);
      dispose(); return;
    }
    Iterator iterator;
    if (!menuItemModiferGroups.isEmpty())
    {
      for (iterator = menuItemModiferGroups.iterator(); iterator.hasNext();) {
        MenuItemModifierSpec menuItemModifierGroup = (MenuItemModifierSpec)iterator.next();
        if (!ticketItem.requiredModifiersAdded(menuItemModifierGroup)) {
          modifierGroupSelected(menuItemModifierGroup);
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select minimum quantity of each group!");
          return;
        }
      }
    }
    setCanceled(false);
    dispose();
  }
  
  public ModifierSelectionModel getModifierSelectionModel() {
    return modifierSelectionModel;
  }
  
  public void setModifierSelectionModel(ModifierSelectionModel modifierSelectionModel) {
    this.modifierSelectionModel = modifierSelectionModel;
  }
  


















  public static boolean isRequiredModifiersAdded(TicketItem ticketItem, MenuItemModifierSpec menuItemModifierGroup)
  {
    return ticketItem.requiredModifiersAdded(menuItemModifierGroup);
  }
  
  private void showModifierSelectionMessage(MenuItemModifierSpec menuItemModifierGroup) {
    String displayName = menuItemModifierGroup.getName();
    int minQuantity = menuItemModifierGroup.getMinQuantity().intValue();
    
    POSMessageDialog.showError(Messages.getString("ModifierSelectionDialog.5") + minQuantity + Messages.getString("ModifierSelectionDialog.6") + displayName);
  }
  
  public void modifierRemoved(TicketItemModifier modifier)
  {
    MenuItemModifierSpec group = MenuItemModifierSpecDAO.getInstance().get(modifier.getGroupId());
    modifierView.setModifierGroup(group);
  }
}
