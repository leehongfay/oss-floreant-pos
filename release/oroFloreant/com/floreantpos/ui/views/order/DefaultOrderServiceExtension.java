package com.floreantpos.ui.views.order;

import com.floreantpos.Messages;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.customer.CustomerSelector;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.model.Customer;
import com.floreantpos.model.Department;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Outlet;
import com.floreantpos.model.SalesArea;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.TableStatus;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.IView;
import com.floreantpos.util.DatabaseConnectionException;
import com.floreantpos.util.DrawerUtil;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.PosGuiUtil;
import com.floreantpos.util.ShiftUtil;
import com.floreantpos.util.TicketAlreadyExistsException;
import java.awt.Component;
import java.util.Calendar;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
















public class DefaultOrderServiceExtension
  extends OrderServiceExtension
{
  public DefaultOrderServiceExtension() {}
  
  public boolean requireLicense()
  {
    return false;
  }
  
  public String getProductName()
  {
    return Messages.getString("DefaultOrderServiceExtension.0");
  }
  
  public String getDescription()
  {
    return Messages.getString("DefaultOrderServiceExtension.1");
  }
  
  public void createNewTicket(OrderType ticketType, List<ShopTable> selectedTables, Customer customer) throws TicketAlreadyExistsException
  {
    createNewTicket(ticketType, selectedTables, customer, 0);
  }
  


  public void setCustomerToTicket(String ticketId) {}
  


  public void setDeliveryDate(String ticketId) {}
  

  public void assignDriver(String ticketId) {}
  

  public boolean finishOrder(String ticketId)
  {
    Ticket ticket = TicketDAO.getInstance().get(ticketId);
    





    int due = (int)POSUtil.getDouble(ticket.getDueAmount());
    if (due != 0) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("DefaultOrderServiceExtension.2"));
      return false;
    }
    
    int option = JOptionPane.showOptionDialog(Application.getPosWindow(), Messages.getString("DefaultOrderServiceExtension.3") + ticket.getId() + Messages.getString("DefaultOrderServiceExtension.4"), Messages.getString("DefaultOrderServiceExtension.5"), 2, 1, null, null, null);
    

    if (option != 0) {
      return false;
    }
    
    OrderController.closeOrder(ticket);
    
    return true;
  }
  


  public void createCustomerMenu(JMenu menu) {}
  


  public void initBackoffice(BackOfficeWindow backOfficeWindow) {}
  


  public void initConfigurationView(JDialog dialog) {}
  


  public String getId()
  {
    return String.valueOf("DefaultOrderServiceExtension".hashCode());
  }
  
  public IView getDeliveryDispatchView(OrderType orderType)
  {
    return null;
  }
  
  public CustomerSelector createNewCustomerSelector()
  {
    return null;
  }
  
  public CustomerSelector createCustomerSelectorView()
  {
    return null;
  }
  


  public void openDeliveryDispatchDialog(OrderType orderType) {}
  

  public IView getDriverView()
  {
    return null;
  }
  
  public List<AbstractAction> getSpecialFunctionActions()
  {
    return null;
  }
  
  public boolean hasValidLicense()
  {
    return true;
  }
  


  public void initUI(PosWindow posWindow) {}
  

  public String getProductVersion()
  {
    return null;
  }
  
  public Component getParent()
  {
    return null;
  }
  
  public void createNewTicket(OrderType ticketType, List<ShopTable> selectedTables, Customer selectedCustomer, int numberOfGuests) throws TicketAlreadyExistsException
  {
    try
    {
      if ((ticketType.isShowGuestSelection().booleanValue()) && (numberOfGuests == 0)) {
        numberOfGuests = PosGuiUtil.captureGuestNumber();
      }
      if (TerminalConfig.isActiveCustomerDisplay()) {
        DrawerUtil.setCustomerDisplayMessage(TerminalConfig.getCustomerDisplayPort(), "Welcome");
      }
      
      if ((ticketType.isRequiredCustomerData().booleanValue()) && (selectedCustomer == null)) {
        selectedCustomer = PosGuiUtil.captureCustomer(ticketType);
        if (selectedCustomer == null) {
          return;
        }
      }
      Application application = Application.getInstance();
      Terminal terminal = application.getTerminal();
      
      Ticket ticket = new Ticket();
      ticket.setTaxIncluded(Boolean.valueOf(application.isPriceIncludesTax()));
      ticket.setOrderType(ticketType);
      ticket.setNumberOfGuests(Integer.valueOf(numberOfGuests));
      ticket.setCustomer(selectedCustomer);
      ticket.setTerminal(terminal);
      
      Department department = terminal.getDepartment();
      if (department != null) {
        ticket.setDepartmentId(department.getId());
      }
      Outlet outlet = terminal.getOutlet();
      if (outlet != null) {
        ticket.setOutletId(outlet.getId());
      }
      ticket.setOwner(Application.getCurrentUser());
      ticket.setShift(ShiftUtil.getCurrentShift());
      ticket.setShouldIncludeInSales(Boolean.valueOf(true));
      
      if (selectedTables != null) {
        for (ShopTable shopTable : selectedTables) {
          shopTable.setTableStatus(TableStatus.Seat);
          SalesArea salesArea = shopTable.getSalesArea();
          if (salesArea != null) {
            ticket.setSalesArea(salesArea);
            ticket.setSalesAreaId(salesArea.getId());
          }
          ticket.addTable(shopTable.getTableNumber().intValue());
        }
      }
      
      Calendar currentTime = DateUtil.getServerTimeCalendar();
      ticket.setCreateDate(currentTime.getTime());
      ticket.setCreationHour(Integer.valueOf(currentTime.get(11)));
      
      OrderView.getInstance().setCurrentTicket(ticket);
      RootView.getInstance().showView("ORDER_VIEW");
    }
    catch (DatabaseConnectionException e) {
      POSMessageDialog.showError("Database connection failure. Please check connection.");
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage());
    }
  }
  
  public void showDeliveryInfo(Ticket ticket, OrderType orderType, Customer customer) {}
}
