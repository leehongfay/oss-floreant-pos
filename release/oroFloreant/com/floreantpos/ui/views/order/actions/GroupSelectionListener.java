package com.floreantpos.ui.views.order.actions;

import com.floreantpos.model.MenuGroup;

public abstract interface GroupSelectionListener
{
  public abstract void groupSelected(MenuGroup paramMenuGroup);
}
