package com.floreantpos.ui.views.order.actions;

import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;

public abstract interface TicketEditListener
{
  public abstract void itemAdded(Ticket paramTicket, TicketItem paramTicketItem);
  
  public abstract void itemRemoved(TicketItem paramTicketItem);
}
