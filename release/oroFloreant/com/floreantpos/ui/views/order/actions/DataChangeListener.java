package com.floreantpos.ui.views.order.actions;

public abstract interface DataChangeListener
{
  public abstract void dataAdded(Object paramObject);
  
  public abstract void dataChanged(Object paramObject);
  
  public abstract void dataRemoved(Object paramObject);
  
  public abstract Object getSelectedData();
  
  public abstract void dataSetUpdated();
  
  public abstract void dataChangeCanceled(Object paramObject);
}
