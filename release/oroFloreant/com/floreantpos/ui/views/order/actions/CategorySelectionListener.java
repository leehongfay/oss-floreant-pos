package com.floreantpos.ui.views.order.actions;

import com.floreantpos.model.MenuCategory;

public abstract interface CategorySelectionListener
{
  public abstract void categorySelected(MenuCategory paramMenuCategory);
}
