package com.floreantpos.ui.views.order.actions;

import com.floreantpos.model.TicketItem;
import com.floreantpos.model.VoidItem;
import com.floreantpos.ui.views.voidticket.VoidItemSplitView;
import com.floreantpos.ui.views.voidticket.VoidTicketItemView;

public abstract interface VoidItemSplitViewsListener
{
  public abstract void itemSelected(TicketItem paramTicketItem, VoidItemSplitView paramVoidItemSplitView, VoidTicketItemView paramVoidTicketItemView, VoidItem paramVoidItem);
}
