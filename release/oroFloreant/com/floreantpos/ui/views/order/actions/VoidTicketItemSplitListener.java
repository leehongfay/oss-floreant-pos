package com.floreantpos.ui.views.order.actions;

import com.floreantpos.model.TicketItem;
import com.floreantpos.model.VoidItem;
import com.floreantpos.ui.views.voidticket.VoidItemSplitView;
import com.floreantpos.ui.views.voidticket.VoidTicketItemView;

public abstract interface VoidTicketItemSplitListener
{
  public abstract void voidTicketItemSelected(TicketItem paramTicketItem, VoidTicketItemView paramVoidTicketItemView, VoidItemSplitView paramVoidItemSplitView, VoidItem paramVoidItem);
}
