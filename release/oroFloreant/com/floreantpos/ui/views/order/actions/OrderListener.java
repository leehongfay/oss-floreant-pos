package com.floreantpos.ui.views.order.actions;

import com.floreantpos.model.Ticket;

public abstract interface OrderListener
{
  public abstract void payOrderSelected(Ticket paramTicket);
}
