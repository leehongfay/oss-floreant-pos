package com.floreantpos.ui.views.order.actions;

import com.floreantpos.model.MenuItem;

public abstract interface ItemSelectionListener
{
  public abstract void itemSelected(MenuItem paramMenuItem);
}
