package com.floreantpos.ui.views.order.actions;

import com.floreantpos.model.TicketItem;
import com.floreantpos.ui.views.TicketSplitView;

public abstract interface SplitItemSelectionListener
{
  public abstract void itemSelected(TicketItem paramTicketItem, TicketSplitView paramTicketSplitView1, TicketSplitView paramTicketSplitView2, int paramInt, Double paramDouble);
}
