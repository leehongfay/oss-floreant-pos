package com.floreantpos.ui.views.order.actions;

public abstract interface OtherViewListener
{
  public abstract void setTableNumber();
  
  public abstract void setCustomerNumber();
  
  public abstract void showOrderInfo();
  
  public abstract void printReceipt();
}
