package com.floreantpos.ui.views.order;

import com.floreantpos.POSConstants;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PaginatedListModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.views.order.actions.GroupSelectionListener;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;


























public class GroupView
  extends SelectionView
{
  private Vector<GroupSelectionListener> listenerList = new Vector();
  
  private MenuCategory menuCategory;
  
  public static final String VIEW_NAME = "GROUP_VIEW";
  
  private ButtonGroup buttonGroup;
  
  public GroupView()
  {
    super(POSConstants.GROUPS, new GridLayout(1, 0, 5, 5), PosUIManager.getSize(100), PosUIManager.getSize(50));
    dataModel = new PaginatedListModel(4);
    remove(actionButtonPanel);
    
    btnPrev.setText("<<");
    btnNext.setText(">>");
    
    add(btnPrev, "West");
    add(btnNext, "East");
  }
  
  public MenuCategory getMenuCategory() {
    return menuCategory;
  }
  
  public void setMenuCategory(MenuCategory menuCategory) {
    this.menuCategory = menuCategory;
    reset();
    
    if (menuCategory == null) {
      return;
    }
    try
    {
      dataModel.setCurrentRowIndex(0);
      MenuGroupDAO.getInstance().loadActiveGroups(menuCategory, dataModel);
      setDataModel(dataModel);
      
      if (dataModel.getSize() <= 1) {
        setVisible(false);
      }
      else {
        setVisible(true);
      }
      
      if (dataModel.getSize() > 0) {
        MenuGroup menuGroup = (MenuGroup)dataModel.getElementAt(0);
        GroupButton groupButton = (GroupButton)getFirstItemButton();
        if (groupButton != null) {
          groupButton.setSelected(true);
          fireGroupSelected(menuGroup);
        }
      }
      else {
        fireGroupSelected(null);
      }
    } catch (Exception e) {
      MessageDialog.showError(e);
    }
  }
  
  protected void renderItems()
  {
    buttonGroup = new ButtonGroup();
    super.renderItems();
  }
  
  protected int getFitableButtonCount() {
    Dimension size = selectionButtonsPanel.getSize();
    Dimension itemButtonSize = getButtonSize();
    
    int horizontalButtonCount = getButtonCount(width, width);
    
    return horizontalButtonCount;
  }
  
  public void addGroupSelectionListener(GroupSelectionListener listener) {
    listenerList.add(listener);
  }
  
  public void removeGroupSelectionListener(GroupSelectionListener listener) {
    listenerList.remove(listener);
  }
  
  private void fireGroupSelected(MenuGroup foodGroup) {
    for (GroupSelectionListener listener : listenerList) {
      listener.groupSelected(foodGroup);
    }
  }
  
  protected AbstractButton createItemButton(Object item)
  {
    MenuGroup menuGroup = (MenuGroup)item;
    GroupButton button = new GroupButton(menuGroup);
    buttonGroup.add(button);
    
    return button;
  }
  
  private class GroupButton extends POSToggleButton implements ActionListener {
    MenuGroup menuGroup;
    
    GroupButton(MenuGroup foodGroup) {
      menuGroup = foodGroup;
      
      setText("<html><body><center>" + foodGroup.getDisplayName() + "</center></body></html>");
      
      if (menuGroup.getButtonColorCode() != null) {
        setBackground(menuGroup.getButtonColor());
      }
      if (menuGroup.getTextColorCode() != null) {
        setForeground(menuGroup.getTextColor());
      }
      
      addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e) {
      GroupView.this.fireGroupSelected(menuGroup);
    }
  }
  
  protected void scrollDown()
  {
    dataModel.setCurrentRowIndex(dataModel.getNextRowIndex());
    MenuGroupDAO.getInstance().loadActiveGroups(menuCategory, dataModel);
    setDataModel(dataModel);
  }
  
  protected void scrollUp()
  {
    dataModel.setCurrentRowIndex(dataModel.getPreviousRowIndex());
    MenuGroupDAO.getInstance().loadActiveGroups(menuCategory, dataModel);
    setDataModel(dataModel);
  }
}
