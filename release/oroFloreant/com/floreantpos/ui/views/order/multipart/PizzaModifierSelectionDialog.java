package com.floreantpos.ui.views.order.multipart;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.MenuItemSize;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.ModifierGroup;
import com.floreantpos.model.Multiplier;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PizzaCrust;
import com.floreantpos.model.PizzaPrice;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItem.PIZZA_SECTION_MODE;
import com.floreantpos.model.TicketItemModifier;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.NumberSelectionDialog2;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.TicketView;
import com.floreantpos.ui.views.order.modifier.ModifierGroupView;
import com.floreantpos.ui.views.order.modifier.ModifierSelectionListener;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Ellipse2D.Double;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.JViewport;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXCollapsiblePane;





























public class PizzaModifierSelectionDialog
  extends POSDialog
  implements ModifierSelectionListener, ActionListener, ChangeListener
{
  private static final String PROP_PIZZA_PRICE = "pizzaPrice";
  private SizeAndCrustSelectionPane sizeAndCrustPanel;
  private PizzaModifierView modifierView;
  private List<Section> sectionList;
  private boolean crustSelected = false;
  
  private Section sectionQuarter1;
  private Section sectionQuarter2;
  private Section sectionQuarter3;
  private Section sectionQuarter4;
  private Section sectionHalf1;
  private Section sectionHalf2;
  private Section sectionWhole;
  private TicketItemModifier crustModifier;
  private CardLayout sectionLayout = new CardLayout();
  JPanel sectionView = new Pizza(sectionLayout);
  private JPanel fullSectionLayout = new TransparentPanel(new GridLayout(1, 1, 2, 2));
  private JPanel halfSectionLayout = new TransparentPanel(new GridLayout(1, 2, 2, 2));
  private JPanel quarterSectionLayout = new TransparentPanel(new GridLayout(2, 2, 2, 2));
  private JTable table;
  private MenuItemSize previousMenuItemSize;
  private MenuItemSize itemSize;
  private TicketItem ticketItem;
  private JPanel wholeSectionView;
  private final MenuItem menuItem;
  private PizzaTicketItemTableModel ticketItemViewerModel;
  private boolean editMode;
  private PosButton btnCustomQuantity;
  private double pizzaQuantity;
  private POSToggleButton btnFull;
  private POSToggleButton btnHalf;
  private JToggleButton btnQuarter;
  private ButtonGroup btnGroup;
  private JToggleButton currentButton;
  private Ticket ticket;
  private JPanel tglBtnPanel;
  private POSToggleButton tglWhole;
  private POSToggleButton tglHalf1;
  private POSToggleButton tglHalf2;
  private POSToggleButton tglQrtr1;
  private POSToggleButton tglQrtr2;
  private POSToggleButton tglQrtr3;
  private POSToggleButton tglQrtr4;
  private ButtonGroup btnSectionGroup;
  
  public PizzaModifierSelectionDialog(Ticket ticket, TicketItem cloneTicketItem, MenuItem menuItem, boolean editMode) {
    this.ticket = ticket;
    this.menuItem = menuItem;
    ticketItem = cloneTicketItem;
    this.editMode = editMode;
    resetPizzaQuantityAndPrice();
    initComponents();
    updateView();
  }
  
  private void initComponents() {
    setTitle("MODIFY PIZZA");
    
    setLayout(new BorderLayout(10, 10));
    JPanel panel = (JPanel)getContentPane();
    panel.setBorder(BorderFactory.createEmptyBorder(7, 7, 7, 7));
    
    JPanel westPanel = new JPanel(new BorderLayout());
    westPanel.add(createSectionPanel(), "Center");
    JPanel centerPanel = new JPanel(new BorderLayout());
    
    JPanel ticketItemTableViewPanel = new JPanel(new BorderLayout());
    ticketItemTableViewPanel.setPreferredSize(PosUIManager.getSize(0, 200));
    
    table = new JTable();
    ticketItemViewerModel = new PizzaTicketItemTableModel();
    table.setModel(ticketItemViewerModel);
    table.setRowHeight(30);
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    table.setAutoResizeMode(3);
    TableColumnModel columnModel = table.getColumnModel();
    columnModel.getColumn(0).setPreferredWidth(200);
    columnModel.getColumn(1).setPreferredWidth(50);
    
    JScrollPane scrollPane = new JScrollPane(table);
    ticketItemTableViewPanel.add(scrollPane);
    
    int size = PosUIManager.getSize(40);
    
    JXCollapsiblePane pizzaItemActionPanel = new JXCollapsiblePane();
    pizzaItemActionPanel.setBackground(Color.WHITE);
    pizzaItemActionPanel.setLayout(new MigLayout("fillx,ins 2 0 2 0", "[" + size + "px][grow][" + size + "px]", "[" + size + "]"));
    pizzaItemActionPanel.setAnimated(true);
    pizzaItemActionPanel.setCollapsed(false);
    pizzaItemActionPanel.setVisible(true);
    
    PosButton btnIncreaseQuantity = new PosButton();
    btnIncreaseQuantity.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PizzaModifierSelectionDialog.access$008(PizzaModifierSelectionDialog.this);
        btnCustomQuantity.setText(NumberUtil.trimDecilamIfNotNeeded(Double.valueOf(pizzaQuantity)));
      }
    });
    btnCustomQuantity = new PosButton(NumberUtil.trimDecilamIfNotNeeded(Double.valueOf(pizzaQuantity)));
    btnCustomQuantity.setForeground(Color.BLUE);
    btnCustomQuantity.setBackground(Color.WHITE);
    Border lineBorder = BorderFactory.createLineBorder(new Color(0.0F, 0.0F, 0.0F, 0.1F), 1);
    btnCustomQuantity.setBorder(lineBorder);
    btnCustomQuantity.setFont(new Font(btnCustomQuantity.getFont().getName(), 1, 20));
    btnCustomQuantity.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        NumberSelectionDialog2 dialog = new NumberSelectionDialog2();
        dialog.setTitle("Enter quantity");
        dialog.setFloatingPoint(false);
        dialog.pack();
        dialog.open();
        
        if (dialog.isCanceled()) {
          return;
        }
        pizzaQuantity = ((int)dialog.getValue());
        btnCustomQuantity.setText(NumberUtil.trimDecilamIfNotNeeded(Double.valueOf(pizzaQuantity)));
      }
    });
    PosButton btnDecreaseQuantity = new PosButton();
    btnDecreaseQuantity.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (pizzaQuantity == 1.0D)
          return;
        PizzaModifierSelectionDialog.access$010(PizzaModifierSelectionDialog.this);
        btnCustomQuantity.setText(NumberUtil.trimDecilamIfNotNeeded(Double.valueOf(pizzaQuantity)));
      }
    });
    btnIncreaseQuantity.setIcon(IconFactory.getIcon("/ui_icons/", "plus_32.png"));
    btnDecreaseQuantity.setIcon(IconFactory.getIcon("/ui_icons/", "minus_32.png"));
    
    pizzaItemActionPanel.add(btnDecreaseQuantity);
    pizzaItemActionPanel.add(btnCustomQuantity, "grow");
    pizzaItemActionPanel.add(btnIncreaseQuantity);
    
    ticketItemTableViewPanel.add(pizzaItemActionPanel, "South");
    
    westPanel.add(ticketItemTableViewPanel, "North");
    sizeAndCrustPanel = new SizeAndCrustSelectionPane();
    centerPanel.add(sizeAndCrustPanel, "North");
    
    modifierView = new PizzaModifierView(ticket, ticketItem, menuItem, this);
    modifierView.addModifierSelectionListener(this);
    centerPanel.add(modifierView, "Center");
    add(centerPanel, "Center");
    add(westPanel, "West");
    
    createButtonPanel();
    doHalfSectionMode();
  }
  
  private JPanel createToggleButtonPanel() {
    tglBtnPanel = new JPanel(new MigLayout("fill, hidemode 3, ins 7 0"));
    tglBtnPanel.setBorder(null);
    btnSectionGroup = new ButtonGroup();
    tglWhole = new POSToggleButton("WHOLE");
    tglHalf1 = new POSToggleButton("H1");
    tglHalf2 = new POSToggleButton("H2");
    tglQrtr1 = new POSToggleButton("Q1");
    tglQrtr2 = new POSToggleButton("Q2");
    tglQrtr3 = new POSToggleButton("Q3");
    tglQrtr4 = new POSToggleButton("Q4");
    
    tglWhole.addChangeListener(this);
    
    tglHalf1.addChangeListener(this);
    tglHalf2.addChangeListener(this);
    
    tglQrtr1.addChangeListener(this);
    tglQrtr2.addChangeListener(this);
    tglQrtr3.addChangeListener(this);
    tglQrtr4.addChangeListener(this);
    
    btnSectionGroup.add(tglWhole);
    btnSectionGroup.add(tglHalf1);
    btnSectionGroup.add(tglHalf2);
    btnSectionGroup.add(tglQrtr1);
    btnSectionGroup.add(tglQrtr2);
    btnSectionGroup.add(tglQrtr3);
    btnSectionGroup.add(tglQrtr4);
    
    tglBtnPanel.add(tglWhole, "grow");
    tglBtnPanel.add(tglHalf1, "grow");
    tglBtnPanel.add(tglHalf2, "grow");
    tglBtnPanel.add(tglQrtr1, "grow");
    tglBtnPanel.add(tglQrtr2, "grow");
    tglBtnPanel.add(tglQrtr3, "grow");
    tglBtnPanel.add(tglQrtr4, "grow");
    
    return tglBtnPanel;
  }
  
  private void updateView() {
    if (ticketItem.getSizeModifier() == null) {
      if (crustModifier != null) {
        ticketItem.setSizeModifier(crustModifier);
        ticketItem.getSizeModifier().calculatePrice();
      }
    }
    else {
      ticketItem.getSizeModifier().calculatePrice();
    }
    ticketItemViewerModel.setTicketItem(ticketItem);
    ticketItemViewerModel.updateView();
    

    List<TicketItemModifier> ticketItemModifiers = ticketItem.getTicketItemModifiers();
    if (ticketItemModifiers == null) {
      return;
    }
    for (Iterator localIterator1 = sectionList.iterator(); localIterator1.hasNext();) { section = (Section)localIterator1.next();
      for (iterator = ticketItemModifiers.iterator(); iterator.hasNext();) {
        TicketItemModifier ticketItemModifier = (TicketItemModifier)iterator.next();
        if ((!ticketItemModifier.isInfoOnly().booleanValue()) && (section.getSectionName().equals(ticketItemModifier.getSectionName())))
          sectionModifierTableModel.addItem(ticketItemModifier);
      }
    }
    Section section;
    Iterator iterator;
    if (ticketItem.getPizzaSectionMode() != null) {
      if (ticketItem.getPizzaSectionMode() == TicketItem.PIZZA_SECTION_MODE.FULL) {
        btnFull.setSelected(true);
        currentButton = btnFull;
        doFullSectionMode();
        if (ticketItem.isPrintedToKitchen().booleanValue()) {
          btnHalf.setEnabled(false);
          btnQuarter.setEnabled(false);
        }
        
      }
      else if (ticketItem.getPizzaSectionMode() == TicketItem.PIZZA_SECTION_MODE.HALF) {
        btnHalf.setSelected(true);
        currentButton = btnHalf;
        doHalfSectionMode();
        if (ticketItem.isPrintedToKitchen().booleanValue()) {
          btnFull.setEnabled(false);
          btnQuarter.setEnabled(false);
        }
      }
      else if (ticketItem.getPizzaSectionMode() == TicketItem.PIZZA_SECTION_MODE.QUARTER) {
        btnQuarter.setSelected(true);
        currentButton = btnQuarter;
        doQuarterSectionMode();
        if (ticketItem.isPrintedToKitchen().booleanValue()) {
          btnHalf.setEnabled(false);
          btnFull.setEnabled(false);
        }
      }
    }
  }
  
  private JPanel createSectionPanel()
  {
    JPanel westPanel = new JPanel(new BorderLayout(5, 5));
    sectionList = new ArrayList();
    sectionWhole = new Section("WHOLE", "WHOLE", 0, true, 1.0D);
    sectionQuarter1 = new Section("Quarter 1", "Quarter 1", 1, false, 0.25D);
    sectionQuarter2 = new Section("Quarter 2", "Quarter 2", 2, false, 0.25D);
    sectionQuarter3 = new Section("Quarter 3", "Quarter 3", 3, false, 0.25D);
    sectionQuarter4 = new Section("Quarter 4", "Quarter 4", 4, false, 0.25D);
    sectionHalf1 = new Section("Half 1", "Half 1", 5, false, 0.5D);
    sectionHalf2 = new Section("Half 2", "Half 2", 6, false, 0.5D);
    
    sectionList.add(sectionWhole);
    
    sectionList.add(sectionQuarter1);
    sectionList.add(sectionQuarter2);
    sectionList.add(sectionQuarter3);
    sectionList.add(sectionQuarter4);
    
    sectionList.add(sectionHalf1);
    sectionList.add(sectionHalf2);
    
    fullSectionLayout.add(sectionWhole);
    
    halfSectionLayout.add(sectionHalf1);
    halfSectionLayout.add(sectionHalf2);
    
    quarterSectionLayout.add(sectionQuarter1);
    quarterSectionLayout.add(sectionQuarter2);
    quarterSectionLayout.add(sectionQuarter3);
    quarterSectionLayout.add(sectionQuarter4);
    
    sectionView.add(fullSectionLayout, "full");
    sectionView.add(halfSectionLayout, "half");
    sectionView.add(quarterSectionLayout, "quarter");
    

    wholeSectionView = new JPanel(new MigLayout("fill,ins 0 0 0 0"));
    wholeSectionView.setOpaque(false);
    westPanel.setOpaque(false);
    
    westPanel.add(createToggleButtonPanel(), "North");
    westPanel.add(sectionView, "Center");
    westPanel.add(wholeSectionView, "South");
    westPanel.setPreferredSize(PosUIManager.getSize(300, 0));
    return westPanel;
  }
  
  public void createButtonPanel() {
    TransparentPanel buttonPanel = new TransparentPanel();
    buttonPanel.setLayout(new MigLayout("fill, ins 2", "", ""));
    
    btnGroup = new ButtonGroup();
    
    btnFull = new POSToggleButton("FULL");
    currentButton = btnFull;
    btnFull.addActionListener(this);
    btnHalf = new POSToggleButton("HALF");
    btnHalf.setSelected(true);
    btnHalf.addActionListener(this);
    
    btnQuarter = new POSToggleButton("QUARTER");
    btnQuarter.addActionListener(this);
    
    btnGroup.add(btnFull);
    btnGroup.add(btnHalf);
    btnGroup.add(btnQuarter);
    
    PosButton btnClear = new PosButton("CLEAR");
    btnClear.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        PizzaModifierSelectionDialog.Section section = getSelectedSection();
        if (section == null) {
          return;
        }
        section.clearSelectedItem();
      }
      
    });
    PosButton btnClearAll = new PosButton("CLEAR ALL");
    btnClearAll.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        PizzaModifierSelectionDialog.Section section = getSelectedSection();
        if (section == null) {
          return;
        }
        section.clearItems();
      }
      
    });
    PosButton btnSave = new PosButton("DONE");
    btnSave.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        if (PizzaModifierSelectionDialog.this.doFinishModifierSelection()) {
          setCanceled(false);
          dispose();
        }
        
      }
    });
    PosButton btnCancel = new PosButton(POSConstants.CANCEL.toUpperCase());
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        setCanceled(true);
        dispose();
      }
    });
    int width = PosUIManager.getSize(97);
    JSeparator separator = new JSeparator(1);
    buttonPanel.add(btnFull, "w " + width + "!, split 5");
    buttonPanel.add(btnHalf, "w " + width + "!");
    buttonPanel.add(btnQuarter, "w " + width + "!");
    buttonPanel.add(separator, "growy");
    buttonPanel.add(btnClear, "grow");
    buttonPanel.add(btnClearAll, "grow");
    buttonPanel.add(btnCancel, "grow");
    buttonPanel.add(btnSave, "grow");
    
    add(buttonPanel, "South");
  }
  
  private boolean doFinishModifierSelection() {
    if (!crustSelected) {
      POSMessageDialog.showError("Please select size and crust.");
      return false;
    }
    List<MenuItemModifierSpec> menuItemModiferGroups = ticketItem.getMenuItem().getMenuItemModiferSpecs();
    for (Iterator iterator = menuItemModiferGroups.iterator(); iterator.hasNext();) {
      MenuItemModifierSpec menuItemModifierGroup = (MenuItemModifierSpec)iterator.next();
      if (!ticketItem.requiredModifiersAdded(menuItemModifierGroup)) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), 
          String.format("Required modifiers for group %s not added!", new Object[] {menuItemModifierGroup.getModifierGroup().getDisplayName() }));
        return false;
      }
    }
    
    updatePizzaQuantityAndPrice();
    
    if (!editMode) {
      OrderView.getInstance().getTicketView().addTicketItem(ticketItem);
      
      int showYesNoQuestionDialog = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Do you want to create more pizza?", "More Pizza");
      
      if (showYesNoQuestionDialog == 0) {
        TicketItem newTicketItem = ticketItem.clone();
        newTicketItem.setId(null);
        ticketItem = newTicketItem;
        reset();
        return false;
      }
    }
    return true;
  }
  
  private void updatePizzaQuantityAndPrice() {
    ticketItem.setQuantity(Double.valueOf(pizzaQuantity));
    







    ticketItem.calculatePrice();
  }
  
  private void resetPizzaQuantityAndPrice() {
    pizzaQuantity = ticketItem.getQuantity().doubleValue();
    ticketItem.setQuantity(Double.valueOf(1.0D));
    List<TicketItemModifier> ticketItemModifiers = ticketItem.getTicketItemModifiers();
    if (ticketItemModifiers != null) {
      for (TicketItemModifier ticketItemModifier : ticketItemModifiers)
        if (!ticketItemModifier.isInfoOnly().booleanValue())
        {
          ticketItemModifier.setItemQuantity(Double.valueOf(ticketItemModifier.getItemQuantity().doubleValue() / pizzaQuantity)); }
    }
    ticketItem.calculatePrice();
  }
  
  private void reset() {
    for (Iterator iterator = sectionList.iterator(); iterator.hasNext();) {
      Section section = (Section)iterator.next();
      if (sectionModifierTableModel.getRows() != null)
      {

        for (Iterator iterator2 = sectionModifierTableModel.getRows().iterator(); iterator2.hasNext();) {
          TicketItemModifier ticketItemModifier = (TicketItemModifier)iterator2.next();
          if (ticketItemModifier != null) {
            iterator2.remove();
          }
        }
        sectionModifierTableModel.fireTableDataChanged();
        section.repaint();
      } }
    modifierView.getModifierGroupView().selectFirst();
    ticketItem.setSizeModifier(getSizeAndCrustModifer());
    ticketItem.getSizeModifier().calculatePrice();
    if (ticketItem.getTicketItemModifiers() != null) {
      ticketItem.getTicketItemModifiers().clear();
    }
    ticketItemViewerModel.setTicketItem(ticketItem);
    ticketItemViewerModel.updateView();
  }
  
  private boolean isMaxModifierAddedFromGroup(MenuItemModifierSpec menuItemModifierGroup, int currentModifierCount) {
    int minQuantity = menuItemModifierGroup.getMinQuantity().intValue();
    int maxQuantity = menuItemModifierGroup.getMaxQuantity().intValue();
    
    if (maxQuantity < minQuantity) {
      maxQuantity = minQuantity;
    }
    
    if (currentModifierCount >= maxQuantity) {
      return true;
    }
    return false;
  }
  

  public void modifierSelected(MenuModifier modifier, Multiplier multiplier)
  {
    MenuItemModifierSpec menuItemModifierGroup = modifier.getMenuItemModifierGroup();
    int countModifier = ticketItem.countModifierFromGroup(menuItemModifierGroup);
    if (isMaxModifierAddedFromGroup(menuItemModifierGroup, countModifier)) {
      POSMessageDialog.showError("You have added maximum number of allowed modifiers from group " + modifier.getMenuItemModifierGroup().getDisplayName());
      modifierGroupSelectionDone(menuItemModifierGroup);
      modifierView.revalidate();
      modifierView.repaint();
      return;
    }
    
    Section selectedSection = getSelectedSection();
    boolean itemSizeSame = false;
    itemSize = sizeAndCrustPanel.getMenuItemSize();
    if (itemSize != null) {
      if (previousMenuItemSize != null) {
        if (previousMenuItemSize == itemSize) {
          itemSizeSame = true;
        }
      }
      else {
        previousMenuItemSize = itemSize;
        itemSizeSame = true;
      }
    }
    
    TicketItemModifier findTicketItemModifierForWholeSection = ticketItem.findTicketItemModifierFor(modifier, getMainSection().getSectionName(), null);
    TicketItemModifier ticketItemModifier = ticketItem.findTicketItemModifierFor(modifier, selectedSection.getSectionName(), null);
    int countSection; if (!mainSection) {
      countSection = 0;
      for (Section sec : sectionList)
      {
        if (sec.getSectionName().startsWith("Half")) {
          if (sec != selectedSection)
          {

            TicketItemModifier ticItemModifier = ticketItem.findTicketItemModifierFor(modifier, sec.getSectionName(), null);
            if ((ticItemModifier != null) && (ticketItemModifier == null)) {
              if (findTicketItemModifierForWholeSection != null) {
                POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Item already added in WHOLE section!");
                return;
              }
              int questionDialog = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Would you like to add this item in WHOLE section?", "Add Modifier");
              
              if (questionDialog != 0) {
                break;
              }
              sec.clearItem(ticItemModifier, sectionModifierTableModel);
              selectedSection = getMainSection();
            }
          }
        } else if (sec.getSectionName().startsWith("Quarter")) {
          if (sec != selectedSection)
          {

            TicketItemModifier ticItemModifier = ticketItem.findTicketItemModifierFor(modifier, sec.getSectionName(), null);
            if ((ticItemModifier != null) && (ticketItemModifier == null))
            {
              if (countSection == 2) {
                if (findTicketItemModifierForWholeSection != null) {
                  POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Item already added in WHOLE section!");
                  return;
                }
                int questionDialog = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Would you like to add this item in WHOLE section?", "Add Modifier");
                
                if (questionDialog == 0) {
                  for (Section sectionClear : sectionList) {
                    if (sectionClear.getSectionName().startsWith("Quarter")) {
                      sectionClear.clearItem(ticItemModifier, sectionModifierTableModel);
                    }
                    sectionClear.repaint();
                  }
                  
                  selectedSection = getMainSection();
                }
              }
              countSection++;
            }
          }
        }
      }
    }
    

    if ((findTicketItemModifierForWholeSection != null) && (!mainSection) && (ticketItemModifier == null)) {
      int questionDialog = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Item already added in pizza, Would you like to add again?", "Add Modifier");
      
      if (questionDialog == 1) {
        return;
      }
    }
    
    if ((ticketItemModifier == null) || (ticketItemModifier.isPrintedToKitchen().booleanValue()) || (!itemSizeSame)) {
      OrderType orderType = ticketItem.getTicket().getOrderType();
      ticketItemModifier = convertToTicketItemModifier(ticketItem, modifier, orderType, multiplier);
      
      ticketItemModifier.setSectionName(selectedSection.getSectionName());
      ticketItemModifier.setMenuModifier(modifier);
      
      if (ticketItemModifier.isShouldSectionWisePrice().booleanValue()) {
        ticketItemModifier.setUnitPrice(Double.valueOf(selectedSection.calculatePrice(ticketItemModifier.getUnitPrice().doubleValue())));
      }
      selectedSection.addItem(ticketItemModifier);
      TicketItemModifier separator = getSeparatorIfNeeded(selectedSection.getSectionName());
      if (separator != null) {
        ticketItem.addToticketItemModifiers(separator);
      }
      
      ticketItem.addToticketItemModifiers(ticketItemModifier);
      if (!ticketItemModifier.isInfoOnly().booleanValue()) {
        double defaultSellPortion = menuItem.getDefaultSellPortion().intValue();
        ticketItem.updateModifiersUnitPrice(defaultSellPortion);
      }
      
    }
    else
    {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Item already added!");
      modifierView.revalidate();
      modifierView.repaint();
      return;
    }
    

    if (countModifier + 1 >= menuItemModifierGroup.getMinQuantity().intValue()) {
      modifierGroupSelectionDone(menuItemModifierGroup);
    }
    

    ticketItemViewerModel.updateView();
    
    modifierView.revalidate();
    modifierView.repaint();
    revalidate();
    repaint();
  }
  
  private TicketItemModifier getSeparatorIfNeeded(String sectionName) {
    List<TicketItemModifier> ticketItemModifiers = ticketItem.getTicketItemModifiers();
    if ((ticketItemViewerModel.getRowCount() == 1) && (sectionName.equals("WHOLE"))) {
      return null;
    }
    if ((ticketItemModifiers != null) && (!ticketItemModifiers.isEmpty())) {
      TicketItemModifier lastItem = (TicketItemModifier)ticketItemModifiers.get(ticketItemModifiers.size() - 1);
      if (sectionName.equals(lastItem.getSectionName())) {
        return null;
      }
    }
    TicketItemModifier ticketItemModifier = new TicketItemModifier();
    ticketItemModifier.setName("== " + sectionName + " ==");
    ticketItemModifier.setModifierType(Integer.valueOf(6));
    ticketItemModifier.setInfoOnly(Boolean.valueOf(true));
    ticketItemModifier.setSectionName(sectionName);
    ticketItemModifier.setTicketItem(ticketItem);
    return ticketItemModifier;
  }
  

  public void clearModifiers(MenuItemModifierSpec modifierGroup) {}
  

  public void modifierGroupSelectionDone(MenuItemModifierSpec menuItemModifierGroup)
  {
    if (!isRequiredModifiersAdded(ticketItem, menuItemModifierGroup)) {
      showModifierSelectionMessage(menuItemModifierGroup);
      modifierView.getModifierGroupView().setSelectedModifierGroup(menuItemModifierGroup);
      return;
    }
    
    if ((menuItemModifierGroup.isJumpGroup().booleanValue()) && (modifierView.getModifierGroupView().hasNextMandatoryGroup())) {
      modifierView.getModifierGroupView().selectNextGroup();
    }
  }
  



  public static boolean isRequiredModifiersAdded(TicketItem ticketItem, MenuItemModifierSpec menuItemModifierGroup)
  {
    return true;
  }
  
  private TicketItemModifier convertToTicketItemModifier(TicketItem ticketItem, MenuModifier menuModifier, OrderType type, Multiplier multiplier) {
    TicketItemModifier ticketItemModifier = new TicketItemModifier();
    
    ticketItemModifier.setTaxIncluded(Boolean.valueOf(Application.getInstance().isPriceIncludesTax()));
    ticketItemModifier.setItemId(menuModifier.getId());
    MenuItemModifierSpec menuItemModifierGroup = menuModifier.getMenuItemModifierGroup();
    if (menuItemModifierGroup != null) {
      ticketItemModifier.setGroupId(menuItemModifierGroup.getId());
    }
    ticketItemModifier.setItemQuantity(Double.valueOf(1.0D));
    ticketItemModifier.setName(menuModifier.getDisplayName().trim());
    ticketItemModifier.setTicketItem(ticketItem);
    double priceForSize = menuModifier.getPriceForSizeAndMultiplier(getSelectedSize(), false, multiplier);
    if (multiplier != null) {
      ticketItemModifier.setMultiplierName(multiplier.getId());
      ticketItemModifier.setName(multiplier.getTicketPrefix() + " " + menuModifier.getDisplayName());
    }
    ticketItemModifier.setUnitPrice(Double.valueOf(priceForSize));
    ticketItemModifier.setTaxes(menuModifier.getTaxByOrderType(type));
    ticketItemModifier.setModifierType(Integer.valueOf(1));
    ticketItemModifier.setShouldPrintToKitchen(menuModifier.isShouldPrintToKitchen());
    ticketItemModifier.setShouldSectionWisePrice(menuModifier.isShouldSectionWisePrice());
    
    return ticketItemModifier;
  }
  
  public MenuItemSize getSelectedSize() {
    List<POSToggleButton> sizeButtonList = sizeAndCrustPanel.sizeButtonList;
    for (POSToggleButton posToggleButton : sizeButtonList) {
      if (posToggleButton.isSelected()) {
        PizzaPrice pizzaPrice = (PizzaPrice)posToggleButton.getClientProperty("pizzaPrice");
        return pizzaPrice.getSize();
      }
    }
    
    return null;
  }
  
  private class Section extends JPanel implements MouseListener
  {
    boolean selected;
    private boolean mainSection;
    private PosButton lblTitle;
    private final String sectionName;
    private final int sortOrder;
    private final String displayTitle;
    private final double price;
    private JTable sectionTable;
    private PizzaModifierSelectionDialog.SectionModifierTableModel sectionModifierTableModel;
    
    public Section(String sectionName, String displayTitle, int sortOrder, boolean main, double price)
    {
      this.sectionName = sectionName;
      this.displayTitle = displayTitle;
      this.sortOrder = sortOrder;
      mainSection = main;
      this.price = price;
      
      setLayout(new BorderLayout());
      lblTitle = new PosButton(sectionName);
      lblTitle.setBackground(Color.LIGHT_GRAY);
      lblTitle.setHorizontalAlignment(0);
      lblTitle.setFont(lblTitle.getFont().deriveFont(1, 14.0F));
      lblTitle.setOpaque(true);
      

      setOpaque(false);
      setPreferredSize(PosUIManager.getSize(160, 170));
      

      setBorder(null);
      








      sectionTable = new JTable();
      sectionTable.setTableHeader(null);
      sectionTable.setRowHeight(PosUIManager.getSize(30));
      sectionModifierTableModel = new PizzaModifierSelectionDialog.SectionModifierTableModel(PizzaModifierSelectionDialog.this);
      sectionTable.setDefaultRenderer(Object.class, new PizzaModifierSelectionDialog.ModifierTableCellRenderer(PizzaModifierSelectionDialog.this));
      sectionTable.setModel(sectionModifierTableModel);
      JScrollPane scrollPane = new JScrollPane(sectionTable);
      scrollPane.setBorder(null);
      JViewport viewPort = scrollPane.getViewport();
      viewPort.setOpaque(false);
      scrollPane.setOpaque(false);
      scrollPane.setBorder(null);
      add(scrollPane, "Center");
      

      sectionTable.addMouseListener(this);
      resizeColumnWidth(sectionTable);
    }
    
    public void resizeColumnWidth(JTable table) {
      TableColumnModel columnModel = table.getColumnModel();
      for (int column = 0; column < table.getColumnCount(); column++) {
        columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
      }
    }
    
    private List getColumnWidth() {
      List<Integer> columnWidth = new ArrayList();
      columnWidth.add(Integer.valueOf(70));
      columnWidth.add(Integer.valueOf(30));
      return columnWidth;
    }
    
    public void paintComponent(Graphics g)
    {
      Graphics2D graphics2d = (Graphics2D)g;
      AlphaComposite composite = (AlphaComposite)graphics2d.getComposite();
      AlphaComposite composite2 = composite.derive(0.75F);
      graphics2d.setComposite(composite2);
      super.paintComponent(g);
    }
    
    public boolean isEmpty() {
      return sectionModifierTableModel.getRows().size() <= 0;
    }
    
    public void clearItems() {
      boolean isPrintedModifierExist = false;
      for (Iterator iterator = ticketItem.getTicketItemModifiers().iterator(); iterator.hasNext();) {
        TicketItemModifier ticketItemModifier = (TicketItemModifier)iterator.next();
        if (!ticketItemModifier.isPrintedToKitchen().booleanValue()) {
          if (ticketItemModifier.getSectionName().equals(getSectionName())) {
            iterator.remove();
            ticketItem.deleteTicketItemModifier(ticketItemModifier);
            sectionModifierTableModel.deleteGivenItem(ticketItemModifier);
          }
        }
        else {
          isPrintedModifierExist = true;
        }
      }
      
      ticketItemViewerModel.updateView();
      sectionTable.repaint();
      repaint();
      if (isPrintedModifierExist) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Modifiers that sent to kitchen can not be deleted!");
      }
      previousMenuItemSize = null;
    }
    
    public void clearSelectedItem() {
      boolean isPrintedModifierExist = false;
      if (sectionModifierTableModel.getRows() == null) {
        return;
      }
      if (sectionModifierTableModel.getRows().size() == 0) {
        return;
      }
      int index = sectionTable.getSelectedRow();
      if (index < 0) {
        return;
      }
      TicketItemModifier selectedModifier = (TicketItemModifier)sectionModifierTableModel.getRowData(index);
      if (selectedModifier == null) {
        return;
      }
      if (!selectedModifier.isPrintedToKitchen().booleanValue()) {
        ticketItem.deleteTicketItemModifier(selectedModifier);
        sectionModifierTableModel.deleteGivenItem(selectedModifier);
        if (sectionModifierTableModel.getRows().size() == 0) {
          clearItems();
        }
      }
      else {
        isPrintedModifierExist = true;
      }
      
      ticketItemViewerModel.updateView();
      repaint();
      if (isPrintedModifierExist) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Modifiers that sent to kitchen can not be deleted!");
      }
      previousMenuItemSize = null;
    }
    
    public void clearItem(TicketItemModifier ticketItemModifier, PizzaModifierSelectionDialog.SectionModifierTableModel sectionModifierTableModel) {
      boolean isPrintedModifierExist = false;
      if (sectionModifierTableModel.getRows() == null) {
        return;
      }
      if (sectionModifierTableModel.getRows().size() == 0) {
        return;
      }
      if (!ticketItemModifier.isPrintedToKitchen().booleanValue()) {
        ticketItem.deleteTicketItemModifierByName(ticketItemModifier);
        sectionModifierTableModel.deleteGivenItemByName(ticketItemModifier);
        if (sectionModifierTableModel.getRows().size() == 0) {
          clearItems();
        }
      }
      else {
        isPrintedModifierExist = true;
      }
      
      ticketItemViewerModel.updateView();
      repaint();
      if (isPrintedModifierExist) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Modifiers that sent to kitchen can not be deleted!");
      }
      previousMenuItemSize = null;
    }
    
    public void setSelected(boolean selected) {
      this.selected = selected;
      repaint();
    }
    
    public boolean isSelected() {
      return selected;
    }
    
    public void addItem(TicketItemModifier newModifier)
    {
      sectionModifierTableModel.addItem(newModifier);
      repaint();
    }
    

    public void mouseClicked(MouseEvent e)
    {
      PizzaModifierSelectionDialog.this.selectSectionTogglebutton(this);
    }
    



    public void mousePressed(MouseEvent e) {}
    


    public void mouseReleased(MouseEvent e) {}
    


    public void mouseEntered(MouseEvent e) {}
    


    public void mouseExited(MouseEvent e) {}
    


    public String getSectionName()
    {
      return sectionName;
    }
    
    public double getPrice() {
      return price;
    }
    
    public double calculatePrice(double modifierPrice) {
      return modifierPrice * getPrice();
    }
  }
  
  public void setSelectedSection(Section section) {
    if (section.isSelected())
    {

      return;
    }
    for (Section sec : sectionList) {
      lblTitle.setBackground(Color.lightGray);
      sec.setSelected(false);
      sec.setBorder(null);
    }
    lblTitle.setBackground(Color.green);
    section.setSelected(true);
    wholeSectionView.revalidate();
    wholeSectionView.repaint();
  }
  
  public Section getSelectedSection() {
    for (Section sec : sectionList) {
      if (sec.isSelected()) {
        return sec;
      }
    }
    return getMainSection();
  }
  
  public Section getMainSection() {
    for (Section sec : sectionList) {
      if (mainSection) {
        return sec;
      }
    }
    
    return null;
  }
  
  public class Pizza extends JPanel {
    int size;
    
    public Pizza(LayoutManager layoutManager) {
      super();
      setOpaque(false);
      setBackground(Color.white);
      setPreferredSize(PosUIManager.getSize(200, 200));
    }
    
    public void setSize(int size) {
      this.size = size;
    }
    
    public void paintComponent(Graphics g)
    {
      super.paintComponent(g);
      
      int height = getHeight();
      int x = 30;
      int width = getWidth() - 2 * x;
      int y = 0;
      
      if (height > width) {
        y = height / 2 - width / 2;
      }
      else
      {
        x = (int)((getWidth() - height) / 1.65D);
        y = height / 15;
        width = height - getWidth() / y;
      }
      height = width;
      g.setColor(Color.WHITE);
      
      Graphics2D g2d = (Graphics2D)g;
      g.setColor(new Color(255, 251, 211));
      Ellipse2D.Double circle = new Ellipse2D.Double(x, y, width, height);
      g2d.fill(circle);
      








      Graphics2D g2 = (Graphics2D)g;
      g2.setColor(Color.green);
      PizzaModifierSelectionDialog.Section selectedSection = getSelectedSection();
      if (selectedSection == null) {
        return;
      }
      
      if (selectedSection.getSectionName().equalsIgnoreCase("Quarter 1")) {
        fillQuarter1(g2, x, y, width);
      }
      else if (selectedSection.getSectionName().equalsIgnoreCase("Quarter 2")) {
        fillQuarter2(g2, x, y, width);
      }
      else if (selectedSection.getSectionName().equalsIgnoreCase("Quarter 3")) {
        fillQuarter3(g2, x, y, width);
      }
      else if (selectedSection.getSectionName().equalsIgnoreCase("Quarter 4")) {
        fillQuarter4(g2, x, y, width);
      }
      else if (selectedSection.getSectionName().equalsIgnoreCase("Half 1")) {
        fillHalf1(g2, x, y, width);
      }
      else if (selectedSection.getSectionName().equalsIgnoreCase("Half 2")) {
        fillHalf2(g2, x, y, width);
      }
    }
    
    void drawCircleByCenter(Graphics g, int x, int y, int radius) {
      Graphics2D g2 = (Graphics2D)g;
      g2.setStroke(new BasicStroke(2.0F));
      g2.setColor(Color.lightGray);
      g.drawOval(x - radius, y - radius, 2 * radius, 2 * radius);
    }
    
    void fillQuarter1(Graphics2D g2, int x, int y, int width)
    {
      g2.fillArc(x, y, width, width, 90, 90);
    }
    
    void fillQuarter2(Graphics2D g2, int x, int y, int width) {
      g2.fillArc(x, y, width, width, 360, 90);
    }
    
    void fillQuarter3(Graphics2D g2, int x, int y, int width) {
      g2.fillArc(x, y, width, width, 180, 90);
    }
    
    void fillQuarter4(Graphics2D g2, int x, int y, int width) {
      g2.fillArc(x, y, width, width, 270, 90);
    }
    
    void fillHalf1(Graphics2D g2, int x, int y, int width) {
      g2.fillArc(x, y, width, width, 90, 180);
    }
    
    void fillHalf2(Graphics2D g2, int x, int y, int width) {
      g2.fillArc(x, y, width, width, 270, 180);
    }
  }
  
  private TicketItemModifier getSizeAndCrustModifer()
  {
    if ((ticketItem != null) && 
      (ticketItem.getSizeModifier() != null)) {
      return this.crustModifier = ticketItem.getSizeModifier();
    }
    return crustModifier;
  }
  
  public void modifierRemoved(TicketItemModifier modifier) {}
  
  class SizeAndCrustSelectionPane extends JPanel { List<PizzaPrice> priceList;
    List<POSToggleButton> sizeButtonList = new ArrayList();
    List<POSToggleButton> crustButtonList = new ArrayList();
    
    JPanel sizePanel = new JPanel();
    JPanel crustPanel = new JPanel();
    
    ButtonGroup sizeBtnGroup = new ButtonGroup();
    ButtonGroup crustBtnGroup = new ButtonGroup();
    MenuItemSize menuItemSize;
    PizzaCrust pizzaCrust;
    
    public SizeAndCrustSelectionPane()
    {
      priceList = menuItem.getPizzaPriceList();
      
      setLayout(new BorderLayout());
      
      sizePanel.setBorder(BorderFactory.createTitledBorder(null, "SIZE", 2, 2));
      
      crustPanel.setBorder(BorderFactory.createTitledBorder(null, "CRUST", 2, 2));
      crustPanel.setLayout(new FlowLayout());
      
      Set<MenuItemSize> uniqueSizeList = new HashSet();
      
      for (PizzaPrice pizzaPrice : priceList) {
        MenuItemSize size = pizzaPrice.getSize();
        if (!uniqueSizeList.contains(size))
        {


          uniqueSizeList.add(size);
          addSizeButton(pizzaPrice, size);
        }
      }
      selectExistingSizeAndCrust();
      
      add(sizePanel, "West");
      add(crustPanel);
    }
    
    private void selectExistingSizeAndCrust() {
      TicketItemModifier sizeAndCrustModifer = PizzaModifierSelectionDialog.this.getSizeAndCrustModifer();
      String crustName; if (sizeAndCrustModifer != null) {
        String sizeAndCrustName = sizeAndCrustModifer.getName();
        String[] split = sizeAndCrustName.split(" ");
        String sizeName = split[0];
        crustName = split[1];
        
        for (POSToggleButton sizeButton : sizeButtonList) {
          PizzaPrice pizzaPrice = (PizzaPrice)sizeButton.getClientProperty("pizzaPrice");
          if (pizzaPrice.getSize().getName().equalsIgnoreCase(sizeName)) {
            sizeButton.setSelected(true);
            renderCrusts(pizzaPrice.getSize());


          }
          else if (ticketItem.isPrintedToKitchen().booleanValue()) {
            sizeButton.setEnabled(false);
          }
        }
        

        for (POSToggleButton crustButton : crustButtonList) {
          PizzaPrice pizzaPrice = (PizzaPrice)crustButton.getClientProperty("pizzaPrice");
          if (pizzaPrice.getCrust().getName().startsWith(crustName)) {
            crustButton.setSelected(true);
            crustSelected = true;

          }
          else if (ticketItem.isPrintedToKitchen().booleanValue()) {
            crustButton.setEnabled(false);
          }
          
        }
        

      }
      else if (!sizeButtonList.isEmpty()) {
        List<Boolean> isBtnSelected = new ArrayList();
        for (Iterator iterator = sizeButtonList.iterator(); iterator.hasNext();) {
          POSToggleButton button = (POSToggleButton)iterator.next();
          if (button.isSelected()) {
            PizzaPrice pizzaPrice = (PizzaPrice)button.getClientProperty("pizzaPrice");
            renderCrusts(pizzaPrice.getSize());
            isBtnSelected.add(Boolean.valueOf(true));
          }
        }
        
        if ((isBtnSelected.isEmpty()) || (isBtnSelected.contains(Boolean.valueOf(false)))) {
          POSToggleButton button = (POSToggleButton)sizeButtonList.get(0);
          PizzaPrice pizzaPrice = (PizzaPrice)button.getClientProperty("pizzaPrice");
          renderCrusts(pizzaPrice.getSize());
          
          button.setSelected(true);
        }
      }
    }
    
    private void addSizeButton(PizzaPrice pizzaPrice, MenuItemSize size)
    {
      POSToggleButton sizeButton = new POSToggleButton(size.getName());
      sizeButton.putClientProperty("pizzaPrice", pizzaPrice);
      if (size.isDefaultSize().booleanValue()) {
        sizeButton.setSelected(true);
      }
      sizeButton.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          POSToggleButton button = (POSToggleButton)e.getSource();
          PizzaPrice pizzaPrice = (PizzaPrice)button.getClientProperty("pizzaPrice");
          renderCrusts(pizzaPrice.getSize());
          
          TicketItemModifier sizeAndCrustModifer = PizzaModifierSelectionDialog.this.getSizeAndCrustModifer();
          if (sizeAndCrustModifer != null) {
            ticketItem.setSizeModifier(sizeAndCrustModifer);
            sizeAndCrustModifer.calculatePrice();
            
            ticketItemViewerModel.updateView();
          }
          
          updateTicketItemModifierPrices();
          modifierView.updateView();
          modifierView.revalidate();
          modifierView.repaint();
          revalidate();
          repaint();
        }
      });
      sizeBtnGroup.add(sizeButton);
      sizeButtonList.add(sizeButton);
      sizePanel.add(sizeButton);
    }
    
    protected void renderCrusts(MenuItemSize size) {
      setMenuItemSize(size);
      for (POSToggleButton component : crustButtonList) {
        crustBtnGroup.remove(component);
      }
      crustPanel.removeAll();
      
      Object availablePrices = menuItem.getAvailablePrices(size);
      TicketItemModifier sizeAndCrustModifer = PizzaModifierSelectionDialog.this.getSizeAndCrustModifer();
      for (final PizzaPrice pizzaPrice : (Set)availablePrices) {
        POSToggleButton crustButton = new POSToggleButton();
        crustButton.setText("<html><center>" + pizzaPrice.getCrust().getName() + "<br/>" + CurrencyUtil.getCurrencySymbol() + pizzaPrice
          .getPrice(menuItem.getDefaultSellPortion().intValue()) + "</center></html>");
        crustButton.putClientProperty("pizzaPrice", pizzaPrice);
        if (((Set)availablePrices).size() == 1) {
          crustSelected = true;
          crustButton.setSelected(true);
          PizzaModifierSelectionDialog.this.pizzaCrustSelected(crustButton);
          setPizzaCrust(pizzaPrice.getCrust());
        }
        if ((pizzaPrice.getCrust().isDefaultCrust().booleanValue()) && (sizeAndCrustModifer == null)) {
          crustSelected = true;
          crustButton.setSelected(true);
          PizzaModifierSelectionDialog.this.pizzaCrustSelected(crustButton);
          setPizzaCrust(pizzaPrice.getCrust());
        }
        if (sizeAndCrustModifer != null) {
          String sizeAndCrustName = sizeAndCrustModifer.getName();
          String[] split = sizeAndCrustName.split(" ");
          String crustName = split[1];
          if (pizzaPrice.getCrust().getName().startsWith(crustName)) {
            crustSelected = true;
            crustButton.setSelected(true);
            PizzaModifierSelectionDialog.this.pizzaCrustSelected(crustButton);
            setPizzaCrust(pizzaPrice.getCrust());
          }
        }
        crustButton.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e) {
            POSToggleButton button = (POSToggleButton)e.getSource();
            PizzaModifierSelectionDialog.this.pizzaCrustSelected(button);
            setPizzaCrust(pizzaPrice.getCrust());
            
            if (PizzaModifierSelectionDialog.this.getSizeAndCrustModifer() != null) {
              ticketItem.setSizeModifier(PizzaModifierSelectionDialog.this.getSizeAndCrustModifer());
              if (ticketItem.getSizeModifier() == null) {
                return;
              }
              ticketItem.getSizeModifier().calculatePrice();
              
              ticketItemViewerModel.updateView();
              
              revalidate();
              repaint();
            }
          }
        });
        crustBtnGroup.add(crustButton);
        crustButtonList.add(crustButton);
        crustPanel.add(crustButton);
      }
      crustPanel.revalidate();
      crustPanel.repaint();
      revalidate();
      repaint();
    }
    
    public MenuItemSize getMenuItemSize() {
      return menuItemSize;
    }
    
    public void setMenuItemSize(MenuItemSize menuItemSize) {
      this.menuItemSize = menuItemSize;
    }
    
    public PizzaCrust getPizzaCrust() {
      return pizzaCrust;
    }
    
    public void setPizzaCrust(PizzaCrust pizzaCrust) {
      this.pizzaCrust = pizzaCrust;
    }
  }
  



  public class ModifierTableCellRenderer
    extends PosTableRenderer
  {
    private boolean inTicketScreen = false;
    


    public ModifierTableCellRenderer() {}
    


    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
      Component rendererComponent = null;
      
      PizzaModifierSelectionDialog.SectionModifierTableModel model = (PizzaModifierSelectionDialog.SectionModifierTableModel)table.getModel();
      Object object = model.getRowData(row);
      if (column == 1) {
        rendererComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      }
      else {
        rendererComponent = super.getTableCellRendererComponent(table, value, isSelected, false, row, column);
        if (column == 0) {
          setHorizontalAlignment(0);
        }
        else {
          setHorizontalAlignment(4);
        }
      }
      if (isSelected) {
        rendererComponent.setBackground(Color.BLUE);
        rendererComponent.setForeground(Color.WHITE);
        return rendererComponent;
      }
      
      rendererComponent.setBackground(table.getBackground());
      rendererComponent.setForeground(Color.BLACK);
      
      if ((object instanceof TicketItemModifier)) {
        ITicketItem ticketItem = (ITicketItem)object;
        if (ticketItem.isPrintedToKitchen().booleanValue()) {
          rendererComponent.setBackground(Color.YELLOW);
          rendererComponent.setForeground(Color.BLACK);
        }
      }
      
      return rendererComponent;
    }
  }
  



















































  private void showModifierSelectionMessage(MenuItemModifierSpec menuItemModifierGroup)
  {
    String displayName = menuItemModifierGroup.getModifierGroup().getDisplayName();
    int minQuantity = menuItemModifierGroup.getMinQuantity().intValue();
    POSMessageDialog.showError("You must select at least " + minQuantity + " modifiers from group " + displayName);
  }
  
  private void pizzaCrustSelected(POSToggleButton button) {
    PizzaPrice pizzaPrice = (PizzaPrice)button.getClientProperty("pizzaPrice");
    
    ticketItem.setUnitPrice(pizzaPrice.getPrice(menuItem.getDefaultSellPortion().intValue()));
    
    TicketItemModifier sizeAndCrustModifer = getSizeAndCrustModifer();
    if (sizeAndCrustModifer != null) {
      sizeAndCrustModifer.setName(pizzaPrice.getSize().getName() + " " + pizzaPrice.getCrust());
      crustModifier = sizeAndCrustModifer;
    }
    else {
      crustModifier = new TicketItemModifier();
      crustModifier.setName(pizzaPrice.getSize().getName() + " " + pizzaPrice.getCrust());
      crustModifier.setModifierType(Integer.valueOf(5));
      crustModifier.setInfoOnly(Boolean.valueOf(true));
      crustModifier.setTicketItem(ticketItem);
    }
    crustSelected = true;
  }
  
  private void doFullSectionMode() {
    wholeSectionView.removeAll();
    fullSectionLayout.removeAll();
    fullSectionLayout.add(sectionWhole);
    sectionLayout.show(sectionView, "full");
    ticketItem.setPizzaSectionMode(TicketItem.PIZZA_SECTION_MODE.FULL);
    tglBtnPanel.setVisible(false);
    
    setTglBtnSelection();
  }
  
  private void allSectionModifierClear() {
    for (Section section : sectionList) {
      if (sectionModifierTableModel.getRows() != null)
      {

        section.clearItems(); }
    }
  }
  
  private void doHalfSectionMode() {
    wholeSectionView.add(sectionWhole, "grow");
    sectionLayout.show(sectionView, "half");
    ticketItem.setPizzaSectionMode(TicketItem.PIZZA_SECTION_MODE.HALF);
    
    tglBtnPanel.setVisible(true);
    
    tglHalf1.setVisible(true);
    tglHalf2.setVisible(true);
    
    tglQrtr1.setVisible(false);
    tglQrtr2.setVisible(false);
    tglQrtr3.setVisible(false);
    tglQrtr4.setVisible(false);
    
    setTglBtnSelection();
    tglWhole.setSelected(true);
  }
  

  private void setTglBtnSelection()
  {
    btnSectionGroup.clearSelection();
    for (Section sec : sectionList) {
      lblTitle.setBackground(Color.lightGray);
      sec.setSelected(false);
      sec.setBorder(null);
    }
    wholeSectionView.setBorder(null);
  }
  
  private void doQuarterSectionMode()
  {
    wholeSectionView.add(sectionWhole, "grow");
    sectionLayout.show(sectionView, "quarter");
    ticketItem.setPizzaSectionMode(TicketItem.PIZZA_SECTION_MODE.QUARTER);
    
    tglBtnPanel.setVisible(true);
    
    tglHalf1.setVisible(false);
    tglHalf2.setVisible(false);
    
    tglQrtr1.setVisible(true);
    tglQrtr2.setVisible(true);
    tglQrtr3.setVisible(true);
    tglQrtr4.setVisible(true);
    
    setTglBtnSelection();
    tglWhole.setSelected(true);
  }
  
  class SectionModifierTableModel extends ListTableModel<TicketItemModifier>
  {
    public SectionModifierTableModel() {
      super();
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      TicketItemModifier item = (TicketItemModifier)rows.get(rowIndex);
      if ((item instanceof TicketItemModifier)) {
        item.calculatePrice();
      }
      
      switch (columnIndex) {
      case 0: 
        if ((item instanceof TicketItemModifier)) {
          return item.getName();
        }
        return " " + item.getNameDisplay();
      
      case 1: 
        Double total = null;
        if ((item instanceof TicketItemModifier)) {
          total = item.getUnitPrice();
          return Double.valueOf(NumberUtil.roundToTwoDigit(total.doubleValue()));
        }
        return null;
      }
      
      
      return null;
    }
    
    public void deleteGivenItem(TicketItemModifier ticketItemModifier) {
      for (Iterator iterator = rows.iterator(); iterator.hasNext();) {
        TicketItemModifier tModifier = (TicketItemModifier)iterator.next();
        
        if (ticketItemModifier == tModifier) {
          iterator.remove();
        }
      }
      
      fireTableDataChanged();
    }
    
    public void deleteGivenItemByName(TicketItemModifier ticketItemModifier) {
      for (Iterator iterator = rows.iterator(); iterator.hasNext();) {
        TicketItemModifier tModifier = (TicketItemModifier)iterator.next();
        
        if (ticketItemModifier.getName().equals(tModifier.getName())) {
          iterator.remove();
        }
      }
      
      fireTableDataChanged();
    }
  }
  

  public void actionPerformed(ActionEvent e)
  {
    if (e.getActionCommand() == "FULL")
    {
      if ((ticketItem.getTicketItemModifiers() != null) && (ticketItem.getTicketItemModifiers().size() > 0)) {
        int questionDialog = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Items of the section will be deleted, Are you sure to change section mode?", "Change Section Mode");
        
        if (questionDialog != 0) {
          currentButton.setSelected(true);
          return;
        }
        allSectionModifierClear();
      }
      doFullSectionMode();
    }
    else if (e.getActionCommand() == "HALF")
    {
      if ((ticketItem.getTicketItemModifiers() != null) && (ticketItem.getTicketItemModifiers().size() > 0)) {
        int questionDialog = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Items of the section will be deleted, Are you sure to change section mode?", "Change Section Mode");
        
        if (questionDialog != 0) {
          currentButton.setSelected(true);
          return;
        }
        allSectionModifierClear();
      }
      doHalfSectionMode();
    }
    else if (e.getActionCommand() == "QUARTER") {
      if ((ticketItem.getTicketItemModifiers() != null) && (ticketItem.getTicketItemModifiers().size() > 0)) {
        int questionDialog = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Items of the section will be deleted, Are you sure to change section mode?", "Change Section Mode");
        
        if (questionDialog != 0) {
          currentButton.setSelected(true);
          return;
        }
        allSectionModifierClear();
      }
      doQuarterSectionMode();
    }
    currentButton = ((JToggleButton)e.getSource());
  }
  
  private void selectSectionTogglebutton(Section section) {
    if (section.getSectionName().equals("WHOLE")) {
      tglWhole.setSelected(true);
    }
    else if (section.getSectionName().equals("Quarter 1")) {
      tglQrtr1.setSelected(true);
    }
    else if (section.getSectionName().equals("Quarter 2")) {
      tglQrtr2.setSelected(true);
    }
    else if (section.getSectionName().equals("Quarter 3")) {
      tglQrtr3.setSelected(true);
    }
    else if (section.getSectionName().equals("Quarter 4")) {
      tglQrtr4.setSelected(true);
    }
    else if (section.getSectionName().equals("Half 1")) {
      tglHalf1.setSelected(true);
    }
    else if (section.getSectionName().equals("Half 2")) {
      tglHalf2.setSelected(true);
    }
  }
  


  public void finishModifierSelection() {}
  

  public void stateChanged(ChangeEvent e)
  {
    JToggleButton toggleButton = (JToggleButton)e.getSource();
    if (toggleButton.getText().equals("WHOLE")) {
      for (Section sec : sectionList) {
        lblTitle.setBackground(Color.lightGray);
        sec.setSelected(false);
      }
    }
    else if (toggleButton.getText().equals("H1")) {
      setSelectedSection(sectionHalf1);
    }
    else if (toggleButton.getText().equals("H2")) {
      setSelectedSection(sectionHalf2);
    }
    else if (toggleButton.getText().equals("Q1")) {
      setSelectedSection(sectionQuarter1);
    }
    else if (toggleButton.getText().equals("Q2")) {
      wholeSectionView.setBorder(null);
      setSelectedSection(sectionQuarter2);
    }
    else if (toggleButton.getText().equals("Q3")) {
      setSelectedSection(sectionQuarter3);
    }
    else if (toggleButton.getText().equals("Q4")) {
      setSelectedSection(sectionQuarter4);
    }
  }
  
  public void updateTicketItemModifierPrices()
  {
    List<TicketItemModifier> ticketItemModifiers = ticketItem.getTicketItemModifiers();
    Iterator iterator; if (ticketItemModifiers != null)
      for (iterator = ticketItemModifiers.iterator(); iterator.hasNext();)
      {
        TicketItemModifier ticketItemModifier = (TicketItemModifier)iterator.next();
        MenuModifier menuModifier = ticketItemModifier.getMenuModifier();
        if (menuModifier != null)
        {

          double priceForSize = menuModifier.getPriceForSizeAndMultiplier(getSelectedSize(), false, modifierView.getSelectedMultiplier());
          ticketItemModifier.setUnitPrice(Double.valueOf(priceForSize));
        }
      }
    updateSectionView();
    if (ticketItem.getSizeModifier() != null) {
      ticketItem.getSizeModifier().calculatePrice();
      ticketItemViewerModel.updateView();
    }
  }
  
  private void updateSectionView() {
    for (Section sec : sectionList) {
      sectionModifierTableModel.fireTableDataChanged();
    }
  }
}
