package com.floreantpos.ui.views.order.multipart;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuItemModifierPage;
import com.floreantpos.model.MenuItemModifierPageItem;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.MenuItemSize;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.Multiplier;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.dao.MenuItemModifierPageDAO;
import com.floreantpos.model.dao.MenuModifierDAO;
import com.floreantpos.model.dao.MultiplierDAO;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PaginatedListModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.SelectionView;
import com.floreantpos.ui.views.order.modifier.ModifierGroupSelectionListener;
import com.floreantpos.ui.views.order.modifier.ModifierGroupView;
import com.floreantpos.ui.views.order.modifier.ModifierSelectionListener;
import com.floreantpos.ui.views.order.modifier.ModifierSelectionModel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.hibernate.Session;































public class PizzaModifierView
  extends SelectionView
  implements ModifierGroupSelectionListener
{
  private ModifierSelectionListener modifierSelectionListener;
  private PosButton btnClear = new PosButton(POSConstants.CLEAR);
  private Multiplier selectedMultiplier;
  private MultiplierButton defaultMultiplierButton;
  private ModifierGroupView modifierGroupView;
  private PizzaModifierSelectionDialog pizzaModifierSelectionDialog;
  private MenuItemModifierSpec menuModifierGroup;
  private TitledBorder titledBorder;
  
  public PizzaModifierView(Ticket ticket, TicketItem ticketItem, MenuItem menuItem, PizzaModifierSelectionDialog pizzaModifierSelectionDialog) {
    super(POSConstants.MODIFIERS, new FlowLayout(), PosUIManager.getSize(120), PosUIManager.getSize(80));
    ModifierSelectionModel modifierSelectionModel = new ModifierSelectionModel(ticket, ticketItem, menuItem);
    this.pizzaModifierSelectionDialog = pizzaModifierSelectionDialog;
    
    setBorder(null);
    titledBorder = new TitledBorder(null, "MODIFIERS", 2, 2);
    selectionButtonsPanel.setBorder(titledBorder);
    modifierGroupView = new ModifierGroupView(modifierSelectionModel);
    
    dataModel = new PaginatedListModel();
    actionButtonPanel.add(btnPrev, "grow,split 2,span");
    actionButtonPanel.add(btnNext, "grow");
    
    add(modifierGroupView, "East");
    
    addMultiplierButtons();
    modifierGroupView.addModifierGroupSelectionListener(this);
    modifierGroupView.selectFirst();
  }
  
  private void addMultiplierButtons() {
    JPanel multiplierPanel = new JPanel(new MigLayout("fillx,center, gap 0, ins 0"));
    LineBorder lineBorder = new LineBorder(Color.lightGray, 1, true);
    CompoundBorder compoundBorder = BorderFactory.createCompoundBorder(new EmptyBorder(5, 0, 5, 0), lineBorder);
    multiplierPanel.setBorder(compoundBorder);
    multiplierPanel.setOpaque(true);
    List<Multiplier> multiplierList = MultiplierDAO.getInstance().findAll();
    ButtonGroup group = new ButtonGroup();
    if (multiplierList != null) {
      for (Multiplier multiplier : multiplierList) {
        MultiplierButton btnMultiplier = new MultiplierButton(multiplier);
        if (multiplier.isDefaultMultiplier().booleanValue()) {
          selectedMultiplier = multiplier;
          defaultMultiplierButton = btnMultiplier;
          btnMultiplier.setSelected(true);
        }
        multiplierPanel.add(btnMultiplier, "grow");
        group.add(btnMultiplier);
      }
    }
    actionButtonPanel.add(multiplierPanel, "newline,span");
  }
  
  protected AbstractButton createItemButton(Object item) {
    Session session = null;
    try
    {
      MenuModifier modifier = (MenuModifier)item;
      
      session = MenuModifierDAO.getInstance().createNewSession();
      session.refresh(modifier);
      
      modifier.setMenuItemModifierGroup(menuModifierGroup);
      ModifierButton modifierButton = new ModifierButton(modifier, selectedMultiplier, pizzaModifierSelectionDialog.getSelectedSize());
      return modifierButton;
    } finally {
      MenuModifierDAO.getInstance().closeSession(session);
    }
  }
  
  public void addModifierSelectionListener(ModifierSelectionListener listener) {
    modifierSelectionListener = listener;
  }
  
  public void removeModifierSelectionListener(ModifierSelectionListener listener) {
    modifierSelectionListener = null;
  }
  
  public void updateView() {
    if (menuModifierGroup == null) {
      return;
    }
    
    renderTitle();
    try {
      dataModel.setPageSize(1);
      dataModel.setCurrentRowIndex(0);
      dataModel.setNumRows(MenuItemModifierPageDAO.getInstance().getRowCount(menuModifierGroup.getId()));
      MenuItemModifierPageDAO.getInstance().loadItems(menuModifierGroup, dataModel);
      setDataModel(dataModel);
    } catch (PosException e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  public void setDataModel(PaginatedListModel items)
  {
    super.setDataModel(items);
    updateButton();
  }
  
  protected void renderItems()
  {
    populateItems();
  }
  
  protected void populateItems() {
    reset();
    if (dataModel.getSize() == 0) {
      return;
    }
    MenuItemModifierPage modifierPage = (MenuItemModifierPage)dataModel.getElementAt(0);
    setButtonSize(new Dimension(modifierPage.getButtonWidth().intValue(), modifierPage.getButtonHeight().intValue()));
    Integer cols = modifierPage.getCols();
    MigLayout migLayout = new MigLayout("center,wrap " + cols);
    if (modifierPage.isFlixibleButtonSize().booleanValue()) {
      migLayout.setLayoutConstraints("fill");
      migLayout.setColumnConstraints("fill,grow");
      migLayout.setRowConstraints("fill,grow");
    }
    selectionButtonsPanel.setLayout(migLayout);
    for (int row = 0; row < modifierPage.getRows().intValue(); row++) {
      for (int col = 0; col < modifierPage.getCols().intValue(); col++) {
        String constraint = String.format("cell %s %s", new Object[] { Integer.valueOf(col), Integer.valueOf(row) });
        if (!modifierPage.isFlixibleButtonSize().booleanValue()) {
          constraint = constraint + String.format(", w %s!, h %s!", new Object[] { modifierPage.getButtonWidth(), modifierPage.getButtonHeight() });
        }
        MenuItemModifierPageItem itemForCell = modifierPage.getItemForCell(col, row);
        if (itemForCell == null) {
          selectionButtonsPanel.add(new JLabel(), constraint);
        }
        else {
          AbstractButton itemButton = createItemButton(itemForCell.getMenuModifier());
          if (itemButton != null) {
            selectionButtonsPanel.add(itemButton, constraint);
          }
        }
      }
    }
    revalidate();
    repaint();
    btnNext.setVisible(dataModel.hasNext());
    btnPrev.setVisible(dataModel.hasPrevious());
  }
  
  private void renderTitle() {
    String displayName = menuModifierGroup.getName();
    String instruction = menuModifierGroup.getInstruction();
    int minQuantity = menuModifierGroup.getMinQuantity().intValue();
    int maxQuantity = menuModifierGroup.getMaxQuantity().intValue();
    titledBorder.setTitle(displayName + Messages.getString("ModifierView.2") + minQuantity + Messages.getString("ModifierView.3") + maxQuantity + (instruction == null ? "" : new StringBuilder().append(" ").append(instruction).toString()));
  }
  
  private class ModifierButton extends PosButton implements ActionListener
  {
    private MenuModifier menuModifier;
    
    public ModifierButton(MenuModifier modifier, Multiplier multiplier, MenuItemSize menuItemSize) {
      menuModifier = modifier;
      
      setText("<html><center>" + modifier.getDisplayName() + "<br/>" + modifier.getPriceForSizeAndMultiplier(menuItemSize, true, multiplier) + "</center></html>");
      
      if (modifier.getButtonColor() != null) {
        setBackground(new Color(modifier.getButtonColor().intValue()));
      }
      
      if (modifier.getTextColor() != null) {
        setForeground(new Color(modifier.getTextColor().intValue()));
      }
      
      setFocusable(true);
      setFocusPainted(true);
      addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e) {
      modifierSelectionListener.modifierSelected(menuModifier, selectedMultiplier);
      
      if (selectedMultiplier == null) {
        selectedMultiplier = defaultMultiplierButton.getMultiplier();
      }
    }
  }
  
  public void setActionButtonsVisible(boolean b) {
    btnClear.setVisible(b);
  }
  
  private class MultiplierButton extends POSToggleButton implements ActionListener {
    private Multiplier multiplier;
    
    public MultiplierButton(Multiplier multiplier) {
      this.multiplier = multiplier;
      setText(multiplier.getId());
      Integer buttonColor = multiplier.getButtonColor();
      if (buttonColor != null) {
        setBackground(new Color(buttonColor.intValue()));
      }
      Integer textColor = multiplier.getTextColor();
      if (textColor != null) {
        setForeground(new Color(textColor.intValue()));
      }
      setBorder(null);
      setBorderPainted(false);
      addActionListener(this);
    }
    
    public Multiplier getMultiplier() {
      return multiplier;
    }
    
    public void actionPerformed(ActionEvent e)
    {
      selectedMultiplier = multiplier;
      updateView();
    }
  }
  
  public void modifierGroupSelected(MenuItemModifierSpec menuModifierGroup)
  {
    if (menuModifierGroup == null) {
      return;
    }
    if ((this.menuModifierGroup != null) && (menuModifierGroup.getId().equals(this.menuModifierGroup.getId())))
      return;
    this.menuModifierGroup = menuModifierGroup;
    updateView();
  }
  
  public ModifierGroupView getModifierGroupView() {
    return modifierGroupView;
  }
  
  public Multiplier getSelectedMultiplier() {
    return selectedMultiplier;
  }
  
  protected void scrollDown()
  {
    dataModel.setCurrentRowIndex(dataModel.getNextRowIndex());
    MenuItemModifierPageDAO.getInstance().loadItems(menuModifierGroup, dataModel);
    setDataModel(dataModel);
  }
  
  protected void scrollUp()
  {
    dataModel.setCurrentRowIndex(dataModel.getPreviousRowIndex());
    MenuItemModifierPageDAO.getInstance().loadItems(menuModifierGroup, dataModel);
    setDataModel(dataModel);
  }
}
