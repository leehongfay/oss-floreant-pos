package com.floreantpos.ui.views.order.multipart;

import com.floreantpos.model.MenuItemSize;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.ModifierMultiplierPrice;
import com.floreantpos.model.Multiplier;
import com.floreantpos.model.PizzaModifierPrice;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class PizzaPriceTableModel extends AbstractTableModel
{
  private List<PizzaModifierPrice> priceList;
  private String[] columnNames;
  private Class[] columnClass;
  
  public PizzaPriceTableModel(List<PizzaModifierPrice> priceList, List<Multiplier> multipliers)
  {
    setPriceList(priceList, multipliers);
  }
  
  public void setPriceList(List<PizzaModifierPrice> priceList, List<Multiplier> multipliers) {
    columnNames = new String[multipliers.size() + 1];
    columnClass = new Class[multipliers.size() + 1];
    columnNames[0] = "Size";
    columnClass[0] = String.class;
    int index = 1;
    for (Multiplier multiplier : multipliers) {
      columnNames[index] = multiplier.getId();
      columnClass[index] = Double.class;
      index++;
    }
    this.priceList = priceList;
    for (PizzaModifierPrice price : priceList) {
      price.initializeSizeAndPriceList(multipliers);
    }
  }
  
  public String getColumnName(int column)
  {
    return columnNames[column];
  }
  
  public Class<?> getColumnClass(int columnIndex)
  {
    if (columnIndex == 0)
      return columnClass[columnIndex];
    return Double.class;
  }
  
  public int getColumnCount()
  {
    return columnNames.length;
  }
  
  public int getRowCount()
  {
    return priceList.size();
  }
  
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    PizzaModifierPrice row = (PizzaModifierPrice)priceList.get(rowIndex);
    if (row == null)
      return null;
    if (0 == columnIndex) {
      if ((row != null) && (row.getSize() != null)) {
        return row.getSize().getName();
      }
    } else {
      ModifierMultiplierPrice price = row.getMultiplier(columnNames[columnIndex]);
      if (price == null)
        return null;
      return price.getPrice();
    }
    return "";
  }
  
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    if (columnIndex == 0)
      return false;
    return true;
  }
  
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    PizzaModifierPrice row = (PizzaModifierPrice)priceList.get(rowIndex);
    if (columnIndex == 0) {
      super.setValueAt(aValue, rowIndex, columnIndex);
    }
    else {
      String priceString = (String)aValue;
      if (priceString.isEmpty())
        return;
      double priceValue = Double.parseDouble(priceString);
      ModifierMultiplierPrice price = row.getMultiplier(columnNames[columnIndex]);
      price.setPrice(Double.valueOf(priceValue));
    }
  }
  
  public List<PizzaModifierPrice> getRows(MenuModifier modifier) {
    for (PizzaModifierPrice pizzaModifierPrice : priceList) {
      pizzaModifierPrice.populateMultiplierPriceListRowValue(modifier);
    }
    return priceList;
  }
}
