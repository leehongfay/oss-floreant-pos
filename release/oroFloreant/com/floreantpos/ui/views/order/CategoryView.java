package com.floreantpos.ui.views.order;

import com.floreantpos.POSConstants;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PaginatedListModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.views.order.actions.CategorySelectionListener;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;

























public class CategoryView
  extends SelectionView
  implements ActionListener
{
  private Vector<CategorySelectionListener> listenerList = new Vector();
  
  private ButtonGroup categoryButtonGroup;
  
  public static final String VIEW_NAME = "CATEGORY_VIEW";
  

  public CategoryView()
  {
    super(POSConstants.CATEGORIES, new GridLayout(0, 1, 5, 5), 100, 100);
    dataModel = new PaginatedListModel(5);
    categoryButtonGroup = new ButtonGroup();
    setPreferredSize(new Dimension(PosUIManager.getSize(120, 100)));
  }
  
  public void initialize() {
    if (isInitialized()) {}
    


    OrderType orderType = OrderView.getInstance().getCurrentTicket().getOrderType();
    
    MenuCategoryDAO categoryDAO = MenuCategoryDAO.getInstance();
    dataModel.setCurrentRowIndex(0);
    categoryDAO.findActiveCategories(dataModel, orderType);
    
    setDataModel(dataModel);
    
    if (dataModel.getSize() == 0) {
      return;
    }
    setInitialized(true);
    CategoryButton categoryButton = (CategoryButton)getFirstItemButton();
    if (categoryButton != null) {
      categoryButton.setSelected(true);
      fireCategorySelected(foodCategory);
    }
    
    if (dataModel.getSize() <= 1) {
      setVisible(false);
    }
    else {
      setVisible(true);
    }
  }
  
  protected AbstractButton createItemButton(Object item)
  {
    MenuCategory menuCategory = (MenuCategory)item;
    CategoryButton button = new CategoryButton(this, menuCategory);
    categoryButtonGroup.add(button);
    


    return button;
  }
  
  public void addCategorySelectionListener(CategorySelectionListener listener) {
    listenerList.add(listener);
  }
  
  public void removeCategorySelectionListener(CategorySelectionListener listener) {
    listenerList.remove(listener);
  }
  
  private void fireCategorySelected(MenuCategory foodCategory) {
    for (CategorySelectionListener listener : listenerList) {
      listener.categorySelected(foodCategory);
    }
  }
  


  private static class CategoryButton
    extends POSToggleButton
  {
    MenuCategory foodCategory;
    


    CategoryButton(CategoryView view, MenuCategory menuCategory)
    {
      foodCategory = menuCategory;
      setText("<html><body><center>" + menuCategory.getDisplayName() + "</center></body></html>");
      
      if (menuCategory.getButtonColor() != null) {
        setBackground(menuCategory.getButtonColor());
      }
      if (menuCategory.getTextColor() != null) {
        setForeground(menuCategory.getTextColor());
      }
      
      addActionListener(view);
    }
  }
  
  public void actionPerformed(ActionEvent e) {
    CategoryButton button = (CategoryButton)e.getSource();
    if (button.isSelected()) {
      fireCategorySelected(foodCategory);
    }
  }
  
  protected void scrollDown()
  {
    dataModel.setCurrentRowIndex(dataModel.getNextRowIndex());
    MenuCategoryDAO.getInstance().findActiveCategories(dataModel, OrderView.getInstance().getCurrentTicket().getOrderType());
    setDataModel(dataModel);
  }
  
  protected void scrollUp()
  {
    dataModel.setCurrentRowIndex(dataModel.getPreviousRowIndex());
    MenuCategoryDAO.getInstance().findActiveCategories(dataModel, OrderView.getInstance().getCurrentTicket().getOrderType());
    setDataModel(dataModel);
  }
}
