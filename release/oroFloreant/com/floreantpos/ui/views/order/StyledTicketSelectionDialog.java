package com.floreantpos.ui.views.order;

import com.floreantpos.Messages;
import com.floreantpos.PosException;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.ScrollableFlowPanel;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.TicketInfoButton;
import com.floreantpos.util.POSUtil;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;



























public class StyledTicketSelectionDialog
  extends OkCancelOptionDialog
  implements MouseListener
{
  private ScrollableFlowPanel buttonsPanel;
  private Map<String, Ticket> addedTicketListModel = new HashMap();
  private POSToggleButton btnSelectAll;
  private boolean isHasDiscounts;
  private boolean isMerge;
  private boolean allowPartialPayment;
  private Integer requiredNumber;
  
  public StyledTicketSelectionDialog() {
    this(false);
  }
  
  public StyledTicketSelectionDialog(boolean allowPartialPayment) {
    super(Application.getPosWindow());
    this.allowPartialPayment = allowPartialPayment;
    initComponent();
    initData();
  }
  
  public StyledTicketSelectionDialog(List<Ticket> tickets) {
    initComponent();
    initData(tickets);
    setResizable(true);
  }
  
  private void initComponent() {
    setOkButtonText(Messages.getString("TicketSelectionDialog.3"));
    buttonsPanel = new ScrollableFlowPanel(3);
    
    btnSelectAll = new POSToggleButton("SELECT ALL");
    btnSelectAll.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        addedTicketListModel.clear();
        for (Component c : buttonsPanel.getContentPane().getComponents()) {
          TicketInfoButton button = (TicketInfoButton)c;
          if (btnSelectAll.isSelected()) {
            button.setSelected(true);
            button.setBorder(BorderFactory.createLineBorder(Color.blue, 4));
            addedTicketListModel.put(button.getTicket().getId(), button.getTicket());
          }
          else {
            button.setSelected(false);
            button.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
          }
        }
      }
    });
    getButtonPanel().add(btnSelectAll, 0);
    
    JScrollPane scrollPane = new PosScrollPane(buttonsPanel, 20, 31);
    scrollPane.getVerticalScrollBar().setPreferredSize(PosUIManager.getSize(40, 0));
    
    getContentPanel().add(scrollPane, "Center");
    setSize(1024, 600);
  }
  
  private void initData() {
    setCaption("Select tickets");
    initData(null);
  }
  
  private void initData(List<Ticket> tickets) {
    try {
      if ((tickets == null) || (tickets.size() <= 0)) {
        User user = Application.getCurrentUser();
        if (!user.hasPermission(UserPermission.EDIT_OTHER_USERS_TICKETS)) {
          tickets = TicketDAO.getInstance().findOpenTicketsForUser(user);
        }
        else {
          tickets = TicketDAO.getInstance().findOpenTickets();
        }
      }
      size = PosUIManager.getSize(150, 150);
      for (Ticket ticket : tickets) {
        ticket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
        if (((ticket.getDueAmount().doubleValue() > 0.0D) && (allowPartialPayment)) || 
          (ticket.getDueAmount().equals(ticket.getTotalAmountWithTips())))
        {


          TicketInfoButton btnTicket = new TicketInfoButton(ticket);
          btnTicket.addMouseListener(this);
          buttonsPanel.add(btnTicket);
          btnTicket.setPreferredSize(size);
          
          if (ticket.getDiscounts().size() > 0)
            isHasDiscounts = true;
        }
      }
    } catch (PosException e) { Dimension size;
      POSMessageDialog.showError(this, e.getLocalizedMessage(), e);
    }
  }
  
  public void doOk()
  {
    if ((addedTicketListModel.isEmpty()) && (requiredNumber == null)) {
      POSMessageDialog.showMessage(Messages.getString("TicketSelectionDialog.5"));
      return;
    }
    if ((requiredNumber != null) && (addedTicketListModel.size() < requiredNumber.intValue())) {
      POSMessageDialog.showMessage("Please select at least " + requiredNumber + " tickets.");
      return;
    }
    
    if (isHasDiscounts) {
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "All discounts will be removed from selected tickets.\nDo you want to proceed?", "Merge Tickets");
      

      if (option != 0) {
        setCanceled(true);
      }
    }
    setCanceled(false);
    dispose();
  }
  
  public void doCancel() {
    addedTicketListModel.clear();
    setCanceled(true);
    dispose();
  }
  
  public List<Ticket> getSelectedTickets() {
    return new ArrayList(addedTicketListModel.values());
  }
  
  public Ticket getMainTicket() {
    return (Ticket)addedTicketListModel.get("root");
  }
  


  public void mouseClicked(MouseEvent e) {}
  

  public void mousePressed(MouseEvent e)
  {
    TicketInfoButton button = (TicketInfoButton)e.getSource();
    if (isMerge) {
      setSelectedSectionForMerge(button);
    }
    else {
      setSelectedSection(button);
    }
  }
  


  public void mouseReleased(MouseEvent e) {}
  


  public void mouseEntered(MouseEvent e) {}
  


  public void mouseExited(MouseEvent e) {}
  


  public void setSelectedSectionForMerge(TicketInfoButton section)
  {
    for (Component c : buttonsPanel.getContentPane().getComponents()) {
      TicketInfoButton sec = (TicketInfoButton)c;
      if (sec == section) {
        if (sec.isSelected()) {
          if ((addedTicketListModel.get("root") == sec.getTicket()) && (addedTicketListModel.values().size() > 1)) {
            POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Root ticket cannot be removed until child empty!");
          }
          else
          {
            if (addedTicketListModel.get("root") == sec.getTicket()) {
              addedTicketListModel.remove("root");
            }
            else {
              addedTicketListModel.remove(sec.getTicket().getId());
            }
            sec.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
            sec.setSelected(false);
          }
        }
        else
        {
          if (addedTicketListModel.isEmpty()) {
            sec.setBorder(BorderFactory.createLineBorder(Color.GREEN, 4));
            addedTicketListModel.put("root", sec.getTicket());
          }
          else {
            sec.setBorder(BorderFactory.createLineBorder(Color.BLUE, 4));
            addedTicketListModel.put(sec.getTicket().getId(), sec.getTicket());
          }
          sec.setSelected(true);
        }
      }
    }
  }
  

  public void setSelectedSection(TicketInfoButton btn)
  {
    for (Component c : buttonsPanel.getContentPane().getComponents()) {
      TicketInfoButton button = (TicketInfoButton)c;
      if (button == btn) {
        setColorOfBtn(button);
      }
    }
  }
  
  private void setColorOfBtn(TicketInfoButton button) {
    if (button.isSelected()) {
      button.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
      button.setSelected(false);
      addedTicketListModel.remove(button.getTicket().getId());
    }
    else {
      button.setBorder(BorderFactory.createLineBorder(Color.blue, 4));
      button.setSelected(true);
      addedTicketListModel.put(button.getTicket().getId(), button.getTicket());
    }
  }
  
  public TicketInfoButton getSelectedSection() {
    for (Component c : buttonsPanel.getComponents()) {
      TicketInfoButton button = (TicketInfoButton)c;
      if (button.isSelected()) {
        return button;
      }
    }
    return null;
  }
  
  public void setMerge(boolean isMerge) {
    this.isMerge = isMerge;
    if (isMerge) {
      setCaption("Select tickets to merge");
      btnSelectAll.setVisible(false);
    }
  }
  
  public int getRequiredNumber() {
    return requiredNumber.intValue();
  }
  
  public void setRequiredNumber(int requiredNumber) {
    this.requiredNumber = Integer.valueOf(requiredNumber);
  }
}
