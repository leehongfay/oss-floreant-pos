package com.floreantpos.ui.views.order;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.main.Application;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.ScrollableFlowPanel;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.CurrencyUtil;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;




























public class TicketSelectionDialog
  extends OkCancelOptionDialog
{
  private ScrollableFlowPanel buttonsPanel;
  private List<Ticket> addedTicketListModel = new ArrayList();
  private POSToggleButton btnSelectAll;
  
  public TicketSelectionDialog() {
    super(Application.getPosWindow(), Messages.getString("TicketSelectionDialog.0"));
    initComponent();
    initData();
  }
  
  public TicketSelectionDialog(List<Ticket> tickets) {
    initComponent();
    rendererTickets(tickets);
    setResizable(true);
  }
  
  private void initComponent() {
    setOkButtonText(Messages.getString("TicketSelectionDialog.3"));
    buttonsPanel = new ScrollableFlowPanel(3);
    
    btnSelectAll = new POSToggleButton("SELECT ALL");
    btnSelectAll.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        addedTicketListModel.clear();
        for (Component c : buttonsPanel.getContentPane().getComponents()) {
          TicketSelectionDialog.TicketButton button = (TicketSelectionDialog.TicketButton)c;
          if (btnSelectAll.isSelected()) {
            button.setSelected(true);
            addedTicketListModel.add(button.getTicket());
          }
          else {
            button.setSelected(false);
          }
        }
      }
    });
    getButtonPanel().add(btnSelectAll, 0);
    
    JScrollPane scrollPane = new PosScrollPane(buttonsPanel, 20, 31);
    scrollPane.getVerticalScrollBar().setPreferredSize(PosUIManager.getSize(40, 0));
    
    getContentPanel().add(scrollPane, "Center");
    setSize(1024, 600);
  }
  
  private void initData() {
    TicketDAO dao = new TicketDAO();
    try {
      List<Ticket> tickets = dao.getTicketsWithSpecificFields(new String[] { Ticket.PROP_ID, Ticket.PROP_DUE_AMOUNT, Ticket.PROP_TOKEN_NO });
      size = PosUIManager.getSize(130, 100);
      for (Ticket ticket : tickets)
        if (ticket.getDueAmount().doubleValue() > 0.0D)
        {

          TicketButton btnTicket = new TicketButton(ticket);
          buttonsPanel.add(btnTicket);
          btnTicket.setPreferredSize(size);
        }
    } catch (PosException e) { Dimension size;
      POSMessageDialog.showError(this, e.getLocalizedMessage(), e);
    }
  }
  
  private void rendererTickets(List<Ticket> tickets) {
    try {
      for (Ticket ticket : tickets) {
        buttonsPanel.add(new TicketButton(ticket));
      }
    } catch (PosException e) {
      POSMessageDialog.showError(this, e.getLocalizedMessage(), e);
    }
  }
  
  public void doOk()
  {
    if (addedTicketListModel.isEmpty()) {
      POSMessageDialog.showMessage(Messages.getString("TicketSelectionDialog.5"));
      return;
    }
    setCanceled(false);
    dispose();
  }
  
  public void doCancel() {
    addedTicketListModel.clear();
    setCanceled(true);
    dispose();
  }
  
  public List<Ticket> getSelectedTickets() {
    return addedTicketListModel;
  }
  
  private class TicketButton extends POSToggleButton implements ActionListener {
    private Ticket ticket;
    
    TicketButton(Ticket ticket) {
      this.ticket = ticket;
      Terminal terminal = Application.getInstance().getTerminal();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("<html><body><center>");
      
      if (!terminal.isShowTableNumber()) {
        stringBuilder.append(
          String.format("<h3 style='margin:0px; font-size: %spx;'>%s</h3>", new Object[] {Integer.valueOf(PosUIManager.getSize(11)), "TABLE# " + ticket.getTableNames() }));

      }
      else if ((ticket.getTableNumbers() != null) && (ticket.getTableNumbers().size() > 0)) {
        String tabNum = "";
        int count = 0;
        for (Integer tableNumber : ticket.getTableNumbers()) {
          tabNum = tabNum + tableNumber;
          if (count < ticket.getTableNumbers().size() - 1) {
            tabNum = tabNum + ", ";
          }
          count++;
        }
        stringBuilder.append(String.format("<h3 style='margin:0px; font-size: %spx;'>%s</h3>", new Object[] { Integer.valueOf(PosUIManager.getSize(11)), "TABLE# " + tabNum }));
      }
      

      stringBuilder.append(String.format("<h3 style='margin:0px; font-size: %spx;'>%s</h3>", new Object[] { Integer.valueOf(PosUIManager.getSize(11)), ticket.getNumberToDisplay() }));
      stringBuilder.append(String.format("<h4 style='margin:0px; font-size: %spx;'>%s</h4>", new Object[] { Integer.valueOf(PosUIManager.getSize(10)), POSConstants.DUE + ": " + 
        CurrencyUtil.getCurrencySymbol() + ticket.getDueAmount() }));
      stringBuilder.append("</center></body></html>");
      setText(stringBuilder.toString());
      setPreferredSize(PosUIManager.getSize(130, 100));
      addActionListener(this);
    }
    
    public Ticket getTicket() {
      return ticket;
    }
    
    public void actionPerformed(ActionEvent e) {
      if (isSelected()) {
        addedTicketListModel.add(ticket);
      }
      else {
        addedTicketListModel.remove(ticket);
      }
    }
  }
}
