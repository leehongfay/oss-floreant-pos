package com.floreantpos.ui.views;

import com.floreantpos.main.Application;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.VoidItem;
import com.floreantpos.model.VoidReason;
import com.floreantpos.model.dao.VoidReasonDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.DoubleDocument;
import com.floreantpos.swing.IntegerDocument;
import com.floreantpos.swing.POSComboBox;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.QwertyKeyPad;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.NotesDialog;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;


public class VoidItemFormDialog
  extends OkCancelOptionDialog
{
  private POSTextField txtVoidItemUnit = new POSTextField(10);
  private TicketItem ticketItem;
  private VoidItem voidItem;
  private POSComboBox cbVoidItemReason;
  private POSToggleButton tbItemWasted;
  private VoidReason voidReason;
  private PosButton addBtn;
  private PosButton decBtn;
  private ComboBoxModel comboBoxModel;
  
  public VoidItemFormDialog(TicketItem ticketItem)
  {
    super(POSUtil.getFocusedWindow(), "VOID ITEM");
    this.ticketItem = ticketItem;
    voidItem = new VoidItem();
    inItComponents();
    
    updateView();
  }
  
  public void inItComponents() {
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Void " + ticketItem.getName());
    add(titlePanel, "North");
    JPanel centerPanel = new JPanel(new MigLayout("fillx,center", "[][]", ""));
    JPanel unitPanel = new JPanel(new BorderLayout(7, 0));
    JLabel lblNumberOfCard = new JLabel("How Many Unit :");
    addBtn = new PosButton("+");
    decBtn = new PosButton("-");
    if (ticketItem.isFractionalUnit().booleanValue()) {
      txtVoidItemUnit.setDocument(new DoubleDocument());
    }
    else {
      txtVoidItemUnit.setDocument(new IntegerDocument());
    }
    unitPanel.add(decBtn, "West");
    unitPanel.add(txtVoidItemUnit, "Center");
    unitPanel.add(addBtn, "East");
    
    JLabel lblHolder = new JLabel("Void Reason :");
    JPanel reasonPanel = new JPanel(new BorderLayout(5, 0));
    
    comboBoxModel = new ComboBoxModel();
    
    cbVoidItemReason = new POSComboBox();
    
    cbVoidItemReason.setModel(comboBoxModel);
    
    cbVoidItemReason.setSelectedIndex(-1);
    PosButton addReasonBtn = new PosButton("+");
    reasonPanel.add(cbVoidItemReason, "Center");
    reasonPanel.add(addReasonBtn, "East");
    
    addReasonBtn.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        VoidItemFormDialog.this.doAddNewVoidReason();
      }
      
    });
    centerPanel.add(lblNumberOfCard, "");
    centerPanel.add(unitPanel);
    tbItemWasted = new POSToggleButton("Item Wasted");
    centerPanel.add(tbItemWasted, "right, wrap");
    centerPanel.add(lblHolder, "alignx right");
    centerPanel.add(reasonPanel, "growx,span");
    
    add(centerPanel, "Center");
    QwertyKeyPad keyBoard = new QwertyKeyPad();
    centerPanel.add(keyBoard, "gaptop 50, newline,span");
    buttonAction();
  }
  




  private void updateView()
  {
    comboBoxModel.setDataList(VoidReasonDAO.getInstance().findAll());
    if (cbVoidItemReason.getMaximumRowCount() > 5) {
      cbVoidItemReason.setMaximumRowCount(5);
    }
    
    txtVoidItemUnit.setText(ticketItem.getItemQuantityDisplay());
  }
  
  private boolean updateModel()
  {
    double quantity = 0.0D;
    try {
      quantity = Double.parseDouble(txtVoidItemUnit.getText());
    } catch (Exception e2) {
      e2.printStackTrace();
    }
    boolean reason = tbItemWasted.isSelected();
    voidReason = ((VoidReason)cbVoidItemReason.getSelectedItem());
    




    if (voidReason != null) {
      voidItem.setVoidReason(voidReason.getReasonText());
    }
    voidItem.setMenuItemId(ticketItem.getMenuItemId());
    voidItem.setMenuItemName(ticketItem.getName());
    voidItem.setUnitPrice(ticketItem.getUnitPrice());
    voidItem.setQuantity(Double.valueOf(quantity));
    voidItem.setItemWasted(Boolean.valueOf(reason));
    voidItem.setVoidDate(new Date());
    voidItem.setModifier(Boolean.valueOf(false));
    voidItem.setPrinterGroup(ticketItem.getPrinterGroup());
    voidItem.setVoidByUser(Application.getCurrentUser());
    voidItem.setTerminal(Application.getInstance().getTerminal());
    double subtotal = ticketItem.getUnitPrice().doubleValue() * voidItem.getQuantity().doubleValue();
    double taxAmount = subtotal * (ticketItem.getTotalTaxRate() / 100.0D);
    voidItem.setTaxAmount(Double.valueOf(taxAmount));
    voidItem.setTotalPrice(Double.valueOf(subtotal + taxAmount));
    return true;
  }
  
  public VoidItem getVoidItem() {
    return voidItem;
  }
  
  public void doOk()
  {
    updateModel();
    setCanceled(false);
    dispose();
  }
  
  private void doAddNewVoidReason()
  {
    try
    {
      NotesDialog noteDialogue = new NotesDialog();
      noteDialogue.setTitle("Add Void Reason");
      noteDialogue.open();
      if (noteDialogue.isCanceled()) {
        return;
      }
      String note = noteDialogue.getNote();
      
      for (Iterator iterator = comboBoxModel.getDataList().iterator(); iterator.hasNext();) {
        VoidReason voidReason = (VoidReason)iterator.next();
        if (voidReason.getReasonText().equalsIgnoreCase(note)) {
          comboBoxModel.setSelectedItem(voidReason);
          return;
        }
      }
      

      this.voidReason = new VoidReason();
      this.voidReason.setReasonText(note);
      VoidReasonDAO.getInstance().saveOrUpdate(this.voidReason);
      comboBoxModel.addElement(this.voidReason);
      comboBoxModel.setSelectedItem(this.voidReason);
    }
    catch (Exception e) {
      POSMessageDialog.showError(this, "Error while creating void reason.", e);
    }
  }
  
  public void buttonAction() {
    try {
      decBtn.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          double value = 0.0D;
          try {
            value = Double.parseDouble(txtVoidItemUnit.getText());
          }
          catch (Exception localException) {}
          value -= 1.0D;
          if (value < 0.0D) {
            JOptionPane.showMessageDialog(null, "Can Not be less than zero");
          } else
            txtVoidItemUnit.setText(NumberUtil.trimDecilamIfNotNeeded(Double.valueOf(value)));
        }
      });
      addBtn.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          double value = 0.0D;
          try {
            value = Double.parseDouble(txtVoidItemUnit.getText());
          }
          catch (Exception localException) {}
          value += 1.0D;
          txtVoidItemUnit.setText(NumberUtil.trimDecilamIfNotNeeded(Double.valueOf(value)));
        }
      });
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }
}
