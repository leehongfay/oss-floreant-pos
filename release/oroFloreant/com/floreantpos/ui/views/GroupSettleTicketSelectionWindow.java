package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PaymentStatusFilter;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.OrderTypeDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.POSDialog;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.table.JTableHeader;
import org.jdesktop.swingx.JXTable;


















public class GroupSettleTicketSelectionWindow
  extends POSDialog
{
  private JXTable ticketList;
  private BeanTableModel<Ticket> tableModel;
  
  public GroupSettleTicketSelectionWindow()
  {
    super(Application.getPosWindow(), true);
    setTitle(Messages.getString("GroupSettleTicketSelectionWindow.0"));
    
    createTicketList();
    
    JPanel contentPanel = new JPanel(new BorderLayout(5, 5));
    contentPanel.add(new PosScrollPane(ticketList, 20, 31));
    
    JPanel buttonPanel = new JPanel();
    
    PosButton btnConfirm = new PosButton(Messages.getString("GroupSettleTicketSelectionWindow.1"));
    btnConfirm.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setCanceled(false);
        dispose();
      }
    });
    buttonPanel.add(btnConfirm);
    
    PosButton psbtnCancel = new PosButton(Messages.getString("GroupSettleTicketSelectionWindow.2"));
    psbtnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
    });
    buttonPanel.add(psbtnCancel);
    
    contentPanel.add(buttonPanel, "South");
    add(contentPanel);
    
    setSize(800, 600);
  }
  
  private void createTicketList() {
    tableModel = new BeanTableModel(Ticket.class);
    tableModel.addColumn(POSConstants.TICKET_LIST_COLUMN_ID, "id");
    tableModel.addColumn(POSConstants.TICKET_LIST_COLUMN_TICKET_TYPE, "ticketType");
    tableModel.addColumn(POSConstants.TICKET_LIST_COLUMN_SERVER, "owner");
    tableModel.addColumn(POSConstants.TICKET_LIST_COLUMN_TOTAL, "totalAmount");
    tableModel.addColumn(POSConstants.TICKET_LIST_COLUMN_DUE, "dueAmount");
    
    ticketList = new JXTable(tableModel);
    ticketList.setSortable(true);
    ticketList.setColumnControlVisible(true);
    ticketList.setRowHeight(60);
    ticketList.setAutoResizeMode(3);
    ticketList.setDefaultRenderer(Object.class, new PosTableRenderer());
    ticketList.setGridColor(Color.LIGHT_GRAY);
    ticketList.getTableHeader().setPreferredSize(new Dimension(100, 40));
    
    User user = Application.getCurrentUser();
    
    TicketDAO dao = TicketDAO.getInstance();
    List<Ticket> tickets = null;
    
    PaymentStatusFilter paymentStatusFilter = TerminalConfig.getPaymentStatusFilter();
    String orderTypeFilter = TerminalConfig.getOrderTypeFilter();
    OrderType orderType = null;
    if (!"ALL".equals(orderTypeFilter)) {
      orderType = OrderTypeDAO.getInstance().findByName(orderTypeFilter);
    }
    
    if (user.canViewAllOpenTickets()) {
      tickets = dao.findTickets(paymentStatusFilter, orderType);
    }
    else {
      tickets = dao.findTicketsForUser(paymentStatusFilter, orderType, user);
    }
    
    tableModel.addRows(tickets);
  }
}
