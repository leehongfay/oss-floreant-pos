package com.floreantpos.ui.views;

import com.floreantpos.Messages;
import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.model.dao.GiftCardDAO;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;




public class GiftCardBatchNumberEntryDialog
  extends POSDialog
{
  private FixedLengthTextField txtBatchNumber;
  private boolean isActive;
  private boolean isActiveDate;
  private String batchNumber;
  
  public String getBatchNumber()
  {
    return batchNumber;
  }
  
  public boolean isActiveDate() {
    return isActiveDate;
  }
  
  public boolean isActive() {
    return isActive;
  }
  
  public GiftCardBatchNumberEntryDialog() {
    super(POSUtil.getBackOfficeWindow(), "");
    init();
  }
  

  public GiftCardBatchNumberEntryDialog(JFrame parent)
  {
    init();
  }
  
  private void init() {
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Enter Batch Number");
    add(titlePanel, "North");
    
    JPanel centerPanel = new JPanel(new MigLayout("fillx,aligny center", "[]20px[]", ""));
    
    JLabel lblNumberOfCard = new JLabel(Messages.getString("GiftCardBatchNumberEntryDialog.4"));
    
    txtBatchNumber = new FixedLengthTextField(20);
    txtBatchNumber.setLength(32);
    
    centerPanel.add(lblNumberOfCard, "cell 0 0, alignx right");
    centerPanel.add(txtBatchNumber, "cell 1 0");
    
    add(centerPanel, "Center");
    
    JPanel buttonPanel = new JPanel(new MigLayout("al center", "sg, fill", ""));
    
    PosButton btnGenerate = new PosButton(Messages.getString("GiftCardBatchNumberEntryDialog.10"));
    buttonPanel.add(btnGenerate, "grow");
    
    btnGenerate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doGenerate();
      }
      

    });
    buttonPanel.add(new PosButton(new CloseDialogAction(this, "CANCEL")));
    
    add(buttonPanel, "South");
  }
  
  public void doGenerate()
  {
    String cardBatchNumber = txtBatchNumber.getText();
    if (StringUtils.isEmpty(cardBatchNumber)) {
      MessageDialog.showError(Messages.getString("GiftCardBatchNumberEntryDialog.13"));
      return;
    }
    boolean findActiveCardByBatchNumber = GiftCardDAO.getInstance().findActiveCardByBatchNumber(cardBatchNumber);
    
    if (findActiveCardByBatchNumber) {
      isActive = true;
    }
    else {
      isActive = false;
    }
    batchNumber = cardBatchNumber;
    setCanceled(false);
    dispose();
  }
}
