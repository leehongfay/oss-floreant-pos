package com.floreantpos.ui.views;

import com.floreantpos.model.ImageResource;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JToggleButton;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang3.StringUtils;



public class ImagePanel
  extends TransparentPanel
  implements ItemListener
{
  private ImageResource imgResource;
  private ButtonGroup btnGroup;
  private JToggleButton btnImage;
  private ActionListener listener;
  
  public ImagePanel() {}
  
  public ImagePanel(ActionListener listener, ImageResource imgResource, ButtonGroup btnGroup)
  {
    this.imgResource = imgResource;
    this.btnGroup = btnGroup;
    this.listener = listener;
    initComponents();
  }
  
  public void initComponents() {
    setLayout(new MigLayout("fill,center,wrap 1", "", ""));
    
    btnImage = new JToggleButton();
    btnImage.setBorder(new EmptyBorder(3, 3, 3, 3));
    btnImage.putClientProperty("selected", imgResource);
    btnImage.setIcon(imgResource.getScaledImage(PosUIManager.getSize(150), PosUIManager.getSize(150)));
    btnImage.setBorderPainted(true);
    btnImage.addItemListener(this);
    btnImage.addActionListener(listener);
    

    JLabel lblDescription = new JLabel(!StringUtils.isEmpty(imgResource.getDescription()) ? "<html><body><center>" + imgResource.getDescription() + "</center></body></html>" : "  ");
    lblDescription.setHorizontalAlignment(0);
    lblDescription.setPreferredSize(new Dimension(100, 20));
    
    btnGroup.add(btnImage);
    add(btnImage, "growx,wrap");
    add(lblDescription, "center,growx");
  }
  



  public void itemStateChanged(ItemEvent e)
  {
    if (btnImage.isSelected()) {
      btnImage.setBorder(BorderFactory.createLineBorder(Color.BLUE, 3));
    }
    else {
      btnImage.setBorder(new EmptyBorder(3, 3, 3, 3));
    }
  }
}
