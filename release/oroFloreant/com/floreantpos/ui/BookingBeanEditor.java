package com.floreantpos.ui;

public abstract class BookingBeanEditor<E>
  extends BeanEditor
{
  public BookingBeanEditor() {}
  
  public abstract void bookingStatus(String paramString);
}
