package com.floreantpos.ui;

import com.floreantpos.ITicketList;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PaymentStatusFilter;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.ui.order.TicketListView;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXCollapsiblePane;






















public class OrderFilterPanel
  extends JXCollapsiblePane
{
  private ITicketList ticketList;
  private TicketListView ticketLists;
  private POSToggleButton btnFilterByOpenStatus;
  private POSToggleButton btnFilterByUnPaidStatus;
  
  public OrderFilterPanel(ITicketList ticketList)
  {
    this.ticketList = ticketList;
    ticketLists = ((TicketListView)ticketList);
    
    setCollapsed(true);
    getContentPane().setLayout(new MigLayout("fill", "fill, grow", ""));
    
    createPaymentStatusFilterPanel();
    createOrderTypeFilterPanel();
    createMyTicketPanel();
  }
  















































  private void createPaymentStatusFilterPanel()
  {
    btnFilterByOpenStatus = new POSToggleButton(PaymentStatusFilter.OPEN.getDisplayString());
    btnFilterByOpenStatus.setActionCommand(PaymentStatusFilter.OPEN.name());
    

    btnFilterByUnPaidStatus = new POSToggleButton(PaymentStatusFilter.CLOSED.getDisplayString());
    btnFilterByUnPaidStatus.setActionCommand(PaymentStatusFilter.CLOSED.name());
    
    ButtonGroup paymentGroup = new ButtonGroup();
    paymentGroup.add(btnFilterByOpenStatus);
    
    paymentGroup.add(btnFilterByUnPaidStatus);
    
    PaymentStatusFilter paymentStatusFilter = TerminalConfig.getPaymentStatusFilter();
    
    switch (3.$SwitchMap$com$floreantpos$model$PaymentStatusFilter[paymentStatusFilter.ordinal()]) {
    case 1: 
      btnFilterByOpenStatus.setSelected(true);
      break;
    
    case 2: 
      break;
    

    case 3: 
      btnFilterByUnPaidStatus.setSelected(true);
    }
    
    

    ActionListener psFilterHandler = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        String actionCommand = e.getActionCommand();
        
        String filter = actionCommand.replaceAll("\\s", "_");
        
        TerminalConfig.setPaymentStatusFilter(filter);
        
        OrderFilterPanel.this.doUpdateTicketList();
      }
      

    };
    btnFilterByOpenStatus.addActionListener(psFilterHandler);
    
    btnFilterByUnPaidStatus.addActionListener(psFilterHandler);
    
    JPanel filterByPaymentStatusPanel = new JPanel(new MigLayout("", "fill, grow", ""));
    filterByPaymentStatusPanel.setBorder(new TitledBorder(Messages.getString("SwitchboardView.3")));
    filterByPaymentStatusPanel.add(btnFilterByOpenStatus);
    
    filterByPaymentStatusPanel.add(btnFilterByUnPaidStatus);
    
    getContentPane().add(filterByPaymentStatusPanel);
  }
  
  private void createOrderTypeFilterPanel() {
    OrderTypeFilterButton btnFilterByOrderTypeALL = new OrderTypeFilterButton(POSConstants.ALL);
    
    JPanel filterByOrderPanel = new JPanel(new MigLayout("", "fill, grow", ""));
    filterByOrderPanel.setBorder(new TitledBorder(Messages.getString("SwitchboardView.4")));
    
    ButtonGroup orderTypeGroup = new ButtonGroup();
    orderTypeGroup.add(btnFilterByOrderTypeALL);
    
    filterByOrderPanel.add(btnFilterByOrderTypeALL);
    
    List<OrderType> orderTypes = Application.getInstance().getOrderTypes();
    for (OrderType orderType : orderTypes) {
      OrderTypeFilterButton orderTypeFilterButton = new OrderTypeFilterButton(orderType.getName());
      orderTypeGroup.add(orderTypeFilterButton);
      filterByOrderPanel.add(orderTypeFilterButton);
    }
    
    getContentPane().add(filterByOrderPanel);
  }
  
  private void createMyTicketPanel() {
    JPanel filterByOwnTicketPanel = new JPanel(new MigLayout("", "fill, grow", ""));
    filterByOwnTicketPanel.setBorder(new TitledBorder("FILTER TICKETS BY OWNER"));
    
    POSToggleButton btnFilterByOrderTypeMyTickets = new POSToggleButton(POSConstants.MY_TICKETS);
    filterByOwnTicketPanel.add(btnFilterByOrderTypeMyTickets, "");
    
    btnFilterByOrderTypeMyTickets.setSelected(TerminalConfig.isFilterByOwner());
    
    btnFilterByOrderTypeMyTickets.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        int state = e.getStateChange();
        if (state == 1) {
          TerminalConfig.setFilterByOwner(true);
          OrderFilterPanel.this.doUpdateTicketList();
        }
        else {
          TerminalConfig.setFilterByOwner(false);
          OrderFilterPanel.this.doUpdateTicketList();
        }
        
      }
      
    });
    getContentPane().add(filterByOwnTicketPanel);
  }
  
  private void updateButton()
  {
    PaymentStatusFilter paymentStatusFilter = TerminalConfig.getPaymentStatusFilter();
    if (paymentStatusFilter.name().equals(PaymentStatusFilter.OPEN.getDisplayString())) {
      btnFilterByOpenStatus.setSelected(true);



    }
    else if (paymentStatusFilter.name().equals(PaymentStatusFilter.CLOSED.getDisplayString())) {
      btnFilterByUnPaidStatus.setSelected(true);
    }
  }
  
  private void doUpdateTicketList() {
    ticketList.updateTicketList();
    ticketLists.updateButtonStatus();
  }
  
  private class OrderTypeFilterButton extends POSToggleButton implements ActionListener
  {
    public OrderTypeFilterButton(String name) {
      String orderTypeFilter = TerminalConfig.getOrderTypeFilter();
      if (orderTypeFilter.equals(name)) {
        setSelected(true);
      }
      setText(name);
      addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e)
    {
      setSelected(true);
      String actionCommand = e.getActionCommand();
      TerminalConfig.setOrderTypeFilter(actionCommand);
      OrderFilterPanel.this.doUpdateTicketList();
    }
  }
}
