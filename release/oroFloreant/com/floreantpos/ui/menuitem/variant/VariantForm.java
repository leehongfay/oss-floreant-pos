package com.floreantpos.ui.menuitem.variant;

import com.floreantpos.POSConstants;
import com.floreantpos.model.ImageResource;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.dao.ImageResourceDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.menuitem.ButtonStylePanel;
import com.floreantpos.ui.menuitem.InventoryPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JTabbedPane;

public class VariantForm
  extends BeanEditor<MenuItem>
{
  private ButtonStylePanel buttonStylePanel;
  private InventoryPanel inventoryPanel;
  
  public VariantForm(MenuItem menuItem)
  {
    initComponents();
    setBean(menuItem);
    inventoryPanel.setMenuItem(menuItem);
  }
  
  private void initComponents()
  {
    setLayout(new BorderLayout());
    JTabbedPane tab = new JTabbedPane();
    
    buttonStylePanel = new ButtonStylePanel();
    buttonStylePanel.setSortOrderVisibility(true);
    
    inventoryPanel = new InventoryPanel((MenuItem)getBean());
    tab.addTab("Button Style", buttonStylePanel);
    tab.addTab("Inventory", inventoryPanel);
    
    add(tab, "Center");
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      MenuItem menuItem = (MenuItem)getBean();
      MenuItemDAO menuItemDAO = new MenuItemDAO();
      if (menuItem.getId() == null) {
        menuItemDAO.save(menuItem);
      }
      else {
        menuItemDAO.saveOrUpdate(menuItem);
      }
    } catch (Exception e) {
      MessageDialog.showError(POSConstants.ERROR_MESSAGE, e);
      return false;
    }
    return true;
  }
  
  protected void updateView()
  {
    MenuItem menuItem = (MenuItem)getBean();
    
    ImageResource imageResource = ImageResourceDAO.getInstance().findById(menuItem.getImageId());
    if (imageResource != null) {
      buttonStylePanel.setImageResource(imageResource);
    }
    Color buttonColor = menuItem.getButtonColor();
    if (buttonColor != null) {
      buttonStylePanel.setButtonColor(buttonColor);
      buttonStylePanel.setTextColor(buttonColor);
    }
    if (menuItem.getTextColor() != null) {
      buttonStylePanel.setTextForegroundColor(menuItem.getTextColor());
    }
    
    buttonStylePanel.setSortOrder(menuItem.getSortOrder().intValue());
    
    inventoryPanel.setSku(menuItem.getSku());
    inventoryPanel.setReOrderLevel(menuItem.getReorderLevel().doubleValue());
    inventoryPanel.setReplenishLevel(menuItem.getReplenishLevel().doubleValue());
    inventoryPanel.setCbDisableStockCount(menuItem.isDisableWhenStockAmountIsZero().booleanValue());
  }
  

  public boolean updateModel()
  {
    MenuItem menuItem = (MenuItem)getBean();
    menuItem.setReorderLevel(Double.valueOf(inventoryPanel.getReOrderLevel()));
    menuItem.setReplenishLevel(Double.valueOf(inventoryPanel.getReplenishLevel()));
    menuItem.setDisableWhenStockAmountIsZero(Boolean.valueOf(inventoryPanel.isCbDisableStockCount()));
    
    menuItem.setShowImageOnly(Boolean.valueOf(buttonStylePanel.isShowTextWithImage()));
    
    menuItem.setButtonColor(buttonStylePanel.getButtonColor());
    menuItem.setButtonColorCode(Integer.valueOf(buttonStylePanel.getButtonColorCode()));
    menuItem.setTextColor(buttonStylePanel.getTextColor());
    menuItem.setTextColorCode(Integer.valueOf(buttonStylePanel.getTextColorCode()));
    menuItem.setImageId(buttonStylePanel.getImageResourceId());
    
    menuItem.setSortOrder(buttonStylePanel.getSortOrder());
    
    menuItem.setSku(inventoryPanel.getSku());
    return true;
  }
  
  public String getDisplayText()
  {
    return "Variant Edit Form";
  }
}
