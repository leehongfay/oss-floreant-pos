package com.floreantpos.ui.menuitem;

import com.floreantpos.Messages;
import com.floreantpos.model.ImageResource;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.ImageGalleryDialog;
import com.floreantpos.util.POSUtil;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.border.EtchedBorder;
import net.miginfocom.swing.MigLayout;



public class ButtonStylePanel
  extends TransparentPanel
{
  private JCheckBox cbShowTextWithImage;
  private JButton btnButtonColor;
  private JButton btnTextColor;
  private JLabel lblImagePreview;
  private ImageResource imageResource;
  private JLabel lblSortOrder;
  private IntegerTextField tfSortOrder;
  
  public ButtonStylePanel()
  {
    initComponents();
  }
  
  private void initComponents() {
    setLayout(new MigLayout("hidemode 3, insets 10", "[][]100[][][][]", "[][][center][][][]"));
    
    cbShowTextWithImage = new JCheckBox(Messages.getString("MenuItemForm.40"));
    
    JLabel lblImage = new JLabel(Messages.getString("MenuItemForm.28"));
    lblImagePreview = new JLabel("");
    JButton btnClearImage = new JButton(Messages.getString("MenuItemForm.34"));
    JButton btnSelectImage = new JButton("...");
    
    btnButtonColor = new JButton();
    
    JLabel lblTextColor = new JLabel(Messages.getString("MenuItemForm.lblTextColor.text"));
    btnTextColor = new JButton(Messages.getString("MenuItemForm.SAMPLE_TEXT"));
    JLabel lblButtonColor = new JLabel();
    
    lblImage.setHorizontalAlignment(11);
    
    cbShowTextWithImage.setActionCommand(Messages.getString("MenuItemForm.41"));
    lblImagePreview.setHorizontalAlignment(0);
    lblImagePreview.setBorder(new EtchedBorder(1, null, null));
    lblImagePreview.setPreferredSize(PosUIManager.getSize(200, 200));
    
    btnButtonColor.setPreferredSize(new Dimension(228, 40));
    
    lblSortOrder = new JLabel("Sort Order");
    tfSortOrder = new IntegerTextField();
    
    add(lblSortOrder, "cell 0 0,right");
    add(tfSortOrder, "cell 1 0,grow, w 220!");
    
    add(lblImage, "cell 0 1,right");
    
    add(lblImagePreview, "cell 1 1, grow");
    add(btnClearImage, "cell  1 1");
    add(btnSelectImage, "cell 1 1");
    
    add(lblButtonColor, "cell 0 2,right");
    add(btnButtonColor, "cell 1 2");
    
    add(lblTextColor, "cell 0 3,right");
    add(btnTextColor, "cell 1 3");
    
    add(cbShowTextWithImage, "cell 1 4");
    
    btnTextColor.setPreferredSize(new Dimension(228, 50));
    
    btnSelectImage.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doSelectImageFile();
      }
      
    });
    btnClearImage.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doClearImage();
      }
      
    });
    btnButtonColor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        Color color = JColorChooser.showDialog(POSUtil.getBackOfficeWindow(), Messages.getString("MenuItemForm.42"), btnButtonColor.getBackground());
        btnButtonColor.setBackground(color);
        btnTextColor.setBackground(color);
      }
      
    });
    btnTextColor.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        Color color = JColorChooser.showDialog(POSUtil.getBackOfficeWindow(), Messages.getString("MenuItemForm.43"), btnTextColor.getForeground());
        btnTextColor.setForeground(color);
      }
    });
    lblSortOrder.setVisible(true);
    tfSortOrder.setVisible(true);
  }
  
  public void setSortOrderVisibility(boolean updateSortOrder) {
    lblSortOrder.setVisible(updateSortOrder);
    tfSortOrder.setVisible(updateSortOrder);
  }
  
  protected void doSelectImageFile() {
    ImageGalleryDialog dialog = ImageGalleryDialog.getInstance();
    dialog.setTitle("Image Gallery");
    dialog.setSize(PosUIManager.getSize(650, 600));
    dialog.setResizable(true);
    dialog.setSelectBtnVisible(true);
    dialog.open();
    if (dialog.isCanceled()) {
      return;
    }
    imageResource = dialog.getImageResource();
    if (imageResource != null) {
      lblImagePreview.setIcon(imageResource.getScaledImage(200, 200));
    }
  }
  
  protected void doClearImage() {
    lblImagePreview.setIcon(null);
    imageResource = null;
  }
  
  public boolean isShowTextWithImage() {
    return cbShowTextWithImage.isSelected();
  }
  
  public void setShowTextWithImage(boolean isSelected) {
    cbShowTextWithImage.setSelected(isSelected);
  }
  
  public Color getButtonColor() {
    return btnButtonColor.getBackground();
  }
  
  public int getButtonColorCode() {
    return btnButtonColor.getBackground().getRGB();
  }
  
  public Color getTextColor() {
    return btnTextColor.getForeground();
  }
  
  public int getTextColorCode() {
    return btnTextColor.getForeground().getRGB();
  }
  
  public void setButtonColor(Color color) {
    btnButtonColor.setBackground(color);
  }
  
  public void setTextColor(Color color) {
    btnTextColor.setBackground(color);
  }
  
  public void setTextForegroundColor(Color color) {
    btnTextColor.setForeground(color);
  }
  
  public String getImageResourceId() {
    if (imageResource != null) {
      return imageResource.getId();
    }
    return "";
  }
  
  public ImageResource getImageResource() {
    return imageResource;
  }
  
  public void setImageResource(ImageResource imageResource) {
    this.imageResource = imageResource;
    if (imageResource != null) {
      lblImagePreview.setIcon(imageResource.getAsIcon());
    }
  }
  
  public Integer getSortOrder() {
    return Integer.valueOf(tfSortOrder.getInteger());
  }
  
  public void setSortOrder(int sortOrder) {
    tfSortOrder.setText(String.valueOf(sortOrder));
  }
}
