package com.floreantpos.ui.menuitem;

import com.floreantpos.Messages;
import com.floreantpos.PosLog;
import com.floreantpos.model.InventoryVendorItems;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.dao.InventoryVendorItemsDAO;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import net.miginfocom.swing.MigLayout;





public class InventoryPanel
  extends TransparentPanel
{
  private POSTextField tfSku;
  private DoubleTextField tfReorderLevel;
  private DoubleTextField tfReplenishLevel;
  private JCheckBox chkDisableStockCount;
  private JLabel lblSku;
  private JList<InventoryVendorItems> listVendorItems;
  private MenuItem menuItem;
  private DefaultListModel model;
  
  public InventoryPanel(MenuItem menuItem)
  {
    this.menuItem = menuItem;
    initComponents();
    initData();
  }
  
  private void initData()
  {
    if (menuItem != null) {
      tfSku.setText(menuItem.getSku());
      tfReorderLevel.setText(menuItem.getReorderLevel() + "");
      tfReplenishLevel.setText(menuItem.getReplenishLevel() + "");
      chkDisableStockCount.setSelected(menuItem.isDisableWhenStockAmountIsZero().booleanValue());
      initVendors();
    }
  }
  
  private void initVendors() {
    if (menuItem == null)
      return;
    List<InventoryVendorItems> vendorItems = InventoryVendorItemsDAO.getInstance().findByItem(menuItem);
    if (vendorItems != null) {
      for (InventoryVendorItems inventoryVendorItems : vendorItems) {
        model.addElement(inventoryVendorItems);
      }
    }
  }
  
  public void setMenuItem(MenuItem menuItem) {
    if (menuItem == null)
      return;
    tfSku.setText(menuItem.getSku());
    tfReorderLevel.setText(menuItem.getReorderLevel() + "");
    tfReplenishLevel.setText(menuItem.getReplenishLevel() + "");
    chkDisableStockCount.setSelected(menuItem.isDisableWhenStockAmountIsZero().booleanValue());
    initVendors();
  }
  
  private void initComponents() {
    setLayout(new MigLayout("hidemode 3,insets 20", "[][]", ""));
    
    lblSku = new JLabel(Messages.getString("MenuItemForm.59"));
    tfSku = new POSTextField();
    
    JLabel lblReorderLevel = new JLabel("Reorder Level");
    tfReorderLevel = new DoubleTextField(20);
    
    JLabel lblReplenishLevel = new JLabel("Replenish Level");
    tfReplenishLevel = new DoubleTextField(20);
    
    chkDisableStockCount = new JCheckBox(Messages.getString("MenuItemForm.18"));
    
    JLabel lblVendor = new JLabel("Vendors");
    lblVendor.setVerticalAlignment(1);
    lblVendor.setHorizontalAlignment(11);
    listVendorItems = new JList();
    listVendorItems.setSelectionMode(0);
    model = new DefaultListModel();
    listVendorItems.setModel(model);
    
    JButton btnAdd = new JButton("Add");
    JButton btnDelete = new JButton("Delete");
    
    btnAdd.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        InventoryPanel.this.doAddVendor();
      }
      
    });
    btnDelete.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        InventoryPanel.this.doDeleteVendor();
      }
      
    });
    add(lblSku, "alignx right");
    add(tfSku, "wrap");
    
    add(lblReorderLevel, "alignx right");
    add(tfReorderLevel, "wrap");
    
    add(lblReplenishLevel, "alignx right");
    add(tfReplenishLevel, "wrap");
    
    add(lblVendor, "right, grow");
    add(new JScrollPane(listVendorItems), "grow, wrap");
    add(btnAdd, "skip 1,split 2, grow");
    add(btnDelete, "grow, wrap");
    
    add(chkDisableStockCount, "skip 1, wrap");
  }
  
  public String getSku()
  {
    return tfSku.getText();
  }
  
  public void setSku(String sku) {
    tfSku.setText(sku);
  }
  
  public double getReOrderLevel() {
    return tfReorderLevel.getDouble();
  }
  
  public void setReOrderLevel(double reOrderLevel) {
    tfReorderLevel.setText(String.valueOf(reOrderLevel));
  }
  
  public double getReplenishLevel() {
    return tfReplenishLevel.getDouble();
  }
  
  public void setReplenishLevel(double replenishLevel) {
    tfReplenishLevel.setText(String.valueOf(replenishLevel));
  }
  
  public boolean isCbDisableStockCount() {
    return chkDisableStockCount.isSelected();
  }
  
  public void setCbDisableStockCount(boolean isSelected) {
    chkDisableStockCount.setSelected(isSelected);
  }
  
  public void setSkuInvisible() {
    lblSku.setVisible(false);
    tfSku.setVisible(false);
  }
  
  public List<InventoryVendorItems> getVendorItems() {
    List<InventoryVendorItems> vendorItems = new ArrayList();
    ListModel<InventoryVendorItems> listModel = listVendorItems.getModel();
    for (int i = 0; i < listModel.getSize(); i++) {
      vendorItems.add(listModel.getElementAt(i));
    }
    return vendorItems;
  }
  
  private void doAddVendor() {
    try {
      VendorSelectionDialog dialog = new VendorSelectionDialog();
      dialog.setMenuItem(menuItem);
      dialog.setDefaultCloseOperation(2);
      dialog.setSize(PosUIManager.getSize(500, 550));
      dialog.open();
      
      if (dialog.isCanceled()) {
        return;
      }
      List<InventoryVendorItems> vendorItems = dialog.getVendorItems();
      for (InventoryVendorItems inventoryVendorItems : vendorItems) {
        model.addElement(inventoryVendorItems);
      }
    } catch (Exception e) {
      PosLog.error(InventoryPanel.class, e.getMessage(), e);
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Something wrong!");
    }
  }
  
  private void doDeleteVendor() {
    try {
      InventoryVendorItems selectedVendorItems = (InventoryVendorItems)listVendorItems.getSelectedValue();
      if (selectedVendorItems == null) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select item!");
        return;
      }
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Do you want to delete selected vendor?", "Delete Vendor");
      if (option != 0) {
        return;
      }
      model.removeElement(selectedVendorItems);
      InventoryVendorItemsDAO.getInstance().delete(selectedVendorItems);
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Vendor successfully deleted.");
    } catch (Exception e) {
      PosLog.error(InventoryPanel.class, e.getMessage(), e);
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Something wrong!");
    }
  }
}
