package com.floreantpos.ui.menuitem;

import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.config.AppProperties;
import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.InventoryVendorItems;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.dao.InventoryVendorDAO;
import com.floreantpos.model.dao.InventoryVendorItemsDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;




public class VendorSelectionDialog
  extends POSDialog
{
  private BeanTableModel<InventoryVendor> tableModel;
  private List<InventoryVendorItems> vendorItems = new ArrayList();
  private JXTable table;
  private MenuItem menuItem;
  
  public VendorSelectionDialog() {
    super(POSUtil.getFocusedWindow(), Dialog.ModalityType.APPLICATION_MODAL);
    initComponents();
    initData();
  }
  
  private void initData() {
    List<InventoryVendor> vendors = InventoryVendorDAO.getInstance().findAll();
    tableModel.setRows(vendors);
  }
  
  private void initComponents() {
    setTitle(AppProperties.getAppName());
    setLayout(new BorderLayout(5, 5));
    tableModel = new BeanTableModel(InventoryVendor.class);
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "name");
    tableModel.setPageSize(10);
    table = new JXTable(tableModel);
    table.setSelectionMode(2);
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.setRowHeight(PosUIManager.getSize(40));
    
    JPanel contentPanel = new JPanel(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(10, 5, 10, 5));
    
    JScrollPane scroll = new JScrollPane(table);
    scroll.setPreferredSize(PosUIManager.getSize(500, 250));
    contentPanel.add(scroll);
    
    JPanel bottomPanel = new JPanel(new MigLayout("center"));
    PosButton btnSelect = new PosButton("Select");
    btnSelect.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        VendorSelectionDialog.this.doAddVendorItems();
      }
    });
    PosButton btnCancel = new PosButton("Cancel");
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setCanceled(true);
        dispose();
      }
      
    });
    bottomPanel.add(btnSelect);
    bottomPanel.add(btnCancel);
    
    contentPanel.add(bottomPanel, "South");
    add(contentPanel);
  }
  
  public MenuItem getMenuItem() {
    return menuItem;
  }
  
  public void setMenuItem(MenuItem menuItem) {
    this.menuItem = menuItem;
  }
  
  public List<InventoryVendorItems> getVendorItems() {
    return vendorItems;
  }
  
  public void setVendorItems(List<InventoryVendorItems> vendorItems) {
    this.vendorItems = vendorItems;
  }
  
  private void doAddVendorItems() {
    try {
      int[] selectedRows = table.getSelectedRows();
      if (selectedRows.length < 1) {
        return;
      }
      
      for (int i : selectedRows) {
        int index = table.convertRowIndexToModel(i);
        InventoryVendor inventoryVendor = (InventoryVendor)tableModel.getRow(index);
        boolean hasVendorItems = InventoryVendorItemsDAO.getInstance().vendorHasItem(inventoryVendor, menuItem, null);
        if (!hasVendorItems)
        {

          InventoryVendorItems inventoryVendorItems = new InventoryVendorItems();
          inventoryVendorItems.setItem(menuItem);
          inventoryVendorItems.setVendor(inventoryVendor);
          vendorItems.add(inventoryVendorItems);
        } }
      setCanceled(false);
      dispose();
    } catch (Exception e) {
      PosLog.error(VendorSelectionDialog.class, e.getMessage(), e);
    }
  }
}
