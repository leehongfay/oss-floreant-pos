package com.floreantpos.ui.dialog;

import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.config.AppProperties;
import com.floreantpos.main.Application;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.util.POSUtil;
import com.orocube.common.util.TerminalUtil;
import com.orocube.licensemanager.LicenseUtil;
import com.orocube.licensemanager.OroLicense;
import com.orocube.licensemanager.ui.PluginFileChooser;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;



public class OroLicenceActivationDialog
  extends POSDialog
{
  private String productName;
  private String productVersion;
  private static OroLicense license;
  private POSTextField tfTerminalKey;
  
  public OroLicenceActivationDialog(String productName, String productVersion)
  {
    this.productName = productName;
    this.productVersion = productVersion;
    setModal(true);
    init();
    initData();
  }
  
  private void initData() {
    tfTerminalKey.setText(TerminalUtil.getSystemUID());
  }
  
  private void init() {
    setTitle(AppProperties.getAppName() + " License Activation");
    setSize(600, 350);
    setLayout(new BorderLayout(5, 5));
    JPanel centerPanel = new JPanel(new MigLayout("fill", "[][grow][]", ""));
    
    JLabel lblActivationMsg = new JLabel("Please activate " + AppProperties.getAppName() + " to continue");
    lblActivationMsg.setFont(new Font(lblActivationMsg.getFont().getName(), 1, 20));
    lblActivationMsg.setHorizontalAlignment(0);
    JLabel lblTerminalKey = new JLabel("Terminal key:");
    tfTerminalKey = new POSTextField();
    tfTerminalKey.setEditable(false);
    
    JButton btnCopy = new JButton("Copy");
    btnCopy.setPreferredSize(PosUIManager.getSize(100, 0));
    btnCopy.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        String terminalKey = tfTerminalKey.getText();
        if (StringUtils.isEmpty(terminalKey)) {
          return;
        }
        StringSelection selection = new StringSelection(terminalKey);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(selection, selection);
      }
      

    });
    add(lblActivationMsg, "North");
    centerPanel.add(lblTerminalKey, "align trailing");
    centerPanel.add(tfTerminalKey, "growx");
    centerPanel.add(btnCopy, "");
    
    add(centerPanel, "Center");
    
    JPanel buttonPanel = new JPanel(new MigLayout("al center", "sg, fill", ""));
    
    PosButton btnSelectLicense = new PosButton("Select license");
    buttonPanel.add(btnSelectLicense, "grow");
    
    btnSelectLicense.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        OroLicenceActivationDialog.this.doSelectLicense();
      }
      
    });
    buttonPanel.add(new PosButton(new CloseDialogAction(this, "Exit")));
    add(buttonPanel, "South");
  }
  
  private void doSelectLicense() {
    File licenseFile = PluginFileChooser.selectPluginFile(Application.getPosWindow());
    if (licenseFile == null) {
      return;
    }
    try {
      OroLicense newLicense = LicenseUtil.loadAndValidate(licenseFile, productName, productVersion, TerminalUtil.getSystemUID());
      LicenseUtil.copyLicenseFile(licenseFile, productName);
      setLicense(newLicense);
      
      if (isValidLicense()) {
        dispose();
        OroLicenseInfoDialog licenseInfoDialog = new OroLicenseInfoDialog(newLicense);
        licenseInfoDialog.setSize(PosUIManager.getSize(500, 500));
        licenseInfoDialog.setLocationRelativeTo(null);
        licenseInfoDialog.setVisible(true);
      }
      else {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "License invalid");
      }
    }
    catch (Exception ex) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "License is invalid, Please select valid license.");
    }
  }
  
  public static OroLicense showDialog(String productName, String productVersion)
  {
    OroLicenceActivationDialog dialog = new OroLicenceActivationDialog(productName, productVersion);
    dialog.open();
    if ((dialog.isCanceled()) && 
      (!dialog.isValidLicense())) {
      System.exit(1);
    }
    
    return license;
  }
  
  public boolean isValidLicense() {
    if (license != null) {
      return license.isValid();
    }
    return false;
  }
  
  public OroLicense getLicense() {
    return license;
  }
  
  public void setLicense(OroLicense license) {
    license = license;
  }
}
