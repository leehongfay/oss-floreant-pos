package com.floreantpos.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.Discount;
import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemDiscount;
import com.floreantpos.model.dao.DiscountDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosOptionPane;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.ScrollableFlowPanel;
import com.floreantpos.ui.ticket.TicketViewerTable;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;






























public class TicketItemDiscountSelectionDialog
  extends OkCancelOptionDialog
  implements ActionListener
{
  private ScrollableFlowPanel itemDiscountButtonPanel;
  private HashMap<String, DiscountButton> buttonMap = new HashMap();
  
  private JPanel itemSearchPanel;
  private JTextField txtSearchItem;
  private TicketViewerTable ticketViewerTable;
  private JScrollPane ticketScrollPane;
  private Ticket ticket;
  
  public TicketItemDiscountSelectionDialog(Ticket ticket)
  {
    super(POSUtil.getFocusedWindow(), Messages.getString("DiscountSelectionDialog.0"));
    this.ticket = ticket;
    initComponent();
  }
  
  private void initComponent() {
    setOkButtonText(POSConstants.SAVE_BUTTON_TEXT);
    setCancelButtonVisible(false);
    getContentPanel().setBorder(new EmptyBorder(5, 5, 0, 5));
    createCouponSearchPanel();
    getContentPanel().add(itemSearchPanel, "North");
    
    itemDiscountButtonPanel = new ScrollableFlowPanel(1);
    
    TitledBorder itemPanelBorder = BorderFactory.createTitledBorder(null, Messages.getString("TicketItemDiscountSelectionDialog.1"), 2, 0);
    
    itemDiscountButtonPanel.setBorder(itemPanelBorder);
    
    JScrollPane scrollPane = new PosScrollPane(itemDiscountButtonPanel, 20, 31);
    
    scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(80, 0));
    
    ticketViewerTable = new TicketViewerTable(ticket);
    ticketViewerTable.setSelectionMode(2);
    ticketViewerTable.setVisibleDeleteButton(0);
    ticketScrollPane = new PosScrollPane(ticketViewerTable);
    
    JPanel mainPanel = new JPanel(new MigLayout("fill,ins 5 5 0 5", "[40%][60%]", ""));
    mainPanel.add(ticketScrollPane, "grow,gaptop 8,gapbottom 2");
    mainPanel.add(itemDiscountButtonPanel, "grow");
    getContentPanel().add(mainPanel, "Center");
    
    rendererDiscounts();
    setSize(1024, 720);
  }
  
  private void createCouponSearchPanel() {
    itemSearchPanel = new JPanel(new BorderLayout(5, 5));
    itemSearchPanel.setBorder(new EmptyBorder(0, 10, 0, 10));
    PosButton btnSearch = new PosButton(IconFactory.getIcon("/ui_icons/", "search.png"));
    btnSearch.setPreferredSize(new Dimension(60, 40));
    
    JLabel lblCoupon = new JLabel(Messages.getString("DiscountSelectionDialog.4"));
    
    txtSearchItem = new JTextField();
    txtSearchItem.addFocusListener(new FocusListener()
    {
      public void focusLost(FocusEvent e) {
        txtSearchItem.setText(Messages.getString("DiscountSelectionDialog.5"));
        txtSearchItem.setForeground(Color.gray);
      }
      
      public void focusGained(FocusEvent e)
      {
        txtSearchItem.setForeground(Color.black);
        txtSearchItem.setText("");
      }
      
    });
    txtSearchItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        if (txtSearchItem.getText().equals("")) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("DiscountSelectionDialog.8"));
          return;
        }
        if ((!TicketItemDiscountSelectionDialog.this.addCouponByBarcode(txtSearchItem.getText())) && 
          (!TicketItemDiscountSelectionDialog.this.addCouponById(txtSearchItem.getText()))) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), Messages.getString("DiscountSelectionDialog.11"));
        }
        
        txtSearchItem.setText("");
      }
      
    });
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        String value = PosOptionPane.showInputDialog(Messages.getString("DiscountSelectionDialog.10"));
        if ((value == null) || (value.isEmpty())) {
          return;
        }
        txtSearchItem.requestFocus();
        
        if ((!TicketItemDiscountSelectionDialog.this.addCouponByBarcode(value)) && 
          (!TicketItemDiscountSelectionDialog.this.addCouponById(value))) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), Messages.getString("DiscountSelectionDialog.11"));
        }
        
      }
    });
    itemSearchPanel.add(lblCoupon, "West");
    itemSearchPanel.add(txtSearchItem);
    itemSearchPanel.add(btnSearch, "East");
  }
  
  private boolean addCouponById(String id) {
    Discount discount = DiscountDAO.getInstance().get(id);
    if (discount == null) {
      return false;
    }
    if (discount.getQualificationType().intValue() == 0) {
      DiscountButton discountButton = (DiscountButton)buttonMap.get(discount.getId());
      applyDiscountToTicketItems(discountButton);
    }
    DiscountButton button = (DiscountButton)buttonMap.get(discount.getId());
    if (button != null) {
      button.setSelected(Boolean.valueOf(true));
    }
    
    return true;
  }
  
  private boolean addCouponByBarcode(String barcode)
  {
    Discount discount = DiscountDAO.getInstance().getDiscountByBarcode(barcode, 0);
    
    if (discount == null) {
      return false;
    }
    
    DiscountButton discountButton = (DiscountButton)buttonMap.get(discount.getId());
    discountButton.setSelected(Boolean.valueOf(true));
    try {
      Thread.sleep(30L);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    applyDiscountToTicketItems(discountButton);
    discountButton.setSelected(Boolean.valueOf(false));
    return true;
  }
  
  public void actionPerformed(ActionEvent e)
  {
    rendererDiscounts();
  }
  
  private void rendererDiscounts() {
    buttonMap.clear();
    itemDiscountButtonPanel.getContentPane().removeAll();
    
    List<Discount> discounts = DiscountDAO.getInstance().getValidItemCoupons();
    for (Discount discount : discounts)
    {


      DiscountButton btnDiscount = new DiscountButton(discount);
      itemDiscountButtonPanel.add(btnDiscount);
      buttonMap.put(discount.getId(), btnDiscount);
    }
    itemDiscountButtonPanel.repaint();
    itemDiscountButtonPanel.revalidate();
  }
  
  public void doOk()
  {
    setCanceled(false);
    dispose();
  }
  
  public void doCancel() {
    buttonMap.clear();
    setCanceled(true);
    dispose();
  }
  
  private class DiscountButton extends JPanel implements ActionListener {
    private Discount discount;
    private PosButton btnDiscount;
    
    DiscountButton(Discount discount) {
      this.discount = discount;
      setPreferredSize(PosUIManager.getSize(120, 120));
      setLayout(new MigLayout("fill", "", "[60%][40%]"));
      
      JLabel lblCoupon = new JLabel();
      lblCoupon.setOpaque(false);
      lblCoupon.setHorizontalAlignment(0);
      lblCoupon.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
      lblCoupon.setText("<html><body><center><strong>" + discount.getName() + "</strong></center></body></html>");
      setBackground(Color.WHITE);
      setForeground(Color.BLACK);
      
      add(lblCoupon, "grow,span,wrap");
      
      btnDiscount = new PosButton(POSConstants.APPLY);
      btnDiscount.addActionListener(this);
      add(btnDiscount, "center");
    }
    







    public void actionPerformed(ActionEvent e)
    {
      TicketItemDiscountSelectionDialog.this.applyDiscountToTicketItems(this);
    }
    
    public Discount getDiscount() {
      return discount;
    }
    
    public void setSelected(Boolean selected) {
      btnDiscount.setSelected(selected.booleanValue());
      if (selected.booleanValue()) {
        btnDiscount.setText(Messages.getString("TicketItemDiscountSelectionDialog.13"));
        btnDiscount.setBackground(Color.PINK);
      }
      else {
        btnDiscount.setText(POSConstants.APPLY);
        btnDiscount.setBackground(UIManager.getColor("control"));
      }
    }
  }
  
  private void applyDiscountToTicketItems(DiscountButton discountButton) {
    int[] selectedRows = ticketViewerTable.getSelectedRows();
    Discount discount = discountButton.getDiscount();
    List<TicketItem> items = new ArrayList();
    
    boolean rePrice = discountButton.getDiscount().getType().intValue() == 2;
    if (rePrice) {
      if (selectedRows.length != 1) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("TicketItemDiscountSelectionDialog.16"));
        return;
      }
      TicketItem ticketItem = (TicketItem)ticketViewerTable.get(selectedRows[0]);
      
      TicketItemDiscount ticketItemDiscount = null;
      List<TicketItemDiscount> discounts = ticketItem.getDiscounts();
      Iterator iterator; if (discounts != null) {
        for (iterator = discounts.iterator(); iterator.hasNext();) {
          TicketItemDiscount itemDiscount = (TicketItemDiscount)iterator.next();
          if (itemDiscount.getDiscountId().equals(discount.getId())) {
            ticketItemDiscount = itemDiscount;
            break;
          }
        }
      }
      double initialAmount = ticketItem.getSubtotalAmount().doubleValue() - ticketItem.calculateDiscount(NumberUtil.convertToBigDecimal(ticketItem.getSubtotalAmount().doubleValue())).doubleValue();
      double value = NumberSelectionDialog2.takeDoubleInput(Messages.getString("TicketItemDiscountSelectionDialog.17"), initialAmount, true);
      
      if (value == -1.0D) {
        return;
      }
      ticketItem.setUnitPrice(Double.valueOf(value));
      ticketItem.calculatePrice();
      
      if (ticketItemDiscount == null) {
        ticketItemDiscount = MenuItem.convertToTicketItemDiscount(discount, ticketItem);
        ticketItem.addTodiscounts(ticketItemDiscount);
      }
      
      ticketItemDiscount.setValue(Double.valueOf(value));
      ticketItemDiscount.setAmount(Double.valueOf(value));
      ticketItem.calculateDiscount(NumberUtil.convertToBigDecimal(ticketItem.getSubtotalAmount().doubleValue()));
      ticketViewerTable.updateView(); return; }
    String questionMessage;
    int i;
    ITicketItem item; if (selectedRows.length == 0) {
      questionMessage = Messages.getString("TicketItemDiscountSelectionDialog.19");
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), questionMessage, POSConstants.CONFIRM) == 1) {
        return;
      }
      for (i = 0; i < ticketViewerTable.getRowCount(); i++) {
        item = ticketViewerTable.get(i);
        if ((item instanceof TicketItem)) {
          TicketItem ticketItem = (TicketItem)item;
          if (!ticketItem.isHasModifiers().booleanValue())
          {

            items.add((TicketItem)item);
          }
        }
      }
    } else {
      questionMessage = selectedRows;i = questionMessage.length; for (item = 0; item < i; item++) { int i = questionMessage[item];
        ITicketItem item = ticketViewerTable.get(i);
        if ((item instanceof TicketItem)) {
          items.add((TicketItem)item);
        }
      }
    }
    if (items.size() == 0) {
      return;
    }
    String msg = "";
    boolean limitOver = false;
    for (TicketItem ticketItem : items) {
      List<TicketItemDiscount> discounts = ticketItem.getDiscounts();
      if (discounts == null) {
        discounts = new ArrayList();
      }
      boolean exists = false;
      if ((discount.getMenuItems() != null) && (!discount.getMenuItems().isEmpty()) && 
        (!DiscountDAO.getInstance().isApplicable(ticketItem.getMenuItemId(), discount.getId()))) {
        msg = msg + ticketItem.getName() + ",";
      }
      else {
        for (Iterator iterator = discounts.iterator(); iterator.hasNext();) {
          TicketItemDiscount ticketItemDiscount = (TicketItemDiscount)iterator.next();
          if ((discount.getMaximumOff().doubleValue() != 0.0D) && (ticketItemDiscount.getCouponQuantity().doubleValue() + 1.0D > discount.getMaximumOff().doubleValue())) {
            exists = true;
            msg = msg + ticketItem.getName();
            limitOver = true;
            break;
          }
          if (ticketItemDiscount.getDiscountId().equals(discount.getId())) {
            ticketItemDiscount.setCouponQuantity(Double.valueOf(ticketItemDiscount.getCouponQuantity().doubleValue() + 1.0D));
            exists = true;
            break;
          }
        }
        if (!exists) {
          boolean isAmountDiscount = discountButton.getDiscount().getType().intValue() == 0;
          if (isAmountDiscount) {
            doSetModifiableAmount(discount, ticketItem);
          }
          else {
            ticketItem.addTodiscounts(MenuItem.convertToTicketItemDiscount(discount, ticketItem));
          }
        }
        
        ticketItem.calculateDiscount(NumberUtil.convertToBigDecimal(ticketItem.getSubtotalAmount().doubleValue()));
      } }
    if (!msg.isEmpty()) {
      if (limitOver) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), 
          Messages.getString("TicketItemDiscountSelectionDialog.22") + " " + msg.substring(0, msg.length()));
      }
      else
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), 
          Messages.getString("TicketItemDiscountSelectionDialog.23") + " " + msg.substring(0, msg.length()));
    }
    ticket.calculatePrice();
    ticketViewerTable.updateView();
  }
  
  private void doSetModifiableAmount(Discount discount, TicketItem ticketItem) {
    double value = NumberSelectionDialog2.takeDoubleInput("Enter modifiable amount", ticketItem.getSubtotalAmount().doubleValue(), true);
    if (value == -1.0D) {
      return;
    }
    TicketItemDiscount ticketItemDiscount = MenuItem.convertToTicketItemDiscount(discount, ticketItem);
    ticketItem.addTodiscounts(ticketItemDiscount);
    
    ticketItemDiscount.setValue(Double.valueOf(value));
    ticketItemDiscount.setAmount(Double.valueOf(value));
  }
}
