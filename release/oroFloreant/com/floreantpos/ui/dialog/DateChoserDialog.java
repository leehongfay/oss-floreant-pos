package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.AttendenceHistory;
import com.floreantpos.model.Shift;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.ShiftUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXDatePicker;

public class DateChoserDialog extends POSDialog
{
  private JXDatePicker tbStartDate;
  private JXDatePicker tbEndDate;
  private IntegerTextField tfStartHour;
  private IntegerTextField tfStartMin;
  private JRadioButton rbStartAm;
  private JRadioButton rbStartPm;
  private IntegerTextField tfEndHour;
  private IntegerTextField tfEndMin;
  private JRadioButton rbEndAm;
  private JRadioButton rbEndPm;
  private ButtonGroup btnGroupStartAmPm;
  private ButtonGroup btnGroupEndAmPm;
  private PosButton btnOk;
  private PosButton btnCancel;
  private AttendenceHistory attendenceHistory;
  private JComboBox cbEmployees;
  private JCheckBox chkClockOut;
  
  public DateChoserDialog(String title)
  {
    super(POSUtil.getBackOfficeWindow(), title);
    attendenceHistory = new AttendenceHistory();
    initUi();
  }
  
  public DateChoserDialog(AttendenceHistory history, String title) {
    super(POSUtil.getBackOfficeWindow(), title);
    attendenceHistory = history;
    initUi();
  }
  
  private void initUi() {
    setIconImage(Application.getApplicationIcon().getImage());
    setResizable(false);
    JPanel mainPanel = new JPanel(new BorderLayout());
    
    mainPanel.setBackground(Color.red);
    
    java.util.List<User> employees = UserDAO.getInstance().findAll();
    cbEmployees = new JComboBox(new ComboBoxModel(employees));
    
    JPanel topPanel = new JPanel(new MigLayout());
    topPanel.add(new JLabel(Messages.getString("DateChoserDialog.0")));
    topPanel.add(cbEmployees);
    mainPanel.add(topPanel, "North");
    
    JPanel panel = new JPanel(new MigLayout("wrap 2", " [][][][][][][][][]", "[][]"));
    panel.setBorder(new TitledBorder("-"));
    
    btnGroupStartAmPm = new ButtonGroup();
    rbStartAm = new JRadioButton("AM");
    rbStartPm = new JRadioButton("PM");
    btnGroupStartAmPm.add(rbStartAm);
    btnGroupStartAmPm.add(rbStartPm);
    rbStartPm.setSelected(true);
    
    btnGroupEndAmPm = new ButtonGroup();
    rbEndAm = new JRadioButton("AM");
    rbEndPm = new JRadioButton("PM");
    btnGroupEndAmPm.add(rbEndAm);
    btnGroupEndAmPm.add(rbEndPm);
    rbEndPm.setSelected(true);
    
    tbStartDate = UiUtil.getCurrentMonthStart();
    
    tbEndDate = UiUtil.getCurrentMonthEnd();
    


    Vector<Integer> hours = new Vector();
    for (int i = 1; i <= 12; i++) {
      hours.add(Integer.valueOf(i));
    }
    
    DefaultComboBoxModel stMinModel = new DefaultComboBoxModel();
    stMinModel.addElement(Integer.valueOf(0));
    stMinModel.addElement(Integer.valueOf(15));
    stMinModel.addElement(Integer.valueOf(30));
    stMinModel.addElement(Integer.valueOf(45));
    
    DefaultComboBoxModel etMinModel = new DefaultComboBoxModel();
    etMinModel.addElement(Integer.valueOf(0));
    etMinModel.addElement(Integer.valueOf(15));
    etMinModel.addElement(Integer.valueOf(30));
    etMinModel.addElement(Integer.valueOf(45));
    
    tfStartHour = new IntegerTextField();
    tfStartMin = new IntegerTextField();
    tfEndHour = new IntegerTextField();
    tfEndMin = new IntegerTextField();
    
    tfStartHour.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        Integer selectedItem = Integer.valueOf(tfStartHour.getInteger());
        if (selectedItem.intValue() == 12) {
          selectedItem = Integer.valueOf(1);
        }
        else {
          selectedItem = Integer.valueOf(selectedItem.intValue() + 1);
        }
        tfEndHour.setText(String.valueOf(selectedItem));
      }
      
    });
    chkClockOut = new JCheckBox(Messages.getString("DateChoserDialog.5"));
    chkClockOut.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        DateChoserDialog.this.enabledItemsForClockOut();
      }
      
    });
    JLabel lblClockIn = new JLabel(Messages.getString("DateChoserDialog.6"));
    panel.add(lblClockIn, "cell 0 0,right");
    panel.add(new JLabel("Date"), "cell 1 0");
    tbStartDate.setFormats(new String[] { "MMM dd, yyyy" });
    panel.add(tbStartDate, "cell 2 0");
    panel.add(new JLabel("Hour"), "cell 3 0");
    panel.add(tfStartHour, "w 40!,cell 4 0");
    panel.add(new JLabel(Messages.getString("DateChoserDialog.11")), "cell 5 0");
    panel.add(tfStartMin, "w 40!,cell 6 0");
    panel.add(rbStartAm, "cell 7 0");
    panel.add(rbStartPm, "cell 8 0");
    
    panel.add(chkClockOut, "cell 0 1");
    panel.add(new JLabel("Date"), "cell 1 1");
    tbEndDate.setFormats(new String[] { "MMM dd, yyyy" });
    panel.add(tbEndDate, "cell 2 1");
    panel.add(new JLabel("Hour"), "cell 3 1");
    panel.add(tfEndHour, "w 40!,cell 4 1");
    panel.add(new JLabel(Messages.getString("DateChoserDialog.20")), "cell 5 1");
    panel.add(tfEndMin, "w 40!,cell 6 1");
    panel.add(rbEndAm, "cell 7 1");
    panel.add(rbEndPm, "cell 8 1");
    
    JPanel footerPanel = new JPanel(new MigLayout("al center center", "sg", ""));
    btnOk = new PosButton(Messages.getString("DateChoserDialog.1"));
    btnCancel = new PosButton(Messages.getString("DateChoserDialog.29"));
    btnCancel.setPreferredSize(new Dimension(100, 0));
    
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setCanceled(true);
        dispose();
      }
      

    });
    btnOk.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        if (DateChoserDialog.this.updateModel()) {
          setCanceled(false);
          dispose();
        }
        
      }
    });
    footerPanel.add(btnOk, "grow");
    footerPanel.add(btnCancel, "grow");
    
    mainPanel.add(panel, "Center");
    mainPanel.add(footerPanel, "South");
    getContentPane().add(mainPanel, "Center");
    
    updateView();
    enabledItemsForClockOut();
  }
  
  private void enabledItemsForClockOut() {
    boolean selected = chkClockOut.isSelected();
    tbEndDate.setEnabled(selected);
    tfEndHour.setEnabled(selected);
    tfEndMin.setEnabled(selected);
  }
  
  private void updateView()
  {
    Calendar startCalendar = Calendar.getInstance();
    Calendar endCalendar = Calendar.getInstance();
    
    if (attendenceHistory.getId() == null) {
      startCalendar.setTime(new Date());
      endCalendar.setTime(new Date());
    }
    else {
      if (attendenceHistory.getClockInTime() != null) {
        startCalendar.setTime(attendenceHistory.getClockInTime());
        
        if (attendenceHistory.getClockOutTime() != null) {
          endCalendar.setTime(attendenceHistory.getClockOutTime());
        }
      }
      cbEmployees.setSelectedItem(attendenceHistory.getUser());
      chkClockOut.setSelected(attendenceHistory.isClockedOut().booleanValue());
    }
    
    tbStartDate.setDate(startCalendar.getTime());
    
    Integer hour = Integer.valueOf(startCalendar.get(10));
    if (hour.equals(Integer.valueOf(0))) {
      hour = Integer.valueOf(12);
    }
    
    tfStartHour.setText(String.valueOf(hour));
    tfStartMin.setText(String.valueOf(startCalendar.get(12)));
    
    if (startCalendar.get(9) == 0) {
      rbStartAm.setSelected(true);
    }
    else {
      rbStartPm.setSelected(true);
    }
    
    tbEndDate.setDate(endCalendar.getTime());
    
    Integer endHour = Integer.valueOf(endCalendar.get(10));
    if (endHour.equals(Integer.valueOf(0))) {
      endHour = Integer.valueOf(12);
    }
    
    tfEndHour.setText(String.valueOf(endHour));
    tfEndMin.setText(String.valueOf(endCalendar.get(12)));
    
    if (endCalendar.get(9) == 0) {
      rbEndAm.setSelected(true);
    }
    else {
      rbEndPm.setSelected(true);
    }
  }
  

  private boolean updateModel()
  {
    Calendar clockInTime = getStartDate();
    Calendar clockOutTime = getEndDate();
    
    PosLog.info(getClass(), "" + clockInTime.getTime().getTime());
    PosLog.info(getClass(), "" + clockOutTime.getTime().getTime());
    
    if (clockInTime.getTime().getTime() > clockOutTime.getTime().getTime()) {
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), Messages.getString("DateChoserDialog.32"));
      return false;
    }
    
    attendenceHistory.setClockInTime(clockInTime.getTime());
    attendenceHistory.setClockInHour(Short.valueOf((short)clockInTime.get(11)));
    
    if (!chkClockOut.isSelected()) {
      attendenceHistory.setClockOutTime(null);
      attendenceHistory.setClockOutHour(null);
    }
    else {
      attendenceHistory.setClockOutTime(clockOutTime.getTime());
      attendenceHistory.setClockOutHour(Short.valueOf((short)clockOutTime.get(11)));
    }
    User employee = (User)cbEmployees.getSelectedItem();
    Shift currentShift = ShiftUtil.getCurrentShift();
    
    attendenceHistory.setClockedOut(Boolean.valueOf(chkClockOut.isSelected()));
    attendenceHistory.setUser(employee);
    attendenceHistory.setTerminal(Application.getInstance().getTerminal());
    attendenceHistory.setShift(currentShift);
    
    return true;
  }
  
  private Calendar getStartDate() {
    if (tbStartDate.getDate() == null) {
      return null;
    }
    
    Calendar clStartDate = Calendar.getInstance();
    clStartDate.setTime(tbStartDate.getDate());
    
    Integer hour = Integer.valueOf(tfStartHour.getInteger());
    if (hour.intValue() == 12) {
      hour = Integer.valueOf(0);
    }
    
    clStartDate.set(10, hour.intValue());
    clStartDate.set(12, Integer.valueOf(tfStartMin.getInteger()).intValue());
    
    if (rbStartAm.isSelected()) {
      clStartDate.set(9, 0);
    }
    else if (rbStartPm.isSelected()) {
      clStartDate.set(9, 1);
    }
    
    return clStartDate;
  }
  
  private Calendar getEndDate() {
    if (tbEndDate.getDate() == null) {
      return null;
    }
    
    Calendar clEndDate = Calendar.getInstance();
    clEndDate.setTime(tbEndDate.getDate());
    
    Integer hour = Integer.valueOf(tfEndHour.getInteger());
    if (hour.intValue() == 12) {
      hour = Integer.valueOf(0);
    }
    
    clEndDate.set(10, hour.intValue());
    clEndDate.set(12, Integer.valueOf(tfEndMin.getInteger()).intValue());
    
    if (rbEndAm.isSelected()) {
      clEndDate.set(9, 0);
    }
    else if (rbEndPm.isSelected()) {
      clEndDate.set(9, 1);
    }
    
    return clEndDate;
  }
  
  public AttendenceHistory getAttendenceHistory() {
    return attendenceHistory;
  }
}
