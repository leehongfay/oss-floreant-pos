package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.config.AppProperties;
import com.floreantpos.main.Application;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.DrawerType;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.Store;
import com.floreantpos.model.dao.CashDrawerDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.report.CashDrawerTransactionReportModel;
import com.floreantpos.report.ReportUtil;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.swing.JRViewer;





























public class CashDrawerTransactionInfoDialog
  extends POSDialog
{
  private JPanel reportViewPanel;
  private TitlePanel titlePanel;
  private CashDrawer cashDrawerReport;
  private PosButton btnPrint;
  
  public CashDrawerTransactionInfoDialog(CashDrawer report)
  {
    super(POSUtil.getFocusedWindow(), "");
    setModal(true);
    cashDrawerReport = report;
    initComponents();
    setSize(PosUIManager.getSize(870, 670));
  }
  
  public void initialize() throws Exception {
    reportViewPanel.removeAll();
    if (cashDrawerReport.getId() != null) {
      cashDrawerReport = CashDrawerDAO.getInstance().loadFullCashDrawer(cashDrawerReport.getId());
    }
    List<PosTransaction> transactions = cashDrawerReport.getTransactions();
    CashDrawerTransactionReportModel reportModel = new CashDrawerTransactionReportModel();
    reportModel.setItems(transactions);
    
    HashMap map = new HashMap();
    Store store = Application.getInstance().getStore();
    map.put("headerLine1", store.getName());
    map.put("headerLine2", store.getAddressLine1());
    map.put("receiptType", cashDrawerReport.getType().intValue() == DrawerType.DRAWER.getTypeNumber() ? "Cash Drawer Sales details" : "Server Bank Sales details");
    map.put("serverName", "Assigned To: " + cashDrawerReport.getAssignedUser());
    map.put("startDate", "Date: " + DateUtil.formatFullDateAndTimeAsString(cashDrawerReport.getStartTime()));
    map.put("status", (transactions == null) || (transactions.isEmpty()) ? "No transaction found." : null);
    
    JasperReport masterReport = ReportUtil.getReport("transaction_report_receipt");
    JasperPrint jasperPrint = JasperFillManager.fillReport(masterReport, map, new JRTableModelDataSource(reportModel));
    
    JRViewer receiptView = new JRViewer(jasperPrint);
    reportViewPanel.add(receiptView);
    reportViewPanel.revalidate();
    reportViewPanel.repaint();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    
    titlePanel = new TitlePanel();
    add(titlePanel, "North");
    
    reportViewPanel = new JPanel(new BorderLayout());
    
    PosScrollPane scrollPane = new PosScrollPane(reportViewPanel);
    add(scrollPane);
    
    JPanel buttonPanel = new JPanel(new MigLayout("fill,hidemode 3,ins 5 5 5 5,wrap 4", "[grow]", ""));
    btnPrint = new PosButton(Messages.getString("DrawerPullReportDialog.8"));
    PosButton btnCancel = new PosButton(POSConstants.CANCEL_BUTTON_TEXT);
    
    buttonPanel.add(new JSeparator(), "grow,span,wrap");
    
    buttonPanel.add(btnCancel, "grow");
    add(buttonPanel, "South");
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        CashDrawerTransactionInfoDialog.this.doCloseDialog();
      }
    });
    btnPrint.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        CashDrawerTransactionInfoDialog.this.doPrintReport();
      }
    });
  }
  
  private void doCloseDialog()
  {
    dispose();
  }
  
  public void setTitle(String title) {
    titlePanel.setTitle(title);
    super.setTitle(AppProperties.getAppName());
  }
  
  private void doPrintReport() {}
}
