package com.floreantpos.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.views.order.OrderController;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;

















public class SendToKitchenOptionSelectionDialog
  extends POSDialog
{
  Ticket ticket;
  private PosButton btnSendNewItem;
  private PosButton btnSendAll;
  
  public SendToKitchenOptionSelectionDialog(Ticket ticket)
    throws HeadlessException
  {
    this.ticket = ticket;
    initializeComponent();
  }
  
  private void initializeComponent() {
    setTitle("Select option");
    setResizable(false);
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Send ticket to kitchen");
    
    add(titlePanel, "North");
    
    JPanel splitTypePanel = new JPanel(new MigLayout("fill,wrap 1,inset 10", "sg", ""));
    
    Font f = new Font("Verdana", 1, PosUIManager.getFontSize(20));
    int width = PosUIManager.getSize(300);
    int height = PosUIManager.getSize(70);
    
    btnSendNewItem = new PosButton("SEND NEW ITEMS");
    btnSendNewItem.setFont(f);
    btnSendNewItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        setCanceled(false);
        dispose();
        SendToKitchenOptionSelectionDialog.this.doSendNewItemsToKitchen();
        POSMessageDialog.showMessage("Items send to kitchen.");
      }
    });
    splitTypePanel.add(btnSendNewItem, "w " + width + "!,h " + height + "!,grow");
    
    btnSendAll = new PosButton("SEND ALL");
    btnSendAll.setFont(f);
    btnSendAll.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        setCanceled(false);
        dispose();
        SendToKitchenOptionSelectionDialog.this.doSendAllItemsToKitchen();
        POSMessageDialog.showMessage("Items send to kitchen.");
      }
    });
    splitTypePanel.add(btnSendAll, "w " + width + "!,h " + height + "!,grow");
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL_BUTTON_TEXT);
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
      
    });
    JPanel actionPanel = new JPanel(new MigLayout("fill"));
    actionPanel.add(btnCancel, "w " + width + "!,h " + height + "!,grow");
    
    add(splitTypePanel, "Center");
    add(actionPanel, "South");
  }
  
  private void doSendNewItemsToKitchen() {
    OrderController.saveOrder(ticket);
    if ((ticket.getOrderType().isShouldPrintToKitchen().booleanValue()) && 
      (ticket.needsKitchenPrint())) {
      ReceiptPrintService.printToKitchen(ticket);
      TicketDAO.getInstance().refresh(ticket);
    }
  }
  
  private void doSendAllItemsToKitchen()
  {
    OrderController.saveOrder(ticket);
    if (ticket.getOrderType().isShouldPrintToKitchen().booleanValue()) {
      ReceiptPrintService.printToKitchen(ticket, false);
      TicketDAO.getInstance().refresh(ticket);
    }
  }
}
