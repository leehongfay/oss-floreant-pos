package com.floreantpos.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.SerialPortUtil;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTextField;
import jssc.SerialPortException;
import net.miginfocom.swing.MigLayout;





















public class AutomatedWeightInputDialog
  extends OkCancelOptionDialog
  implements ActionListener
{
  private double defaultValue;
  private static DoubleTextField tfNumber;
  private boolean clearPreviousNumber = true;
  
  private static final String UNIT_KG = "KG";
  
  private static final String UNIT_LB = "LB";
  private static final String UNIT_OZ = "OZ";
  private static final String UNIT_G = "G";
  private POSToggleButton btnLB;
  private POSToggleButton btnOZ;
  private POSToggleButton btnKG;
  private POSToggleButton btnG;
  private PosButton btnRefresh;
  private JTextField tfUnitC;
  private ButtonGroup group;
  
  public AutomatedWeightInputDialog(String title)
  {
    super(Application.getPosWindow(), title);
    init();
  }
  
  private void init() {
    setResizable(false);
    JPanel leftPanel = new JPanel();
    
    MigLayout layout = new MigLayout("fill,inset 0", "sg, fill", "");
    leftPanel.setLayout(layout);
    
    Dimension size = PosUIManager.getSize_w100_h70();
    
    tfNumber = new DoubleTextField();
    tfNumber.setText(String.valueOf(defaultValue));
    tfNumber.setFont(tfNumber.getFont().deriveFont(1, 24.0F));
    tfNumber.setFocusable(true);
    tfNumber.requestFocus();
    tfNumber.setBackground(Color.WHITE);
    
    tfUnitC = new JTextField();
    tfUnitC.setText(Messages.getString("AutomatedWeightInputDialog.0"));
    tfUnitC.setFont(tfNumber.getFont().deriveFont(1, 24.0F));
    tfUnitC.setFocusable(false);
    tfUnitC.setEnabled(false);
    tfUnitC.setHorizontalAlignment(4);
    tfUnitC.setBackground(Color.WHITE);
    tfUnitC.setForeground(Color.LIGHT_GRAY);
    
    leftPanel.add(tfNumber, "span 2, grow");
    leftPanel.add(tfUnitC, "span, grow");
    
    String[][] numbers = { { "7", "8", "9" }, { "4", "5", "6" }, { "1", "2", "3" }, { ".", "0", "CLEAR ALL" } };
    String[][] iconNames = { { "7.png", "8.png", "9.png" }, { "4.png", "5.png", "6.png" }, { "1.png", "2.png", "3.png" }, { "dot.png", "0.png", "" } };
    

    for (int i = 0; i < numbers.length; i++) {
      for (int j = 0; j < numbers[i].length; j++) {
        PosButton posButton = new PosButton();
        posButton.setFocusable(false);
        ImageIcon icon = IconFactory.getIcon("/ui_icons/", iconNames[i][j]);
        String buttonText = String.valueOf(numbers[i][j]);
        
        if (icon == null) {
          posButton.setText(buttonText);
        }
        else {
          posButton.setIcon(icon);
          if (POSConstants.CLEAR_ALL.equals(buttonText)) {
            posButton.setText(Messages.getString("AutomatedWeightInputDialog.1"));
          }
        }
        
        posButton.setActionCommand(buttonText);
        posButton.addActionListener(this);
        String constraints = "w " + width + "!,h " + height + "!,grow";
        if (j == numbers[i].length - 1) {
          constraints = constraints + ",wrap";
        }
        leftPanel.add(posButton, constraints);
      }
    }
    getContentPanel().add(leftPanel, "Center");
    createRightPanel();
  }
  
  private void createRightPanel() {
    JPanel rightPanel = new JPanel(new MigLayout("fill, inset 0 10 0 0", "sg, fill", ""));
    
    Dimension size = PosUIManager.getSize_w100_h70();
    
    group = new ButtonGroup();
    
    btnRefresh = new PosButton(Messages.getString("AutomatedWeightInputDialog.2"));
    btnLB = new POSToggleButton("LB");
    btnOZ = new POSToggleButton("OZ");
    btnKG = new POSToggleButton("KG");
    btnG = new POSToggleButton("G");
    
    btnRefresh.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        readWeight();
      }
    });
    btnLB.addActionListener(this);
    btnOZ.addActionListener(this);
    btnKG.addActionListener(this);
    btnKG.setSelected(true);
    btnG.addActionListener(this);
    
    group.add(btnLB);
    group.add(btnOZ);
    group.add(btnKG);
    group.add(btnG);
    
    rightPanel.add(btnRefresh, "w " + width + "!,h " + 40 + "!,wrap");
    rightPanel.add(btnLB, "w " + width + "!, h " + height + "!,wrap");
    rightPanel.add(btnOZ, "w " + width + "!, h " + height + "!,wrap");
    rightPanel.add(btnKG, "w " + width + "!, h " + height + "!,wrap");
    rightPanel.add(btnG, "w " + width + "!, h " + height + "!,wrap");
    getContentPanel().add(rightPanel, "East");
  }
  
  public void readWeight()
  {
    try {
      String weightString = SerialPortUtil.readWeight(TerminalConfig.getScalePort());
      updateScaleView(weightString);
    }
    catch (Exception ex) {
      POSMessageDialog.showError(ex.getMessage());
    }
  }
  
  protected void updateScaleView(String value) throws SerialPortException {
    value = value.replaceAll("\n", "");
    String patternFormat = "(\\d*\\.?\\d*)(lb|oz|g|kg)";
    Pattern pattern = Pattern.compile(patternFormat, 2);
    Matcher m = pattern.matcher(value);
    
    if (m.find()) {
      tfNumber.setText(m.group(1));
      String unitName = m.group(2).toUpperCase();
      
      if (unitName.equals("KG")) {
        btnKG.setSelected(true);
        tfUnitC.setText("KG");
      }
      else if (unitName.equals("LB")) {
        btnLB.setSelected(true);
        tfUnitC.setText("LB");
      }
      else if (unitName.equals("G")) {
        btnG.setSelected(true);
        tfUnitC.setText("G");
      }
      else if (unitName.equals("OZ")) {
        btnOZ.setSelected(true);
        tfUnitC.setText("OZ");
      }
    }
    else {
      tfNumber.setText(String.valueOf(0));
      group.clearSelection();
    }
  }
  
  private double convert(String unitTo, double inputValue) {
    String unitFrom = tfUnitC.getText();
    
    if (unitFrom.equals("KG")) {
      if (unitTo.equals("LB")) {
        return NumberUtil.roundToTwoDigit(2.2D * inputValue);
      }
      if (unitTo.equals("OZ")) {
        return NumberUtil.roundToTwoDigit(35.27D * inputValue);
      }
      if (unitTo.equals("G")) {
        return NumberUtil.roundToTwoDigit(1000.0D * inputValue);
      }
    }
    else if (unitFrom.equals("LB")) {
      if (unitTo.equals("KG")) {
        return NumberUtil.roundToTwoDigit(0.45D * inputValue);
      }
      if (unitTo.equals("OZ")) {
        return NumberUtil.roundToTwoDigit(16.0D * inputValue);
      }
      if (unitTo.equals("G")) {
        return NumberUtil.roundToTwoDigit(453.59D * inputValue);
      }
    }
    else if (unitFrom.equals("G")) {
      if (unitTo.equals("KG")) {
        return NumberUtil.roundToTwoDigit(0.001D * inputValue);
      }
      if (unitTo.equals("OZ")) {
        return NumberUtil.roundToTwoDigit(0.035D * inputValue);
      }
      if (unitTo.equals("LB")) {
        return NumberUtil.roundToTwoDigit(0.00220462D * inputValue);
      }
    }
    else if (unitFrom.equals("OZ")) {
      if (unitTo.equals("KG")) {
        return NumberUtil.roundToTwoDigit(0.0283495D * inputValue);
      }
      if (unitTo.equals("G")) {
        return NumberUtil.roundToTwoDigit(28.3495D * inputValue);
      }
      if (unitTo.equals("LB")) {
        return NumberUtil.roundToTwoDigit(0.0625D * inputValue);
      }
    }
    return inputValue;
  }
  
  private void doClearAll() {
    tfNumber.setText(String.valueOf(0.0D));
  }
  
  private void doInsertNumber(String number)
  {
    if (clearPreviousNumber) {
      tfNumber.setText("0");
      clearPreviousNumber = false;
    }
    
    String s = tfNumber.getText();
    double d = 0.0D;
    try
    {
      d = Double.parseDouble(s);
    }
    catch (Exception localException) {}
    
    if ((d == 0.0D) && (!s.contains("."))) {
      tfNumber.setText(number);
      return;
    }
    
    s = s + number;
    if (!validate(s)) {
      POSMessageDialog.showError(this, POSConstants.INVALID_NUMBER);
      return;
    }
    tfNumber.setText(s);
  }
  
  private void doInsertDot() {
    String string = tfNumber.getText() + ".";
    if (!validate(string)) {
      POSMessageDialog.showError(this, POSConstants.INVALID_NUMBER);
      return;
    }
    tfNumber.setText(string);
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    
    double value = tfNumber.getDouble();
    
    if (actionCommand.equals(POSConstants.CLEAR_ALL)) {
      doClearAll();
    }
    else if (actionCommand.equals(".")) {
      doInsertDot();
    }
    else if (actionCommand.equals("LB")) {
      tfNumber.setText(String.valueOf(convert("LB", value)));
      tfUnitC.setText("LB");
    }
    else if (actionCommand.equals("OZ")) {
      tfNumber.setText(String.valueOf(convert("OZ", value)));
      tfUnitC.setText("OZ");
    }
    else if (actionCommand.equals("G")) {
      tfNumber.setText(String.valueOf(convert("G", value)));
      tfUnitC.setText("G");
    }
    else if (actionCommand.equals("KG")) {
      tfNumber.setText(String.valueOf(convert("KG", value)));
      tfUnitC.setText("KG");
    }
    else {
      doInsertNumber(actionCommand);
    }
  }
  
  private boolean validate(String str) {
    try {
      Double.parseDouble(str);
    } catch (Exception x) {
      return false;
    }
    return true;
  }
  
  public double getValue() {
    return Double.parseDouble(tfNumber.getText());
  }
  
  public void setValue(double value) {
    tfNumber.setText(String.valueOf(value));
  }
  
  public static double takeDoubleInput(String title, double initialAmount) {
    AutomatedWeightInputDialog dialog = new AutomatedWeightInputDialog(Messages.getString("AutomatedWeightInputDialog.3"));
    dialog.setCaption(title);
    dialog.setValue(initialAmount);
    if (TerminalConfig.isActiveScaleDisplay()) {
      dialog.readWeight();
    }
    dialog.pack();
    dialog.open();
    
    if (dialog.isCanceled()) {
      return -1.0D;
    }
    
    return dialog.getValue();
  }
  
  public void doOk()
  {
    if (!validate(tfNumber.getText())) {
      POSMessageDialog.showError(this, POSConstants.INVALID_NUMBER);
      return;
    }
    setCanceled(false);
    dispose();
  }
}
