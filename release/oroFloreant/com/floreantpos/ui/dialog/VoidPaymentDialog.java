package com.floreantpos.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.config.CardConfig;
import com.floreantpos.extension.PaymentGatewayPlugin;
import com.floreantpos.main.Application;
import com.floreantpos.model.ActionHistory;
import com.floreantpos.model.CreditCardTransaction;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.RefundTransaction;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.VoidTransaction;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.views.payment.CardProcessor;
import com.floreantpos.util.CurrencyUtil;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JPanel;






























public class VoidPaymentDialog
  extends OkCancelOptionDialog
{
  private Ticket ticket;
  private List<PosTransaction> selectedTransactions = new ArrayList();
  private Map<String, PosButton> buttonMap = new HashMap();
  private JPanel buttonsPanel;
  
  public VoidPaymentDialog(Window window, Ticket ticket) {
    this.ticket = ticket;
    initComponents();
    initialize();
    setDefaultCloseOperation(0);
  }
  
  private void initComponents() {
    setCaption("Void payment");
    setOkButtonText(POSConstants.SAVE_BUTTON_TEXT);
    setCancelButtonVisible(false);
    setDefaultCloseOperation(2);
    getContentPanel().add(createCenterPanel());
  }
  
  public void initialize() {
    try {
      rendererTransactions();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.CANNOT_LOAD_VOID_REASONS, e);
    }
  }
  
  private JPanel createCenterPanel()
  {
    JPanel contentPanel = new JPanel();
    contentPanel.setLayout(new BorderLayout());
    buttonsPanel = new JPanel(new FlowLayout(1));
    contentPanel.add(buttonsPanel);
    
    return contentPanel;
  }
  
  private void rendererTransactions() {
    buttonsPanel.removeAll();
    ActionListener l; if (ticket != null) {
      l = new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          PosButton button = (PosButton)e.getSource();
          PosTransaction transaction = (PosTransaction)button.getClientProperty("transaction");
          int option = POSMessageDialog.showYesNoQuestionDialog(VoidPaymentDialog.this, "Are you sure to void this payment?", "Confirm");
          
          if (option != 0) {
            return;
          }
          
          if ((transaction instanceof CreditCardTransaction)) {
            CardProcessor cardProcessor = CardConfig.getPaymentGateway().getProcessor();
            try {
              cardProcessor.voidTransaction(transaction);
              if (!transaction.isVoided().booleanValue()) {
                cardProcessor.refundTransaction(transaction, transaction.getAmount().doubleValue());
                if (!transaction.isRefunded()) {
                  POSMessageDialog.showError("Failed to reverse this payment.");
                  return;
                }
              }
            } catch (Exception e2) {
              try {
                cardProcessor.refundTransaction(transaction, transaction.getAmount().doubleValue());
                if (!transaction.isRefunded()) {
                  POSMessageDialog.showError("Failed to reverse this payment.");
                  return;
                }
              } catch (Exception e3) {
                POSMessageDialog.showError("Failed to reverse this payment.");
                return;
              }
            }
          }
          
          transaction.setVoided(Boolean.valueOf(true));
          ticket.setPaidAmount(Double.valueOf(ticket.getPaidAmount().doubleValue() - transaction.getAmount().doubleValue()));
          ticket.setDueAmount(Double.valueOf(ticket.getDueAmount().doubleValue() + transaction.getAmount().doubleValue()));
          ticket.setClosed(Boolean.valueOf(false));
          ticket.setPaid(Boolean.valueOf(false));
          ticket.calculatePrice();
          TicketDAO.getInstance().update(ticket);
          ActionHistory history = new ActionHistory();
          history.setActionName("Trans reversal");
          history.setDescription("Trans# " + transaction.getId());
          ((PosButton)e.getSource()).setEnabled(false);
        }
      };
      for (PosTransaction t : ticket.getTransactions())
        if ((!(t instanceof RefundTransaction)) && (!(t instanceof VoidTransaction)) && (!t.isVoided().booleanValue()))
        {

          PosButton btnTransactionType = new PosButton();
          btnTransactionType.putClientProperty("transaction", t);
          btnTransactionType.addActionListener(l);
          btnTransactionType.setPreferredSize(PosUIManager.getSize(120, 110));
          updateView(t, btnTransactionType);
          buttonsPanel.add(btnTransactionType, "aligny top");
          buttonMap.put(t.getId(), btnTransactionType);
        }
    }
    setCancelButtonVisible(false);
    buttonsPanel.revalidate();
    buttonsPanel.repaint();
  }
  
  private void updateView(PosTransaction t, PosButton btnTransactionType) {
    double refundedAmountForTransaction = 0.0D;
    if (refundedAmountForTransaction > 0.0D) {
      btnTransactionType.setText("<html><center>" + t.getPaymentType() + "<br><h2>" + CurrencyUtil.getCurrencySymbol() + t.getAmount() + "</h2><h5>Refunded -" + 
        CurrencyUtil.getCurrencySymbol() + refundedAmountForTransaction + "</h5></html>");
      btnTransactionType.setEnabled(t.getAmount().doubleValue() != refundedAmountForTransaction);
      if (t.getAmount().doubleValue() != refundedAmountForTransaction) {
        selectedTransactions.add(t);
        btnTransactionType.setSelected(true);
      }
    }
    else {
      selectedTransactions.add(t);
      btnTransactionType.setSelected(true);
      btnTransactionType.setText("<html><center>" + t.getPaymentType() + "<br><h2>" + CurrencyUtil.getCurrencySymbol() + t.getAmount());
    }
  }
  
  public void doOk()
  {
    try {
      canceled = false;
      dispose();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, e);
    }
  }
}
