package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PrinterGroup;
import com.floreantpos.model.TaxGroup;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemSeat;
import com.floreantpos.model.dao.PrinterGroupDAO;
import com.floreantpos.model.dao.TaxGroupDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.PosComboRenderer;
import com.floreantpos.swing.QwertyKeyPad;
import com.floreantpos.ui.views.order.OrderView;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;




























public class MiscTicketItemDialog
  extends OkCancelOptionDialog
{
  private TicketItem ticketItem;
  private JComboBox cbTaxGroup;
  private FixedLengthTextField tfItemName;
  private DoubleTextField tfItemPrice;
  private JComboBox cbPrinterGroup;
  private JLabel lblTax;
  private OrderType orderType;
  
  public MiscTicketItemDialog()
  {
    super(Application.getPosWindow(), true);
    setTitle(Messages.getString("MiscTicketItemDialog.0"));
    initComponents();
  }
  
  private void initComponents() {
    JPanel contentPane = new JPanel(new MigLayout("inset 0, fillx", "", ""));
    
    setTitle(Messages.getString("MiscTicketItemDialog.4"));
    setCaption(Messages.getString("MiscTicketItemDialog.4"));
    
    JLabel lblName = new JLabel(Messages.getString("MiscTicketItemDialog.6"));
    contentPane.add(lblName, "newline,alignx trailing");
    
    tfItemName = new FixedLengthTextField();
    tfItemName.setLength(120);
    contentPane.add(tfItemName, "grow, span, h 40");
    
    JLabel lblPrice = new JLabel(Messages.getString("MiscTicketItemDialog.9"));
    contentPane.add(lblPrice, "newline,alignx trailing");
    
    tfItemPrice = new DoubleTextField();
    contentPane.add(tfItemPrice, "grow, w 120, h 40");
    
    lblTax = new JLabel(Messages.getString(Messages.getString("MiscTicketItemDialog.2")));
    contentPane.add(lblTax, "alignx trailing");
    
    PosComboRenderer comboRenderer = new PosComboRenderer();
    comboRenderer.setEnableDefaultValueShowing(false);
    
    cbTaxGroup = new JComboBox();
    cbTaxGroup.setRenderer(comboRenderer);
    contentPane.add(cbTaxGroup, "w 200!, h 40");
    
    contentPane.add(new JLabel(Messages.getString("MiscTicketItemDialog.15")), "alignx trailing");
    
    cbPrinterGroup = new JComboBox();
    cbPrinterGroup.setRenderer(comboRenderer);
    contentPane.add(cbPrinterGroup, "w 200!, h 40");
    
    QwertyKeyPad keyPad = new QwertyKeyPad();
    contentPane.add(keyPad, "newline, grow, span, gaptop 10");
    
    getContentPanel().add(contentPane);
    
    initData();
  }
  
  private void initData() {
    List<TaxGroup> taxGroups = TaxGroupDAO.getInstance().findAll();
    
    cbTaxGroup.addItem(null);
    for (TaxGroup tax : taxGroups) {
      cbTaxGroup.addItem(tax);
    }
    String defaultTaxId = TerminalConfig.getMiscItemDefaultTaxId();
    if (!defaultTaxId.equals("-1")) {
      for (int i = 0; i < taxGroups.size(); i++) {
        TaxGroup tax = (TaxGroup)taxGroups.get(i);
        if (tax.getId().equals(defaultTaxId)) {
          cbTaxGroup.setSelectedIndex(i);
          break;
        }
      }
    }
    
    List<PrinterGroup> printerGroups = PrinterGroupDAO.getInstance().findAll();
    cbPrinterGroup.setModel(new ComboBoxModel(printerGroups));
  }
  
  public void doCancel() {
    setCanceled(true);
    ticketItem = null;
    dispose();
  }
  
  public void doOk() {
    double amount = tfItemPrice.getDouble();
    String itemName = tfItemName.getText();
    
    if (StringUtils.isEmpty(itemName)) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("MiscTicketItemDialog.1"));
      return;
    }
    
    if (Double.isNaN(amount)) {
      amount = 0.0D;
    }
    
    setCanceled(false);
    
    ticketItem = new TicketItem();
    ticketItem.setTaxIncluded(Boolean.valueOf(Application.getInstance().isPriceIncludesTax()));
    ticketItem.setQuantity(Double.valueOf(1.0D));
    ticketItem.setUnitPrice(Double.valueOf(amount));
    ticketItem.setName(itemName);
    ticketItem.setMenuItemId("0");
    ticketItem.setCategoryName(POSConstants.MISC_BUTTON_TEXT);
    ticketItem.setGroupName(POSConstants.MISC_BUTTON_TEXT);
    ticketItem.setShouldPrintToKitchen(Boolean.valueOf(true));
    
    OrderView orderView = OrderView.getInstance();
    Object selectedSeat = orderView.getSelectedSeatNumber();
    if ((selectedSeat instanceof Integer)) {
      ticketItem.setSeatNumber((Integer)selectedSeat);
    }
    else if ((selectedSeat instanceof TicketItemSeat)) {
      TicketItemSeat seat = (TicketItemSeat)orderView.getSelectedSeatNumber();
      ticketItem.setSeat(seat);
      ticketItem.setSeatNumber(seat.getSeatNumber());
    }
    TaxGroup selectedObject = (TaxGroup)cbTaxGroup.getSelectedItem();
    MenuItem.setItemTaxes(ticketItem, selectedObject, getOrderType());
    
    PrinterGroup printerGroup = (PrinterGroup)cbPrinterGroup.getSelectedItem();
    if (printerGroup != null) {
      ticketItem.setPrinterGroup(printerGroup);
    }
    
    dispose();
  }
  
  public TicketItem getTicketItem() {
    return ticketItem;
  }
  
  public OrderType getOrderType() {
    return orderType;
  }
  
  public void setOrderType(OrderType orderType) {
    this.orderType = orderType;
  }
}
