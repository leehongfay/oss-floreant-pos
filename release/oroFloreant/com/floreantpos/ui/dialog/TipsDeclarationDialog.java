package com.floreantpos.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.main.Application;
import com.floreantpos.model.DeclaredTips;
import com.floreantpos.model.StoreSession;
import com.floreantpos.model.TipsCashoutReport;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.DeclaredTipsDAO;
import com.floreantpos.model.dao.GratuityDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.ButtonColumn;
import com.floreantpos.swing.PaginatedTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;

























public class TipsDeclarationDialog
  extends POSDialog
{
  private TitlePanel titlePanel;
  private JXTable table;
  private DeclaredTipsTableModel tableModel;
  private User user;
  private StoreSession currentSession;
  private PosButton btnCancel;
  
  public TipsDeclarationDialog(User user, StoreSession currentSession, List<DeclaredTips> declaredTips)
  {
    super(Application.getPosWindow(), "", true);
    this.user = user;
    this.currentSession = currentSession;
    initComponents();
    initData();
    setDeclaredTips(declaredTips);
  }
  
  public void setDeclaredTips(List declaredTips) {
    tableModel.setRows(declaredTips);
    table.repaint();
  }
  
  public void setTitle(String title)
  {
    titlePanel.setTitle(title);
  }
  
  private void initComponents() {
    TransparentPanel container = new TransparentPanel();
    titlePanel = new TitlePanel();
    TransparentPanel contentPanel = new TransparentPanel();
    TransparentPanel bottomActionPanel = new TransparentPanel(new MigLayout("al center", "", ""));
    
    JSeparator jSeparator1 = new JSeparator();
    
    btnCancel = new PosButton();
    
    JScrollPane jScrollPane1 = new JScrollPane();
    table = new JXTable();
    table.setRowHeight(PosUIManager.getSize(40));
    table.getTableHeader().setPreferredSize(PosUIManager.getSize(0, 30));
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    
    getContentPane().setLayout(new BorderLayout(5, 5));
    
    container.setLayout(new BorderLayout(5, 5));
    
    PosButton btnShowServerTipsReport = new PosButton("TIPS DETAILS");
    btnShowServerTipsReport.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TipsDeclarationDialog.this.showServerTipsHistory();
      }
      
    });
    PosButton btnAdd = new PosButton("ADD");
    btnAdd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TipsDeclarationDialog.this.doDeclareNewTips();
      }
      
    });
    btnCancel.setText("DONE");
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TipsDeclarationDialog.this.btnCloseActionPerformed(evt);
      }
      
    });
    bottomActionPanel.add(btnShowServerTipsReport);
    bottomActionPanel.add(btnAdd);
    bottomActionPanel.add(btnCancel);
    
    container.add(jSeparator1, "Center");
    container.add(bottomActionPanel, "South");
    
    getContentPane().add(container, "South");
    
    contentPanel.setLayout(new BorderLayout());
    contentPanel.add(titlePanel, "North");
    
    jScrollPane1.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(10, 10, 5, 10), jScrollPane1.getBorder()));
    jScrollPane1.setViewportView(table);
    
    contentPanel.add(jScrollPane1, "Center");
    JPanel cashSummaryPanel = new JPanel(new MigLayout("center,ins 0"));
    
    contentPanel.add(cashSummaryPanel, "South");
    getContentPane().add(contentPanel, "Center");
  }
  
  private void showServerTipsHistory() {
    try {
      TipsCashoutReport report = GratuityDAO.getInstance().createReport(null, null, user, true);
      TipsCashoutReportDialog dialog = new TipsCashoutReportDialog(report);
      dialog.setCaption(POSConstants.SERVER_TIPS_REPORT);
      dialog.setSize(650, 600);
      dialog.open();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private void doDeclareNewTips() {
    try {
      double declareTips = NumberSelectionDialog2.takeDoubleInput("Enter declare tips amount", 0.0D);
      if (declareTips < 0.0D)
        return;
      DeclaredTips declaredTips = new DeclaredTips();
      declaredTips.setOwnerId(user.getId());
      declaredTips.setAmount(Double.valueOf(declareTips));
      declaredTips.setDeclaredTime(new Date());
      declaredTips.setSessionId(currentSession.getId());
      DeclaredTipsDAO.getInstance().saveOrUpdate(declaredTips);
      tableModel.addItem(declaredTips);
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Error", e);
    }
  }
  
  public void initData() {
    table.getSelectionModel().setSelectionMode(0);
    tableModel = new DeclaredTipsTableModel();
    table.setModel(tableModel);
    
    AbstractAction declareTipsAction = new AbstractAction()
    {
      public void actionPerformed(ActionEvent e)
      {
        int row = Integer.parseInt(e.getActionCommand());
        DeclaredTips declareTips = (DeclaredTips)tableModel.getRowData(row);
        doDeclareTips(declareTips);
      }
    };
    addButtonColumnAction(declareTipsAction, 2);
    resizeTableColumns();
  }
  
  private void addButtonColumnAction(AbstractAction action, int columnNum) {
    ButtonColumn buttonColumn = new ButtonColumn(table, action, columnNum)
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        return super.getTableCellRendererComponent(table, value, false, hasFocus, row, column);
      }
      
      public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
      {
        return super.getTableCellEditorComponent(table, value, false, row, column);
      }
    };
    MatteBorder selectedBorder = BorderFactory.createMatteBorder(3, 3, 3, 3, table.getBackground());
    MatteBorder unselectedBorder = BorderFactory.createMatteBorder(3, 3, 3, 3, table.getBackground());
    
    Border border1 = new CompoundBorder(selectedBorder, btnCancel.getBorder());
    Border border2 = new CompoundBorder(unselectedBorder, btnCancel.getBorder());
    
    buttonColumn.setUnselectedBorder(border1);
    buttonColumn.setFocusBorder(border2);
  }
  
  protected void doDeclareTips(DeclaredTips declaredTips) {
    try {
      double declareTips = NumberSelectionDialog2.takeDoubleInput("Enter declare tips amount", declaredTips.getAmount().doubleValue());
      if (declareTips < 0.0D)
        return;
      declaredTips.setAmount(Double.valueOf(declareTips));
      declaredTips.setDeclaredTime(new Date());
      DeclaredTipsDAO.getInstance().saveOrUpdate(declaredTips);
      tableModel.fireTableDataChanged();
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Error", e);
    }
  }
  
  private void resizeTableColumns() {
    table.setAutoResizeMode(4);
    setColumnWidth(1, PosUIManager.getSize(100));
    setColumnWidth(2, PosUIManager.getSize(90));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = table.getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  public void setInfo(String info) {
    titlePanel.setTitle(info);
  }
  
  private void btnCloseActionPerformed(ActionEvent evt) {
    setCanceled(false);
    dispose();
  }
  
  public boolean hasOpenTickets() {
    return tableModel.getRowCount() > 0;
  }
  
  class DeclaredTipsTableModel extends PaginatedTableModel
  {
    public DeclaredTipsTableModel() {
      super();
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      if (columnIndex == 2) {
        return true;
      }
      return false;
    }
    
    public DeclaredTips getSelectedRow() {
      int index = table.getSelectedRow();
      if (index < 0) {
        return null;
      }
      index = table.convertRowIndexToModel(index);
      return (DeclaredTips)tableModel.getRowData(index);
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      List<DeclaredTips> rows = getRows();
      if (rows == null)
        return null;
      DeclaredTips t = (DeclaredTips)rows.get(rowIndex);
      if (t == null)
        return null;
      switch (columnIndex) {
      case 0: 
        Date transactionTime = t.getDeclaredTime();
        if (transactionTime == null)
          return "";
        return DateUtil.formatFullDateAndTimeAsString(transactionTime);
      case 1: 
        return t.getAmount();
      case 2: 
        return "EDIT";
      }
      return "";
    }
  }
}
