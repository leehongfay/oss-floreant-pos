package com.floreantpos.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.actions.CloseDialogAction;
import com.floreantpos.model.KitchenTicket;
import com.floreantpos.model.KitchenTicketItem;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.dao.KitchenTicketDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.ticket.TicketViewerTable;
import com.floreantpos.ui.ticket.TicketViewerTableModel;
import com.jidesoft.swing.TitledSeparator;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;





public class ReorderDialog
  extends POSDialog
{
  private Ticket ticket;
  private List<TicketItem> ticketItems;
  private TicketViewerTable reorderedItemsTable;
  
  public ReorderDialog(Ticket ticket)
  {
    this.ticket = ticket;
    init();
  }
  
  private void init() {
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Reorder Items");
    add(titlePanel, "North");
    
    JPanel centerPanel = new JPanel(new MigLayout("fill", "[grow][][grow][]", ""));
    
    JPanel leftTicketPanel = new JPanel(new BorderLayout());
    JLabel lblTitle1 = new JLabel("Ticket");
    lblTitle1.setForeground(Color.BLUE);
    TitledSeparator sep1 = new TitledSeparator(lblTitle1, 0);
    

    JLabel lblTitle2 = new JLabel("Reordered Items");
    lblTitle2.setForeground(Color.BLUE);
    TitledSeparator sep2 = new TitledSeparator(lblTitle2, 0);
    
    JPanel centerSelectPanel = new JPanel(new MigLayout("center"));
    PosButton btnSelect = new PosButton();
    btnSelect.setToolTipText("Send single item");
    btnSelect.setIcon(IconFactory.getIcon("/ui_icons/", "next.png"));
    
    PosButton btnSendAll = new PosButton();
    btnSendAll.setToolTipText("Send all");
    btnSendAll.setIcon(IconFactory.getIcon("/ui_icons/", "triple_arrow.png"));
    
    PosButton btnLastOrder = new PosButton();
    btnLastOrder.setToolTipText("Send last order");
    btnLastOrder.setIcon(IconFactory.getIcon("/ui_icons/", "double_arrow.png"));
    
    centerSelectPanel.add(btnSelect, "center, wrap");
    centerSelectPanel.add(btnLastOrder, "center, wrap");
    centerSelectPanel.add(btnSendAll, "center, wrap");
    
    JPanel rightSelectedItemPanel = new JPanel(new BorderLayout());
    

    JPanel rightControlPanel = new JPanel(new MigLayout());
    
    PosButton btnAdd = new PosButton();
    PosButton btnSub = new PosButton();
    PosButton btnRemove = new PosButton();
    PosButton btnRemoveAll = new PosButton();
    
    btnAdd.setToolTipText("Increase item number");
    btnSub.setToolTipText("Decrease item number");
    btnRemove.setToolTipText("Remove item");
    btnRemoveAll.setToolTipText("Remove all");
    
    btnAdd.setIcon(IconFactory.getIcon("/ui_icons/", "add.png"));
    btnSub.setIcon(IconFactory.getIcon("/ui_icons/", "remove.png"));
    btnRemove.setIcon(IconFactory.getIcon("/ui_icons/", "clear.png"));
    btnRemoveAll.setIcon(IconFactory.getIcon("/ui_icons/", "remove_all.png"));
    
    int height = 50;
    
    btnAdd.setPreferredSize(PosUIManager.getSize(0, height));
    btnSub.setPreferredSize(PosUIManager.getSize(0, height));
    btnRemove.setPreferredSize(PosUIManager.getSize(0, height));
    
    rightControlPanel.add(btnAdd, "growx, wrap");
    rightControlPanel.add(btnSub, "growx, wrap");
    rightControlPanel.add(btnRemove, "growx, wrap");
    rightControlPanel.add(btnRemoveAll, "growx, wrap");
    
    final TicketViewerTable ticketViewerTable = new TicketViewerTable(ticket);
    
    ticketViewerTable.setRowHeight(PosUIManager.getSize(45));
    resizeColumnWidth(ticketViewerTable);
    ticketViewerTable.setSelectionMode(0);
    
    PosScrollPane scrollPane = new PosScrollPane(ticketViewerTable, 20, 31);
    
    Ticket newTicket = new Ticket();
    reorderedItemsTable = new TicketViewerTable(newTicket);
    
    reorderedItemsTable.setRowHeight(PosUIManager.getSize(45));
    resizeColumnWidth(reorderedItemsTable);
    
    reorderedItemsTable.setSelectionMode(0);
    
    PosScrollPane rightScrollPane = new PosScrollPane(reorderedItemsTable, 20, 31);
    

    leftTicketPanel.add(sep1, "North");
    leftTicketPanel.add(scrollPane, "Center");
    
    rightSelectedItemPanel.add(sep2, "North");
    rightSelectedItemPanel.add(rightScrollPane, "Center");
    
    centerPanel.add(leftTicketPanel, "grow");
    centerPanel.add(centerSelectPanel, "");
    centerPanel.add(rightSelectedItemPanel, "grow");
    centerPanel.add(rightControlPanel, "");
    
    btnSelect.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ReorderDialog.this.doAddReOrderTicketItem(ticketViewerTable);
        ticketViewerTable.clearSelection();
      }
      
    });
    btnSendAll.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ReorderDialog.this.doSendAllItems(ticketViewerTable);
        ticketViewerTable.clearSelection();
      }
      
    });
    btnLastOrder.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ReorderDialog.this.doAddLastOrder(ticketViewerTable);
      }
      
    });
    btnAdd.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ReorderDialog.this.doAddReorderTicketItems();
      }
      
    });
    btnSub.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ReorderDialog.this.doSubstractReorderTicketItems();
      }
      
    });
    btnRemove.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        ReorderDialog.this.doRemoveReorderTicketItems();
      }
      
    });
    btnRemoveAll.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        ReorderDialog.this.doRemoveAll();
      }
      
    });
    JPanel buttonPanel = new JPanel(new MigLayout("al center, insets 10 10 10 80", "sg, fill", ""));
    
    PosButton btnDone = new PosButton("DONE");
    
    buttonPanel.add(btnDone, "grow");
    
    btnDone.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doReorder();
      }
      
    });
    buttonPanel.add(new PosButton(new CloseDialogAction(this, Messages.getString("GiftCardDeActiveView.11"))));
    
    add(centerPanel, "Center");
    add(buttonPanel, "South");
  }
  
  public void resizeColumnWidth(JTable table)
  {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(PosUIManager.getSize(250)));
    columnWidth.add(Integer.valueOf(PosUIManager.getSize(30)));
    columnWidth.add(Integer.valueOf(PosUIManager.getSize(30)));
    
    return columnWidth;
  }
  
  public void doReorder() {
    List<TicketItem> ticketItems = reorderedItemsTable.getTicketItems();
    if (ticketItems == null) {
      return;
    }
    setTicketItems(ticketItems);
    setCanceled(false);
    dispose();
  }
  
  private void doAddLastOrder(TicketViewerTable ticketViewerTable) {
    KitchenTicket lastKitchenTicket = KitchenTicketDAO.getInstance().findByLastOrderParentId(ticket.getId());
    List<KitchenTicketItem> kitchenTicketItems = lastKitchenTicket.getTicketItems();
    for (Iterator iterator = kitchenTicketItems.iterator(); iterator.hasNext();) {
      kitchenTicketItem = (KitchenTicketItem)iterator.next();
      
      List<TicketItem> ticketItems = ticketViewerTable.getTicketItems();
      for (iterator2 = ticketItems.iterator(); iterator2.hasNext();) {
        TicketItem ticketItem = (TicketItem)iterator2.next();
        if (ticketItem.getId().equals(kitchenTicketItem.getTicketItemId())) {
          TicketItem newTicketItem = ticketItem.cloneAsNew();
          newTicketItem.calculatePrice();
          reorderedItemsTable.addTicketItem(newTicketItem);
        }
      } }
    KitchenTicketItem kitchenTicketItem;
    Iterator iterator2;
    reorderedItemsTable.repaint();
  }
  
  private void doAddReOrderTicketItem(TicketViewerTable ticketViewerTable)
  {
    try {
      int index = ticketViewerTable.getSelectedRow();
      
      if (index < 0) {
        return;
      }
      index = ticketViewerTable.convertRowIndexToModel(index);
      TicketViewerTableModel ticketViewerTableModel = ticketViewerTable.getModel();
      Object selectedItem = ticketViewerTableModel.get(index);
      if ((selectedItem instanceof TicketItem)) {
        TicketItem ticketItem = (TicketItem)selectedItem;
        TicketItem newTicketItem = ticketItem.cloneAsNew();
        newTicketItem.calculatePrice();
        reorderedItemsTable.addTicketItem(newTicketItem);
        reorderedItemsTable.repaint();
      }
    }
    catch (Exception e1) {
      e1.printStackTrace();
    }
  }
  
  private void doSendAllItems(TicketViewerTable ticketViewerTable)
  {
    try
    {
      List<TicketItem> ticketItems = ticketViewerTable.getTicketItems();
      
      for (Iterator iterator = ticketItems.iterator(); iterator.hasNext();) {
        TicketItem ticketItem = (TicketItem)iterator.next();
        TicketItem newTicketItem = ticketItem.cloneAsNew();
        newTicketItem.calculatePrice();
        reorderedItemsTable.addTicketItem(newTicketItem);
      }
      reorderedItemsTable.repaint();
    }
    catch (Exception e1) {
      e1.printStackTrace();
    }
  }
  
  public List<TicketItem> getTicketItems() {
    return ticketItems;
  }
  
  public void setTicketItems(List<TicketItem> ticketItems) {
    this.ticketItems = ticketItems;
  }
  
  private void doAddReorderTicketItems() {
    int index = reorderedItemsTable.getSelectedRow();
    
    if (index < 0) {
      return;
    }
    index = reorderedItemsTable.convertRowIndexToModel(index);
    TicketViewerTableModel reorderedTicketItemsTableModel = reorderedItemsTable.getModel();
    Object selectedItem = reorderedTicketItemsTableModel.get(index);
    if ((selectedItem instanceof TicketItem)) {
      TicketItem ticketItem = (TicketItem)selectedItem;
      
      ticketItem.setQuantity(Double.valueOf(ticketItem.getQuantity().doubleValue() + 1.0D));
      ticketItem.calculatePrice();
      
      reorderedTicketItemsTableModel.update();
      reorderedItemsTable.getSelectionModel().setSelectionInterval(index, index);
    }
  }
  
  private void doSubstractReorderTicketItems() {
    int index = reorderedItemsTable.getSelectedRow();
    
    if (index < 0) {
      return;
    }
    index = reorderedItemsTable.convertRowIndexToModel(index);
    TicketViewerTableModel reorderedTicketItemsTableModel = reorderedItemsTable.getModel();
    Object selectedItem = reorderedTicketItemsTableModel.get(index);
    if ((selectedItem instanceof TicketItem)) {
      TicketItem ticketItem = (TicketItem)selectedItem;
      
      if (ticketItem.getQuantity().doubleValue() == 1.0D) {
        reorderedTicketItemsTableModel.delete(index);
      }
      else {
        ticketItem.setQuantity(Double.valueOf(ticketItem.getQuantity().doubleValue() - 1.0D));
        ticketItem.calculatePrice();
        reorderedTicketItemsTableModel.update();
        reorderedItemsTable.getSelectionModel().setSelectionInterval(index, index);
      }
    }
  }
  
  private void doRemoveReorderTicketItems() {
    int index = reorderedItemsTable.getSelectedRow();
    
    if (index < 0) {
      return;
    }
    index = reorderedItemsTable.convertRowIndexToModel(index);
    TicketViewerTableModel reorderedTicketItemsTableModel = reorderedItemsTable.getModel();
    Object selectedItem = reorderedTicketItemsTableModel.get(index);
    if ((selectedItem instanceof TicketItem)) {
      reorderedTicketItemsTableModel.delete(index);
    }
  }
  
  private void doRemoveAll() {
    reorderedItemsTable.getModel().removeAll();
    reorderedItemsTable.repaint();
  }
}
