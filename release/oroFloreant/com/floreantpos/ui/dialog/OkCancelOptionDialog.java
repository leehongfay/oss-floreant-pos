package com.floreantpos.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;

















public abstract class OkCancelOptionDialog
  extends POSDialog
{
  private PosButton btnOk;
  private PosButton btnCancel;
  private TitlePanel titlePanel;
  private TransparentPanel contentPanel;
  private TransparentPanel buttonPanel;
  
  public OkCancelOptionDialog()
  {
    super(POSUtil.getFocusedWindow(), "", true);
    init();
    titlePanel.setTitle("");
  }
  
  public OkCancelOptionDialog(String title) {
    super(Application.getPosWindow(), title);
    init();
    titlePanel.setTitle(title);
  }
  
  public OkCancelOptionDialog(Window owner) {
    super(owner, "");
    init();
  }
  
  public OkCancelOptionDialog(Frame owner, boolean model) {
    super(owner, model);
    init();
  }
  
  public OkCancelOptionDialog(Dialog owner, boolean model) {
    super(owner, model);
    init();
  }
  
  public OkCancelOptionDialog(Window owner, String title) {
    super(owner, title);
    init();
    titlePanel.setTitle(title);
  }
  
  private void init() {
    setLayout(new BorderLayout(10, 10));
    setIconImage(Application.getApplicationIcon().getImage());
    setDefaultCloseOperation(0);
    
    titlePanel = new TitlePanel();
    add(titlePanel, "North");
    
    contentPanel = new TransparentPanel();
    contentPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
    add(contentPanel, "Center");
    
    JSeparator jSeparator1 = new JSeparator();
    buttonPanel = new TransparentPanel();
    buttonPanel.setLayout(new FlowLayout());
    btnOk = new PosButton();
    btnCancel = new PosButton();
    
    TransparentPanel southPanel = new TransparentPanel();
    southPanel.setLayout(new BorderLayout());
    
    buttonPanel.setLayout(new FlowLayout());
    



    btnOk.setText(POSConstants.OK.toUpperCase());
    btnOk.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent evt) {
        doOk();
      }
      
    });
    buttonPanel.add(btnOk);
    

    btnCancel.setText(POSConstants.CANCEL.toUpperCase());
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent evt) {
        doCancel();
      }
      
    });
    buttonPanel.add(btnCancel);
    
    southPanel.add(jSeparator1, "North");
    southPanel.add(buttonPanel, "Center");
    
    contentPanel.setLayout(new BorderLayout());
    
    add(southPanel, "South");
  }
  
  public TransparentPanel getButtonPanel() {
    return buttonPanel;
  }
  
  public void setCaption(String caption) {
    titlePanel.setTitle(caption);
  }
  
  public void setOkButtonText(String text) {
    btnOk.setText(text);
  }
  
  public void setCancelButtonText(String text) {
    btnCancel.setText(text);
  }
  
  public void setCancelButtonVisible(boolean visible) {
    btnCancel.setVisible(visible);
  }
  
  public void setOkButtonVisible(boolean visible) {
    btnOk.setVisible(visible);
  }
  
  public JPanel getContentPanel() {
    return contentPanel;
  }
  
  public abstract void doOk();
  
  public void doCancel() {
    setCanceled(true);
    dispose();
  }
}
