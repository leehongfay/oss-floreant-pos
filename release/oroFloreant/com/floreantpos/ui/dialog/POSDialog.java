package com.floreantpos.ui.dialog;

import com.floreantpos.config.AppProperties;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.util.POSUtil;
import java.awt.Dialog;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.Window;
import javax.swing.JDialog;
import javax.swing.JFrame;

















public class POSDialog
  extends JDialog
{
  protected boolean canceled = true;
  
  public POSDialog() throws HeadlessException {
    super(Application.getPosWindow(), true);
    PosWindow posWindow = Application.getPosWindow();
    if (posWindow != null) {
      setIconImage(posWindow.getIconImage());
    }
    
    initUI();
  }
  
  public POSDialog(Dialog owner, boolean modal) {
    super(owner, modal);
    PosWindow posWindow = Application.getPosWindow();
    if (posWindow != null) {
      setIconImage(posWindow.getIconImage());
    }
    initUI();
  }
  
  public POSDialog(Dialog owner, String title, boolean modal, GraphicsConfiguration gc) {
    super(owner, AppProperties.getAppName(), modal, gc);
    PosWindow posWindow = Application.getPosWindow();
    if (posWindow != null) {
      setIconImage(posWindow.getIconImage());
    }
    initUI();
  }
  
  public POSDialog(Dialog owner, String title, boolean modal) {
    super(owner, AppProperties.getAppName(), modal);
    PosWindow posWindow = Application.getPosWindow();
    if (posWindow != null) {
      setIconImage(posWindow.getIconImage());
    }
    initUI();
  }
  
  public POSDialog(Dialog owner, String title) {
    super(owner, AppProperties.getAppName());
    PosWindow posWindow = Application.getPosWindow();
    if (posWindow != null) {
      setIconImage(posWindow.getIconImage());
    }
    initUI();
  }
  
  public POSDialog(Dialog owner) {
    super(owner);
    PosWindow posWindow = Application.getPosWindow();
    if (posWindow != null) {
      setIconImage(posWindow.getIconImage());
    }
    initUI();
  }
  
  public POSDialog(Frame owner, String title, boolean modal, GraphicsConfiguration gc) {
    super(owner, AppProperties.getAppName(), modal, gc);
    PosWindow posWindow = Application.getPosWindow();
    if (posWindow != null) {
      setIconImage(posWindow.getIconImage());
    }
    initUI();
  }
  
  public POSDialog(Frame owner, String title, boolean modal) {
    super(owner, AppProperties.getAppName(), modal);
    PosWindow posWindow = Application.getPosWindow();
    if (posWindow != null) {
      setIconImage(posWindow.getIconImage());
    }
    initUI();
  }
  
  public POSDialog(Frame owner, String title) {
    super(owner, AppProperties.getAppName(), true);
    setIconImage(Application.getPosWindow().getIconImage());
    initUI();
  }
  
  public POSDialog(Frame owner) {
    super(owner);
    setIconImage(Application.getPosWindow().getIconImage());
    initUI();
  }
  
  public POSDialog(Window owner, Dialog.ModalityType modalityType) {
    super(owner, modalityType);
    setIconImage(Application.getPosWindow().getIconImage());
    initUI();
  }
  
  public POSDialog(Window owner, String title, Dialog.ModalityType modalityType, GraphicsConfiguration gc) {
    super(owner, AppProperties.getAppName(), modalityType, gc);
    setIconImage(Application.getPosWindow().getIconImage());
    initUI();
  }
  
  public POSDialog(Window owner, String title, Dialog.ModalityType modalityType) {
    super(owner, AppProperties.getAppName(), modalityType);
    setIconImage(Application.getPosWindow().getIconImage());
    initUI();
  }
  
  public POSDialog(Window owner) {
    super(owner);
    setIconImage(Application.getPosWindow().getIconImage());
    initUI();
  }
  
  public POSDialog(Frame owner, boolean modal) {
    super(owner, modal);
    
    initUI();
    setIconImage(Application.getPosWindow().getIconImage());
  }
  
  public POSDialog(Window owner, String title) {
    this(owner, AppProperties.getAppName(), true);
    
    initUI();
    setIconImage(Application.getPosWindow().getIconImage());
  }
  
  public POSDialog(Window owner, String title, boolean modal) {
    super(owner, AppProperties.getAppName(), modal ? Dialog.ModalityType.APPLICATION_MODAL : Dialog.ModalityType.MODELESS);
    
    initUI();
  }
  
  protected void initUI() {}
  
  public void open()
  {
    canceled = true;
    
    if (isUndecorated()) {
      Window owner = getOwner();
      if ((owner instanceof JFrame)) {
        JFrame frame = (JFrame)owner;
        setLocationRelativeTo(frame.getContentPane());
      }
      else {
        setLocationRelativeTo(owner);
      }
    }
    else
    {
      setLocationRelativeTo(getOwner());
    }
    setVisible(true);
  }
  
  public void openFullScreen() {
    canceled = true;
    if (TerminalConfig.isKioskMode()) {
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      setSize(screenSize);
      setUndecorated(true);
    }
    else {
      setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
    }
    setResizable(true);
    setLocationRelativeTo(POSUtil.getFocusedWindow());
    setVisible(true);
  }
  







  public void openUndecoratedFullScreen()
  {
    openFullScreen();
  }
  
  public boolean isCanceled() {
    return canceled;
  }
  
  public void setCanceled(boolean canceled) {
    this.canceled = canceled;
  }
}
