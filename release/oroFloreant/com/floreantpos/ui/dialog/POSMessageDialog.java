package com.floreantpos.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.config.AppProperties;
import com.floreantpos.main.Application;
import com.floreantpos.swing.PosUIManager;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.apache.log4j.Logger;




























public class POSMessageDialog
{
  public POSMessageDialog() {}
  
  private static Logger logger = Logger.getLogger(Application.class);
  
  private static void showDialog(Component parent, String message, int messageType, int optionType) {
    showDialog(parent, message, messageType, optionType, null);
  }
  
  private static boolean showDialog(Component parent, String message, int messageType, int optionType, String option) {
    JOptionPane optionPane = null;
    if (option != null) {
      optionPane = new JOptionPane(message, messageType, optionType, null, new String[] { option, POSConstants.OK });
    }
    else {
      optionPane = new JOptionPane(message, messageType, optionType);
    }
    Object[] options = optionPane.getComponents();
    for (Object object : options) {
      if ((object instanceof JPanel)) {
        JPanel panel = (JPanel)object;
        Component[] components = panel.getComponents();
        for (Component component : components) {
          if ((component instanceof JButton)) {
            component.setPreferredSize(new Dimension(getPreferredSizewidth, 60));
          }
        }
      }
    }
    
    JDialog dialog = optionPane.createDialog(parent, AppProperties.getAppName());
    dialog.setModal(true);
    dialog.setVisible(true);
    if (option == null) {
      return false;
    }
    Object selectedValue = optionPane.getValue();
    if ((selectedValue != null) && (selectedValue.equals(option))) {
      return true;
    }
    return false;
  }
  
  public static void showMessage(String message) {
    showDialog(Application.getPosWindow(), message, 1, -1);
  }
  
  public static void showMessage(Component parent, String message) {
    showDialog(parent, message, 1, -1);
  }
  
  public static void showError(String message) {
    showDialog(Application.getPosWindow(), message, 0, -1);
  }
  
  public static void showError(Component parent, String message) {
    showDialog(parent, message, 0, -1);
  }
  
  public static boolean showErrorWithOption(Component parent, String message, String option) {
    return showDialog(parent, message, 0, -1, option);
  }
  
  public static void showError(Component parent, String message, Throwable x) {
    logger.error(message, x);
    showDialog(parent, message, 0, -1);
  }
  
  public static int showYesNoQuestionDialog(Component parent, String message, String title) {
    return showYesNoQuestionDialog(parent, message, title, null, null);
  }
  
  public static int showYesNoQuestionDialog(Component parent, String message, String title, String yesButtonText, String noButtonText) {
    JOptionPane optionPane = null;
    if ((yesButtonText != null) && (noButtonText != null)) {
      optionPane = new JOptionPane(message, 3, 1, null, new String[] { yesButtonText, noButtonText });

    }
    else
    {
      optionPane = new JOptionPane(message, 3, 0);
    }
    Object[] options = optionPane.getComponents();
    for (Object object : options) {
      if ((object instanceof JPanel)) {
        JPanel panel = (JPanel)object;
        Component[] components = panel.getComponents();
        for (Component component : components) {
          if ((component instanceof JButton)) {
            component.setPreferredSize(new Dimension(getPreferredSizewidth, 60));
          }
        }
      }
    }
    
    JDialog dialog = optionPane.createDialog(parent, title);
    dialog.setVisible(true);
    
    Object selectedValue = optionPane.getValue();
    if (selectedValue == null) {
      return -1;
    }
    if ((selectedValue instanceof String)) {
      return selectedValue.equals(noButtonText) ? 1 : 0;
    }
    return ((Integer)selectedValue).intValue();
  }
  
  public static boolean showMessageAndPromtToPrint(String msg) {
    return showMessageAndPromtToPrint(null, msg);
  }
  
  public static boolean showMessageAndPromtToPrint(Component parent, String msg)
  {
    JOptionPane promtPane = new JOptionPane(msg, 1, 1, null, new String[] { POSConstants.PRINT, POSConstants.OK.toUpperCase() });
    
    Object[] optionValues = promtPane.getComponents();
    for (Object object : optionValues) {
      if ((object instanceof JPanel)) {
        JPanel panel = (JPanel)object;
        Component[] components = panel.getComponents();
        
        boolean printIcon = true;
        for (Component component : components) {
          if ((component instanceof JButton)) {
            component.setPreferredSize(PosUIManager.getSize(100, 50));
            JButton button = (JButton)component;
            if (printIcon) {
              button.setIcon(IconFactory.getIcon("/ui_icons/", "print_32.png"));
              button.setText("PRINT");
              printIcon = false;
              button.setFocusable(false);
            }
          }
        }
      }
    }
    JDialog dialog = promtPane.createDialog(parent == null ? Application.getPosWindow() : parent, "Message");
    dialog.setDefaultCloseOperation(0);
    dialog.setIconImage(Application.getApplicationIcon().getImage());
    dialog.setVisible(true);
    Object selectedValue = (String)promtPane.getValue();
    
    if (selectedValue.equals(POSConstants.PRINT)) {
      return true;
    }
    return false;
  }
}
