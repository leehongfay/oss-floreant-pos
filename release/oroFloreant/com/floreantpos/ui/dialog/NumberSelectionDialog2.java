package com.floreantpos.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.config.AppProperties;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.NumericKeypad;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.JTextField;


















public class NumberSelectionDialog2
  extends OkCancelOptionDialog
{
  private int defaultValue;
  private JTextField tfNumber;
  private boolean floatingPoint;
  private PosButton incrementQuantitybtn;
  private PosButton decrementQuantitybtn;
  
  public NumberSelectionDialog2()
  {
    super(POSUtil.getFocusedWindow());
    init();
  }
  
  public NumberSelectionDialog2(boolean allowFlotingPoint) {
    super(POSUtil.getFocusedWindow());
    setFloatingPoint(allowFlotingPoint);
    init();
  }
  
  private void init() {
    setResizable(false);
    JPanel contentPane = getContentPanel();
    contentPane.setLayout(new BorderLayout(5, 5));
    JPanel firstpanel = new JPanel(new BorderLayout(5, 0));
    
    if (isFloatingPoint(true)) {
      tfNumber = new DoubleTextField(10);
    }
    else {
      tfNumber = new IntegerTextField(10);
    }
    tfNumber.setHorizontalAlignment(11);
    tfNumber.setFont(tfNumber.getFont().deriveFont(1, PosUIManager.getNumberFieldFontSize()));
    tfNumber.setFocusable(true);
    

    tfNumber.setBackground(Color.WHITE);
    
    firstpanel.add(tfNumber, "Center");
    incrementQuantitybtn = new PosButton("+");
    incrementQuantitybtn.setFont(new Font("Arial", 0, 30));
    incrementQuantitybtn.setFocusable(false);
    
    firstpanel.add(incrementQuantitybtn, "East");
    incrementQuantitybtn.setPreferredSize(PosUIManager.getSize(60, 60));
    decrementQuantitybtn = new PosButton("-");
    decrementQuantitybtn.setPreferredSize(PosUIManager.getSize(60, 60));
    decrementQuantitybtn.setFont(new Font("Arial", 1, 30));
    decrementQuantitybtn.setFocusable(false);
    
    firstpanel.add(decrementQuantitybtn, "West");
    contentPane.add(firstpanel, "North");
    
    JPanel secondpanel = new JPanel(new BorderLayout());
    NumericKeypad numericKeypad = new NumericKeypad();
    secondpanel.add(numericKeypad, "Center");
    
    contentPane.add(secondpanel, "South");
    
    tfNumber.requestFocus();
    setControlButtonsVisible(true);
    buttonAction();
  }
  

  public void doOk()
  {
    if (!validate(tfNumber.getText())) {
      POSMessageDialog.showError(this, POSConstants.INVALID_NUMBER);
      return;
    }
    setCanceled(false);
    dispose();
  }
  
  public void buttonAction() {
    try {
      incrementQuantitybtn.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          double total = Double.parseDouble(tfNumber.getText());
          if ((total == Math.floor(total)) || (!Double.isInfinite(total))) {
            total += 1.0D;
            tfNumber.setText("" + NumberUtil.trimDecilamIfNotNeeded(Double.valueOf(total)));
          }
          
        }
      });
      decrementQuantitybtn.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          double total = Double.parseDouble(tfNumber.getText());
          if ((total == Math.floor(total)) || (!Double.isInfinite(total)))
          {
            total -= 1.0D;
            if (total >= 0.0D) {
              tfNumber.setText("" + NumberUtil.trimDecilamIfNotNeeded(Double.valueOf(total)));
            }
          }
        }
      });
    } catch (NumberFormatException e) {
      e.printStackTrace();
    }
  }
  
  public void setControlButtonsVisible(boolean visible) {
    incrementQuantitybtn.setVisible(visible);
    decrementQuantitybtn.setVisible(visible);
  }
  
  private boolean validate(String str) {
    if (isFloatingPoint(false)) {
      try {
        Double.parseDouble(str);
      } catch (Exception x) {
        return false;
      }
    } else {
      try
      {
        Long.parseLong(str);
      } catch (Exception x) {
        return false;
      }
    }
    return true;
  }
  
  public void setTitle(String title)
  {
    super.setCaption(title);
    super.setTitle(title);
  }
  
  public void setDialogTitle(String title) {
    super.setTitle(title);
  }
  
  public double getValue() {
    return Double.parseDouble(tfNumber.getText());
  }
  
  public void setValue(double value) {
    if (value == 0.0D) {
      if (floatingPoint) {
        tfNumber.setText("0");
        tfNumber.selectAll();
      }
      else {
        tfNumber.setText("");
      }
    } else if (isFloatingPoint(true)) {
      tfNumber.setText(String.valueOf(value));
    }
    else {
      tfNumber.setText(String.valueOf((int)value));
    }
    tfNumber.selectAll();
  }
  
  public boolean isFloatingPoint(boolean b) {
    return floatingPoint;
  }
  
  public void setFloatingPoint(boolean decimalAllowed) {
    floatingPoint = decimalAllowed;
  }
  
  public int getDefaultValue() {
    return defaultValue;
  }
  
  public void setDefaultValue(int defaultValue) {
    this.defaultValue = defaultValue;
    tfNumber.setText(String.valueOf(defaultValue));
  }
  
  public static long takeIntInput(String title) {
    NumberSelectionDialog2 dialog = new NumberSelectionDialog2(false);
    dialog.setTitle(title);
    dialog.pack();
    dialog.open();
    if (dialog.isCanceled()) {
      return -1L;
    }
    return dialog.getValue();
  }
  
  public static long takeIntInput(String title, double initialAmount) {
    NumberSelectionDialog2 dialog = new NumberSelectionDialog2(false);
    dialog.setTitle(title);
    dialog.setValue(initialAmount);
    dialog.pack();
    dialog.open();
    if (dialog.isCanceled()) {
      return -1L;
    }
    return dialog.getValue();
  }
  
  public static double takeDoubleInput(String title, String dialogTitle, double initialAmount) {
    NumberSelectionDialog2 dialog = new NumberSelectionDialog2(true);
    dialog.setValue(initialAmount);
    dialog.setTitle(title);
    dialog.setDialogTitle(dialogTitle);
    dialog.pack();
    dialog.open();
    if (dialog.isCanceled()) {
      return NaN.0D;
    }
    return dialog.getValue();
  }
  
  public static double takeDoubleInput(String title, double initialAmount) {
    NumberSelectionDialog2 dialog = new NumberSelectionDialog2(true);
    dialog.setTitle(title);
    dialog.setValue(initialAmount);
    dialog.pack();
    dialog.open();
    if (dialog.isCanceled()) {
      return -1.0D;
    }
    return dialog.getValue();
  }
  
  public static double show(Component parent, String title, double initialAmount) {
    NumberSelectionDialog2 dialog2 = new NumberSelectionDialog2(true);
    dialog2.setTitle(title);
    dialog2.pack();
    dialog2.setLocationRelativeTo(parent);
    dialog2.setValue(initialAmount);
    dialog2.setVisible(true);
    
    if (dialog2.isCanceled()) {
      return NaN.0D;
    }
    
    return dialog2.getValue();
  }
  
  public static double takeDoubleInput(String title, double initialAmount, boolean floatingPoint) {
    NumberSelectionDialog2 dialog = new NumberSelectionDialog2(true);
    dialog.setTitle(AppProperties.getAppName());
    dialog.setCaption(title);
    dialog.setValue(initialAmount);
    dialog.setFloatingPoint(floatingPoint);
    dialog.pack();
    dialog.open();
    if (dialog.isCanceled()) {
      return -1.0D;
    }
    return dialog.getValue();
  }
  
  public void setUtilityButtonsVisible(boolean visible) {
    incrementQuantitybtn.setVisible(visible);
    decrementQuantitybtn.setVisible(visible);
  }
}
