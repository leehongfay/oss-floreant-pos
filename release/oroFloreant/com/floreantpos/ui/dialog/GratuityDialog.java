package com.floreantpos.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.config.AppProperties;
import com.floreantpos.main.Application;
import com.floreantpos.model.ActionHistory;
import com.floreantpos.model.Gratuity;
import com.floreantpos.model.GratuityPaymentHistory;
import com.floreantpos.model.TipsCashoutReport;
import com.floreantpos.model.TipsCashoutReportData;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.ActionHistoryDAO;
import com.floreantpos.model.dao.GratuityDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.swing.ListComboBoxModel;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.JTableHeader;
import net.miginfocom.swing.MigLayout;























public class GratuityDialog
  extends OkCancelOptionDialog
  implements ActionListener
{
  private JList<User> userJList;
  private JButton btnGo;
  private JLabel lblUserId;
  private JLabel lblUserName;
  private JLabel lblTotalGratuity;
  private JTable tableGratuityViewer;
  private JPanel contentPane;
  private GratuityTableModel gratuityTableModel;
  private PosButton btnGratuityHistory;
  private JLabel lblTipsDue;
  private JLabel lblTipsPaid;
  private PosButton btnGratuityDetails;
  private Map<String, TipsCashoutReportData> itemMap;
  private User currentUser;
  
  public GratuityDialog(Window parent, User assignedUser)
  {
    super(parent, "");
    currentUser = assignedUser;
    setModal(true);
    setTitle(AppProperties.getAppName());
    initComponents();
    UserDAO userDAO = new UserDAO();
    List users = new ArrayList();
    users.add("ALL");
    users.addAll(userDAO.findAll());
    
    userJList.setModel(new ListComboBoxModel(users));
    tableGratuityViewer.getTableHeader().setPreferredSize(PosUIManager.getSize(0, 35));
    tableGratuityViewer.setRowHeight(PosUIManager.getSize(35));
    tableGratuityViewer.setDefaultRenderer(Object.class, new PosTableRenderer());
    tableGratuityViewer.setModel(this.gratuityTableModel = new GratuityTableModel(null));
    
    for (Object user : users) {
      if (((user instanceof User)) && 
        (((User)user).getId().equals(assignedUser.getId()))) {
        userJList.setSelectedValue(user, true);
        break;
      }
    }
    
    btnGo.addActionListener(this);
  }
  
  private void initComponents() {
    setCaption("SERVER GRATUITY");
    contentPane = new JPanel();
    contentPane.setLayout(new MigLayout("fill"));
    



    userJList = new JList();
    
    JPanel leftUserListPanel = new JPanel(new MigLayout("fill"));
    leftUserListPanel.setBorder(BorderFactory.createTitledBorder("Users"));
    leftUserListPanel.setPreferredSize(PosUIManager.getSize(200, 0));
    leftUserListPanel.add(new JScrollPane(userJList), "grow");
    getContentPanel().add(leftUserListPanel, "West");
    userJList.setFixedCellHeight(40);
    
    userJList.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        showGratuity(GratuityDialog.this.getSelectedUser());
      }
    });
    btnGo = new JButton();
    


    btnGratuityHistory = new PosButton();
    btnGratuityHistory.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        GratuityDialog.this.showServerTipsHistory();
      }
    });
    btnGratuityHistory.setText("HISTORY");
    
    btnGratuityDetails = new PosButton();
    btnGratuityDetails.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        GratuityDialog.this.showServerTipsDetails();
      }
    });
    btnGratuityDetails.setText("DETAILS");
    
    getButtonPanel().add(btnGratuityDetails, 0);
    getButtonPanel().add(btnGratuityHistory, 0);
    


    contentPane.add(new JLabel(POSConstants.USER_ID + ":"));
    lblUserId = new JLabel();
    contentPane.add(lblUserId, "wrap");
    
    contentPane.add(new JLabel(POSConstants.USER_NAME + ":"));
    lblUserName = new JLabel();
    contentPane.add(lblUserName, "wrap");
    
    contentPane.add(new JLabel(POSConstants.DETAILS));
    
    contentPane.add(new JSeparator(), "grow,span");
    
    JScrollPane scrollPane1 = new JScrollPane();
    contentPane.add(scrollPane1, "grow,span");
    
    tableGratuityViewer = new JTable();
    scrollPane1.setViewportView(tableGratuityViewer);
    
    contentPane.add(new JSeparator(), "newline,span");
    
    JLabel lblGratuity = new JLabel(POSConstants.TOTAL_GRATUITY + ":");
    lblTotalGratuity = new JLabel();
    lblTipsPaid = new JLabel();
    lblTipsDue = new JLabel();
    
    Font font = new Font(null, 1, 14);
    

    lblTipsDue.setFont(font);
    
    contentPane.add(lblGratuity, "split 2,right,span");
    contentPane.add(lblTotalGratuity, "wrap,gapright 5");
    
    contentPane.add(new JLabel("Paid amount: "), "split 2,right,span");
    contentPane.add(lblTipsPaid, "wrap,gapright 5");
    
    contentPane.add(new JLabel("Due amount: "), "split 2,right,span");
    contentPane.add(lblTipsDue, "wrap,gapright 5");
    
    getContentPanel().add(contentPane);
  }
  
  private void showServerTipsHistory() {
    try {
      ListComboBoxModel model = (ListComboBoxModel)userJList.getModel();
      TipsCashoutReportDialog dialog = new TipsCashoutReportDialog(null, model, getSelectedUser(), true);
      dialog.setCaption(POSConstants.SERVER_TIPS_REPORT);
      dialog.setSize(650, 600);
      dialog.open();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private void showServerTipsDetails() {
    try {
      GratuityDAO gratuityDAO = new GratuityDAO();
      TipsCashoutReport report = gratuityDAO.createReport(null, null, getSelectedUser());
      
      TipsCashoutReportDialog dialog = new TipsCashoutReportDialog(report);
      dialog.setCaption(POSConstants.SERVER_TIPS_REPORT);
      dialog.setSize(650, 600);
      dialog.open();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private class GratuityTableModel extends ListTableModel
  {
    public GratuityTableModel() {
      super(gratuities);
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      TipsCashoutReportData gratuity = (TipsCashoutReportData)rows.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return gratuity.getOwner().getId();
      case 1: 
        return gratuity.getOwner().getFirstName();
      
      case 2: 
        return gratuity.getOwner().getLastName();
      
      case 3: 
        return Double.valueOf(NumberUtil.roundToTwoDigit(gratuity.getAmount().doubleValue()));
      case 4: 
        return Double.valueOf(NumberUtil.roundToTwoDigit(gratuity.getAmount().doubleValue() - gratuity.getTipsPaidAmount().doubleValue()));
      }
      return null;
    }
  }
  
  public void showGratuity(User user)
  {
    GratuityDAO dao = new GratuityDAO();
    List<Gratuity> gratuities = dao.findByUser(user, true);
    
    itemMap = new HashMap();
    for (Gratuity gratuity : gratuities) {
      String ownerId = gratuity.getOwnerId();
      if (ownerId != null)
      {

        TipsCashoutReportData serverTipsData = (TipsCashoutReportData)itemMap.get(ownerId);
        if (serverTipsData == null) {
          TipsCashoutReportData newTipsData = new TipsCashoutReportData();
          newTipsData.setOwner(user);
          newTipsData.setTicketId(gratuity.getTicketId());
          newTipsData.setAmount(gratuity.getTransactionTipsAmount());
          newTipsData.setTipsPaidAmount(gratuity.getTipsPaidAmount());
          itemMap.put(ownerId, newTipsData);
        }
        else {
          serverTipsData.setAmount(Double.valueOf(serverTipsData.getAmount().doubleValue() + gratuity.getTransactionTipsAmount().doubleValue()));
          serverTipsData.setTipsPaidAmount(Double.valueOf(serverTipsData.getTipsPaidAmount().doubleValue() + gratuity.getTipsPaidAmount().doubleValue()));
        }
      }
    }
    double totalGratuity = 0.0D;
    double paidTips = 0.0D;
    for (Iterator iterator = itemMap.values().iterator(); iterator.hasNext();) {
      TipsCashoutReportData data = (TipsCashoutReportData)iterator.next();
      if (data.getAmount().doubleValue() - data.getTipsPaidAmount().doubleValue() <= 0.0D) {
        iterator.remove();
      }
      else {
        totalGratuity += data.getAmount().doubleValue();
        paidTips += data.getTipsPaidAmount().doubleValue();
      }
    }
    if (user != null) {
      lblUserId.setText(String.valueOf(user.getId()));
      lblUserName.setText(user.getFirstName() + " " + user.getLastName());
    }
    else {
      lblUserId.setText("ALL");
      lblUserName.setText("ALL");
    }
    lblTotalGratuity.setText(NumberUtil.formatNumber(Double.valueOf(totalGratuity)));
    lblTipsPaid.setText(NumberUtil.formatNumber(Double.valueOf(paidTips)));
    lblTipsDue.setText(NumberUtil.formatNumber(Double.valueOf(totalGratuity - paidTips)));
    
    gratuityTableModel.setRows(new ArrayList(itemMap.values()));
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    if (POSConstants.GO.equalsIgnoreCase(actionCommand)) {
      showGratuity(getSelectedUser());
    }
  }
  















  private User getSelectedUser()
  {
    Object selectedObj = userJList.getSelectedValue();
    if ((selectedObj instanceof User)) {
      return (User)selectedObj;
    }
    return null;
  }
  
  public void doOk()
  {
    try {
      User user = getSelectedUser();
      TipsCashoutReportData tipsData = null;
      
      if (user == null) {
        int selectedRow = tableGratuityViewer.getSelectedRow();
        if (selectedRow == -1) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select tips entry which you want to pay.");
          return;
        }
        tipsData = (TipsCashoutReportData)gratuityTableModel.getRowData(selectedRow);
        if (tipsData == null) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select tips entry which you want to pay.");
          return;
        }
        user = UserDAO.getInstance().get(tipsData.getOwner().getId());
      }
      
      List<Gratuity> rows = GratuityDAO.getInstance().findByUser(user, false);
      if ((rows == null) || (rows.size() <= 0)) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Grautity not found.");
        return;
      }
      if ((tipsData == null) && 
        (gratuityTableModel.getRowCount() > 0)) {
        tipsData = (TipsCashoutReportData)gratuityTableModel.getRowData(0);
      }
      double tipsDueAmount = 0.0D;
      if (tipsData != null) {
        try {
          tipsDueAmount = Double.valueOf(tipsData.getAmount().doubleValue() - tipsData.getTipsPaidAmount().doubleValue()).doubleValue();
        }
        catch (Exception localException) {}
      }
      if (tipsDueAmount == 0.0D) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Nothing to pay.");
        return;
      }
      tipsDueAmount = NumberUtil.roundToTwoDigit(tipsDueAmount);
      Double cashTipsPayAmount = Double.valueOf(NumberSelectionDialog2.takeDoubleInput("Enter tips amount ", tipsDueAmount));
      if (cashTipsPayAmount.doubleValue() == -1.0D)
        return;
      if (cashTipsPayAmount.doubleValue() < 0.0D) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Negetive amount not allowed.");
        return;
      }
      
      if (cashTipsPayAmount.doubleValue() > tipsDueAmount) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Exceed tips amount cannot be accepted.");
        return;
      }
      double totalInputTipsAmount = cashTipsPayAmount.doubleValue();
      List<Gratuity> gratuityList = new ArrayList();
      List<GratuityPaymentHistory> gratuityPaymentHistoryList = new ArrayList();
      for (Gratuity gratuity : rows)
        if (!gratuity.isPaid().booleanValue())
        {

          gratuityList.add(gratuity);
          double tipsTenderedAmount = gratuity.getAmount().doubleValue();
          double paidAmount = gratuity.getTipsPaidAmount().doubleValue();
          gratuity.calculateTransactionTips();
          double tipsTransactionAmount = gratuity.getTransactionTipsAmount().doubleValue();
          double tipsDue = NumberUtil.roundToTwoDigit(tipsTransactionAmount - paidAmount);
          
          GratuityPaymentHistory paymentHistory = new GratuityPaymentHistory();
          paymentHistory.setAmount(Double.valueOf(tipsDue));
          paymentHistory.setTenderedAmount(Double.valueOf(totalInputTipsAmount));
          paymentHistory.setCashDrawer(user.getActiveDrawerPullReport());
          paymentHistory.setTransactionTime(new Date());
          paymentHistory.setGratuity(gratuity);
          paymentHistory.setPaidBy(currentUser);
          
          gratuityPaymentHistoryList.add(paymentHistory);
          
          if (cashTipsPayAmount.doubleValue() == tipsDue) {
            gratuity.setTipsPaidAmount(Double.valueOf(paidAmount + tipsDue));
            if (tipsTenderedAmount != tipsTransactionAmount) break;
            gratuity.setPaid(Boolean.valueOf(true)); break;
          }
          
          if (cashTipsPayAmount.doubleValue() > tipsDue) {
            gratuity.setTipsPaidAmount(Double.valueOf(paidAmount + tipsDue));
            if (tipsTenderedAmount == tipsTransactionAmount)
              gratuity.setPaid(Boolean.valueOf(true));
            cashTipsPayAmount = Double.valueOf(cashTipsPayAmount.doubleValue() - tipsDue);
          }
          else {
            paymentHistory.setAmount(cashTipsPayAmount);
            gratuity.setTipsPaidAmount(Double.valueOf(paidAmount + cashTipsPayAmount.doubleValue()));
            gratuity.setPaid(Boolean.valueOf(false));
            break;
          }
        }
      new GratuityDAO().payGratuities(gratuityList, gratuityPaymentHistoryList);
      String actionMessage = POSConstants.PAY_TIPS;
      ActionHistoryDAO.getInstance().saveHistory(Application.getCurrentUser(), ActionHistory.PAY_TIPS, actionMessage);
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Successfully complete");
      showGratuity(getSelectedUser());
    } catch (PosException ex) {
      BOMessageDialog.showError(contentPane, ex.getMessage(), ex);
    }
  }
}
