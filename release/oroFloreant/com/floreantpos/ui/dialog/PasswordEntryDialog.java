package com.floreantpos.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import org.apache.commons.lang.StringUtils;
import org.hibernate.exception.JDBCConnectionException;


















public class PasswordEntryDialog
  extends OkCancelOptionDialog
  implements ActionListener
{
  private JPasswordField tfPassword;
  private JLabel statusLabel;
  private PosButton btnClear;
  private PosButton btnClearAll;
  private boolean autoLogOffMode;
  private User user;
  
  public PasswordEntryDialog()
  {
    super(Application.getPosWindow(), true);
    init();
  }
  
  public PasswordEntryDialog(Frame parent) {
    super(parent, true);
    init();
  }
  
  private void init()
  {
    btnClear = new PosButton();
    btnClear.setText(Messages.getString("PasswordEntryDialog.11"));
    
    btnClearAll = new PosButton();
    btnClearAll.setText(Messages.getString("PasswordEntryDialog.12"));
    
    JPanel contentPane = new JPanel(new BorderLayout(10, 10));
    getContentPanel().add(contentPane);
    
    JPanel inputPanel = createInputPanel();
    contentPane.add(inputPanel, "North");
    
    JPanel keyboardPanel = createKeyboardPanel();
    contentPane.add(keyboardPanel);
  }
  
  private JPanel createInputPanel() {
    JPanel inputPanel = new JPanel(new BorderLayout(5, 5));
    
    tfPassword = new JPasswordField();
    tfPassword.setFont(tfPassword.getFont().deriveFont(1, PosUIManager.getNumberFieldFontSize()));
    tfPassword.setFocusable(true);
    tfPassword.requestFocus();
    tfPassword.setBackground(Color.WHITE);
    tfPassword.addKeyListener(new KeyListener()
    {
      public void keyTyped(KeyEvent e) {}
      



      public void keyReleased(KeyEvent e)
      {
        String secretKey = PasswordEntryDialog.this.getPasswordAsString();
        Integer defaultPassLength = Application.getInstance().getTerminal().getDefaultPassLength();
        if ((secretKey != null) && (secretKey.length() == defaultPassLength.intValue())) {
          statusLabel.setText("");
          if (PasswordEntryDialog.this.checkLogin(secretKey)) {
            setCanceled(false);
            dispose();
          }
        }
      }
      


      public void keyPressed(KeyEvent e) {}
    });
    inputPanel.add(tfPassword, "North");
    
    statusLabel = new JLabel();
    statusLabel.setHorizontalAlignment(0);
    inputPanel.add(statusLabel);
    
    return inputPanel;
  }
  
  private JPanel createKeyboardPanel() {
    JPanel buttonPanel = new JPanel(new GridLayout(0, 3, 5, 5));
    
    String[][] numbers = { { "7", "8", "9" }, { "4", "5", "6" }, { "1", "2", "3" }, { "0" } };
    String[][] iconNames = { { "7.png", "8.png", "9.png" }, { "4.png", "5.png", "6.png" }, { "1.png", "2.png", "3.png" }, { "0.png" } };
    

    Dimension size = PosUIManager.getSize_w120_h70();
    
    for (int i = 0; i < numbers.length; i++) {
      for (int j = 0; j < numbers[i].length; j++) {
        String buttonText = String.valueOf(numbers[i][j]);
        
        PosButton posButton = new PosButton();
        posButton.setAction(loginAction);
        ImageIcon icon = IconFactory.getIcon("/ui_icons/", iconNames[i][j]);
        if (icon != null) {
          posButton.setIcon(icon);
        }
        else {
          posButton.setText(buttonText);
        }
        
        posButton.setPreferredSize(size);
        posButton.setIconTextGap(0);
        posButton.setActionCommand(buttonText);
        buttonPanel.add(posButton);
      }
    }
    ImageIcon clearIcon = IconFactory.getIcon("/ui_icons/", "clear.png");
    btnClear.setIcon(clearIcon);
    btnClear.setIconTextGap(0);
    
    ImageIcon clearAllIcon = IconFactory.getIcon("/ui_icons/", "clear.png");
    btnClearAll.setIcon(clearAllIcon);
    btnClearAll.setIconTextGap(0);
    
    buttonPanel.add(btnClear);
    buttonPanel.add(btnClearAll);
    
    btnClear.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PasswordEntryDialog.this.doClear();
      }
      
    });
    btnClearAll.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PasswordEntryDialog.this.doClearAll();
      }
      
    });
    return buttonPanel;
  }
  

















  public void doOk()
  {
    if (checkLogin(getPasswordAsString())) {
      setCanceled(false);
      dispose();
    }
  }
  
  public void doCancel()
  {
    user = null;
    setCanceled(true);
    dispose();
  }
  
  private void doClearAll() {
    statusLabel.setText("");
    tfPassword.setText("");
  }
  
  private void doClear() {
    statusLabel.setText("");
    String passwordAsString = getPasswordAsString();
    if (StringUtils.isNotEmpty(passwordAsString)) {
      passwordAsString = passwordAsString.substring(0, passwordAsString.length() - 1);
    }
    tfPassword.setText(passwordAsString);
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    
    if (POSConstants.CANCEL.equalsIgnoreCase(actionCommand)) {
      doCancel();
    }
    else if (POSConstants.OK.equalsIgnoreCase(actionCommand)) {
      doOk();

    }
    else if (StringUtils.isNotEmpty(actionCommand)) {
      tfPassword.setText(getPasswordAsString() + actionCommand);
    }
  }
  
  public void setTitle(String title)
  {
    super.setCaption(title);
    
    super.setTitle(title);
  }
  
  public void setDialogTitle(String title) {
    super.setTitle(title);
  }
  
  private String getPasswordAsString() {
    return new String(tfPassword.getPassword());
  }
  
  public static void main(String[] args) {
    PasswordEntryDialog dialog2 = new PasswordEntryDialog();
    dialog2.pack();
    dialog2.setVisible(true);
  }
  
  public static String show(Component parent, String title) {
    PasswordEntryDialog dialog2 = new PasswordEntryDialog();
    dialog2.setTitle(title);
    dialog2.pack();
    dialog2.setLocationRelativeTo(parent);
    dialog2.setVisible(true);
    
    if (dialog2.isCanceled()) {
      return null;
    }
    
    return dialog2.getPasswordAsString();
  }
  
  public static User getUser(Component parent, String title) {
    return getUser(parent, title, title);
  }
  
  public static User getUser(Component parent, String windowTitle, String title) {
    PasswordEntryDialog dialog2 = new PasswordEntryDialog();
    dialog2.setTitle(title);
    dialog2.setDialogTitle(windowTitle);
    dialog2.pack();
    dialog2.setLocationRelativeTo(parent);
    dialog2.setVisible(true);
    if (dialog2.isCanceled()) {
      return null;
    }
    
    return dialog2.getUser();
  }
  








  private synchronized boolean checkLogin(String secretKey)
  {
    try
    {
      user = UserDAO.getInstance().findUserBySecretKey(secretKey);
    } catch (JDBCConnectionException e) {
      POSMessageDialog.showError(this, "Database connection lost, please try again");
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage());
      if (RootView.getInstance().getCurrentViewName() == null) {
        Application.getInstance().shutdownPOS();
      }
      return false;
    }
    
    if (user == null) {
      statusLabel.setText(Messages.getString("PasswordEntryDialog.30"));
      return false;
    }
    if (user.getType() == null) {
      statusLabel.setText(Messages.getString("PasswordEntryDialog.5"));
      return false;
    }
    List<User> linkedUserList = user.getLinkedUser();
    
    if (linkedUserList != null) {
      for (User roleUser : linkedUserList) {
        if (roleUser.isClockedIn().booleanValue()) {
          user = roleUser;
          break;
        }
      }
    }
    
    if (isAutoLogOffMode()) {
      String viewName = RootView.getInstance().getCurrentViewName();
      
      if (viewName.equals("TABLE_MAP")) {
        if (!user.hasPermission(UserPermission.CREATE_TICKET)) {
          statusLabel.setText(Messages.getString("PasswordEntryDialog.4"));
          return false;


        }
        



      }
      else if (viewName.equals("ALL FUNCTIONS")) {
        if (!user.hasPermission(UserPermission.ALL_FUNCTIONS)) {
          statusLabel.setText(Messages.getString("PasswordEntryDialog.4"));
          return false;
        }
      }
      else if (viewName.equals("ORDER_VIEW")) {
        if ((!OrderView.getInstance().getCurrentTicket().getOwner().getId().equals(user.getId())) && (
          (!user.hasPermission(UserPermission.CREATE_TICKET)) || ((!user.hasPermission(UserPermission.PERFORM_ADMINISTRATIVE_TASK)) && (!user.hasPermission(UserPermission.PERFORM_MANAGER_TASK))))) {
          statusLabel.setText(Messages.getString("PasswordEntryDialog.4"));
          return false;
        }
        
      }
      else if ((viewName.equals("KD")) && 
        (!user.hasPermission(UserPermission.KITCHEN_DISPLAY))) {
        statusLabel.setText(Messages.getString("PasswordEntryDialog.4"));
        return false;
      }
    }
    else
    {
      Object orderTypes = Application.getInstance().getOrderTypes();
      String defaultView = TerminalConfig.getDefaultView();
      if (orderTypes != null) {
        for (OrderType orderType : (List)orderTypes) {
          if ((defaultView != null) && (defaultView.equals(orderType.getName())) && 
            (!user.hasPermission(UserPermission.CREATE_TICKET))) {
            statusLabel.setText(Messages.getString("PasswordEntryDialog.4"));
            return false;
          }
        }
      }
      























      if ((defaultView != null) && (defaultView.equals("KD")) && 
        (!user.hasPermission(UserPermission.KITCHEN_DISPLAY))) {
        statusLabel.setText(Messages.getString("PasswordEntryDialog.4"));
        return false;
      }
    }
    

    setAutoLogOffMode(false);
    return true;
  }
  
  Action loginAction = new AbstractAction() {
    public void actionPerformed(ActionEvent e) {
      tfPassword.setText(PasswordEntryDialog.this.getPasswordAsString() + e.getActionCommand());
      
      String secretKey = PasswordEntryDialog.this.getPasswordAsString();
      if ((secretKey != null) && (secretKey.length() == Application.getInstance().getTerminal().getDefaultPassLength().intValue())) {
        statusLabel.setText("");
        if (PasswordEntryDialog.this.checkLogin(secretKey)) {
          setCanceled(false);
          dispose();
        }
      }
    }
  };
  
  public User getUser() {
    return user;
  }
  


  public boolean isAutoLogOffMode()
  {
    return autoLogOffMode;
  }
  


  public void setAutoLogOffMode(boolean autoLogOffMode)
  {
    this.autoLogOffMode = autoLogOffMode;
  }
}
