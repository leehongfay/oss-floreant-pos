package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.CashBreakdown;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.Currency;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketDiscount;
import com.floreantpos.model.User;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.NumericKeypad;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;





















public class MultiCurrencyTenderDialog
  extends OkCancelOptionDialog
{
  private List<Currency> currencyList;
  private double dueAmount;
  private double totalTenderedAmount;
  private double totalCashBackAmount;
  private List<CurrencyRow> currencyRows = new ArrayList();
  private Ticket ticket;
  
  public MultiCurrencyTenderDialog(Ticket ticket, List<Currency> currencyList) {
    super(POSUtil.getFocusedWindow());
    this.ticket = ticket;
    this.currencyList = currencyList;
    dueAmount = ticket.getDueAmount().doubleValue();
    init();
  }
  
  public MultiCurrencyTenderDialog(List<Ticket> tickets, List<Currency> currencyList) {
    super(POSUtil.getFocusedWindow());
    this.currencyList = currencyList;
    this.ticket = ((Ticket)tickets.get(tickets.size() - 1));
    dueAmount = 0.0D;
    for (Ticket ticket : tickets) {
      dueAmount += ticket.getDueAmount().doubleValue();
    }
    init();
  }
  
  public MultiCurrencyTenderDialog(Ticket ticket, List<Currency> currencyList, double dueAmount) {
    super(POSUtil.getFocusedWindow());
    this.ticket = ticket;
    this.currencyList = currencyList;
    this.dueAmount = dueAmount;
    init();
  }
  
  private void init() {
    JPanel contentPane = getContentPanel();
    setOkButtonText(POSConstants.SAVE_BUTTON_TEXT);
    setTitle(Messages.getString("MultiCurrencyTenderDialog.0"));
    setCaption(Messages.getString("MultiCurrencyTenderDialog.1") + CurrencyUtil.getCurrencySymbol() + dueAmount);
    setResizable(true);
    
    MigLayout layout = new MigLayout("inset 0", "[grow,fill]", "[grow,fill]");
    contentPane.setLayout(layout);
    
    JPanel inputPanel = new JPanel(new MigLayout("fill,ins 0", "[fill][right]30px[120px,grow,fill,right][120px,grow,fill,right][][]", ""));
    
    inputPanel.add(getJLabel("Currency", 1, 16, 10));
    inputPanel.add(getJLabel("Remaining", 1, 16, 0), "gapleft 20");
    inputPanel.add(getJLabel("Tender", 1, 16, 11), "center");
    inputPanel.add(getJLabel("Cash Back", 1, 16, 11), "center");
    
    for (Currency currency : currencyList) {
      CurrencyRow item = new CurrencyRow(currency, dueAmount);
      inputPanel.add(currencyName, "newline");
      inputPanel.add(lblRemainingBalance);
      inputPanel.add(tfTenderdAmount, "grow");
      inputPanel.add(tfCashBackAmount, "grow");
      


      currencyRows.add(item);
    }
    contentPane.add(inputPanel, "cell 0 2,alignx left,aligny top");
    
    NumericKeypad numericKeypad = new NumericKeypad();
    contentPane.add(new JSeparator(), "newline, gapbottom 5,gaptop 10");
    contentPane.add(numericKeypad, "newline");
  }
  
  private JLabel getJLabel(String text, int bold, int fontSize, int align) {
    JLabel lbl = new JLabel(text);
    lbl.setFont(lbl.getFont().deriveFont(bold, PosUIManager.getSize(fontSize)));
    lbl.setHorizontalAlignment(align);
    return lbl;
  }
  
  private DoubleTextField getDoubleTextField(String text, int bold, int fontSize, int align) {
    DoubleTextField tf = new DoubleTextField();
    tf.setText(text);
    tf.setFont(tf.getFont().deriveFont(bold, PosUIManager.getSize(fontSize)));
    tf.setHorizontalAlignment(align);
    tf.setBackground(Color.WHITE);
    return tf;
  }
  
  public void doOk()
  {
    updateView();
    
    if (!isValidAmount()) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Invalid Amount");
      return;
    }
    
    User currentUser = Application.getCurrentUser();
    CashDrawer cashDrawer = currentUser.getActiveDrawerPullReport();
    
    for (CurrencyRow rowItem : currencyRows) {
      CashBreakdown item = cashDrawer.getCurrencyBalance(currency);
      if (item == null) {
        item = new CashBreakdown();
        item.setCurrency(currency);
        cashDrawer.addTocashBreakdownList(item);
      }
      double tenderAmount = rowItem.getTenderAmount();
      double cashBackAmount = rowItem.getCashBackAmount();
      
      if (tenderAmount > 0.0D) {
        ticket.addProperty(currency.getName(), String.valueOf(tenderAmount));
      }
      if (cashBackAmount > 0.0D) {
        ticket.addProperty(currency.getName() + "_CASH_BACK", String.valueOf(cashBackAmount));
      }
      item.setBalance(Double.valueOf(NumberUtil.roundToThreeDigit(item.getBalance().doubleValue() + tenderAmount - cashBackAmount)));
    }
    
    setCanceled(false);
    dispose();
  }
  
  private boolean isValidAmount() {
    double remainingBalance = dueAmount - (totalTenderedAmount - totalCashBackAmount);
    if ((totalTenderedAmount <= 0.0D) || (remainingBalance < 0.0D)) {
      return false;
    }
    
    double toleranceAmount = CurrencyUtil.getMainCurrency().getTolerance().doubleValue();
    if (remainingBalance > toleranceAmount) {
      return true;
    }
    
    if (!isTolerable()) {
      return false;
    }
    
    return true;
  }
  
  private boolean isTolerable() {
    double tolerance = CurrencyUtil.getMainCurrency().getTolerance().doubleValue();
    double diff = dueAmount - (totalTenderedAmount - totalCashBackAmount);
    if (diff <= tolerance) {
      if (diff > 0.0D) {
        doAddToleranceToTicketDiscount(diff);
      }
      return true;
    }
    return false;
  }
  
  private void doAddToleranceToTicketDiscount(double discountAmount) {
    TicketDiscount ticketDiscount = new TicketDiscount();
    ticketDiscount.setName("Tolerance");
    ticketDiscount.setValue(Double.valueOf(NumberUtil.roundToThreeDigit(discountAmount)));
    ticketDiscount.setAutoApply(Boolean.valueOf(true));
    ticketDiscount.setTicket(ticket);
    ticket.addTodiscounts(ticketDiscount);
    ticket.calculatePrice();
  }
  
  private void updateView() {
    totalTenderedAmount = 0.0D;
    totalCashBackAmount = 0.0D;
    
    for (CurrencyRow rowItem : currencyRows) {
      double tenderAmount = rowItem.getTenderAmount();
      double cashBackAmount = rowItem.getCashBackAmount();
      totalTenderedAmount += tenderAmount / currency.getExchangeRate().doubleValue();
      totalCashBackAmount += cashBackAmount / currency.getExchangeRate().doubleValue();
    }
    for (CurrencyRow currInput : currencyRows) {
      double remainingBalance = (dueAmount - (totalTenderedAmount - totalCashBackAmount)) * currency.getExchangeRate().doubleValue();
      currInput.setRemainingBalance(remainingBalance);
    }
  }
  
  public double getTenderedAmount() {
    return totalTenderedAmount;
  }
  
  public double getChangeDueAmount() {
    return totalTenderedAmount - dueAmount;
  }
  
  class CurrencyRow
    implements ActionListener, FocusListener
  {
    Currency currency;
    DoubleTextField tfTenderdAmount;
    DoubleTextField tfCashBackAmount;
    double remainingBalance;
    JLabel lblRemainingBalance;
    JLabel currencyName;
    PosButton btnExact = new PosButton(Messages.getString("MultiCurrencyTenderDialog.17"));
    PosButton btnRound = new PosButton(Messages.getString("MultiCurrencyTenderDialog.18"));
    
    public CurrencyRow(Currency currency, double dueAmountInMainCurrency) {
      this.currency = currency;
      remainingBalance = (currency.getExchangeRate().doubleValue() * dueAmountInMainCurrency);
      
      lblRemainingBalance = MultiCurrencyTenderDialog.this.getJLabel(NumberUtil.format3DigitNumber(Double.valueOf(remainingBalance)), 0, 16, 4);
      currencyName = MultiCurrencyTenderDialog.this.getJLabel(currency.getName(), 0, 16, 10);
      tfTenderdAmount = MultiCurrencyTenderDialog.this.getDoubleTextField("", 0, 16, 4);
      tfCashBackAmount = MultiCurrencyTenderDialog.this.getDoubleTextField("", 0, 16, 4);
      
      tfTenderdAmount.addFocusListener(this);
      tfCashBackAmount.addFocusListener(this);
      btnExact.addActionListener(this);
      btnRound.addActionListener(this);
    }
    
    double getTenderAmount() {
      double tenderAmount = tfTenderdAmount.getDouble();
      if (Double.isNaN(tenderAmount)) {
        return 0.0D;
      }
      
      return tenderAmount;
    }
    
    double getCashBackAmount() {
      double cashBackAmount = tfCashBackAmount.getDouble();
      if (Double.isNaN(cashBackAmount)) {
        return 0.0D;
      }
      
      return cashBackAmount;
    }
    
    void setRemainingBalance(double remainingBalance) {
      this.remainingBalance = remainingBalance;
      lblRemainingBalance.setText(NumberUtil.format3DigitNumber(Double.valueOf(remainingBalance)));
    }
    
    double getRemainingBalance() {
      return remainingBalance;
    }
    
    public void actionPerformed(ActionEvent e)
    {
      Object source = e.getSource();
      if (source == btnExact) {
        tfTenderdAmount.setText(String.valueOf(NumberUtil.roundToThreeDigit(remainingBalance)));
      }
      else {
        tfTenderdAmount.setText(String.valueOf(NumberUtil.roundToThreeDigit(Math.ceil(remainingBalance))));
      }
    }
    


    public void focusGained(FocusEvent e) {}
    

    public void focusLost(FocusEvent e)
    {
      MultiCurrencyTenderDialog.this.updateView();
    }
  }
}
