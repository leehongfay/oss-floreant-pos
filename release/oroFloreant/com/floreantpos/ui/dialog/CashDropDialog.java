package com.floreantpos.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import com.floreantpos.model.CashDropTransaction;
import com.floreantpos.model.PaymentType;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.CashDropTransactionDAO;
import com.floreantpos.swing.POSLabel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import net.miginfocom.swing.MigLayout;


















public class CashDropDialog
  extends POSDialog
{
  private List<CashDropTransaction> cashDropList;
  private DefaultListSelectionModel selectionModel;
  private CashDropTableModel tableModel;
  private String currencySymbol;
  private PosButton btnClose;
  private PosButton btnDeleteSelected;
  private PosButton btnDown;
  private PosButton btnNewCashDrop;
  private PosButton btnUp;
  private JScrollPane jScrollPane1;
  private JSeparator jSeparator1;
  private POSLabel lblActiveCashDrop;
  private TransparentPanel midPanel;
  private JTable tableCashDrops;
  private TitlePanel titlePanel1;
  private TransparentPanel transparentPanel1;
  private TransparentPanel transparentPanel2;
  private TransparentPanel transparentPanel3;
  private User currentUser;
  private Terminal drawerTerminal;
  
  public CashDropDialog(User currentUser, Terminal drawerTerminal)
  {
    super(Application.getPosWindow(), true);
    this.currentUser = currentUser;
    this.drawerTerminal = drawerTerminal;
    
    initComponents();
    
    currencySymbol = CurrencyUtil.getCurrencySymbol();
    lblActiveCashDrop.setText("");
    TitledBorder titledBorder = new TitledBorder(Messages.getString("CashDropDialog.1") + drawerTerminal.getName());
    titledBorder.setTitleJustification(2);
    midPanel.setBorder(titledBorder);
    
    selectionModel = new DefaultListSelectionModel();
    selectionModel.setSelectionMode(0);
    tableCashDrops.setSelectionModel(selectionModel);
    
    tableCashDrops.setTableHeader(null);
    tableCashDrops.setDefaultRenderer(Object.class, new TableRenderer());
    tableModel = new CashDropTableModel();
    tableCashDrops.setModel(tableModel);
    
    setTitle(Messages.getString("CashDropDialog.2"));
  }
  
  public void initDate() throws Exception {
    CashDropTransactionDAO dao = new CashDropTransactionDAO();
    
    cashDropList = dao.findUnsettled(drawerTerminal, null);
    tableModel.fireTableDataChanged();
  }
  
  private void initComponents() {
    titlePanel1 = new TitlePanel();
    transparentPanel1 = new TransparentPanel();
    jSeparator1 = new JSeparator();
    transparentPanel3 = new TransparentPanel(new MigLayout("al center", "sg, fill", ""));
    btnNewCashDrop = new PosButton();
    btnDeleteSelected = new PosButton();
    btnClose = new PosButton();
    midPanel = new TransparentPanel();
    transparentPanel2 = new TransparentPanel();
    btnUp = new PosButton();
    btnDown = new PosButton();
    lblActiveCashDrop = new POSLabel();
    jScrollPane1 = new JScrollPane();
    tableCashDrops = new JTable();
    tableCashDrops.setRowHeight(PosUIManager.getSize(40));
    
    getContentPane().setLayout(new BorderLayout(5, 5));
    
    titlePanel1.setTitle(Messages.getString("CashDropDialog.3"));
    getContentPane().add(titlePanel1, "North");
    
    transparentPanel1.setLayout(new BorderLayout(5, 5));
    transparentPanel1.add(jSeparator1, "North");
    
    btnNewCashDrop.setText(Messages.getString("CashDropDialog.4"));
    btnNewCashDrop.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        CashDropDialog.this.btnNewCashDropActionPerformed(evt);
      }
      
    });
    transparentPanel3.add(btnNewCashDrop);
    
    btnDeleteSelected.setText(Messages.getString("CashDropDialog.5"));
    btnDeleteSelected.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        CashDropDialog.this.btnDeleteSelectedActionPerformed(evt);
      }
      
    });
    transparentPanel3.add(btnDeleteSelected);
    
    btnClose.setText(Messages.getString("CashDropDialog.6"));
    btnClose.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        CashDropDialog.this.btnCloseActionPerformed(evt);
      }
      
    });
    transparentPanel3.add(btnClose);
    
    transparentPanel1.add(transparentPanel3, "Center");
    
    getContentPane().add(transparentPanel1, "South");
    
    midPanel.setLayout(new BorderLayout(1, 5));
    
    transparentPanel2.setLayout(new GridLayout(0, 1, 2, 2));
    
    transparentPanel2.setBorder(BorderFactory.createEmptyBorder(5, 1, 10, 5));
    btnUp.setIcon(IconFactory.getIcon("/ui_icons/", "up.png"));
    btnUp.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        CashDropDialog.this.doScrollUp(evt);
      }
      
    });
    transparentPanel2.add(btnUp);
    
    btnDown.setIcon(IconFactory.getIcon("/ui_icons/", "down.png"));
    btnDown.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        CashDropDialog.this.doScrollDown(evt);
      }
      
    });
    transparentPanel2.add(btnDown);
    
    midPanel.add(transparentPanel2, "East");
    
    lblActiveCashDrop.setHorizontalAlignment(0);
    lblActiveCashDrop.setText(Messages.getString("CashDropDialog.11"));
    midPanel.add(lblActiveCashDrop, "North");
    
    tableCashDrops.setModel(new DefaultTableModel(new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null } }, new String[] { "Title 1", "Title 2", "Title 3", "Title 4" }));
    

    jScrollPane1.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(5, 10, 10, 10), jScrollPane1.getBorder()));
    jScrollPane1.setViewportView(tableCashDrops);
    
    midPanel.add(jScrollPane1, "Center");
    
    getContentPane().add(midPanel, "Center");
    
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    int width = PosUIManager.getSize(606);
    int height = PosUIManager.getSize(472);
    setBounds((width - width) / 2, (height - height) / 2, width, height);
  }
  
  private void doScrollDown(ActionEvent evt) {
    if (cashDropList == null) {
      return;
    }
    int selectedRow = tableCashDrops.getSelectedRow();
    if (selectedRow < 0) {
      selectedRow = 0;
    }
    else if (selectedRow >= cashDropList.size() - 1) {
      selectedRow = 0;
    }
    else {
      selectedRow++;
    }
    
    selectionModel.setLeadSelectionIndex(selectedRow);
    Rectangle cellRect = tableCashDrops.getCellRect(selectedRow, 0, false);
    tableCashDrops.scrollRectToVisible(cellRect);
  }
  
  private void doScrollUp(ActionEvent evt) {
    if (cashDropList == null) {
      return;
    }
    int selectedRow = tableCashDrops.getSelectedRow();
    if (selectedRow <= 0) {
      selectedRow = cashDropList.size() - 1;
    }
    else if (selectedRow > cashDropList.size() - 1) {
      selectedRow = cashDropList.size() - 1;
    }
    else {
      selectedRow--;
    }
    
    selectionModel.setLeadSelectionIndex(selectedRow);
    Rectangle cellRect = tableCashDrops.getCellRect(selectedRow, 0, false);
    tableCashDrops.scrollRectToVisible(cellRect);
  }
  
  private void btnCloseActionPerformed(ActionEvent evt) {
    dispose();
  }
  
  private void btnDeleteSelectedActionPerformed(ActionEvent evt) {
    try {
      int index = tableCashDrops.getSelectedRow();
      if (index >= 0) {
        CashDropTransaction transaction = (CashDropTransaction)cashDropList.get(index);
        CashDropTransactionDAO dao = new CashDropTransactionDAO();
        dao.deleteCashDrop(transaction, drawerTerminal);
        tableModel.removeCashDrop(transaction);
      }
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("CashDropDialog.16"), e);
    }
  }
  
  private void btnNewCashDropActionPerformed(ActionEvent evt) {
    try {
      NumberSelectionDialog2 dialog = new NumberSelectionDialog2();
      dialog.setTitle(Messages.getString("CashDropDialog.17"));
      dialog.setFloatingPoint(true);
      dialog.pack();
      dialog.open();
      
      if (!dialog.isCanceled()) {
        double amount = dialog.getValue();
        CashDropTransaction transaction = new CashDropTransaction();
        transaction.setDrawerResetted(Boolean.valueOf(false));
        transaction.setTerminal(drawerTerminal);
        
        if (currentUser.isStaffBankStarted().booleanValue()) {
          transaction.setCashDrawer(currentUser.getActiveDrawerPullReport());
        } else {
          transaction.setCashDrawer(drawerTerminal.getCurrentCashDrawer());
        }
        transaction.setPaymentType(PaymentType.CASH.toString());
        transaction.setUser(currentUser);
        transaction.setTransactionTime(new Date());
        transaction.setAmount(Double.valueOf(amount));
        
        CashDropTransactionDAO dao = new CashDropTransactionDAO();
        dao.saveNewCashDrop(transaction, drawerTerminal);
        tableModel.addCashDrop(transaction);
      }
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("CashDropDialog.18"), e);
    }
  }
  
  class CashDropTableModel extends AbstractTableModel {
    CashDropTableModel() {}
    
    public int getRowCount() { if (cashDropList == null) {
        return 0;
      }
      int size = cashDropList.size();
      return size;
    }
    
    public int getColumnCount() {
      return 2;
    }
    
    public void addCashDrop(int index, CashDropTransaction t) {
      if (cashDropList == null) {
        return;
      }
      cashDropList.add(index, t);
      fireTableRowsInserted(index, index);
      
      Rectangle cellRect = tableCashDrops.getCellRect(index, 0, false);
      tableCashDrops.scrollRectToVisible(cellRect);
      selectionModel.setLeadSelectionIndex(index);
    }
    
    public void addCashDrop(CashDropTransaction t) {
      int size = cashDropList.size();
      cashDropList.add(t);
      fireTableRowsInserted(size, size);
      
      Rectangle cellRect = tableCashDrops.getCellRect(size, 0, false);
      tableCashDrops.scrollRectToVisible(cellRect);
      selectionModel.setLeadSelectionIndex(size);
    }
    
    public void removeCashDrop(CashDropTransaction t) {
      int index = cashDropList.indexOf(t);
      if (index >= 0) {
        cashDropList.remove(index);
        fireTableRowsDeleted(index, index);
      }
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      if (cashDropList == null) {
        return "";
      }
      CashDropTransaction t = (CashDropTransaction)cashDropList.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return t.getTransactionTime();
      
      case 1: 
        return Double.valueOf(t.getAmount().doubleValue());
      }
      return "";
    }
  }
  
  class TableRenderer extends DefaultTableCellRenderer
  {
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
    Font font = getFont().deriveFont(1, 14.0F);
    


    private JCheckBox checkBox = new JCheckBox();
    
    public TableRenderer() {
      checkBox.setHorizontalAlignment(0);
    }
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
      if ((value instanceof Boolean)) {
        checkBox.setSelected(((Boolean)value).booleanValue());
        if (isSelected) {
          checkBox.setBackground(table.getSelectionBackground());
        }
        else {
          checkBox.setBackground(table.getBackground());
        }
        return checkBox;
      }
      JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      label.setFont(font);
      if ((value instanceof Date)) {
        String string = dateFormat.format(value);
        label.setText(string);
        label.setHorizontalAlignment(4);
      }
      if ((value instanceof Double)) {
        String string = NumberUtil.formatNumber(Double.valueOf(((Double)value).doubleValue()));
        label.setText(currencySymbol + string);
        label.setHorizontalAlignment(4);
      }
      else {
        label.setHorizontalAlignment(2);
      }
      return label;
    }
  }
}
