package com.floreantpos.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.model.MenuCategoryForm;
import com.floreantpos.ui.views.MenuCategorySelectionView;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
















public class MenuCategorySelectionDialog
  extends POSDialog
  implements ActionListener
{
  private MenuCategorySelectionView menuCategorySelectorPanel;
  private List<MenuCategory> categoryList;
  private int selectionMode;
  private TitlePanel titelpanel;
  private static MenuCategorySelectionDialog instance;
  
  public MenuCategorySelectionDialog(List<MenuCategory> menuCategoryList)
  {
    super(POSUtil.getFocusedWindow(), "");
    categoryList = menuCategoryList;
    initComponents();
  }
  
  public MenuCategorySelectionDialog() {
    super(POSUtil.getFocusedWindow(), "");
    setTitle("Select menu category");
    initComponents();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    setTitle("Select Menu Category");
    titelpanel = new TitlePanel();
    titelpanel.setTitle("Select one or more menu category");
    
    add(titelpanel, "North");
    
    JPanel contentPane = new JPanel(new MigLayout("fill,hidemode 3,inset 0 10 0 10"));
    menuCategorySelectorPanel = new MenuCategorySelectionView(categoryList);
    contentPane.add(menuCategorySelectorPanel, "grow,span,wrap");
    
    PosButton btnOk = new PosButton("SELECT");
    btnOk.setActionCommand(POSConstants.OK);
    btnOk.setBackground(Color.GREEN);
    
    btnOk.setFocusable(false);
    btnOk.addActionListener(this);
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL);
    btnCancel.setFocusable(false);
    btnCancel.addActionListener(this);
    
    JPanel footerPanel = new JPanel(new MigLayout("center,ins 0 5 5 5", "", ""));
    
    PosButton btnEdit = new PosButton();
    PosButton btnAdd = new PosButton();
    
    btnAdd.setText(POSConstants.ADD.toUpperCase());
    btnEdit.setText(POSConstants.EDIT.toUpperCase());
    
    btnEdit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MenuCategorySelectionDialog.this.editSelectedRow();
      }
      

    });
    btnAdd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          MenuCategory menuCategory = new MenuCategory();
          BeanEditor<MenuCategory> editor = new MenuCategoryForm(menuCategory);
          
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.setPreferredSize(PosUIManager.getSize(900, 650));
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          MenuCategory category = (MenuCategory)editor.getBean();
          menuCategorySelectorPanel.getModel().addRow(category);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    footerPanel.add(btnAdd);
    footerPanel.add(btnEdit);
    footerPanel.add(btnOk);
    footerPanel.add(btnCancel);
    
    add(footerPanel, "South");
    
    add(contentPane);
  }
  
  public void setSelectionMode(int selectionMode) {
    this.selectionMode = selectionMode;
    if (selectionMode == 0) {
      titelpanel.setTitle("SELECT A MENU CATEGORY");
    }
    else {
      titelpanel.setTitle("Select one or more menu category");
    }
    menuCategorySelectorPanel.setSelectionMode(selectionMode);
  }
  
  public void setSelectedOrderType(OrderType orderType) {
    menuCategorySelectorPanel.setSelectedOrdertype(orderType);
  }
  
  private void doOk() {
    if (selectionMode == 0) {
      MenuCategory category = getSelectedRowData();
      if (category == null) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select menu category");
        return;
      }
    }
    else {
      List<MenuCategory> menuCategories = menuCategorySelectorPanel.getSelectedMenuCategoryList();
      if ((menuCategories == null) || (menuCategories.isEmpty())) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select menu category");
        return;
      }
    }
    setCanceled(false);
    dispose();
  }
  
  private void doCancel() {
    setCanceled(true);
    dispose();
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    
    if (POSConstants.CANCEL.equalsIgnoreCase(actionCommand)) {
      doCancel();
    }
    else if (POSConstants.OK.equalsIgnoreCase(actionCommand)) {
      doOk();
    }
  }
  
  private void editSelectedRow() {
    try {
      int index = menuCategorySelectorPanel.getSelectedRow();
      if (index < 0)
        return;
      MenuCategory category = (MenuCategory)menuCategorySelectorPanel.getModel().getRow(index);
      MenuCategoryDAO.getInstance().initialize(category);
      menuCategorySelectorPanel.getModel().setRow(index, category);
      
      BeanEditor<MenuCategory> editor = new MenuCategoryForm(category);
      
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      menuCategorySelectorPanel.repaintTable();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public List<MenuCategory> getSelectedCategories() {
    return menuCategorySelectorPanel.getSelectedMenuCategoryList();
  }
  
  public MenuCategory getSelectedRowData() {
    int index = menuCategorySelectorPanel.getSelectedRow();
    if (index < 0)
      return null;
    return (MenuCategory)menuCategorySelectorPanel.getModel().getRow(index);
  }
  
  public static MenuCategorySelectionDialog getInstance() {
    if (instance == null) {
      instance = new MenuCategorySelectionDialog();
    }
    return instance;
  }
}
