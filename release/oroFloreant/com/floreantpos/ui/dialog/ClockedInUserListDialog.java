package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.actions.StaffBankStartStopAction;
import com.floreantpos.main.Application;
import com.floreantpos.model.ActionHistory;
import com.floreantpos.model.AttendenceHistory;
import com.floreantpos.model.Shift;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.ActionHistoryDAO;
import com.floreantpos.model.dao.AttendenceHistoryDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.ButtonColumn;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.MatteBorder;
import javax.swing.table.JTableHeader;

















public class ClockedInUserListDialog
  extends OkCancelOptionDialog
{
  BeanTableModel<User> tableModel;
  JTable userListTable;
  private PosButton btnForceClockOut;
  private boolean dataUpdated = false;
  private User currentUser;
  
  public ClockedInUserListDialog(User currentUser) {
    super(Application.getPosWindow(), true);
    this.currentUser = currentUser;
    
    setTitle("Clocked in users");
    setCaption("Following users are clocked in");
    
    JPanel contentPane = getContentPanel();
    contentPane.setLayout(new BorderLayout(5, 5));
    contentPane.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
    
    tableModel = new BeanTableModel(User.class)
    {
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 1)
          return true;
        return false;
      }
      
      public Object getValueAt(int rowIndex, int columnIndex)
      {
        if (columnIndex == 1)
          return "FORCE CLOCK OUT";
        return super.getValueAt(rowIndex, columnIndex);
      }
      
      public void setValueAt(Object value, int rowIndex, int columnIndex)
      {
        if (columnIndex != 1)
          super.setValueAt(value, rowIndex, columnIndex);
      }
    };
    tableModel.addColumn("Name", "fullName");
    tableModel.addColumn("", "clockedIn");
    
    userListTable = new JTable(tableModel);
    userListTable.setRowHeight(PosUIManager.getSize(45));
    userListTable.setShowGrid(false);
    userListTable.getSelectionModel().setSelectionMode(0);
    contentPane.add(new JScrollPane(userListTable));
    userListTable.getTableHeader().setPreferredSize(PosUIManager.getSize(0, 35));
    
    btnForceClockOut = new PosButton("FORCE CLOCK OUT");
    btnForceClockOut.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        User user = getSelectedUser();
        if (user == null) {
          return;
        }
        ClockedInUserListDialog.this.doForceClockOutUser(user);
      }
      
    });
    AbstractAction action = new AbstractAction()
    {
      public void actionPerformed(ActionEvent e)
      {
        User user = getSelectedUser();
        if (user == null) {
          return;
        }
        ClockedInUserListDialog.this.doForceClockOutUser(user);
      }
    };
    ButtonColumn buttonColumn = new ButtonColumn(userListTable, action, 1)
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        return super.getTableCellRendererComponent(table, value, false, hasFocus, row, column);
      }
      
      public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
      {
        return super.getTableCellEditorComponent(table, value, false, row, column);
      }
    };
    MatteBorder selectedBorder = BorderFactory.createMatteBorder(5, 5, 5, 5, userListTable.getBackground());
    MatteBorder unselectedBorder = BorderFactory.createMatteBorder(5, 5, 5, 5, userListTable.getBackground());
    
    Border border1 = new CompoundBorder(selectedBorder, btnForceClockOut.getBorder());
    Border border2 = new CompoundBorder(unselectedBorder, btnForceClockOut.getBorder());
    
    buttonColumn.setUnselectedBorder(border1);
    buttonColumn.setFocusBorder(border2);
    
    updateView();
    setOkButtonText("DONE");
    setCancelButtonVisible(false);
  }
  














  private void updateView()
  {
    List<User> users = UserDAO.getInstance().findClockedInUsers();
    tableModel.setRows(users);
  }
  
  private void doForceClockOutUser(User user) {
    try {
      if ((!currentUser.isManager()) && (!currentUser.isAdministrator())) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "You have no permission to force clock out.");
        return;
      }
      if (TicketDAO.getInstance().hasOpenTickets(user)) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please close or transfer all open tickets");
        return;
      }
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Are you sure you want to force clock out?", "Confirm") != 0) {
        return;
      }
      if (user.isStaffBankStarted().booleanValue()) {
        StaffBankStartStopAction staffBankStartStopAction = new StaffBankStartStopAction(user);
        staffBankStartStopAction.performForceCloseStaffBank(currentUser);
        ActionHistory actionHistory = new ActionHistory();
        actionHistory.setActionName("STAFF_BANK_FORCE_CLOSE");
        actionHistory.setDescription("Forcefully closed staff bank of staff " + user.getId());
        actionHistory.setActionTime(new Date());
        actionHistory.setPerformer(currentUser);
        ActionHistoryDAO.getInstance().save(actionHistory);
      }
      
      AttendenceHistoryDAO attendenceHistoryDAO = new AttendenceHistoryDAO();
      AttendenceHistory attendenceHistory = attendenceHistoryDAO.findHistoryByClockedInTime(user);
      if (attendenceHistory == null) {
        attendenceHistory = new AttendenceHistory();
        Date lastClockInTime = user.getLastClockInTime();
        Calendar c = Calendar.getInstance();
        c.setTime(lastClockInTime);
        attendenceHistory.setClockInTime(lastClockInTime);
        attendenceHistory.setClockInHour(Short.valueOf((short)c.get(10)));
        attendenceHistory.setUser(user);
        attendenceHistory.setTerminal(Application.getInstance().getTerminal());
        attendenceHistory.setShift(user.getCurrentShift());
      }
      
      Shift shift = user.getCurrentShift();
      Calendar calendar = Calendar.getInstance();
      
      user.doClockOut(attendenceHistory, shift, calendar);
      
      String msg = Messages.getString("ClockInOutAction.8") + user.getFirstName() + " " + user.getLastName() + Messages.getString("ClockInOutAction.10");
      if (POSMessageDialog.showMessageAndPromtToPrint(msg)) {
        ReceiptPrintService.printClockInOutReceipt(user);
      }
      updateView();
      dataUpdated = true;
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public void hideUser(User user) {
    tableModel.removeRow(user);
  }
  
  public User getSelectedUser() {
    int selectedRow = userListTable.getSelectedRow();
    if (selectedRow == -1)
      return null;
    return (User)tableModel.getRows().get(selectedRow);
  }
  
  public void doOk()
  {
    setCanceled(true);
    dispose();
  }
  
  public boolean isDataUpdated() {
    return dataUpdated;
  }
}
