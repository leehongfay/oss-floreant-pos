package com.floreantpos.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.Store;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.util.NumberUtil;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;






















public class LengthInputDialog
  extends OkCancelOptionDialog
  implements ActionListener
{
  private double defaultValue;
  private static DoubleTextField tfNumber;
  private boolean clearPreviousNumber = true;
  
  private static final String UNIT_KM = "KM";
  private static final String UNIT_MILE = "MILE";
  private JComboBox tfUnitC;
  
  public LengthInputDialog(String title)
  {
    super(title);
    init();
    Store store = Application.getInstance().getStore();
    
    String lengthUnitName = store.getProperty("deliveryConfig.unitName");
    if (StringUtils.isNotEmpty(lengthUnitName)) {
      tfUnitC.setSelectedItem(lengthUnitName);
    }
  }
  
  private void init() {
    setResizable(false);
    JPanel leftPanel = new JPanel();
    
    MigLayout layout = new MigLayout("fill,inset 0", "sg, fill", "");
    leftPanel.setLayout(layout);
    
    Dimension size = PosUIManager.getSize_w100_h70();
    
    tfNumber = new DoubleTextField();
    tfNumber.setText(String.valueOf(defaultValue));
    tfNumber.setFont(tfNumber.getFont().deriveFont(1, 24.0F));
    tfNumber.setFocusable(true);
    tfNumber.requestFocus();
    tfNumber.setBackground(Color.WHITE);
    
    Vector units = new Vector();
    units.add("KM");
    units.add("MILE");
    
    tfUnitC = new JComboBox(units);
    tfUnitC.setFont(tfNumber.getFont().deriveFont(1, 24.0F));
    tfUnitC.setEnabled(false);
    
    leftPanel.add(tfNumber, "span 2, grow");
    leftPanel.add(tfUnitC, "span, grow");
    
    String[][] numbers = { { "7", "8", "9" }, { "4", "5", "6" }, { "1", "2", "3" }, { ".", "0", "CLEAR ALL" } };
    String[][] iconNames = { { "7.png", "8.png", "9.png" }, { "4.png", "5.png", "6.png" }, { "1.png", "2.png", "3.png" }, { "dot.png", "0.png", "" } };
    

    for (int i = 0; i < numbers.length; i++) {
      for (int j = 0; j < numbers[i].length; j++) {
        PosButton posButton = new PosButton();
        posButton.setFocusable(false);
        ImageIcon icon = IconFactory.getIcon("/ui_icons/", iconNames[i][j]);
        String buttonText = String.valueOf(numbers[i][j]);
        
        if (icon == null) {
          posButton.setText(buttonText);
        }
        else {
          posButton.setIcon(icon);
          if (POSConstants.CLEAR_ALL.equals(buttonText)) {
            posButton.setText(Messages.getString("LengthInputDialog.2"));
          }
        }
        
        posButton.setActionCommand(buttonText);
        posButton.addActionListener(this);
        String constraints = "w " + width + "!,h " + height + "!,grow";
        if (j == numbers[i].length - 1) {
          constraints = constraints + ",wrap";
        }
        leftPanel.add(posButton, constraints);
      }
    }
    getContentPanel().add(leftPanel, "Center");
  }
  
  private double convert(String unitTo, double inputValue) {
    String unitFrom = tfUnitC.getSelectedItem().toString();
    
    if (unitFrom.equals("KM")) {
      if (unitTo.equals("MILE")) {
        return NumberUtil.roundToTwoDigit(0.621371D * inputValue);
      }
    }
    else if ((unitFrom.equals("MILE")) && 
      (unitTo.equals("KM"))) {
      return NumberUtil.roundToTwoDigit(1.609344D * inputValue);
    }
    
    return inputValue;
  }
  
  private void doClearAll() {
    tfNumber.setText(String.valueOf(0.0D));
  }
  
  private void doInsertNumber(String number)
  {
    if (clearPreviousNumber) {
      tfNumber.setText("0");
      clearPreviousNumber = false;
    }
    
    String s = tfNumber.getText();
    double d = 0.0D;
    try
    {
      d = Double.parseDouble(s);
    }
    catch (Exception localException) {}
    
    if ((d == 0.0D) && (!s.contains("."))) {
      tfNumber.setText(number);
      return;
    }
    
    s = s + number;
    if (!validate(s)) {
      POSMessageDialog.showError(this, POSConstants.INVALID_NUMBER);
      return;
    }
    tfNumber.setText(s);
  }
  
  private void doInsertDot() {
    String string = tfNumber.getText() + ".";
    if (!validate(string)) {
      POSMessageDialog.showError(this, POSConstants.INVALID_NUMBER);
      return;
    }
    tfNumber.setText(string);
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    
    double value = tfNumber.getDouble();
    
    if (actionCommand.equals(POSConstants.CLEAR_ALL)) {
      doClearAll();
    }
    else if (actionCommand.equals(".")) {
      doInsertDot();
    }
    else if (actionCommand.equals("MILE")) {
      tfNumber.setText(String.valueOf(convert("MILE", value)));
    }
    else if (actionCommand.equals("KM")) {
      tfNumber.setText(String.valueOf(convert("KM", value)));
    }
    else {
      doInsertNumber(actionCommand);
    }
  }
  
  private boolean validate(String str) {
    try {
      Double.parseDouble(str);
    } catch (Exception x) {
      return false;
    }
    return true;
  }
  
  public double getValue() {
    return Double.parseDouble(tfNumber.getText());
  }
  
  public double getDefaultUnitValue() {
    return convert("KM", Double.parseDouble(tfNumber.getText()));
  }
  
  public void setValue(double value) {
    tfNumber.setText(String.valueOf(value));
  }
  
  public static double takeDoubleInput(String title, double initialAmount) {
    LengthInputDialog dialog = new LengthInputDialog(Messages.getString("LengthInputDialog.6"));
    dialog.setCaption(title);
    dialog.setValue(initialAmount);
    dialog.pack();
    dialog.open();
    
    if (dialog.isCanceled()) {
      return -1.0D;
    }
    return dialog.getDefaultUnitValue();
  }
  
  public void doOk()
  {
    if (!validate(tfNumber.getText())) {
      POSMessageDialog.showError(this, POSConstants.INVALID_NUMBER);
      return;
    }
    setCanceled(false);
    dispose();
  }
}
