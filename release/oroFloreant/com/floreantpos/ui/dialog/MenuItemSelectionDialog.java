package com.floreantpos.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.model.MenuItemForm;
import com.floreantpos.ui.model.MenuItemSelectionView;
import com.floreantpos.ui.model.MenuItemSelectionView.MenuItemTableModel;
import com.floreantpos.ui.model.PizzaItemForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
















public class MenuItemSelectionDialog
  extends POSDialog
  implements ActionListener
{
  private MenuItemSelectionView menuSelectorPanel;
  private List<MenuItem> menuItemList;
  private int selectionMode;
  private TitlePanel titelpanel;
  private boolean inventoryItemOnly;
  private static MenuItemSelectionDialog instance;
  
  public MenuItemSelectionDialog(List<MenuItem> menuItemList)
  {
    super(POSUtil.getFocusedWindow(), "");
    this.menuItemList = menuItemList;
    initComponents();
  }
  
  public MenuItemSelectionDialog(List<MenuItem> menuItemList, boolean inventoryItemOnly) {
    super(POSUtil.getFocusedWindow(), "");
    this.menuItemList = menuItemList;
    this.inventoryItemOnly = inventoryItemOnly;
    initComponents();
  }
  
  public MenuItemSelectionDialog() {
    super(POSUtil.getFocusedWindow(), "");
    setTitle("Select menu item");
    initComponents();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    setTitle("Select Menu Item");
    titelpanel = new TitlePanel();
    titelpanel.setTitle("Select one or more menu item");
    
    add(titelpanel, "North");
    
    JPanel contentPane = new JPanel(new MigLayout("fill,hidemode 3,inset 0 10 0 10"));
    menuSelectorPanel = new MenuItemSelectionView(menuItemList, inventoryItemOnly);
    contentPane.add(menuSelectorPanel, "grow,span,wrap");
    
    PosButton btnOk = new PosButton("SELECT");
    btnOk.setActionCommand(POSConstants.OK);
    btnOk.setBackground(Color.GREEN);
    
    btnOk.setFocusable(false);
    btnOk.addActionListener(this);
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL);
    btnCancel.setFocusable(false);
    btnCancel.addActionListener(this);
    
    JPanel footerPanel = new JPanel(new MigLayout("center,ins 0 5 5 5", "", ""));
    
    PosButton btnEdit = new PosButton();
    PosButton btnAdd = new PosButton();
    
    btnAdd.setText(POSConstants.ADD.toUpperCase());
    btnEdit.setText(POSConstants.EDIT.toUpperCase());
    
    btnEdit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MenuItemSelectionDialog.this.editSelectedRow();
      }
      

    });
    btnAdd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          MenuItem menuMenuItem = new MenuItem();
          BeanEditor<MenuItem> editor = new MenuItemForm(menuMenuItem);
          
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.setPreferredSize(PosUIManager.getSize(900, 650));
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          MenuItem menuItemItem = (MenuItem)editor.getBean();
          menuSelectorPanel.getModel().addItem(menuItemItem);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    footerPanel.add(btnAdd);
    footerPanel.add(btnEdit);
    footerPanel.add(btnOk);
    footerPanel.add(btnCancel);
    
    add(footerPanel, "South");
    
    add(contentPane);
  }
  
  public void setSelectionMode(int selectionMode) {
    this.selectionMode = selectionMode;
    if (selectionMode == 0) {
      titelpanel.setTitle("SELECT A MENU ITEM");
    }
    else {
      titelpanel.setTitle("Select one or more menu item");
    }
    menuSelectorPanel.setSelectionMode(selectionMode);
  }
  
  public void setSelectedGroup(MenuGroup group) {
    menuSelectorPanel.setSelectedGroup(group);
  }
  
  private void doOk() {
    if (selectionMode == 0) {
      MenuItem menuItem = getSelectedRowData();
      if (menuItem == null) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select menu Item");
        return;
      }
    }
    else {
      List<MenuItem> menuMenuItems = menuSelectorPanel.getSelectedMenuItemList();
      if ((menuMenuItems == null) || (menuMenuItems.isEmpty())) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select menu Item");
        return;
      }
    }
    setCanceled(false);
    dispose();
  }
  
  private void doCancel() {
    setCanceled(true);
    dispose();
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    
    if (POSConstants.CANCEL.equalsIgnoreCase(actionCommand)) {
      doCancel();
    }
    else if (POSConstants.OK.equalsIgnoreCase(actionCommand)) {
      doOk();
    }
  }
  
  private void editSelectedRow() {
    try {
      int index = menuSelectorPanel.getSelectedRow();
      if (index < 0)
        return;
      MenuItem menuItem = (MenuItem)menuSelectorPanel.getModel().getRowData(index);
      MenuItemDAO.getInstance().initialize(menuItem);
      menuSelectorPanel.getModel().addItem(menuItem);
      
      BeanEditor<MenuItem> editor = null;
      
      if (menuItem.isPizzaType().booleanValue()) {
        editor = new PizzaItemForm(menuItem);
      }
      else {
        editor = new MenuItemForm(menuItem);
      }
      
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      menuSelectorPanel.repaintTable();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public List<MenuItem> getSelectedItems() {
    return menuSelectorPanel.getSelectedMenuItemList();
  }
  
  public MenuItem getSelectedRowData() {
    int index = menuSelectorPanel.getSelectedRow();
    if (index < 0)
      return null;
    return (MenuItem)menuSelectorPanel.getModel().getRowData(index);
  }
  
  public static MenuItemSelectionDialog getInstance() {
    if (instance == null) {
      instance = new MenuItemSelectionDialog();
    }
    return instance;
  }
}
