package com.floreantpos.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.model.PaymentType;
import com.floreantpos.swing.PosButton;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;






























public class PaymentTypeSelectionDialog
  extends POSDialog
{
  PaymentType selectedPaymentType;
  private PaymentSelectionButton btnCash;
  private PaymentSelectionButton btnGiftCert;
  
  public PaymentTypeSelectionDialog()
  {
    setTitle(Messages.getString("PaymentTypeSelectionDialog.0"));
    
    initComponents();
  }
  





  private void initComponents()
  {
    JPanel content = new JPanel(new MigLayout("gap 5px 20px, fill"));
    content.setBorder(new EmptyBorder(5, 5, 5, 5));
    
    JPanel genericPanel = new JPanel(new GridLayout(1, 0, 15, 15));
    btnCash = new PaymentSelectionButton(PaymentType.CASH);
    genericPanel.add(btnCash, "grow,wrap");
    btnGiftCert = new PaymentSelectionButton(PaymentType.GIFT_CERTIFICATE);
    genericPanel.add(btnGiftCert);
    content.add(genericPanel, "height 60px, wrap, growx");
    
    JPanel creditCardPanel = new JPanel(new GridLayout(1, 0, 10, 10));
    creditCardPanel.add(new PaymentSelectionButton(PaymentType.CREDIT_VISA));
    creditCardPanel.add(new PaymentSelectionButton(PaymentType.CREDIT_MASTER_CARD));
    creditCardPanel.add(new PaymentSelectionButton(PaymentType.CREDIT_AMEX));
    creditCardPanel.add(new PaymentSelectionButton(PaymentType.CREDIT_DISCOVERY));
    
    creditCardPanel.setBorder(new CompoundBorder(new TitledBorder(Messages.getString("PaymentTypeSelectionDialog.4")), new EmptyBorder(10, 10, 10, 10)));
    content.add(creditCardPanel, "wrap, height 110px, growx");
    
    JPanel debitCardPanel = new JPanel(new GridLayout(1, 0, 10, 10));
    debitCardPanel.add(new PaymentSelectionButton(PaymentType.DEBIT_VISA));
    debitCardPanel.add(new PaymentSelectionButton(PaymentType.DEBIT_MASTER_CARD));
    
    debitCardPanel.setBorder(new CompoundBorder(new TitledBorder(Messages.getString("PaymentTypeSelectionDialog.6")), new EmptyBorder(10, 10, 10, 10)));
    content.add(debitCardPanel, "wrap, height 110px, growx");
    
    PosButton cancel = new PosButton(Messages.getString("PaymentTypeSelectionDialog.8"));
    cancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setCanceled(true);
        dispose();
      }
      
    });
    content.add(cancel, "alignx center, gaptop 20px");
    
    add(content);
    
    pack();
  }
  
  public PaymentType getSelectedPaymentType() {
    return selectedPaymentType;
  }
  
  class PaymentSelectionButton extends PosButton implements ActionListener
  {
    PaymentType paymentType;
    
    public PaymentSelectionButton(PaymentType p)
    {
      paymentType = p;
      
      if (p.getImageFile() != null) {
        setIcon(IconFactory.getIcon("/ui_icons/", "" + p.getImageFile()));
      }
      else {
        setText(p.getDisplayString());
      }
      
      addActionListener(this);
      setEnabled(paymentType.isSupported());
    }
    
    public void actionPerformed(ActionEvent e)
    {
      selectedPaymentType = paymentType;
      setCanceled(false);
      dispose();
    }
  }
  
  public void setCashButtonVisible(boolean visible) {
    btnCash.setVisible(visible);
    btnGiftCert.setVisible(visible);
  }
}
