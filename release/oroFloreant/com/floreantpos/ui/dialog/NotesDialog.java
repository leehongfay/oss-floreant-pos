package com.floreantpos.ui.dialog;

import com.floreantpos.main.Application;
import com.floreantpos.ui.views.NoteView;
import java.awt.Container;
import javax.swing.BorderFactory;























public class NotesDialog
  extends OkCancelOptionDialog
{
  private NoteView noteView;
  
  public NotesDialog()
  {
    super(Application.getPosWindow(), true);
    initComponents();
  }
  
  private void initComponents()
  {
    noteView = new NoteView();
    setDefaultCloseOperation(2);
    
    noteView.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
    getContentPane().add(noteView, "Center");
    
    pack();
  }
  
  public void doOk()
  {
    setCanceled(false);
    dispose();
  }
  
  public void setTitle(String title)
  {
    super.setTitle(Application.getTitle());
    setCaption(title);
  }
  
  public String getNote() {
    return noteView.getNote();
  }
  
  public void setNote(String note) {
    noteView.setText(note);
  }
}
