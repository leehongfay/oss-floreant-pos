package com.floreantpos.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.QwertyKeyPad;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.JTextField;


















public class GlobalInputDialog
  extends OkCancelOptionDialog
{
  private JTextField tfInput;
  private QwertyKeyPad qwertyKeyPad;
  private PosButton jbSearch;
  
  public GlobalInputDialog()
  {
    super(POSUtil.getFocusedWindow(), POSConstants.SEARCH_ITEM_BUTTON_TEXT);
    setDefaultCloseOperation(2);
    init();
    setSize(PosUIManager.getSize(900, 500));
  }
  
  public GlobalInputDialog(Frame parent) {
    super(parent, POSConstants.SEARCH_ITEM_BUTTON_TEXT);
    setDefaultCloseOperation(2);
    init();
    setSize(PosUIManager.getSize(900, 500));
  }
  
  public GlobalInputDialog(Window parent) {
    super(parent, POSConstants.SEARCH_ITEM_BUTTON_TEXT);
    setDefaultCloseOperation(2);
    init();
    setSize(PosUIManager.getSize(900, 500));
  }
  
  private void init() {
    JPanel contentPane = getContentPanel();
    JPanel topPanel = new JPanel(new BorderLayout(5, 5));
    contentPane.setLayout(new BorderLayout(5, 5));
    
    tfInput = new JTextField();
    tfInput.setFont(tfInput.getFont().deriveFont(1, PosUIManager.getNumberFieldFontSize()));
    tfInput.setFocusable(true);
    tfInput.requestFocus();
    tfInput.setBackground(Color.WHITE);
    tfInput.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        setCanceled(false);
        dispose();
      }
      
    });
    jbSearch = new PosButton("Search");
    jbSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        setCanceled(false);
        dispose();
      }
    });
    topPanel.add(tfInput);
    topPanel.add(jbSearch, "East");
    
    qwertyKeyPad = new QwertyKeyPad();
    
    contentPane.add(tfInput, "North");
    contentPane.add(qwertyKeyPad);
  }
  
  public void doOk()
  {
    setCanceled(false);
    dispose();
  }
  
  public String getInput() {
    return tfInput.getText();
  }
}
