package com.floreantpos.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.model.Ticket;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.views.TicketDetailView;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JSeparator;
import org.jdesktop.layout.GroupLayout;
import org.jdesktop.layout.GroupLayout.ParallelGroup;
import org.jdesktop.layout.GroupLayout.SequentialGroup;











public class TicketDetailDialog
  extends POSDialog
{
  private PosButton btnFinish;
  private JSeparator jSeparator1;
  private TicketDetailView ticketDetailView;
  private TitlePanel titlePanel1;
  
  public TicketDetailDialog()
  {
    initComponents();
    
    setResizable(false);
    pack();
  }
  






  private void initComponents()
  {
    titlePanel1 = new TitlePanel();
    jSeparator1 = new JSeparator();
    btnFinish = new PosButton();
    ticketDetailView = new TicketDetailView();
    
    setDefaultCloseOperation(2);
    setTitle(POSConstants.TICKET_DETAIL);
    
    titlePanel1.setTitle(POSConstants.TICKET_DETAIL);
    
    btnFinish.setText(POSConstants.OK);
    btnFinish.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        TicketDetailDialog.this.doFinish(evt);
      }
      
    });
    GroupLayout layout = new GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(layout
      .createParallelGroup(1)
      .add(layout.createSequentialGroup()
      .add(layout.createParallelGroup(1)
      .add(layout.createSequentialGroup()
      .addContainerGap()
      .add(layout.createParallelGroup(1)
      .add(titlePanel1, -1, 428, 32767)
      .add(2, layout.createSequentialGroup()
      .add(layout.createParallelGroup(2)
      .add(1, ticketDetailView, -1, 428, 32767)
      .add(1, jSeparator1, -1, 428, 32767))
      .addPreferredGap(0))))
      .add(layout.createSequentialGroup()
      .add(140, 140, 140)
      .add(btnFinish, -2, 130, -2)))
      .addContainerGap()));
    
    layout.setVerticalGroup(layout
      .createParallelGroup(1)
      .add(layout.createSequentialGroup()
      .addContainerGap()
      .add(titlePanel1, -2, -1, -2)
      .addPreferredGap(0)
      .add(ticketDetailView, -2, -1, -2)
      .addPreferredGap(0)
      .add(jSeparator1, -2, -1, -2)
      .addPreferredGap(0)
      .add(btnFinish, -2, 42, -2)
      .addContainerGap(-1, 32767)));
    

    pack();
  }
  
  private void doFinish(ActionEvent evt) {
    setCanceled(false);
    
    dispose();
  }
  






  public void setTicket(Ticket ticket)
  {
    ticketDetailView.setTicket(ticket);
  }
}
