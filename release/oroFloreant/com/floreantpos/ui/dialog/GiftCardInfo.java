package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.GiftCard;
import com.floreantpos.model.Store;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.report.ReportUtil;
import com.floreantpos.swing.PosButton;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.swing.JRViewer;
import net.sf.jasperreports.swing.JRViewerToolbar;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;




















public class GiftCardInfo
  extends POSDialog
{
  private GiftCard giftCard;
  private static Log log = LogFactory.getLog(GiftCardInfo.class);
  private JasperPrint print;
  
  public GiftCardInfo(GiftCard giftCard) {
    this.giftCard = giftCard;
    initializeComponents();
  }
  




  private String cardNumberWithDashe(String symbol)
  {
    String cardNumber = giftCard.getCardNumber();
    
    StringBuilder gcn = new StringBuilder(cardNumber);
    for (int i = 0; i < gcn.length(); i++) {
      if ((i == 4) || (i == 9) || (i == 14)) {
        gcn.insert(i, "-");
      }
    }
    
    return gcn.toString();
  }
  

  public JRViewer createReport()
  {
    Map<String, Object> parameters = new HashMap();
    
    parameters.put("cafeName", Application.getInstance().getStore().getName());
    parameters.put("cardNumber", cardNumberWithDashe("-"));
    parameters.put("cardHolder", giftCard.getOwnerName());
    parameters.put("batchNumber", giftCard.getBatchNo());
    parameters.put("balance", String.valueOf(giftCard.getBalance()));
    parameters.put("cardLogo", Application.getApplicationIcon().getImage());
    parameters.put("bgImage", new ImageIcon(getClass().getResource("/images/giftCardBackground.jpg")).getImage());
    parameters.put("currency_symbol", CurrencyUtil.getCurrencySymbol());
    
    JasperReport report = ReportUtil.getReport("gift_card");
    try {
      print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource());
      JRViewer viewer = new JRViewer(print);
      Component[] viewerComponents = viewer.getComponents();
      for (Component component : viewerComponents) {
        PosLog.info(getClass(), "");
        if ((component instanceof JRViewerToolbar)) {
          component.setVisible(false);
        }
      }
    } catch (JRException e) {
      log.error(e);
      
      return null; }
    JRViewer viewer;
    return viewer;
  }
  
  private void initializeComponents()
  {
    if (giftCard == null) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("GiftCardInfo.0"));
      return;
    }
    setTitle(Messages.getString("GiftCardInfo.1"));
    setLayout(new BorderLayout(10, 10));
    
    JPanel mainPanel = new JPanel(new BorderLayout(10, 10));
    
    JPanel northPanel = new JPanel();
    northPanel.setLayout(new BorderLayout());
    northPanel.add(createReport(), "Center");
    

    JPanel southPanel = new JPanel(new BorderLayout());
    PosButton btnPrint = new PosButton(Messages.getString("GiftCardInfo.2"));
    PosButton btnClose = new PosButton(Messages.getString("GiftCardInfo.3"));
    btnClose.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });
    btnPrint.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          print.setName(Messages.getString("GiftCardInfo.4"));
          ReceiptPrintService.printQuitely(print);
        }
        catch (JRException e1) {
          GiftCardInfo.log.error(e);
        }
        
      }
    });
    JPanel btnPanel = new JPanel();
    btnPanel.setLayout(new MigLayout("fill,center"));
    btnPanel.add(btnPrint, "split 2,grow");
    btnPanel.add(btnClose, "grow");
    southPanel.add(btnPanel, "South");
    
    mainPanel.add(northPanel, "Center");
    mainPanel.add(southPanel, "South");
    
    add(mainPanel, "Center");
    




































































    PosButton btnPrintStoreCopy = new PosButton(Messages.getString("TransactionCompletionDialog.38"));
  }
}
