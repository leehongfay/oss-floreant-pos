package com.floreantpos.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.Discount;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketDiscount;
import com.floreantpos.model.dao.DiscountDAO;
import com.floreantpos.swing.ButtonColumn;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosOptionPane;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.ScrollableFlowPanel;
import com.floreantpos.ui.views.order.OrderController;
import com.floreantpos.util.DiscountUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;






























public class TicketDiscountSelectionDialog
  extends OkCancelOptionDialog
{
  private ScrollableFlowPanel orderDiscountButtonPanel;
  private Ticket ticket;
  private JPanel itemSearchPanel;
  private JTextField txtSearchItem;
  private JPanel discountInfoPanel;
  private JTable discountTable;
  private DiscountTableModel discountTableModel;
  
  public TicketDiscountSelectionDialog(Ticket ticket)
  {
    super(POSUtil.getFocusedWindow(), "SELECT ORDER DISCOUNT");
    this.ticket = ticket;
    initComponent();
  }
  
  private void initComponent() {
    setOkButtonText(POSConstants.SAVE_BUTTON_TEXT);
    setCancelButtonVisible(false);
    createCouponSearchPanel();
    getContentPanel().setBorder(new EmptyBorder(0, 10, 0, 10));
    getContentPanel().add(itemSearchPanel, "North");
    
    orderDiscountButtonPanel = new ScrollableFlowPanel(3);
    
    TitledBorder orderPanelBorder = BorderFactory.createTitledBorder(null, Messages.getString("TicketDiscountSelectionDialog.0"), 2, 0);
    
    orderDiscountButtonPanel.setBorder(orderPanelBorder);
    
    JScrollPane orderScrollPane = new PosScrollPane(orderDiscountButtonPanel, 20, 31);
    
    orderScrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(80, 0));
    getContentPanel().add(orderDiscountButtonPanel, "Center");
    createTicketDiscountPanel();
    rendererDiscounts();
    setSize(1024, 720);
  }
  
  public void createTicketDiscountPanel() {
    discountInfoPanel = new JPanel(new MigLayout("fill,hidemode 3,ins 8 5 2 5"));
    
    discountTable = new JTable();
    discountTable.setGridColor(Color.LIGHT_GRAY);
    discountTable.setCellSelectionEnabled(false);
    discountTable.setColumnSelectionAllowed(false);
    discountTable.setRowSelectionAllowed(false);
    
    discountTable.setAutoscrolls(true);
    discountTable.setRowHeight(PosUIManager.getSize(40));
    discountTable.setShowGrid(true);
    discountTable.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
    discountTable.setFocusable(false);
    discountTable.setAutoResizeMode(4);
    discountTableModel = new DiscountTableModel();
    discountTable.setModel(discountTableModel);
    discountTable.getColumnModel().getColumn(1).setCellRenderer(new DefaultTableCellRenderer()
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component rendererComponent = super.getTableCellRendererComponent(table, value, isSelected, false, row, column);
        setHorizontalAlignment(4);
        if (isSelected) {
          return rendererComponent;
        }
        rendererComponent.setBackground(table.getBackground());
        return rendererComponent;
      }
      
      protected void setValue(Object value)
      {
        if (value == null) {
          setText("");
          return;
        }
        String text = value.toString();
        if (((value instanceof Double)) || ((value instanceof Float))) {
          text = NumberUtil.formatNumberAcceptNegative(Double.valueOf(((Number)value).doubleValue()));
        }
        setText(text);
      }
    });
    setColumnWidth(0, PosUIManager.getSize(250));
    AbstractAction action = new AbstractAction()
    {
      public void actionPerformed(ActionEvent e) {
        int row = Integer.parseInt(e.getActionCommand());
        TicketDiscount item = (TicketDiscount)discountTableModel.getRowData(row);
        List<TicketDiscount> discounts = ticket.getDiscounts();
        if (discounts != null) {
          for (Iterator iterator = discounts.iterator(); iterator.hasNext();) {
            TicketDiscount ticketDiscount = (TicketDiscount)iterator.next();
            if (ticketDiscount == item) {
              iterator.remove();
              break;
            }
          }
          OrderController.saveOrder(ticket);
          TicketDiscountSelectionDialog.this.rendererTicketDiscounts();
        }
      }
    };
    ButtonColumn coloum = new ButtonColumn(discountTable, action, 0)
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JPanel panel = (JPanel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        int verticalGap = PosUIManager.getSize(15);
        int horizontalGap = PosUIManager.getSize(2);
        PosButton button = (PosButton)panel.getComponent(0);
        button.setOpaque(false);
        button.setBorder(new EmptyBorder(verticalGap, horizontalGap, verticalGap, horizontalGap));
        button.setIcon(IconFactory.getIcon("/ui_icons/", "delete-icon.png"));
        table.setRowHeight(row, table.getRowHeight(0));
        return panel;
      }
      
      public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
      {
        return super.getTableCellEditorComponent(table, value, false, row, column);
      }
    };
    coloum.showColumnValueInLabel(true);
    JScrollPane scrollPane = new JScrollPane(discountTable);
    discountInfoPanel.add(scrollPane, "grow,span");
    getContentPanel().add(discountInfoPanel, "West");
    rendererTicketDiscounts();
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = discountTable.getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMinWidth(width);
  }
  
  private void rendererTicketDiscounts() {
    List<TicketDiscount> discounts = ticket.getDiscounts();
    discountTableModel.setRows(discounts);
    discountInfoPanel.revalidate();
    discountInfoPanel.repaint();
  }
  
  private void createCouponSearchPanel() {
    itemSearchPanel = new JPanel(new BorderLayout(5, 5));
    itemSearchPanel.setBorder(new EmptyBorder(0, 5, 0, 5));
    PosButton btnSearch = new PosButton(IconFactory.getIcon("/ui_icons/", "search.png"));
    btnSearch.setPreferredSize(new Dimension(60, 40));
    
    JLabel lblCoupon = new JLabel(Messages.getString("DiscountSelectionDialog.4"));
    
    txtSearchItem = new JTextField();
    txtSearchItem.addFocusListener(new FocusListener()
    {
      public void focusLost(FocusEvent e) {
        txtSearchItem.setText(Messages.getString("DiscountSelectionDialog.5"));
        txtSearchItem.setForeground(Color.gray);
      }
      
      public void focusGained(FocusEvent e)
      {
        txtSearchItem.setForeground(Color.black);
        txtSearchItem.setText("");
      }
      
    });
    txtSearchItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        if (txtSearchItem.getText().equals("")) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("DiscountSelectionDialog.8"));
          return;
        }
        if ((!TicketDiscountSelectionDialog.this.addCouponByBarcode(txtSearchItem.getText())) && 
          (TicketDiscountSelectionDialog.this.addCouponById(txtSearchItem.getText()))) {
          POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("DiscountSelectionDialog.11"));
        }
        
        txtSearchItem.setText("");
      }
      
    });
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        String value = PosOptionPane.showInputDialog(Messages.getString("DiscountSelectionDialog.10"));
        if ((value == null) || (value.isEmpty())) {
          return;
        }
        txtSearchItem.requestFocus();
        
        if ((!TicketDiscountSelectionDialog.this.addCouponByBarcode(value)) && 
          (!TicketDiscountSelectionDialog.this.addCouponById(value))) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), Messages.getString("DiscountSelectionDialog.11"));
        }
        
      }
    });
    itemSearchPanel.add(lblCoupon, "West");
    itemSearchPanel.add(txtSearchItem);
    itemSearchPanel.add(btnSearch, "East");
  }
  
  private boolean addCouponById(String id) {
    Discount discount = DiscountDAO.getInstance().get(id);
    if (discount == null) {
      return false;
    }
    doApplyDiscountOnTicket(discount);
    return true;
  }
  
  private boolean addCouponByBarcode(String barcode) {
    Discount discount = DiscountDAO.getInstance().getDiscountByBarcode(barcode, 1);
    
    if (discount == null) {
      return false;
    }
    doApplyDiscountOnTicket(discount);
    return true;
  }
  
  private void rendererDiscounts() {
    List<Discount> discounts = DiscountDAO.getInstance().getTicketValidCoupon();
    for (Discount discount : discounts) {
      DiscountButton btnDiscount = new DiscountButton(discount);
      btnDiscount.setSelected(Boolean.valueOf(false));
      orderDiscountButtonPanel.add(btnDiscount);
    }
    orderDiscountButtonPanel.repaint();
    orderDiscountButtonPanel.revalidate();
  }
  
  public void doOk()
  {
    setCanceled(false);
    dispose();
  }
  
  public void doCancel() {
    setCanceled(true);
    dispose();
  }
  
  private void doApplyDiscountOnTicket(Discount discount) {
    TicketDiscount orderDiscount = null;
    List<TicketDiscount> couponAndDiscounts = ticket.getDiscounts();
    if ((couponAndDiscounts != null) && (!couponAndDiscounts.isEmpty())) {
      orderDiscount = (TicketDiscount)couponAndDiscounts.get(0);
      if (orderDiscount.getDiscountId().equals(discount.getId())) {}
    }
    





    if ((orderDiscount != null) && (orderDiscount.getCouponQuantity().doubleValue() <= discount.getMaximumOff().doubleValue())) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("TicketDiscountSelectionDialog.2"));
      return;
    }
    if (orderDiscount != null) {
      Discount existingDiscount = DiscountDAO.getInstance().get(orderDiscount.getDiscountId());
      if ((existingDiscount != null) && (existingDiscount.getType().intValue() == 2)) {
        return;
      }
    }
    
    boolean rePrice = discount.getType().intValue() == 2;
    Double discountValue = Double.valueOf(0.0D);
    if (rePrice) {
      String dialogTitle = Messages.getString("TicketItemDiscountSelectionDialog.17");
      double repriceValue = NumberSelectionDialog2.takeDoubleInput(dialogTitle, 0.0D, true);
      if ((repriceValue == -1.0D) || (repriceValue > ticket.getTotalAmount().doubleValue()))
        return;
      discountValue = Double.valueOf(repriceValue);
      if (discountValue.doubleValue() == 0.0D) {
        return;
      }
    }
    else {
      discountValue = discount.getValue();
      if (discount.isModifiable().booleanValue()) {
        discountValue = getModifiedValue(discount);
        if (discountValue == null) {
          return;
        }
      }
    }
    if ((orderDiscount == null) || (orderDiscount.getValue().doubleValue() != discountValue.doubleValue())) {
      orderDiscount = Ticket.convertToTicketDiscount(discount, ticket);
      ticket.addTodiscounts(orderDiscount);
    }
    else {
      orderDiscount.setCouponQuantity(Double.valueOf(orderDiscount.getCouponQuantity().doubleValue() + 1.0D));
    }
    orderDiscount.setValue(discountValue);
    rendererTicketDiscounts();
  }
  
  private Double getModifiedValue(Discount discount) {
    ticket.calculatePrice();
    Double newValue = Double.valueOf(NumberSelectionDialog2.takeDoubleInput(Messages.getString("TicketDiscountSelectionDialog.3"), discount.getValue().doubleValue(), true));
    if (newValue.doubleValue() == -1.0D)
      return null;
    if (newValue.doubleValue() > 0.0D) {
      return newValue;
    }
    return Double.valueOf(0.0D);
  }
  
  private class DiscountButton extends JPanel implements ActionListener {
    private Discount discount;
    private PosButton btnDiscount;
    
    DiscountButton(Discount discount) {
      this.discount = discount;
      setPreferredSize(PosUIManager.getSize(120, 120));
      setLayout(new MigLayout("fill", "", "[60%][40%]"));
      
      JLabel lblCoupon = new JLabel();
      lblCoupon.setOpaque(false);
      lblCoupon.setHorizontalAlignment(0);
      lblCoupon.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
      lblCoupon.setText("<html><body><center><strong>" + discount.getName() + "</strong></center></body></html>");
      setBackground(Color.WHITE);
      setForeground(Color.BLACK);
      
      add(lblCoupon, "grow,span,wrap");
      
      btnDiscount = new PosButton(POSConstants.APPLY);
      btnDiscount.addActionListener(this);
      add(btnDiscount, "center");
    }
    
    protected void paintComponent(Graphics g)
    {
      Graphics2D g2 = (Graphics2D)g;
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      setBorder(BorderFactory.createLineBorder(Color.red));
      super.paintComponent(g);
    }
    
    public void actionPerformed(ActionEvent e) {
      TicketDiscountSelectionDialog.this.doApplyDiscountOnTicket(discount);
    }
    
    public void setSelected(Boolean selected) {
      btnDiscount.setSelected(selected.booleanValue());
      if (selected.booleanValue()) {
        btnDiscount.setText(Messages.getString("TicketItemDiscountSelectionDialog.13"));
        btnDiscount.setBackground(Color.PINK);
      }
      else {
        btnDiscount.setText(POSConstants.APPLY);
        btnDiscount.setBackground(UIManager.getColor("control"));
      }
    }
  }
  
  public class DiscountTableModel extends ListTableModel<TicketDiscount> {
    public DiscountTableModel() {
      super();
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      if (columnIndex == 0)
        return true;
      return false;
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      TicketDiscount discount = (TicketDiscount)rows.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return discount.getNameDisplay();
      
      case 1: 
        if (discount.getType().intValue() == 2) {
          return discount.getValue();
        }
        return NumberUtil.formatNumberAcceptNegative(Double.valueOf(-DiscountUtil.calculateDiscountAmount(ticket.getSubtotalAmount().doubleValue(), discount).doubleValue() * discount.getCouponQuantity().doubleValue()));
      }
      return null;
    }
  }
}
