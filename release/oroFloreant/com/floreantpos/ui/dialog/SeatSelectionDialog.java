package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.ShopTableDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import net.miginfocom.swing.MigLayout;


















public class SeatSelectionDialog
  extends POSDialog
  implements ActionListener
{
  private TitlePanel titlePanel;
  private Integer selectedSeat;
  private List<Integer> seats = new ArrayList();
  private List<Integer> orderSeats;
  
  public SeatSelectionDialog(List<Integer> tableNumbers, List<Integer> orderSeats) {
    List<ShopTable> tables = ShopTableDAO.getInstance().getByNumbers(tableNumbers);
    int orderSeatNumber; if (tables != null) {
      int seatNumber = 1;
      for (ShopTable shopTable : tables)
        for (i = 0; i < shopTable.getCapacity().intValue(); i++) {
          seats.add(Integer.valueOf(seatNumber));
          seatNumber++;
        }
      int i;
      this.orderSeats = orderSeats;
      if (orderSeats != null) {
        for (Integer i = Integer.valueOf(0); i.intValue() < orderSeats.size(); i = i = Integer.valueOf(i.intValue() + 1)) {
          orderSeatNumber = ((Integer)orderSeats.get(i.intValue())).intValue();
          if (orderSeatNumber != 0)
          {
            if (!seats.contains(Integer.valueOf(orderSeatNumber))) {
              seats.add(Integer.valueOf(orderSeatNumber));
            }
          }
          orderSeatNumber = i;
        }
      }
    }
    




    init();
  }
  
  public SeatSelectionDialog(Ticket ticket, List<Integer> orderSeats) {
    List<ShopTable> tables = ticket.getTables();
    int orderSeatNumber; if (tables != null) {
      int seatNumber = 1;
      for (ShopTable shopTable : tables)
        for (i = 0; i < shopTable.getCapacity().intValue(); i++) {
          seats.add(Integer.valueOf(seatNumber));
          seatNumber++;
        }
      int i;
      this.orderSeats = orderSeats;
      if (orderSeats != null) {
        for (Integer i = Integer.valueOf(0); i.intValue() < orderSeats.size(); i = i = Integer.valueOf(i.intValue() + 1)) {
          orderSeatNumber = ((Integer)orderSeats.get(i.intValue())).intValue();
          if (orderSeatNumber != 0)
          {
            if (!seats.contains(Integer.valueOf(orderSeatNumber))) {
              seats.add(Integer.valueOf(orderSeatNumber));
            }
          }
          orderSeatNumber = i;
        }
      }
    }
    




    init();
  }
  
  private void init() {
    setResizable(false);
    
    Container contentPane = getContentPane();
    
    MigLayout layout = new MigLayout("fillx,wrap 3", "[60px,fill][60px,fill][60px,fill]", "[][][][][]");
    contentPane.setLayout(layout);
    
    titlePanel = new TitlePanel();
    contentPane.add(titlePanel, "spanx ,growy,height 60,wrap");
    
    for (Integer seat : seats) {
      PosButton posButton = new PosButton();
      posButton.setFocusable(false);
      posButton.setFont(posButton.getFont().deriveFont(1, 24.0F));
      posButton.setText(String.valueOf(seat));
      if ((orderSeats != null) && (orderSeats.contains(seat))) {
        posButton.setBackground(Color.GREEN);
      }
      posButton.addActionListener(this);
      String constraints = "grow, height 55";
      contentPane.add(posButton, constraints);
    }
    PosButton btnShared = new PosButton(Messages.getString("SeatSelectionDialog.0"));
    btnShared.setFocusable(false);
    if ((orderSeats != null) && (orderSeats.contains(Integer.valueOf(0)))) {
      btnShared.setBackground(Color.GREEN);
    }
    btnShared.addActionListener(this);
    contentPane.add(btnShared, "grow, height 55");
    
    PosButton btnCustom = new PosButton(Messages.getString("SeatSelectionDialog.1"));
    btnCustom.setFocusable(false);
    btnCustom.addActionListener(this);
    contentPane.add(btnCustom, "grow, height 55");
    
    PosButton btnCancel = new PosButton(Messages.getString("SeatSelectionDialog.2"));
    btnCancel.setFocusable(false);
    btnCancel.addActionListener(this);
    contentPane.add(btnCancel, "newline, span,grow, height 55");
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    
    boolean canceled = false;
    if (Messages.getString("SeatSelectionDialog.0").equalsIgnoreCase(actionCommand)) {
      selectedSeat = Integer.valueOf(0);
    }
    else if (Messages.getString("SeatSelectionDialog.1").equalsIgnoreCase(actionCommand)) {
      selectedSeat = Integer.valueOf(-1);
    }
    else if (Messages.getString("SeatSelectionDialog.2").equalsIgnoreCase(actionCommand)) {
      canceled = true;
    }
    else {
      selectedSeat = Integer.valueOf(actionCommand);
    }
    setCanceled(canceled);
    dispose();
  }
  
  public Integer getSeatNumber() {
    return selectedSeat;
  }
  
  public void setTitle(String title) {
    titlePanel.setTitle(title);
    
    super.setTitle(title);
  }
  
  public void setDialogTitle(String title) {
    super.setTitle(title);
  }
}
