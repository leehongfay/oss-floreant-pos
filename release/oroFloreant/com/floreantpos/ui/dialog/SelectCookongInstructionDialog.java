package com.floreantpos.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.model.CookingInstruction;
import com.floreantpos.model.dao.CookingInstructionDAO;
import com.floreantpos.swing.PosButton;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;

















public class SelectCookongInstructionDialog
  extends POSDialog
  implements ActionListener
{
  private CookingInstruction cookingInstruction;
  private JComboBox cbCookingInstructions;
  private PosButton btnNew;
  private PosButton btnOk;
  private PosButton btnCancel;
  
  public SelectCookongInstructionDialog()
    throws HeadlessException
  {}
  
  protected void initUI()
  {
    setLayout(new MigLayout());
    
    CookingInstructionDAO dao = new CookingInstructionDAO();
    List<CookingInstruction> cookingInstructions = dao.findAll();
    DefaultComboBoxModel cbModel = new DefaultComboBoxModel(cookingInstructions.toArray());
    
    cbCookingInstructions = new JComboBox(cbModel);
    cbCookingInstructions.setFont(cbCookingInstructions.getFont().deriveFont(16));
    btnNew = new PosButton(POSConstants.NEW);
    btnOk = new PosButton(POSConstants.OK);
    btnCancel = new PosButton(POSConstants.CANCEL);
    add(cbCookingInstructions, "wrap, span, grow, h 30");
    add(new JSeparator(), "wrap, span, grow");
    add(btnNew, "al right,width 120, height 30");
    add(btnOk, "al right,width 120, height 30");
    add(btnCancel, "width 120, height 30");
    
    btnNew.addActionListener(this);
    btnOk.addActionListener(this);
    btnCancel.addActionListener(this);
  }
  
  private void doOk() {
    cookingInstruction = ((CookingInstruction)cbCookingInstructions.getSelectedItem());
    setCanceled(false);
    dispose();
  }
  
  private void doCancel() {
    setCanceled(true);
    dispose();
  }
  
  private void doCreateNew() {
    NewCookongInstructionDialog dialog = new NewCookongInstructionDialog();
    dialog.pack();
    dialog.open();
    
    if (!dialog.isCanceled()) {
      cookingInstruction = dialog.getCookingInstruction();
      DefaultComboBoxModel model = (DefaultComboBoxModel)cbCookingInstructions.getModel();
      model.addElement(cookingInstruction);
      model.setSelectedItem(cookingInstruction);
    }
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    
    if (POSConstants.OK.equalsIgnoreCase(actionCommand)) {
      doOk();
    }
    else if (POSConstants.CANCEL.equalsIgnoreCase(actionCommand)) {
      doCancel();
    }
    else if (POSConstants.NEW.equalsIgnoreCase(actionCommand)) {
      doCreateNew();
    }
  }
  
  public CookingInstruction getCookingInstruction() {
    return cookingInstruction;
  }
  
  public CookingInstruction getSelectedCookingInstruction() {
    return cookingInstruction;
  }
}
