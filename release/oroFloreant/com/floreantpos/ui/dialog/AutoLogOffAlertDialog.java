package com.floreantpos.ui.dialog;

import com.floreantpos.config.AppProperties;
import com.floreantpos.main.Application;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

public class AutoLogOffAlertDialog
  extends POSDialog
{
  private Timer timer;
  private JLabel lblCountTimer;
  
  public AutoLogOffAlertDialog()
  {
    super(Application.getPosWindow(), true);
    setTitle(AppProperties.getAppName());
    initComponents();
    startCountDown();
  }
  
  private void startCountDown() {
    timer = new Timer(0, new CountDownTask(null));
    timer.setDelay(1000);
    timer.start();
  }
  
  private void initComponents() {
    JPanel contentPanel = new JPanel(new BorderLayout(5, 5));
    contentPanel.setBorder(BorderFactory.createEmptyBorder(20, 30, 20, 30));
    JLabel lblAutoLogOffMsg = new JLabel("Auto log off in", 0);
    Font font1 = new Font(lblAutoLogOffMsg.getName(), 1, PosUIManager.getFontSize(30));
    lblAutoLogOffMsg.setFont(font1);
    lblCountTimer = new JLabel("30 s", 0);
    Font font2 = new Font(lblCountTimer.getName(), 1, PosUIManager.getFontSize(300));
    lblCountTimer.setFont(font2);
    PosButton btnCancel = new PosButton("Cancel");
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
    });
    contentPanel.add(lblAutoLogOffMsg, "North");
    contentPanel.add(lblCountTimer);
    contentPanel.add(btnCancel, "South");
    add(contentPanel);
  }
  
  private class CountDownTask implements ActionListener {
    int count = 30;
    int countLabelFontSize = PosUIManager.getFontSize(300);
    int subscriptFontSize = PosUIManager.getFontSize(36);
    
    private CountDownTask() {}
    
    public void actionPerformed(ActionEvent e) { if (count >= 0) {
        String text = String.format("<html><span style='font-size: %d'>%d</span><sub style='font-size: %d'>sec</sub>", new Object[] { Integer.valueOf(countLabelFontSize), Integer.valueOf(count), Integer.valueOf(subscriptFontSize) });
        lblCountTimer.setText(text);
      }
      else {
        timer.stop();
        setCanceled(false);
        dispose();
      }
      count -= 1;
    }
  }
}
