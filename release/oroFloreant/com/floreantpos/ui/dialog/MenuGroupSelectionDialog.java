package com.floreantpos.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.model.MenuGroupForm;
import com.floreantpos.ui.views.MenuGroupSelectionView;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
















public class MenuGroupSelectionDialog
  extends POSDialog
  implements ActionListener
{
  private MenuGroupSelectionView menuGroupSelectorPanel;
  private List<MenuGroup> menuGroupList;
  private int selectionMode;
  private TitlePanel titelpanel;
  private static MenuGroupSelectionDialog instance;
  
  public MenuGroupSelectionDialog(List<MenuGroup> menuGroupList)
  {
    super(POSUtil.getFocusedWindow(), "");
    this.menuGroupList = menuGroupList;
    initComponents();
  }
  
  public MenuGroupSelectionDialog() {
    super(POSUtil.getFocusedWindow(), "");
    setTitle("Select menu group");
    initComponents();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    setTitle("Select Menu Group");
    titelpanel = new TitlePanel();
    titelpanel.setTitle("Select one or more menu group");
    
    add(titelpanel, "North");
    
    JPanel contentPane = new JPanel(new MigLayout("fill,hidemode 3,inset 0 10 0 10"));
    menuGroupSelectorPanel = new MenuGroupSelectionView(menuGroupList);
    contentPane.add(menuGroupSelectorPanel, "grow,span,wrap");
    
    PosButton btnOk = new PosButton("SELECT");
    btnOk.setActionCommand(POSConstants.OK);
    btnOk.setBackground(Color.GREEN);
    
    btnOk.setFocusable(false);
    btnOk.addActionListener(this);
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL);
    btnCancel.setFocusable(false);
    btnCancel.addActionListener(this);
    
    JPanel footerPanel = new JPanel(new MigLayout("center,ins 0 5 5 5", "", ""));
    
    PosButton btnEdit = new PosButton();
    PosButton btnAdd = new PosButton();
    
    btnAdd.setText(POSConstants.ADD.toUpperCase());
    btnEdit.setText(POSConstants.EDIT.toUpperCase());
    
    btnEdit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        MenuGroupSelectionDialog.this.editSelectedRow();
      }
      

    });
    btnAdd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          MenuGroup menuGroup = new MenuGroup();
          BeanEditor<MenuGroup> editor = new MenuGroupForm(menuGroup);
          
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.setPreferredSize(PosUIManager.getSize(900, 650));
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          menuGroup = (MenuGroup)editor.getBean();
          menuGroupSelectorPanel.getModel().addRow(menuGroup);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    footerPanel.add(btnAdd);
    footerPanel.add(btnEdit);
    footerPanel.add(btnOk);
    footerPanel.add(btnCancel);
    
    add(footerPanel, "South");
    
    add(contentPane);
  }
  
  public void setSelectionMode(int selectionMode) {
    this.selectionMode = selectionMode;
    if (selectionMode == 0) {
      titelpanel.setTitle("SELECT A MENU GROUP");
    }
    else {
      titelpanel.setTitle("Select one or more menu group");
    }
    menuGroupSelectorPanel.setSelectionMode(selectionMode);
  }
  
  public void setSelectedGroup(MenuCategory menuCategory) {
    menuGroupSelectorPanel.setSelectedCategory(menuCategory);
  }
  
  private void doOk() {
    if (selectionMode == 0) {
      MenuGroup menuItem = getSelectedRowData();
      if (menuItem == null) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select menu group");
        return;
      }
    }
    else {
      List<MenuGroup> menuGroupList = menuGroupSelectorPanel.getSelectedMenuGroupList();
      if ((menuGroupList == null) || (menuGroupList.isEmpty())) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select menu group");
        return;
      }
    }
    setCanceled(false);
    dispose();
  }
  
  private void doCancel() {
    setCanceled(true);
    dispose();
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    
    if (POSConstants.CANCEL.equalsIgnoreCase(actionCommand)) {
      doCancel();
    }
    else if (POSConstants.OK.equalsIgnoreCase(actionCommand)) {
      doOk();
    }
  }
  
  private void editSelectedRow() {
    try {
      int index = menuGroupSelectorPanel.getSelectedRow();
      if (index < 0)
        return;
      MenuGroup menuGroup = (MenuGroup)menuGroupSelectorPanel.getModel().getRow(index);
      MenuGroupDAO.getInstance().initialize(menuGroup);
      menuGroupSelectorPanel.getModel().setRow(index, menuGroup);
      
      BeanEditor<MenuGroup> editor = new MenuGroupForm(menuGroup);
      
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      menuGroupSelectorPanel.repaintTable();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public List<MenuGroup> getSelectedMenuGroups() {
    return menuGroupSelectorPanel.getSelectedMenuGroupList();
  }
  
  public MenuGroup getSelectedRowData() {
    int index = menuGroupSelectorPanel.getSelectedRow();
    if (index < 0)
      return null;
    return (MenuGroup)menuGroupSelectorPanel.getModel().getRow(index);
  }
  
  public static MenuGroupSelectionDialog getInstance() {
    if (instance == null) {
      instance = new MenuGroupSelectionDialog();
    }
    return instance;
  }
}
