package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.actions.DrawerAssignmentAction;
import com.floreantpos.actions.StaffBankCloseAction;
import com.floreantpos.config.AppProperties;
import com.floreantpos.main.Application;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.CashDropTransaction;
import com.floreantpos.model.DrawerType;
import com.floreantpos.model.PaymentType;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.TipsCashoutReport;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.CashDrawerDAO;
import com.floreantpos.model.dao.CashDropTransactionDAO;
import com.floreantpos.model.dao.GratuityDAO;
import com.floreantpos.model.dao.PosTransactionDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.print.PosPrintService;
import com.floreantpos.swing.ListComboBoxModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.DrawerUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXDatePicker;





































public class CashDrawerInfoDialog
  extends POSDialog
  implements ActionListener
{
  private TitlePanel titlePanel;
  private CashDrawer cashDrawer;
  private PosButton btnCloseDrawer;
  private PosButton btnDrawerBleed;
  private PosButton btnNoSale;
  private PosButton btnPayout;
  private PosButton btnCloseStaffBank;
  private PosButton btnEncashTips;
  private PosButton btnDeclareTips;
  private User currentUser;
  private Terminal drawerTerminal;
  private PosButton btnPrint;
  private PosButton btnTransactionDetails;
  private CashDrawerInfoView reportViewPanel;
  private JPanel serverLeftActionPanel;
  private JPanel buttonPanel;
  private PosButton btnFinish;
  private boolean showSummaryReport;
  private boolean showInfoOnly;
  
  public CashDrawerInfoDialog(CashDrawer report)
  {
    this(null, report);
  }
  
  public CashDrawerInfoDialog(User currentUser, CashDrawer report) {
    this(currentUser, report, false);
  }
  
  public CashDrawerInfoDialog(User currentUser, CashDrawer report, boolean summaryReport) {
    super(POSUtil.getFocusedWindow());
    setModal(true);
    this.currentUser = currentUser;
    cashDrawer = report;
    drawerTerminal = report.getTerminal();
    showSummaryReport = summaryReport;
    initComponents();
    setSize(PosUIManager.getSize(800, 670));
  }
  
  private void updateView() {
    if (showInfoOnly)
      return;
    if ((cashDrawer.getId() == null) || (cashDrawer.isClosed())) {
      setEnableFields(false);
      return;
    }
    boolean thisTerminalDrawer = cashDrawer.getTerminal().getId().intValue() == Application.getInstance().getTerminal().getId().intValue();
    boolean staffBank = cashDrawer.getDrawerType() == DrawerType.STAFF_BANK;
    if (staffBank) {
      btnCloseDrawer.setText("CLOSE STAFF BANK");
    }
    btnCloseDrawer.setEnabled((cashDrawer.getId() != null) && (cashDrawer.isOpen()));
    btnCloseStaffBank.setEnabled((cashDrawer.getId() != null) && (cashDrawer.isOpen()));
    btnDeclareTips.setVisible(staffBank);
    btnEncashTips.setVisible(staffBank);
    btnCloseStaffBank.setVisible(staffBank);
    
    btnDrawerBleed.setVisible(!staffBank);
    btnNoSale.setVisible((!staffBank) && (thisTerminalDrawer));
    btnPayout.setVisible(thisTerminalDrawer);
    btnCloseDrawer.setVisible(!staffBank);
  }
  
  private void setEnableFields(boolean b) {
    btnCloseDrawer.setEnabled(b);
    btnCloseStaffBank.setVisible(b);
    btnDeclareTips.setEnabled(b);
    btnEncashTips.setEnabled(b);
    btnCloseStaffBank.setEnabled(b);
    btnDrawerBleed.setEnabled(b);
    btnPayout.setEnabled(b);
    btnCloseDrawer.setVisible(b);
  }
  
  public void showInfoOnly(boolean b) {
    showInfoOnly = b;
    serverLeftActionPanel.setVisible(!b);
    if (showInfoOnly) {
      buttonPanel.removeAll();
      buttonPanel.add(new JSeparator(), "grow,span,wrap");
      buttonPanel.add(btnTransactionDetails, "grow");
      buttonPanel.add(btnPrint, "grow");
      buttonPanel.add(btnFinish, "grow");
    }
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    
    titlePanel = new TitlePanel();
    add(titlePanel, "North");
    
    reportViewPanel = new CashDrawerInfoView(cashDrawer, showSummaryReport);
    add(reportViewPanel);
    
    buttonPanel = new JPanel(new MigLayout("fill,hidemode 3,ins 5 5 5 5,wrap 4", "[grow]", ""));
    btnPrint = new PosButton(Messages.getString("DrawerPullReportDialog.8"));
    btnFinish = new PosButton(POSConstants.SAVE_BUTTON_TEXT);
    
    btnDrawerBleed = new PosButton(POSConstants.DRAWER_BLEED);
    btnNoSale = new PosButton(Messages.getString("ManagerDialog.1"));
    btnPayout = new PosButton(POSConstants.PAYOUT_BUTTON_TEXT);
    btnCloseDrawer = new PosButton("CLOSE DRAWER");
    
    btnDrawerBleed.addActionListener(this);
    btnNoSale.addActionListener(this);
    btnPayout.addActionListener(this);
    
    btnCloseDrawer.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          if (!currentUser.isClockedIn().booleanValue()) {
            POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "You are not clockd in.\n Please clock in first to close drawer.");
            return;
          }
          DrawerUtil.kickDrawer();
          DrawerAssignmentAction drawerAction = new DrawerAssignmentAction(drawerTerminal, currentUser);
          drawerAction.execute();
          cashDrawer = CashDrawerDAO.getInstance().get(cashDrawer.getId());
          refreshReport();
        } catch (Exception ex) {
          PosLog.error(getClass(), ex);
        }
        
      }
    });
    btnCloseStaffBank = new PosButton("CLOSE STAFF BANK");
    btnCloseStaffBank.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          if (!currentUser.isClockedIn().booleanValue()) {
            POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "You are not clockd in.\n Please clock in first to staff bank.");
            return;
          }
          StaffBankCloseAction drawerAction = new StaffBankCloseAction(cashDrawer, currentUser);
          drawerAction.execute();
          cashDrawer = CashDrawerDAO.getInstance().get(cashDrawer.getId());
          refreshReport();
        } catch (Exception ex) {
          PosLog.error(getClass(), ex);
        }
        
      }
    });
    btnDeclareTips = new PosButton("DECLARE TIPS");
    btnDeclareTips.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        CashDrawerInfoDialog.this.doAddTips();
      }
    });
    btnEncashTips = new PosButton("ENCASH TIPS");
    btnEncashTips.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        CashDrawerInfoDialog.this.doAddEncashTips();
      }
      
    });
    serverLeftActionPanel = new JPanel(new MigLayout("fill,hidemode 3,ins 0 20 0 20,wrap 1", "[120px,grow]", ""));
    
    buttonPanel.add(new JSeparator(), "grow,span,wrap");
    buttonPanel.add(btnPrint, "gapleft 160,grow");
    
    btnTransactionDetails = new PosButton(POSConstants.DETAILS.toUpperCase());
    btnTransactionDetails.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        CashDrawerInfoDialog.this.showTransactionDetails();
      }
      
    });
    serverLeftActionPanel.add(btnDrawerBleed, "grow");
    serverLeftActionPanel.add(btnNoSale, "grow");
    serverLeftActionPanel.add(btnPayout, "grow");
    
    buttonPanel.add(btnTransactionDetails, "grow");
    buttonPanel.add(btnCloseDrawer, "grow");
    buttonPanel.add(btnCloseStaffBank, "grow");
    
    serverLeftActionPanel.add(btnEncashTips, "grow");
    

    buttonPanel.add(btnFinish, "grow");
    
    add(buttonPanel, "South");
    add(serverLeftActionPanel, "West");
    
    btnFinish.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        CashDrawerInfoDialog.this.doCloseDialog();
      }
    });
    btnPrint.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        CashDrawerInfoDialog.this.doPrintReport();
      }
    });
  }
  
  public void refreshReport() throws Exception
  {
    reportViewPanel.createReport(!showInfoOnly);
    updateView();
  }
  
  private void showTransactionDetails() {
    try {
      if (cashDrawer == null) {
        return;
      }
      if (showSummaryReport) {
        List<PosTransaction> transactions = PosTransactionDAO.getInstance().getStoreSessionTransactions(cashDrawer.getStoreOperationData());
        cashDrawer.setTransactions(transactions);
      }
      CashDrawerTransactionInfoDialog dialog = new CashDrawerTransactionInfoDialog(cashDrawer);
      dialog.setTitle(cashDrawer.getType().intValue() == DrawerType.DRAWER.getTypeNumber() ? "Cash Drawer Sales details" : "Server Bank Sales details");
      dialog.initialize();
      dialog.setDefaultCloseOperation(2);
      dialog.open();
    } catch (Exception e2) {
      PosLog.error(getClass(), e2);
    }
  }
  
  private void doAddTips() {
    try {
      if (cashDrawer == null) {
        return;
      }
      Double declaredTipsAmount = Double.valueOf(NumberSelectionDialog2.takeDoubleInput("Enter tips amount", "Tips Amount", cashDrawer.getTipsPaid().doubleValue()));
      if (declaredTipsAmount.isNaN()) {
        return;
      }
      cashDrawer.setDeclaredTips(declaredTipsAmount);
      TerminalDAO.getInstance().performBatchSave(new Object[] { cashDrawer });
      refreshReport();
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    }
  }
  
  private void doAddEncashTips() {
    doManageGratuity();
  }
  












  private void doCloseDialog()
  {
    dispose();
  }
  
  public void setTitle(String title) {
    titlePanel.setTitle(title);
    super.setTitle(AppProperties.getAppName());
  }
  
  private void doPrintReport() {
    try {
      PosPrintService.printDrawerPullReport(cashDrawer);
    } catch (Exception ex) {
      POSMessageDialog.showError(this, Messages.getString("DrawerPullReportDialog.122") + ex.getMessage());
      PosLog.error(getClass(), ex);
    }
  }
  
  public void actionPerformed(ActionEvent e)
  {
    PosButton sourceBtn = (PosButton)e.getSource();
    if (sourceBtn == btnDrawerBleed) {
      doDrawerBleed();
    }
    else if (sourceBtn == btnNoSale) {
      doDrawerKick();
    }
    else if (sourceBtn == btnPayout) {
      doPayout();
    }
  }
  
  private void doManageGratuity() {
    GratuityDialog dialog = new GratuityDialog(this, currentUser);
    dialog.setCaption("Tips Management");
    dialog.setOkButtonText("PAY");
    dialog.setSize(PosUIManager.getSize(780, 550));
    dialog.open();
    if (dialog.isCanceled())
      return;
    try {
      refreshReport();
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    }
  }
  
  private void doDrawerBleed() {
    try {
      DrawerUtil.kickDrawer();
      NumberSelectionDialog2 dialog = new NumberSelectionDialog2();
      dialog.setTitle(Messages.getString("CashDropDialog.17"));
      dialog.setFloatingPoint(true);
      dialog.pack();
      dialog.open();
      
      if (!dialog.isCanceled()) {
        double amount = dialog.getValue();
        CashDropTransaction transaction = new CashDropTransaction();
        transaction.setDrawerResetted(Boolean.valueOf(false));
        transaction.setTerminal(drawerTerminal);
        
        if (currentUser.isStaffBankStarted().booleanValue()) {
          transaction.setCashDrawer(currentUser.getActiveDrawerPullReport());
        } else {
          transaction.setCashDrawer(drawerTerminal.getCurrentCashDrawer());
        }
        transaction.setPaymentType(PaymentType.CASH.toString());
        transaction.setUser(currentUser);
        transaction.setTransactionTime(new Date());
        transaction.setAmount(Double.valueOf(amount));
        
        CashDropTransactionDAO dao = new CashDropTransactionDAO();
        dao.saveNewCashDrop(transaction, drawerTerminal);
      }
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("CashDropDialog.18"), e);
    }
  }
  
  private void doDrawerKick() {
    try {
      
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private void doPayout() {
    try {
      TerminalDAO.getInstance().refresh(drawerTerminal);
      if (!POSUtil.checkDrawerAssignment(drawerTerminal, currentUser)) {
        return;
      }
      DrawerUtil.kickDrawer();
      PayoutDialog dialog = new PayoutDialog(this, currentUser, drawerTerminal);
      dialog.open();
      refreshReport();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private void doManageServerTips() {
    try {
      JPanel panel = new JPanel(new MigLayout());
      List<User> users = UserDAO.getInstance().findAll();
      
      JXDatePicker fromDatePicker = UiUtil.getCurrentMonthStart();
      JXDatePicker toDatePicker = UiUtil.getCurrentMonthEnd();
      
      panel.add(new JLabel(POSConstants.SELECT_USER + ":"), "grow");
      JComboBox userCombo = new JComboBox(new ListComboBoxModel(users));
      panel.add(userCombo, "grow, wrap");
      panel.add(new JLabel(POSConstants.FROM + ":"), "grow");
      panel.add(fromDatePicker, "wrap");
      panel.add(new JLabel(POSConstants.TO_), "grow");
      panel.add(toDatePicker);
      
      int option = JOptionPane.showOptionDialog(Application.getPosWindow(), panel, POSConstants.SELECT_CRIETERIA, 2, 3, null, null, null);
      
      if (option != 0) {
        return;
      }
      
      GratuityDAO gratuityDAO = new GratuityDAO();
      TipsCashoutReport report = gratuityDAO.createReport(fromDatePicker.getDate(), toDatePicker.getDate(), (User)userCombo.getSelectedItem());
      
      TipsCashoutReportDialog dialog = new TipsCashoutReportDialog(report);
      dialog.setSize(400, 600);
      dialog.open();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
}
