package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.actions.GroupSettleTicketAction;
import com.floreantpos.actions.SettleTicketAction;
import com.floreantpos.actions.TicketTransferAction;
import com.floreantpos.main.Application;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.ButtonColumn;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;

































public class ServerOpenTicketListDialog
  extends POSDialog
{
  private TitlePanel titlePanel;
  private JXTable tableTicket;
  private TicketTableModel tableModel;
  private List<Ticket> ticketList;
  private DefaultListSelectionModel selectionModel;
  private User currentUser;
  private User serverUser;
  private PosButton btnCancel;
  private boolean filterByUser;
  
  public ServerOpenTicketListDialog(User currentUser, User user, boolean filterByUser)
  {
    super(Application.getPosWindow(), true);
    this.currentUser = currentUser;
    serverUser = user;
    this.filterByUser = filterByUser;
    initComponents();
    initData();
    updateView();
  }
  
  public void setTitle(String title)
  {
    super.setTitle(POSConstants.OPEN_TICKETS);
    titlePanel.setTitle(title);
  }
  
  private void initComponents() {
    TransparentPanel container = new TransparentPanel();
    titlePanel = new TitlePanel();
    TransparentPanel contentPanel = new TransparentPanel();
    TransparentPanel bottomActionPanel = new TransparentPanel(new MigLayout("al center", "sg, fill", ""));
    
    JSeparator jSeparator1 = new JSeparator();
    
    btnCancel = new PosButton();
    
    JScrollPane jScrollPane1 = new JScrollPane();
    tableTicket = new JXTable();
    tableTicket.setRowHeight(PosUIManager.getSize(30));
    tableTicket.getTableHeader().setPreferredSize(PosUIManager.getSize(0, 30));
    
    getContentPane().setLayout(new BorderLayout(5, 5));
    
    container.setLayout(new BorderLayout(5, 5));
    container.add(jSeparator1, "North");
    
    PosButton btnTransferAll = new PosButton(POSConstants.TRANSFER_ALL);
    btnTransferAll.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        ServerOpenTicketListDialog.this.doTransferTickets();
      }
      
    });
    bottomActionPanel.add(btnTransferAll);
    
    PosButton btnSettleAll = new PosButton(POSConstants.SETTLE_ALL);
    btnSettleAll.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        ServerOpenTicketListDialog.this.doGroupSettle();
      }
      
    });
    bottomActionPanel.add(btnSettleAll);
    
    PosButton btnCloseAll = new PosButton(POSConstants.CLOSE_ALL);
    btnCloseAll.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        ServerOpenTicketListDialog.this.doCloseTickets();
      }
      
    });
    bottomActionPanel.add(btnCloseAll);
    
    btnCancel.setText("DONE");
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        ServerOpenTicketListDialog.this.btnCloseActionPerformed(evt);
      }
      
    });
    bottomActionPanel.add(btnCancel);
    
    container.add(bottomActionPanel, "Center");
    getContentPane().add(container, "South");
    
    contentPanel.setLayout(new BorderLayout());
    contentPanel.add(titlePanel, "North");
    
    jScrollPane1.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(10, 10, 5, 10), jScrollPane1.getBorder()));
    jScrollPane1.setViewportView(tableTicket);
    
    contentPanel.add(jScrollPane1, "Center");
    JPanel cashSummaryPanel = new JPanel(new MigLayout("center,ins 0"));
    
    contentPanel.add(cashSummaryPanel, "South");
    getContentPane().add(contentPanel, "Center");
  }
  
  public void initData() {
    selectionModel = new DefaultListSelectionModel();
    selectionModel.setSelectionMode(0);
    tableTicket.getSelectionModel().setSelectionMode(0);
    
    tableTicket.setDefaultRenderer(Object.class, new TableRenderer());
    tableModel = new TicketTableModel();
    tableTicket.setModel(tableModel);
    
    AbstractAction transferAction = new AbstractAction()
    {
      public void actionPerformed(ActionEvent e)
      {
        int row = Integer.parseInt(e.getActionCommand());
        Ticket ticket = (Ticket)tableModel.getRowData(row);
        ServerOpenTicketListDialog.this.doTransferTicket(ticket);
        ServerOpenTicketListDialog.this.updateView();
      }
    };
    AbstractAction settleAction = new AbstractAction()
    {
      public void actionPerformed(ActionEvent e)
      {
        int row = Integer.parseInt(e.getActionCommand());
        Ticket ticket = (Ticket)tableModel.getRowData(row);
        new SettleTicketAction(ticket, currentUser).actionPerformed(e);
        ServerOpenTicketListDialog.this.updateView();
      }
    };
    AbstractAction closeTicketAction = new AbstractAction()
    {
      public void actionPerformed(ActionEvent e)
      {
        int row = Integer.parseInt(e.getActionCommand());
        Ticket ticket = (Ticket)tableModel.getRowData(row);
        doCloseTicket(ticket);
        ServerOpenTicketListDialog.this.updateView();
      }
    };
    addButtonColumnAction(transferAction, 5);
    addButtonColumnAction(settleAction, 6);
    addButtonColumnAction(closeTicketAction, 7);
    resizeTableColumns();
    TableColumnModelExt columnModel = (TableColumnModelExt)tableTicket.getColumnModel();
    columnModel.getColumnExt(2).setVisible(!filterByUser);
  }
  
  private void addButtonColumnAction(AbstractAction action, int columnNum) {
    ButtonColumn buttonColumn = new ButtonColumn(tableTicket, action, columnNum)
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        return super.getTableCellRendererComponent(table, value, false, hasFocus, row, column);
      }
      
      public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
      {
        return super.getTableCellEditorComponent(table, value, false, row, column);
      }
    };
    MatteBorder selectedBorder = BorderFactory.createMatteBorder(3, 3, 3, 3, tableTicket.getBackground());
    MatteBorder unselectedBorder = BorderFactory.createMatteBorder(3, 3, 3, 3, tableTicket.getBackground());
    
    Border border1 = new CompoundBorder(selectedBorder, btnCancel.getBorder());
    Border border2 = new CompoundBorder(unselectedBorder, btnCancel.getBorder());
    
    buttonColumn.setUnselectedBorder(border1);
    buttonColumn.setFocusBorder(border2);
  }
  
  private void doTransferTicket(Ticket ticket) {
    try {
      TicketTransferAction transferAction = new TicketTransferAction(ticket, currentUser);
      transferAction.execute();
      if (!transferAction.isTransfered()) {
        return;
      }
      updateView();
    } catch (Exception e1) {
      POSMessageDialog.showError(Messages.getString("UserTransferDialog.4"));
      PosLog.error(getClass(), e1);
    }
  }
  
  protected void doCloseTicket(Ticket ticket) {
    int due = (int)POSUtil.getDouble(ticket.getDueAmount());
    
    if (due != 0) {
      int option = POSMessageDialog.showYesNoQuestionDialog(Application.getPosWindow(), Messages.getString("ServerOpenTicketListDialog.0"), POSConstants.CONFIRM);
      
      if (option != 0) {
        return;
      }
    }
    int option = JOptionPane.showOptionDialog(Application.getPosWindow(), 
      Messages.getString("SwitchboardView.6") + ticket.getId() + Messages.getString("SwitchboardView.7"), POSConstants.CONFIRM, 2, 1, null, null, null);
    

    if (option != 0) {
      return;
    }
    TicketDAO.closeOrders(new Ticket[] { ticket });
  }
  
  private void doTransferTickets() {
    List<Ticket> tickets = tableModel.getRows();
    if ((tickets == null) || (tickets.isEmpty()))
      return;
    UserListDialog dialog = new UserListDialog();
    dialog.setTitle(Messages.getString("UserTransferDialog.0"));
    dialog.setCaption(Messages.getString("UserTransferDialog.1"));
    dialog.setSize(PosUIManager.getSize(400, 600));
    dialog.open();
    
    if (dialog.isCanceled())
      return;
    User selectedUser = dialog.getSelectedUser();
    if (selectedUser == null) {
      return;
    }
    try {
      for (Ticket ticket : tickets) {
        doTransferTicket(ticket, selectedUser);
      }
      POSMessageDialog.showMessage(Messages.getString("ServerOpenTicketListDialog.1"));
      updateView();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage());
    }
  }
  
  protected void doTransferTicket(Ticket ticket, User selectedUser) {
    ticket.setOwner(selectedUser);
    TicketDAO.getInstance().saveOrUpdate(ticket);
  }
  
  private void doCloseTickets() {
    List<Ticket> tickets = tableModel.getRows();
    if ((tickets == null) || (tickets.isEmpty()))
      return;
    int option = POSMessageDialog.showYesNoQuestionDialog(Application.getPosWindow(), Messages.getString("ServerOpenTicketListDialog.2"), POSConstants.CONFIRM);
    try
    {
      if (option == 0) {
        TicketDAO.closeOrders((Ticket[])tickets.toArray(new Ticket[tickets.size()]));
      }
      updateView();
    }
    catch (Exception localException) {}
  }
  
  private void doGroupSettle() {
    List<Ticket> ticketList = tableModel.getRows();
    if ((ticketList == null) || (ticketList.isEmpty())) {
      return;
    }
    List<Ticket> tickets = new ArrayList();
    for (Ticket ticket : ticketList) {
      if (!ticket.getOrderType().isBarTab().booleanValue())
      {
        tickets.add(ticket); }
    }
    GroupSettleTicketAction action = new GroupSettleTicketAction(tickets, currentUser);
    action.actionPerformed(null);
    updateView();
  }
  
  private void updateView() {
    if (filterByUser) {
      ticketList = TicketDAO.getInstance().findOpenTicketsForUser(serverUser);
    }
    else {
      ticketList = TicketDAO.getInstance().findOpenTickets();
    }
    if (ticketList != null) {
      for (Ticket ticket : ticketList) {
        TicketDAO.getInstance().loadFullTicket(ticket);
      }
    }
    tableModel.setItems(ticketList);
    tableModel.fireTableDataChanged();
  }
  
  private void resizeTableColumns() {
    tableTicket.setAutoResizeMode(4);
    setColumnWidth(0, PosUIManager.getSize(100));
    setColumnWidth(1, PosUIManager.getSize(150));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = tableTicket.getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  public void setInfo(String info) {
    titlePanel.setTitle(info);
  }
  
  private void btnCloseActionPerformed(ActionEvent evt) {
    setCanceled(false);
    dispose();
  }
  
  public boolean hasOpenTickets() {
    return tableModel.getRowCount() > 0;
  }
  
  class TicketTableModel extends ListTableModel<Ticket>
  {
    public TicketTableModel() {
      super();
    }
    
    public void setItems(List<Ticket> ticketList) {
      setRows(ticketList);
    }
    
    public int getRowCount() {
      if (ticketList == null) {
        return 0;
      }
      int size = ticketList.size();
      return size;
    }
    
    public int getColumnCount() {
      return 8;
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      if ((columnIndex == 5) || (columnIndex == 6) || (columnIndex == 7)) {
        return true;
      }
      return false;
    }
    
    public Ticket getSelectedRow() {
      int index = tableTicket.getSelectedRow();
      if (index < 0) {
        return null;
      }
      index = tableTicket.convertRowIndexToModel(index);
      return (Ticket)tableModel.getRowData(index);
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      if (ticketList == null)
        return "";
      Ticket t = (Ticket)ticketList.get(rowIndex);
      switch (columnIndex) {
      case 0: 
        return t.getTokenNo();
      case 1: 
        return t.getOrderType();
      case 2: 
        return t.getOwner();
      case 3: 
        return t.getTotalAmountWithTips();
      case 4: 
        return t.getDueAmount();
      case 5: 
        return POSConstants.TRANSFER;
      case 6: 
        return POSConstants.SETTLE;
      case 7: 
        return POSConstants.CLOSE.toUpperCase();
      }
      return "";
    }
  }
  

  class TableRenderer
    extends DefaultTableCellRenderer
  {
    public TableRenderer() {}
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
      JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      if ((value instanceof Date)) {
        label.setHorizontalAlignment(4);
      }
      if ((value instanceof Double)) {
        label.setHorizontalAlignment(4);
      }
      else if (column == 1) {
        label.setHorizontalAlignment(2);
      }
      else {
        label.setHorizontalAlignment(0);
      }
      return label;
    }
  }
}
