package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.QwertyKeyPad;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.POSUtil;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;


















public class ItemSearchDialog
  extends OkCancelOptionDialog
{
  private JComboBox cbMenuGroup;
  private JComboBox cbMenuCategory;
  private JTextField tfName;
  private PosButton btnSearch;
  private JTable table;
  private BeanTableModel<MenuItem> tableModel;
  private MenuItem menuItem;
  private OrderType orderType;
  
  public ItemSearchDialog()
  {
    super(POSUtil.getFocusedWindow(), POSConstants.SEARCH_ITEM_BUTTON_TEXT);
    setDefaultCloseOperation(2);
    setPreferredSize(PosUIManager.getSize(900, 700));
    init();
  }
  
  public ItemSearchDialog(Frame parent) {
    super(parent, POSConstants.SEARCH_ITEM_BUTTON_TEXT);
    setDefaultCloseOperation(2);
    setPreferredSize(PosUIManager.getSize(900, 700));
    init();
  }
  
  private void init() {
    setResizable(false);
    
    JPanel contentPane = getContentPanel();
    contentPane.setBorder(BorderFactory.createEmptyBorder(3, 15, 0, 15));
    
    MigLayout layout = new MigLayout("fillx, inset 0");
    contentPane.setLayout(layout);
    
    table = new JTable();
    table.setRowHeight(40);
    table.getTableHeader().setPreferredSize(PosUIManager.getSize(0, 30));
    table.addKeyListener(new KeyAdapter()
    {
      public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == 10) {
          doOk();
        }
      }
    });
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          doOk();
        }
        
      }
    });
    tableModel = new BeanTableModel(MenuItem.class, 20);
    tableModel.addColumn("BARCODE", "barcode");
    tableModel.addColumn("SKU", "sku");
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "displayName");
    tableModel.addColumn(POSConstants.PRICE.toUpperCase() + " (" + CurrencyUtil.getCurrencySymbol() + ")", "price");
    tableModel.addColumn(Messages.getString("MenuItemExplorer.14"), "availableUnit");
    
    table.setModel(tableModel);
    
    tfName = new JTextField();
    tfName.setFont(tfName.getFont().deriveFont(1, PosUIManager.getNumberFieldFontSize()));
    tfName.setBackground(Color.WHITE);
    
    btnSearch = new PosButton("Search");
    ActionListener searchActionListener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ItemSearchDialog.this.doSetMenuItem();
      }
      
    };
    tfName.addActionListener(searchActionListener);
    btnSearch.addActionListener(searchActionListener);
    KeyListener keyListener = new KeyListener()
    {
      public void keyTyped(KeyEvent e) {}
      


      public void keyReleased(KeyEvent e)
      {
        if (tfName.getText().length() > 2) {
          ItemSearchDialog.this.doSetMenuItem();
        } else if (tfName.getText().isEmpty()) {
          tableModel.removeAll();
        }
      }
      

      public void keyPressed(KeyEvent e) {}
    };
    tfName.addKeyListener(keyListener);
    
    contentPane.add(createSelectionPanel(), "spanx ,grow");
    contentPane.add(tfName, "spanx,split 2 , growx");
    contentPane.add(btnSearch, "wrap");
    contentPane.add(new PosScrollPane(table), "span,  grow");
    QwertyKeyPad keypad = new QwertyKeyPad();
    keypad.setKeyPadListener(keyListener);
    contentPane.add(keypad, "spanx ,grow");
    
    resizeColumnWidth(table);
    tfName.setFocusable(true);
    tfName.requestFocus();
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  public void updateFilterPanel(OrderType orderType) {
    this.orderType = orderType;
    cbMenuCategory.removeAllItems();
    cbMenuGroup.removeAllItems();
    cbMenuCategory.addItem(null);
    List<MenuCategory> categoryGroups = MenuCategoryDAO.getInstance().findActiveCategories(orderType);
    for (Iterator localIterator = categoryGroups.iterator(); localIterator.hasNext();) { categoryGroup = (MenuCategory)localIterator.next();
      cbMenuCategory.addItem(categoryGroup);
    }
    MenuCategory categoryGroup;
    cbMenuGroup.addItem(null);
    Object menuGroups = MenuGroupDAO.getInstance().loadActiveGroupsByOrderType(orderType);
    for (MenuGroup menuGroup : (List)menuGroups) {
      cbMenuGroup.addItem(menuGroup);
    }
  }
  
  private JPanel createSelectionPanel() {
    JPanel panel = new JPanel();
    panel.setLayout(new MigLayout("ins 0", "[][]15[][]", "[]5[]"));
    
    JLabel lbCategory = new JLabel("Category");
    cbMenuCategory = new JComboBox();
    
    JLabel lbGroup = new JLabel("Group");
    cbMenuGroup = new JComboBox();
    
    cbMenuCategory.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        MenuCategory selectedItem = (MenuCategory)cbMenuCategory.getSelectedItem();
        List<MenuGroup> menuGroups = null;
        
        if (selectedItem == null) {
          menuGroups = MenuGroupDAO.getInstance().loadActiveGroupsByOrderType(orderType);
        }
        else {
          menuGroups = MenuGroupDAO.getInstance().findByParent(selectedItem);
        }
        
        DefaultComboBoxModel model = (DefaultComboBoxModel)cbMenuGroup.getModel();
        model.removeAllElements();
        model.addElement(null);
        for (MenuGroup menuGroup : menuGroups) {
          model.addElement(menuGroup);
        }
        
      }
    });
    disableFocus(new Component[] { lbCategory, cbMenuCategory, lbGroup, cbMenuGroup });
    
    cbMenuCategory.setMinimumSize(PosUIManager.getSize(150, 30));
    cbMenuGroup.setMinimumSize(PosUIManager.getSize(150, 30));
    
    panel.add(lbCategory);
    panel.add(cbMenuCategory);
    panel.add(lbGroup);
    panel.add(cbMenuGroup);
    return panel;
  }
  
  private void disableFocus(Component... components) {
    for (Component component : components) {
      component.setFocusable(false);
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(250));
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(50));
    
    return columnWidth;
  }
  
  public void doOk()
  {
    int index = table.getSelectedRow();
    if (index < 0)
      return;
    menuItem = ((MenuItem)tableModel.getRow(index));
    tableModel.removeAll();
    tfName.setText("");
    setCanceled(false);
    dispose();
  }
  
  public MenuItem getMenuItem() {
    return menuItem;
  }
  
  public void setMenuItem(MenuItem menuItem) {
    this.menuItem = menuItem;
  }
  
  private void doSetMenuItem() {
    String itemName = tfName.getText();
    if (StringUtils.isEmpty(itemName)) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("ItemSearchDialog.0"));
      return;
    }
    MenuItemDAO dao = new MenuItemDAO();
    
    List<MenuItem> menuItems = dao.getMenuItemByName((MenuGroup)cbMenuGroup.getSelectedItem(), itemName);
    tableModel.removeAll();
    if ((menuItems != null) && (menuItems.size() > 0)) {
      tableModel.addRows(menuItems);
    }
    table.repaint();
  }
}
