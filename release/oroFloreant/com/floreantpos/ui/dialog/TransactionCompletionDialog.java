package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.actions.EmailSendAction;
import com.floreantpos.customer.CustomerSelectorDialog;
import com.floreantpos.customer.DefaultCustomerListView;
import com.floreantpos.mailservices.MailService;
import com.floreantpos.main.Application;
import com.floreantpos.model.Customer;
import com.floreantpos.model.PosPrinters;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosOptionPane;
import com.floreantpos.util.AESencrp;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.util.List;
import javax.mail.util.ByteArrayDataSource;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import org.apache.commons.lang.StringUtils;






















public class TransactionCompletionDialog
  extends POSDialog
{
  private double tenderedAmount;
  private double totalAmount;
  private double paidAmount;
  private double dueAmount;
  private double gratuityAmount;
  private double changeAmount;
  private JLabel lblTenderedAmount;
  private JLabel lblTotalAmount;
  private JLabel lblPaidAmount;
  private JLabel lblDueAmount;
  private JLabel lblChangeDue;
  private JLabel lblGratuityAmount;
  private PosTransaction completedTransaction;
  private List<PosTransaction> completedTransactions;
  private boolean isCard;
  private List<Ticket> tickets;
  private Customer customer;
  private PosButton btnEmail;
  private PosButton btnClose;
  private PosButton btnPrintStoreCopy;
  
  public TransactionCompletionDialog(PosTransaction transaction)
  {
    completedTransaction = transaction;
    isCard = completedTransaction.isCard();
    initializeComponents();
  }
  
  public TransactionCompletionDialog(List<PosTransaction> transaction) {
    completedTransactions = transaction;
    initializeComponents();
  }
  
  private void initializeComponents() {
    setTitle(POSConstants.TRANSACTION_COMPLETED);
    
    setLayout(new MigLayout("align 50% 0%, ins 20", "[]20[]", ""));
    
    add(createLabel(Messages.getString("TransactionCompletionDialog.3") + ":", 2), "grow");
    lblTotalAmount = createLabel("0.0", 4);
    add(lblTotalAmount, "span, grow");
    
    add(createLabel(Messages.getString("TransactionCompletionDialog.8") + ":", 2), "newline,grow");
    lblTenderedAmount = createLabel("0.0", 4);
    add(lblTenderedAmount, "span, grow");
    
    add(new JSeparator(), "newline,span, grow");
    
    add(createLabel(Messages.getString("TransactionCompletionDialog.14") + ":", 2), "newline,grow");
    lblPaidAmount = createLabel("0.0", 4);
    add(lblPaidAmount, "span, grow");
    
    add(createLabel(Messages.getString("TransactionCompletionDialog.19") + ":", 2), "newline,grow");
    lblDueAmount = createLabel("0.0", 4);
    add(lblDueAmount, "span, grow");
    
    add(new JSeparator(), "newline,span, grow");
    
    add(createLabel(Messages.getString("TransactionCompletionDialog.25") + ":", 2), "newline,grow");
    lblGratuityAmount = createLabel("0.0", 4);
    add(lblGratuityAmount, "span, grow");
    
    add(new JSeparator(), "newline,span, grow");
    
    add(createLabel(Messages.getString("TransactionCompletionDialog.31") + ":", 2), "grow");
    lblChangeDue = createLabel("0.0", 4);
    add(lblChangeDue, "span, grow");
    
    add(new JSeparator(), "sg mygroup,newline,span,grow");
    
    btnEmail = new PosButton("EMAIL");
    btnEmail.setAction(new EmailSendAction()
    {
      public Ticket getTicket()
      {
        return completedTransaction.getTicket();
      }
    });
    btnClose = new PosButton(Messages.getString("TransactionCompletionDialog.37"));
    btnClose.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        dispose();
      }
      
    });
    btnPrintStoreCopy = new PosButton(Messages.getString("TransactionCompletionDialog.38"));
    btnPrintStoreCopy.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          if (tickets != null) {
            for (Ticket ticket : tickets) {
              ReceiptPrintService.printTicket(ticket);
            }
            
          } else
            ReceiptPrintService.printTransaction(completedTransaction, false);
        } catch (Exception ee) {
          POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("TransactionCompletionDialog.39"), ee);
        }
        
      }
    });
    PosButton btnPrintAllCopy = new PosButton(Messages.getString("TransactionCompletionDialog.40"));
    btnPrintAllCopy.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          if (tickets != null) {
            for (Ticket ticket : tickets) {
              ReceiptPrintService.printTicket(ticket);
            }
            
          } else
            ReceiptPrintService.printTransaction(completedTransaction, true);
        } catch (Exception ee) {
          POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("TransactionCompletionDialog.41"), ee);
        }
        
      }
      
    });
    JPanel p = new JPanel();
    p.add(btnEmail);
    if (isCard) {
      p.add(btnPrintAllCopy, "newline,skip, h 50");
      p.add(btnPrintStoreCopy, "skip, h 50");
      p.add(btnClose, "skip, h 50");
    }
    else {
      btnPrintStoreCopy.setText(Messages.getString("TransactionCompletionDialog.0"));
      p.add(btnPrintStoreCopy, "skip, h 50");
      p.add(btnClose, "skip, h 50");
    }
    
    add(p, "newline, span 2, grow, gaptop 15px");
  }
  
  private void doSendEmail() {
    try {
      Ticket ticket = completedTransaction.getTicket();
      if (ticket == null) {
        return;
      }
      String customerId = ticket.getCustomerId();
      String email = null;
      if (customerId == null) {
        int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "No customer is set for the order. Do you want to set customer?", "Customer selection");
        

        if (option != 0) {
          return;
        }
        
        CustomerSelectorDialog dialog = new CustomerSelectorDialog(new DefaultCustomerListView());
        dialog.setCreateNewTicket(false);
        dialog.updateView(true);
        dialog.openUndecoratedFullScreen();
        
        if (dialog.isCanceled()) {
          return;
        }
        customer = dialog.getSelectedCustomer();
        email = customer.getEmail().trim();
      }
      else {
        customer = CustomerDAO.getInstance().get(customerId);
        email = customer.getEmail();
      }
      
      if (StringUtils.isEmpty(email)) {
        email = PosOptionPane.showInputDialog("Enter Email Address", "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", "Invalid email! Please enter valid email.", true);
        if (StringUtils.isEmpty(email)) {
          POSMessageDialog.showError("Cannot send email without customer email address");
          return;
        }
        Customer existedCustomer = CustomerDAO.getInstance().findByPhoneOrEmail(null, email);
        if (existedCustomer != null) {
          POSMessageDialog.showError("Email already exists with different customer, please choose a different one.");
          return;
        }
        customer.setEmail(email.trim());
        CustomerDAO.getInstance().saveOrUpdate(customer);
      }
      

      ticket.setCustomer(customer);
      TicketDAO.getInstance().saveOrUpdate(ticket);
      completedTransaction.setTicket(ticket);
      
      Terminal terminal = Application.getInstance().refreshAndGetTerminal();
      String smtpHost = terminal.getSmtpHost();
      String senderEmail = terminal.getSmtpSender();
      String encryptedPassword = terminal.getSmtpPassword();
      String password = null;
      if (!StringUtils.isEmpty(encryptedPassword)) {
        password = AESencrp.decrypt(encryptedPassword);
      }
      
      if (StringUtils.isEmpty(smtpHost)) {
        POSMessageDialog.showError("SMTP Host not found! Please set configuration!");
        return;
      }
      if (StringUtils.isEmpty(senderEmail)) {
        POSMessageDialog.showError("Sender email not found! Please set configuration!");
        return;
      }
      if (StringUtils.isEmpty(password)) {
        POSMessageDialog.showError("Password not found! Please set configuration!");
        return;
      }
      
      JasperPrint jasperPrint = ReceiptPrintService.createJasperFileForTicketReceipt(ticket, Boolean.valueOf(true));
      jasperPrint.setName("TICKET_RECEIPT" + ticket.getId());
      jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
      
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      
      JRPdfExporter exporter = new JRPdfExporter();
      exporter.setParameter(JRPdfExporterParameter.JASPER_PRINT, jasperPrint);
      exporter.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
      try {
        exporter.exportReport();
      } catch (JRException e) {
        PosLog.error(getClass(), e);
      }
      byte[] bytes = byteArrayOutputStream.toByteArray();
      ByteArrayDataSource bads = new ByteArrayDataSource(bytes, "application/pdf");
      MailService.sendMail(email, "Ticket Receipt", "Ticket Receipt", "TICKET_RECEIPT_" + ticket.getId() + ".pdf", bads);
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Order payment receipt successfully sent to \"" + email + "\"");
    } catch (Exception e) {
      PosLog.error(TransactionCompletionDialog.class, e);
    }
  }
  
  protected JLabel createLabel(String text, int alignment)
  {
    JLabel label = new JLabel(text);
    label.setFont(new Font("Tahoma", 1, 24));
    
    label.setHorizontalAlignment(alignment);
    label.setText(text);
    return label;
  }
  
  public double getTenderedAmount() {
    return tenderedAmount;
  }
  
  public void setTenderedAmount(double amountTendered) {
    tenderedAmount = amountTendered;
  }
  
  public void updateView() {
    lblTotalAmount.setText(NumberUtil.formatNumber(Double.valueOf(totalAmount)));
    lblTenderedAmount.setText(NumberUtil.formatNumber(Double.valueOf(tenderedAmount)));
    lblPaidAmount.setText(NumberUtil.formatNumber(Double.valueOf(paidAmount)));
    lblDueAmount.setText(NumberUtil.formatNumber(Double.valueOf(dueAmount)));
    lblGratuityAmount.setText(NumberUtil.formatNumber(Double.valueOf(gratuityAmount)));
    lblChangeDue.setText(NumberUtil.formatNumber(Double.valueOf(changeAmount)));
  }
  
  public double getDueAmount() {
    return dueAmount;
  }
  
  public void setDueAmount(double dueAmount) {
    this.dueAmount = dueAmount;
  }
  
  public double getPaidAmount() {
    return paidAmount;
  }
  
  public void setPaidAmount(double paidAmount) {
    this.paidAmount = paidAmount;
  }
  
  public double getTotalAmount() {
    return totalAmount;
  }
  
  public void setTotalAmount(double totalAmount) {
    this.totalAmount = totalAmount;
  }
  
  public double getGratuityAmount() {
    return gratuityAmount;
  }
  
  public void setGratuityAmount(double gratuityAmount) {
    this.gratuityAmount = gratuityAmount;
  }
  
  public double getChangeAmount() {
    return changeAmount;
  }
  
  public void setChangeAmount(double changeAmount) {
    this.changeAmount = changeAmount;
  }
  
  public void setCompletedTransaction(PosTransaction completedTransaction) {
    this.completedTransaction = completedTransaction;
  }
  
  public boolean isCard() {
    return isCard;
  }
  
  public void setCard(boolean isCard) {
    this.isCard = isCard;
  }
  
  public void setTickets(List<Ticket> tickets) {
    this.tickets = tickets;
  }
  
  public List<PosTransaction> getCompletedTransactions() {
    return completedTransactions;
  }
}
