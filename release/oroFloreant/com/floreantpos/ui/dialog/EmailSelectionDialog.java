package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.QwertyKeyPad;
import com.floreantpos.util.POSUtil;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;




public abstract class EmailSelectionDialog
  extends OkCancelOptionDialog
{
  private JRadioButton rbPrimaryEmail = new JRadioButton(Messages.getString("EmailSelectionDialog.0"));
  private JRadioButton rbSecondaryEmail = new JRadioButton(Messages.getString("EmailSelectionDialog.1"));
  private JRadioButton rbNewEmail = new JRadioButton(Messages.getString("EmailSelectionDialog.2"));
  
  private JLabel lblPrimaryEmail = new JLabel();
  private JLabel lblSecondaryEmail = new JLabel();
  private FixedLengthTextField tfNewEmail = new FixedLengthTextField();
  private String email1;
  private String email2;
  
  public EmailSelectionDialog(String title, String email1, String email2)
  {
    super(POSUtil.getFocusedWindow());
    setCaption(title);
    this.email1 = email1;
    this.email2 = email2;
    initComponents();
  }
  
  private void initComponents() {
    setOkButtonText(Messages.getString("EmailSelectionDialog.3"));
    Font f = new Font(lblPrimaryEmail.getFont().getName(), 0, 17);
    Color textColor = Color.gray;
    
    JPanel contentPanel = getContentPanel();
    contentPanel.setLayout(new MigLayout("center,fillx,wrap 1", "[grow]"));
    
    JSeparator sep = new JSeparator();
    JSeparator sep2 = new JSeparator();
    
    String gapLeft = ",gapleft 20";
    String grow = "growx,span";
    
    rbPrimaryEmail.setFocusable(false);
    rbSecondaryEmail.setFocusable(false);
    rbNewEmail.setFocusable(false);
    tfNewEmail.setFocusable(false);
    tfNewEmail.setEditable(false);
    
    if (StringUtils.isNotEmpty(email1)) {
      rbPrimaryEmail.setSelected(true);
    }
    else if (StringUtils.isNotEmpty(email2)) {
      rbSecondaryEmail.setSelected(true);
    }
    else {
      tfNewEmail.setFocusable(true);
      rbNewEmail.setSelected(true);
      tfNewEmail.setEditable(true);
      tfNewEmail.requestFocus();
    }
    ButtonGroup buttonGroup = new ButtonGroup();
    buttonGroup.add(rbPrimaryEmail);
    buttonGroup.add(rbSecondaryEmail);
    buttonGroup.add(rbNewEmail);
    
    ActionListener l = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tfNewEmail.setEditable(rbNewEmail.isSelected());
        tfNewEmail.setFocusable(rbNewEmail.isSelected());
        if (tfNewEmail.isFocusable())
          tfNewEmail.requestFocus();
      }
    };
    rbNewEmail.addActionListener(l);
    rbPrimaryEmail.addActionListener(l);
    rbSecondaryEmail.addActionListener(l);
    
    lblPrimaryEmail.setForeground(textColor);
    lblSecondaryEmail.setForeground(textColor);
    
    rbPrimaryEmail.setFont(f);
    rbSecondaryEmail.setFont(f);
    rbNewEmail.setFont(f);
    
    String noEmailString = Messages.getString("EmailSelectionDialog.8");
    lblPrimaryEmail.setText(StringUtils.isNotEmpty(email1) ? email1 : noEmailString);
    lblSecondaryEmail.setText(StringUtils.isNotEmpty(email2) ? email2 : noEmailString);
    
    QwertyKeyPad keypad = new QwertyKeyPad(true);
    
    contentPanel.add(rbPrimaryEmail, grow);
    contentPanel.add(lblPrimaryEmail, grow + gapLeft);
    contentPanel.add(sep, "growx,span,h 1!");
    contentPanel.add(rbSecondaryEmail, grow);
    contentPanel.add(lblSecondaryEmail, grow + gapLeft);
    contentPanel.add(sep2, "growx,span,h 1!");
    contentPanel.add(rbNewEmail, grow);
    contentPanel.add(tfNewEmail, "h 40!,span,growx,");
    contentPanel.add(keypad, "grow,span");
  }
  
  public abstract boolean doSendEmail(String paramString);
  
  public void doOk()
  {
    if ((rbNewEmail.isSelected()) && (StringUtils.isEmpty(tfNewEmail.getText()))) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("EmailSelectionDialog.13"));
      return;
    }
    if ((rbPrimaryEmail.isSelected()) && (StringUtils.isEmpty(email1))) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("EmailSelectionDialog.14"));
      return;
    }
    if ((rbSecondaryEmail.isSelected()) && (StringUtils.isEmpty(email2))) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("EmailSelectionDialog.14"));
      return;
    }
    if (!doSendEmail(getEmailAddress())) {
      return;
    }
    setCanceled(false);
    dispose();
  }
  
  private String getEmailAddress() {
    if (rbPrimaryEmail.isSelected())
      return email1;
    if (rbSecondaryEmail.isSelected()) {
      return email2;
    }
    return tfNewEmail.getText();
  }
}
