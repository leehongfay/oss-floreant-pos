package com.floreantpos.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.VoidReason;
import com.floreantpos.model.dao.VoidReasonDAO;
import com.floreantpos.services.PosTransactionService;
import com.floreantpos.swing.ListComboBoxModel;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.util.POSUtil;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;




























public class VoidTicketDialog
  extends OkCancelOptionDialog
{
  private JComboBox cbVoidReasons;
  private POSToggleButton btnItemWasted;
  private Ticket ticket;
  
  public VoidTicketDialog()
  {
    this(POSUtil.getFocusedWindow(), null);
  }
  
  public VoidTicketDialog(Window window, Ticket ticket) {
    super(window, "");
    this.ticket = ticket;
    initComponents();
    initialize();
    setDefaultCloseOperation(0);
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }
  
  private void initComponents() {
    setCaption(POSConstants.VOID);
    setOkButtonText(POSConstants.SAVE_BUTTON_TEXT);
    setDefaultCloseOperation(2);
    
    Font font2 = new Font("Tahoma", 1, PosUIManager.getFontSize(16));
    JLabel lblReason = new JLabel();
    btnItemWasted = new POSToggleButton(POSConstants.ITEMS_WASTED);
    lblReason.setText("REASON:");
    
    Dimension size = PosUIManager.getSize(300, 35);
    cbVoidReasons = new JComboBox();
    cbVoidReasons.setPreferredSize(size);
    cbVoidReasons.setFont(font2);
    
    PosButton btnNewVoidReason = new PosButton();
    btnNewVoidReason.setText("...");
    btnNewVoidReason.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        VoidTicketDialog.this.doAddNewVoidReason();
      }
    });
    JPanel inputPanel = new JPanel(new MigLayout("fill,ins 0 5 2 5,hidemode 3", "[grow]", "[][][grow]"));
    inputPanel.add(lblReason, "grow,wrap");
    inputPanel.add(cbVoidReasons, "grow,split 3");
    inputPanel.add(btnNewVoidReason, "growy");
    inputPanel.add(btnItemWasted, "h 50!,growy,wrap");
    getContentPanel().add(inputPanel);
  }
  
  public void initialize() {
    try {
      VoidReasonDAO dao = new VoidReasonDAO();
      List<VoidReason> voidReasons = dao.findAll();
      cbVoidReasons.setModel(new ListComboBoxModel(voidReasons));
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.CANNOT_LOAD_VOID_REASONS, e);
    }
  }
  
  private void doAddNewVoidReason() {
    try {
      NotesDialog dialog = new NotesDialog();
      dialog.setTitle(POSConstants.ENTER_VOID_REASON);
      dialog.pack();
      dialog.open();
      
      if (!dialog.isCanceled()) {
        String newVoidReason = dialog.getNote();
        VoidReason voidReason = new VoidReason();
        voidReason.setReasonText(newVoidReason);
        
        VoidReasonDAO dao = new VoidReasonDAO();
        dao.save(voidReason);
        
        if ((cbVoidReasons.getModel() instanceof ListComboBoxModel)) {
          ListComboBoxModel model = (ListComboBoxModel)cbVoidReasons.getModel();
          model.addElement(voidReason);
        }
      }
    } catch (Throwable e) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  public void doOk()
  {
    try {
      VoidReason voidReason = (VoidReason)cbVoidReasons.getSelectedItem();
      if (voidReason != null) {
        ticket.setVoidReason(voidReason.getReasonText());
      }
      ticket.setWasted(Boolean.valueOf(btnItemWasted.isSelected()));
      PosTransactionService.getInstance().voidTicket(ticket, Application.getCurrentUser());
      canceled = false;
      dispose();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), POSConstants.ERROR_MESSAGE, e);
    }
  }
}
