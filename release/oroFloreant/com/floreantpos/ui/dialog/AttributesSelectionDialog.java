package com.floreantpos.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.bo.ui.explorer.ExplorerButtonPanel;
import com.floreantpos.model.Attribute;
import com.floreantpos.model.dao.AttributeDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.model.AttributeEntryForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;





















public class AttributesSelectionDialog
  extends POSDialog
{
  private JXTable table;
  private BeanTableModel<Attribute> tableModel;
  private Attribute selectedAttribute;
  private List<Attribute> attributes;
  private boolean singleGroupSelection;
  
  public AttributesSelectionDialog(List<Attribute> existingAttributes)
  {
    super(POSUtil.getFocusedWindow(), "");
    init();
    List<Attribute> attributeList = AttributeDAO.getInstance().findAll();
    if (existingAttributes != null) {
      for (Attribute attribute : attributeList) {
        if (existingAttributes.contains(attribute)) {
          attribute.setEnable(Boolean.valueOf(true));
        }
      }
    }
    tableModel.setRows(attributeList);
  }
  
  private void init() {
    setLayout(new BorderLayout(5, 5));
    setTitle("Select Attribute");
    TitlePanel titelpanel = new TitlePanel();
    titelpanel.setTitle("Select one or more attribute");
    
    add(titelpanel, "North");
    tableModel = new BeanTableModel(Attribute.class)
    {
      public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 1) {
          Attribute attribute = (Attribute)tableModel.getRow(rowIndex);
          if (attribute != null) {
            return attribute.getGroup() + ": " + attribute.getName();
          }
        }
        return super.getValueAt(rowIndex, columnIndex);
      }
    };
    tableModel.addColumn("", "enable");
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "name");
    
    table = new JXTable(tableModel);
    table.setSelectionMode(2);
    table.setTableHeader(null);
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.setRowHeight(PosUIManager.getSize(30));
    table.setShowGrid(true, false);
    
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          AttributesSelectionDialog.this.editSelectedRow();
        }
        else {
          AttributesSelectionDialog.this.selectItem();
        }
        
      }
    });
    JPanel contentPanel = new JPanel(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(5, 10, 5, 10));
    contentPanel.add(new JScrollPane(table));
    
    add(contentPanel);
    resizeColumnWidth(table);
    add(createButtonPanel(), "South");
  }
  
  private TransparentPanel createButtonPanel() {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton btnEdit = explorerButton.getEditButton();
    JButton btnAdd = explorerButton.getAddButton();
    
    JButton btnOk = new JButton("SELECT");
    btnOk.setHorizontalTextPosition(4);
    btnOk.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        AttributesSelectionDialog.this.doOk();
      }
      
    });
    JButton btnCancel = new JButton(POSConstants.CANCEL);
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setCanceled(true);
        dispose();
      }
      
    });
    btnAdd.setText(POSConstants.ADD);
    btnEdit.setText(POSConstants.EDIT);
    
    btnEdit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        AttributesSelectionDialog.this.editSelectedRow();
      }
      

    });
    btnAdd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          Attribute attribute = new Attribute();
          AttributeEntryForm editor = new AttributeEntryForm(attribute);
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          Attribute foodItem = (Attribute)editor.getBean();
          tableModel.addRow(foodItem);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    btnOk.setBackground(Color.GREEN);
    
    TransparentPanel panel = new TransparentPanel(new MigLayout("center,ins 0 0 5 0", "sg,fill", ""));
    int h = PosUIManager.getSize(40);
    panel.add(btnAdd, "h " + h);
    panel.add(btnEdit, "h " + h);
    panel.add(btnOk, "h " + h);
    panel.add(btnCancel, "h " + h);
    return panel;
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(250));
    
    return columnWidth;
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      Attribute attribute = (Attribute)tableModel.getRow(index);
      tableModel.setRow(index, attribute);
      
      AttributeEntryForm editor = new AttributeEntryForm(attribute);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doOk() {
    try {
      attributes = new ArrayList();
      List<Attribute> modifierList = tableModel.getRows();
      for (Iterator iterator = modifierList.iterator(); iterator.hasNext();) {
        Attribute attribute = (Attribute)iterator.next();
        if (attribute.isEnable().booleanValue()) {
          attributes.add(attribute);
        }
      }
      
      setCanceled(false);
      dispose();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public Attribute getSelectedAttribute() {
    return selectedAttribute;
  }
  
  public List<Attribute> getSelectedAttributeList() {
    return attributes;
  }
  
  public void setSelectedAttribute(Attribute selectedAttribute) {
    this.selectedAttribute = selectedAttribute;
  }
  
  private void selectItem() {
    if (table.getSelectedRow() < 0) {
      return;
    }
    int selectedRow = table.getSelectedRow();
    selectedRow = table.convertRowIndexToModel(selectedRow);
    Attribute attribute = (Attribute)tableModel.getRow(selectedRow);
    if ((singleGroupSelection) && (!attribute.isEnable().booleanValue())) {
      for (Attribute att : tableModel.getRows()) {
        if (att.getGroup() == attribute.getGroup()) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select one attribute from one group.\n " + att.getName() + " already added from " + att
            .getGroup());
          return;
        }
      }
    }
    attribute.setEnable(Boolean.valueOf(!attribute.isEnable().booleanValue()));
    table.repaint();
  }
  
  public void setSingleGroupSelection(boolean b) {
    singleGroupSelection = b;
  }
}
