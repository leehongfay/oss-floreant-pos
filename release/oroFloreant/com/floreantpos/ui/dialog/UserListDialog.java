package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import java.awt.BorderLayout;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.JTableHeader;
















public class UserListDialog
  extends OkCancelOptionDialog
{
  BeanTableModel<User> tableModel;
  JTable userListTable;
  
  public UserListDialog()
  {
    super(Application.getPosWindow(), true);
    setTitle(Messages.getString("UserListDialog.0"));
    setCaption(Messages.getString("UserListDialog.0"));
    
    JPanel contentPane = getContentPanel();
    contentPane.setLayout(new BorderLayout(5, 5));
    contentPane.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
    
    tableModel = new BeanTableModel(User.class);
    tableModel.addColumn("Name", "fullName");
    
    userListTable = new JTable(tableModel);
    userListTable.setRowHeight(PosUIManager.getSize(60));
    userListTable.getSelectionModel().setSelectionMode(0);
    contentPane.add(new JScrollPane(userListTable));
    userListTable.getTableHeader().setPreferredSize(PosUIManager.getSize(0, 40));
    
    List<User> users = UserDAO.getInstance().findClockedInUsers();
    tableModel.addRows(users);
    if ((users != null) && (!users.isEmpty())) {
      userListTable.getSelectionModel().setSelectionInterval(0, 0);
    }
  }
  
  public void setUsers(List<User> users) {
    tableModel.removeAll();
    tableModel.addRows(users);
  }
  
  public void hideUser(User user) {
    tableModel.removeRow(user);
  }
  
  public User getSelectedUser() {
    return (User)tableModel.getRows().get(userListTable.getSelectedRow());
  }
  
  public void doOk()
  {
    int selectedRow = userListTable.getSelectedRow();
    if (selectedRow == -1) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("UserListDialog.4"));
      return;
    }
    
    setCanceled(false);
    dispose();
  }
}
