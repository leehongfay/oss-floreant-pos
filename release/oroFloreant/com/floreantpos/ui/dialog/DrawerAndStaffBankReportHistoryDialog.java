package com.floreantpos.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.DrawerType;
import com.floreantpos.model.StoreSession;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.CashDrawerDAO;
import com.floreantpos.swing.ButtonColumn;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.util.NumberUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;



























public class DrawerAndStaffBankReportHistoryDialog
  extends POSDialog
{
  private TitlePanel titlePanel;
  private JTable tableDrawerReport;
  private DrawerReportTableModel tableModel;
  private List<CashDrawer> drawerPullReportList;
  private DefaultListSelectionModel selectionModel;
  private User user;
  private PosButton btnCancel;
  private Terminal currentTerminal;
  private JLabel lblTotalCash;
  private StoreSession currentStoreOperationData;
  
  public DrawerAndStaffBankReportHistoryDialog(JFrame parent, User user, StoreSession currentStoreOperationData)
  {
    super(parent, true);
    setTitle("Store history");
    this.user = user;
    this.currentStoreOperationData = currentStoreOperationData;
    initComponents();
    initData();
    updateView();
  }
  
  public void initData() {
    currentTerminal = Application.getInstance().getTerminal();
    
    selectionModel = new DefaultListSelectionModel();
    selectionModel.setSelectionMode(0);
    tableDrawerReport.getSelectionModel().setSelectionMode(0);
    
    tableDrawerReport.setDefaultRenderer(Object.class, new TableRenderer());
    tableModel = new DrawerReportTableModel();
    tableDrawerReport.setModel(tableModel);
    
    AbstractAction action = new AbstractAction()
    {
      public void actionPerformed(ActionEvent e)
      {
        int row = Integer.parseInt(e.getActionCommand());
        CashDrawer pullReport = (CashDrawer)tableModel.getRowData(row);
        DrawerAndStaffBankReportHistoryDialog.this.showReport(pullReport);
        DrawerAndStaffBankReportHistoryDialog.this.updateView();
      }
    };
    ButtonColumn buttonColumn = new ButtonColumn(tableDrawerReport, action, 5)
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        return super.getTableCellRendererComponent(table, value, false, hasFocus, row, column);
      }
      
      public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
      {
        return super.getTableCellEditorComponent(table, value, false, row, column);
      }
    };
    MatteBorder selectedBorder = BorderFactory.createMatteBorder(2, 10, 2, 10, tableDrawerReport.getBackground());
    MatteBorder unselectedBorder = BorderFactory.createMatteBorder(2, 10, 2, 10, tableDrawerReport.getBackground());
    
    Border border1 = new CompoundBorder(selectedBorder, btnCancel.getBorder());
    Border border2 = new CompoundBorder(unselectedBorder, btnCancel.getBorder());
    
    buttonColumn.setUnselectedBorder(border1);
    buttonColumn.setFocusBorder(border2);
    
    resizeTableColumns();
  }
  
  private void updateView() {
    if (currentStoreOperationData == null)
      return;
    drawerPullReportList = new ArrayList();
    List<CashDrawer> drawerStatusList = CashDrawerDAO.getInstance().findByStoreOperationData(currentStoreOperationData);
    for (CashDrawer cashDrawer : drawerStatusList)
    {

      drawerPullReportList.add(cashDrawer);
    }
    tableModel.setItems(drawerPullReportList);
    tableModel.fireTableDataChanged();
    lblTotalCash.setText(NumberUtil.formatNumber(Double.valueOf(tableModel.getTotalCashDeposit())));
  }
  
  private void resizeTableColumns() {
    tableDrawerReport.setAutoResizeMode(4);
    setColumnWidth(0, PosUIManager.getSize(200));
    setColumnWidth(1, PosUIManager.getSize(100));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = tableDrawerReport.getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  private void initComponents() {
    TransparentPanel container = new TransparentPanel();
    titlePanel = new TitlePanel();
    TransparentPanel contentPanel = new TransparentPanel();
    TransparentPanel bottomActionPanel = new TransparentPanel(new MigLayout("al center", "sg, fill", ""));
    
    JSeparator jSeparator1 = new JSeparator();
    
    btnCancel = new PosButton();
    
    JScrollPane jScrollPane1 = new JScrollPane();
    tableDrawerReport = new JTable();
    tableDrawerReport.setRowHeight(PosUIManager.getSize(30));
    tableDrawerReport.getTableHeader().setPreferredSize(PosUIManager.getSize(0, 30));
    
    getContentPane().setLayout(new BorderLayout(5, 5));
    
    container.setLayout(new BorderLayout(5, 5));
    container.add(jSeparator1, "North");
    
    btnCancel.setText(POSConstants.SAVE_BUTTON_TEXT);
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        DrawerAndStaffBankReportHistoryDialog.this.btnCloseActionPerformed(evt);
      }
      
    });
    bottomActionPanel.add(btnCancel);
    
    container.add(bottomActionPanel, "Center");
    getContentPane().add(container, "South");
    
    contentPanel.setLayout(new BorderLayout());
    contentPanel.add(titlePanel, "North");
    
    jScrollPane1.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(10, 15, 10, 15), jScrollPane1.getBorder()));
    jScrollPane1.setViewportView(tableDrawerReport);
    
    contentPanel.add(jScrollPane1, "Center");
    
    JPanel cashSummaryPanel = new JPanel(new MigLayout("center,ins 0 20 0 20"));
    lblTotalCash = new JLabel();
    Font font = new Font(null, 1, 14);
    lblTotalCash.setFont(font);
    JLabel lblCash = new JLabel("TOTAL CASH DEPOSIT: ");
    lblCash.setFont(font);
    cashSummaryPanel.add(lblCash);
    cashSummaryPanel.add(lblTotalCash, "gapright 20");
    
    contentPanel.add(cashSummaryPanel, "South");
    getContentPane().add(contentPanel, "Center");
    setSize(PosUIManager.getSize(830, 550));
  }
  
  private void showReport(CashDrawer report) {
    if (report == null) {
      return;
    }
    if (report.getDrawerType() == DrawerType.STAFF_BANK) {
      showBankStatus(report);
    }
    else {
      showDrawerStatus(report);
    }
  }
  
  private void showBankStatus(CashDrawer report) {
    try {
      CashDrawerInfoDialog dialog = new CashDrawerInfoDialog(report);
      dialog.setTitle("BANK STATUS");
      dialog.showInfoOnly(true);
      dialog.refreshReport();
      dialog.setDefaultCloseOperation(2);
      dialog.open();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private void showDrawerStatus(CashDrawer report) {
    try {
      if (report.getTerminal() == null) {
        return;
      }
      CashDrawerInfoDialog dialog = new CashDrawerInfoDialog(report);
      dialog.setTitle(POSConstants.DRAWER_PULL_BUTTON_TEXT);
      dialog.showInfoOnly(true);
      dialog.refreshReport();
      dialog.setDefaultCloseOperation(2);
      dialog.open();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public void setInfo(String info) {
    titlePanel.setTitle(info);
  }
  
  private void btnCloseActionPerformed(ActionEvent evt) {
    dispose();
  }
  
  class DrawerReportTableModel extends ListTableModel<CashDrawer> {
    private double totalCashDeposit;
    
    public DrawerReportTableModel() {
      super();
    }
    
    public void setItems(List<CashDrawer> drawerPullReportList) {
      double totalCashDeposit = 0.0D;
      if (drawerPullReportList != null) {
        for (CashDrawer report : drawerPullReportList) {
          totalCashDeposit += report.getCashToDeposit().doubleValue();
        }
      }
      setTotalCashDeposit(totalCashDeposit);
      setRows(drawerPullReportList);
    }
    
    public double getTotalCashDeposit() {
      return totalCashDeposit;
    }
    
    public void setTotalCashDeposit(double totalCashDeposit) {
      this.totalCashDeposit = totalCashDeposit;
    }
    
    public int getRowCount() {
      if (drawerPullReportList == null) {
        return 0;
      }
      int size = drawerPullReportList.size();
      return size;
    }
    
    public int getColumnCount() {
      return 6;
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      if (columnIndex == 5) {
        return true;
      }
      
      return false;
    }
    
    public CashDrawer getSelectedRow() {
      int index = tableDrawerReport.getSelectedRow();
      if (index < 0) {
        return null;
      }
      index = tableDrawerReport.convertRowIndexToModel(index);
      return (CashDrawer)tableModel.getRowData(index);
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      if (drawerPullReportList == null)
        return "";
      CashDrawer t = (CashDrawer)drawerPullReportList.get(rowIndex);
      User assignedUser = t.getAssignedUser();
      
      switch (columnIndex) {
      case 0: 
        if (t.getDrawerType() == DrawerType.DRAWER)
          return "Terminal - " + t.getTerminal();
        return t.getAssignedUser();
      case 1: 
        return t.getDrawerType().toString();
      case 2: 
        if (assignedUser != null)
          return t.getReportTime() == null ? "ASSIGNED" : "CLOSED";
      case 3: 
        if (assignedUser != null)
          return NumberUtil.formatNumber(t.getCashToDeposit());
      case 4: 
        if (assignedUser != null)
          return NumberUtil.formatNumber(t.getDrawerAccountable());
      case 5: 
        return "STATUS";
      }
      return "";
    }
  }
  
  class TableRenderer extends DefaultTableCellRenderer
  {
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
    

    public TableRenderer() {}
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
      JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      label.setIcon(null);
      if (column == 0) {
        CashDrawer t = (CashDrawer)drawerPullReportList.get(row);
        if ((t.getDrawerType() != DrawerType.STAFF_BANK) && (t.getTerminal().getId().intValue() == currentTerminal.getId().intValue()))
          label.setIcon(IconFactory.getIcon("/ui_icons/", "check_mark.png"));
      }
      if ((value instanceof Date)) {
        String string = dateFormat.format(value);
        label.setText(string);
        label.setHorizontalAlignment(4);
      }
      if ((column == 3) || (column == 4) || ((value instanceof Double))) {
        label.setHorizontalAlignment(4);
      }
      else {
        label.setHorizontalAlignment(2);
      }
      if ((column == 1) || (column == 2))
        label.setHorizontalAlignment(0);
      return label;
    }
  }
}
