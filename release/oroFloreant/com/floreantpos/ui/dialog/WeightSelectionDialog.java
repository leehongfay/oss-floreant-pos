package com.floreantpos.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.dao.InventoryUnitDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;




















public class WeightSelectionDialog
  extends POSDialog
  implements ActionListener
{
  private int defaultValue;
  private TitlePanel titlePanel;
  private JTextField tfNumber;
  private boolean floatingPoint;
  private PosButton btnCancel;
  private boolean clearPreviousNumber = true;
  private JComboBox cbUnits;
  private InventoryUnit selectedUnit;
  
  public WeightSelectionDialog()
  {
    init();
    cbUnits.setVisible(false);
  }
  
  public WeightSelectionDialog(Frame parent) {
    super(parent, true);
    init();
    cbUnits.setVisible(false);
  }
  
  public WeightSelectionDialog(MenuItem menuItem) {
    init();
    List<Object> units = new ArrayList();
    if (menuItem.getUnit() != null) {
      List<InventoryUnit> items = InventoryUnitDAO.getInstance().findByGroupId(menuItem.getUnit().getUnitGroupId());
      units.addAll(items);
    }
    cbUnits.setModel(new ComboBoxModel(units));
  }
  
  private void init() {
    setResizable(false);
    Container contentPane = getContentPane();
    
    MigLayout layout = new MigLayout("fill,hidemode 3", "", "");
    contentPane.setLayout(layout);
    
    titlePanel = new TitlePanel();
    contentPane.add(titlePanel, "newline,height 60");
    
    tfNumber = new JTextField();
    tfNumber.setText(String.valueOf(defaultValue));
    tfNumber.setFont(tfNumber.getFont().deriveFont(1, 24.0F));
    tfNumber.setFocusable(true);
    tfNumber.requestFocus();
    tfNumber.setBackground(Color.WHITE);
    
    cbUnits = new JComboBox();
    contentPane.add(tfNumber, "gapleft 5,newline,split 2,gapright 5, grow");
    contentPane.add(cbUnits, "gapright 5,grow");
    
    JPanel buttonsPanel = new JPanel(new MigLayout("fill", "sg,fill", ""));
    
    String[][] numbers = { { "7", "8", "9" }, { "4", "5", "6" }, { "1", "2", "3" }, { ".", "0", "CLEAR ALL" } };
    String[][] iconNames = { { "7.png", "8.png", "9.png" }, { "4.png", "5.png", "6.png" }, { "1.png", "2.png", "3.png" }, { "dot.png", "0.png", "" } };
    

    for (int i = 0; i < numbers.length; i++) {
      for (int j = 0; j < numbers[i].length; j++) {
        PosButton posButton = new PosButton();
        posButton.setFocusable(false);
        ImageIcon icon = IconFactory.getIcon("/ui_icons/", iconNames[i][j]);
        String buttonText = String.valueOf(numbers[i][j]);
        
        if (icon == null) {
          posButton.setText(buttonText);
        }
        else {
          posButton.setIcon(icon);
          if (POSConstants.CLEAR_ALL.equals(buttonText)) {
            posButton.setText("CLEAR ALL");
          }
        }
        
        posButton.setActionCommand(buttonText);
        posButton.addActionListener(this);
        String constraints = "grow,height 55";
        if (j == numbers[i].length - 1) {
          constraints = constraints + ",wrap";
        }
        buttonsPanel.add(posButton, constraints);
      }
    }
    contentPane.add(buttonsPanel, "newline,grow");
    contentPane.add(new JSeparator(), "newline,grow,gaptop 5");
    
    PosButton btnOk = new PosButton(POSConstants.OK);
    btnOk.setFocusable(false);
    btnOk.addActionListener(this);
    
    btnCancel = new PosButton(POSConstants.CANCEL);
    btnCancel.setFocusable(false);
    btnCancel.addActionListener(this);
    
    JPanel buttonPanel = new JPanel(new MigLayout("al center center", "fill,sg", ""));
    buttonPanel.add(btnOk, "w 100!");
    buttonPanel.add(btnCancel);
    
    contentPane.add(buttonPanel, "newline,grow");
  }
  
  private void doOk() {
    if (!validate(tfNumber.getText())) {
      POSMessageDialog.showError(this, POSConstants.INVALID_NUMBER);
      return;
    }
    selectedUnit = ((InventoryUnit)cbUnits.getSelectedItem());
    setCanceled(false);
    dispose();
  }
  
  private void doCancel() {
    setCanceled(true);
    dispose();
  }
  
  private void doClearAll() {
    tfNumber.setText(String.valueOf(defaultValue));
  }
  
  private void doInsertNumber(String number)
  {
    if (clearPreviousNumber) {
      tfNumber.setText("0");
      clearPreviousNumber = false;
    }
    
    String s = tfNumber.getText();
    double d = 0.0D;
    try
    {
      d = Double.parseDouble(s);
    }
    catch (Exception localException) {}
    
    if ((d == 0.0D) && (!s.contains("."))) {
      tfNumber.setText(number);
      return;
    }
    
    s = s + number;
    if (!validate(s)) {
      POSMessageDialog.showError(this, POSConstants.INVALID_NUMBER);
      return;
    }
    tfNumber.setText(s);
  }
  
  private void doInsertDot()
  {
    String string = tfNumber.getText() + ".";
    if (!validate(string)) {
      POSMessageDialog.showError(this, POSConstants.INVALID_NUMBER);
      return;
    }
    tfNumber.setText(string);
  }
  
  public void actionPerformed(ActionEvent e)
  {
    String actionCommand = e.getActionCommand();
    
    if (POSConstants.CANCEL.equalsIgnoreCase(actionCommand)) {
      doCancel();
    }
    else if (POSConstants.OK.equalsIgnoreCase(actionCommand)) {
      doOk();
    }
    else if (actionCommand.equals(POSConstants.CLEAR_ALL)) {
      doClearAll();
    }
    else if (actionCommand.equals(".")) {
      doInsertDot();
    }
    else {
      doInsertNumber(actionCommand);
    }
  }
  
  private boolean validate(String str) {
    if (isFloatingPoint()) {
      try {
        Double.parseDouble(str);
      } catch (Exception x) {
        return false;
      }
    } else {
      try
      {
        Integer.parseInt(str);
      } catch (Exception x) {
        return false;
      }
    }
    return true;
  }
  
  public void setTitle(String title) {
    titlePanel.setTitle(title);
    
    super.setTitle(title);
  }
  
  public void setDialogTitle(String title) {
    super.setTitle(title);
  }
  
  public double getValue() {
    return Double.parseDouble(tfNumber.getText());
  }
  
  public void setValue(double value) {
    if (value == 0.0D) {
      tfNumber.setText("0");
    }
    else if (isFloatingPoint()) {
      tfNumber.setText(String.valueOf(value));
    }
    else {
      tfNumber.setText(String.valueOf((int)value));
    }
  }
  
  public boolean isFloatingPoint() {
    return floatingPoint;
  }
  
  public void setFloatingPoint(boolean decimalAllowed) {
    floatingPoint = decimalAllowed;
  }
  
  public static void main(String[] args) {
    WeightSelectionDialog dialog2 = new WeightSelectionDialog();
    dialog2.pack();
    dialog2.setVisible(true);
  }
  
  public int getDefaultValue() {
    return defaultValue;
  }
  
  public void setDefaultValue(int defaultValue) {
    this.defaultValue = defaultValue;
    tfNumber.setText(String.valueOf(defaultValue));
  }
  
  public static int takeIntInput(String title) {
    WeightSelectionDialog dialog = new WeightSelectionDialog();
    dialog.setTitle(title);
    dialog.pack();
    dialog.open();
    
    if (dialog.isCanceled()) {
      return -1;
    }
    
    return (int)dialog.getValue();
  }
  
  public static double takeDoubleInput(String title, String dialogTitle, double initialAmount) {
    WeightSelectionDialog dialog = new WeightSelectionDialog();
    dialog.setFloatingPoint(true);
    dialog.setValue(initialAmount);
    dialog.setTitle(title);
    dialog.setDialogTitle(dialogTitle);
    dialog.pack();
    dialog.open();
    
    if (dialog.isCanceled()) {
      return NaN.0D;
    }
    
    return dialog.getValue();
  }
  
  public static double takeDoubleInput(String title, double initialAmount) {
    WeightSelectionDialog dialog = new WeightSelectionDialog();
    dialog.setFloatingPoint(true);
    dialog.setTitle(title);
    dialog.setValue(initialAmount);
    dialog.pack();
    dialog.open();
    
    if (dialog.isCanceled()) {
      return -1.0D;
    }
    
    return dialog.getValue();
  }
  
  public static double takeDoubleInput(String title, double initialAmount, MenuItem menuItem) {
    WeightSelectionDialog dialog = new WeightSelectionDialog(menuItem);
    dialog.setFloatingPoint(true);
    dialog.setTitle(title);
    dialog.setValue(initialAmount);
    dialog.pack();
    dialog.open();
    
    if (dialog.isCanceled()) {
      return -1.0D;
    }
    
    return dialog.getValue();
  }
  
  public static double show(Component parent, String title, double initialAmount) {
    WeightSelectionDialog dialog2 = new WeightSelectionDialog();
    dialog2.setFloatingPoint(true);
    dialog2.setTitle(title);
    dialog2.pack();
    dialog2.setLocationRelativeTo(parent);
    dialog2.setValue(initialAmount);
    dialog2.setVisible(true);
    
    if (dialog2.isCanceled()) {
      return NaN.0D;
    }
    
    return dialog2.getValue();
  }
  
  public InventoryUnit getSelectedUnit() {
    return selectedUnit;
  }
}
