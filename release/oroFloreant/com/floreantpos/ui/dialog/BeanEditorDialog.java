package com.floreantpos.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.config.AppProperties;
import com.floreantpos.main.Application;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dialog.ModalityType;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JSeparator;















public class BeanEditorDialog
  extends JDialog
  implements WindowListener
{
  protected BeanEditor beanEditor;
  private boolean canceled = false;
  private TransparentPanel beanEditorContainer;
  
  public BeanEditorDialog() { this((BeanEditor)null); }
  
  public BeanEditorDialog(Window parent, BeanEditor beanEditor)
  {
    super(parent, Dialog.ModalityType.APPLICATION_MODAL);
    initComponents();
    setBeanEditor(beanEditor);
    addWindowListener(this);
  }
  
  public BeanEditorDialog(BeanEditor beanEditor) {
    super(POSUtil.getFocusedWindow(), Dialog.ModalityType.APPLICATION_MODAL);
    initComponents();
    
    setBeanEditor(beanEditor);
    
    addWindowListener(this);
  }
  
  public BeanEditorDialog(Frame owner, BeanEditor beanEditor) {
    super(owner, Dialog.ModalityType.APPLICATION_MODAL);
    initComponents();
    
    setBeanEditor(beanEditor);
    
    addWindowListener(this);
  }
  

  private PosButton btnCancel;
  
  private PosButton btnOk;
  
  private void initComponents()
  {
    setTitle(AppProperties.getAppName());
    titlePanel = new TitlePanel();
    jPanel1 = new TransparentPanel();
    jSeparator1 = new JSeparator();
    buttonPanel = new TransparentPanel();
    btnOk = new PosButton();
    btnCancel = new PosButton();
    beanEditorContainer = new TransparentPanel(new BorderLayout());
    
    setIconImage(Application.getApplicationIcon().getImage());
    setDefaultCloseOperation(0);
    getContentPane().add(titlePanel, "North");
    
    jPanel1.setLayout(new BorderLayout());
    
    jPanel1.add(jSeparator1, "North");
    
    buttonPanel.setLayout(new FlowLayout());
    
    btnOk.setText(POSConstants.OK.toUpperCase());
    btnOk.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        BeanEditorDialog.this.performOk(evt);
      }
      
    });
    buttonPanel.add(btnOk);
    
    btnCancel.setText(POSConstants.CANCEL.toUpperCase());
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        BeanEditorDialog.this.performCancel(evt);
      }
      
    });
    buttonPanel.add(btnCancel);
    
    jPanel1.add(buttonPanel, "Center");
    
    getContentPane().add(jPanel1, "South");
    
    beanEditorContainer.setLayout(new BorderLayout());
    
    getContentPane().add(beanEditorContainer, "Center");
  }
  
  public void dispose()
  {
    if (beanEditor != null) {
      beanEditor = null;
    }
    
    super.dispose();
  }
  
  private void performCancel(ActionEvent evt) {
    canceled = true;
    dispose();
  }
  
  private void performOk(ActionEvent evt) {
    try {
      if (beanEditor == null) {
        return;
      }
      
      boolean saved = beanEditor.save();
      if (saved) {
        dispose();
      }
    } catch (Exception e) {
      POSMessageDialog.showError(this, "Sorry, an unexpected error has occured. Could not save.", e);
    }
  }
  


  private TransparentPanel jPanel1;
  
  private TransparentPanel buttonPanel;
  
  private JSeparator jSeparator1;
  
  private TitlePanel titlePanel;
  
  public void open()
  {
    canceled = false;
    pack();
    setLocationRelativeTo(getOwner());
    super.setVisible(true);
  }
  
  public void openFullScreen() {
    canceled = false;
    setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
    setLocationRelativeTo(getOwner());
    setVisible(true);
  }
  
  public void openWithScale(int w, int h)
  {
    canceled = false;
    setSize(PosUIManager.getSize(w, h));
    setLocationRelativeTo(getOwner());
    super.setVisible(true);
  }
  
  public boolean isCanceled() {
    return canceled;
  }
  
  public Frame getParentFrame() {
    return (Frame)getOwner();
  }
  
  public void windowOpened(WindowEvent e) {}
  
  public void windowClosing(WindowEvent e)
  {
    performCancel(null);
  }
  

  public void windowClosed(WindowEvent e) {}
  

  public void windowIconified(WindowEvent e) {}
  

  public void windowDeiconified(WindowEvent e) {}
  

  public void windowActivated(WindowEvent e) {}
  
  public void windowDeactivated(WindowEvent e) {}
  
  public BeanEditor getBeanEditor()
  {
    return beanEditor;
  }
  
  public Object getBean() {
    if (beanEditor != null) {
      return beanEditor.getBean();
    }
    return null;
  }
  
  public void setBean(Object bean) {
    if (beanEditor != null) {
      beanEditor.setBean(bean);
    }
  }
  
  public void setBeanEditor(BeanEditor beanEditor) {
    if (this.beanEditor != beanEditor) {
      this.beanEditor = beanEditor;
      beanEditorContainer.removeAll();
      
      if (beanEditor != null) {
        beanEditor.setEditorDialog(this);
        beanEditorContainer.add(beanEditor);
        titlePanel.setTitle(beanEditor.getDisplayText());
      }
      beanEditorContainer.revalidate();
    }
  }
  
  public TransparentPanel getButtonPanel() {
    return buttonPanel;
  }
}
