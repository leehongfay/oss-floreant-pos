package com.floreantpos.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.model.Attribute;
import com.floreantpos.model.AttributeGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.dao.AttributeDAO;
import com.floreantpos.swing.ButtonColumn;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.model.AttributeEntryForm;
import com.floreantpos.util.NumericGlobalIdGenerator;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxEditor;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;

public class VariantAttributesSelectionDialog
  extends OkCancelOptionDialog
{
  private JXTable attributeTable = new JXTable();
  private VariantAttributeModel tableModel = new VariantAttributeModel();
  
  private List<AttributeGroup> groups;
  private JButton btnAddNew;
  private List<MenuItem> existingVariants = new ArrayList();
  private List<MenuItem> variants;
  List<Attribute> attributes = new ArrayList();
  private final List<Attribute> attributeList;
  private MenuItem parentMenuItem;
  private JComboBox cbAttributes;
  
  public VariantAttributesSelectionDialog(List<MenuItem> variants) {
    existingVariants = variants;
    attributeList = AttributeDAO.getInstance().findAll();
    createUI();
    setTitle("Select Attributes");
    setCaption("Select Attributes");
    attributeTable.setModel(tableModel);
    updateView();
    
    AbstractAction action = new AbstractAction()
    {
      public void actionPerformed(ActionEvent e)
      {
        int row = Integer.parseInt(e.getActionCommand());
        Attribute attribute = (Attribute)tableModel.getRowData(row);
        tableModel.deleteItem(attribute);
        tableModel.fireTableRowsDeleted(row, row);
      }
    };
    ButtonColumn buttonColumn = new ButtonColumn(attributeTable, action, 1)
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        PosButton button = (PosButton)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        button.setIcon(IconFactory.getIcon("/ui_icons/", "delete-icon.png"));
        button.setOpaque(false);
        button.setBorder(null);
        return button;
      }
      
      public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
      {
        return super.getTableCellEditorComponent(table, value, false, row, column);
      }
    };
    resizeTableColumns();
  }
  
  private void updateView() {
    List<Attribute> attributes = new ArrayList();
    if (existingVariants != null) {
      for (MenuItem variant : existingVariants) {
        if (variant.getAttributes() != null) {
          for (Attribute att : variant.getAttributes()) {
            if (!attributes.contains(att)) {
              attributes.add(att);
            }
          }
        }
      }
    }
    Collections.sort(attributes, new Comparator()
    {
      public int compare(Attribute o1, Attribute o2)
      {
        return o1.getGroup().getName().compareTo(o2.getGroup().getName());
      }
    });
    tableModel.setRows(attributes);
  }
  
  public void setParentMenuItem(MenuItem parent) {
    parentMenuItem = parent;
  }
  
  private void createUI() {
    JPanel contentPanel = getContentPanel();
    contentPanel.setLayout(new MigLayout("aligny top,ins 0 10 0 10", "[100%]", ""));
    
    JPanel topPanel = new JPanel(new MigLayout("fillx,ins 2"));
    cbAttributes = new JComboBox(new ComboBoxModel(new ArrayList()));
    cbAttributes.setOpaque(false);
    cbAttributes.setBorder(null);
    cbAttributes.setEditable(true);
    cbAttributes.setEditor(new CustomComboBoxEditor());
    cbAttributes.setRenderer(new ColorCellRenderer());
    

    cbAttributes.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if (e.getStateChange() == 1) {}
      }
      

    });
    cbAttributes.setUI(new BasicComboBoxUI()
    {
      protected JButton createArrowButton() {
        new JButton()
        {
          public int getWidth() {
            return 0;
          }
        };
      }
    });
    cbAttributes.remove(getComponent(0));
    addPopupMouseListener();
    topPanel.add(new JLabel("Attribute"), "split 3,h 30!");
    topPanel.add(cbAttributes, "growx,h 30!");
    
    JButton btnSelect = new JButton("Select");
    btnSelect.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        VariantAttributesSelectionDialog.this.doSelectAttribute();
      }
    });
    topPanel.add(btnSelect, "h 30!");
    
    JPanel attributeListPanel = new JPanel(new MigLayout("fill"));
    attributeListPanel.setBorder(BorderFactory.createTitledBorder("Attributes"));
    
    attributeTable.setTableHeader(null);
    attributeTable.setRowHeight(30);
    attributeTable.setRowSelectionAllowed(false);
    attributeTable.setCellSelectionEnabled(false);
    attributeListPanel.add(new JScrollPane(attributeTable), "grow,span,wrap");
    
    contentPanel.add(topPanel, "growx,span 2,aligny top,wrap");
    contentPanel.add(attributeListPanel, "grow,aligny top");
  }
  
  private void resizeTableColumns() {
    attributeTable.setAutoResizeMode(4);
    setColumnWidth(1, PosUIManager.getSize(50));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = attributeTable.getColumnModel().getColumn(columnNumber);
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  private void doSelectAttribute() {
    AttributesSelectionDialog dialog = new AttributesSelectionDialog(tableModel.getRows());
    dialog.setSize(PosUIManager.getSize(500, 440));
    dialog.open();
    if (dialog.isCanceled())
      return;
    List<Attribute> attributes = dialog.getSelectedAttributeList();
    if (attributes == null)
      return;
    Collections.sort(attributes, new Comparator()
    {
      public int compare(Attribute o1, Attribute o2)
      {
        return o1.getGroup().getName().compareTo(o2.getGroup().getName());
      }
    });
    tableModel.setRows(attributes); }
  
  class ColorCellRenderer implements ListCellRenderer { ColorCellRenderer() {}
    
    protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
    
    private Dimension preferredSize = new Dimension(0, 30);
    Border emptyBorder = new EmptyBorder(5, 30, 5, 5);
    
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
      JLabel renderer = (JLabel)defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      renderer.setBackground(Color.WHITE);
      renderer.setBorder(emptyBorder);
      renderer.setHorizontalAlignment(2);
      if (((value instanceof Attribute)) && (((Attribute)value).getId() != null)) {
        Attribute attribute = (Attribute)value;
        renderer.setText("  " + attribute.getGroup() + ": " + attribute.getName());
        
        if ((cellHasFocus) || (isSelected)) {
          renderer.setBackground(new Color(0.0F, 0.0F, 0.0F, 0.1F));
          renderer.setBorder(new LineBorder(Color.DARK_GRAY));
          renderer.setForeground(Color.BLUE);
        }
        else {
          renderer.setForeground(Color.BLACK);
          renderer.setBorder(null);
        }
      }
      else {
        renderer.setForeground(new Color(201, 244, 236));
        renderer.setBorder(null);
        renderer.setHorizontalAlignment(0);
        renderer.setFont(new Font(renderer.getFont().getName(), 1, 12));
        renderer.setBackground(Color.GRAY);
      }
      renderer.setPreferredSize(preferredSize);
      return renderer;
    }
  }
  
  private void doCreateNewAttribute() {
    Attribute att = new Attribute();
    CustomComboBoxEditor editor = (CustomComboBoxEditor)cbAttributes.getEditor();
    String name = ((VariantAttributesSelectionDialog.CustomComboBoxEditor.AttributePanel)editor.getEditorComponent()).getText();
    att.setName(name);
    AttributeEntryForm inventoryUnitFormTree = new AttributeEntryForm(att);
    BeanEditorDialog dialog = new BeanEditorDialog(inventoryUnitFormTree);
    dialog.setPreferredSize(PosUIManager.getSize(500, 400));
    dialog.open();
    if (dialog.isCanceled())
      return;
    Attribute newAtt = (Attribute)inventoryUnitFormTree.getBean();
    tableModel.addItem(newAtt);
  }
  

  public void doOk()
  {
    List<Attribute> attributes = tableModel.getRows();
    Map<String, MenuItem> itemMap; if ((attributes == null) || (attributes.isEmpty())) {
      variants = new ArrayList();
    }
    else {
      itemMap = new LinkedHashMap();
      variants = generatedPossibleVariants(attributes);
      if (existingVariants != null) {
        for (MenuItem item : existingVariants) {
          item.setName(item.getVariantName());
          List<Attribute> exstingsAttributes = item.getAttributes();
          Collections.sort(exstingsAttributes, new Comparator()
          {
            public int compare(Attribute o1, Attribute o2)
            {
              return o1.getId().compareTo(o2.getId());
            }
          });
          String str = "";
          for (Attribute att : exstingsAttributes) {
            str = str + att.getName() + "-";
          }
          itemMap.put(str, item);
        }
      }
      if (variants != null) {
        for (MenuItem item : variants) {
          item.setName(item.getVariantName());
          List<Attribute> exstingsAttributes = item.getAttributes();
          Collections.sort(exstingsAttributes, new Comparator()
          {
            public int compare(Attribute o1, Attribute o2)
            {
              return o1.getId().compareTo(o2.getId());
            }
          });
          String str = "";
          for (Attribute att : exstingsAttributes) {
            str = str + att.getName() + "-";
          }
          MenuItem mapItem = (MenuItem)itemMap.get(str);
          if (mapItem != null) {
            item.setBarcode(mapItem.getBarcode());
            item.setPrice(mapItem.getPrice());
            item.setVisible(mapItem.isVisible());
          }
          Collections.sort(exstingsAttributes, new Comparator()
          {
            public int compare(Attribute o1, Attribute o2)
            {
              return o1.getGroup().getId().compareTo(o2.getGroup().getId());
            }
          });
        }
      }
    }
    setCanceled(false);
    dispose();
  }
  
  private List<MenuItem> generatedPossibleVariants(List<Attribute> attributes) {
    Map<AttributeGroup, List<Attribute>> itemMap = new HashMap();
    for (Attribute attribute : attributes) {
      List<Attribute> attributeList = (List)itemMap.get(attribute.getGroup());
      if (attributeList == null) {
        attributeList = new ArrayList();
        itemMap.put(attribute.getGroup(), attributeList);
      }
      attributeList.add(attribute);
    }
    
    for (AttributeGroup group : itemMap.keySet()) {
      if (group.getAttributes() == null) {}
    }
    

    Object[] arr = attributes.toArray();
    List variants = new ArrayList();
    int r = itemMap.size();
    int n = arr.length;
    generateCombination(variants, arr, n, r);
    return variants;
  }
  
  private void addPopupMouseListener() {
    try {
      Field popupInBasicComboBoxUI = BasicComboBoxUI.class.getDeclaredField("popup");
      popupInBasicComboBoxUI.setAccessible(true);
      
      BasicComboPopup popup = (BasicComboPopup)popupInBasicComboBoxUI.get(cbAttributes.getUI());
      
      Field scrollerInBasicComboPopup = BasicComboPopup.class.getDeclaredField("scroller");
      scrollerInBasicComboPopup.setAccessible(true);
      JScrollPane scroller = (JScrollPane)scrollerInBasicComboPopup.get(popup);
      
      scroller.getViewport().getView().addMouseListener(new MouseListener()
      {
        public void mouseReleased(MouseEvent e)
        {
          Object selectedItem = cbAttributes.getSelectedItem();
          if ((selectedItem instanceof Attribute)) {
            Attribute attribute = (Attribute)selectedItem;
            if (attribute.getId() != null) {
              if (tableModel.getRows().contains(attribute)) {
                return;
              }
              tableModel.addItem(attribute);
            }
            else {
              VariantAttributesSelectionDialog.this.doCreateNewAttribute();
            }
          }
          cbAttributes.hidePopup();
          VariantAttributesSelectionDialog.CustomComboBoxEditor editor = (VariantAttributesSelectionDialog.CustomComboBoxEditor)cbAttributes.getEditor();
          ((VariantAttributesSelectionDialog.CustomComboBoxEditor.AttributePanel)editor.getEditorComponent()).selectAll();
        }
        



        public void mousePressed(MouseEvent e) {}
        


        public void mouseExited(MouseEvent e) {}
        


        public void mouseEntered(MouseEvent e) {}
        


        public void mouseClicked(MouseEvent e) {}
      });
    }
    catch (NoSuchFieldException e)
    {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }
  }
  
  void combinationUtil(List variants, Object[] arr, int n, int r, int index, Object[] data, int i) {
    if (index == r) {
      List<String> groups = new ArrayList();
      List<Attribute> attributes = new ArrayList();
      for (int j = 0; j < r; j++)
      {
        Attribute att = (Attribute)data[j];
        if (groups.contains(att.getGroup().getId())) {
          return;
        }
        groups.add(att.getGroup().getId());
        attributes.add(att);
      }
      MenuItem variant = new MenuItem();
      variant.setName(parentMenuItem.getName());
      variant.setParentMenuItem(parentMenuItem);
      variant.setMenuGroupId(parentMenuItem.getMenuGroupId());
      variant.setMenuGroupName(parentMenuItem.getMenuGroupName());
      variant.setMenuCategoryId(parentMenuItem.getMenuCategoryId());
      variant.setMenuCategoryName(parentMenuItem.getMenuCategoryName());
      
      variant.setUnit(parentMenuItem.getUnit());
      variant.setVariant(Boolean.valueOf(true));
      variant.setInventoryItem(parentMenuItem.isInventoryItem());
      variant.setAttributes(attributes);
      String generateGlobalId = NumericGlobalIdGenerator.generateGlobalId();
      variant.setBarcode(generateGlobalId);
      variant.setSku(generateGlobalId);
      
      variants.add(variant);
      return;
    }
    
    if (i >= n) {
      return;
    }
    data[index] = arr[i];
    combinationUtil(variants, arr, n, r, index + 1, data, i + 1);
    combinationUtil(variants, arr, n, r, index, data, i + 1);
  }
  
  void generateCombination(List variants, Object[] arr, int n, int r) {
    Object[] data = new Object[r];
    combinationUtil(variants, arr, n, r, 0, data, 0);
  }
  
  public List<MenuItem> getVariants() {
    return variants;
  }
  
  public class VariantAttributeModel extends ListTableModel<Attribute>
  {
    public VariantAttributeModel() {
      super();
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      if (columnIndex == 1)
        return true;
      return false;
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      Attribute data = (Attribute)rows.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return data.getGroup() + " : " + data.getName();
      case 1: 
        return "";
      }
      return null;
    }
  }
  
  class CustomComboBoxEditor implements ComboBoxEditor {
    AttributePanel panel;
    
    public CustomComboBoxEditor() {
      panel = new AttributePanel();
    }
    
    public void setItem(Object anObject) {
      if (anObject != null) {
        panel.setText(anObject.toString());
      }
    }
    
    public Component getEditorComponent() {
      return panel;
    }
    
    public Object getItem() {
      return panel.getText();
    }
    
    public void selectAll() {
      panel.selectAll();
    }
    

    public void addActionListener(ActionListener l) {}
    
    public void removeActionListener(ActionListener l) {}
    
    class AttributePanel
      extends JPanel
    {
      JTextField textField;
      
      public AttributePanel()
      {
        setLayout(new BorderLayout());
        
        textField = new JTextField("");
        textField.setColumns(20);
        textField.setRequestFocusEnabled(true);
        textField.requestFocus();
        textField.addKeyListener(new KeyListener()
        {
          public void keyTyped(KeyEvent e) {}
          



          public void keyReleased(KeyEvent e)
          {
            if (e.getKeyCode() == 27) {
              return;
            }
            if ((e.getKeyCode() == 40) || (e.getKeyCode() == 38)) {
              if (!cbAttributes.isPopupVisible())
                cbAttributes.showPopup();
              return;
            }
            String txt = (String)getItem();
            comboFilter(txt);
          }
          



          public void keyPressed(KeyEvent e) {}
        });
        textField.addFocusListener(new FocusListener()
        {
          public void focusLost(FocusEvent e) {}
          


          public void focusGained(FocusEvent e)
          {
            selectAll();
          }
        });
        textField.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            Object selectedItem = cbAttributes.getSelectedItem();
            if ((selectedItem instanceof Attribute)) {
              Attribute attribute = (Attribute)selectedItem;
              if (attribute.getId() != null) {
                if (!tableModel.getRows().contains(attribute)) {
                  tableModel.addItem(attribute);
                }
              }
              else {
                VariantAttributesSelectionDialog.this.doCreateNewAttribute();
              }
            }
            setText("");
            cbAttributes.hidePopup();
          }
        });
        add(textField);
      }
      
      public void comboFilter(String enteredText) {
        if ((enteredText.isEmpty()) && 
          (cbAttributes.isPopupVisible())) {
          cbAttributes.hidePopup();
          return;
        }
        
        List filterArray = new ArrayList();
        for (Attribute attribute : attributeList) {
          if (attribute.getName().toLowerCase().startsWith(enteredText.toLowerCase())) {
            filterArray.add(attribute);
          }
        }
        Attribute newAtt = new Attribute();
        newAtt.setName("Create New");
        newAtt.setGroup(new AttributeGroup());
        filterArray.add(newAtt);
        
        if (filterArray.size() > 0) {
          ComboBoxModel model = (ComboBoxModel)cbAttributes.getModel();
          model.removeAllElements();
          for (Object s : filterArray) {
            model.addElement(s);
          }
          setText(enteredText);
        }
        if (!cbAttributes.isPopupVisible()) {
          cbAttributes.showPopup();
        }
        resize();
      }
      
      public void resize() {
        Object child = cbAttributes.getAccessibleContext().getAccessibleChild(0);
        BasicComboPopup popup = (BasicComboPopup)child;
        JList list = popup.getList();
        Dimension preferred = list.getPreferredSize();
        width = (textField.getWidth() + 20);
        int rowHeight = height / cbAttributes.getItemCount();
        int maxHeight = cbAttributes.getMaximumRowCount() * rowHeight;
        height = Math.min(height, maxHeight);
        
        Container c = SwingUtilities.getAncestorOfClass(JScrollPane.class, list);
        JScrollPane scrollPane = (JScrollPane)c;
        
        scrollPane.setPreferredSize(preferred);
        scrollPane.setMaximumSize(preferred);
        
        Dimension popupSize = popup.getSize();
        width = textField.getWidth();
        height += 2;
        Component parent = popup.getParent();
        parent.setSize(popupSize);
        
        parent.validate();
        parent.repaint();
        
        Window mainFrame = SwingUtilities.windowForComponent(cbAttributes);
        Window popupWindow = SwingUtilities.windowForComponent(popup);
        
        if (popupWindow != mainFrame)
          popupWindow.pack();
      }
      
      public void setText(String s) {
        if (s.equals("Create New")) {
          return;
        }
        textField.setText(s);
      }
      
      public String getText() {
        return textField.getText();
      }
      
      public void setIcon(Icon i) {
        repaint();
      }
      
      public void selectAll() {
        textField.selectAll();
      }
      
      public void addActionListener(ActionListener l) {}
      
      public void removeActionListener(ActionListener l) {}
    }
  }
}
