package com.floreantpos.ui.dialog;

import com.floreantpos.config.AppProperties;
import com.floreantpos.main.Application;
import com.floreantpos.util.POSUtil;
import com.orocube.licensemanager.OroLicense;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import net.miginfocom.swing.MigLayout;

public class OroLicenseInfoDialog
  extends JDialog
{
  private OroLicense license;
  
  public OroLicenseInfoDialog(OroLicense license)
  {
    super(Application.getPosWindow());
    this.license = license;
    initComponents();
  }
  
  public OroLicenseInfoDialog() {
    super(POSUtil.getBackOfficeWindow());
  }
  
  private void initComponents() {
    setSize(500, 400);
    setLayout(new BorderLayout());
    setModal(true);
    setTitle(AppProperties.getAppName());
    
    Image applicationImage = Application.getApplicationIcon().getImage();
    
    JPanel container = new JPanel(new BorderLayout());
    container.setBorder(new EmptyBorder(20, 20, 20, 20));
    
    JPanel topPanel = new JPanel(new MigLayout("fillx"));
    JPanel contentPanel = new JPanel(new MigLayout("fillx,center,inset 20", "[][]", ""));
    contentPanel.setBorder(new LineBorder(Color.gray));
    contentPanel.setBackground(Color.white);
    
    JLabel l = new JLabel("License Information");
    l.setFont(new Font(l.getFont().getName(), 1, 24));
    l.setOpaque(true);
    
    JLabel iconLabel = new JLabel(new ImageIcon(applicationImage), 0);
    iconLabel.setBackground(Color.WHITE);
    iconLabel.setOpaque(true);
    topPanel.add(iconLabel, "split 2");
    
    topPanel.add(l, "h 40!,grow,center,wrap");
    topPanel.add(new JSeparator(), "grow,span");
    
    container.add(topPanel, "North");
    
    JLabel lblPluginName = new JLabel("Product Name");
    JLabel txtPluginName = new JLabel(license.getProductName());
    contentPanel.add(lblPluginName);
    contentPanel.add(txtPluginName, "wrap");
    
    JLabel lblVersion = new JLabel("Version ");
    JLabel txtVersion = new JLabel(license.getProductVersion());
    contentPanel.add(lblVersion);
    contentPanel.add(txtVersion, "wrap");
    
    JLabel lblIssuerName = new JLabel("Issuer Name");
    JLabel txtIssuerName = new JLabel(license.getIssuer());
    contentPanel.add(lblIssuerName);
    contentPanel.add(txtIssuerName, "wrap");
    
    JLabel lblHolderName = new JLabel("Holder Name");
    JLabel txtHolderName = new JLabel(license.getHolderName());
    contentPanel.add(lblHolderName);
    contentPanel.add(txtHolderName, "wrap");
    
    JLabel lblHolderEmail = new JLabel("Holder Email");
    JLabel txtHolderEmail = new JLabel(license.getHolderEmail());
    txtHolderEmail.setForeground(Color.blue);
    contentPanel.add(lblHolderEmail);
    contentPanel.add(txtHolderEmail, "wrap");
    
    JLabel lblIssueDate = new JLabel("Issue Date");
    JLabel txtIssueDate = new JLabel(new SimpleDateFormat("dd MMM yyyy hh:mm a").format(license.getIssueDate()));
    contentPanel.add(lblIssueDate);
    contentPanel.add(txtIssueDate, "wrap");
    
    JLabel lblExpiryDate = new JLabel("Expiry Date");
    long MILLIS_PER_DAY = 86400000L;
    long msDiff = license.getExpiryDate().getTime() - new Date().getTime();
    long daysDiff = Math.round(msDiff / MILLIS_PER_DAY);
    
    JLabel txtExpiryDate = new JLabel(new SimpleDateFormat("dd MMM yyyy hh:mm a").format(license.getExpiryDate()) + " (" + daysDiff + (Math.abs(daysDiff) > 1L ? " days)" : " day)"));
    contentPanel.add(lblExpiryDate);
    contentPanel.add(txtExpiryDate, "wrap");
    
    container.add(contentPanel);
    
    JButton btnOk = new JButton("Close");
    btnOk.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        dispose();
      }
      
    });
    JButton btnUpgrade = new JButton("Update");
    btnUpgrade.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e) {}


    });
    JPanel buttonPanel = new JPanel(new MigLayout("center"));
    
    buttonPanel.add(btnOk, "h 40!,w 100!");
    
    container.add(buttonPanel, "South");
    
    setContentPane(container);
  }
  













  public void setVisible(boolean b)
  {
    setLocationRelativeTo(null);
    super.setVisible(b);
  }
  
  public OroLicense getLicense() {
    return license;
  }
  
  public void setLicense(OroLicense license) {
    this.license = license;
    initComponents();
  }
}
