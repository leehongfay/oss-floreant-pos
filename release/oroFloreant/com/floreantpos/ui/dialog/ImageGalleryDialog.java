package com.floreantpos.ui.dialog;

import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.model.ImageResource;
import com.floreantpos.model.ImageResource.IMAGE_CATEGORY;
import com.floreantpos.model.dao.BaseImageResourceDAO;
import com.floreantpos.model.dao.ImageResourceDAO;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PaginatedTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.views.ImagePanel;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.FlowLayout;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;







public class ImageGalleryDialog
  extends POSDialog
  implements ActionListener
{
  private static ImageGalleryDialog instance = null;
  
  private JPanel centerPanel;
  private JButton btnBack;
  private JButton btnForward;
  private ImageResource selectedImageResource;
  private PaginatedTableModel imageGalleryModel;
  private JPanel southPanel;
  private POSToggleButton tbtnAll;
  private POSToggleButton tbtnUnlisted;
  private POSToggleButton tbtnFloorPlan;
  private POSToggleButton tbtnProducts;
  private POSToggleButton tbtnPeople;
  private POSToggleButton tbtnDeleted;
  private ImageResource.IMAGE_CATEGORY defaultCategory;
  private JButton btnSearch;
  private POSTextField tfSearch;
  private PosButton btnDelete;
  private int maxWidth;
  private int maxHeight;
  private PosButton btnSelect;
  
  public static synchronized ImageGalleryDialog getInstance()
  {
    if (instance == null) {
      instance = new ImageGalleryDialog();
    }
    return instance;
  }
  
  private ImageGalleryDialog() {
    super(POSUtil.getBackOfficeWindow(), Dialog.ModalityType.APPLICATION_MODAL);
    imageGalleryModel = new PaginatedTableModel()
    {
      public Object getValueAt(int rowIndex, int columnIndex) {
        return null;
      }
    };
    imageGalleryModel.setPageSize(10);
    imageGalleryModel.setNumRows(BaseImageResourceDAO.getInstance().rowCount());
    init();
    setSize(PosUIManager.getSize(950, 720));
    setResizable(true);
  }
  
  private void init() {
    setLayout(new BorderLayout());
    JPanel topPanel = new JPanel(new BorderLayout(5, 5));
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Image Gallery");
    topPanel.add(titlePanel, "North");
    
    JPanel filterPanel = new JPanel();
    
    ButtonGroup btnGroup = new ButtonGroup();
    
    tbtnAll = new POSToggleButton("ALL");
    tbtnUnlisted = new POSToggleButton("UNLISTED");
    tbtnFloorPlan = new POSToggleButton("FLOORPLAN");
    tbtnProducts = new POSToggleButton("PRODUCTS");
    tbtnPeople = new POSToggleButton("PEOPLE");
    tbtnDeleted = new POSToggleButton("DELETED");
    
    btnGroup.add(tbtnAll);
    btnGroup.add(tbtnUnlisted);
    btnGroup.add(tbtnFloorPlan);
    btnGroup.add(tbtnProducts);
    btnGroup.add(tbtnPeople);
    btnGroup.add(tbtnDeleted);
    
    tbtnAll.addActionListener(this);
    tbtnUnlisted.addActionListener(this);
    tbtnFloorPlan.addActionListener(this);
    tbtnProducts.addActionListener(this);
    tbtnPeople.addActionListener(this);
    tbtnDeleted.addActionListener(this);
    
    filterPanel.add(tbtnAll);
    filterPanel.add(tbtnUnlisted);
    filterPanel.add(tbtnFloorPlan);
    filterPanel.add(tbtnProducts);
    filterPanel.add(tbtnPeople);
    filterPanel.add(tbtnDeleted);
    
    topPanel.add(filterPanel, "Center");
    



    JLabel lblSearch = new JLabel("Search");
    lblSearch.setHorizontalAlignment(0);
    tfSearch = new POSTextField(25);
    btnSearch = new JButton("Search");
    
    JPanel searchPanel = new JPanel();
    

    searchPanel.add(lblSearch, "right,growx");
    searchPanel.add(tfSearch, "grow");
    searchPanel.add(btnSearch, "grow");
    
    topPanel.add(searchPanel, "Last");
    
    add(topPanel, "North");
    
    centerPanel = new JPanel(new MigLayout("center,wrap 5", "sg,fill", ""));
    centerPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
    
    southPanel = new JPanel(new BorderLayout());
    
    JScrollPane scrollPane = new JScrollPane(centerPanel, 20, 31);
    
    scrollPane.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
    
    JPanel backwordForwardButtonPanel = new JPanel(new BorderLayout(5, 5));
    backwordForwardButtonPanel.setBorder(new EmptyBorder(10, 5, 0, 5));
    btnBack = new JButton("<<<");
    btnForward = new JButton(">>>");
    
    btnBack.setEnabled(false);
    
    backwordForwardButtonPanel.add(btnBack, "West");
    backwordForwardButtonPanel.add(btnForward, "East");
    
    tbtnAll.setSelected(true);
    defaultCategory = null;
    createImagePanel(defaultCategory, null);
    
    btnForward.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        imageGalleryModel.setCurrentRowIndex(imageGalleryModel.getNextRowIndex());
        ImageGalleryDialog.this.createImagePanel(defaultCategory, null);
      }
      
    });
    btnBack.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        imageGalleryModel.setCurrentRowIndex(imageGalleryModel.getPreviousRowIndex());
        ImageGalleryDialog.this.createImagePanel(defaultCategory, null);
      }
      
    });
    add(scrollPane, "Center");
    
    JPanel buttonPanel = new JPanel();
    buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    
    buttonPanel.setLayout(new BorderLayout());
    JPanel insideBtnPanel = new JPanel(new FlowLayout());
    





    buttonPanel.add(insideBtnPanel);
    
    PosButton btnUpload = new PosButton("Upload");
    
    btnUpload.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ImageGalleryDialog.this.doShowUploadDialog();
      }
    });
    PosButton btnBulkImport = new PosButton("Bulk import");
    
    btnBulkImport.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ImageGalleryDialog.this.doShowBulkImportDialog();
      }
    });
    JPanel westPanel = new JPanel();
    westPanel.add(btnUpload);
    westPanel.add(btnBulkImport);
    buttonPanel.add(westPanel, "West");
    PosButton btnCancel = new PosButton("Close");
    buttonPanel.add(btnCancel, "East");
    
    btnCancel.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        setCanceled(true);
        dispose();
      }
      

    });
    PosButton btnEdit = new PosButton("Edit");
    


    btnEdit.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ImageGalleryDialog.this.doShowEditDialog();
      }
      

    });
    btnDelete = new PosButton("Delete");
    

    btnDelete.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ImageGalleryDialog.this.doDeleteImage();
      }
      

    });
    btnSelect = new PosButton("Select");
    btnSelect.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (selectedImageResource == null) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select image!");
          return;
        }
        setCanceled(false);
        dispose();

      }
      

    });
    insideBtnPanel.add(btnSelect);
    insideBtnPanel.add(btnEdit);
    insideBtnPanel.add(btnDelete);
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ImageGalleryDialog.this.createImagePanel(defaultCategory, tfSearch.getText());
      }
      

    });
    tfSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ImageGalleryDialog.this.createImagePanel(defaultCategory, tfSearch.getText());
      }
      

    });
    southPanel.add(backwordForwardButtonPanel, "North");
    
    southPanel.add(buttonPanel, "South");
    add(southPanel, "South");
  }
  
  public ImageResource getImageResource()
  {
    if (selectedImageResource != null) {
      return selectedImageResource;
    }
    return null;
  }
  
  private void createImagePanel(ImageResource.IMAGE_CATEGORY imgCategory, String description)
  {
    centerPanel.removeAll();
    centerPanel.revalidate();
    centerPanel.repaint();
    BaseImageResourceDAO.getInstance().getImages(imageGalleryModel, imgCategory, description);
    List<ImageResource> rows = imageGalleryModel.getRows();
    ButtonGroup btnGroup = new ButtonGroup();
    for (ImageResource imageResource : rows) {
      ImagePanel imgPanel = new ImagePanel(this, imageResource, btnGroup);
      centerPanel.add(imgPanel, "aligny, top");
    }
    
    btnBack.setEnabled(imageGalleryModel.hasPrevious());
    btnForward.setEnabled(imageGalleryModel.hasNext());
    centerPanel.revalidate();
    centerPanel.repaint();
  }
  
  public void actionPerformed(ActionEvent e)
  {
    doCategorySelection(e);
  }
  
  private void doCategorySelection(ActionEvent e)
  {
    JToggleButton tgButton = (JToggleButton)e.getSource();
    if (tgButton.isSelected()) {
      selectedImageResource = ((ImageResource)tgButton.getClientProperty("selected"));
    }
    
    if (e.getSource() == tbtnAll) {
      defaultCategory = null;
      createImagePanel(defaultCategory, null);
      btnDelete.setEnabled(true);
    }
    else if (e.getSource() == tbtnUnlisted)
    {
      defaultCategory = ImageResource.IMAGE_CATEGORY.UNLISTED;
      createImagePanel(defaultCategory, null);
      btnDelete.setEnabled(true);
    }
    else if (e.getSource() == tbtnFloorPlan) {
      defaultCategory = ImageResource.IMAGE_CATEGORY.FLOORPLAN;
      createImagePanel(defaultCategory, null);
      btnDelete.setEnabled(true);
    }
    else if (e.getSource() == tbtnPeople) {
      defaultCategory = ImageResource.IMAGE_CATEGORY.PEOPLE;
      createImagePanel(defaultCategory, null);
      btnDelete.setEnabled(true);
    }
    else if (e.getSource() == tbtnProducts) {
      defaultCategory = ImageResource.IMAGE_CATEGORY.PRODUCTS;
      createImagePanel(defaultCategory, null);
      btnDelete.setEnabled(true);
    }
    else if (e.getSource() == tbtnDeleted) {
      defaultCategory = ImageResource.IMAGE_CATEGORY.DELETED;
      createImagePanel(defaultCategory, null);
      btnDelete.setEnabled(false);
    }
  }
  
  private void doDeleteImage() {
    if (selectedImageResource == null) {
      return;
    }
    int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Do you want to delete this item?", "Delete");
    
    if (option != 0) {
      return;
    }
    try {
      selectedImageResource.setImageCategoryNum(Integer.valueOf(ImageResource.IMAGE_CATEGORY.DELETED.getType()));
      BaseImageResourceDAO.getInstance().saveOrUpdate(selectedImageResource);
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Image successfully deleted!");
      createImagePanel(defaultCategory, null);
    } catch (Exception error) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), error.getMessage(), error);
    }
  }
  
  private void doShowUploadDialog() {
    try {
      ImageUploaderDialog dialog = new ImageUploaderDialog(null);
      dialog.setImageMaximumSize(maxWidth, maxHeight);
      dialog.selectImage();
      File selectedImageFile = dialog.getSelectedImageFile();
      if (selectedImageFile == null) {
        return;
      }
      dialog.setTitle("Image Upload");
      dialog.setDefaultCloseOperation(2);
      dialog.setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      createImagePanel(defaultCategory, null);
    } catch (PosException e1) {
      JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", 0);
    } catch (Exception e2) {
      PosLog.error(getClass(), e2);
      JOptionPane.showMessageDialog(null, "Unexpected error", "Error", 0);
    }
  }
  
  private void doShowBulkImportDialog()
  {
    try {
      ImageUploaderDialog dialog = new ImageUploaderDialog();
      dialog.setImageMaximumSize(maxWidth, maxHeight);
      dialog.selectBulkImage();
      List<File> selectedImageFile = dialog.getSelectedImageFiles();
      if ((selectedImageFile == null) || (selectedImageFile.size() < 1)) {
        return;
      }
      dialog.setTitle("Image Upload");
      dialog.setDefaultCloseOperation(2);
      dialog.setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      createImagePanel(defaultCategory, null);
    } catch (PosException e1) {
      JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", 0);
    } catch (Exception e2) {
      PosLog.error(getClass(), e2);
      JOptionPane.showMessageDialog(null, "Unexpected error", "Error", 0);
    }
  }
  
  private void doShowEditDialog()
  {
    if (selectedImageResource == null) {
      return;
    }
    ImageUploaderDialog dialog = new ImageUploaderDialog(selectedImageResource);
    
    dialog.setTitle("Image Edit");
    dialog.setDefaultCloseOperation(2);
    



    dialog.openFullScreen();
    if (dialog.isCanceled()) {
      return;
    }
    createImagePanel(defaultCategory, null);
  }
  
  public void setImageMaximumSize(int maxWidth, int maxHeight) {
    this.maxWidth = maxWidth;
    this.maxHeight = maxHeight;
  }
  
  public void setSelectBtnVisible(boolean isVisible) {}
}
