package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.main.Application;
import com.floreantpos.model.AttendenceHistory;
import com.floreantpos.model.EmployeeInOutHistory;
import com.floreantpos.model.Shift;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.AttendenceHistoryDAO;
import com.floreantpos.model.dao.EmployeeInOutHistoryDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.ShiftUtil;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.ListSelectionModel;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.table.JTableHeader;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.joda.time.DateTime;
import org.joda.time.Period;



















public class AttendanceDialog
  extends POSDialog
{
  private SimpleDateFormat dateFormat = new SimpleDateFormat("MMM,dd  hh:mm a");
  private JXTable table;
  private User currentUser;
  private static final SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss");
  private Timer clockTimer = new Timer(1000, new ClockTimerHandler(null));
  private TitlePanel titlePanel;
  
  public AttendanceDialog(User user) {
    currentUser = user;
    setLayout(new MigLayout("fill"));
    setTitle("CLOCKED IN/OUT STATUS");
    
    JPanel contentPanel = new JPanel(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(5, 10, 5, 10));
    
    contentPanel.add(new PosScrollPane(this.table = new JXTable(new AttendenceHistoryTableModel(AttendenceHistoryDAO.getInstance().findAll()))));
    table.getSelectionModel().setSelectionMode(0);
    table.setRowHeight(45);
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    table.getTableHeader().setPreferredSize(PosUIManager.getSize(0, 40));
    
    JPanel bottomPanel = new JPanel(new MigLayout("center,hidemode 3,ins 5 0 5 0"));
    
    titlePanel = new TitlePanel();
    titlePanel.setTitle("");
    add(titlePanel, "grow,wrap");
    
    JLabel clockInStatus = new JLabel();
    clockInStatus.setFont(new Font(getFont().getName(), 1, 20));
    
    bottomPanel.add(clockInStatus, "grow");
    
    PosButton btnClockIn = new PosButton(Messages.getString("ClockInOutAction.5"));
    btnClockIn.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        AttendanceDialog.this.performClockIn(currentUser);
      }
      
    });
    PosButton btnClockOut = new PosButton(Messages.getString("ClockInOutAction.6"));
    btnClockOut.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        AttendanceDialog.this.performClockOut(currentUser);
      }
      
    });
    PosButton btnDriverIn = new PosButton("DRIVER IN");
    btnDriverIn.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        performDriverIn(currentUser);
      }
      
    });
    PosButton btnDriverOut = new PosButton("DRIVER OUT");
    btnDriverOut.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        performDriverOut(currentUser);
      }
    });
    
    if (user.isClockedIn().booleanValue()) {
      bottomPanel.add(btnClockOut);
    }
    else {
      bottomPanel.add(btnClockIn);
    }
    
    if ((user.isDriver().booleanValue()) && 
      (user.isClockedIn().booleanValue())) {
      if (user.isAvailableForDelivery().booleanValue()) {
        bottomPanel.add(btnDriverOut);
      }
      else {
        bottomPanel.add(btnDriverIn);
      }
    }
    

    PosButton btnCancel = new PosButton(Messages.getString("ClockInOutAction.7"));
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        dispose();
      }
      
    });
    PosButton btnPrint = new PosButton("PRINT");
    btnPrint.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          String title = "- Emp. :" + currentUser.getFullName() + " (#" + currentUser.getId() + ")";
          String data = " Clocked in : " + dateFormat.format(currentUser.getLastClockInTime()) + "\n Printed On: " + dateFormat.format(new Date());
          
          ReceiptPrintService.printGenericReport(title, data);
        } catch (Exception ee) {
          POSMessageDialog.showError(Application.getPosWindow(), ee.getMessage(), ee);
        }
      }
    });
    bottomPanel.add(btnPrint);
    
    bottomPanel.add(btnCancel);
    
    bottomPanel.add(new JSeparator(), "newline,span");
    contentPanel.add(bottomPanel, "South");
    add(contentPanel, "grow");
    showAttendanceTime();
    viewReport();
    clockTimer.start();
  }
  
  private void viewReport() {
    try {
      Date fromDate = currentUser.getLastClockInTime();
      Date toDate = DateUtil.endOfDay(new Date());
      
      AttendenceHistoryDAO dao = new AttendenceHistoryDAO();
      List<AttendenceHistory> historyList = dao.findAttendanceHistory(fromDate, toDate, currentUser);
      AttendenceHistoryTableModel model = (AttendenceHistoryTableModel)table.getModel();
      model.setRows(historyList);
    } catch (Exception e) {
      BOMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  private class ClockTimerHandler implements ActionListener {
    private ClockTimerHandler() {}
    
    public void actionPerformed(ActionEvent e) { if (!isShowing()) {
        clockTimer.stop();
        return;
      }
      
      showAttendanceTime();
    }
  }
  
  public void setVisible(boolean aFlag)
  {
    super.setVisible(aFlag);
    
    if (aFlag) {
      clockTimer.start();
    }
    else {
      clockTimer.stop();
    }
  }
  
  public void showAttendanceTime() {
    Date lastClockedInTime = currentUser.getLastClockInTime();
    Date currentTime = new Date();
    
    DateTime startFrom = new DateTime(lastClockedInTime);
    DateTime endTo = new DateTime(currentTime);
    
    Period p = new Period(startFrom, endTo);
    int hours = p.getHours();
    int minutes = p.getMinutes();
    int seconds = p.getMinutes();
    String userString = "Welcome  " + currentUser.getFullName() + "(#" + currentUser.getId() + ")";
    String status = "You have been clocked In for ";
    if (hours > 0) {
      if (hours > 1) {
        status = status + hours + " Hours ";
      }
      else {
        status = status + hours + " Hour ";
      }
    }
    if (minutes > 1) {
      status = status + minutes + " Minutes ";
    }
    else {
      status = status + minutes + " Minute ";
    }
    
    if (currentUser.isClockedIn().booleanValue()) {
      titlePanel.setTitle("<html><h3 style='color:gray'>" + userString + "<br>" + status + "</h3></html>");
    }
    else {
      titlePanel.setTitle("<html>" + userString + "<br>" + "" + "</html>");
    }
  }
  
  class AttendenceHistoryTableModel extends ListTableModel {
    String[] columnNames = {
      Messages.getString("AttendanceHistoryExplorer.7"), Messages.getString("AttendanceHistoryExplorer.8"), Messages.getString("AttendanceHistoryExplorer.9"), Messages.getString("AttendanceHistoryExplorer.10"), Messages.getString("AttendanceHistoryExplorer.11"), Messages.getString("AttendanceHistoryExplorer.12"), Messages.getString("AttendanceHistoryExplorer.13") };
    
    AttendenceHistoryTableModel() {
      setRows(list);
      setColumnNames(columnNames);
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      AttendenceHistory history = (AttendenceHistory)rows.get(rowIndex);
      
      switch (columnIndex)
      {
      case 0: 
        return history.getUser().getId();
      
      case 1: 
        return history.getUser().getFirstName() + " " + history.getUser().getLastName();
      

      case 2: 
        Date date = history.getClockInTime();
        if (date != null) {
          return dateFormat.format(date);
        }
        return "";
      

      case 3: 
        Date date2 = history.getClockOutTime();
        if (date2 != null) {
          return dateFormat.format(date2);
        }
        return "";
      
      case 4: 
        return history.isClockedOut();
      
      case 5: 
        if (history.getShift() == null) {
          return "";
        }
        return history.getShift().getId();
      
      case 6: 
        return history.getTerminal().getId();
      }
      return null;
    }
  }
  
  private void performClockIn(User user) {
    try {
      if (user == null) {
        return;
      }
      
      if ((user.isClockedIn() != null) && (user.isClockedIn().booleanValue()))
      {
        POSMessageDialog.showMessage(Messages.getString("ClockInOutAction.11") + user.getFirstName() + " " + user.getLastName() + Messages.getString("ClockInOutAction.13"));
        return;
      }
      
      Shift currentShift = ShiftUtil.getCurrentShift();
      





      Calendar currentTime = Calendar.getInstance();
      user.doClockIn(Application.getInstance().getTerminal(), currentShift, currentTime);
      

      POSMessageDialog.showMessage(Messages.getString("ClockInOutAction.14") + user.getFirstName() + " " + user.getLastName() + Messages.getString("ClockInOutAction.16"));
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private void performClockOut(User user) {
    try {
      if (user == null) {
        return;
      }
      
      AttendenceHistoryDAO attendenceHistoryDAO = new AttendenceHistoryDAO();
      AttendenceHistory attendenceHistory = attendenceHistoryDAO.findHistoryByClockedInTime(user);
      if (attendenceHistory == null) {
        attendenceHistory = new AttendenceHistory();
        Date lastClockInTime = user.getLastClockInTime();
        Calendar c = Calendar.getInstance();
        c.setTime(lastClockInTime);
        attendenceHistory.setClockInTime(lastClockInTime);
        attendenceHistory.setClockInHour(Short.valueOf((short)c.get(10)));
        attendenceHistory.setUser(user);
        attendenceHistory.setTerminal(Application.getInstance().getTerminal());
        attendenceHistory.setShift(user.getCurrentShift());
      }
      
      Shift shift = user.getCurrentShift();
      Calendar calendar = Calendar.getInstance();
      
      user.doClockOut(attendenceHistory, shift, calendar);
      

      POSMessageDialog.showMessage(Messages.getString("ClockInOutAction.8") + user.getFirstName() + " " + user.getLastName() + Messages.getString("ClockInOutAction.10"));
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public void performDriverOut(User user) {
    try {
      if (user == null) {
        return;
      }
      
      Shift currentShift = ShiftUtil.getCurrentShift();
      Terminal terminal = Application.getInstance().getTerminal();
      
      Calendar currentTime = Calendar.getInstance();
      user.setAvailableForDelivery(Boolean.valueOf(false));
      user.setLastClockOutTime(currentTime.getTime());
      
      LogFactory.getLog(Application.class).info("terminal id befor saving clockIn=" + terminal.getId());
      
      EmployeeInOutHistory attendenceHistory = new EmployeeInOutHistory();
      attendenceHistory.setOutTime(currentTime.getTime());
      attendenceHistory.setOutHour(Short.valueOf((short)currentTime.get(11)));
      attendenceHistory.setClockOut(Boolean.valueOf(true));
      attendenceHistory.setUser(user);
      attendenceHistory.setTerminal(terminal);
      attendenceHistory.setShift(currentShift);
      
      UserDAO.getInstance().saveDriverOut(user, attendenceHistory, currentShift, currentTime);
      
      POSMessageDialog.showMessage("Driver " + user.getFirstName() + " " + user.getLastName() + " is out for delivery.");
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public void performDriverIn(User user) {
    try {
      if (user == null) {
        return;
      }
      if (!user.isClockedIn().booleanValue()) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("ClockInOutAction.2"));
        return;
      }
      
      EmployeeInOutHistoryDAO attendenceHistoryDAO = new EmployeeInOutHistoryDAO();
      EmployeeInOutHistory attendenceHistory = attendenceHistoryDAO.findDriverHistoryByClockedInTime(user);
      if (attendenceHistory == null) {
        attendenceHistory = new EmployeeInOutHistory();
        Date lastClockOutTime = user.getLastClockOutTime();
        Calendar c = Calendar.getInstance();
        c.setTime(lastClockOutTime);
        attendenceHistory.setOutTime(lastClockOutTime);
        attendenceHistory.setOutHour(Short.valueOf((short)c.get(10)));
        attendenceHistory.setUser(user);
        attendenceHistory.setTerminal(Application.getInstance().getTerminal());
        attendenceHistory.setShift(user.getCurrentShift());
      }
      
      Shift shift = user.getCurrentShift();
      Calendar calendar = Calendar.getInstance();
      
      user.setAvailableForDelivery(Boolean.valueOf(true));
      
      attendenceHistory.setClockOut(Boolean.valueOf(false));
      attendenceHistory.setInTime(calendar.getTime());
      attendenceHistory.setInHour(Short.valueOf((short)calendar.get(11)));
      
      UserDAO.getInstance().saveDriverIn(user, attendenceHistory, shift, calendar);
      
      POSMessageDialog.showMessage("Driver " + user.getFirstName() + " " + user.getLastName() + " is in after delivery");
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
}
