package com.floreantpos.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.PosException;
import com.floreantpos.model.Discount;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketDiscount;
import com.floreantpos.model.dao.DiscountDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.util.NumberUtil;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.jdesktop.layout.GroupLayout;
import org.jdesktop.layout.GroupLayout.ParallelGroup;
import org.jdesktop.layout.GroupLayout.SequentialGroup;

public class CouponAndDiscountDialog extends POSDialog implements ActionListener, ListSelectionListener
{
  private List<Discount> couponList;
  private TicketDiscount ticketCoupon;
  private Ticket ticket;
  private PosButton btnCancel;
  private PosButton btnDown;
  private JButton btnEditValue;
  private PosButton btnOk;
  private PosButton btnUp;
  private JLabel jLabel1;
  private JLabel jLabel2;
  private JLabel jLabel3;
  private JLabel jLabel4;
  private JLabel jLabel5;
  private JScrollPane jScrollPane1;
  private JSeparator jSeparator1;
  private JSeparator jSeparator2;
  private JSeparator jSeparator3;
  private JLabel lblTotalDiscount;
  private JList listCoupons;
  private JTextField tfName;
  private JTextField tfNumber;
  private JTextField tfType;
  private JTextField tfValue;
  private TitlePanel titlePanel1;
  
  public CouponAndDiscountDialog()
  {
    initComponents();
    
    tfValue.getDocument().addDocumentListener(new DocumentListener()
    {
      public void insertUpdate(DocumentEvent e) {
        try {
          double totalDiscount = Double.parseDouble(tfValue.getText());
          lblTotalDiscount.setText(NumberUtil.formatNumber(Double.valueOf(totalDiscount)));
        }
        catch (Exception localException) {}
      }
      
      public void removeUpdate(DocumentEvent e) {
        try {
          double totalDiscount = Double.parseDouble(tfValue.getText());
          lblTotalDiscount.setText(NumberUtil.formatNumber(Double.valueOf(totalDiscount)));
        }
        catch (Exception localException) {}
      }
      
      public void changedUpdate(DocumentEvent e) {
        try {
          double totalDiscount = Double.parseDouble(tfValue.getText());
          lblTotalDiscount.setText(NumberUtil.formatNumber(Double.valueOf(totalDiscount)));

        }
        catch (Exception localException) {}
      }
    });
    lblTotalDiscount.setText("");
    btnEditValue.setEnabled(false);
    
    btnUp.setActionCommand("scrollUP");
    btnDown.setActionCommand("scrollDown");
    btnUp.addActionListener(this);
    btnDown.addActionListener(this);
    listCoupons.addListSelectionListener(this);
    
    listCoupons.setCellRenderer(new CouponListRenderer());
    
    ticketCoupon = new TicketDiscount();
  }
  






  private void initComponents()
  {
    titlePanel1 = new TitlePanel();
    jScrollPane1 = new JScrollPane();
    listCoupons = new JList();
    btnCancel = new PosButton();
    btnOk = new PosButton();
    jSeparator1 = new JSeparator();
    btnUp = new PosButton();
    btnDown = new PosButton();
    jSeparator2 = new JSeparator();
    jLabel1 = new JLabel();
    jLabel2 = new JLabel();
    jLabel3 = new JLabel();
    jLabel4 = new JLabel();
    tfName = new JTextField();
    tfNumber = new JTextField();
    tfType = new JTextField();
    tfValue = new JTextField();
    btnEditValue = new JButton();
    jSeparator3 = new JSeparator();
    jLabel5 = new JLabel();
    lblTotalDiscount = new JLabel();
    
    setDefaultCloseOperation(2);
    
    titlePanel1.setTitle(Messages.getString("CouponAndDiscountDialog.3"));
    
    jScrollPane1.setViewportView(listCoupons);
    
    btnCancel.setIcon(IconFactory.getIcon("/ui_icons/", "cancel.png"));
    btnCancel.setText(Messages.getString("CouponAndDiscountDialog.6"));
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        CouponAndDiscountDialog.this.doCancel(evt);
      }
      
    });
    btnOk.setIcon(IconFactory.getIcon("/ui_icons/", "finish.png"));
    btnOk.setText(Messages.getString("CouponAndDiscountDialog.9"));
    btnOk.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        CouponAndDiscountDialog.this.doOk(evt);
      }
      
    });
    btnUp.setIcon(IconFactory.getIcon("/ui_icons/", "up.png"));
    
    btnDown.setIcon(IconFactory.getIcon("/ui_icons/", "down.png"));
    
    jSeparator2.setOrientation(1);
    
    jLabel1.setText(Messages.getString("CouponAndDiscountDialog.14") + ":");
    
    jLabel2.setText(Messages.getString("CouponAndDiscountDialog.16") + ":");
    
    jLabel3.setText(Messages.getString("CouponAndDiscountDialog.18") + ":");
    
    jLabel4.setText(Messages.getString("CouponAndDiscountDialog.20") + ":");
    
    tfName.setEditable(false);
    
    tfNumber.setEditable(false);
    
    tfType.setEditable(false);
    
    tfValue.setEditable(false);
    
    btnEditValue.setText(Messages.getString("CouponAndDiscountDialog.22"));
    btnEditValue.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        CouponAndDiscountDialog.this.doEnterValue(evt);
      }
      
    });
    jLabel5.setFont(new Font("Tahoma", 1, 18));
    jLabel5.setHorizontalAlignment(0);
    jLabel5.setText(Messages.getString("CouponAndDiscountDialog.24"));
    
    lblTotalDiscount.setFont(new Font("Tahoma", 1, 18));
    lblTotalDiscount.setForeground(new java.awt.Color(204, 51, 0));
    lblTotalDiscount.setHorizontalAlignment(0);
    
    GroupLayout layout = new GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(1).add(layout
      .createSequentialGroup()
      .addContainerGap()
      .add(layout
      .createParallelGroup(1)
      .add(jSeparator1, -1, 642, 32767)
      .add(titlePanel1, -1, 642, 32767)
      .add(layout
      .createSequentialGroup()
      .add(jScrollPane1, -2, 216, -2)
      .addPreferredGap(0)
      .add(layout
      .createParallelGroup(1)
      .add(btnUp, -2, -1, -2)
      
      .add(btnDown, -2, -1, -2))
      
      .addPreferredGap(0)
      .add(jSeparator2, -2, -1, -2)
      
      .add(layout
      .createParallelGroup(1)
      .add(2, layout
      .createSequentialGroup()
      .addPreferredGap(0)
      .add(btnOk, -2, 114, -2)
      
      .addPreferredGap(0)
      .add(btnCancel, -2, 117, -2))
      
      .add(layout
      .createSequentialGroup()
      .add(17, 17, 17)
      .add(layout.createParallelGroup(1).add(jLabel4).add(jLabel3)
      .add(jLabel2).add(jLabel1))
      .addPreferredGap(0)
      .add(layout
      .createParallelGroup(1)
      .add(layout.createSequentialGroup()
      .add(tfValue, -1, 130, 32767)
      .addPreferredGap(0).add(btnEditValue))
      .add(tfType, -1, 225, 32767)
      .add(tfNumber, -1, 225, 32767)
      .add(tfName, -1, 225, 32767)))
      .add(layout
      .createSequentialGroup()
      .add(18, 18, 18)
      .add(layout.createParallelGroup(1)
      .add(jLabel5, -1, 354, 32767)
      .add(jSeparator3, -1, 354, 32767)
      .add(lblTotalDiscount, -1, 354, 32767))))))
      .addContainerGap()));
    layout.setVerticalGroup(layout.createParallelGroup(1).add(layout
      .createSequentialGroup()
      .addContainerGap()
      .add(titlePanel1, -2, -1, -2)
      
      .addPreferredGap(0)
      .add(layout
      .createParallelGroup(1)
      .add(jScrollPane1, -1, 323, 32767)
      .add(jSeparator2, -1, 323, 32767)
      .add(layout
      .createSequentialGroup()
      .add(layout
      .createParallelGroup(3)
      .add(jLabel1)
      .add(tfName, -2, -1, -2))
      
      .addPreferredGap(0)
      .add(layout
      .createParallelGroup(3)
      .add(jLabel2)
      .add(tfNumber, -2, -1, -2))
      
      .addPreferredGap(0)
      .add(layout
      .createParallelGroup(3)
      .add(jLabel3)
      .add(tfType, -2, -1, -2))
      
      .addPreferredGap(0)
      .add(layout
      .createParallelGroup(3)
      .add(jLabel4)
      .add(btnEditValue)
      .add(tfValue, -2, -1, -2))
      
      .addPreferredGap(0)
      .add(jSeparator3, -2, 10, -2)
      .add(layout
      .createParallelGroup(1)
      .add(layout
      .createSequentialGroup()
      .add(5, 5, 5)
      .add(btnUp, -2, -1, -2)
      
      .addPreferredGap(0)
      .add(btnDown, -2, -1, -2))
      
      .add(layout.createSequentialGroup().addPreferredGap(0).add(jLabel5)
      .addPreferredGap(0).add(lblTotalDiscount)))))
      .addPreferredGap(0)
      .add(jSeparator1, -2, 10, -2)
      .addPreferredGap(0)
      .add(layout
      .createParallelGroup(2)
      .add(btnCancel, -2, -1, -2)
      
      .add(btnOk, -2, -1, -2))
      .addContainerGap()));
    
    pack();
  }
  
  public TicketDiscount getSelectedCoupon()
  {
    try {
      double parseDouble = NumberUtil.parse(tfValue.getText()).doubleValue();
      ticketCoupon.setValue(Double.valueOf(parseDouble));
    }
    catch (Exception x) {
      throw new PosException(Messages.getString("CouponAndDiscountDialog.27"));
    }
    return ticketCoupon;
  }
  
  private void doEnterValue(ActionEvent evt) {
    NumberSelectionDialog2 dialog = new NumberSelectionDialog2();
    dialog.setFloatingPoint(true);
    dialog.setTitle(Messages.getString("CouponAndDiscountDialog.28"));
    dialog.pack();
    dialog.open();
    
    if (!dialog.isCanceled()) {
      double value = dialog.getValue();
      tfValue.setText(NumberUtil.formatNumber(Double.valueOf(value)));
    }
  }
  
  private void doOk(ActionEvent evt) {
    try {
      TicketDiscount selectedCoupon = getSelectedCoupon();
      if (selectedCoupon == null) {
        POSMessageDialog.showError(this, Messages.getString("CouponAndDiscountDialog.29"));
        return;
      }
      setCanceled(false);
      dispose();
    } catch (PosException e) {
      POSMessageDialog.showError(this, e.getMessage());
    }
  }
  
  private void doCancel(ActionEvent evt) {
    setCanceled(true);
    dispose();
  }
  
  public void initData() throws Exception {
    DiscountDAO dao = new DiscountDAO();
    couponList = dao.getValidCoupons();
    listCoupons.setModel(new CouponListModel());
  }
  
  public void actionPerformed(ActionEvent e) {
    if ("scrollUP".equals(e.getActionCommand())) {
      if ((couponList == null) || (couponList.size() == 0)) {
        return;
      }
      int selectedRow = listCoupons.getSelectedIndex();
      
      if (selectedRow <= 0) {
        selectedRow = 0;
      }
      else {
        selectedRow--;
      }
      
      listCoupons.setSelectedIndex(selectedRow);
      Rectangle cellRect = listCoupons.getCellBounds(selectedRow, selectedRow);
      listCoupons.scrollRectToVisible(cellRect);
    }
    else if ("scrollDown".equals(e.getActionCommand())) {
      if ((couponList == null) || (couponList.size() == 0)) {
        return;
      }
      int selectedRow = listCoupons.getSelectedIndex();
      
      if (selectedRow < 0) {
        selectedRow = 0;
      }
      else if (selectedRow < couponList.size() - 1)
      {


        selectedRow++;
      }
      
      listCoupons.setSelectedIndex(selectedRow);
      Rectangle cellRect = listCoupons.getCellBounds(selectedRow, selectedRow);
      y += 20;
      listCoupons.scrollRectToVisible(cellRect);
    }
  }
  
  public void updateCouponView(Discount coupon) {
    if (coupon == null) {
      tfName.setText("");
      tfNumber.setText("");
      tfType.setText("");
      tfValue.setText("");
      return;
    }
    
    btnEditValue.setEnabled(false);
    
    tfName.setText(coupon.getName());
    if (coupon.getType().intValue() == 0) {
      btnEditValue.setEnabled(true);
    }
    
    tfNumber.setText(String.valueOf(coupon.getId()));
    tfType.setText(Discount.COUPON_TYPE_NAMES[coupon.getType().intValue()]);
    tfValue.setText(NumberUtil.formatNumber(coupon.getValue()));
    
    double totalDiscount = 0.0D;
    double subtotal = ticket.getSubtotalAmount().doubleValue();
    
    ticketCoupon.setDiscountId(coupon.getId());
    ticketCoupon.setName(coupon.getName());
    ticketCoupon.setType(coupon.getType());
    ticketCoupon.setValue(coupon.getValue());
    
    totalDiscount = ticket.calculateDiscountFromType(ticketCoupon, subtotal);
    ticketCoupon.setValue(Double.valueOf(totalDiscount));
    
    lblTotalDiscount.setText(NumberUtil.formatNumber(Double.valueOf(totalDiscount)));
  }
  
  public void valueChanged(ListSelectionEvent e) {
    Discount coupon = (Discount)listCoupons.getSelectedValue();
    updateCouponView(coupon);
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  

  public void setTicket(Ticket ticket) { this.ticket = ticket; }
  
  class CouponListModel extends javax.swing.AbstractListModel {
    CouponListModel() {}
    
    public int getSize() {
      if (couponList == null) {
        return 0;
      }
      return couponList.size();
    }
    
    public Object getElementAt(int index) {
      return couponList.get(index);
    }
  }
  
  class CouponListRenderer extends DefaultListCellRenderer {
    CouponListRenderer() {}
    
    public java.awt.Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
      Discount coupon = (Discount)value;
      



      return super.getListCellRendererComponent(list, coupon.getName(), index, isSelected, cellHasFocus);
    }
  }
}
