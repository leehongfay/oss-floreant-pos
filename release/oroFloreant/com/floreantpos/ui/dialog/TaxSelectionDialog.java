package com.floreantpos.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.bo.ui.explorer.ExplorerButtonPanel;
import com.floreantpos.model.Tax;
import com.floreantpos.model.TaxGroup;
import com.floreantpos.model.dao.TaxDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.model.TaxForm;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;





















public class TaxSelectionDialog
  extends POSDialog
{
  private JXTable table;
  private BeanTableModel<Tax> tableModel;
  private List<Tax> selectedTaxes;
  private TaxGroup taxGroup;
  
  public TaxSelectionDialog(TaxGroup taxGroup)
  {
    super(POSUtil.getFocusedWindow(), "");
    this.taxGroup = taxGroup;
    init();
    List<Tax> taxList = TaxDAO.getInstance().findAll();
    List<Tax> existingTaxs = null;
    if (taxGroup != null) {
      existingTaxs = taxGroup.getTaxes();
    }
    if (existingTaxs != null) {
      for (Tax tax : taxList) {
        if (existingTaxs.contains(tax)) {
          tax.setEnable(Boolean.valueOf(true));
        }
      }
    }
    tableModel.setRows(taxList);
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
  }
  
  private void init() {
    setLayout(new BorderLayout(5, 5));
    setTitle("Select Tax");
    TitlePanel titelpanel = new TitlePanel();
    titelpanel.setTitle("Tax Group: " + taxGroup.getName());
    
    add(titelpanel, "North");
    tableModel = new BeanTableModel(Tax.class)
    {
      public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 2) {
          Tax tax = (Tax)tableModel.getRow(rowIndex);
          if (tax != null) {
            return NumberUtil.trimDecilamIfNotNeeded(tax.getRate()) + "%";
          }
        }
        return super.getValueAt(rowIndex, columnIndex);
      }
    };
    tableModel.addColumn("", "enable");
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "name");
    tableModel.addColumn("RATE", "rate");
    
    table = new JXTable(tableModel);
    table.setSelectionMode(2);
    table.setTableHeader(null);
    table.setRowHeight(PosUIManager.getSize(30));
    table.setShowGrid(true, false);
    
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          TaxSelectionDialog.this.editSelectedRow();
        }
        else {
          TaxSelectionDialog.this.selectItem();
        }
        
      }
    });
    JPanel contentPanel = new JPanel(new BorderLayout());
    JScrollPane scroll = new JScrollPane(table);
    contentPanel.add(scroll);
    
    add(contentPanel);
    resizeColumnWidth(table);
    add(createButtonPanel(), "South");
  }
  
  private TransparentPanel createButtonPanel() {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton btnEdit = explorerButton.getEditButton();
    JButton btnAdd = explorerButton.getAddButton();
    
    JButton btnOk = new JButton("DONE");
    btnOk.setHorizontalTextPosition(4);
    btnOk.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TaxSelectionDialog.this.doOk();
      }
      
    });
    JButton btnCancel = new JButton(POSConstants.CANCEL);
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setCanceled(true);
        dispose();
      }
    });
    btnAdd.setText("New Tax");
    btnEdit.setText(POSConstants.EDIT);
    
    btnEdit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        TaxSelectionDialog.this.editSelectedRow();
      }
      

    });
    btnAdd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          Tax tax = new Tax();
          TaxForm editor = new TaxForm(tax);
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          Tax item = (Tax)editor.getBean();
          tableModel.addRow(item);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    btnOk.setBackground(Color.GREEN);
    
    TransparentPanel panel = new TransparentPanel(new MigLayout("center,ins 0 0 5 0", "sg,fill", ""));
    int h = PosUIManager.getSize(40);
    panel.add(btnAdd, "h " + h);
    
    panel.add(btnOk, "h " + h);
    panel.add(btnCancel, "h " + h);
    return panel;
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(250));
    columnWidth.add(Integer.valueOf(50));
    
    return columnWidth;
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      Tax tax = (Tax)tableModel.getRow(index);
      tableModel.setRow(index, tax);
      
      TaxForm editor = new TaxForm(tax);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doOk() {
    try {
      selectedTaxes = new ArrayList();
      List<Tax> items = tableModel.getRows();
      for (Iterator iterator = items.iterator(); iterator.hasNext();) {
        Tax tax = (Tax)iterator.next();
        if (tax.isEnable().booleanValue()) {
          selectedTaxes.add(tax);
        }
      }
      setCanceled(false);
      dispose();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public List<Tax> getSelectedTaxList() {
    return selectedTaxes;
  }
  
  private void selectItem() {
    if (table.getSelectedRow() < 0) {
      return;
    }
    int selectedRow = table.getSelectedRow();
    selectedRow = table.convertRowIndexToModel(selectedRow);
    Tax tax = (Tax)tableModel.getRow(selectedRow);
    tax.setEnable(Boolean.valueOf(!tax.isEnable().booleanValue()));
    table.repaint();
  }
}
