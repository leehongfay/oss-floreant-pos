package com.floreantpos.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.model.Department;
import com.floreantpos.model.SalesArea;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.dao.SalesAreaDAO;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.ScrollableFlowPanel;
import com.floreantpos.ui.TitlePanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;































public class SalesAreaSelectionDialog
  extends POSDialog
{
  private ScrollableFlowPanel buttonsPanel;
  private SalesArea addedSalesArea;
  
  public SalesAreaSelectionDialog()
  {
    initializeComponent();
    rendererSalesAreas();
    
    setResizable(true);
  }
  
  private void initializeComponent() {
    setTitle("Select sales area");
    setLayout(new BorderLayout());
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Select sales area");
    add(titlePanel, "North");
    
    JPanel buttonActionPanel = new JPanel(new MigLayout("fill"));
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL.toUpperCase());
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        setCanceled(true);
        dispose();
      }
      
    });
    buttonActionPanel.add(btnCancel, "w 120!,center");
    
    JPanel footerPanel = new JPanel(new BorderLayout());
    footerPanel.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
    footerPanel.add(new JSeparator(), "North");
    footerPanel.add(buttonActionPanel);
    
    add(footerPanel, "South");
    
    buttonsPanel = new ScrollableFlowPanel(3);
    
    JScrollPane scrollPane = new PosScrollPane(buttonsPanel, 20, 31);
    scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(80, 0));
    scrollPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5), scrollPane.getBorder()));
    
    add(scrollPane, "Center");
    
    setSize(800, 600);
  }
  
  private void rendererSalesAreas() {
    try {
      buttonsPanel.getContentPane().removeAll();
      Terminal terminal = Application.getInstance().getTerminal();
      Department dept = terminal.getDepartment();
      SalesArea salesArea = terminal.getSalesArea();
      if (dept != null) {
        List<SalesArea> salesAreas = SalesAreaDAO.getInstance().findSalesAreaByDept(dept);
        
        setSalesAreaButtonPanel(salesArea, salesAreas);
      }
      else if (dept == null) {
        List<SalesArea> salesAreas = SalesAreaDAO.getInstance().findAll();
        
        setSalesAreaButtonPanel(salesArea, salesAreas);
      }
    } catch (PosException e) {
      POSMessageDialog.showError(this, e.getLocalizedMessage(), e);
    }
  }
  
  private void setSalesAreaButtonPanel(SalesArea salesArea, List<SalesArea> salesAreas) {
    ButtonGroup group = new ButtonGroup();
    
    for (SalesArea sArea : salesAreas) {
      SalesAreaButton salesAreaButton = new SalesAreaButton(sArea);
      if ((salesArea != null) && (salesArea.equals(sArea))) {
        salesAreaButton.setFocusable(true);
        salesAreaButton.setFocusPainted(true);
      }
      buttonsPanel.add(salesAreaButton);
      group.add(salesAreaButton);
    }
    buttonsPanel.repaint();
    buttonsPanel.revalidate();
  }
  
  public SalesArea getSelectedSalesArea() {
    return addedSalesArea;
  }
  
  private class SalesAreaButton extends POSToggleButton implements ActionListener {
    private static final int BUTTON_SIZE = 119;
    SalesArea salesArea;
    
    SalesAreaButton(SalesArea salesArea) {
      this.salesArea = salesArea;
      setVerticalTextPosition(3);
      setHorizontalTextPosition(0);
      setText("<html><body><center>" + salesArea.getName() + "</center></body></html>");
      
      setPreferredSize(new Dimension(119, TerminalConfig.getMenuItemButtonHeight()));
      addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e) {
      addedSalesArea = salesArea;
      setCanceled(false);
      dispose();
    }
  }
}
