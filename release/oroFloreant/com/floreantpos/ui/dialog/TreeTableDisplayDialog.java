package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.tree.TreePath;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;

public class TreeTableDisplayDialog extends OkCancelOptionDialog
{
  JXTreeTable tree;
  
  public TreeTableDisplayDialog(JXTreeTable tree)
  {
    this.tree = tree;
    initComponents();
  }
  
  private void initComponents() {
    setDefaultCloseOperation(2);
    
    setCaption(Messages.getString("TreeTableDisplayDialog.0"));
    setTitle(Messages.getString("TreeTableDisplayDialog.1"));
    
    getContentPanel().add(new JScrollPane(tree));
    
    pack();
  }
  
  public void doOk() {
    setCanceled(false);
    dispose();
  }
  
  public DefaultMutableTreeTableNode getPath() {
    int index = tree.getSelectedRow();
    
    TreePath treePath = tree.getPathForRow(index);
    return (DefaultMutableTreeTableNode)treePath.getLastPathComponent();
  }
}
