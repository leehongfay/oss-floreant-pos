package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.CashBreakdown;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.Currency;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.NumericKeypad;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.util.POSUtil;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;






















public class MultiCurrencyAmountSelectionDialog
  extends OkCancelOptionDialog
{
  private List<Currency> currencyList;
  private double totalAmount;
  private List<CurrencyRow> currencyRows = new ArrayList();
  private CashDrawer cashDrawer;
  private boolean reconcile;
  
  public MultiCurrencyAmountSelectionDialog(CashDrawer cashDrawer, double initialAmount, List<Currency> currencyList) {
    super(POSUtil.getFocusedWindow());
    this.currencyList = currencyList;
    this.cashDrawer = cashDrawer;
    init();
  }
  
  public void setReconcile(boolean reconcile) {
    this.reconcile = reconcile;
  }
  
  private void init() {
    JPanel contentPane = getContentPanel();
    setOkButtonText(POSConstants.SAVE_BUTTON_TEXT);
    setTitle(Messages.getString("MultiCurrencyAssignDrawerDialog.0"));
    setCaption(Messages.getString("MultiCurrencyAssignDrawerDialog.1"));
    
    MigLayout layout = new MigLayout("inset 0", "[grow,fill]", "[grow,fill]");
    contentPane.setLayout(layout);
    
    JPanel inputPanel = new JPanel();
    GridLayout gridLayout = new GridLayout(0, 2, 10, 5);
    inputPanel.setLayout(gridLayout);
    
    JLabel lblCurrency = getJLabel(Messages.getString("MultiCurrencyAssignDrawerDialog.2"), 1, 16, 0);
    JLabel lblAmount = getJLabel(Messages.getString("MultiCurrencyAssignDrawerDialog.3"), 1, 16, 0);
    
    inputPanel.add(lblCurrency);
    inputPanel.add(lblAmount);
    
    for (Currency currency : currencyList) {
      JLabel currencyName = getJLabel(currency.getName(), 0, 16, 0);
      DoubleTextField tfTenderedAmount = getDoubleTextField("", 0, 16, 4);
      
      inputPanel.add(currencyName);
      inputPanel.add(tfTenderedAmount);
      
      CurrencyRow item = new CurrencyRow(currency, tfTenderedAmount);
      currencyRows.add(item);
    }
    contentPane.add(inputPanel, "cell 0 0,alignx left,aligny top");
    
    NumericKeypad numericKeypad = new NumericKeypad();
    contentPane.add(new JSeparator(), "gapbottom 5,gaptop 5,cell 0 1");
    contentPane.add(numericKeypad, "cell 0 2");
  }
  
  private JLabel getJLabel(String text, int bold, int fontSize, int align) {
    JLabel lbl = new JLabel(text);
    lbl.setFont(lbl.getFont().deriveFont(bold, PosUIManager.getSize(fontSize)));
    lbl.setHorizontalAlignment(align);
    return lbl;
  }
  
  private DoubleTextField getDoubleTextField(String text, int bold, int fontSize, int align) {
    DoubleTextField tf = new DoubleTextField();
    tf.setText(text);
    tf.setFont(tf.getFont().deriveFont(bold, PosUIManager.getSize(fontSize)));
    tf.setHorizontalAlignment(align);
    tf.setBackground(Color.WHITE);
    return tf;
  }
  
  private class CurrencyRow {
    Currency currency;
    DoubleTextField tfAmount;
    
    public CurrencyRow(Currency currency, DoubleTextField tfAmount) {
      this.currency = currency;
      this.tfAmount = tfAmount;
    }
  }
  
  public void doOk()
  {
    totalAmount = 0.0D;
    for (CurrencyRow rowItem : currencyRows) {
      CashBreakdown item = cashDrawer.getCurrencyBalance(currency);
      if (item == null) {
        item = new CashBreakdown();
        item.setCurrency(currency);
        cashDrawer.addTocashBreakdownList(item);
      }
      double amount = tfAmount.getDouble();
      if (Double.isNaN(amount)) {
        amount = 0.0D;
      }
      if (!reconcile) {
        item.setBalance(Double.valueOf(amount));
      } else {
        item.setTotalAmount(Double.valueOf(amount));
      }
      totalAmount += amount / currency.getExchangeRate().doubleValue();
    }
    
    if ((reconcile) && (totalAmount < cashDrawer.getDrawerAccountable().doubleValue()) && 
      (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Do you want to save partial reconciliation?", "Confirm") != 0)) {
      return;
    }
    
    setCanceled(false);
    dispose();
  }
  
  public double getTotalAmount() {
    return totalAmount;
  }
}
