package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;



















public class MultipleNumberSelectionDialog
  extends POSDialog
  implements ActionListener
{
  private TitlePanel titlePanel;
  private List<Integer> numbers = new ArrayList();
  private List<Integer> selectedNumbers = new ArrayList();
  private PosButton btnOk;
  
  public MultipleNumberSelectionDialog(List<Integer> numbers)
  {
    this.numbers = numbers;
    init();
  }
  
  private void init() {
    setTitle("SELECT SPLIT TICKETS");
    setLayout(new BorderLayout());
    
    MigLayout layout = new MigLayout("fillx,wrap 3,inset 10", "sg fill", "");
    JPanel contentPane = new JPanel();
    contentPane.setLayout(layout);
    
    titlePanel = new TitlePanel();
    titlePanel.setTitle("Select tickets to share item.");
    add(titlePanel, "North");
    
    int h = PosUIManager.getSize(70);
    for (Integer seat : numbers) {
      POSToggleButton posButton = new POSToggleButton();
      posButton.setActionCommand("" + seat);
      posButton.setFocusable(false);
      posButton.setFont(posButton.getFont().deriveFont(1, 24.0F));
      posButton.setText(String.valueOf(seat));
      if ((!selectedNumbers.isEmpty()) && (selectedNumbers.contains(seat))) {
        posButton.setBackground(Color.GREEN);
      }
      posButton.addActionListener(this);
      String constraints = "grow, height " + h;
      contentPane.add(posButton, constraints);
    }
    add(contentPane);
    JPanel buttonPanel = new JPanel(new MigLayout("fill", "sg fill"));
    btnOk = new PosButton("DONE");
    btnOk.setFocusable(false);
    btnOk.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (selectedNumbers.isEmpty()) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select splitted tickets no.");
          return;
        }
        setCanceled(false);
        dispose();
      }
    });
    buttonPanel.add(new JSeparator(), "span,grow,gapbottom 3,wrap");
    buttonPanel.add(btnOk, "grow,height " + h);
    
    PosButton btnCancel = new PosButton(Messages.getString("SeatSelectionDialog.2"));
    btnCancel.setFocusable(false);
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setCanceled(true);
        dispose();
      }
    });
    buttonPanel.add(btnCancel, "grow, height " + h);
    add(buttonPanel, "South");
  }
  
  public void actionPerformed(ActionEvent e) {
    POSToggleButton btnNumber = (POSToggleButton)e.getSource();
    Integer command = null;
    try {
      command = Integer.valueOf(e.getActionCommand());
    } catch (Exception ex) {
      return;
    }
    if ((command != null) && (!selectedNumbers.contains(command))) {
      selectedNumbers.add(command);
      btnNumber.setBackground(Color.GREEN);
    }
    else {
      selectedNumbers.remove(command);
      btnNumber.setBackground(btnOk.getBackground());
    }
  }
  
  public List<Integer> getViewNumbers() {
    return selectedNumbers;
  }
  
  public void setDialogTitle(String title) {
    super.setTitle(title);
  }
}
