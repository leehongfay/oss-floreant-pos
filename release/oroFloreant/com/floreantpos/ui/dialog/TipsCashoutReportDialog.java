package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.model.StoreSession;
import com.floreantpos.model.StoreSessionControl;
import com.floreantpos.model.TipsCashoutReport;
import com.floreantpos.model.TipsCashoutReportData;
import com.floreantpos.model.TipsCashoutReportTableModel;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.GratuityDAO;
import com.floreantpos.print.PosPrintService;
import com.floreantpos.swing.ListComboBoxModel;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.ui.views.TicketReceiptView;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.StoreUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JTable;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JasperPrint;
import org.jdesktop.swingx.JXDatePicker;


















public class TipsCashoutReportDialog
  extends OkCancelOptionDialog
  implements ActionListener
{
  private TipsCashoutReport report;
  private JPanel reportPanel;
  private JXDatePicker fromDatePicker;
  private JXDatePicker toDatePicker;
  private User user;
  private JComboBox cbSessionReport = new JComboBox();
  private JComboBox cbUserList = new JComboBox();
  
  public TipsCashoutReportDialog(TipsCashoutReport report) {
    this(report, null, null, false);
  }
  
  public TipsCashoutReportDialog(TipsCashoutReport tipsReport, ListComboBoxModel userListModel, User selectedUser, boolean showFilterPanel) {
    super(POSUtil.getFocusedWindow());
    setTitle(POSConstants.SERVER_TIPS_REPORT);
    setOkButtonText("PRINT");
    
    report = tipsReport;
    user = selectedUser;
    
    JPanel contentPanel = getContentPanel();
    
    if (showFilterPanel) {
      JPanel filterPanel = new JPanel(new MigLayout("ins 10"));
      fromDatePicker = UiUtil.getCurrentMonthStart();
      toDatePicker = UiUtil.getCurrentMonthEnd();
      
      StoreSession currentData = StoreUtil.getCurrentStoreOperation().getCurrentData();
      fromDatePicker.setDate(currentData.getOpenTime());
      
      cbUserList.setModel(userListModel);
      if (selectedUser == null) {
        cbUserList.setSelectedIndex(0);
      }
      else {
        cbUserList.setSelectedItem(selectedUser);
      }
      cbSessionReport.addItem("All Session");
      cbSessionReport.addItem("Current Session");
      
      cbSessionReport.setSelectedIndex(0);
      cbSessionReport.addItemListener(new ItemListener()
      {
        public void itemStateChanged(ItemEvent e)
        {
          fromDatePicker.setEnabled(cbSessionReport.getSelectedIndex() == 0);
          toDatePicker.setEnabled(cbSessionReport.getSelectedIndex() == 0);
        }
      });
      filterPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 10, 0, 10), BorderFactory.createTitledBorder("")));
      filterPanel.add(new JLabel("User"));
      filterPanel.add(cbUserList);
      filterPanel.add(cbSessionReport);
      filterPanel.add(new JLabel(POSConstants.FROM + ":"));
      filterPanel.add(fromDatePicker);
      filterPanel.add(new JLabel(POSConstants.TO_));
      filterPanel.add(toDatePicker);
      
      JButton btnGo = new JButton("GO");
      btnGo.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          reportPanel.removeAll();
          GratuityDAO gratuityDAO = new GratuityDAO();
          int selectedIndex = cbSessionReport.getSelectedIndex();
          Object selectedItem = cbUserList.getSelectedItem();
          if ((selectedItem instanceof User)) {
            user = ((User)selectedItem);
          }
          else {
            user = null;
          }
          report = gratuityDAO.createReport(fromDatePicker.getDate(), toDatePicker.getDate(), user, selectedIndex == 1);
          TipsCashoutReportDialog.this.createReport();
          reportPanel.revalidate();
          reportPanel.repaint();
        }
      });
      filterPanel.add(btnGo);
      add(filterPanel, "North");
      GratuityDAO gratuityDAO = new GratuityDAO();
      report = gratuityDAO.createReport(fromDatePicker.getDate(), toDatePicker.getDate(), user, false);
    }
    
    reportPanel = new JPanel(new MigLayout("wrap 1, ax 50%", "", ""));
    






















    TipsCashoutReportTableModel model = new TipsCashoutReportTableModel();
    if (report != null) {
      List<TipsCashoutReportData> datas = report.getDatas();
      model.setRows(datas);
    }
    JTable table = new JTable(model);
    table.setDefaultRenderer(Object.class, new PosTableRenderer());
    

    PosScrollPane scrollPane = new PosScrollPane(reportPanel);
    scrollPane.getVerticalScrollBar().setUnitIncrement(20);
    
    createReport();
    
    contentPanel.add(scrollPane);
  }
  
  private void createReport() {
    try {
      if (report == null)
        return;
      JasperPrint jasperPrint = PosPrintService.createServerTipsReport(report);
      TicketReceiptView receiptView = new TicketReceiptView(jasperPrint);
      reportPanel.add(receiptView.getReportPanel());
    } catch (Exception x) {
      PosLog.error(getClass(), x);
      POSMessageDialog.showError(this, Messages.getString("TipsCashoutReportDialog.32") + x.getMessage());
    }
  }
  
  public void actionPerformed(ActionEvent e) {}
  
  public void doOk()
  {
    try
    {
      PosPrintService.printServerTipsReport(report);
    } catch (Exception x) {
      PosLog.error(getClass(), x);
      POSMessageDialog.showError(this, Messages.getString("TipsCashoutReportDialog.32") + x.getMessage());
    }
  }
}
