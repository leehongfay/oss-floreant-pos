package com.floreantpos.ui.dialog;

import com.floreantpos.main.Application;
import com.floreantpos.model.CashBreakdown;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.Currency;
import com.floreantpos.model.Terminal;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.POSUtil;
import com.jidesoft.swing.TitledSeparator;
import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import net.miginfocom.swing.MigLayout;







public class CashReconciliationDialog
  extends OkCancelOptionDialog
{
  private static MyOwnFocusTraversalPolicy newPolicy;
  private IntegerTextField tfFiveThousands;
  private IntegerTextField tfTwoThousands;
  private IntegerTextField tfThousand;
  private IntegerTextField tfFiveHundreds;
  private IntegerTextField tfHundreds;
  private IntegerTextField tfFifties;
  private IntegerTextField tfTwenties;
  private IntegerTextField tfTens;
  private IntegerTextField tfFives;
  private IntegerTextField tfTwos;
  private IntegerTextField tfOnes;
  private IntegerTextField tf$_1_0_Coins;
  private IntegerTextField tf$0_50_Coins;
  private IntegerTextField tf$_0_25_Coins;
  private IntegerTextField tf$_0_15_Coins;
  private IntegerTextField tf$_0_10_Coins;
  private IntegerTextField tf$_0_05_Coins;
  private IntegerTextField tf$_0_01_Coins;
  private DoubleTextField tfTotalCash;
  private JLabel lblTotalCash;
  private CashDrawer cashDrawer;
  private CashBreakdown selectedCashBreakdown;
  private Map<String, CashBreakdown> itemMap = new HashMap();
  private Map<String, CurrencySummaryRow> summaryMap = new HashMap();
  private Currency selectedCurrency;
  private double totalReconcileAmount;
  
  public CashReconciliationDialog(CashDrawer report)
  {
    super(POSUtil.getFocusedWindow(), "Cash Reconciliation Dialog");
    setCaption("Cash Reconciliation");
    cashDrawer = report;
    List<CashBreakdown> cashBreakdowns = cashDrawer.getCashBreakdownList();
    if (cashBreakdowns != null) {
      for (CashBreakdown curBalance : cashBreakdowns) {
        itemMap.put(curBalance.getCurrency().getName(), curBalance);
      }
    }
    initComponent();
    callOrderController();
    createComponents();
    if (selectedCurrency != null) {
      updateView();
    }
  }
  
  public void createComponents() {
    JPanel container = new JPanel(new MigLayout("fillx,hidemode 3,inset 0,wrap 1"));
    
    createTopCurrencyButtonPanel();
    
    if (Application.getInstance().getTerminal().isEnableMultiCurrency().booleanValue()) {
      JLabel lblTitle = new JLabel("CASH RECONCIALIATION");
      lblTitle.setForeground(UIManager.getColor("TitledBorder.titleColor"));
      lblTitle.setFont(new Font(lblTitle.getFont().getName(), 1, 14));
      TitledSeparator sep = new TitledSeparator(lblTitle, 0);
      container.add(sep, "grow,span");
    }
    
    JLabel lblFiveThousands = new JLabel("5000's");
    JLabel lblTwoThousands = new JLabel("2000's");
    JLabel lblThousands = new JLabel("1000's");
    JLabel lblFiveHundreds = new JLabel("500's");
    JLabel lblHundreds = new JLabel("100's");
    JLabel lblFifties = new JLabel("50's");
    JLabel lblTwenties = new JLabel("20's");
    JLabel lblTens = new JLabel("10's");
    JLabel lblFives = new JLabel("5's");
    JLabel lblTwos = new JLabel("2's");
    JLabel lblOnes = new JLabel("1's");
    JLabel lbl$_1_0_Coins = new JLabel("$1.0 COIN");
    JLabel lbl$0_50_Coins = new JLabel(".50 COIN");
    JLabel lbl$_0_25_Coins = new JLabel(".25 COIN");
    JLabel lbl$_0_15_Coins = new JLabel(".15 COIN");
    JLabel lbl$_0_10_Coins = new JLabel(".10 COIN");
    JLabel lbl$_0_05_Coins = new JLabel(".05 COIN");
    JLabel lbl$_0_01_Coins = new JLabel(".01 COIN");
    
    int col = 0;
    int row = 0;
    JPanel numberInputPanel = new JPanel(new MigLayout("fill", "[][]20px[][]", ""));
    numberInputPanel.add(lblFiveThousands, "grow,cell " + col + " " + row++);
    numberInputPanel.add(lblTwoThousands, "grow,cell " + col + " " + row++);
    numberInputPanel.add(lblThousands, "grow,cell " + col + " " + row++);
    numberInputPanel.add(lblFiveHundreds, "grow,cell " + col + " " + row++);
    numberInputPanel.add(lblHundreds, "grow,cell " + col + " " + row++);
    numberInputPanel.add(lblFifties, "grow,cell " + col + " " + row++);
    numberInputPanel.add(lblTwenties, "grow,cell " + col + " " + row++);
    numberInputPanel.add(lblTens, "grow,cell " + col + " " + row++);
    numberInputPanel.add(lblFives, "grow,cell " + col + " " + row++);
    
    col++;
    row = 0;
    numberInputPanel.add(tfFiveThousands, "grow,cell " + col + " " + row++);
    numberInputPanel.add(tfTwoThousands, "grow,cell " + col + " " + row++);
    numberInputPanel.add(tfThousand, "grow,cell " + col + " " + row++);
    numberInputPanel.add(tfFiveHundreds, "grow,cell " + col + " " + row++);
    numberInputPanel.add(tfHundreds, "grow,cell " + col + " " + row++);
    numberInputPanel.add(tfFifties, "grow,cell " + col + " " + row++);
    numberInputPanel.add(tfTwenties, "grow,cell " + col + " " + row++);
    numberInputPanel.add(tfTens, "grow,cell " + col + " " + row++);
    numberInputPanel.add(tfFives, "grow,cell " + col + " " + row++);
    col++;
    row = 0;
    
    numberInputPanel.add(lblTwos, "grow,cell " + col + " " + row++);
    numberInputPanel.add(lblOnes, "grow,cell " + col + " " + row++);
    
    numberInputPanel.add(lbl$_1_0_Coins, "right,cell " + col + " " + row++);
    numberInputPanel.add(lbl$0_50_Coins, "right,cell " + col + " " + row++);
    numberInputPanel.add(lbl$_0_25_Coins, "right,cell " + col + " " + row++);
    numberInputPanel.add(lbl$_0_15_Coins, "right,cell " + col + " " + row++);
    numberInputPanel.add(lbl$_0_10_Coins, "right,cell " + col + " " + row++);
    numberInputPanel.add(lbl$_0_05_Coins, "right,cell " + col + " " + row++);
    numberInputPanel.add(lbl$_0_01_Coins, "right,cell " + col + " " + row++);
    col++;
    row = 0;
    
    numberInputPanel.add(tfTwos, "grow,cell " + col + " " + row++);
    numberInputPanel.add(tfOnes, "grow,cell " + col + " " + row++);
    
    numberInputPanel.add(tf$_1_0_Coins, "grow,cell " + col + " " + row++);
    numberInputPanel.add(tf$0_50_Coins, "grow,cell " + col + " " + row++);
    numberInputPanel.add(tf$_0_25_Coins, "grow,cell " + col + " " + row++);
    numberInputPanel.add(tf$_0_15_Coins, "grow,cell " + col + " " + row++);
    numberInputPanel.add(tf$_0_10_Coins, "grow,cell " + col + " " + row++);
    numberInputPanel.add(tf$_0_05_Coins, "grow,cell " + col + " " + row++);
    numberInputPanel.add(tf$_0_01_Coins, "grow,cell " + col + " " + row++);
    
    container.add(numberInputPanel, "grow");
    
    addCurrencySummaryPanel(container);
    
    getContentPanel().add(container);
  }
  
  private void createTopCurrencyButtonPanel() {
    JPanel topPanel = new JPanel(new MigLayout("center,ins 10 0 0 0", "sg,fill", ""));
    ButtonGroup group = new ButtonGroup();
    Terminal terminal = Application.getInstance().getTerminal();
    if (terminal.isEnableMultiCurrency().booleanValue()) {
      for (Currency currency : CurrencyUtil.getAllCurrency()) {
        POSToggleButton btnCurrency = new POSToggleButton(currency.getName());
        btnCurrency.putClientProperty("currency", currency);
        btnCurrency.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            CashReconciliationDialog.this.updateModel();
            selectedCurrency = ((Currency)((POSToggleButton)e.getSource()).getClientProperty("currency"));
            CashReconciliationDialog.this.updateView();
          }
        });
        if (currency.isMain().booleanValue()) {
          btnCurrency.setSelected(true);
          selectedCurrency = currency;
        }
        topPanel.add(btnCurrency);
        group.add(btnCurrency);
      }
      add(topPanel, "North");
    }
    else {
      TitlePanel titlePanel = new TitlePanel();
      titlePanel.setTitle("Cash reconciliation");
      add(titlePanel, "North");
      selectedCurrency = CurrencyUtil.getMainCurrency();
      updateView();
    }
  }
  
  private void addCurrencySummaryPanel(JPanel container) {
    JPanel calculatePanel = new JPanel(new MigLayout("fill,right,ins 0,wrap 2", "30px[60px,grow,right][60px,grow,fill,right]", ""));
    
    Terminal terminal = Application.getInstance().getTerminal();
    if (terminal.isEnableMultiCurrency().booleanValue()) {
      for (Currency currency : CurrencyUtil.getAllCurrency()) {
        CurrencySummaryRow item = new CurrencySummaryRow(calculatePanel, currency);
        summaryMap.put(currency.getName(), item);
      }
    }
    else {
      Currency mainCurrency = CurrencyUtil.getMainCurrency();
      CurrencySummaryRow item = new CurrencySummaryRow(calculatePanel, mainCurrency);
      summaryMap.put(mainCurrency.getName(), item);
    }
    
    JLabel lblTitle2 = new JLabel(terminal.isEnableMultiCurrency().booleanValue() ? "CURRENCY BREAKDOWN" : "CASH TO DEPOSIT");
    lblTitle2.setForeground(UIManager.getColor("TitledBorder.titleColor"));
    lblTitle2.setFont(new Font(lblTitle2.getFont().getName(), 1, 14));
    TitledSeparator sep2 = new TitledSeparator(lblTitle2, 0);
    container.add(sep2, "grow,span");
    
    container.add(calculatePanel, "grow");
  }
  
  public void initComponent() {
    tfFiveThousands = new IntegerTextField(12);
    tfTwoThousands = new IntegerTextField(12);
    tfThousand = new IntegerTextField(12);
    tfFiveHundreds = new IntegerTextField(12);
    tfHundreds = new IntegerTextField(12);
    tfFifties = new IntegerTextField(12);
    tfTwenties = new IntegerTextField(12);
    tfTens = new IntegerTextField(12);
    tfFives = new IntegerTextField(12);
    tfTwos = new IntegerTextField(12);
    tfOnes = new IntegerTextField(12);
    tf$_1_0_Coins = new IntegerTextField(12);
    tf$0_50_Coins = new IntegerTextField(12);
    tf$_0_25_Coins = new IntegerTextField(12);
    tf$_0_15_Coins = new IntegerTextField(12);
    tf$_0_10_Coins = new IntegerTextField(12);
    tf$_0_05_Coins = new IntegerTextField(12);
    tf$_0_01_Coins = new IntegerTextField(12);
    tfTotalCash = new DoubleTextField(12);
    
    FocusListener listener = new FocusListener()
    {
      public void focusLost(FocusEvent e)
      {
        CashReconciliationDialog.this.calculateTotal();
      }
      



      public void focusGained(FocusEvent e) {}
    };
    tfFiveThousands.addFocusListener(listener);
    tfTwoThousands.addFocusListener(listener);
    tfThousand.addFocusListener(listener);
    tfFiveHundreds.addFocusListener(listener);
    tfHundreds.addFocusListener(listener);
    tfFifties.addFocusListener(listener);
    tfTwenties.addFocusListener(listener);
    tfTens.addFocusListener(listener);
    tfFives.addFocusListener(listener);
    tfTwos.addFocusListener(listener);
    tfOnes.addFocusListener(listener);
    tf$_1_0_Coins.addFocusListener(listener);
    tf$0_50_Coins.addFocusListener(listener);
    tf$_0_25_Coins.addFocusListener(listener);
    tf$_0_15_Coins.addFocusListener(listener);
    tf$_0_10_Coins.addFocusListener(listener);
    tf$_0_05_Coins.addFocusListener(listener);
    tf$_0_01_Coins.addFocusListener(listener);
    
    lblTotalCash = new JLabel("TOTAL CASH");
    
    Font f = new Font(lblTotalCash.getFont().getName(), 1, 16);
    lblTotalCash.setFont(f);
    tfTotalCash.setFont(f);
    
    tfTotalCash.setHorizontalAlignment(4);
  }
  
  private void updateView() {
    selectedCashBreakdown = ((CashBreakdown)itemMap.get(selectedCurrency.getName()));
    if (selectedCashBreakdown == null) {
      selectedCashBreakdown = new CashBreakdown();
      selectedCashBreakdown.setCurrency(selectedCurrency);
      selectedCashBreakdown.setCashDrawer(cashDrawer);
      itemMap.put(selectedCurrency.getName(), selectedCashBreakdown);
    }
    tfFiveThousands.setText(String.valueOf(selectedCashBreakdown.getNoOfFiveThousands()));
    tfTwoThousands.setText(String.valueOf(selectedCashBreakdown.getNoOfTwoThousands()));
    tfThousand.setText(String.valueOf(selectedCashBreakdown.getNoOfThousands()));
    tfFiveHundreds.setText(String.valueOf(selectedCashBreakdown.getNoOfFiveHundreds()));
    
    tfHundreds.setText(String.valueOf(selectedCashBreakdown.getNoOfHundreds()));
    tfFifties.setText(String.valueOf(selectedCashBreakdown.getNoOfFifties()));
    tfTwenties.setText(String.valueOf(selectedCashBreakdown.getNoOfTwenties()));
    tfTens.setText(String.valueOf(selectedCashBreakdown.getNoOfTens()));
    tfFives.setText(String.valueOf(selectedCashBreakdown.getNoOfFives()));
    tfTwos.setText(String.valueOf(selectedCashBreakdown.getNoOfTwos()));
    tfOnes.setText(String.valueOf(selectedCashBreakdown.getNoOfOnes()));
    tf$_1_0_Coins.setText(String.valueOf(selectedCashBreakdown.getNoOfOneDollerCoin()));
    tf$0_50_Coins.setText(String.valueOf(selectedCashBreakdown.getNoOfFiftyCentCoin()));
    tf$_0_25_Coins.setText(String.valueOf(selectedCashBreakdown.getNoOfTwentyFiveCentCoin()));
    tf$_0_15_Coins.setText(String.valueOf(selectedCashBreakdown.getNoOfFifteenCentCoin()));
    tf$_0_10_Coins.setText(String.valueOf(selectedCashBreakdown.getNoOfTenCentCoin()));
    tf$_0_05_Coins.setText(String.valueOf(selectedCashBreakdown.getNoOfFiveCentCoin()));
    tf$_0_01_Coins.setText(String.valueOf(selectedCashBreakdown.getNoOfOneCentCoin()));
    calculateTotal();
    
    lblTotalCash.setText("TOTAL CASH (" + selectedCurrency.getName() + ")  " + selectedCurrency.getSymbol());
    tfTotalCash.setText(String.valueOf(selectedCashBreakdown.getTotalAmount()));
  }
  
  private void calculateTotal() {
    double amountNoOfFiveThousands = tfFiveThousands.getInteger() * 5000;
    double amountNoOfTwoThousands = tfTwoThousands.getInteger() * 2000;
    double amountNoOfThousands = tfThousand.getInteger() * 1000;
    double amountNoOfFiveHundreds = tfFiveHundreds.getInteger() * 500;
    
    double amountNoOfHundreds = tfHundreds.getInteger() * 100;
    double amountNoOfFifties = tfFifties.getInteger() * 50;
    double amountNoOfTwenties = tfTwenties.getInteger() * 20;
    double amountNoOfTens = tfTens.getInteger() * 10;
    double amountNoOfFives = tfFives.getInteger() * 5;
    double amountNoOfTwos = tfTwos.getInteger() * 2;
    double amountNoOfOnes = tfOnes.getInteger();
    double amountNoOfOneCoin = tf$_1_0_Coins.getInteger();
    double amountNoOfFiftyCentCoin = tf$0_50_Coins.getInteger() * 0.5D;
    double amountNoOfTwentyFiveCentCoin = tf$_0_25_Coins.getInteger() * 0.25D;
    double amountNoOfFifteenCentCoin = tf$_0_15_Coins.getInteger() * 0.15D;
    double amountNoOfTenCentCoin = tf$_0_10_Coins.getInteger() * 0.1D;
    double amountNoOfFiveCentCoin = tf$_0_05_Coins.getInteger() * 0.05D;
    double amountNoOfOneCentCoin = tf$_0_01_Coins.getInteger() * 0.01D;
    
    double amountTotalCash = amountNoOfFiveThousands + amountNoOfTwoThousands + amountNoOfThousands + amountNoOfFiveHundreds + amountNoOfHundreds + amountNoOfFifties + amountNoOfTwenties + amountNoOfTens + amountNoOfFives + amountNoOfTwos + amountNoOfOnes;
    
    amountTotalCash += amountNoOfOneCoin + amountNoOfFiftyCentCoin + amountNoOfTwentyFiveCentCoin + amountNoOfTenCentCoin + amountNoOfFiveCentCoin + amountNoOfFifteenCentCoin + amountNoOfOneCentCoin;
    
    selectedCashBreakdown.setTotalAmount(Double.valueOf(amountTotalCash));
    updateTotalForCurrency();
  }
  
  private void updateTotalForCurrency() {
    CurrencySummaryRow row = (CurrencySummaryRow)summaryMap.get(selectedCurrency.getName());
    Double totalAmount = selectedCashBreakdown.getTotalAmount();
    if (row != null) {
      tfCashToDeposit.setEditable(totalAmount.doubleValue() == 0.0D);
      row.setSubtotalAmount(totalAmount.doubleValue());
    }
  }
  
  public void callOrderController() {
    Vector<Component> order = new Vector();
    
    order.add(tfFiveThousands);
    order.add(tfTwoThousands);
    order.add(tfThousand);
    order.add(tfFiveHundreds);
    order.add(tfHundreds);
    order.add(tfFifties);
    order.add(tfTwenties);
    order.add(tfTens);
    order.add(tfFives);
    order.add(tfTwos);
    order.add(tfOnes);
    order.add(tf$_1_0_Coins);
    order.add(tf$0_50_Coins);
    order.add(tf$_0_25_Coins);
    order.add(tf$_0_15_Coins);
    order.add(tf$_0_10_Coins);
    order.add(tf$_0_05_Coins);
    order.add(tf$_0_01_Coins);
    
    newPolicy = new MyOwnFocusTraversalPolicy(order);
    
    setFocusCycleRoot(true);
    setFocusTraversalPolicy(newPolicy);
  }
  
  public static class MyOwnFocusTraversalPolicy extends FocusTraversalPolicy {
    Vector<Component> order;
    
    public MyOwnFocusTraversalPolicy(Vector<Component> order) {
      this.order = new Vector(order.size());
      this.order.addAll(order);
    }
    
    public Component getComponentAfter(Container focusCycleRoot, Component aComponent) {
      int idx = (order.indexOf(aComponent) + 1) % order.size();
      return (Component)order.get(idx);
    }
    
    public Component getComponentBefore(Container focusCycleRoot, Component aComponent) {
      int idx = order.indexOf(aComponent) - 1;
      if (idx < 0) {
        idx = order.size() - 1;
      }
      return (Component)order.get(idx);
    }
    
    public Component getDefaultComponent(Container focusCycleRoot) {
      return (Component)order.get(0);
    }
    
    public Component getLastComponent(Container focusCycleRoot) {
      return (Component)order.lastElement();
    }
    
    public Component getFirstComponent(Container focusCycleRoot) {
      return (Component)order.get(0);
    }
  }
  
  private boolean updateModel() {
    selectedCashBreakdown.setNoOfFiveThousands(Integer.valueOf(tfFiveThousands.getInteger()));
    selectedCashBreakdown.setNoOfTwoThousands(Integer.valueOf(tfTwoThousands.getInteger()));
    selectedCashBreakdown.setNoOfThousands(Integer.valueOf(tfThousand.getInteger()));
    selectedCashBreakdown.setNoOfFiveHundreds(Integer.valueOf(tfFiveHundreds.getInteger()));
    selectedCashBreakdown.setNoOfHundreds(Integer.valueOf(tfHundreds.getInteger()));
    selectedCashBreakdown.setNoOfFifties(Integer.valueOf(tfFifties.getInteger()));
    selectedCashBreakdown.setNoOfTwenties(Integer.valueOf(tfTwenties.getInteger()));
    selectedCashBreakdown.setNoOfTens(Integer.valueOf(tfTens.getInteger()));
    selectedCashBreakdown.setNoOfFives(Integer.valueOf(tfFives.getInteger()));
    selectedCashBreakdown.setNoOfTwos(Integer.valueOf(tfTwos.getInteger()));
    selectedCashBreakdown.setNoOfOnes(Integer.valueOf(tfOnes.getInteger()));
    selectedCashBreakdown.setNoOfOneDollerCoin(Integer.valueOf(tf$_1_0_Coins.getInteger()));
    selectedCashBreakdown.setNoOfFiftyCentCoin(Integer.valueOf(tf$0_50_Coins.getInteger()));
    selectedCashBreakdown.setNoOfTwentyFiveCentCoin(Integer.valueOf(tf$_0_25_Coins.getInteger()));
    selectedCashBreakdown.setNoOfFifteenCentCoin(Integer.valueOf(tf$_0_15_Coins.getInteger()));
    selectedCashBreakdown.setNoOfTenCentCoin(Integer.valueOf(tf$_0_10_Coins.getInteger()));
    selectedCashBreakdown.setNoOfFiveCentCoin(Integer.valueOf(tf$_0_05_Coins.getInteger()));
    selectedCashBreakdown.setNoOfOneCentCoin(Integer.valueOf(tf$_0_01_Coins.getInteger()));
    return true;
  }
  
  public void doOk()
  {
    calculateTotal();
    updateTotalForCurrency();
    
    if (!updateModel()) {
      return;
    }
    List<CashBreakdown> cashBreakdowns = cashDrawer.getCashBreakdownList();
    if (cashBreakdowns != null) {
      cashBreakdowns.clear();
    }
    totalReconcileAmount = 0.0D;
    for (Iterator iterator = itemMap.values().iterator(); iterator.hasNext();) {
      CashBreakdown cashBreakdown = (CashBreakdown)iterator.next();
      CurrencySummaryRow summary = (CurrencySummaryRow)summaryMap.get(cashBreakdown.getCurrency().getName());
      cashBreakdown.setBalance(Double.valueOf(summary.getCashToDeposit()));
      cashBreakdown.setTotalAmount(Double.valueOf(summary.getCashToDeposit()));
      if (cashBreakdown.getTotalAmount().doubleValue() > 0.0D) {
        totalReconcileAmount += cashBreakdown.getTotalAmount().doubleValue() / cashBreakdown.getCurrency().getExchangeRate().doubleValue();
        cashDrawer.addTocashBreakdownList(cashBreakdown);
      }
    }
    if ((totalReconcileAmount < cashDrawer.getDrawerAccountable().doubleValue()) && 
      (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Do you want to save partial reconciliation?", "Confirm") != 0)) {
      return;
    }
    
    setCanceled(false);
    dispose();
  }
  
  public double getTotalReconcilieAmount() {
    return totalReconcileAmount;
  }
  
  class CurrencySummaryRow
  {
    Currency currency;
    DoubleTextField tfSubtotalAmount;
    DoubleTextField tfCashToDeposit;
    JLabel totalAmount;
    JLabel currencyName;
    
    public CurrencySummaryRow(JPanel calculatePanel, Currency currency) {
      this.currency = currency;
      
      currencyName = CashReconciliationDialog.this.getJLabel(currency.getName(), 0, 14, 10);
      tfSubtotalAmount = CashReconciliationDialog.this.getDoubleTextField("", 0, 14, 4);
      tfCashToDeposit = CashReconciliationDialog.this.getDoubleTextField("", 0, 14, 4);
      tfSubtotalAmount.setVisible(false);
      
      tfSubtotalAmount.setEnabled(false);
      
      calculatePanel.add(currencyName);
      
      calculatePanel.add(tfCashToDeposit);
      newPolicyorder.add(tfCashToDeposit);
    }
    
    double getSubtotalAmount() {
      double subtotal = tfSubtotalAmount.getDouble();
      if (Double.isNaN(subtotal)) {
        return 0.0D;
      }
      
      return subtotal;
    }
    
    double getCashToDeposit() {
      double cashToDeposit = tfCashToDeposit.getDouble();
      if (Double.isNaN(cashToDeposit)) {
        return 0.0D;
      }
      
      return cashToDeposit;
    }
    
    void setSubtotalAmount(double subtotalAmount) {
      tfSubtotalAmount.setText(String.valueOf(subtotalAmount));
      tfCashToDeposit.setText(String.valueOf(subtotalAmount));
    }
  }
  
  private JLabel getJLabel(String text, int bold, int fontSize, int align) {
    JLabel lbl = new JLabel(text);
    lbl.setFont(lbl.getFont().deriveFont(bold, PosUIManager.getSize(fontSize)));
    lbl.setHorizontalAlignment(align);
    return lbl;
  }
  
  private DoubleTextField getDoubleTextField(String text, int bold, int fontSize, int align) {
    DoubleTextField tf = new DoubleTextField();
    tf.setText(text);
    tf.setFont(tf.getFont().deriveFont(bold, PosUIManager.getSize(fontSize)));
    tf.setHorizontalAlignment(align);
    
    return tf;
  }
}
