package com.floreantpos.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.actions.PosAction;
import com.floreantpos.main.Application;
import com.floreantpos.model.CashTransaction;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.RefundTransaction;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.VoidTransaction;
import com.floreantpos.services.PosTransactionService;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;
































public class RefundDialog
  extends OkCancelOptionDialog
{
  private Ticket ticket;
  private Map<String, PosButton> buttonMap = new HashMap();
  private PosButton btnCashRefund;
  private PosButton btnGiftCardRefund;
  private JPanel buttonsPanel;
  private double totalTransactionTaxAmount = 0.0D;
  
  public RefundDialog(Window window, Ticket ticket) {
    super(window, "");
    this.ticket = ticket;
    initComponents();
    rendererTransactions();
    calculateTaxAmount();
  }
  
  private void initComponents() {
    setCaption("Refund...");
    setOkButtonText(POSConstants.SAVE_BUTTON_TEXT);
    setDefaultCloseOperation(0);
    
    JPanel contentPanel = new JPanel();
    contentPanel.setLayout(new MigLayout("inset 0,fill,hidemode 3", "[grow]", ""));
    Font font2 = new Font("Tahoma", 1, PosUIManager.getFontSize(16));
    
    JPanel inputPanel = new JPanel(new MigLayout("fill,ins 0 5 2 5,hidemode 3", "[grow]", "[grow]"));
    buttonsPanel = new JPanel(new MigLayout("wrap 3,center", "sg,fill", ""));
    btnCashRefund = new PosButton("<html><center><h2> CASH </h2></center></html>");
    btnCashRefund.setAction(new CashRefundAction());
    btnCashRefund.setMinimumSize(PosUIManager.getSize(120, 110));
    
    btnGiftCardRefund = new PosButton("<html><center><h2> GIFT CARD </h2></center></html>");
    btnGiftCardRefund.setAction(new GiftCardRefundAction());
    btnGiftCardRefund.setMinimumSize(PosUIManager.getSize(120, 110));
    
    JLabel lblTransaction = new JLabel("Select transaction to refund");
    lblTransaction.setFont(font2);
    
    PosScrollPane scrollPane = new PosScrollPane(buttonsPanel);
    scrollPane.setBorder(BorderFactory.createTitledBorder(null, "REFUND USING", 2, 2));
    inputPanel.add(scrollPane, "grow,aligny bottom,span");
    contentPanel.add(inputPanel, "grow");
    getContentPanel().add(contentPanel);
  }
  
  private void rendererTransactions() {
    buttonsPanel.removeAll();
    buttonsPanel.add(btnCashRefund, "aligny top");
    buttonsPanel.add(btnGiftCardRefund, "aligny top");
    if (ticket != null) {
      for (PosTransaction t : ticket.getTransactions())
        if ((!t.isVoided().booleanValue()) && (!(t instanceof RefundTransaction)) && (!(t instanceof VoidTransaction)) && (!(t instanceof CashTransaction)))
        {

          PosButton btnRefund = new PosButton();
          btnRefund.putClientProperty("transaction", t);
          btnRefund.setAction(new RefundAction());
          btnRefund.setMinimumSize(PosUIManager.getSize(120, 110));
          updateButtonText(t, btnRefund);
          buttonsPanel.add(btnRefund, "aligny top");
          buttonMap.put(t.getId(), btnRefund);
        }
    }
    boolean enableButton = ticket.getPaidAmount() != ticket.getRefundAmount();
    btnCashRefund.setEnabled(enableButton);
    double refundableAmount = getRefundableAmount();
    btnCashRefund.setText("<html><center><b>CASH </b><br><h2> " + refundableAmount + "</h2></center></html>");
    btnGiftCardRefund.setEnabled(enableButton);
    btnGiftCardRefund.setText("<html><center><b>GIFT CARD </b><br><h2> " + refundableAmount + "</h2></center></html>");
    
    buttonsPanel.revalidate();
    buttonsPanel.repaint();
  }
  
  private void updateButtonText(PosTransaction t, PosButton btnRefund) {
    double refundedAmountForTransaction = getRefundedAmount(t);
    if (refundedAmountForTransaction > 0.0D) {
      btnRefund.setText("<html><center>" + t.getPaymentType() + "<br><h2>" + CurrencyUtil.getCurrencySymbol() + t.getAmount() + "</h2><h5>Refunded -" + CurrencyUtil.getCurrencySymbol() + refundedAmountForTransaction + "</h5></html>");
      btnRefund.setEnabled(t.getAmount().doubleValue() != refundedAmountForTransaction);
      if (t.getAmount().doubleValue() != refundedAmountForTransaction) {
        btnRefund.setSelected(true);
      }
    }
    else {
      btnRefund.setSelected(true);
      btnRefund.setText("<html><center>" + t.getPaymentType() + "<br><h2>" + CurrencyUtil.getCurrencySymbol() + t.getAmount() + "<h2></center></html>");
    }
  }
  
  private double getTotalTransactionAmount(List<PosTransaction> selectedTransactions) {
    double refundAmount = 0.0D;
    for (PosTransaction transaction : selectedTransactions) {
      if (!transaction.isVoided().booleanValue())
      {

        refundAmount += transaction.getAmount().doubleValue(); }
    }
    return refundAmount;
  }
  
  private void calculateTaxAmount() {
    totalTransactionTaxAmount = 0.0D;
    for (PosTransaction t : ticket.getTransactions()) {
      if ((!(t instanceof RefundTransaction)) && (!t.isVoided().booleanValue())) {
        totalTransactionTaxAmount += t.getTaxAmount().doubleValue();
      }
    }
  }
  
  public void doOk()
  {
    setCanceled(false);
    dispose();
  }
  
  private void doRefund(List<PosTransaction> selectedTransactions, boolean forceCashRefund) {
    doRefund(selectedTransactions, forceCashRefund, false);
  }
  
  private void doRefund(List<PosTransaction> selectedTransactions, boolean forceCashRefund, boolean refundUsingGiftCard) {
    try {
      double refundableAmount = getRefundableAmount();
      String giftCardNo = null;
      if (refundUsingGiftCard) {
        GlobalInputDialog inputDialog = new GlobalInputDialog();
        inputDialog.setCaption("Enter gift card number ");
        inputDialog.open();
        if (inputDialog.isCanceled()) {
          return;
        }
        giftCardNo = inputDialog.getInput();
        if (giftCardNo == null)
          return;
      }
      double refundAmount = NumberSelectionDialog2.takeDoubleInput("Enter refund amount", refundableAmount);
      if (refundAmount < 0.0D) {
        return;
      }
      if (refundAmount == 0.0D) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Refund amount must be greater than zero.");
        return;
      }
      double refundedTransactionAmount = getRefundedAmount(selectedTransactions);
      double paidAmount = NumberUtil.roundToTwoDigit(ticket.getPaidAmount().doubleValue()) - refundedTransactionAmount;
      
      if ((refundAmount > refundableAmount) || (refundAmount > NumberUtil.roundToTwoDigit(getTotalTransactionAmount(selectedTransactions) - refundedTransactionAmount))) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Refund amount cannot be greater than selected transaction amount.");
        return;
      }
      
      paidAmount = NumberUtil.roundToTwoDigit(paidAmount);
      if (refundAmount > paidAmount) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Refund amount cannot be greater than paid amount.");
        return;
      }
      Double refundTaxAmount = Double.valueOf(NumberUtil.roundToTwoDigit(totalTransactionTaxAmount * refundAmount / ticket.getPaidAmount().doubleValue()));
      double refundedAmount = PosTransactionService.getInstance().refundTicket(ticket, refundAmount, refundTaxAmount, Application.getCurrentUser(), selectedTransactions, forceCashRefund, giftCardNo);
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Refunded " + CurrencyUtil.getCurrencySymbol() + refundedAmount);
      if ((buttonMap.size() == 0) || (getRefundedAmount(new ArrayList(ticket.getTransactions())) >= ticket.getPaidAmount().doubleValue())) {
        doOk();
      }
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage());
    }
  }
  
  private double getRefundableAmount() {
    double refundableAmount = 0.0D;
    if (ticket.getDueAmount().doubleValue() >= 0.0D) {
      return 0.0D;
    }
    refundableAmount = Math.abs(ticket.getDueAmount().doubleValue());
    

    return NumberUtil.roundToTwoDigit(refundableAmount);
  }
  
  private double getRefundedAmount(List<PosTransaction> selectedTransactions) {
    double refundedAmountForTransaction = 0.0D;
    for (PosTransaction posTransaction : selectedTransactions) {
      if (!posTransaction.isVoided().booleanValue())
      {

        String refundedAmountText = posTransaction.getProperty("REFUNDED_AMOUNT");
        if (StringUtils.isNotEmpty(refundedAmountText)) {
          try {
            refundedAmountForTransaction += Double.parseDouble(refundedAmountText);
          } catch (Exception localException) {}
        }
      }
    }
    return NumberUtil.roundToTwoDigit(refundedAmountForTransaction);
  }
  
  private double getRefundedAmount(PosTransaction posTransaction) {
    double refundedAmountForTransaction = 0.0D;
    String refundedAmountText = posTransaction.getProperty("REFUNDED_AMOUNT");
    if (StringUtils.isNotEmpty(refundedAmountText)) {
      try {
        refundedAmountForTransaction = Double.parseDouble(refundedAmountText);
      }
      catch (Exception localException) {}
    }
    return NumberUtil.roundToTwoDigit(refundedAmountForTransaction);
  }
  
  private class RefundAction extends PosAction {
    public RefundAction() {
      setRequiredPermission(UserPermission.PERFORM_MANAGER_TASK);
      setMandatoryPermission(true);
    }
    
    public void execute()
    {
      PosButton button = (PosButton)event.getSource();
      PosTransaction transaction = (PosTransaction)button.getClientProperty("transaction");
      String refundedAmountText = transaction.getProperty("REFUNDED_AMOUNT");
      double refundAmount = 0.0D;
      if (StringUtils.isNotEmpty(refundedAmountText)) {
        try {
          refundAmount = Double.parseDouble(refundedAmountText);
        }
        catch (Exception localException) {}
        if (refundAmount >= transaction.getAmount().doubleValue()) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Transaction already refunded.");
          button.setSelected(false);
          return;
        }
      }
      List<PosTransaction> selectedTransactions = new ArrayList();
      selectedTransactions.add(transaction);
      RefundDialog.this.doRefund(selectedTransactions, false);
    }
  }
  
  private class CashRefundAction extends PosAction {
    public CashRefundAction() {
      setRequiredPermission(UserPermission.PERFORM_MANAGER_TASK);
      setMandatoryPermission(true);
    }
    
    public void execute()
    {
      List<PosTransaction> selectedTransactions = new ArrayList();
      for (PosTransaction t : ticket.getTransactions()) {
        if ((!(t instanceof RefundTransaction)) && (!(t instanceof VoidTransaction)))
        {
          selectedTransactions.add(t); }
      }
      RefundDialog.this.doRefund(selectedTransactions, true);
    }
  }
  
  private class GiftCardRefundAction extends PosAction {
    public GiftCardRefundAction() {
      setRequiredPermission(UserPermission.PERFORM_MANAGER_TASK);
      setMandatoryPermission(true);
    }
    
    public void execute()
    {
      List<PosTransaction> selectedTransactions = new ArrayList();
      for (PosTransaction t : ticket.getTransactions()) {
        if ((!(t instanceof RefundTransaction)) && (!(t instanceof VoidTransaction)))
        {
          selectedTransactions.add(t); }
      }
      RefundDialog.this.doRefund(selectedTransactions, false, true);
    }
  }
}
