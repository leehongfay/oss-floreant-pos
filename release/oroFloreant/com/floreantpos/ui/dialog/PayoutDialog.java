package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.ActionHistory;
import com.floreantpos.model.PayoutReason;
import com.floreantpos.model.PayoutRecepient;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.ActionHistoryDAO;
import com.floreantpos.model.dao.PayOutTransactionDAO;
import com.floreantpos.ui.views.PayOutView;
import com.floreantpos.util.NumberUtil;
import java.awt.Dialog;
import java.awt.Frame;
import javax.swing.JPanel;































public class PayoutDialog
  extends OkCancelOptionDialog
{
  private PayOutView payOutView;
  private User currentUser;
  private Terminal drawerTerminal;
  
  public PayoutDialog(Dialog parent, User user, Terminal drawerTerminal)
  {
    super(parent, true);
    currentUser = user;
    this.drawerTerminal = drawerTerminal;
    setTitle(Application.getTitle() + POSConstants.PAYOUT_BUTTON_TEXT);
    initComponents();
    payOutView.initialize();
  }
  
  public PayoutDialog(Frame parent, User user, Terminal drawerTerminal) {
    super(parent, true);
    currentUser = user;
    this.drawerTerminal = drawerTerminal;
    setTitle(Application.getTitle() + POSConstants.PAYOUT_BUTTON_TEXT);
    initComponents();
    payOutView.initialize();
  }
  
  private void initComponents() {
    setCaption(POSConstants.PAYOUT_BUTTON_TEXT);
    setOkButtonText(POSConstants.FINISH);
    
    payOutView = new PayOutView();
    
    setDefaultCloseOperation(2);
    getContentPanel().add(payOutView);
    pack();
  }
  
  public void doOk()
  {
    double payoutAmount = payOutView.getPayoutAmount();
    PayoutReason reason = payOutView.getReason();
    PayoutRecepient recepient = payOutView.getRecepient();
    String note = payOutView.getNote();
    
    try
    {
      PayOutTransactionDAO.getInstance().createPayoutTransaction(reason, recepient, note, payoutAmount, currentUser, drawerTerminal);
      String actionMessage = "";
      actionMessage = actionMessage + Messages.getString("PayoutDialog.2") + ":" + NumberUtil.formatNumber(Double.valueOf(payoutAmount));
      ActionHistoryDAO.getInstance().saveHistory(Application.getCurrentUser(), ActionHistory.PAY_OUT, actionMessage);
      setCanceled(false);
      dispose();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
}
