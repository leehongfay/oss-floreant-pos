package com.floreantpos.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.actions.DrawerAssignmentAction;
import com.floreantpos.actions.StoreSessionReportAction;
import com.floreantpos.main.Application;
import com.floreantpos.model.AttendenceHistory;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.DrawerType;
import com.floreantpos.model.Shift;
import com.floreantpos.model.StoreSession;
import com.floreantpos.model.StoreSessionControl;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.AttendenceHistoryDAO;
import com.floreantpos.model.dao.CashDrawerDAO;
import com.floreantpos.model.dao.PosTransactionDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.print.PosPrintService;
import com.floreantpos.swing.ButtonColumn;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.StoreUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;




























public class DrawerAndStaffBankReportDialog
  extends POSDialog
{
  private TitlePanel titlePanel;
  private JTable tableDrawerReport;
  private DrawerReportTableModel tableModel;
  private List<CashDrawer> drawerPullReportList;
  private DefaultListSelectionModel selectionModel;
  private PosButton btnReportHistory;
  private PosButton btnCloseStore;
  private User user;
  private Terminal currentTerminal;
  
  public DrawerAndStaffBankReportDialog(User user)
  {
    super(Application.getPosWindow(), true);
    setTitle("Store status");
    this.user = user;
    initComponents();
    initData();
  }
  
  public void initData() {
    currentTerminal = Application.getInstance().getTerminal();
    
    selectionModel = new DefaultListSelectionModel();
    selectionModel.setSelectionMode(0);
    tableDrawerReport.getSelectionModel().setSelectionMode(0);
    
    tableDrawerReport.setDefaultRenderer(Object.class, new TableRenderer());
    tableModel = new DrawerReportTableModel();
    tableDrawerReport.setModel(tableModel);
    
    updateView();
    boolean hasStoreAccess = (StoreUtil.isStoreOpen()) && (user.hasPermission(UserPermission.OPEN_CLOSE_STORE));
    
    btnCloseStore.setVisible(hasStoreAccess);
    
    AbstractAction action = new AbstractAction()
    {
      public void actionPerformed(ActionEvent e)
      {
        int row = Integer.parseInt(e.getActionCommand());
        CashDrawer pullReport = (CashDrawer)tableModel.getRowData(row);
        if (pullReport.getId() != null) {
          DrawerAndStaffBankReportDialog.this.showReport(pullReport);
        }
        else {
          if (!user.isClockedIn().booleanValue()) {
            POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "You are not clockd in.\n Please clock in first to assign drawer.");
            return;
          }
          DrawerAssignmentAction action = new DrawerAssignmentAction(pullReport.getTerminal(), user);
          action.execute();
        }
        DrawerAndStaffBankReportDialog.this.updateView();
      }
    };
    ButtonColumn buttonColumn = new ButtonColumn(tableDrawerReport, action, 3)
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        return super.getTableCellRendererComponent(table, value, false, hasFocus, row, column);
      }
      
      public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
      {
        return super.getTableCellEditorComponent(table, value, false, row, column);
      }
    };
    MatteBorder selectedBorder = BorderFactory.createMatteBorder(5, 15, 5, 15, tableDrawerReport.getBackground());
    MatteBorder unselectedBorder = BorderFactory.createMatteBorder(5, 15, 5, 15, tableDrawerReport.getBackground());
    
    Border border1 = new CompoundBorder(selectedBorder, btnCloseStore.getBorder());
    Border border2 = new CompoundBorder(unselectedBorder, btnCloseStore.getBorder());
    
    buttonColumn.setUnselectedBorder(border1);
    buttonColumn.setFocusBorder(border2);
    
    resizeTableColumns();
  }
  
  private void updateView() {
    StoreSession currentStoreOperationData = StoreUtil.getCurrentStoreOperation().getCurrentData();
    if (currentStoreOperationData == null)
      return;
    drawerPullReportList = new ArrayList();
    List<Terminal> terminalList = TerminalDAO.getInstance().findCashDrawerTerminals();
    List<CashDrawer> drawerStatusList = CashDrawerDAO.getInstance().findByStoreOperationData(currentStoreOperationData, Boolean.valueOf(true));
    for (CashDrawer cashDrawer : drawerStatusList) {
      if (cashDrawer.getReportTime() == null) {
        drawerPullReportList.add(cashDrawer);
      }
    }
    
    for (Terminal terminal : terminalList) {
      if (terminal.getCurrentCashDrawer() == null) {
        CashDrawer toBeAssignedPullReport = new CashDrawer();
        toBeAssignedPullReport.setTerminal(terminal);
        toBeAssignedPullReport.setAssignedUser(user);
        drawerPullReportList.add(toBeAssignedPullReport);
      }
    }
    tableModel.setRows(drawerPullReportList);
    tableModel.fireTableDataChanged();
  }
  
  private void resizeTableColumns() {
    tableDrawerReport.setAutoResizeMode(4);
    setColumnWidth(1, PosUIManager.getSize(100));
    setColumnWidth(2, PosUIManager.getSize(150));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = tableDrawerReport.getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  private void initComponents() {
    TransparentPanel container = new TransparentPanel();
    titlePanel = new TitlePanel();
    TransparentPanel contentPanel = new TransparentPanel();
    TransparentPanel bottomActionPanel = new TransparentPanel(new MigLayout("al center", "sg, fill", ""));
    
    JSeparator jSeparator1 = new JSeparator();
    
    PosButton btnOK = new PosButton();
    
    JScrollPane jScrollPane1 = new JScrollPane();
    tableDrawerReport = new JTable();
    tableDrawerReport.setRowHeight(PosUIManager.getSize(40));
    tableDrawerReport.getTableHeader().setPreferredSize(PosUIManager.getSize(0, 40));
    
    getContentPane().setLayout(new BorderLayout(5, 5));
    
    container.setLayout(new BorderLayout(5, 5));
    container.add(jSeparator1, "North");
    
    PosButton btnSessionReport = new PosButton("REPORT");
    btnSessionReport.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        StoreSessionReportAction action = new StoreSessionReportAction(StoreUtil.getCurrentStoreOperation().getCurrentData());
        action.execute();
      }
      
    });
    btnReportHistory = new PosButton("HISTORY");
    btnReportHistory.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        DrawerAndStaffBankReportHistoryDialog dialog = new DrawerAndStaffBankReportHistoryDialog(Application.getPosWindow(), user, StoreUtil.getCurrentStoreOperation().getCurrentData());
        dialog.setInfo("Cash drawer and staff bank history");
        dialog.setSize(PosUIManager.getSize(880, 580));
        dialog.open();
      }
      
    });
    btnCloseStore = new PosButton("CLOSE STORE");
    btnCloseStore.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        DrawerAndStaffBankReportDialog.this.doCloseStore();
      }
    });
    bottomActionPanel.add(btnSessionReport);
    bottomActionPanel.add(btnReportHistory);
    bottomActionPanel.add(btnCloseStore);
    
    btnOK.setText(POSConstants.SAVE_BUTTON_TEXT);
    btnOK.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        DrawerAndStaffBankReportDialog.this.btnCloseActionPerformed(evt);
      }
      
    });
    bottomActionPanel.add(btnOK);
    
    container.add(bottomActionPanel, "Center");
    getContentPane().add(container, "South");
    
    contentPanel.setLayout(new BorderLayout());
    contentPanel.add(titlePanel, "North");
    
    jScrollPane1.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(10, 15, 10, 15), jScrollPane1.getBorder()));
    jScrollPane1.setViewportView(tableDrawerReport);
    
    contentPanel.add(jScrollPane1, "Center");
    getContentPane().add(contentPanel, "Center");
    setSize(PosUIManager.getSize(830, 550));
  }
  
  private void showReport(CashDrawer report) {
    if (report == null) {
      return;
    }
    if (report.getDrawerType() == DrawerType.STAFF_BANK) {
      showBankStatus(report);
    }
    else {
      showDrawerStatus(report);
    }
  }
  
  private void doCloseStore() {
    try {
      if (!user.isClockedIn().booleanValue()) {
        POSMessageDialog.showMessage("You are not clocked in.\nPlease clock in first to close the store.");
        return;
      }
      








      if (PosTransactionDAO.getInstance().hasUnauthorizedTransactions(null).booleanValue()) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please authorize tickets that are not authorize yet.");
        return;
      }
      StoreSession currentData = StoreUtil.getCurrentStoreOperation().getCurrentData();
      List<CashDrawer> reportList = CashDrawerDAO.getInstance().findByStoreOperationData(currentData, Boolean.valueOf(true));
      if (reportList != null) {
        for (CashDrawer report : reportList) {
          if (report.getReportTime() == null) {
            String message = "Not all cash drawers/staff banks are closed. Please close them first.";
            POSMessageDialog.showMessage(this, message);
            return;
          }
        }
      }
      Object clockInUsers = UserDAO.getInstance().findClockedInUsers();
      boolean hasClockedInUser = (clockInUsers != null) && (((List)clockInUsers).size() > 0);
      if ((hasClockedInUser) && (((List)clockInUsers).size() == 1)) {
        hasClockedInUser = !((User)((List)clockInUsers).get(0)).getId().equals(user.getId());
      }
      if (hasClockedInUser) {
        String message = "Not all users are cloked out. Please clocked out all users first.";
        POSMessageDialog.showMessage(this, message);
        return;
      }
      if (POSMessageDialog.showYesNoQuestionDialog(this, "Are you sure you want to close store?", "Confirm") != 0) {
        return;
      }
      if (user.isClockedIn().booleanValue()) {
        doClockedOutUser(user);
      }
      currentData.setCloseTime(new Date());
      currentData.setClosedBy(user);
      StoreUtil.closeStore(user);
      PosPrintService.printCashDrawerReportSummary(currentData);
      
      String msg = "Store is closed";
      if (POSMessageDialog.showMessageAndPromtToPrint(msg)) {
        PosPrintService.printCashDrawerReportSummary(currentData);
      }
      dispose();
    }
    catch (PosException e) {
      POSMessageDialog.showError(this, e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage(), e);
    }
  }
  
  private void doClockedOutUser(User user) {
    try {
      AttendenceHistoryDAO attendenceHistoryDAO = new AttendenceHistoryDAO();
      AttendenceHistory attendenceHistory = attendenceHistoryDAO.findHistoryByClockedInTime(user);
      if (attendenceHistory == null) {
        attendenceHistory = new AttendenceHistory();
        Date lastClockInTime = user.getLastClockInTime();
        Calendar c = Calendar.getInstance();
        c.setTime(lastClockInTime);
        attendenceHistory.setClockInTime(lastClockInTime);
        attendenceHistory.setClockInHour(Short.valueOf((short)c.get(10)));
        attendenceHistory.setUser(user);
        attendenceHistory.setTerminal(Application.getInstance().getTerminal());
        attendenceHistory.setShift(user.getCurrentShift());
      }
      
      Shift shift = user.getCurrentShift();
      Calendar calendar = Calendar.getInstance();
      
      user.doClockOut(attendenceHistory, shift, calendar);
      
      POSMessageDialog.showMessage(Messages.getString("ClockInOutAction.8") + user.getFirstName() + " " + user.getLastName() + Messages.getString("ClockInOutAction.10"));
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private void showBankStatus(CashDrawer report) {
    try {
      CashDrawerInfoDialog dialog = new CashDrawerInfoDialog(user, report);
      dialog.setTitle(report.getDrawerType() == DrawerType.STAFF_BANK ? "BANK STATUS" : POSConstants.DRAWER_PULL_BUTTON_TEXT);
      dialog.refreshReport();
      dialog.setDefaultCloseOperation(2);
      dialog.openFullScreen();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private void showDrawerStatus(CashDrawer report) {
    try {
      if (report.getTerminal() == null) {
        return;
      }
      CashDrawerInfoDialog dialog = new CashDrawerInfoDialog(user, report);
      dialog.setTitle(POSConstants.DRAWER_PULL_BUTTON_TEXT);
      dialog.refreshReport();
      dialog.setDefaultCloseOperation(2);
      dialog.openFullScreen();
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public void setInfo(String info) {
    titlePanel.setTitle(info);
  }
  
  private void btnCloseActionPerformed(ActionEvent evt) {
    dispose();
  }
  
  class DrawerReportTableModel extends ListTableModel<CashDrawer>
  {
    public DrawerReportTableModel() {
      super();
    }
    
    public int getRowCount() {
      if (drawerPullReportList == null) {
        return 0;
      }
      int size = drawerPullReportList.size();
      return size;
    }
    
    public int getColumnCount() {
      return 4;
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      if (columnIndex == 3) {
        return true;
      }
      
      return false;
    }
    
    public CashDrawer getSelectedRow() {
      int index = tableDrawerReport.getSelectedRow();
      if (index < 0) {
        return null;
      }
      index = tableDrawerReport.convertRowIndexToModel(index);
      return (CashDrawer)tableModel.getRowData(index);
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      if (drawerPullReportList == null)
        return "";
      CashDrawer t = (CashDrawer)drawerPullReportList.get(rowIndex);
      User assignedUser = t.getAssignedUser();
      
      switch (columnIndex) {
      case 0: 
        if (t.getDrawerType() == DrawerType.DRAWER)
          return "Terminal - " + t.getTerminal();
        return t.getAssignedUser();
      case 1: 
        return t.getDrawerType().toString();
      case 2: 
        if (t.getId() == null)
          return "NOT ASSIGNED";
        if (assignedUser != null)
          return t.getReportTime() == null ? "ASSIGNED" : "CLOSED";
      case 3: 
        if (t.getId() == null) {
          return "ASSIGN";
        }
        return "STATUS";
      }
      return "";
    }
  }
  
  class TableRenderer extends DefaultTableCellRenderer
  {
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
    
    TableRenderer() {}
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) { JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      label.setIcon(null);
      if (column == 0) {
        CashDrawer t = (CashDrawer)drawerPullReportList.get(row);
        if ((t.getDrawerType() != DrawerType.STAFF_BANK) && (t.getTerminal().getId().intValue() == currentTerminal.getId().intValue()))
          label.setIcon(IconFactory.getIcon("/ui_icons/", "check_mark.png"));
      }
      if ((value instanceof Date)) {
        String string = dateFormat.format(value);
        label.setText(string);
        label.setHorizontalAlignment(4);
      }
      if ((value instanceof Double)) {
        label.setHorizontalAlignment(4);
      }
      else {
        label.setHorizontalAlignment(2);
      }
      if ((column == 1) || (column == 2))
        label.setHorizontalAlignment(0);
      return label;
    }
  }
}
