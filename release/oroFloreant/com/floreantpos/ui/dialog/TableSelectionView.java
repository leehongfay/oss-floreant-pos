package com.floreantpos.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.TableStatus;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.ShopTableDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.ScrollableFlowPanel;
import com.floreantpos.swing.ShopTableButton;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.RootView;
import com.jidesoft.swing.JideScrollPane;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;




















public class TableSelectionView
  extends JPanel
  implements ActionListener
{
  private DefaultListModel<ShopTableButton> addedTableListModel = new DefaultListModel();
  private Map<ShopTable, ShopTableButton> tableButtonMap = new HashMap();
  
  private ScrollableFlowPanel buttonsPanel;
  
  private POSToggleButton btnGroup;
  
  private POSToggleButton btnUnGroup;
  private PosButton btnDone;
  private PosButton btnCancel;
  private PosButton btnRefresh;
  private ButtonGroup btnGroups;
  
  public TableSelectionView()
  {
    init();
  }
  
  private void init() {
    setLayout(new BorderLayout());
    
    buttonsPanel = new ScrollableFlowPanel(1);
    
    setLayout(new BorderLayout(10, 10));
    
    TitledBorder titledBorder1 = BorderFactory.createTitledBorder(null, POSConstants.TABLES, 2, 0);
    
    JPanel leftPanel = new JPanel(new BorderLayout(5, 5));
    leftPanel.setBorder(new CompoundBorder(titledBorder1, new EmptyBorder(2, 2, 2, 2)));
    


    JideScrollPane scrollPane = new JideScrollPane(buttonsPanel, 20, 31);
    scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(60, 0));
    
    leftPanel.add(scrollPane, "Center");
    
    add(leftPanel, "Center");
    
    createButtonActionPanel();
  }
  
  private void createButtonActionPanel() {
    TitledBorder titledBorder2 = BorderFactory.createTitledBorder(null, "-", 2, 0);
    
    JPanel rightPanel = new JPanel(new BorderLayout(20, 20));
    rightPanel.setPreferredSize(new Dimension(120, 0));
    rightPanel.setBorder(new CompoundBorder(titledBorder2, new EmptyBorder(2, 2, 6, 2)));
    
    JPanel actionBtnPanel = new JPanel(new MigLayout("ins 2 2 0 2, hidemode 3, flowy", "grow", ""));
    
    btnGroups = new ButtonGroup();
    
    btnGroup = new POSToggleButton(POSConstants.GROUP);
    btnUnGroup = new POSToggleButton(POSConstants.UNGROUP);
    btnDone = new PosButton(POSConstants.SAVE_BUTTON_TEXT);
    btnCancel = new PosButton(POSConstants.CANCEL);
    
    btnGroup.addActionListener(this);
    btnUnGroup.addActionListener(this);
    btnDone.addActionListener(this);
    btnCancel.addActionListener(this);
    
    btnDone.setVisible(false);
    btnCancel.setVisible(false);
    
    btnGroup.setIcon(new ImageIcon(getClass().getResource("/images/plus.png")));
    btnUnGroup.setIcon(new ImageIcon(getClass().getResource("/images/minus2.png")));
    
    btnGroups.add(btnGroup);
    btnGroups.add(btnUnGroup);
    
    actionBtnPanel.add(btnGroup, "grow");
    actionBtnPanel.add(btnUnGroup, "grow");
    actionBtnPanel.add(btnDone, "grow");
    actionBtnPanel.add(btnCancel, "grow");
    
    rightPanel.add(actionBtnPanel);
    
    btnRefresh = new PosButton(POSConstants.REFRESH);
    btnRefresh.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        redererTable();
        btnGroups.clearSelection();
      }
    });
    rightPanel.add(btnRefresh, "South");
    
    add(rightPanel, "East");
  }
  
  public synchronized void redererTable()
  {
    addedTableListModel.clear();
    buttonsPanel.getContentPane().removeAll();
    
    checkTables();
    
    List<ShopTable> tables = ShopTableDAO.getInstance().findAll();
    
    for (ShopTable shopTable : tables) {
      ShopTableButton tableButton = new ShopTableButton(shopTable);
      tableButton.setPreferredSize(new Dimension(157, 138));
      tableButton.setFont(new Font(tableButton.getFont().getName(), tableButton.getFont().getStyle(), 30));
      
      tableButton.setText(tableButton.getText());
      tableButton.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          TableSelectionView.this.addTable(e);
        }
        
      });
      tableButton.update();
      buttonsPanel.add(tableButton);
      tableButtonMap.put(shopTable, tableButton);
    }
    
    rendererTablesTicket();
  }
  







  private void rendererTablesTicket() {}
  






  private boolean addTable(ActionEvent e)
  {
    ShopTableButton button = (ShopTableButton)e.getSource();
    int tableNumber = button.getId();
    
    ShopTable shopTable = ShopTableDAO.getInstance().getByNumber(tableNumber);
    
    if (shopTable == null) {
      POSMessageDialog.showError(this, Messages.getString("TableSelectionDialog.2") + e + Messages.getString("TableSelectionDialog.3"));
      return false;
    }
    
    if (btnGroup.isSelected()) {
      if (addedTableListModel.contains(button)) {
        return true;
      }
      if (button.getShopTable().getTableStatus().equals(TableStatus.Seat)) {
        return true;
      }
      
      button.getShopTable().setTableStatus(TableStatus.Seat);
      button.setBackground(Color.green);
      button.setForeground(Color.black);
      addedTableListModel.addElement(button);
      return false;
    }
    
    if (btnUnGroup.isSelected()) {
      if (addedTableListModel.contains(button)) {
        return true;
      }
      Ticket ticket = button.getTicket();
      if (ticket == null) {
        return false;
      }
      
      String ticketId = ticket.getId();
      
      Enumeration<ShopTableButton> elements = addedTableListModel.elements();
      while (elements.hasMoreElements()) {
        ShopTableButton shopTableButton = (ShopTableButton)elements.nextElement();
        if (!shopTableButton.getTicket().getId().equals(ticketId)) {
          return false;
        }
      }
      
      if (addedTableListModel.size() >= ticket.getTableNumbers().size() - 1) {
        return false;
      }
      
      button.getShopTable().setTableStatus(TableStatus.Seat);
      button.setBackground(Color.green);
      button.setForeground(Color.black);
      addedTableListModel.addElement(button);
      return false;
    }
    
    if ((shopTable.getTableStatus().equals(TableStatus.Seat)) && (!btnGroup.isSelected())) {
      if (!button.hasUserAccess()) {
        return false;
      }
      editTicket(button.getTicket());
      return false;
    }
    
    if ((!btnGroup.isSelected()) && (!btnGroup.isSelected())) {
      if (!addedTableListModel.contains(button)) {
        addedTableListModel.addElement(button);
      }
      doCreateNewTicket();
      clearSelection();
    }
    return true;
  }
  
  public List<ShopTable> getTables() {
    Enumeration<ShopTableButton> elements = addedTableListModel.elements();
    List<ShopTable> tables = new ArrayList();
    
    while (elements.hasMoreElements()) {
      ShopTableButton shopTableButton = (ShopTableButton)elements.nextElement();
      tables.add(shopTableButton.getShopTable());
    }
    
    return tables;
  }
  
  private void clearSelection() {
    redererTable();
    btnGroups.clearSelection();
    btnGroup.setVisible(true);
    btnUnGroup.setVisible(true);
    btnDone.setVisible(false);
    btnCancel.setVisible(false);
  }
  
  public void actionPerformed(ActionEvent e)
  {
    Object object = e.getSource();
    if (object == btnGroup) {
      addedTableListModel.clear();
      btnUnGroup.setVisible(false);
      btnDone.setVisible(true);
      btnCancel.setVisible(true);
    }
    else if (object == btnUnGroup) {
      addedTableListModel.clear();
      btnGroup.setVisible(false);
      btnDone.setVisible(true);
      btnCancel.setVisible(true);
    }
    else if (object == btnDone) {
      if (btnGroup.isSelected()) {
        doGroupAction();
        clearSelection();
      }
      else if (btnUnGroup.isSelected()) {
        doUnGroupAction();
        clearSelection();
      }
    }
    else if (object == btnCancel) {
      clearSelection();
    }
  }
  








  private void doCreateNewTicket() {}
  







  private boolean editTicket(Ticket ticket)
  {
    if (ticket == null) {
      return false;
    }
    
    Ticket ticketToEdit = TicketDAO.getInstance().loadFullTicket(ticket.getId());
    
    OrderView.getInstance().setCurrentTicket(ticketToEdit);
    RootView.getInstance().showView("ORDER_VIEW");
    return true;
  }
  
  private void doGroupAction() {
    doCreateNewTicket();
  }
  
  private void doUnGroupAction() {
    if ((addedTableListModel == null) || (addedTableListModel.isEmpty())) {
      return;
    }
    
    Enumeration<ShopTableButton> elements = addedTableListModel.elements();
    
    if (!((ShopTableButton)addedTableListModel.elementAt(0)).hasUserAccess()) {
      return;
    }
    while (elements.hasMoreElements()) {
      ShopTableButton button = (ShopTableButton)elements.nextElement();
      ShopTable shopTable = button.getShopTable();
      Ticket ticket = button.getTicket();
      if (ticket != null) {
        for (Iterator iterator = ticket.getTableNumbers().iterator(); iterator.hasNext();) {
          Integer id = (Integer)iterator.next();
          if (button.getId() == id.intValue()) {
            iterator.remove();
          }
        }
        

        shopTable.setTableStatus(TableStatus.Available);
        ShopTableDAO.getInstance().saveOrUpdate(shopTable);
        TicketDAO.getInstance().saveOrUpdate(ticket);
      }
    }
  }
  
  private void checkTables()
  {
    List<ShopTable> allTables = ShopTableDAO.getInstance().findAll();
    
    if ((allTables == null) || (allTables.isEmpty()))
    {
      int userInput = 0;
      
      int result = POSMessageDialog.showYesNoQuestionDialog(Application.getPosWindow(), 
        Messages.getString("TableSelectionView.0"), Messages.getString("TableSelectionView.1"));
      
      if (result == 0)
      {
        userInput = (int)NumberSelectionDialog2.takeIntInput(Messages.getString("TableSelectionView.2"));
        
        if (userInput == 0) {
          POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("TableSelectionView.3"));
          return;
        }
        
        if (userInput != -1) {
          ShopTableDAO.getInstance().createNewTables(userInput);
        }
      }
    }
  }
}
