package com.floreantpos.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.config.AppProperties;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.util.POSUtil;
import com.orocube.common.util.TerminalUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;


















public class AboutDialog
  extends POSDialog
{
  private JTextField tfTerminalKey;
  private JCheckBox chkCheckUpdateOnStartUp;
  
  public AboutDialog()
  {
    super(POSUtil.getBackOfficeWindow(), Messages.getString("AboutDialog.0"));
  }
  
  protected void initUI()
  {
    JPanel container = new JPanel(new BorderLayout());
    container.setBorder(new EmptyBorder(20, 20, 20, 20));
    
    JPanel contentPanel = new JPanel(new MigLayout("fill,wrap"));
    contentPanel.setBackground(Color.white);
    contentPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY), new EmptyBorder(10, 20, 20, 20)));
    JLabel logoLabel = new JLabel(IconFactory.getIcon("/icons/", "header_logo.png"));
    contentPanel.add(logoLabel, "North");
    JLabel l = new JLabel("<html><center><h2>Version " + AppProperties.getAppVersion() + "</h2></center></html>");
    contentPanel.add(l);
    
    tfTerminalKey = new JTextField();
    tfTerminalKey.setEditable(false);
    contentPanel.add(new JSeparator(), "grow");
    contentPanel.add(tfTerminalKey, "center");
    contentPanel.add(new JSeparator(), "grow");
    contentPanel.add(new JLabel("Copyright © 2017 OROCUBE LLC, www.orocube.com,support@orocube.com"), "center");
    
    tfTerminalKey.setFont(new Font(tfTerminalKey.getFont().getName(), 0, 18));
    tfTerminalKey.setText(TerminalUtil.getSystemUID());
    tfTerminalKey.setBackground(Color.WHITE);
    tfTerminalKey.setBorder(null);
    
    JPanel buttonPanel = new JPanel(new MigLayout("center"));
    
    chkCheckUpdateOnStartUp = new JCheckBox("Automatically check for new update");
    chkCheckUpdateOnStartUp.setSelected(TerminalConfig.isCheckUpdateOnStartUp());
    buttonPanel.add(chkCheckUpdateOnStartUp, "center,wrap");
    
    JButton btnLicenseInfo = new JButton("LICENSE INFO");
    btnLicenseInfo.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        OroLicenseInfoDialog licenseInfoDialog = new OroLicenseInfoDialog();
        licenseInfoDialog.setLicense(Application.getInstance().getLicense());
        licenseInfoDialog.setSize(PosUIManager.getSize(500, 500));
        licenseInfoDialog.setLocationRelativeTo(null);
        licenseInfoDialog.setVisible(true);
      }
    });
    btnLicenseInfo.setPreferredSize(PosUIManager.getSize(100, 40));
    buttonPanel.add(btnLicenseInfo, "split 3");
    
    JButton btnCheckForUpdate = new JButton("CHECK FOR UPDATE");
    btnCheckForUpdate.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        Application.getInstance().checkForUpdate();
      }
    });
    btnCheckForUpdate.setPreferredSize(PosUIManager.getSize(100, 40));
    buttonPanel.add(btnCheckForUpdate);
    
    JButton btnOk = new JButton(Messages.getString("AboutDialog.5"));
    btnOk.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TerminalConfig.setCheckUpdateOnStartUp(chkCheckUpdateOnStartUp.isSelected());
        dispose();
      }
    });
    btnOk.setPreferredSize(PosUIManager.getSize(100, 40));
    buttonPanel.add(btnOk);
    container.add(buttonPanel, "South");
    
    container.add(contentPanel);
    add(container);
  }
}
