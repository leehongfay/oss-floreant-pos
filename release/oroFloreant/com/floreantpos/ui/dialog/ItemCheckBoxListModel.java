package com.floreantpos.ui.dialog;

import com.floreantpos.model.MenuItem;
import com.floreantpos.swing.CheckBoxList.Entry;

public class ItemCheckBoxListModel extends com.floreantpos.swing.CheckBoxListModel<CheckBoxList.Entry>
{
  public ItemCheckBoxListModel(String[] names)
  {
    super(names);
  }
  
  public Object getValueAt(int row, int col)
  {
    CheckBoxList.Entry entry = (CheckBoxList.Entry)getItems().get(row);
    switch (col) {
    case 0: 
      return Boolean.valueOf(checked);
    
    case 1: 
      if ((value instanceof MenuItem)) {
        return ((MenuItem)value).getName();
      }
      return value;
    
    case 2: 
      return com.floreantpos.util.NumberUtil.formatNumber(((MenuItem)value).getPrice());
    }
    
    throw new InternalError();
  }
  

  public void setValueAt(Object value, int row, int col)
  {
    if (col == 0) {
      CheckBoxList.Entry entry = (CheckBoxList.Entry)getItems().get(row);
      checked = value.equals(Boolean.TRUE);
      
      fireTableRowsUpdated(row, row);
    }
  }
  
  public Class getColumnClass(int col)
  {
    switch (col) {
    case 0: 
      return Boolean.class;
    case 1: 
      return String.class;
    case 2: 
      return String.class;
    }
    throw new InternalError();
  }
}
