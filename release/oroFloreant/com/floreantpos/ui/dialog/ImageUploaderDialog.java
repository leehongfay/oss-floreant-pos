package com.floreantpos.ui.dialog;

import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.model.ImageResource;
import com.floreantpos.model.ImageResource.IMAGE_CATEGORY;
import com.floreantpos.model.dao.ImageResourceDAO;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.prefs.Preferences;
import javax.activation.MimetypesFileTypeMap;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.io.FileUtils;




public class ImageUploaderDialog
  extends POSDialog
{
  public static String LAST_USED_FOLDER = "";
  public static ImageResource.IMAGE_CATEGORY imageType = ImageResource.IMAGE_CATEGORY.PRODUCTS;
  public JLabel lblImagePreview;
  public JLabel lblDescription;
  public File imageFile;
  public File[] imageFiles;
  public ImageResource imgResource;
  public POSTextField tfDescription;
  public POSToggleButton tbtnUnlisted;
  public POSToggleButton tbtnFloorPlan;
  public POSToggleButton tbtnProducts;
  public POSToggleButton tbtnPeople;
  private int maxWidth;
  private int maxHeight;
  private JPanel imagePanel;
  private ArrayList<File> imageList;
  private boolean isMultipleSelection;
  private JProgressBar progressBar;
  private JLabel lblProgressing;
  private PosButton btnSelect;
  
  public ImageUploaderDialog(ImageResource imgResource) {
    super(POSUtil.getBackOfficeWindow(), true);
    this.imgResource = imgResource;
    init();
    updateView();
  }
  
  public ImageUploaderDialog() {
    super(POSUtil.getBackOfficeWindow(), true);
    init();
    updateView();
    isMultipleSelection = true;
  }
  

  private void init()
  {
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Image Upload");
    add(titlePanel, "North");
    
    JPanel filterPanel = new JPanel();
    
    ButtonGroup btnGroup = new ButtonGroup();
    
    tbtnUnlisted = new POSToggleButton("UNLISTED");
    tbtnFloorPlan = new POSToggleButton("FLOORPLAN");
    tbtnProducts = new POSToggleButton("PRODUCTS");
    tbtnPeople = new POSToggleButton("PEOPLE");
    
    btnGroup.add(tbtnUnlisted);
    btnGroup.add(tbtnFloorPlan);
    btnGroup.add(tbtnProducts);
    btnGroup.add(tbtnPeople);
    
    filterPanel.add(tbtnUnlisted);
    filterPanel.add(tbtnFloorPlan);
    filterPanel.add(tbtnProducts);
    filterPanel.add(tbtnPeople);
    


    tbtnProducts.setEnabled(true);
    
    JPanel centerPanel = new JPanel(new BorderLayout(5, 5));
    centerPanel.setBorder(new EmptyBorder(20, 5, 20, 5));
    centerPanel.add(filterPanel, "North");
    
    lblImagePreview = new JLabel();
    
    lblDescription = new JLabel("Description");
    tfDescription = new POSTextField(50);
    JLabel lblSeparator = new JLabel(" ");
    imagePanel = new JPanel(new FlowLayout());
    
    imagePanel.add(lblImagePreview, "center");
    
    JScrollPane scrollPane = new JScrollPane(imagePanel, 20, 30);
    
    scrollPane.setPreferredSize(new Dimension(350, 220));
    scrollPane.setVisible(true);
    
    centerPanel.add(scrollPane);
    
    JPanel desPanel = new JPanel();
    desPanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 20));
    desPanel.setLayout(new BorderLayout(5, 0));
    
    desPanel.add(lblDescription, "West");
    
    desPanel.add(tfDescription, "Center");
    
    centerPanel.add(desPanel, "South");
    
    JPanel buttonPanel = new JPanel(new MigLayout("fillx, hidemode 3", "[grow][grow][grow]", ""));
    
    progressBar = new JProgressBar();
    progressBar.setBounds(40, 40, 160, 30);
    progressBar.setValue(0);
    progressBar.setMinimum(0);
    progressBar.setStringPainted(true);
    
    buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 20, 5, 20));
    btnSelect = new PosButton("Select Image");
    
    btnSelect.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          if (!isMultipleSelection) {
            selectImage();
          }
          else {
            selectBulkImage();
          }
        }
        catch (PosException e1) {
          JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", 0);
        } catch (Exception e2) {
          JOptionPane.showMessageDialog(null, "Unsupported File Format", "Error", 0);
        }
        
      }
    });
    PosButton btnOk = new PosButton("Done");
    btnOk.setPreferredSize(PosUIManager.getSize(100, 0));
    
    btnOk.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doOk();
        setCanceled(false);
      }
      

    });
    PosButton btnCancel = new PosButton("Cancel");
    
    btnCancel.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        setCanceled(true);
        dispose();
      }
      
    });
    lblProgressing = new JLabel("Importing Images: ");
    lblProgressing.setVisible(false);
    
    buttonPanel.add(btnSelect, "");
    buttonPanel.add(lblProgressing, "left,split 2");
    buttonPanel.add(progressBar, "");
    buttonPanel.add(btnOk, "growy,right,skip 2, split 2");
    buttonPanel.add(btnCancel, "");
    add(centerPanel, "Center");
    add(buttonPanel, "South");
  }
  

  public void doOk()
  {
    if (!isMultipleSelection) {
      doSaveSingleImage();
      ImageResourceDAO.getInstance().saveOrUpdate(imgResource);
    }
    else {
      doSaveMultipleImage();
    }
    dispose();
  }
  
















  private void doSaveSingleImage()
  {
    try
    {
      if (imgResource == null) {
        imgResource = new ImageResource();
      }
      imgResource.setDescription(tfDescription.getText());
      File imageFile = (File)lblImagePreview.getClientProperty("image");
      if (imageFile != null) {
        imgResource.setImageData(POSUtil.convertImageToBlob(imageFile));
      }
      
      if (tbtnUnlisted.isSelected()) {
        imgResource.setImageCategoryNum(Integer.valueOf(ImageResource.IMAGE_CATEGORY.UNLISTED.getType()));
        imageType = ImageResource.IMAGE_CATEGORY.UNLISTED;
      }
      else if (tbtnFloorPlan.isSelected()) {
        imgResource.setImageCategoryNum(Integer.valueOf(ImageResource.IMAGE_CATEGORY.FLOORPLAN.getType()));
        imageType = ImageResource.IMAGE_CATEGORY.FLOORPLAN;
      }
      else if (tbtnPeople.isSelected()) {
        imgResource.setImageCategoryNum(Integer.valueOf(ImageResource.IMAGE_CATEGORY.PEOPLE.getType()));
        imageType = ImageResource.IMAGE_CATEGORY.PEOPLE;
      }
      else if (tbtnProducts.isSelected()) {
        imgResource.setImageCategoryNum(Integer.valueOf(ImageResource.IMAGE_CATEGORY.PRODUCTS.getType()));
        imageType = ImageResource.IMAGE_CATEGORY.PRODUCTS;
      }
    } catch (Exception e) {
      PosLog.error(ImageUploaderDialog.class, e.getMessage(), e);
    }
  }
  
  public void doSaveMultipleImage()
  {
    try {
      List<ImageResource> imageResourceList = new ArrayList();
      for (Iterator iterator = imageList.iterator(); iterator.hasNext();) {
        File file = (File)iterator.next();
        imgResource = new ImageResource();
        
        if (file != null)
        {
          imgResource.setImageData(POSUtil.convertImageToBlob(file));
        }
        
        if (tbtnUnlisted.isSelected()) {
          imgResource.setImageCategoryNum(Integer.valueOf(ImageResource.IMAGE_CATEGORY.UNLISTED.getType()));
          imageType = ImageResource.IMAGE_CATEGORY.UNLISTED;
        }
        else if (tbtnFloorPlan.isSelected()) {
          imgResource.setImageCategoryNum(Integer.valueOf(ImageResource.IMAGE_CATEGORY.FLOORPLAN.getType()));
          imageType = ImageResource.IMAGE_CATEGORY.FLOORPLAN;
        }
        else if (tbtnPeople.isSelected()) {
          imgResource.setImageCategoryNum(Integer.valueOf(ImageResource.IMAGE_CATEGORY.PEOPLE.getType()));
          imageType = ImageResource.IMAGE_CATEGORY.PEOPLE;
        }
        else if (tbtnProducts.isSelected()) {
          imgResource.setImageCategoryNum(Integer.valueOf(ImageResource.IMAGE_CATEGORY.PRODUCTS.getType()));
          imageType = ImageResource.IMAGE_CATEGORY.PRODUCTS;
        }
        imageResourceList.add(imgResource);
      }
      ImageResourceDAO.getInstance().saveOrUpdate(imageResourceList);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  public void selectBulkImage() throws Exception {
    try {
      Preferences prefs = Preferences.userRoot().node(getClass().getName());
      JFileChooser fileChooser = null;
      if (LAST_USED_FOLDER != null) {
        fileChooser = new JFileChooser(prefs.get(LAST_USED_FOLDER, new File(".").getAbsolutePath()));
      }
      else {
        fileChooser = new JFileChooser();
      }
      fileChooser.setMultiSelectionEnabled(true);
      fileChooser.setFileSelectionMode(2);
      fileChooser.setAcceptAllFileFilterUsed(false);
      int option = fileChooser.showOpenDialog(this);
      
      imageList = new ArrayList();
      if (option == 0)
      {
        File[] selectedFiles = fileChooser.getSelectedFiles();
        for (File file : selectedFiles) {
          if (file.isDirectory()) {
            File[] f = file.listFiles();
            for (int i = 0; i < f.length; i++) {
              if (f[i].isDirectory()) {
                addFileIfImageType(f[i]);
              }
              else {
                addFileIfImageType(f[i]);
              }
            }
          }
          else
          {
            addFileIfImageType(file);
          }
        }
      }
      else {
        return;
      }
    } catch (Exception e) {
      throw e;
    }
    System.out.println();
  }
  
  boolean cancel = false;
  
  public void showImages() {
    if (!isMultipleSelection) {
      progressBar.setVisible(false);
      return;
    }
    btnSelect.setVisible(false);
    ExecutorService executorService = Executors.newSingleThreadExecutor();
    
    executorService.execute(new Runnable() {
      public void run() {
        try {
          if (cancel) {
            return;
          }
          i = 1;
          progressBar.setMaximum(imageList.size());
          lblProgressing.setVisible(true);
          for (File file : imageList) {
            JLabel lblImagePreview = new JLabel();
            Image rImage = ImageIO.read(file);
            
            lblImagePreview.putClientProperty("image", file);
            
            ImageIcon imageIcon = new ImageIcon(rImage.getScaledInstance(500, 500, 1));
            lblImagePreview.setIcon(imageIcon);
            imagePanel.add(lblImagePreview);
            imagePanel.revalidate();
            imagePanel.repaint();
            Thread.sleep(500L);
            progressBar.setValue(i);
            i++;
          }
        } catch (Exception e) { int i;
          e.printStackTrace();
        }
      }
    });
  }
  
  private void addFileIfImageType(File file)
  {
    String mimetype = new MimetypesFileTypeMap().getContentType(file);
    String type = mimetype.split("/")[0];
    if (type.equals("image")) {
      imageList.add(file);
    }
  }
  
  public void selectImage() throws Exception {
    try {
      Preferences prefs = Preferences.userRoot().node(getClass().getName());
      JFileChooser fileChooser = null;
      if (LAST_USED_FOLDER != null) {
        fileChooser = new JFileChooser(prefs.get(LAST_USED_FOLDER, new File(".").getAbsolutePath()));
      }
      else {
        fileChooser = new JFileChooser();
      }
      fileChooser.setMultiSelectionEnabled(false);
      fileChooser.setFileSelectionMode(0);
      int option = fileChooser.showOpenDialog(this);
      
      if (option == 0) {
        imageFile = fileChooser.getSelectedFile();
        prefs.put(LAST_USED_FOLDER, imageFile.getPath());
        
        byte[] itemImage = FileUtils.readFileToByteArray(imageFile);
        int imageSize = itemImage.length / 1024;
        
        Image rImage = null;
        if ((imageSize > 500) && (maxWidth <= 0) && (maxHeight <= 0)) {
          if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "The image is too large. Do you want to resize?", "Confirm") != 0)
          {
            itemImage = null;
            throw new PosException("The image is too large. Please select an image within 500kb in size");
          }
          maxWidth = 200;
          maxHeight = 200;
        }
        
        if ((maxWidth > 0) && (maxHeight > 0)) {
          rImage = ImageIO.read(imageFile).getScaledInstance(maxWidth, maxHeight, 4);
          BufferedImage bufferedImage = new BufferedImage(rImage.getWidth(null), rImage.getHeight(null), 2);
          
          Graphics2D bGr = bufferedImage.createGraphics();
          bGr.drawImage(rImage, 0, 0, null);
          bGr.dispose();
          File outputfile = new File("saved.png");
          try {
            ImageIO.write(bufferedImage, "png", outputfile);
          } catch (IOException e) {
            POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage());
          }
          lblImagePreview.putClientProperty("image", outputfile);
        }
        else {
          rImage = ImageIO.read(imageFile);
          lblImagePreview.putClientProperty("image", imageFile);
        }
        
        lblImagePreview.setText("");
        lblImagePreview.setIcon(new ImageIcon(rImage));
      }
    } catch (Exception e) {
      throw e;
    }
  }
  
  private void updateView() {
    ImageResource.IMAGE_CATEGORY selectedImageType = imageType;
    if ((imgResource != null) && (imgResource.getImageCategory() != null)) {
      selectedImageType = imgResource.getImageCategory();
    }
    
    if (selectedImageType == ImageResource.IMAGE_CATEGORY.UNLISTED) {
      tbtnUnlisted.setSelected(true);
    }
    else if (selectedImageType == ImageResource.IMAGE_CATEGORY.FLOORPLAN) {
      tbtnFloorPlan.setSelected(true);
    }
    else if (selectedImageType == ImageResource.IMAGE_CATEGORY.PRODUCTS) {
      tbtnProducts.setSelected(true);
    }
    else if (selectedImageType == ImageResource.IMAGE_CATEGORY.PEOPLE) {
      tbtnPeople.setSelected(true);
    }
    if (imgResource == null) {
      return;
    }
    ImageIcon rImage = imgResource.getScaledImage(120, 120);
    if (rImage == null) {
      return;
    }
    lblImagePreview.setText("");
    lblImagePreview.setIcon(rImage);
    
    Image image = rImage.getImage();
    BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), 2);
    
    Graphics2D bGr = bufferedImage.createGraphics();
    bGr.drawImage(image, 0, 0, null);
    bGr.dispose();
    File outputfile = new File("saved.png");
    try {
      ImageIO.write(bufferedImage, "png", outputfile);
    } catch (IOException e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage());
    }
    lblImagePreview.putClientProperty("image", outputfile);
    
    if (imgResource.getDescription() != null) {
      tfDescription.setText(imgResource.getDescription());
    }
  }
  
  public File getSelectedImageFile() {
    return imageFile;
  }
  
  public List<File> getSelectedImageFiles() {
    return imageList;
  }
  
  public void setImageMaximumSize(int maxWidth, int maxHeight) {
    this.maxWidth = maxWidth;
    this.maxHeight = maxHeight;
  }
  
  public void open()
  {
    showImages();
    super.open();
  }
}
