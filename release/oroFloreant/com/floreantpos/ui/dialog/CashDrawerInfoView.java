package com.floreantpos.ui.dialog;

import com.floreantpos.model.CashDrawer;
import com.floreantpos.print.PosPrintService;
import com.floreantpos.services.report.CashDrawerReportService;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.ui.views.TicketReceiptView;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import net.sf.jasperreports.engine.JasperPrint;





























public class CashDrawerInfoView
  extends JPanel
{
  private JPanel reportViewPanel;
  private CashDrawer cashDrawerReport;
  private boolean summaryReport;
  
  public CashDrawerInfoView(CashDrawer report)
  {
    this(report, false);
  }
  
  public CashDrawerInfoView(CashDrawer report, boolean summaryReport) {
    cashDrawerReport = report;
    this.summaryReport = summaryReport;
    initComponents();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    reportViewPanel = new JPanel(new BorderLayout());
    PosScrollPane scrollPane = new PosScrollPane(reportViewPanel);
    add(scrollPane);
  }
  
  public void createReport(boolean calculate) throws Exception {
    reportViewPanel.removeAll();
    JasperPrint jasperPrint = null;
    
    if ((calculate) || ((cashDrawerReport.getReportTime() == null) && (!summaryReport)))
    {
      CashDrawerReportService cashDrawerReportService2 = new CashDrawerReportService(cashDrawerReport);
      cashDrawerReportService2.populateReport();
    }
    
    if (summaryReport) {
      jasperPrint = PosPrintService.populateCashDrawerSummaryReportParameters(cashDrawerReport);
    }
    else {
      jasperPrint = PosPrintService.populateDrawerPullReportParameters(cashDrawerReport);
    }
    TicketReceiptView receiptView = new TicketReceiptView(jasperPrint);
    receiptView.setZoom(1.25F);
    reportViewPanel.add(receiptView.getReportPanel());
    reportViewPanel.revalidate();
    reportViewPanel.repaint();
  }
}
