package com.floreantpos.ui.dialog;

import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.NumericKeypad;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.util.POSUtil;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;


















public class ReconcileAmountSelectionDialog
  extends OkCancelOptionDialog
{
  private DoubleTextField tfNumber;
  private DoubleTextField tfTips;
  
  public ReconcileAmountSelectionDialog(String titledText, String dialogTitle, double defaultValue)
  {
    super(POSUtil.getFocusedWindow());
    init();
    setDefaultValue(defaultValue);
    setTitle(dialogTitle);
    setCaption(titledText);
  }
  
  private void init() {
    JPanel contentPane = getContentPanel();
    contentPane.setLayout(new MigLayout("inset 0,fillx", "", ""));
    
    tfNumber = new DoubleTextField();
    tfNumber.setFont(tfNumber.getFont().deriveFont(1, PosUIManager.getNumberFieldFontSize()));
    tfNumber.setFocusable(true);
    tfNumber.requestFocus();
    tfNumber.setBackground(Color.WHITE);
    JLabel lblAmount = new JLabel("Amount:");
    lblAmount.setFont(lblAmount.getFont().deriveFont(0, 18.0F));
    contentPane.add(lblAmount, "alignx right");
    contentPane.add(tfNumber, "alignx right,height 40px,aligny top,grow,wrap");
    
    tfTips = new DoubleTextField();
    tfTips.setFont(tfNumber.getFont().deriveFont(1, PosUIManager.getNumberFieldFontSize()));
    tfTips.setBackground(Color.WHITE);
    JLabel lblTips = new JLabel("Tips:");
    lblTips.setFont(lblTips.getFont().deriveFont(0, 18.0F));
    contentPane.add(lblTips, "alignx right");
    contentPane.add(tfTips, "alignx right,height 40px,aligny top,grow,wrap");
    
    NumericKeypad numericKeypad = new NumericKeypad();
    contentPane.add(numericKeypad, "grow,gaptop 10,span");
  }
  
  public void doOk()
  {
    setCanceled(false);
    dispose();
  }
  
  public void setTitle(String title) {
    super.setCaption(title);
    super.setTitle(title);
  }
  
  public double getValue() {
    return tfNumber.getDoubleOrZero();
  }
  
  public double getTipsAmount() {
    return tfTips.getDoubleOrZero();
  }
  
  public void setDefaultValue(double defaultValue) {
    tfNumber.setText(String.valueOf(defaultValue));
  }
  
  public void setTipsAmount(double tipsAmount) {
    tfTips.setText(String.valueOf(tipsAmount));
  }
}
