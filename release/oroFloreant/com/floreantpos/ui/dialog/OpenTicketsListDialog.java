package com.floreantpos.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.views.order.CashierModeNextActionDialog;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;



public class OpenTicketsListDialog
  extends POSDialog
{
  private List<Ticket> openTickets;
  private OpenTicketListTableModel tableModel;
  private PosButton btnClose;
  private PosButton btnScrollDown;
  private PosButton btnScrollUp;
  private PosButton btnTransferServer;
  private PosButton btnVoid;
  private JScrollPane jScrollPane1;
  private JTable openTicketListTable;
  private TitlePanel titlePanel;
  private TransparentPanel transparentPanel1;
  private TransparentPanel transparentPanel2;
  private TransparentPanel transparentPanel3;
  private TransparentPanel transparentPanel4;
  private DefaultListSelectionModel selectionModel;
  
  public OpenTicketsListDialog()
  {
    init();
  }
  
  private void init() {
    initComponents();
    setTitle(POSConstants.ACTIVE_TICKETS);
    titlePanel.setTitle(POSConstants.ACTIVE_TICKETS_BEFORE_DRAWER_RESET);
    
    TicketDAO dao = new TicketDAO();
    
    if (TerminalConfig.isCashierMode()) {
      openTickets = dao.findOpenTicketsForUser(Application.getCurrentUser());
    }
    else {
      openTickets = dao.findOpenTickets();
    }
    
    tableModel = new OpenTicketListTableModel();
    openTicketListTable.setModel(tableModel);
    openTicketListTable.setDefaultRenderer(Object.class, new TicketTableCellRenderer());
    openTicketListTable.setRowHeight(40);
    
    selectionModel = new DefaultListSelectionModel();
    selectionModel.setSelectionMode(0);
    openTicketListTable.setSelectionModel(selectionModel);
    
    if (TerminalConfig.isCashierMode()) {
      selectionModel.addListSelectionListener(new ListSelectionListener()
      {
        public void valueChanged(ListSelectionEvent e) {
          Ticket ticket = (Ticket)openTickets.get(openTicketListTable.getSelectedRow());
          ticket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
          OrderView.getInstance().setCurrentTicket(ticket);
          
          dispose();
        }
      });
    }
  }
  





  private void initComponents()
  {
    titlePanel = new TitlePanel();
    transparentPanel1 = new TransparentPanel();
    transparentPanel3 = new TransparentPanel();
    
    btnClose = new PosButton();
    transparentPanel4 = new TransparentPanel();
    btnScrollUp = new PosButton();
    btnScrollDown = new PosButton();
    transparentPanel2 = new TransparentPanel();
    jScrollPane1 = new JScrollPane();
    openTicketListTable = new JTable();
    
    setDefaultCloseOperation(2);
    getContentPane().add(titlePanel, "North");
    
    transparentPanel1.setLayout(new BorderLayout());
    
    transparentPanel3.setLayout(new FlowLayout(2));
    
    transparentPanel3.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 5));
    
    if (!TerminalConfig.isCashierMode()) {
      btnVoid = new PosButton();
      btnVoid.setText(POSConstants.VOID);
      btnVoid.setPreferredSize(new Dimension(100, 50));
      btnVoid.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent evt) {
          OpenTicketsListDialog.this.doVoidTicket(evt);
        }
        
      });
      transparentPanel3.add(btnVoid);
      
      btnTransferServer = new PosButton();
      btnTransferServer.setText("<html><body><center>TRANSFER<br>SERVER</center></body></html>");
      btnTransferServer.setPreferredSize(new Dimension(100, 50));
      btnTransferServer.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent evt) {
          OpenTicketsListDialog.this.doTransferServer(evt);
        }
        
      });
      transparentPanel3.add(btnTransferServer);
    }
    
    btnClose.setText(POSConstants.CLOSE);
    btnClose.setPreferredSize(new Dimension(100, 50));
    btnClose.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        OpenTicketsListDialog.this.doClose(evt);
      }
      
    });
    transparentPanel3.add(btnClose);
    
    transparentPanel1.add(transparentPanel3, "Center");
    
    transparentPanel4.setBorder(BorderFactory.createEmptyBorder(1, 5, 1, 5));
    btnScrollUp.setIcon(IconFactory.getIcon("/ui_icons/", "up.png"));
    btnScrollUp.setPreferredSize(new Dimension(80, 50));
    btnScrollUp.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        OpenTicketsListDialog.this.doScrollUp(evt);
      }
      
    });
    transparentPanel4.add(btnScrollUp);
    
    btnScrollDown.setIcon(IconFactory.getIcon("/ui_icons/", "down.png"));
    btnScrollDown.setPreferredSize(new Dimension(80, 50));
    btnScrollDown.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        OpenTicketsListDialog.this.doScrollDown(evt);
      }
      
    });
    transparentPanel4.add(btnScrollDown);
    
    transparentPanel1.add(transparentPanel4, "West");
    
    getContentPane().add(transparentPanel1, "South");
    
    transparentPanel2.setLayout(new BorderLayout(0, 5));
    
    transparentPanel2.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
    openTicketListTable.setModel(new DefaultTableModel(new Object[0][], new String[0]));
    



    jScrollPane1.setViewportView(openTicketListTable);
    
    transparentPanel2.add(jScrollPane1, "Center");
    
    getContentPane().add(transparentPanel2, "Center");
    
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    setBounds((width - 646) / 2, (height - 435) / 2, 646, 435);
  }
  
  private void doScrollUp(ActionEvent evt) {
    int selectedRow = openTicketListTable.getSelectedRow();
    int rowCount = tableModel.getRowCount();
    
    if (selectedRow <= 0) {
      selectedRow = rowCount - 1;
    }
    else if (selectedRow > rowCount - 1) {
      selectedRow = rowCount - 1;
    }
    else {
      selectedRow--;
    }
    openTicketListTable.transferFocus();
    selectionModel.setLeadSelectionIndex(selectedRow);
    Rectangle cellRect = openTicketListTable.getCellRect(selectedRow, 0, false);
    openTicketListTable.scrollRectToVisible(cellRect);
  }
  
  private void doScrollDown(ActionEvent evt) {
    int selectedRow = openTicketListTable.getSelectedRow();
    int rowCount = tableModel.getRowCount();
    
    if (selectedRow < 0) {
      selectedRow = 0;
    }
    else if (selectedRow >= rowCount - 1) {
      selectedRow = 0;
    }
    else {
      selectedRow++;
    }
    openTicketListTable.transferFocus();
    selectionModel.setLeadSelectionIndex(selectedRow);
    Rectangle cellRect = openTicketListTable.getCellRect(selectedRow, 0, false);
    openTicketListTable.scrollRectToVisible(cellRect);
  }
  
  private Ticket getSelectedTicket() {
    int row = openTicketListTable.getSelectedRow();
    if (row < 0) {
      POSMessageDialog.showError(this, POSConstants.SELECT_TICKET);
      return null;
    }
    return (Ticket)openTickets.get(row);
  }
  
  private void doClose(ActionEvent evt) {
    canceled = false;
    dispose();
    
    if (TerminalConfig.isCashierMode()) {
      String message = Messages.getString("OpenTicketsListDialog.0");
      CashierModeNextActionDialog dialog = new CashierModeNextActionDialog(message);
      dialog.open();
    }
  }
  
  private void doTransferServer(ActionEvent evt) {
    try {
      Ticket ticket = getSelectedTicket();
      
      if (ticket != null) {
        UserListDialog dialog = new UserListDialog();
        dialog.open();
        if (!dialog.isCanceled()) {
          User selectedUser = dialog.getSelectedUser();
          if (!ticket.getOwner().equals(selectedUser)) {
            ticket.setOwner(selectedUser);
            TicketDAO.getInstance().update(ticket);
            openTicketListTable.repaint();
          }
        }
      }
    } catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE);
    }
  }
  
  private void doPrintTicket(ActionEvent evt) {
    JOptionPane.showMessageDialog(this, Messages.getString("OpenTicketsListDialog.6"));
  }
  
  private void doVoidTicket(ActionEvent evt) {
    Ticket ticket = getSelectedTicket();
    if (ticket == null) {
      return;
    }
    
    Ticket ticketToVoid = TicketDAO.getInstance().loadFullTicket(ticket.getId());
    
    VoidTicketDialog dialog = new VoidTicketDialog();
    dialog.setTicket(ticketToVoid);
    dialog.open();
    
    if (!dialog.isCanceled()) {
      tableModel.removeTicket(ticketToVoid);
    }
  }
  







  class OpenTicketListTableModel
    extends AbstractTableModel
  {
    OpenTicketListTableModel() {}
    






    public int getRowCount()
    {
      if (openTickets == null) {
        return 0;
      }
      return openTickets.size();
    }
    
    public int getColumnCount() {
      return 4;
    }
    
    public String getColumnName(int column)
    {
      switch (column) {
      case 0: 
        return POSConstants.TICKET_ID;
      
      case 1: 
        return POSConstants.SERVER;
      
      case 2: 
        return POSConstants.DATE_TIME;
      
      case 3: 
        return POSConstants.TOTAL;
      }
      return null;
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      if (openTickets == null) {
        return null;
      }
      Ticket ticket = (Ticket)openTickets.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return Long.valueOf(ticket.getId());
      
      case 1: 
        return ticket.getOwner().toString();
      
      case 2: 
        return ticket.getCreateDate();
      
      case 3: 
        return Double.valueOf(ticket.getTotalAmountWithTips().doubleValue());
      }
      
      return null;
    }
    
    void removeTicket(Ticket ticket) {
      openTickets.remove(ticket);
      fireTableDataChanged();
    }
  }
  
  class TicketTableCellRenderer extends DefaultTableCellRenderer {
    Font font = getFont().deriveFont(1, 12.0F);
    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM-dd-yy hh:mm a");
    String currencySymbol = CurrencyUtil.getCurrencySymbol();
    
    TicketTableCellRenderer() {}
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) { JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      label.setFont(font);
      
      if ((value instanceof Date)) {
        label.setText(dateFormat.format(value));
        label.setHorizontalAlignment(0);
      }
      else if ((value instanceof Double)) {
        label.setText(currencySymbol + NumberUtil.formatNumber(Double.valueOf(((Double)value).doubleValue())));
        label.setHorizontalAlignment(4);
      }
      else {
        label.setHorizontalAlignment(2);
      }
      return label;
    }
  }
}
