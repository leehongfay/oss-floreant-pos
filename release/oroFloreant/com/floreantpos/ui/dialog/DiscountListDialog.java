package com.floreantpos.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketDiscount;
import com.floreantpos.swing.PosButton;
import com.floreantpos.util.NumberUtil;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.AbstractTableModel;




















public class DiscountListDialog
  extends POSDialog
  implements ActionListener
{
  private JPanel contentPane;
  private PosButton buttonOK;
  private PosButton buttonCancel;
  private PosButton btnScrollUp;
  private PosButton btnScrollDown;
  private PosButton btnDeleteSelected;
  private JTable tableDiscounts;
  private List<Ticket> tickets;
  private DiscountViewTableModel discountViewTableModel;
  private DefaultListSelectionModel selectionModel;
  private boolean modified = false;
  























































































  public DiscountListDialog(List<Ticket> tickets)
  {
    $$$setupUI$$$();this.tickets = tickets;setSize(700, 500);discountViewTableModel = new DiscountViewTableModel();tableDiscounts.setModel(discountViewTableModel);selectionModel = new DefaultListSelectionModel();selectionModel.setSelectionMode(0);tableDiscounts.setSelectionModel(selectionModel);btnScrollUp.setActionCommand("scrollUP");btnScrollDown.setActionCommand("scrollDown");btnScrollUp.addActionListener(this);btnScrollDown.addActionListener(this);setContentPane(contentPane);setModal(true);getRootPane().setDefaultButton(buttonOK);buttonOK.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        DiscountListDialog.this.onOK();
      }
      

    }
    



























































      );buttonCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        DiscountListDialog.this.onCancel();

      }
      

    });setDefaultCloseOperation(0);
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        DiscountListDialog.this.onCancel();

      }
      

    });contentPane.registerKeyboardAction(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        DiscountListDialog.this.onCancel();
      }
    }, KeyStroke.getKeyStroke(27, 0), 1);
    btnDeleteSelected.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        DiscountListDialog.this.doDeleteSelection();
      }
    });
  }
  
  private void doDeleteSelection() {
    try {
      int selectedRow = selectionModel.getLeadSelectionIndex();
      if (selectedRow < 0) {
        POSMessageDialog.showError(this, POSConstants.SELECT_ITEM_TO_DELETE);
        return;
      }
      if (ConfirmDeleteDialog.showMessage(this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
        return;
      }
      
      Object object = discountViewTableModel.get(selectedRow);
      modified = discountViewTableModel.delete((TicketCoupon)object);
    } catch (Exception e) {
      POSMessageDialog.showError(this, Messages.getString("DiscountListDialog.2"), e);
    }
  }
  
  private void onOK() {
    tickets = null;
    setCanceled(false);
    dispose();
  }
  
  private void onCancel() {
    tickets = null;
    setCanceled(true);
    dispose();
  }
  













  private void $$$setupUI$$$()
  {
    contentPane = new JPanel();
    contentPane.setLayout(new GridLayoutManager(2, 1, new Insets(10, 10, 10, 10), -1, -1));
    JPanel panel1 = new JPanel();
    panel1.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
    contentPane.add(panel1, new GridConstraints(1, 0, 1, 1, 2, 1, 3, 1, null, null, null, 0, false));
    
    Spacer spacer1 = new Spacer();
    panel1.add(spacer1, new GridConstraints(0, 0, 1, 1, 0, 1, 4, 1, null, null, null, 0, false));
    
    JPanel panel2 = new JPanel();
    panel2.setLayout(new FlowLayout(1, 5, 5));
    panel1.add(panel2, new GridConstraints(0, 1, 1, 1, 0, 3, 3, 3, null, null, null, 0, false));
    
    btnDeleteSelected = new PosButton();
    btnDeleteSelected.setIcon(IconFactory.getIcon("/ui_icons/", "delete.png"));
    btnDeleteSelected.setPreferredSize(new Dimension(140, 50));
    btnDeleteSelected.setText(Messages.getString("DiscountListDialog.5"));
    panel2.add(btnDeleteSelected);
    buttonOK = new PosButton();
    buttonOK.setIcon(IconFactory.getIcon("/ui_icons/", "finish.png"));
    buttonOK.setPreferredSize(new Dimension(120, 50));
    buttonOK.setText(POSConstants.OK);
    panel2.add(buttonOK);
    buttonCancel = new PosButton();
    buttonCancel.setIcon(IconFactory.getIcon("/ui_icons/", "cancel.png"));
    buttonCancel.setPreferredSize(new Dimension(120, 50));
    buttonCancel.setText(POSConstants.CANCEL);
    panel2.add(buttonCancel);
    JPanel panel3 = new JPanel();
    panel3.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
    contentPane.add(panel3, new GridConstraints(0, 0, 1, 1, 0, 3, 5, 5, null, new Dimension(458, 310), null, 0, false));
    

    JScrollPane scrollPane1 = new JScrollPane();
    panel3.add(scrollPane1, new GridConstraints(0, 0, 1, 1, 0, 3, 5, 5, null, null, null, 0, false));
    

    tableDiscounts = new JTable();
    scrollPane1.setViewportView(tableDiscounts);
    JPanel panel4 = new JPanel();
    panel4.setLayout(new FormLayout("fill:p:grow", "center:d:grow,top:4dlu:noGrow,center:d:grow"));
    panel3.add(panel4, new GridConstraints(0, 1, 1, 1, 0, 3, 5, 5, null, null, null, 0, false));
    

    btnScrollUp = new PosButton();
    btnScrollUp.setIcon(IconFactory.getIcon("/ui_icons/", "up.png"));
    btnScrollUp.setPreferredSize(new Dimension(50, 50));
    btnScrollUp.setText("");
    CellConstraints cc = new CellConstraints();
    panel4.add(btnScrollUp, cc.xy(1, 1, CellConstraints.CENTER, CellConstraints.BOTTOM));
    btnScrollDown = new PosButton();
    btnScrollDown.setIcon(IconFactory.getIcon("/ui_icons/", "down.png"));
    btnScrollDown.setPreferredSize(new Dimension(50, 50));
    btnScrollDown.setText("");
    panel4.add(btnScrollDown, cc.xy(1, 3, CellConstraints.CENTER, CellConstraints.TOP));
  }
  


  public JComponent $$$getRootComponent$$$()
  {
    return contentPane;
  }
  
  class DiscountViewTableModel extends AbstractTableModel {
    String[] columnNames = {
      Messages.getString("DiscountListDialog.18"), Messages.getString("DiscountListDialog.19"), Messages.getString("DiscountListDialog.20") };
    ArrayList rows = new ArrayList();
    
    DiscountViewTableModel() {
      for (Iterator iter = tickets.iterator(); iter.hasNext();) {
        ticket = (Ticket)iter.next();
        List<TicketDiscount> coupons = ticket.getDiscounts();
        
        if (coupons != null) {
          for (TicketDiscount coupon : coupons) {
            DiscountListDialog.TicketCoupon ticketDiscount = new DiscountListDialog.TicketCoupon(DiscountListDialog.this, ticket, coupon);
            rows.add(ticketDiscount);
          }
        }
      }
      Ticket ticket;
    }
    
    public int getRowCount()
    {
      return rows.size();
    }
    
    public int getColumnCount() {
      return columnNames.length;
    }
    
    public String getColumnName(int column)
    {
      return columnNames[column];
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      DiscountListDialog.TicketCoupon ticketDiscount = (DiscountListDialog.TicketCoupon)rows.get(rowIndex);
      Object discountObject = ticketDiscount.getDiscountObject();
      
      switch (columnIndex) {
      case 0: 
        if ((discountObject instanceof TicketDiscount)) {
          return ((TicketDiscount)discountObject).getName();
        }
        return null;
      
      case 1: 
        if ((discountObject instanceof TicketDiscount)) {
          return com.floreantpos.model.Discount.COUPON_TYPE_NAMES[((TicketDiscount)discountObject).getType().intValue()];
        }
        return null;
      
      case 2: 
        if ((discountObject instanceof TicketDiscount)) {
          return NumberUtil.formatNumber(((TicketDiscount)discountObject).getValue());
        }
        return null;
      }
      
      return null;
    }
    
    public boolean delete(DiscountListDialog.TicketCoupon ticketDiscount) {
      Ticket ticket = ticketDiscount.getTicket();
      Object object = ticketDiscount.getDiscountObject();
      
      if ((object instanceof DiscountListDialog.TicketCoupon)) {
        boolean b = ticket.getDiscounts().remove(object);
        rows.remove(ticketDiscount);
        fireTableDataChanged();
        return b;
      }
      return false;
    }
    
    public Object get(int index) {
      return rows.get(index);
    }
  }
  
  public void actionPerformed(ActionEvent e) {
    if ("scrollUP".equals(e.getActionCommand()))
    {
      int selectedRow = selectionModel.getLeadSelectionIndex();
      
      if (selectedRow <= 0) {
        selectedRow = 0;
      }
      else {
        selectedRow--;
      }
      
      selectionModel.setLeadSelectionIndex(selectedRow);
      Rectangle cellRect = tableDiscounts.getCellRect(selectedRow, 0, false);
      tableDiscounts.scrollRectToVisible(cellRect);
    }
    else if ("scrollDown".equals(e.getActionCommand())) {
      int selectedRow = selectionModel.getLeadSelectionIndex();
      
      if (selectedRow < 0) {
        selectedRow = 0;
      }
      else if (selectedRow < discountViewTableModel.getRowCount() - 1)
      {


        selectedRow++;
      }
      
      selectionModel.setLeadSelectionIndex(selectedRow);
      Rectangle cellRect = tableDiscounts.getCellRect(selectedRow, 0, false);
      tableDiscounts.scrollRectToVisible(cellRect);
    }
  }
  
  public boolean isModified() {
    return modified;
  }
  
  class TicketCoupon
  {
    private Ticket ticket;
    private Object discountObject;
    
    public TicketCoupon() {}
    
    public TicketCoupon(Ticket ticket, Object discount)
    {
      this.ticket = ticket;
      discountObject = discount;
    }
    
    public Object getDiscountObject() {
      return discountObject;
    }
    
    public void setDiscountObject(Object discount) {
      discountObject = discount;
    }
    
    public Ticket getTicket() {
      return ticket;
    }
    
    public void setTicket(Ticket ticket) {
      this.ticket = ticket;
    }
    
    public boolean equals(Object obj)
    {
      if (!(obj instanceof TicketCoupon)) {
        return false;
      }
      
      TicketCoupon other = (TicketCoupon)obj;
      return discountObject.equals(discountObject);
    }
  }
}
