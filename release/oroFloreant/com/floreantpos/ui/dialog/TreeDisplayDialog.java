package com.floreantpos.ui.dialog;

import com.floreantpos.config.AppProperties;
import com.floreantpos.util.POSUtil;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

public class TreeDisplayDialog extends OkCancelOptionDialog
{
  JTree tree;
  
  public TreeDisplayDialog(JTree tree)
  {
    super(POSUtil.getFocusedWindow());
    setTitle(AppProperties.getAppName());
    this.tree = tree;
    initComponents();
  }
  
  private void initComponents() {
    setDefaultCloseOperation(2);
    
    setCaption("InventoryLocationTree Display");
    
    getContentPanel().add(new javax.swing.JScrollPane(tree));
    
    pack();
  }
  
  public void doOk() {
    setCanceled(false);
    dispose();
  }
  
  public DefaultMutableTreeNode getPath() {
    return (DefaultMutableTreeNode)tree.getLastSelectedPathComponent();
  }
}
