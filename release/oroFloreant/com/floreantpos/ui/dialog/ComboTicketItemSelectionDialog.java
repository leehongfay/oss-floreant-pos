package com.floreantpos.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.PosException;
import com.floreantpos.main.Application;
import com.floreantpos.model.ComboGroup;
import com.floreantpos.model.ComboTicketItem;
import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.ModifiableTicketItem;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemModifier;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.ScrollableFlowPanel;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.ticket.MultiLineTableCellRenderer;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.modifier.ModifierSelectionDialog;
import com.floreantpos.ui.views.order.modifier.ModifierSelectionModel;
import com.floreantpos.ui.views.order.multipart.PizzaModifierSelectionDialog;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import com.jidesoft.swing.SimpleScrollPane;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import net.miginfocom.swing.MigLayout;
import org.hibernate.Session;






























public class ComboTicketItemSelectionDialog
  extends OkCancelOptionDialog
{
  private JPanel menuItemViewPanel;
  private ScrollableFlowPanel buttonsPanel;
  private ScrollableFlowPanel comboGroupPanel;
  private Map<String, TicketItem> ticketItemComboMap = new LinkedHashMap();
  private Map<String, List<MenuItem>> groupMap = new HashMap();
  private MenuItem parentMenuItem;
  private ComboTicketItem ticketItem;
  private ComboGroup selectedComboGroup;
  private JTable table;
  private PosButton btnCustomQuantity;
  private PosButton btnIncreaseQuantity;
  private PosButton btnDecreaseQuantity;
  
  public ComboTicketItemSelectionDialog(MenuItem menuItem, TicketItem ticketItem) {
    super(Application.getPosWindow(), true);
    this.ticketItem = ((ComboTicketItem)ticketItem);
    parentMenuItem = menuItem;
    initComponent();
    rendererGroup();
    updateView();
  }
  
  private void initComponent() {
    setOkButtonText("DONE");
    setCaption(parentMenuItem + " - Select combo items");
    getContentPanel().setBorder(new EmptyBorder(0, 10, 0, 10));
    
    JPanel centerPanel = new JPanel(new BorderLayout());
    menuItemViewPanel = new JPanel(new MigLayout("fill"));
    buttonsPanel = new ScrollableFlowPanel(1);
    
    JScrollPane scrollPane = new PosScrollPane(buttonsPanel, 20, 31);
    scrollPane.getVerticalScrollBar().setPreferredSize(PosUIManager.getSize(60, 0));
    scrollPane.setBorder(null);
    
    menuItemViewPanel.add(scrollPane, "grow,span");
    centerPanel.add(menuItemViewPanel, "Center");
    getContentPanel().add(centerPanel, "Center");
    
    table = new JTable();
    table.setRowHeight(40);
    table.setModel(new ComboTicketItemTableModel());
    table.setTableHeader(null);
    table.setDefaultRenderer(Object.class, new MultiLineTableCellRenderer());
    table.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        TicketItem comboTicketItem = ComboTicketItemSelectionDialog.this.getComboTicketItem();
        boolean editable = (comboTicketItem != null) && (ComboTicketItemSelectionDialog.this.isGroupItem(comboTicketItem));
        btnCustomQuantity.setEnabled(editable);
        btnIncreaseQuantity.setEnabled(editable);
        btnDecreaseQuantity.setEnabled(editable);
        btnCustomQuantity.setText("...");
        btnCustomQuantity.setEnabled(editable);
        if (comboTicketItem == null) {
          return;
        }
        double quantity = comboTicketItem.getQuantity().doubleValue();
        updateQuantity(quantity);
      }
      
    });
    JScrollPane scroll = new JScrollPane(table);
    scroll.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(5, 0, 2, 3), scroll.getBorder()));
    scroll.setPreferredSize(PosUIManager.getSize(250, 0));
    
    JPanel leftTableView = new JPanel(new BorderLayout());
    TitledBorder leftSelectedComboTitleBorder = new TitledBorder("Selected combo items");
    leftSelectedComboTitleBorder.setTitleJustification(2);
    leftTableView.setBorder(leftSelectedComboTitleBorder);
    leftTableView.add(scroll);
    getContentPanel().add(leftTableView, "West");
    
    int size = PosUIManager.getSize(30);
    JPanel itemtemActionPanel = new JPanel();
    itemtemActionPanel.setLayout(new MigLayout("fillx,ins 2 0 2 0", "[" + size + "px][grow][" + size + "px]", "[" + size + "]"));
    
    btnIncreaseQuantity = new PosButton();
    btnIncreaseQuantity.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TicketItem comboTicketItem = ComboTicketItemSelectionDialog.this.getComboTicketItem();
        if (comboTicketItem == null)
          return;
        if (!ComboTicketItemSelectionDialog.this.isGroupItem(comboTicketItem))
          return;
        double previousQuantity = comboTicketItem.getQuantity().doubleValue();
        comboTicketItem.setQuantity(Double.valueOf(previousQuantity + 1.0D));
        if (!ComboTicketItemSelectionDialog.this.hasGroupOrValidateQuantity(comboTicketItem, previousQuantity)) {
          return;
        }
        updateQuantity(comboTicketItem.getQuantity().doubleValue());
      }
    });
    btnCustomQuantity = new PosButton("...");
    btnCustomQuantity.setFont(new Font(btnCustomQuantity.getFont().getName(), 1, 20));
    btnCustomQuantity.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TicketItem comboTicketItem = ComboTicketItemSelectionDialog.this.getComboTicketItem();
        if (comboTicketItem == null)
          return;
        if (!ComboTicketItemSelectionDialog.this.isGroupItem(comboTicketItem)) {
          return;
        }
        NumberSelectionDialog2 dialog = new NumberSelectionDialog2();
        dialog.setTitle("Enter quantity");
        dialog.setFloatingPoint(false);
        dialog.pack();
        dialog.open();
        
        if (dialog.isCanceled()) {
          return;
        }
        double quantity = (int)dialog.getValue();
        if (quantity <= 0.0D)
          return;
        double previousQuantity = comboTicketItem.getQuantity().doubleValue();
        comboTicketItem.setQuantity(Double.valueOf(quantity));
        if (!ComboTicketItemSelectionDialog.this.hasGroupOrValidateQuantity(comboTicketItem, previousQuantity)) {
          return;
        }
        updateQuantity(comboTicketItem.getQuantity().doubleValue());
      }
    });
    btnDecreaseQuantity = new PosButton();
    btnDecreaseQuantity.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TicketItem comboTicketItem = ComboTicketItemSelectionDialog.this.getComboTicketItem();
        if (comboTicketItem == null)
          return;
        if (!ComboTicketItemSelectionDialog.this.isGroupItem(comboTicketItem))
          return;
        double previousQuantity = comboTicketItem.getQuantity().doubleValue();
        if (previousQuantity == 1.0D)
          return;
        comboTicketItem.setQuantity(Double.valueOf(previousQuantity - 1.0D));
        if (!ComboTicketItemSelectionDialog.this.hasGroupOrValidateQuantity(comboTicketItem, previousQuantity)) {
          return;
        }
        updateQuantity(comboTicketItem.getQuantity().doubleValue());
      }
      
    });
    btnIncreaseQuantity.setIcon(IconFactory.getIcon("/ui_icons/", "plus_32.png"));
    btnDecreaseQuantity.setIcon(IconFactory.getIcon("/ui_icons/", "minus_32.png"));
    
    itemtemActionPanel.add(btnDecreaseQuantity);
    itemtemActionPanel.add(btnCustomQuantity, "grow");
    itemtemActionPanel.add(btnIncreaseQuantity);
    
    leftTableView.add(itemtemActionPanel, "South");
    
    comboGroupPanel = new ScrollableFlowPanel(1);
    
    SimpleScrollPane scrollPane2 = new SimpleScrollPane(comboGroupPanel);
    scrollPane2.setBorder(null);
    scrollPane2.setHorizontalScrollBarPolicy(30);
    scrollPane2.setVerticalScrollBarPolicy(20);
    scrollPane2.setViewportView(comboGroupPanel);
    
    JPanel menuGroupContainer = new JPanel(new MigLayout("fill"));
    TitledBorder titledBorder = new TitledBorder("Group");
    titledBorder.setTitleJustification(2);
    menuGroupContainer.setBorder(titledBorder);
    menuGroupContainer.setPreferredSize(PosUIManager.getSize(0, 110));
    menuGroupContainer.add(scrollPane2, "grow");
    
    centerPanel.add(menuGroupContainer, "North");
    

    PosButton btnModify = new PosButton("MODIFY");
    btnModify.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        TicketItem comboTicketItem = ComboTicketItemSelectionDialog.this.getComboTicketItem();
        if (comboTicketItem == null)
          return;
        comboTicketItem.setTicket(OrderView.getInstance().getCurrentTicket());
        if (((comboTicketItem instanceof ModifiableTicketItem)) || (comboTicketItem.isPizzaType().booleanValue())) {
          openModifierDialog(comboTicketItem);
          comboTicketItem.setTicket(null);
          ComboTicketItemSelectionDialog.this.updateView();
        }
      }
    });
    getButtonPanel().add(btnModify, 0);
  }
  
  private boolean isGroupItem(TicketItem comboTicketItem) {
    if (comboTicketItem.getGroupId() == null) {
      return false;
    }
    return true;
  }
  
  private boolean hasGroupOrValidateQuantity(TicketItem comboTicketItem, double previousQuantity) {
    if (comboTicketItem.getGroupId() == null)
      return false;
    Iterator localIterator; if (parentMenuItem.getComboGroups() != null)
      for (localIterator = parentMenuItem.getComboGroups().iterator(); localIterator.hasNext();) { group = (ComboGroup)localIterator.next();
        if (group.getId().equals(comboTicketItem.getGroupId())) {
          selectedComboGroup = group;
          rendererItems(selectedComboGroup);
          break;
        }
      }
    ComboGroup group;
    int count = 0;
    for (TicketItem ticketItem : ticketItemComboMap.values()) {
      if (selectedComboGroup.getId().equals(ticketItem.getGroupId())) {
        count = (int)(count + ticketItem.getQuantity().doubleValue());
      }
    }
    if (count > selectedComboGroup.getMaxQuantity().intValue()) {
      comboTicketItem.setQuantity(Double.valueOf(previousQuantity));
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Maximum limit is : " + selectedComboGroup.getMaxQuantity());
      return false;
    }
    return true;
  }
  
  protected void updateQuantity(double quantity) {
    btnCustomQuantity.setText(NumberUtil.trimDecilamIfNotNeeded(Double.valueOf(quantity)));
    table.repaint();
  }
  
  private TicketItem getComboTicketItem() {
    int row = table.getSelectedRow();
    if (row == -1)
      return null;
    int index = table.convertRowIndexToModel(row);
    ITicketItem item = (ITicketItem)((ComboTicketItemTableModel)table.getModel()).getRowData(index);
    TicketItem comboTicketItem = null;
    if ((item instanceof TicketItem)) {
      comboTicketItem = (TicketItem)item;
    }
    else if ((item instanceof TicketItemModifier)) {
      TicketItemModifier ticketItemModifier = (TicketItemModifier)item;
      comboTicketItem = ticketItemModifier.getTicketItem();
    }
    return comboTicketItem;
  }
  
  public void openModifierDialog(TicketItem ticketItem) {
    try {
      Ticket ticket = ticketItem.getTicket();
      MenuItem menuItem = ticketItem.getMenuItem();
      MenuItemDAO.getInstance().initialize(menuItem);
      List<TicketItemModifier> ticketItemModifiers = ticketItem.getTicketItemModifiers();
      if (ticketItemModifiers == null) {
        ticketItemModifiers = new ArrayList();
      }
      TicketItem cloneTicketItem = ticketItem.clone();
      boolean pizzaType = ticketItem.isPizzaType().booleanValue();
      if (pizzaType) {
        PizzaModifierSelectionDialog dialog = new PizzaModifierSelectionDialog(ticket, cloneTicketItem, menuItem, true);
        dialog.openFullScreen();
        
        if (dialog.isCanceled()) {
          return;
        }
        sizeModifier = cloneTicketItem.getSizeModifier();
        sizeModifier.setTicketItem(ticketItem);
        ticketItem.setSizeModifier(sizeModifier);
        ticketItem.setQuantity(cloneTicketItem.getQuantity());
        ticketItem.setUnitPrice(cloneTicketItem.getUnitPrice());
      }
      else {
        ModifierSelectionDialog dialog = new ModifierSelectionDialog(new ModifierSelectionModel(ticket, cloneTicketItem, menuItem));
        dialog.open();
        
        if (dialog.isCanceled()) {
          return;
        }
      }
      List<TicketItemModifier> addedTicketItemModifiers = cloneTicketItem.getTicketItemModifiers();
      if (addedTicketItemModifiers == null) {
        addedTicketItemModifiers = new ArrayList();
      }
      ticketItemModifiers.clear();
      for (TicketItemModifier ticketItemModifier : addedTicketItemModifiers) {
        ticketItemModifier.setTicketItem(ticketItem);
        ticketItem.addToticketItemModifiers(ticketItemModifier);
      }
    } catch (Exception e) { TicketItemModifier sizeModifier;
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private void rendererGroup() {
    List<TicketItem> comboItems = this.ticketItem.getComboItems();
    if (comboItems != null) {
      for (TicketItem ticketItem : comboItems) {
        if (ticketItem.getGroupId() != null)
        {

          ticketItemComboMap.put(ticketItem.getMenuItemId(), ticketItem); }
      }
    }
    if (parentMenuItem.isVariant().booleanValue()) {
      parentMenuItem = parentMenuItem.getParentMenuItem();
      MenuItemDAO.getInstance().initialize(parentMenuItem);
    }
    Object comboGroups = parentMenuItem.getComboGroups();
    if (comboGroups != null) {
      Dimension size = PosUIManager.getSize(115, 60);
      ButtonGroup group = new ButtonGroup();
      boolean selectedFirst = true;
      for (ComboGroup comboGroup : (List)comboGroups) {
        ComboGroupButton groupButton = new ComboGroupButton(comboGroup);
        groupButton.setSelected(selectedFirst);
        groupButton.setPreferredSize(size);
        comboGroupPanel.add(groupButton);
        List<MenuItem> items = comboGroup.getItems();
        try {
          if (items != null) {
            for (MenuItem item : items) {
              TicketItem ticketItem = (TicketItem)ticketItemComboMap.get(item.getId());
              if (ticketItem != null)
                ticketItem.setMenuItem(item);
            }
            groupMap.put(comboGroup.getId(), items);
          }
        } catch (PosException e) {
          e.printStackTrace();
        }
        group.add(groupButton);
        selectedFirst = false;
      }
      
      if ((comboGroups != null) && (((List)comboGroups).size() > 0)) {
        selectedComboGroup = ((ComboGroup)((List)comboGroups).get(0));
        rendererItems(selectedComboGroup);
        TitledBorder titleBorder = BorderFactory.createTitledBorder(selectedComboGroup.getName() + " Min:" + selectedComboGroup.getMinQuantity() + " Max:" + selectedComboGroup
          .getMaxQuantity());
        titleBorder.setTitleJustification(2);
        menuItemViewPanel.setBorder(titleBorder);
      }
      else {
        TitledBorder titleBorder = BorderFactory.createTitledBorder("-");
        titleBorder.setTitleJustification(2);
        menuItemViewPanel.setBorder(titleBorder);
      }
    }
  }
  
  private void rendererItems(ComboGroup comboGroup) {
    try {
      buttonsPanel.getContentPane().removeAll();
      Dimension size = PosUIManager.getSize(115, 80);
      
      if (comboGroup == null)
        return;
      List<MenuItem> items = (List)groupMap.get(comboGroup.getId());
      if (items != null) {
        for (MenuItem menuItem : items) {
          MenuItemButton btnMenuItem = new MenuItemButton(menuItem);
          TicketItem ticketItem = (TicketItem)ticketItemComboMap.get(menuItem.getId());
          if (ticketItem != null) {
            btnMenuItem.setSelected(true);
          }
          buttonsPanel.getContentPane().add(btnMenuItem);
          btnMenuItem.setPreferredSize(size);
        }
      }
      buttonsPanel.getContentPane().revalidate();
      buttonsPanel.getContentPane().repaint();
    } catch (PosException e) {
      POSMessageDialog.showError(this, e.getLocalizedMessage(), e);
    }
  }
  
  public void doOk()
  {
    if (!isRequiredItemsAdded())
      return;
    List<TicketItem> comboItems = this.ticketItem.getComboItems();
    Iterator iterator; if (comboItems != null) {
      for (iterator = comboItems.iterator(); iterator.hasNext();) {
        TicketItem ticketItem = (TicketItem)iterator.next();
        if (ticketItem.getGroupId() != null)
        {

          iterator.remove(); }
      }
    }
    for (TicketItem item : ticketItemComboMap.values()) {
      item.setParentTicketItem(this.ticketItem);
      item.setTicket(null);
      this.ticketItem.addTocomboItems(item);
    }
    setCanceled(false);
    dispose();
  }
  
  private boolean isRequiredItemsAdded() {
    List<ComboGroup> comboGroups = parentMenuItem.getComboGroups();
    if (comboGroups == null)
      return true;
    for (ComboGroup group : comboGroups) {
      int count = 0;
      for (TicketItem ticketItem : ticketItemComboMap.values()) {
        if (group.getId().equals(ticketItem.getGroupId())) {
          count = (int)(count + ticketItem.getQuantity().doubleValue());
        }
      }
      if (count < group.getMinQuantity().intValue()) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select minimum " + group
          .getMinQuantity() + " quantity from group " + group.getName());
        rendererItems(group);
        return false;
      }
    }
    return true;
  }
  
  private void updateView() {
    ComboTicketItemTableModel model = (ComboTicketItemTableModel)table.getModel();
    List<ITicketItem> items = new ArrayList();
    List<TicketItem> comboItems = ticketItem.getComboItems();
    if (comboItems != null) {
      for (TicketItem comboItem : comboItems) {
        if (comboItem.getGroupId() == null)
        {
          items.add(comboItem); }
      }
    }
    items.addAll(ticketItemComboMap.values());
    model.setRows(items);
  }
  
  public void doCancel() {
    ticketItemComboMap.clear();
    setCanceled(true);
    dispose();
  }
  
  public Collection<TicketItem> getSelectedMenuItems() {
    return ticketItemComboMap.values();
  }
  
  private class MenuItemButton extends POSToggleButton implements ActionListener {
    private int BUTTON_SIZE = 100;
    MenuItem menuItem;
    
    MenuItemButton(MenuItem menuItem) {
      this.menuItem = menuItem;
      setFocusable(false);
      setVerticalTextPosition(3);
      setHorizontalTextPosition(0);
      BUTTON_SIZE = PosUIManager.getSize(100);
      
      ImageIcon image = menuItem.getImage();
      if (image != null) {
        if (menuItem.isShowImageOnly().booleanValue()) {
          setIcon(image);
        }
        else {
          setIcon(image);
          setText("<html><body><center>" + menuItem.getDisplayName() + "</center></body></html>");
        }
        
      }
      else {
        setText("<html><body><center>" + menuItem.getName() + "</center></body></html>");
      }
      
      Color buttonColor = menuItem.getButtonColor();
      if (buttonColor != null) {
        setBackground(buttonColor);
      }
      Color textColor = menuItem.getTextColor();
      if (textColor != null) {
        setForeground(textColor);
      }
      
      setPreferredSize(new Dimension(BUTTON_SIZE, BUTTON_SIZE));
      addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e) {
      if (isSelected()) {
        if (!ComboTicketItemSelectionDialog.this.validQuantity()) {
          setSelected(false);
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Maximum limit is : " + selectedComboGroup.getMaxQuantity());
          return;
        }
        Session session = null;
        try {
          session = MenuItemDAO.getInstance().getSession();
          menuItem = ((MenuItem)session.merge(menuItem));
          if (!addComboItem(menuItem)) {
            setSelected(false);
            return;
          }
          
          MenuItemDAO.getInstance().closeSession(session); } finally { MenuItemDAO.getInstance().closeSession(session);
        }
      }
      
      TicketItem ticketItem = (TicketItem)ticketItemComboMap.get(menuItem.getId());
      if (ticketItem != null) {
        ticketItemComboMap.remove(menuItem.getId());
      }
      
      ComboTicketItemSelectionDialog.this.updateView();
    }
  }
  
  private boolean validQuantity() {
    int count = 1;
    for (TicketItem ticketItem : ticketItemComboMap.values()) {
      if (selectedComboGroup.getId().equals(ticketItem.getGroupId())) {
        count = (int)(count + ticketItem.getQuantity().doubleValue());
      }
    }
    if (count > selectedComboGroup.getMaxQuantity().intValue()) {
      return false;
    }
    return true;
  }
  
  public boolean addComboItem(MenuItem menuItem) {
    Ticket currentTicket = OrderView.getInstance().getCurrentTicket();
    


    TicketItem comboTicketItem = menuItem.convertToTicketItem(currentTicket, 1.0D);
    comboTicketItem.setMenuItem(menuItem);
    comboTicketItem.setTicket(currentTicket);
    if (menuItem.isPizzaType().booleanValue()) {
      PizzaModifierSelectionDialog dialog = new PizzaModifierSelectionDialog(currentTicket, comboTicketItem, menuItem, false);
      dialog.openFullScreen();
      
      if (dialog.isCanceled()) {
        return false;
      }
    }
    else if (menuItem.hasMandatoryModifiers()) {
      ModifierSelectionDialog dialog = new ModifierSelectionDialog(new ModifierSelectionModel(currentTicket, comboTicketItem, menuItem));
      dialog.open();
      if (dialog.isCanceled())
        return false;
    }
    ticketItemComboMap.put(menuItem.getId(), comboTicketItem);
    comboTicketItem.setMenuItemId(menuItem.getId());
    comboTicketItem.setGroupId(selectedComboGroup.getId());
    comboTicketItem.setName(menuItem.getName());
    comboTicketItem.setTicket(null);
    comboTicketItem.setQuantity(Double.valueOf(1.0D));
    return true;
  }
  
  private class ComboGroupButton extends POSToggleButton implements ActionListener {
    ComboGroup comboGroup;
    
    ComboGroupButton(ComboGroup comboGroup) {
      this.comboGroup = comboGroup;
      
      setText("<html><body><center>" + comboGroup.getName() + "</center></body></html>");
      addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e) {
      menuItemViewPanel.setBorder(BorderFactory.createTitledBorder(comboGroup.getName() + " Min:" + comboGroup.getMinQuantity() + " Max:" + comboGroup
        .getMaxQuantity()));
      ComboTicketItemSelectionDialog.this.rendererItems(comboGroup);
      selectedComboGroup = comboGroup;
    }
  }
  
  public class ComboTicketItemTableModel extends ListTableModel<ITicketItem> {
    public ComboTicketItemTableModel() {
      super();
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      ITicketItem item = (ITicketItem)rows.get(rowIndex);
      switch (columnIndex) {
      case 0: 
        return item.getItemQuantityDisplay() + "x " + item.getNameDisplay();
      }
      return null;
    }
    
    public void setRows(List<ITicketItem> rows)
    {
      List<ITicketItem> ticketItemWithModifiers = new ArrayList();
      for (ITicketItem item : rows) {
        ticketItemWithModifiers.add(item);
        if ((item instanceof TicketItem)) {
          TicketItem ticketItem = (TicketItem)item;
          if ((ticketItem.isHasModifiers().booleanValue()) && (ticketItem.getTicketItemModifiers() != null)) {
            ticketItemWithModifiers.addAll(ticketItem.getTicketItemModifiers());
          }
        }
      }
      super.setRows(ticketItemWithModifiers);
    }
  }
}
