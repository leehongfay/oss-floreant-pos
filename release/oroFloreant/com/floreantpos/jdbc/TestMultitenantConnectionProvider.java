package com.floreantpos.jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import org.hibernate.HibernateException;
import org.hibernate.c3p0.internal.C3P0ConnectionProvider;
import org.hibernate.engine.config.spi.ConfigurationService;
import org.hibernate.service.spi.ServiceRegistryAwareService;
import org.hibernate.service.spi.ServiceRegistryImplementor;

public class TestMultitenantConnectionProvider implements org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider, ServiceRegistryAwareService
{
  private C3P0ConnectionProvider connectionProvider = null;
  
  public TestMultitenantConnectionProvider() {}
  
  public void injectServices(ServiceRegistryImplementor serviceRegistry) { Map lSettings = ((ConfigurationService)serviceRegistry.getService(ConfigurationService.class)).getSettings();
    
    connectionProvider = new C3P0ConnectionProvider();
    connectionProvider.injectServices(serviceRegistry);
    connectionProvider.configure(lSettings);
  }
  
  public boolean isUnwrappableAs(Class unwrapType)
  {
    return false;
  }
  
  public <T> T unwrap(Class<T> unwrapType)
  {
    return null;
  }
  
  public Connection getAnyConnection() throws SQLException
  {
    return connectionProvider.getConnection();
  }
  
  public void releaseAnyConnection(Connection connection) throws SQLException
  {
    try {
      connection.createStatement().execute("SET SCHEMA 'public'");
    } catch (SQLException e) {
      throw new HibernateException("Could not alter JDBC connection to specified schema [public]", e);
    }
    connectionProvider.closeConnection(connection);
  }
  
  public Connection getConnection(String tenantIdentifier) throws SQLException
  {
    Connection connection = getAnyConnection();
    try {
      connection.createStatement().execute("SET SCHEMA " + tenantIdentifier + "");
    } catch (SQLException e) {
      throw new HibernateException("Could not alter JDBC connection to specified schema [" + tenantIdentifier + "]", e);
    }
    return connection;
  }
  
  public void releaseConnection(String tenantIdentifier, Connection connection) throws SQLException
  {
    releaseAnyConnection(connection);
  }
  
  public boolean supportsAggressiveRelease()
  {
    return false;
  }
}
