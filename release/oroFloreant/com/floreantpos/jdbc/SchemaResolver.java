package com.floreantpos.jdbc;

import org.apache.commons.lang.StringUtils;

public class SchemaResolver implements org.hibernate.context.spi.CurrentTenantIdentifierResolver { public SchemaResolver() {}
  
  private static final ThreadLocal<String> tenant = new ThreadLocal();
  
  public static void setTenant(String tenant) {
    tenant.set(tenant);
  }
  
  public String resolveCurrentTenantIdentifier()
  {
    String string = (String)tenant.get();
    if (StringUtils.isEmpty(string)) {
      return "public";
    }
    return string;
  }
  
  public boolean validateExistingCurrentSessions()
  {
    return false;
  }
  






  public static void initTenant(String tenant)
  {
    setTenant(tenant);
  }
}
