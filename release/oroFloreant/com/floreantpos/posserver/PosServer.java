package com.floreantpos.posserver;

import com.floreantpos.PosLog;
import com.floreantpos.model.dao._RootDAO;
import java.io.IOException;
import java.net.ServerSocket;

public class PosServer implements Runnable
{
  public static final int PORT = 5656;
  
  public PosServer()
  {
    new Thread(this).start();
  }
  
  public void run()
  {
    ServerSocket ss = null;
    try
    {
      PosLog.info(getClass(), "listening on ...5656");
      ss = new ServerSocket(5656);
      listen(ss); return;
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    } finally {
      if (ss != null) {
        try {
          ss.close();
        }
        catch (IOException localIOException2) {}
      }
    }
  }
  









  static void listen(ServerSocket ss)
    throws Exception
  {
    String resp = "";
    String ids = "";
    for (;;)
    {
      PosLog.info(PosServer.class, "Waiting For Connections....");
      java.net.Socket s = ss.accept();
      
      PosRequestHandler posRequestHandler = new PosRequestHandler(s);
      posRequestHandler.start();
    }
  }
  
  public static void main(String[] args)
    throws Exception
  {}
}
