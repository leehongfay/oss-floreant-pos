package com.floreantpos.posserver;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Checks")
@XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.FIELD)
public class Checks
{
  @javax.xml.bind.annotation.XmlElement(name="Check")
  private List<Check> checks;
  
  public Checks() {}
  
  public List<Check> getCheckList()
  {
    return checks;
  }
  
  public void setCheckList(List<Check> checks) {
    this.checks = checks;
  }
}
