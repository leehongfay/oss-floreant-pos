package com.floreantpos.posserver;

import com.floreantpos.Messages;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;






public class PosServerClient
  extends POSDialog
{
  private static JLabel lblStatus;
  private JTextField txtServerId;
  private JTextField txtTable;
  private JTextArea txtReqMsg;
  private JTextArea txtRespMsg;
  private JButton btnSend;
  private JButton btnRequest;
  
  public PosServerClient()
  {
    intializeComponents();
  }
  
  public void intializeComponents() {
    setLayout(new BorderLayout());
    
    JPanel container = new JPanel(new BorderLayout());
    
    JPanel headerPanel = new JPanel(new BorderLayout());
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle(Messages.getString("PosServerClient.0"));
    
    lblStatus = new JLabel("");
    
    headerPanel.add(titlePanel, "North");
    headerPanel.add(lblStatus, "South");
    
    JLabel lblServerId = new JLabel(Messages.getString("PosServerClient.2"));
    JLabel lblTable = new JLabel(Messages.getString("PosServerClient.3"));
    
    txtServerId = new JTextField(20);
    txtTable = new JTextField(20);
    
    btnSend = new JButton(Messages.getString("PosServerClient.4"));
    btnRequest = new JButton(Messages.getString("PosServerClient.5"));
    
    JPanel centerPanel = new JPanel(new MigLayout());
    
    centerPanel.add(lblServerId);
    centerPanel.add(txtServerId);
    centerPanel.add(btnSend, "wrap");
    centerPanel.add(lblTable);
    centerPanel.add(txtTable, "wrap");
    centerPanel.add(new JLabel());
    centerPanel.add(btnRequest, "wrap");
    
    JPanel msgPanel = new JPanel(new MigLayout());
    
    JLabel lblReq = new JLabel(Messages.getString("PosServerClient.5"));
    JLabel lblRes = new JLabel(Messages.getString("PosServerClient.10"));
    
    txtReqMsg = new JTextArea(5, 50);
    txtRespMsg = new JTextArea(5, 50);
    
    txtReqMsg.setLineWrap(true);
    txtRespMsg.setLineWrap(true);
    
    msgPanel.add(lblReq, "wrap");
    msgPanel.add(txtReqMsg, "grow, wrap");
    msgPanel.add(lblRes, "wrap");
    msgPanel.add(txtRespMsg, "grow");
    
    container.add(headerPanel, "North");
    container.add(msgPanel, "South");
    container.add(centerPanel, "Center");
    
    add(container, "Center");
    
    setSize(500, 500);
  }
  











  public static void main(String[] args)
    throws Exception
  {
    new PosServerClient().open();
  }
}
