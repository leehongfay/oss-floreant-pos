package com.floreantpos.posserver;

import com.floreantpos.Messages;
import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.PaymentType;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.Store;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.services.PosTransactionService;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.ui.views.payment.PosPaymentWaitDialog;
import com.floreantpos.ui.views.payment.SettleTicketProcessor;
import java.io.DataOutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.jfree.util.Log;
import org.xml.sax.InputSource;

public class PosRequestHandler extends Thread
{
  private Socket socket;
  
  public PosRequestHandler(Socket socket) throws Exception
  {
    this.socket = socket;
  }
  
  public void run()
  {
    try
    {
      for (;;) {
        byte[] b1 = new byte['ஸ'];
        socket.getInputStream().read(b1);
        
        String request = new String(b1).trim();
        if (request.length() <= 0) {
          break;
        }
        
        PosLog.info(getClass(), "Request From Terminal==>[" + request + "]");
        
        int index = request.indexOf("<");
        request = request.substring(index);
        
        POSRequest posRequest = createRequest(request);
        POSResponse posResponse = createResponse(posRequest);
        
        String resp = convertResponseToString(posResponse);
        
        PosLog.info(getClass(), "Reponse to Terminal===>[" + resp + "]");
        
        DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
        byte[] tosend = resp.getBytes();
        dos.write(tosend, 0, tosend.length);
        dos.flush();
      }
      return;
    }
    catch (Exception e) {
      Log.debug("Error:" + e);
    }
    finally {
      try {
        Thread.sleep(5000L);
        socket.close();
      } catch (Exception e) {
        Log.debug("Error:" + e);
      }
    }
  }
  
  private POSRequest createRequest(String requestString) throws Exception {
    InputSource is = new InputSource();
    is.setCharacterStream(new StringReader(requestString));
    
    JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] { POSRequest.class });
    Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
    return (POSRequest)unmarshaller.unmarshal(is);
  }
  
  private POSResponse createResponse(POSRequest posRequest) {
    POSResponse posResponse = new POSResponse();
    





































    return posResponse;
  }
  
  private String convertResponseToString(POSResponse posResponse) throws Exception {
    JAXBContext messageContext = JAXBContext.newInstance(new Class[] { POSResponse.class });
    Marshaller marshaller = messageContext.createMarshaller();
    StringWriter dataWriter = new StringWriter();
    marshaller.marshal(posResponse, dataWriter);
    
    String resp = "";
    resp = dataWriter.toString();
    resp = resp.replaceAll("<\\?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"\\?>", "");
    
    String len = String.format("%05d", new Object[] { Integer.valueOf(resp.length()) });
    resp = len + resp;
    
    return resp;
  }
  
  private POSResponse addAllTables(POSRequest posRequest) {
    POSResponse posResponse = new POSResponse();
    
    User user = UserDAO.getInstance().findUserBySecretKey(posDefaultInfo.server);
    List<Ticket> ticketsForUser = TicketDAO.getInstance().findOpenTicketsForUser(user);
    
    Checks checks = new Checks();
    
    checks.setCheckList(new ArrayList());
    
    for (Ticket ticket : ticketsForUser) {
      List<Integer> tableNumbers = ticket.getTableNumbers();
      if ((tableNumbers != null) && (tableNumbers.size() > 0)) {
        Check chk = new Check();
        String tableNumber = ((Integer)tableNumbers.get(0)).toString();
        if (((Integer)tableNumbers.get(0)).intValue() < 10) {
          tableNumber = "0" + ((Integer)tableNumbers.get(0)).toString();
        }
        chk.setTableNo(tableNumber);
        chk.setTableName("");
        chk.setChkName(String.valueOf(ticket.getId()));
        chk.setChkNo(String.valueOf(ticket.getId()));
        chk.setAmt(String.valueOf(Math.round((ticket.getDueAmount().doubleValue() - ticket.getTaxAmount().doubleValue()) * 100.0D)));
        chk.setTax(String.valueOf(Math.round(ticket.getTaxAmount().doubleValue() * 100.0D)));
        checks.getCheckList().add(chk);
      }
    }
    posResponse.setChecks(checks);
    
    POSDefaultInfo posDefaultInfo = new POSDefaultInfo();
    posDefaultInfo.setServer(posDefaultInfo.server);
    posDefaultInfo.setTable(posDefaultInfo.table);
    posDefaultInfo.setCheck(posDefaultInfo.check);
    posDefaultInfo.setRes("1");
    posDefaultInfo.setrText(Messages.getString("PosRequestHandler.0"));
    
    posResponse.setPosDefaultInfo(posDefaultInfo);
    
    return posResponse;
  }
  
  private POSResponse addTable(POSRequest posRequest) {
    POSResponse posResponse = new POSResponse();
    
    User user = UserDAO.getInstance().findUserBySecretKey(posDefaultInfo.server);
    List<Ticket> ticketsForUser = TicketDAO.getInstance().findOpenTicketsForUser(user);
    
    Checks checks = new Checks();
    
    checks.setCheckList(new ArrayList());
    
    for (Ticket ticket : ticketsForUser) {
      List<Integer> tableNumbers = ticket.getTableNumbers();
      if ((tableNumbers != null) && (tableNumbers.size() > 0) && 
        (tableNumbers.contains(Integer.valueOf(Integer.parseInt(posDefaultInfo.table))))) {
        Check chk = new Check();
        String tableNumber = ((Integer)tableNumbers.get(0)).toString();
        if (((Integer)tableNumbers.get(0)).intValue() < 10) {
          tableNumber = "0" + ((Integer)tableNumbers.get(0)).toString();
        }
        chk.setTableNo(String.valueOf(tableNumber));
        chk.setTableName("");
        chk.setChkName("");
        chk.setChkNo(String.valueOf(ticket.getId()));
        chk.setAmt(String.valueOf(Math.round((ticket.getDueAmount().doubleValue() - ticket.getTaxAmount().doubleValue()) * 100.0D)));
        chk.setTax(String.valueOf(Math.round(ticket.getTaxAmount().doubleValue() * 100.0D)));
        checks.getCheckList().add(chk);
        break;
      }
    }
    
    posResponse.setChecks(checks);
    
    POSDefaultInfo posDefaultInfo = new POSDefaultInfo();
    posDefaultInfo.setServer(posDefaultInfo.server);
    posDefaultInfo.setTable(posDefaultInfo.table);
    posDefaultInfo.setCheck(posDefaultInfo.check);
    posDefaultInfo.setRes("1");
    posDefaultInfo.setrText(Messages.getString("PosRequestHandler.0"));
    
    posResponse.setPosDefaultInfo(posDefaultInfo);
    
    return posResponse;
  }
  
  private POSResponse applyPayment(POSRequest posRequest) {
    POSResponse posResponse = new POSResponse();
    
    Ticket ticket = TicketDAO.getInstance().loadFullTicket(posDefaultInfo.check);
    String paymentType = payment.cardType;
    
    PosTransaction transaction = null;
    if (paymentType.equals("8")) {
      transaction = PaymentType.CASH.createTransaction();
      transaction.setCaptured(Boolean.valueOf(true));
    }
    else
    {
      if (paymentType.equals("1")) {
        transaction = PaymentType.CREDIT_MASTER_CARD.createTransaction();
      }
      else if (paymentType.equals("2")) {
        transaction = PaymentType.CREDIT_VISA.createTransaction();
      }
      else if (paymentType.equals("4")) {
        transaction = PaymentType.CREDIT_DISCOVERY.createTransaction();
      }
      else if (paymentType.equals("5")) {
        transaction = PaymentType.CREDIT_AMEX.createTransaction();
      }
      



      transaction.setCaptured(Boolean.valueOf(false));
      transaction.setCardNumber(payment.acct);
      
      String exp = payment.exp;
      if (exp != null) {
        transaction.setCardExpMonth(exp.substring(0, 2));
        transaction.setCardExpYear(exp.substring(2, 4));
      }
    }
    
    double tenderAmount = Double.parseDouble(payment.pamt) / 100.0D;
    transaction.setTenderAmount(Double.valueOf(tenderAmount));
    transaction.setTicket(ticket);
    
    if (tenderAmount >= ticket.getDueAmount().doubleValue()) {
      transaction.setAmount(ticket.getDueAmount());
    }
    else {
      transaction.setAmount(Double.valueOf(tenderAmount));
    }
    
    PosTransactionService transactionService = PosTransactionService.getInstance();
    try {
      double dueAmount = ticket.getDueAmount().doubleValue();
      transactionService.settleTicket(ticket, transaction, Application.getCurrentUser());
      SettleTicketProcessor.printTicket(ticket, transaction);
      SettleTicketProcessor.showTransactionCompleteMsg(dueAmount, tenderAmount, ticket, transaction);
      
      if (SettleTicketProcessor.waitDialog.isVisible()) {
        SettleTicketProcessor.waitDialog.setCanceled(false);
        SettleTicketProcessor.waitDialog.dispose();
        RootView.getInstance().showDefaultView();
      }
      
      POSDefaultInfo posDefaultInfo = new POSDefaultInfo();
      posDefaultInfo.setServer(posDefaultInfo.server);
      posDefaultInfo.setTable(posDefaultInfo.table);
      posDefaultInfo.setCheck(posDefaultInfo.check);
      posDefaultInfo.setRes("1");
      posDefaultInfo.setrText(Messages.getString("PosRequestHandler.0"));
      
      posResponse.setPosDefaultInfo(posDefaultInfo);
      
      return posResponse;
    }
    catch (Exception e) {
      Log.debug(Messages.getString("PosRequestHandler.24") + e);
    }
    return posResponse;
  }
  
  private POSResponse printCheck(POSRequest posRequest) {
    POSResponse posResponse = new POSResponse();
    
    POSDefaultInfo posDefaultInfo = new POSDefaultInfo();
    posDefaultInfo.setServer(posDefaultInfo.server);
    posDefaultInfo.setTable(posDefaultInfo.table);
    posDefaultInfo.setCheck(posDefaultInfo.check);
    
    List<PrintText> printTexts = getPrintText(posDefaultInfo.check);
    
    posResponse.setPrintChecks(printTexts);
    posResponse.setPosDefaultInfo(posDefaultInfo);
    
    return posResponse;
  }
  
  private List<PrintText> getPrintText(String checkId)
  {
    Ticket ticket = TicketDAO.getInstance().loadFullTicket(checkId);
    
    List<PrintText> printTexts = new ArrayList();
    
    Store store = Application.getInstance().getStore();
    Terminal terminal = Application.getInstance().getTerminal();
    
    printTexts.add(new PrintText(store.getName(), "center"));
    printTexts.add(new PrintText(store.getAddressLine1(), "center"));
    printTexts.add(new PrintText(store.getTelephone(), "center"));
    
    printTexts.add(new PrintText(Messages.getString("PosRequestHandler.25"), "center"));
    
    String line = "__________________________________";
    
    printTexts.add(new PrintText(line, "center"));
    printTexts.add(new PrintText("*" + ticket.getOrderType() + "*", "center"));
    printTexts.add(new PrintText(Messages.getString("PosRequestHandler.29") + ticket.getTerminal().getId()));
    printTexts.add(new PrintText(Messages.getString("PosRequestHandler.30") + ticket.getId()));
    if (terminal.isShowTableNumber()) {
      printTexts.add(new PrintText(Messages.getString("PosRequestHandler.31") + ticket.getTableNumbers()));
    }
    else {
      printTexts.add(new PrintText(Messages.getString("PosRequestHandler.31") + ticket.getTableNames()));
    }
    
    printTexts.add(new PrintText(Messages.getString("PosRequestHandler.32") + ticket.getNumberOfGuests()));
    printTexts.add(new PrintText(Messages.getString("PosRequestHandler.33") + ticket.getOwner().getFirstName()));
    printTexts.add(new PrintText(Messages.getString("PosRequestHandler.34") + new Date()));
    
    printTexts.add(new PrintText(line, "center"));
    printTexts.add(new PrintText(Messages.getString("PosRequestHandler.35"), "right"));
    printTexts.add(new PrintText(line, "center"));
    
    if (ticket.getTicketItems() != null) {
      List<TicketItem> ticketItems = ticket.getTicketItems();
      for (TicketItem ticketItem : ticketItems) {
        printTexts.add(new PrintText(ticketItem.getName() + "   " + ticketItem.getQuantity() + "    " + ticketItem.getUnitPriceDisplay(), "right"));
      }
    }
    
    printTexts.add(new PrintText(line, "center"));
    
    printTexts.add(new PrintText(Messages.getString("PosRequestHandler.38") + ticket.getSubtotalAmount(), "right"));
    printTexts.add(new PrintText(Messages.getString("PosRequestHandler.39") + ticket.getTaxAmount(), "right"));
    
    printTexts.add(new PrintText(line, "center"));
    
    printTexts.add(new PrintText(Messages.getString("PosRequestHandler.40") + ticket.getTotalAmountWithTips(), "right"));
    printTexts.add(new PrintText(Messages.getString("PosRequestHandler.41") + ticket.getPaidAmount(), "right"));
    printTexts.add(new PrintText(Messages.getString("PosRequestHandler.42") + ticket.getDueAmount(), "right"));
    
    return printTexts;
  }
}
