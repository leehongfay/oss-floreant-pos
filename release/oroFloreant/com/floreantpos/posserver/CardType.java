package com.floreantpos.posserver;

public class CardType
{
  public static final String DEBIT = "0";
  public static final String CREDIT_MASTER_CARD = "1";
  public static final String CREDIT_VISA = "2";
  public static final String CREDIT_DISCOVERY = "4";
  public static final String CREDIT_AMEX = "5";
  public static final String CASH = "8";
  public static final String GIFT_CERTIFICATE = "9";
  
  public CardType() {}
}
