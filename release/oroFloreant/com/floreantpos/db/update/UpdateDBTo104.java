package com.floreantpos.db.update;

import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.TicketDAO;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;




















public class UpdateDBTo104
{
  public UpdateDBTo104() {}
  
  public void update()
    throws Exception
  {
    TicketDAO dao = TicketDAO.getInstance();
    Session session = null;
    Transaction transaction = null;
    try
    {
      session = dao.createNewSession();
      transaction = session.beginTransaction();
      
      Criteria criteria = session.createCriteria(Ticket.class);
      criteria.setProjection(Projections.rowCount());
      Object result = criteria.uniqueResult();
      int numTickets = 0;
      if ((result instanceof Number)) {
        numTickets = ((Number)result).intValue();
      }
      int increment = 1000;
      int count = 0;
      
      criteria.setProjection(null);
      while (count < numTickets) {
        criteria.setFirstResult(count);
        criteria.setMaxResults(1000);
        List<Ticket> list = criteria.list();
        for (Ticket ticket : list) {
          ticket.calculatePrice();
          session.update(ticket);
        }
        
        count += 1000;
      }
      transaction.commit();
    } catch (Exception e) {
      transaction.rollback();
      throw e;
    } finally {
      dao.closeSession(session);
    }
  }
}
