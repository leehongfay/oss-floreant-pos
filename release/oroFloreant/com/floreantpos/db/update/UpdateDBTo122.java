package com.floreantpos.db.update;

import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.InventoryTransactionType;
import com.floreantpos.model.dao.InventoryTransactionDAO;
import com.floreantpos.util.CopyUtil;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;



















public class UpdateDBTo122
{
  public UpdateDBTo122() {}
  
  public void update()
    throws Exception
  {
    doConvertUnchangedInventoryTransToInOut();
  }
  
  private void doConvertUnchangedInventoryTransToInOut() throws Exception {
    InventoryTransactionDAO dao = InventoryTransactionDAO.getInstance();
    List<InventoryTransaction> unchangedTransactions = dao.findTransactions("TRANSFER", InventoryTransactionType.UNCHANGED);
    Session session = null;
    Transaction tx = null;
    try {
      session = dao.createNewSession();
      tx = session.beginTransaction();
      if ((unchangedTransactions != null) && (unchangedTransactions.size() > 0)) {
        for (InventoryTransaction inventoryTransaction : unchangedTransactions) {
          InventoryTransaction outTrans = (InventoryTransaction)CopyUtil.deepCopy(inventoryTransaction);
          InventoryTransaction inTrans = (InventoryTransaction)CopyUtil.deepCopy(inventoryTransaction);
          
          outTrans.setId(null);
          outTrans.setTransactionType(InventoryTransactionType.OUT);
          outTrans.setToInventoryLocation(null);
          dao.saveOrUpdate(outTrans, session);
          
          inTrans.setId(null);
          inTrans.setTransactionType(InventoryTransactionType.IN);
          inTrans.setFromInventoryLocation(null);
          dao.saveOrUpdate(inTrans, session);
        }
        removeUnchangedInventoryTransactions(unchangedTransactions, session);
      }
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      throw e;
    } finally {
      dao.closeSession(session);
    }
  }
  
  private void removeUnchangedInventoryTransactions(List<InventoryTransaction> unchangedTransactions, Session session) {
    for (InventoryTransaction inventoryTransaction : unchangedTransactions) {
      session.delete(inventoryTransaction);
    }
  }
}
