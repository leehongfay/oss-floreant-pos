package com.floreantpos.db.update;

import com.floreantpos.PosLog;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketDiscount;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemCookingInstruction;
import com.floreantpos.model.TicketItemDiscount;
import com.floreantpos.model.TicketItemTax;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.util.DatabaseUtil;
import com.floreantpos.util.POSUtil;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;



















public class UpdateDBTo101
{
  private long INCREMENT = 1000L;
  private SessionFactory sessionFactory;
  private String dbConnectString;
  private String user;
  private String password;
  
  public UpdateDBTo101(String dbConnectString, String user, String password, SessionFactory sf) {
    this.dbConnectString = dbConnectString;
    this.user = user;
    this.password = password;
    sessionFactory = sf;
  }
  
  public void update() throws Exception {
    executeLiquibaseUpdate();
    executeUpdateSql();
    updateMenuPageItems();
    updateTicketItem();
    updateTicketItemModifier();
    updateTicket();
  }
  
  private void updateMenuPageItems() {
    List<MenuCategory> categories = MenuCategoryDAO.getInstance().findAll();
    for (Iterator localIterator1 = categories.iterator(); localIterator1.hasNext();) { menuCategory = (MenuCategory)localIterator1.next();
      List<MenuGroup> menuGroups = MenuGroupDAO.getInstance().findByParent(menuCategory);
      for (MenuGroup menuGroup : menuGroups) {
        menuGroup.setMenuCategory(menuCategory);
        MenuGroupDAO.getInstance().update(menuGroup);
      }
    }
    MenuCategory menuCategory;
  }
  
  private void executeLiquibaseUpdate() throws SQLException, DatabaseException, LiquibaseException { Connection connection = DriverManager.getConnection(dbConnectString, user, password);
    Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
    Liquibase liquibase = new Liquibase("db-changelog.xml", new ClassLoaderResourceAccessor(), database);
    liquibase.update(new Contexts(), new LabelExpression());
  }
  
  private void executeUpdateSql()
  {
    String[] sql = { "update menu_page_item set menu_item_name=(select m.name from menu_item as m where m.id=menu_item_id)", "update menu_page_item set btn_color=(select m.btn_color from menu_item as m where m.id=menu_item_id)", "update menu_page_item set TEXT_COLOR=(select m.TEXT_COLOR from menu_item as m where m.id=menu_item_id)", "update menu_page_item set IMAGE_ID=(select m.IMAGE_ID from menu_item as m where m.id=menu_item_id)", "update menu_page_item set SHOW_IMAGE_ONLY=(select m.SHOW_IMAGE_ONLY from menu_item as m where m.id=menu_item_id)", "UPDATE ticket SET owner_type_id=(select n_user_type from users where users.id=ticket.owner_id)" };
    





    Session session = sessionFactory.openSession();
    Transaction transaction = session.beginTransaction();
    for (int i = 0; i < sql.length; i++) {
      try {
        session.createSQLQuery(sql[i]).executeUpdate();
      } catch (Exception e) {
        PosLog.error(DatabaseUtil.class, e);
      }
    }
    transaction.commit();
    session.close();
  }
  







  private void updateTicketItem()
  {
    doConvertTicketItemTaxToProperties();
    doConvertTicketItemDiscountToProperties();
    doConvertTicketItemCookingInstructionToProperties();
  }
  





  private void updateTicketItemModifier()
  {
    doConvertTicketItemModifierTaxToProperties();
  }
  






  private void updateTicket()
  {
    doConvertTicketDiscountToProperties();
    doCopyTicketTableNumbersToStringProperty();
  }
  
  private void doConvertTicketItemTaxToProperties() {
    long count = getRowCount("ticket_item_tax");
    if (count == 0L)
      return;
    long startIndex = 0L;
    do {
      TerminalDAO dao = TerminalDAO.getInstance();
      List list = dao.executeSqlQuery("select id,name,rate,tax_amount,ticket_itemid from ticket_item_tax", startIndex, INCREMENT);
      if ((list == null) || (list.size() <= 0))
        return;
      Map<String, List<TicketItemTax>> ticketItemMap = new HashMap();
      for (Iterator iterator = list.iterator(); iterator.hasNext();) {
        Object[] object = (Object[])iterator.next();
        index = 0;
        TicketItemTax ticketItemTax = new TicketItemTax();
        ticketItemTax.setId(String.valueOf(object[(index++)]));
        ticketItemTax.setName(String.valueOf(object[(index++)]));
        ticketItemTax.setRate(Double.valueOf(POSUtil.parseDouble("" + object[(index++)])));
        ticketItemTax.setTaxAmount(Double.valueOf(POSUtil.parseDouble("" + object[(index++)])));
        
        String ticketItemId = String.valueOf(object[(index++)]);
        if (!StringUtils.isEmpty(ticketItemId))
        {

          List<TicketItemTax> ticketItemTaxList = (List)ticketItemMap.get(ticketItemId);
          if (ticketItemTaxList == null) {
            ticketItemTaxList = new ArrayList();
            ticketItemMap.put(ticketItemId, ticketItemTaxList);
          }
          ticketItemTaxList.add(ticketItemTax); } }
      int index;
      List<String> deleteSqlList = new ArrayList();
      List<String> insertSqlList = new ArrayList();
      
      for (String ticketItemId : ticketItemMap.keySet()) {
        List<TicketItemTax> ticketItemTaxes = (List)ticketItemMap.get(ticketItemId);
        TicketItem tmpTicketItem = new TicketItem(ticketItemId);
        tmpTicketItem.setTaxes(ticketItemTaxes);
        tmpTicketItem.buildTaxes();
        
        String propertyValue = tmpTicketItem.getTaxesProperty();
        String updateSql = "Update ticket_item set taxes_property='" + propertyValue + "' where id='" + ticketItemId + "'";
        insertSqlList.add(updateSql);
        
        PosLog.debug(UpdateDBTo101.class, updateSql);
      }
      dao.executeSqlQuery(deleteSqlList);
      dao.executeSqlQuery(insertSqlList);
      startIndex += INCREMENT;
      count -= INCREMENT;
    } while (count > 0L);
  }
  
  private long getRowCount(String tableName)
  {
    PosLog.debug(UpdateDBTo101.class, "Searching table data from " + tableName);
    
    TerminalDAO dao = TerminalDAO.getInstance();
    List list2 = dao.executeSqlQuery("select count(*) from " + tableName);
    long count = 0L;
    if ((list2 != null) && (list2.size() > 0)) {
      count = ((Number)list2.get(0)).longValue();
    }
    
    PosLog.debug(UpdateDBTo101.class, "Found row count " + count + " from " + tableName);
    return count;
  }
  
  private void doConvertTicketItemDiscountToProperties() {
    long count = getRowCount("ticket_item_discount");
    if (count == 0L)
      return;
    long startIndex = 0L;
    do {
      TerminalDAO dao = TerminalDAO.getInstance();
      String sql = "select id,discount_id,name,type,auto_apply,coupon_quanity,minimum_amount,value,amount,ticket_itemid from ticket_item_discount";
      List list = dao.executeSqlQuery(sql, startIndex, INCREMENT);
      if ((list == null) || (list.size() <= 0)) {
        return;
      }
      Map<String, List<TicketItemDiscount>> ticketItemMap = new HashMap();
      for (Iterator iterator = list.iterator(); iterator.hasNext();) {
        object = (Object[])iterator.next();
        if (!StringUtils.isEmpty(String.valueOf(object[5])))
        {
          int index = 0;
          TicketItemDiscount ticketItemDiscount = new TicketItemDiscount();
          ticketItemDiscount.setId(String.valueOf(object[(index++)]));
          ticketItemDiscount.setDiscountId(String.valueOf(object[(index++)]));
          ticketItemDiscount.setName(String.valueOf(object[(index++)]));
          ticketItemDiscount.setType(Integer.valueOf(POSUtil.parseInteger("" + object[(index++)])));
          ticketItemDiscount.setAutoApply(Boolean.valueOf("" + object[(index++)]));
          ticketItemDiscount.setCouponQuantity(Double.valueOf(POSUtil.parseDouble("" + object[(index++)])));
          ticketItemDiscount.setMinimumAmount(Double.valueOf(POSUtil.parseDouble("" + object[(index++)])));
          ticketItemDiscount.setValue(Double.valueOf(POSUtil.parseDouble("" + object[(index++)])));
          ticketItemDiscount.setAmount(Double.valueOf(POSUtil.parseDouble("" + object[(index++)])));
          ticketItemDiscount.setTicketItemId(String.valueOf(object[(index++)]));
          
          List<TicketItemDiscount> ticketItemDiscountList = (List)ticketItemMap.get(ticketItemDiscount.getTicketItemId());
          if (ticketItemDiscountList == null) {
            ticketItemDiscountList = new ArrayList();
            ticketItemMap.put(ticketItemDiscount.getTicketItemId(), ticketItemDiscountList);
          }
          ticketItemDiscountList.add(ticketItemDiscount); } }
      Object[] object;
      List<String> updateSqlList = new ArrayList();
      
      for (String ticketItemId : ticketItemMap.keySet()) {
        List<TicketItemDiscount> ticketItemDiscounts = (List)ticketItemMap.get(ticketItemId);
        
        TicketItem tmpTicketItem = new TicketItem();
        tmpTicketItem.setId(ticketItemId);
        tmpTicketItem.setDiscounts(ticketItemDiscounts);
        tmpTicketItem.buildDiscounts();
        
        String propertyValue = tmpTicketItem.getDiscountsProperty();
        String updateSql = "Update ticket_item set discounts_property='" + propertyValue + "' where id='" + ticketItemId + "'";
        updateSqlList.add(updateSql);
        
        PosLog.debug(UpdateDBTo101.class, updateSql);
      }
      dao.executeSqlQuery(updateSqlList);
      startIndex += INCREMENT;
      count -= INCREMENT;
    } while (count > 0L);
  }
  
  private void doConvertTicketItemCookingInstructionToProperties()
  {
    long count = getRowCount("ticket_item_cooking_instruction");
    if (count == 0L)
      return;
    long startIndex = 0L;
    do {
      TerminalDAO dao = TerminalDAO.getInstance();
      String sql = "select description,printedtokitchen,saved,ticket_item_id from ticket_item_cooking_instruction";
      List list = dao.executeSqlQuery(sql, startIndex, INCREMENT);
      if ((list == null) || (list.size() <= 0)) {
        return;
      }
      Map<String, List<TicketItemCookingInstruction>> ticketItemMap = new HashMap();
      for (Iterator iterator = list.iterator(); iterator.hasNext();) {
        object = (Object[])iterator.next();
        TicketItemCookingInstruction ticketItemCookingInstruction = new TicketItemCookingInstruction();
        int index = 0;
        ticketItemCookingInstruction.setDescription(String.valueOf(object[(index++)]));
        ticketItemCookingInstruction.setPrintedToKitchen(Boolean.valueOf("" + object[(index++)]));
        ticketItemCookingInstruction.setSaved(Boolean.valueOf("" + object[(index++)]));
        ticketItemCookingInstruction.setTicketItemId(String.valueOf(object[(index++)]));
        if (!StringUtils.isEmpty(ticketItemCookingInstruction.getTicketItemId()))
        {
          List<TicketItemCookingInstruction> ticketItemCookingInstructionList = (List)ticketItemMap.get(ticketItemCookingInstruction.getTicketItemId());
          if (ticketItemCookingInstructionList == null) {
            ticketItemCookingInstructionList = new ArrayList();
            ticketItemMap.put(ticketItemCookingInstruction.getTicketItemId(), ticketItemCookingInstructionList);
          }
          ticketItemCookingInstructionList.add(ticketItemCookingInstruction); } }
      Object[] object;
      List<String> updateSqlList = new ArrayList();
      
      for (String ticketItemId : ticketItemMap.keySet()) {
        List<TicketItemCookingInstruction> ticketItemCookingInstructiones = (List)ticketItemMap.get(ticketItemId);
        TicketItem tmpTicketItem = new TicketItem();
        tmpTicketItem.setId(ticketItemId);
        tmpTicketItem.setCookingInstructions(ticketItemCookingInstructiones);
        tmpTicketItem.buildCoookingInstructions();
        String propertyValue = tmpTicketItem.getCookingInstructionsProperty();
        String updateSql = "Update ticket_item set cooking_instructions_property='" + propertyValue + "' where id='" + ticketItemId + "'";
        updateSqlList.add(updateSql);
        
        PosLog.debug(UpdateDBTo101.class, updateSql);
      }
      dao.executeSqlQuery(updateSqlList);
      startIndex += INCREMENT;
      count -= INCREMENT;
    } while (count > 0L);
  }
  
  private void doCopyTicketTableNumbersToStringProperty() {
    long count = getRowCount("ticket_table_num");
    if (count == 0L) {
      return;
    }
    long startIndex = 0L;
    do {
      TerminalDAO dao = TerminalDAO.getInstance();
      String sql = "select table_id,ticket_id from ticket_table_num";
      List list = dao.executeSqlQuery(sql, startIndex, INCREMENT);
      if ((list == null) || (list.size() <= 0)) {
        return;
      }
      Map<String, List<Integer>> ticketMap = new HashMap();
      for (Iterator iterator = list.iterator(); iterator.hasNext();) {
        object = (Object[])iterator.next();
        int index = 0;
        Integer tableNumber = Integer.valueOf(POSUtil.parseInteger("" + object[(index++)]));
        String ticketId = String.valueOf(object[(index++)]);
        
        List<Integer> tableNumberList = (List)ticketMap.get(ticketId);
        if (tableNumberList == null) {
          tableNumberList = new ArrayList();
          ticketMap.put(ticketId, tableNumberList);
        }
        tableNumberList.add(tableNumber); }
      Object[] object;
      List<String> updateSqlList = new ArrayList();
      
      for (String ticketId : ticketMap.keySet()) {
        String ticketTableNumbers = "";
        List<Integer> tableNumbers = (List)ticketMap.get(ticketId);
        Iterator iterator; if ((tableNumbers != null) && (tableNumbers.size() > 0)) {
          for (iterator = tableNumbers.iterator(); iterator.hasNext();) {
            Integer tableNo = (Integer)iterator.next();
            ticketTableNumbers = ticketTableNumbers + tableNo;
            if (iterator.hasNext())
              ticketTableNumbers = ticketTableNumbers + ",";
          }
        }
        String updateSql = "Update ticket set table_num='" + ticketTableNumbers + "' where id='" + ticketId + "'";
        updateSqlList.add(updateSql);
        PosLog.debug(UpdateDBTo101.class, updateSql);
      }
      dao.executeSqlQuery(updateSqlList);
      startIndex += INCREMENT;
      count -= INCREMENT;
    } while (count > 0L);
  }
  
  private void doConvertTicketDiscountToProperties() {
    long count = getRowCount("ticket_discount");
    if (count == 0L) {
      return;
    }
    long startIndex = 0L;
    do {
      TerminalDAO dao = TerminalDAO.getInstance();
      String sql = "select id,discount_id,name,type,auto_apply,coupon_quanity,minimum_tamount,value,total_amount,ticket_id from ticket_discount";
      List list = dao.executeSqlQuery(sql, startIndex, INCREMENT);
      if ((list == null) || (list.size() <= 0)) {
        return;
      }
      Map<String, List<TicketDiscount>> ticketMap = new HashMap();
      for (Iterator iterator = list.iterator(); iterator.hasNext();) {
        object = (Object[])iterator.next();
        if (!StringUtils.isEmpty(String.valueOf(object[5])))
        {
          int index = 0;
          TicketDiscount ticketDiscount = new TicketDiscount();
          ticketDiscount.setId(String.valueOf(object[(index++)]));
          ticketDiscount.setDiscountId(String.valueOf(object[(index++)]));
          ticketDiscount.setName(String.valueOf(object[(index++)]));
          ticketDiscount.setType(Integer.valueOf(POSUtil.parseInteger("" + object[(index++)])));
          ticketDiscount.setAutoApply(Boolean.valueOf("" + object[(index++)]));
          ticketDiscount.setCouponQuantity(Double.valueOf(POSUtil.parseDouble("" + object[(index++)])));
          ticketDiscount.setMinimumAmount(Double.valueOf(POSUtil.parseDouble("" + object[(index++)])));
          ticketDiscount.setValue(Double.valueOf(POSUtil.parseDouble("" + object[(index++)])));
          ticketDiscount.setTotalDiscountAmount(Double.valueOf(POSUtil.parseDouble("" + object[(index++)])));
          ticketDiscount.setTicketId(String.valueOf(object[(index++)]));
          
          List<TicketDiscount> ticketDiscountList = (List)ticketMap.get(ticketDiscount.getTicketId());
          if (ticketDiscountList == null) {
            ticketDiscountList = new ArrayList();
            ticketMap.put(ticketDiscount.getTicketId(), ticketDiscountList);
          }
          ticketDiscountList.add(ticketDiscount); } }
      Object[] object;
      List<String> updateSqlList = new ArrayList();
      
      for (String ticketId : ticketMap.keySet()) {
        List<TicketDiscount> ticketDiscounts = (List)ticketMap.get(ticketId);
        Ticket tmpTicket = new Ticket(ticketId);
        tmpTicket.setDiscounts(ticketDiscounts);
        tmpTicket.buildDiscounts();
        
        String propertyValue = tmpTicket.getDiscountsProperty();
        String updateSql = "Update ticket set discounts_property='" + propertyValue + "' where id='" + ticketId + "'";
        updateSqlList.add(updateSql);
        PosLog.debug(UpdateDBTo101.class, updateSql);
      }
      dao.executeSqlQuery(updateSqlList);
      startIndex += INCREMENT;
      count -= INCREMENT;
    } while (count > 0L);
  }
  
  private void doConvertTicketItemModifierTaxToProperties() {
    long count = getRowCount("ticket_item_tax");
    if (count == 0L)
      return;
    long startIndex = 0L;
    do {
      TerminalDAO dao = TerminalDAO.getInstance();
      List list = dao.executeSqlQuery("select id,name,rate,tax_amount,ticket_item_modifier_id from ticket_item_tax", startIndex, INCREMENT);
      if ((list == null) || (list.size() <= 0))
        return;
      Map<String, List<TicketItemTax>> ticketItemMap = new HashMap();
      for (Iterator iterator = list.iterator(); iterator.hasNext();) {
        object = (Object[])iterator.next();
        int index = 0;
        TicketItemTax ticketItemTax = new TicketItemTax();
        ticketItemTax.setId(String.valueOf(object[(index++)]));
        ticketItemTax.setName(String.valueOf(object[(index++)]));
        ticketItemTax.setRate(Double.valueOf(POSUtil.parseDouble("" + object[(index++)])));
        ticketItemTax.setTaxAmount(Double.valueOf(POSUtil.parseDouble("" + object[(index++)])));
        
        String ticketItemModifierId = String.valueOf(object[(index++)]);
        if (!StringUtils.isEmpty(ticketItemModifierId))
        {

          List<TicketItemTax> ticketItemTaxList = (List)ticketItemMap.get(ticketItemModifierId);
          if (ticketItemTaxList == null) {
            ticketItemTaxList = new ArrayList();
            ticketItemMap.put(ticketItemModifierId, ticketItemTaxList);
          }
          ticketItemTaxList.add(ticketItemTax); } }
      Object[] object;
      List<String> updateSqlList = new ArrayList();
      for (String ticketItemModifierId : ticketItemMap.keySet()) {
        List<TicketItemTax> ticketItemTaxes = (List)ticketItemMap.get(ticketItemModifierId);
        TicketItem tmpTicketItem = new TicketItem(ticketItemModifierId);
        tmpTicketItem.setTaxes(ticketItemTaxes);
        tmpTicketItem.buildTaxes();
        
        String propertyValue = tmpTicketItem.getTaxesProperty();
        String updateSql = "Update ticket_item_modifier set taxes_property='" + propertyValue + "' where id='" + ticketItemModifierId + "'";
        updateSqlList.add(updateSql);
        
        PosLog.debug(UpdateDBTo101.class, updateSql);
      }
      dao.executeSqlQuery(updateSqlList);
      startIndex += INCREMENT;
      count -= INCREMENT;
    } while (count > 0L);
  }
}
