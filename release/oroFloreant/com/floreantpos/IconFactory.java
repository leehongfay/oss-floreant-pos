package com.floreantpos;

import com.floreantpos.model.ImageResource;
import com.floreantpos.model.dao.ImageResourceDAO;
import java.awt.Dimension;
import java.awt.Image;
import java.util.HashMap;
import javax.swing.ImageIcon;
import org.apache.commons.lang.StringUtils;


















public class IconFactory
{
  public IconFactory() {}
  
  private static HashMap<String, ImageIcon> iconCache = new HashMap();
  private static HashMap<String, ImageResource> imageResourceCache = new HashMap();
  
  public static ImageIcon getIcon(String iconName) {
    ImageIcon icon = (ImageIcon)iconCache.get(iconName);
    
    if (icon == null) {
      try {
        icon = new ImageIcon(IconFactory.class.getResource("/ui_icons/" + iconName));
        iconCache.put(iconName, icon);
      } catch (Exception x) {
        return getDefaultIcon(iconName);
      }
    }
    return icon;
  }
  
  private static ImageIcon getDefaultIcon(String iconName) {
    ImageIcon icon = (ImageIcon)iconCache.get(iconName);
    if (icon == null) {
      try {
        icon = new ImageIcon(IconFactory.class.getResource("/images/" + iconName));
        iconCache.put(iconName, icon);
      }
      catch (Exception localException) {}
    }
    return icon;
  }
  
  public static ImageIcon getIcon(String path, String iconName) {
    ImageIcon icon = (ImageIcon)iconCache.get(iconName);
    
    if (icon == null) {
      try {
        icon = new ImageIcon(IconFactory.class.getResource(path + iconName));
        iconCache.put(iconName, icon);
      } catch (Exception x) {
        return getIcon(iconName);
      }
    }
    return icon;
  }
  
  public static ImageIcon getIcon(String path, String iconName, Dimension size) {
    ImageIcon icon = (ImageIcon)iconCache.get(iconName);
    
    if (icon == null) {
      try {
        icon = new ImageIcon(IconFactory.class.getResource(path + iconName));
        icon = new ImageIcon(icon.getImage().getScaledInstance(width, height, 4));
        iconCache.put(iconName, icon);
      } catch (Exception x) {
        return getIcon(iconName);
      }
    }
    return icon;
  }
  
  public static ImageIcon getIconFromImageResource(String imageResourceId) {
    try {
      if (imageResourceId == null) {
        return null;
      }
      ImageResource imageResource = (ImageResource)imageResourceCache.get(imageResourceId);
      if (imageResource != null) {
        return new ImageIcon(imageResource.getImage());
      }
      
      imageResource = ImageResourceDAO.getInstance().findById(imageResourceId);
      if (imageResource != null) {
        imageResourceCache.put(imageResourceId, imageResource);
        return new ImageIcon(imageResource.getImage());
      }
    }
    catch (Exception localException) {}
    
    return null;
  }
  
  public static ImageIcon getIconFromImageResource(String imageResourceId, int width, int height) {
    try {
      if (StringUtils.isEmpty(imageResourceId)) {
        return null;
      }
      ImageResource imageResource = (ImageResource)imageResourceCache.get(imageResourceId);
      if ((imageResource != null) && (imageResource.getImage() != null)) {
        return new ImageIcon(imageResource.getImage().getScaledInstance(width, height, 1));
      }
      
      imageResource = ImageResourceDAO.getInstance().get(imageResourceId);
      if ((imageResource != null) && (imageResource.getImage() != null)) {
        imageResourceCache.put(String.valueOf(imageResourceId), imageResource);
        return new ImageIcon(imageResource.getImage().getScaledInstance(width, height, 1));
      }
    }
    catch (Exception localException) {}
    

    return null;
  }
}
