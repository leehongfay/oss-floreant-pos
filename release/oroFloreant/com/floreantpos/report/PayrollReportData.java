package com.floreantpos.report;

import com.floreantpos.model.User;
import java.util.Date;






public class PayrollReportData
{
  User user;
  Date date;
  Date from;
  Date to;
  double totalHour;
  double overtime;
  double rate;
  double overtimeRate;
  double regularPayment;
  double overtimePayment;
  double totalPayment;
  Date fromDateOfWeek;
  Date toDateOfWeek;
  double totalDeclaredTips;
  double totalNoCashTips;
  int firstDayOfWeek;
  private static final int SECOND = 1000;
  private static final int MINUTE = 60000;
  private static final int HOUR = 3600000;
  private static final int DAY = 86400000;
  
  public PayrollReportData() {}
  
  public User getUser()
  {
    return user;
  }
  
  public void setUser(User user) {
    this.user = user;
  }
  
  public Date getFrom() {
    return from;
  }
  
  public void setFrom(Date from) {
    this.from = from;
  }
  
  public Date getTo() {
    return to;
  }
  
  public void setTo(Date to) {
    this.to = to;
  }
  
  public double getTotalHour() {
    return totalHour;
  }
  
  public void setTotalHour(double totalHour) {
    this.totalHour = totalHour;
  }
  
  public double getRate() {
    return rate;
  }
  
  public void setRate(double rate) {
    this.rate = rate;
  }
  
  public double getTotalPayment() {
    return totalPayment;
  }
  
  public void setTotalPayment(double payment) {
    totalPayment = payment;
  }
  
  public Date getDate() {
    return date;
  }
  
  public void setDate(Date date) {
    this.date = date;
  }
  
  public void calculate() {
    long fromTime = from.getTime();
    long toTime = to.getTime();
    
    long milliseconds = toTime - fromTime;
    if (milliseconds < 0L) {
      totalHour = 0.0D;
      return;
    }
    
    double seconds = milliseconds / 1000.0D;
    double minutes = seconds / 60.0D;
    double hours = minutes / 60.0D;
    
    double diff = toTime - fromTime;
    diff /= 8.64E7D;
    totalHour = hours;
    rate = user.getCostPerHour().doubleValue();
    totalPayment = (totalHour * rate);
  }
  




  public void calculateTotalHour()
  {
    long fromTime = from.getTime();
    long toTime = to.getTime();
    
    long milliseconds = toTime - fromTime;
    long totalWorkHour = milliseconds / 3600000L;
    
    long regularWHInms = 144000000L;
    long overTimeWHms = milliseconds - regularWHInms;
    
    if (milliseconds < 0L) {
      totalHour = 0.0D;
      return;
    }
    
    double seconds = milliseconds / 1000.0D;
    double minutes = seconds / 60.0D;
    double hours = minutes / 60.0D;
    
    double diff = toTime - fromTime;
    diff /= 8.64E7D;
    totalHour += hours;
    overtime = (totalHour > 40.0D ? totalHour - 40.0D : 0.0D);
    rate = user.getCostPerHour().doubleValue();
    overtimeRate = user.getOvertimeRatePerHour().doubleValue();
    regularPayment = ((totalHour - overtime) * rate);
    overtimePayment = (overtime * overtimeRate);
    totalPayment = (regularPayment + overtimePayment);
  }
  
  public Date getFromDateOfWeek()
  {
    return fromDateOfWeek;
  }
  
  public void setFromDateOfWeek(Date fromDateOfWeek) {
    this.fromDateOfWeek = fromDateOfWeek;
  }
  
  public Date getToDateOfWeek() {
    return toDateOfWeek;
  }
  
  public void setToDateOfWeek(Date toDateOfWeek) {
    this.toDateOfWeek = toDateOfWeek;
  }
  
  public double getOvertime() {
    return overtime;
  }
  
  public void setOvertime(double overtime) {
    this.overtime = overtime;
  }
  
  public double getOvertimeRate() {
    return overtimeRate;
  }
  
  public void setOvertimeRate(double overtimeRate) {
    this.overtimeRate = overtimeRate;
  }
  
  public double getRegularPayment() {
    return regularPayment;
  }
  
  public void setRegularPayment(double regularPayment) {
    this.regularPayment = regularPayment;
  }
  
  public double getOvertimePayment() {
    return overtimePayment;
  }
  
  public void setOvertimePayment(double overtimePayment) {
    this.overtimePayment = overtimePayment;
  }
  
  public double getTotalDeclaredTips() {
    return totalDeclaredTips;
  }
  
  public void setTotalDeclaredTips(double totalDeclaredTips) {
    this.totalDeclaredTips = totalDeclaredTips;
  }
  
  public double getTotalNoCashTips() {
    return totalNoCashTips;
  }
  
  public void setTotalNoCashTips(double totalNoCashTips) {
    this.totalNoCashTips = totalNoCashTips;
  }
  
  public int getFirstDayOfWeek() {
    return firstDayOfWeek;
  }
  
  public void setFirstDayOfWeek(int firstDayOfWeek) {
    this.firstDayOfWeek = firstDayOfWeek;
  }
}
