package com.floreantpos.report;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.VoidItem;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.dao.VoidItemDAO;
import com.floreantpos.model.ext.PaperSize;
import com.floreantpos.services.report.ReportService;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.POSUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;
import org.jdesktop.swingx.calendar.DateUtils;



















public class VoidItemReport
  extends Report
{
  private VoidItemReportModel voidItemReportModel;
  private VoidItemReportModel voidTicketReportModel;
  
  public VoidItemReport() {}
  
  public void refresh()
    throws Exception
  {
    boolean isModelAdded = createModels();
    if (!isModelAdded) {
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), "No Information found!");
      viewer = null;
      return;
    }
    JasperReport itemReport = ReportUtil.getReport("deleteItem_sub_report");
    JasperReport ticketReport = ReportUtil.getReport("deleteItem_sub_report");
    
    HashMap map = new HashMap();
    ReportUtil.populateRestaurantProperties(map);
    map.put("reportTitle", "Void Item Report");
    map.put("reportTime", ReportService.formatFullDate(new Date()));
    map.put("dateRange", ReportService.formatShortDate(getStartDate()) + " to " + ReportService.formatShortDate(getEndDate()));
    map.put("terminalName", getTerminal() == null ? POSConstants.ALL : getTerminal().getName());
    map.put("currency", Messages.getString("SalesReport.8") + CurrencyUtil.getCurrencyName() + " (" + CurrencyUtil.getCurrencySymbol() + ")");
    map.put("itemDataSource", new JRTableModelDataSource(voidItemReportModel));
    map.put("ticketDataSource", new JRTableModelDataSource(voidTicketReportModel));
    map.put("ticketSection", "Void Tickets");
    map.put("itemSection", "Void Items");
    map.put("itemReport", itemReport);
    map.put("ticketReport", ticketReport);
    JasperReport masterReport = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("void_item_report"));
    JasperPrint print = JasperFillManager.fillReport(masterReport, map, new JREmptyDataSource());
    viewer = new JRViewer(print);
  }
  
  public boolean isDateRangeSupported()
  {
    return true;
  }
  
  public boolean isTypeSupported()
  {
    return true;
  }
  
  public boolean createModels() {
    Date date1 = DateUtils.startOfDay(getStartDate());
    Date date2 = DateUtils.endOfDay(getEndDate());
    
    List<VoidItem> voidItems = VoidItemDAO.getInstance().findByDate(date1, date2, getTerminal());
    if ((voidItems == null) || (voidItems.isEmpty())) {
      return false;
    }
    List<Ticket> voidTickets = TicketDAO.getInstance().findVoidTicketByDate(date1, date2, getTerminal());
    List<DeletedItem> deletedItems = new ArrayList();
    List<DeletedItem> deletedTickets = new ArrayList();
    
    for (VoidItem voidItem : voidItems) {
      DeletedItem deletedItem = new DeletedItem();
      deletedItem.setId(voidItem.getId());
      deletedItem.setVoidDate(voidItem.getVoidDate());
      deletedItem.setTicketId(voidItem.getTicketId());
      deletedItem.setQuantity(voidItem.getQuantity().doubleValue());
      deletedItem.setName(voidItem.getMenuItemName());
      deletedItem.setTotal(voidItem.getTotalPrice().doubleValue());
      deletedItem.setVoidReason(voidItem.getVoidReason());
      User owner = TicketDAO.getInstance().findOwner(voidItem.getTicketId());
      deletedItem.setOwner(owner);
      deletedItem.setVoidUser(voidItem.getVoidByUser());
      deletedItems.add(deletedItem);
    }
    
    voidItemReportModel = new VoidItemReportModel();
    voidItemReportModel.setItems(deletedItems);
    
    for (Ticket voidTicket : voidTickets) {
      DeletedItem deletedItem = new DeletedItem();
      deletedItem.setId(voidTicket.getId());
      deletedItem.setVoidDate(voidTicket.getClosingDate());
      deletedItem.setTicketId(voidTicket.getId());
      
      List<VoidItem> voidItemList = VoidItemDAO.getInstance().getVoidItems(voidTicket.getId());
      
      double amount = 0.0D;
      double quantity = 0.0D;
      if (voidItemList != null) {
        for (VoidItem item : voidItemList) {
          amount += item.getTotalPrice().doubleValue();
          quantity += item.getQuantity().doubleValue();
        }
        
        deletedItem.setQuantity(quantity);
        deletedItem.setTotal(amount);
      }
      
      deletedItem.setVoidReason(voidTicket.getVoidReason());
      deletedItem.setOwner(voidTicket.getOwner());
      deletedItem.setVoidUser(voidTicket.getVoidedBy());
      deletedTickets.add(deletedItem);
    }
    
    voidTicketReportModel = new VoidItemReportModel();
    voidTicketReportModel.setItems(deletedTickets);
    return true;
  }
}
