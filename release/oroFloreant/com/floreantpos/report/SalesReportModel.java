package com.floreantpos.report;

import java.text.DecimalFormat;
import java.util.List;
import javax.swing.table.AbstractTableModel;

















public class SalesReportModel
  extends AbstractTableModel
{
  private static DecimalFormat formatter = new DecimalFormat("#,##0.00");
  
  private String[] columnNames = { "barcode", "name", "qty", "price", "cost", "costPercentage", "grossTotal", "tax", "netTotal", "group", "discount" };
  
  private List<ReportItem> items;
  private double grandTotal;
  private double totalQuantity;
  private double taxTotal;
  private double netTotal;
  private double discountTotal;
  
  public SalesReportModel() {}
  
  public int getRowCount()
  {
    if (items == null) {
      return 0;
    }
    
    return items.size();
  }
  
  public int getColumnCount() {
    return columnNames.length;
  }
  
  public String getColumnName(int column)
  {
    return columnNames[column];
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    ReportItem item = (ReportItem)items.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      return item.getBarcode() != null ? item.getBarcode() : item.getId();
    case 1: 
      return item.getName();
    
    case 2: 
      return Double.valueOf(item.getQuantity());
    
    case 3: 
      return Double.valueOf(item.getPrice());
    case 4: 
      return Double.valueOf(item.getCost());
    
    case 5: 
      if (item.getAdjustedPrice() > 0.0D)
        return Double.valueOf(item.getCost() / item.getAdjustedPrice() * 100.0D);
      return Double.valueOf(0.0D);
    
    case 6: 
      return Double.valueOf(item.getGrossTotal());
    
    case 7: 
      return Double.valueOf(item.getTaxTotal());
    
    case 8: 
      return Double.valueOf(item.getNetTotal());
    
    case 9: 
      if (item.getGroupName() != null) {
        return "Group: " + item.getGroupName();
      }
      return "Group: NONE";
    
    case 10: 
      return Double.valueOf(item.getDiscount());
    }
    
    return null;
  }
  
  public List<ReportItem> getItems() {
    return items;
  }
  
  public void setItems(List<ReportItem> items) {
    this.items = items;
  }
  
  public double getGrandTotal() {
    return grandTotal;
  }
  
  public String getGrandTotalAsString() {
    return formatter.format(grandTotal);
  }
  
  public void setGrandTotal(double grandTotal) {
    this.grandTotal = grandTotal;
  }
  
  public void calculateGrandTotal() {
    grandTotal = 0.0D;
    if (items == null) {
      return;
    }
    
    for (ReportItem item : items) {
      grandTotal += item.getNetTotal();
    }
  }
  
  public String getTaxTotalAsString() {
    return formatter.format(taxTotal);
  }
  
  public void setTaxTotal(double taxTotal) {
    this.taxTotal = taxTotal;
  }
  
  public void calculateTaxTotal() {
    taxTotal = 0.0D;
    if (items == null) {
      return;
    }
    
    for (ReportItem item : items) {
      taxTotal += item.getTaxTotal();
    }
  }
  
  public String getNetTotalAsString() {
    return formatter.format(netTotal);
  }
  
  public void setNetTotal(double netTotal) {
    this.netTotal = netTotal;
  }
  
  public void calculateNetTotal() {
    netTotal = 0.0D;
    if (items == null) {
      return;
    }
    
    for (ReportItem item : items) {
      netTotal += item.getGrossTotal();
    }
  }
  
  public String getTotalQuantityAsString() {
    return String.valueOf(totalQuantity);
  }
  
  public void setTotalQuantity(int totalQuantity) {
    this.totalQuantity = totalQuantity;
  }
  
  public void calculateTotalQuantity() {
    totalQuantity = 0.0D;
    if (items == null) {
      return;
    }
    
    for (ReportItem item : items) {
      totalQuantity += item.getQuantity();
    }
  }
  
  public String getDiscountTotalAsString() {
    return String.valueOf(discountTotal);
  }
  
  public void setDiscountTotal(int discountTotal) {
    this.discountTotal = discountTotal;
  }
  
  public void calculateDiscountTotal() {
    discountTotal = 0.0D;
    if (items == null) {
      return;
    }
    
    for (ReportItem item : items) {
      discountTotal += item.getDiscount();
    }
  }
}
