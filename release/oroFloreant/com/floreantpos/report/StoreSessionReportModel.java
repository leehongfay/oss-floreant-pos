package com.floreantpos.report;

import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.StoreSession;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.CashDrawerDAO;
import com.floreantpos.print.PosPrintService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.swing.table.AbstractTableModel;
import org.apache.commons.collections.map.HashedMap;




















public class StoreSessionReportModel
  extends AbstractTableModel
{
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy hh:mm a");
  private String[] columnNames = { "openTime", "closeTime", "openedBy", "closedBy", "totalAmount", "salesTax", "totalRevenues", "creditCardReceipt", "grossReceipt", "cashAccountable" };
  
  private List<StoreSession> items;
  private Map<String, CashDrawer> reportMap = new HashedMap();
  

  public StoreSessionReportModel() {}
  
  public int getRowCount()
  {
    if (items == null) {
      return 0;
    }
    return items.size();
  }
  
  public int getColumnCount() {
    return columnNames.length;
  }
  
  public String getColumnName(int column)
  {
    return columnNames[column];
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    StoreSession item = (StoreSession)items.get(rowIndex);
    CashDrawer summary = (CashDrawer)reportMap.get(item.getId());
    
    switch (columnIndex) {
    case 0: 
      Date openTime = item.getOpenTime();
      if (openTime == null)
        return "";
      return dateFormat.format(openTime);
    
    case 1: 
      Date closeTime = item.getCloseTime();
      if (closeTime == null)
        return "";
      return dateFormat.format(closeTime);
    
    case 2: 
      User openedBy = item.getOpenedBy();
      if (openedBy == null)
        return "";
      return openedBy.getFirstName();
    
    case 3: 
      User closedBy = item.getClosedBy();
      if (closedBy == null)
        return "";
      return closedBy.getFirstName();
    
    case 4: 
      return summary.getNetSales();
    
    case 5: 
      return summary.getSalesTax();
    case 6: 
      return summary.getTotalRevenue();
    case 7: 
      return summary.getCreditCardReceiptAmount();
    case 8: 
      return summary.getGrossReceipts();
    case 9: 
      return summary.getDrawerAccountable();
    }
    return null;
  }
  
  public List<StoreSession> getItems() {
    return items;
  }
  
  public void setItems(List<StoreSession> items) {
    this.items = items;
    if (items != null) {
      for (StoreSession storeSession : items) {
        List<CashDrawer> cashDrawers = CashDrawerDAO.getInstance().findByStoreOperationData(storeSession, null);
        if (cashDrawers != null) {
          CashDrawer report = PosPrintService.populateCashDrawerReportSummary(cashDrawers);
          reportMap.put(storeSession.getId(), report);
        }
      }
    }
  }
}
