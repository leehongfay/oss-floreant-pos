package com.floreantpos.report;

import com.floreantpos.swing.ListTableModel;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class InventoryOnHandReportModel extends ListTableModel
{
  SimpleDateFormat dateFormat2 = new SimpleDateFormat("MMM-dd-yyyy hh:mm a");
  
  DecimalFormat decimalFormat = new DecimalFormat("0.00");
  
  public InventoryOnHandReportModel() {
    super(new String[] { "groupName", "itemName", "sku", "location", "qty", "unit", "itemCost", "totalCost" });
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    InventoryStockData data = (InventoryStockData)rows.get(rowIndex);
    
    switch (columnIndex)
    {
    case 0: 
      return data.getMenuGroup();
    
    case 1: 
      return data.getMenuItemName();
    
    case 2: 
      return data.getSku();
    
    case 3: 
      return data.getLocationName();
    
    case 4: 
      return Double.valueOf(data.getQuantityInHand());
    
    case 5: 
      return data.getUnit();
    
    case 6: 
      return Double.valueOf(data.getMenuItemCost());
    
    case 7: 
      return Double.valueOf(data.getQuantityInHand() * data.getMenuItemCost());
    }
    return null;
  }
}
