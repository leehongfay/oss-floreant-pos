package com.floreantpos.report;

import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.Store;
import com.floreantpos.model.dao.InventoryTransactionDAO;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;
import org.jdesktop.swingx.JXDatePicker;



















































































public class InventoryAverageUnitCostReportView
  extends TransparentPanel
{
  private JButton btnGo;
  private JXDatePicker fromDatePicker;
  private JXDatePicker toDatePicker;
  private JPanel reportPanel;
  private JPanel contentPane;
  private JComboBox jcbTtype;
  private JComboBox cbGroup;
  private JComboBox cbCategory;
  private List<MenuGroup> groups;
  
  private void viewReport(List<InventoryTransaction> inventList, String searchKey)
  {
    try
    {
      Set<MenuItem> uniqueMenuItems = new HashSet();
      for (InventoryTransaction trans : inventList) {
        uniqueMenuItems.add(trans.getMenuItem());
      }
      
      Date date = fromDatePicker.getDate();
      Calendar c = Calendar.getInstance();
      c.setTime(date);
      c.add(2, -1);
      date = c.getTime();
      
      Date startOfMonth = DateUtil.startOfMonth(date);
      Date endOfMonth = DateUtil.endOfMonth(date);
      for (Iterator localIterator2 = uniqueMenuItems.iterator(); localIterator2.hasNext();) { menuItem = (MenuItem)localIterator2.next();
        openingBalance = InventoryTransactionDAO.getInstance().findOpeningBalance(menuItem, startOfMonth, endOfMonth);
        for (InventoryTransaction trans : inventList)
          if (trans.getMenuItem() == menuItem) {
            trans.setOpeningCost(openingBalance.getUnitCost().doubleValue());
            trans.setOpeningQty(openingBalance.getQuantity().doubleValue());
            trans.setOpeningTotalCost(openingBalance.getTotal().doubleValue());
          }
      }
      MenuItem menuItem;
      InventoryTransaction openingBalance;
      JasperReport report = ReportParser.getReport("/reports/inventoryAverageUnitCost.jasper");
      
      HashMap properties = new HashMap();
      ReportUtil.populateRestaurantProperties(properties);
      String dateRange = "Report on  " + DateUtil.getOnlyFormattedDate(new Date());
      
      properties.put("DateRange", dateRange);
      
      properties.put("group", searchKey);
      

      Store store = Application.getInstance().getStore();
      
      properties.put("companyName", store.getName());
      properties.put("address", store.getAddressLine1());
      properties.put("city", store.getAddressLine2());
      properties.put("address3", store.getAddressLine3());
      properties.put("phone", store.getTelephone());
      
      InventoryAverageCostUnitReportModel reportModel = new InventoryAverageCostUnitReportModel();
      if ((inventList != null) && (inventList.size() > 0)) {
        reportModel.setRows(inventList);
      }
      else {
        reportPanel.removeAll();
        POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), "No information found!");
        return;
      }
      
      properties.put("totalExpenses", String.valueOf(reportModel.getTotalAmount()));
      properties.put("totalQuantity", String.valueOf(reportModel.getTotalQuantity()));
      
      JasperPrint print = JasperFillManager.fillReport(report, properties, new JRTableModelDataSource(reportModel));
      
      JRViewer viewer = new JRViewer(print);
      reportPanel.removeAll();
      reportPanel.add(viewer);
      reportPanel.revalidate();
    } catch (JRException e) {
      e.printStackTrace();
    }
  }
  


  public InventoryAverageUnitCostReportView()
  {
    $$$setupUI$$$();TerminalDAO terminalDAO = new TerminalDAO();List terminals = terminalDAO.findAll();terminals.add(0, POSConstants.ALL);setLayout(new BorderLayout());add(contentPane);btnGo.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        InventoryTransactionDAO dao = new InventoryTransactionDAO();
        




        Object selectedCategory = cbCategory.getSelectedItem();
        MenuCategory category = null;
        if ((selectedCategory instanceof MenuCategory)) {
          category = (MenuCategory)selectedCategory;
        }
        Object selectedGroup = cbGroup.getSelectedItem();
        MenuGroup group = null;
        if ((selectedGroup instanceof MenuGroup)) {
          group = (MenuGroup)selectedGroup;
        }
        
        Date startOfMonth = DateUtil.startOfMonth(fromDatePicker.getDate());
        Date endOfMonth = DateUtil.endOfMonth(fromDatePicker.getDate());
        List<InventoryTransaction> findTransaction = dao.findTransactionsForAvgCosting(group, startOfMonth, endOfMonth);
        

        String strCategory = "All";
        String strGroup = "All";
        




        if (category != null) {
          strCategory = category.getName();
        }
        
        if (group != null) {
          strGroup = group.getName();
        }
        String searchKey = " Category: " + strCategory + "; Group: " + strGroup + ";";
        InventoryAverageUnitCostReportView.this.viewReport(findTransaction, searchKey);
      }
    });
  }
  


















































































































  private void $$$setupUI$$$()
  {
    contentPane = new JPanel();
    contentPane.setLayout(new MigLayout("fill,hidemode 3", "", "[][][grow]"));
    









    JLabel lblFrom = new JLabel(POSConstants.MONTH + ":");
    


    fromDatePicker = UiUtil.getCurrentMonthStart();
    
    btnGo = new JButton();
    btnGo.setPreferredSize(PosUIManager.getSize(100, 0));
    btnGo.setText(POSConstants.GO);
    
    JLabel lblCategory = new JLabel("Category:");
    cbCategory = new JComboBox();
    
    cbCategory.addItem("<All>");
    
    List<MenuCategory> categories = MenuCategoryDAO.getInstance().findAll();
    for (Iterator localIterator = categories.iterator(); localIterator.hasNext();) { category = (MenuCategory)localIterator.next();
      cbCategory.addItem(category);
    }
    MenuCategory category;
    cbCategory.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        Object selectedItem = cbCategory.getSelectedItem();
        List<MenuGroup> groupList; if ((selectedItem instanceof MenuCategory)) {
          groupList = MenuGroupDAO.getInstance().findByParent((MenuCategory)selectedItem);
          cbGroup.removeAllItems();
          for (MenuGroup group : groupList) {
            cbGroup.addItem(group);
          }
        }
        else {
          cbGroup.removeAllItems();
          cbGroup.addItem("<All>");
          for (MenuGroup group : groups) {
            cbGroup.addItem(group);
          }
          
        }
      }
    });
    JLabel lblGroup = new JLabel("Groups:");
    cbGroup = new JComboBox();
    cbGroup.addItem("<All>");
    groups = MenuGroupDAO.getInstance().findAll();
    for (MenuGroup group : groups) {
      cbGroup.addItem(group);
    }
    
    JSeparator separator1 = new JSeparator();
    reportPanel = new JPanel();
    reportPanel.setLayout(new BorderLayout(0, 0));
    

    contentPane.add(lblCategory, "split 9");
    contentPane.add(cbCategory, "gapright 20");
    contentPane.add(lblGroup);
    contentPane.add(cbGroup, "gapright 20");
    contentPane.add(lblFrom);
    contentPane.add(fromDatePicker, "gapright 20");
    


    contentPane.add(btnGo);
    contentPane.add(separator1, "newline,growx");
    contentPane.add(reportPanel, "newline,grow,span");
  }
  


  public JComponent $$$getRootComponent$$$()
  {
    return contentPane;
  }
}
