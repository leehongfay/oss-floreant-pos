package com.floreantpos.report;

import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.ui.ticket.TicketItemRowCreator;
import com.floreantpos.util.NumberUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
















public class TicketDataSource
  extends AbstractReportDataSource
{
  public TicketDataSource()
  {
    super(new String[] { "itemName", "itemQty", "itemSubtotal" });
  }
  
  public TicketDataSource(Ticket ticket) {
    super(new String[] { "itemName", "itemQty", "itemSubtotal" });
    
    setTicket(ticket);
  }
  
  private void setTicket(Ticket ticket) {
    Collections.sort(ticket.getTicketItems(), new Comparator()
    {
      public int compare(TicketItem o1, TicketItem o2)
      {
        return o1.isVoided().compareTo(o2.isVoided());
      }
    });
    ArrayList<ITicketItem> rows = new ArrayList();
    
    LinkedHashMap<String, ITicketItem> tableRows = new LinkedHashMap();
    TicketItemRowCreator.calculateTicketRows(ticket, tableRows, true, false, true);
    
    rows.addAll(tableRows.values());
    setRows(rows);
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    ITicketItem item = (ITicketItem)rows.get(rowIndex);
    if ((item instanceof TicketItem)) {
      TicketItem ticketItem = (TicketItem)item;
      ticketItem.setIncludeVoidQuantity(Boolean.valueOf(true));
    }
    
    switch (columnIndex) {
    case 0: 
      return item.getNameDisplay();
    
    case 1: 
      return "";
    

    case 2: 
      Double total = item.getSubTotalAmountDisplay();
      if (total == null) {
        return null;
      }
      return NumberUtil.formatNumberAcceptNegative(total);
    }
    
    return null;
  }
}
