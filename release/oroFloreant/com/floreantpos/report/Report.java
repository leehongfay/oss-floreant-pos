package com.floreantpos.report;

import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.UserType;
import com.floreantpos.model.util.DateUtil;
import java.util.Date;
import java.util.List;
import net.sf.jasperreports.view.JRViewer;




















public abstract class Report
{
  public static final int REPORT_TYPE_1 = 0;
  public static final int REPORT_TYPE_2 = 1;
  private Date fromDate;
  private Date toDate;
  private Terminal terminal;
  private UserType userType;
  private int reportType = 0;
  private boolean includeFreeItem = false;
  private boolean isShowInGroups = false;
  
  protected JRViewer viewer;
  
  private MenuGroup menuGroup;
  private List<InventoryLocation> inventoryLocations;
  
  public Report() {}
  
  public abstract void refresh()
    throws Exception;
  
  public abstract boolean isDateRangeSupported();
  
  public abstract boolean isTypeSupported();
  
  public JRViewer getViewer() { return viewer; }
  
  private List<MenuCategory> menuCategories;
  
  public Date getEndDate() { if (toDate == null) {
      return DateUtil.endOfDay(new Date());
    }
    return toDate; }
  
  private List<MenuGroup> menuGroups;
  
  public void setEndDate(Date toDate) { this.toDate = toDate; }
  
  private List<Terminal> terminals;
  private List<User> users;
  public int getReportType() {
    return reportType;
  }
  
  public void setReportType(int reportType) {
    this.reportType = reportType;
  }
  
  public boolean isIncludedFreeItems() {
    return includeFreeItem;
  }
  
  public void setIncludeFreeItems(boolean includeFreeItem) {
    this.includeFreeItem = includeFreeItem;
  }
  
  public boolean isShowInGroups() {
    return isShowInGroups;
  }
  
  public void setShowInGroups(boolean isShowInGroups) {
    this.isShowInGroups = isShowInGroups;
  }
  
  public Terminal getTerminal() {
    return terminal;
  }
  
  public void setTerminal(Terminal terminal) {
    this.terminal = terminal;
  }
  
  public UserType getUserType() {
    return userType;
  }
  
  public void setUserType(UserType userType) {
    this.userType = userType;
  }
  
  public Date getStartDate() {
    if (fromDate == null) {
      return DateUtil.startOfDay(new Date());
    }
    return fromDate;
  }
  
  public void setStartDate(Date fromDate) {
    this.fromDate = fromDate;
  }
  
  public MenuGroup getMenuGroup() {
    return menuGroup;
  }
  
  public void setMenuGroup(MenuGroup menuGroup) {
    this.menuGroup = menuGroup;
  }
  
  public List<InventoryLocation> getInventoryLocations() {
    return inventoryLocations;
  }
  
  public void setInventoryLocations(List<InventoryLocation> inventoryLocations) {
    this.inventoryLocations = inventoryLocations;
  }
  
  public List<MenuCategory> getMenuCategories() {
    return menuCategories;
  }
  
  public void setMenuCategories(List<MenuCategory> menuCategories) {
    this.menuCategories = menuCategories;
  }
  
  public List<MenuGroup> getMenuGroups() {
    return menuGroups;
  }
  
  public void setMenuGroups(List<MenuGroup> menuGroups) {
    this.menuGroups = menuGroups;
  }
  
  public List<Terminal> getTerminals() {
    return terminals;
  }
  
  public void setTerminals(List<Terminal> terminals) {
    this.terminals = terminals;
  }
  
  public List<User> getUsers() {
    return users;
  }
  
  public void setUsers(List<User> users) {
    this.users = users;
  }
}
