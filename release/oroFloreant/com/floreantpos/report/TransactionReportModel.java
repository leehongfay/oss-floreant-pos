package com.floreantpos.report;

import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.User;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.table.AbstractTableModel;



















public class TransactionReportModel
  extends AbstractTableModel
{
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
  private String[] columnNames = { "transTime", "transNo", "owner", "paymentType", "totalAmount" };
  
  private List<PosTransaction> items;
  
  public TransactionReportModel() {}
  
  public int getRowCount()
  {
    if (items == null) {
      return 0;
    }
    return items.size();
  }
  
  public int getColumnCount() {
    return columnNames.length;
  }
  
  public String getColumnName(int column)
  {
    return columnNames[column];
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    PosTransaction transaction = (PosTransaction)items.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      return dateFormat.format(transaction.getTransactionTime());
    
    case 1: 
      return transaction.getId();
    
    case 2: 
      User user = transaction.getUser();
      if (user == null)
        return "";
      return user.getFirstName() + " [" + user.getId() + "]";
    
    case 3: 
      return transaction.getPaymentType();
    
    case 4: 
      return transaction.getAmount();
    }
    return null;
  }
  
  public List<PosTransaction> getItems() {
    return items;
  }
  
  public void setItems(List<PosTransaction> items) {
    this.items = items;
  }
}
