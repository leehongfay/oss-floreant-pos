package com.floreantpos.report;

import com.floreantpos.PosLog;
import com.floreantpos.model.Store;
import com.floreantpos.model.dao.StoreDAO;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;




















public class ReportUtil
{
  private static Log logger = LogFactory.getLog(ReportUtil.class);
  private static final String USER_REPORT_DIR = "/printerlayouts/";
  private static final String DEFAULT_REPORT_DIR = "/com/floreantpos/report/template/";
  
  public ReportUtil() {}
  
  public static void populateRestaurantProperties(Map map) { JasperReport reportHeader = getReport("report_header");
    
    Store store = StoreDAO.getRestaurant();
    if (store != null) {
      map.put("restaurantName", store.getName());
      map.put("addressLine1", store.getAddressLine1());
      map.put("addressLine2", store.getAddressLine2());
      map.put("addressLine3", store.getAddressLine3());
      map.put("phone", store.getTelephone());
      map.put("reportHeader", reportHeader);
    }
  }
  
  public static JasperReport getReport(String reportName) {
    InputStream resource = null;
    try
    {
      resource = ReceiptPrintService.class.getResourceAsStream("/printerlayouts/" + reportName + ".jasper");
      JasperReport localJasperReport1; if (resource == null) {
        if (new File(ReceiptPrintService.class.getResource("/printerlayouts/" + reportName + ".jrxml").getFile()) != null) {
          return compileReport(reportName);
        }
        
        throw new Exception();
      }
      


      return (JasperReport)JRLoader.loadObject(resource);
    }
    catch (Exception e)
    {
      return getDefaultReport(reportName);
    }
    finally {
      IOUtils.closeQuietly(resource);
    }
  }
  
  private static JasperReport compileReport(String reportName) throws Exception
  {
    InputStream in = null;
    InputStream in2 = null;
    FileOutputStream out = null;
    File jasperFile = null;
    try
    {
      File jrxmlFile = new File(ReceiptPrintService.class.getResource("/printerlayouts/" + reportName + ".jrxml").getFile());
      File dir = jrxmlFile.getParentFile();
      jasperFile = new File(dir, reportName + ".jasper");
      
      in = ReceiptPrintService.class.getResourceAsStream("/printerlayouts/" + reportName + ".jrxml");
      out = new FileOutputStream(jasperFile);
      JasperCompileManager.compileReportToStream(in, out);
      
      in2 = ReceiptPrintService.class.getResourceAsStream("/printerlayouts/" + reportName + ".jasper");
      return (JasperReport)JRLoader.loadObject(in2);
    }
    catch (Exception e) {
      PosLog.info(ReportUtil.class, e + "");
      IOUtils.closeQuietly(out);
      if (jasperFile != null) {
        jasperFile.delete();
      }
      
      throw e;
    }
    finally
    {
      IOUtils.closeQuietly(in);
      IOUtils.closeQuietly(in2);
      IOUtils.closeQuietly(out);
    }
  }
  
  private static JasperReport getDefaultReport(String reportName) {
    InputStream resource = null;
    
    try
    {
      resource = ReceiptPrintService.class.getResourceAsStream("/com/floreantpos/report/template/" + reportName + ".jasper");
      return (JasperReport)JRLoader.loadObject(resource);
    }
    catch (Exception e) {
      logger.error(e);
      return null;
    }
    finally {
      IOUtils.closeQuietly(resource);
    }
  }
  
  public static void main(String[] args) {
    URL resource = ReceiptPrintService.class.getResource("/printerlayouts/ticket-receipt.jrxml");
    String externalForm = resource.getFile();
    PosLog.info(ReportUtil.class, resource.getProtocol());
    PosLog.info(ReportUtil.class, externalForm);
  }
}
