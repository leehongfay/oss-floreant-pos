package com.floreantpos.report;

import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.InventoryTransactionType;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.Store;
import com.floreantpos.model.dao.InventoryTransactionDAO;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.MultiSelectComboBox;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;
import org.jdesktop.swingx.JXDatePicker;


















public class InventoryTransactionReportView
  extends TransparentPanel
{
  private JButton btnGo;
  private JXDatePicker fromDatePicker;
  private JXDatePicker toDatePicker;
  private JPanel reportPanel;
  private JPanel contentPane;
  private JComboBox cbGroup;
  private JComboBox cbCategory;
  private List<MenuGroup> groups;
  private JTextField tfNameSku;
  private JCheckBox chkShowInGroups;
  private MultiSelectComboBox<InventoryTransactionType> mscTtype;
  
  private void viewReport()
  {
    Date fromDate = fromDatePicker.getDate();
    Date toDate = toDatePicker.getDate();
    
    if (fromDate.after(toDate)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      return;
    }
    
    Calendar calendar = Calendar.getInstance();
    calendar.clear();
    
    Calendar calendar2 = Calendar.getInstance();
    calendar2.setTime(fromDate);
    
    calendar.set(1, calendar2.get(1));
    calendar.set(2, calendar2.get(2));
    calendar.set(5, calendar2.get(5));
    calendar.set(10, 0);
    calendar.set(12, 0);
    calendar.set(13, 0);
    fromDate = calendar.getTime();
    
    calendar.clear();
    calendar2.setTime(toDate);
    calendar.set(1, calendar2.get(1));
    calendar.set(2, calendar2.get(2));
    calendar.set(5, calendar2.get(5));
    calendar.set(10, 23);
    calendar.set(12, 59);
    calendar.set(13, 59);
    toDate = calendar.getTime();
    
    InventoryTransactionDAO dao = new InventoryTransactionDAO();
    Object selectedItem = mscTtype.getSelectedItems();
    InventoryTransactionType transactionType = null;
    if ((selectedItem instanceof InventoryTransactionType)) {
      transactionType = (InventoryTransactionType)selectedItem;
    }
    Object selectedCategory = cbCategory.getSelectedItem();
    MenuCategory category = null;
    if ((selectedCategory instanceof MenuCategory)) {
      category = (MenuCategory)selectedCategory;
    }
    Object selectedGroup = cbGroup.getSelectedItem();
    MenuGroup group = null;
    if ((selectedGroup instanceof MenuGroup)) {
      group = (MenuGroup)selectedGroup;
    }
    
    String nameOrSku = tfNameSku.getText();
    
    List<InventoryTransaction> findTransaction = dao.findTransactionsByType(nameOrSku, transactionType, group, fromDate, toDate);
    
    String strTransactionType = "All";
    String strCategory = "All";
    String strGroup = "All";
    
    if (transactionType != null) {
      strTransactionType = transactionType.name();
    }
    
    if (category != null) {
      strCategory = category.getName();
    }
    
    if (group != null) {
      strGroup = group.getName();
    }
    String searchKey = "Transaction type: " + strTransactionType + "; Category: " + strCategory + "; Group: " + strGroup + ";";
    try
    {
      JasperReport report = ReportParser.getReport("/reports/inventoryTransactionReport.jasper");
      
      HashMap properties = new HashMap();
      ReportUtil.populateRestaurantProperties(properties);
      String dateRange = "From " + DateUtil.getOnlyFormattedDate(fromDate) + " To " + DateUtil.getOnlyFormattedDate(toDate);
      
      properties.put("DateRange", dateRange);
      
      properties.put("group", searchKey);
      

      Store store = Application.getInstance().getStore();
      
      properties.put("companyName", store.getName());
      properties.put("address", store.getAddressLine1());
      properties.put("city", store.getAddressLine2());
      properties.put("address3", store.getAddressLine3());
      properties.put("phone", store.getTelephone());
      
      properties.put("isGroup", Boolean.valueOf(chkShowInGroups.isSelected()));
      
      InventoryTransactionReportModel reportModel = new InventoryTransactionReportModel();
      if ((findTransaction != null) && (findTransaction.size() > 0)) {
        reportModel.setRows(findTransaction);
      }
      else {
        reportPanel.removeAll();
        POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), "No information found!");
        return;
      }
      
      properties.put("totalExpenses", String.valueOf(reportModel.getTotalAmount()));
      properties.put("totalQuantity", String.valueOf(reportModel.getTotalQuantity()));
      
      JasperPrint print = JasperFillManager.fillReport(report, properties, new JRTableModelDataSource(reportModel));
      
      JRViewer viewer = new JRViewer(print);
      reportPanel.removeAll();
      reportPanel.add(viewer);
      reportPanel.revalidate();
    } catch (JRException e) {
      e.printStackTrace();
    }
  }
  


  public InventoryTransactionReportView()
  {
    $$$setupUI$$$();TerminalDAO terminalDAO = new TerminalDAO();List terminals = terminalDAO.findAll();terminals.add(0, POSConstants.ALL);setLayout(new BorderLayout());add(contentPane);btnGo.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        InventoryTransactionReportView.this.viewReport();
      }
    });
  }
  


































































































































  private void $$$setupUI$$$()
  {
    contentPane = new JPanel();
    contentPane.setLayout(new MigLayout("fillx,hidemode 3", "", "[][][][grow]"));
    JLabel lblTransactionType = new JLabel("Transaction Type:");
    mscTtype = new MultiSelectComboBox();
    InventoryTransactionType[] itt = InventoryTransactionType.values();
    
    List transactionTypes = new ArrayList();
    
    for (int i = 0; i < itt.length; i++) {
      transactionTypes.add(itt[i]);
    }
    


    mscTtype.setItems(transactionTypes);
    JLabel lblFrom = new JLabel(POSConstants.FROM + ":");
    JLabel lblToDate = new JLabel(POSConstants.TO + ":");
    fromDatePicker = UiUtil.getCurrentMonthStart();
    toDatePicker = UiUtil.getCurrentMonthEnd();
    btnGo = new JButton();
    btnGo.setPreferredSize(PosUIManager.getSize(100, 0));
    btnGo.setText(POSConstants.GO);
    
    JLabel lblCategory = new JLabel("Category:");
    cbCategory = new JComboBox();
    
    cbCategory.addItem("<All>");
    
    List<MenuCategory> categories = MenuCategoryDAO.getInstance().findAll();
    for (Iterator localIterator = categories.iterator(); localIterator.hasNext();) { category = (MenuCategory)localIterator.next();
      cbCategory.addItem(category);
    }
    MenuCategory category;
    cbCategory.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        Object selectedItem = cbCategory.getSelectedItem();
        List<MenuGroup> groupList; if ((selectedItem instanceof MenuCategory)) {
          groupList = MenuGroupDAO.getInstance().findByParent((MenuCategory)selectedItem);
          cbGroup.removeAllItems();
          for (MenuGroup group : groupList) {
            cbGroup.addItem(group);
          }
        }
        else {
          cbGroup.removeAllItems();
          cbGroup.addItem("<All>");
          for (MenuGroup group : groups) {
            cbGroup.addItem(group);
          }
          
        }
      }
    });
    JLabel lblGroup = new JLabel("Groups:");
    cbGroup = new JComboBox();
    cbGroup.addItem("<All>");
    groups = MenuGroupDAO.getInstance().findAll();
    for (MenuGroup group : groups) {
      cbGroup.addItem(group);
    }
    
    JLabel lblNameSku = new JLabel("Name / SKU: ");
    tfNameSku = new JTextField();
    tfNameSku.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        InventoryTransactionReportView.this.viewReport();
      }
      
    });
    chkShowInGroups = new JCheckBox("Show in groups");
    
    JSeparator separator1 = new JSeparator();
    reportPanel = new JPanel();
    reportPanel.setLayout(new BorderLayout(0, 0));
    contentPane.add(lblNameSku, "split 12");
    contentPane.add(tfNameSku, "growx, w 200!");
    contentPane.add(lblTransactionType, "");
    contentPane.add(mscTtype);
    contentPane.add(lblCategory);
    contentPane.add(cbCategory);
    contentPane.add(lblGroup);
    contentPane.add(cbGroup);
    contentPane.add(lblFrom);
    contentPane.add(fromDatePicker);
    contentPane.add(lblToDate);
    contentPane.add(toDatePicker, "wrap");
    contentPane.add(chkShowInGroups, "split 2");
    contentPane.add(btnGo);
    contentPane.add(separator1, "newline,growx,span");
    contentPane.add(reportPanel, "newline,grow,span");
  }
  


  public JComponent $$$getRootComponent$$$()
  {
    return contentPane;
  }
}
