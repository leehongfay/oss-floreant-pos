package com.floreantpos.report;

import com.floreantpos.model.User;
import com.floreantpos.model.UserType;
import com.floreantpos.swing.ListTableModel;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;















public class WeeklyPayrollReportModel
  extends ListTableModel
{
  SimpleDateFormat dateFormat2 = new SimpleDateFormat("EEE MMM dd");
  
  DecimalFormat decimalFormat = new DecimalFormat("0.00");
  
  public WeeklyPayrollReportModel() {
    super(new String[] { "userID", "userName", "role", "regHour", "overtime", "regPmnt", "otPmnt", "total", "payment", "decTips", "nonCashTips", "dateOfWeek" });
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    WeeklyPayrollReportData data = (WeeklyPayrollReportData)rows.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      User user = data.getUser();
      User parentUser = user.getParentUser();
      if (parentUser != null) {
        return String.valueOf(parentUser.getId());
      }
      return String.valueOf(user.getId());
    
    case 1: 
      return data.getUser().getFirstName() + " " + data.getUser().getLastName();
    
    case 2: 
      return data.getUser().getType().getName();
    
    case 3: 
      return data.getRegularHourDisplay();
    
    case 4: 
      return data.getOvertimeDisplay();
    
    case 5: 
      return Double.valueOf(data.getRegularPayment());
    
    case 6: 
      return Double.valueOf(data.getOvertimePayment());
    
    case 7: 
      return data.getTotalHourDisplay();
    
    case 8: 
      return Double.valueOf(data.getTotalPayment());
    

    case 9: 
      return null;
    
    case 10: 
      return null;
    
    case 11: 
      Calendar cal = Calendar.getInstance();
      cal.setTime(data.getFromDateOfWeek());
      cal.setFirstDayOfWeek(data.getFirstDayOfWeek());
      int week = cal.get(4);
      return "Week " + week + " (" + dateFormat2.format(data.getFromDateOfWeek()) + " - " + dateFormat2.format(data.getToDateOfWeek()) + ")";
    
    case 12: 
      return "To: " + dateFormat2.format(data.getToDateOfWeek());
    }
    return null;
  }
}
