package com.floreantpos.report;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.CreditCardTransaction;
import com.floreantpos.model.CustomPaymentTransaction;
import com.floreantpos.model.CustomerAccountTransaction;
import com.floreantpos.model.DebitCardTransaction;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.dao.PosTransactionDAO;
import com.floreantpos.model.ext.PaperSize;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.services.report.ReportService;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.PanelTester;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;
import org.jdesktop.swingx.JXDatePicker;



















public class CreditCardReportView
  extends JPanel
{
  private JXDatePicker fromDatePicker = UiUtil.getCurrentMonthStart();
  private JXDatePicker toDatePicker = UiUtil.getCurrentMonthEnd();
  private JButton btnGo = new JButton(POSConstants.GO);
  private JPanel reportContainer;
  
  public CreditCardReportView() {
    super(new BorderLayout());
    
    JPanel topPanel = new JPanel(new MigLayout());
    
    topPanel.add(new JLabel(POSConstants.FROM + ":"), "grow");
    topPanel.add(fromDatePicker);
    topPanel.add(new JLabel(POSConstants.TO + ":"), "grow");
    topPanel.add(toDatePicker);
    topPanel.add(btnGo, "skip 1, al right");
    add(topPanel, "North");
    
    JPanel centerPanel = new JPanel(new BorderLayout());
    centerPanel.setBorder(new EmptyBorder(0, 10, 10, 10));
    centerPanel.add(new JSeparator(), "North");
    
    reportContainer = new JPanel(new BorderLayout());
    centerPanel.add(reportContainer);
    
    add(centerPanel);
    
    btnGo.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          CreditCardReportView.this.viewReport();
        } catch (Exception e1) {
          POSMessageDialog.showError(CreditCardReportView.this, POSConstants.ERROR_MESSAGE, e1);
        }
      }
    });
  }
  
  private void viewReport() throws Exception
  {
    Date fromDate = fromDatePicker.getDate();
    Date toDate = toDatePicker.getDate();
    
    if (fromDate.after(toDate)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      return;
    }
    
    fromDate = DateUtil.startOfDay(fromDate);
    toDate = DateUtil.endOfDay(toDate);
    
    Date currentTime = new Date();
    int saleCount = 0;
    double totalSales = 0.0D;
    double totalTips = 0.0D;
    
    List<PosTransaction> newTransactionList = new ArrayList();
    Class[] transactionTypes = { CreditCardTransaction.class, DebitCardTransaction.class, CustomPaymentTransaction.class, CustomerAccountTransaction.class };
    for (Class class1 : transactionTypes) {
      List<PosTransaction> transactions = PosTransactionDAO.getInstance().findTransactions(null, class1, fromDate, toDate);
      for (PosTransaction transaction : transactions) {
        saleCount++;
        totalSales += transaction.getAmount().doubleValue();
        totalTips += transaction.getTipsAmount().doubleValue();
        newTransactionList.add(transaction);
      }
    }
    
    HashMap map = new HashMap();
    ReportUtil.populateRestaurantProperties(map);
    map.put("reportTitle", Messages.getString("CreditCardReportView.0"));
    map.put("fromDate", ReportService.formatFullDate(fromDate));
    map.put("toDate", ReportService.formatFullDate(toDate));
    map.put("reportTime", ReportService.formatFullDate(currentTime));
    map.put("tips", "Tips (" + CurrencyUtil.getCurrencySymbol() + ")");
    map.put("totalHeader", "Total (" + CurrencyUtil.getCurrencySymbol() + ")");
    
    map.put("saleCount", String.valueOf(saleCount));
    map.put("totalSales", NumberUtil.formatNumber(Double.valueOf(totalSales - totalTips)));
    map.put("totalTips", NumberUtil.formatNumber(Double.valueOf(totalTips)));
    map.put("total", NumberUtil.formatNumber(Double.valueOf(totalSales)));
    
    JasperReport jasperReport = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("credit-card-report"));
    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, new JRTableModelDataSource(new CardReportModel(newTransactionList)));
    JRViewer viewer = new JRViewer(jasperPrint);
    reportContainer.removeAll();
    reportContainer.add(viewer);
    reportContainer.revalidate();
  }
  
  public static void main(String[] args)
  {
    PanelTester.width = 800;
    PanelTester.height = 500;
    PanelTester.test(new CreditCardReportView());
  }
}
