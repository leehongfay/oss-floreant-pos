package com.floreantpos.report;

import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.InventoryTransactionType;
import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.swing.ListTableModel;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class InventoryTransactionReportModel extends ListTableModel
{
  SimpleDateFormat dateFormat2 = new SimpleDateFormat("MMM-dd-yy HH:mm ");
  
  DecimalFormat decimalFormat = new DecimalFormat("0.00");
  private double totalAmount;
  private double totalQuantity;
  double totalOnHandValue;
  double amount;
  String type;
  private Double unitCost;
  
  public InventoryTransactionReportModel() {
    super(new String[] { "itemgroup", "items", "sku", "vendor", "tranTime", "tranType", "reasonType", "quantity", "unit", "cost", "total" });
  }
  
  public void setItems(List<InventoryTransaction> transactions) {
    setRows(transactions);
    
    totalAmount = 0.0D;
    for (InventoryTransaction item : transactions) {
      totalAmount += item.getTotal().doubleValue();
    }
  }
  








  public void setQuantity(List<InventoryTransaction> transactions)
  {
    setRows(transactions);
    totalQuantity = 0.0D;
    for (InventoryTransaction item : transactions)
    {
      if (item.getType().equals(InventoryTransactionType.IN)) {
        totalQuantity += item.getQuantity().doubleValue();
      }
      else if (item.getType().equals(InventoryTransactionType.OUT)) {
        totalQuantity -= item.getQuantity().doubleValue();
      }
    }
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    InventoryTransaction transactionData = (InventoryTransaction)rows.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      MenuGroup menuGroup = transactionData.getMenuItem().getParent();
      if (menuGroup == null) {
        return "";
      }
      return menuGroup.getName();
    
    case 1: 
      return transactionData.getMenuItem().getDisplayName();
    
    case 2: 
      return transactionData.getMenuItem().getSku();
    
    case 3: 
      InventoryVendor vendor = transactionData.getVendor();
      if (vendor == null) {
        return transactionData.getTicketId();
      }
      String vendorName = vendor.getName();
      if (StringUtils.isNotEmpty(vendorName)) {
        return vendorName;
      }
      
      return "";
    

    case 4: 
      return dateFormat2.format(transactionData.getTransactionDate());
    
    case 5: 
      return transactionData.getTransactionType().toString();
    
    case 6: 
      return transactionData.getReason() == null ? "" : transactionData.getReason().toString();
    
    case 7: 
      totalQuantity = transactionData.getQuantity().doubleValue();
      if (transactionData.getReason() == null) {
        return Double.valueOf(totalQuantity);
      }
      type = transactionData.getTransactionType().toString();
      if ("OUT".equals(type)) {
        return Double.valueOf(-totalQuantity);
      }
      return Double.valueOf(totalQuantity);
    case 8: 
      return transactionData.getUnit();
    
    case 9: 
      unitCost = transactionData.getUnitCost();
      
      return Double.valueOf(unitCost.doubleValue());
    
    case 10: 
      totalOnHandValue = transactionData.getTotal().doubleValue();
      if (transactionData.getReason() == null) {
        return Double.valueOf(totalOnHandValue);
      }
      type = transactionData.getTransactionType().toString();
      
      if ("OUT".equals(type)) {
        return Double.valueOf(-totalOnHandValue);
      }
      
      return Double.valueOf(totalQuantity * unitCost.doubleValue());
    }
    
    return null;
  }
  
  public double getTotalAmount() {
    return totalAmount;
  }
  
  public double getTotalQuantity() {
    return totalQuantity;
  }
}
