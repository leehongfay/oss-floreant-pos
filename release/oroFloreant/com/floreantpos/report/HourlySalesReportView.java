package com.floreantpos.report;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.Shift;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.AttendenceHistoryDAO;
import com.floreantpos.model.dao.ShiftDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.ext.PaperSize;
import com.floreantpos.services.report.ReportService;
import com.floreantpos.swing.ListComboBoxModel;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;
import org.jdesktop.swingx.JXDatePicker;


























public class HourlySalesReportView
  extends TransparentPanel
{
  private JButton btnGo;
  private JComboBox cbTerminal;
  private JXDatePicker fromDatePicker;
  private JXDatePicker toDatePicker;
  private JPanel reportPanel;
  private JPanel contentPane;
  private JComboBox jcbSalesType;
  
  public HourlySalesReportView()
  {
    setLayout(new BorderLayout());
    createUI();
  }
  
  private void viewReport() throws Exception {
    Date fromDate = fromDatePicker.getDate();
    Date toDate = toDatePicker.getDate();
    
    if (fromDate.after(toDate)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      return;
    }
    
    Terminal terminal = null;
    if ((cbTerminal.getSelectedItem() instanceof Terminal)) {
      terminal = (Terminal)cbTerminal.getSelectedItem();
    }
    
    Calendar calendar = Calendar.getInstance();
    calendar.clear();
    
    Calendar calendar2 = Calendar.getInstance();
    calendar2.setTime(fromDate);
    
    calendar.set(1, calendar2.get(1));
    calendar.set(2, calendar2.get(2));
    calendar.set(5, calendar2.get(5));
    calendar.set(10, 0);
    calendar.set(12, 0);
    calendar.set(13, 0);
    fromDate = calendar.getTime();
    
    calendar.clear();
    calendar2.setTime(toDate);
    calendar.set(1, calendar2.get(1));
    calendar.set(2, calendar2.get(2));
    calendar.set(5, calendar2.get(5));
    calendar.set(10, 23);
    calendar.set(12, 59);
    calendar.set(13, 59);
    toDate = calendar.getTime();
    
    TicketDAO ticketDAO = TicketDAO.getInstance();
    AttendenceHistoryDAO attendenceHistoryDAO = new AttendenceHistoryDAO();
    ArrayList<LaborReportData> rows = new ArrayList();
    
    DecimalFormat formatter = new DecimalFormat("00");
    double manHour;
    for (int i = 0; i < 24; i++) {
      List<Ticket> tickets = ticketDAO.findTicketsForLaborHour(fromDate, toDate, i, terminal);
      List<User> users = attendenceHistoryDAO.findNumberOfClockedInUserAtHour(fromDate, toDate, i, terminal);
      
      manHour = users.size();
      double totalChecks = 0.0D;
      double totalGuests = 0.0D;
      double totalSales = 0.0D;
      double labor = 0.0D;
      double salesPerMHr = 0.0D;
      double guestsPerMHr = 0.0D;
      double checksPerMHr = 0.0D;
      
      if ((tickets != null) && (tickets.size() > 0))
      {


        for (Ticket ticket : tickets) {
          totalChecks += 1.0D;
          totalGuests += ticket.getNumberOfGuests().intValue();
          if (jcbSalesType.getSelectedIndex() == 0) {
            totalSales += ticket.getSubtotalAmount().doubleValue();
          }
          else if (jcbSalesType.getSelectedIndex() == 1) {
            totalSales += ticket.getTotalAmountWithTips().doubleValue();
          }
          else if (jcbSalesType.getSelectedIndex() == 2) {
            totalSales += ticket.getPaidAmount().doubleValue();
          }
        }
        

        for (User user : users) {
          labor += (user.getCostPerHour() == null ? 0.0D : user.getCostPerHour().doubleValue());
        }
        if (manHour > 0.0D) {
          labor /= manHour;
          salesPerMHr = totalSales / manHour;
          guestsPerMHr = totalGuests / manHour;
          checksPerMHr = totalChecks / manHour;
        }
        

        LaborReportData reportData = new LaborReportData();
        String am = "AM";
        String pm = "PM";
        if ((i >= 0) && (i <= 11)) {
          reportData.setPeriod(formatter.format(i) + " " + am);
        }
        else {
          reportData.setPeriod(formatter.format(i) + " " + pm);
        }
        
        reportData.setManHour(manHour);
        reportData.setNoOfChecks(totalChecks);
        reportData.setSales(totalSales);
        reportData.setNoOfGuests(totalGuests);
        reportData.setLabor(labor);
        reportData.setSalesPerMHr(salesPerMHr);
        reportData.setGuestsPerMHr(guestsPerMHr);
        reportData.setCheckPerMHr(checksPerMHr);
        if ((labor == 0.0D) && (totalSales == 0.0D)) {
          reportData.setLaborCost(0.0D);
        }
        else {
          reportData.setLaborCost(labor * 100.0D / totalSales);
        }
        
        rows.add(reportData);
      }
    }
    
    ArrayList<LaborReportData> shiftReportRows = new ArrayList();
    ShiftDAO shiftDAO = new ShiftDAO();
    List<Shift> shifts = shiftDAO.findAll();
    for (Shift shift : shifts)
    {
      List<Ticket> tickets = ticketDAO.findTicketsForShift(fromDate, toDate, shift, terminal);
      List<User> users = attendenceHistoryDAO.findNumberOfClockedInUserAtShift(fromDate, toDate, shift, terminal);
      
      double manHour = users.size();
      double totalChecks = 0.0D;
      double totalGuests = 0.0D;
      double totalSales = 0.0D;
      double labor = 0.0D;
      double salesPerMHr = 0.0D;
      double guestsPerMHr = 0.0D;
      double checksPerMHr = 0.0D;
      
      if ((tickets != null) && (tickets.size() > 0))
      {


        for (Ticket ticket : tickets) {
          totalChecks += 1.0D;
          totalGuests += ticket.getNumberOfGuests().intValue();
          if (jcbSalesType.getSelectedIndex() == 0) {
            totalSales += ticket.getSubtotalAmount().doubleValue();
          }
          else if (jcbSalesType.getSelectedIndex() == 1) {
            totalSales += ticket.getTotalAmountWithTips().doubleValue();
          }
          else if (jcbSalesType.getSelectedIndex() == 2) {
            totalSales += ticket.getPaidAmount().doubleValue();
          }
        }
        
        for (User user : users) {
          labor += (user.getCostPerHour() == null ? 0.0D : user.getCostPerHour().doubleValue());
        }
        if (manHour > 0.0D) {
          labor /= manHour;
          salesPerMHr = totalSales / manHour;
          guestsPerMHr = totalGuests / manHour;
          checksPerMHr = totalChecks / manHour;
        }
        

        LaborReportData reportData = new LaborReportData();
        reportData.setPeriod(shift.getName());
        reportData.setManHour(manHour);
        reportData.setNoOfChecks(totalChecks);
        reportData.setSales(totalSales);
        reportData.setNoOfGuests(totalGuests);
        reportData.setLabor(labor);
        reportData.setSalesPerMHr(salesPerMHr);
        reportData.setGuestsPerMHr(guestsPerMHr);
        reportData.setCheckPerMHr(checksPerMHr);
        reportData.setLaborCost(labor * 100.0D / totalSales);
        
        shiftReportRows.add(reportData);
      }
    }
    
    JasperReport hourlyReport = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("hourly_labor_subreport"));
    JasperReport shiftReport = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("hourly_labor_shift_subreport"));
    
    JasperReport report = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("hourly_labor_report"));
    
    HashMap properties = new HashMap();
    ReportUtil.populateRestaurantProperties(properties);
    properties.put("reportTitle", "Hourly Sales Report");
    properties.put("reportTime", ReportService.formatFullDate(new Date()));
    properties.put("fromDay", ReportService.formatShortDate(fromDate));
    properties.put("toDay", ReportService.formatShortDate(toDate));
    properties.put("salesType", jcbSalesType.getSelectedItem());
    properties.put("incr", Messages.getString("HourlyLaborReportView.0"));
    properties.put("cntr", terminal == null ? POSConstants.ALL : terminal.getName());
    
    properties.put("hourlyReport", hourlyReport);
    properties.put("hourlyReportDatasource", new JRTableModelDataSource(new HourlySalesReportModel(rows)));
    properties.put("shiftReport", shiftReport);
    properties.put("shiftReportDatasource", new JRTableModelDataSource(new HourlySalesReportModel(shiftReportRows)));
    
    JasperPrint print = JasperFillManager.fillReport(report, properties, new JREmptyDataSource());
    
    JRViewer viewer = new JRViewer(print);
    reportPanel.removeAll();
    reportPanel.add(viewer);
    reportPanel.revalidate();
  }
  
  private void createUI()
  {
    fromDatePicker = UiUtil.getCurrentMonthStart();
    toDatePicker = UiUtil.getCurrentMonthEnd();
    btnGo = new JButton();
    btnGo.setText(POSConstants.GO);
    btnGo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          HourlySalesReportView.this.viewReport();
        } catch (Exception e1) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.ERROR_MESSAGE, e1);
        }
      }
    });
    cbTerminal = new JComboBox();
    TerminalDAO terminalDAO = new TerminalDAO();
    List terminals = terminalDAO.findAll();
    terminals.add(0, POSConstants.ALL);
    cbTerminal.setModel(new ListComboBoxModel(terminals));
    
    jcbSalesType = new JComboBox();
    jcbSalesType.addItem("Gross Sales");
    jcbSalesType.addItem("Net Sales");
    jcbSalesType.addItem("Received Amount");
    
    setLayout(new BorderLayout());
    
    JPanel topPanel = new JPanel(new MigLayout("", "[][][][][][]"));
    
    topPanel.add(new JLabel(POSConstants.START_DATE + ":"));
    fromDatePicker.setFormats(new String[] { "dd MMM yy" });
    topPanel.add(fromDatePicker);
    topPanel.add(new JLabel(POSConstants.END_DATE + ":"));
    toDatePicker.setFormats(new String[] { "dd MMM yy" });
    topPanel.add(toDatePicker);
    topPanel.add(new JLabel(POSConstants.TERMINAL_LABEL + ":"));
    topPanel.add(cbTerminal);
    topPanel.add(new JLabel("Sales Type:"));
    topPanel.add(jcbSalesType);
    topPanel.add(btnGo, "width 60!");
    add(topPanel, "North");
    
    JPanel centerPanel = new JPanel(new BorderLayout());
    centerPanel.setBorder(new EmptyBorder(0, 10, 10, 10));
    centerPanel.add(new JSeparator(), "North");
    
    reportPanel = new JPanel(new BorderLayout());
    centerPanel.add(reportPanel);
    
    add(centerPanel);
  }
  
  public JComponent $$$getRootComponent$$$() {
    return contentPane;
  }
  
  public static enum SalesType {
    GROSS_SALES,  NET_SALES;
    

    private SalesType() {}
  }
  
  public static class LaborReportData
  {
    private String period;
    private double noOfChecks;
    private double noOfGuests;
    private double sales;
    private double manHour;
    
    public LaborReportData() {}
    
    public double getCheckPerMHr() { return checkPerMHr; }
    
    private double labor;
    
    public void setCheckPerMHr(double checkPerMHr) { this.checkPerMHr = checkPerMHr; }
    
    private double salesPerMHr;
    
    public double getGuestsPerMHr() { return guestsPerMHr; }
    
    private double guestsPerMHr;
    
    public void setGuestsPerMHr(double guestsPerMHr) { this.guestsPerMHr = guestsPerMHr; }
    
    private double checkPerMHr;
    private double laborCost;
    public double getLabor() { return labor; }
    
    public void setLabor(double labor)
    {
      this.labor = labor;
    }
    
    public double getLaborCost() {
      return laborCost;
    }
    
    public void setLaborCost(double laborCost) {
      this.laborCost = laborCost;
    }
    
    public double getManHour() {
      return manHour;
    }
    
    public void setManHour(double manHour) {
      this.manHour = manHour;
    }
    
    public double getNoOfChecks() {
      return noOfChecks;
    }
    
    public void setNoOfChecks(double noOfChecks) {
      this.noOfChecks = noOfChecks;
    }
    
    public double getNoOfGuests() {
      return noOfGuests;
    }
    
    public void setNoOfGuests(double noOfGuests) {
      this.noOfGuests = noOfGuests;
    }
    
    public String getPeriod() {
      return period;
    }
    
    public void setPeriod(String period) {
      this.period = period;
    }
    
    public double getSales() {
      return sales;
    }
    
    public void setSales(double sales) {
      this.sales = sales;
    }
    
    public double getSalesPerMHr() {
      return salesPerMHr;
    }
    
    public void setSalesPerMHr(double salesPerMHr) {
      this.salesPerMHr = salesPerMHr;
    }
  }
}
