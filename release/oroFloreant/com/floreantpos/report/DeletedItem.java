package com.floreantpos.report;

import com.floreantpos.model.User;

public class DeletedItem
{
  private String id;
  private java.util.Date voidDate;
  private String ticketId;
  private String name;
  private double quantity;
  private double total;
  private String voidReason;
  private User owner;
  private User voidUser;
  
  public DeletedItem() {}
  
  public String getId() {
    return id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public java.util.Date getVoidDate() {
    return voidDate;
  }
  
  public void setVoidDate(java.util.Date date) {
    voidDate = date;
  }
  
  public String getTicketId() {
    return ticketId;
  }
  
  public void setTicketId(String ticketId) {
    this.ticketId = ticketId;
  }
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public double getQuantity() {
    return quantity;
  }
  
  public void setQuantity(double quantity) {
    this.quantity = quantity;
  }
  
  public double getTotal() {
    return total;
  }
  
  public void setTotal(double total) {
    this.total = total;
  }
  
  public String getVoidReason() {
    return voidReason;
  }
  
  public void setVoidReason(String voidReason) {
    this.voidReason = voidReason;
  }
  
  public User getOwner() {
    return owner;
  }
  
  public void setOwner(User owner) {
    this.owner = owner;
  }
  
  public User getVoidUser() {
    return voidUser;
  }
  
  public void setVoidUser(User voidUser) {
    this.voidUser = voidUser;
  }
}
