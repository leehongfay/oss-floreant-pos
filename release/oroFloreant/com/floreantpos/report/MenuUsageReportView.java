package com.floreantpos.report;

import com.floreantpos.POSConstants;
import com.floreantpos.model.ext.PaperSize;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.services.report.ReportService;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;
import org.jdesktop.swingx.JXDatePicker;



















public class MenuUsageReportView
  extends JPanel
{
  private JXDatePicker fromDatePicker = UiUtil.getCurrentMonthStart();
  private JXDatePicker toDatePicker = UiUtil.getCurrentMonthEnd();
  private JButton btnGo = new JButton(POSConstants.GO);
  private JPanel reportContainer;
  
  public MenuUsageReportView() {
    super(new BorderLayout());
    
    JPanel topPanel = new JPanel(new MigLayout());
    
    topPanel.add(new JLabel(POSConstants.FROM + ":"), "grow");
    topPanel.add(fromDatePicker);
    topPanel.add(new JLabel(POSConstants.TO + ":"), "grow");
    topPanel.add(toDatePicker);
    topPanel.add(btnGo, "skip 1, al right");
    add(topPanel, "North");
    
    JPanel centerPanel = new JPanel(new BorderLayout());
    centerPanel.setBorder(new EmptyBorder(0, 10, 10, 10));
    centerPanel.add(new JSeparator(), "North");
    
    reportContainer = new JPanel(new BorderLayout());
    centerPanel.add(reportContainer);
    
    add(centerPanel);
    
    btnGo.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          MenuUsageReportView.this.viewReport();
        } catch (Exception e1) {
          POSMessageDialog.showError(MenuUsageReportView.this, POSConstants.ERROR_MESSAGE, e1);
        }
      }
    });
  }
  
  private void viewReport() throws Exception
  {
    Date fromDate = fromDatePicker.getDate();
    Date toDate = toDatePicker.getDate();
    
    if (fromDate.after(toDate)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      return;
    }
    
    fromDate = DateUtil.startOfDay(fromDate);
    toDate = DateUtil.endOfDay(toDate);
    
    ReportService reportService = new ReportService();
    MenuUsageReport report = reportService.getMenuUsageReport(fromDate, toDate);
    
    HashMap map = new HashMap();
    ReportUtil.populateRestaurantProperties(map);
    map.put("reportTitle", "MENU USAGE REPORT");
    map.put("fromDate", ReportService.formatShortDate(fromDate));
    map.put("toDate", ReportService.formatShortDate(toDate));
    map.put("reportTime", ReportService.formatFullDate(new Date()));
    
    JasperReport jasperReport = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("menu_usage_report"));
    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, new JRTableModelDataSource(report.getTableModel()));
    JRViewer viewer = new JRViewer(jasperPrint);
    reportContainer.removeAll();
    reportContainer.add(viewer);
    reportContainer.revalidate();
  }
}
