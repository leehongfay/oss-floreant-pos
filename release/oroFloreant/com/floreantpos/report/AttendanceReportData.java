package com.floreantpos.report;

import com.floreantpos.model.User;
import java.util.Date;
















public class AttendanceReportData
{
  User user;
  String name;
  Date clockIn;
  Date clockOut;
  double workTime;
  
  public AttendanceReportData() {}
  
  public User getUser()
  {
    return user;
  }
  
  public void setUser(User user) {
    this.user = user;
  }
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public Date getClockIn() {
    return clockIn;
  }
  
  public void setClockIn(Date clockIn) {
    this.clockIn = clockIn;
  }
  
  public Date getClockOut() {
    return clockOut;
  }
  
  public void setClockOut(Date clockOut) {
    this.clockOut = clockOut;
  }
  
  public double getWorkTime() {
    return workTime;
  }
  
  public void setWorkTime(double workTime) {
    this.workTime = workTime;
  }
  
  public void calculate() {
    long cin = clockIn.getTime();
    if (clockOut == null) {
      return;
    }
    long cout = clockOut.getTime();
    
    long milliseconds = cout - cin;
    if (milliseconds < 0L) {
      workTime = 0.0D;
      return;
    }
    
    double seconds = milliseconds / 1000.0D;
    double minutes = seconds / 60.0D;
    double hours = minutes / 60.0D;
    
    workTime = hours;
  }
}
