package com.floreantpos.report;

import com.floreantpos.POSConstants;
import com.floreantpos.model.ext.PaperSize;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.services.report.ReportService;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;
import org.jdesktop.swingx.JXDatePicker;



















public class SalesDetailReportView
  extends JPanel
{
  private SimpleDateFormat fullDateFormatter = new SimpleDateFormat("yyyy MMM dd, hh:mm a");
  private SimpleDateFormat shortDateFormatter = new SimpleDateFormat("yyyy MMM dd");
  
  private JXDatePicker fromDatePicker = UiUtil.getCurrentMonthStart();
  private JXDatePicker toDatePicker = UiUtil.getCurrentMonthEnd();
  private JButton btnGo = new JButton(POSConstants.GO);
  private JPanel reportContainer;
  
  public SalesDetailReportView() {
    super(new BorderLayout());
    
    JPanel topPanel = new JPanel(new MigLayout());
    
    topPanel.add(new JLabel(POSConstants.FROM + ":"), "grow");
    topPanel.add(fromDatePicker);
    topPanel.add(new JLabel(POSConstants.TO + ":"), "grow");
    topPanel.add(toDatePicker);
    topPanel.add(btnGo, "skip 1, al right");
    add(topPanel, "North");
    
    JPanel centerPanel = new JPanel(new BorderLayout());
    centerPanel.setBorder(new EmptyBorder(0, 10, 10, 10));
    centerPanel.add(new JSeparator(), "North");
    
    reportContainer = new JPanel(new BorderLayout());
    centerPanel.add(reportContainer);
    
    add(centerPanel);
    
    btnGo.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          SalesDetailReportView.this.viewReport();
        } catch (Exception e1) {
          POSMessageDialog.showError(SalesDetailReportView.this, POSConstants.ERROR_MESSAGE, e1);
        }
      }
    });
  }
  
  private void viewReport() throws Exception
  {
    Date fromDate = fromDatePicker.getDate();
    Date toDate = toDatePicker.getDate();
    
    if (fromDate.after(toDate)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      return;
    }
    
    fromDate = DateUtil.startOfDay(fromDate);
    toDate = DateUtil.endOfDay(toDate);
    
    ReportService reportService = new ReportService();
    SalesDetailedReport report = reportService.getSalesDetailedReport(fromDate, toDate);
    
    JasperReport drawerPullReport = ReportUtil.getReport("sales_summary_balance_detailed__1");
    JasperReport creditCardReport = ReportUtil.getReport("sales_summary_balance_detailed_2");
    
    HashMap map = new HashMap();
    ReportUtil.populateRestaurantProperties(map);
    map.put("fromDate", shortDateFormatter.format(fromDate));
    map.put("toDate", shortDateFormatter.format(toDate));
    map.put("reportTime", fullDateFormatter.format(new Date()));
    map.put("giftCertReturnCount", Integer.valueOf(report.getGiftCertReturnCount()));
    map.put("giftCertReturnAmount", Double.valueOf(report.getGiftCertReturnAmount()));
    map.put("giftCertChangeCount", Integer.valueOf(report.getGiftCertChangeCount()));
    map.put("giftCertChangeAmount", Double.valueOf(report.getGiftCertChangeAmount()));
    map.put("tipsCount", Integer.valueOf(report.getTipsCount()));
    map.put("tipsAmount", Double.valueOf(report.getChargedTips()));
    map.put("tipsPaidAmount", Double.valueOf(report.getTipsPaid()));
    map.put("drawerPullReport", drawerPullReport);
    map.put("drawerPullDatasource", new JRTableModelDataSource(report.getDrawerPullDataTableModel()));
    map.put("creditCardReport", creditCardReport);
    map.put("creditCardReportDatasource", new JRTableModelDataSource(report.getCreditCardDataTableModel()));
    
    JasperReport jasperReport = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("sales_detail_report"));
    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, new JREmptyDataSource());
    JRViewer viewer = new JRViewer(jasperPrint);
    reportContainer.removeAll();
    reportContainer.add(viewer);
    reportContainer.revalidate();
  }
}
