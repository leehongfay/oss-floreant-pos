package com.floreantpos.report;

import com.floreantpos.POSConstants;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.AttendenceHistoryDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.model.ext.PaperSize;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;
import org.jdesktop.swingx.JXDatePicker;



























public class AttendanceReportView
  extends TransparentPanel
{
  private JButton btnGo;
  private JXDatePicker fromDatePicker;
  private JXDatePicker toDatePicker;
  private JPanel reportPanel;
  private JComboBox cbUserType;
  
  public AttendanceReportView()
  {
    setLayout(new BorderLayout());
    createUI();
  }
  
  private void createUI()
  {
    fromDatePicker = UiUtil.getCurrentMonthStart();
    toDatePicker = UiUtil.getDeafultDate();
    toDatePicker.setDate(new Date());
    
    btnGo = new JButton();
    btnGo.setText(POSConstants.GO);
    btnGo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          AttendanceReportView.this.viewReport();
        } catch (Exception e1) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.ERROR_MESSAGE, e1);
        }
        
      }
    });
    cbUserType = new JComboBox();
    
    UserDAO dao = new UserDAO();
    List<User> userTypes = dao.findAll();
    
    Vector list = new Vector();
    list.add(POSConstants.ALL);
    list.addAll(userTypes);
    
    cbUserType.setModel(new DefaultComboBoxModel(list));
    
    setLayout(new BorderLayout());
    
    JPanel topPanel = new JPanel(new MigLayout());
    
    topPanel.add(new JLabel(POSConstants.START_DATE + ":"));
    fromDatePicker.setFormats(new String[] { "MMM dd, yyyy" });
    topPanel.add(fromDatePicker);
    topPanel.add(new JLabel(POSConstants.END_DATE + ":"));
    toDatePicker.setFormats(new String[] { "MMM dd, yyyy" });
    topPanel.add(toDatePicker);
    topPanel.add(new JLabel(POSConstants.USER + ":"));
    topPanel.add(cbUserType);
    topPanel.add(btnGo, "width 60!");
    add(topPanel, "North");
    
    JPanel centerPanel = new JPanel(new BorderLayout());
    centerPanel.setBorder(new EmptyBorder(0, 10, 10, 10));
    centerPanel.add(new JSeparator(), "North");
    
    reportPanel = new JPanel(new BorderLayout());
    centerPanel.add(reportPanel);
    
    add(centerPanel);
  }
  
  private void viewReport() throws Exception {
    Date fromDate = fromDatePicker.getDate();
    Date toDate = toDatePicker.getDate();
    
    if (fromDate.after(toDate)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      return;
    }
    
    Calendar calendar = Calendar.getInstance();
    calendar.clear();
    
    Calendar calendar2 = Calendar.getInstance();
    calendar2.setTime(fromDate);
    
    calendar.set(1, calendar2.get(1));
    calendar.set(2, calendar2.get(2));
    calendar.set(5, calendar2.get(5));
    calendar.set(10, 0);
    calendar.set(12, 0);
    calendar.set(13, 0);
    fromDate = calendar.getTime();
    
    calendar.clear();
    calendar2.setTime(toDate);
    calendar.set(1, calendar2.get(1));
    calendar.set(2, calendar2.get(2));
    calendar.set(5, calendar2.get(5));
    calendar.set(10, 23);
    calendar.set(12, 59);
    calendar.set(13, 59);
    toDate = calendar.getTime();
    
    User user = null;
    if (!cbUserType.getSelectedItem().equals(POSConstants.ALL)) {
      user = (User)cbUserType.getSelectedItem();
    }
    
    AttendenceHistoryDAO dao = new AttendenceHistoryDAO();
    List<AttendanceReportData> attendanceList = dao.findAttendance(fromDate, toDate, user);
    
    JasperReport report = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("EmployeeAttendanceReport"));
    
    HashMap properties = new HashMap();
    ReportUtil.populateRestaurantProperties(properties);
    properties.put("fromDate", DateUtil.formatFullDateAsString(fromDate));
    properties.put("toDate", DateUtil.formatFullDateAsString(toDate));
    properties.put("reportDate", DateUtil.formatFullDateAndTimeAsString(new Date()));
    
    AttendanceReportModel reportModel = new AttendanceReportModel();
    reportModel.setRows(attendanceList);
    
    JasperPrint print = JasperFillManager.fillReport(report, properties, new JRTableModelDataSource(reportModel));
    
    JRViewer viewer = new JRViewer(print);
    reportPanel.removeAll();
    reportPanel.add(viewer);
    reportPanel.revalidate();
  }
}
