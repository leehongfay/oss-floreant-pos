package com.floreantpos.report;

import com.floreantpos.swing.ListTableModel;
import com.floreantpos.util.NumberUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;


















public class ShiftwiseSalesSummaryReportModel
  extends ListTableModel
{
  public ShiftwiseSalesSummaryReportModel(List<ShiftwiseSalesSummaryData> dataList)
  {
    super(new String[] { "shiftName", "categoryName", "count", "gross", "discount", "netSales" }, dataList);
  }
  
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    ShiftwiseSalesSummaryData data = (ShiftwiseSalesSummaryData)rows.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      return "Shift: " + shiftName;
    
    case 1: 
      return categoryName;
    
    case 2: 
      return NumberUtil.formatNumber(Double.valueOf(count));
    
    case 3: 
      return NumberUtil.formatNumber(Double.valueOf(gross));
    
    case 4: 
      return NumberUtil.formatNumber(Double.valueOf(discount));
    
    case 5: 
      return NumberUtil.formatNumber(Double.valueOf(netSales));
    }
    
    
    return null;
  }
  
  public static class ShiftwiseSalesSummaryData { private String shiftName;
    private String categoryName;
    private double count;
    private double gross;
    private double discount;
    private double netSales;
    
    public ShiftwiseSalesSummaryData() {}
    
    public void calculate() { netSales = (gross - discount); }
    
    public String getCategoryName()
    {
      return categoryName;
    }
    
    public void setCategoryName(String categoryName) {
      this.categoryName = categoryName;
    }
    
    public double getCount() {
      return count;
    }
    
    public void setCount(double count) {
      this.count = count;
    }
    
    public double getDiscount() {
      return discount;
    }
    
    public void setDiscount(double discount) {
      this.discount = discount;
    }
    
    public double getGross() {
      return gross;
    }
    
    public void setGross(double gross) {
      this.gross = gross;
    }
    
    public double getNetSales() {
      return netSales;
    }
    
    public void setNetSales(double netSales) {
      this.netSales = netSales;
    }
    
    public String getShiftName() {
      return shiftName;
    }
    
    public void setShiftName(String shiftName) {
      this.shiftName = shiftName;
    }
  }
  
  public static void main(String[] args) throws Exception
  {
    ArrayList list = new ArrayList();
    
    ShiftwiseSalesSummaryData data = new ShiftwiseSalesSummaryData();
    data.setShiftName("SHIFT1");
    data.setCategoryName("C");
    list.add(data);
    
    data = new ShiftwiseSalesSummaryData();
    data.setShiftName("SHIFT1");
    data.setCategoryName("C2");
    list.add(data);
    
    data = new ShiftwiseSalesSummaryData();
    data.setShiftName("SHIFT2");
    data.setCategoryName("C");
    list.add(data);
    
    JasperReport report = ReportUtil.getReport("sales_summary_report2");
    JasperPrint print = JasperFillManager.fillReport(report, new HashMap(), new JRBeanCollectionDataSource(list));
    
    JasperViewer.viewReport(print, true);
  }
}
