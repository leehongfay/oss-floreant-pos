package com.floreantpos.report;

import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.Store;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.swing.TransparentPanel;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;
import org.jdesktop.swingx.JXDatePicker;











































public class PurchaseReportView
  extends TransparentPanel
{
  private JButton btnGo;
  private JXDatePicker fromDatePicker;
  private JXDatePicker toDatePicker;
  private JPanel reportPanel;
  private JPanel contentPane;
  
  private void viewReport(List<MenuItem> inventList)
  {
    try
    {
      JasperReport report = ReportUtil.getReport("purchaseReport");
      
      HashMap properties = new HashMap();
      ReportUtil.populateRestaurantProperties(properties);
      properties.put("reportDate", new Date());
      properties.put("reportTitle", "Purchase Order");
      
      Store store = Application.getInstance().getStore();
      
      properties.put("companyName", store.getName());
      properties.put("address", store.getAddressLine1());
      properties.put("city", store.getAddressLine2());
      properties.put("phone", store.getTelephone());
      properties.put("fax", store.getZipCode());
      properties.put("email", store.getAddressLine3());
      







      PurchaseReportModel reportModel = new PurchaseReportModel();
      reportModel.setRows(inventList);
      
      JasperPrint print = JasperFillManager.fillReport(report, properties, new JRTableModelDataSource(reportModel));
      
      JRViewer viewer = new JRViewer(print);
      reportPanel.removeAll();
      reportPanel.add(viewer);
      reportPanel.revalidate();
    } catch (JRException e) {
      PosLog.error(getClass(), e);
    }
  }
  


  public PurchaseReportView()
  {
    $$$setupUI$$$();TerminalDAO terminalDAO = new TerminalDAO();List terminals = terminalDAO.findAll();terminals.add(0, POSConstants.ALL);setLayout(new BorderLayout());add(contentPane);btnGo.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuItemDAO dao = new MenuItemDAO();
        List<MenuItem> findPayroll = dao.findAll();
        PurchaseReportView.this.viewReport(findPayroll);
      }
    });
  }
  





























































  public PurchaseReportView(List<MenuItem> inventoryList)
  {
    $$$setupUI$$$();TerminalDAO terminalDAO = new TerminalDAO();List terminals = terminalDAO.findAll();setLayout(new BorderLayout());add(contentPane);viewReport(inventoryList);
  }
  






  private void $$$setupUI$$$()
  {
    contentPane = new JPanel();
    contentPane.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
    JPanel panel1 = new JPanel();
    panel1.setLayout(new GridLayoutManager(2, 7, new Insets(10, 10, 10, 10), 10, 10));
    contentPane.add(panel1, new GridConstraints(0, 0, 1, 1, 0, 3, 3, 1, null, null, null, 0, false));
    

    Spacer spacer1 = new Spacer();
    panel1.add(spacer1, new GridConstraints(0, 1, 1, 1, 0, 1, 4, 1, null, null, null, 0, false));
    


    btnGo = new JButton();
    btnGo.setText(POSConstants.GO);
    panel1.add(btnGo, new GridConstraints(0, 0, 1, 1, 4, 0, 1, 0, null, new Dimension(147, 23), null, 0, false));
    





    JSeparator separator1 = new JSeparator();
    panel1.add(separator1, new GridConstraints(1, 0, 1, 7, 0, 3, 4, 4, null, null, null, 0, false));
    
    reportPanel = new JPanel();
    reportPanel.setLayout(new BorderLayout(0, 0));
    contentPane.add(reportPanel, new GridConstraints(1, 0, 1, 1, 0, 3, 3, 3, null, null, null, 0, false));
  }
  




  public JComponent $$$getRootComponent$$$()
  {
    return contentPane;
  }
}
