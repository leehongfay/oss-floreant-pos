package com.floreantpos.report;

import com.floreantpos.swing.ListTableModel;
import java.util.List;

























public class HourlySalesReportModel
  extends ListTableModel
{
  private String[] columnNames = { "period", "checks", "guests", "sales", "manHour", "labor", "salesPerMHr", "guestsPerMHr", "checksPerMHr", "laborCost" };
  
  public HourlySalesReportModel() {
    setColumnNames(columnNames);
  }
  
  public HourlySalesReportModel(List rows) {
    setColumnNames(columnNames);
    setRows(rows);
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    HourlySalesReportView.LaborReportData reportData = (HourlySalesReportView.LaborReportData)rows.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      return reportData.getPeriod();
    
    case 1: 
      return Double.valueOf(reportData.getNoOfChecks());
    
    case 2: 
      return Double.valueOf(reportData.getNoOfGuests());
    
    case 3: 
      return Double.valueOf(reportData.getSales());
    
    case 4: 
      return Double.valueOf(reportData.getManHour());
    
    case 5: 
      return Double.valueOf(reportData.getLabor());
    
    case 6: 
      return Double.valueOf(reportData.getSalesPerMHr());
    
    case 7: 
      return Double.valueOf(reportData.getGuestsPerMHr());
    
    case 8: 
      return Double.valueOf(reportData.getCheckPerMHr());
    
    case 9: 
      return Double.valueOf(reportData.getLaborCost());
    }
    return null;
  }
}
