package com.floreantpos.report;

import org.apache.commons.lang.StringUtils;

public class InventoryStockData {
  public static String PROP_MENU_ITEM_ID = "menuItemId";
  public static String PROP_MENU_ITEM_NAME = "menuItemName";
  public static String PROP_MENU_GROUP = "menuGroup";
  public static String PROP_LOCATION_ID = "locationId";
  public static String PROP_LOCATION_NAME = "locationName";
  public static String PROP_SKU = "sku";
  public static String PROP_UNIT = "unit";
  public static String PROP_MENU_ITEM_COST = "menuItemCost";
  public static String PROP_QUANTITY_IN_HAND = "quantityInHand";
  public static String PROP_TOTOAL_COST = "totalCost";
  private String menuGroup;
  private String menuItemName;
  private String menuItemId;
  private String locationId;
  private String sku;
  private String locationName;
  private double quantityInHand;
  private String unit;
  private double menuItemCost;
  private double totalCost;
  
  public InventoryStockData() {}
  
  public String getMenuGroup() { return menuGroup; }
  
  public void setMenuGroup(String menuGroup)
  {
    this.menuGroup = menuGroup;
  }
  
  public String getMenuItemName() {
    return menuItemName;
  }
  
  public void setMenuItemName(String menuItemName) {
    this.menuItemName = menuItemName;
  }
  
  public String getMenuItemId() {
    return menuItemId;
  }
  
  public void setMenuItemId(String menuItemId) {
    this.menuItemId = menuItemId;
  }
  
  public String getLocationId() {
    return locationId;
  }
  
  public void setLocationId(String locationId) {
    this.locationId = locationId;
  }
  
  public String getSku() {
    return sku == null ? "" : sku;
  }
  
  public void setSku(String sku) {
    this.sku = sku;
  }
  
  public String getLocationName() {
    if (StringUtils.isEmpty(locationName))
      return "";
    return locationName;
  }
  
  public void setLocationName(String locationName) {
    this.locationName = locationName;
  }
  
  public double getQuantityInHand() {
    return quantityInHand;
  }
  
  public void setQuantityInHand(double quantityInHand) {
    this.quantityInHand = quantityInHand;
  }
  
  public String getUnit() {
    if (StringUtils.isEmpty(unit))
      return "";
    return unit;
  }
  
  public void setUnit(String unit) {
    this.unit = unit;
  }
  
  public double getMenuItemCost() {
    return menuItemCost;
  }
  
  public void setMenuItemCost(double menuItemCost) {
    this.menuItemCost = menuItemCost;
  }
  
  public double getTotalCost() {
    return quantityInHand * menuItemCost;
  }
  
  public void setTotalCost(double totalCost) {
    this.totalCost = totalCost;
  }
}
