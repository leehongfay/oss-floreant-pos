package com.floreantpos.report;

import com.floreantpos.POSConstants;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.model.ext.PaperSize;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.services.report.ReportService;
import com.floreantpos.swing.time.TimeComboBox;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JRViewer;
import org.jdesktop.swingx.JXDatePicker;






















public class SalesBalanceReportView
  extends JPanel
{
  private SimpleDateFormat fullDateFormatter = new SimpleDateFormat("dd MMM yyyy, hh:mm a");
  
  private JXDatePicker fromDatePicker = UiUtil.getCurrentMonthStart();
  private JXDatePicker toDatePicker = UiUtil.getCurrentMonthEnd();
  private JComboBox cbUserType;
  private JButton btnToday;
  private JButton btnGo = new JButton(POSConstants.GO);
  private JPanel reportContainer;
  private JComboBox<Date> jcbStartTime;
  private JComboBox<Date> jcbEndTime;
  
  public SalesBalanceReportView() throws ParseException {
    super(new BorderLayout());
    
    JPanel topPanel = new JPanel(new MigLayout());
    
    btnToday = new JButton(POSConstants.TODAYS_REPORT.toUpperCase());
    btnToday.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          Date today = new Date();
          
          SalesBalanceReportView.this.viewReport(DateUtil.startOfDay(today), DateUtil.endOfDay(today));
        }
        catch (Exception ex) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), ex.getMessage(), ex);
        }
      }
    });
    cbUserType = new JComboBox();
    
    UserDAO dao = new UserDAO();
    List<User> userTypes = dao.findAll();
    
    Vector list = new Vector();
    list.add(POSConstants.ALL);
    list.addAll(userTypes);
    
    cbUserType.setModel(new DefaultComboBoxModel(list));
    
    TimeComboBox tcb = new TimeComboBox();
    jcbStartTime = tcb.getDefaultTimeComboBox();
    jcbEndTime = tcb.getDefaultTimeComboBox();
    
    topPanel.add(new JLabel(POSConstants.FROM + ":"));
    topPanel.add(fromDatePicker);
    topPanel.add(jcbStartTime);
    topPanel.add(new JLabel(POSConstants.TO + ":"));
    topPanel.add(toDatePicker);
    topPanel.add(jcbEndTime);
    topPanel.add(new JLabel(POSConstants.USER + ":"));
    topPanel.add(cbUserType);
    topPanel.add(btnGo, "width 60!");
    topPanel.add(btnToday);
    add(topPanel, "North");
    
    JPanel centerPanel = new JPanel(new BorderLayout());
    centerPanel.setBorder(new EmptyBorder(0, 10, 10, 10));
    centerPanel.add(new JSeparator(), "North");
    
    reportContainer = new JPanel(new BorderLayout());
    centerPanel.add(reportContainer);
    
    add(centerPanel);
    

    btnGo.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          Date fromDate = fromDatePicker.getDate();
          Date toDate = toDatePicker.getDate();
          
          Date fromTime = (Date)jcbStartTime.getSelectedItem();
          Date toTime = (Date)jcbEndTime.getSelectedItem();
          
          fromDate = DateUtil.copyTime(fromDate, fromTime);
          toDate = DateUtil.copyTime(toDate, toTime);
          
          SalesBalanceReportView.this.viewReport(fromDate, toDate);
        }
        catch (Exception e1) {
          POSMessageDialog.showError(SalesBalanceReportView.this, POSConstants.ERROR_MESSAGE, e1);
        }
      }
    });
  }
  
  private void viewReport(Date fromDate, Date toDate)
    throws Exception
  {
    if (fromDate.after(toDate)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      return;
    }
    
    User user = null;
    if (!cbUserType.getSelectedItem().equals(POSConstants.ALL)) {
      user = (User)cbUserType.getSelectedItem();
    }
    
    ReportService reportService = new ReportService();
    SalesBalanceReport report = reportService.getSalesBalanceReport(DateUtil.startOfDay(fromDate), DateUtil.endOfDay(toDate), user);
    
    HashMap map = new HashMap();
    ReportUtil.populateRestaurantProperties(map);
    map.put("fromTime", Long.valueOf(fromDate.getTime()));
    map.put("toTime", Long.valueOf(toDate.getTime()));
    map.put("fromDate", fullDateFormatter.format(fromDate));
    map.put("toDate", fullDateFormatter.format(toDate));
    map.put("reportTime", fullDateFormatter.format(new Date()));
    map.put("userName", user == null ? POSConstants.ALL : user.getFullName());
    
    map.put("grossTaxableSales", NumberUtil.formatNumber(Double.valueOf(report.getGrossTaxableSalesAmount())));
    map.put("grossNonTaxableSales", NumberUtil.formatNumber(Double.valueOf(report.getGrossNonTaxableSalesAmount())));
    map.put("discounts", NumberUtil.formatNumber(Double.valueOf(report.getDiscountAmount())));
    map.put("netSales", NumberUtil.formatNumber(Double.valueOf(report.getNetSalesAmount())));
    map.put("salesTaxes", NumberUtil.formatNumber(Double.valueOf(report.getSalesTaxAmount())));
    map.put("totalRevenues", NumberUtil.formatNumber(Double.valueOf(report.getTotalRevenueAmount())));
    map.put("giftCertSold", NumberUtil.formatNumber(Double.valueOf(report.getGiftCertSalesAmount())));
    map.put("payIns", NumberUtil.formatNumber(Double.valueOf(report.getPayInsAmount())));
    map.put("cashTips", NumberUtil.formatNumber(Double.valueOf(report.getCashTipsAmount())));
    map.put("chargedTips", NumberUtil.formatNumber(Double.valueOf(report.getChargedTipsAmount())));
    map.put("grossReceipts", NumberUtil.formatNumber(Double.valueOf(report.getGrossReceiptsAmount())));
    map.put("cashReceipts", NumberUtil.formatNumber(Double.valueOf(report.getCashReceiptsAmount())));
    map.put("creditCardReceipts", NumberUtil.formatNumber(Double.valueOf(report.getCreditCardReceiptsAmount())));
    map.put("debitCardReceipts", NumberUtil.formatNumber(Double.valueOf(report.getDebitCardReceiptsAmount())));
    map.put("memberPaymentReceipts", NumberUtil.formatNumber(Double.valueOf(report.getMemberPaymentAmount())));
    map.put("customPayment", NumberUtil.formatNumber(Double.valueOf(report.getCustomPaymentAmount())));
    map.put("grossTipsPaid", NumberUtil.formatNumber(Double.valueOf(report.getGrossTipsPaidAmount())));
    map.put("arReceipts", NumberUtil.formatNumber(Double.valueOf(report.getArReceiptsAmount())));
    map.put("giftCertReceipts", NumberUtil.formatNumber(Double.valueOf(report.getGiftCertReceipts())));
    map.put("cashBack", NumberUtil.formatNumber(Double.valueOf(report.getCashBackAmount())));
    map.put("cashRefund", NumberUtil.formatNumber(Double.valueOf(report.getCashRefundAmount())));
    map.put("receiptDiff", NumberUtil.formatNumber(Double.valueOf(report.getReceiptDiffAmount())));
    
    map.put("cashPayout", NumberUtil.formatNumber(Double.valueOf(report.getCashPayoutAmount())));
    map.put("cashAccountable", NumberUtil.formatNumber(Double.valueOf(report.getCashAccountableAmount())));
    map.put("drawerPulls", NumberUtil.formatNumber(Double.valueOf(report.getDrawerPullsAmount())));
    map.put("coCurrent", NumberUtil.formatNumber(Double.valueOf(report.getCoCurrentAmount())));
    map.put("coPrevious", NumberUtil.formatNumber(Double.valueOf(report.getCoPreviousAmount())));
    map.put("coOverShort", NumberUtil.formatNumber(Double.valueOf(report.getOverShortAmount())));
    map.put("days", String.valueOf((int)((toDate.getTime() - fromDate.getTime()) * (1.15740741D * Math.pow(10.0D, -8.0D))) + 1));
    
    map.put("visaCreditCardSum", NumberUtil.formatNumber(Double.valueOf(report.getVisaCreditCardAmount())));
    map.put("mastercardSum", NumberUtil.formatNumber(Double.valueOf(report.getMasterCardAmount())));
    map.put("amexSum", NumberUtil.formatNumber(Double.valueOf(report.getAmexAmount())));
    map.put("discoverySum", NumberUtil.formatNumber(Double.valueOf(report.getDiscoveryAmount())));
    



    JasperReport jasperReport = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("sales_balance_report"));
    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, new JREmptyDataSource());
    JRViewer viewer = new JRViewer(jasperPrint);
    reportContainer.removeAll();
    reportContainer.add(viewer);
    reportContainer.revalidate();
  }
}
