package com.floreantpos.report;

public class ReportConstants
{
  public static final String SALES_REPORT = "sales_report";
  public static final String SHIFTWISE_SALES_SUMMARY_REPORT = "shiftwise_sales_summary_report";
  public static final String SALES_BALANCE_REPORT = "sales_balance_report";
  public static final String KEY_STATISTICS_REPORT = "key_statistics_report";
  public static final String SALES_EXCEPTION_REPORT = "sales_exception_report";
  public static final String SALES_DETAIL_REPORT = "sales_detail_report";
  public static final String MENU_USAGE_REPORT = "menu_usage_report";
  public static final String VOID_ITEM_REPORT = "void_item_report";
  public static final String JOURNAL_REPORT = "journal_report";
  public static final String GIFT_CARD_SUMMARY_REPORT = "gift_card_summary_report";
  public static final String GIFT_CARD_DETAIL_REPORT = "gift_card_detail_report";
  public static final String CREDIT_CARD_REPORT = "credit-card-report";
  public static final String OPEN_TICKET_SUMMARY_REPORT = "open_ticket_summary_report";
  public static final String HOURLY_LABOR_REPORT = "hourly_labor_report";
  public static final String HOURLY_LABOR_SHIFT_SUBREPORT = "hourly_labor_shift_subreport";
  public static final String HOURLY_LABOR_SUBREPORT = "hourly_labor_subreport";
  public static final String DAILY_SUMMARY_REPORT = "daily_summary_report";
  public static final String DAILY_SUMMARY_SUBREPORT = "daily_summary_subreport";
  public static final String TRAC_TIPS_REPORT = "track_tips_report";
  public static final String PAYROLL_REPORT = "PayrollReport";
  public static final String WEEKLY_PAYROLL_REPORT = "WeeklyPayrollReport";
  public static final String SERVER_PRODUCTIVITY_REPORT = "server_productivity_report";
  public static final String EMPLOYEE_ATTENDENCE_REPORT = "EmployeeAttendanceReport";
  public static final String PAYMENT_RECEIVED_REPORT = "payment_receive_report";
  public static final String CUSTOMER_PAYMENT_REPORT = "customer-payment-report";
  
  public ReportConstants() {}
}
