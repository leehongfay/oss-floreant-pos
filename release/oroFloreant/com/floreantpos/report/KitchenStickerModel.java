package com.floreantpos.report;

import com.floreantpos.model.KitchenTicketItem;
import java.util.List;

public class KitchenStickerModel extends AbstractReportDataSource
{
  public KitchenStickerModel(List<KitchenSticker> items)
  {
    super(new String[] { "itemCount", "item", "itemModifier", "cookingInstruction", "time", "other", "token" });
    
    setRows(items);
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    KitchenSticker item = (KitchenSticker)rows.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      return item.getItemCount();
    case 1: 
      return item.getItemName();
    case 2: 
      return getModifiers(item);
    case 3: 
      return getCookingInsgtruction(item);
    case 4: 
      return item.getTime();
    case 5: 
      return item.getOrderType();
    case 6: 
      return item.getToken();
    }
    
    return null;
  }
  
  public String getModifiers(KitchenSticker item) {
    List<KitchenTicketItem> modifiers = item.getModifiers();
    String modifierName = "<html><body>";
    if (modifiers == null) {
      return modifierName + "</body></html>";
    }
    for (KitchenTicketItem kitchenTicketItem : modifiers) {
      modifierName = modifierName + kitchenTicketItem.getMenuItemName() + "<br>";
    }
    return modifierName + "</body></html>";
  }
  
  public String getCookingInsgtruction(KitchenSticker item) {
    List<KitchenTicketItem> kitchenTicketItems = item.getCookingInstructions();
    String instruction = "<html><body>";
    if (kitchenTicketItems == null) {
      return instruction + "</body></html>";
    }
    for (KitchenTicketItem kitchenTicketItem : kitchenTicketItems) {
      instruction = instruction + kitchenTicketItem.getMenuItemName() + "<br>";
    }
    return instruction;
  }
  
  public static class KitchenSticker {
    private String itemName;
    private List<KitchenTicketItem> cookingInstructions;
    private List<KitchenTicketItem> modifiers;
    private String time;
    private String orderType;
    private String itemCount;
    private Integer token;
    
    public KitchenSticker() {}
    
    public String getItemName() { return itemName; }
    
    public void setItemName(String itemName)
    {
      this.itemName = itemName;
    }
    
    public String getTime() {
      return time;
    }
    
    public void setTime(String time) {
      this.time = time;
    }
    
    public String getOrderType() {
      return orderType;
    }
    
    public void setOrderType(String orderType) {
      this.orderType = orderType;
    }
    
    public String getItemCount() {
      return itemCount;
    }
    
    public void setItemCount(String itemCount) {
      this.itemCount = itemCount;
    }
    
    public List<KitchenTicketItem> getCookingInstructions() {
      return cookingInstructions;
    }
    
    public void setCookingInstructions(List<KitchenTicketItem> cookingInstructions) {
      this.cookingInstructions = cookingInstructions;
    }
    
    public List<KitchenTicketItem> getModifiers() {
      return modifiers;
    }
    
    public void setModifiers(List<KitchenTicketItem> modifiers) {
      this.modifiers = modifiers;
    }
    
    public Integer getToken() {
      return token;
    }
    
    public void setToken(Integer token) {
      this.token = token;
    }
  }
}
