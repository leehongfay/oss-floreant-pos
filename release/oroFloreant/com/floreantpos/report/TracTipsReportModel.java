package com.floreantpos.report;

import com.floreantpos.swing.ListTableModel;
import java.util.List;


















public class TracTipsReportModel
  extends ListTableModel
{
  private String[] columnNames = { "id", "name", "grossSales", "ccSalesTips", "ccTips", "trackCCTipsRate", "trackCashSales", "trackCashTips", "trackCashTipsRate", "tipsPaid", "trackDeclareTips", "netTips" };
  
  public TracTipsReportModel()
  {
    setColumnNames(columnNames);
  }
  
  public TracTipsReportModel(List rows) {
    setColumnNames(columnNames);
    setRows(rows);
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    TracTipsReportView.TracTipsReportData reportData = (TracTipsReportView.TracTipsReportData)rows.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      if (reportData.getEmployeeID() != null)
        return reportData.getEmployeeID();
      return "";
    
    case 1: 
      if (reportData.getEmployeeName() != null)
        return reportData.getEmployeeName();
      return "";
    
    case 2: 
      return Double.valueOf(reportData.getGrossSales());
    
    case 3: 
      return Double.valueOf(reportData.getCreditCardSalesWithoutTips());
    
    case 4: 
      return Double.valueOf(reportData.getCreditCardTips());
    
    case 5: 
      return Double.valueOf(reportData.getTracCreditCardTipsRate() / 100.0D);
    
    case 6: 
      return Double.valueOf(reportData.getTracCashSalesWithoutTips());
    
    case 7: 
      return Double.valueOf(reportData.getTracCashTips());
    
    case 8: 
      return Double.valueOf(reportData.getTracCashTipsRate() / 100.0D);
    
    case 9: 
      return Double.valueOf(reportData.getTipsPaid());
    case 10: 
      return Double.valueOf(reportData.getDeclareTips());
    case 11: 
      return Double.valueOf(reportData.getNetTips());
    }
    
    return null;
  }
}
