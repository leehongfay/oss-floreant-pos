package com.floreantpos.report;

import com.floreantpos.model.MenuItem;
import com.floreantpos.model.PurchaseOrderItem;
import java.util.List;

















public class PurchaseOrderItemDataSource
  extends AbstractReportDataSource
{
  public PurchaseOrderItemDataSource(List<PurchaseOrderItem> items)
  {
    super(new String[] { "barcodeNumber", "itemName", "price" });
    setRows(items);
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    PurchaseOrderItem item = (PurchaseOrderItem)rows.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      return item.getMenuItem().getBarcode();
    case 1: 
      return item.getName();
    case 2: 
      return "$ " + item.getPriceDisplay();
    }
    
    return null;
  }
}
