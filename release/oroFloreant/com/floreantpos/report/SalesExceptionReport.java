package com.floreantpos.report;

import com.floreantpos.model.Discount;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketDiscount;
import com.floreantpos.swing.ListTableModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
















public class SalesExceptionReport
{
  private Date fromDate;
  private Date toDate;
  private Date reportTime;
  
  public SalesExceptionReport() {}
  
  private List<VoidData> voidedTickets = new ArrayList();
  private Map<String, DiscountData> disountMap = new HashMap();
  
  public void addVoidToVoidData(Ticket ticket) {
    double amount = ticket.getSubtotalAmount().doubleValue();
    String voidReason = ticket.getVoidReason();
    










    VoidData voidData = new VoidData();
    ticketid = ticket.getId();
    voidData.setReasonCode(voidReason);
    voidData.setCount(1);
    voidData.setAmount(amount);
    wasted = ticket.isWasted().booleanValue();
    
    voidedTickets.add(voidData);
  }
  
  public void addDiscountData(Ticket ticket)
  {
    List<TicketDiscount> discounts = ticket.getDiscounts();
    if (discounts != null) {
      for (TicketDiscount discount : discounts) {
        String name = discount.getName();
        DiscountData discountData = (DiscountData)disountMap.get(discount.getDiscountId());
        if (discountData == null) {
          discountData = new DiscountData();
          code = discount.getDiscountId();
          name = name;
          disountMap.put(discount.getDiscountId(), discountData);
        }
        
        totalCount = DiscountData.access$204(discountData);
        totalDiscount = (totalDiscount + discount.getValue().doubleValue());
        totalGuest = (totalGuest + ticket.getNumberOfGuests().intValue());
        totalNetSales = (totalNetSales + ticket.getSubtotalAmount().doubleValue());
        partySize = (totalGuest / totalCount);
        checkSize = (totalNetSales / totalCount);
      }
    }
  }
  
  public void addEmptyDiscounts(List<Discount> discounts) {
    if (discounts != null)
      for (Discount discount : discounts) {
        String name = discount.getName();
        DiscountData discountData = (DiscountData)disountMap.get(discount.getId());
        if (discountData == null) {
          discountData = new DiscountData();
          code = discount.getId();
          name = name;
          disountMap.put(discount.getId(), discountData);
        }
      } }
  
  public static void main(String[] args) {}
  
  public static class VoidData { String ticketid;
    private String reasonCode;
    private int count;
    private double amount;
    boolean wasted;
    
    public VoidData() {}
    
    public double getAmount() { return amount; }
    
    public void setAmount(double amount)
    {
      this.amount = amount;
    }
    
    public int getCount() {
      return count;
    }
    
    public void setCount(int count) {
      this.count = count;
    }
    
    public String getReasonCode() {
      return reasonCode;
    }
    

    public void setReasonCode(String reasonCode) { this.reasonCode = reasonCode; }
  }
  
  public static class DiscountData {
    private String code;
    private String name;
    private int totalCount;
    private double totalDiscount;
    private double totalNetSales;
    private double totalGuest;
    private double partySize;
    private double checkSize;
    private double countPercentage;
    private double ratioDNet;
    
    public DiscountData() {}
    
    public double getCheckSize() { return checkSize; }
    
    public void setCheckSize(double checkSize)
    {
      this.checkSize = checkSize;
    }
    
    public String getCode() {
      return code;
    }
    
    public void setCode(String code) {
      this.code = code;
    }
    
    public double getCountPercentage() {
      return countPercentage;
    }
    
    public void setCountPercentage(double countPercentage) {
      this.countPercentage = countPercentage;
    }
    
    public String getName() {
      return name;
    }
    
    public void setName(String name) {
      this.name = name;
    }
    
    public double getPartySize() {
      return partySize;
    }
    
    public void setPartySize(double partySize) {
      this.partySize = partySize;
    }
    
    public double getRatioDNet() {
      return ratioDNet;
    }
    
    public void setRatioDNet(double ratioDNet) {
      this.ratioDNet = ratioDNet;
    }
    
    public int getTotalCount() {
      return totalCount;
    }
    
    public void setTotalCount(int totalCount) {
      this.totalCount = totalCount;
    }
    
    public double getTotalDiscount() {
      return totalDiscount;
    }
    
    public void setTotalDiscount(double totalDiscount) {
      this.totalDiscount = totalDiscount;
    }
    
    public double getTotalGuest() {
      return totalGuest;
    }
    
    public void setTotalGuest(double totalGuest) {
      this.totalGuest = totalGuest;
    }
    
    public double getTotalNetSales() {
      return totalNetSales;
    }
    
    public void setTotalNetSales(double totalNetSales) {
      this.totalNetSales = totalNetSales;
    }
  }
  







  public Date getFromDate()
  {
    return fromDate;
  }
  
  public void setFromDate(Date fromDate) {
    this.fromDate = fromDate;
  }
  
  public Date getReportTime() {
    return reportTime;
  }
  
  public void setReportTime(Date reportTime) {
    this.reportTime = reportTime;
  }
  
  public Date getToDate() {
    return toDate;
  }
  
  public void setToDate(Date toDate) {
    this.toDate = toDate;
  }
  
  public VoidTableModel getVoidTableModel() {
    VoidTableModel model = new VoidTableModel();
    model.setRows(voidedTickets);
    
    return model;
  }
  
  public DiscountTableModel getDiscountTableModel() {
    DiscountTableModel model = new DiscountTableModel();
    ArrayList list = new ArrayList(disountMap.values());
    model.setRows(list);
    
    return model;
  }
  
  public class VoidTableModel extends ListTableModel {
    public VoidTableModel() {
      setColumnNames(new String[] { "code", "reason", "wast", "qty", "amount" });
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      SalesExceptionReport.VoidData data = (SalesExceptionReport.VoidData)rows.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return String.valueOf(ticketid);
      
      case 1: 
        return data.getReasonCode();
      
      case 2: 
        return wasted ? "Y" : "N";
      
      case 3: 
        return String.valueOf(data.getCount());
      
      case 4: 
        return Double.valueOf(data.getAmount());
      }
      
      return null;
    }
  }
  
  public class DiscountTableModel extends ListTableModel
  {
    public DiscountTableModel() {
      setColumnNames(new String[] { "no", "name", "code", "totalCount", "totalDiscount", "totalNetSales", "totalGuests", "partySize", "checkSize", "countPercent", "ratioDnet" });
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      SalesExceptionReport.DiscountData data = (SalesExceptionReport.DiscountData)rows.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return SalesExceptionReport.DiscountData.access$000(data);
      
      case 1: 
        return SalesExceptionReport.DiscountData.access$100(data);
      
      case 2: 
        return SalesExceptionReport.DiscountData.access$000(data);
      
      case 3: 
        return Integer.valueOf(SalesExceptionReport.DiscountData.access$200(data));
      
      case 4: 
        return Double.valueOf(SalesExceptionReport.DiscountData.access$300(data));
      case 5: 
        return Double.valueOf(SalesExceptionReport.DiscountData.access$500(data));
      case 6: 
        return Double.valueOf(SalesExceptionReport.DiscountData.access$400(data));
      case 7: 
        return Double.valueOf(SalesExceptionReport.DiscountData.access$600(data));
      case 8: 
        return Double.valueOf(SalesExceptionReport.DiscountData.access$700(data));
      case 9: 
        return Double.valueOf(SalesExceptionReport.DiscountData.access$800(data));
      case 10: 
        return Double.valueOf(SalesExceptionReport.DiscountData.access$900(data));
      }
      
      return null;
    }
  }
}
