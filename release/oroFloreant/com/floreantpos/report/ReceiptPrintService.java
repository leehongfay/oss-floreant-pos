package com.floreantpos.report;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.config.CardConfig;
import com.floreantpos.extension.PaymentGatewayPlugin;
import com.floreantpos.main.Application;
import com.floreantpos.model.CardReader;
import com.floreantpos.model.Currency;
import com.floreantpos.model.Customer;
import com.floreantpos.model.CustomerAccountTransaction;
import com.floreantpos.model.Gratuity;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.KitchenTicket;
import com.floreantpos.model.KitchenTicketItem;
import com.floreantpos.model.LabelItem;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PaymentType;
import com.floreantpos.model.PosPrinters;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.Printer;
import com.floreantpos.model.PrinterGroup;
import com.floreantpos.model.PurchaseOrder;
import com.floreantpos.model.ReceiptParam;
import com.floreantpos.model.RefundTransaction;
import com.floreantpos.model.SalesArea;
import com.floreantpos.model.Store;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.TerminalPrinters;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemTax;
import com.floreantpos.model.User;
import com.floreantpos.model.VirtualPrinter;
import com.floreantpos.model.VoidItem;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.dao.PrinterGroupDAO;
import com.floreantpos.model.dao.StoreDAO;
import com.floreantpos.model.dao.TerminalPrintersDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.ext.KitchenStatus;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.payment.CardProcessor;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.PrintServiceUtil;
import com.floreantpos.util.ReceiptUtil;
import java.awt.print.PrinterAbortException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.swing.ImageIcon;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import us.fatehi.magnetictrack.bankcard.BankCardMagneticTrack;
import us.fatehi.magnetictrack.bankcard.PrimaryAccountNumber;
import us.fatehi.magnetictrack.bankcard.Track1FormatB;
import us.fatehi.magnetictrack.bankcard.Track2;















































public class ReceiptPrintService
{
  private static final String DATA = "data";
  private static final String TITLE = "title";
  private static final String ORDER_ = "ORDER-";
  public static final String PROP_PRINTER_NAME = "printerName";
  private static final String TIP_AMOUNT = "tipAmount";
  private static final String SERVICE_CHARGE = "serviceCharge";
  private static final String DELIVERY_CHARGE = "deliveryCharge";
  private static final String TAX_AMOUNT = "taxAmount";
  private static final String DISCOUNT_AMOUNT = "discountAmount";
  private static final String HEADER_LINE1 = "headerLine1";
  private static final String REPORT_DATE = "reportDate";
  private static final String SHOW_FOOTER = "showFooter";
  private static final String SHOW_HEADER_SEPARATOR = "showHeaderSeparator";
  private static final String SHOW_SUBTOTAL = "showSubtotal";
  private static final String RECEIPT_TYPE = "receiptType";
  private static final String SUB_TOTAL_TEXT = "subTotalText";
  private static final String QUANTITY_TEXT = "quantityText";
  private static final String ITEM_TEXT = "itemText";
  private static Log logger = LogFactory.getLog(ReceiptPrintService.class);
  
  public static final String CUSTOMER_COPY = "Customer Copy";
  public static final String DRIVER_COPY = "Driver Copy";
  public static final String CENTER = "center";
  public static final String LEFT = "left";
  public static final String RIGHT = "right";
  
  public ReceiptPrintService() {}
  
  public static void printGenericReport(String title, String data)
    throws Exception
  {
    HashMap<String, Object> map = new HashMap(2);
    map.put("title", title);
    map.put("data", data);
    JasperPrint jasperPrint = createJasperPrint(ReportUtil.getReport("generic-receipt"), map, new JREmptyDataSource());
    
    jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
    printQuitely(jasperPrint);
  }
  
  public static void printClockInOutReceipt(User user) throws Exception {
    try {
      Date currentTime = new Date();
      Store store = Application.getInstance().getStore();
      
      SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
      SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd MMM, hh:mm aaa");
      SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm:ss aaa");
      
      String clockInOutInfo = "Clock out at : " + timeFormat.format(currentTime);
      
      String sep = "\n.......................................................................";
      String data = "\n" + store.getName();
      data = data + "\n" + store.getAddressLine1();
      data = data + sep;
      data = data + "\n-" + user.getFullName() + " (#" + user.getId() + ")" + (user.isClockedIn().booleanValue() ? " clock in" : " clock out") + "-";
      data = data + sep;
      data = data + "\nDate             : " + dateFormat.format(currentTime);
      data = data + "\n" + clockInOutInfo;
      data = data + "\nPrinted On   : " + dateFormat2.format(new Date());
      data = data + sep;
      
      printGenericReport("", data);
    } catch (Exception ee) {
      throw ee;
    }
  }
  
  public static void testPrinter(String deviceName, String title, String data) throws Exception {
    HashMap<String, Object> map = new HashMap(2);
    map.put("title", title);
    map.put("data", data);
    JasperPrint jasperPrint = createJasperPrint(ReportUtil.getReport("test-printer"), map, new JREmptyDataSource());
    jasperPrint.setProperty("printerName", deviceName);
    printQuitely(jasperPrint);
  }
  
  public static JasperPrint createJasperPrint(JasperReport report, Map<String, Object> properties, JRDataSource dataSource) throws Exception {
    JasperPrint jasperPrint = JasperFillManager.fillReport(report, properties, dataSource);
    return jasperPrint;
  }
  
  public static JasperPrint createPrint(Ticket ticket, Map<String, Object> map, PosTransaction transaction) throws Exception {
    TicketDataSource dataSource = new TicketDataSource(ticket);
    return createJasperPrint(ReportUtil.getReport("ticket-receipt"), map, new JRTableModelDataSource(dataSource));
  }
  
  public static JasperPrint createPurchaseOrderPrint(PurchaseOrder order) throws Exception {
    TicketPrintProperties printProperties = new TicketPrintProperties(null, false, true, true);
    
    printProperties.setPrintCookingInstructions(false);
    HashMap map = populatePurchaseOrderProperties(order, printProperties, null);
    map.put("copyType", "");
    
    OrderDataSource dataSource = new OrderDataSource(order);
    String receiptName = "order_receipt";
    map.put("previousDueText", "Previous Due  ");
    map.put("previousDue", "");
    
    return createJasperPrint(ReportUtil.getReport(receiptName), map, new JRTableModelDataSource(dataSource));
  }
  
  public static JasperPrint createPurchaseOrderItemsBarcodePrint(List<LabelItem> orderedItemList) throws Exception {
    HashMap map = new HashMap();
    LabelPrinterTableModel dataSource = new LabelPrinterTableModel(orderedItemList);
    String receiptName = "barcode_report";
    
    return createJasperPrint(ReportUtil.getReport(receiptName), map, new JRTableModelDataSource(dataSource));
  }
  
  public static JasperPrint printKitchenStickerItems(List<KitchenStickerModel.KitchenSticker> stickerItemList) throws Exception {
    try {
      HashMap map = new HashMap();
      KitchenStickerModel dataSource = new KitchenStickerModel(stickerItemList);
      String receiptName = "kitchenStickerReport";
      
      return createJasperPrint(ReportUtil.getReport(receiptName), map, new JRTableModelDataSource(dataSource));
    } catch (Exception e) {
      PosLog.error(ReceiptPrintService.class, e.getMessage(), e);
    }
    return null;
  }
  























  public static JasperPrint createJasperFileForTicketReceipt(Ticket ticket)
  {
    return createJasperFileForTicketReceipt(ticket, null);
  }
  
  public static JasperPrint createJasperFileForTicketReceipt(Ticket ticket, Boolean ignorePagination) {
    try {
      TicketPrintProperties printProperties = new TicketPrintProperties(null, false, true, true);
      
      printProperties.setPrintCookingInstructions(false);
      HashMap map = populateTicketProperties(ticket, printProperties, null);
      if (ignorePagination != null) {
        map.put("IS_IGNORE_PAGINATION", ignorePagination);
      }
      
      return createPrint(ticket, map, null);
    } catch (Exception e) {
      PosLog.error(ReceiptPrintService.class, e);
    }
    return null;
  }
  
  public static void printTicket(Ticket ticket) {
    try {
      TicketPrintProperties printProperties = new TicketPrintProperties(null, false, true, true);
      
      printProperties.setPrintCookingInstructions(false);
      map = populateTicketProperties(ticket, printProperties, null);
      
      List<TerminalPrinters> terminalPrinters = TerminalPrintersDAO.getInstance().findTerminalPrinters();
      
      List<Printer> activeReceiptPrinters = new ArrayList();
      
      for (TerminalPrinters terminalPrinters2 : terminalPrinters)
      {
        int printerType = terminalPrinters2.getVirtualPrinter().getType().intValue();
        
        if (printerType == 1)
        {
          Printer printer = new Printer(terminalPrinters2.getVirtualPrinter(), terminalPrinters2.getPrinterName());
          activeReceiptPrinters.add(printer);
        }
      }
      
      if ((activeReceiptPrinters == null) || (activeReceiptPrinters.isEmpty()))
      {
        jasperPrint = createPrint(ticket, map, null);
        ((JasperPrint)jasperPrint).setName("ORDER-" + ticket.getId());
        ((JasperPrint)jasperPrint).setProperty("printerName", Application.getPrinters().getReceiptPrinter());
        printQuitely((JasperPrint)jasperPrint);

      }
      else
      {
        for (jasperPrint = activeReceiptPrinters.iterator(); ((Iterator)jasperPrint).hasNext();) { Printer activeReceiptPrinter = (Printer)((Iterator)jasperPrint).next();
          
          JasperPrint jasperPrint = createPrint(ticket, map, null);
          jasperPrint.setName("ORDER-" + ticket.getId() + activeReceiptPrinter.getDeviceName());
          jasperPrint.setProperty("printerName", activeReceiptPrinter.getDeviceName());
          printQuitely(jasperPrint);
        }
      }
    } catch (Exception e) { HashMap map;
      Object jasperPrint;
      logger.error(POSConstants.PRINT_ERROR, e);
    }
  }
  
  public static void printTicket(Ticket ticket, String copyType) {
    try {
      TicketPrintProperties printProperties = new TicketPrintProperties(null, false, true, true);
      
      printProperties.setPrintCookingInstructions(false);
      map = populateTicketProperties(ticket, printProperties, null);
      map.put("copyType", copyType);
      map.put("cardPayment", Boolean.valueOf(true));
      
      List<TerminalPrinters> terminalPrinters = TerminalPrintersDAO.getInstance().findTerminalPrinters();
      
      List<Printer> activeReceiptPrinters = new ArrayList();
      
      for (TerminalPrinters terminalPrinters2 : terminalPrinters)
      {
        int printerType = terminalPrinters2.getVirtualPrinter().getType().intValue();
        
        if (printerType == 1)
        {
          Printer printer = new Printer(terminalPrinters2.getVirtualPrinter(), terminalPrinters2.getPrinterName());
          activeReceiptPrinters.add(printer);
        }
      }
      
      if ((activeReceiptPrinters == null) || (activeReceiptPrinters.isEmpty()))
      {
        jasperPrint = createPrint(ticket, map, null);
        ((JasperPrint)jasperPrint).setName("ORDER-" + ticket.getId());
        ((JasperPrint)jasperPrint).setProperty("printerName", Application.getPrinters().getReceiptPrinter());
        printQuitely((JasperPrint)jasperPrint);

      }
      else
      {
        for (jasperPrint = activeReceiptPrinters.iterator(); ((Iterator)jasperPrint).hasNext();) { Printer activeReceiptPrinter = (Printer)((Iterator)jasperPrint).next();
          
          JasperPrint jasperPrint = createPrint(ticket, map, null);
          jasperPrint.setName("ORDER-" + ticket.getId() + activeReceiptPrinter.getDeviceName());
          jasperPrint.setProperty("printerName", activeReceiptPrinter.getDeviceName());
          printQuitely(jasperPrint);
        }
      }
    } catch (Exception e) { HashMap map;
      Object jasperPrint;
      logger.error(POSConstants.PRINT_ERROR, e);
    }
  }
  
  public static JasperPrint createRefundPrint(Ticket ticket, HashMap map) throws Exception {
    TicketDataSource dataSource = new TicketDataSource(ticket);
    return createJasperPrint(ReportUtil.getReport("ticket-receipt"), map, new JRTableModelDataSource(dataSource));
  }
  
  public static void printRefundTicket(Ticket ticket, List<PosTransaction> posTransactions) {
    try {
      if ((posTransactions == null) || (posTransactions.isEmpty()))
        return;
      double refundAmount = 0.0D;
      if (posTransactions != null) {
        for (PosTransaction t : posTransactions) {
          if (((t instanceof RefundTransaction)) || (t.isVoided().booleanValue()))
            refundAmount += t.getAmount().doubleValue();
        }
      }
      TicketPrintProperties printProperties = new TicketPrintProperties("*** REFUND RECEIPT ***", true, true, true);
      
      printProperties.setPrintCookingInstructions(false);
      HashMap map = populateTicketProperties(ticket, printProperties, null);
      map.put("refundAmountText", Messages.getString("ReceiptPrintService.1"));
      map.put("refundAmount", NumberUtil.formatNumber(Double.valueOf(refundAmount)));
      map.put("cashRefundText", Messages.getString("ReceiptPrintService.2"));
      map.put("cashRefund", NumberUtil.formatNumber(Double.valueOf(refundAmount)));
      
      JasperPrint jasperPrint = createRefundPrint(ticket, map);
      jasperPrint.setName("REFUND_" + ticket.getId());
      jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
      printQuitely(jasperPrint);
    }
    catch (Exception e) {
      logger.error(POSConstants.PRINT_ERROR, e);
    }
  }
  
  public static void printRefundTicket(Ticket ticket, RefundTransaction posTransaction) {
    try {
      TicketPrintProperties printProperties = new TicketPrintProperties("*** REFUND RECEIPT ***", true, true, true);
      
      printProperties.setPrintCookingInstructions(false);
      HashMap map = populateTicketProperties(ticket, printProperties, posTransaction);
      map.put("refundAmountText", Messages.getString("ReceiptPrintService.1"));
      map.put("refundAmount", NumberUtil.formatNumber(posTransaction.getAmount()));
      map.put("cashRefundText", Messages.getString("ReceiptPrintService.2"));
      map.put("cashRefund", NumberUtil.formatNumber(posTransaction.getAmount()));
      
      JasperPrint jasperPrint = createRefundPrint(ticket, map);
      jasperPrint.setName("REFUND_" + ticket.getId());
      jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
      printQuitely(jasperPrint);
    }
    catch (Exception e) {
      logger.error(POSConstants.PRINT_ERROR, e);
    }
  }
  
  public static void printVoidTicket(Ticket ticket) {
    try {
      TicketPrintProperties printProperties = new TicketPrintProperties("*** VOID RECEIPT ***", true, true, true);
      printProperties.setPrintCookingInstructions(false);
      HashMap map = populateTicketProperties(ticket, printProperties, null);
      
      String refundText = "";
      if (ticket.getTransactions() != null) {
        Set<PosTransaction> posTransactions = ticket.getTransactions();
        double refundAmount = 0.0D;
        if (posTransactions != null) {
          for (PosTransaction t : posTransactions) {
            if (((t instanceof RefundTransaction)) || (t.isVoided().booleanValue()))
              refundAmount += t.getAmount().doubleValue();
          }
        }
        refundAmount = NumberUtil.roundToTwoDigit(refundAmount);
      }
      map.put("additionalProperties", "<html><b>" + refundText + "</b></html>");
      map.put("additionalPaymentProperties", "");
      
      JasperPrint jasperPrint = createPrint(ticket, map, null);
      jasperPrint.setName("VOID_" + ticket.getId());
      jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
      printQuitely(jasperPrint);
    } catch (Exception e) {
      logger.error(POSConstants.PRINT_ERROR, e);
    }
  }
  
  public static void printTransaction(PosTransaction transaction) {
    try {
      Ticket ticket = transaction.getTicket();
      
      TicketPrintProperties printProperties = new TicketPrintProperties(Messages.getString("ReceiptPrintService.3"), true, true, true);
      printProperties.setPrintCookingInstructions(false);
      HashMap map = populateTicketProperties(ticket, printProperties, transaction);
      
      if ((transaction != null) && (transaction.isCard())) {
        CardReader cardReader = CardReader.fromString(transaction.getCardReader());
        
        if (cardReader == CardReader.EXTERNAL_TERMINAL) {
          return;
        }
        
        map.put("cardPayment", Boolean.valueOf(true));
        map.put("copyType", Messages.getString("ReceiptPrintService.4"));
        JasperPrint jasperPrint = createPrint(ticket, map, transaction);
        jasperPrint.setName("Ticket-" + ticket.getId() + "-CustomerCopy");
        jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
        printQuitely(jasperPrint);
        
        map.put("copyType", Messages.getString("ReceiptPrintService.5"));
        jasperPrint = createPrint(ticket, map, transaction);
        jasperPrint.setName("Ticket-" + ticket.getId() + "-MerchantCopy");
        jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
        printQuitely(jasperPrint);
      }
      else if (transaction.getPaymentType().equals(PaymentType.CUSTOMER_ACCOUNT.getDisplayString())) {
        map.put("additionalPaymentProperties", createMemberAccountInfo(transaction.getTicket()));
        JasperPrint jasperPrint = createPrint(ticket, map, transaction);
        jasperPrint.setName("Ticket-" + ticket.getId());
        jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
        printQuitely(jasperPrint);
      }
      else {
        JasperPrint jasperPrint = createPrint(ticket, map, transaction);
        jasperPrint.setName("Ticket-" + ticket.getId());
        jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
        printQuitely(jasperPrint);
      }
    }
    catch (Exception e) {
      logger.error(POSConstants.PRINT_ERROR, e);
    }
  }
  
  private static String createMemberAccountInfo(Ticket ticket) {
    StringBuilder info = new StringBuilder();
    info.append("<html>");
    
    Customer customer = CustomerDAO.getInstance().findById(ticket.getCustomerId());
    beginRow(info);
    addColumn(info, "Payment from member balance ---");
    endRow(info);
    
    beginRow(info);
    addColumn(info, "Member: " + customer.getId());
    endRow(info);
    
    beginRow(info);
    addColumn(info, "Name: " + customer.getName());
    endRow(info);
    
    beginRow(info);
    addColumn(info, "Balance: " + CurrencyUtil.getCurrencySymbol() + NumberUtil.formatNumberAcceptNegative(customer.getBalance()));
    endRow(info);
    
    info.append("</html>");
    return info.toString();
  }
  
  public static JasperPrint getTransactionReceipt(PosTransaction transaction)
  {
    try {
      Ticket ticket = transaction.getTicket();
      
      TicketPrintProperties printProperties = new TicketPrintProperties(Messages.getString("ReceiptPrintService.3"), true, true, true);
      printProperties.setPrintCookingInstructions(false);
      HashMap map = populateTicketProperties(ticket, printProperties, transaction);
      map.put("IS_IGNORE_PAGINATION", Boolean.valueOf(true));
      if ((transaction != null) && (transaction.isCard()))
      {
        CardReader cardReader = CardReader.fromString(transaction.getCardReader());
        
        if (cardReader == CardReader.EXTERNAL_TERMINAL) {
          return null;
        }
        
        map.put("cardPayment", Boolean.valueOf(true));
        map.put("copyType", Messages.getString("ReceiptPrintService.4"));
        JasperPrint jasperPrint = createPrint(ticket, map, transaction);
        jasperPrint.setName("Ticket-" + ticket.getId() + "-CustomerCopy");
        jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
        
        map.put("copyType", Messages.getString("ReceiptPrintService.5"));
        jasperPrint = createPrint(ticket, map, transaction);
        jasperPrint.setName("Ticket-" + ticket.getId() + "-MerchantCopy");
        jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
        return jasperPrint;
      }
      
      JasperPrint jasperPrint = createPrint(ticket, map, transaction);
      jasperPrint.setName("Ticket-" + ticket.getId());
      jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
      return jasperPrint;
    }
    catch (Exception e) {
      logger.error(POSConstants.PRINT_ERROR, e);
    }
    return null;
  }
  
  public static void printTransaction(PosTransaction transaction, boolean printCustomerCopy) {
    try {
      Ticket ticket = transaction.getTicket();
      
      TicketPrintProperties printProperties = new TicketPrintProperties(Messages.getString("ReceiptPrintService.6"), true, true, true);
      printProperties.setPrintCookingInstructions(false);
      HashMap map = populateTicketProperties(ticket, printProperties, transaction);
      
      if ((transaction != null) && (transaction.isCard())) {
        map.put("cardPayment", Boolean.valueOf(true));
        map.put("copyType", Messages.getString("ReceiptPrintService.7"));
        
        JasperPrint jasperPrint = createPrint(ticket, map, transaction);
        jasperPrint.setName("Ticket-" + ticket.getId() + "-MerchantCopy");
        jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
        printQuitely(jasperPrint);
        
        if (printCustomerCopy) {
          map.put("copyType", Messages.getString("ReceiptPrintService.8"));
          
          jasperPrint = createPrint(ticket, map, transaction);
          jasperPrint.setName("Ticket-" + ticket.getId() + "-CustomerCopy");
          jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
          printQuitely(jasperPrint);
        }
      }
      else {
        JasperPrint jasperPrint = createPrint(ticket, map, transaction);
        jasperPrint.setName("Ticket-" + ticket.getId());
        jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
        printQuitely(jasperPrint);
      }
    }
    catch (Exception e) {
      logger.error(POSConstants.PRINT_ERROR, e);
    }
  }
  
  private static void beginRow(StringBuilder html) {
    html.append("<div>");
  }
  
  private static void endRow(StringBuilder html) {
    html.append("</div>");
  }
  
  private static void addColumn(StringBuilder html, String columnText) {
    html.append("<span>" + columnText + "</span>");
  }
  
  public static HashMap populateTicketProperties(Ticket ticket, TicketPrintProperties printProperties, PosTransaction transaction) {
    return populateTicketProperties(ticket, printProperties, transaction, false);
  }
  
  public static HashMap populateTicketProperties(Ticket ticket, TicketPrintProperties printProperties, PosTransaction transaction, boolean kitchenReceipt) {
    Application.getInstance().refreshStore();
    Store store = Application.getInstance().getStore();
    Terminal terminal = Application.getInstance().getTerminal();
    
    String header = "";
    String footer = "";
    String bottom = "";
    String orderInfo = "";
    String extraOrderInfo = "";
    OrderType orderType = ticket.getOrderType();
    String orderTypeId = "";
    HashMap parameters = new HashMap();
    
    if (orderType != null) {
      orderTypeId = orderType.getId();
    }
    
    if (kitchenReceipt) {
      header = ReceiptUtil.getReceiptSection(store, "kitchen.header", orderTypeId);
      orderInfo = ReceiptUtil.getReceiptSection(store, "kitchen.order.info", orderTypeId);
      extraOrderInfo = ReceiptUtil.getReceiptSection(store, "kitchen.order.extrainfo1", orderTypeId);
      footer = ReceiptUtil.getReceiptSection(store, "kitchen.footer", orderTypeId);
      bottom = ReceiptUtil.getReceiptSection(store, "kitchen.bottom", orderTypeId);
      
      if (Application.getPrinters().getDefaultKitchenPrinter() != null) {
        setPaginationProperty(parameters, Application.getPrinters().getDefaultKitchenPrinter().getDeviceName());
      }
    }
    else {
      header = ReceiptUtil.getReceiptSection(store, "ticket.header", orderTypeId);
      orderInfo = ReceiptUtil.getReceiptSection(store, "ticket.order.info", orderTypeId);
      extraOrderInfo = ReceiptUtil.getReceiptSection(store, "ticket.order.extrainfo1", orderTypeId);
      footer = ReceiptUtil.getReceiptSection(store, "ticket.footer", orderTypeId);
      bottom = ReceiptUtil.getReceiptSection(store, "ticket.bottom", orderTypeId);
      
      setPaginationProperty(parameters, Application.getPrinters().getReceiptPrinter());
    }
    
    boolean showHeaderLogo = Boolean.valueOf(ReceiptUtil.getReceiptSection(store, kitchenReceipt ? "kitchen.header.logo.show" : "ticket.header.logo.show", orderTypeId)).booleanValue();
    double totalAmount = ticket.getTotalAmount().doubleValue();
    double tipAmount = 0.0D;
    
    String currencySymbol = CurrencyUtil.getCurrencySymbol();
    
    parameters.put(ReceiptParam.STORE_NAME.getParamName(), store.getName());
    parameters.put(ReceiptParam.STORE_ADDRESS1.getParamName(), store.getAddressLine1());
    parameters.put(ReceiptParam.STORE_ADDRESS2.getParamName(), store.getAddressLine2());
    parameters.put(ReceiptParam.STORE_ADDRESS3.getParamName(), store.getAddressLine3());
    parameters.put(ReceiptParam.STORE_PHONE_NO.getParamName(), store.getTelephone());
    
    parameters.put(ReceiptParam.CURRENCY_SYMBOL.getParamName(), currencySymbol);
    parameters.put(ReceiptParam.TICKET_ID.getParamName(), ticket.getId());
    parameters.put(ReceiptParam.TICKET_SHORT_ID.getParamName(), ticket.getShortId());
    parameters.put(ReceiptParam.ORDER_DATE.getParamName(), DateUtil.formatFullDateAndTimeAsString(ticket.getCreateDate()));
    parameters.put(ReceiptParam.TOKEN_NO.getParamName(), "" + ticket.getTokenNo());
    
    if ((ticket.getOrderType().isDelivery().booleanValue()) || (ticket.getOrderType().isPickup().booleanValue())) {
      if (ticket.isCustomerWillPickup().booleanValue()) {
        parameters.put(ReceiptParam.ORDER_TYPE.getParamName(), "PICKUP");
      }
      else {
        parameters.put(ReceiptParam.ORDER_TYPE.getParamName(), "DELIVERY");
      }
      parameters.put(ReceiptParam.DELIVERY_ADDRESS.getParamName(), ticket.getDeliveryAddress() + ticket.getExtraDeliveryInfo());
      Date deliveryDate = ticket.getDeliveryDate();
      if (deliveryDate != null) {
        parameters.put(ReceiptParam.DELIVERY_DATE.getParamName(), DateUtil.formatFullDateAndTimeAsString(deliveryDate));
      }
    } else {
      parameters.put(ReceiptParam.ORDER_TYPE.getParamName(), ticket.getOrderType());
    }
    
    parameters.put(ReceiptParam.TERMINAL_ID.getParamName(), ticket.getTerminal().getId());
    parameters.put(ReceiptParam.TERMINAL_NAME.getParamName(), ticket.getTerminal().getName());
    parameters.put(ReceiptParam.SERVER_NAME.getParamName(), ticket.getOwner().getFullName());
    parameters.put(ReceiptParam.SERVER_ID.getParamName(), ticket.getOwner().getId());
    parameters.put(ReceiptParam.GUEST_COUNT.getParamName(), ticket.getNumberOfGuests().toString());
    
    if (terminal.isShowTableNumber()) {
      parameters.put(ReceiptParam.TABLE_NO.getParamName(), ticket.getTableNumbers() != null ? ticket.getTableNumbers().toString() : "");
    }
    else {
      parameters.put(ReceiptParam.TABLE_NO.getParamName(), ticket.getTableNames());
    }
    if (ticket.getCustomer() != null) {
      Customer customer = ticket.getCustomer();
      parameters.put(ReceiptParam.CUSTOMER_NAME.getParamName(), customer.getName());
      parameters.put(ReceiptParam.CUSTOMER_ID.getParamName(), customer.getMemberId() != null ? customer.getMemberId() : "");
      parameters.put(ReceiptParam.CUSTOMER_PHONE.getParamName(), customer.getMobileNo());
      
      parameters.put(ReceiptParam.CUSTOMER_SIGNATURE.getParamName(), customer.getSignatureImageId());
    }
    if (ticket.getSalesArea() != null) {
      SalesArea salesArea = ticket.getSalesArea();
      parameters.put(ReceiptParam.SALES_AREA.getParamName(), salesArea.getName());
    }
    if (ticket.getTransactions() != null) {
      Set<PosTransaction> transactions = ticket.getTransactions();
      String strPaymentType = "";
      for (Iterator iterator = transactions.iterator(); iterator.hasNext();) {
        PosTransaction posTransaction = (PosTransaction)iterator.next();
        strPaymentType = posTransaction.getPaymentType();
        if (iterator.hasNext()) {
          strPaymentType = strPaymentType + ",";
        }
      }
      parameters.put(ReceiptParam.PAYMENT_TYPE.getParamName(), strPaymentType);
    }
    if (ticket.getAssignedDriver() != null) {
      User driver = ticket.getAssignedDriver();
      parameters.put(ReceiptParam.DRIVER_NAME.getParamName(), driver.getFullName());
      parameters.put(ReceiptParam.DRIVER_ID.getParamName(), driver.getId());
    }
    String barcodeParamName = ReceiptParam.BARCODE.getParamName();
    if ((header.contains(barcodeParamName)) || (orderInfo.contains(barcodeParamName)) || (extraOrderInfo.contains(barcodeParamName)) || (footer.contains(barcodeParamName)) || (bottom.contains(barcodeParamName))) {
      parameters.put(barcodeParamName, String.valueOf(ticket.getId()));
    }
    parameters.put(ReceiptParam.RECEIPT_TYPE.getParamName(), printProperties.getReceiptTypeName());
    
    String splitTicketId = ticket.getProperty("SPLIT_TICKET");
    if (StringUtils.isNotEmpty(splitTicketId))
    {
      parameters.put("splitTicketId", splitTicketId);
    }
    parameters.put(ReceiptParam.PRINT_DATE.getParamName(), DateUtil.formatFullDateAndTimeAsString(new Date()));
    
    parameters.put("itemText", POSConstants.RECEIPT_REPORT_ITEM_LABEL);
    parameters.put("quantityText", POSConstants.RECEIPT_REPORT_QUANTITY_LABEL);
    parameters.put("subTotalText", POSConstants.RECEIPT_REPORT_SUBTOTAL_LABEL);
    parameters.put("showSubtotal", Boolean.valueOf(printProperties.isShowSubtotal()));
    parameters.put("showHeaderSeparator", Boolean.TRUE);
    parameters.put("showFooter", Boolean.valueOf(printProperties.isShowFooter()));
    


















    parameters.put("ticketHeader", getReceiptParamValuesAsString(parameters, orderInfo));
    parameters.put("additionalOrderInfo", getReceiptParamValuesAsString(parameters, extraOrderInfo));
    


    parameters.put("headerLine1", getReceiptParamValuesAsString(parameters, header));
    

    if (printProperties.isShowFooter()) {
      if (ticket.getDiscountAmount().doubleValue() > 0.0D) {
        parameters.put("discountAmount", NumberUtil.formatNumber(ticket.getDiscountAmount()));
      }
      
      if (ticket.getTaxAmount().doubleValue() != 0.0D) {
        parameters.put("taxAmount", NumberUtil.formatNumber(ticket.getTaxAmount(), true));
      }
      
      if (ticket.getServiceCharge().doubleValue() > 0.0D) {
        parameters.put("serviceCharge", NumberUtil.formatNumber(ticket.getServiceCharge()));
      }
      
      if (ticket.getDeliveryCharge().doubleValue() > 0.0D) {
        parameters.put("deliveryCharge", NumberUtil.formatNumber(ticket.getDeliveryCharge()));
      }
      
      if (ticket.getGratuity() != null) {
        tipAmount = ticket.getGratuity().getAmount().doubleValue();
        parameters.put("tipAmount", NumberUtil.formatNumber(Double.valueOf(tipAmount)));
      }
      
      parameters.put("totalText", POSConstants.SUBTOTAL + currencySymbol);
      parameters.put("discountText", POSConstants.RECEIPT_REPORT_DISCOUNT_LABEL + currencySymbol);
      parameters.put("taxText", POSConstants.RECEIPT_REPORT_TAX_LABEL + currencySymbol);
      parameters.put("serviceChargeText", POSConstants.RECEIPT_REPORT_SERVICE_CHARGE_LABEL + currencySymbol);
      parameters.put("deliveryChargeText", POSConstants.RECEIPT_REPORT_DELIVERY_CHARGE_LABEL + currencySymbol);
      parameters.put("tipsText", POSConstants.RECEIPT_REPORT_TIPS_LABEL + currencySymbol);
      parameters.put("netAmountText", POSConstants.RECEIPT_REPORT_TOTAL_LABEL + currencySymbol);
      parameters.put("paidAmountText", POSConstants.RECEIPT_REPORT_PAIDAMOUNT_LABEL + currencySymbol);
      parameters.put("dueAmountText", POSConstants.RECEIPT_REPORT_DUEAMOUNT_LABEL + currencySymbol);
      parameters.put("changeAmountText", POSConstants.RECEIPT_REPORT_CHANGEAMOUNT_LABEL + currencySymbol);
      
      parameters.put("netAmount", NumberUtil.formatNumber(Double.valueOf(totalAmount), true));
      parameters.put("paidAmount", NumberUtil.formatNumber(ticket.getPaidAmount()));
      Double refundAmount = ticket.getRefundAmount();
      if (transaction != null) {
        Double tenderAmount = transaction.getTenderAmount();
        parameters.put("tenderAmountText", POSConstants.RECEIPT_TENDERED_AMOUNT_LABEL + currencySymbol);
        parameters.put("tenderAmount", NumberUtil.formatNumber(tenderAmount, true));
      }
      if (refundAmount.doubleValue() > 0.0D) {
        parameters.put("refundAmountText", "Refund Amount" + currencySymbol);
        parameters.put("refundAmount", NumberUtil.formatNumber(refundAmount));
      }
      parameters.put("dueAmount", NumberUtil.formatNumber(Double.valueOf(ticket.getDueAmount().doubleValue() - ticket.getGratuityAmount())));
      parameters.put("grandSubtotal", NumberUtil.formatNumber(ticket.getSubtotalAmount(), true));
      

      parameters.put("footerMessage", getReceiptParamValuesAsString(parameters, footer));
      parameters.put("bottomMessage", getReceiptParamValuesAsString(parameters, bottom));
      parameters.put("copyType", printProperties.getReceiptCopyType());
      
      if (ticket.isRefunded().booleanValue()) {
        populateRefundProperties(transaction, ticket.getTransactions(), parameters);
      }
      
      if (((StringUtils.isEmpty(splitTicketId)) || (transaction != null) || (ticket.isClosed().booleanValue())) || 
      


        (transaction != null)) {
        double changedAmount = transaction.getTenderAmount().doubleValue() - transaction.getAmount().doubleValue();
        if (changedAmount < 0.0D) {
          changedAmount = 0.0D;
        }
        parameters.put("changedAmount", NumberUtil.formatNumber(Double.valueOf(changedAmount)));
        
        if (transaction.isCard()) {
          if (!orderType.isRetailOrder().booleanValue()) {
            parameters.put("cardPayment", Boolean.valueOf(true));
          }
          String cardInformationForReceipt = CardConfig.getPaymentGateway().getProcessor().getCardInformationForReceipt(transaction);
          if (StringUtils.isEmpty(cardInformationForReceipt)) {
            cardInformationForReceipt = getCardInformation(transaction);
          }
          parameters.put("approvalCode", cardInformationForReceipt);
        }
        
        if ((transaction instanceof CustomerAccountTransaction)) {
          if (!orderType.isRetailOrder().booleanValue()) {
            parameters.put("cardPayment", Boolean.valueOf(true));
          }
          
          String customerBlnceInformationForReceipt = getCustBlnceInformation(transaction);
          parameters.put("approvalCode", customerBlnceInformationForReceipt);
        }
      }
      StringBuilder paymentSummary = null;
      if (StringUtils.isEmpty(splitTicketId)) {
        paymentSummary = buildPayments(ticket);
      }
      if (paymentSummary != null) {
        parameters.put("additionalPaymentProperties", paymentSummary.toString());
      }
      if (Application.getInstance().getTerminal().isEnableMultiCurrency().booleanValue()) {
        StringBuilder multiCurrencyBreakdownCashBack = buildMultiCurrency(ticket, printProperties);
        if (multiCurrencyBreakdownCashBack != null) {
          parameters.put("additionalProperties", multiCurrencyBreakdownCashBack.toString());
        }
        else {
          StringBuilder multiCurrencyTotalAmount = buildMultiCurrencyTotalAmount(ticket, printProperties);
          if ((multiCurrencyTotalAmount != null) && (splitTicketId == null))
            parameters.put("additionalProperties", multiCurrencyTotalAmount.toString());
        }
      }
    }
    if (showHeaderLogo)
    {

      ImageIcon storeLogo = store.getStoreLogo();
      if (storeLogo != null) {
        parameters.put("storeLogoIcon", storeLogo.getImage());
      }
    }
    boolean isShowTipsSuggestion = store.getProperty(orderTypeId + "." + "receipt.show_tips_suggestion") == null ? false : Boolean.valueOf(store.getProperty(orderTypeId + "." + "receipt.show_tips_suggestion")).booleanValue();
    
    if (isShowTipsSuggestion) {
      parameters.put("showTips", tipsCalculation(ticket));
    }
    
    boolean isShowTipsBlock = store.getProperty(orderTypeId + "." + "receipt.show_tips_block") == null ? false : Boolean.valueOf(store.getProperty(orderTypeId + "." + "receipt.show_tips_block")).booleanValue();
    parameters.put("showTipsBlock", Boolean.valueOf(isShowTipsBlock));
    
    boolean isShowTaxBreakdown = store.getProperty(orderTypeId + "." + "receipt.show_tax_breakdown") == null ? false : Boolean.valueOf(store.getProperty(orderTypeId + "." + "receipt.show_tax_breakdown")).booleanValue();
    parameters.put("isShowTaxBreakdown", Boolean.valueOf(isShowTaxBreakdown));
    if (isShowTaxBreakdown) {
      StringBuilder taxBreakdown = getTaxBreakdown(ticket);
      if (taxBreakdown != null) {
        parameters.put("taxBreakdownText", taxBreakdown.toString());
      }
    }
    
    return parameters;
  }
  
  private static void setPaginationProperty(HashMap parameters, String printerName) {
    if ((printerName != null) && (printerName.contains("PDF"))) {
      parameters.put("IS_IGNORE_PAGINATION", Boolean.valueOf(true));
    }
    else {
      parameters.put("IS_IGNORE_PAGINATION", Boolean.valueOf(false));
    }
  }
  
  private static String getReceiptParamValuesAsString(Map map, String info) {
    if (info == null)
      return "";
    ReceiptParam[] receiptParams = ReceiptParam.values();
    for (ReceiptParam receiptParam : receiptParams) {
      String paramName = receiptParam.getParamName();
      Object object = map.get(paramName);
      if ((object == null) || (StringUtils.isEmpty(object.toString())) || (object.toString().equals("[]"))) {
        String text = "<" + receiptParam.getParamName() + ">.*</" + receiptParam.getParamName() + ">";
        info = info.replaceAll("<br>" + text, "");
        info = info.replaceAll(text, "");
      }
      else {
        String val = object.toString();
        try {
          info = info.replaceAll("\\$" + paramName, val);
        } catch (Exception e) {
          info = info.replaceAll("\\$" + paramName, "\\" + val);
        }
      } }
    return info;
  }
  
  private static String getPropertyValue(Store store, String propertyName, String propLength) {
    int length = 1;
    try {
      length = Integer.valueOf(store.getProperty(propLength)).intValue();
    }
    catch (Exception localException) {}
    String propertyValue = "";
    if (length > 1) {
      for (int i = 0; i < length; i++)
      {
        propertyValue = propertyValue + (StringUtils.isEmpty(store.getProperty(propertyName + "." + (i + 1))) ? "" : store.getProperty(new StringBuilder().append(propertyName).append(".").append(i + 1).toString()));
      }
      
    }
    else {
      propertyValue = StringUtils.isEmpty(store.getProperty(propertyName + "." + 1)) ? "" : store.getProperty(propertyName + "." + 1);
    }
    return propertyValue;
  }
  
  private static StringBuilder buildTicketIdWithSplitNo(Ticket ticket, String splitTicketIdText) {
    StringBuilder topTicketIdBuilder = new StringBuilder();
    beginRow(topTicketIdBuilder);
    addColumn(topTicketIdBuilder, POSConstants.RECEIPT_REPORT_TICKET_NO_LABEL + ticket.getId());
    endRow(topTicketIdBuilder);
    if (!StringUtils.isEmpty(splitTicketIdText)) {
      beginRow(topTicketIdBuilder);
      addColumn(topTicketIdBuilder, splitTicketIdText);
      endRow(topTicketIdBuilder);
    }
    return topTicketIdBuilder;
  }
  
  private static void populateRefundProperties(PosTransaction t, Set<PosTransaction> transactions, HashMap map) {
    if (transactions == null)
      return;
    TicketPrintProperties printProperties = new TicketPrintProperties("*** REFUND RECEIPT ***", true, true, true);
    printProperties.setPrintCookingInstructions(false);
    double refundAmount = 0.0D;
    for (PosTransaction transaction : transactions) {
      if (((transaction instanceof RefundTransaction)) || (transaction.isVoided().booleanValue()))
        refundAmount += transaction.getAmount().doubleValue();
    }
    String refundText = "";
    if (t != null) {
      refundText = "<br>" + t.getPaymentType() + " VOID/REFUND " + CurrencyUtil.getCurrencySymbol() + "&nbsp;" + t.getAmount();
    }
    else {
      refundText = "<br>Total Void/Refund " + CurrencyUtil.getCurrencySymbol() + "&nbsp;" + NumberUtil.formatNumber(Double.valueOf(refundAmount));
    }
    map.put("additionalProperties", "<html><b>" + refundText + "</b></html>");
  }
  
  public static HashMap populatePurchaseOrderProperties(PurchaseOrder order, TicketPrintProperties printProperties, PosTransaction transaction) {
    Store store = StoreDAO.getRestaurant();
    double totalAmount = order.getTotalAmount().doubleValue();
    
    HashMap map = new HashMap();
    setPaginationProperty(map, Application.getPrinters().getReceiptPrinter());
    String currencySymbol = CurrencyUtil.getCurrencySymbol();
    ImageIcon logo = IconFactory.getIcon("/icons/", "header_logo.png");
    if (logo != null)
    {
      map.put("logo", logo.getImage());
    }
    
    map.put("currencySymbol", currencySymbol);
    map.put("nameText", "Vendor: ");
    map.put("addressText", "Address: ");
    map.put("slNoText", "Sl. No.");
    map.put("itemText", "Description");
    map.put("quantityText", "Quantity");
    map.put("priceText", "Unit Cost (" + CurrencyUtil.getCurrencySymbol() + ")");
    map.put("unitText", "Unit");
    map.put("subTotalText", "Amount (" + CurrencyUtil.getCurrencySymbol() + ")");
    
    map.put("signature1", "Signature");
    map.put("signature2", "Verified By");
    InventoryVendor vendor = order.getVendor();
    if (vendor != null) {
      String vendorName = vendor.getName();
      String vendorAddress = vendor.getAddress();
      String vendorMobile = vendor.getPhone();
      
      map.put("customerName", vendorName);
      String address = "";
      if ((vendorAddress != null) && (!vendorAddress.isEmpty())) {
        address = address + vendorAddress;
      }
      if ((vendorMobile != null) && (!vendorMobile.isEmpty())) {
        address = address + ", Cell:" + vendorMobile;
      }
      map.put("customerAddress", address);
    }
    
    map.put("receiptType", printProperties.getReceiptTypeName());
    map.put("showSubtotal", Boolean.valueOf(printProperties.isShowSubtotal()));
    map.put("showHeaderSeparator", Boolean.TRUE);
    map.put("showFooter", Boolean.valueOf(printProperties.isShowFooter()));
    
    map.put("reportDate", POSConstants.RECEIPT_REPORT_DATE_LABEL + DateUtil.formatFullDateAndTimeAsString(new Date()));
    
    StringBuilder ticketHeaderBuilder2 = buildOrderInfo(order, printProperties);
    
    map.put("ticketHeader2", ticketHeaderBuilder2.toString());
    map.put("ticketHeader3", "Purchase Order");
    
    map.put("headerLine1", store.getName());
    
    if (printProperties.isShowFooter()) {
      if (order.getDiscountAmount().doubleValue() > 0.0D) {
        map.put("discountAmount", NumberUtil.formatNumber(order.getDiscountAmount()));
      }
      
      if (order.getTaxAmount().doubleValue() > 0.0D) {
        map.put("taxAmount", NumberUtil.formatNumber(order.getTaxAmount()));
      }
      
      map.put("totalText", POSConstants.RECEIPT_REPORT_TOTAL_LABEL + currencySymbol);
      map.put("discountText", POSConstants.RECEIPT_REPORT_DISCOUNT_LABEL + currencySymbol);
      map.put("taxText", POSConstants.RECEIPT_REPORT_TAX_LABEL + currencySymbol);
      map.put("serviceChargeText", POSConstants.RECEIPT_REPORT_SERVICE_CHARGE_LABEL + currencySymbol);
      map.put("tipsText", POSConstants.RECEIPT_REPORT_TIPS_LABEL + currencySymbol);
      map.put("netAmountText", POSConstants.RECEIPT_REPORT_NETAMOUNT_LABEL + currencySymbol);
      map.put("paidAmountText", POSConstants.RECEIPT_REPORT_PAIDAMOUNT_LABEL + currencySymbol);
      map.put("dueAmountText", POSConstants.RECEIPT_REPORT_DUEAMOUNT_LABEL + currencySymbol);
      map.put("changeAmountText", POSConstants.RECEIPT_REPORT_CHANGEAMOUNT_LABEL + currencySymbol);
      
      map.put("netAmount", NumberUtil.formatNumber(Double.valueOf(totalAmount)));
      map.put("paidAmount", NumberUtil.formatNumber(order.getPaidAmount()));
      map.put("dueAmount", NumberUtil.formatNumber(order.getDueAmount()));
      map.put("grandSubtotal", NumberUtil.formatNumber(order.getSubtotalAmount()));
      map.put("footerMessage", store.getTicketFooterMessage());
      map.put("copyType", printProperties.getReceiptCopyType());
      
      if (transaction != null) {
        double changedAmount = transaction.getTenderAmount().doubleValue() - transaction.getAmount().doubleValue();
        if (changedAmount < 0.0D) {
          changedAmount = 0.0D;
        }
        map.put("changedAmount", NumberUtil.formatNumber(Double.valueOf(changedAmount)));
        
        if (transaction.isCard()) {
          map.put("cardPayment", Boolean.valueOf(true));
          
          if (StringUtils.isNotEmpty(transaction.getCardTrack())) {
            BankCardMagneticTrack track = BankCardMagneticTrack.from(transaction.getCardTrack());
            String string = transaction.getCardType();
            string = string + "<br/>APPROVAL: " + transaction.getCardAuthCode();
            try
            {
              string = string + "<br/>ACCT: " + getCardNumber(track);
              string = string + "<br/>EXP: " + track.getTrack1().getExpirationDate();
              string = string + "<br/>CARDHOLDER: " + track.getTrack1().getName();
            } catch (Exception e) {
              logger.equals(e);
            }
            
            map.put("approvalCode", string);
          }
          else {
            String string = "APPROVAL: " + transaction.getCardAuthCode();
            string = string + "<br/>Card processed in ext. device.";
            
            map.put("approvalCode", string);
          }
        }
      }
      
      String messageString = "<html>";
      messageString = messageString + "</html>";
      map.put("additionalProperties", messageString);
    }
    
    return map;
  }
  
  private static String tipsCalculation(Ticket ticket) {
    StringBuilder buildTips = new StringBuilder();
    buildTips.append("<html>");
    
    double totalAmount = ticket.getTotalAmountWithTips().doubleValue();
    
    double tenPercent = totalAmount * 10.0D / 100.0D;
    double fiftheenPercent = totalAmount * 15.0D / 100.0D;
    double twentyPercent = totalAmount * 20.0D / 100.0D;
    double twentyFivePercent = totalAmount * 25.0D / 100.0D;
    
    beginRow(buildTips);
    addColumn(buildTips, "<br>10% Tip is   " + 
      NumberUtil.formatNumber(Double.valueOf(tenPercent)) + "<br>" + "15% Tip is   " + NumberUtil.formatNumber(Double.valueOf(fiftheenPercent)) + "<br>" + "20% Tip is   " + NumberUtil.formatNumber(Double.valueOf(twentyPercent)) + "<br>" + "25% Tip is   " + NumberUtil.formatNumber(Double.valueOf(twentyFivePercent)));
    endRow(buildTips);
    
    buildTips.append("</html>");
    return buildTips.toString();
  }
  
  private static StringBuilder buildOrderInfo(PurchaseOrder order, TicketPrintProperties printProperties) {
    String orderNo = " ";
    if (order.getOrderId() != null) {
      orderNo = order.getOrderId();
    }
    StringBuilder ticketHeaderBuilder = new StringBuilder();
    ticketHeaderBuilder.append("<html>");
    
    InventoryLocation location = order.getInventoryLocation();
    
    String inventoryLocation = "";
    String locationAddress = "";
    
    if (location != null) {
      inventoryLocation = location.getName();
      locationAddress = location.getAddress();
    }
    
    if (order.getVarificationDate() != null) {
      beginRow(ticketHeaderBuilder);
      addColumn(ticketHeaderBuilder, "Date: " + DateUtil.formatFullDateAndTimeAsString(order.getVarificationDate()));
      endRow(ticketHeaderBuilder);
    }
    beginRow(ticketHeaderBuilder);
    addColumn(ticketHeaderBuilder, "Purchase Order #" + orderNo);
    endRow(ticketHeaderBuilder);
    
    if (StringUtils.isNotEmpty(inventoryLocation)) {
      beginRow(ticketHeaderBuilder);
      addColumn(ticketHeaderBuilder, "Inventory Location: " + inventoryLocation);
      endRow(ticketHeaderBuilder);
    }
    
    if (StringUtils.isNotEmpty(locationAddress)) {
      beginRow(ticketHeaderBuilder);
      addColumn(ticketHeaderBuilder, "Inventory Location Address: " + locationAddress);
      endRow(ticketHeaderBuilder);
    }
    
    ticketHeaderBuilder.append("</html>");
    return ticketHeaderBuilder;
  }
  


















  private static StringBuilder buildTicketHeader(Ticket ticket, TicketPrintProperties printProperties)
  {
    Terminal terminal = Application.getInstance().getTerminal();
    StringBuilder ticketHeaderBuilder = new StringBuilder();
    ticketHeaderBuilder.append("<html>");
    
    beginRow(ticketHeaderBuilder);
    
    OrderType orderType = ticket.getOrderType();
    
    if ((orderType.isDelivery().booleanValue()) || (orderType.isPickup().booleanValue())) {
      if (ticket.isCustomerWillPickup().booleanValue()) {
        addColumn(ticketHeaderBuilder, "*PICKUP*");
      }
      else {
        addColumn(ticketHeaderBuilder, "*DELIVERY*");
      }
    }
    else {
      addColumn(ticketHeaderBuilder, "*" + ticket.getOrderType() + "*");
    }
    endRow(ticketHeaderBuilder);
    
    beginRow(ticketHeaderBuilder);
    addColumn(ticketHeaderBuilder, POSConstants.RECEIPT_REPORT_TERMINAL_LABEL + Application.getInstance().getTerminal().getId());
    endRow(ticketHeaderBuilder);
    
    if (ticket.isSourceOnline()) {
      beginRow(ticketHeaderBuilder);
      addColumn(ticketHeaderBuilder, "Online Order Id#:" + ticket.getProperty("onlineOrderId"));
      endRow(ticketHeaderBuilder);
    }
    
    if ((orderType.isShowTableSelection().booleanValue()) || (orderType.isShowGuestSelection().booleanValue()))
    {
      String tableInfo = "";
      if (terminal.isShowTableNumber()) {
        List<Integer> tableNumbers = ticket.getTableNumbers();
        tableInfo = (String)((tableNumbers != null) && (tableNumbers.size() != 0) ? tableNumbers : "");
      }
      else {
        tableInfo = ticket.getTableNames();
      }
      beginRow(ticketHeaderBuilder);
      addColumn(ticketHeaderBuilder, POSConstants.RECEIPT_REPORT_TABLE_NO_LABEL + tableInfo + ", " + POSConstants.RECEIPT_REPORT_GUEST_NO_LABEL + ticket.getNumberOfGuests());
      endRow(ticketHeaderBuilder);
    }
    
    beginRow(ticketHeaderBuilder);
    addColumn(ticketHeaderBuilder, POSConstants.RECEIPT_REPORT_SERVER_LABEL + ticket.getOwner());
    endRow(ticketHeaderBuilder);
    
    beginRow(ticketHeaderBuilder);
    addColumn(ticketHeaderBuilder, POSConstants.RECEIPT_REPORT_DATE_LABEL + DateUtil.formatFullDateAndTimeAsString(new Date()));
    endRow(ticketHeaderBuilder);
    
    beginRow(ticketHeaderBuilder);
    addColumn(ticketHeaderBuilder, "");
    endRow(ticketHeaderBuilder);
    
    User driver = ticket.getAssignedDriver();
    if (driver != null) {
      beginRow(ticketHeaderBuilder);
      addColumn(ticketHeaderBuilder, "*Driver*");
      endRow(ticketHeaderBuilder);
      
      if (StringUtils.isNotEmpty(driver.getFullName())) {
        beginRow(ticketHeaderBuilder);
        addColumn(ticketHeaderBuilder, driver.getFullName());
        endRow(ticketHeaderBuilder);
      }
      
      beginRow(ticketHeaderBuilder);
      addColumn(ticketHeaderBuilder, "");
      endRow(ticketHeaderBuilder);
    }
    

    if ((orderType.isDelivery().booleanValue()) || (ticket.isSourceOnline()))
    {
      String customerName = ticket.getProperty("CUSTOMER_NAME");
      String customerLastName = ticket.getProperty("CUSTOMER_LAST_NAME");
      String customerMobile = ticket.getProperty("CUSTOMER_MOBILE");
      
      if (StringUtils.isNotEmpty(customerName)) {
        beginRow(ticketHeaderBuilder);
        addColumn(ticketHeaderBuilder, Messages.getString("ReceiptPrintService.9"));
        endRow(ticketHeaderBuilder);
        
        if (StringUtils.isNotEmpty(customerName)) {
          beginRow(ticketHeaderBuilder);
          if (customerLastName == null) {
            customerLastName = "";
          }
          addColumn(ticketHeaderBuilder, customerName + " " + customerLastName);
          endRow(ticketHeaderBuilder);
        }
        
        if ((!ticket.isCustomerWillPickup().booleanValue()) && (StringUtils.isNotEmpty(ticket.getDeliveryAddress()))) {
          beginRow(ticketHeaderBuilder);
          addColumn(ticketHeaderBuilder, ticket.getDeliveryAddress());
          endRow(ticketHeaderBuilder);
          
          if (StringUtils.isNotEmpty(ticket.getExtraDeliveryInfo())) {
            beginRow(ticketHeaderBuilder);
            addColumn(ticketHeaderBuilder, ticket.getExtraDeliveryInfo());
            endRow(ticketHeaderBuilder);
          }
        }
        else {
          beginRow(ticketHeaderBuilder);
          addColumn(ticketHeaderBuilder, ticket.getExtraDeliveryInfo());
          endRow(ticketHeaderBuilder);
        }
        
        if (StringUtils.isNotEmpty(customerMobile)) {
          beginRow(ticketHeaderBuilder);
          addColumn(ticketHeaderBuilder, "Tel: " + customerMobile);
          endRow(ticketHeaderBuilder);
        }
        
        if (ticket.getDeliveryDate() != null) {
          beginRow(ticketHeaderBuilder);
          addColumn(ticketHeaderBuilder, "Delivery: " + DateUtil.formatFullDateAndTimeAsString(ticket.getDeliveryDate()));
          endRow(ticketHeaderBuilder);
        }
      }
    }
    
    ticketHeaderBuilder.append("</html>");
    return ticketHeaderBuilder;
  }
  
  public static StringBuilder getTaxBreakdown(Ticket ticket) {
    StringBuilder taxBreakdownBuilder = new StringBuilder();
    taxBreakdownBuilder.append("<html><table>");
    Map<String, Double> taxProperties = new HashMap();
    List<TicketItem> ticketItems = ticket.getTicketItems();
    List<TicketItemTax> taxes = new ArrayList();
    for (Iterator localIterator1 = ticketItems.iterator(); localIterator1.hasNext();) { ticketItem = (TicketItem)localIterator1.next();
      for (TicketItemTax ticketItemTax : ticketItem.getTaxes()) {
        String key = ticketItemTax.getName();
        Double taxAmount = (Double)taxProperties.get(key);
        if (taxAmount == null) {
          taxAmount = Double.valueOf(0.0D);
        }
        taxAmount = Double.valueOf(taxAmount.doubleValue() + ticketItemTax.getTaxAmount().doubleValue());
        taxProperties.put(key, taxAmount);
        taxes.add(ticketItemTax);
      }
    }
    TicketItem ticketItem;
    if (taxProperties.isEmpty()) {
      return null;
    }
    String currencySymbol = CurrencyUtil.getCurrencySymbol();
    for (Object entry : taxProperties.entrySet()) {
      String taxValue = NumberUtil.formatNumber((Double)((Map.Entry)entry).getValue());
      String taxName = (String)((Map.Entry)entry).getKey();
      int numberOfSpace = 24;
      int length = (int)(Math.log10(((Double)((Map.Entry)entry).getValue()).doubleValue()) + 1.0D);
      if (length > 2) {
        numberOfSpace++;
      }
      else if (length > 1) {
        numberOfSpace += 2;
      }
      else {
        numberOfSpace += 3;
      }
      
      beginRow(taxBreakdownBuilder);
      addColumn(taxBreakdownBuilder, getHtmlText(taxName + currencySymbol, numberOfSpace, "right"));
      addColumn(taxBreakdownBuilder, getHtmlText(taxValue, numberOfSpace, "right"));
      endRow(taxBreakdownBuilder);
    }
    taxBreakdownBuilder.append("</table></html>");
    return taxBreakdownBuilder;
  }
  
  private static StringBuilder buildMultiCurrencyTotalAmount(Ticket ticket, TicketPrintProperties printProperties) {
    DecimalFormat decimalFormat = new DecimalFormat("0.00");
    
    StringBuilder currencyAmountBuilder = new StringBuilder();
    currencyAmountBuilder.append("<html><table>");
    
    String sep = "------------------------------------";
    
    beginRow(currencyAmountBuilder);
    addColumn(currencyAmountBuilder, "&nbsp;");
    addColumn(currencyAmountBuilder, "&nbsp;");
    addColumn(currencyAmountBuilder, "&nbsp;");
    endRow(currencyAmountBuilder);
    
    beginRow(currencyAmountBuilder);
    addColumn(currencyAmountBuilder, "<b>Currency breakdown</b>");
    endRow(currencyAmountBuilder);
    
    beginRow(currencyAmountBuilder);
    addColumn(currencyAmountBuilder, sep);
    endRow(currencyAmountBuilder);
    
    beginRow(currencyAmountBuilder);
    addColumn(currencyAmountBuilder, getHtmlText("", 10, "center"));
    addColumn(currencyAmountBuilder, getHtmlText("Net Amount", 10, "center"));
    

    addColumn(currencyAmountBuilder, getHtmlText("Due", 10, "center"));
    endRow(currencyAmountBuilder);
    
    beginRow(currencyAmountBuilder);
    addColumn(currencyAmountBuilder, sep);
    endRow(currencyAmountBuilder);
    
    int rowCount = 0;
    List<Currency> allCurrency = CurrencyUtil.getAllCurrency();
    if (allCurrency != null) {
      for (Currency currency : allCurrency) {
        if (currency != null)
        {

          String key = currency.getName();
          double rate = currency.getExchangeRate().doubleValue();
          beginRow(currencyAmountBuilder);
          addColumn(currencyAmountBuilder, getHtmlText(key, 10, "left"));
          addColumn(currencyAmountBuilder, getHtmlText(decimalFormat.format(ticket.getTotalAmountWithTips().doubleValue() * rate), 10, "right"));
          


          addColumn(currencyAmountBuilder, getHtmlText(decimalFormat.format(ticket.getDueAmount().doubleValue() * rate), 10, "right"));
          endRow(currencyAmountBuilder);
          rowCount++;
        }
      }
    }
    if (rowCount == 0) {
      return null;
    }
    currencyAmountBuilder.append("</table></html>");
    return currencyAmountBuilder;
  }
  
  private static StringBuilder buildMultiCurrency(Ticket ticket, TicketPrintProperties printProperties)
  {
    DecimalFormat decimalFormat = new DecimalFormat("0.000");
    
    StringBuilder currencyAmountBuilder = new StringBuilder();
    currencyAmountBuilder.append("<html><table>");
    
    String sep = "------------------------------------";
    
    beginRow(currencyAmountBuilder);
    addColumn(currencyAmountBuilder, "&nbsp;");
    addColumn(currencyAmountBuilder, "&nbsp;");
    addColumn(currencyAmountBuilder, "&nbsp;");
    endRow(currencyAmountBuilder);
    
    String groupSettleTickets = ticket.getProperty("GROUP_SETTLE_TICKETS");
    if (groupSettleTickets == null) {
      groupSettleTickets = "";
    }
    
    beginRow(currencyAmountBuilder);
    addColumn(currencyAmountBuilder, groupSettleTickets + "<b>Currency breakdown</b>");
    endRow(currencyAmountBuilder);
    
    beginRow(currencyAmountBuilder);
    addColumn(currencyAmountBuilder, sep);
    endRow(currencyAmountBuilder);
    
    beginRow(currencyAmountBuilder);
    addColumn(currencyAmountBuilder, getHtmlText("", 10, "center"));
    addColumn(currencyAmountBuilder, getHtmlText("Paid", 10, "center"));
    addColumn(currencyAmountBuilder, getHtmlText("Cashback", 10, "center"));
    endRow(currencyAmountBuilder);
    
    beginRow(currencyAmountBuilder);
    addColumn(currencyAmountBuilder, sep);
    endRow(currencyAmountBuilder);
    
    int rowCount = 0;
    for (Currency currency : CurrencyUtil.getAllCurrency())
      if (currency != null)
      {

        String key = currency.getName();
        
        String paidAmount = ticket.getProperty(key);
        String cashBackAmount = ticket.getProperty(key + "_CASH_BACK");
        
        if (paidAmount == null) {
          paidAmount = "0";
        }
        if (cashBackAmount == null) {
          cashBackAmount = "0";
        }
        Double paid = Double.valueOf(paidAmount);
        Double changeDue = Double.valueOf(cashBackAmount);
        if ((paid.doubleValue() != 0.0D) || (changeDue.doubleValue() != 0.0D))
        {

          beginRow(currencyAmountBuilder);
          addColumn(currencyAmountBuilder, getHtmlText(key, 10, "left"));
          addColumn(currencyAmountBuilder, getHtmlText(decimalFormat.format(paid), 10, "right"));
          addColumn(currencyAmountBuilder, getHtmlText(decimalFormat.format(changeDue), 10, "right"));
          endRow(currencyAmountBuilder);
          rowCount++;
        }
      }
    if (rowCount == 0) {
      return null;
    }
    currencyAmountBuilder.append("</table></html>");
    return currencyAmountBuilder;
  }
  
  private static StringBuilder buildPayments(Ticket ticket) {
    Set<PosTransaction> transactionList = ticket.getTransactions();
    if ((transactionList == null) || (transactionList.size() <= 1)) {
      return null;
    }
    ArrayList<PosTransaction> transactions = new ArrayList(transactionList);
    Collections.sort(transactions, new Comparator()
    {
      public int compare(PosTransaction o1, PosTransaction o2)
      {
        return o1.getTransactionTime().compareTo(o2.getTransactionTime());
      }
      
    });
    StringBuilder paymentsBuilder = new StringBuilder();
    paymentsBuilder.append("<html><table>");
    
    beginRow(paymentsBuilder);
    addColumn(paymentsBuilder, "&nbsp;");
    addColumn(paymentsBuilder, "&nbsp;");
    addColumn(paymentsBuilder, "&nbsp;");
    endRow(paymentsBuilder);
    
    beginRow(paymentsBuilder);
    addColumn(paymentsBuilder, "Payments:");
    endRow(paymentsBuilder);
    
    String tagStart = "";
    String tagEng = "";
    for (PosTransaction transaction : transactions) {
      beginRow(paymentsBuilder);
      tagStart = transaction.isVoided().booleanValue() ? "<strike>" : "";
      tagEng = transaction.isVoided().booleanValue() ? "</strike>" : "";
      addColumn(paymentsBuilder, tagStart + getHtmlText(transaction.getPaymentType(), 10, "left") + tagEng);
      addColumn(paymentsBuilder, tagStart + getHtmlText(transaction.getTipsAmount().doubleValue() > 0.0D ? NumberUtil.formatNumber(transaction.getTipsAmount()) : "", 10, "right") + tagEng);
      Double amount = transaction.getAmount();
      addColumn(paymentsBuilder, getHtmlText(new StringBuilder().append(tagStart).append(NumberUtil.formatNumberAcceptNegative(Double.valueOf((transaction instanceof RefundTransaction) ? -amount.doubleValue() : amount.doubleValue()))).toString(), 10, "right") + tagEng);
      endRow(paymentsBuilder);
    }
    
    paymentsBuilder.append("</table></html>");
    return paymentsBuilder;
  }
  
  public static String getHtmlText(String txt, int length, String align) {
    if (txt.length() > 30) {
      txt = txt.substring(0, 30);
    }
    
    if (align.equals("center")) {
      int space = (length - txt.length()) / 2;
      for (int i = 1; i < space; i++) {
        txt = "&nbsp;" + txt + "&nbsp;";
      }
    }
    else if (align.equals("right")) {
      int space = length - txt.length();
      for (int i = 1; i < space; i++) {
        txt = "&nbsp;" + txt;
      }
    }
    else if (align.equals("left")) {
      int space = length - txt.length();
      for (int i = 1; i < space; i++) {
        txt = txt + "&nbsp;";
      }
    }
    return txt;
  }
  
  public static JasperPrint createKitchenPrint(KitchenTicket ticket) throws Exception {
    HashMap map = new HashMap();
    
    map.put("cardPayment", Boolean.valueOf(true));
    map.put("showHeaderSeparator", Boolean.TRUE);
    map.put("showHeaderSeparator", Boolean.TRUE);
    KitchenTicketDataSource dataSource = new KitchenTicketDataSource(ticket);
    
    return createJasperPrint(ReportUtil.getReport("kitchen-receipt"), map, new JRTableModelDataSource(dataSource));
  }
  
  public static JasperPrint createKitchenPrint(String virtualPrinterName, KitchenTicket ticket, String deviceName) throws Exception {
    return createKitchenPrint(virtualPrinterName, ticket, deviceName, false);
  }
  
  public static JasperPrint createKitchenPrint(String virtualPrinterName, KitchenTicket ticket, String deviceName, boolean ignorePagination) throws Exception {
    TicketPrintProperties printProperties = new TicketPrintProperties(null, false, true, true);
    
    HashMap map = populateTicketProperties(ticket.getParentTicket(), printProperties, null, true);
    map.put("IS_IGNORE_PAGINATION", Boolean.valueOf(ignorePagination));
    map.put("showHeaderSeparator", Boolean.TRUE);
    if (containsVoidItemOnly(ticket)) {
      map.put("headerLine1", "*" + Messages.getString("VOID ITEMS") + "*");
    }
    String ticketType = ticket.getOrderType().toString();
    if (StringUtils.isNotEmpty(ticketType)) {
      ticketType = ticketType.replaceAll("_", " ");
    }
    OrderType orderType = ticket.getOrderType();
    if ((orderType.isDelivery().booleanValue()) || (orderType.isPickup().booleanValue())) {
      if (ticket.getParentTicket().isCustomerWillPickup().booleanValue()) {
        map.put("orderType", "*" + Messages.getString("PICKUP") + "*");
      }
      else {
        map.put("orderType", "*" + Messages.getString("DELIVERY") + "*");
      }
    }
    else {
      map.put("orderType", "* " + ticketType + " *");
    }
    
    map.put("printerName", "Printer Name : " + virtualPrinterName);
    KitchenTicketDataSource dataSource = new KitchenTicketDataSource(ticket);
    
    String reportName = "kitchen-receipt";
    
    if (Application.getInstance().getTerminal().isGroupByCatagoryKitReceipt().booleanValue()) {
      reportName = "kitchen-receipt-with-group";
    }
    return createJasperPrint(ReportUtil.getReport(reportName), map, new JRTableModelDataSource(dataSource));
  }
  
  private static boolean containsVoidItemOnly(KitchenTicket ticket) {
    for (KitchenTicketItem item : ticket.getTicketItems()) {
      if (!item.isVoided().booleanValue()) {
        return false;
      }
    }
    return true;
  }
  
  public static JasperPrint createKitchenVoidPrint(String virtualPrinterName, KitchenTicket ticket, String deviceName) throws Exception {
    HashMap map = new HashMap();
    
    map.put("headerLine1", Application.getInstance().getStore().getName());
    map.put("cardPayment", Boolean.valueOf(true));
    map.put("showHeaderSeparator", Boolean.TRUE);
    map.put("showHeaderSeparator", Boolean.TRUE);
    map.put(ReceiptParam.TICKET_ID.getParamName(), POSConstants.RECEIPT_REPORT_TICKET_NO_LABEL + ticket.getTicketId());
    if ((ticket.getTableNumbers() != null) && (ticket.getTableNumbers().size() > 0)) {
      map.put(ReceiptParam.TABLE_NO.getParamName(), POSConstants.RECEIPT_REPORT_TABLE_NO_LABEL + ticket.getTableNumbers());
    }
    
    if (StringUtils.isNotEmpty(ticket.getCustomerName())) {
      map.put("customer", Messages.getString("ReceiptPrintService.0") + ticket.getCustomerName());
    }
    
    map.put(ReceiptParam.SERVER_NAME.getParamName(), POSConstants.RECEIPT_REPORT_SERVER_LABEL + ticket.getServerName());
    
    map.put("reportDate", Messages.getString("ReceiptPrintService.119") + DateUtil.getReportDate());
    
    map.put("ticketHeader", "VOID ITEMS");
    String ticketType = ticket.getOrderType().toString();
    if (StringUtils.isNotEmpty(ticketType)) {
      ticketType = ticketType.replaceAll("_", " ");
    }
    map.put("orderType", "** VOID **");
    map.put("printerName", "Printer Name : " + virtualPrinterName);
    

    KitchenTicketDataSource dataSource = new KitchenTicketDataSource(ticket);
    
    String reportName = "kitchen-receipt";
    
    return createJasperPrint(ReportUtil.getReport(reportName), map, new JRTableModelDataSource(dataSource));
  }
  
  public static void printToKitchen(Ticket ticket) {
    printToKitchen(ticket, true);
  }
  
  public static void printToKitchen(Ticket ticket, boolean isFilterKitchenPrintedItems) {
    printToKitchen(ticket, isFilterKitchenPrintedItems, true);
  }
  
  public static void printToKitchen(Ticket ticket, boolean isFilterKitchenPrintedItems, boolean saveAndUpdateStatus) {
    try {
      List<KitchenTicket> kitchenTickets = KitchenTicket.fromTicket(ticket, isFilterKitchenPrintedItems);
      doPrintToKitchenAndSaveStatus(ticket, kitchenTickets);
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public static void printItemsToKitchen(Ticket ticket, List<TicketItem> ticketItems) {
    try {
      if ((ticket == null) && (ticketItems == null)) {
        return;
      }
      List<KitchenTicket> kitchenTickets = KitchenTicket.fromTicket(ticket, true, ticketItems);
      doPrintToKitchenAndSaveStatus(ticket, kitchenTickets);
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Successfully printed to kitchen.");
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private static void doPrintToKitchenAndSaveStatus(Ticket ticket, List<KitchenTicket> kitchenTickets) throws Exception, JRException {
    for (KitchenTicket kitchenTicket : kitchenTickets) {
      kitchenTicket.setParentTicket(ticket);
      Printer printer = kitchenTicket.getPrinter();
      String deviceName = printer.getDeviceName();
      
      JasperPrint jasperPrint = createKitchenPrint(printer.getVirtualPrinter().getName(), kitchenTicket, deviceName);
      
      jasperPrint.setName("FP_KitchenReceipt_" + ticket.getId() + "_" + kitchenTicket.getSequenceNumber());
      jasperPrint.setProperty("printerName", deviceName);
      
      printQuitely(jasperPrint);
      
      String stickerPrinter = PosPrinters.getStickerPrinter();
      if (!StringUtils.isEmpty(stickerPrinter))
      {

        doPrintKitchenStickers(kitchenTicket, stickerPrinter); }
    }
    TicketDAO.getInstance().saveKitchenPrintStatus(ticket, kitchenTickets);
  }
  
  public static void doPrintKitchenStickers(Ticket ticket) {
    try {
      stickerPrinter = PosPrinters.getStickerPrinter();
      if (StringUtils.isEmpty(stickerPrinter)) {
        return;
      }
      List<KitchenTicket> kitchenTickets = KitchenTicket.fromTicket(ticket, true);
      
      for (KitchenTicket kitchenTicket : kitchenTickets) {
        kitchenTicket.setParentTicket(ticket);
        doPrintKitchenStickers(kitchenTicket, stickerPrinter);
      }
    } catch (Exception e) { String stickerPrinter;
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  private static void doPrintKitchenStickers(KitchenTicket kitchenTicket, String deviceName) {
    try {
      List<KitchenTicketItem> ticketItems = kitchenTicket.getTicketItems();
      List<KitchenStickerModel.KitchenSticker> kitchenStickers = new ArrayList();
      
      for (Iterator iterator = ticketItems.iterator(); iterator.hasNext();) {
        KitchenTicketItem kitchenTicketItem = (KitchenTicketItem)iterator.next();
        if ((kitchenTicketItem.isPrintKitchenSticker().booleanValue()) && 
        

          (!kitchenTicketItem.isModifierItem()) && (!kitchenTicketItem.isCookingInstruction()))
        {

          int quantity = (int)Math.ceil(kitchenTicketItem.getQuantity().doubleValue());
          for (int i = 0; i < quantity; i++) {
            KitchenStickerModel.KitchenSticker kitchenSticker = new KitchenStickerModel.KitchenSticker();
            kitchenSticker.setToken(kitchenTicket.getTokenNo());
            kitchenSticker.setItemName(kitchenTicketItem.getMenuItemName());
            kitchenSticker.setModifiers(kitchenTicket.getModifiersForTicketItem(kitchenTicketItem.getTicketItemId()));
            kitchenSticker.setCookingInstructions(kitchenTicket.getCookingInstructionForTicketItem(kitchenTicketItem.getTicketItemId()));
            kitchenSticker.setTime("Time: " + DateUtil.formatSmall(new Date()));
            kitchenSticker.setOrderType(kitchenTicket.getOrderType().getName());
            kitchenSticker.setItemCount("Item: " + (i + 1) + " of " + quantity);
            kitchenStickers.add(kitchenSticker);
          }
        }
      }
      JasperPrint jasperPrint = printKitchenStickerItems(kitchenStickers);
      
      jasperPrint.setName("Kitchen_Sticker_" + kitchenTicket.getSequenceNumber());
      jasperPrint.setProperty("printerName", deviceName);
      
      printQuitely(jasperPrint);
    } catch (Exception e) {
      PosLog.error(ReceiptPrintService.class, e.getMessage(), e);
    }
  }
  
  public static JasperPrint getKitchenJasperPrint(Ticket ticket, boolean isFilterKitchenPrintedItems) {
    return getKitchenJasperPrint(ticket, isFilterKitchenPrintedItems, false);
  }
  
  public static JasperPrint getKitchenJasperPrint(Ticket ticket, boolean isFilterKitchenPrintedItems, boolean ignorePagination) {
    try {
      List<KitchenTicket> kitchenTickets = KitchenTicket.fromTicket(ticket, isFilterKitchenPrintedItems);
      
      Iterator localIterator = kitchenTickets.iterator(); if (localIterator.hasNext()) { KitchenTicket kitchenTicket = (KitchenTicket)localIterator.next();
        kitchenTicket.setParentTicket(ticket);
        Printer printer = kitchenTicket.getPrinter();
        String deviceName = printer.getDeviceName();
        
        JasperPrint jasperPrint = createKitchenPrint(printer.getVirtualPrinter().getName(), kitchenTicket, deviceName, ignorePagination);
        
        jasperPrint.setName("FP_KitchenReceipt_" + ticket.getId() + "_" + kitchenTicket.getSequenceNumber());
        jasperPrint.setProperty("printerName", deviceName);
        return jasperPrint;
      }
    } catch (Exception e) {
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
    return null;
  }
  
  public static void printVoidItemsToKitchen(Ticket ticket) {
    try {
      if (!ticket.getOrderType().isShouldPrintToKitchen().booleanValue())
        return;
      List<VoidItem> voidItems = new ArrayList();
      for (TicketItem ticketItem : ticket.getTicketItems()) {
        voidItem = ticketItem.getVoidItem();
        if (voidItem != null) {
          if (voidItem.getVoidedModifiers() != null) {
            voidItems.addAll(voidItem.getVoidedModifiers());
          }
          voidItems.add(voidItem);
        }
      }
      Object itemMap = new HashMap();
      Date serverTimestamp = StoreDAO.geServerTimestamp();
      for (voidItem = voidItems.iterator(); voidItem.hasNext();) { voidItem = (VoidItem)voidItem.next();
        if (!voidItem.isCooked())
        {

          printerGroup = voidItem.getPrinterGroup();
          List<Printer> printers = getPrinters(printerGroup);
          if (printers != null)
          {


            for (Printer printer : printers) {
              KitchenTicket kitchenTicket = (KitchenTicket)((Map)itemMap).get(printer);
              if (kitchenTicket == null) {
                kitchenTicket = new KitchenTicket();
                kitchenTicket.setPrinterGroup(printerGroup);
                kitchenTicket.setTicketId(ticket.getId());
                kitchenTicket.setTokenNo(ticket.getTokenNo());
                kitchenTicket.setCreateDate(serverTimestamp);
                kitchenTicket.setOrderType(ticket.getOrderType());
                
                if (ticket.getTableNumbers() != null) {
                  kitchenTicket.setTableNumbers(new ArrayList(ticket.getTableNumbers()));
                }
                
                kitchenTicket.setServerName(ticket.getOwner().getFirstName());
                kitchenTicket.setStatus(KitchenStatus.WAITING.name());
                
                if (StringUtils.isNotEmpty(ticket.getProperty("CUSTOMER_NAME"))) {
                  kitchenTicket.setCustomerName(ticket.getProperty("CUSTOMER_NAME"));
                }
                kitchenTicket.setPrinter(printer);
                ((Map)itemMap).put(printer, kitchenTicket);
              }
              KitchenTicketItem item = new KitchenTicketItem();
              item.setTicketItemId(voidItem.getId());
              item.setMenuItemCode(String.valueOf(voidItem.getMenuItemId()));
              item.setMenuItemName(voidItem.getMenuItemName());
              item.setQuantity(voidItem.getQuantity());
              item.setUnitName("");
              item.setMenuItemGroupName("VOID");
              
              item.setSortOrder(Integer.valueOf(10001));
              item.setStatus(KitchenStatus.VOID.name());
              item.setKitchenTicket(kitchenTicket);
              kitchenTicket.addToticketItems(item); } } } }
      VoidItem voidItem;
      PrinterGroup printerGroup;
      for (KitchenTicket kitchenTicket : ((Map)itemMap).values()) {
        Printer printer = kitchenTicket.getPrinter();
        String deviceName = printer.getDeviceName();
        JasperPrint jasperPrint = createKitchenVoidPrint(printer.getVirtualPrinter().getName(), kitchenTicket, deviceName);
        jasperPrint.setName("FP_Void_KitchenReceipt_" + ticket.getId() + "_" + kitchenTicket.getSequenceNumber());
        jasperPrint.setProperty("printerName", deviceName);
        printQuitely(jasperPrint);
      }
    }
    catch (Exception e) {
      VoidItem voidItem;
      POSMessageDialog.showError(Application.getPosWindow(), e.getMessage(), e);
    }
  }
  
  public static List<Printer> getPrinters(PrinterGroup printerGroup) {
    PosPrinters printers = PosPrinters.load();
    
    if (printerGroup == null) {
      printerGroup = PrinterGroupDAO.getInstance().getDefaultPrinterGroup();
    }
    List<Printer> printerAll = new ArrayList();
    if (printerGroup == null) {
      printerAll.add(printers.getDefaultKitchenPrinter());
      return printerAll;
    }
    List<String> printerNames = printerGroup.getPrinterNames();
    List<Printer> kitchenPrinters = printers.getKitchenPrinters();
    for (Printer printer : kitchenPrinters) {
      if (printerNames.contains(printer.getVirtualPrinter().getName())) {
        printerAll.add(printer);
      }
    }
    if ((printerAll.isEmpty()) && (PrintServiceUtil.getFallBackPrinter() != null)) {
      printerAll.add(PrintServiceUtil.getFallBackPrinter());
    }
    return printerAll;
  }
  
  public static void printQuitely(JasperPrint jasperPrint) throws JRException {
    try {
      JRPrintServiceExporter exporter = new JRPrintServiceExporter();
      exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
      exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE, PrintServiceUtil.getPrintServiceForPrinter(jasperPrint.getProperty("printerName")));
      exporter.exportReport();
    } catch (Exception x) {
      if ((x == null) || (!(x.getCause() instanceof PrinterAbortException)))
      {


        String msg = "No print selected\n";
        logger.error(msg + x);
      }
    }
  }
  
  private static String getCardNumber(BankCardMagneticTrack track) {
    String no = "";
    try
    {
      if (track.getTrack1().hasPrimaryAccountNumber()) {
        no = track.getTrack1().getPrimaryAccountNumber().getAccountNumber();
        no = "************" + no.substring(12);
      }
      else if (track.getTrack2().hasPrimaryAccountNumber()) {
        no = track.getTrack2().getPrimaryAccountNumber().getAccountNumber();
        no = "************" + no.substring(12);
      }
    } catch (Exception e) {
      logger.error(e);
    }
    
    return no;
  }
  
  private static String getCardInformation(PosTransaction transaction) {
    String string = "<br/>CARD INFO: ------------------------";
    string = string + "<br/>PROCESS: " + transaction.getCardReader();
    string = string + "<br/> TYPE: " + transaction.getCardType();
    try {
      String cardNumber = transaction.getCardNumber();
      if (transaction.getCardNumber() != null) {
        string = string + "<br/> ACCT: **** **** **** " + cardNumber.substring(cardNumber.length() - 4, cardNumber.length());
      }
      if (transaction.getCardHolderName() != null) {
        string = string + "<br/> CARDHOLDER: " + transaction.getCardHolderName();
      }
      if (transaction.getCardTransactionId() != null) {
        string = string + "<br/> TRANS ID: " + transaction.getCardTransactionId();
      }
      string = string + "<br/> APPROVAL: " + transaction.getCardAuthCode();
    } catch (Exception e) {
      logger.equals(e);
    }
    return string;
  }
  
  private static String getCustBlnceInformation(PosTransaction transaction) {
    CustomerAccountTransaction custBlnceTrans = (CustomerAccountTransaction)transaction;
    String string = "";
    try {
      Customer customer = CustomerDAO.getInstance().findById(custBlnceTrans.getCustomerId());
      if (customer == null) {
        return string;
      }
      string = string + "<br/>PAY FROM CUSTOMER BALANCE: ------------------------";
      string = string + "<br/>CUSTOMER ID: " + customer.getId();
      string = string + "<br/>CUSTOEMER NAME: " + customer.getName();
      string = string + "<br/>AMOUNT: " + CurrencyUtil.getCurrencySymbol() + transaction.getAmount();
      string = string + "<br/>BALANCE: " + CurrencyUtil.getCurrencySymbol() + customer.getBalance();
    } catch (Exception e) {
      logger.equals(e);
    }
    return string;
  }
  

  public static void printCloudTicket(Ticket ticket, Terminal terminal, PosTransaction transaction)
  {
    try
    {
      TicketPrintProperties printProperties = new TicketPrintProperties(null, false, true, true);
      printProperties.setPrintCookingInstructions(false);
      map = populateTicketProperties(ticket, printProperties, transaction);
      
      List<TerminalPrinters> terminalPrinters = TerminalPrintersDAO.getInstance().findTerminalPrinters(terminal);
      List<Printer> activeReceiptPrinters = new ArrayList();
      for (TerminalPrinters terminalPrinters2 : terminalPrinters) {
        int printerType = terminalPrinters2.getVirtualPrinter().getType().intValue();
        if (printerType == 1) {
          Printer printer = new Printer(terminalPrinters2.getVirtualPrinter(), terminalPrinters2.getPrinterName());
          activeReceiptPrinters.add(printer);
        }
      }
      if ((activeReceiptPrinters == null) || (activeReceiptPrinters.isEmpty())) {
        jasperPrint = createPrint(ticket, map, null);
        ((JasperPrint)jasperPrint).setName("ORDER-" + ticket.getId());
        ((JasperPrint)jasperPrint).setProperty("printerName", Application.getPrinters().getReceiptPrinter());
        printQuitely((JasperPrint)jasperPrint);
      }
      else {
        for (jasperPrint = activeReceiptPrinters.iterator(); ((Iterator)jasperPrint).hasNext();) { Printer activeReceiptPrinter = (Printer)((Iterator)jasperPrint).next();
          JasperPrint jasperPrint = createPrint(ticket, map, null);
          jasperPrint.setName("ORDER-" + ticket.getId() + activeReceiptPrinter.getDeviceName());
          jasperPrint.setProperty("printerName", activeReceiptPrinter.getDeviceName());
          printQuitely(jasperPrint);
        }
      } } catch (Exception e) { HashMap map;
      Object jasperPrint;
      logger.error(POSConstants.PRINT_ERROR, e);
    }
  }
}
