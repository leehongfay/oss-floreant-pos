package com.floreantpos.report;

import com.floreantpos.main.Application;
import com.floreantpos.model.KitchenTicket;
import com.floreantpos.model.KitchenTicketItem;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Store;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.util.NumberUtil;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.commons.lang.StringUtils;


















public class KitchenTicketDataSource
  extends AbstractReportDataSource
{
  private Store store;
  
  public KitchenTicketDataSource()
  {
    super(new String[] { "courseIcon", "groupName", "itemNo", "itemName", "itemQty" });
  }
  
  public KitchenTicketDataSource(KitchenTicket ticket) {
    super(new String[] { "courseIcon", "groupId", "groupName", "itemNo", "itemName", "itemQty", "colorCode", "isVoidItem" });
    store = Application.getInstance().getStore();
    setTicket(ticket);
  }
  
  private void setTicket(KitchenTicket kitchenTicket) {
    OrderType orderType = kitchenTicket.getOrderType();
    if ((orderType != null) && (!orderType.isAllowSeatBasedOrder().booleanValue())) {
      Terminal terminal = Application.getInstance().getTerminal();
      if ((terminal != null) && (terminal.isGroupByCatagoryKitReceipt().booleanValue())) {
        Collections.sort(kitchenTicket.getTicketItems(), new Comparator() {
          public int compare(KitchenTicketItem o1, KitchenTicketItem o2) {
            return o1.getMenuItemGroupName().compareTo(o2.getMenuItemGroupName());
          }
        });
      }
    }
    




    setRows(kitchenTicket.getTicketItems());
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    KitchenTicketItem item = (KitchenTicketItem)rows.get(rowIndex);
    
    switch (columnIndex)
    {
    case 0: 
      return item.getCourseIcon();
    case 1: 
      return item.getMenuItemGroupName();
    
    case 2: 
      return item.getMenuItemGroupName();
    
    case 3: 
      return item.getMenuItemCode();
    
    case 4: 
      return item.getMenuItemNameDisplay();
    
    case 5: 
      return NumberUtil.trimDecilamIfNotNeeded(item.getQuantity());
    
    case 6: 
      Ticket ticket = item.getKitchenTicket().getParentTicket();
      String orderTypeId = "";
      if ((ticket != null) && (ticket.getOrderType() != null)) {
        orderTypeId = ticket.getOrderType().getId();
        if (StringUtils.isNotEmpty(orderTypeId)) {
          orderTypeId = orderTypeId + ".";
        }
      }
      if (item.isModifierItem()) {
        return store.getProperty(orderTypeId + "kitchen_ticket.modifier.color");
      }
      if (item.isCookingInstruction()) {
        return store.getProperty(orderTypeId + "kitchen_ticket.instruction.color");
      }
      if (item.getMenuItemName().contains("Seat**")) {
        return store.getProperty(orderTypeId + "kitchen_ticket.seat.color");
      }
      
      return "BLACK";
    case 7: 
      return item.isVoided();
    }
    return null;
  }
}
