package com.floreantpos.report;

import com.floreantpos.POSConstants;
import com.floreantpos.model.CreditCardTransaction;
import com.floreantpos.model.DebitCardTransaction;
import com.floreantpos.swing.ListTableModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

















public class SalesDetailedReport
{
  private Date fromDate;
  private Date toDate;
  private Date reportTime;
  int giftCertReturnCount;
  double giftCertReturnAmount;
  int giftCertChangeCount;
  double giftCertChangeAmount;
  int tipsCount;
  double chargedTips;
  double tipsPaid;
  double tipsDifferential;
  
  public SalesDetailedReport() {}
  
  private List<DrawerPullData> drawerPullDatas = new ArrayList();
  private Map<String, CreditCardData> creditCardDatas = new HashMap();
  
  public void addCreditCardData(CreditCardTransaction t) {
    CreditCardData data = (CreditCardData)creditCardDatas.get(t.getCardType());
    if (data == null) {
      data = new CreditCardData();
      data.setCardName(t.getCardType());
      creditCardDatas.put(t.getCardType(), data);
    }
    data.setSalesCount(data.getSalesCount() + 1);
    data.setSalesAmount(data.getSalesAmount() + t.getAmount().doubleValue());
    data.setNetSalesAmount(data.getNetSalesAmount() + t.getAmount().doubleValue());
  }
  
  public void addCreditCardData(DebitCardTransaction t)
  {
    CreditCardData data = (CreditCardData)creditCardDatas.get(t.getCardType());
    if (data == null) {
      data = new CreditCardData();
      data.setCardName(t.getCardType());
      creditCardDatas.put(t.getCardType(), data);
    }
    data.setSalesCount(data.getSalesCount() + 1);
    data.setSalesAmount(data.getSalesAmount() + t.getAmount().doubleValue());
    data.setNetSalesAmount(data.getNetSalesAmount() + t.getAmount().doubleValue());
  }
  


  public void addDrawerPullData(DrawerPullData data) { drawerPullDatas.add(data); }
  
  public static class DrawerPullData {
    private String drawerPullId;
    private int ticketCount;
    private double idealAmount;
    private double actualAmount;
    private double varinceAmount;
    
    public DrawerPullData() {}
    
    public double getActualAmount() { return actualAmount; }
    
    public void setActualAmount(double actualAmount)
    {
      this.actualAmount = actualAmount;
    }
    
    public String getDrawerPullId() {
      return drawerPullId;
    }
    
    public void setDrawerPullId(String drawerPullId) {
      this.drawerPullId = drawerPullId;
    }
    
    public double getIdealAmount() {
      return idealAmount;
    }
    
    public void setIdealAmount(double idealAmount) {
      this.idealAmount = idealAmount;
    }
    
    public int getTicketCount() {
      return ticketCount;
    }
    
    public void setTicketCount(int ticketCount) {
      this.ticketCount = ticketCount;
    }
    
    public double getVarinceAmount() {
      return varinceAmount;
    }
    
    public void setVarinceAmount(double varinceAmount) {
      this.varinceAmount = varinceAmount;
    }
  }
  
  public static class CreditCardData {
    String cardName;
    int salesCount;
    double salesAmount;
    int returnCount;
    double returnAmount;
    double netSalesAmount;
    double netTipsAmount;
    double percentage;
    
    public CreditCardData() {}
    
    public String getCardName() { return cardName; }
    
    public void setCardName(String cardName)
    {
      this.cardName = cardName;
    }
    
    public double getNetSalesAmount() {
      return netSalesAmount;
    }
    
    public void setNetSalesAmount(double netSalesAmount) {
      this.netSalesAmount = netSalesAmount;
    }
    
    public double getNetTipsAmount() {
      return netTipsAmount;
    }
    
    public void setNetTipsAmount(double netTipsAmount) {
      this.netTipsAmount = netTipsAmount;
    }
    
    public double getPercentage() {
      return percentage;
    }
    
    public void setPercentage(double percentage) {
      this.percentage = percentage;
    }
    
    public double getReturnAmount() {
      return returnAmount;
    }
    
    public void setReturnAmount(double returnAmount) {
      this.returnAmount = returnAmount;
    }
    
    public int getReturnCount() {
      return returnCount;
    }
    
    public void setReturnCount(int returnCount) {
      this.returnCount = returnCount;
    }
    
    public double getSalesAmount() {
      return salesAmount;
    }
    
    public void setSalesAmount(double salesAmount) {
      this.salesAmount = salesAmount;
    }
    
    public int getSalesCount() {
      return salesCount;
    }
    
    public void setSalesCount(int salesCount) {
      this.salesCount = salesCount;
    }
  }
  
  public Date getFromDate()
  {
    return fromDate;
  }
  
  public void setFromDate(Date fromDate) {
    this.fromDate = fromDate;
  }
  
  public Date getReportTime() {
    return reportTime;
  }
  
  public void setReportTime(Date reportTime) {
    this.reportTime = reportTime;
  }
  
  public Date getToDate() {
    return toDate;
  }
  
  public void setToDate(Date toDate) {
    this.toDate = toDate;
  }
  
  public DrawerPullDataTableModel getDrawerPullDataTableModel() {
    DrawerPullDataTableModel model = new DrawerPullDataTableModel();
    model.setRows(drawerPullDatas);
    
    return model;
  }
  
  public CreditCardDataTableModel getCreditCardDataTableModel() {
    CreditCardDataTableModel model = new CreditCardDataTableModel();
    ArrayList list = new ArrayList(creditCardDatas.values());
    model.setRows(list);
    
    return model;
  }
  
  public class DrawerPullDataTableModel extends ListTableModel {
    public DrawerPullDataTableModel() {
      setColumnNames(new String[] { "no", "count", "ideal", "actual", "variant" });
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      SalesDetailedReport.DrawerPullData data = (SalesDetailedReport.DrawerPullData)rows.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return POSConstants.DRAWER_PULL_ + SalesDetailedReport.DrawerPullData.access$000(data);
      
      case 1: 
        return Integer.valueOf(data.getTicketCount());
      
      case 2: 
        return Double.valueOf(SalesDetailedReport.DrawerPullData.access$100(data));
      
      case 3: 
        return Double.valueOf(SalesDetailedReport.DrawerPullData.access$200(data));
      
      case 4: 
        return Double.valueOf(data.getVarinceAmount());
      }
      
      return null;
    }
  }
  
  public class CreditCardDataTableModel extends ListTableModel
  {
    public CreditCardDataTableModel() {
      setColumnNames(new String[] { "creditCard", "salesCount", "salesAmount", "returnCount", "returnAmount", "netAmount", "netTipsAmount", "percentage" });
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      SalesDetailedReport.CreditCardData data = (SalesDetailedReport.CreditCardData)rows.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return cardName;
      
      case 1: 
        return Integer.valueOf(salesCount);
      
      case 2: 
        return Double.valueOf(salesAmount);
      
      case 3: 
        return Integer.valueOf(returnCount);
      
      case 4: 
        return Double.valueOf(returnAmount);
      case 5: 
        return Double.valueOf(netSalesAmount);
      case 6: 
        return Double.valueOf(netTipsAmount);
      case 7: 
        return Double.valueOf(percentage);
      }
      
      return null;
    }
  }
  
  public double getChargedTips()
  {
    return chargedTips;
  }
  
  public void setChargedTips(double chargedTips) {
    this.chargedTips = chargedTips;
  }
  
  public double getGiftCertChangeAmount() {
    return giftCertChangeAmount;
  }
  
  public void setGiftCertChangeAmount(double giftCertChangeAmount) {
    this.giftCertChangeAmount = giftCertChangeAmount;
  }
  
  public int getGiftCertChangeCount() {
    return giftCertChangeCount;
  }
  
  public void setGiftCertChangeCount(int giftCertChangeCount) {
    this.giftCertChangeCount = giftCertChangeCount;
  }
  
  public double getGiftCertReturnAmount() {
    return giftCertReturnAmount;
  }
  
  public void setGiftCertReturnAmount(double giftCertReturnAmount) {
    this.giftCertReturnAmount = giftCertReturnAmount;
  }
  
  public int getGiftCertReturnCount() {
    return giftCertReturnCount;
  }
  
  public void setGiftCertReturnCount(int giftCertReturnCount) {
    this.giftCertReturnCount = giftCertReturnCount;
  }
  
  public double getTipsDifferential() {
    return tipsDifferential;
  }
  
  public void setTipsDifferential(double tipsDifferential) {
    this.tipsDifferential = tipsDifferential;
  }
  
  public double getTipsPaid() {
    return tipsPaid;
  }
  
  public void setTipsPaid(double tipsPaid) {
    this.tipsPaid = tipsPaid;
  }
  
  public int getTipsCount() {
    return tipsCount;
  }
  
  public void setTipsCount(int tipsCount) {
    this.tipsCount = tipsCount;
  }
}
