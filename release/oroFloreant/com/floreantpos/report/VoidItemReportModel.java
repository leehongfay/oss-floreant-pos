package com.floreantpos.report;

import com.floreantpos.model.User;
import com.floreantpos.model.util.DateUtil;
import java.util.List;
import javax.swing.table.AbstractTableModel;

















public class VoidItemReportModel
  extends AbstractTableModel
{
  private String[] columnNames = { "date", "order", "qty", "itemName", "amount", "voidReason", "employee", "deletedBy" };
  
  private List<DeletedItem> items;
  
  public VoidItemReportModel() {}
  
  public int getRowCount()
  {
    if (items == null) {
      return 0;
    }
    
    return items.size();
  }
  
  public int getColumnCount() {
    return columnNames.length;
  }
  
  public String getColumnName(int column)
  {
    return columnNames[column];
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    DeletedItem item = (DeletedItem)items.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      return DateUtil.formatSmall(item.getVoidDate());
    case 1: 
      return item.getTicketId();
    
    case 2: 
      return Double.valueOf(item.getQuantity());
    
    case 3: 
      return item.getName();
    
    case 4: 
      return Double.valueOf(item.getTotal());
    
    case 5: 
      return item.getVoidReason();
    
    case 6: 
      if (item.getOwner() != null)
        return item.getOwner().getFullName();
      return "";
    
    case 7: 
      if (item.getVoidUser() != null)
        return item.getVoidUser().getFullName();
      return "";
    }
    
    return null;
  }
  
  public List<DeletedItem> getItems() {
    return items;
  }
  
  public void setItems(List<DeletedItem> items) {
    this.items = items;
  }
}
