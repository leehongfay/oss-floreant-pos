package com.floreantpos.report;

import com.floreantpos.model.CustomPaymentTransaction;
import com.floreantpos.model.Customer;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.util.NumberUtil;
import java.text.SimpleDateFormat;
import java.util.List;




















public class CardReportModel
  extends ListTableModel<PosTransaction>
{
  final String DATE_FORMAT = "MM-dd-yy hh:mm a";
  
  public CardReportModel(List<PosTransaction> datas) {
    super(new String[] { "ticketId", "paymentType", "cardType", "member", "cardReader", "date", "server", "authCode", "tips", "total", "tenderAmount", "transactionType", "customPaymentName", "customPaymentRef", "customPaymentFieldName", "captured", "authorizable", "cardNumber", "cardAuthCode", "cardTransactionID", "cardMarchantGateway", "giftCertNumber", "giftCertFaceValue", "giftCertPaidAmnt", "giftCertCashBackAmnt", "terminalID", "payoutReasonID", "payoutRecepientID" }, datas);
  }
  



  public Object getValueAt(int rowIndex, int columnIndex)
  {
    PosTransaction transaction = (PosTransaction)getRowData(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      return String.valueOf(transaction.getTicket().getId());
    
    case 1: 
      String paymentType = transaction.getPaymentType();
      if ((paymentType != null) && (paymentType.length() > 6)) {
        paymentType = paymentType.substring(0, 6);
      }
      return paymentType;
    
    case 2: 
      String cardType = transaction.getCardType();
      if ((cardType != null) && (cardType.length() > 6)) {
        cardType = cardType.substring(0, 6);
      } else
        cardType = "";
      return cardType;
    

    case 3: 
      if (transaction.getCustomerId() != null) {
        String customerId = transaction.getCustomerId();
        Customer customer = CustomerDAO.getInstance().get(customerId);
        if (customer != null) {
          return customer.getName();
        }
      }
      return "";
    
    case 4: 
      if ((transaction instanceof CustomPaymentTransaction)) {
        return ((CustomPaymentTransaction)transaction).getCustomPaymentName();
      }
      String cardReader = transaction.getCardReader();
      if ((cardReader != null) && (cardReader.length() > 3)) {
        cardReader = cardReader.substring(0, 3);
      }
      return cardReader;
    
    case 5: 
      SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yy hh:mm a");
      return String.valueOf(sdf.format(transaction.getTransactionTime()));
    
    case 6: 
      return transaction.getTicket().getOwner().getFullName();
    
    case 7: 
      return transaction.getCardAuthCode();
    
    case 8: 
      return NumberUtil.formatNumber(transaction.getTipsAmount());
    
    case 9: 
      return NumberUtil.formatNumber(transaction.getAmount());
    
    case 10: 
      return transaction.getTransactionType();
    case 11: 
      if ((transaction instanceof CustomPaymentTransaction)) {
        return ((CustomPaymentTransaction)transaction).getCustomPaymentName();
      }
      return null;
    case 12: 
      if ((transaction instanceof CustomPaymentTransaction)) {
        return ((CustomPaymentTransaction)transaction).getCustomPaymentRef();
      }
      return null;
    case 13: 
      if ((transaction instanceof CustomPaymentTransaction)) {
        return ((CustomPaymentTransaction)transaction).getCustomPaymentFieldName();
      }
      return null;
    case 14: 
      return transaction.isCaptured();
    case 15: 
      return transaction.isAuthorizable();
    case 16: 
      return transaction.getCardNumber();
    case 17: 
      return transaction.getCardAuthCode();
    case 18: 
      return transaction.getCardTransactionId();
    case 19: 
      return transaction.getCardMerchantGateway();
    
    case 20: 
      return transaction.getGiftCertNumber();
    case 21: 
      return transaction.getGiftCertFaceValue();
    case 22: 
      return transaction.getGiftCertPaidAmount();
    case 23: 
      return transaction.getGiftCertCashBackAmount();
    case 24: 
      return transaction.getTerminal().getId();
    case 25: 
      return "";
    case 26: 
      return "";
    }
    
    return null;
  }
}
