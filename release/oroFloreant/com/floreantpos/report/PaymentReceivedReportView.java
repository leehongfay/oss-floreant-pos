package com.floreantpos.report;

import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.Customer;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.PosTransactionDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.model.ext.PaperSize;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.services.report.ReportService;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;
import org.jdesktop.swingx.JXDatePicker;





















public class PaymentReceivedReportView
  extends TransparentPanel
{
  private JButton btnGo;
  private JXDatePicker fromDatePicker;
  private JXDatePicker toDatePicker;
  private JPanel reportPanel;
  private JPanel contentPane;
  private JComboBox jcbEmployee;
  
  public PaymentReceivedReportView()
  {
    setLayout(new BorderLayout());
    createUI();
  }
  
  private void viewReport() throws Exception {
    Date fromDate = fromDatePicker.getDate();
    Date toDate = toDatePicker.getDate();
    
    if (fromDate.after(toDate)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      return;
    }
    
    Calendar calendar = Calendar.getInstance();
    calendar.clear();
    
    Calendar calendar2 = Calendar.getInstance();
    calendar2.setTime(fromDate);
    
    calendar.set(1, calendar2.get(1));
    calendar.set(2, calendar2.get(2));
    calendar.set(5, calendar2.get(5));
    calendar.set(10, 0);
    calendar.set(12, 0);
    calendar.set(13, 0);
    fromDate = calendar.getTime();
    
    calendar.clear();
    calendar2.setTime(toDate);
    calendar.set(1, calendar2.get(1));
    calendar.set(2, calendar2.get(2));
    calendar.set(5, calendar2.get(5));
    calendar.set(10, 23);
    calendar.set(12, 59);
    calendar.set(13, 59);
    toDate = calendar.getTime();
    
    Object selectedItem = jcbEmployee.getSelectedItem();
    User user = null;
    if ((selectedItem instanceof User)) {
      user = (User)selectedItem;
    }
    
    List<PosTransaction> transactionList = PosTransactionDAO.getInstance().findCreditTransactions(fromDate, toDate, user);
    
    if ((transactionList == null) || (transactionList.size() < 1)) {
      reportPanel.removeAll();
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), "The document has no pages.");
      return;
    }
    JasperReport report = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("payment_receive_report"));
    
    Terminal terminal = Application.getInstance().getTerminal();
    HashMap properties = new HashMap();
    ReportUtil.populateRestaurantProperties(properties);
    properties.put("reportTitle", "Payment Received Report");
    properties.put("reportDate", "<b>Report on:</b> " + ReportService.formatFullDate(new Date()));
    properties.put("terminalName", terminal == null ? POSConstants.ALL : terminal.getName());
    properties.put("startDate", "<b>From:</b> " + DateUtil.formatFullDateAsString(fromDate));
    properties.put("endDate", "<b>To:</b> " + DateUtil.formatFullDateAsString(toDate));
    properties.put("totalMsg", "Total: ");
    properties.put("totalAmount", "Total(" + CurrencyUtil.getCurrencySymbol() + ")");
    PaymentReceivedReportModel reportModel = new PaymentReceivedReportModel();
    reportModel.setRows(transactionList);
    
    JasperPrint print = JasperFillManager.fillReport(report, properties, new JRTableModelDataSource(reportModel));
    
    JRViewer viewer = new JRViewer(print);
    reportPanel.removeAll();
    reportPanel.add(viewer);
    reportPanel.revalidate();
  }
  
  private void createUI() {
    fromDatePicker = UiUtil.getCurrentMonthStart();
    toDatePicker = UiUtil.getDeafultDate();
    toDatePicker.setDate(new Date());
    
    btnGo = new JButton();
    btnGo.setText(POSConstants.GO);
    btnGo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          PaymentReceivedReportView.this.viewReport();
        } catch (Exception e1) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.ERROR_MESSAGE, e1);
        }
        
      }
    });
    jcbEmployee = new JComboBox();
    
    UserDAO dao = new UserDAO();
    List<User> userTypes = dao.findActiveUsersForPayroll();
    
    Vector list = new Vector();
    list.add(POSConstants.ALL);
    list.addAll(userTypes);
    
    jcbEmployee.setModel(new DefaultComboBoxModel(list));
    
    setLayout(new BorderLayout());
    
    JPanel topPanel = new JPanel(new MigLayout());
    
    topPanel.add(new JLabel(POSConstants.EMPLOYEE + ":"));
    topPanel.add(jcbEmployee);
    topPanel.add(new JLabel(POSConstants.START_DATE + ":"));
    fromDatePicker.setFormats(new String[] { "dd MMM yy" });
    topPanel.add(fromDatePicker);
    topPanel.add(new JLabel(POSConstants.END_DATE + ":"));
    toDatePicker.setFormats(new String[] { "dd MMM yy" });
    topPanel.add(toDatePicker);
    topPanel.add(btnGo, "width 60!");
    add(topPanel, "North");
    
    JPanel centerPanel = new JPanel(new BorderLayout());
    centerPanel.setBorder(new EmptyBorder(0, 10, 10, 10));
    centerPanel.add(new JSeparator(), "North");
    
    reportPanel = new JPanel(new BorderLayout());
    centerPanel.add(reportPanel);
    
    add(centerPanel);
  }
  
  public JComponent $$$getRootComponent$$$() {
    return contentPane;
  }
  
  public class PaymentReceivedReportModel extends ListTableModel {
    public PaymentReceivedReportModel() {
      super();
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      PosTransaction data = (PosTransaction)rows.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        User user = data.getUser();
        return user.getFullName();
      
      case 1: 
        return DateUtil.formatFullDateAndTimeWithoutYearAsString(data.getTransactionTime());
      
      case 2: 
        return data.getId();
      
      case 3: 
        Ticket ticket = data.getTicket();
        if (ticket == null) {
          return "";
        }
        return data.getTicket().getId();
      
      case 4: 
        return data.getPaymentType();
      
      case 5: 
        return data.getAmount();
      
      case 6: 
        if (data.getTicket().getCustomer() != null) {
          return data.getTicket().getCustomer().getName();
        }
        
        return "";
      }
      
      
      return null;
    }
  }
}
