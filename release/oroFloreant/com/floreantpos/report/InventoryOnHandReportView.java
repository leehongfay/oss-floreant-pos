package com.floreantpos.report;

import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.Store;
import com.floreantpos.model.dao.InventoryLocationDAO;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.report.dao.ReportDAO;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;
import org.jdesktop.swingx.JXDatePicker;





public class InventoryOnHandReportView
  extends TransparentPanel
{
  private JTextField tfNameSku;
  private JComboBox<Object> cbGroup;
  private JComboBox<Object> cbSortBy;
  private JComboBox<Object> cbLocation;
  private JXDatePicker dpDate = new JXDatePicker();
  private JCheckBox chkShowInGroups;
  private JPanel reportPanel;
  private boolean showGroupSummary;
  
  public InventoryOnHandReportView()
  {
    this(true);
  }
  
  public InventoryOnHandReportView(boolean showGroupSummary) {
    this.showGroupSummary = showGroupSummary;
    initComponents();
    loadData();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    
    JPanel contentPane = new JPanel();
    contentPane.setLayout(new MigLayout("fill,hidemode 3", "", "[][][grow]"));
    add(contentPane);
    
    chkShowInGroups = new JCheckBox("Show in groups");
    JButton btnGo = new JButton();
    btnGo.setPreferredSize(PosUIManager.getSize(100, 0));
    btnGo.setText(POSConstants.GO);
    
    JLabel lblNameSku = new JLabel("Name / SKU: ");
    tfNameSku = new JTextField();
    tfNameSku.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        InventoryOnHandReportView.this.showReport();
      }
    });
    JLabel lblLocation = new JLabel("Location: ");
    cbLocation = new JComboBox();
    cbLocation.addItem("<All>");
    
    JLabel lblGroup = new JLabel("Groups:");
    cbGroup = new JComboBox();
    cbGroup.addItem("<All>");
    
    cbSortBy = new JComboBox();
    cbSortBy.addItem(POSConstants.NAME);
    cbSortBy.addItem(MenuItem.PROP_SKU.toUpperCase());
    cbSortBy.setSelectedIndex(0);
    
    JSeparator separator1 = new JSeparator();
    reportPanel = new JPanel();
    reportPanel.setLayout(new BorderLayout(0, 0));
    
    dpDate.setDate(new Date());
    
    btnGo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        InventoryOnHandReportView.this.showReport();
      }
      
    });
    contentPane.add(lblNameSku, "split ");
    contentPane.add(tfNameSku, "grow, gapright 20, w 200!");
    contentPane.add(lblLocation, "");
    contentPane.add(cbLocation, "gapright 20");
    
    contentPane.add(lblGroup);
    contentPane.add(cbGroup, "gapright 20");
    contentPane.add(cbSortBy);
    contentPane.add(new JLabel("Date"));
    contentPane.add(dpDate, "gapright 20");
    if (showGroupSummary)
      contentPane.add(chkShowInGroups);
    contentPane.add(btnGo);
    contentPane.add(separator1, "newline,growx, span");
    contentPane.add(reportPanel, "newline,grow,span");
    
    add(contentPane);
  }
  
  private void loadData() {
    List<InventoryLocation> locations = InventoryLocationDAO.getInstance().findAll();
    for (Iterator localIterator = locations.iterator(); localIterator.hasNext();) { location = (InventoryLocation)localIterator.next();
      cbLocation.addItem(location); }
    InventoryLocation location;
    Object groups = MenuGroupDAO.getInstance().findAll();
    for (MenuGroup group : (List)groups) {
      cbGroup.addItem(group);
    }
  }
  
  private void showReport() {
    try {
      String nameOrSku = tfNameSku.getText();
      Object selectedLocation = cbLocation.getSelectedItem();
      InventoryLocation location = null;
      if ((selectedLocation instanceof InventoryLocation)) {
        location = (InventoryLocation)selectedLocation;
      }
      
      Object selectedGroup = cbGroup.getSelectedItem();
      MenuGroup group = null;
      if ((selectedGroup instanceof MenuGroup)) {
        group = (MenuGroup)selectedGroup;
      }
      ReportDAO dao = new ReportDAO();
      List<InventoryStockData> stockDataList = dao.getInventoryOnHandReportData(nameOrSku, location, group, dpDate.getDate(), chkShowInGroups
        .isSelected());
      Iterator<InventoryStockData> iterator; if (stockDataList != null) {
        doSortReportDataList(stockDataList);
        for (iterator = stockDataList.iterator(); iterator.hasNext();) {
          InventoryStockData inventoryStockData = (InventoryStockData)iterator.next();
          if (!chkShowInGroups.isSelected()) {
            inventoryStockData.setMenuGroup(null);
          }
          if (inventoryStockData.getQuantityInHand() == 0.0D) {
            iterator.remove();
          }
        }
      }
      InventoryOnHandReportModel model = new InventoryOnHandReportModel();
      model.setRows(stockDataList);
      viewReport(model);
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    }
  }
  
  private void doSortReportDataList(List<InventoryStockData> stockDataList) {
    if (cbSortBy.getSelectedIndex() == 1) {
      Collections.sort(stockDataList, new Comparator()
      {
        public int compare(InventoryStockData o1, InventoryStockData o2)
        {
          return o1.getSku().compareTo(o2.getSku());
        }
        
      });
    } else {
      Collections.sort(stockDataList, new Comparator()
      {
        public int compare(InventoryStockData o1, InventoryStockData o2)
        {
          return o1.getMenuItemName().compareTo(o2.getMenuItemName());
        }
      });
    }
    if (chkShowInGroups.isSelected()) {
      Collections.sort(stockDataList, new Comparator()
      {
        public int compare(InventoryStockData o1, InventoryStockData o2)
        {
          if ((o1.getMenuGroup() == null) || (o2.getMenuGroup() == null))
            return 0;
          return o1.getMenuGroup().compareTo(o2.getMenuGroup());
        }
      });
    }
  }
  
  private void viewReport(InventoryOnHandReportModel model) {
    try {
      String reportName = showGroupSummary ? "inventoryNewReport.jasper" : "inventoryStockReport.jasper";
      JasperReport report = ReportParser.getReport("/reports/" + reportName);
      
      Map<String, Object> properties = new HashMap();
      Store store = Application.getInstance().getStore();
      properties.put("companyName", store.getName());
      properties.put("address", store.getAddressLine1());
      properties.put("city", store.getAddressLine2());
      properties.put("address3", store.getAddressLine3());
      properties.put("phone", store.getTelephone());
      properties.put("fax", store.getZipCode());
      properties.put("email", store.getAddressLine3());
      properties.put("isGroup", Boolean.valueOf(chkShowInGroups.isSelected()));
      
      properties.put("reportDate", new Date());
      properties.put("date", new SimpleDateFormat("dd MMM yyyy, hh:mm a").format(new Date()));
      
      JasperPrint print = JasperFillManager.fillReport(report, properties, new JRTableModelDataSource(model));
      
      reportPanel.removeAll();
      reportPanel.add(new JRViewer(print));
      reportPanel.revalidate();
    } catch (JRException e) {
      PosLog.error(getClass(), e);
    }
  }
}
