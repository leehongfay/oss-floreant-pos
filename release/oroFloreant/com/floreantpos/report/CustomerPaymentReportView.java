package com.floreantpos.report;

import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.Customer;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.dao.PosTransactionDAO;
import com.floreantpos.model.ext.PaperSize;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.services.report.ReportService;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;
import org.jdesktop.swingx.JXDatePicker;




















public class CustomerPaymentReportView
  extends TransparentPanel
{
  private JButton btnGo;
  private JXDatePicker fromDatePicker;
  private JXDatePicker toDatePicker;
  private JPanel reportPanel;
  private JPanel contentPane;
  private JComboBox jcbCustomer;
  
  public CustomerPaymentReportView()
  {
    setLayout(new BorderLayout());
    createUI();
  }
  
  private void viewReport() throws Exception {
    Date fromDate = fromDatePicker.getDate();
    Date toDate = toDatePicker.getDate();
    
    if (fromDate.after(toDate)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      return;
    }
    
    Calendar calendar = Calendar.getInstance();
    calendar.clear();
    
    Calendar calendar2 = Calendar.getInstance();
    calendar2.setTime(fromDate);
    
    calendar.set(1, calendar2.get(1));
    calendar.set(2, calendar2.get(2));
    calendar.set(5, calendar2.get(5));
    calendar.set(10, 0);
    calendar.set(12, 0);
    calendar.set(13, 0);
    fromDate = calendar.getTime();
    
    calendar.clear();
    calendar2.setTime(toDate);
    calendar.set(1, calendar2.get(1));
    calendar.set(2, calendar2.get(2));
    calendar.set(5, calendar2.get(5));
    calendar.set(10, 23);
    calendar.set(12, 59);
    calendar.set(13, 59);
    toDate = calendar.getTime();
    
    Object selectedItem = jcbCustomer.getSelectedItem();
    Customer customer = null;
    if ((selectedItem instanceof Customer)) {
      customer = (Customer)selectedItem;
    }
    
    List<CustomerAccountTransactionItem> transactionList = PosTransactionDAO.getInstance().findCustomerAccountTransactions(fromDate, toDate, customer);
    if ((transactionList == null) || (transactionList.size() < 1)) {
      reportPanel.removeAll();
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), "The document has no pages.");
      return;
    }
    JasperReport report = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("customer-payment-report"));
    
    Terminal terminal = Application.getInstance().getTerminal();
    HashMap properties = new HashMap();
    ReportUtil.populateRestaurantProperties(properties);
    properties.put("reportTitle", " Customer Payment Report");
    properties.put("reportDate", "<b>Report on:</b> " + ReportService.formatFullDate(new Date()));
    properties.put("terminalName", terminal == null ? POSConstants.ALL : terminal.getName());
    properties.put("startDate", "<b>From:</b> " + DateUtil.formatFullDateAsString(fromDate));
    properties.put("endDate", "<b>To:</b> " + DateUtil.formatFullDateAsString(toDate));
    properties.put("tips", "Tips (" + CurrencyUtil.getCurrencySymbol() + ")");
    properties.put("totalAmount", "Total (" + CurrencyUtil.getCurrencySymbol() + ")");
    CustomerPaymentReportModel reportModel = new CustomerPaymentReportModel();
    reportModel.setRows(transactionList);
    
    JasperPrint print = JasperFillManager.fillReport(report, properties, new JRTableModelDataSource(reportModel));
    
    JRViewer viewer = new JRViewer(print);
    reportPanel.removeAll();
    reportPanel.add(viewer);
    reportPanel.revalidate();
  }
  
  private void createUI()
  {
    fromDatePicker = UiUtil.getCurrentMonthStart();
    toDatePicker = UiUtil.getDeafultDate();
    toDatePicker.setDate(new Date());
    
    btnGo = new JButton();
    btnGo.setText(POSConstants.GO);
    btnGo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          CustomerPaymentReportView.this.viewReport();
        } catch (Exception e1) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.ERROR_MESSAGE, e1);
        }
        
      }
    });
    jcbCustomer = new JComboBox();
    
    CustomerDAO dao = new CustomerDAO();
    List<Customer> userTypes = dao.findAll();
    
    Vector list = new Vector();
    list.add(POSConstants.ALL);
    list.addAll(userTypes);
    
    jcbCustomer.setModel(new DefaultComboBoxModel(list));
    
    setLayout(new BorderLayout());
    
    JPanel topPanel = new JPanel(new MigLayout());
    
    topPanel.add(new JLabel("Customer:"));
    topPanel.add(jcbCustomer);
    topPanel.add(new JLabel(POSConstants.START_DATE + ":"));
    fromDatePicker.setFormats(new String[] { "dd MMM yy" });
    topPanel.add(fromDatePicker);
    topPanel.add(new JLabel(POSConstants.END_DATE + ":"));
    toDatePicker.setFormats(new String[] { "dd MMM yy" });
    topPanel.add(toDatePicker);
    topPanel.add(btnGo, "width 60!");
    add(topPanel, "North");
    
    JPanel centerPanel = new JPanel(new BorderLayout());
    centerPanel.setBorder(new EmptyBorder(0, 10, 10, 10));
    centerPanel.add(new JSeparator(), "North");
    
    reportPanel = new JPanel(new BorderLayout());
    centerPanel.add(reportPanel);
    
    add(centerPanel);
  }
  
  public JComponent $$$getRootComponent$$$() {
    return contentPane;
  }
  
  public class CustomerPaymentReportModel extends ListTableModel {
    public CustomerPaymentReportModel() {
      super();
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      CustomerPaymentReportView.CustomerAccountTransactionItem data = (CustomerPaymentReportView.CustomerAccountTransactionItem)rows.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return data.getCustomerName();
      
      case 1: 
        return data.getCustomerId();
      
      case 2: 
        return data.getTicketNo();
      
      case 3: 
        return data.getTransactionNo();
      case 4: 
        return DateUtil.formatReportDateAsString(data.getDate());
      
      case 5: 
        return data.getTips();
      
      case 6: 
        return data.getTotalAmount();
      }
      return null;
    }
  }
  
  public static class CustomerAccountTransactionItem {
    private String customerName;
    private String customerId;
    private String ticketNo;
    private String transactionNo;
    private Date date;
    private Double tips;
    private Double totalAmount;
    
    public CustomerAccountTransactionItem() {}
    
    public String getCustomerId() { return customerId; }
    
    public void setCustomerId(String id)
    {
      customerId = id;
    }
    
    public String getTicketNo() {
      return ticketNo;
    }
    
    public void setTicketNo(String ticketNo) {
      this.ticketNo = ticketNo;
    }
    
    public String getTransactionNo() {
      return transactionNo;
    }
    
    public void setTransactionNo(String transactionNo) {
      this.transactionNo = transactionNo;
    }
    
    public Date getDate() {
      return date;
    }
    
    public void setDate(Date date) {
      this.date = date;
    }
    
    public Double getTips() {
      return tips;
    }
    
    public void setTips(Double tips) {
      this.tips = tips;
    }
    
    public Double getTotalAmount() {
      return totalAmount;
    }
    
    public void setTotalAmount(Double totalAmount) {
      this.totalAmount = totalAmount;
    }
    
    public String getCustomerName() {
      return customerName;
    }
    
    public void setCustomerName(String customerName) {
      this.customerName = customerName;
    }
  }
}
