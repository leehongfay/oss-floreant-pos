package com.floreantpos.report;

import com.floreantpos.model.LabelItem;
import com.floreantpos.model.MenuItem;
import com.floreantpos.util.NumberUtil;
import java.util.List;















public class LabelPrinterTableModel
  extends AbstractReportDataSource
{
  public LabelPrinterTableModel(List<LabelItem> items)
  {
    super(new String[] { "barcodeNumber", "itemName", "memberPrice", "retailPrice" });
    setRows(items);
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    LabelItem item = (LabelItem)rows.get(rowIndex);
    
    MenuItem menuItem = item.getMenuItem();
    switch (columnIndex) {
    case 0: 
      return menuItem.getBarcode();
    case 1: 
      return "<html><body>" + menuItem.getName() + "</body></html>";
    case 2: 
      if (menuItem.getPrice() != null) {
        return "<html><body>Member Price: " + NumberUtil.getCurrencyFormat(menuItem.getPrice()) + "</body></html>";
      }
      return "";
    case 3: 
      if (menuItem.getRetailPrice() != null) {
        return "<html><body>Retail Price: " + NumberUtil.getCurrencyFormat(menuItem.getRetailPrice()) + "</body></html>";
      }
      return "";
    }
    
    return null;
  }
}
