package com.floreantpos.report;

import com.floreantpos.POSConstants;
import com.floreantpos.model.GiftCard;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.dao.GiftCardDAO;
import com.floreantpos.model.dao.PosTransactionDAO;
import com.floreantpos.model.ext.PaperSize;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.services.report.ReportService;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.swingx.JXDatePicker;



















public class GiftCardDetailReportView
  extends TransparentPanel
{
  private JButton btnGo;
  private JPanel reportPanel;
  private JXDatePicker fromDatePicker;
  private JXDatePicker toDatePicker;
  private JTextField tfCardNumber;
  
  public GiftCardDetailReportView()
  {
    setLayout(new BorderLayout());
    createUI();
  }
  
  private void viewReport() throws Exception {
    String cardNumber = tfCardNumber.getText();
    Date fromDate = fromDatePicker.getDate();
    Date toDate = toDatePicker.getDate();
    
    if (StringUtils.isEmpty(cardNumber)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please enter card number!");
      reportPanel.removeAll();
      return;
    }
    
    if (fromDate.after(toDate)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      reportPanel.removeAll();
      return;
    }
    GiftCard giftCard = GiftCardDAO.getInstance().get(cardNumber);
    if (giftCard == null) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Gift card not available!");
      reportPanel.removeAll();
      return;
    }
    
    List<PosTransaction> transactions = PosTransactionDAO.getInstance().findTransactionListByGiftCardNumber(cardNumber, fromDate, toDate);
    
    if (transactions.isEmpty()) {
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), "No information found!");
      reportPanel.removeAll();
      return;
    }
    
    JasperReport report = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("gift_card_detail_report"));
    
    HashMap properties = new HashMap();
    ReportUtil.populateRestaurantProperties(properties);
    properties.put("reportTitle", "Gift Card Detail Report");
    properties.put("reportTime", ReportService.formatFullDate(new Date()));
    properties.put("titleAmount", "Amount (" + CurrencyUtil.getCurrencySymbol() + ")");
    properties.put("cardNo", giftCard.getCardNumber() != null ? giftCard.getCardNumber() : "");
    properties.put("cardOwner", giftCard.getOwnerName() != null ? giftCard.getOwnerName() : "");
    properties.put("endBalance", giftCard.getBalance() != null ? CurrencyUtil.getCurrencySymbol() + " " + NumberUtil.formatNumber(giftCard.getBalance()) : "");
    properties.put("activeDate", ReportService.formatFullDate(giftCard.getActivationDate()));
    JasperPrint print = JasperFillManager.fillReport(report, properties, new JRTableModelDataSource(new GiftCardDetailModel(transactions)));
    
    JRViewer viewer = new JRViewer(print);
    reportPanel.removeAll();
    reportPanel.add(viewer);
    reportPanel.revalidate();
  }
  
  private void createUI() {
    JLabel lblCardNumber = new JLabel("Card number:");
    tfCardNumber = new JTextField(20);
    fromDatePicker = UiUtil.getCurrentMonthStart();
    toDatePicker = UiUtil.getCurrentMonthEnd();
    btnGo = new JButton();
    btnGo.setText(POSConstants.GO);
    btnGo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ev) {
        try {
          GiftCardDetailReportView.this.viewReport();
        } catch (Exception e) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.ERROR_MESSAGE, e);
        }
        
      }
    });
    setLayout(new BorderLayout());
    
    JPanel topPanel = new JPanel(new MigLayout());
    topPanel.add(lblCardNumber);
    topPanel.add(tfCardNumber, "grow");
    topPanel.add(new JLabel(POSConstants.START_DATE + ":"));
    fromDatePicker.setFormats(new String[] { "dd MMM yy" });
    topPanel.add(fromDatePicker);
    topPanel.add(new JLabel(POSConstants.END_DATE + ":"));
    toDatePicker.setFormats(new String[] { "dd MMM yy" });
    topPanel.add(toDatePicker);
    topPanel.add(btnGo, "width 60!");
    add(topPanel, "North");
    
    JPanel centerPanel = new JPanel(new BorderLayout());
    centerPanel.setBorder(new EmptyBorder(0, 10, 10, 10));
    centerPanel.add(new JSeparator(), "North");
    
    reportPanel = new JPanel(new BorderLayout());
    centerPanel.add(reportPanel);
    
    add(centerPanel);
  }
  
  public class GiftCardDetailModel extends ListTableModel {
    private String[] columnNames = { "txNumber", "ticketNo", "txTime", "txAmount" };
    
    public GiftCardDetailModel(List rows) {
      setColumnNames(columnNames);
      setRows(rows);
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      PosTransaction transaction = (PosTransaction)rows.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return transaction.getId();
      case 1: 
        if (StringUtils.isNotEmpty(transaction.getTicketId())) {
          return transaction.getTicketId();
        }
        return "";
      case 2: 
        if (transaction.getTransactionTime() != null)
          return DateUtil.formatReportDateAsString(transaction.getTransactionTime());
        return "";
      case 3: 
        return transaction.getAmount();
      }
      
      return null;
    }
  }
}
