package com.floreantpos.report;

import com.floreantpos.model.User;
import com.floreantpos.model.UserType;
import com.floreantpos.swing.ListTableModel;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;















public class PayrollReportModel
  extends ListTableModel
{
  SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd MMM yy hh:mm a");
  
  DecimalFormat decimalFormat = new DecimalFormat("0.00");
  
  public PayrollReportModel() {
    super(new String[] { "userID", "userName", "from", "to", "total", "role", "rate", "payment", "userSSN" });
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    PayrollReportData data = (PayrollReportData)rows.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      User user = data.getUser();
      User parentUser = user.getParentUser();
      if (parentUser != null) {
        return String.valueOf(parentUser.getId());
      }
      return String.valueOf(user.getId());
    
    case 1: 
      return data.getUser().getFirstName() + " " + data.getUser().getLastName();
    
    case 2: 
      return dateFormat2.format(data.getFrom());
    
    case 3: 
      return dateFormat2.format(data.getTo());
    
    case 4: 
      return Double.valueOf(data.getTotalHour());
    
    case 5: 
      return data.getUser().getType().getName();
    
    case 6: 
      return decimalFormat.format(data.getRate());
    
    case 7: 
      return Double.valueOf(data.getTotalPayment());
    
    case 8: 
      return String.valueOf(data.getUser().getId());
    }
    return null;
  }
}
