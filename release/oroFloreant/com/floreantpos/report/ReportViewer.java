package com.floreantpos.report;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.UserType;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.dao.UserTypeDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.ListComboBoxModel;
import com.floreantpos.swing.MultiSelectComboBox;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.swing.time.TimeComboBox;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXDatePicker;


























public class ReportViewer
  extends JPanel
{
  private JButton btnRefresh;
  private JButton btnClear;
  private JLabel lblGroup;
  private JComboBox<MenuGroup> cbGroup;
  private JComboBox cbTerminal;
  private JXDatePicker dpEndDate;
  private JXDatePicker dpStartDate;
  private JComboBox<Date> jcbStartTime;
  private JComboBox<Date> jcbEndTime;
  private JLabel lblFromDate;
  private JLabel lblToDate;
  private JLabel lblTerminal;
  private JCheckBox chkBoxFree;
  private JCheckBox chkShowInGroups;
  private JLabel lblUserType;
  private JComboBox cbUserType;
  private TransparentPanel reportSearchOptionPanel;
  private TransparentPanel reportConstraintPanel;
  private TransparentPanel reportPanel;
  private Report report;
  private MultiSelectComboBox<MenuGroup> cbMenuGroup;
  
  public ReportViewer()
  {
    initComponents();
  }
  
  public ReportViewer(Report report) {
    initComponents();
    
    cbMenuGroup.setItems(MenuGroupDAO.getInstance().findAll());
    
    TerminalDAO terminalDAO = new TerminalDAO();
    List drawerTerminals = new ArrayList();
    drawerTerminals.add(0, POSConstants.ALL);
    
    List<Terminal> terminals = terminalDAO.findAll();
    for (Terminal terminal : terminals) {
      if (terminal.isHasCashDrawer().booleanValue()) {
        drawerTerminals.add(terminal);
      }
    }
    cbTerminal.setModel(new ListComboBoxModel(drawerTerminals));
    setReport(report);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    
    reportSearchOptionPanel = new TransparentPanel(new BorderLayout());
    reportConstraintPanel = new TransparentPanel();
    reportConstraintPanel.setLayout(new MigLayout("", "[][][]"));
    
    lblGroup = new JLabel("Group");
    
    cbGroup = new JComboBox();
    
    lblTerminal = new JLabel(Messages.getString("ReportViewer.3"));
    
    cbTerminal = new JComboBox();
    cbTerminal.setPreferredSize(new Dimension(115, 0));
    
    lblFromDate = new JLabel(POSConstants.START_DATE + ":");
    dpStartDate = UiUtil.getCurrentMonthStart();
    
    TimeComboBox tcb = new TimeComboBox();
    jcbStartTime = tcb.getDefaultTimeComboBox();
    jcbEndTime = tcb.getDefaultTimeComboBox();
    
    lblToDate = new JLabel(POSConstants.END_DATE + ":");
    dpEndDate = UiUtil.getCurrentMonthEnd();
    
    chkBoxFree = new JCheckBox(Messages.getString("ReportViewer.6"));
    
    chkShowInGroups = new JCheckBox("Show In Groups");
    
    lblUserType = new JLabel(Messages.getString("ReportViewer.7"));
    cbUserType = new JComboBox();
    
    btnRefresh = new JButton(Messages.getString("ReportViewer.2"));
    reportPanel = new TransparentPanel();
    
    btnRefresh.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        ReportViewer.this.doRefreshReport(evt);
      }
    });
    btnClear = new JButton("CLEAR");
    reportPanel = new TransparentPanel();
    btnClear.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent evt)
      {
        doClear(evt);
      }
      
    });
    cbMenuGroup = new MultiSelectComboBox();
    reportConstraintPanel.add(lblGroup);
    reportConstraintPanel.add(cbMenuGroup, "split 8");
    reportConstraintPanel.add(lblTerminal);
    reportConstraintPanel.add(cbTerminal);
    reportConstraintPanel.add(lblFromDate);
    reportConstraintPanel.add(dpStartDate);
    reportConstraintPanel.add(jcbStartTime);
    reportConstraintPanel.add(lblToDate);
    reportConstraintPanel.add(dpEndDate);
    reportConstraintPanel.add(jcbEndTime, "wrap");
    reportConstraintPanel.add(new JLabel(""));
    reportConstraintPanel.add(chkBoxFree, "split 5");
    reportConstraintPanel.add(chkShowInGroups);
    reportConstraintPanel.add(new JLabel(""));
    reportConstraintPanel.add(btnRefresh);
    
    reportSearchOptionPanel.add(reportConstraintPanel, "North");
    reportSearchOptionPanel.add(new JSeparator(), "Center");
    
    reportPanel.setLayout(new BorderLayout());
    
    add(reportSearchOptionPanel, "North");
    add(reportPanel, "Center");
  }
  
  private void doRefreshReport(ActionEvent evt)
  {
    Date fromDate = dpStartDate.getDate();
    Date toDate = dpEndDate.getDate();
    
    Date startTime = (Date)jcbStartTime.getSelectedItem();
    Date endTime = (Date)jcbEndTime.getSelectedItem();
    
    fromDate = DateUtil.copyTime(fromDate, startTime);
    toDate = DateUtil.copyTime(toDate, endTime);
    
    if (fromDate.after(toDate)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      return;
    }
    try
    {
      reportPanel.removeAll();
      reportPanel.revalidate();
      
      if (report != null) {
        UserType userType = null;
        if ((cbUserType.getSelectedItem() instanceof UserType)) {
          userType = (UserType)cbUserType.getSelectedItem();
        }
        report.setUserType(userType);
        
        Terminal terminal = null;
        if ((cbTerminal.getSelectedItem() instanceof Terminal)) {
          terminal = (Terminal)cbTerminal.getSelectedItem();
        }
        
        report.setMenuGroups(cbMenuGroup.getSelectedItems());
        report.setTerminal(terminal);
        report.setStartDate(fromDate);
        report.setEndDate(toDate);
        report.setIncludeFreeItems(chkBoxFree.isSelected());
        report.setShowInGroups(chkShowInGroups.isSelected());
        report.refresh();
        
        if ((report != null) && (report.getViewer() != null)) {
          reportPanel.add(report.getViewer());
          reportPanel.revalidate();
        }
      }
    }
    catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  public void doClear(ActionEvent evt) {
    if (report != null)
    {
      cbUserType.setSelectedIndex(0);
      cbTerminal.setSelectedIndex(0);
    }
  }
  
  public Report getReport()
  {
    return report;
  }
  
  public void setReport(Report report) {
    this.report = report;
    
    if ((report instanceof VoidItemReport)) {
      reportConstraintPanel.removeAll();
      reportConstraintPanel.add(lblTerminal);
      reportConstraintPanel.add(cbTerminal);
      reportConstraintPanel.add(lblFromDate);
      reportConstraintPanel.add(dpStartDate);
      reportConstraintPanel.add(jcbStartTime);
      reportConstraintPanel.add(lblToDate);
      reportConstraintPanel.add(dpEndDate);
      reportConstraintPanel.add(jcbEndTime);
      reportConstraintPanel.add(btnRefresh);
    }
    if ((report instanceof OpenTicketSummaryReport)) {
      reportConstraintPanel.removeAll();
      
      UserTypeDAO dao = new UserTypeDAO();
      List<UserType> userTypes = dao.findAll();
      
      List list = new ArrayList();
      list.add(0, POSConstants.ALL);
      list.addAll(userTypes);
      
      cbUserType.setModel(new ListComboBoxModel(list));
      cbUserType.setPreferredSize(cbTerminal.getPreferredSize());
      reportConstraintPanel.add(lblUserType);
      reportConstraintPanel.add(cbUserType, "gap 0px 20px");
      reportConstraintPanel.add(lblTerminal);
      reportConstraintPanel.add(cbTerminal);
      reportConstraintPanel.add(new JLabel(""));
      reportConstraintPanel.add(btnRefresh);
      reportConstraintPanel.add(btnClear);
    }
  }
}
