package com.floreantpos.report;


public class InventoryItemWithLocation
{
  private String group;
  
  private String name;
  
  private String location;
  
  private String barcode;
  private double unitInHand;
  private double unitCost;
  private double total;
  private String unit;
  
  public InventoryItemWithLocation() {}
  
  public String getName()
  {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  





  public String getBarcode()
  {
    return barcode;
  }
  
  public void setBarcode(String barcode) {
    this.barcode = barcode;
  }
  
  public double getUnitInHand() {
    return unitInHand;
  }
  
  public void setUnitInHand(double unitInHand) {
    this.unitInHand = unitInHand;
  }
  
  public double getTotal() {
    return total;
  }
  
  public void setTotal(double total) {
    this.total = total;
  }
  





  public double getUnitCost()
  {
    return unitCost;
  }
  
  public String getGroup() {
    return group;
  }
  
  public void setGroup(String group) {
    this.group = group;
  }
  
  public String getLocation() {
    return location;
  }
  
  public void setLocation(String location) {
    this.location = location;
  }
  
  public void setUnitCost(double unitCost) {
    this.unitCost = unitCost;
  }
  
  public String getUnit() {
    return unit;
  }
  
  public void setUnit(String unit) {
    this.unit = unit;
  }
}
