package com.floreantpos.report;

import com.floreantpos.POSConstants;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.StoreSession;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.CashDrawerDAO;
import com.floreantpos.model.dao.StoreSessionDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.model.ext.PaperSize;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.print.PosPrintService;
import com.floreantpos.services.report.ReportService;
import com.floreantpos.swing.ListComboBoxModel;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;
import org.jdesktop.swingx.JXDatePicker;


























public class DailySummaryReportView
  extends TransparentPanel
{
  private JButton btnGo;
  private JComboBox cbTerminal;
  private JXDatePicker fromDatePicker;
  private JXDatePicker toDatePicker;
  private JPanel reportPanel;
  private JComboBox jcbEmployee;
  
  public DailySummaryReportView()
  {
    setLayout(new BorderLayout());
    createUI();
  }
  
  private void viewReport() throws Exception {
    Date fromDate = fromDatePicker.getDate();
    Date toDate = toDatePicker.getDate();
    
    if (fromDate.after(toDate)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      return;
    }
    
    User userType = null;
    if ((jcbEmployee.getSelectedItem() instanceof User)) {
      userType = (User)jcbEmployee.getSelectedItem();
    }
    
    Terminal terminal = null;
    if ((cbTerminal.getSelectedItem() instanceof Terminal)) {
      terminal = (Terminal)cbTerminal.getSelectedItem();
    }
    
    Calendar calendar = Calendar.getInstance();
    calendar.clear();
    
    Calendar calendar2 = Calendar.getInstance();
    calendar2.setTime(fromDate);
    
    calendar.set(1, calendar2.get(1));
    calendar.set(2, calendar2.get(2));
    calendar.set(5, calendar2.get(5));
    calendar.set(10, 0);
    calendar.set(12, 0);
    calendar.set(13, 0);
    fromDate = calendar.getTime();
    
    calendar.clear();
    calendar2.setTime(toDate);
    calendar.set(1, calendar2.get(1));
    calendar.set(2, calendar2.get(2));
    calendar.set(5, calendar2.get(5));
    calendar.set(10, 23);
    calendar.set(12, 59);
    calendar.set(13, 59);
    toDate = calendar.getTime();
    
    List<StoreSession> storeSessions = StoreSessionDAO.getInstance().findSessions(fromDate, toDate);
    List<DailySummaryReportData> rows = new ArrayList();
    for (StoreSession storeSession : storeSessions) {
      List<CashDrawer> cashDrawers = CashDrawerDAO.getInstance().findByStoreOperationData(storeSession);
      CashDrawer cashDrawersReportSummary = PosPrintService.populateCashDrawerReportSummary(cashDrawers);
      cashDrawersReportSummary.setStartTime(storeSession.getOpenTime());
      cashDrawersReportSummary.setAssignedBy(storeSession.getOpenedBy());
      cashDrawersReportSummary.setReportTime(storeSession.getCloseTime());
      cashDrawersReportSummary.setClosedBy(storeSession.getClosedBy());
      
      DailySummaryReportData data = new DailySummaryReportData();
      data.setPeriod(DateUtil.formatFullDateAsString(cashDrawersReportSummary.getStartTime()));
      String number = storeSession.getId();
      String lastFourDigit = number.substring(number.length() - 4, number.length());
      data.setSessionID(lastFourDigit);
      double balance = CashDrawerDAO.getInstance().getSumOfOpeningBalance(storeSession);
      data.setOpeningBalance(balance);
      data.setDeposit(cashDrawersReportSummary.getCashToDeposit().doubleValue());
      data.setOverOutage(cashDrawersReportSummary.getCashToDeposit().doubleValue() - cashDrawersReportSummary.getDrawerAccountable().doubleValue());
      data.setCash(cashDrawersReportSummary.getCashReceiptAmount().doubleValue());
      data.setTips(cashDrawersReportSummary.getTipsPaid().doubleValue());
      data.setOnAccount(cashDrawersReportSummary.getCashReceiptAmount().doubleValue() - balance);
      double customPayment = cashDrawersReportSummary.getCustomPaymentAmount() != null ? cashDrawersReportSummary.getCustomPaymentAmount().doubleValue() : 0.0D;
      data.setOtherPayment(customPayment + cashDrawersReportSummary.getGiftCardAddBalance() + cashDrawersReportSummary.getCreditCardReceiptAmount().doubleValue() + cashDrawersReportSummary.getDebitCardReceiptAmount().doubleValue());
      data.setPayInOut(0.0D + cashDrawersReportSummary.getPayOutAmount().doubleValue() * -1.0D);
      data.setVoids(cashDrawersReportSummary.getTotalVoid().doubleValue());
      int guestNumber = StoreSessionDAO.getInstance().getStoreSessionTotalGuest(storeSession).intValue();
      data.setTotalGuest(guestNumber);
      
      rows.add(data);
    }
    

    if (rows.isEmpty()) {
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), "no data");
      return;
    }
    JasperReport dailySummaryReport = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("daily_summary_subreport"));
    
    JasperReport report = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("daily_summary_report"));
    
    HashMap properties = new HashMap();
    ReportUtil.populateRestaurantProperties(properties);
    properties.put("reportTitle", POSConstants.DAILY_SUMMARY_REPORT);
    properties.put("reportTime", ReportService.formatFullDate(new Date()));
    properties.put("fromDay", ReportService.formatFullDate(fromDate));
    properties.put("toDay", ReportService.formatFullDate(toDate));
    properties.put(POSConstants.TYPE, POSConstants.BY_RANGE_ACTUAL);
    properties.put("type", userType == null ? POSConstants.ALL : userType.getFullName());
    properties.put("cntr", terminal == null ? POSConstants.ALL : terminal.getName());
    
    properties.put("dailySummaryReport", dailySummaryReport);
    properties.put("dailySummaryReportDatasource", new JRTableModelDataSource(new DailySummaryReportModel(rows)));
    
    JasperPrint print = JasperFillManager.fillReport(report, properties, new JREmptyDataSource());
    
    JRViewer viewer = new JRViewer(print);
    reportPanel.removeAll();
    reportPanel.add(viewer);
    reportPanel.revalidate();
  }
  
  private void createUI()
  {
    fromDatePicker = UiUtil.getCurrentMonthStart();
    toDatePicker = UiUtil.getCurrentMonthEnd();
    btnGo = new JButton();
    btnGo.setText(POSConstants.GO);
    btnGo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ev) {
        try {
          DailySummaryReportView.this.viewReport();
        } catch (Exception e) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.ERROR_MESSAGE, e);
        }
        
      }
    });
    jcbEmployee = new JComboBox();
    
    UserDAO dao = new UserDAO();
    List<User> userTypes = dao.findAll();
    
    Vector list = new Vector();
    list.add(POSConstants.ALL);
    list.addAll(userTypes);
    
    jcbEmployee.setModel(new DefaultComboBoxModel(list));
    
    cbTerminal = new JComboBox();
    TerminalDAO terminalDAO = new TerminalDAO();
    List terminals = terminalDAO.findAll();
    terminals.add(0, POSConstants.ALL);
    cbTerminal.setModel(new ListComboBoxModel(terminals));
    
    setLayout(new BorderLayout());
    
    JPanel topPanel = new JPanel(new MigLayout());
    
    topPanel.add(new JLabel(POSConstants.START_DATE + ":"));
    fromDatePicker.setFormats(new String[] { "dd MMM yy" });
    topPanel.add(fromDatePicker);
    topPanel.add(new JLabel(POSConstants.END_DATE + ":"));
    toDatePicker.setFormats(new String[] { "dd MMM yy" });
    topPanel.add(toDatePicker);
    topPanel.add(new JLabel(POSConstants.EMPLOYEE + ":"));
    topPanel.add(jcbEmployee);
    topPanel.add(new JLabel(POSConstants.TERMINAL_LABEL + ":"));
    topPanel.add(cbTerminal);
    topPanel.add(btnGo, "width 60!");
    add(topPanel, "North");
    
    JPanel centerPanel = new JPanel(new BorderLayout());
    centerPanel.setBorder(new EmptyBorder(0, 10, 10, 10));
    centerPanel.add(new JSeparator(), "North");
    
    reportPanel = new JPanel(new BorderLayout());
    centerPanel.add(reportPanel);
    
    add(centerPanel);
  }
  
  public static class DailySummaryReportData { private String period;
    private String sessionID;
    private double openingBalance;
    private double deposit;
    private double overOutage;
    private double cash;
    private double tips;
    private double onAccount;
    private double otherPayment;
    private double payInOut;
    private double voids;
    private int totalGuest;
    private double avgChk;
    
    public DailySummaryReportData() {}
    
    public String getPeriod() { return period; }
    
    public void setPeriod(String period)
    {
      this.period = period;
    }
    
    public double getDeposit() {
      return deposit;
    }
    
    public void setDeposit(double deposit) {
      this.deposit = deposit;
    }
    
    public double getOverOutage() {
      return overOutage;
    }
    
    public void setOverOutage(double overOutage) {
      this.overOutage = overOutage;
    }
    
    public double getCash() {
      return cash;
    }
    
    public void setCash(double cash) {
      this.cash = cash;
    }
    
    public double getOnAccount() {
      return onAccount;
    }
    
    public void setOnAccount(double onAccount) {
      this.onAccount = onAccount;
    }
    
    public double getOtherPayment() {
      return otherPayment;
    }
    
    public void setOtherPayment(double otherPayment) {
      this.otherPayment = otherPayment;
    }
    
    public double getPayInOut() {
      return payInOut;
    }
    
    public void setPayInOut(double payInOut) {
      this.payInOut = payInOut;
    }
    
    public double getVoids() {
      return voids;
    }
    
    public void setVoids(double voids) {
      this.voids = voids;
    }
    
    public int getTotalGuest() {
      return totalGuest;
    }
    
    public void setTotalGuest(int totalGuest) {
      this.totalGuest = totalGuest;
    }
    
    public double getAvgChk() {
      return avgChk;
    }
    
    public void setAvgChk(double avgChk) {
      this.avgChk = avgChk;
    }
    
    public String getSessionID() {
      return sessionID;
    }
    
    public void setSessionID(String sessionID) {
      this.sessionID = sessionID;
    }
    
    public double getOpeningBalance() {
      return openingBalance;
    }
    
    public void setOpeningBalance(double openingBalance) {
      this.openingBalance = openingBalance;
    }
    
    public double getTips() {
      return tips;
    }
    
    public void setTips(double tips) {
      this.tips = tips;
    }
  }
}
