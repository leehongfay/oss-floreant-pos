package com.floreantpos.report.dao;

import com.floreantpos.model.InventoryClosingBalance;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.InventoryStock;
import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.dao.InventoryClosingBalanceDAO;
import com.floreantpos.model.dao.InventoryStockDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.report.InventoryStockData;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DoubleType;

public class ReportDAO
{
  public ReportDAO() {}
  
  public List<InventoryStockData> getInventoryOnhandReprotDataWithGroup(String nameOrSku, InventoryLocation location, MenuGroup menuGroup)
  {
    InventoryStockDAO stockDAO = InventoryStockDAO.getInstance();
    Session session = null;
    try
    {
      session = stockDAO.createNewSession();
      
      String hql = "select stock.%s, stock.%s, stock.%s, stock.%s, location.%s, menuItem.%s, menuGroup.%s from InventoryStock stock, InventoryLocation location, MenuItem menuItem, MenuGroup menuGroup where stock.%s=location.%s and stock.%s = menuItem.%s and menuItem.%s=menuGroup.%s";
      









      hql = String.format(hql, new Object[] { InventoryStock.PROP_ITEM_NAME, InventoryStock.PROP_SKU, InventoryStock.PROP_QUANTITY_IN_HAND, InventoryStock.PROP_UNIT, InventoryLocation.PROP_NAME, MenuItem.PROP_COST, MenuGroup.PROP_NAME, InventoryStock.PROP_LOCATION_ID, InventoryLocation.PROP_ID, InventoryStock.PROP_MENU_ITEM_ID, MenuItem.PROP_ID, MenuItem.PROP_MENU_GROUP_ID, MenuGroup.PROP_ID });
      













      String str = nameOrSku.toLowerCase();
      if (StringUtils.isNotEmpty(str)) {
        String name = nameOrSku.toLowerCase();
        String sku = nameOrSku;
        hql = hql + " and (LOWER( stock.itemName ) like '%" + name + "%'" + " or stock.sku ='" + sku + "')";
      }
      if (location != null) {
        hql = hql + " and stock.locationId= '" + location.getId() + "'";
      }
      
      if (menuGroup != null) {
        hql = hql + " and " + MenuItem.PROP_MENU_GROUP_ID + "= '" + menuGroup.getId() + "'";
      }
      hql = hql + " order by menuGroup." + MenuGroup.PROP_NAME + ", menuItem." + MenuItem.PROP_NAME;
      
      String hql2 = "select stock.%s, stock.%s, stock.%s, stock.%s, location.%s, menuItem.%s from InventoryStock stock, InventoryLocation location, MenuItem menuItem where stock.%s=location.%s and stock.%s = menuItem.%s and (menuItem.%s is null)";
      







      hql2 = String.format(hql2, new Object[] { InventoryStock.PROP_ITEM_NAME, InventoryStock.PROP_SKU, InventoryStock.PROP_QUANTITY_IN_HAND, InventoryStock.PROP_UNIT, InventoryLocation.PROP_NAME, MenuItem.PROP_COST, InventoryStock.PROP_LOCATION_ID, InventoryLocation.PROP_ID, InventoryStock.PROP_MENU_ITEM_ID, MenuItem.PROP_ID, MenuItem.PROP_MENU_GROUP_ID });
      











      if (StringUtils.isNotEmpty(str)) {
        String name = nameOrSku.toLowerCase();
        String sku = nameOrSku;
        hql2 = hql2 + " and (LOWER( stock.itemName ) like '%" + name + "%'" + " or stock.sku ='" + sku + "')";
      }
      
      if (location != null) {
        hql2 = hql2 + " and stock.locationId= '" + location.getId() + "'";
      }
      
      if (menuGroup != null) {
        hql2 = hql2 + " and " + MenuItem.PROP_MENU_GROUP_ID + "= '" + menuGroup.getId() + "'";
      }
      hql2 = hql2 + " order by menuItem." + MenuItem.PROP_NAME;
      
      Query query = session.createQuery(hql);
      Query query2 = session.createQuery(hql2);
      
      List<Object[]> listItemWithParent = query.list();
      
      List<Object[]> listItemsWithoutParernt = query2.list();
      
      if ((listItemsWithoutParernt != null) && (!listItemsWithoutParernt.isEmpty())) {
        listItemWithParent.addAll(listItemsWithoutParernt);
      }
      
      List<InventoryStockData> inventoryStockDatas = new ArrayList();
      for (Object localObject1 = listItemWithParent.iterator(); ((Iterator)localObject1).hasNext();) { Object[] obj = (Object[])((Iterator)localObject1).next();
        InventoryStockData data = new InventoryStockData();
        data.setMenuItemName(obj[0] == null ? null : obj[0].toString());
        data.setSku(obj[1] == null ? null : obj[1].toString());
        data.setQuantityInHand(obj[2] == null ? 0.0D : new Double(obj[2].toString()).doubleValue());
        data.setUnit(obj[3] == null ? "" : obj[3].toString());
        data.setLocationName(obj[4] == null ? null : obj[4].toString());
        data.setMenuItemCost((obj[5] == null ? null : new Double(obj[5].toString())).doubleValue());
        
        data.setMenuGroup(obj.length <= 6 ? "No Group" : obj[6].toString());
        inventoryStockDatas.add(data);
      }
      
      return inventoryStockDatas;
    }
    finally {
      stockDAO.closeSession(session);
    }
  }
  
  public List<InventoryStockData> getInventoryOnhandReprotDataWithoutGroup(String nameOrSku, InventoryLocation location, MenuGroup menuGroup)
  {
    InventoryStockDAO stockDAO = InventoryStockDAO.getInstance();
    Session session = null;
    try
    {
      session = stockDAO.createNewSession();
      
      String hql = "select stock.%s, stock.%s, stock.%s, stock.%s, location.%s, menuItem.%s from InventoryStock stock, InventoryLocation location, MenuItem menuItem where stock.%s=location.%s and stock.%s = menuItem.%s";
      







      hql = String.format(hql, new Object[] { InventoryStock.PROP_ITEM_NAME, InventoryStock.PROP_SKU, InventoryStock.PROP_QUANTITY_IN_HAND, InventoryStock.PROP_UNIT, InventoryLocation.PROP_NAME, MenuItem.PROP_COST, InventoryStock.PROP_LOCATION_ID, InventoryLocation.PROP_ID, InventoryStock.PROP_MENU_ITEM_ID, MenuItem.PROP_ID, MenuItem.PROP_MENU_GROUP_ID });
      











      if (StringUtils.isNotEmpty(nameOrSku)) {
        String name = nameOrSku.toLowerCase();
        String sku = nameOrSku;
        hql = hql + " and (LOWER( stock.itemName ) like '%" + name + "%'" + " or stock.sku ='" + sku + "')";
      }
      if (location != null) {
        hql = hql + " and stock.locationId= '" + location.getId() + "'";
      }
      
      if (menuGroup != null) {
        hql = hql + " and " + MenuItem.PROP_MENU_GROUP_ID + "= '" + menuGroup.getId() + "'";
      }
      hql = hql + " order by menuItem." + MenuItem.PROP_NAME;
      Query query = session.createQuery(hql);
      
      List<Object[]> listItemsWithoutParernt = query.list();
      
      List<InventoryStockData> inventoryStockDatas = new ArrayList();
      for (Object localObject1 = listItemsWithoutParernt.iterator(); ((Iterator)localObject1).hasNext();) { Object[] obj = (Object[])((Iterator)localObject1).next();
        InventoryStockData data = new InventoryStockData();
        data.setMenuItemName(obj[0] == null ? null : obj[0].toString());
        data.setSku(obj[1] == null ? null : obj[1].toString());
        data.setQuantityInHand(obj[2] == null ? 0.0D : new Double(obj[2].toString()).doubleValue());
        data.setUnit(obj[3] == null ? "" : obj[3].toString());
        data.setLocationName(obj[4] == null ? null : obj[4].toString());
        data.setMenuItemCost((obj[5] == null ? null : new Double(obj[5].toString())).doubleValue());
        inventoryStockDatas.add(data);
      }
      
      return inventoryStockDatas;
    }
    finally {
      stockDAO.closeSession(session);
    }
  }
  
  public List<InventoryStockData> getInventoryOnHandReportData(String nameOrSku, InventoryLocation location, MenuGroup menuGroup, Date toDate, boolean grouping)
  {
    InventoryStockDAO stockDAO = InventoryStockDAO.getInstance();
    Session session = null;
    Criteria criteria = null;
    try
    {
      session = stockDAO.createNewSession();
      Map<String, InventoryStockData> itemMap = getInventoryItems(nameOrSku, menuGroup, session);
      List<String> menuItemsIds = new ArrayList(itemMap.keySet());
      if (menuItemsIds.size() <= 0) {
        return new ArrayList();
      }
      Object dataMap = new HashMap();
      Date lastClosingDate = InventoryClosingBalanceDAO.getInstance().getLastClosingDate(toDate);
      Iterator iterator; if (lastClosingDate != null) {
        lastClosingDate = DateUtil.startOfDay(lastClosingDate);
        criteria = session.createCriteria(InventoryClosingBalance.class);
        ProjectionList list = Projections.projectionList();
        list.add(Projections.property(InventoryClosingBalance.PROP_MENU_ITEM_ID), InventoryStockData.PROP_MENU_ITEM_ID);
        list.add(Projections.property(InventoryClosingBalance.PROP_UNIT), InventoryStockData.PROP_UNIT);
        list.add(Projections.property(InventoryClosingBalance.PROP_LOCATION_ID), InventoryStockData.PROP_LOCATION_ID);
        list.add(Projections.property(InventoryClosingBalance.PROP_BALANCE), InventoryStockData.PROP_QUANTITY_IN_HAND);
        criteria.setProjection(list);
        if (location != null) {
          criteria.add(Restrictions.eq(InventoryClosingBalance.PROP_LOCATION_ID, location.getId()));
        }
        criteria.add(Restrictions.in(InventoryClosingBalance.PROP_MENU_ITEM_ID, menuItemsIds));
        if (lastClosingDate != null) {
          criteria.add(Restrictions.ge(InventoryClosingBalance.PROP_CLOSING_DATE, lastClosingDate));
          criteria.add(Restrictions.lt(InventoryClosingBalance.PROP_CLOSING_DATE, DateUtil.endOfDay(lastClosingDate)));
        }
        criteria.setResultTransformer(Transformers.aliasToBean(InventoryStockData.class));
        
        List dataList = criteria.list();
        if ((dataList != null) && (dataList.size() > 0)) {
          for (iterator = dataList.iterator(); iterator.hasNext();) {
            InventoryStockData inventoryStockData = (InventoryStockData)iterator.next();
            ((Map)dataMap).put(generateKey(inventoryStockData), inventoryStockData);
          }
        }
      }
      Map<String, String> locationMap = populateInventoryTransactionsSummary(menuItemsIds, location, nameOrSku, menuGroup, toDate, session, (Map)dataMap, lastClosingDate);
      
      List stockDataList = new ArrayList(((Map)dataMap).values());
      for (Iterator iterator = stockDataList.iterator(); iterator.hasNext();) {
        InventoryStockData data = (InventoryStockData)iterator.next();
        InventoryStockData itemData = (InventoryStockData)itemMap.get(data.getMenuItemId());
        if ((itemData == null) || (data.getQuantityInHand() == 0.0D)) {
          iterator.remove();
        }
        else {
          data.setMenuItemName(itemData.getMenuItemName());
          data.setMenuGroup(itemData.getMenuGroup());
          data.setLocationName((String)locationMap.get(data.getLocationId()));
          data.setSku(itemData.getSku());
          data.setMenuItemCost(itemData.getMenuItemCost());
        } }
      return stockDataList;
    } finally {
      stockDAO.closeSession(session);
    }
  }
  
  private Map<String, String> populateInventoryTransactionsSummary(List<String> menuItemsIds, InventoryLocation location, String nameOrSku, MenuGroup menuGroup, Date date, Session session, Map<String, InventoryStockData> dataMap, Date lastClosingDate)
  {
    Criteria criteria = session.createCriteria(InventoryLocation.class);
    ProjectionList projList = Projections.projectionList();
    projList.add(Projections.property(InventoryLocation.PROP_ID), InventoryLocation.PROP_ID);
    projList.add(Projections.property(InventoryLocation.PROP_NAME), InventoryLocation.PROP_NAME);
    criteria.setProjection(projList);
    criteria.setResultTransformer(Transformers.aliasToBean(InventoryLocation.class));
    if (location != null) {
      criteria.add(Restrictions.eq(InventoryLocation.PROP_ID, location.getId()));
    }
    List locations = criteria.list();
    Map<String, String> locationMap = new HashMap();
    Iterator locIterator; if (locations != null) {
      for (locIterator = locations.iterator(); locIterator.hasNext();) {
        InventoryLocation locationObj = (InventoryLocation)locIterator.next();
        locationId = locationObj.getId();
        locationMap.put(locationId, locationObj.getName());
        criteria = session.createCriteria(InventoryTransaction.class);
        criteria.createAlias(InventoryTransaction.PROP_MENU_ITEM, "item");
        String alias = "item.";
        
        ProjectionList projections = Projections.projectionList();
        projections.add(Projections.property(alias + "id"), InventoryStockData.PROP_MENU_ITEM_ID);
        projections.add(Projections.property(InventoryTransaction.PROP_UNIT), InventoryStockData.PROP_UNIT);
        projections.add(Projections.sqlProjection("sum(quantity*tran_type) AS " + InventoryStockData.PROP_QUANTITY_IN_HAND, new String[] { InventoryStockData.PROP_QUANTITY_IN_HAND }, new org.hibernate.type.Type[] { new DoubleType() }));
        
        projections.add(Projections.groupProperty(InventoryTransaction.PROP_UNIT));
        projections.add(Projections.groupProperty("item.id"));
        criteria.setProjection(projections);
        
        if (lastClosingDate != null) {
          criteria.add(Restrictions.ge(InventoryTransaction.PROP_TRANSACTION_DATE, lastClosingDate));
        }
        criteria.add(Restrictions.le(InventoryTransaction.PROP_TRANSACTION_DATE, DateUtil.endOfDay(date)));
        if (locationId != null) {
          criteria.add(Restrictions.or(Restrictions.eq(InventoryTransaction.PROP_FROM_LOCATION_ID, locationId), 
            Restrictions.eq(InventoryTransaction.PROP_TO_LOCATION_ID, locationId)));
        }
        criteria.add(Restrictions.in("item." + MenuItem.PROP_ID, menuItemsIds));
        criteria.setResultTransformer(Transformers.aliasToBean(InventoryStockData.class));
        List dataList = criteria.list();
        if ((dataList != null) && (dataList.size() > 0))
          for (iterator = dataList.iterator(); iterator.hasNext();) {
            InventoryStockData tData = (InventoryStockData)iterator.next();
            tData.setLocationId(locationId);
            String key = generateKey(tData);
            InventoryStockData stockData = (InventoryStockData)dataMap.get(key);
            if (stockData != null) {
              stockData.setQuantityInHand(stockData.getQuantityInHand() + tData.getQuantityInHand());
            }
            else
              dataMap.put(key, tData);
          }
      }
    }
    String locationId;
    Iterator iterator;
    return locationMap;
  }
  
  private String generateKey(InventoryStockData tData) {
    return tData.getMenuItemId() + tData.getUnit() + tData.getLocationId();
  }
  
  private Map<String, InventoryStockData> getInventoryItems(String nameOrSku, MenuGroup menuGroup, Session session) {
    Criteria criteria = session.createCriteria(MenuItem.class);
    ProjectionList list = Projections.projectionList();
    list.add(Projections.property(MenuItem.PROP_ID), InventoryStockData.PROP_MENU_ITEM_ID);
    list.add(Projections.property(MenuItem.PROP_NAME), InventoryStockData.PROP_MENU_ITEM_NAME);
    list.add(Projections.property(MenuItem.PROP_MENU_GROUP_NAME), InventoryStockData.PROP_MENU_GROUP);
    list.add(Projections.property(MenuItem.PROP_COST), InventoryStockData.PROP_MENU_ITEM_COST);
    list.add(Projections.property(MenuItem.PROP_UNIT_NAME), InventoryStockData.PROP_UNIT);
    list.add(Projections.property(MenuItem.PROP_SKU), InventoryStockData.PROP_SKU);
    criteria.setProjection(list);
    



    if (StringUtils.isNotEmpty(nameOrSku)) {
      Disjunction disjuction = Restrictions.disjunction();
      disjuction.add(Restrictions.ilike(MenuItem.PROP_NAME, nameOrSku, MatchMode.START));
      disjuction.add(Restrictions.ilike(MenuItem.PROP_SKU, nameOrSku, MatchMode.EXACT));
      criteria.add(disjuction);
    }
    if (menuGroup != null) {
      criteria.add(Restrictions.eq(MenuItem.PROP_MENU_GROUP_ID, menuGroup.getId()));
    }
    criteria.add(Restrictions.eqOrIsNull(MenuItem.PROP_INVENTORY_ITEM, Boolean.TRUE));
    criteria.setResultTransformer(Transformers.aliasToBean(InventoryStockData.class));
    List dataList = criteria.list();
    
    Map<String, InventoryStockData> itemMap = new HashMap();
    Iterator iterator; if ((dataList != null) && (dataList.size() > 0)) {
      for (iterator = dataList.iterator(); iterator.hasNext();) {
        InventoryStockData inventoryStockData = (InventoryStockData)iterator.next();
        itemMap.put(inventoryStockData.getMenuItemId(), inventoryStockData);
      }
    }
    
    return itemMap;
  }
}
