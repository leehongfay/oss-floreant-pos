package com.floreantpos.report;

import com.floreantpos.POSConstants;
import com.floreantpos.model.GiftCard;
import com.floreantpos.model.dao.GiftCardDAO;
import com.floreantpos.model.ext.PaperSize;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.services.report.ReportService;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;
import org.jdesktop.swingx.JXDatePicker;





















public class GiftCardSummaryReportView
  extends TransparentPanel
{
  private JButton btnGo;
  private JPanel reportPanel;
  private JXDatePicker fromDatePicker;
  private JXDatePicker toDatePicker;
  private JCheckBox chkExpired;
  private JComboBox<String> cbSortOptions;
  
  public GiftCardSummaryReportView()
  {
    setLayout(new BorderLayout());
    createUI();
  }
  
  private void viewReport() throws Exception {
    Date fromDate = fromDatePicker.getDate();
    Date toDate = toDatePicker.getDate();
    String sortOption = (String)cbSortOptions.getSelectedItem();
    if (fromDate.after(toDate)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      reportPanel.removeAll();
      return;
    }
    List<GiftCard> giftCardList = GiftCardDAO.getInstance().findGiftCards(fromDate, toDate, chkExpired.isSelected(), sortOption);
    
    JasperReport report = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("gift_card_summary_report"));
    
    HashMap properties = new HashMap();
    ReportUtil.populateRestaurantProperties(properties);
    properties.put("reportTitle", "Gift Card Summary Report");
    properties.put("reportTime", ReportService.formatFullDate(new Date()));
    GiftCardSummaryModel giftCardSummaryModel = new GiftCardSummaryModel(giftCardList);
    JasperPrint print = JasperFillManager.fillReport(report, properties, new JRTableModelDataSource(giftCardSummaryModel));
    
    JRViewer viewer = new JRViewer(print);
    reportPanel.removeAll();
    reportPanel.add(viewer);
    reportPanel.revalidate();
  }
  
  private void createUI() {
    fromDatePicker = UiUtil.getCurrentMonthStart();
    toDatePicker = UiUtil.getCurrentMonthEnd();
    chkExpired = new JCheckBox("Show expired cards only");
    cbSortOptions = new JComboBox();
    ComboBoxModel<String> comboBoxModel = new ComboBoxModel();
    comboBoxModel.addElement("Activation date");
    comboBoxModel.addElement("Owner");
    cbSortOptions.setModel(comboBoxModel);
    
    btnGo = new JButton();
    btnGo.setText(POSConstants.GO);
    btnGo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ev) {
        try {
          GiftCardSummaryReportView.this.viewReport();
        } catch (Exception e) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.ERROR_MESSAGE, e);
        }
        
      }
    });
    setLayout(new BorderLayout());
    
    JPanel topPanel = new JPanel(new MigLayout());
    topPanel.add(new JLabel("Activated between "));
    fromDatePicker.setFormats(new String[] { "dd MMM yy" });
    topPanel.add(fromDatePicker);
    topPanel.add(new JLabel("and "));
    toDatePicker.setFormats(new String[] { "dd MMM yy" });
    topPanel.add(toDatePicker);
    topPanel.add(new JLabel("Sort by:"), "gapLeft 20");
    topPanel.add(cbSortOptions);
    topPanel.add(chkExpired, "gapLeft 20");
    topPanel.add(btnGo, "width 60!,gapLeft 20");
    add(topPanel, "North");
    
    JPanel centerPanel = new JPanel(new BorderLayout());
    centerPanel.setBorder(new EmptyBorder(0, 10, 10, 10));
    centerPanel.add(new JSeparator(), "North");
    
    reportPanel = new JPanel(new BorderLayout());
    centerPanel.add(reportPanel);
    
    add(centerPanel);
  }
  
  public class GiftCardSummaryModel extends ListTableModel {
    private String[] columnNames = { "cardNumber", "ownerName", "batch", "activeDate", "deactiveDate", "exDate", "balance" };
    
    public GiftCardSummaryModel(List rows) {
      setColumnNames(columnNames);
      setRows(rows);
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      GiftCard giftCard = (GiftCard)rows.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return giftCard.getCardNumber();
      
      case 1: 
        return giftCard.getOwnerName();
      
      case 2: 
        return giftCard.getBatchNo();
      
      case 3: 
        if (giftCard.getActivationDate() != null)
          return DateUtil.formatReportDateAsString(giftCard.getActivationDate());
        return "";
      
      case 4: 
        if (giftCard.getDeActivationDate() != null)
          return DateUtil.formatReportDateAsString(giftCard.getDeActivationDate());
        return "";
      case 5: 
        if (giftCard.getExpiryDate() != null)
          return DateUtil.formatReportDateAsString(giftCard.getExpiryDate());
        return "";
      
      case 6: 
        return giftCard.getBalance();
      }
      
      return null;
    }
  }
}
