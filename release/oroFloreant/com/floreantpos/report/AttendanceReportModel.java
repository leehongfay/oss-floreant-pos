package com.floreantpos.report;

import com.floreantpos.model.User;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.ListTableModel;
import java.text.DecimalFormat;
import java.util.List;















public class AttendanceReportModel
  extends ListTableModel
{
  DecimalFormat decimalFormat = new DecimalFormat("0.00");
  
  public AttendanceReportModel() {
    super(new String[] { "employeeId", "employeeName", "clockIn", "clockOut", "workTime" });
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    AttendanceReportData data = (AttendanceReportData)rows.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      return String.valueOf(data.getUser().getId());
    
    case 1: 
      return data.getUser().getFirstName() + " " + data.getUser().getLastName();
    
    case 2: 
      return DateUtil.formatFullDateAndTimeAsString(data.getClockIn());
    

    case 3: 
      if (data.getClockOut() != null) {
        return DateUtil.formatFullDateAndTimeAsString(data.getClockOut());
      }
      
      break;
    case 4: 
      if (data.getWorkTime() != 0.0D) {
        return decimalFormat.format(data.getWorkTime());
      }
      
      break;
    }
    
    return null;
  }
}
