package com.floreantpos.report;

import com.floreantpos.model.User;
import java.util.Date;








public class WeeklyPayrollReportData
{
  private User user;
  private long totalWorkHourMs;
  private double regularWorkHour;
  private double overtimeHour;
  private double costPerHour;
  private double overtimeRatePerHour;
  private double regularPayment;
  private double overtimePayment;
  private double totalPayment;
  private Date fromDateOfWeek;
  private Date toDateOfWeek;
  private double totalDeclaredTips;
  private double totalNoCashTips;
  private int firstDayOfWeek;
  private String regularHourDisplay;
  private String overtimeDisplay;
  private String totalHourDisplay;
  
  public WeeklyPayrollReportData() {}
  
  public void calculateTotalHour()
  {
    double totalWorkHour = totalWorkHourMs / 3600000.0D;
    regularWorkHour = (totalWorkHour < 40.0D ? totalWorkHour : 40.0D);
    overtimeHour = (totalWorkHour > 40.0D ? totalWorkHour - 40.0D : 0.0D);
    costPerHour = user.getCostPerHour().doubleValue();
    overtimeRatePerHour = user.getOvertimeRatePerHour().doubleValue();
    regularPayment = (regularWorkHour * costPerHour);
    overtimePayment = (overtimeHour * overtimeRatePerHour);
    totalPayment = (regularPayment + overtimePayment);
    
    long totalWorkInHour = getHour(totalWorkHourMs);
    long totalWorkInMin = getMin(totalWorkHourMs);
    
    totalHourDisplay = (totalWorkInHour + "h " + totalWorkInMin + "m");
    regularHourDisplay = (totalWorkHour < 40.0D ? totalHourDisplay : "40h");
    
    long overtimeMS = (overtimeHour * 3600000.0D);
    long otHour = getHour(overtimeMS);
    long otMin = getMin(overtimeMS);
    overtimeDisplay = (otHour + "h " + otMin + "m");
  }
  
  private long getHour(long valueInMS) {
    return valueInMS / 3600000L;
  }
  
  private long getMin(long valueInMS) {
    long hour = valueInMS / 3600000L;
    valueInMS -= hour * 3600000L;
    return valueInMS / 60000L;
  }
  
  public User getUser() {
    return user;
  }
  
  public void setUser(User user) {
    this.user = user;
  }
  
  public double getRegularHour() {
    return regularWorkHour;
  }
  
  public void setRegularHour(double totalHour) {
    regularWorkHour = totalHour;
  }
  
  public double getRate() {
    return costPerHour;
  }
  
  public void setRate(double rate) {
    costPerHour = rate;
  }
  
  public double getTotalPayment() {
    return totalPayment;
  }
  
  public void setTotalPayment(double payment) {
    totalPayment = payment;
  }
  
  public Date getFromDateOfWeek() {
    return fromDateOfWeek;
  }
  
  public void setFromDateOfWeek(Date fromDateOfWeek) {
    this.fromDateOfWeek = fromDateOfWeek;
  }
  
  public Date getToDateOfWeek() {
    return toDateOfWeek;
  }
  
  public void setToDateOfWeek(Date toDateOfWeek) {
    this.toDateOfWeek = toDateOfWeek;
  }
  
  public double getOvertime() {
    return overtimeHour;
  }
  
  public void setOvertime(double overtime) {
    overtimeHour = overtime;
  }
  
  public double getOvertimeRate() {
    return overtimeRatePerHour;
  }
  
  public void setOvertimeRate(double overtimeRate) {
    overtimeRatePerHour = overtimeRate;
  }
  
  public double getRegularPayment() {
    return regularPayment;
  }
  
  public void setRegularPayment(double regularPayment) {
    this.regularPayment = regularPayment;
  }
  
  public double getOvertimePayment() {
    return overtimePayment;
  }
  
  public void setOvertimePayment(double overtimePayment) {
    this.overtimePayment = overtimePayment;
  }
  
  public double getTotalDeclaredTips() {
    return totalDeclaredTips;
  }
  
  public void setTotalDeclaredTips(double totalDeclaredTips) {
    this.totalDeclaredTips = totalDeclaredTips;
  }
  
  public double getTotalNoCashTips() {
    return totalNoCashTips;
  }
  
  public void setTotalNoCashTips(double totalNoCashTips) {
    this.totalNoCashTips = totalNoCashTips;
  }
  
  public int getFirstDayOfWeek() {
    return firstDayOfWeek;
  }
  
  public void setFirstDayOfWeek(int firstDayOfWeek) {
    this.firstDayOfWeek = firstDayOfWeek;
  }
  
  public long getTotalWorkHourMs() {
    return totalWorkHourMs;
  }
  
  public void setTotalWorkHourMs(long totalWorkHourMs) {
    this.totalWorkHourMs = totalWorkHourMs;
  }
  
  public String getRegularHourDisplay() {
    return regularHourDisplay;
  }
  
  public void setRegularHourDisplay(String regularHourDisplay) {
    this.regularHourDisplay = regularHourDisplay;
  }
  
  public String getOvertimeDisplay() {
    return overtimeDisplay;
  }
  
  public void setOvertimeDisplay(String overtimeDisplay) {
    this.overtimeDisplay = overtimeDisplay;
  }
  
  public String getTotalHourDisplay() {
    return totalHourDisplay;
  }
  
  public void setTotalHourDisplay(String totalHourDisplay) {
    this.totalHourDisplay = totalHourDisplay;
  }
}
