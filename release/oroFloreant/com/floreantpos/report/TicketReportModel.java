package com.floreantpos.report;

import com.floreantpos.model.OrderType;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.util.NumberUtil;
import java.text.DecimalFormat;
import java.util.List;
import javax.swing.table.AbstractTableModel;
















public class TicketReportModel
  extends AbstractTableModel
{
  private static DecimalFormat formatter = new DecimalFormat("#,##0.00");
  

  private String[] columnNames = { "id", "date", "orderType", "owner", "total", "due" };
  
  private List<Ticket> items;
  private double grandTotal;
  private double totalDue;
  
  public TicketReportModel() {}
  
  public int getRowCount()
  {
    if (items == null) { return 0;
    }
    return items.size();
  }
  
  public int getColumnCount() {
    return columnNames.length;
  }
  
  public String getColumnName(int column)
  {
    return columnNames[column];
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    Ticket ticket = (Ticket)items.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      return String.valueOf(ticket.getId());
    
    case 1: 
      return DateUtil.formatFullDateAndTimeAsString(ticket.getCreateDate());
    




    case 2: 
      return ticket.getOrderType().getName();
    



    case 3: 
      return ticket.getOwner().getFullName();
    
    case 4: 
      return NumberUtil.formatNumber(ticket.getTotalAmountWithTips());
    case 5: 
      return NumberUtil.formatNumber(ticket.getDueAmount());
    }
    return null;
  }
  
  public List<Ticket> getItems() {
    return items;
  }
  
  public void setItems(List<Ticket> items) {
    this.items = items;
  }
  
  public String getGrandTotalAsString() {
    return formatter.format(grandTotal);
  }
  
  public void setGrandTotal(double grandTotal) {
    this.grandTotal = grandTotal;
  }
  
  public void calculateGrandTotal() {
    grandTotal = 0.0D;
    if (items == null) {
      return;
    }
    
    for (Ticket item : items) {
      grandTotal += item.getDueAmount().doubleValue();
    }
  }
  
  public String getTotalDueAsString() {
    return formatter.format(totalDue);
  }
  
  public void setTotalDue(double totalDue) { this.totalDue = totalDue; }
  
  public void calculateTotalDue() {
    totalDue = 0.0D;
    if (items == null) {
      return;
    }
    for (Ticket item : items) {
      totalDue += item.getDueAmount().doubleValue();
    }
  }
}
