package com.floreantpos.report;

import com.floreantpos.model.MenuItem;
import com.floreantpos.swing.ListTableModel;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
















public class PurchaseReportModel
  extends ListTableModel
{
  SimpleDateFormat dateFormat2 = new SimpleDateFormat("MMM-dd-yyyy hh:mm a");
  
  DecimalFormat decimalFormat = new DecimalFormat("0.00");
  
  public PurchaseReportModel() {
    super(new String[] { "item", "description", "quantity", "price", "total" });
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    MenuItem data = (MenuItem)rows.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      return String.valueOf(data.getDisplayName());
    
    case 1: 
      return data.getDescription();
    
    case 2: 
      return String.valueOf(data.getReplenishLevel());
    
    case 3: 
      return String.valueOf(data.getAverageUnitPurchasePrice());
    
    case 4: 
      return "";
    }
    return null;
  }
}
