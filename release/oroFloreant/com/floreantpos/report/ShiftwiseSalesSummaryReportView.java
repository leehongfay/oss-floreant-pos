package com.floreantpos.report;

import com.floreantpos.POSConstants;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.UserType;
import com.floreantpos.model.dao.SalesSummaryDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.dao.UserTypeDAO;
import com.floreantpos.model.ext.PaperSize;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.ListComboBoxModel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;
import org.jdesktop.layout.GroupLayout;
import org.jdesktop.layout.GroupLayout.ParallelGroup;
import org.jdesktop.layout.GroupLayout.SequentialGroup;
import org.jdesktop.swingx.JXDatePicker;






public class ShiftwiseSalesSummaryReportView
  extends JPanel
{
  public static final int REPORT_KEY_STATISTICS = 1;
  public static final int REPORT_SALES_ANALYSIS = 2;
  private int reportType;
  private JButton btnGo;
  private JComboBox cbTerminal;
  private JComboBox cbEmployeeType;
  private JXDatePicker fromDatePicker;
  private JLabel jLabel1;
  private JLabel jLabel2;
  private JLabel jLabel3;
  private JLabel jLabel4;
  private JSeparator jSeparator1;
  private JPanel reportPanel;
  private JXDatePicker toDatePicker;
  
  public ShiftwiseSalesSummaryReportView()
  {
    initComponents();
    
    UserTypeDAO dao = new UserTypeDAO();
    List<UserType> userTypes = dao.findAll();
    
    Vector list = new Vector();
    list.add(null);
    list.addAll(userTypes);
    
    cbEmployeeType.setModel(new DefaultComboBoxModel(list));
    
    TerminalDAO terminalDAO = new TerminalDAO();
    List terminals = terminalDAO.findAll();
    terminals.add(0, POSConstants.ALL);
    cbTerminal.setModel(new ListComboBoxModel(terminals));
  }
  






  private void initComponents()
  {
    jLabel1 = new JLabel();
    jLabel2 = new JLabel();
    jLabel3 = new JLabel();
    jLabel4 = new JLabel();
    fromDatePicker = UiUtil.getCurrentMonthStart();
    toDatePicker = UiUtil.getCurrentMonthEnd();
    cbEmployeeType = new JComboBox();
    cbTerminal = new JComboBox();
    btnGo = new JButton();
    jSeparator1 = new JSeparator();
    reportPanel = new JPanel();
    
    jLabel1.setText(POSConstants.FROM + ":");
    
    jLabel2.setText(POSConstants.TO + ":");
    
    jLabel3.setText(POSConstants.EMPLOYEE_TYPE + ":");
    
    jLabel4.setText(POSConstants.TERMINAL_LABEL + ":");
    
    btnGo.setText(POSConstants.GO);
    btnGo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        ShiftwiseSalesSummaryReportView.this.showReport(evt);
      }
      
    });
    reportPanel.setLayout(new BorderLayout());
    
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(1).add(layout
      .createSequentialGroup().addContainerGap().add(layout
      .createParallelGroup(1).add(jSeparator1, -1, 502, 32767).add(layout
      .createSequentialGroup().add(layout.createParallelGroup(1).add(jLabel1).add(jLabel2)).addPreferredGap(0).add(layout
      .createParallelGroup(1, false).add(toDatePicker, -1, -1, 32767).add(fromDatePicker, -1, -1, 32767))
      .add(20, 20, 20).add(layout.createParallelGroup(1).add(jLabel3).add(jLabel4)).addPreferredGap(0).add(layout
      .createParallelGroup(1, false).add(cbTerminal, 0, -1, 32767).add(cbEmployeeType, 0, 137, 32767)).addPreferredGap(0).add(btnGo, -2, 72, -2))
      .add(reportPanel, -1, 502, 32767)).addContainerGap()));
    layout.setVerticalGroup(layout.createParallelGroup(1).add(layout
      .createSequentialGroup().addContainerGap().add(layout
      .createParallelGroup(1).add(jLabel3).add(layout
      .createSequentialGroup().add(cbEmployeeType, -2, -1, -2).addPreferredGap(0).add(layout
      .createParallelGroup(3).add(jLabel4).add(cbTerminal, -2, -1, -2).add(btnGo))).add(layout
      .createSequentialGroup().add(layout.createParallelGroup(1).add(jLabel1).add(fromDatePicker, -2, -1, -2))
      .addPreferredGap(0).add(layout
      .createParallelGroup(1).add(jLabel2).add(toDatePicker, -2, -1, -2)))).addPreferredGap(0)
      .add(jSeparator1, -2, 10, -2).addPreferredGap(0).add(reportPanel, -1, 303, 32767)
      .addContainerGap()));
    
    layout.linkSize(new Component[] { cbTerminal, cbEmployeeType, jLabel3, jLabel4 }, 2);
    
    layout.linkSize(new Component[] { fromDatePicker, jLabel1, jLabel2, toDatePicker }, 2);
  }
  
  private boolean initCriteria()
  {
    fromDate = fromDatePicker.getDate();
    toDate = toDatePicker.getDate();
    
    if (fromDate.after(toDate)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      return false;
    }
    
    dateDiff = ((int)((toDate.getTime() - fromDate.getTime()) * (1.15740741D * Math.pow(10.0D, -8.0D))) + 1);
    userType = ((UserType)cbEmployeeType.getSelectedItem());
    


    terminal = null;
    if ((cbTerminal.getSelectedItem() instanceof Terminal)) {
      terminal = ((Terminal)cbTerminal.getSelectedItem());
    }
    
    Calendar calendar = Calendar.getInstance();
    calendar.clear();
    
    Calendar calendar2 = Calendar.getInstance();
    calendar2.setTime(fromDate);
    
    calendar.set(1, calendar2.get(1));
    calendar.set(2, calendar2.get(2));
    calendar.set(5, calendar2.get(5));
    calendar.set(10, 0);
    calendar.set(12, 0);
    calendar.set(13, 0);
    fromDate = calendar.getTime();
    
    calendar.clear();
    calendar2.setTime(toDate);
    calendar.set(1, calendar2.get(1));
    calendar.set(2, calendar2.get(2));
    calendar.set(5, calendar2.get(5));
    calendar.set(10, 23);
    calendar.set(12, 59);
    calendar.set(13, 59);
    toDate = calendar.getTime();
    
    return true;
  }
  
  private void showReport(ActionEvent evt) {
    try {
      if (!initCriteria()) {
        return;
      }
      
      if (reportType == 1) {
        showKeyStatisticsReport();
      }
      else if (reportType == 2) {
        showShiftwiseSalesReport();
      }
    } catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  private void showShiftwiseSalesReport() throws Exception {
    SalesSummaryDAO dao = new SalesSummaryDAO();
    List<ShiftwiseSalesSummaryReportModel.ShiftwiseSalesSummaryData> datas = dao.findSalesAnalysis(fromDate, toDate, userType, terminal);
    
    Map properties = new HashMap();
    ReportUtil.populateRestaurantProperties(properties);
    properties.put("subtitle", POSConstants.SALES_SUMMARY_REPORT);
    properties.put("reportTime", DateUtil.formatFullDateAndTimeAsString(new Date()));
    properties.put("fromDate", DateUtil.formatFullDateAsString(fromDate));
    properties.put("toDate", DateUtil.formatFullDateAsString(toDate));
    if (userType == null) {
      properties.put("reportType", POSConstants.SYSTEM_TOTAL);
    }
    else {
      properties.put("reportType", userType.getName());
    }
    
    properties.put("terminal", terminal == null ? POSConstants.ALL : terminal.getName());
    

    JasperReport report = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("shiftwise_sales_summary_report"));
    JasperPrint print = JasperFillManager.fillReport(report, properties, new JRTableModelDataSource(new ShiftwiseSalesSummaryReportModel(datas)));
    openReport(print);
  }
  
  private void showKeyStatisticsReport() throws Exception {
    SalesSummaryDAO dao = new SalesSummaryDAO();
    SalesStatistics summary = dao.findKeyStatistics(fromDate, toDate, userType, terminal);
    
    Map properties = new HashMap();
    ReportUtil.populateRestaurantProperties(properties);
    properties.put("subtitle", POSConstants.SALES_SUMMARY_REPORT);
    properties.put("Capacity", String.valueOf(summary.getCapacity()));
    properties.put("GuestCount", String.valueOf(summary.getGuestCount()));
    properties.put("GuestPerSeat", NumberUtil.formatNumber(Double.valueOf(summary.getGuestPerSeat())));
    properties.put("reportTime", fullDateFormatter.format(new Date()));
    properties.put("fromDate", shortDateFormatter.format(fromDate));
    properties.put("toDate", shortDateFormatter.format(toDate));
    if (userType == null) {
      properties.put("reportType", POSConstants.SYSTEM_TOTAL);
    }
    else {
      properties.put("reportType", userType.getName());
    }
    properties.put("shift", POSConstants.ALL);
    properties.put("centre", terminal == null ? POSConstants.ALL : terminal.getName());
    properties.put("days", String.valueOf(dateDiff));
    
    properties.put("Capacity", String.valueOf(summary.getCapacity()));
    properties.put("GuestCount", String.valueOf(summary.getGuestCount()));
    properties.put("GuestPerSeat", NumberUtil.formatNumber(Double.valueOf(summary.getGuestPerCheck())));
    properties.put("TableTrnOvr", NumberUtil.formatNumber(Double.valueOf(summary.getTableTurnOver())));
    properties.put("AVGGuest", NumberUtil.formatNumber(Double.valueOf(summary.getAvgGuest())));
    properties.put("OpenChecks", String.valueOf(summary.getOpenChecks()));
    properties.put("VOIDChecks", String.valueOf(summary.getVoidChecks()));
    properties.put("OPPDChecks", String.valueOf(" "));
    properties.put("TRNGChecks", String.valueOf(" "));
    properties.put("ROPNChecks", String.valueOf(summary.getRopnChecks()));
    properties.put("MergeChecks", String.valueOf(" "));
    properties.put("LaborHour", NumberUtil.formatNumber(Double.valueOf(summary.getLaborHour())));
    properties.put("LaborSales", NumberUtil.formatNumber(Double.valueOf(summary.getGrossSale())));
    properties.put("Tables", String.valueOf(summary.getTables()));
    properties.put("CheckCount", String.valueOf(summary.getCheckCount()));
    properties.put("GuestPerChecks", NumberUtil.formatNumber(Double.valueOf(summary.getGuestPerCheck())));
    properties.put("TrnOvrTime", String.valueOf(" "));
    properties.put("AVGChecks", NumberUtil.formatNumber(Double.valueOf(summary.getAvgCheck())));
    properties.put("OPENAmount", NumberUtil.formatNumber(Double.valueOf(summary.getOpenAmount())));
    properties.put("VOIDAmount", NumberUtil.formatNumber(Double.valueOf(summary.getVoidAmount())));
    properties.put("PAIDChecks", String.valueOf(summary.getPaidChecks()));
    properties.put("TRNGAmount", String.valueOf(" "));
    properties.put("ROPNAmount", NumberUtil.formatNumber(Double.valueOf(summary.getRopnAmount())));
    properties.put("NTaxChecks", String.valueOf(summary.getNtaxChecks()));
    properties.put("NTaxAmount", NumberUtil.formatNumber(Double.valueOf(summary.getNtaxAmount())));
    properties.put("MergeAmount", String.valueOf(" "));
    properties.put("Labor", NumberUtil.formatNumber(Double.valueOf(summary.getLaborCost())));
    properties.put("LaborCost", NumberUtil.formatNumber(Double.valueOf(summary.getLaborCost() / summary.getGrossSale() * 100.0D)));
    
    JasperReport report = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("key_statistics_report"));
    JasperPrint print = JasperFillManager.fillReport(report, properties, new JRTableModelDataSource(new SalesStatistics.ShiftwiseDataTableModel(summary.getSalesTableDataList())));
    openReport(print);
  }
  
  private void openReport(JasperPrint print)
  {
    JRViewer viewer = new JRViewer(print);
    reportPanel.removeAll();
    reportPanel.add(viewer);
    reportPanel.revalidate();
  }
  













  private SimpleDateFormat fullDateFormatter = new SimpleDateFormat("dd MMM yyyy, hh:mm a");
  private SimpleDateFormat shortDateFormatter = new SimpleDateFormat("dd MMM yyyy");
  private Date fromDate;
  private Date toDate;
  private int dateDiff;
  private UserType userType;
  private Terminal terminal;
  
  public int getReportType()
  {
    return reportType;
  }
  
  public void setReportType(int reportType) {
    this.reportType = reportType;
  }
}
