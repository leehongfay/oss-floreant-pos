package com.floreantpos.report;

import java.io.InputStream;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

public class ReportParser
{
  public ReportParser() {}
  
  public static JasperReport getReport(String reportName) throws JRException
  {
    InputStream resource = null;
    try
    {
      resource = ReportParser.class.getResourceAsStream(reportName);
      return (JasperReport)JRLoader.loadObject(resource);
    } finally {
      org.apache.commons.io.IOUtils.closeQuietly(resource);
    }
  }
}
