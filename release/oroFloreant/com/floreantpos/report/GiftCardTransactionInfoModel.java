package com.floreantpos.report;

import com.floreantpos.model.GiftCertificateTransaction;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.ListTableModel;

public class GiftCardTransactionInfoModel extends ListTableModel
{
  public GiftCardTransactionInfoModel()
  {
    super(new String[] { "transactionTime", "desc", "transactionAmount" });
  }
  

  public Object getValueAt(int rowIndex, int columnIndex)
  {
    PosTransaction data = (PosTransaction)rows.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      String valueStr = DateUtil.formatReportDateAsString(data.getTransactionTime());
      return valueStr;
    case 1: 
      if ((data instanceof GiftCertificateTransaction))
        return "Sale";
      if ((data instanceof com.floreantpos.model.CashTransaction)) {
        return "Add to balance";
      }
      return "";
    
    case 2: 
      if ((data instanceof GiftCertificateTransaction)) {
        return Double.valueOf(data.getAmount().doubleValue() * -1.0D);
      }
      return data.getAmount();
    }
    return null;
  }
}
