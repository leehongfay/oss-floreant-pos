package com.floreantpos.report;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.UserType;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.ext.PaperSize;
import com.floreantpos.services.report.ReportService;
import com.floreantpos.util.CurrencyUtil;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;




















public class OpenTicketSummaryReport
  extends Report
{
  public OpenTicketSummaryReport() {}
  
  public void refresh()
    throws Exception
  {
    List<Ticket> tickets = TicketDAO.getInstance().findOpenTickets(getTerminal(), getUserType());
    TicketReportModel reportModel = new TicketReportModel();
    reportModel.setItems(tickets);
    reportModel.calculateGrandTotal();
    reportModel.calculateTotalDue();
    
    HashMap map = new HashMap();
    ReportUtil.populateRestaurantProperties(map);
    map.put("reportTitle", Messages.getString("OpenTicketSummaryReport.0"));
    map.put("reportTime", ReportService.formatFullDate(new Date()));
    String value = "<b>User Type:</b> " + (getUserType() == null ? POSConstants.ALL : getUserType().getName());
    value = value + " <b> Terminal:</b> " + (getTerminal() == null ? POSConstants.ALL : getTerminal().getName());
    value = value + " <b> Currency:</b> " + Messages.getString("SalesReport.8") + CurrencyUtil.getCurrencyName() + " (" + CurrencyUtil.getCurrencySymbol() + ")";
    map.put("userTypeTerminalCurrency", value);
    map.put("grandTotal", reportModel.getGrandTotalAsString());
    map.put("totalDue", reportModel.getTotalDueAsString());
    
    JasperReport masterReport = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("open_ticket_summary_report"));
    JasperPrint print = JasperFillManager.fillReport(masterReport, map, new JRTableModelDataSource(reportModel));
    viewer = new JRViewer(print);
  }
  
  public boolean isDateRangeSupported()
  {
    return false;
  }
  
  public boolean isTypeSupported()
  {
    return false;
  }
}
