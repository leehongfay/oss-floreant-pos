package com.floreantpos.report;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemModifier;
import com.floreantpos.model.dao.TicketItemDAO;
import com.floreantpos.model.ext.PaperSize;
import com.floreantpos.services.report.ReportService;
import com.floreantpos.util.CurrencyUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;

















public class SalesReport
  extends Report
{
  private SalesReportModel itemReportModel;
  private SalesReportModel modifierReportModel;
  private boolean isInventory;
  
  public SalesReport(boolean isInventory)
  {
    this.isInventory = isInventory;
  }
  
  public void refresh() throws Exception
  {
    createModels();
    
    JasperReport itemReport = ReportUtil.getReport("sales_sub_report");
    JasperReport modifierReport = ReportUtil.getReport("sales_sub_report");
    
    HashMap map = new HashMap();
    ReportUtil.populateRestaurantProperties(map);
    String reportTitle = isInventory ? Messages.getString("SalesReport.3") : Messages.getString("SalesReport.9");
    map.put("reportTitle", reportTitle);
    map.put("reportTime", ReportService.formatFullDate(new Date()));
    map.put("dateRange", ReportService.formatFullDate(getStartDate()) + " to " + ReportService.formatFullDate(getEndDate()));
    map.put("terminalName", getTerminal() == null ? POSConstants.ALL : getTerminal().getName());
    map.put("itemDataSource", new JRTableModelDataSource(itemReportModel));
    map.put("modifierDataSource", new JRTableModelDataSource(modifierReportModel));
    map.put("currency", Messages.getString("SalesReport.8") + " " + CurrencyUtil.getCurrencyName() + " (" + CurrencyUtil.getCurrencySymbol() + ")");
    map.put("itemReport", itemReport);
    map.put("isShowGroup", Boolean.valueOf(isShowInGroups()));
    map.put("modifierReport", modifierReport);
    if (modifierReportModel.getItems().size() > 0) {
      map.put("modifierSection", "MODIFIERS");
    }
    
    JasperReport masterReport = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("sales_report"));
    
    JasperPrint print = JasperFillManager.fillReport(masterReport, map, new JREmptyDataSource());
    viewer = new JRViewer(print);
  }
  
  public boolean isDateRangeSupported()
  {
    return true;
  }
  
  public boolean isTypeSupported()
  {
    return true;
  }
  
  public void createModels() {
    Date startDate = getStartDate();
    Date toDate = getEndDate();
    Terminal terminal = getTerminal();
    MenuGroup menuGroup = getMenuGroup();
    List<MenuGroup> groups = getMenuGroups();
    HashMap<String, ReportItem> itemMap = new LinkedHashMap();
    HashMap<String, ReportItem> modifierMap = new LinkedHashMap();
    


    List<TicketItem> ticketItems = TicketItemDAO.getInstance().findTicketItemWithinDate(startDate, toDate, terminal, groups, isInventory);
    
    String key = null;
    for (TicketItem ticketItem : ticketItems) {
      if ((ticketItem.getUnitPrice().doubleValue() != 0.0D) || (isIncludedFreeItems()))
      {

        String menuItemId = ticketItem.getMenuItemId();
        key = (menuItemId == null ? ticketItem.getName() : menuItemId) + "-" + ticketItem.getUnitPrice();
        
        ReportItem reportItem = (ReportItem)itemMap.get(key);
        if (reportItem == null) {
          reportItem = new ReportItem();
          reportItem.setId(key);
          reportItem.setPrice(ticketItem.getUnitPrice().doubleValue());
          reportItem.setCost(ticketItem.getUnitCost().doubleValue());
          reportItem.setName(ticketItem.getName());
          reportItem.setTaxRate(ticketItem.getTotalTaxRate());
          reportItem.setGroupName(ticketItem.getGroupName());
          reportItem.setBarcode(menuItemId);
          reportItem.setAdjustedPrice(ticketItem.getAdjustedUnitPrice().doubleValue());
          itemMap.put(key, reportItem);
        }
        reportItem.setQuantity(reportItem.getQuantity() + ticketItem.getQuantity().doubleValue());
        reportItem.setGrossTotal(reportItem.getGrossTotal() + ticketItem.getAdjustedTotalWithoutModifiers().doubleValue());
        reportItem.setDiscount(reportItem.getDiscount() + ticketItem.getAdjustedDiscountWithoutModifiers().doubleValue());
        reportItem.setTaxTotal(reportItem.getTaxTotal() + ticketItem.getAdjustedTaxWithoutModifiers().doubleValue());
        double adjustedAmount = ticketItem.isTaxIncluded().booleanValue() ? ticketItem.getAdjustedTotalWithoutModifiers().doubleValue() - ticketItem.getAdjustedTaxWithoutModifiers().doubleValue() : ticketItem.getAdjustedSubtotalWithoutModifiers().doubleValue();
        reportItem.setNetTotal(reportItem.getNetTotal() + adjustedAmount);
      }
    }
    itemReportModel = new SalesReportModel();
    Object values = itemMap.values();
    for (Iterator iterator = ((Collection)values).iterator(); iterator.hasNext();) {
      reportItem = (ReportItem)iterator.next();
      if (reportItem.getQuantity() == 0.0D)
        iterator.remove();
    }
    ReportItem reportItem;
    itemReportModel.setItems(new ArrayList((Collection)values));
    
    List<TicketItemModifier> ticketModifierItems = TicketItemDAO.getInstance().findTicketItemModifierWithinDate(startDate, toDate, terminal, groups, isInventory);
    for (TicketItemModifier modifier : ticketModifierItems)
      if ((modifier.getUnitPrice().doubleValue() != 0.0D) || (isIncludedFreeItems()))
      {

        String itemId = modifier.getItemId();
        key = (itemId == null ? modifier.getName() : itemId) + "-" + modifier.getModifierType() + "-" + modifier.getUnitPrice();
        
        ReportItem modifierReportItem = (ReportItem)modifierMap.get(key);
        if (modifierReportItem == null) {
          modifierReportItem = new ReportItem();
          modifierReportItem.setId(itemId);
          modifierReportItem.setPrice(modifier.getUnitPrice().doubleValue());
          modifierReportItem.setName(modifier.getName());
          modifierReportItem.setTaxRate(modifier.getTotalTaxRate());
          modifierReportItem.setAdjustedPrice(modifier.getAdjustedUnitPrice().doubleValue());
          modifierMap.put(key, modifierReportItem);
        }
        modifierReportItem.setQuantity(modifierReportItem.getQuantity() + modifier.getItemQuantity().doubleValue() * modifier.getTicketItemQuantity());
        modifierReportItem.setDiscount(modifierReportItem.getDiscount() + modifier.getAdjustedDiscount().doubleValue());
        
        modifierReportItem.setGrossTotal(modifierReportItem.getGrossTotal() + modifier.getAdjustedTotal().doubleValue());
        modifierReportItem.setTaxTotal(modifierReportItem.getTaxTotal() + modifier.getAdjustedTax().doubleValue());
        double adjustedAmount = modifier.isTaxIncluded().booleanValue() ? modifier.getAdjustedSubtotal().doubleValue() - modifier.getAdjustedTax().doubleValue() : modifier.getAdjustedSubtotal().doubleValue();
        modifierReportItem.setNetTotal(modifierReportItem.getNetTotal() + adjustedAmount);
      }
    modifierReportModel = new SalesReportModel();
    ArrayList<ReportItem> modifiers = new ArrayList(modifierMap.values());
    modifierReportModel.setItems(modifiers);
  }
}
