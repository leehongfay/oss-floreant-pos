package com.floreantpos.report;

import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.Ticket;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.table.AbstractTableModel;


















public class CashDrawerTransactionReportModel
  extends AbstractTableModel
{
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
  private String[] columnNames = { "transTime", "transNo", "token", "totalAmount" };
  
  private List<PosTransaction> items;
  
  public CashDrawerTransactionReportModel() {}
  
  public int getRowCount()
  {
    if (items == null) {
      return 0;
    }
    return items.size();
  }
  
  public int getColumnCount() {
    return columnNames.length;
  }
  
  public String getColumnName(int column)
  {
    return columnNames[column];
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    PosTransaction transaction = (PosTransaction)items.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      return dateFormat.format(transaction.getTransactionTime());
    
    case 1: 
      return transaction.getId();
    
    case 2: 
      return "" + transaction.getTicket().getTokenNo();
    
    case 3: 
      return transaction.getAmount();
    }
    return null;
  }
  
  public List<PosTransaction> getItems() {
    return items;
  }
  
  public void setItems(List<PosTransaction> items) {
    this.items = items;
  }
}
