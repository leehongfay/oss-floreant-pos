package com.floreantpos.report;

import com.floreantpos.POSConstants;
import com.floreantpos.model.CashTransaction;
import com.floreantpos.model.CreditCardTransaction;
import com.floreantpos.model.DebitCardTransaction;
import com.floreantpos.model.DeclaredTips;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.GenericDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.model.ext.PaperSize;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.services.report.ReportService;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.ListComboBoxModel;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.jdesktop.swingx.JXDatePicker;



















public class TracTipsReportView
  extends TransparentPanel
{
  private JButton btnGo;
  private JComboBox cbTerminal;
  private JXDatePicker fromDatePicker;
  private JXDatePicker toDatePicker;
  private JPanel reportPanel;
  private JComboBox jcbEmployee;
  private Session session;
  
  public TracTipsReportView()
  {
    setLayout(new BorderLayout());
    createUI();
  }
  
  private void viewReport() throws Exception {
    GenericDAO dao = new GenericDAO();
    Date fromDate = DateUtil.startOfDay(fromDatePicker.getDate());
    Date toDate = DateUtil.endOfDay(toDatePicker.getDate());
    
    if (fromDate.after(toDate)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      return;
    }
    
    User selectedUser = null;
    if ((jcbEmployee.getSelectedItem() instanceof User)) {
      selectedUser = (User)jcbEmployee.getSelectedItem();
    }
    
    Terminal terminal = null;
    if ((cbTerminal.getSelectedItem() instanceof Terminal)) {
      terminal = (Terminal)cbTerminal.getSelectedItem();
    }
    session = dao.createNewSession();
    List<TracTipsReportData> rows = new ArrayList();
    
    List<User> users = new ArrayList();
    ComboBoxModel model; if (selectedUser == null) {
      model = (ComboBoxModel)jcbEmployee.getModel();
      List dataList = model.getDataList();
      for (Object object : dataList) {
        if ((object instanceof User)) {
          users.add((User)object);
        }
      }
    }
    else {
      users.add(selectedUser);
    }
    for (User user : users) {
      if ((hasDeclaredTips(session, fromDate, toDate, user)) || (hasTransactions(session, fromDate, toDate, user)))
      {


        TracTipsReportData reportData = new TracTipsReportData();
        reportData.setEmployeeID(user.getId());
        reportData.setEmployeeName(user.getFullName());
        calculateGrossSales(reportData, session, fromDate, toDate, user);
        calculateCreditCardSales(reportData, session, fromDate, toDate, user);
        calculateCashTotalWithoutTips(reportData, session, fromDate, toDate, user);
        calculateDeclareTips(reportData, session, fromDate, toDate, user);
        reportData.setNetTips(reportData.getCreditCardTips() + reportData.getTracCashTips() + reportData.getDeclareTips());
        rows.add(reportData);
      }
    }
    if (rows.isEmpty()) {
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), "no data");
      return;
    }
    
    JasperReport trakTipsReport = ReportUtil.getReport(PaperSize.getReportNameAccording2Size("track_tips_report"));
    
    HashMap properties = new HashMap();
    ReportUtil.populateRestaurantProperties(properties);
    properties.put("reportTitle", "TRAC Tips");
    properties.put("reportTime", ReportService.formatFullDate(new Date()));
    properties.put("fromDay", ReportService.formatFullDate(fromDate));
    properties.put("toDay", ReportService.formatFullDate(toDate));
    properties.put("type", selectedUser == null ? POSConstants.ALL : selectedUser.getFullName());
    properties.put("cntr", terminal == null ? POSConstants.ALL : terminal.getName());
    
    TracTipsReportModel reportModel = new TracTipsReportModel();
    reportModel.setRows(rows);
    JasperPrint print = JasperFillManager.fillReport(trakTipsReport, properties, new JRTableModelDataSource(reportModel));
    
    JRViewer viewer = new JRViewer(print);
    reportPanel.removeAll();
    reportPanel.add(viewer);
    reportPanel.revalidate();
  }
  
  private void createUI() {
    fromDatePicker = UiUtil.getCurrentMonthStart();
    toDatePicker = UiUtil.getCurrentMonthEnd();
    btnGo = new JButton();
    btnGo.setText(POSConstants.GO);
    btnGo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          TracTipsReportView.this.viewReport();
        } catch (Exception e1) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.ERROR_MESSAGE, e1);
        }
        
      }
    });
    jcbEmployee = new JComboBox();
    
    UserDAO dao = new UserDAO();
    List<User> userTypes = dao.findAll();
    
    Vector list = new Vector();
    list.add(POSConstants.ALL);
    list.addAll(userTypes);
    
    jcbEmployee.setModel(new ComboBoxModel(list));
    
    cbTerminal = new JComboBox();
    TerminalDAO terminalDAO = new TerminalDAO();
    List terminals = terminalDAO.findAll();
    terminals.add(0, POSConstants.ALL);
    cbTerminal.setModel(new ListComboBoxModel(terminals));
    
    setLayout(new BorderLayout());
    
    JPanel topPanel = new JPanel(new MigLayout());
    
    topPanel.add(new JLabel(POSConstants.START_DATE + ":"));
    fromDatePicker.setFormats(new String[] { "dd MMM yy" });
    topPanel.add(fromDatePicker);
    topPanel.add(new JLabel(POSConstants.END_DATE + ":"));
    toDatePicker.setFormats(new String[] { "dd MMM yy" });
    topPanel.add(toDatePicker);
    topPanel.add(new JLabel(POSConstants.EMPLOYEE + ":"));
    topPanel.add(jcbEmployee);
    topPanel.add(new JLabel(POSConstants.TERMINAL_LABEL + ":"));
    topPanel.add(cbTerminal);
    topPanel.add(btnGo, "width 60!");
    add(topPanel, "North");
    
    JPanel centerPanel = new JPanel(new BorderLayout());
    centerPanel.setBorder(new EmptyBorder(0, 10, 10, 10));
    centerPanel.add(new JSeparator(), "North");
    
    reportPanel = new JPanel(new BorderLayout());
    centerPanel.add(reportPanel);
    
    add(centerPanel);
  }
  
  public static class TracTipsReportData {
    private String employeeID;
    private String employeeName;
    private double grossSales;
    private double creditCardSalesWithoutTips;
    private double creditCardTips;
    private double tracCreditCardTipsRate;
    private double tracCashSalesWithoutTips;
    private double tracCashTips;
    private double tracCashTipsRate;
    private double tipsPaid;
    private double netTips;
    private double declareTips;
    
    public TracTipsReportData() {}
    
    public String getEmployeeID() { return employeeID; }
    
    public void setEmployeeID(String employeeID)
    {
      this.employeeID = employeeID;
    }
    
    public String getEmployeeName() {
      return employeeName;
    }
    
    public void setEmployeeName(String employeeName) {
      this.employeeName = employeeName;
    }
    
    public double getGrossSales() {
      return grossSales;
    }
    
    public void setGrossSales(double grossSales) {
      this.grossSales = grossSales;
    }
    
    public double getCreditCardTips() {
      return creditCardTips;
    }
    
    public void setCreditCardTips(double creditCardTips) {
      this.creditCardTips = creditCardTips;
    }
    
    public double getTracCreditCardTipsRate() {
      return tracCreditCardTipsRate;
    }
    
    public void setTracCreditCardTipsRate(double trackCreditCardTipsRate) {
      tracCreditCardTipsRate = trackCreditCardTipsRate;
    }
    
    public double getTracCashTips() {
      return tracCashTips;
    }
    
    public void setTracCashTips(double trackCashTips) {
      tracCashTips = trackCashTips;
    }
    
    public double getTracCashTipsRate() {
      return tracCashTipsRate;
    }
    
    public void setTracCashTipsRate(double trackCashTipsRate) {
      tracCashTipsRate = trackCashTipsRate;
    }
    
    public double getTipsPaid() {
      return tipsPaid;
    }
    
    public void setTipsPaid(double tipsPaid) {
      this.tipsPaid = tipsPaid;
    }
    
    public double getNetTips() {
      return netTips;
    }
    
    public void setNetTips(double netTips) {
      this.netTips = netTips;
    }
    
    public double getCreditCardSalesWithoutTips() {
      return creditCardSalesWithoutTips;
    }
    
    public void setCreditCardSalesWithoutTips(double creditCardSalesWithoutTips) {
      this.creditCardSalesWithoutTips = creditCardSalesWithoutTips;
    }
    
    public double getTracCashSalesWithoutTips() {
      return tracCashSalesWithoutTips;
    }
    
    public void setTracCashSalesWithoutTips(double trackCashSalesWithoutTips) {
      tracCashSalesWithoutTips = trackCashSalesWithoutTips;
    }
    
    public double getDeclareTips() {
      return declareTips;
    }
    
    public void setDeclareTips(double declareTips) {
      this.declareTips = declareTips;
    }
  }
  
  private boolean hasTransactions(Session session, Date fromDate, Date toDate, User user)
  {
    Criteria criteria = session.createCriteria(PosTransaction.class);
    criteria.add(Restrictions.between(PosTransaction.PROP_TRANSACTION_TIME, fromDate, toDate));
    criteria.add(Restrictions.eq(PosTransaction.PROP_USER_ID, user == null ? null : user.getId()));
    
    criteria.setProjection(Projections.rowCount());
    Number rowCount = (Number)criteria.uniqueResult();
    if (rowCount != null) {
      return rowCount.intValue() > 0;
    }
    
    return false;
  }
  
  private boolean hasDeclaredTips(Session session, Date fromDate, Date toDate, User user) {
    Criteria criteria = session.createCriteria(DeclaredTips.class);
    criteria.add(Restrictions.between(DeclaredTips.PROP_DECLARED_TIME, fromDate, toDate));
    criteria.add(Restrictions.eq(DeclaredTips.PROP_OWNER_ID, user.getId()));
    
    criteria.setProjection(Projections.rowCount());
    Number rowCount = (Number)criteria.uniqueResult();
    if (rowCount != null) {
      return rowCount.intValue() > 0;
    }
    
    return false;
  }
  
  private void calculateGrossSales(TracTipsReportData reportData, Session session, Date fromDate, Date toDate, User user) {
    Criteria criteria = session.createCriteria(PosTransaction.class);
    criteria.add(Restrictions.between(PosTransaction.PROP_TRANSACTION_TIME, fromDate, toDate));
    criteria.add(Restrictions.eq(PosTransaction.PROP_USER_ID, user == null ? null : user.getId()));
    
    criteria.setProjection(Projections.sum(PosTransaction.PROP_AMOUNT));
    
    Double grossSales = (Double)criteria.uniqueResult();
    if (grossSales != null) {
      reportData.setGrossSales(grossSales.doubleValue());
    }
  }
  
  private void calculateCreditCardSales(TracTipsReportData reportData, Session session, Date fromDate, Date toDate, User user) {
    Criteria criteria = session.createCriteria(CreditCardTransaction.class);
    criteria.add(Restrictions.between(PosTransaction.PROP_TRANSACTION_TIME, fromDate, toDate));
    criteria.add(Restrictions.eq(PosTransaction.PROP_USER_ID, user == null ? null : user.getId()));
    
    criteria.setProjection(Projections.sum(PosTransaction.PROP_AMOUNT));
    Double totalCreditCardAmount = (Double)criteria.uniqueResult();
    criteria.setProjection(null);
    criteria.setProjection(Projections.sum(PosTransaction.PROP_TIPS_AMOUNT));
    
    Double creditCardTipsAmount = (Double)criteria.uniqueResult();
    if (totalCreditCardAmount == null) {
      totalCreditCardAmount = Double.valueOf(0.0D);
    }
    if (creditCardTipsAmount == null) {
      creditCardTipsAmount = Double.valueOf(0.0D);
    }
    double creditCardWithoutTips = totalCreditCardAmount.doubleValue() - creditCardTipsAmount.doubleValue();
    
    Criteria criteria2 = session.createCriteria(DebitCardTransaction.class);
    criteria2.add(Restrictions.between(PosTransaction.PROP_TRANSACTION_TIME, fromDate, toDate));
    criteria2.add(Restrictions.eq(PosTransaction.PROP_USER_ID, user == null ? null : user.getId()));
    
    criteria2.setProjection(Projections.sum(PosTransaction.PROP_AMOUNT));
    Double totalDebitCardAmount = (Double)criteria2.uniqueResult();
    criteria2.setProjection(null);
    criteria2.setProjection(Projections.sum(PosTransaction.PROP_TIPS_AMOUNT));
    Double debitCardTipsAmount = (Double)criteria2.uniqueResult();
    if (totalDebitCardAmount == null) {
      totalDebitCardAmount = Double.valueOf(0.0D);
    }
    if (debitCardTipsAmount == null) {
      debitCardTipsAmount = Double.valueOf(0.0D);
    }
    double debitCardWithoutTips = totalDebitCardAmount.doubleValue() - debitCardTipsAmount.doubleValue();
    
    double totalCardSalsesWithoutTips = creditCardWithoutTips + debitCardWithoutTips;
    
    reportData.setCreditCardTips(creditCardTipsAmount.doubleValue() + debitCardTipsAmount.doubleValue());
    if ((creditCardTipsAmount.doubleValue() <= 0.0D) || (totalCreditCardAmount.doubleValue() <= 0.0D)) {
      reportData.setTracCreditCardTipsRate(0.0D);
    }
    if (totalCardSalsesWithoutTips > 0.0D) {
      reportData.setTracCreditCardTipsRate(reportData.getCreditCardTips() * 100.0D / totalCardSalsesWithoutTips);
    }
    reportData.setCreditCardSalesWithoutTips(totalCardSalsesWithoutTips);
  }
  
  private void calculateCashTotalWithoutTips(TracTipsReportData reportData, Session session, Date fromDate, Date toDate, User user) {
    Criteria criteria = session.createCriteria(CashTransaction.class);
    criteria.add(Restrictions.between(PosTransaction.PROP_TRANSACTION_TIME, fromDate, toDate));
    criteria.add(Restrictions.eq(PosTransaction.PROP_USER_ID, user == null ? null : user.getId()));
    
    criteria.setProjection(Projections.sum(CashTransaction.PROP_AMOUNT));
    Double totalCashAmount = (Double)criteria.uniqueResult();
    criteria.setProjection(null);
    criteria.setProjection(Projections.sum(PosTransaction.PROP_TIPS_AMOUNT));
    Double cashTipsAmount = (Double)criteria.uniqueResult();
    if (totalCashAmount == null) {
      totalCashAmount = Double.valueOf(0.0D);
    }
    if (cashTipsAmount == null) {
      cashTipsAmount = Double.valueOf(0.0D);
    }
    double cashAmountWithoutTips = totalCashAmount.doubleValue() - cashTipsAmount.doubleValue();
    
    reportData.setTracCashTips(cashTipsAmount.doubleValue());
    if ((cashTipsAmount.doubleValue() <= 0.0D) || (totalCashAmount.doubleValue() <= 0.0D)) {
      reportData.setTracCashTipsRate(0.0D);
    }
    if (totalCashAmount.doubleValue() > 0.0D)
      reportData.setTracCashTipsRate(cashTipsAmount.doubleValue() * 100.0D / totalCashAmount.doubleValue());
    reportData.setTracCashSalesWithoutTips(cashAmountWithoutTips);
  }
  
  private void calculateDeclareTips(TracTipsReportData reportData, Session session, Date fromDate, Date toDate, User user) {
    Criteria criteria = session.createCriteria(DeclaredTips.class);
    criteria.add(Restrictions.between(DeclaredTips.PROP_DECLARED_TIME, fromDate, toDate));
    criteria.add(Restrictions.eq(DeclaredTips.PROP_OWNER_ID, user.getId()));
    criteria.setProjection(Projections.sum(DeclaredTips.PROP_AMOUNT));
    
    Double declareTips = (Double)criteria.uniqueResult();
    if (declareTips != null) {
      reportData.setDeclareTips(declareTips.doubleValue());
    }
  }
}
