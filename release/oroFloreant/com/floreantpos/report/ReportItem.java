package com.floreantpos.report;

import com.floreantpos.model.TicketItemTax;
import java.util.List;




















public class ReportItem
{
  private String id;
  private String name;
  private double price;
  private double adjustedPrice;
  private double cost;
  private double quantity;
  private double taxRate;
  private double netTotal;
  private double grossTotal;
  private double taxTotal;
  private double discount;
  private String groupName;
  private String barcode;
  
  public ReportItem() {}
  
  public String getName()
  {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public double getQuantity() {
    return quantity;
  }
  
  public void setQuantity(double quantity) {
    this.quantity = quantity;
  }
  
  public double getTaxRate() {
    return taxRate;
  }
  
  public void setTaxRate(double taxRate) {
    this.taxRate = taxRate;
  }
  

  public void setTaxRate(List<TicketItemTax> taxes) {}
  
  public double getNetTotal()
  {
    return netTotal;
  }
  
  public void setNetTotal(double total) {
    netTotal = total;
  }
  
  public double getPrice() {
    return price;
  }
  
  public void setPrice(double price) {
    this.price = price;
  }
  
  public String getId() {
    return id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public double getDiscount() {
    return discount;
  }
  
  public void setDiscount(double discount) {
    this.discount = discount;
  }
  
  public double getGrossTotal() {
    return grossTotal;
  }
  
  public void setGrossTotal(double netTotal) {
    grossTotal = netTotal;
  }
  
  public double getTaxTotal() {
    return taxTotal;
  }
  
  public void setTaxTotal(double taxTotal) {
    this.taxTotal = taxTotal;
  }
  
  public String getBarcode() {
    return barcode;
  }
  
  public void setBarcode(String barcode) {
    this.barcode = barcode;
  }
  
  public String getGroupName() {
    return groupName;
  }
  
  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }
  
  public double getCost() {
    return cost;
  }
  
  public void setCost(double cost) {
    this.cost = cost;
  }
  
  public double getAdjustedPrice() {
    return adjustedPrice;
  }
  
  public void setAdjustedPrice(double adjustedPrice) {
    this.adjustedPrice = adjustedPrice;
  }
}
