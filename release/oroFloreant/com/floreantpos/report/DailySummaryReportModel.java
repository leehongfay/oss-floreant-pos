package com.floreantpos.report;

import com.floreantpos.swing.ListTableModel;
import java.util.List;

























public class DailySummaryReportModel
  extends ListTableModel
{
  private String[] columnNames = { "period", "sessionID", "openingBalance", "deposits", "outage", "cash", "tips", "onAcct", "otherPmt", "payInOut", "voids", "totalGuest", "avgChk" };
  
  public DailySummaryReportModel()
  {
    setColumnNames(columnNames);
  }
  
  public DailySummaryReportModel(List rows) {
    setColumnNames(columnNames);
    setRows(rows);
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    DailySummaryReportView.DailySummaryReportData reportData = (DailySummaryReportView.DailySummaryReportData)rows.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      return reportData.getPeriod();
    
    case 1: 
      return reportData.getSessionID();
    
    case 2: 
      return Double.valueOf(reportData.getOpeningBalance());
    
    case 3: 
      return Double.valueOf(reportData.getDeposit());
    
    case 4: 
      return Double.valueOf(reportData.getOverOutage());
    
    case 5: 
      return Double.valueOf(reportData.getCash());
    
    case 6: 
      return Double.valueOf(reportData.getTips());
    
    case 7: 
      return Double.valueOf(reportData.getOnAccount());
    
    case 8: 
      return Double.valueOf(reportData.getOtherPayment());
    
    case 9: 
      return Double.valueOf(reportData.getPayInOut());
    
    case 10: 
      return Double.valueOf(reportData.getVoids());
    
    case 11: 
      return Integer.valueOf(reportData.getTotalGuest());
    
    case 12: 
      return Double.valueOf(reportData.getAvgChk());
    }
    return null;
  }
}
