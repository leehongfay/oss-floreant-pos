package com.floreantpos.report;




public class PurchaseReportData
{
  private String item;
  

  private String description;
  

  private int quantity;
  

  private double price;
  

  private double total;
  


  public PurchaseReportData() {}
  


  public String getItem()
  {
    return item;
  }
  
  public void setItem(String item) {
    this.item = item;
  }
  
  public String getDescription() {
    return description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }
  
  public int getQuantity() {
    return quantity;
  }
  
  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }
  
  public double getPrice() {
    return price;
  }
  
  public void setPrice(double price) {
    this.price = price;
  }
  
  public double getTotal() {
    return total;
  }
  
  public void setTotal(double total) {
    this.total = total;
  }
}
