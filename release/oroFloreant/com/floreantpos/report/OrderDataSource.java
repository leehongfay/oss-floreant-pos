package com.floreantpos.report;

import com.floreantpos.model.PurchaseOrder;
import com.floreantpos.model.PurchaseOrderItem;
import java.util.ArrayList;
import java.util.List;
















public class OrderDataSource
  extends AbstractReportDataSource
{
  public OrderDataSource(PurchaseOrder order)
  {
    super(new String[] { "itemSL", "itemImage", "itemName", "itemQty", "unit", "itemPrice", "itemSubtotal" });
    setOrder(order);
  }
  
  private void setOrder(PurchaseOrder order) {
    ArrayList<PurchaseOrderItem> rows = new ArrayList();
    rows.addAll(order.getOrderItems());
    setRows(rows);
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    PurchaseOrderItem item = (PurchaseOrderItem)rows.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      return String.valueOf(rowIndex + 1);
    
    case 1: 
      return null;
    case 2: 
      return item.getName();
    
    case 3: 
      return item.getQuantityDisplay();
    
    case 4: 
      return item.getItemUnitName();
    
    case 5: 
      return item.getPriceDisplay();
    
    case 6: 
      return item.getTotalDisplay();
    }
    
    return null;
  }
}
