package com.floreantpos;










public class StoreAlreadyOpenException
  extends RuntimeException
{
  public StoreAlreadyOpenException() {}
  








  public StoreAlreadyOpenException(String arg0)
  {
    super(arg0);
  }
  
  public StoreAlreadyOpenException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }
  
  public StoreAlreadyOpenException(Throwable arg0) {
    super(arg0);
  }
}
