package com.floreantpos.webservice;

import com.floreantpos.Database;
import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.config.AppConfig;
import com.floreantpos.main.Application;
import com.floreantpos.model.Address;
import com.floreantpos.model.Currency;
import com.floreantpos.model.Department;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.ModifierGroup;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Outlet;
import com.floreantpos.model.PriceRule;
import com.floreantpos.model.PriceShift;
import com.floreantpos.model.PriceTable;
import com.floreantpos.model.PriceTableItem;
import com.floreantpos.model.SalesArea;
import com.floreantpos.model.Shift;
import com.floreantpos.model.Store;
import com.floreantpos.model.StoreSessionControl;
import com.floreantpos.model.Tax;
import com.floreantpos.model.TaxGroup;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.dao.AddressDAO;
import com.floreantpos.model.dao.CurrencyDAO;
import com.floreantpos.model.dao.DepartmentDAO;
import com.floreantpos.model.dao.MenuCategoryDAO;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.model.dao.MenuModifierDAO;
import com.floreantpos.model.dao.ModifierGroupDAO;
import com.floreantpos.model.dao.OrderTypeDAO;
import com.floreantpos.model.dao.OutletDAO;
import com.floreantpos.model.dao.PriceRuleDAO;
import com.floreantpos.model.dao.PriceShiftDAO;
import com.floreantpos.model.dao.PriceTableDAO;
import com.floreantpos.model.dao.PriceTableItemDAO;
import com.floreantpos.model.dao.SalesAreaDAO;
import com.floreantpos.model.dao.ShiftDAO;
import com.floreantpos.model.dao.StoreDAO;
import com.floreantpos.model.dao.TaxDAO;
import com.floreantpos.model.dao.TaxGroupDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.swing.POSPasswordField;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.StoreUtil;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.SwingWorker.StateValue;
import javax.ws.rs.core.GenericEntity;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;


















public class CloudSyncDialog
  extends POSDialog
  implements ActionListener
{
  private static final String UPLOAD = "CD";
  private static final String DOWNLOAD = "UD";
  private static final String SAVE = "SAVE";
  private static final String CANCEL = "cancel";
  private static final String TEST = "test";
  private POSTextField tfServerAddress;
  private POSTextField tfStoreId;
  private POSTextField tfUserName;
  private POSPasswordField tfPassword;
  private PosButton btnTestConnection;
  private PosButton btnUpload;
  private PosButton btnDownload;
  private JButton btnCancel;
  private PosButton btnSave;
  private JComboBox databaseCombo;
  private TitlePanel titlePanel;
  private JLabel lblServerAddress;
  private JLabel lblStoreId;
  private JLabel lblUserName;
  private JLabel lblUserPassword;
  private boolean connectionSuccess;
  private JLabel lblConnectionStatus;
  private JProgressBar pbSync;
  private JLabel progressStatus;
  private JTabbedPane tabbedPane;
  private JPanel configPanel;
  
  public CloudSyncDialog()
    throws HeadlessException
  {
    super(POSUtil.getBackOfficeWindow());
    setFieldValues();
    addUIListeners();
  }
  
  public void setVisible(boolean b)
  {
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        CloudSyncDialog.this.checkConnection(false);
      }
    });
    super.setVisible(b);
  }
  
  protected void initUI() {
    tabbedPane = new JTabbedPane();
    JPanel cloudTabPanel = new JPanel(new MigLayout("inset 5 20 20 20,fill,hidemode 3"));
    
    JPanel cloudButtonPanel = new JPanel();
    cloudButtonPanel.setLayout(new MigLayout("fill"));
    
    cloudTabPanel.add(cloudButtonPanel, "growx,span");
    tabbedPane.addTab("Download/Upload", cloudTabPanel);
    
    lblConnectionStatus = new JLabel();
    lblConnectionStatus.setFont(new Font("Arial", 1, 20));
    
    pbSync = new JProgressBar();
    pbSync.setValue(0);
    pbSync.setMaximum(100);
    pbSync.setStringPainted(true);
    pbSync.setPreferredSize(new Dimension(0, 30));
    
    progressStatus = new JLabel("Click download/upload to sync with server.");
    pbSync.setVisible(false);
    
    btnCancel = new JButton(Messages.getString("DatabaseConfigurationDialog.28").toUpperCase());
    btnCancel.setActionCommand("cancel");
    
    cloudTabPanel.add(new JSeparator(), "newline,grow");
    cloudTabPanel.add(progressStatus, "right,newline,split 2");
    cloudTabPanel.add(pbSync, "grow");
    cloudTabPanel.add(new JLabel("Connection Status:"), "newline,split 3");
    cloudTabPanel.add(lblConnectionStatus);
    this.lblStoreId = new JLabel("Store Id:");
    this.lblStoreId.setFont(new Font("Arial", 1, 16));
    this.lblStoreId.setForeground(Color.GRAY);
    cloudTabPanel.add(lblConnectionStatus, "grow");
    cloudTabPanel.add(this.lblStoreId, "right,grow");
    cloudTabPanel.add(new JSeparator(), "newline,grow");
    cloudTabPanel.add(btnCancel, "newline,center,span");
    
    configPanel = new JPanel();
    configPanel.setLayout(new MigLayout("fill", "[][fill, grow]", ""));
    

    titlePanel = new TitlePanel();
    tfServerAddress = new POSTextField();
    tfStoreId = new POSTextField();
    tfUserName = new POSTextField();
    tfPassword = new POSPasswordField();
    databaseCombo = new JComboBox(Database.values());
    
    String databaseProviderName = AppConfig.getDatabaseProviderName();
    if (StringUtils.isNotEmpty(databaseProviderName)) {
      databaseCombo.setSelectedItem(Database.getByProviderName(databaseProviderName));
    }
    updateTitle();
    configPanel.add(titlePanel, "span, grow, wrap");
    
    lblServerAddress = new JLabel("Server Address:");
    configPanel.add(lblServerAddress);
    configPanel.add(tfServerAddress, "grow, wrap");
    JLabel lblStoreId = new JLabel("Store Id:");
    lblUserName = new JLabel("Username:");
    configPanel.add(new JLabel("Store Id:"));
    configPanel.add(tfStoreId, "grow, wrap");
    configPanel.add(lblUserName);
    configPanel.add(tfUserName, "grow, wrap");
    lblUserPassword = new JLabel("Password:");
    configPanel.add(lblUserPassword);
    configPanel.add(tfPassword, "grow, wrap");
    configPanel.add(new JSeparator(), "span, grow, gaptop 10");
    
    btnTestConnection = new PosButton(Messages.getString("DatabaseConfigurationDialog.26").toUpperCase());
    btnTestConnection.setActionCommand("test");
    btnSave = new PosButton(Messages.getString("DatabaseConfigurationDialog.27").toUpperCase());
    btnSave.setActionCommand("SAVE");
    
    JPanel buttonPanel = new JPanel(new MigLayout("inset 0, fill", "grow", ""));
    
    btnUpload = new PosButton("UPLOAD");
    btnUpload.setActionCommand("CD");
    
    btnDownload = new PosButton("DOWNLOAD");
    btnDownload.setActionCommand("UD");
    btnDownload.setIcon(IconFactory.getIcon("/ui_icons/", "page-down.png"));
    btnUpload.setIcon(IconFactory.getIcon("/ui_icons/", "page-up.png"));
    
    JButton btnSetting = new JButton(IconFactory.getIcon("/ui_icons/", "backoffice.png"));
    btnSetting.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        POSDialog dialog = new POSDialog(POSUtil.getFocusedWindow());
        dialog.add(configPanel);
        dialog.setSize(getSize());
        dialog.open();
      }
      
    });
    btnDownload.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        final CloudSyncDialog.Worker worker = new CloudSyncDialog.Worker(CloudSyncDialog.this);
        worker.setActionCommand("UD");
        worker.addPropertyChangeListener(new PropertyChangeListener()
        {
          public void propertyChange(PropertyChangeEvent pcEvt)
          {
            if ("progress".equals(pcEvt.getPropertyName())) {
              pbSync.setValue(((Integer)pcEvt.getNewValue()).intValue());
            }
            else if (pcEvt.getNewValue() == SwingWorker.StateValue.DONE) {
              try {
                worker.get();
              } catch (InterruptedException|ExecutionException e) {
                e.printStackTrace();
              }
              
            }
          }
        });
        worker.execute();
      }
      
    });
    btnUpload.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        final CloudSyncDialog.Worker worker = new CloudSyncDialog.Worker(CloudSyncDialog.this);
        worker.setActionCommand("CD");
        worker.addPropertyChangeListener(new PropertyChangeListener()
        {
          public void propertyChange(PropertyChangeEvent pcEvt)
          {
            if ("progress".equals(pcEvt.getPropertyName())) {
              pbSync.setValue(((Integer)pcEvt.getNewValue()).intValue());
            }
            else if (pcEvt.getNewValue() == SwingWorker.StateValue.DONE) {
              try {
                worker.get();
              } catch (InterruptedException|ExecutionException e) {
                e.printStackTrace();
              }
              
            }
          }
        });
        worker.execute();
      }
    });
    cloudButtonPanel.add(btnSetting, "right,wrap");
    cloudButtonPanel.add(btnDownload, "split 2,gaptop 20,center");
    cloudButtonPanel.add(btnUpload);
    PosButton btnExit = new PosButton("EXIT");
    btnExit.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        CloudSyncDialog.this.closeConfigDialog();
      }
    });
    cloudButtonPanel.add(btnExit);
    
    buttonPanel.add(btnTestConnection);
    buttonPanel.add(btnSave);
    buttonPanel.add(btnExit);
    
    configPanel.add(buttonPanel, "span,right");
    
    add(tabbedPane);
  }
  
  private void closeConfigDialog() {
    Window windowAncestor = SwingUtilities.getWindowAncestor(configPanel);
    if ((windowAncestor instanceof POSDialog)) {
      ((POSDialog)windowAncestor).setCanceled(false);
      windowAncestor.dispose();
    }
  }
  
  private void addUIListeners() {
    btnTestConnection.addActionListener(this);
    btnUpload.addActionListener(this);
    btnSave.addActionListener(this);
    btnCancel.addActionListener(this);
    btnDownload.addActionListener(this);
  }
  
  private void setFieldValues() {
    Store store = Application.getInstance().getStore();
    tfServerAddress.setText(store.getProperty("web.service.url"));
    tfStoreId.setText(store.getProperty("web.service.schema"));
    tfUserName.setText(store.getProperty("web.service.username"));
    tfPassword.setText(store.getProperty("web.service.password"));
  }
  
  public void actionPerformed(ActionEvent e) {
    try {
      String command = e.getActionCommand();
      
      if ("cancel".equalsIgnoreCase(command)) {
        dispose();
        return;
      }
      setCursor(Cursor.getPredefinedCursor(3));
      if ("test".equalsIgnoreCase(command)) {
        connectionSuccess = checkConnection(false);
        if (!connectionSuccess) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("DatabaseConfigurationDialog.32"));
        }
        else {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("DatabaseConfigurationDialog.31"));
        }
      }
      else if ("SAVE".equalsIgnoreCase(command)) {
        checkConnection(true);
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Successfully saved.");
        closeConfigDialog();
      }
    } catch (Exception e2) {
      e2.printStackTrace();
      POSMessageDialog.showMessage(this, e2.getMessage());
    } finally {
      setCursor(Cursor.getDefaultCursor());
    }
  }
  
  public void updateTitle() {
    super.setTitle("Sync with cloud");
    titlePanel.setTitle("Please enter server information.");
  }
  
  private boolean checkConnection(boolean updateStore)
  {
    String service_url = tfServerAddress.getText();
    String storeId = tfStoreId.getText();
    String userName = tfUserName.getText();
    String password = new String(tfPassword.getPassword());
    
    if (updateStore) {
      Store store = Application.getInstance().getStore();
      store.addProperty("web.service.url", service_url);
      store.addProperty("web.service.schema", storeId);
      store.addProperty("web.service.username", userName);
      store.addProperty("web.service.password", password);
      StoreDAO.getInstance().saveOrUpdate(store);
    }
    
    String status = PosWebService.checkConnection(service_url, storeId, userName, password);
    String[] statusInfo = status.split(",");
    if (statusInfo.length > 0) {
      if (statusInfo[0].equals("success")) {
        if ((updateStore) && (statusInfo.length > 1)) {
          System.out.println("Connected");
        }
        lblConnectionStatus.setText("Connected");
        lblConnectionStatus.setForeground(Color.GREEN);
        lblStoreId.setText("Store Id: " + storeId);
        return true;
      }
      lblConnectionStatus.setText(statusInfo[1]);
      lblConnectionStatus.setForeground(Color.RED);
    }
    return false;
  }
  
  private void saveData() {
    try {
      setCursor(Cursor.getPredefinedCursor(3));
      try {
        pbSync.setVisible(true);
        pbSync.setValue(0);
        progressStatus.setForeground(Color.BLUE);
        
        progressStatus.setText("Downloading taxes..");
        List taxes = PosWebService.getTaxes();
        for (Iterator iterator = taxes.iterator(); iterator.hasNext();) {
          Tax item = (Tax)iterator.next();
          TaxDAO dao = TaxDAO.getInstance();
          Tax existingItem = dao.get(item.getId());
          if (existingItem != null) {
            id = existingItem.getId();
            long version = existingItem.getVersion();
            PropertyUtils.copyProperties(existingItem, item);
            existingItem.setId(id);
            existingItem.setVersion(version);
            dao.update(existingItem);
          }
          else {
            item.setVersion(0L);
            dao.save(item);
          } }
        String id;
        pbSync.setValue(5);
        List taxGroups = PosWebService.getTaxGroups();
        for (Iterator iterator = taxGroups.iterator(); iterator.hasNext();) {
          TaxGroup item = (TaxGroup)iterator.next();
          List<Tax> existingsTax = new ArrayList();
          if (item.getTaxes() != null) {
            for (Tax tax : item.getTaxes()) {
              Tax existingTax = TaxDAO.getInstance().get(tax.getId());
              if (existingsTax != null)
              {
                existingsTax.add(existingTax); }
            }
          }
          item.setTaxes(existingsTax);
          TaxGroupDAO dao = TaxGroupDAO.getInstance();
          TaxGroup existingItem = dao.get(item.getId());
          if (existingItem != null) {
            String id = existingItem.getId();
            long version = existingItem.getVersion();
            PropertyUtils.copyProperties(existingItem, item);
            existingItem.setId(id);
            existingItem.setVersion(version);
            dao.update(existingItem);
          }
          else {
            item.setVersion(0L);
            dao.save(item);
          }
        }
        pbSync.setValue(10);
        progressStatus.setText("Downloading currencies..");
        
        List currencyList = PosWebService.getCurrencies();
        for (Iterator iterator = currencyList.iterator(); iterator.hasNext();) {
          Currency item = (Currency)iterator.next();
          CurrencyDAO dao = CurrencyDAO.getInstance();
          Currency existingItem = dao.get(item.getId());
          if (existingItem != null) {
            String id = existingItem.getId();
            long version = existingItem.getVersion();
            PropertyUtils.copyProperties(existingItem, item);
            existingItem.setId(id);
            existingItem.setVersion(version);
            dao.update(existingItem);
          }
          else {
            item.setVersion(0L);
            dao.save(item);
          }
        }
        pbSync.setValue(15);
        progressStatus.setText("Downloading outlets..");
        
        List outletList = PosWebService.getOutlets();
        for (Iterator iterator = outletList.iterator(); iterator.hasNext();) {
          Outlet item = (Outlet)iterator.next();
          OutletDAO dao = OutletDAO.getInstance();
          Outlet existingItem = dao.get(item.getId());
          if (existingItem != null) {
            String id = existingItem.getId();
            long version = existingItem.getVersion();
            PropertyUtils.copyProperties(existingItem, item);
            existingItem.setId(id);
            existingItem.setVersion(version);
            dao.update(existingItem);
          }
          else {
            item.setVersion(0L);
            dao.save(item);
          }
        }
        pbSync.setValue(20);
        progressStatus.setText("Downloading departments..");
        
        List departments = PosWebService.getDepartments();
        for (Iterator iterator = departments.iterator(); iterator.hasNext();) {
          Department item = (Department)iterator.next();
          DepartmentDAO dao = DepartmentDAO.getInstance();
          Department existingItem = dao.get(item.getId());
          if (existingItem != null) {
            String id = existingItem.getId();
            long version = existingItem.getVersion();
            PropertyUtils.copyProperties(existingItem, item);
            existingItem.setId(id);
            existingItem.setVersion(version);
            dao.update(existingItem);
          }
          else {
            item.setVersion(0L);
            dao.save(item);
          }
        }
        pbSync.setValue(25);
        progressStatus.setText("Downloading sales area..");
        
        List salesAreaList = PosWebService.getSalesArea();
        for (Iterator iterator = salesAreaList.iterator(); iterator.hasNext();) {
          SalesArea item = (SalesArea)iterator.next();
          SalesAreaDAO dao = SalesAreaDAO.getInstance();
          SalesArea existingItem = dao.get(item.getId());
          if (existingItem != null) {
            String id = existingItem.getId();
            long version = existingItem.getVersion();
            PropertyUtils.copyProperties(existingItem, item);
            existingItem.setId(id);
            existingItem.setVersion(version);
            dao.update(existingItem);
          }
          else {
            item.setVersion(0L);
            dao.save(item);
          }
        }
        pbSync.setValue(30);
        progressStatus.setText("Downloading order types..");
        
        List ordertypeList = PosWebService.getOrderTypes();
        for (Iterator iterator = ordertypeList.iterator(); iterator.hasNext();) {
          OrderType item = (OrderType)iterator.next();
          OrderTypeDAO dao = OrderTypeDAO.getInstance();
          OrderType existingItem = dao.get(item.getId());
          if (existingItem != null) {
            String id = existingItem.getId();
            long version = existingItem.getVersion();
            PropertyUtils.copyProperties(existingItem, item);
            existingItem.setId(id);
            existingItem.setVersion(version);
            dao.update(existingItem);
          }
          else {
            item.setVersion(0L);
            dao.save(item);
          }
        }
        pbSync.setValue(40);
        progressStatus.setText("Downloading terminal type..");
        







































































































        pbSync.setValue(65);
        progressStatus.setText("Downloading menu categories..");
        List menuCategories = PosWebService.getMenuCategories();
        for (Iterator iterator = menuCategories.iterator(); iterator.hasNext();) {
          MenuCategory item = (MenuCategory)iterator.next();
          MenuCategoryDAO dao = MenuCategoryDAO.getInstance();
          MenuCategory existingItem = dao.get(item.getId());
          if (existingItem != null) {
            String id = existingItem.getId();
            long version = existingItem.getVersion();
            PropertyUtils.copyProperties(existingItem, item);
            existingItem.setId(id);
            existingItem.setVersion(version);
            dao.update(existingItem);
          }
          else {
            item.setVersion(0L);
            dao.save(item);
          }
        }
        pbSync.setValue(70);
        progressStatus.setText("Downloading menu groups..");
        List menuGroups = PosWebService.getMenuGroups();
        for (Iterator iterator = menuGroups.iterator(); iterator.hasNext();) {
          MenuGroup item = (MenuGroup)iterator.next();
          MenuGroupDAO dao = MenuGroupDAO.getInstance();
          MenuGroup existingItem = dao.get(item.getId());
          if (existingItem != null) {
            String id = existingItem.getId();
            long version = existingItem.getVersion();
            PropertyUtils.copyProperties(existingItem, item);
            existingItem.setId(id);
            existingItem.setVersion(version);
            dao.update(existingItem);
          }
          else {
            item.setVersion(0L);
            dao.save(item);
          }
        }
        pbSync.setValue(75);
        progressStatus.setText("Downloading menu items..");
        List menuItems = PosWebService.getMenuItems();
        for (Iterator iterator = menuItems.iterator(); iterator.hasNext();) {
          MenuItem item = (MenuItem)iterator.next();
          MenuItemDAO dao = MenuItemDAO.getInstance();
          MenuItem existingItem = dao.get(item.getId());
          if (existingItem != null) {
            String id = existingItem.getId();
            long version = existingItem.getVersion();
            PropertyUtils.copyProperties(existingItem, item);
            existingItem.setId(id);
            existingItem.setVersion(version);
            dao.update(existingItem);
          }
          else {
            item.setVersion(0L);
            dao.save(item);
          }
        }
        pbSync.setValue(80);
        progressStatus.setText("Downloading modifier groups..");
        List modifierGroups = PosWebService.getModifierGroups();
        for (Iterator iterator = modifierGroups.iterator(); iterator.hasNext();) {
          ModifierGroup item = (ModifierGroup)iterator.next();
          ModifierGroupDAO dao = ModifierGroupDAO.getInstance();
          ModifierGroup existingItem = dao.get(item.getId());
          if (existingItem != null) {
            String id = existingItem.getId();
            long version = existingItem.getVersion();
            PropertyUtils.copyProperties(existingItem, item);
            existingItem.setId(id);
            existingItem.setVersion(version);
            dao.update(existingItem);
          }
          else {
            item.setVersion(0L);
            dao.save(item);
          }
        }
        pbSync.setValue(85);
        progressStatus.setText("Downloading modifiers..");
        List menuModifiers = PosWebService.getMenuModifiers();
        for (Iterator iterator = menuModifiers.iterator(); iterator.hasNext();) {
          MenuModifier item = (MenuModifier)iterator.next();
          MenuModifierDAO dao = MenuModifierDAO.getInstance();
          MenuModifier existingItem = dao.get(item.getId());
          if (existingItem != null) {
            String id = existingItem.getId();
            long version = existingItem.getVersion();
            PropertyUtils.copyProperties(existingItem, item);
            existingItem.setId(id);
            existingItem.setVersion(version);
            dao.update(existingItem);
          }
          else {
            item.setVersion(0L);
            dao.save(item);
          }
        }
        pbSync.setValue(90);
        
        progressStatus.setText("Downloading price list..");
        List priceShifts = PosWebService.getPriceShift();
        for (Iterator iterator = priceShifts.iterator(); iterator.hasNext();) {
          PriceShift item = (PriceShift)iterator.next();
          PriceShiftDAO dao = PriceShiftDAO.getInstance();
          
          Shift existingItem = ShiftDAO.getInstance().findByName(item.getName());
          if (existingItem == null) {
            dao.save(item);
          }
        }
        List priceList = PosWebService.getPriceList();
        for (Iterator iterator = priceList.iterator(); iterator.hasNext();) {
          PriceTable item = (PriceTable)iterator.next();
          PriceTableDAO dao = PriceTableDAO.getInstance();
          
          existingItem = PriceTableDAO.getInstance().get(item.getId());
          if (existingItem != null) {
            String id = existingItem.getId();
            version = existingItem.getVersion();
            PropertyUtils.copyProperties(existingItem, item);
            existingItem.setId(id);
            existingItem.setVersion(version);
            
            dao.update(existingItem);
          }
          else {
            item.setVersion(0L);
            dao.save(item);
          }
          List<PriceTableItem> priceTableItems = item.getPriceTableItems();
          if (priceTableItems != null)
            for (PriceTableItem priceTableItem : priceTableItems) {
              PriceTableItem existingPriceTableItem = PriceTableItemDAO.getInstance().get(priceTableItem.getId());
              if (existingPriceTableItem != null) {
                String id = existingPriceTableItem.getId();
                long version = existingItem.getVersion();
                PropertyUtils.copyProperties(existingPriceTableItem, priceTableItem);
                existingPriceTableItem.setId(id);
                existingPriceTableItem.setVersion(version);
                PriceTableItemDAO.getInstance().update(existingPriceTableItem);
              }
              else {
                priceTableItem.setVersion(0L);
                PriceTableItemDAO.getInstance().save(priceTableItem);
              }
            } }
        PriceTable existingItem;
        long version;
        List priceRules = PosWebService.getPriceRules();
        for (Iterator iterator = priceRules.iterator(); iterator.hasNext();) {
          PriceRule item = (PriceRule)iterator.next();
          Shift priceShift = item.getPriceShift();
          if (priceShift != null)
            item.setPriceShift((PriceShift)ShiftDAO.getInstance().findByName(priceShift.getName()));
          PriceRuleDAO dao = PriceRuleDAO.getInstance();
          
          PriceRule existingItem = PriceRuleDAO.getInstance().get(item.getId());
          if (existingItem != null) {
            String id = existingItem.getId();
            long version = existingItem.getVersion();
            PropertyUtils.copyProperties(existingItem, item);
            existingItem.setId(id);
            existingItem.setVersion(version);
            dao.update(existingItem);
          }
          else {
            item.setVersion(0L);
            dao.save(item);
          }
        }
        pbSync.setValue(100);
        progressStatus.setText("Download complete");
      } catch (Exception ex) {
        ex.printStackTrace();
        progressStatus.setText("Download failed .");
        progressStatus.setForeground(Color.RED);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      POSMessageDialog.showError("Download Failed");
    } finally {
      setCursor(Cursor.getDefaultCursor());
    }
  }
  
  private void uploadData() {
    try {
      setCursor(Cursor.getPredefinedCursor(3));
      progressStatus.setForeground(Color.BLUE);
      pbSync.setVisible(true);
      pbSync.setValue(50);
      
      uploadOutlet();
      uploadTaxes();
      




      progressStatus.setText("Uploading store session data..");
      PosWebService.uploadStoreSessionData(StoreUtil.getCurrentStoreOperation().getCurrentData());
      pbSync.setValue(100);
      progressStatus.setText("Upload complete");
      progressStatus.setForeground(Color.GREEN);
    } catch (Exception ex) {
      ex.printStackTrace();
      progressStatus.setForeground(Color.RED);
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Upload Failed");
      pbSync.setValue(0);
    } finally {
      setCursor(Cursor.getDefaultCursor());
    }
  }
  
  private void uploadMenuItems() throws Exception {
    List<MenuItem> menuItems = MenuItemDAO.getInstance().findAll();
    if ((menuItems != null) && (!menuItems.isEmpty())) {
      progressStatus.setText("Uploading menuitems..");
      for (MenuItem object : menuItems) {
        MenuItemDAO.getInstance().initialize(object);
        
        object.setStockUnits(null);
        object.setComboGroups(null);
        object.setComboItems(null);
        object.setParentMenuItem(null);
        MenuGroup menuGroup = object.getParent();
        if (menuGroup != null)
        {

          MenuCategory itemCategory = menuGroup.getParent();
          MenuCategory category = new MenuCategory();
          if (itemCategory != null) {
            PropertyUtils.copyProperties(category, itemCategory);
            category.setDepartments(null);
            category.setOrderTypes(null);
          }
          menuGroup.setParent(category);
        } }
      PosWebService.uploadData(new GenericEntity(menuItems) {}, "menuitem");
    }
  }
  
  private void uploadMenuModifierGroup() throws Exception
  {
    List<ModifierGroup> modifierGroups = ModifierGroupDAO.getInstance().findAll();
    if ((modifierGroups != null) && (!modifierGroups.isEmpty())) {
      progressStatus.setText("Uploading modifier groups..");
      PosWebService.uploadData(new GenericEntity(modifierGroups) {}, "modifiergroup");
      
      pbSync.setValue(70);
    }
  }
  
  private void uploadMenuModifier() throws Exception {
    List<MenuModifier> menuModifiers = MenuModifierDAO.getInstance().findAll();
    if ((menuModifiers != null) && (!menuModifiers.isEmpty())) {
      progressStatus.setText("Uploading modifiers..");
      PosWebService.uploadData(new GenericEntity(menuModifiers) {}, "modifier");
      
      pbSync.setValue(60);
    }
  }
  
  private void uploadMenuGroup() throws Exception {
    List<MenuGroup> menuGroups = MenuGroupDAO.getInstance().findAll();
    if ((menuGroups != null) && (!menuGroups.isEmpty())) {
      progressStatus.setText("Uploading menu groups..");
      PosWebService.uploadData(new GenericEntity(menuGroups) {}, "menugroup");
      
      pbSync.setValue(50);
    }
  }
  
  private void uploadMenuCategories() throws Exception {
    List<MenuCategory> menuCategories = MenuCategoryDAO.getInstance().findAll();
    if ((menuCategories != null) && (!menuCategories.isEmpty())) {
      progressStatus.setText("Uploading menu categories..");
      PosWebService.uploadData(new GenericEntity(menuCategories) {}, "menucategory");
      
      pbSync.setValue(40);
    }
  }
  
  private void uploadOutlet() throws Exception {
    List<Outlet> outlets = new ArrayList();
    Store thisStore = StoreDAO.getRestaurant();
    Outlet outlet = OutletDAO.getInstance().get(String.valueOf(thisStore.getUniqueId()));
    Address address = new Address();
    address.setAddressLine(thisStore.getAddressLine1());
    if (outlet == null) {
      outlet = new Outlet();
      outlet.setId(String.valueOf(thisStore.getUniqueId()));
      outlet.setName(thisStore.getOutletName());
      OutletDAO.getInstance().save(outlet);
      List<Terminal> terminals = TerminalDAO.getInstance().findAll();
      if (terminals != null) {
        for (Terminal terminal : terminals) {
          terminal.setOutlet(outlet);
          TerminalDAO.getInstance().saveOrUpdate(terminal);
        }
      }
    }
    else {
      outlet.setName(thisStore.getOutletName());
    }
    AddressDAO.getInstance().saveOrUpdate(address);
    outlet.setAddress(address);
    outlets.add(outlet);
    PosWebService.uploadData(new GenericEntity(outlets) {}, "outlet");
  }
  
  private void uploadTaxes() throws Exception
  {
    List<Tax> taxes = TaxDAO.getInstance().findAll();
    if ((taxes != null) && (!taxes.isEmpty())) {
      progressStatus.setText("Uploading taxes..");
      PosWebService.uploadData(new GenericEntity(taxes) {}, "tax");
      
      pbSync.setValue(10);
    }
    List<TaxGroup> taxeGroups = TaxGroupDAO.getInstance().findAll();
    if ((taxeGroups != null) && (!taxeGroups.isEmpty())) {
      PosWebService.uploadData(new GenericEntity(taxeGroups) {}, "taxgroup");
      
      pbSync.setValue(20);
    }
  }
  
  public static CloudSyncDialog show(Frame parent)
  {
    CloudSyncDialog dialog = new CloudSyncDialog();
    dialog.setSize(PosUIManager.getSize(500, 350));
    dialog.open();
    
    return dialog;
  }
  
  class Worker extends SwingWorker<Void, Void>
  {
    String command;
    
    public Worker() {}
    
    public void setActionCommand(String command) {
      this.command = command;
    }
    
    protected Void doInBackground() throws Exception
    {
      btnDownload.setEnabled(false);
      btnUpload.setEnabled(false);
      btnCancel.setEnabled(false);
      if (command.equals("UD")) {
        CloudSyncDialog.this.saveData();
      } else if (command.equals("CD"))
        CloudSyncDialog.this.uploadData();
      return null;
    }
    
    protected void done()
    {
      super.done();
      pbSync.setVisible(false);
      btnDownload.setEnabled(true);
      btnUpload.setEnabled(true);
      btnCancel.setEnabled(true);
    }
  }
}
