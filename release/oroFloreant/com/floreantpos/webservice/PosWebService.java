package com.floreantpos.webservice;

import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.Currency;
import com.floreantpos.model.Department;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.ModifierGroup;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Outlet;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.PriceRule;
import com.floreantpos.model.PriceShift;
import com.floreantpos.model.PriceTable;
import com.floreantpos.model.SalesArea;
import com.floreantpos.model.Store;
import com.floreantpos.model.StoreSession;
import com.floreantpos.model.Tax;
import com.floreantpos.model.TaxGroup;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.TerminalType;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.UserType;
import com.floreantpos.model.dao.CashDrawerDAO;
import com.floreantpos.model.dao.OutletDAO;
import com.floreantpos.model.dao.PosTransactionDAO;
import com.floreantpos.services.report.CashDrawerReportService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MultivaluedMap;

public class PosWebService
{
  public PosWebService() {}
  
  public static List getDataList(String entity) throws Exception
  {
    try
    {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/" + entity);
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<OrderType> getOrderTypes() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/ordertype");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<Outlet> getOutlets() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/outlet");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<SalesArea> getSalesArea() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/salesarea");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<Department> getDepartments() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/department");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<TerminalType> getTerminalTypes() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/terminaltype");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<Terminal> getTerminals() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/terminal");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<Currency> getCurrencies() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/currency");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<UserPermission> getUserPermissions() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/userpermission");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<UserType> getUserTypes() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/usertype");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<User> getUsers() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/user");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<Tax> getTaxes() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/tax");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<TaxGroup> getTaxGroups() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/taxgroup");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<MenuCategory> getMenuCategories() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/MenuCategory");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<MenuGroup> getMenuGroups() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/MenuGroup");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<MenuItem> getMenuItems() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/MenuItem");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<ModifierGroup> getModifierGroups() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/ModifierGroup");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<MenuModifier> getMenuModifiers() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/MenuModifier");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<PriceRule> getPriceRules() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/PriceRule");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<PriceTable> getPriceList() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/PriceTable");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static List<PriceShift> getPriceShift() throws Exception {
    try {
      Client client = Client.create();
      client.getProperties();
      WebResource webResource = client.resource(Store.getWebServiceUrl() + "/PriceShift");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).get(ClientResponse.class);
      (List)response.getEntity(new GenericType() {});
    }
    catch (Exception e) {
      throw new Exception(e);
    }
  }
  
  public static String checkConnection(String url, String storeId, String adminUserName, String adminPassword) {
    try {
      Client client = Client.create();
      client.getProperties();
      MultivaluedMap map = new com.sun.jersey.core.util.MultivaluedMapImpl();
      map.add("store_id", storeId);
      map.add("user_name", adminUserName);
      map.add("password", adminPassword);
      
      WebResource webResource = client.resource(url + "/service/data/access");
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).post(ClientResponse.class, map);
      if (response.getStatus() != 200) {
        throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
      }
      String accessInfo = (String)response.getEntity(String.class);
      if (accessInfo.length() > 50) {
        return "error,Invalid server address";
      }
      return accessInfo;
    } catch (Exception ex) {}
    return "error, Connection failed.";
  }
  





  public static void uploadStoreSessionData(StoreSession storeSession)
    throws Exception
  {
    storeSession.setOutletId(OutletDAO.getInstance().get(String.valueOf(com.floreantpos.model.dao.StoreDAO.getRestaurant().getUniqueId())).getId());
    makeXMLTransient(storeSession.getClosedBy());
    makeXMLTransient(storeSession.getOpenedBy());
    List<StoreSession> sessions = new ArrayList();
    sessions.add(storeSession);
    GenericEntity<List<StoreSession>> entity = new GenericEntity(sessions) {};
    uploadData(entity, "storesession");
    uploadSessionCashDrawers(storeSession);
    uploadSessionTransactions(storeSession);
  }
  
  private static void uploadSessionCashDrawers(StoreSession storeSession) throws Exception {
    List<CashDrawer> cashDrawers = CashDrawerDAO.getInstance().findByStoreOperationData(storeSession, Boolean.valueOf(storeSession.getCloseTime() == null));
    if ((cashDrawers != null) && (!cashDrawers.isEmpty())) {
      for (CashDrawer cashDrawer : cashDrawers) {
        if (cashDrawer.getReportTime() == null) {
          CashDrawerReportService reportService2 = new CashDrawerReportService(cashDrawer);
          reportService2.populateReport();
        }
        makeXMLTransient(cashDrawer);
      }
      uploadData(new GenericEntity(cashDrawers) {}, "cashdrawer");
    }
  }
  
  private static void makeXMLTransient(CashDrawer cashDrawer)
  {
    cashDrawer.setTransactions(null);
    makeXMLTransient(cashDrawer.getTerminal());
    makeXMLTransient(cashDrawer.getClosedBy());
    makeXMLTransient(cashDrawer.getAssignedBy());
    makeXMLTransient(cashDrawer.getAssignedUser());
    StoreSession storeOperationData = cashDrawer.getStoreOperationData();
    storeOperationData.setOpenedBy(null);
    storeOperationData.setClosedBy(null);
  }
  
  private static void makeXMLTransient(User user) {
    if (user == null)
      return;
    user.setCurrentCashDrawer(null);
    user.setLinkedUser(null);
  }
  
  private static void makeXMLTransient(Terminal terminal) {
    if (terminal == null)
      return;
    terminal.setCurrentCashDrawer(null);
    terminal.setAssignedUser(null);
  }
  
  private static void uploadSessionTransactions(StoreSession storeSession) throws Exception {
    List<PosTransaction> posTransactions = PosTransactionDAO.getInstance().getStoreSessionTransactions(storeSession);
    if ((posTransactions != null) && (!posTransactions.isEmpty())) {
      for (PosTransaction posTransaction : posTransactions) {
        posTransaction.setTicket(null);
        makeXMLTransient(posTransaction.getUser());
        makeXMLTransient(posTransaction.getCashDrawer());
      }
      uploadData(new GenericEntity(posTransactions) {}, "transactions");
    }
  }
  
  public static void uploadData(GenericEntity entity, String pathInfo) throws Exception
  {
    Client client = Client.create();
    client.getProperties();
    WebResource webResource = client.resource(Store.getWebServiceUrl() + "/" + pathInfo + "/save");
    try {
      ClientResponse response = (ClientResponse)webResource.accept(new String[] { "application/json" }).post(ClientResponse.class, entity);
      if (response.getStatus() == 200) {
        System.out.println("uploaded...");
      }
    } catch (Exception ex) {
      throw new Exception(ex);
    }
  }
}
