package com.floreantpos.webservice;

import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

public class CloudSyncAction
  extends AbstractAction
{
  public CloudSyncAction()
  {
    super("Cloud Sync");
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    try {
      CloudSyncDialog.show(POSUtil.getBackOfficeWindow());
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e);
    }
  }
}
