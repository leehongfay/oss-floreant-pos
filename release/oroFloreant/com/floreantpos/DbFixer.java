package com.floreantpos;

import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuPage;
import com.floreantpos.model.MenuPageItem;
import com.floreantpos.model.dao.MenuPageDAO;
import com.floreantpos.model.dao.MenuPageItemDAO;
import com.floreantpos.util.DatabaseUtil;
import java.io.PrintStream;
import java.util.List;





public class DbFixer
{
  public DbFixer() {}
  
  public static void main(String[] args)
  {
    DatabaseUtil.initialize();
    


































    List<MenuPage> menuPages = MenuPageDAO.getInstance().findAll();
    for (MenuPage menuPage : menuPages) {
      MenuPageDAO.getInstance().initialize(menuPage);
      List<MenuPageItem> pageItems = menuPage.getPageItems();
      for (MenuPageItem menuPageItem : pageItems) {
        MenuItem menuItem = menuPageItem.getMenuItem();
        menuPageItem.setMenuItem(menuItem);
        MenuPageItemDAO.getInstance().update(menuPageItem);
      }
    }
    
    System.out.println("Done");
    System.exit(0);
  }
}
