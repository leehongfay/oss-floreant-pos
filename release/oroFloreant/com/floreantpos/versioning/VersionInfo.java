package com.floreantpos.versioning;

public class VersionInfo { private static final String appName = "OROPOS";
  private static final String version = "1.4.120";
  private static final String numericVersion = "1020";
  
  public VersionInfo() {}
  
  public static String getAppName() { return "OROPOS"; }
  
  public static String getVersion()
  {
    return "1.4.120";
  }
  
  public static int getNumericVersion() {
    return Integer.parseInt("1020");
  }
}
