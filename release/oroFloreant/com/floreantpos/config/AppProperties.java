package com.floreantpos.config;

import com.floreantpos.PosLog;
import com.floreantpos.versioning.VersionInfo;
import org.apache.commons.configuration.PropertiesConfiguration;

















public class AppProperties
{
  private static PropertiesConfiguration properties;
  
  static
  {
    try
    {
      properties = new PropertiesConfiguration(AppProperties.class.getResource("/app.properties"));
    } catch (Exception e) {
      PosLog.error(AppProperties.class, e);
    }
  }
  
  public static String getAppName() {
    return VersionInfo.getAppName();
  }
  
  public static String getAppVersion() {
    return VersionInfo.getVersion();
  }
  
  public AppProperties() {}
}
