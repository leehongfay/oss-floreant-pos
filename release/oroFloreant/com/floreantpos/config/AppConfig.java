package com.floreantpos.config;

import com.floreantpos.Database;
import com.floreantpos.PosLog;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.util.AESencrp;
import java.io.File;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;




















public class AppConfig
{
  public static final String DATABASE_URL = "database_url";
  public static final String DATABASE_PORT = "database_port";
  public static final String DATABASE_NAME = "database_name";
  public static final String DATABASE_USER = "database_user";
  public static final String DATABASE_PASSWORD = "database_pass";
  public static final String CONNECTION_STRING = "connection_string";
  public static final String DATABASE_PROVIDER_NAME = "database_provider_name";
  private static final String KITCHEN_PRINT_ON_ORDER_SETTLE = "kitchen_print_on_order_settle";
  private static final String KITCHEN_PRINT_ON_ORDER_FINISH = "kitchen_print_on_order_finish";
  private static final String PRINT_RECEIPT_ON_ORDER_SETTLE = "print_receipt_on_order_settle";
  private static final String PRINT_RECEIPT_ON_ORDER_FINISH = "print_receipt_on_order_finish";
  private static PropertiesConfiguration config;
  
  static
  {
    try
    {
      File configFile = new File(DataProvider.get().getAppConfigFileLocation(), "app.config");
      if (!configFile.exists()) {
        configFile.createNewFile();
      }
      
      config = new PropertiesConfiguration(configFile);
      config.setAutoSave(true);
    }
    catch (Exception e) {
      PosLog.error(AppConfig.class, e);
    }
  }
  
  public static PropertiesConfiguration getConfig() {
    return config;
  }
  
  public static boolean getBoolean(String key, boolean defaultValue) {
    return config.getBoolean(key, defaultValue);
  }
  
  public static int getInt(String key, int defaultValue) {
    return config.getInt(key, defaultValue);
  }
  
  public static void putInt(String key, int value) {
    config.setProperty(key, Integer.valueOf(value));
  }
  
  public static String getString(String key) {
    return config.getString(key, null);
  }
  
  public static String getString(String key, String defaultValue) {
    return config.getString(key, defaultValue);
  }
  
  public static void put(String key, boolean value) {
    config.setProperty(key, Boolean.valueOf(value));
  }
  
  public static void put(String key, String value) {
    config.setProperty(key, value);
  }
  
  public static String getDatabaseHost() {
    return config.getString("database_url", "localhost");
  }
  
  public static void setDatabaseHost(String url) {
    config.setProperty("database_url", url);
  }
  
  public static String getConnectString() {
    return config.getString("connection_string", Database.DERBY_SINGLE.getConnectString("", "", ""));
  }
  
  public static void setConnectString(String connectionString) {
    config.setProperty("connection_string", connectionString);
  }
  
  public static String getDatabasePort() {
    return config.getString("database_port", null);
  }
  
  public static void setDatabasePort(String port) {
    config.setProperty("database_port", port);
  }
  
  public static String getDatabaseName() {
    return config.getString("database_name", "posdb");
  }
  
  public static void setDatabaseName(String name) {
    config.setProperty("database_name", name);
  }
  
  public static String getDatabaseUser() {
    return config.getString("database_user", "app");
  }
  
  public static void setDatabaseUser(String user) {
    config.setProperty("database_user", user);
  }
  
  public static String getDatabasePassword() {
    String password = config.getString("database_pass", "sa");
    if (StringUtils.isNotEmpty(password)) {
      try {
        return AESencrp.decrypt(password);
      }
      catch (Exception localException) {}
    }
    
    return password;
  }
  
  public static void setDatabasePassword(String password) {
    try {
      config.setProperty("database_pass", AESencrp.encrypt(password));
    } catch (Exception e) {
      PosLog.error(AppConfig.class, e);
    }
  }
  
  public static void setDatabaseProviderName(String databaseProviderName) {
    config.setProperty("database_provider_name", databaseProviderName);
  }
  
  public static String getDatabaseProviderName() {
    return config.getString("database_provider_name", Database.DERBY_SINGLE.getProviderName());
  }
  
  public static Database getDefaultDatabase() {
    return Database.getByProviderName(getDatabaseProviderName());
  }
  
  public static boolean isPrintReceiptOnOrderFinish() {
    return getBoolean("print_receipt_on_order_finish", false);
  }
  
  public static void setPrintReceiptOnOrderFinish(boolean print) {
    config.setProperty("print_receipt_on_order_finish", Boolean.valueOf(print));
  }
  
  public static boolean isPrintReceiptOnOrderSettle() {
    return getBoolean("print_receipt_on_order_settle", false);
  }
  
  public static void setPrintReceiptOnOrderSettle(boolean print) {
    config.setProperty("print_receipt_on_order_settle", Boolean.valueOf(print));
  }
  
  public static boolean isPrintToKitchenOnOrderFinish() {
    return getBoolean("kitchen_print_on_order_finish", false);
  }
  
  public static void setPrintToKitchenOnOrderFinish(boolean print) {
    config.setProperty("kitchen_print_on_order_finish", Boolean.valueOf(print));
  }
  
  public static boolean isPrintToKitchenOnOrderSettle() {
    return getBoolean("kitchen_print_on_order_settle", false);
  }
  
  public static void setPrintToKitchenOnOrderSettle(boolean print) {
    config.setProperty("kitchen_print_on_order_settle", Boolean.valueOf(print));
  }
  
  public AppConfig() {}
}
