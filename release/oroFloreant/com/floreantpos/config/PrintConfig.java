package com.floreantpos.config;

public class PrintConfig
{
  public static final String REPORT_PRINTER_NAME = "report_printer_name";
  public static final String RECEIPT_PRINTER_NAME = "receipt_printer_name";
  public static final String KITCHEN_PRINTER_NAME = "kitchen_printer_name";
  public static final String P_PRINT_RECEIPT_ON_ORDER_FINISH = "print_receipt_on_order_finish";
  public static final String P_PRINT_RECEIPT_ON_ORDER_SETTLE = "print_receipt_on_order_settle";
  public static final String P_PRINT_TO_KITCHEN_ON_ORDER_FINISH = "print_to_kitchen_on_order_finish";
  public static final String P_PRINT_TO_KITCHEN_ON_ORDER_SETTLE = "print_to_kitchen_on_order_settle";
  
  public PrintConfig() {}
}
