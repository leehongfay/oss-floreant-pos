package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.model.VirtualPrinter;
import com.floreantpos.model.dao.VirtualPrinterDAO;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.Container;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;
























public class VirtualPrinterConfigDialog
  extends POSDialog
{
  private VirtualPrinter printer;
  private FixedLengthTextField tfName;
  
  public VirtualPrinterConfigDialog()
    throws HeadlessException
  {
    super(POSUtil.getBackOfficeWindow(), true);
    setTitle(Messages.getString("VirtualPrinterConfigDialog.0"));
    
    init();
    
    pack();
    setResizable(false);
    setDefaultCloseOperation(2);
  }
  
  public void init() {
    getContentPane().setLayout(new MigLayout("", "[][grow]", "[][][]"));
    
    JLabel lblName = new JLabel(Messages.getString("VirtualPrinterConfigDialog.4"));
    getContentPane().add(lblName, "cell 0 0,alignx trailing");
    
    tfName = new FixedLengthTextField(60);
    getContentPane().add(tfName, "cell 1 0,growx");
    










    JSeparator separator = new JSeparator();
    getContentPane().add(separator, "cell 0 1 2 1,growx, gap top 50px");
    
    JPanel panel = new JPanel();
    getContentPane().add(panel, "cell 0 4 2 1,grow");
    
    JButton btnOk = new JButton(Messages.getString("VirtualPrinterConfigDialog.9"));
    btnOk.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doAddPrinter();
      }
    });
    panel.add(btnOk);
    
    JButton btnCancel = new JButton(Messages.getString("VirtualPrinterConfigDialog.10"));
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
    });
    panel.add(btnCancel);
  }
  
  protected void doAddPrinter()
  {
    try {
      String name = tfName.getText();
      if (StringUtils.isEmpty(name)) {
        POSMessageDialog.showMessage(this, Messages.getString("VirtualPrinterConfigDialog.11"));
        return;
      }
      
      VirtualPrinterDAO printerDAO = VirtualPrinterDAO.getInstance();
      
      if (printerDAO.findPrinterByName(name) != null) {
        POSMessageDialog.showMessage(this, Messages.getString("VirtualPrinterConfigDialog.12"));
        return;
      }
      
      if (printer == null) {
        printer = new VirtualPrinter();
      }
      
      printer.setName(name);
      























      printerDAO.saveOrUpdate(printer);
      
      setCanceled(false);
      dispose();
    }
    catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage(), e);
    }
  }
  
  public VirtualPrinter getPrinter() {
    return printer;
  }
  
  public void setPrinter(VirtualPrinter printer) {
    this.printer = printer;
    
    if (printer != null) {
      tfName.setText(printer.getName());
    }
  }
}
