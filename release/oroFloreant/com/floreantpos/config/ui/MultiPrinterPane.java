package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.config.AppConfig;
import com.floreantpos.config.AppProperties;
import com.floreantpos.main.Application;
import com.floreantpos.model.Printer;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.TerminalPrinters;
import com.floreantpos.model.User;
import com.floreantpos.model.VirtualPrinter;
import com.floreantpos.model.dao.TerminalPrintersDAO;
import com.floreantpos.model.dao.VirtualPrinterDAO;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.jdesktop.swingx.JXTable;

















public class MultiPrinterPane
  extends JPanel
{
  private List<Printer> printers = new ArrayList();
  
  private DefaultListModel<Printer> listModel;
  
  private JXTable table;
  
  private BeanTableModel<Printer> tableModel;
  private int selectedPrinterType;
  private Terminal terminal;
  
  public MultiPrinterPane() {}
  
  public MultiPrinterPane(String title, List<Printer> allPrinters)
  {
    this(title, allPrinters, Application.getInstance().getTerminal());
  }
  
  public MultiPrinterPane(String title, List<Printer> allPrinters, Terminal terminal)
  {
    this.terminal = terminal;
    
    setBorder(BorderFactory.createTitledBorder(title));
    setLayout(new BorderLayout(10, 10));
    
    JPanel panel = new JPanel();
    add(panel, "South");
    
    JButton btnAdd = new JButton(Messages.getString("MultiPrinterPane.0"));
    btnAdd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        PrinterTypeSelectionDialog dialog = new PrinterTypeSelectionDialog();
        dialog.open();
        if (dialog.isCanceled()) {
          return;
        }
        selectedPrinterType = dialog.getSelectedPrinterType();
        doAddPrinter();
      }
    });
    panel.add(btnAdd);
    
    JButton btnEdit = new JButton(Messages.getString("MultiPrinterPane.1"));
    btnEdit.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doEditPrinter();
      }
    });
    panel.add(btnEdit);
    
    JButton btnDelete = new JButton(POSConstants.DELETE.toUpperCase());
    btnDelete.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        doDeletePrinter();
      }
    });
    panel.add(btnDelete);
    
    JButton btnTest = new JButton(Messages.getString("MultiPrinterPane.5"));
    btnTest.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MultiPrinterPane.this.testPrinter();
      }
    });
    panel.add(btnTest);
    
    listModel = new DefaultListModel();
    Iterator localIterator;
    if (printers != null) {
      for (localIterator = printers.iterator(); localIterator.hasNext();) { printer = (Printer)localIterator.next();
        listModel.addElement(printer);
      }
    }
    Printer printer;
    tableModel = new BeanTableModel(Printer.class);
    tableModel.addColumn(Messages.getString("MultiPrinterPane.4"), "virtualPrinter");
    tableModel.addColumn(Messages.getString("MultiPrinterPane.6"), "deviceName");
    tableModel.addColumn(Messages.getString("MultiPrinterPane.8"), "type");
    
    Object virtualPrinters = VirtualPrinterDAO.getInstance().findAll();
    
    if ((virtualPrinters != null) && (!((List)virtualPrinters).isEmpty())) {
      for (VirtualPrinter virtualPrinter : (List)virtualPrinters) {
        Printer printer = new Printer();
        printer.setVirtualPrinter(virtualPrinter);
        printer.setDeviceName("");
        TerminalPrinters terminalPrinter = TerminalPrintersDAO.getInstance().findPrinters(printer.getVirtualPrinter(), terminal);
        if ((terminalPrinter != null) && 
          (virtualPrinter.getName().equals(terminalPrinter.getVirtualPrinter().getName()))) {
          printer.setDeviceName(terminalPrinter.getPrinterName());
        }
        

        printers.add(printer);
      }
    }
    
    tableModel.addRows(printers);
    table = new JXTable(tableModel);
    add(new JScrollPane(table), "Center");
  }
  





  protected void doEditPrinter()
  {
    int index = table.getSelectedRow();
    if (index < 0) {
      return;
    }
    index = table.convertRowIndexToModel(index);
    Printer customPrinter = (Printer)tableModel.getRow(index);
    
    AddPrinterDialog dialog = new AddPrinterDialog();
    dialog.setPrinterType(customPrinter.getType());
    dialog.setPrinter(customPrinter);
    dialog.open();
    
    if (dialog.isCanceled()) {
      return;
    }
    Printer p = dialog.getPrinter();
    
    if (p.isDefaultPrinter()) {
      for (Printer printer2 : printers) {
        printer2.setDefaultPrinter(false);
      }
    }
    


    VirtualPrinter virtualPrinter = p.getVirtualPrinter();
    
    VirtualPrinterDAO.getInstance().saveOrUpdate(virtualPrinter);
    
    TerminalPrinters terminalPrinter = TerminalPrintersDAO.getInstance().findPrinters(virtualPrinter, terminal);
    
    if (terminalPrinter == null) {
      terminalPrinter = new TerminalPrinters();
    }
    terminalPrinter.setTerminal(terminal);
    terminalPrinter.setPrinterName(p.getDeviceName());
    terminalPrinter.setVirtualPrinter(p.getVirtualPrinter());
    TerminalPrintersDAO.getInstance().saveOrUpdate(terminalPrinter);
    
    refresh();
  }
  
  protected void doDeletePrinter() {
    int index = table.getSelectedRow();
    if (index < 0) {
      return;
    }
    index = table.convertRowIndexToModel(index);
    Printer customPrinter = (Printer)tableModel.getRow(index);
    
    List<TerminalPrinters> terminalPrinters = TerminalPrintersDAO.getInstance().findAll();
    for (TerminalPrinters terminalPrinter : terminalPrinters) {
      if (terminalPrinter.getVirtualPrinter().equals(customPrinter.getVirtualPrinter())) {
        TerminalPrintersDAO.getInstance().delete(terminalPrinter);
      }
    }
    
    if (customPrinter.getVirtualPrinter() != null) {
      VirtualPrinterDAO.getInstance().delete(customPrinter.getVirtualPrinter());
    }
    refresh();
  }
  
  protected void doAddPrinter() {
    AddPrinterDialog dialog = new AddPrinterDialog();
    dialog.setPrinterType(VirtualPrinter.PRINTER_TYPE_NAMES[selectedPrinterType]);
    titlePanel.setTitle(VirtualPrinter.PRINTER_TYPE_NAMES[selectedPrinterType] + " - Printer");
    dialog.open();
    
    if (dialog.isCanceled()) {
      return;
    }
    
    Printer p = dialog.getPrinter();
    
    VirtualPrinterDAO printerDAO = VirtualPrinterDAO.getInstance();
    
    if (printerDAO.findPrinterByName(p.getVirtualPrinter().getName()) != null) {
      POSMessageDialog.showMessage(this, Messages.getString("VirtualPrinterConfigDialog.12")); return;
    }
    
    Iterator localIterator;
    if (p.isDefaultPrinter()) {
      for (localIterator = printers.iterator(); localIterator.hasNext();) { printer = (Printer)localIterator.next();
        printer.setDefaultPrinter(false);
      }
    }
    Printer printer;
    VirtualPrinter virtualPrinter = p.getVirtualPrinter();
    
    for (Printer printer : printers) {
      if (virtualPrinter.equals(printer.getVirtualPrinter())) {
        POSMessageDialog.showError(getParent(), Messages.getString("MultiPrinterPane.2"));
        return;
      }
    }
    
    virtualPrinter.setType(Integer.valueOf(selectedPrinterType));
    VirtualPrinterDAO.getInstance().saveOrUpdate(virtualPrinter);
    
    TerminalPrinters terminalPrinters = new TerminalPrinters();
    terminalPrinters.setTerminal(terminal);
    terminalPrinters.setPrinterName(p.getDeviceName());
    terminalPrinters.setVirtualPrinter(p.getVirtualPrinter());
    TerminalPrintersDAO.getInstance().saveOrUpdate(terminalPrinters);
    
    printers.add(p);
    listModel.addElement(p);
    
    refresh();
  }
  
  private void refresh() {
    printers.clear();
    List<VirtualPrinter> virtualPrinters = VirtualPrinterDAO.getInstance().findAll();
    
    if ((virtualPrinters != null) && (!virtualPrinters.isEmpty())) {
      for (VirtualPrinter virtualPrinter : virtualPrinters) {
        Printer printer = new Printer();
        printer.setVirtualPrinter(virtualPrinter);
        printer.setDeviceName("");
        TerminalPrinters terminalPrinter = TerminalPrintersDAO.getInstance().findPrinters(printer.getVirtualPrinter(), terminal);
        if ((terminalPrinter != null) && 
          (virtualPrinter.getName().equals(terminalPrinter.getVirtualPrinter().getName()))) {
          printer.setDeviceName(terminalPrinter.getPrinterName());
        }
        

        printers.add(printer);
      }
    }
    tableModel.removeAll();
    tableModel.addRows(printers);
    table.validate();
    table.repaint();
  }
  
  private void testPrinter() {
    int index = table.getSelectedRow();
    if (index < 0) {
      return;
    }
    index = table.convertRowIndexToModel(index);
    Printer printer = (Printer)tableModel.getRow(index);
    
    if (printer.getDeviceName() == null) {
      PosLog.info(getClass(), "No print selected for " + printer.getType());
      return;
    }
    try
    {
      String title = "System Information";
      String data = printer.getDeviceName() + "-" + printer.getVirtualPrinter().getName();
      data = data + "\n Terminal : " + Application.getInstance().getTerminal().getName();
      data = data + "\n Current User : " + Application.getCurrentUser().getFirstName();
      data = data + "\n " + AppProperties.getAppName() + Messages.getString("MultiPrinterPane.3") + AppProperties.getAppVersion();
      data = data + "\n Database Name : " + AppConfig.getDatabaseName() + AppConfig.getDatabaseHost() + AppConfig.getDatabasePort();
      ReceiptPrintService.testPrinter(printer.getDeviceName(), title, data);
    }
    catch (Exception localException) {}
  }
}
