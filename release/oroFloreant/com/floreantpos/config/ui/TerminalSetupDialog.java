package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.TitledView;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;





















public class TerminalSetupDialog
  extends JDialog
{
  private final PosButton psbtnSave = new PosButton();
  private TerminalConfigurationView terminalConfigurationView;
  private final Action saveAction = new SwingAction();
  private final Action closeAction = new SwingAction_1();
  
  public TerminalSetupDialog() {
    super(Application.getPosWindow(), true);
    





    JPanel panel = new JPanel();
    getContentPane().add(panel, "South");
    panel.setLayout(new MigLayout("", "[64px][71px][][][][][][][][][][][][]", "[][41px]"));
    
    JSeparator separator = new JSeparator();
    panel.add(separator, "cell 0 0 14 1,growx");
    psbtnSave.setAction(saveAction);
    psbtnSave.setMargin(new Insets(10, 20, 10, 20));
    psbtnSave.setText(Messages.getString("TerminalSetupDialog.4"));
    panel.add(psbtnSave, "cell 12 1,alignx left,aligny top");
    
    PosButton psbtnClose = new PosButton();
    psbtnClose.setAction(closeAction);
    psbtnClose.setMargin(new Insets(10, 20, 10, 20));
    psbtnClose.setText(Messages.getString("TerminalSetupDialog.6"));
    panel.add(psbtnClose, "cell 13 1,alignx left,aligny top");
    
    TitledView titledView = new TitledView();
    titledView.setTitle(Messages.getString("TerminalSetupDialog.0"));
    getContentPane().add(titledView, "North");
    
    terminalConfigurationView = new TerminalConfigurationView();
    getContentPane().add(terminalConfigurationView, "Center");
  }
  




  public static void main(String[] args) { EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          TerminalSetupDialog dialog = new TerminalSetupDialog();
          dialog.setDefaultCloseOperation(2);
          dialog.setVisible(true);
        } catch (Exception e) {
          PosLog.error(getClass(), e);
        }
      }
    }); }
  
  private class SwingAction extends AbstractAction {
    public SwingAction() {
      putValue("Name", "SaveAction");
      putValue("ShortDescription", Messages.getString("TerminalSetupDialog.10"));
    }
    
    public void actionPerformed(ActionEvent e) { if (terminalConfigurationView.canSave())
        terminalConfigurationView.save();
    }
  }
  
  private class SwingAction_1 extends AbstractAction {
    public SwingAction_1() {
      putValue("Name", "CloseAction");
      putValue("ShortDescription", Messages.getString("TerminalSetupDialog.12"));
    }
    
    public void actionPerformed(ActionEvent e) { dispose(); }
  }
}
