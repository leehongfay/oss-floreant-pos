package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.PosLog;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.main.Main;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.model.TerminalForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.codec.binary.Base64;

















public class TerminalConfigurationView
  extends ConfigurationView
{
  private JCheckBox cbShowDbConfiguration = new JCheckBox(Messages.getString("TerminalConfigurationView.5"));
  private JCheckBox cbKioskMode = new JCheckBox(Messages.getString("TerminalConfigurationView.3"));
  private DoubleTextField tfScaleFactor;
  private IntegerTextField tfSecretKeyLength;
  private JLabel lblSMTP_Host = new JLabel(Messages.getString("TerminalConfigurationView.26"));
  private JTextField tfSMTP = new JTextField(15);
  private JLabel lblSender = new JLabel(Messages.getString("TerminalConfigurationView.28"));
  private JTextField tfSenderEmail = new JTextField(15);
  
  private JSlider jsResize;
  
  private JComboBox cbTerminalNumber;
  private JPasswordField tfPassword;
  private static String encryptedPassword;
  private static String encryptionAlgorithm = "AES";
  private static byte[] key = "!@#$!@#$%^&**&^%".getBytes();
  

  public TerminalConfigurationView()
  {
    initComponents();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    
    JPanel contentPanel = new JPanel(new MigLayout("gap 5px 10px", "[][][grow]", ""));
    
    JLabel lblTerminalNumber = new JLabel(Messages.getString("TerminalConfigurationView.TERMINAL_NUMBER"));
    contentPanel.add(lblTerminalNumber, "alignx left,aligny center");
    
    cbTerminalNumber = new JComboBox();
    cbTerminalNumber.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        Object selectedItem = cbTerminalNumber.getSelectedItem();
        tfSecretKeyLength.setText(String.valueOf(((Terminal)selectedItem).getDefaultPassLength()));
      }
      
    });
    contentPanel.add(cbTerminalNumber, "aligny top,grow");
    
    contentPanel.add(new JLabel(Messages.getString("TerminalConfigurationView.33") + ":"), "newline, span 1");
    tfSecretKeyLength = new IntegerTextField(6);
    contentPanel.add(tfSecretKeyLength, "wrap");
    
    tfPassword = new JPasswordField(15);
    JPanel emailSetupJPanel = new JPanel(new MigLayout("gap 5px 10px", "[][][grow]", ""));
    emailSetupJPanel.setBorder(BorderFactory.createTitledBorder(Messages.getString("TerminalConfigurationView.44")));
    emailSetupJPanel.add(lblSMTP_Host, "newline,span 1");
    emailSetupJPanel.add(tfSMTP);
    emailSetupJPanel.add(lblSender, "newline,span 1");
    emailSetupJPanel.add(tfSenderEmail);
    emailSetupJPanel.add(new JLabel(Messages.getString("TerminalConfigurationView.48")), "newline,span 1");
    emailSetupJPanel.add(tfPassword);
    contentPanel.add(emailSetupJPanel, "span 2");
    
    contentPanel.add(cbKioskMode, "newline");
    contentPanel.add(cbShowDbConfiguration, "newline,span 3");
    
    contentPanel.add(new JLabel(Messages.getString("TerminalConfigurationView.18")), "newline, span 2");
    
    int FPS_MIN = 10;
    int FPS_MAX = 50;
    int FPS_INIT = 10;
    jsResize = new JSlider(0, FPS_MIN, FPS_MAX, FPS_INIT);
    jsResize.addChangeListener(new ChangeListener()
    {
      public void stateChanged(ChangeEvent e) {
        JSlider source = (JSlider)e.getSource();
        if (!source.getValueIsAdjusting()) {
          double fps = source.getValue();
          fps /= 10.0D;
          tfScaleFactor.setText(String.valueOf(fps));
        }
      }
    });
    contentPanel.add(jsResize);
    
    tfScaleFactor = new DoubleTextField(5);
    contentPanel.add(tfScaleFactor);
    
    JScrollPane scrollPane = new JScrollPane(contentPanel);
    scrollPane.setBorder(null);
    add(scrollPane);
  }
  











  public static void main(String[] args)
  {
    JFrame frame = new JFrame();
    frame.getContentPane().add(new TerminalConfigurationView());
    frame.setSize(500, 400);
    frame.setDefaultCloseOperation(3);
    frame.setVisible(true);
  }
  
  public boolean canSave() {
    return true;
  }
  
  public boolean save()
  {
    int terminalNumber = 0;
    double scaleFactor = tfScaleFactor.getDouble();
    
    if (scaleFactor > 5.0D) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), "");
      return false;
    }
    try
    {
      Object selectedItem = cbTerminalNumber.getSelectedItem();
      ((Terminal)selectedItem).setDefaultPassLength(Integer.valueOf(Integer.parseInt(tfSecretKeyLength.getText())));
      terminalNumber = ((Terminal)selectedItem).getId().intValue();
      TerminalDAO.getInstance().saveOrUpdate((Terminal)selectedItem);
    } catch (Exception x) {
      POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("TerminalConfigurationView.14"));
      return false;
    }
    
    int defaultPassLen = tfSecretKeyLength.getInteger();
    if (defaultPassLen == 0) {
      defaultPassLen = 4;
    }
    TerminalConfig.setTerminalId(terminalNumber);
    TerminalConfig.setKioskMode(cbKioskMode.isSelected());
    TerminalConfig.setShowDbConfigureButton(cbShowDbConfiguration.isSelected());
    TerminalConfig.setSMTPHost(tfSMTP.getText());
    TerminalConfig.setSenderEmail(tfSenderEmail.getText());
    setEncryptedPassword(tfPassword.getText());
    TerminalConfig.setSenderPassword(getEncryptedPassword());
    TerminalConfig.setScreenScaleFactor(scaleFactor);
    

    restartPOS();
    
    return true;
  }
  
  public void initialize()
    throws Exception
  {
    cbTerminalNumber.setModel(new ComboBoxModel(TerminalDAO.getInstance().findAll()));
    Terminal terminal = Application.getInstance().refreshAndGetTerminal();
    cbTerminalNumber.setSelectedItem(terminal);
    tfSecretKeyLength.setText(String.valueOf(terminal.getDefaultPassLength()));
    cbKioskMode.setSelected(TerminalConfig.isKioskMode());
    cbShowDbConfiguration.setSelected(TerminalConfig.isShowDbConfigureButton());
    tfScaleFactor.setText("" + TerminalConfig.getScreenScaleFactor());
    jsResize.setValue((int)(TerminalConfig.getScreenScaleFactor() * 10.0D));
    tfSMTP.setText(TerminalConfig.getSMTPHost());
    tfSenderEmail.setText(TerminalConfig.getSenderEmail());
    encryptedPassword = TerminalConfig.getSenderPassword();
    if (encryptedPassword != null) {
      tfPassword.setText(getDecryptedPassword());
    }
    setInitialized(true);
  }
  
















  public String getName()
  {
    return Messages.getString("TerminalConfigurationView.47");
  }
  
  private void doCreateTerminal(ActionEvent evt) {
    TerminalForm editor = new TerminalForm();
    BeanEditorDialog dialog = new BeanEditorDialog(editor);
    dialog.open();
    
    if (!dialog.isCanceled()) {
      Terminal term = (Terminal)editor.getBean();
      term.setName(editor.getName());
      cbTerminalNumber.addItem(Integer.valueOf(editor.getTerminalId()));
      cbTerminalNumber.setSelectedItem(Integer.valueOf(editor.getTerminalId()));
    }
  }
  



  public void restartPOS()
  {
    JOptionPane optionPane = new JOptionPane(Messages.getString("TerminalConfigurationView.52"), 3, 2, Application.getApplicationIcon(), new String[] { Messages.getString("TerminalConfigurationView.54") });
    
    Object[] optionValues = optionPane.getComponents();
    for (Object object : optionValues) {
      if ((object instanceof JPanel)) {
        JPanel panel = (JPanel)object;
        Component[] components = panel.getComponents();
        
        for (Component component : components) {
          if ((component instanceof JButton)) {
            component.setPreferredSize(new Dimension(100, 80));
            JButton button = (JButton)component;
            button.setPreferredSize(PosUIManager.getSize(100, 50));
          }
        }
      }
    }
    JDialog dialog = optionPane.createDialog(Application.getPosWindow(), Messages.getString("TerminalConfigurationView.55"));
    dialog.setIconImage(Application.getApplicationIcon().getImage());
    dialog.setLocationRelativeTo(Application.getPosWindow());
    dialog.setVisible(true);
    Object selectedValue = (String)optionPane.getValue();
    if (selectedValue != null)
    {
      if (selectedValue.equals(Messages.getString("TerminalConfigurationView.53"))) {
        try {
          Main.restart();
        }
        catch (IOException|InterruptedException|URISyntaxException localIOException1) {}
      }
    }
  }
  



  public static void setEncryptedPassword(String password)
  {
    byte[] dataToSend = password.getBytes();
    Cipher c = null;
    try {
      c = Cipher.getInstance(encryptionAlgorithm);
    } catch (NoSuchAlgorithmException e) {
      PosLog.error(TerminalConfigurationView.class, e);
    } catch (NoSuchPaddingException e) {
      PosLog.error(TerminalConfigurationView.class, e);
    }
    SecretKeySpec k = new SecretKeySpec(key, encryptionAlgorithm);
    try {
      c.init(1, k);
    } catch (InvalidKeyException e) {
      PosLog.error(TerminalConfigurationView.class, e);
    }
    byte[] encryptedData = "".getBytes();
    try {
      encryptedData = c.doFinal(dataToSend);
    } catch (IllegalBlockSizeException e) {
      PosLog.error(TerminalConfigurationView.class, e);
    } catch (BadPaddingException e) {
      PosLog.error(TerminalConfigurationView.class, e);
    }
    byte[] encryptedByteValue = new Base64().encode(encryptedData);
    encryptedPassword = new String(encryptedByteValue);
  }
  
  public static String getEncryptedPassword() {
    return encryptedPassword;
  }
  
  private static String getDecryptedPassword() {
    byte[] encryptedData = new Base64().decode(encryptedPassword.getBytes());
    Cipher c = null;
    try {
      c = Cipher.getInstance(encryptionAlgorithm);
    } catch (NoSuchAlgorithmException e) {
      PosLog.error(TerminalConfigurationView.class, e);
    } catch (NoSuchPaddingException e) {
      PosLog.error(TerminalConfigurationView.class, e);
    }
    SecretKeySpec k = new SecretKeySpec(key, encryptionAlgorithm);
    try {
      c.init(2, k);
    } catch (InvalidKeyException e1) {
      PosLog.error(TerminalConfigurationView.class, e1);
    }
    byte[] decrypted = null;
    try {
      decrypted = c.doFinal(encryptedData);
    } catch (IllegalBlockSizeException e) {
      PosLog.error(TerminalConfigurationView.class, e);
    } catch (BadPaddingException e) {
      PosLog.error(TerminalConfigurationView.class, e);
    }
    return new String(decrypted);
  }
}
