package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import com.floreantpos.model.PosPrinters;
import com.floreantpos.model.Printer;
import com.floreantpos.model.PrinterGroup;
import com.floreantpos.model.VirtualPrinter;
import com.floreantpos.swing.CheckBoxList;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;



















public class AddPrinterGroupDialog
  extends POSDialog
{
  private FixedLengthTextField tfName = new FixedLengthTextField(60);
  private CheckBoxList<Printer> printerList;
  private JCheckBox chkDefault;
  private PrinterGroup printerGroup;
  private List<Printer> printers;
  
  public AddPrinterGroupDialog() throws HeadlessException
  {
    super(POSUtil.getBackOfficeWindow(), true);
    setTitle(Messages.getString("AddPrinterGroupDialog.0"));
    
    init();
    
    setDefaultCloseOperation(2);
    pack();
  }
  
  private void init() {
    JPanel contentPane = (JPanel)getContentPane();
    contentPane.setLayout(new MigLayout("", "[][grow]", ""));
    
    add(new JLabel(Messages.getString("AddPrinterGroupDialog.4")));
    add(tfName, "grow, wrap");
    
    chkDefault = new JCheckBox(Messages.getString("AddPrinterGroupDialog.1"));
    
    add(new JLabel(), "grow");
    add(chkDefault, "wrap");
    
    PosPrinters printersKitchen = PosPrinters.load();
    printers = printersKitchen.getKitchenPrinters();
    printerList = new CheckBoxList(new Vector(printers));
    
    JPanel listPanel = new JPanel(new BorderLayout());
    listPanel.setBorder(new TitledBorder(Messages.getString("AddPrinterGroupDialog.6")));
    listPanel.add(new JScrollPane(printerList));
    
    add(listPanel, "newline, span 2, grow");
    
    JPanel panel = new JPanel();
    contentPane.add(panel, "cell 0 4 3 1,grow");
    
    JButton btnOk = new JButton(Messages.getString("AddPrinterGroupDialog.9"));
    btnOk.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (StringUtils.isEmpty(tfName.getText())) {
          POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("AddPrinterGroupDialog.10"));
          return;
        }
        
        List checkedValues = printerList.getCheckedValues();
        if ((checkedValues == null) || (checkedValues.size() == 0)) {
          POSMessageDialog.showError(Application.getPosWindow(), Messages.getString("AddPrinterGroupDialog.11"));
          return;
        }
        
        setCanceled(false);
        dispose();
      }
    });
    panel.add(btnOk);
    
    JButton btnCancel = new JButton(Messages.getString("AddPrinterGroupDialog.12"));
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
    });
    panel.add(btnCancel);
  }
  
  public PrinterGroup getPrinterGroup() {
    if (printerGroup == null) {
      printerGroup = new PrinterGroup();
    }
    printerGroup.setIsDefault(chkDefault.isSelected());
    printerGroup.setName(tfName.getText());
    
    List checkedValues = printerList.getCheckedValues();
    if (checkedValues != null) {
      List<String> names = new ArrayList();
      
      for (Object object : checkedValues) {
        Printer p = (Printer)object;
        names.add(p.getVirtualPrinter().getName());
      }
      
      printerGroup.setPrinterNames(names);
    }
    return printerGroup;
  }
  
  public void setPrinterGroup(PrinterGroup group) {
    printerGroup = group;
    tfName.setText(group.getName());
    
    chkDefault.setSelected(group.isIsDefault());
    
    Vector<Printer> selectedPrinters = new Vector();
    for (Printer printer : printers) {
      if (printerGroup.getPrinterNames().contains(printer.getVirtualPrinter().getName())) {
        selectedPrinters.add(printer);
      }
    }
    printerList.selectItems(selectedPrinters);
  }
}
