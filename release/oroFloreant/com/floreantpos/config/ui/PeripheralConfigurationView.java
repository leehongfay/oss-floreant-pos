package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.FloreantPlugin;
import com.floreantpos.main.Application;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.DrawerUtil;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.SerialPortUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class PeripheralConfigurationView
  extends ConfigurationView
{
  public static final String CONFIG_TAB_PERIPHERAL = Messages.getString("PeripheralConfigurationView.0");
  private JCheckBox chkHasCashDrawer;
  private JTextField tfDrawerName = new JTextField(10);
  private JTextField tfDrawerCodes = new JTextField(15);
  private DoubleTextField tfDrawerInitialBalance = new DoubleTextField(6);
  
  private JCheckBox cbCustomerDisplay;
  
  private JTextField tfCustomerDisplayPort;
  private JTextField tfCustomerDisplayMessage;
  private JCheckBox cbScaleActive;
  private JTextField tfScalePort;
  private FixedLengthTextField tfScaleDisplayMessage;
  private List<ConfigurationSubEditor> subEditors = new ArrayList();
  
  public PeripheralConfigurationView() {
    initComponents();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    
    JPanel contentPanel = new JPanel();
    contentPanel.setLayout(new MigLayout("", "[grow]", "[][]"));
    JPanel drawerConfigPanel = new JPanel(new MigLayout());
    drawerConfigPanel.setBorder(BorderFactory.createTitledBorder(Messages.getString("PeripheralConfigurationView.4")));
    
    chkHasCashDrawer = new JCheckBox(Messages.getString("TerminalConfigurationView.15"));
    drawerConfigPanel.add(chkHasCashDrawer, "span 5, wrap");
    
    drawerConfigPanel.add(new JLabel(Messages.getString("TerminalConfigurationView.25")));
    drawerConfigPanel.add(tfDrawerName, "");
    
    drawerConfigPanel.add(new JLabel(Messages.getString("TerminalConfigurationView.27")), "newline");
    drawerConfigPanel.add(tfDrawerCodes, Messages.getString("TerminalConfigurationView.29"));
    
    JButton btnDrawerTest = new JButton(Messages.getString("TerminalConfigurationView.11"));
    btnDrawerTest.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        String text = tfDrawerCodes.getText();
        if (StringUtils.isEmpty(text)) {
          text = TerminalConfig.getDefaultDrawerControlCodes();
        }
        
        String[] split = text.split(",");
        char[] codes = new char[split.length];
        for (int i = 0; i < split.length; i++) {
          try {
            codes[i] = ((char)Integer.parseInt(split[i]));
          } catch (Exception x) {
            codes[i] = '0';
          }
        }
        
        DrawerUtil.kickDrawer(tfDrawerName.getText(), codes);
      }
    });
    drawerConfigPanel.add(btnDrawerTest);
    
    JButton btnRestoreDrawerDefault = new JButton(Messages.getString("TerminalConfigurationView.32"));
    btnRestoreDrawerDefault.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        tfDrawerName.setText("COM1");
        tfDrawerCodes.setText(TerminalConfig.getDefaultDrawerControlCodes());
      }
    });
    drawerConfigPanel.add(btnRestoreDrawerDefault);
    



    contentPanel.add(drawerConfigPanel, "span 3, grow, wrap");
    
    chkHasCashDrawer.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        doEnableDisableDrawerPull();
      }
      
    });
    JPanel customerDisplayPanel = new JPanel(new MigLayout());
    customerDisplayPanel.setBorder(BorderFactory.createTitledBorder(Messages.getString("PeripheralConfigurationView.5")));
    
    cbCustomerDisplay = new JCheckBox(Messages.getString("PeripheralConfigurationView.6"));
    tfCustomerDisplayPort = new JTextField(20);
    tfCustomerDisplayMessage = new FixedLengthTextField(20);
    
    JButton btnTest = new JButton(Messages.getString("PeripheralConfigurationView.7"));
    btnTest.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        DrawerUtil.setCustomerDisplayMessage(tfCustomerDisplayPort.getText(), String.format("%200s", new Object[] { "" }));
        DrawerUtil.setCustomerDisplayMessage(tfCustomerDisplayPort.getText(), tfCustomerDisplayMessage.getText());
      }
      
    });
    JButton btnRestoreCustomerDefault = new JButton(Messages.getString("TerminalConfigurationView.32"));
    btnRestoreCustomerDefault.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        tfCustomerDisplayPort.setText(Messages.getString("PeripheralConfigurationView.8"));
        tfCustomerDisplayMessage.setText(Messages.getString("PeripheralConfigurationView.9"));
      }
      
    });
    customerDisplayPanel.add(cbCustomerDisplay, "wrap");
    customerDisplayPanel.add(new JLabel(Messages.getString("PeripheralConfigurationView.11")));
    customerDisplayPanel.add(tfCustomerDisplayPort, "wrap");
    customerDisplayPanel.add(new JLabel(Messages.getString("PeripheralConfigurationView.13")));
    customerDisplayPanel.add(tfCustomerDisplayMessage);
    customerDisplayPanel.add(btnTest);
    customerDisplayPanel.add(btnRestoreCustomerDefault);
    
    contentPanel.add(customerDisplayPanel, "grow,wrap");
    
    JPanel scaleDisplayPanel = new JPanel(new MigLayout());
    scaleDisplayPanel.setBorder(BorderFactory.createTitledBorder(Messages.getString("PeripheralConfigurationView.15")));
    
    cbScaleActive = new JCheckBox(Messages.getString("PeripheralConfigurationView.16"));
    tfScalePort = new JTextField(20);
    tfScaleDisplayMessage = new FixedLengthTextField(20);
    
    JButton btnTestScale = new JButton(Messages.getString("PeripheralConfigurationView.17"));
    btnTestScale.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PeripheralConfigurationView.this.testScaleMachine();
      }
      
    });
    JButton btnRestoreScaleDefault = new JButton(Messages.getString("TerminalConfigurationView.32"));
    btnRestoreScaleDefault.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        tfScalePort.setText(Messages.getString("PeripheralConfigurationView.18"));
      }
      
    });
    scaleDisplayPanel.add(cbScaleActive, "wrap");
    scaleDisplayPanel.add(new JLabel(Messages.getString("PeripheralConfigurationView.20")));
    scaleDisplayPanel.add(tfScalePort);
    scaleDisplayPanel.add(btnTestScale);
    scaleDisplayPanel.add(btnRestoreScaleDefault);
    
    if (TerminalConfig.getScaleActivationValue().equals("cas10")) {
      contentPanel.add(scaleDisplayPanel, "grow,wrap");
    }
    for (FloreantPlugin plugin : ExtensionManager.getPlugins()) {
      List<ConfigurationSubEditor> subEditorComponents = plugin.getSubEditors();
      if (subEditorComponents != null) {
        subEditors.addAll(subEditorComponents);
        for (ConfigurationSubEditor subEditor : subEditorComponents) {
          contentPanel.add(subEditor, "grow,wrap");
        }
      }
    }
    JScrollPane scrollPane = new JScrollPane(contentPanel);
    scrollPane.setBorder(null);
    add(scrollPane);
  }
  
  protected void doEnableDisableDrawerPull() {
    boolean selected = chkHasCashDrawer.isSelected();
    tfDrawerName.setEnabled(selected);
    tfDrawerCodes.setEnabled(selected);
    tfDrawerInitialBalance.setEnabled(selected);
  }
  
  public boolean save()
    throws Exception
  {
    TerminalConfig.setDrawerPortName(tfDrawerName.getText());
    String drawerControlCodes = tfDrawerCodes.getText();
    if (StringUtils.isNotEmpty(drawerControlCodes)) {
      drawerControlCodes = drawerControlCodes.replaceAll(",", "_");
    }
    TerminalConfig.setDrawerControlCodes(drawerControlCodes);
    
    TerminalConfig.setCustomerDisplay(cbCustomerDisplay.isSelected());
    TerminalConfig.setCustomerDisplayPort(tfCustomerDisplayPort.getText());
    TerminalConfig.setCustomerDisplayMessage(tfCustomerDisplayMessage.getText());
    
    TerminalConfig.setScaleDisplay(cbScaleActive.isSelected());
    TerminalConfig.setScalePort(tfScalePort.getText());
    TerminalConfig.setScaleDisplayMessage(tfScaleDisplayMessage.getText());
    
    TerminalDAO terminalDAO = TerminalDAO.getInstance();
    Terminal terminal = terminalDAO.get(Integer.valueOf(TerminalConfig.getTerminalId()));
    if (terminal == null) {
      terminal = new Terminal();
      terminal.setId(Integer.valueOf(TerminalConfig.getTerminalId()));
      
      terminal.setName(String.valueOf(TerminalConfig.getTerminalId()));
    }
    
    terminal.setHasCashDrawer(Boolean.valueOf(chkHasCashDrawer.isSelected()));
    

    terminalDAO.saveOrUpdate(terminal);
    
    if (subEditors != null) {
      for (ConfigurationSubEditor subEditor : subEditors) {
        subEditor.save();
      }
    }
    return true;
  }
  
  public void initialize()
    throws Exception
  {
    Terminal terminal = Application.getInstance().refreshAndGetTerminal();
    chkHasCashDrawer.setSelected(terminal.isHasCashDrawer().booleanValue());
    tfDrawerName.setText(TerminalConfig.getDrawerPortName());
    String drawerControlCodes = TerminalConfig.getDrawerControlCodes();
    if (StringUtils.isNotEmpty(drawerControlCodes)) {
      drawerControlCodes = drawerControlCodes.replaceAll("_", ",");
    }
    tfDrawerCodes.setText(drawerControlCodes);
    
    cbCustomerDisplay.setSelected(TerminalConfig.isActiveCustomerDisplay());
    tfCustomerDisplayPort.setText(TerminalConfig.getCustomerDisplayPort());
    tfCustomerDisplayMessage.setText(TerminalConfig.getCustomerDisplayMessage());
    
    cbScaleActive.setSelected(TerminalConfig.isActiveScaleDisplay());
    tfScalePort.setText(TerminalConfig.getScalePort());
    tfScaleDisplayMessage.setText(TerminalConfig.getScaleDisplayMessage());
    
    doEnableDisableDrawerPull();
    
    if (subEditors != null) {
      for (ConfigurationSubEditor subEditor : subEditors) {
        subEditor.initialize();
      }
    }
    setInitialized(true);
  }
  
  private void testScaleMachine()
  {
    try {
      String string = SerialPortUtil.readWeight(tfScalePort.getText());
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), string);
    }
    catch (Exception ex) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), ex.getMessage());
      LogFactory.getLog(PeripheralConfigurationView.class).error(ex);
    }
  }
  
  public String getName()
  {
    return CONFIG_TAB_PERIPHERAL;
  }
}
