package com.floreantpos.config.ui;

import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.bo.ui.explorer.TerminalForm;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.FloreantPlugin;
import com.floreantpos.extension.TicketImportPlugin;
import com.floreantpos.main.Application;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import net.miginfocom.swing.MigLayout;
import org.hibernate.StaleStateException;



















public class TerminalConfigurationDialog
  extends POSDialog
  implements ChangeListener, ActionListener
{
  private static final String OK = POSConstants.OK;
  private static final String CANCEL = POSConstants.CANCEL;
  private JTabbedPane tabbedPane = new JTabbedPane();
  private List<ConfigurationView> views = new ArrayList();
  private Terminal terminal;
  
  public TerminalConfigurationDialog() {
    this(Application.getInstance().refreshAndGetTerminal());
  }
  
  public TerminalConfigurationDialog(Terminal terminal) {
    super(POSUtil.getBackOfficeWindow(), true);
    this.terminal = terminal;
    boolean currentTerminal = this.terminal.getId() == Application.getInstance().getTerminal().getId();
    if (currentTerminal) {
      setTitle(POSConstants.CONFIGURE_WINDOW_TITLE);
    }
    else {
      setTitle("Configure Terminal " + terminal.getId());
    }
    
    initComponents();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    
    JPanel contentPanel = new JPanel(new MigLayout("fill", "", "[fill,grow][]"));
    
    tabbedPane.addChangeListener(this);
    contentPanel.add(tabbedPane, "span, grow");
    
    addView(new TerminalForm(terminal));
    addView(new PrintConfigurationView(terminal));
    if (DataProvider.get().getCurrentTerminal() == terminal) {
      addView(new CardConfigurationView());
      addView(new DatabaseConfigurationView());
      addView(new PeripheralConfigurationView());
      addView(new GiftCardConfigurationView());
      addView(new KitchenDisplayConfigurationView());
      addView(new EmailConfigurationView(Application.getInstance().refreshAndGetTerminal()));
      addView(new PaymentOptionConfigurationView(Application.getInstance().refreshAndGetTerminal()));
      
      TicketImportPlugin ticketImportPlugin = (TicketImportPlugin)ExtensionManager.getPlugin(TicketImportPlugin.class);
      if (ticketImportPlugin != null) {
        addView(new TicketImportConfigurationView());
      }
    }
    JPanel bottomPanel = new JPanel(new MigLayout("fill"));
    
    JButton btnCancel = new JButton(CANCEL);
    btnCancel.addActionListener(this);
    bottomPanel.add(btnCancel, "dock east, gaptop 5,gapright 8, gapbottom 10");
    JButton btnOk = new JButton(OK);
    btnOk.addActionListener(this);
    bottomPanel.add(btnOk, "dock east, gapright 5, gaptop 5, gapbottom 10");
    
    add(bottomPanel, "South");
    setDefaultCloseOperation(2);
    
    for (FloreantPlugin plugin : ExtensionManager.getPlugins()) {
      plugin.initConfigurationView(this);
    }
    add(contentPanel, "Center");
  }
  
  public void addView(ConfigurationView view) {
    tabbedPane.addTab(view.getName(), view);
    views.add(view);
  }
  
  public void setVisible(boolean b)
  {
    super.setVisible(b);
    
    if (b) {
      stateChanged(null);
    }
  }
  
  public void stateChanged(ChangeEvent e) {
    ConfigurationView view = (ConfigurationView)tabbedPane.getSelectedComponent();
    if (!view.isInitialized()) {
      try {
        view.initialize();
      } catch (Exception e1) {
        POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e1);
      }
    }
  }
  
  public void actionPerformed(ActionEvent e) {
    if (OK.equalsIgnoreCase(e.getActionCommand())) {
      try {
        for (ConfigurationView view : views) {
          if ((view.isInitialized()) && 
            (!view.save())) {
            return;
          }
        }
        setCanceled(false);
        dispose();
      } catch (StaleStateException x) {
        POSMessageDialog.showError(this, x.getMessage());
      } catch (PosException x) {
        POSMessageDialog.showError(this, x.getMessage());
      } catch (Exception x) {
        POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, x);
      }
    }
    if (CANCEL.equalsIgnoreCase(e.getActionCommand())) {
      setCanceled(true);
      dispose();
    }
  }
}
