package com.floreantpos.config.ui;

import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.model.ImageResource;
import com.floreantpos.model.Store;
import com.floreantpos.model.dao.ImageResourceDAO;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.ImageGalleryDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;





















public class StoreBrandingConfigurationView
  extends ConfigurationView
{
  private Store store;
  private JLabel lblLogoPreview = new JLabel("");
  private JButton btnClearLogoImage = new JButton(POSConstants.CLEAR);
  
  private ImageResource logoImageResource;
  private JLabel lblImagePreview = new JLabel("");
  private JButton btnClearImage = new JButton(POSConstants.CLEAR);
  private ImageResource imageResource;
  
  public StoreBrandingConfigurationView(Store store) {
    this.store = store;
    setLayout(new BorderLayout());
    JPanel contentPanel = new JPanel(new MigLayout("fillx,hidemode 3", "[][][]", ""));
    
    JPanel tabBrandingForLogoPanel = new JPanel(new MigLayout("fillx", "grow"));
    TitledBorder border = BorderFactory.createTitledBorder(null, "Logo", 2, 0);
    tabBrandingForLogoPanel.setBorder(border);
    
    lblLogoPreview.setBorder(border);
    lblLogoPreview.setHorizontalAlignment(0);
    lblLogoPreview.setBorder(new EtchedBorder(1, null, null));
    lblLogoPreview.setPreferredSize(PosUIManager.getSize(128, 128));
    
    JButton btnSelectLogoImage = new JButton("...");
    
    tabBrandingForLogoPanel.add(lblLogoPreview, "center");
    
    btnSelectLogoImage.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doSelectLogoImageFile();
      }
      
    });
    btnClearLogoImage.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doClearLogoImage();
      }
      
    });
    JPanel tabBrandingForLogingScreenPanel = new JPanel();
    tabBrandingForLogingScreenPanel.setLayout(new MigLayout("fillx", "grow"));
    TitledBorder border1 = BorderFactory.createTitledBorder(null, "Login Screen Background", 2, 0);
    tabBrandingForLogingScreenPanel.setBorder(border1);
    
    lblImagePreview.setBorder(border1);
    lblImagePreview.setHorizontalAlignment(0);
    lblImagePreview.setBorder(new EtchedBorder(1, null, null));
    lblImagePreview.setPreferredSize(PosUIManager.getSize(400, 250));
    JButton btnSelectImage = new JButton("...");
    
    tabBrandingForLogingScreenPanel.add(lblImagePreview, "center");
    btnSelectImage.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doSelectImageFile();
      }
      
    });
    btnClearImage.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doClearImage();
      }
    });
    contentPanel.add(tabBrandingForLogoPanel, "grow");
    contentPanel.add(btnClearLogoImage, "split 2");
    contentPanel.add(btnSelectLogoImage, "wrap");
    
    contentPanel.add(tabBrandingForLogingScreenPanel, "grow");
    contentPanel.add(btnClearImage, "split 2");
    contentPanel.add(btnSelectImage);
    
    JScrollPane scrollPane = new JScrollPane(contentPanel);
    scrollPane.setBorder(null);
    add(scrollPane);
  }
  
  protected void doSelectImageFile() {
    ImageGalleryDialog dialog = ImageGalleryDialog.getInstance();
    dialog.setTitle("Image Gallery");
    dialog.setSize(PosUIManager.getSize(650, 600));
    dialog.setResizable(false);
    dialog.open();
    if (dialog.isCanceled()) {
      return;
    }
    imageResource = dialog.getImageResource();
    if (imageResource != null) {
      lblImagePreview.setIcon(imageResource.getScaledImage(400, 250));
    }
  }
  
  protected void doClearImage() {
    lblImagePreview.setIcon(null);
    imageResource = null;
  }
  
  protected void doSelectLogoImageFile()
  {
    ImageGalleryDialog dialog = ImageGalleryDialog.getInstance();
    dialog.setSelectBtnVisible(true);
    dialog.setImageMaximumSize(128, 128);
    dialog.setTitle("Image Gallery");
    dialog.setSize(PosUIManager.getSize(650, 600));
    dialog.setResizable(false);
    dialog.open();
    if (dialog.isCanceled()) {
      return;
    }
    logoImageResource = dialog.getImageResource();
    if (logoImageResource != null) {
      ImageIcon storeLogo = new ImageIcon(logoImageResource.getImage());
      if ((storeLogo.getIconWidth() > 128) || (storeLogo.getIconHeight() > 128)) {
        storeLogo = new ImageIcon(POSUtil.getScaledImage(logoImageResource.getImage(), 128, 128));
      }
      lblLogoPreview.setIcon(storeLogo);
    }
  }
  
  protected void doClearLogoImage() {
    lblLogoPreview.setIcon(null);
    logoImageResource = null;
  }
  
  public boolean save() throws Exception
  {
    if (!isInitialized()) {
      return true;
    }
    if (logoImageResource != null) {
      store.addProperty("ticket.header.logo.imageid", logoImageResource.getId());
      ImageIcon storeLogo = new ImageIcon(logoImageResource.getImage());
      if ((storeLogo.getIconWidth() > 128) || (storeLogo.getIconHeight() > 128)) {
        storeLogo = new ImageIcon(POSUtil.getScaledImage(logoImageResource.getImage(), 128, 128));
      }
      store.setStoreLogo(storeLogo);
    }
    else {
      store.addProperty("ticket.header.logo.imageid", null);
    }
    
    if (imageResource != null) {
      store.addProperty("loginscreen.background", imageResource.getId());
      Dimension size = Application.getPosWindow().getPreferredSize();
      store.setLoginScreenBackground(imageResource.getScaledImage((int)size.getWidth(), (int)size.getHeight()));
    }
    else {
      store.addProperty("loginscreen.background", null);
    }
    return true;
  }
  
  public void initialize() throws Exception
  {
    logoImageResource = ImageResourceDAO.getInstance().findById(store.getProperty("ticket.header.logo.imageid"));
    if (logoImageResource != null) {
      ImageIcon storeLogo = new ImageIcon(logoImageResource.getImage());
      if ((storeLogo.getIconWidth() > 128) || (storeLogo.getIconHeight() > 128)) {
        storeLogo = new ImageIcon(POSUtil.getScaledImage(logoImageResource.getImage(), 128, 128));
      }
      lblLogoPreview.setIcon(storeLogo);
    }
    imageResource = ImageResourceDAO.getInstance().findById(store.getProperty("loginscreen.background"));
    if (imageResource != null) {
      ImageIcon scaledImage = imageResource.getScaledImage(400, 250);
      lblImagePreview.setIcon(scaledImage);
    }
    setInitialized(true);
  }
  
  public String getName()
  {
    return "Branding";
  }
}
