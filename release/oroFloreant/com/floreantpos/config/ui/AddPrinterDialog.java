package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.model.Printer;
import com.floreantpos.model.VirtualPrinter;
import com.floreantpos.model.dao.VirtualPrinterDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;





















public class AddPrinterDialog
  extends POSDialog
{
  private static final String DO_NOT_PRINT = "Do not print";
  private Printer printer;
  private VirtualPrinter virtualPrinter;
  private FixedLengthTextField tfName;
  private JComboBox cbVirtualPrinter;
  private JComboBox cbDevice;
  private JCheckBox chckbxDefault;
  private JCheckBox chkPrintAuto;
  TitlePanel titlePanel;
  
  public AddPrinterDialog()
    throws HeadlessException
  {
    super(POSUtil.getBackOfficeWindow(), true);
    
    setTitle(Messages.getString("AddPrinterDialog.0"));
    
    setMinimumSize(new Dimension(400, 200));
    
    setDefaultCloseOperation(2);
    pack();
  }
  
  public void initUI()
  {
    setLayout(new BorderLayout(5, 5));
    JPanel centerPanel = new JPanel();
    centerPanel.setLayout(new MigLayout("", "[][grow][]", "[][][][][grow]"));
    
    titlePanel = new TitlePanel();
    titlePanel.setTitle("Printer Type:");
    add(titlePanel, "North");
    
    JLabel lblName = new JLabel("Virtual Printer Name : ");
    centerPanel.add(lblName, "cell 0 0,alignx trailing");
    
    tfName = new FixedLengthTextField(20);
    
    cbVirtualPrinter = new JComboBox();
    cbVirtualPrinter.setEnabled(false);
    List<VirtualPrinter> virtualPrinters = VirtualPrinterDAO.getInstance().findAll();
    cbVirtualPrinter.setModel(new DefaultComboBoxModel(virtualPrinters.toArray(new VirtualPrinter[0])));
    

    centerPanel.add(tfName, "cell 1 0,growx");
    
    JButton btnNew = new JButton(Messages.getString("AddPrinterDialog.7"));
    btnNew.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doAddNewVirtualPrinter();
      }
      

    });
    JLabel lblDevice = new JLabel(Messages.getString("AddPrinterDialog.9"));
    centerPanel.add(lblDevice, "cell 0 1,alignx trailing");
    
    cbDevice = new JComboBox();
    List printerServices = new ArrayList();
    printerServices.add("Do not print");
    PrintService[] lookupPrintServices = PrintServiceLookup.lookupPrintServices(null, null);
    
    cbDevice.addItem(null);
    for (int i = 0; i < lookupPrintServices.length; i++) {
      printerServices.add(lookupPrintServices[i]);
    }
    cbDevice.setModel(new ComboBoxModel(printerServices));
    
    cbDevice.setRenderer(new PrintServiceComboRenderer());
    centerPanel.add(cbDevice, "cell 1 1,growx");
    
    chckbxDefault = new JCheckBox(Messages.getString("AddPrinterDialog.12"));
    centerPanel.add(chckbxDefault, "cell 1 2");
    
    chkPrintAuto = new JCheckBox("Auto print receipt when settled");
    centerPanel.add(chkPrintAuto, "cell 1 3");
    
    JSeparator separator = new JSeparator();
    centerPanel.add(separator, "cell 0 3 3 1,growx,gapy 50px");
    
    add(centerPanel, "Center");
    
    JPanel panel = new JPanel();
    
    JButton btnOk = new JButton(Messages.getString("AddPrinterDialog.16"));
    btnOk.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doAddPrinter();
      }
    });
    panel.add(btnOk);
    
    JButton btnCancel = new JButton(Messages.getString("AddPrinterDialog.17"));
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
    });
    panel.add(btnCancel);
    add(panel, "South");
  }
  
  protected void doAddNewVirtualPrinter() {
    VirtualPrinterConfigDialog dialog = new VirtualPrinterConfigDialog();
    dialog.open();
    
    if (dialog.isCanceled()) {
      return;
    }
    
    VirtualPrinter virtualPrinter = dialog.getPrinter();
    DefaultComboBoxModel<VirtualPrinter> model = (DefaultComboBoxModel)cbVirtualPrinter.getModel();
    model.addElement(virtualPrinter);
    cbVirtualPrinter.setSelectedItem(virtualPrinter);
  }
  




  protected void doAddPrinter()
  {
    String name = tfName.getText();
    if (StringUtils.isEmpty(name)) {
      POSMessageDialog.showMessage(this, Messages.getString("VirtualPrinterConfigDialog.11"));
      return;
    }
    
    if (virtualPrinter == null) {
      virtualPrinter = new VirtualPrinter();
    }
    
    virtualPrinter.setName(name);
    PrintService printService = null;
    Object selectedObject = cbDevice.getSelectedItem();
    if ((selectedObject instanceof PrintService))
    {

      printService = (PrintService)selectedObject;
    }
    
    boolean defaultPrinter = chckbxDefault.isSelected();
    
    if (printer == null) {
      printer = new Printer();
    }
    printer.setVirtualPrinter(virtualPrinter);
    if ((printService != null) && (printService.getName() != null)) {
      printer.setDeviceName(printService.getName());
    }
    else {
      printer.setDeviceName(null);
    }
    printer.setDefaultPrinter(defaultPrinter);
    
    TerminalConfig.setAutoPrintReceipt(chkPrintAuto.isSelected());
    
    setCanceled(false);
    dispose();
  }
  
  public Printer getPrinter() {
    return printer;
  }
  
  public void setPrinter(Printer printer) {
    this.printer = printer;
    virtualPrinter = printer.getVirtualPrinter();
    
    tfName.setText(printer.getVirtualPrinter().getName());
    chckbxDefault.setSelected(printer.isDefaultPrinter());
    if (printer != null) {
      cbVirtualPrinter.setSelectedItem(printer.getVirtualPrinter());
      chkPrintAuto.setSelected(TerminalConfig.isAutoPrintReceipt());
      
      if (printer.getDeviceName() == "No Print") {
        cbDevice.setSelectedItem("Do not print");
        return;
      }
      ComboBoxModel deviceModel = (ComboBoxModel)cbDevice.getModel();
      for (int i = 0; i < deviceModel.getSize(); i++) {
        Object selectedObject = deviceModel.getElementAt(i);
        if ((selectedObject instanceof PrintService))
        {
          PrintService printService = (PrintService)selectedObject;
          if ((printService != null) && 
            (printService.getName().equals(printer.getDeviceName()))) {
            cbDevice.setSelectedIndex(i);
            break;
          }
        }
      }
    }
  }
  
  public void setPrinterType(String selectedPrinterType) { chkPrintAuto.setVisible(selectedPrinterType.equals(VirtualPrinter.PRINTER_TYPE_NAMES[1])); }
}
