package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.config.CardConfig;
import com.floreantpos.model.CardReader;
import com.floreantpos.swing.POSTextField;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import net.miginfocom.swing.MigLayout;



















public class DefaultMerchantGatewayConfigurationView
  extends ConfigurationView
{
  private POSTextField tfMerchantAccount;
  private JComboBox cbCardReader;
  private JPasswordField tfMerchantPass;
  private JCheckBox cbSandboxMode;
  private JCheckBox chckbxAllowMagneticSwipe;
  private JCheckBox chckbxAllowCardManual;
  private JCheckBox chckbxAllowExternalTerminal;
  private String link = "http://reseller.authorize.net/application/?resellerId=27144";
  private JButton btnCreateNewMerchantAccount;
  
  public DefaultMerchantGatewayConfigurationView() {
    setLayout(new MigLayout("", "[][grow]", ""));
    
    JLabel lblMagneticCardReader = new JLabel(Messages.getString("CardConfigurationView.9"));
    add(lblMagneticCardReader, "cell 0 3,alignx leading");
    
    cbCardReader = new JComboBox();
    cbCardReader.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        updateCheckBoxes();
      }
    });
    add(cbCardReader, "cell 1 3,growx");
    
    JLabel lblMerchantAccount = new JLabel(Messages.getString("CardConfigurationView.19"));
    add(lblMerchantAccount, "cell 0 5,alignx leading");
    
    tfMerchantAccount = new POSTextField();
    add(tfMerchantAccount, "cell 1 5,growx");
    
    JLabel lblSecretCode = new JLabel(Messages.getString("CardConfigurationView.22"));
    add(lblSecretCode, "cell 0 6,alignx leading");
    
    cbCardReader.setModel(new DefaultComboBoxModel(CardReader.values()));
    
    tfMerchantPass = new JPasswordField();
    add(tfMerchantPass, "cell 1 6,growx");
    
    chckbxAllowMagneticSwipe = new JCheckBox(Messages.getString("CardConfigurationView.3"));
    chckbxAllowMagneticSwipe.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        updateCardList();
      }
    });
    add(chckbxAllowMagneticSwipe, "skip 1, newline");
    
    chckbxAllowCardManual = new JCheckBox(Messages.getString("CardConfigurationView.5"));
    chckbxAllowCardManual.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        updateCardList();
      }
    });
    add(chckbxAllowCardManual, "skip 1, newline");
    
    chckbxAllowExternalTerminal = new JCheckBox(Messages.getString("CardConfigurationView.7"));
    chckbxAllowExternalTerminal.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        updateCardList();
      }
    });
    add(chckbxAllowExternalTerminal, "skip 1, newline");
    
    cbSandboxMode = new JCheckBox(Messages.getString("CardConfigurationView.25"));
    add(cbSandboxMode, "skip 1, newline");
    
    btnCreateNewMerchantAccount = new JButton(Messages.getString("CardConfigurationView.0"));
    btnCreateNewMerchantAccount.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          DefaultMerchantGatewayConfigurationView.this.openBrowser(link);
        }
        catch (Exception localException) {}
      }
    });
    btnCreateNewMerchantAccount.setForeground(Color.RED);
    btnCreateNewMerchantAccount.setFont(new Font(getFont().getName(), 1, 11));
    
    add(btnCreateNewMerchantAccount, "skip 1, newline");
  }
  
  public void setVisibleLinkButton(String btnText, String link, boolean visible) {
    this.link = link;
    btnCreateNewMerchantAccount.setText(btnText);
    btnCreateNewMerchantAccount.setVisible(visible);
  }
  
  private void openBrowser(String link) throws Exception {
    URI uri = new URI(link);
    if (Desktop.isDesktopSupported()) {
      Desktop.getDesktop().browse(uri);
    }
  }
  
  public void initialize() throws Exception
  {
    chckbxAllowMagneticSwipe.setSelected(CardConfig.isSwipeCardSupported());
    chckbxAllowCardManual.setSelected(CardConfig.isManualEntrySupported());
    chckbxAllowExternalTerminal.setSelected(CardConfig.isExtTerminalSupported());
    
    CardReader card = CardConfig.getCardReader();
    cbCardReader.setSelectedItem(card);
    
    String merchantAccount = CardConfig.getMerchantAccount();
    if (merchantAccount != null) {
      tfMerchantAccount.setText(merchantAccount);
    }
    
    String merchantPass = CardConfig.getMerchantPass();
    if (merchantPass != null) {
      tfMerchantPass.setText(merchantPass);
    }
    
    cbSandboxMode.setSelected(CardConfig.isSandboxMode());
    
    updateCardList();
  }
  
  public void setMerchantDefaultValue(String accountNo, String pass) {
    tfMerchantAccount.setText(accountNo);
    tfMerchantPass.setText(pass);
  }
  
  protected void updateCheckBoxes() {
    CardReader selectedItem = (CardReader)cbCardReader.getSelectedItem();
    if (selectedItem == CardReader.SWIPE) {
      chckbxAllowMagneticSwipe.setSelected(true);
    }
    else if (selectedItem == CardReader.MANUAL) {
      chckbxAllowCardManual.setSelected(true);
    }
    else if (selectedItem == CardReader.EXTERNAL_TERMINAL) {
      chckbxAllowExternalTerminal.setSelected(true);
    }
  }
  
  private DefaultComboBoxModel<CardReader> createComboBoxModel(Vector items) {
    DefaultComboBoxModel model = new DefaultComboBoxModel();
    for (Object object : items) {
      model.addElement(object);
    }
    return model;
  }
  
  protected void updateCardList() {
    boolean swipeSupported = chckbxAllowMagneticSwipe.isSelected();
    boolean manualSupported = chckbxAllowCardManual.isSelected();
    boolean extSupported = chckbxAllowExternalTerminal.isSelected();
    
    CardReader currentReader = (CardReader)cbCardReader.getSelectedItem();
    Vector<CardReader> readers = new Vector(3);
    
    if (swipeSupported) {
      readers.add(CardReader.SWIPE);
    }
    
    if (manualSupported) {
      readers.add(CardReader.MANUAL);
    }
    
    if (extSupported) {
      readers.add(CardReader.EXTERNAL_TERMINAL);
    }
    
    cbCardReader.setModel(createComboBoxModel(readers));
    if (readers.contains(currentReader)) {
      cbCardReader.setSelectedItem(currentReader);
    }
    
    if ((!swipeSupported) && (!manualSupported) && (!extSupported)) {
      cbCardReader.setEnabled(false);
      
      tfMerchantAccount.setEnabled(false);
      tfMerchantPass.setEnabled(false);
      cbSandboxMode.setEnabled(false);
    }
    else {
      cbCardReader.setEnabled(true);
      
      tfMerchantAccount.setEnabled(true);
      tfMerchantPass.setEnabled(true);
      cbSandboxMode.setEnabled(true);
    }
    
    if ((swipeSupported) || (manualSupported))
    {
      tfMerchantAccount.setEnabled(true);
      tfMerchantPass.setEnabled(true);
      cbSandboxMode.setEnabled(true);
    }
    else
    {
      tfMerchantAccount.setEnabled(false);
      tfMerchantPass.setEnabled(false);
      cbSandboxMode.setEnabled(false);
    }
  }
  
  public boolean save() throws Exception
  {
    CardConfig.setSwipeCardSupported(chckbxAllowMagneticSwipe.isSelected());
    CardConfig.setManualEntrySupported(chckbxAllowCardManual.isSelected());
    CardConfig.setExtTerminalSupported(chckbxAllowExternalTerminal.isSelected());
    
    CardReader cardReader = (CardReader)cbCardReader.getSelectedItem();
    CardConfig.setCardReader(cardReader);
    
    CardConfig.setMerchantAccount(tfMerchantAccount.getText());
    CardConfig.setMerchantPass(new String(tfMerchantPass.getPassword()));
    
    CardConfig.setSandboxMode(cbSandboxMode.isSelected());
    
    return true;
  }
  
  public String getName()
  {
    return "";
  }
}
