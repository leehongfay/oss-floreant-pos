package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.Currency;
import com.floreantpos.model.Outlet;
import com.floreantpos.model.Store;
import com.floreantpos.model.TaxGroup;
import com.floreantpos.model.dao.CurrencyDAO;
import com.floreantpos.model.dao.OutletDAO;
import com.floreantpos.model.dao.TaxGroupDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;




















public class ChargeConfigurationView
  extends ConfigurationView
{
  private POSTextField tfServiceCharge;
  private POSTextField tfDefaultGratuity;
  private POSTextField tfOvertimeMarkup;
  private JCheckBox chkUseDetailedReconciliation;
  private JCheckBox chkItemSalesPriceIncludesTax;
  private JComboBox cbTaxGroup;
  private JComboBox cbCurrency;
  private Outlet outlet;
  private Store store;
  
  public ChargeConfigurationView(Store store)
  {
    this.store = store;
    initComponents();
    initData();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    JPanel contentPanel = new JPanel(new MigLayout("fillx,hidemode 3", "[][grow]", ""));
    
    JLabel lblServiceCharge = new JLabel(Messages.getString("RestaurantConfigurationView.42") + ":");
    contentPanel.add(lblServiceCharge, "alignx trailing");
    
    tfServiceCharge = new POSTextField();
    contentPanel.add(tfServiceCharge, "growx");
    
    JLabel label = new JLabel("%");
    contentPanel.add(label, "wrap");
    
    JLabel lblDefaultGratuity = new JLabel(Messages.getString("RestaurantConfigurationView.48") + ":");
    contentPanel.add(lblDefaultGratuity, "flowy,alignx trailing");
    
    tfDefaultGratuity = new POSTextField();
    contentPanel.add(tfDefaultGratuity, "growx");
    
    JLabel label_1 = new JLabel("%");
    contentPanel.add(label_1, "wrap");
    
    JLabel lblOvertimeMarkup = new JLabel("Overtime markup:");
    contentPanel.add(lblOvertimeMarkup, "flowy,alignx trailing");
    
    tfOvertimeMarkup = new POSTextField();
    contentPanel.add(tfOvertimeMarkup, "growx");
    
    JLabel label_2 = new JLabel("%");
    contentPanel.add(label_2, "wrap");
    
    JLabel lblTaxGroup = new JLabel("Tax Group:");
    cbTaxGroup = new JComboBox();
    
    contentPanel.add(lblTaxGroup, "alignx trailing");
    contentPanel.add(cbTaxGroup, "grow, wrap");
    
    JLabel lblCurrency = new JLabel("Currency:");
    cbCurrency = new JComboBox();
    
    contentPanel.add(lblCurrency, "alignx trailing");
    contentPanel.add(cbCurrency, "grow, wrap");
    
    chkUseDetailedReconciliation = new JCheckBox("Use detailed reconciliation");
    contentPanel.add(chkUseDetailedReconciliation, "skip 1, wrap");
    
    chkItemSalesPriceIncludesTax = new JCheckBox(Messages.getString("TaxConfigurationView.4"));
    contentPanel.add(chkItemSalesPriceIncludesTax, "skip 1,wrap");
    
    JScrollPane scrollPane = new JScrollPane(contentPanel);
    scrollPane.setBorder(null);
    add(scrollPane);
  }
  
  private void initData() {
    List<TaxGroup> taxGroups = new ArrayList();
    taxGroups.add(null);
    taxGroups.addAll(TaxGroupDAO.getInstance().findAll());
    cbTaxGroup.setModel(new ComboBoxModel(taxGroups));
    
    List<Currency> currencyList = new ArrayList();
    currencyList.add(null);
    currencyList.addAll(CurrencyDAO.getInstance().findAll());
    cbCurrency.setModel(new ComboBoxModel(currencyList));
  }
  
  public boolean save()
    throws Exception
  {
    if (!isInitialized()) {
      return true;
    }
    double serviceCharge = 0.0D;
    double gratuityPercentage = 0.0D;
    String currencyName = null;
    String currencySymbol = null;
    
    if (StringUtils.isEmpty(currencyName)) {
      currencyName = POSConstants.DOLLAR;
    }
    if (StringUtils.isEmpty(currencySymbol)) {
      currencySymbol = "$";
    }
    
    serviceCharge = Double.parseDouble(tfServiceCharge.getText());
    gratuityPercentage = Double.parseDouble(tfDefaultGratuity.getText());
    double overtimeMarkup = Double.parseDouble(tfOvertimeMarkup.getText());
    if (overtimeMarkup > 100.0D) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Overtime markup rate should not be more than 100% !");
      return false;
    }
    

    store.setCurrencyName(currencyName);
    store.setCurrencySymbol(currencySymbol);
    store.addProperty("overtime.markup", String.valueOf(overtimeMarkup));
    
    store.setUseDetailedReconciliation(Boolean.valueOf(chkUseDetailedReconciliation.isSelected()));
    store.setItemPriceIncludesTax(Boolean.valueOf(chkItemSalesPriceIncludesTax.isSelected()));
    
    if (outlet != null)
    {
      outlet.setServiceChargePercentage(Double.valueOf(serviceCharge));
      outlet.setDefaultGratuityPercentage(Double.valueOf(gratuityPercentage));
      
      TaxGroup taxGroup = (TaxGroup)cbTaxGroup.getSelectedItem();
      outlet.setTaxGroup(taxGroup);
      Currency currency = (Currency)cbCurrency.getSelectedItem();
      outlet.setCurrency(currency);
      OutletDAO.getInstance().saveOrUpdate(outlet);
    }
    
    return true;
  }
  
  public void initialize() throws Exception
  {
    outlet = OutletDAO.getInstance().get(String.valueOf(store.getUniqueId()));
    
    if (outlet != null) {
      tfServiceCharge.setText(String.valueOf(outlet.getServiceChargePercentage()));
      tfDefaultGratuity.setText(String.valueOf(outlet.getDefaultGratuityPercentage()));
      cbTaxGroup.setSelectedItem(outlet.getTaxGroup());
      cbCurrency.setSelectedItem(outlet.getCurrency());
    }
    tfOvertimeMarkup.setText(String.valueOf(store.getOvertimeMarkup()));
    chkUseDetailedReconciliation.setSelected(store.isUseDetailedReconciliation().booleanValue());
    chkItemSalesPriceIncludesTax.setSelected(store.isItemPriceIncludesTax().booleanValue());
    setInitialized(true);
  }
  
  public String getName()
  {
    return POSConstants.CONFIG_TAB_CHARGE;
  }
}
