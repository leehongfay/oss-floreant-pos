package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.config.GiftCardConfig;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.FloreantPlugin;
import com.floreantpos.extension.GiftCardPaymentPlugin;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import net.miginfocom.swing.MigLayout;





















public class GiftCardConfigurationView
  extends ConfigurationView
{
  private JComboBox cbPaymentGateway;
  private JPanel pluginConfigPanel = new JPanel(new BorderLayout());
  
  public GiftCardConfigurationView() {
    createUI();
  }
  
  private void createUI() {
    setLayout(new BorderLayout());
    
    JPanel contentPanel = new JPanel();
    contentPanel.setLayout(new MigLayout("", "[][grow]", "[][][][][][][][]"));
    
    JLabel lblMerchantGateway = new JLabel(Messages.getString("CardConfigurationView.2"));
    contentPanel.add(lblMerchantGateway, "cell 0 4,alignx leading");
    
    cbPaymentGateway = new JComboBox();
    cbPaymentGateway.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          GiftCardConfigurationView.this.updatePluginConfigUI();
        } catch (Exception e1) {
          POSMessageDialog.showError(GiftCardConfigurationView.this, e1.getMessage(), e1);
        }
      }
    });
    contentPanel.add(cbPaymentGateway, "cell 1 4,growx");
    contentPanel.add(pluginConfigPanel, "newline,span,wrap,growx");
    
    JScrollPane scrollPane = new JScrollPane(contentPanel);
    scrollPane.setBorder(null);
    add(scrollPane);
  }
  
  private void initialMerchantGateways()
  {
    DefaultComboBoxModel<GiftCardPaymentPlugin> model = new DefaultComboBoxModel();
    List<FloreantPlugin> plugins = ExtensionManager.getPlugins(GiftCardPaymentPlugin.class);
    
    for (FloreantPlugin plugin : plugins) {
      if ((plugin instanceof GiftCardPaymentPlugin)) {
        model.addElement((GiftCardPaymentPlugin)plugin);
      }
    }
    
    cbPaymentGateway.setModel(model);
  }
  
  public boolean save()
    throws Exception
  {
    GiftCardPaymentPlugin plugin = (GiftCardPaymentPlugin)cbPaymentGateway.getSelectedItem();
    plugin.getConfigurationPane().save();
    
    GiftCardConfig.setPaymentGateway(plugin);
    
    return true;
  }
  
  public void initialize() throws Exception
  {
    initialMerchantGateways();
    
    updatePluginConfigUI();
    
    setInitialized(true);
  }
  
  private void updatePluginConfigUI() throws Exception {
    GiftCardPaymentPlugin plugin = (GiftCardPaymentPlugin)cbPaymentGateway.getSelectedItem();
    pluginConfigPanel.removeAll();
    if (plugin == null) {
      return;
    }
    ConfigurationView configurationPane = plugin.getConfigurationPane();
    configurationPane.initialize();
    pluginConfigPanel.add(configurationPane);
    revalidate();
    repaint();
  }
  
  public String getName()
  {
    return Messages.getString("GiftCardConfigurationView.0");
  }
}
