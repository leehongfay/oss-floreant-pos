package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.Store;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.POSTextField;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;


















public class StoreConfigurationView
  extends ConfigurationView
{
  private Store store;
  private FixedLengthTextField tfRestaurantName;
  private FixedLengthTextField tfOutletName;
  private FixedLengthTextField tfAddressLine1;
  private FixedLengthTextField tfAddressLine2;
  private FixedLengthTextField tfAddressLine3;
  private POSTextField tfTelephone;
  private JTextField tfZipCode;
  
  public StoreConfigurationView(Store store)
  {
    this.store = store;
    
    setLayout(new BorderLayout());
    JPanel contentPanel = new JPanel(new MigLayout("fillx,hidemode 3", "[][grow][][grow]", ""));
    
    JLabel lblNewLabel = new JLabel(Messages.getString("RestaurantConfigurationView.3") + ":");
    contentPanel.add(lblNewLabel, "cell 0 1,alignx trailing");
    
    tfRestaurantName = new FixedLengthTextField();
    tfRestaurantName.setLength(120);
    contentPanel.add(tfRestaurantName, "cell 1 1 3 1,growx");
    
    JLabel lblOutletName = new JLabel("Outlet Name:");
    contentPanel.add(lblOutletName, "cell 0 2,alignx trailing");
    
    tfOutletName = new FixedLengthTextField();
    tfOutletName.setLength(120);
    contentPanel.add(tfOutletName, "cell 1 2 3 1,growx");
    
    JLabel lblAddressLine = new JLabel(Messages.getString("RestaurantConfigurationView.7") + ":");
    contentPanel.add(lblAddressLine, "cell 0 3,alignx trailing");
    
    tfAddressLine1 = new FixedLengthTextField();
    tfAddressLine1.setLength(60);
    contentPanel.add(tfAddressLine1, "cell 1 3 3 1,growx");
    
    JLabel lblAddressLine_1 = new JLabel(Messages.getString("RestaurantConfigurationView.11") + ":");
    contentPanel.add(lblAddressLine_1, "cell 0 4,alignx trailing");
    
    tfAddressLine2 = new FixedLengthTextField();
    tfAddressLine2.setLength(60);
    contentPanel.add(tfAddressLine2, "cell 1 4 3 1,growx");
    
    JLabel lblAddressLine_2 = new JLabel(Messages.getString("RestaurantConfigurationView.15") + ":");
    contentPanel.add(lblAddressLine_2, "cell 0 5,alignx trailing");
    
    tfAddressLine3 = new FixedLengthTextField();
    tfAddressLine3.setLength(60);
    contentPanel.add(tfAddressLine3, "cell 1 5 3 1,growx");
    
    JLabel lblZipCode = new JLabel(Messages.getString("RestaurantConfigurationView.19"));
    contentPanel.add(lblZipCode, "cell 0 6,alignx trailing");
    
    tfZipCode = new JTextField();
    contentPanel.add(tfZipCode, "cell 1 6,growx");
    tfZipCode.setColumns(10);
    
    JLabel lblPhone = new JLabel(Messages.getString("RestaurantConfigurationView.22"));
    contentPanel.add(lblPhone, "cell 0 7,alignx trailing");
    
    tfTelephone = new POSTextField();
    contentPanel.add(tfTelephone, "cell 1 7,growx");
    
    JScrollPane scrollPane = new JScrollPane(contentPanel);
    scrollPane.setBorder(null);
    add(scrollPane);
  }
  
  public boolean save() throws Exception
  {
    if (!isInitialized()) {
      return true;
    }
    String name = null;
    String addr1 = null;
    String addr2 = null;
    String addr3 = null;
    String telephone = null;
    
    name = tfRestaurantName.getText();
    addr1 = tfAddressLine1.getText();
    addr2 = tfAddressLine2.getText();
    addr3 = tfAddressLine3.getText();
    telephone = tfTelephone.getText();
    
    store.setName(name);
    store.setOutletName(tfOutletName.getText());
    store.setAddressLine1(addr1);
    store.setAddressLine2(addr2);
    store.setAddressLine3(addr3);
    store.setZipCode(tfZipCode.getText());
    store.setTelephone(telephone);
    
    return true;
  }
  
  public void initialize() throws Exception
  {
    tfRestaurantName.setText(store.getName());
    tfOutletName.setText(store.getOutletName());
    tfAddressLine1.setText(store.getAddressLine1());
    tfAddressLine2.setText(store.getAddressLine2());
    tfAddressLine3.setText(store.getAddressLine3());
    tfZipCode.setText(store.getZipCode());
    tfTelephone.setText(store.getTelephone());
    
    setInitialized(true);
  }
  
  public String getName()
  {
    return POSConstants.CONFIG_TAB_STORE;
  }
}
