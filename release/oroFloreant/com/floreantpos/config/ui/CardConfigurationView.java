package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.config.CardConfig;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.FloreantPlugin;
import com.floreantpos.extension.MercuryGatewayPlugin;
import com.floreantpos.extension.PaymentGatewayPlugin;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;




















public class CardConfigurationView
  extends ConfigurationView
{
  private JComboBox cbGateway;
  private DoubleTextField tfBarTabLimit = new DoubleTextField(10);
  private DoubleTextField tfAdvanceTipsPercentage = new DoubleTextField(10);
  
  private JPanel pluginConfigPanel = new JPanel(new BorderLayout());
  private JCheckBox chkPreAuthBarTab;
  
  public CardConfigurationView() {
    createUI();
  }
  
  private void createUI() {
    setLayout(new BorderLayout());
    
    JPanel contentPanel = new JPanel();
    contentPanel.setLayout(new MigLayout("", "[][grow]", ""));
    
    JLabel lblMerchantGateway = new JLabel(Messages.getString("CardConfigurationView.2"));
    contentPanel.add(lblMerchantGateway, "cell 0 4,alignx leading");
    
    cbGateway = new JComboBox();
    cbGateway.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          CardConfigurationView.this.updatePluginConfigUI();
        } catch (Exception e1) {
          POSMessageDialog.showError(CardConfigurationView.this, e1.getMessage(), e1);
        }
        
      }
    });
    chkPreAuthBarTab = new JCheckBox();
    chkPreAuthBarTab.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        JCheckBox chkbox = (JCheckBox)e.getSource();
        tfBarTabLimit.setEnabled(chkbox.isSelected());
      }
    });
    contentPanel.add(cbGateway, "cell 1 4,growx");
    contentPanel.add(pluginConfigPanel, "newline,span,wrap,growx");
    
    contentPanel.add(new JLabel("Preauthorization of bar tab"), "cell 0 6");
    contentPanel.add(chkPreAuthBarTab, "cell 1 6");
    
    contentPanel.add(new JLabel(Messages.getString("CardConfigurationView.1")), "cell 0 7");
    contentPanel.add(tfBarTabLimit, "cell 1 7");
    
    contentPanel.add(new JLabel(Messages.getString("CardConfigurationView.4")), "cell 0 8");
    contentPanel.add(tfAdvanceTipsPercentage, "cell 1 8");
    
    contentPanel.add(new JLabel(Messages.getString("CardConfigurationView.10")), "cell 1 8");
    
    JSeparator separator = new JSeparator(0);
    contentPanel.add(separator, "newline, growx, span 10, wrap");
    
    JScrollPane scrollPane = new JScrollPane(contentPanel);
    scrollPane.setBorder(null);
    add(scrollPane);
  }
  
  private void initialMerchantGateways()
  {
    DefaultComboBoxModel<PaymentGatewayPlugin> model = new DefaultComboBoxModel();
    List<FloreantPlugin> plugins = ExtensionManager.getPlugins(PaymentGatewayPlugin.class);
    
    for (FloreantPlugin plugin : plugins) {
      if (!(plugin instanceof MercuryGatewayPlugin)) {
        model.addElement((PaymentGatewayPlugin)plugin);
      }
    }
    
    cbGateway.setModel(model);
    cbGateway.setSelectedItem(CardConfig.getPaymentGateway());
  }
  
  public boolean save() throws Exception
  {
    PaymentGatewayPlugin plugin = (PaymentGatewayPlugin)cbGateway.getSelectedItem();
    if (!plugin.getConfigurationPane().save()) {
      return false;
    }
    
    CardConfig.setPaymentGateway(plugin);
    
    CardConfig.setPreAuthBartab(chkPreAuthBarTab.isSelected());
    CardConfig.setBartabLimit(tfBarTabLimit.getDouble());
    CardConfig.setAdvanceTipsPercentage(tfAdvanceTipsPercentage.getDouble());
    
    return true;
  }
  
  public void initialize() throws Exception
  {
    initialMerchantGateways();
    
    chkPreAuthBarTab.setSelected(CardConfig.isPreAuthBartab());
    tfBarTabLimit.setEnabled(chkPreAuthBarTab.isSelected());
    tfBarTabLimit.setText(String.valueOf(CardConfig.getBartabLimit()));
    tfAdvanceTipsPercentage.setText(String.valueOf(CardConfig.getAdvanceTipsPercentage()));
    
    updatePluginConfigUI();
    
    setInitialized(true);
  }
  
  private void updatePluginConfigUI() throws Exception {
    PaymentGatewayPlugin plugin = (PaymentGatewayPlugin)cbGateway.getSelectedItem();
    pluginConfigPanel.removeAll();
    if (plugin == null) {
      return;
    }
    ConfigurationView configurationPane = plugin.getConfigurationPane();
    configurationPane.initialize();
    pluginConfigPanel.add(configurationPane);
    revalidate();
    repaint();
  }
  
  public String getName()
  {
    return Messages.getString("CardConfigurationView.6");
  }
}
