package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.PosLog;
import com.floreantpos.customPayment.CustomPaymentForm;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.model.CustomPayment;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.dao.CustomPaymentDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;




public class PaymentOptionConfigurationView
  extends ConfigurationView
{
  private Terminal terminal;
  private JCheckBox chkCashPayment;
  private JCheckBox chkCardPayment;
  private JCheckBox chkGiftCardPayment;
  private JCheckBox chkCustomPayment;
  private JCheckBox chkShowIndivBtn;
  private JCheckBox chkCustomerBlncePayment;
  private JTable table;
  
  public PaymentOptionConfigurationView(Terminal terminal)
  {
    this.terminal = terminal;
    setLayout(new BorderLayout());
    
    createUI();
    initData();
    initEvent();
  }
  
  public void initData()
  {
    OrderServiceExtension orderServiceExtension = (OrderServiceExtension)ExtensionManager.getPlugin(OrderServiceExtension.class);
    if (orderServiceExtension == null) {
      chkCustomerBlncePayment.setVisible(false);
    }
    
    List<CustomPayment> customPaymentList = CustomPaymentDAO.getInstance().findAll();
    BeanTableModel tableModel = (BeanTableModel)table.getModel();
    tableModel.removeAll();
    tableModel.addRows(customPaymentList);
  }
  
  private void initEvent() {
    chkCustomPayment.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        JCheckBox chkBox = (JCheckBox)e.getSource();
        if (chkBox.isSelected()) {
          chkShowIndivBtn.setSelected(false);
          chkShowIndivBtn.setEnabled(false);
        }
        else
        {
          chkShowIndivBtn.setEnabled(true);
        }
      }
    });
  }
  

  private void createUI()
  {
    JPanel mainPanel = new JPanel(new MigLayout("fillx, hidemode 3"));
    
    chkCashPayment = new JCheckBox("Hide CASH payment button");
    chkCardPayment = new JCheckBox("Hide CARD payment button");
    chkGiftCardPayment = new JCheckBox("Hide GIFT CARD payment button");
    
    chkCustomPayment = new JCheckBox("Hide CUSTOM payment");
    chkShowIndivBtn = new JCheckBox("Show custom payments as individual buttons");
    
    chkCustomerBlncePayment = new JCheckBox("Hide CUST. ACCT. payment button");
    
    table = new JTable();
    JScrollPane scrollPane = new JScrollPane(table);
    
    BeanTableModel<CustomPayment> tableModel = new BeanTableModel(CustomPayment.class);
    
    tableModel.addColumn(Messages.getString("CustomPaymentBrowser.0"), CustomPayment.PROP_ID);
    tableModel.addColumn(Messages.getString("CustomPaymentBrowser.1"), CustomPayment.PROP_NAME);
    tableModel.addColumn(Messages.getString("CustomPaymentBrowser.2"), CustomPayment.PROP_REF_NUMBER_FIELD_NAME);
    
    table.setModel(tableModel);
    
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          PaymentOptionConfigurationView.this.doEditPayment();
        }
        
      }
    });
    JPanel customPaymentPanel = new JPanel(new BorderLayout());
    TitledBorder border = BorderFactory.createTitledBorder(null, "CUSTOM PAYMENT", 2, 0);
    customPaymentPanel.setBorder(border);
    customPaymentPanel.add(scrollPane, "Center");
    
    JPanel buttonPanel = new JPanel(new MigLayout("center"));
    
    JButton btnNew = new JButton("NEW");
    JButton btnEdit = new JButton("EDIT");
    JButton btnDelete = new JButton("DELETE");
    
    btnNew.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PaymentOptionConfigurationView.this.doAddPayment();
      }
      
    });
    btnEdit.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PaymentOptionConfigurationView.this.doEditPayment();
      }
      
    });
    btnDelete.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PaymentOptionConfigurationView.this.doDeletePayment();
      }
      
    });
    buttonPanel.add(btnNew);
    buttonPanel.add(btnEdit);
    buttonPanel.add(btnDelete);
    
    customPaymentPanel.add(buttonPanel, "South");
    
    mainPanel.add(chkCashPayment, "wrap");
    mainPanel.add(chkCardPayment, "wrap");
    mainPanel.add(chkGiftCardPayment, "wrap");
    mainPanel.add(chkCustomerBlncePayment, "wrap");
    mainPanel.add(new JSeparator(), "grow, wrap");
    mainPanel.add(chkCustomPayment, "wrap");
    mainPanel.add(chkShowIndivBtn, "gapleft 15, wrap");
    
    mainPanel.add(customPaymentPanel, "grow, span");
    
    add(mainPanel);
  }
  
  public boolean save() {
    try {
      if (!updateModel()) {
        return false;
      }
      
      TerminalDAO termDAO = new TerminalDAO();
      termDAO.saveOrUpdate(terminal);
      
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(e.getMessage());
    }
    
    return false;
  }
  
  public void updateView()
  {
    try {
      if (terminal == null) {
        return;
      }
      

      boolean hideCashButton = terminal.getProperty("hideCashPayment") == null ? false : Boolean.valueOf(terminal.getProperty("hideCashPayment")).booleanValue();
      
      boolean hideCardButton = terminal.getProperty("hideCardPayment") == null ? false : Boolean.valueOf(terminal.getProperty("hideCardPayment")).booleanValue();
      
      boolean hideGiftCardButton = terminal.getProperty("hideGiftCardPayment") == null ? false : Boolean.valueOf(terminal.getProperty("hideGiftCardPayment")).booleanValue();
      
      boolean hideCustomPaymentButton = terminal.getProperty("hideCustomPayment") == null ? false : Boolean.valueOf(terminal.getProperty("hideCustomPayment")).booleanValue();
      
      boolean showIndivButton = terminal.getProperty("showIndivBtn") == null ? false : Boolean.valueOf(terminal.getProperty("showIndivBtn")).booleanValue();
      

      boolean hideCustBlnceButton = terminal.getProperty("hideCustBlncPayment") == null ? false : Boolean.valueOf(terminal.getProperty("hideCustBlncPayment")).booleanValue();
      
      chkCashPayment.setSelected(hideCashButton);
      chkCardPayment.setSelected(hideCardButton);
      chkGiftCardPayment.setSelected(hideGiftCardButton);
      chkCustomPayment.setSelected(hideCustomPaymentButton);
      chkShowIndivBtn.setSelected(showIndivButton);
      
      chkCustomerBlncePayment.setSelected(hideCustBlnceButton);
      
      chkShowIndivBtn.setEnabled(!hideCustomPaymentButton);
      
      setInitialized(true);
    }
    catch (Exception e) {
      PosLog.error(PaymentOptionConfigurationView.class, e);
    }
  }
  
  public boolean updateModel() {
    Terminal term = terminal;
    
    term.addProperty("hideCashPayment", String.valueOf(chkCashPayment.isSelected()));
    term.addProperty("hideCardPayment", String.valueOf(chkCardPayment.isSelected()));
    term.addProperty("hideGiftCardPayment", String.valueOf(chkGiftCardPayment.isSelected()));
    term.addProperty("hideCustomPayment", String.valueOf(chkCustomPayment.isSelected()));
    term.addProperty("showIndivBtn", String.valueOf(chkShowIndivBtn.isSelected()));
    term.addProperty("hideCustBlncPayment", String.valueOf(chkCustomerBlncePayment.isSelected()));
    
    return true;
  }
  
  public void initialize() throws Exception
  {
    updateView();
  }
  

  public String getName()
  {
    return "Payment";
  }
  
  private void doAddPayment() {
    CustomPaymentForm customPaymentForm = new CustomPaymentForm();
    customPaymentForm.createNew();
    customPaymentForm.setFieldsEnable(true);
    
    BeanEditorDialog dialog = new BeanEditorDialog(customPaymentForm);
    dialog.openWithScale(500, 500);
    BeanTableModel tableModel = (BeanTableModel)table.getModel();
    tableModel.addRow(customPaymentForm.getBean());
  }
  
  private void doEditPayment() {
    int index = table.getSelectedRow();
    if (index < 0) {
      return;
    }
    index = table.convertRowIndexToModel(index);
    
    BeanTableModel tableModel = (BeanTableModel)table.getModel();
    CustomPayment customPayment = (CustomPayment)tableModel.getRow(index);
    CustomPaymentForm customPaymentForm = new CustomPaymentForm(customPayment);
    customPaymentForm.setFieldsEnable(true);
    BeanEditorDialog dialog = new BeanEditorDialog(customPaymentForm);
    
    dialog.openWithScale(500, 500);
    tableModel.fireTableDataChanged();
  }
  
  private void doDeletePayment() {
    int index = table.getSelectedRow();
    if (index < 0) {
      return;
    }
    index = table.convertRowIndexToModel(index);
    
    BeanTableModel tableModel = (BeanTableModel)table.getModel();
    CustomPayment customPayment = (CustomPayment)tableModel.getRow(index);
    CustomPaymentForm customPaymentForm = new CustomPaymentForm(customPayment);
    
    int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Are you sure to delete this custom payment?", "Delete Confirmation:");
    if (option != 0) {
      return;
    }
    customPaymentForm.delete();
    tableModel.removeRow(index);
    tableModel.fireTableDataChanged();
  }
}
