package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.model.CardReader;
import com.floreantpos.model.Customer;
import com.floreantpos.model.Department;
import com.floreantpos.model.ModifiableTicketItem;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PaymentType;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.ReceiptParam;
import com.floreantpos.model.SalesArea;
import com.floreantpos.model.Store;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemCookingInstruction;
import com.floreantpos.model.TicketItemModifier;
import com.floreantpos.model.TicketItemSeat;
import com.floreantpos.model.TicketItemTax;
import com.floreantpos.model.TransactionType;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.OrderTypeDAO;
import com.floreantpos.model.dao.StoreDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.ListModel;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.OrderInfoView;
import com.floreantpos.ui.views.TicketReceiptView;
import com.floreantpos.util.GlobalIdGenerator;
import com.floreantpos.util.NumericGlobalIdGenerator;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.ReceiptUtil;
import com.floreantpos.util.ShiftUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JasperPrint;
import org.apache.commons.lang.StringUtils;



































public class ReceiptConfigurationView
  extends JPanel
{
  private JTextPane ticketHeaderTextArea;
  private JTextPane ticketfooterTextArea;
  private JTextPane ticketBottomTextArea;
  private JTextPane orderInfoTextPane;
  private JTextPane additionalOrderInfoTextPane;
  private JList<ReceiptParam> listParams;
  private OrderType selectedOrderType;
  private Store store;
  private ButtonGroup btnGroup2;
  private boolean kitchenReceipt;
  private OrderInfoView view;
  private KitchenReceiptView kitchenReceiptView;
  private JCheckBox cbTipsSuggestion = new JCheckBox("Show tips suggestion");
  private JComboBox cbModifierColor = new JComboBox();
  private JComboBox cbSeatColor = new JComboBox();
  private JComboBox cbHeaderTextFontFamily = new JComboBox();
  private JComboBox cbHeaderTextFontSize = new JComboBox();
  private JComboBox cbBodyTextFontFamily = new JComboBox();
  private JComboBox cbBodyTextFontSize = new JComboBox();
  private String[] fonts;
  private JComboBox cbCookingInstructionColor = new JComboBox();
  private JCheckBox chkModifierPrice = new JCheckBox("Show modifier price");
  private JCheckBox cbTipsBlock = new JCheckBox("Show tips block");
  private JCheckBox chkTaxBreakDown = new JCheckBox("Show tax breakdown");
  private POSToggleButton btnGuestCheck;
  private POSToggleButton btnGuestCheckCreditCard;
  
  public ReceiptConfigurationView() {
    this(false);
  }
  
  public ReceiptConfigurationView(boolean kitchenReceipt) {
    this.kitchenReceipt = kitchenReceipt;
    initComponents();
    try {
      initialize();
    } catch (Exception e1) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e1.getMessage(), e1);
    }
  }
  
  public void initialize() throws Exception {
    Application.getInstance().refreshStore();
    store = Application.getInstance().getStore();
    ReceiptParam[] receiptParams = ReceiptParam.values();
    Comparator<ReceiptParam> receiptParamcComparator = new Comparator()
    {
      public int compare(ReceiptParam o1, ReceiptParam o2) {
        return o1.getParamName().compareToIgnoreCase(o2.getParamName());
      }
    };
    Arrays.sort(receiptParams, receiptParamcComparator);
    listParams.setModel(new ListModel(Arrays.asList(receiptParams)));
    
    doUpdateFields();
  }
  
  private String getHtmlColorValue(Object selectedItem) {
    String hex = "#000000";
    Color color = Color.black;
    if (selectedItem != null)
      color = color;
    try {
      hex = "#" + Integer.toHexString(color.getRGB()).substring(2);
    }
    catch (Exception localException) {}
    return hex;
  }
  
  public boolean save(boolean isContainOrderTypeId) throws Exception {
    ticketHeaderTextArea.getDocument().putProperty("__EndOfLine__", "<br>");
    orderInfoTextPane.getDocument().putProperty("__EndOfLine__", "<br>");
    additionalOrderInfoTextPane.getDocument().putProperty("__EndOfLine__", "<br>");
    ticketfooterTextArea.getDocument().putProperty("__EndOfLine__", "<br>");
    ticketBottomTextArea.getDocument().putProperty("__EndOfLine__", "<br>");
    
    String ticketHeader = ticketHeaderTextArea.getText();
    String orderInfo = orderInfoTextPane.getText();
    String extraOrderInfo = additionalOrderInfoTextPane.getText();
    String footerInfo = ticketfooterTextArea.getText();
    String bottomInfo = ticketBottomTextArea.getText();
    
    if (ticketHeader.replaceAll("<br>", "").isEmpty()) {
      ticketHeader = "";
    }
    if (orderInfo.replaceAll("<br>", "").isEmpty()) {
      orderInfo = "";
    }
    if (extraOrderInfo.replaceAll("<br>", "").isEmpty()) {
      extraOrderInfo = "";
    }
    if (footerInfo.replaceAll("<br>", "").isEmpty()) {
      footerInfo = "";
    }
    if (bottomInfo.replaceAll("<br>", "").isEmpty()) {
      bottomInfo = "";
    }
    String orderTypeId = "";
    if ((selectedOrderType != null) && (isContainOrderTypeId)) {
      orderTypeId = selectedOrderType.getId() + ".";
    }
    
    Map<String, String> properties = store.getProperties();
    
    if (!kitchenReceipt) {
      POSUtil.storeLongProperty(properties, orderTypeId + "ticket.header", ticketHeader, 255);
      POSUtil.storeLongProperty(properties, orderTypeId + "ticket.order.info", orderInfo, 255);
      POSUtil.storeLongProperty(properties, orderTypeId + "ticket.order.extrainfo1", extraOrderInfo, 255);
      POSUtil.storeLongProperty(properties, orderTypeId + "ticket.footer", footerInfo, 255);
      POSUtil.storeLongProperty(properties, orderTypeId + "ticket.bottom", bottomInfo, 255);
      store.addProperty(orderTypeId + "ticket.modifier.color", getHtmlColorValue(cbModifierColor.getSelectedItem()));
      store.addProperty(orderTypeId + "ticket.seat.color", getHtmlColorValue(cbSeatColor.getSelectedItem()));
      store.addProperty(orderTypeId + "ticket.instruction.color", getHtmlColorValue(cbCookingInstructionColor.getSelectedItem()));
      store.addProperty(orderTypeId + "receipt.show_tips_suggestion", String.valueOf(cbTipsSuggestion.isSelected()));
      store.addProperty(orderTypeId + "receipt.show_tips_block", String.valueOf(cbTipsBlock.isSelected()));
      store.addProperty(orderTypeId + "receipt.show_tax_breakdown", String.valueOf(chkTaxBreakDown.isSelected()));
      boolean isShowModifierPrice = chkModifierPrice.isSelected();
      store.addProperty("showModifierPrice", String.valueOf(isShowModifierPrice));
    }
    else {
      POSUtil.storeLongProperty(properties, orderTypeId + "kitchen.header", ticketHeader, 255);
      POSUtil.storeLongProperty(properties, orderTypeId + "kitchen.order.info", orderInfo, 255);
      POSUtil.storeLongProperty(properties, orderTypeId + "kitchen.order.extrainfo1", extraOrderInfo, 255);
      POSUtil.storeLongProperty(properties, orderTypeId + "kitchen.footer", footerInfo, 255);
      POSUtil.storeLongProperty(properties, orderTypeId + "ticket.bottom", bottomInfo, 255);
      store.addProperty(orderTypeId + "kitchen_ticket.modifier.color", getHtmlColorValue(cbModifierColor.getSelectedItem()));
      store.addProperty(orderTypeId + "kitchen_ticket.seat.color", getHtmlColorValue(cbSeatColor.getSelectedItem()));
      store.addProperty(orderTypeId + "kitchen_ticket.instruction.color", getHtmlColorValue(cbCookingInstructionColor.getSelectedItem()));
    }
    
    String storeLogoParam = ReceiptParam.STORE_LOGO.getParamName();
    
    if ((ticketHeader.contains(storeLogoParam)) || (extraOrderInfo.contains(storeLogoParam)) || (orderInfo.contains(storeLogoParam)) || (footerInfo.contains(storeLogoParam)) || (bottomInfo.contains(storeLogoParam))) {
      POSUtil.storeLongProperty(properties, orderTypeId + "ticket.header.logo.show", "true", 255);
    }
    else {
      POSUtil.storeLongProperty(properties, orderTypeId + "ticket.header.logo.show", "false", 255);
    }
    
    StoreDAO.getInstance().saveOrUpdate(store);
    Application.getInstance().refreshStore();
    return true;
  }
  
  private void initComponents()
  {
    setLayout(new BorderLayout());
    JPanel contentPanel = new JPanel();
    
    int size = getPosWindowgetSizewidth / 3 + 80;
    int width = PosUIManager.getSize(size);
    int height = PosUIManager.getSize(125);
    int fontSize = PosUIManager.getDefaultFontSize();
    
    contentPanel.setLayout(new MigLayout("ins 0 5 0 5 ,wrap 1,filly", "[" + width + "]", "[" + height + "][grow," + height + "][grow," + height + "][grow," + height + "][" + height + "][]"));
    
    listParams = new JList();
    listParams.setFocusable(false);
    listParams.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
          ReceiptConfigurationView.this.insertReceiptParam();
        }
      }
    });
    JScrollPane listParamScrollPane = new JScrollPane(listParams);
    listParamScrollPane.setBorder(BorderFactory.createTitledBorder("Params"));
    listParamScrollPane.setPreferredSize(PosUIManager.getSize(130, 200));
    add(listParamScrollPane, "West");
    
    ticketHeaderTextArea = new JTextPane();
    SimpleAttributeSet attributes = new SimpleAttributeSet();
    StyleConstants.setFontSize(attributes, fontSize);
    StyleConstants.setAlignment(attributes, 1);
    ticketHeaderTextArea.setParagraphAttributes(attributes, true);
    
    JScrollPane headerSectionScrollPane = new JScrollPane(ticketHeaderTextArea);
    headerSectionScrollPane.setBorder(BorderFactory.createTitledBorder(Messages.getString("ReceiptConfigurationView.26")));
    contentPanel.add(headerSectionScrollPane, "grow");
    
    orderInfoTextPane = new JTextPane();
    orderInfoTextPane.setParagraphAttributes(attributes, true);
    JScrollPane orderInfoScrollPane = new JScrollPane(orderInfoTextPane);
    orderInfoScrollPane.setBorder(BorderFactory.createTitledBorder(Messages.getString("ReceiptConfigurationView.28")));
    contentPanel.add(orderInfoScrollPane, "grow");
    
    additionalOrderInfoTextPane = new JTextPane();
    StyleConstants.setAlignment(attributes, 0);
    additionalOrderInfoTextPane.setParagraphAttributes(attributes, true);
    JScrollPane orderExtraInfoScrollPane = new JScrollPane(additionalOrderInfoTextPane);
    orderExtraInfoScrollPane.setBorder(BorderFactory.createTitledBorder(Messages.getString("ReceiptConfigurationView.30")));
    contentPanel.add(orderExtraInfoScrollPane, "grow");
    
    ticketBottomTextArea = new JTextPane();
    StyleConstants.setAlignment(attributes, 0);
    ticketBottomTextArea.setParagraphAttributes(attributes, true);
    
    JScrollPane bottomSectionScrollPane = new JScrollPane(ticketBottomTextArea);
    bottomSectionScrollPane.setBorder(BorderFactory.createTitledBorder("Bottom Section"));
    contentPanel.add(bottomSectionScrollPane, "grow");
    
    ticketfooterTextArea = new JTextPane();
    StyleConstants.setAlignment(attributes, 1);
    ticketfooterTextArea.setParagraphAttributes(attributes, true);
    
    JScrollPane footerSectionScrollPane = new JScrollPane(ticketfooterTextArea);
    footerSectionScrollPane.setBorder(BorderFactory.createTitledBorder(Messages.getString("ReceiptConfigurationView.32")));
    contentPanel.add(footerSectionScrollPane, "grow");
    
    contentPanel.add(createTipsPanel(), "grow");
    


    contentPanel.add(createColorSelectionPanel(), "grow");
    
    JScrollPane scrollPane = new JScrollPane(contentPanel);
    scrollPane.setBorder(null);
    add(scrollPane);
    
    JPanel bottomPanel = new JPanel(new MigLayout("fillx,ins 3 0 3 0"));
    bottomPanel.add(new JSeparator(), "span,growx");
    
    JButton btnRestoreDefault = new JButton(POSConstants.RESTORE_DEFAULTS);
    btnRestoreDefault.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), Messages.getString("ReceiptConfigurationView.35"), POSConstants.CONFIRM) == 1)
        {
          return;
        }
        ReceiptConfigurationView.this.populateDefaultReceiptProperties();
      }
    });
    bottomPanel.add(btnRestoreDefault, "split 2,center, gapbottom 0");
    JButton btnOk = new JButton(POSConstants.SAVE);
    btnOk.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          save(true);
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("ReceiptConfigurationView.36"));
          if (kitchenReceipt) {
            ReceiptConfigurationView.this.doShowKitchenReceiptPrintPriview(false);
          } else
            ReceiptConfigurationView.this.doShowPrintPriview(false);
        } catch (Exception e1) {
          BOMessageDialog.showError(e1.getMessage());
        }
      }
    });
    bottomPanel.add(btnOk);
    
    add(bottomPanel, "South");
    
    if (kitchenReceipt) {
      initKitchenReceiptPrintPreview();
    }
    else {
      initReceiptPrintPreview();
    }
  }
  
  private JPanel createTipsPanel() {
    JPanel tipsPanel = new JPanel(new MigLayout());
    tipsPanel.add(cbTipsSuggestion);
    tipsPanel.add(chkModifierPrice);
    tipsPanel.add(cbTipsBlock);
    tipsPanel.add(chkTaxBreakDown);
    return tipsPanel;
  }
  
  private JPanel createTextSelectionPanel()
  {
    JPanel textSelectionPanel = new JPanel(new GridLayout(0, 1));
    
    GraphicsEnvironment graphics = GraphicsEnvironment.getLocalGraphicsEnvironment();
    fonts = graphics.getAvailableFontFamilyNames();
    cbHeaderTextFontFamily.setModel(new DefaultComboBoxModel(fonts));
    cbBodyTextFontFamily.setModel(new DefaultComboBoxModel(fonts));
    
    Integer[] sizes = { Integer.valueOf(10), Integer.valueOf(12), Integer.valueOf(14), Integer.valueOf(16), Integer.valueOf(18), Integer.valueOf(20), Integer.valueOf(22), Integer.valueOf(24) };
    
    cbHeaderTextFontSize.setModel(new DefaultComboBoxModel(sizes));
    cbBodyTextFontSize.setModel(new DefaultComboBoxModel(sizes));
    
    JPanel headerTextPanel = new JPanel(new MigLayout("ins 2,fillx", "[]10[]", ""));
    headerTextPanel.setBorder(BorderFactory.createTitledBorder(Messages.getString("ReceiptConfigurationView.72")));
    headerTextPanel.add(new JLabel(Messages.getString("ReceiptConfigurationView.73")), "split 2");
    headerTextPanel.add(cbHeaderTextFontFamily);
    headerTextPanel.add(new JLabel(Messages.getString("ReceiptConfigurationView.74")), "split 2");
    headerTextPanel.add(cbHeaderTextFontSize);
    
    JPanel bodyTextPanel = new JPanel(new MigLayout("ins 2,fillx", "[]10[]", ""));
    bodyTextPanel.setBorder(BorderFactory.createTitledBorder(Messages.getString("ReceiptConfigurationView.71")));
    bodyTextPanel.add(new JLabel(Messages.getString("ReceiptConfigurationView.73")), "split 2");
    bodyTextPanel.add(cbBodyTextFontFamily);
    bodyTextPanel.add(new JLabel(Messages.getString("ReceiptConfigurationView.74")), "split 2");
    bodyTextPanel.add(cbBodyTextFontSize);
    
    textSelectionPanel.add(headerTextPanel, "grow, wrap");
    textSelectionPanel.add(bodyTextPanel, "grow");
    return textSelectionPanel;
  }
  
  private JPanel createColorSelectionPanel() {
    JPanel colorSelectionPanel = new JPanel(new MigLayout("ins 2,fillx"));
    colorSelectionPanel.setBorder(BorderFactory.createTitledBorder(Messages.getString("ReceiptConfigurationView.9")));
    cbModifierColor.setModel(new DefaultComboBoxModel(SimpleColor.values()));
    cbCookingInstructionColor.setModel(new DefaultComboBoxModel(SimpleColor.values()));
    cbSeatColor.setModel(new DefaultComboBoxModel(SimpleColor.values()));
    colorSelectionPanel.add(new JLabel(Messages.getString("ReceiptConfigurationView.10")), "split 2");
    colorSelectionPanel.add(cbModifierColor, "");
    colorSelectionPanel.add(new JLabel(Messages.getString("ReceiptConfigurationView.13")));
    colorSelectionPanel.add(cbSeatColor, "");
    colorSelectionPanel.add(new JLabel(Messages.getString("ReceiptConfigurationView.15")));
    colorSelectionPanel.add(cbCookingInstructionColor, "");
    return colorSelectionPanel;
  }
  
  private void populateDefaultReceiptProperties() {
    try {
      refresh();
      if (kitchenReceipt) {
        ReceiptUtil.populateDefaultKitchenReceiptProperties(store);
      } else {
        ReceiptUtil.populateDefaultTicketReceiptProperties(store);
      }
      deleteOrderTypeBasedProperties();
      initialize();
      save(false);
      if (kitchenReceipt) {
        doShowKitchenReceiptPrintPriview(false);
      } else
        doShowPrintPriview(false);
    } catch (Exception e1) {
      BOMessageDialog.showError(e1.getMessage());
    }
  }
  
  private void deleteOrderTypeBasedProperties() {
    if (selectedOrderType == null) {
      return;
    }
    String ordertypeId = selectedOrderType.getId();
    try {
      if (!kitchenReceipt) {
        POSUtil.removeLongProperty(store.getProperties(), ordertypeId + "." + "ticket.header");
        POSUtil.removeLongProperty(store.getProperties(), ordertypeId + "." + "ticket.order.info");
        POSUtil.removeLongProperty(store.getProperties(), ordertypeId + "." + "ticket.footer");
        POSUtil.removeLongProperty(store.getProperties(), ordertypeId + "." + "ticket.order.extrainfo1");
        POSUtil.removeLongProperty(store.getProperties(), ordertypeId + "." + "ticket.order.extrainfo2");
        POSUtil.removeLongProperty(store.getProperties(), ordertypeId + "." + "ticket.modifier.color");
        POSUtil.removeLongProperty(store.getProperties(), ordertypeId + "." + "ticket.seat.color");
        POSUtil.removeLongProperty(store.getProperties(), ordertypeId + "." + "ticket.instruction.color");
        
        POSUtil.removeLongProperty(store.getProperties(), ordertypeId + "." + "ticket.header.logo.show");
      }
      else {
        POSUtil.removeLongProperty(store.getProperties(), ordertypeId + "." + "kitchen.header");
        POSUtil.removeLongProperty(store.getProperties(), ordertypeId + "." + "kitchen.order.info");
        POSUtil.removeLongProperty(store.getProperties(), ordertypeId + "." + "kitchen.footer");
        POSUtil.removeLongProperty(store.getProperties(), ordertypeId + "." + "kitchen.order.extrainfo1");
        POSUtil.removeLongProperty(store.getProperties(), ordertypeId + "." + "kitchen.order.extrainfo2");
        POSUtil.removeLongProperty(store.getProperties(), ordertypeId + "." + "kitchen_ticket.modifier.color");
        POSUtil.removeLongProperty(store.getProperties(), ordertypeId + "." + "kitchen_ticket.seat.color");
        POSUtil.removeLongProperty(store.getProperties(), ordertypeId + "." + "kitchen_ticket.instruction.color");
        
        POSUtil.removeLongProperty(store.getProperties(), ordertypeId + "." + "kitchen.header.logo.show");
      }
      StoreDAO.getInstance().saveOrUpdate(store);
      Application.getInstance().refreshStore();
    } catch (Exception e) {
      PosLog.error(ReceiptConfigurationView.class, e.getMessage(), e);
    }
  }
  
  private void initReceiptPrintPreview()
  {
    List<Ticket> ticketsToShow = new ArrayList();
    try {
      view = new OrderInfoView(ticketsToShow);
      view.setPreferredSize(PosUIManager.getSize(500, 0));
      view.setBorder(new EmptyBorder(8, 0, 5, 0));
    }
    catch (Exception localException) {}
    add(view, "East");
    view.add(createReceiptViewButtonActionPanel(), "South");
    
    JPanel panel = new JPanel(new MigLayout("wrap 1,ins 0 3 0 3", "sg,fill", ""));
    ActionListener listener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        POSToggleButton source = (POSToggleButton)e.getSource();
        OrderType clientProperty = (OrderType)source.getClientProperty("orderType");
        if (clientProperty != null)
          selectedOrderType = clientProperty;
        String guestCheckType = (String)source.getClientProperty("guestCheck");
        if (StringUtils.isEmpty(guestCheckType)) {
          guestCheckType = null;
          btnGroup2.clearSelection();
        }
        List<Ticket> ticketsToShow = new ArrayList();
        Ticket createNewTicket = createNewTicket(selectedOrderType, guestCheckType);
        ticketsToShow.add(createNewTicket);
        ReceiptConfigurationView.this.doUpdateFields();
        view.setTickets(ticketsToShow);
        try {
          view.getReportPanel().removeAll();
          if (guestCheckType == null) {
            view.createReport();
          } else {
            view.createReport((PosTransaction)createNewTicket.getTransactions().iterator().next());
          }
          view.revalidate();
          view.repaint();
        } catch (Exception e1) {
          e1.printStackTrace();
        }
        
      }
    };
    ButtonGroup btnGroup = new ButtonGroup();
    selectedOrderType = null;
    List<OrderType> orderTypes = OrderTypeDAO.getInstance().findAll();
    String constraints = "w " + PosUIManager.getSize(90);
    if ((orderTypes == null) || (orderTypes.isEmpty())) {
      doShowOrdertypeMissMessage();
      return;
    }
    
    for (OrderType orderType : orderTypes) {
      POSToggleButton btnOrderType = new POSToggleButton("<html><center>" + orderType.getName() + "</center></html>");
      btnGroup.add(btnOrderType);
      panel.add(btnOrderType, constraints);
      btnOrderType.putClientProperty("orderType", orderType);
      btnOrderType.addActionListener(listener);
      
      if (selectedOrderType == null) {
        selectedOrderType = orderType;
        btnOrderType.setSelected(true);
        ticketsToShow.add(createNewTicket(orderType, null));
      }
    }
    panel.add(new JSeparator(), "gaptop 20");
    btnGroup2 = new ButtonGroup();
    btnGuestCheck = new POSToggleButton("<html><center>Guest Check</center></html>");
    btnGuestCheck.putClientProperty("guestCheck", "cash");
    btnGuestCheck.addActionListener(listener);
    btnGroup2.add(btnGuestCheck);
    panel.add(btnGuestCheck, constraints);
    
    btnGuestCheckCreditCard = new POSToggleButton("<html><center>Guest Check (Credit Card)</center><html>");
    btnGuestCheckCreditCard.putClientProperty("guestCheck", "creditCard");
    btnGuestCheckCreditCard.addActionListener(listener);
    btnGroup2.add(btnGuestCheckCreditCard);
    panel.add(btnGuestCheckCreditCard, constraints);
    view.add(panel, "East");
    
    doShowPrintPriview(false);
  }
  
  private String getProperty(String orderTypeId, String propertyName) {
    if (StringUtils.isNotEmpty(orderTypeId)) {
      orderTypeId = orderTypeId + ".";
    }
    String property = store.getProperty(orderTypeId + propertyName);
    if (StringUtils.isEmpty(property)) {
      property = store.getProperty(propertyName);
    }
    return property;
  }
  
  private void doUpdateFields() {
    String ticketHeader = "";
    String orderInfo = "";
    String additionalOrderInfo = "";
    String footerInfo = "";
    
    String orderTypeId = "";
    if (selectedOrderType != null) {
      orderTypeId = selectedOrderType.getId();
    }
    if (!kitchenReceipt) {
      ticketHeader = ReceiptUtil.getReceiptSection(store, "ticket.header", orderTypeId);
      orderInfo = ReceiptUtil.getReceiptSection(store, "ticket.order.info", orderTypeId);
      additionalOrderInfo = ReceiptUtil.getReceiptSection(store, "ticket.order.extrainfo1", orderTypeId);
      footerInfo = ReceiptUtil.getReceiptSection(store, "ticket.footer", orderTypeId);
      
      cbModifierColor.setSelectedItem(SimpleColor.getColor(getProperty(orderTypeId, "ticket.modifier.color")));
      cbSeatColor.setSelectedItem(SimpleColor.getColor(getProperty(orderTypeId, "ticket.seat.color")));
      cbCookingInstructionColor.setSelectedItem(SimpleColor.getColor(getProperty(orderTypeId, "ticket.instruction.color")));
      
      boolean isShowTips = store.getProperty(orderTypeId + "." + "receipt.show_tips_suggestion") == null ? false : Boolean.valueOf(store.getProperty(orderTypeId + "." + "receipt.show_tips_suggestion")).booleanValue();
      cbTipsSuggestion.setSelected(isShowTips);
      boolean isShowModifierPrice = store.getProperty("showModifierPrice") == null ? false : Boolean.valueOf(store.getProperty("showModifierPrice")).booleanValue();
      
      chkModifierPrice.setSelected(isShowModifierPrice);
      boolean isShowTipsBlock = store.getProperty(orderTypeId + "." + "receipt.show_tips_block") == null ? false : Boolean.valueOf(store.getProperty(orderTypeId + "." + "receipt.show_tips_block")).booleanValue();
      cbTipsBlock.setSelected(isShowTipsBlock);
      
      boolean isShowTaxBreakdown = store.getProperty(orderTypeId + "." + "receipt.show_tax_breakdown") == null ? false : Boolean.valueOf(store.getProperty(orderTypeId + "." + "receipt.show_tax_breakdown")).booleanValue();
      chkTaxBreakDown.setSelected(isShowTaxBreakdown);
    }
    else {
      ticketHeader = ReceiptUtil.getReceiptSection(store, "kitchen.header", orderTypeId);
      orderInfo = ReceiptUtil.getReceiptSection(store, "kitchen.order.info", orderTypeId);
      additionalOrderInfo = ReceiptUtil.getReceiptSection(store, "kitchen.order.extrainfo1", orderTypeId);
      footerInfo = ReceiptUtil.getReceiptSection(store, "kitchen.footer", orderTypeId);
      
      cbModifierColor.setSelectedItem(SimpleColor.getColor(getProperty(orderTypeId, "kitchen_ticket.modifier.color")));
      cbSeatColor.setSelectedItem(SimpleColor.getColor(getProperty(orderTypeId, "kitchen_ticket.seat.color")));
      cbCookingInstructionColor.setSelectedItem(SimpleColor.getColor(getProperty(orderTypeId, "kitchen_ticket.instruction.color")));
    }
    chkModifierPrice.setVisible(!kitchenReceipt);
    ticketHeader = ticketHeader.replaceAll("<br>", "\n");
    orderInfo = orderInfo.replaceAll("<br>", "\n");
    additionalOrderInfo = additionalOrderInfo.replaceAll("<br>", "\n");
    footerInfo = footerInfo.replaceAll("<br>", "\n");
    
    ticketHeaderTextArea.setText(ticketHeader);
    orderInfoTextPane.setText(orderInfo);
    additionalOrderInfoTextPane.setText(additionalOrderInfo);
    ticketfooterTextArea.setText(footerInfo);
  }
  
  private void initKitchenReceiptPrintPreview() {
    try {
      kitchenReceiptView = new KitchenReceiptView();
      kitchenReceiptView.setBorder(new EmptyBorder(8, 0, 5, 0));
      kitchenReceiptView.setPreferredSize(PosUIManager.getSize(500, 0));
    }
    catch (Exception localException) {}
    add(kitchenReceiptView, "East");
    kitchenReceiptView.add(createReceiptViewButtonActionPanel(), "South");
    
    JPanel panel = new JPanel(new MigLayout("wrap 1,ins 0 3 0 3", "sg,fill", ""));
    ActionListener listener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        POSToggleButton source = (POSToggleButton)e.getSource();
        OrderType clientProperty = (OrderType)source.getClientProperty("orderType");
        if (clientProperty != null)
          selectedOrderType = clientProperty;
        ReceiptConfigurationView.this.doUpdateFields();
        ReceiptConfigurationView.this.doShowKitchenReceiptPrintPriview(true);
      }
      
    };
    ButtonGroup btnGroup = new ButtonGroup();
    selectedOrderType = null;
    List<OrderType> orderTypes = OrderTypeDAO.getInstance().findAll();
    if ((orderTypes == null) || (orderTypes.isEmpty())) {
      doShowOrdertypeMissMessage();
      return;
    }
    for (OrderType orderType : orderTypes) {
      POSToggleButton btnOrderType = new POSToggleButton("<html><center>" + orderType.getName() + "</center></html>");
      btnGroup.add(btnOrderType);
      String constraints = "w " + PosUIManager.getSize(90);
      panel.add(btnOrderType, constraints);
      btnOrderType.putClientProperty("orderType", orderType);
      btnOrderType.addActionListener(listener);
      
      if (selectedOrderType == null) {
        selectedOrderType = orderType;
        btnOrderType.setSelected(true);
      }
    }
    kitchenReceiptView.add(panel, "East");
    doShowKitchenReceiptPrintPriview(false);
  }
  
  private void doShowOrdertypeMissMessage() {
    POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("ReceiptConfigurationView.66"));
    removeAll();
  }
  
  private Component createReceiptViewButtonActionPanel() {
    JPanel bottomPanel = new JPanel(new MigLayout("ins 5 5 0 5"));
    
    JButton btnPrintPreview = new JButton(Messages.getString("ReceiptConfigurationView.64"));
    btnPrintPreview.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        if (kitchenReceipt) {
          ReceiptConfigurationView.this.doShowKitchenReceiptPrintPriview(true);
        } else
          ReceiptConfigurationView.this.doShowPrintPriview(true);
      }
    });
    JButton btnPrintTestPage = new JButton(Messages.getString("ReceiptConfigurationView.65"));
    btnPrintTestPage.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          save(false);
          if (kitchenReceipt) {
            kitchenReceiptView.print();
          } else
            view.print();
        } catch (Exception e1) {
          BOMessageDialog.showError(e1.getMessage());
        }
      }
    });
    bottomPanel.add(btnPrintPreview, "gapleft 100!,split 2");
    bottomPanel.add(btnPrintTestPage);
    return bottomPanel;
  }
  
  private void doShowPrintPriview(boolean save) {
    try {
      if (save) {
        save(true);
      }
      if ((btnGuestCheckCreditCard.isSelected()) || (btnGuestCheck.isSelected())) {
        String guestCheckType = null;
        if (btnGuestCheck.isSelected()) {
          guestCheckType = (String)btnGuestCheck.getClientProperty("guestCheck");
        }
        else if (btnGuestCheckCreditCard.isSelected()) {
          guestCheckType = (String)btnGuestCheckCreditCard.getClientProperty("guestCheck");
        }
        
        List<Ticket> ticketsToShow = new ArrayList();
        Ticket createNewTicket = createNewTicket(selectedOrderType, guestCheckType);
        ticketsToShow.add(createNewTicket);
        doUpdateFields();
        view.setTickets(ticketsToShow);
        try {
          view.getReportPanel().removeAll();
          if (guestCheckType == null) {
            view.createReport();
          } else
            view.createReport((PosTransaction)createNewTicket.getTransactions().iterator().next());
        } catch (Exception e1) {
          e1.printStackTrace();
        }
      }
      else
      {
        List<Ticket> ticketsToShow = new ArrayList();
        Ticket createNewTicket = createNewTicket(selectedOrderType, null);
        ticketsToShow.add(createNewTicket);
        view.setTickets(ticketsToShow);
        try {
          view.getReportPanel().removeAll();
          view.createReport();
        } catch (Exception e1) {
          e1.printStackTrace();
        }
      }
      view.revalidate();
      view.repaint();
    } catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage(), e);
    }
  }
  
  private void doShowKitchenReceiptPrintPriview(boolean save) {
    try {
      if (save)
        save(true);
      Ticket createNewTicket = createNewTicket(selectedOrderType, null);
      try {
        kitchenReceiptView.getReportPanel().removeAll();
        kitchenReceiptView.createReport(createNewTicket);
        kitchenReceiptView.revalidate();
        kitchenReceiptView.repaint();
      }
      catch (Exception localException1) {}
    } catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage(), e);
    }
  }
  
  public Ticket createNewTicket(OrderType ticketType, String guestCheckType) {
    int numberOfGuests = 4;
    
    Date deliveryDate = null;
    boolean customerWillPickUp = false;
    String shippingAddress = null;
    String extraDeliveryInfo = null;
    String phoneExtension = null;
    String extraManagerInstruction = null;
    Double deliveryChargeAmount = Double.valueOf(0.0D);
    
    Customer customer = new Customer();
    customer.setFirstName("Firstname");
    customer.setLastName("Lastname");
    customer.setMobileNo("+02............");
    customer.setZipCode("565656");
    customer.setMemberId("121");
    customer.setSignatureImageId("sig111");
    if (ticketType.isDelivery().booleanValue()) {
      deliveryDate = new Date();
      shippingAddress = "Customer address.....";
      extraDeliveryInfo = Messages.getString("ReceiptConfigurationView.70");
      phoneExtension = "Phone extension..";
      extraManagerInstruction = "Manager instructions..";
      customerWillPickUp = false;
      deliveryChargeAmount = Double.valueOf(0.0D);
    }
    Application application = Application.getInstance();
    
    Ticket ticket = new Ticket();
    ticket.setTaxIncluded(Boolean.valueOf(Application.getInstance().isPriceIncludesTax()));
    ticket.setOrderType(ticketType);
    ticket.setNumberOfGuests(Integer.valueOf(numberOfGuests));
    
    if (customerWillPickUp) {
      ticket.setCustomerWillPickup(Boolean.valueOf(true));
    }
    else {
      ticket.setDeliveryDate(deliveryDate);
      ticket.setDeliveryAddress(shippingAddress);
      ticket.setExtraDeliveryInfo(extraDeliveryInfo);
      ticket.addProperty("PHONE_EXTENSION", phoneExtension);
      ticket.addProperty("MANAGER_INSTRUCTION", extraManagerInstruction);
      ticket.setDeliveryCharge(deliveryChargeAmount);
      ticket.setCustomerWillPickup(Boolean.valueOf(false));
    }
    
    if (customer != null) {
      ticket.setCustomer(customer);
    }
    Terminal terminal = application.getTerminal();
    ticket.setTerminal(terminal);
    
    Department department = terminal.getDepartment();
    if (department != null) {
      ticket.setDepartmentId(department.getId());
    }
    SalesArea salesArea = terminal.getSalesArea();
    if (salesArea != null) {
      ticket.setSalesAreaId(salesArea.getId());
    }
    User currentUser = Application.getCurrentUser();
    ticket.setOwner(currentUser);
    ticket.setShift(ShiftUtil.getCurrentShift());
    
    if (selectedOrderType.isShowTableSelection().booleanValue()) {
      List<Integer> tables = new ArrayList();
      tables.add(Integer.valueOf(1));
      tables.add(Integer.valueOf(2));
      ticket.setTableNumbers(tables);
    }
    
    Calendar currentTime = DateUtil.getServerTimeCalendar();
    ticket.setCreateDate(currentTime.getTime());
    ticket.setCreationHour(Integer.valueOf(currentTime.get(11)));
    
    ticket.setId(NumericGlobalIdGenerator.generateGlobalId());
    ticket.setTokenNo(Integer.valueOf(1));
    
    TicketItemTax ticketItemTax1 = new TicketItemTax();
    ticketItemTax1.setId(GlobalIdGenerator.generateGlobalId());
    ticketItemTax1.setName("US");
    ticketItemTax1.setRate(Double.valueOf(6.0D));
    
    TicketItemTax ticketItemTax2 = new TicketItemTax();
    ticketItemTax2.setId(GlobalIdGenerator.generateGlobalId());
    ticketItemTax2.setName("BD");
    ticketItemTax2.setRate(Double.valueOf(15.0D));
    
    for (int i = 0; i < 5; i++) {
      ModifiableTicketItem item = new ModifiableTicketItem();
      item.setId(NumericGlobalIdGenerator.generateGlobalId());
      item.setMenuItemId("0");
      item.setName("Item " + (i + 1));
      item.setQuantity(Double.valueOf(i + 1.0D));
      item.setUnitPrice(Double.valueOf(2.5D));
      item.setShouldPrintToKitchen(Boolean.valueOf(true));
      item.setTaxes(Arrays.asList(new TicketItemTax[] { ticketItemTax1, ticketItemTax2 }));
      ticket.addToticketItems(item);
      
      if (i == 1) {
        for (int j = 0; j <= 1; j++) {
          TicketItemModifier newModifier = new TicketItemModifier();
          newModifier.setId(NumericGlobalIdGenerator.generateGlobalId());
          newModifier.setItemId("0");
          newModifier.setGroupId("0");
          newModifier.setItemQuantity(Double.valueOf(2.0D));
          newModifier.setName("Modifier " + (j + 1));
          newModifier.setUnitPrice(Double.valueOf(1.5D));
          newModifier.setShouldPrintToKitchen(Boolean.valueOf(true));
          newModifier.setTicketItem(item);
          item.addToticketItemModifiers(newModifier);
        }
      }
      else if (i == 2) {
        TicketItemCookingInstruction cookingInstruction = new TicketItemCookingInstruction();
        cookingInstruction.setDescription("Fry (Cooking Ins.)");
        item.addCookingInstruction(cookingInstruction);
      }
      if (i == 2) {
        TicketItem seatTicketItem = new TicketItem();
        TicketItemSeat seatItem = new TicketItemSeat();
        seatTicketItem.setName("Seat** 1");
        seatTicketItem.setShouldPrintToKitchen(Boolean.valueOf(true));
        seatTicketItem.setTreatAsSeat(Boolean.valueOf(true));
        seatTicketItem.setSeatNumber(Integer.valueOf(1));
        seatTicketItem.setPrintedToKitchen(Boolean.valueOf(false));
        seatTicketItem.setShouldPrintToKitchen(Boolean.valueOf(true));
        seatTicketItem.setSeat(seatItem);
        seatItem.setSeatNumber(Integer.valueOf(1));
        ticket.addToticketItems(seatTicketItem);
      }
      else if (i == 3) {
        TicketItem seatTicketItem = new TicketItem();
        TicketItemSeat seatItem = new TicketItemSeat();
        seatTicketItem.setName("Seat** Shared");
        seatTicketItem.setShouldPrintToKitchen(Boolean.valueOf(true));
        seatTicketItem.setTreatAsSeat(Boolean.valueOf(true));
        seatTicketItem.setSeatNumber(null);
        seatTicketItem.setPrintedToKitchen(Boolean.valueOf(false));
        seatTicketItem.setShouldPrintToKitchen(Boolean.valueOf(true));
        seatTicketItem.setSeat(seatItem);
        seatItem.setSeatNumber(Integer.valueOf(1));
        ticket.addToticketItems(seatTicketItem);
      }
    }
    ticket.calculatePrice();
    
    PosTransaction transaction = null;
    PaymentType paymentType = null;
    if ((guestCheckType != null) && (guestCheckType.equals("creditCard"))) {
      paymentType = PaymentType.CREDIT_VISA;
      transaction = paymentType.createTransaction();
      transaction.setCaptured(Boolean.valueOf(false));
      transaction.setCardMerchantGateway("Payment gateway name..");
      transaction.setCardReader(CardReader.MANUAL.name());
      transaction.setCardHolderName("Card holder name");
      transaction.setCardNumber("4111111111111111");
      transaction.setCardExpMonth("12");
      transaction.setCardExpYear("2020");
      transaction.setCardType(paymentType.getDisplayString());
      transaction.setCardAuthCode("00000");
      transaction.setTransactionType(TransactionType.CREDIT.name());
      
      ticket.setCashier(currentUser);
    }
    else {
      paymentType = PaymentType.CASH;
      transaction = paymentType.createTransaction();
    }
    transaction.setTenderAmount(ticket.getDueAmount());
    transaction.setAmount(ticket.getDueAmount());
    transaction.setTicket(ticket);
    ticket.addTotransactions(transaction);
    
    ticket.setVoided(Boolean.valueOf(false));
    ticket.setTerminal(terminal);
    ticket.setPaidAmount(Double.valueOf(ticket.getPaidAmount().doubleValue() + transaction.getAmount().doubleValue()));
    

    if (ticket.isSourceOnline()) {
      ticket.setDueAmount(Double.valueOf(ticket.getTotalAmountWithTips().doubleValue() - ticket.getPaidAmount().doubleValue()));
    }
    else {
      ticket.calculatePrice();
    }
    
    Date currentDate = new Date();
    if (ticket.getDueAmount().doubleValue() == 0.0D) {
      ticket.setPaid(Boolean.valueOf(true));
    }
    else {
      ticket.setPaid(Boolean.valueOf(false));
      ticket.setClosed(Boolean.valueOf(false));
    }
    transaction.setTerminal(terminal);
    transaction.setUser(currentUser);
    transaction.setTransactionTime(currentDate);
    return ticket;
  }
  
  private void insertReceiptParam() {
    Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
    JTextComponent textComponent = null;
    if (!(focusOwner instanceof JTextComponent)) {
      return;
    }
    textComponent = (JTextComponent)focusOwner;
    ReceiptParam receiptParam = (ReceiptParam)listParams.getSelectedValue();
    if (receiptParam == null) {
      return;
    }
    String text = "<" + receiptParam.getParamName() + ">" + "$" + receiptParam.getParamName() + "</" + receiptParam.getParamName() + ">";
    textComponent.replaceSelection(text);
  }
  
  public class KitchenReceiptView extends JPanel
  {
    private JPanel reportPanel;
    private Ticket ticket;
    
    public KitchenReceiptView() {
      createUI();
    }
    
    public void createUI() {
      reportPanel = new JPanel(new MigLayout("wrap 1, ax 50%", "", ""));
      PosScrollPane scrollPane = new PosScrollPane(reportPanel);
      scrollPane.getVerticalScrollBar().setUnitIncrement(20);
      setLayout(new BorderLayout());
      add(scrollPane);
    }
    
    public void createReport(Ticket ticket) throws Exception {
      this.ticket = ticket;
      JasperPrint jasperPrint = ReceiptPrintService.getKitchenJasperPrint(ticket, true, true);
      TicketReceiptView receiptView = new TicketReceiptView(jasperPrint);
      reportPanel.add(receiptView.getReportPanel());
    }
    
    public void print() throws Exception {
      ReceiptPrintService.printToKitchen(ticket, false, false);
    }
    
    public JPanel getReportPanel() {
      return reportPanel;
    }
  }
  
  static enum SimpleColor {
    black(Color.black),  red(Color.red);
    
    private SimpleColor(Color color) { this.color = color; }
    
    public static SimpleColor getColor(String value)
    {
      Color color = getAwtColor(value);
      if (color != null) {
        for (SimpleColor simpleColor : values()) {
          if (color.getRGB() == color.getRGB()) {
            return simpleColor;
          }
        }
      }
      return black;
    }
    
    public static Color getAwtColor(String propertyValue) {
      try {
        return Color.decode(propertyValue);
      }
      catch (Exception localException) {}
      return Color.black;
    }
    
    static String[] listModel() {
      List<SimpleColor> values = Arrays.asList(values());
      String s = values.toString().replaceAll(" ", "");
      return s.substring(1, s.length() - 1).split(",");
    }
    
    final Color color;
  }
  
  public void refresh() {
    Application.getInstance().refreshStore();
    store = Application.getInstance().getStore();
  }
}
