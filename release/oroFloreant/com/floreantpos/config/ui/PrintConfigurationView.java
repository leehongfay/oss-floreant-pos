package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.PosPrinters;
import com.floreantpos.model.Store;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.dao.StoreDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.ext.PaperSize;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;











































public class PrintConfigurationView
  extends ConfigurationView
{
  private JComboBox cbReceiptPrinterName;
  private JComboBox cbReportPrinterName;
  private JComboBox<PaperSize> cbReportPageSize;
  public static final String Ticket_Header = "ticket.header";
  public static final String Ticket_Footer = "ticket.footer";
  private PosPrinters printers;
  private Store store;
  private Terminal terminal;
  
  public PrintConfigurationView()
  {
    this(Application.getInstance().getTerminal());
  }
  
  public PrintConfigurationView(Terminal terminal)
  {
    printers = PosPrinters.load(terminal);
    this.terminal = terminal;
    initComponents();
  }
  
  public String getName()
  {
    return POSConstants.CONFIG_TAB_PRINT;
  }
  
  public void initialize() throws Exception
  {
    PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
    
    cbReportPrinterName.setModel(new DefaultComboBoxModel(printServices));
    cbReceiptPrinterName.setModel(new DefaultComboBoxModel(printServices));
    
    PrintServiceComboRenderer comboRenderer = new PrintServiceComboRenderer(null);
    cbReportPrinterName.setRenderer(comboRenderer);
    cbReceiptPrinterName.setRenderer(comboRenderer);
    
    setSelectedPrinter(cbReportPrinterName, printers.getReportPrinter());
    setSelectedPrinter(cbReceiptPrinterName, printers.getReceiptPrinter());
    
    Terminal currentTerminal = DataProvider.get().getCurrentTerminal();
    String property = currentTerminal.getProperty("report.paper_size");
    if (StringUtils.isNotEmpty(property)) {
      PaperSize paperSize = PaperSize.valueOf(property);
      cbReportPageSize.setSelectedItem(paperSize);
    }
    
    store = Application.getInstance().getStore();
    
    setInitialized(true);
    
    if ((printServices == null) || (printServices.length == 0)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), Messages.getString("PrintConfigurationView.0"));
    }
  }
  







  private void setSelectedPrinter(JComboBox whichPrinter, String printerName)
  {
    int printerCount = whichPrinter.getItemCount();
    for (int i = 0; i < printerCount; i++) {
      PrintService printService = (PrintService)whichPrinter.getItemAt(i);
      if (printService.getName().equals(printerName)) {
        whichPrinter.setSelectedIndex(i);
        return;
      }
    }
  }
  
  public boolean save() throws Exception
  {
    PrintService printService = (PrintService)cbReportPrinterName.getSelectedItem();
    printers.setReportPrinter(printService == null ? null : printService.getName());
    
    printService = (PrintService)cbReceiptPrinterName.getSelectedItem();
    printers.setReceiptPrinter(printService == null ? null : printService.getName());
    
    PaperSize selectedPaperSize = (PaperSize)cbReportPageSize.getSelectedItem();
    
    Terminal currentTerminal = DataProvider.get().getCurrentTerminal();
    currentTerminal.addProperty("report.paper_size", selectedPaperSize.name());
    TerminalDAO.getInstance().update(currentTerminal);
    
    StoreDAO.getInstance().saveOrUpdate(store);
    Application.getInstance().refreshStore();
    return true;
  }
  






  private void initComponents()
  {
    setLayout(new BorderLayout());
    JPanel contentPanel = new JPanel();
    contentPanel.setLayout(new MigLayout("", "[][grow,fill]", ""));
    



    cbReportPrinterName = new JComboBox();
    
    JLabel jLabel1 = new JLabel();
    

    jLabel1.setText(Messages.getString("PrintConfigurationView.8"));
    cbReceiptPrinterName = new JComboBox();
    



    MultiPrinterPane multiPrinterPane = new MultiPrinterPane(Messages.getString("PrintConfigurationView.1"), printers.getKitchenPrinters(), terminal);
    contentPanel.add(multiPrinterPane, "cell 0 1 2 1,growx,h 200!");
    
    PrinterGroupView printerGroupView = new PrinterGroupView(Messages.getString("PrintConfigurationView.13"));
    printerGroupView.setPreferredSize(new Dimension(0, 400));
    contentPanel.add(printerGroupView, "cell 0 2 2 1,growx,h 200!,wrap");
    
    JLabel lblTest = new JLabel();
    lblTest.setText("Report Paper Size:");
    contentPanel.add(lblTest, "cell 0 3, growx");
    
    cbReportPageSize = new JComboBox(PaperSize.values());
    contentPanel.add(cbReportPageSize, "cell 1 3, w 150! ,wrap");
    
    JScrollPane scrollPane = new JScrollPane(contentPanel);
    scrollPane.setBorder(null);
    add(scrollPane);
  }
  
  private class PrintServiceComboRenderer extends DefaultListCellRenderer {
    private PrintServiceComboRenderer() {}
    
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
      JLabel listCellRendererComponent = (JLabel)super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      PrintService printService = (PrintService)value;
      
      if (printService != null) {
        listCellRendererComponent.setText(printService.getName());
      }
      
      return listCellRendererComponent;
    }
  }
}
