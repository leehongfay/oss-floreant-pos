package com.floreantpos.config.ui;

import com.floreantpos.Database;
import com.floreantpos.Messages;
import com.floreantpos.PosLog;
import com.floreantpos.config.AppConfig;
import com.floreantpos.main.Application;
import com.floreantpos.main.Main;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.swing.POSPasswordField;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.DatabaseConnectionException;
import com.floreantpos.util.DatabaseUtil;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;





















public class DatabaseConfigurationDialog
  extends POSDialog
  implements ActionListener
{
  private static final String CREATE_DATABASE = "CD";
  private static final String UPDATE_DATABASE = "UD";
  private static final String SAVE = "SAVE";
  private static final String CANCEL = "cancel";
  private static final String TEST = "test";
  private POSTextField tfServerAddress;
  private POSTextField tfServerPort;
  private POSTextField tfDatabaseName;
  private POSTextField tfUserName;
  private POSPasswordField tfPassword;
  private PosButton btnTestConnection;
  private PosButton btnCreateDb;
  private PosButton btnUpdateDb;
  private PosButton btnExit;
  private PosButton btnSave;
  private JComboBox databaseCombo;
  private TitlePanel titlePanel;
  private JLabel lblServerAddress;
  private JLabel lblServerPort;
  private JLabel lblDbName;
  private JLabel lblUserName;
  private JLabel lblDbPassword;
  private boolean connectionSuccess;
  
  public DatabaseConfigurationDialog()
    throws HeadlessException
  {
    setFieldValues();
    addUIListeners();
  }
  
  protected void initUI() {
    getContentPane().setLayout(new MigLayout("fill", "[][fill, grow]", ""));
    
    titlePanel = new TitlePanel();
    tfServerAddress = new POSTextField();
    tfServerPort = new POSTextField();
    tfDatabaseName = new POSTextField();
    tfUserName = new POSTextField();
    tfPassword = new POSPasswordField();
    databaseCombo = new JComboBox(Database.values());
    
    String databaseProviderName = AppConfig.getDatabaseProviderName();
    if (StringUtils.isNotEmpty(databaseProviderName)) {
      Database database = Database.getByProviderName(databaseProviderName);
      if (database != null) {
        databaseCombo.setSelectedItem(database);
      }
    }
    
    getContentPane().add(titlePanel, "span, grow, wrap");
    
    getContentPane().add(new JLabel(Messages.getString("DatabaseConfigurationDialog.8")));
    getContentPane().add(databaseCombo, "grow, wrap");
    lblServerAddress = new JLabel(Messages.getString("DatabaseConfigurationDialog.10") + ":");
    getContentPane().add(lblServerAddress);
    getContentPane().add(tfServerAddress, "grow, wrap");
    lblServerPort = new JLabel(Messages.getString("DatabaseConfigurationDialog.13") + ":");
    getContentPane().add(lblServerPort);
    getContentPane().add(tfServerPort, "grow, wrap");
    lblDbName = new JLabel(Messages.getString("DatabaseConfigurationDialog.16") + ":");
    getContentPane().add(lblDbName);
    getContentPane().add(tfDatabaseName, "grow, wrap");
    lblUserName = new JLabel(Messages.getString("DatabaseConfigurationDialog.19") + ":");
    getContentPane().add(lblUserName);
    getContentPane().add(tfUserName, "grow, wrap");
    lblDbPassword = new JLabel(Messages.getString("DatabaseConfigurationDialog.22") + ":");
    getContentPane().add(lblDbPassword);
    getContentPane().add(tfPassword, "grow, wrap");
    getContentPane().add(new JSeparator(), "span, grow, gaptop 10");
    
    btnTestConnection = new PosButton(Messages.getString("DatabaseConfigurationDialog.26").toUpperCase());
    btnTestConnection.setActionCommand("test");
    btnSave = new PosButton(Messages.getString("DatabaseConfigurationDialog.27").toUpperCase());
    btnSave.setActionCommand("SAVE");
    btnExit = new PosButton(Messages.getString("DatabaseConfigurationDialog.28").toUpperCase());
    btnExit.setActionCommand("cancel");
    
    JPanel buttonPanel = new JPanel(new MigLayout("inset 0, fill", "grow", ""));
    
    btnCreateDb = new PosButton(Messages.getString("DatabaseConfigurationDialog.29").toUpperCase());
    btnCreateDb.setActionCommand("CD");
    
    btnUpdateDb = new PosButton(Messages.getString("UPDATE_DATABASE").toUpperCase());
    btnUpdateDb.setActionCommand("UD");
    
    buttonPanel.add(btnUpdateDb);
    buttonPanel.add(btnCreateDb);
    buttonPanel.add(btnTestConnection);
    buttonPanel.add(btnSave);
    buttonPanel.add(btnExit);
    
    getContentPane().add(buttonPanel, "span, grow");
  }
  
  private void addUIListeners() {
    btnTestConnection.addActionListener(this);
    btnCreateDb.addActionListener(this);
    btnSave.addActionListener(this);
    btnExit.addActionListener(this);
    btnUpdateDb.addActionListener(this);
    
    databaseCombo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Database selectedDb = (Database)databaseCombo.getSelectedItem();
        
        if (selectedDb == Database.DERBY_SINGLE) {
          DatabaseConfigurationDialog.this.setFieldsVisible(false);
          return;
        }
        
        DatabaseConfigurationDialog.this.setFieldsVisible(true);
        
        String databasePort = AppConfig.getDatabasePort();
        if (StringUtils.isEmpty(databasePort)) {
          databasePort = selectedDb.getDefaultPort();
        }
        
        tfServerPort.setText(databasePort);
      }
    });
  }
  
  private void setFieldValues() {
    Database selectedDb = (Database)databaseCombo.getSelectedItem();
    if (selectedDb == null) {
      selectedDb = Database.DERBY_SINGLE;
    }
    
    String databaseURL = AppConfig.getDatabaseHost();
    tfServerAddress.setText(databaseURL);
    
    String databasePort = AppConfig.getDatabasePort();
    if (StringUtils.isEmpty(databasePort)) {
      databasePort = selectedDb.getDefaultPort();
    }
    
    tfServerPort.setText(databasePort);
    tfDatabaseName.setText(AppConfig.getDatabaseName());
    tfUserName.setText(AppConfig.getDatabaseUser());
    tfPassword.setText(AppConfig.getDatabasePassword());
    
    if (selectedDb == Database.DERBY_SINGLE) {
      setFieldsVisible(false);
    }
    else {
      setFieldsVisible(true);
    }
  }
  
  public void actionPerformed(ActionEvent e) {
    try {
      String command = e.getActionCommand();
      if ("cancel".equalsIgnoreCase(command)) {
        dispose();
        return;
      }
      
      Database selectedDb = (Database)databaseCombo.getSelectedItem();
      
      String providerName = selectedDb.getProviderName();
      String databaseURL = tfServerAddress.getText();
      String databasePort = tfServerPort.getText();
      String databaseName = tfDatabaseName.getText();
      String user = tfUserName.getText();
      String pass = new String(tfPassword.getPassword());
      
      String connectionString = selectedDb.getConnectString(databaseURL, databasePort, databaseName);
      String hibernateDialect = selectedDb.getHibernateDialect();
      String driverClass = selectedDb.getHibernateConnectionDriverClass();
      
      setCursor(Cursor.getPredefinedCursor(3));
      
      Application.getInstance().setSystemInitialized(false);
      saveConfig(selectedDb, providerName, databaseURL, databasePort, databaseName, user, pass, connectionString, hibernateDialect);
      
      if ("test".equalsIgnoreCase(command)) {
        try {
          DatabaseUtil.checkConnection(connectionString, hibernateDialect, driverClass, user, pass);
        } catch (DatabaseConnectionException e1) {
          JOptionPane.showMessageDialog(this, Messages.getString("DatabaseConfigurationDialog.32"));
          return;
        }
        
        connectionSuccess = true;
        JOptionPane.showMessageDialog(this, Messages.getString("DatabaseConfigurationDialog.31"));
      }
      else if ("UD".equals(command)) {
        int i = JOptionPane.showConfirmDialog(this, 
          Messages.getString("DatabaseConfigurationDialog.0"), Messages.getString("DatabaseConfigurationDialog.1"), 0);
        if (i != 0) {
          return;
        }
        


        setCursor(Cursor.getPredefinedCursor(3));
        
        boolean databaseUpdated = DatabaseUtil.updateDatabase(connectionString, hibernateDialect, driverClass, user, pass);
        if (databaseUpdated) {
          connectionSuccess = true;
          JOptionPane.showMessageDialog(this, Messages.getString("DatabaseConfigurationDialog.2"));
        }
        else {
          JOptionPane.showMessageDialog(this, Messages.getString("DatabaseConfigurationDialog.3"));
        }
      }
      else if ("CD".equals(command))
      {
        int i = JOptionPane.showConfirmDialog(this, 
          Messages.getString("DatabaseConfigurationDialog.33"), Messages.getString("DatabaseConfigurationDialog.34"), 0);
        if (i != 0) {
          return;
        }
        
        i = JOptionPane.showConfirmDialog(this, 
          Messages.getString("DatabaseConfigurationDialog.4"), Messages.getString("DatabaseConfigurationDialog.5"), 0);
        boolean generateSampleData = false;
        if (i == 0) {
          generateSampleData = true;
        }
        setCursor(Cursor.getPredefinedCursor(3));
        
        String createDbConnectString = selectedDb.getCreateDbConnectString(databaseURL, databasePort, databaseName);
        
        boolean databaseCreated = DatabaseUtil.createDatabase(createDbConnectString, hibernateDialect, driverClass, user, pass, generateSampleData);
        
        if (databaseCreated) {
          JOptionPane.showMessageDialog(this, Messages.getString("DatabaseConfigurationDialog.6") + 
            Messages.getString("DatabaseConfigurationDialog.7"));
          
          Main.restart();
          connectionSuccess = true;
        }
        else {
          JOptionPane.showMessageDialog(this, Messages.getString("DatabaseConfigurationDialog.36"));
        }
      }
      else if ("SAVE".equalsIgnoreCase(command)) {
        if (connectionSuccess) {
          Application.getInstance().initializeSystem();
        }
        dispose();
      }
    } catch (Exception e2) {
      PosLog.error(getClass(), e2);
      POSMessageDialog.showMessage(this, e2.getMessage());
    } finally {
      setCursor(Cursor.getDefaultCursor());
    }
  }
  
  private void isAuthorizedToPerformDbChange() {
    DatabaseUtil.initialize();
    
    UserDAO.getInstance().findAll();
    
    String password = JOptionPane.showInputDialog(Messages.getString("DatabaseConfigurationDialog.9"));
    User user2 = UserDAO.getInstance().findUserBySecretKey(password);
    if ((user2 == null) || (!user2.isAdministrator())) {
      POSMessageDialog.showError(this, Messages.getString("DatabaseConfigurationDialog.11"));
      return;
    }
  }
  
  private void saveConfig(Database selectedDb, String providerName, String databaseURL, String databasePort, String databaseName, String user, String pass, String connectionString, String hibernateDialect)
  {
    AppConfig.setDatabaseProviderName(providerName);
    AppConfig.setConnectString(connectionString);
    AppConfig.setDatabaseHost(databaseURL);
    AppConfig.setDatabasePort(databasePort);
    AppConfig.setDatabaseName(databaseName);
    AppConfig.setDatabaseUser(user);
    AppConfig.setDatabasePassword(pass);
  }
  
  public void setTitle(String title) {
    super.setTitle(Messages.getString("DatabaseConfigurationDialog.37"));
    
    titlePanel.setTitle(title);
  }
  
  private void setFieldsVisible(boolean visible) {
    lblServerAddress.setVisible(visible);
    tfServerAddress.setVisible(visible);
    
    lblServerPort.setVisible(visible);
    tfServerPort.setVisible(visible);
    
    lblDbName.setVisible(visible);
    tfDatabaseName.setVisible(visible);
    
    lblUserName.setVisible(visible);
    tfUserName.setVisible(visible);
    
    lblDbPassword.setVisible(visible);
    tfPassword.setVisible(visible);
  }
  
  public static DatabaseConfigurationDialog show(Frame parent) {
    DatabaseConfigurationDialog dialog = new DatabaseConfigurationDialog();
    dialog.setTitle(Messages.getString("DatabaseConfigurationDialog.38"));
    dialog.pack();
    dialog.open();
    
    return dialog;
  }
}
