package com.floreantpos.config.ui;

import com.floreantpos.Database;
import com.floreantpos.Messages;
import com.floreantpos.config.AppConfig;
import com.floreantpos.main.Application;
import com.floreantpos.main.Main;
import com.floreantpos.swing.POSPasswordField;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.DatabaseConnectionException;
import com.floreantpos.util.DatabaseUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;



















public class DatabaseConfigurationView
  extends ConfigurationView
  implements ActionListener
{
  private static final String CONFIGURE_DB = "CD";
  private static final String SAVE = "SAVE";
  private static final String CANCEL = "cancel";
  private static final String TEST = "test";
  private POSTextField tfServerAddress;
  private POSTextField tfServerPort;
  private POSTextField tfDatabaseName;
  private POSTextField tfUserName;
  private POSPasswordField tfPassword;
  private JButton btnTestConnection;
  private JButton btnCreateDb;
  private JButton btnSave;
  private JComboBox databaseCombo;
  private JLabel lblServerAddress;
  private JLabel lblServerPort;
  private JLabel lblDbName;
  private JLabel lblUserName;
  private JLabel lblDbPassword;
  
  public DatabaseConfigurationView()
    throws HeadlessException
  {
    initUI();
    addUIListeners();
  }
  
  protected void initUI() {
    setLayout(new BorderLayout());
    
    JPanel contentPanel = new JPanel();
    contentPanel.setLayout(new MigLayout("fill", "[][grow,fill]", "[][][][][][][][grow,fill]"));
    tfServerAddress = new POSTextField();
    tfServerPort = new POSTextField();
    tfDatabaseName = new POSTextField();
    tfUserName = new POSTextField();
    tfPassword = new POSPasswordField();
    databaseCombo = new JComboBox(Database.values());
    
    String databaseProviderName = AppConfig.getDatabaseProviderName();
    if (StringUtils.isNotEmpty(databaseProviderName)) {
      databaseCombo.setSelectedItem(Database.getByProviderName(databaseProviderName));
    }
    
    contentPanel.add(new JLabel(Messages.getString("DatabaseConfigurationDialog.8")));
    contentPanel.add(databaseCombo, "grow, wrap");
    lblServerAddress = new JLabel(Messages.getString("DatabaseConfigurationDialog.10") + ":");
    contentPanel.add(lblServerAddress);
    contentPanel.add(tfServerAddress, "grow, wrap");
    lblServerPort = new JLabel(Messages.getString("DatabaseConfigurationDialog.13") + ":");
    contentPanel.add(lblServerPort);
    contentPanel.add(tfServerPort, "grow, wrap");
    lblDbName = new JLabel(Messages.getString("DatabaseConfigurationDialog.16") + ":");
    contentPanel.add(lblDbName);
    contentPanel.add(tfDatabaseName, "grow, wrap");
    lblUserName = new JLabel(Messages.getString("DatabaseConfigurationDialog.19") + ":");
    contentPanel.add(lblUserName);
    contentPanel.add(tfUserName, "grow, wrap");
    lblDbPassword = new JLabel(Messages.getString("DatabaseConfigurationDialog.22") + ":");
    contentPanel.add(lblDbPassword);
    contentPanel.add(tfPassword, "grow, wrap");
    contentPanel.add(new JSeparator(), "span, grow, gaptop 10");
    
    btnTestConnection = new JButton(Messages.getString("DatabaseConfigurationDialog.26"));
    btnTestConnection.setActionCommand("test");
    btnSave = new JButton(Messages.getString("DatabaseConfigurationDialog.27"));
    btnSave.setActionCommand("SAVE");
    
    JPanel buttonPanel = new JPanel(new FlowLayout(2));
    btnCreateDb = new JButton(Messages.getString("DatabaseConfigurationDialog.29"));
    btnCreateDb.setActionCommand("CD");
    buttonPanel.add(btnCreateDb);
    buttonPanel.add(btnTestConnection);
    buttonPanel.add(btnSave);
    
    contentPanel.add(buttonPanel, "span, grow");
    
    JScrollPane scrollPane = new JScrollPane(contentPanel);
    scrollPane.setBorder(null);
    add(scrollPane);
  }
  
  private void addUIListeners() {
    btnTestConnection.addActionListener(this);
    btnCreateDb.addActionListener(this);
    btnSave.addActionListener(this);
    
    databaseCombo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Database selectedDb = (Database)databaseCombo.getSelectedItem();
        
        if (selectedDb == Database.DERBY_SINGLE) {
          DatabaseConfigurationView.this.setFieldsVisible(false);
          return;
        }
        
        DatabaseConfigurationView.this.setFieldsVisible(true);
        
        String databasePort = AppConfig.getDatabasePort();
        if (StringUtils.isEmpty(databasePort)) {
          databasePort = selectedDb.getDefaultPort();
        }
        
        tfServerPort.setText(databasePort);
      }
    });
  }
  
  public void actionPerformed(ActionEvent e) {
    try {
      String command = e.getActionCommand();
      
      Database selectedDb = (Database)databaseCombo.getSelectedItem();
      
      String providerName = selectedDb.getProviderName();
      String databaseURL = tfServerAddress.getText();
      String databasePort = tfServerPort.getText();
      String databaseName = tfDatabaseName.getText();
      String user = tfUserName.getText();
      String pass = new String(tfPassword.getPassword());
      
      String connectionString = selectedDb.getConnectString(databaseURL, databasePort, databaseName);
      String hibernateDialect = selectedDb.getHibernateDialect();
      String driverClass = selectedDb.getHibernateConnectionDriverClass();
      
      if ("test".equalsIgnoreCase(command)) {
        Application.getInstance().setSystemInitialized(false);
        saveConfig(selectedDb, providerName, databaseURL, databasePort, databaseName, user, pass, connectionString, hibernateDialect);
        try
        {
          DatabaseUtil.checkConnection(connectionString, hibernateDialect, driverClass, user, pass);
        } catch (DatabaseConnectionException e1) {
          JOptionPane.showMessageDialog(POSUtil.getBackOfficeWindow(), Messages.getString("DatabaseConfigurationDialog.32"));
          return;
        }
        
        JOptionPane.showMessageDialog(POSUtil.getBackOfficeWindow(), Messages.getString("DatabaseConfigurationDialog.31"));
      }
      else if ("CD".equals(command)) {
        Application.getInstance().setSystemInitialized(false);
        
        int i = JOptionPane.showConfirmDialog(POSUtil.getBackOfficeWindow(), 
          Messages.getString("DatabaseConfigurationDialog.33"), Messages.getString("DatabaseConfigurationDialog.34"), 0);
        if (i != 0) {
          return;
        }
        
        i = JOptionPane.showConfirmDialog(POSUtil.getBackOfficeWindow(), 
          Messages.getString("DatabaseConfigurationView.3"), Messages.getString("DatabaseConfigurationView.4"), 0);
        boolean generateSampleData = false;
        if (i == 0) {
          generateSampleData = true;
        }
        saveConfig(selectedDb, providerName, databaseURL, databasePort, databaseName, user, pass, connectionString, hibernateDialect);
        
        String connectionString2 = selectedDb.getCreateDbConnectString(databaseURL, databasePort, databaseName);
        
        setCursor(Cursor.getPredefinedCursor(3));
        boolean createDatabase = DatabaseUtil.createDatabase(connectionString2, hibernateDialect, driverClass, user, pass, generateSampleData);
        setCursor(Cursor.getDefaultCursor());
        
        if (createDatabase)
        {
          JOptionPane.showMessageDialog(POSUtil.getBackOfficeWindow(), "Database created. Default password is 1111.\n\nThe system will now restart.");
          
          Main.restart();
        }
        else {
          JOptionPane.showMessageDialog(POSUtil.getBackOfficeWindow(), Messages.getString("DatabaseConfigurationDialog.36"));
        }
      }
      else if ("SAVE".equalsIgnoreCase(command)) {
        Application.getInstance().setSystemInitialized(false);
        saveConfig(selectedDb, providerName, databaseURL, databasePort, databaseName, user, pass, connectionString, hibernateDialect);
      }
      else if (!"cancel".equalsIgnoreCase(command)) {}
      



      return;
    }
    catch (Exception e2)
    {
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), e2.getMessage());
    }
  }
  
  private void saveConfig(Database selectedDb, String providerName, String databaseURL, String databasePort, String databaseName, String user, String pass, String connectionString, String hibernateDialect)
  {
    AppConfig.setDatabaseProviderName(providerName);
    AppConfig.setConnectString(connectionString);
    AppConfig.setDatabaseHost(databaseURL);
    AppConfig.setDatabasePort(databasePort);
    AppConfig.setDatabaseName(databaseName);
    AppConfig.setDatabaseUser(user);
    AppConfig.setDatabasePassword(pass);
  }
  
  private void setFieldsVisible(boolean visible) {
    lblServerAddress.setVisible(visible);
    tfServerAddress.setVisible(visible);
    
    lblServerPort.setVisible(visible);
    tfServerPort.setVisible(visible);
    
    lblDbName.setVisible(visible);
    tfDatabaseName.setVisible(visible);
    
    lblUserName.setVisible(visible);
    tfUserName.setVisible(visible);
    
    lblDbPassword.setVisible(visible);
    tfPassword.setVisible(visible);
  }
  
  public boolean save() throws Exception
  {
    return true;
  }
  
  public void initialize() throws Exception
  {
    Database selectedDb = (Database)databaseCombo.getSelectedItem();
    
    String databaseURL = AppConfig.getDatabaseHost();
    tfServerAddress.setText(databaseURL);
    
    String databasePort = AppConfig.getDatabasePort();
    if (StringUtils.isEmpty(databasePort)) {
      databasePort = selectedDb.getDefaultPort();
    }
    
    tfServerPort.setText(databasePort);
    tfDatabaseName.setText(AppConfig.getDatabaseName());
    tfUserName.setText(AppConfig.getDatabaseUser());
    tfPassword.setText(AppConfig.getDatabasePassword());
    
    if (selectedDb == Database.DERBY_SINGLE) {
      setFieldsVisible(false);
    }
    else {
      setFieldsVisible(true);
    }
    
    setInitialized(true);
  }
  
  public String getName()
  {
    return Messages.getString("DatabaseConfigurationView.5");
  }
}
