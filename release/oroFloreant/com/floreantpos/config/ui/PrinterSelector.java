package com.floreantpos.config.ui;

import com.floreantpos.POSConstants;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JComboBox;
























public class PrinterSelector
  extends ConfigurationView
{
  private JComboBox cbPrinters;
  
  public PrinterSelector()
  {
    initComponents();
  }
  







  private void initComponents()
  {
    cbPrinters = new JComboBox();
    
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(layout
      .createParallelGroup(GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
      .addContainerGap()
      .addComponent(cbPrinters, 0, 376, 32767)
      .addContainerGap()));
    
    layout.setVerticalGroup(layout
      .createParallelGroup(GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
      .addContainerGap()
      .addComponent(cbPrinters, -2, -1, -2)
      .addContainerGap(-1, 32767)));
  }
  







  public String getName()
  {
    return POSConstants.SELECT_PRINTER;
  }
  
  public void initialize() throws Exception
  {
    PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
    cbPrinters.setModel(new DefaultComboBoxModel(printServices));
  }
  
  public boolean save() throws Exception
  {
    return false;
  }
}
