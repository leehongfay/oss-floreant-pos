package com.floreantpos.config.ui;

import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.PriceTable;
import com.floreantpos.model.Store;
import com.floreantpos.model.dao.PriceTableDAO;
import com.floreantpos.model.dao.StoreDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;




public class LabelConfigurationView
  extends TransparentPanel
{
  private Store store;
  private JCheckBox chkBarcode;
  private JCheckBox chkMemberPrice;
  private JCheckBox chkRetailPrice;
  private JComboBox<PriceTable> cbPriceTable;
  private JLabel lblPriceTable;
  private PosButton btnSave;
  
  public LabelConfigurationView()
  {
    createUI();
    initData();
    updateView();
    initHandler();
  }
  
  private void initData()
  {
    List<PriceTable> priceTableList = new ArrayList();
    priceTableList.add(null);
    priceTableList.addAll(PriceTableDAO.getInstance().findAll());
    cbPriceTable.setModel(new ComboBoxModel(priceTableList));
  }
  
  private void initHandler() {
    chkRetailPrice.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e) {
        JCheckBox chkBox = (JCheckBox)e.getSource();
        cbPriceTable.setVisible(chkBox.isSelected());
        lblPriceTable.setVisible(chkBox.isSelected());
      }
      

    });
    btnSave.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        save();
      }
    });
  }
  
  private void createUI() {
    setLayout(new BorderLayout());
    JPanel itemInfoPanel = new JPanel(new MigLayout("hidemode 3", "[][grow][]", ""));
    JScrollPane scrollPane = new JScrollPane(itemInfoPanel, 20, 31);
    
    TitledBorder border = BorderFactory.createTitledBorder(null, "Label Configuration", 2, 0);
    itemInfoPanel.setBorder(border);
    
    chkBarcode = new JCheckBox("Print barcode");
    chkMemberPrice = new JCheckBox("Print member price");
    chkRetailPrice = new JCheckBox("Print retail price");
    cbPriceTable = new JComboBox();
    lblPriceTable = new JLabel("Select retail price: ");
    
    itemInfoPanel.add(chkBarcode, "wrap");
    itemInfoPanel.add(chkMemberPrice, "wrap");
    itemInfoPanel.add(chkRetailPrice, "wrap");
    itemInfoPanel.add(lblPriceTable, "gapleft 20");
    itemInfoPanel.add(cbPriceTable, "w 100!");
    cbPriceTable.setVisible(false);
    lblPriceTable.setVisible(false);
    
    scrollPane.setBorder(null);
    
    JPanel buttonPanel = new JPanel(new MigLayout("center"));
    btnSave = new PosButton("Save");
    buttonPanel.add(btnSave);
    add(scrollPane, "Center");
    add(buttonPanel, "South");
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      StoreDAO.getInstance().saveOrUpdate(store);
      POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), "Label configuration successfully updated.");
      return true;
    } catch (Exception e) {
      PosLog.error(LabelConfigurationView.class, e.getMessage(), e);
      POSMessageDialog.showError(e.getMessage());
    }
    
    return false;
  }
  
  public void updateView() {
    try {
      store = Application.getInstance().getStore();
      boolean isPrintBarcode = store.getProperty("label.barcode") == null ? true : Boolean.valueOf(store
        .getProperty("label.barcode")).booleanValue();
      boolean isPrintMemberPrice = store.getProperty("label.member.price") == null ? true : Boolean.valueOf(store
        .getProperty("label.member.price")).booleanValue();
      boolean isPrintRetailPrice = store.getProperty("label.isPrint.retail.price") == null ? true : Boolean.valueOf(store
        .getProperty("label.isPrint.retail.price")).booleanValue();
      String retailPriceId = store.getProperty("label.retail.price");
      
      chkBarcode.setSelected(isPrintBarcode);
      chkMemberPrice.setSelected(isPrintMemberPrice);
      chkRetailPrice.setSelected(isPrintRetailPrice);
      lblPriceTable.setVisible(isPrintRetailPrice);
      cbPriceTable.setVisible(isPrintRetailPrice);
      if (StringUtils.isNotEmpty(retailPriceId)) {
        PriceTable priceTable = PriceTableDAO.getInstance().get(retailPriceId);
        cbPriceTable.setSelectedItem(priceTable);
      }
    }
    catch (Exception e) {
      PosLog.error(LabelConfigurationView.class, e);
    }
  }
  
  public boolean updateModel() {
    try {
      boolean isPrintBarcode = chkBarcode.isSelected();
      boolean isPrintMemberPrice = chkMemberPrice.isSelected();
      boolean isPrintRetailPrice = chkRetailPrice.isSelected();
      PriceTable priceTable = (PriceTable)cbPriceTable.getSelectedItem();
      
      store.addProperty("label.barcode", String.valueOf(isPrintBarcode));
      store.addProperty("label.member.price", String.valueOf(isPrintMemberPrice));
      store.addProperty("label.isPrint.retail.price", String.valueOf(isPrintRetailPrice));
      store.addProperty("label.retail.price", priceTable.getId());
    } catch (Exception e) {
      PosLog.error(LabelConfigurationView.class, e.getMessage(), e);
    }
    return true;
  }
  
  public String getName()
  {
    return "Label";
  }
}
