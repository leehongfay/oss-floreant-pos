package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.config.AppConfig;
import com.floreantpos.config.TerminalConfig;
import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;

public class KitchenDisplayConfigurationView
  extends ConfigurationView
{
  private JTextField txtYellowTime;
  private JTextField txtRedTime;
  private JCheckBox chkEnableKds;
  private JCheckBox chkShowKdsOnLoginScreen;
  
  public KitchenDisplayConfigurationView()
  {
    initComponents();
    chkShowKdsOnLoginScreen.setEnabled(false);
    txtYellowTime.setEnabled(false);
    txtRedTime.setEnabled(false);
  }
  
  public boolean save()
    throws Exception
  {
    AppConfig.put("YellowTimeOut", txtYellowTime.getText());
    AppConfig.put("RedTimeOut", txtRedTime.getText());
    
    TerminalConfig.setShowKDSOnLogInScreen(chkShowKdsOnLoginScreen.isSelected());
    TerminalConfig.setKDSenabled(chkEnableKds.isSelected());
    return true;
  }
  
  public void initialize()
    throws Exception
  {
    String yellowTimeOut = AppConfig.getString("YellowTimeOut");
    String redTimeOut = AppConfig.getString("RedTimeOut");
    
    chkEnableKds.setSelected(TerminalConfig.isKDSenabled());
    chkShowKdsOnLoginScreen.setSelected(TerminalConfig.isShowKDSOnLogInScreen());
    if (yellowTimeOut != null) {
      txtYellowTime.setText(yellowTimeOut);
    }
    if (redTimeOut != null) {
      txtRedTime.setText(redTimeOut);
    }
    setInitialized(true);
  }
  
  public String getName()
  {
    return Messages.getString("KitchenDisplayConfigurationView.0");
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    txtYellowTime = new JTextField(5);
    txtRedTime = new JTextField(5);
    
    JPanel contentPanel = new JPanel(new BorderLayout());
    
    JPanel chkBoxPanel = new JPanel(new BorderLayout());
    chkEnableKds = new JCheckBox("Enable KDS");
    chkShowKdsOnLoginScreen = new JCheckBox("Show KDS button on login screen");
    chkEnableKds.addItemListener(new ItemListener()
    {

      public void itemStateChanged(ItemEvent e)
      {
        if (e.getStateChange() == 1) {
          chkShowKdsOnLoginScreen.setEnabled(true);
          txtYellowTime.setEnabled(true);
          txtRedTime.setEnabled(true);
          txtYellowTime.setText("90");
          txtRedTime.setText("120");
        }
        else {
          chkShowKdsOnLoginScreen.setEnabled(false);
          txtYellowTime.setEnabled(false);
          txtRedTime.setEnabled(false);
        }
        
      }
    });
    chkBoxPanel.add(chkEnableKds, "North");
    chkBoxPanel.add(chkShowKdsOnLoginScreen, "South");
    contentPanel.add(chkBoxPanel, "North");
    JPanel footerPanel = new JPanel(new MigLayout());
    
    footerPanel.setBorder(BorderFactory.createTitledBorder(Messages.getString("KitchenDisplayConfigurationView.1")));
    
    JLabel lblYellowTime = new JLabel(Messages.getString("PrintConfigurationView.7"));
    JLabel lblRedTime = new JLabel(Messages.getString("PrintConfigurationView.9"));
    
    footerPanel.add(lblYellowTime, "grow");
    footerPanel.add(txtYellowTime, "grow");
    footerPanel.add(new JLabel(Messages.getString("PrintConfigurationView.14")), "grow, wrap");
    footerPanel.add(lblRedTime, "grow");
    footerPanel.add(txtRedTime, "grow");
    footerPanel.add(new JLabel(Messages.getString("PrintConfigurationView.18")), "grow");
    contentPanel.add(footerPanel, "Center");
    add(contentPanel);
  }
}
