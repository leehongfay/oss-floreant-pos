package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.config.AppConfig;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.POSTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;



















public class TicketImportConfigurationView
  extends ConfigurationView
{
  public static final String CONFIG_TAB_TAX = "Ticket Import";
  private POSTextField tfURL = new POSTextField(30);
  private POSTextField tfSecretKey = new POSTextField(10);
  private IntegerTextField tfPollInterval = new IntegerTextField(6);
  
  public TicketImportConfigurationView() {
    JPanel contentPanel = new JPanel();
    contentPanel.setLayout(new MigLayout("", "[]", "[]"));
    
    contentPanel.add(new JLabel(Messages.getString("TicketImportConfigurationView.4")));
    contentPanel.add(tfURL, "wrap");
    
    contentPanel.add(new JLabel(Messages.getString("TicketImportConfigurationView.6")));
    contentPanel.add(tfPollInterval, "wrap");
    
    add(contentPanel);
  }
  
  public boolean save() throws Exception
  {
    if (!isInitialized()) {
      return true;
    }
    
    AppConfig.put("ticket_import_url", tfURL.getText());
    AppConfig.put("ticket_import_secret_key", tfSecretKey.getText());
    AppConfig.putInt("ticket_import_poll_interval", tfPollInterval.getInteger());
    
    return true;
  }
  
  public void initialize() throws Exception
  {
    tfURL.setText(AppConfig.getString("ticket_import_url", "http://cloud.floreantpos.org/webstore/"));
    tfSecretKey.setText(AppConfig.getString("ticket_import_secret_key", "12345"));
    tfPollInterval.setText(AppConfig.getString("ticket_import_poll_interval", "60"));
    
    setInitialized(true);
  }
  
  public String getName()
  {
    return "Ticket Import";
  }
}
