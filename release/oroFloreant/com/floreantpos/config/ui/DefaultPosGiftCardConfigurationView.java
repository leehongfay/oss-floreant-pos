package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.config.GiftCardConfig;
import com.floreantpos.model.CardReader;
import com.floreantpos.swing.POSTextField;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import net.miginfocom.swing.MigLayout;




















public class DefaultPosGiftCardConfigurationView
  extends ConfigurationView
{
  private POSTextField tfMerchantAccount;
  private JComboBox cbCardReader;
  private JPasswordField tfMerchantPass;
  private JCheckBox chckbxAllowMagneticSwipe;
  private JCheckBox chckbxAllowCardManual;
  
  public DefaultPosGiftCardConfigurationView()
  {
    setLayout(new MigLayout("", "[][grow]", ""));
    
    JLabel lblMagneticCardReader = new JLabel(Messages.getString("CardConfigurationView.9"));
    add(lblMagneticCardReader, "cell 0 3,alignx leading");
    
    cbCardReader = new JComboBox();
    cbCardReader.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        updateCheckBoxes();
      }
    });
    add(cbCardReader, "cell 1 3,growx");
    
    JLabel lblMerchantAccount = new JLabel(Messages.getString("CardConfigurationView.19"));
    add(lblMerchantAccount, "cell 0 5,alignx leading");
    
    tfMerchantAccount = new POSTextField();
    add(tfMerchantAccount, "cell 1 5,growx");
    
    JLabel lblSecretCode = new JLabel(Messages.getString("CardConfigurationView.22"));
    add(lblSecretCode, "cell 0 6,alignx leading");
    
    cbCardReader.setModel(new DefaultComboBoxModel(CardReader.values()));
    
    tfMerchantPass = new JPasswordField();
    add(tfMerchantPass, "cell 1 6,growx");
    
    chckbxAllowMagneticSwipe = new JCheckBox(Messages.getString("CardConfigurationView.3"));
    chckbxAllowMagneticSwipe.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        updateCardList();
      }
    });
    add(chckbxAllowMagneticSwipe, "skip 1, newline");
    
    chckbxAllowCardManual = new JCheckBox(Messages.getString("CardConfigurationView.5"));
    chckbxAllowCardManual.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        updateCardList();
      }
    });
    add(chckbxAllowCardManual, "skip 1, newline");
  }
  


























  private void openBrowser(String link)
    throws Exception
  {
    URI uri = new URI(link);
    if (Desktop.isDesktopSupported()) {
      Desktop.getDesktop().browse(uri);
    }
  }
  
  public void initialize() throws Exception
  {
    chckbxAllowMagneticSwipe.setSelected(GiftCardConfig.isSwipeCardSupported());
    chckbxAllowCardManual.setSelected(GiftCardConfig.isManualEntrySupported());
    

    CardReader card = GiftCardConfig.getCardReader();
    cbCardReader.setSelectedItem(card);
    
    String merchantAccount = GiftCardConfig.getMerchantAccount();
    if (merchantAccount != null) {
      tfMerchantAccount.setText(merchantAccount);
    }
    
    String merchantPass = GiftCardConfig.getMerchantPass();
    if (merchantPass != null) {
      tfMerchantPass.setText(merchantPass);
    }
    


    updateCardList();
  }
  
  public void setMerchantDefaultValue(String accountNo, String pass) {
    tfMerchantAccount.setText(accountNo);
    tfMerchantPass.setText(pass);
  }
  
  protected void updateCheckBoxes() {
    CardReader selectedItem = (CardReader)cbCardReader.getSelectedItem();
    if (selectedItem == CardReader.SWIPE) {
      chckbxAllowMagneticSwipe.setSelected(true);
    }
    else if (selectedItem == CardReader.MANUAL) {
      chckbxAllowCardManual.setSelected(true);
    }
  }
  


  private DefaultComboBoxModel<CardReader> createComboBoxModel(Vector items)
  {
    DefaultComboBoxModel model = new DefaultComboBoxModel();
    for (Object object : items) {
      model.addElement(object);
    }
    return model;
  }
  
  protected void updateCardList() {
    boolean swipeSupported = chckbxAllowMagneticSwipe.isSelected();
    boolean manualSupported = chckbxAllowCardManual.isSelected();
    

    CardReader currentReader = (CardReader)cbCardReader.getSelectedItem();
    Vector<CardReader> readers = new Vector(3);
    
    if (swipeSupported) {
      readers.add(CardReader.SWIPE);
    }
    
    if (manualSupported) {
      readers.add(CardReader.MANUAL);
    }
    




    cbCardReader.setModel(createComboBoxModel(readers));
    if (readers.contains(currentReader)) {
      cbCardReader.setSelectedItem(currentReader);
    }
    
    if ((!swipeSupported) && (!manualSupported)) {
      cbCardReader.setEnabled(false);
      
      tfMerchantAccount.setEnabled(false);
      tfMerchantPass.setEnabled(false);
    }
    else
    {
      cbCardReader.setEnabled(true);
      
      tfMerchantAccount.setEnabled(true);
      tfMerchantPass.setEnabled(true);
    }
    

    if ((swipeSupported) || (manualSupported))
    {
      tfMerchantAccount.setEnabled(true);
      tfMerchantPass.setEnabled(true);

    }
    else
    {
      tfMerchantAccount.setEnabled(false);
      tfMerchantPass.setEnabled(false);
    }
  }
  
  public boolean save()
    throws Exception
  {
    GiftCardConfig.setSwipeCardSupported(chckbxAllowMagneticSwipe.isSelected());
    GiftCardConfig.setManualEntrySupported(chckbxAllowCardManual.isSelected());
    

    CardReader cardReader = (CardReader)cbCardReader.getSelectedItem();
    GiftCardConfig.setCardReader(cardReader);
    
    GiftCardConfig.setMerchantAccount(tfMerchantAccount.getText());
    GiftCardConfig.setMerchantPass(new String(tfMerchantPass.getPassword()));
    


    return true;
  }
  
  public String getName()
  {
    return "";
  }
}
