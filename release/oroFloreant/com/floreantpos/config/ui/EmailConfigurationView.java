package com.floreantpos.config.ui;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.mailservices.MailService;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.AESencrp;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;




public class EmailConfigurationView
  extends ConfigurationView
{
  private JTextField tfSMTP = new JTextField(25);
  private IntegerTextField tfSMTPPort = new IntegerTextField(8);
  private JTextField tfSenderEmail = new JTextField(25);
  private JPasswordField tfPassword = new JPasswordField(25);
  private static String encryptedPassword;
  private Terminal terminal;
  
  public EmailConfigurationView(Terminal terminal)
  {
    this.terminal = terminal;
    initializeComponents();
  }
  
  private void initializeComponents() {
    setLayout(new BorderLayout());
    
    JLabel lblSMTP_Host = new JLabel(Messages.getString("TerminalConfigurationView.26"));
    JLabel lblSMTP_Port = new JLabel(Messages.getString("EmailConfigurationView.0"));
    JLabel lblSender = new JLabel(Messages.getString("TerminalConfigurationView.28"));
    JLabel lblPassword = new JLabel(Messages.getString("TerminalConfigurationView.48"));
    
    JPanel emailSetupJPanel = new JPanel(new MigLayout("wrap 2", "[][grow]", ""));
    emailSetupJPanel.setBorder(BorderFactory.createTitledBorder(Messages.getString("TerminalConfigurationView.44")));
    emailSetupJPanel.add(lblSMTP_Host);
    emailSetupJPanel.add(tfSMTP);
    emailSetupJPanel.add(lblSMTP_Port);
    emailSetupJPanel.add(tfSMTPPort);
    emailSetupJPanel.add(lblSender);
    emailSetupJPanel.add(tfSenderEmail);
    emailSetupJPanel.add(lblPassword);
    emailSetupJPanel.add(tfPassword);
    
    JButton btnEmailTest = new JButton(Messages.getString("EmailConfigurationView.4"));
    btnEmailTest.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        EmailConfigurationView.this.doSendTestEmail();
      }
    });
    emailSetupJPanel.add(btnEmailTest, "skip 1");
    
    JScrollPane scrollPane = new JScrollPane(emailSetupJPanel, 20, 31);
    scrollPane.setBorder(null);
    add(scrollPane, "Center");
  }
  
  private void doSendTestEmail() {
    try {
      String smtpHost = tfSMTP.getText();
      Integer smtpPort = Integer.valueOf(tfSMTPPort.getInteger());
      String senderEmail = tfSenderEmail.getText();
      String password = new String(tfPassword.getPassword());
      MailService.sendMail(smtpHost.trim(), smtpPort, senderEmail.trim(), password.trim(), senderEmail.trim(), 
        Messages.getString("EmailConfigurationView.6"), 
        Messages.getString("EmailConfigurationView.7"), null, null);
      POSMessageDialog.showMessage(this, Messages.getString("EmailConfigurationView.8"));
    } catch (Exception e2) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), Messages.getString("EmailConfigurationView.9"), e2);
    }
  }
  
  public boolean save() {
    try {
      if (!updateModel()) {
        return false;
      }
      
      TerminalDAO termDAO = new TerminalDAO();
      termDAO.saveOrUpdate(terminal);
      
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(e.getMessage());
    }
    
    return false;
  }
  
  public void updateView() {
    try {
      if (terminal == null) {
        return;
      }
      String smtpHost = terminal.getSmtpHost();
      Integer smtpPort = terminal.getSmtpPort();
      
      tfSMTP.setText(StringUtils.isEmpty(smtpHost) ? Messages.getString("EmailConfigurationView.10") : smtpHost);
      tfSMTPPort.setText(String.valueOf(smtpPort.intValue() == 0 ? 465 : smtpPort.intValue()));
      tfSenderEmail.setText(terminal.getSmtpSender());
      encryptedPassword = terminal.getSmtpPassword();
      if (encryptedPassword != null) {
        tfPassword.setText(AESencrp.decrypt(encryptedPassword));
      }
      setInitialized(true);
    } catch (Exception e) {
      PosLog.error(EmailConfigurationView.class, e);
    }
  }
  
  public boolean updateModel() {
    try {
      Terminal term = terminal;
      term.setSmtpHost(tfSMTP.getText());
      term.setSmtpPort(Integer.valueOf(tfSMTPPort.getInteger()));
      term.setSmtpSender(tfSenderEmail.getText());
      term.setSmtpPassword(AESencrp.encrypt(new String(tfPassword.getPassword())));
      return true;
    } catch (Exception e) {
      PosLog.error(EmailConfigurationView.class, e);
    }
    return false;
  }
  
  public void initialize() throws Exception
  {
    updateView();
  }
  

  public String getName()
  {
    return POSConstants.CONFIG_TAB_EMAIL;
  }
}
