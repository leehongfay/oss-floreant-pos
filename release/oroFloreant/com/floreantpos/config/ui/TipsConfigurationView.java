package com.floreantpos.config.ui;

import com.floreantpos.model.Store;
import com.floreantpos.model.TipsReceivedBy;
import java.awt.BorderLayout;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import net.miginfocom.swing.MigLayout;



















public class TipsConfigurationView
  extends ConfigurationView
{
  private Store store;
  private JRadioButton chkDriver;
  private JRadioButton chkServerForDelivery;
  private JRadioButton chkCashierForDelivery;
  private JRadioButton chkServerForNonDelivery;
  private JRadioButton chkCashierForNonDelivery;
  
  public TipsConfigurationView(Store store)
  {
    this.store = store;
    setLayout(new BorderLayout());
    JPanel contentPanel = new JPanel(new MigLayout("", "[][]", ""));
    
    JLabel lblTipsReceivedBy = new JLabel("Tips received by:");
    JLabel lblDrvrSet = new JLabel("When driver is set:");
    JLabel lblDrvrNotSet = new JLabel(" When driver not set:");
    
    ButtonGroup btnGrpDrvrSet = new ButtonGroup();
    chkDriver = new JRadioButton(TipsReceivedBy.Driver.name());
    chkServerForDelivery = new JRadioButton(TipsReceivedBy.Server.name());
    chkCashierForDelivery = new JRadioButton(TipsReceivedBy.Cashier.name());
    
    btnGrpDrvrSet.add(chkDriver);
    btnGrpDrvrSet.add(chkServerForDelivery);
    btnGrpDrvrSet.add(chkCashierForDelivery);
    
    ButtonGroup btnGrpDrvrNotSet = new ButtonGroup();
    chkServerForNonDelivery = new JRadioButton(TipsReceivedBy.Server.name());
    chkCashierForNonDelivery = new JRadioButton(TipsReceivedBy.Cashier.name());
    
    btnGrpDrvrNotSet.add(chkServerForNonDelivery);
    btnGrpDrvrNotSet.add(chkCashierForNonDelivery);
    
    contentPanel.add(lblTipsReceivedBy, "wrap");
    contentPanel.add(lblDrvrSet, "gapleft 20,wrap");
    contentPanel.add(chkDriver, "skip 1, wrap");
    contentPanel.add(chkServerForDelivery, "skip 1, wrap");
    contentPanel.add(chkCashierForDelivery, "skip 1, wrap");
    contentPanel.add(lblDrvrNotSet, "gapleft 20,,wrap");
    contentPanel.add(chkServerForNonDelivery, "skip 1, wrap");
    contentPanel.add(chkCashierForNonDelivery, "skip 1, wrap");
    
    JScrollPane scrollPane = new JScrollPane(contentPanel);
    scrollPane.setBorder(null);
    add(scrollPane);
  }
  
  public boolean save() throws Exception
  {
    if (!isInitialized()) {
      return true;
    }
    String tipsReceiverForDelivery = null;
    if (chkDriver.isSelected()) {
      tipsReceiverForDelivery = TipsReceivedBy.Driver.name();
    }
    else if (chkCashierForDelivery.isSelected()) {
      tipsReceiverForDelivery = TipsReceivedBy.Cashier.name();
    }
    else {
      tipsReceiverForDelivery = TipsReceivedBy.Server.name();
    }
    
    store.addProperty("tips.receiver.delivery", tipsReceiverForDelivery);
    
    String tipsReceiverForNonDelivery = null;
    if (chkCashierForNonDelivery.isSelected()) {
      tipsReceiverForNonDelivery = TipsReceivedBy.Cashier.name();
    }
    else {
      tipsReceiverForNonDelivery = TipsReceivedBy.Server.name();
    }
    
    store.addProperty("tips.receiver.nonDelivery", tipsReceiverForNonDelivery);
    
    return true;
  }
  
  public void initialize() throws Exception
  {
    setInitialized(true);
    TipsReceivedBy tipsReceiverForDelivery = store.getTipsReceivedByForDeliveryOrder();
    if (tipsReceiverForDelivery == TipsReceivedBy.Server) {
      chkServerForDelivery.setSelected(true);
    }
    else if (tipsReceiverForDelivery == TipsReceivedBy.Driver) {
      chkDriver.setSelected(true);
    }
    else if (tipsReceiverForDelivery == TipsReceivedBy.Cashier) {
      chkCashierForDelivery.setSelected(true);
    }
    TipsReceivedBy tipsReceiverForNonDelivery = store.getTipsReceivedByForNonDeliveryOrder();
    if (tipsReceiverForNonDelivery == TipsReceivedBy.Server) {
      chkServerForNonDelivery.setSelected(true);
    }
    else if (tipsReceiverForNonDelivery == TipsReceivedBy.Cashier) {
      chkCashierForNonDelivery.setSelected(true);
    }
  }
  
  public String getName()
  {
    return "Tips";
  }
}
