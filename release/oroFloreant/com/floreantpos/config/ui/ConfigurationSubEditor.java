package com.floreantpos.config.ui;

import javax.swing.JPanel;
















public abstract class ConfigurationSubEditor
  extends JPanel
{
  private boolean initialized = false;
  
  public ConfigurationSubEditor() {}
  
  public abstract boolean save() throws Exception;
  
  public abstract void initialize() throws Exception;
  
  public boolean isInitialized()
  {
    return initialized;
  }
  
  public void setInitialized(boolean initialized) {
    this.initialized = initialized;
  }
}
