package com.floreantpos.config.ui;

public abstract interface UISaveHandler
{
  public abstract boolean canSave();
  
  public abstract void save();
}
