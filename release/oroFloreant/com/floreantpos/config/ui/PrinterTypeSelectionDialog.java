package com.floreantpos.config.ui;

import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import net.miginfocom.swing.MigLayout;































public class PrinterTypeSelectionDialog
  extends POSDialog
  implements ActionListener
{
  private JPanel buttonsPanel;
  private POSToggleButton btnReportPrinter;
  private POSToggleButton btnReceiptPrinter;
  private POSToggleButton btnKitchenPrinter;
  private POSToggleButton btnPackingPrinter;
  private POSToggleButton btnKitchenDisplayPrinter;
  private POSToggleButton btnLabelPrinter;
  private POSToggleButton btnStickerPrinter;
  private int selectedPrinterType;
  
  public PrinterTypeSelectionDialog()
  {
    super(POSUtil.getBackOfficeWindow(), true);
    initializeComponent();
    initializeData();
  }
  
  private void initializeComponent() {
    setTitle("Select Printer Type");
    setLayout(new BorderLayout());
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Select Printer Type");
    add(titlePanel, "North");
    
    JPanel buttonActionPanel = new JPanel(new MigLayout("fill"));
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL.toUpperCase());
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        selectedPrinterType = 0;
        setCanceled(true);
        dispose();
      }
      
    });
    buttonActionPanel.add(btnCancel, "grow, span");
    
    JPanel footerPanel = new JPanel(new BorderLayout());
    footerPanel.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
    footerPanel.add(new JSeparator(), "North");
    footerPanel.add(buttonActionPanel);
    
    add(footerPanel, "South");
    
    buttonsPanel = new JPanel(new MigLayout("fill", "sg, fill", ""));
    
    add(buttonsPanel, "Center");
    
    setSize(500, 280);
  }
  
  private void initializeData() {
    try {
      ButtonGroup group = new ButtonGroup();
      btnReportPrinter = new POSToggleButton(com.floreantpos.model.VirtualPrinter.PRINTER_TYPE_NAMES[0]);
      btnReceiptPrinter = new POSToggleButton(com.floreantpos.model.VirtualPrinter.PRINTER_TYPE_NAMES[1]);
      btnKitchenPrinter = new POSToggleButton(com.floreantpos.model.VirtualPrinter.PRINTER_TYPE_NAMES[2]);
      btnPackingPrinter = new POSToggleButton(com.floreantpos.model.VirtualPrinter.PRINTER_TYPE_NAMES[3]);
      btnKitchenDisplayPrinter = new POSToggleButton(com.floreantpos.model.VirtualPrinter.PRINTER_TYPE_NAMES[4]);
      btnLabelPrinter = new POSToggleButton(com.floreantpos.model.VirtualPrinter.PRINTER_TYPE_NAMES[5]);
      btnStickerPrinter = new POSToggleButton(com.floreantpos.model.VirtualPrinter.PRINTER_TYPE_NAMES[6]);
      
      btnReportPrinter.addActionListener(this);
      btnReceiptPrinter.addActionListener(this);
      btnKitchenPrinter.addActionListener(this);
      btnPackingPrinter.addActionListener(this);
      btnKitchenDisplayPrinter.addActionListener(this);
      btnLabelPrinter.addActionListener(this);
      btnStickerPrinter.addActionListener(this);
      
      buttonsPanel.add(btnReportPrinter, "growy");
      buttonsPanel.add(btnReceiptPrinter, "growy");
      buttonsPanel.add(btnKitchenPrinter, "growy");
      buttonsPanel.add(btnPackingPrinter, "growy");
      buttonsPanel.add(btnKitchenDisplayPrinter, "growy");
      buttonsPanel.add(btnLabelPrinter, "growy");
      buttonsPanel.add(btnStickerPrinter, "growy");
      
      group.add(btnReportPrinter);
      group.add(btnReceiptPrinter);
      group.add(btnKitchenPrinter);
      group.add(btnPackingPrinter);
      group.add(btnKitchenDisplayPrinter);
      group.add(btnLabelPrinter);
      group.add(btnStickerPrinter);
    }
    catch (PosException e) {
      POSMessageDialog.showError(this, e.getLocalizedMessage(), e);
    }
  }
  
  protected void doFinish() {
    setCanceled(false);
    dispose();
  }
  
  public int getSelectedPrinterType() {
    return selectedPrinterType;
  }
  
  public void actionPerformed(ActionEvent e)
  {
    try {
      Object source = e.getSource();
      
      if (source == btnReportPrinter) {
        selectedPrinterType = 0;
      }
      else if (source == btnReceiptPrinter) {
        selectedPrinterType = 1;
      }
      else if (source == btnKitchenPrinter) {
        selectedPrinterType = 2;
      }
      else if (source == btnPackingPrinter) {
        selectedPrinterType = 3;
      }
      else if (source == btnKitchenDisplayPrinter) {
        selectedPrinterType = 4;
      }
      else if (source == btnLabelPrinter) {
        selectedPrinterType = 5;
      }
      else if (source == btnStickerPrinter) {
        selectedPrinterType = 6;
      }
      doFinish();
    } catch (Exception ex) {
      PosLog.error(PrinterTypeSelectionDialog.class, ex.getMessage(), ex);
    }
  }
}
