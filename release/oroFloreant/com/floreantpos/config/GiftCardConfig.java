package com.floreantpos.config;

import com.floreantpos.PosLog;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.FloreantPlugin;
import com.floreantpos.extension.GiftCardPaymentPlugin;
import com.floreantpos.extension.PosGiftCardPlugin;
import com.floreantpos.model.CardReader;
import com.floreantpos.util.AESencrp;
import java.util.List;
import org.apache.commons.lang.StringUtils;
















public class GiftCardConfig
{
  private static final String GIFT_CARD_MERCHANT_PASS = "GiftCardMerchantPass";
  private static final String GIFT_CARD_MERCHANT_ACCOUNT = "GiftCardMerchantAccount";
  private static final String CARD_READER = "CARD_READER";
  
  public GiftCardConfig() {}
  
  public static boolean isSwipeCardSupported()
  {
    return AppConfig.getBoolean("support-swipe-card", true);
  }
  
  public static void setSwipeCardSupported(boolean b) {
    AppConfig.put("support-swipe-card", b);
  }
  
  public static boolean isManualEntrySupported() {
    return AppConfig.getBoolean("support-card-manual-entry", true);
  }
  
  public static void setManualEntrySupported(boolean b) {
    AppConfig.put("support-card-manual-entry", b);
  }
  
  public static boolean isExtTerminalSupported() {
    return AppConfig.getBoolean("support-ext-terminal", true);
  }
  
  public static void setExtTerminalSupported(boolean b) {
    AppConfig.put("support-ext-terminal", b);
  }
  
  public static void setCardReader(CardReader card) {
    if (card == null) {
      AppConfig.put("CARD_READER", "");
      return;
    }
    AppConfig.put("CARD_READER", card.name());
  }
  
  public static CardReader getCardReader() {
    String string = AppConfig.getString("CARD_READER", "SWIPE");
    return CardReader.fromString(string);
  }
  
  public static void setMerchantAccount(String account) {
    AppConfig.put("GiftCardMerchantAccount", account);
  }
  
  public static String getMerchantAccount() {
    return AppConfig.getString("GiftCardMerchantAccount", null);
  }
  
  public static void setMerchantPass(String pass)
  {
    try {
      if (StringUtils.isEmpty(pass)) {
        AppConfig.put("GiftCardMerchantPass", "");
        return;
      }
      
      AppConfig.put("GiftCardMerchantPass", AESencrp.encrypt(pass));
    } catch (Exception e) {
      PosLog.error(GiftCardConfig.class, e);
    }
  }
  
  public static String getMerchantPass() throws Exception {
    String string = AppConfig.getString("GiftCardMerchantPass");
    
    if (StringUtils.isNotEmpty(string)) {
      return AESencrp.decrypt(string);
    }
    
    return string;
  }
  































  public static void setPaymentGateway(GiftCardPaymentPlugin paymentGateway)
  {
    AppConfig.put("payment-gateway-id", paymentGateway.getId());
  }
  
  public static GiftCardPaymentPlugin getPaymentGateway() {
    String gatewayId = AppConfig.getString("payment-gateway-id", PosGiftCardPlugin.ID);
    List<FloreantPlugin> plugins = ExtensionManager.getPlugins(GiftCardPaymentPlugin.class);
    
    for (FloreantPlugin plugin : plugins) {
      if (gatewayId.equals(plugin.getId())) {
        return (GiftCardPaymentPlugin)plugin;
      }
    }
    

    return new PosGiftCardPlugin();
  }
}
