package com.floreantpos.config;

import com.floreantpos.Messages;
import com.floreantpos.model.PaymentStatusFilter;
import com.floreantpos.ui.views.SwitchboardView;
import com.floreantpos.util.PasswordHasher;
import java.awt.Color;
import java.util.Locale;
import java.util.StringTokenizer;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;













































public class TerminalConfig
{
  private static final String USE_SETTLEMENT_PROMPT = "UseSettlementPrompt";
  private static final String SHOW_GUEST_SELECTION = "show_guest_selection";
  private static final String ORDER_TYPE_FILTER = "order_type_filter";
  private static final String PS_FILTER = "ps_filter";
  private static final String SHOW_TABLE_SELECTION = "show_table_selection";
  private static final String REGULAR_MODE = "regular_mode";
  private static final String KITCHEN_MODE = "kitchen_mode";
  private static final String CASHIER_MODE = "cashier_mode";
  private static final String SHOW_DB_CONFIGURATION = "show_db_configuration";
  private static final String UI_DEFAULT_FONT = "ui_default_font";
  private static final String TOUCH_FONT_SIZE = "TOUCH_FONT_SIZE";
  private static final String SCREEN_COMPONENT_SIZE_RATIO = "SCREEN_COMPONENT_SIZE_RATIO";
  private static final String TOUCH_BUTTON_HEIGHT = "TOUCH_BUTTON_HEIGHT";
  private static final String FLOOR_BUTTON_WIDTH = "FLOOR_BUTTON_WIDTH";
  private static final String FLOOR_BUTTON_HEIGHT = "FLOOR_BUTTON_HEIGHT";
  private static final String FLOOR_BUTTON_FONT_SIZE = "FLOOR_BUTTON_FONT_SIZE";
  private static final String ADMIN_PASSWORD = "admin_pass";
  private static final String SHOW_BARCODE_ON_RECEIPT = "show_barcode_on_receipt";
  private static final String DEFAULT_VIEW = "default_view";
  private static final String ACTIVE_CUSTOMER_DISPLAY = "active_customer_display";
  private static final String ACTIVE_SCALE_DISPLAY = "active_scale_display";
  private static final String ACTIVE_CALLER_ID_DEVICE = "active_caller_id_device";
  private static final String CALLER_ID_DEVICE = "caller_id_device";
  private static final String ORDER_VIEW_PAGE_SIZE = "order_view_page_size";
  private static final String SHOW_KDS_ON_LOGIN_SCREEN = "showkdsonloginscreen";
  private static final String KDS_ENABLED = "kdsenabled";
  static final String TERMINAL_ID = "terminal_id";
  static final String KIOSK_MODE = "kiosk_mode";
  static final String SMTP_HOST = "smtp_host";
  static final String SENDER_EMAIL = "sender_email";
  private static final String SENDER_PASSWORD = "sender_password";
  private static boolean multiple_order_supported = true;
  
  private static final String DEFAULT_LOCALE = "defaultLocal";
  
  private static final String PAGINATED_MENU_VIEW = "paginated_menu_view";
  
  private static final String AUTO_PRINT_RECEIPT = "auto_print_receipt";
  
  private static final String CHECK_FOR_UPDATE_ON_START_UP = "check_update_on_startup";
  
  private static final String OWNER_FILTER = "owner_filter";
  
  private static final String KDS_TICKETS_PER_PAGE = "kds.ticket.per_page";
  
  public static final String RED_TIME_OUT = "RedTimeOut";
  
  public static final String YELLOW_TIME_OUT = "YellowTimeOut";
  
  public static final String KDS_BACKGROUND = "kds.background";
  
  public static final String KDS_TEXT_COLOR = "kds.textcolor";
  
  public static final String KDS_SHOW_BORDER = "kds.border";
  public static final String KDS_SHOW_HORIZONTAL_LINE = "kds.sep";
  public static final String KDS_INITIAL_COMPONENT = "kds.initial.component";
  public static final String KDS_INITIAL_BG = "kds.initial.bg";
  public static final String KDS_INITIAL_FG = "kds.initial.fg";
  public static final String KDS_WARNING_COMPONENT = "kds.warning.component";
  public static final String KDS_WARNING_BG = "kds.warning.bg";
  public static final String KDS_WARNING_FG = "kds.warning.fg";
  public static final String KDS_OVER_COMPONENT = "kds.over.component";
  public static final String KDS_OVER_BG = "kds.over.bd";
  public static final String KDS_OVER_FG = "kds.over.fg";
  public static final String HIDE_INLINE_ITEM_BUMP = "kds.hide_item_bump";
  public static final String ORDER_FILTER_PANEL_VISIBLE = "order.filter_panel.visible";
  
  public TerminalConfig() {}
  
  private static PropertiesConfiguration config = AppConfig.getConfig();
  
  public static void setSMTPHost(String SMTP_host) {
    config.setProperty("smtp_host", SMTP_host); }
  
  public static String getSMTPHost()
  {
    return (String)config.getProperty("smtp_host");
  }
  
  public static void setSenderEmail(String sender) {
    config.setProperty("sender_email", sender);
  }
  
  public static void setSenderPassword(String senderPassword) {
    config.setProperty("sender_password", senderPassword);
  }
  
  public static String getSenderPassword() {
    return (String)config.getProperty("sender_password");
  }
  
  public static String getSenderEmail() {
    return (String)config.getProperty("sender_email");
  }
  
  public static int getTerminalId() {
    return config.getInt("terminal_id", -1);
  }
  
  public static void setTerminalId(int id) {
    config.setProperty("terminal_id", Integer.valueOf(id));
  }
  
  public static boolean isShowKDSOnLogInScreen() {
    return config.getBoolean("showkdsonloginscreen", false);
  }
  
  public static void setShowKDSOnLogInScreen(boolean show) {
    config.setProperty("showkdsonloginscreen", Boolean.valueOf(show));
  }
  
  public static boolean isKDSenabled() {
    return config.getBoolean("kdsenabled", true);
  }
  
  public static void setKDSenabled(boolean enabled) {
    config.setProperty("kdsenabled", Boolean.valueOf(enabled));
  }
  
  public static boolean isKioskMode() {
    return config.getBoolean("kiosk_mode", false);
  }
  
  public static void setKioskMode(boolean fullscreen) {
    config.setProperty("kiosk_mode", Boolean.valueOf(fullscreen));
  }
  
  public static String getAdminPassword() {
    return config.getString("admin_pass", PasswordHasher.hashPassword("1111"));
  }
  
  public static void setAdminPassword(String password) {
    config.setProperty("admin_pass", PasswordHasher.hashPassword(password));
  }
  
  public static boolean matchAdminPassword(String password) {
    return getAdminPassword().equals(PasswordHasher.hashPassword(password));
  }
  
  public static void setTouchScreenButtonHeight(int height) {
    config.setProperty("TOUCH_BUTTON_HEIGHT", Integer.valueOf(height));
  }
  
  public static int getTouchScreenButtonHeight() {
    return config.getInt("TOUCH_BUTTON_HEIGHT", 60);
  }
  
  public static void setFloorButtonWidth(int width) {
    config.setProperty("FLOOR_BUTTON_WIDTH", Integer.valueOf(width));
  }
  
  public static int getFloorButtonWidth() {
    return config.getInt("FLOOR_BUTTON_WIDTH", 90);
  }
  
  public static void setFloorButtonHeight(int height) {
    config.setProperty("FLOOR_BUTTON_HEIGHT", Integer.valueOf(height));
  }
  
  public static int getFloorButtonHeight() {
    return config.getInt("FLOOR_BUTTON_HEIGHT", 90);
  }
  
  public static void setFloorButtonFontSize(int size) {
    config.setProperty("FLOOR_BUTTON_FONT_SIZE", Integer.valueOf(size));
  }
  
  public static int getFloorButtonFontSize() {
    return config.getInt("FLOOR_BUTTON_FONT_SIZE", 30);
  }
  
  public static void setMenuItemButtonHeight(int height) {
    config.setProperty("menu_button_height", Integer.valueOf(height));
  }
  
  public static int getMenuItemButtonHeight() {
    return config.getInt("menu_button_height", 120);
  }
  
  public static void setMenuItemButtonWidth(int width) {
    config.setProperty("menu_button_width", Integer.valueOf(width));
  }
  
  public static int getMenuItemButtonWidth() {
    return config.getInt("menu_button_width", 120);
  }
  
  public static void setTouchScreenFontSize(int size) {
    config.setProperty("TOUCH_FONT_SIZE", Integer.valueOf(size));
  }
  
  public static int getTouchScreenFontSize() {
    return config.getInt("TOUCH_FONT_SIZE", 12);
  }
  
  public static void setScreenScaleFactor(double size) {
    config.setProperty("SCREEN_COMPONENT_SIZE_RATIO", Double.valueOf(size));
  }
  
  public static double getScreenScaleFactor() {
    return config.getDouble("SCREEN_COMPONENT_SIZE_RATIO", 1.0D);
  }
  
  public static boolean isOrderFilterPanelVisible() {
    return config.getBoolean("order.filter_panel.visible", true);
  }
  
  public static void setOrderFilterPanelVisible(boolean visible) {
    config.setProperty("order.filter_panel.visible", Boolean.valueOf(visible));
  }
  























  public static String getUiDefaultFont()
  {
    return config.getString("ui_default_font");
  }
  
  public static void setUiDefaultFont(String fontName) {
    config.setProperty("ui_default_font", fontName);
  }
  







  public static void setShowDbConfigureButton(boolean show)
  {
    config.setProperty("show_db_configuration", Boolean.valueOf(show));
  }
  
  public static boolean isShowDbConfigureButton() {
    return config.getBoolean("show_db_configuration", true);
  }
  
  public static void setShowBarcodeOnReceipt(boolean show) {
    config.setProperty("show_barcode_on_receipt", Boolean.valueOf(show));
  }
  
  public static boolean isShowBarcodeOnReceipt() {
    return config.getBoolean("show_barcode_on_receipt", false);
  }
  
  public static void setEnabledCallerIdDevice(boolean show) {
    config.setProperty("active_caller_id_device", Boolean.valueOf(show));
  }
  
  public static boolean isEanbledCallerIdDevice() {
    return config.getBoolean("active_caller_id_device", false);
  }
  















  public static boolean isMultipleOrderSupported()
  {
    return multiple_order_supported;
  }
  
  public static void setCustomerDisplay(boolean show) {
    config.setProperty("active_customer_display", Boolean.valueOf(show));
  }
  
  public static boolean isActiveCustomerDisplay() {
    return config.getBoolean("active_customer_display", false);
  }
  
  public static void setScaleDisplay(boolean show) {
    config.setProperty("active_scale_display", Boolean.valueOf(show));
  }
  
  public static boolean isActiveScaleDisplay() {
    return config.getBoolean("active_scale_display", false);
  }
  
  public static boolean isCashierMode() {
    return false;
  }
  
  public static void setCashierMode(boolean cashierMode) {
    config.setProperty("cashier_mode", Boolean.valueOf(cashierMode));
  }
  
  public static boolean isRegularMode() {
    return config.getBoolean("regular_mode", false);
  }
  
  public static void setRegularMode(boolean regularMode) {
    config.setProperty("regular_mode", Boolean.valueOf(regularMode));
  }
  
  public static boolean isKitchenMode() {
    return config.getBoolean("kitchen_mode", false);
  }
  
  public static void setKitchenMode(boolean kitchenMode) {
    config.setProperty("kitchen_mode", Boolean.valueOf(kitchenMode));
  }
  







  public static String getOrderTypeFilter()
  {
    return config.getString("order_type_filter", "ALL");
  }
  
  public static void setOrderTypeFilter(String filter) {
    config.setProperty("order_type_filter", filter);
  }
  
  public static String getCallerIdDevice() {
    return config.getString("caller_id_device", Messages.getString("TerminalConfig.0"));
  }
  
  public static void setCallerIdDevice(String device) {
    config.setProperty("caller_id_device", device);
  }
  
  public static PaymentStatusFilter getPaymentStatusFilter() {
    return PaymentStatusFilter.fromString(config.getString("ps_filter"));
  }
  
  public static void setPaymentStatusFilter(String filter) {
    config.setProperty("ps_filter", filter);
  }
  
  public static void setShouldShowTableSelection(boolean showTableSelection) {
    config.setProperty("show_table_selection", Boolean.valueOf(showTableSelection));
  }
  
  public static boolean isShouldShowTableSelection() {
    return config.getBoolean("show_table_selection", Boolean.TRUE).booleanValue();
  }
  
  public static void setShouldShowGuestSelection(boolean showGuestSelection) {
    config.setProperty("show_guest_selection", Boolean.valueOf(showGuestSelection));
  }
  
  public static boolean isShouldShowGuestSelection() {
    return config.getBoolean("show_guest_selection", Boolean.TRUE).booleanValue();
  }
  
  public static void setUseSettlementPrompt(boolean settlementPrompt) {
    config.setProperty("UseSettlementPrompt", Boolean.valueOf(settlementPrompt));
  }
  
  public static boolean isUseSettlementPrompt() {
    return config.getBoolean("UseSettlementPrompt", Boolean.FALSE).booleanValue();
  }
  
  public static void setMiscItemDefaultTaxId(String id) {
    config.setProperty("mistitemdefaulttaxid", id);
  }
  
  public static String getMiscItemDefaultTaxId() {
    return config.getString("mistitemdefaulttaxid", "-1");
  }
  
  public static void setDrawerPortName(String drawerPortName) {
    config.setProperty("drawerPortName", drawerPortName);
  }
  
  public static String getDrawerPortName() {
    return config.getString("drawerPortName", "COM1");
  }
  
  public static void setCustomerDisplayPort(String customerDisplayPort) {
    config.setProperty("customerDisplayPort", customerDisplayPort);
  }
  
  public static String getCustomerDisplayPort() {
    return config.getString("customerDisplayPort", "COM1");
  }
  
  public static void setCustomerDisplayMessage(String customerDisplayMessage) {
    config.setProperty("customerDisplayMessage", customerDisplayMessage);
  }
  
  public static String getCustomerDisplayMessage() {
    return config.getString("customerDisplayMessage", "12345678912345678912");
  }
  
  public static String getScaleActivationValue() {
    return config.getString("wd", "");
  }
  
  public static void setScalePort(String scalePort) {
    config.setProperty("scaleDisplayPort", scalePort);
  }
  
  public static String getScalePort() {
    return config.getString("scalePort", "COM7");
  }
  
  public static void setScaleDisplayMessage(String scaleDisplayMessage) {
    config.setProperty("scaleDisplayMessage", scaleDisplayMessage);
  }
  
  public static String getScaleDisplayMessage() {
    return config.getString("scaleDisplayMessage", "1234");
  }
  
  public static void setDrawerControlCodes(String controlCode) {
    config.setProperty("controlCode", controlCode);
  }
  
  public static String getDrawerControlCodes() {
    return config.getString("controlCode", "27,112,0,25,250");
  }
  
  public static String getDefaultDrawerControlCodes() {
    return "27,112,0,25,250";
  }
  
  public static String getDrawerPullReportHiddenColumns() {
    return config.getString("drawerPullReportColumns", "");
  }
  
  public static void setDrawerPullReportHiddenColumns(String value) {
    config.setProperty("drawerPullReportColumns", value);
  }
  
  public static String getTicketListViewHiddenColumns() {
    return config.getString("listViewColumns", "");
  }
  
  public static void setTicketListViewHiddenColumns(String value) {
    config.setProperty("listViewColumns", value);
  }
  
  public static String getDefaultView() {
    return config.getString("default_view", SwitchboardView.VIEW_NAME);
  }
  
  public static void setDefaultView(String viewName) {
    config.setProperty("default_view", viewName);
  }
  
  public static char[] getDrawerControlCodesArray() {
    String drawerControlCodes = getDefaultDrawerControlCodes();
    if (StringUtils.isEmpty(drawerControlCodes)) {
      drawerControlCodes = getDefaultDrawerControlCodes();
    }
    
    String[] split = drawerControlCodes.split(",");
    char[] codes = new char[split.length];
    for (int i = 0; i < split.length; i++) {
      try {
        codes[i] = ((char)Integer.parseInt(split[i]));
      } catch (Exception x) {
        codes[i] = '0';
      }
    }
    return codes;
  }
  
  public static void setDefaultLocale(String defaultLocal) {
    config.setProperty("defaultLocal", defaultLocal);
  }
  
  public static Locale getDefaultLocale() {
    String defaultLocaleString = config.getString("defaultLocal", null);
    if (StringUtils.isEmpty(defaultLocaleString)) {
      return null;
    }
    String language = "";
    String country = "";
    String variant = "";
    StringTokenizer st = new StringTokenizer(defaultLocaleString, "_");
    if (st.hasMoreTokens())
      language = st.nextToken();
    if (st.hasMoreTokens())
      country = st.nextToken();
    if (st.hasMoreTokens())
      variant = st.nextToken();
    Locale disName = new Locale(language, country, variant);
    
    return disName;
  }
  
  public static boolean isPaginatedMenuView() {
    return config.getBoolean("paginated_menu_view", Boolean.FALSE).booleanValue();
  }
  
  public static void setPaginatedMenuView(boolean selected) {
    config.setProperty("paginated_menu_view", Boolean.valueOf(selected));
  }
  
  public static boolean isAutoPrintReceipt() {
    return config.getBoolean("auto_print_receipt", Boolean.TRUE).booleanValue();
  }
  
  public static void setAutoPrintReceipt(boolean selected) {
    config.setProperty("auto_print_receipt", Boolean.valueOf(selected));
  }
  
  public static boolean isFilterByOwner() {
    return config.getBoolean("owner_filter", true);
  }
  
  public static void setFilterByOwner(boolean filter) {
    config.setProperty("owner_filter", Boolean.valueOf(filter));
  }
  
  public static void setOrderViewPageSize(int orderViewPageSize) {
    config.setProperty("order_view_page_size", Integer.valueOf(orderViewPageSize));
  }
  
  public static int getOrderViewPageSize() {
    return config.getInt("order_view_page_size", 10);
  }
  
  public static void setCheckUpdateOnStartUp(boolean checkUpdate) {
    config.setProperty("check_update_on_startup", Boolean.valueOf(checkUpdate));
  }
  
  public static boolean isCheckUpdateOnStartUp() {
    return config.getBoolean("check_update_on_startup", Boolean.FALSE).booleanValue();
  }
  
  public static void setKDSTicketsPerPage(int value) {
    config.setProperty("kds.ticket.per_page", Integer.valueOf(value));
  }
  
  public static int getKDSTicketsPerPage() {
    return config.getInt("kds.ticket.per_page", 8);
  }
  
  public static Color getColor(String propName, Color dfltColor) {
    Integer colorCode = Integer.valueOf(config.getInt(propName, 0));
    if ((colorCode == null) || (colorCode.intValue() == 0)) {
      return dfltColor;
    }
    return new Color(colorCode.intValue());
  }
  
  public static Color getColor(String propName) {
    return getColor(propName, null);
  }
}
