package com.floreantpos;










public class StoreAlreadyCloseException
  extends RuntimeException
{
  public StoreAlreadyCloseException() {}
  








  public StoreAlreadyCloseException(String arg0)
  {
    super(arg0);
  }
  
  public StoreAlreadyCloseException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }
  
  public StoreAlreadyCloseException(Throwable arg0) {
    super(arg0);
  }
}
