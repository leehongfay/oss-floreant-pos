package com.floreantpos.model;



public class TipsCashoutReportData
{
  private String ticketId;
  

  private String saleType;
  

  private Double ticketTotal;
  

  private Double cashTips;
  

  private Double chargedTips;
  
  private Double tipsPaidAmount;
  
  private boolean paid;
  
  private User owner;
  
  private Double amount;
  
  private Double declareTipsAmount;
  

  public TipsCashoutReportData() {}
  

  public String getSaleType()
  {
    return saleType;
  }
  
  public void setSaleType(String saleType) {
    this.saleType = saleType;
    if (this.saleType == null) {
      this.saleType = "";
    }
    else {
      this.saleType = this.saleType.replaceAll("_", " ");
    }
  }
  
  public String getTicketId() {
    return ticketId;
  }
  
  public void setTicketId(String ticketId) {
    this.ticketId = ticketId;
  }
  
  public Double getTicketTotal() {
    return ticketTotal;
  }
  
  public void setTicketTotal(Double ticketTotal) {
    this.ticketTotal = ticketTotal;
  }
  
  public Double getCashTips() {
    return Double.valueOf(cashTips == null ? 0.0D : cashTips.doubleValue());
  }
  
  public void setCashTips(Double tips) {
    cashTips = tips;
  }
  
  public boolean isPaid() {
    return paid;
  }
  
  public void setPaid(boolean paid) {
    this.paid = paid;
  }
  
  public Double getTipsPaidAmount() {
    return Double.valueOf(tipsPaidAmount == null ? 0.0D : tipsPaidAmount.doubleValue());
  }
  
  public void setTipsPaidAmount(Double tipsPaidAmount) {
    this.tipsPaidAmount = tipsPaidAmount;
  }
  
  public Double getChargedTips() {
    return Double.valueOf(chargedTips == null ? 0.0D : chargedTips.doubleValue());
  }
  
  public void setChargedTips(Double chargedTips) {
    this.chargedTips = chargedTips;
  }
  
  public User getOwner() {
    return owner;
  }
  
  public void setOwner(User owner) {
    this.owner = owner;
  }
  
  public Double getAmount() {
    return Double.valueOf(amount == null ? 0.0D : amount.doubleValue());
  }
  
  public void setAmount(Double amount) {
    this.amount = amount;
  }
  
  public double getDueAmount() {
    return getAmount().doubleValue() - getTipsPaidAmount().doubleValue() - declareTipsAmount.doubleValue();
  }
  
  public Double getDeclareTipsAmount() {
    return Double.valueOf(declareTipsAmount == null ? 0.0D : declareTipsAmount.doubleValue());
  }
  
  public void setDeclareTipsAmount(Double declareTipsAmount) {
    this.declareTipsAmount = declareTipsAmount;
  }
}
