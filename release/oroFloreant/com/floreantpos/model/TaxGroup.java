package com.floreantpos.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.floreantpos.model.base.BaseTaxGroup;
import java.util.Iterator;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;





@JsonIgnoreProperties(ignoreUnknown=true, value={""})
@XmlRootElement
public class TaxGroup
  extends BaseTaxGroup
{
  private static final long serialVersionUID = 1L;
  
  public TaxGroup() {}
  
  public TaxGroup(String id)
  {
    super(id);
  }
  





  public TaxGroup(String id, String name)
  {
    super(id, name);
  }
  




  public String toString()
  {
    String name = super.getName();
    List<Tax> taxes = getTaxes();
    if ((taxes == null) || (taxes.isEmpty())) {
      return name;
    }
    name = name + " (";
    for (Iterator iterator = taxes.iterator(); iterator.hasNext();) {
      Tax tax = (Tax)iterator.next();
      name = name + tax.getName() + ":" + tax.getRate();
      if (iterator.hasNext()) {
        name = name + ", ";
      }
    }
    name = name + ")";
    return name;
  }
  
  public String getUniqueId() {
    return ("taxgroup_" + getName() + "_" + getId()).replaceAll("\\s+", "_");
  }
}
