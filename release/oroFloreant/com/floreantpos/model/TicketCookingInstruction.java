package com.floreantpos.model;

import com.floreantpos.model.base.BaseTicketCookingInstruction;























public class TicketCookingInstruction
  extends BaseTicketCookingInstruction
{
  private static final long serialVersionUID = 1L;
  
  public TicketCookingInstruction() {}
  
  public String toString()
  {
    return getDescription();
  }
}
