package com.floreantpos.model;

import com.floreantpos.model.base.BaseAttribute;

public class Attribute
  extends BaseAttribute
{
  private static final long serialVersionUID = 1L;
  private Double extraPrice;
  private Boolean enable;
  
  public Attribute() {}
  
  public Attribute(String id)
  {
    super(id);
  }
  




  public String toString()
  {
    return super.getName();
  }
  
  public void setExtraPrice(Double extraPrice) {
    this.extraPrice = extraPrice;
  }
  
  public Double getExtraPrice() {
    return extraPrice == null ? new Double(0.0D) : extraPrice;
  }
  
  public void setEnable(Boolean enable) {
    this.enable = enable;
  }
  
  public Boolean isEnable() {
    return enable == null ? Boolean.FALSE : enable;
  }
}
