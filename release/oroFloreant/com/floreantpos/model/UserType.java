package com.floreantpos.model;

import com.floreantpos.model.base.BaseUserType;
import java.util.Set;
import javax.xml.bind.annotation.XmlRootElement;























@XmlRootElement(name="user-type")
public class UserType
  extends BaseUserType
{
  private static final long serialVersionUID = 1L;
  
  public UserType() {}
  
  public UserType(String id)
  {
    super(id);
  }
  

  public void clearPermissions()
  {
    Set<UserPermission> permissions = getPermissions();
    if (permissions != null) {
      permissions.clear();
    }
  }
  
  public boolean hasPermission(UserPermission permission) {
    Set<UserPermission> permissions = getPermissions();
    if (permissions == null) {
      return false;
    }
    
    return permissions.contains(permission);
  }
  
  public String toString()
  {
    String s = getName();
    


    return s;
  }
}
