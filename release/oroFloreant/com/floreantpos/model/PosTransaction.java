package com.floreantpos.model;

import com.floreantpos.config.CardConfig;
import com.floreantpos.model.base.BasePosTransaction;
import com.floreantpos.model.dao.CashDrawerDAO;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.lang.StringUtils;



















@XmlRootElement
public class PosTransaction
  extends BasePosTransaction
{
  private static final long serialVersionUID = 1L;
  private transient boolean refunded = false;
  private transient boolean syncEdited;
  private String cardTrack;
  private String cardNo;
  private String cardExpYear;
  private String cardExpMonth;
  public static final String CASH = "CASH";
  public static final String GIFT_CERT = "GIFT_CERT";
  
  public PosTransaction() {}
  
  public PosTransaction(String id) { super(id); }
  







  public PosTransaction(String id, String transactionType, String paymentType)
  {
    super(id, transactionType, paymentType);
  }
  


  public static final String CREDIT_CARD = "CREDIT_CARD";
  

  public static final String DEBIT_CARD = "DEBIT_CARD";
  

  public static final String CASH_DROP = "CASH_DROP";
  

  public static final String REFUND = "REFUND";
  

  public static final String PAY_OUT = "PAY_OUT";
  
  public static final String VOID_TRANS = "VOID_TRANS";
  
  public static final String GIFT_CARD_BALANCE_ADD = "GIFT_CARD_BALANCE_ADD";
  
  public String getTransactionType()
  {
    String type = super.getTransactionType();
    
    if (StringUtils.isEmpty(type)) {
      return TransactionType.CREDIT.name();
    }
    
    return type;
  }
  
  public void updateTerminalBalance() {
    Terminal terminal = getTerminal();
    if (terminal == null) {
      return;
    }
    
    Double amount = getAmount();
    if ((amount == null) || (amount.doubleValue() == 0.0D)) {}
  }
  















  public boolean isCard()
  {
    return ((this instanceof CreditCardTransaction)) || ((this instanceof DebitCardTransaction));
  }
  
  public void addProperty(String name, String value) {
    if (getProperties() == null) {
      setProperties(new HashMap());
    }
    
    getProperties().put(name, value);
  }
  
  public boolean hasProperty(String key) {
    return getProperty(key) != null;
  }
  
  public String getProperty(String key) {
    if (getProperties() == null) {
      return null;
    }
    
    return (String)getProperties().get(key);
  }
  
  public boolean isPropertyValueTrue(String propertyName) {
    String property = getProperty(propertyName);
    
    return POSUtil.getBoolean(property);
  }
  
  public Double calculateTotalAmount() {
    return Double.valueOf(getAmount().doubleValue() + getTipsAmount().doubleValue());
  }
  
  public Double calculateAuthorizeAmount()
  {
    double advanceTipsPercentage = CardConfig.getAdvanceTipsPercentage();
    return Double.valueOf(getTenderAmount().doubleValue() + getTenderAmount().doubleValue() * (advanceTipsPercentage / 100.0D));
  }
  
  @XmlTransient
  public String getCardTrack() {
    return cardTrack;
  }
  
  public void setCardTrack(String cardTrack) {
    this.cardTrack = cardTrack;
  }
  
  public String getCardNo() {
    return cardNo;
  }
  
  public void setCardNo(String cardNo) {
    this.cardNo = cardNo;
  }
  
  public String getCardExpYear() {
    return cardExpYear;
  }
  
  public void setCardExpYear(String expYear) {
    cardExpYear = expYear;
  }
  
  public String getCardExpMonth() {
    return cardExpMonth;
  }
  
  public void setCardExpMonth(String expMonth) {
    cardExpMonth = expMonth;
  }
  
  public String getGlobalId() {
    return super.getId();
  }
  
  public String getTicketId() {
    Ticket ticket = getTicket();
    if (ticket == null) {
      return "";
    }
    return ticket.getId();
  }
  
  public void setAmount(Double amount)
  {
    super.setAmount(amount);
  }
  
  public void calculateTaxAmount() {
    if (getAmount().doubleValue() <= 0.0D)
      return;
    Ticket ticket = getTicket();
    if ((ticket == null) || (ticket.getTotalAmount().doubleValue() <= 0.0D) || (isVoided().booleanValue()) || ((this instanceof RefundTransaction)))
      return;
    setTaxAmount(Double.valueOf(NumberUtil.roundToTwoDigit((getAmount().doubleValue() - getTipsAmount().doubleValue()) * ticket.getTaxAmount().doubleValue() / ticket.getTotalAmount().doubleValue())));
  }
  
  public boolean isRefunded() {
    return refunded;
  }
  
  public void setRefunded(boolean refunded) {
    this.refunded = refunded;
  }
  
  public Department getDepartment() {
    return DataProvider.get().getDepartmentById(getDepartmentId());
  }
  
  public void setDepartment(Department department) {
    String departmentId = null;
    if (department != null) {
      departmentId = department.getId();
    }
    super.setDepartmentId(departmentId);
  }
  
  public Terminal getTerminal() {
    return DataProvider.get().getTerminalById(getTerminalId());
  }
  
  public void setTerminal(Terminal terminal) {
    Integer terminalId = null;
    if (terminal != null) {
      terminalId = terminal.getId();
    }
    super.setTerminalId(terminalId);
  }
  
  public User getUser() {
    return DataProvider.get().getUserById(getUserId());
  }
  
  public void setUser(User user) {
    String userId = null;
    if (user != null) {
      userId = user.getId();
    }
    super.setUserId(userId);
  }
  
  public void setCashDrawer(CashDrawer cashDrawer) {
    super.setCashDrawerId(cashDrawer.getId());
  }
  
  public CashDrawer getCashDrawer() {
    return CashDrawerDAO.getInstance().get(getCashDrawerId());
  }
  
  public boolean isSyncEdited() {
    return syncEdited;
  }
  
  public void setSyncEdited(boolean syncEdited) {
    this.syncEdited = syncEdited;
  }
}
