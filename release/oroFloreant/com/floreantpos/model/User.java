package com.floreantpos.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.floreantpos.IconFactory;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.base.BaseUser;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.util.AESencrp;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.lang.StringUtils;




















@JsonIgnoreProperties(ignoreUnknown=true, value={"image", "status", "linkedUser", "linkedUsersIfExists"})
@XmlRootElement(name="user")
public class User
  extends BaseUser
{
  private static final long serialVersionUID = 1L;
  public static final String USER_TYPE_MANAGER = "MANAGER";
  public static final String USER_TYPE_CASHIER = "CASHIER";
  public static final String USER_TYPE_SERVER = "SERVER";
  
  public User() {}
  
  public User(String id)
  {
    super(id);
  }
  





  public UserType getType()
  {
    String userTypeId2 = getUserTypeId();
    if (userTypeId2 != null) {
      return DataProvider.get().getUserType(userTypeId2);
    }
    return null;
  }
  
  public void setType(UserType userType) {
    if (userType != null) {
      super.setUserTypeId(userType.getId());
    }
    else {
      super.setUserTypeId(null);
    }
  }
  
  public Shift getCurrentShift() {
    return DataProvider.get().getShiftById(getCurrentShiftId());
  }
  
  public void setCurrentShift(Shift shift) {
    if (shift != null) {
      super.setCurrentShiftId(shift.getId());
    }
    else {
      super.setCurrentShiftId(null);
    }
  }
  


  public Boolean isActive()
  {
    return active == null ? Boolean.TRUE : active;
  }
  
  public boolean hasPermission(UserPermission permission) {
    return getType().hasPermission(permission);
  }
  



  /**
   * @deprecated
   */
  public CashDrawer getCurrentCashDrawer()
  {
    return super.getCurrentCashDrawer();
  }
  
  public CashDrawer getActiveDrawerPullReport() {
    CashDrawer currentDrawerPullReport = super.getCurrentCashDrawer();
    if (currentDrawerPullReport != null)
      return currentDrawerPullReport;
    Terminal terminal = Application.getInstance().getTerminal();
    if (terminal != null) {
      return terminal.getCurrentCashDrawer();
    }
    return null;
  }
  
  public void doClockIn(Terminal terminal, Shift shift, Calendar currentTime) {
    UserDAO.getInstance().refresh(this);
    if (isClockedIn().booleanValue()) {
      throw new PosException("Duplicate clock in attempt for user " + getFullName());
    }
    List<User> linkedUsers = getLinkedUser();
    if (linkedUsers != null) {
      for (User user : linkedUsers) {
        if (user.isClockedIn().booleanValue()) {
          throw new PosException("Duplicate clock in attempt for user " + getFullName());
        }
      }
    }
    
    setClockedIn(Boolean.valueOf(true));
    setCurrentShift(shift);
    setLastClockInTime(currentTime.getTime());
    if (isDriver().booleanValue()) {
      setAvailableForDelivery(Boolean.valueOf(true));
    }
    
    AttendenceHistory attendenceHistory = new AttendenceHistory();
    attendenceHistory.setClockInTime(currentTime.getTime());
    attendenceHistory.setClockInHour(Short.valueOf((short)currentTime.get(11)));
    attendenceHistory.setUser(this);
    attendenceHistory.setTerminal(terminal);
    attendenceHistory.setShift(shift);
    
    UserDAO.getInstance().saveClockIn(this, attendenceHistory, shift, currentTime);
  }
  
  public void doClockOut(AttendenceHistory attendenceHistory, Shift shift, Calendar currentTime) {
    setClockedIn(Boolean.valueOf(false));
    setCurrentShift(null);
    setLastClockInTime(null);
    setLastClockOutTime(null);
    if (isDriver().booleanValue()) {
      setAvailableForDelivery(Boolean.valueOf(false));
    }
    
    attendenceHistory.setClockedOut(Boolean.valueOf(true));
    attendenceHistory.setClockOutTime(currentTime.getTime());
    attendenceHistory.setClockOutHour(Short.valueOf((short)currentTime.get(11)));
    
    UserDAO.getInstance().saveClockOut(this, attendenceHistory, shift, currentTime);
  }
  
  public boolean canViewAllOpenTickets() {
    if (getType() == null) {
      return false;
    }
    
    Set<UserPermission> permissions = getType().getPermissions();
    if (permissions == null) {
      return false;
    }
    
    for (UserPermission permission : permissions) {
      if (permission.equals(UserPermission.EDIT_OTHER_USERS_TICKETS)) {
        return true;
      }
    }
    return false;
  }
  
  public boolean canViewAllCloseTickets() {
    if (getType() == null) {
      return false;
    }
    
    Set<UserPermission> permissions = getType().getPermissions();
    if (permissions == null) {
      return false;
    }
    
    for (UserPermission permission : permissions) {
      if (permission.equals(UserPermission.EDIT_OTHER_USERS_TICKETS)) {
        return true;
      }
    }
    
    return false;
  }
  
  public void setFullName(String str) {}
  
  public String getStatus()
  {
    if (isClockedIn().booleanValue()) {
      if (isAvailableForDelivery().booleanValue()) {
        return "Available";
      }
      
      return "Driving";
    }
    

    return "Not available";
  }
  
  public String getFullName()
  {
    return getFirstName() + " " + getLastName();
  }
  
  public String toString()
  {
    return getFirstName() + " " + getLastName();
  }
  
  public boolean isManager() {
    return hasPermission(UserPermission.PERFORM_MANAGER_TASK);
  }
  
  public boolean isAdministrator() {
    return hasPermission(UserPermission.PERFORM_ADMINISTRATIVE_TASK);
  }
  
  public void setImage(ImageIcon image) {}
  
  @XmlTransient
  public ImageIcon getImage()
  {
    return IconFactory.getIconFromImageResource(getImageId(), 150, 150);
  }
  
  public boolean hasLinkedUser() {
    List<User> linkedUser = getLinkedUser();
    if ((linkedUser != null) && (linkedUser.size() > 0)) {
      return true;
    }
    if (getParentUser() == null) {
      return false;
    }
    return getParentUser().hasLinkedUser();
  }
  
  public List<User> getLinkedUsersIfExists() {
    List<User> linkedUser = getLinkedUser();
    if (linkedUser != null) {
      if (!linkedUser.contains(this)) {
        linkedUser.add(0, this);
      }
      return linkedUser;
    }
    if (getParentUser() == null) {
      return null;
    }
    return getParentUser().getLinkedUsersIfExists();
  }
  
  public void setEncryptedPassword(String plainText) {
    try {
      super.setPassword(AESencrp.encrypt(plainText));
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    }
  }
  
  public String getPasswordAsPlainText() {
    String secretKey = super.getPassword();
    if (StringUtils.isNotEmpty(secretKey)) {
      try {
        return AESencrp.decrypt(secretKey);
      } catch (Exception e) {
        PosLog.error(getClass(), e);
      }
    }
    return secretKey;
  }
}
