package com.floreantpos.model;

import com.floreantpos.main.Application;
import com.floreantpos.model.base.BaseTicketItemModifier;
import com.floreantpos.model.ext.KitchenStatus;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;





























public class TicketItemModifier
  extends BaseTicketItemModifier
  implements ITicketItem
{
  private static final long serialVersionUID = 1L;
  public static final int NORMAL_MODIFIER = 1;
  public static final int EXTRA_MODIFIER = 3;
  public static final int CRUST = 5;
  public static final int SEPERATOR = 6;
  public static final String TRANSIENT_PROP_TICKET_ITEM_QUANTITY = "ticketItemQuantity";
  public MenuModifier menuModifier;
  private boolean selected;
  private int tableRowNum;
  private double ticketItemQuantity;
  private List<TicketItemTax> taxes;
  
  public TicketItemModifier() {}
  
  public TicketItemModifier(String id)
  {
    super(id);
  }
  
  public TicketItemModifier(TicketItem ticketItem, String name, double unitPrice, double quantity) {
    setTicketItem(ticketItem);
    setName(name);
    setUnitPrice(Double.valueOf(unitPrice));
    setItemQuantity(Double.valueOf(quantity));
  }
  
  public TicketItemModifier(String id, TicketItem ticketItem, String name, double unitPrice, double quantity, TicketItemTax tax) {
    setId(id);
    setTicketItem(ticketItem);
    setName(name);
    setUnitPrice(Double.valueOf(unitPrice));
    setItemQuantity(Double.valueOf(quantity));
    addTotaxes(tax);
  }
  
  public int getTableRowNum() {
    return tableRowNum;
  }
  
  public void setTableRowNum(int tableRowNum) {
    this.tableRowNum = tableRowNum;
  }
  
  public String toString()
  {
    return getName();
  }
  
  public boolean canAddCookingInstruction() {
    return false;
  }
  
  public void calculatePrice() {
    if (isInfoOnly().booleanValue()) {
      return;
    }
    

    BigDecimal unitPrice = NumberUtil.convertToBigDecimal(getUnitPrice().doubleValue());
    BigDecimal subTotalAmount = NumberUtil.round(calculateSubTotal(unitPrice));
    BigDecimal discountAmount = NumberUtil.round(calculateDiscount(subTotalAmount));
    BigDecimal taxAmount = NumberUtil.round(calculateTax(subTotalAmount, discountAmount));
    BigDecimal totalAmount = NumberUtil.convertToBigDecimal(0.0D);
    
    if (isTaxIncluded().booleanValue()) {
      totalAmount = subTotalAmount.subtract(discountAmount);
    } else {
      totalAmount = subTotalAmount.subtract(discountAmount).add(taxAmount);
    }
    totalAmount = NumberUtil.round(totalAmount);
    
    setSubTotalAmount(Double.valueOf(subTotalAmount.doubleValue()));
    setDiscountAmount(Double.valueOf(discountAmount.doubleValue()));
    setTaxAmount(Double.valueOf(taxAmount.doubleValue()));
    setTotalAmount(Double.valueOf(totalAmount.doubleValue()));
    

    setAdjustedUnitPrice(getUnitPrice());
    setAdjustedDiscount(Double.valueOf(discountAmount.doubleValue()));
    setAdjustedSubtotal(getSubTotalAmount());
    setAdjustedTax(getTaxAmount());
    setAdjustedTotal(getTotalAmount());
  }
  
  public void calculateAdjustedPrice() {
    Ticket ticket = getTicketItem().getTicket();
    if (ticket == null) {
      return;
    }
    
    BigDecimal subTotalAmount = NumberUtil.convertToBigDecimal(getSubTotalAmount().doubleValue());
    BigDecimal discountAmount = NumberUtil.convertToBigDecimal(getDiscountAmount().doubleValue());
    BigDecimal subtotalAfterDiscount = NumberUtil.round(subTotalAmount.subtract(discountAmount));
    
    BigDecimal ticketDiscountAmount = NumberUtil.round(NumberUtil.convertToBigDecimal(ticket.getTicketDiscountAmount()));
    BigDecimal ticketSubtotalAmount = NumberUtil.round(NumberUtil.convertToBigDecimal(ticket.getSubtotalAmountWithVoidItems()));
    BigDecimal totalItemDiscountAmount = NumberUtil.round(NumberUtil.convertToBigDecimal(ticket.getItemDiscountAmount()));
    BigDecimal ticketSubtotalAfterItemDiscount = NumberUtil.round(ticketSubtotalAmount.subtract(totalItemDiscountAmount));
    BigDecimal ticketSubtotalAfterAllDiscount = NumberUtil.round(ticketSubtotalAfterItemDiscount.subtract(ticketDiscountAmount));
    
    BigDecimal adjustedSubtotal = NumberUtil.round(subtotalAfterDiscount.multiply(ticketSubtotalAfterAllDiscount).divide(ticketSubtotalAfterItemDiscount, 4, RoundingMode.HALF_UP));
    
    BigDecimal adjustedDiscount = NumberUtil.round(subTotalAmount.subtract(adjustedSubtotal));
    













    BigDecimal adjustedTax = NumberUtil.round(calculateTax(adjustedSubtotal, new BigDecimal("0")));
    





    BigDecimal adjustedTotal = NumberUtil.convertToBigDecimal(0.0D);
    
    if (isTaxIncluded().booleanValue()) {
      adjustedTotal = adjustedSubtotal;
    }
    else {
      adjustedTotal = NumberUtil.round(adjustedSubtotal.add(adjustedTax));
    }
    
    setAdjustedUnitPrice(
      Double.valueOf(adjustedSubtotal.divide(NumberUtil.convertToBigDecimal(getItemQuantity().doubleValue()), 4, RoundingMode.HALF_UP).doubleValue()));
    setAdjustedDiscount(Double.valueOf(adjustedDiscount.doubleValue()));
    setAdjustedSubtotal(Double.valueOf(adjustedSubtotal.doubleValue()));
    setAdjustedTax(Double.valueOf(adjustedTax.doubleValue()));
    setAdjustedTotal(Double.valueOf(adjustedTotal.doubleValue()));
  }
  
  public double calculateDiscount(double subtotalAmount) {
    double discount = 0.0D;
    List<TicketItemDiscount> discounts = getTicketItem().getDiscounts();
    if (discounts != null) {
      for (TicketItemDiscount ticketItemDiscount : discounts) {
        if (ticketItemDiscount.getType().intValue() != 2)
        {

          if (ticketItemDiscount.getType().intValue() == 1) {
            discount += ticketItemDiscount.calculateDiscount(subtotalAmount);
          }
          else if (ticketItemDiscount.getType().intValue() == 0) {
            double discountPercentage = ticketItemDiscount.getValue().doubleValue() * 100.0D / getTicketItem().getSubtotalAmount().doubleValue();
            TicketItemDiscount percentDiscount = new TicketItemDiscount();
            percentDiscount.setType(Integer.valueOf(1));
            percentDiscount.setValue(Double.valueOf(discountPercentage));
            percentDiscount.setTicketItem(getTicketItem());
            percentDiscount.setCouponQuantity(ticketItemDiscount.getCouponQuantity());
            discount += percentDiscount.calculateDiscount(subtotalAmount);
          } }
      }
    }
    if (discount > Math.abs(subtotalAmount)) {
      return subtotalAmount;
    }
    return discount;
  }
  
  public BigDecimal calculateDiscount(BigDecimal subtotalAmount) {
    BigDecimal discount = NumberUtil.convertToBigDecimal(0.0D);
    List<TicketItemDiscount> discounts = getTicketItem().getDiscounts();
    if (discounts != null) {
      for (TicketItemDiscount ticketItemDiscount : discounts) {
        if (ticketItemDiscount.getType().intValue() != 2)
        {

          if (ticketItemDiscount.getType().intValue() == 1) {
            discount = discount.add(ticketItemDiscount.calculateDiscount(subtotalAmount));
          }
          else if (ticketItemDiscount.getType().intValue() == 0) {
            double discountPercentage = ticketItemDiscount.getValue().doubleValue() * 100.0D / getTicketItem().getSubtotalAmount().doubleValue();
            TicketItemDiscount percentDiscount = new TicketItemDiscount();
            percentDiscount.setType(Integer.valueOf(1));
            percentDiscount.setValue(Double.valueOf(discountPercentage));
            percentDiscount.setTicketItem(getTicketItem());
            percentDiscount.setCouponQuantity(ticketItemDiscount.getCouponQuantity());
            discount = discount.add(percentDiscount.calculateDiscount(subtotalAmount));
          } }
      }
    }
    if (discount.compareTo(subtotalAmount.abs()) > 0) {
      return subtotalAmount;
    }
    return NumberUtil.round(discount);
  }
  
  public void merge(TicketItemModifier otherItem) {
    setItemQuantity(Double.valueOf(getItemQuantity().doubleValue() + otherItem.getItemQuantity().doubleValue()));
  }
  
  private BigDecimal calculateTax(BigDecimal subtotalAmount, BigDecimal discountAmount)
  {
    BigDecimal totalTaxAmount = NumberUtil.convertToBigDecimal("0");
    subtotalAmount = subtotalAmount.subtract(discountAmount);
    List<TicketItemTax> taxList = getTaxes();
    BigDecimal taxRatePercentage; if (taxList != null) { double actualPrice;
      if (isTaxIncluded().booleanValue()) {
        taxRatePercentage = NumberUtil.convertToBigDecimal(0.0D);
        for (TicketItemTax ticketItemModifierTax : taxList) {
          taxRatePercentage = taxRatePercentage.add(NumberUtil.convertToBigDecimal(ticketItemModifierTax.getRate().doubleValue()).divide(NumberUtil.convertToBigDecimal("100"), 4, RoundingMode.HALF_UP));
        }
        
        if (taxRatePercentage.compareTo(NumberUtil.convertToBigDecimal("0")) == 0) {
          return totalTaxAmount;
        }
        
        BigDecimal totalTaxRatePercentage = taxRatePercentage.add(NumberUtil.convertToBigDecimal(1.0D));
        actualPrice = subtotalAmount.doubleValue() / totalTaxRatePercentage.doubleValue();
        
        for (TicketItemTax ticketItemModifierTax : taxList) {
          taxRatePercentage = NumberUtil.convertToBigDecimal(ticketItemModifierTax.getRate().doubleValue()).divide(NumberUtil.convertToBigDecimal("100"), 4, RoundingMode.HALF_UP);
          BigDecimal tax = NumberUtil.convertToBigDecimal(actualPrice).multiply(taxRatePercentage);
          ticketItemModifierTax.setTaxAmount(Double.valueOf(tax.doubleValue()));
          totalTaxAmount = totalTaxAmount.add(tax);
        }
      }
      else
      {
        for (TicketItemTax ticketItemModifierTax : taxList) {
          BigDecimal taxRatePercentage = NumberUtil.convertToBigDecimal(ticketItemModifierTax.getRate().doubleValue()).divide(NumberUtil.convertToBigDecimal("100"), 4, RoundingMode.HALF_UP);
          if (taxRatePercentage.compareTo(NumberUtil.convertToBigDecimal("0")) == 0) {
            return totalTaxAmount;
          }
          
          BigDecimal tax = subtotalAmount.multiply(taxRatePercentage);
          ticketItemModifierTax.setTaxAmount(Double.valueOf(tax.doubleValue()));
          totalTaxAmount = totalTaxAmount.add(tax);
        }
      }
    }
    

    buildTaxes();
    return NumberUtil.round(totalTaxAmount);
  }
  
  public double getTotalTaxRate()
  {
    List<TicketItemTax> ticketItemTaxes = getTaxes();
    if ((ticketItemTaxes == null) || (ticketItemTaxes.isEmpty())) {
      return 0.0D;
    }
    double totalTaxRate = 0.0D;
    for (TicketItemTax tax : ticketItemTaxes) {
      totalTaxRate += tax.getRate().doubleValue();
    }
    return totalTaxRate;
  }
  
  private BigDecimal calculateSubTotal(BigDecimal unitPrice)
  {
    Double modifierQty = getItemQuantity();
    if (getTicketItem() != null) {
      modifierQty = Double.valueOf(modifierQty.doubleValue() * getTicketItem().getQuantity().doubleValue());
    }
    return unitPrice.multiply(NumberUtil.convertToBigDecimal(modifierQty.doubleValue()));
  }
  
  public String getNameDisplay()
  {
    return getItemQuantityDisplay() + " " + getNameDisplay(false);
  }
  
  public String getNameDisplay(boolean kitchenReceipt) {
    if (isInfoOnly().booleanValue()) {
      return getName().trim();
    }
    double itemQuantity = getItemQuantity().doubleValue();
    String itemQuantityDisplay = "";
    if (itemQuantity > 1.0D)
      itemQuantityDisplay = NumberUtil.trimDecilamIfNotNeeded(Double.valueOf(itemQuantity)) + "x ";
    String display;
    String display;
    if (itemQuantity > 1.0D) {
      display = itemQuantityDisplay + getName();
    }
    else {
      display = getName().trim();
    }
    if (getModifierType().intValue() == 1) {
      display = display + "*";
    }
    Store store = Application.getInstance().getStore();
    
    boolean isShowModifierPrice = store.getProperty("showModifierPrice") == null ? false : Boolean.valueOf(store.getProperty("showModifierPrice")).booleanValue();
    
    if ((isShowModifierPrice) && (getUnitPrice().doubleValue() > 0.0D) && (!kitchenReceipt)) {
      display = itemQuantityDisplay + getName() + " @" + NumberUtil.formatNumber(getUnitPrice());
    }
    
    return " -- " + display;
  }
  
  public Double getUnitPriceDisplay()
  {
    if (isInfoOnly().booleanValue()) {
      return null;
    }
    return getUnitPrice();
  }
  







  public String getItemQuantityDisplay()
  {
    return "";
  }
  
  public Double getTaxAmountWithoutModifiersDisplay()
  {
    if (isInfoOnly().booleanValue()) {
      return null;
    }
    
    return getTaxAmount();
  }
  
  public Double getTotalAmountWithoutModifiersDisplay()
  {
    if (isInfoOnly().booleanValue()) {
      return null;
    }
    
    return getTotalAmount();
  }
  
  public Double getSubTotalAmountWithoutModifiersDisplay()
  {
    if (isInfoOnly().booleanValue()) {
      return null;
    }
    
    return getSubTotalAmount();
  }
  
  public String getItemCode()
  {
    return "";
  }
  
  public boolean isSelected() {
    return selected;
  }
  
  public void setSelected(boolean selected) {
    this.selected = selected;
  }
  
  public Double getItemQuantity()
  {
    Double itemQuantity = super.getItemQuantity();
    if (itemQuantity.doubleValue() == 0.0D) {
      itemQuantity = Double.valueOf(super.getItemCount().intValue());
    }
    
    return itemQuantity;
  }
  
  public boolean canAddDiscount()
  {
    return false;
  }
  
  public boolean canVoid()
  {
    return false;
  }
  
  public boolean canAddAdOn()
  {
    return false;
  }
  
  public KitchenStatus getKitchenStatusValue() {
    return KitchenStatus.fromString(super.getStatus());
  }
  
  public void setKitchenStatusValue(KitchenStatus kitchenStatus) {
    super.setStatus(kitchenStatus.name());
  }
  
  public Double getSubTotalAmountDisplay()
  {
    return null;
  }
  
  @XmlTransient
  public MenuModifier getMenuModifier() {
    return menuModifier;
  }
  
  public void setMenuModifier(MenuModifier menuModifier) {
    this.menuModifier = menuModifier;
  }
  
  public boolean isSaved()
  {
    return getId() != null;
  }
  
  public void setTaxes(List<TicketItemTax> taxes) {
    this.taxes = taxes;
    if (this.taxes == null) {
      this.taxes = new ArrayList();
    }
  }
  
  public List<TicketItemTax> getTaxes() {
    if (taxes == null) {
      taxes = new ArrayList();
      
      String property = super.getTaxesProperty();
      if (StringUtils.isNotEmpty(property)) {
        JsonReader jsonParser = Json.createReader(new StringReader(property));
        JsonArray jsonArray = jsonParser.readArray();
        jsonParser.close();
        for (int i = 0; i < jsonArray.size(); i++) {
          JsonObject tObj = (JsonObject)jsonArray.get(i);
          TicketItemTax tit = new TicketItemTax();
          tit.setName(tObj.getString(TicketItemTax.PROP_NAME));
          tit.setRate(Double.valueOf(POSUtil.parseDouble("" + tObj.get(TicketItemTax.PROP_RATE))));
          tit.setTaxAmount(Double.valueOf(POSUtil.parseDouble("" + tObj.get(TicketItemTax.PROP_TAX_AMOUNT))));
          taxes.add(tit);
        }
      }
    }
    return taxes;
  }
  
  public void addTotaxes(TicketItemTax tax) {
    if (taxes == null) {
      taxes = new ArrayList();
    }
    taxes = getTaxes();
    taxes.add(tax);
  }
  
  public void buildTaxes() {
    if ((taxes == null) || (taxes.isEmpty())) {
      setTaxesProperty(null);
      return;
    }
    JSONArray jsonArray = new JSONArray();
    for (TicketItemTax ticketItemTax : taxes) {
      jsonArray.put(ticketItemTax.toJson());
    }
    setTaxesProperty(jsonArray.toString());
  }
  
  public void setTicketItemQuantity(double ticketItemQuantity) {
    this.ticketItemQuantity = ticketItemQuantity;
  }
  
  public double getTicketItemQuantity() {
    if (getTicketItem() != null) {
      return getTicketItem().getQuantity().doubleValue();
    }
    return ticketItemQuantity;
  }
}
