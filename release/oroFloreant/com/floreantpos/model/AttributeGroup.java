package com.floreantpos.model;

import com.floreantpos.model.base.BaseAttributeGroup;





public class AttributeGroup
  extends BaseAttributeGroup
{
  private static final long serialVersionUID = 1L;
  
  public AttributeGroup() {}
  
  public AttributeGroup(String id)
  {
    super(id);
  }
  





  public AttributeGroup(String id, String name)
  {
    super(id, name);
  }
  




  public String toString()
  {
    return super.getName();
  }
}
