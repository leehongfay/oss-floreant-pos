package com.floreantpos.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.floreantpos.main.Application;
import com.floreantpos.model.base.BaseMenuCategory;
import java.awt.Color;
import java.util.Set;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.lang.StringUtils;





















@JsonIgnoreProperties(ignoreUnknown=true, value={"buttonColor", "textColor", "menuGroups", "discounts", "orderTypes", "departments"})
@XmlRootElement(name="menu-category")
public class MenuCategory
  extends BaseMenuCategory
{
  private static final long serialVersionUID = 1L;
  private Color buttonColor;
  private Color textColor;
  
  public MenuCategory() {}
  
  public MenuCategory(String id)
  {
    super(id);
  }
  





  public MenuCategory(String id, String name)
  {
    super(id, name);
  }
  







  public Integer getSortOrder()
  {
    return Integer.valueOf(sortOrder == null ? 9999 : sortOrder.intValue());
  }
  
  @XmlTransient
  public Color getButtonColor() {
    if (buttonColor != null) {
      return buttonColor;
    }
    
    if ((getButtonColorCode() == null) || (getButtonColorCode().intValue() == 0)) {
      return null;
    }
    
    return this.buttonColor = new Color(getButtonColorCode().intValue());
  }
  
  public void setButtonColor(Color buttonColor) {
    this.buttonColor = buttonColor;
  }
  
  @XmlTransient
  public Color getTextColor() {
    if (textColor != null) {
      return textColor;
    }
    
    if ((getTextColorCode() == null) || (getTextColorCode().intValue() == 0)) {
      return null;
    }
    
    return this.textColor = new Color(getTextColorCode().intValue());
  }
  
  public void setTextColor(Color textColor) {
    this.textColor = textColor;
  }
  
  @JsonIgnore
  public String getDisplayName() {
    Terminal terminal = Application.getInstance().getTerminal();
    if ((terminal != null) && (terminal.isTranslatedName().booleanValue()) && (StringUtils.isNotEmpty(getTranslatedName()))) {
      return getTranslatedName();
    }
    
    return super.getName();
  }
  
  @XmlTransient
  public Set<Department> getDepartments() {
    return super.getDepartments();
  }
  
  public String toString()
  {
    return getDisplayName();
  }
  
  public String getUniqueId() {
    return ("menu_category_" + getName() + "_" + getId()).replaceAll("\\s+", "_");
  }
}
