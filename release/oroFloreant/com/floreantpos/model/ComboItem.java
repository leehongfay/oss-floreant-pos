package com.floreantpos.model;

import com.floreantpos.model.base.BaseComboItem;
import com.floreantpos.model.dao.MenuItemDAO;
import org.apache.commons.lang.StringUtils;



public class ComboItem
  extends BaseComboItem
{
  private static final long serialVersionUID = 1L;
  private MenuItem menuItem;
  
  public ComboItem() {}
  
  public ComboItem(String id)
  {
    super(id);
  }
  



  public MenuItem getMenuItem()
  {
    if (menuItem == null) {
      String itemId = getMenuItemId();
      if (StringUtils.isEmpty(itemId)) {
        return null;
      }
      menuItem = MenuItemDAO.getInstance().loadInitialized(itemId);
    }
    return menuItem;
  }
  
  public void setMenuItem(MenuItem menuItem) {
    this.menuItem = menuItem;
    if (menuItem != null) {
      setMenuItemId(menuItem.getId());
      setName(menuItem.getDisplayName());
      setPrice(menuItem.getPrice());
    }
  }
}
