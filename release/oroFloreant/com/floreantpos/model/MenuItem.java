package com.floreantpos.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.floreantpos.IconFactory;
import com.floreantpos.main.Application;
import com.floreantpos.model.base.BaseMenuItem;
import com.floreantpos.model.dao.CourseDAO;
import com.floreantpos.model.dao.ImageResourceDAO;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.model.util.DataProvider;
import java.awt.Color;
import java.awt.Image;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;


















@JsonIgnoreProperties(ignoreUnknown=true, value={"buttonColor", "textColor", "image", "orderTypeList", "properties", "menuItemModiferSpecs", "pizzaPriceList", "discounts", "variants", "comboGroups", "comboItems", "stockUnits", "terminals", "recepieItems", "units", "sizes"})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MenuItem
  extends BaseMenuItem
{
  private static final long serialVersionUID = 1L;
  public static final String TRANSIENT_PROP_VENDOR_NAME = "vendorNames";
  private transient String vendorNames;
  @XmlTransient
  private Color buttonColor;
  @XmlTransient
  private Color textColor;
  @XmlTransient
  private ImageIcon image;
  private Double priceRulePrice;
  private MenuItem parentMenuItem;
  private MenuItemInventoryStatus stockStatus;
  public static final String TYPE_RAW_METARIAL = "Raw Material";
  public static final String TYPE_INVENTORY_ITEM = "Inventory Item";
  public static final String TYPE_MENU_ITEM = "Menu Item";
  private transient JSONObject properties;
  
  public MenuItem() {}
  
  public MenuItem(String id)
  {
    super(id);
  }
  






  public MenuItem(String id, String name, Double price)
  {
    super(id, name, price);
  }
  





















  public void setImage(ImageIcon image)
  {
    this.image = image;
  }
  
  public ImageIcon getImage() {
    return IconFactory.getIconFromImageResource(getImageId(), 80, 80);
  }
  
  public ImageIcon getImage(int w, int h) {
    return IconFactory.getIconFromImageResource(getImageId(), w - 20, h - 20);
  }
  
  public ImageIcon getScaledImageIcon(int w, int h) {
    ImageResource imageResource = ImageResourceDAO.getInstance().findById(getImageId());
    if (imageResource != null) {
      image = new ImageIcon(imageResource.getImage().getScaledInstance(w, h, 1));
    }
    return image;
  }
  
  public Integer getSortOrder()
  {
    return Integer.valueOf(super.getSortOrder() == null ? 9999 : super.getSortOrder().intValue());
  }
  
  @XmlTransient
  public Color getButtonColor() {
    if ((buttonColor != null) && (buttonColor.getRGB() == getButtonColorCode().intValue())) {
      return buttonColor;
    }
    
    if (getButtonColorCode() == null) {
      return null;
    }
    
    return this.buttonColor = new Color(getButtonColorCode().intValue());
  }
  
  public void setButtonColor(Color buttonColor) {
    this.buttonColor = buttonColor;
  }
  
  @XmlTransient
  public Color getTextColor() {
    if ((textColor != null) && (textColor.getRGB() == getTextColorCode().intValue())) {
      return textColor;
    }
    
    if (getTextColorCode() == null) {
      return null;
    }
    
    return this.textColor = new Color(getTextColorCode().intValue());
  }
  
  public void setTextColor(Color textColor) {
    this.textColor = textColor;
  }
  
  @XmlTransient
  public String getDisplayName() {
    if (StringUtils.isNotEmpty(super.getTranslatedName())) {
      return super.getTranslatedName();
    }
    String name = super.getName();
    return name;
  }
  
  @XmlTransient
  public String getVariantName() {
    String name = super.getName();
    List<Attribute> attributes = getAttributes();
    if (attributes == null) {
      return name;
    }
    name = name + " (";
    for (Iterator iterator = attributes.iterator(); iterator.hasNext();) {
      Attribute attribute = (Attribute)iterator.next();
      name = name + attribute.getName();
      if (iterator.hasNext()) {
        name = name + ", ";
      }
    }
    name = name + ")";
    return name;
  }
  


  public void setVariantName(String variantName) {}
  

  public void setDisplayName(String displayName) {}
  

  public String toString()
  {
    return getDisplayName();
  }
  
  public String getUniqueId() {
    return ("menu_item_" + getName() + "_" + getId()).replaceAll("\\s+", "_");
  }
  



  public TicketItem convertToTicketItem(Ticket ticket, double itemQuantity)
  {
    return convertToTicketItem(ticket, itemQuantity, false);
  }
  
  public TicketItem convertToTicketItem(Ticket ticket, double itemQuantity, boolean hasComboModifiers) {
    TicketItem ticketItem = null;
    MenuItem menuItem = this;
    boolean isComboItem = isComboItem().booleanValue();
    Terminal terminal = Application.getInstance().getTerminal();
    Department department = terminal.getDepartment();
    SalesArea salesArea = null;
    OrderType orderType = null;
    String customerId = null;
    CustomerGroup customerGroup = null;
    
    if (isVariant().booleanValue()) {
      menuItem = getParentMenuItem();
      isComboItem = menuItem.isComboItem().booleanValue();
    }
    if (isComboItem) {
      ticketItem = new ComboTicketItem();
    }
    else if ((menuItem.isPizzaType().booleanValue()) || (menuItem.hasModifiers()) || (hasComboModifiers)) {
      ticketItem = new ModifiableTicketItem();
    }
    else {
      ticketItem = new TicketItem();
    }
    ticketItem.setTaxIncluded(Boolean.valueOf(Application.getInstance().isPriceIncludesTax()));
    if (ticket != null) {
      orderType = ticket.getOrderType();
      salesArea = ticket.getSalesArea();
      customerId = ticket.getCustomerId();
    }
    if ((orderType != null) && (orderType.isEnableCourse().booleanValue())) {
      ticketItem.setCourseId(menuItem.getCourseOrganizeId());
    }
    if (customerId != null) {
      Customer customer = DataProvider.get().getCustomer(customerId);
      customerGroup = customer.getCustomerGroup();
    }
    
    ticketItem.setMenuItemId(getId());
    ticketItem.setMenuItem(this);
    
    ticketItem.setComboItem(menuItem.isComboItem());
    ticketItem.setPizzaType(isPizzaType());
    ticketItem.setFractionalUnit(menuItem.isFractionalUnit());
    
    if (menuItem.isFractionalUnit().booleanValue()) {
      ticketItem.setQuantity(Double.valueOf(itemQuantity));
      ticketItem.setUnitName(menuItem.getUnitName());
    }
    else {
      ticketItem.setQuantity(Double.valueOf(itemQuantity));
    }
    
    ticketItem.setName(getDisplayName());
    ticketItem.setGroupId(menuItem.getMenuGroupId());
    ticketItem.setGroupName(menuItem.getMenuGroupName());
    ticketItem.setCategoryName(menuItem.getMenuCategoryName());
    ticketItem.setCategoryId(menuItem.getMenuCategoryId());
    ticketItem.setServiceChargeApplicable(menuItem.isServiceChargeApplicable());
    ticketItem.setServiceChargeRate(menuItem.getServiceCharge());
    ticketItem.setTaxOnServiceCharge(menuItem.isTaxOnServiceCharge());
    
    if (priceRulePrice == null) {
      priceRulePrice = Double.valueOf(DataProvider.get().getPriceFromPriceRule(this, ticket.getOrderType(), department, salesArea, customerGroup));
    }
    if (isVariant().booleanValue()) {
      ticketItem.setUnitPrice(Double.valueOf(priceRulePrice.doubleValue() + getParentMenuItem().getPrice().doubleValue()));
    }
    else {
      ticketItem.setUnitPrice(priceRulePrice);
    }
    ticketItem.setUnitCost(getCost());
    ticketItem.setInventoryItem(menuItem.isInventoryItem());
    ticketItem.setTicket(ticket);
    
    ticketItem.setShouldPrintToKitchen(menuItem.isShouldPrintToKitchen());
    ticketItem.setBeverage(menuItem.isBeverage());
    ticketItem.setPrinterGroup(menuItem.getPrinterGroup());
    
    setItemTaxes(ticketItem, menuItem.getTaxGroup(), ticket.getOrderType());
    

    ticketItem.setShouldPrintToKitchen(menuItem.isShouldPrintToKitchen());
    ticketItem.setPrintKitchenSticker(menuItem.isPrintKitchenSticker());
    
    List<Discount> discountList = menuItem.getDiscounts();
    if (discountList != null) {
      for (Discount discount : discountList) {
        if (discount.isAutoApply().booleanValue()) {
          TicketItemDiscount ticketItemDiscount = convertToTicketItemDiscount(discount, ticketItem);
          ticketItem.addTodiscounts(ticketItemDiscount);
        }
      }
    }
    
    return ticketItem;
  }
  



  public String getCourseOrganizeId()
  {
    String courseId = super.getCourseId();
    if (StringUtils.isEmpty(courseId)) {
      courseId = DataProvider.get().getDefaultCourseId();
    }
    return courseId;
  }
  
  public static TicketItemDiscount convertToTicketItemDiscount(Discount discount, TicketItem ticketItem) {
    TicketItemDiscount ticketItemDiscount = new TicketItemDiscount();
    ticketItemDiscount.setDiscountId(discount.getId());
    ticketItemDiscount.setAutoApply(discount.isAutoApply());
    ticketItemDiscount.setName(discount.getName());
    ticketItemDiscount.setType(discount.getType());
    ticketItemDiscount.setMinimumAmount(discount.getMinimumBuy());
    ticketItemDiscount.setValue(discount.getValue());
    ticketItemDiscount.setCouponQuantity(Double.valueOf(1.0D));
    ticketItemDiscount.setTicketItem(ticketItem);
    return ticketItemDiscount;
  }
  
  public boolean hasModifiers()
  {
    return super.isHasModifiers().booleanValue();
  }
  
  public boolean hasMandatoryModifiers() {
    return super.isHasMandatoryModifiers().booleanValue();
  }
  
  public boolean hasAutoShowGroup() {
    List<MenuItemModifierSpec> modiferGroups = getMenuItemModiferSpecs();
    if ((modiferGroups == null) || (modiferGroups.size() == 0)) {
      return false;
    }
    
    for (MenuItemModifierSpec menuItemModifierGroup : modiferGroups) {
      if (menuItemModifierGroup.isAutoShow().booleanValue()) {
        return true;
      }
    }
    
    return false;
  }
  
  private static TaxGroup getOutletTaxGroup() {
    Outlet outlet = Application.getOutlet();
    if (outlet != null) {
      TaxGroup outletTaxGroup = outlet.getTaxGroup();
      return outletTaxGroup;
    }
    return null;
  }
  
  public static void setItemTaxes(TicketItem ticketItem, TaxGroup itemTaxGroup, OrderType orderType) {
    List<TicketItemTax> ticketItemTaxes = new ArrayList();
    boolean isFound; if (ticketItem.getTicket() != null) {
      String forHereToGo = ticketItem.getTicket().getProperty("ticket.ForHereToGo");
      isFound = forHereToGo != null;
      if (isFound) {
        if (forHereToGo.equals("FOR HERE")) {
          itemTaxGroup = orderType.getForHereTaxGroup();
        }
        else if (forHereToGo.equals("TO GO")) {
          itemTaxGroup = orderType.getToGoTaxGroup();
        }
      }
    }
    
    if (itemTaxGroup == null) {
      TaxGroup orderTypeTaxGroup = orderType.getDefaultTaxGroup();
      if (orderTypeTaxGroup != null) {
        itemTaxGroup = orderTypeTaxGroup;
      }
      else {
        itemTaxGroup = getOutletTaxGroup();
      }
    }
    if (itemTaxGroup == null) {
      return;
    }
    
    List<Tax> taxes = itemTaxGroup.getTaxes();
    if (taxes != null) {
      for (Tax tax : taxes) {
        TicketItemTax ticketItemTax = new TicketItemTax();
        ticketItemTax.setId(tax.getId());
        ticketItemTax.setName(tax.getName());
        ticketItemTax.setRate(tax.getRate());
        ticketItemTaxes.add(ticketItemTax);
      }
      ticketItem.setTaxes(ticketItemTaxes);
    }
  }
  
  public String getStringWithUnderScore(String orderType, String additionalString)
  {
    orderType = orderType.replaceAll(" ", "_");
    
    return orderType + additionalString;
  }
  
  public Set<MenuItemSize> getSizes() {
    Set<MenuItemSize> sizes = new HashSet();
    
    List<PizzaPrice> priceList = getPizzaPriceList();
    if (priceList != null) {
      for (PizzaPrice pizzaPrice : priceList) {
        sizes.add(pizzaPrice.getSize());
      }
    }
    
    return sizes;
  }
  
  public Set<PizzaCrust> getCrustsForSize(MenuItemSize size) {
    Set<PizzaCrust> crusts = new HashSet();
    
    List<PizzaPrice> priceList = getPizzaPriceList();
    if (priceList != null) {
      for (PizzaPrice pizzaPrice : priceList) {
        if (size.equals(pizzaPrice.getSize())) {
          crusts.add(pizzaPrice.getCrust());
        }
      }
    }
    
    return crusts;
  }
  
  public Set<PizzaPrice> getAvailablePrices(MenuItemSize size) {
    Set<PizzaPrice> prices = new HashSet();
    
    List<PizzaPrice> priceList = getPizzaPriceList();
    if (priceList != null) {
      for (PizzaPrice pizzaPrice : priceList) {
        if (size.equals(pizzaPrice.getSize())) {
          prices.add(pizzaPrice);
        }
      }
    }
    
    return prices;
  }
  
  public List<IUnit> getUnits() {
    List<IUnit> units = new ArrayList();
    InventoryUnit unit = getUnit();
    List<InventoryUnit> groupUnits; if (unit != null) {
      groupUnits = unit.getUnitGroup().getUnits();
      if (groupUnits != null) {
        units.addAll(groupUnits);
      }
    }
    if (getStockUnits() != null) {
      for (InventoryStockUnit stockUnit : getStockUnits()) {
        units.add(stockUnit.getPackagingUnit());
      }
    }
    return units;
  }
  
  public Double getCost(IUnit selectedUnit) {
    if (selectedUnit == null) {
      return getCost();
    }
    if ((selectedUnit instanceof InventoryUnit)) {
      InventoryUnit inventoryUnit = (InventoryUnit)selectedUnit;
      return Double.valueOf(getCost().doubleValue() * getUnit().getConversionRate().doubleValue() / inventoryUnit.getBaseUnitConversionValue());
    }
    PackagingUnit stockUnit;
    if ((selectedUnit instanceof PackagingUnit)) {
      stockUnit = (PackagingUnit)selectedUnit;
      for (InventoryStockUnit itemStockUnit : getStockUnits()) {
        if (stockUnit.getCode().equals(itemStockUnit.getPackagingUnit().getCode())) {
          return Double.valueOf(itemStockUnit.getConversionValue().doubleValue() * getBaseUnitQuantity(itemStockUnit.getUnit().getUniqueCode()) * getCost().doubleValue());
        }
      }
    }
    
    return getCost();
  }
  
  public double getBaseUnitQuantity(String unit) {
    if ((getUnit() == null) || (getUnit().getCode().equals(unit))) {
      return 1.0D;
    }
    if (getStockUnits() != null) {
      for (InventoryStockUnit packUnit : getStockUnits()) {
        if (packUnit.getPackagingUnit().getCode().equals(unit)) {
          return packUnit.getConversionValue().doubleValue() * getBaseUnitQuantity(packUnit.getUnit().getUniqueCode());
        }
      }
    }
    for (InventoryUnit groupUnit : getUnit().getUnitGroup().getUnits()) {
      if (groupUnit.getCode().equals(unit)) {
        return getUnit().getConversionRate().doubleValue() / groupUnit.getConversionRate().doubleValue();
      }
    }
    return 1.0D;
  }
  


  public Double getAverageUnitPurchasePrice()
  {
    Double avgprice = super.getAverageUnitPurchasePrice();
    if (avgprice.doubleValue() == 0.0D)
      return getCost();
    return avgprice;
  }
  
  public String getSku()
  {
    String sku2 = super.getSku();
    if (StringUtils.isEmpty(sku2)) {
      return super.getBarcode();
    }
    return sku2;
  }
  
  public Double getAvailableUnit() {
    if (stockStatus != null)
      return stockStatus.getAvailableUnit();
    stockStatus = getStockStatus();
    return Double.valueOf(stockStatus == null ? 0.0D : stockStatus.getAvailableUnit().doubleValue());
  }
  
  public Double getUnitOnHand() {
    if (stockStatus != null) {
      return stockStatus.getUnitOnHand();
    }
    stockStatus = getStockStatus();
    return Double.valueOf(stockStatus == null ? 0.0D : stockStatus.getUnitOnHand().doubleValue());
  }
  
  public void setUnitOnHand(Double unitOnHand) {
    if (stockStatus == null) {
      stockStatus = new MenuItemInventoryStatus();
      stockStatus.setId(getId());
    }
    stockStatus.setUnitOnHand(unitOnHand);
  }
  
  public void setAvailableUnit(Double availableUnit) {
    if (stockStatus == null) {
      stockStatus = new MenuItemInventoryStatus();
      stockStatus.setId(getId());
    }
    stockStatus.setAvailableUnit(availableUnit);
  }
  
  public Double getRetailPrice() {
    return getPrice();
  }
  
  @Deprecated
  public MenuGroup getParent() {
    String groupId = getMenuGroupId();
    if (StringUtils.isNotEmpty(groupId)) {
      return MenuGroupDAO.getInstance().get(groupId);
    }
    return null;
  }
  
  public void setParent(MenuGroup menuGroup) {
    setMenuGroup(menuGroup);
  }
  
  public void setMenuGroup(MenuGroup menuGroup) {
    if (menuGroup == null) {
      setMenuGroupId(null);
      setMenuGroupName(null);
      setMenuCategoryId(null);
      setMenuCategoryName(null);
    }
    else {
      setMenuGroupId(menuGroup.getId());
      setMenuGroupName(menuGroup.getDisplayName());
      setMenuCategoryId(menuGroup.getMenuCategoryId());
      setMenuCategoryName(menuGroup.getMenuCategoryName());
    }
  }
  
  public PrinterGroup getPrinterGroup() {
    return DataProvider.get().getPrinterGroupById(getPrinterGroupId());
  }
  
  public void setPrinterGroup(PrinterGroup printerGroup) {
    String printerGroupId = null;
    if (printerGroup != null) {
      printerGroupId = printerGroup.getId();
    }
    super.setPrinterGroupId(printerGroupId);
  }
  
  public InventoryUnit getUnit() {
    return DataProvider.get().getInventoryUnitById(getUnitId());
  }
  
  public void setUnit(InventoryUnit unit) {
    String unitId = null;
    if (unit != null) {
      unitId = unit.getId();
    }
    super.setUnitId(unitId);
  }
  
  public ReportGroup getReportGroup() {
    return DataProvider.get().getReportGroupById(getReportGroupId());
  }
  
  public void setReportGroup(ReportGroup reportGroup) {
    String reportGroupId = null;
    if (reportGroup != null) {
      reportGroupId = reportGroup.getId();
    }
    super.setReportGroupId(reportGroupId);
  }
  
  public TaxGroup getTaxGroup() {
    return DataProvider.get().getTaxGroupById(getTaxGroupId());
  }
  
  public void setTaxGroup(TaxGroup taxGroup) {
    String taxGroupId = null;
    if (taxGroup != null) {
      taxGroupId = taxGroup.getId();
    }
    super.setTaxGroupId(taxGroupId);
  }
  

  public void setOrderTypeList(List checkedValues) {}
  
  public MenuItem getParentMenuItem()
  {
    if (parentMenuItem != null) {
      return parentMenuItem;
    }
    String parentMenuItemId = getParentMenuItemId();
    if (StringUtils.isEmpty(parentMenuItemId))
      return null;
    parentMenuItem = MenuItemDAO.getInstance().get(parentMenuItemId);
    return parentMenuItem;
  }
  
  public void setParentMenuItem(MenuItem parentMenuItem) {
    this.parentMenuItem = parentMenuItem;
    String parentMenuItemId = null;
    if (parentMenuItem != null) {
      parentMenuItemId = parentMenuItem.getId();
    }
    super.setParentMenuItemId(parentMenuItemId);
  }
  
  public String getParentMenuItemId()
  {
    if ((parentMenuItem != null) && (StringUtils.isNotEmpty(parentMenuItem.getId())))
      return parentMenuItem.getId();
    return super.getParentMenuItemId();
  }
  
  public MenuItemInventoryStatus getStockStatus() {
    if (!isInventoryItem().booleanValue()) {
      return null;
    }
    if ((stockStatus != null) || (getId() == null))
      return stockStatus;
    stockStatus = DataProvider.get().getMenuItemStockStatus(this);
    return stockStatus;
  }
  
  public void setStockStatus(MenuItemInventoryStatus stockStatus) {
    this.stockStatus = stockStatus;
  }
  
  public void setPropertiesJson(String propertiesJson)
  {
    super.setPropertiesJson(propertiesJson);
    if (StringUtils.isNotEmpty(propertiesJson)) {
      properties = new JSONObject(propertiesJson);
    }
    else {
      properties = new JSONObject();
    }
  }
  
  private void buildPropertis() {
    if (properties != null) {
      return;
    }
    String json = getPropertiesJson();
    if (StringUtils.isEmpty(json)) {
      properties = new JSONObject();
      return;
    }
    properties = new JSONObject(json);
  }
  
  public void addProperty(String key, String value) {
    buildPropertis();
    properties.put(key, value);
    setPropertiesJson(properties.toString());
  }
  
  public String getProperty(String key) {
    buildPropertis();
    if (properties.has(key)) {
      return properties.getString(key);
    }
    return null;
  }
  
  public void removeProperty(String key) {
    buildPropertis();
    properties.remove(key);
  }
  
  @Deprecated
  public Course getCourse() {
    String courseId = getCourseId();
    if (StringUtils.isNotEmpty(courseId)) {
      return CourseDAO.getInstance().get(courseId);
    }
    return null;
  }
  
  public void setCourse(Course course) {
    setCourseId(course == null ? null : course.getId());
  }
  
  public MenuItem clone() {
    MenuItem newMenuItem = new MenuItem();
    
    newMenuItem = (MenuItem)SerializationUtils.clone(this);
    String newName = doDuplicateName(getName());
    newMenuItem.setName(newName);
    newMenuItem.setId(null);
    
    List<InventoryStockUnit> stockUnits = newMenuItem.getStockUnits();
    Iterator localIterator1; if (stockUnits != null)
      for (localIterator1 = stockUnits.iterator(); localIterator1.hasNext();) { stockUnit = (InventoryStockUnit)localIterator1.next();
        stockUnit.setId(null);
        stockUnit.setMenuItem(newMenuItem);
      }
    InventoryStockUnit stockUnit;
    Object comboItems = newMenuItem.getComboItems();
    if (comboItems != null)
      for (stockUnit = ((List)comboItems).iterator(); stockUnit.hasNext();) { comboItem = (ComboItem)stockUnit.next();
        comboItem.setId(null);
      }
    ComboItem comboItem;
    List<ComboGroup> comboGroups = newMenuItem.getComboGroups();
    if (comboGroups != null) {
      for (ComboGroup comboGroup : comboGroups) {
        comboGroup.setId(null);
      }
    }
    copyModifierSpecsToMenuItem(newMenuItem, newMenuItem.getMenuItemModiferSpecs());
    if (newMenuItem.isPizzaType().booleanValue()) {
      List<PizzaPrice> pizzaPriceList = newMenuItem.getPizzaPriceList();
      if (pizzaPriceList != null) {
        List<PizzaPrice> newPriceList = new ArrayList();
        for (PizzaPrice pizzaPrice : pizzaPriceList) {
          PizzaPrice newPrice = (PizzaPrice)SerializationUtils.clone(pizzaPrice);
          newPrice.setId(null);
          newPriceList.add(newPrice);
        }
        newMenuItem.setPizzaPriceList(newPriceList);
      }
    }
    newMenuItem.setVariants(null);
    return newMenuItem;
  }
  
  public static void copyModifierSpecsToMenuItem(MenuItem newMenuItem, List<MenuItemModifierSpec> menuItemModifierSpecs) {
    List<MenuItemModifierSpec> newSpecs = new ArrayList();
    if (menuItemModifierSpecs != null) {
      for (MenuItemModifierSpec menuModifierSpec : menuItemModifierSpecs) {
        menuModifierSpec.setId(null);
        Set<MenuItemModifierPage> specPages = menuModifierSpec.getModifierPages();
        Set<MenuItemModifierPage> newPages = new HashSet();
        if (specPages != null) {
          for (MenuItemModifierPage menuItemModifierPage : specPages) {
            List<MenuItemModifierPageItem> newPageItems = new ArrayList();
            menuItemModifierPage.setId(null);
            menuItemModifierPage.setModifierSpecId(menuModifierSpec.getId());
            List<MenuItemModifierPageItem> pageItems = menuItemModifierPage.getPageItems();
            if (pageItems != null) {
              for (MenuItemModifierPageItem menuItemModifierPageItem : pageItems) {
                menuItemModifierPageItem.setId(null);
                menuItemModifierPageItem.setParentPage(menuItemModifierPage);
                newPageItems.add(menuItemModifierPageItem);
              }
            }
            menuItemModifierPage.setPageItems(newPageItems);
            newPages.add(menuItemModifierPage);
          }
        }
        menuModifierSpec.setModifierPages(newPages);
        newSpecs.add(menuModifierSpec);
      }
    }
    newMenuItem.setMenuItemModiferSpecs(newSpecs);
  }
  
  private static String doDuplicateName(String existingName) {
    String newName = new String();
    int lastIndexOf = existingName.lastIndexOf(" ");
    if (lastIndexOf == -1) {
      newName = existingName + " 1";
    }
    else {
      String processName = existingName.substring(lastIndexOf + 1, existingName.length());
      if (StringUtils.isNumeric(processName)) {
        Integer count = Integer.valueOf(processName);
        count = Integer.valueOf(count.intValue() + 1);
        newName = existingName.replace(processName, String.valueOf(count));
      }
      else {
        newName = existingName + " 1";
      }
    }
    return newName;
  }
  
  public String getVendorNames() {
    return vendorNames;
  }
  
  public void setVendorNames(String vendorName) {
    vendorNames = vendorName;
  }
}
