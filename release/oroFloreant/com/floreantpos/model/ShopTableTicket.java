package com.floreantpos.model;

import com.floreantpos.model.base.BaseShopTableTicket;
import com.google.gson.Gson;
import com.google.gson.JsonElement;




public class ShopTableTicket
  extends BaseShopTableTicket
{
  private static final long serialVersionUID = 1L;
  
  public ShopTableTicket() {}
  
  public JsonElement toJson()
  {
    Gson gson = new Gson();
    return gson.toJsonTree(this);
  }
}
