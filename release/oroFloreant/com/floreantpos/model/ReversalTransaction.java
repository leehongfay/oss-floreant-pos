package com.floreantpos.model;

import com.floreantpos.model.base.BaseReversalTransaction;





public class ReversalTransaction
  extends BaseReversalTransaction
{
  private static final long serialVersionUID = 1L;
  
  public ReversalTransaction() {}
  
  public ReversalTransaction(String id)
  {
    super(id);
  }
  






  public ReversalTransaction(String id, String transactionType, String paymentType)
  {
    super(id, transactionType, paymentType);
  }
}
