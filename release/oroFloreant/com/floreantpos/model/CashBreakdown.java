package com.floreantpos.model;

import com.floreantpos.model.base.BaseCashBreakdown;
import com.floreantpos.model.dao.CashDrawerDAO;
import com.floreantpos.model.util.DataProvider;
import org.apache.commons.lang.StringUtils;

public class CashBreakdown extends BaseCashBreakdown
{
  private static final long serialVersionUID = 1L;
  
  public CashBreakdown() {}
  
  public Currency getCurrency()
  {
    return DataProvider.get().getCurrency(getCurrencyId());
  }
  
  public void setCurrency(Currency currency) {
    if (currency == null) {
      setCurrencyId(null);
    }
    else {
      setCurrencyId(currency.getId());
    }
  }
  
  public String getCurrencyName() {
    return getCurrency().getName();
  }
  
  public void setCurrencyName(String currencyName) {}
  
  public CashDrawer getCashDrawer()
  {
    String cashDrawerId = getCashDrawerId();
    if (StringUtils.isNotEmpty(cashDrawerId)) {
      return CashDrawerDAO.getInstance().get(cashDrawerId);
    }
    return null;
  }
  
  public void setCashDrawer(CashDrawer cashDrawer) {
    if (cashDrawer != null) {
      setCashDrawerId(cashDrawer.getId());
    }
    else {
      setCashDrawerId(null);
    }
  }
  
  public Double getBalance() {
    CashDrawer cashDrawer = getCashDrawer();
    return (cashDrawer == null) || (cashDrawer.getReportTime() == null) ? super.getBalance() : super.getTotalAmount();
  }
}
