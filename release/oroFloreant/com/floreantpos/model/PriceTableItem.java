package com.floreantpos.model;

import com.floreantpos.model.base.BasePriceTableItem;
import com.floreantpos.model.dao.MenuItemDAO;

public class PriceTableItem extends BasePriceTableItem
{
  private static final long serialVersionUID = 1L;
  private MenuItem menuItem;
  private String itemName;
  private double regularPrice;
  
  public PriceTableItem() {}
  
  public PriceTableItem(String id)
  {
    super(id);
  }
  




  public MenuItem getMenuItem()
  {
    if (menuItem == null) {
      menuItem = MenuItemDAO.getInstance().get(getMenuItemId());
    }
    return menuItem;
  }
  
  public void setMenuItem(MenuItem menuItem) {
    this.menuItem = menuItem;
  }
  
  public String getItemName() {
    return getMenuItem().getName();
  }
  
  public double getRegularPrice() {
    return getMenuItem().getPrice().doubleValue();
  }
}
