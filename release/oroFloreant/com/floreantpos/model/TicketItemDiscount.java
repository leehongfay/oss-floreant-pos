package com.floreantpos.model;

import com.floreantpos.model.base.BaseTicketItemDiscount;
import com.floreantpos.model.ext.KitchenStatus;
import com.floreantpos.util.DiscountUtil;
import com.floreantpos.util.NumberUtil;
import java.math.BigDecimal;
import java.math.RoundingMode;
import org.json.JSONObject;

public class TicketItemDiscount
  extends BaseTicketItemDiscount
  implements ITicketItem
{
  private static final long serialVersionUID = 1L;
  private double discountPercentage;
  private int tableRowNum;
  private String ticketItemId;
  
  public TicketItemDiscount() {}
  
  public TicketItemDiscount(String id)
  {
    super(id);
  }
  

  public TicketItemDiscount(String id, String name, double quantity, double value, int type)
  {
    setId(id);
    setDiscountId(id);
    setName(name);
    setCouponQuantity(Double.valueOf(quantity));
    setValue(Double.valueOf(value));
    setType(Integer.valueOf(type));
  }
  

  public TicketItemDiscount(TicketItemDiscount fromDiscount)
  {
    setDiscountId(fromDiscount.getDiscountId());
    setName(fromDiscount.getName());
    setType(fromDiscount.getType());
    setAutoApply(fromDiscount.isAutoApply());
    setCouponQuantity(fromDiscount.getCouponQuantity());
    setMinimumAmount(fromDiscount.getMinimumAmount());
    setValue(fromDiscount.getValue());
    setAmount(fromDiscount.getAmount());
  }
  

  public double calculateDiscount(double subtotalAmount)
  {
    double quantity = 1.0D;
    double couponQuantity = getCouponQuantity().doubleValue();
    if (getMinimumAmount().doubleValue() > 1.0D) {
      quantity = Math.floor(getTicketItem().getQuantity().doubleValue() / getMinimumAmount().doubleValue());
      if (couponQuantity >= quantity) {
        couponQuantity = quantity;
      }
    }
    double discountAmount = DiscountUtil.calculateDiscountAmount(this, subtotalAmount).doubleValue() * couponQuantity;
    if ((discountAmount > 0.0D) && (discountAmount > subtotalAmount))
      discountAmount = subtotalAmount;
    setAmount(Double.valueOf(discountAmount));
    return discountAmount;
  }
  
  public BigDecimal calculateDiscount(BigDecimal subtotalAmount) {
    BigDecimal quantity = NumberUtil.convertToBigDecimal("1");
    BigDecimal couponQuantity = NumberUtil.convertToBigDecimal(getCouponQuantity().doubleValue());
    BigDecimal minAmount = NumberUtil.convertToBigDecimal(getMinimumAmount().doubleValue());
    
    if (minAmount.compareTo(BigDecimal.ONE) > 0) {
      quantity = NumberUtil.convertToBigDecimal(getTicketItem().getQuantity().doubleValue()).divide(minAmount, 4, RoundingMode.HALF_DOWN);
      if (couponQuantity.compareTo(quantity) >= 0) {
        couponQuantity = quantity;
      }
    }
    BigDecimal discountAmount = DiscountUtil.calculateDiscountAmount(this, subtotalAmount).multiply(couponQuantity);
    if ((discountAmount.compareTo(BigDecimal.ZERO) > 0) && (discountAmount.compareTo(subtotalAmount) > 0))
      discountAmount = subtotalAmount;
    setAmount(Double.valueOf(discountAmount.doubleValue()));
    return discountAmount;
  }
  
  public void setTableRowNum(int tableRowNum) {
    this.tableRowNum = tableRowNum;
  }
  
  public int getTableRowNum() {
    return tableRowNum;
  }
  
  public String getItemCode()
  {
    return "";
  }
  
  public String toString()
  {
    return getName();
  }
  
  public String getNameDisplay()
  {
    double couponQuantity = getCouponQuantity().doubleValue();
    String display;
    String display; if (couponQuantity > 1.0D) {
      display = NumberUtil.trimDecilamIfNotNeeded(Double.valueOf(couponQuantity)) + "x " + getName();
    }
    else {
      display = getName().trim();
    }
    return display;
  }
  

  public boolean canAddCookingInstruction()
  {
    return false;
  }
  
  public boolean canAddDiscount()
  {
    return false;
  }
  
  public boolean canVoid()
  {
    return false;
  }
  
  public boolean canAddAdOn()
  {
    return false;
  }
  
  public Boolean isPrintedToKitchen()
  {
    return Boolean.valueOf(false);
  }
  
  public Double getUnitPriceDisplay()
  {
    return null;
  }
  
  public String getItemQuantityDisplay()
  {
    return null;
  }
  
  public Double getTaxAmountWithoutModifiersDisplay()
  {
    return null;
  }
  
  public Double getTotalAmountWithoutModifiersDisplay()
  {
    return null;
  }
  
  public Double getSubTotalAmountWithoutModifiersDisplay()
  {
    return null;
  }
  


  public void setDiscountAmount(Double amount) {}
  

  public Double getDiscountAmount()
  {
    return null;
  }
  
  public KitchenStatus getKitchenStatusValue()
  {
    return null;
  }
  
  public Double getSubTotalAmountDisplay()
  {
    if (getTicketItem() != null) {
      return getTicketItem().getDiscountAmount();
    }
    
    double discount = getAmount().doubleValue();
    if (discount <= 0.0D)
      return null;
    return Double.valueOf(-discount);
  }
  
  public double getDiscountPercentage() {
    return discountPercentage;
  }
  
  public void setDiscountPercentage(double discountPercentage) {
    this.discountPercentage = discountPercentage;
  }
  
  public JSONObject toJson() {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put(PROP_DISCOUNT_ID, getDiscountId());
    jsonObject.put(PROP_NAME, getName());
    jsonObject.put(PROP_TYPE, getType());
    jsonObject.put(PROP_AUTO_APPLY, isAutoApply());
    jsonObject.put(PROP_COUPON_QUANTITY, getCouponQuantity());
    jsonObject.put(PROP_MINIMUM_AMOUNT, getMinimumAmount());
    jsonObject.put(PROP_VALUE, getValue());
    jsonObject.put(PROP_AMOUNT, getAmount());
    return jsonObject;
  }
  
  public String getTicketItemId() {
    return ticketItemId;
  }
  
  public void setTicketItemId(String ticketItemId) {
    this.ticketItemId = ticketItemId;
  }
  


  public boolean isSaved()
  {
    return getTicketItem() == null ? false : getTicketItem().isSaved();
  }
}
