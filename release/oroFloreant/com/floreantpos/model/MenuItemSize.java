package com.floreantpos.model;

import com.floreantpos.model.base.BaseMenuItemSize;
import org.apache.commons.lang.StringUtils;






public class MenuItemSize
  extends BaseMenuItemSize
{
  private static final long serialVersionUID = 1L;
  
  public MenuItemSize() {}
  
  public MenuItemSize(String id)
  {
    super(id);
  }
  


  public String getTranslatedName()
  {
    String translatedName = super.getTranslatedName();
    if (StringUtils.isEmpty(translatedName)) {
      return getName();
    }
    
    return translatedName;
  }
  
  public String toString()
  {
    return getName();
  }
}
