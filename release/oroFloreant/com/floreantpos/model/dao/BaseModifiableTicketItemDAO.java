package com.floreantpos.model.dao;

import com.floreantpos.model.ModifiableTicketItem;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseModifiableTicketItemDAO
  extends _RootDAO
{
  public static ModifiableTicketItemDAO instance;
  
  public BaseModifiableTicketItemDAO() {}
  
  public static ModifiableTicketItemDAO getInstance()
  {
    if (null == instance) instance = new ModifiableTicketItemDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return ModifiableTicketItem.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public ModifiableTicketItem cast(Object object)
  {
    return (ModifiableTicketItem)object;
  }
  
  public ModifiableTicketItem get(String key) throws HibernateException
  {
    return (ModifiableTicketItem)get(getReferenceClass(), key);
  }
  
  public ModifiableTicketItem get(String key, Session s) throws HibernateException
  {
    return (ModifiableTicketItem)get(getReferenceClass(), key, s);
  }
  
  public ModifiableTicketItem load(String key) throws HibernateException
  {
    return (ModifiableTicketItem)load(getReferenceClass(), key);
  }
  
  public ModifiableTicketItem load(String key, Session s) throws HibernateException
  {
    return (ModifiableTicketItem)load(getReferenceClass(), key, s);
  }
  
  public ModifiableTicketItem loadInitialize(String key, Session s) throws HibernateException
  {
    ModifiableTicketItem obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<ModifiableTicketItem> findAll()
  {
    return super.findAll();
  }
  


  public List<ModifiableTicketItem> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<ModifiableTicketItem> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(ModifiableTicketItem modifiableTicketItem)
    throws HibernateException
  {
    return (String)super.save(modifiableTicketItem);
  }
  







  public String save(ModifiableTicketItem modifiableTicketItem, Session s)
    throws HibernateException
  {
    return (String)save(modifiableTicketItem, s);
  }
  





  public void saveOrUpdate(ModifiableTicketItem modifiableTicketItem)
    throws HibernateException
  {
    saveOrUpdate(modifiableTicketItem);
  }
  







  public void saveOrUpdate(ModifiableTicketItem modifiableTicketItem, Session s)
    throws HibernateException
  {
    saveOrUpdate(modifiableTicketItem, s);
  }
  




  public void update(ModifiableTicketItem modifiableTicketItem)
    throws HibernateException
  {
    update(modifiableTicketItem);
  }
  






  public void update(ModifiableTicketItem modifiableTicketItem, Session s)
    throws HibernateException
  {
    update(modifiableTicketItem, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(ModifiableTicketItem modifiableTicketItem)
    throws HibernateException
  {
    delete(modifiableTicketItem);
  }
  






  public void delete(ModifiableTicketItem modifiableTicketItem, Session s)
    throws HibernateException
  {
    delete(modifiableTicketItem, s);
  }
  









  public void refresh(ModifiableTicketItem modifiableTicketItem, Session s)
    throws HibernateException
  {
    refresh(modifiableTicketItem, s);
  }
}
