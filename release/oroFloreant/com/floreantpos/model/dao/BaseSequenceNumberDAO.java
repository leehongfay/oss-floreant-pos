package com.floreantpos.model.dao;

import com.floreantpos.model.SequenceNumber;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseSequenceNumberDAO
  extends _RootDAO
{
  public static SequenceNumberDAO instance;
  
  public BaseSequenceNumberDAO() {}
  
  public static SequenceNumberDAO getInstance()
  {
    if (null == instance) instance = new SequenceNumberDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return SequenceNumber.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public SequenceNumber cast(Object object)
  {
    return (SequenceNumber)object;
  }
  
  public SequenceNumber get(String key) throws HibernateException
  {
    return (SequenceNumber)get(getReferenceClass(), key);
  }
  
  public SequenceNumber get(String key, Session s) throws HibernateException
  {
    return (SequenceNumber)get(getReferenceClass(), key, s);
  }
  
  public SequenceNumber load(String key) throws HibernateException
  {
    return (SequenceNumber)load(getReferenceClass(), key);
  }
  
  public SequenceNumber load(String key, Session s) throws HibernateException
  {
    return (SequenceNumber)load(getReferenceClass(), key, s);
  }
  
  public SequenceNumber loadInitialize(String key, Session s) throws HibernateException
  {
    SequenceNumber obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<SequenceNumber> findAll()
  {
    return super.findAll();
  }
  


  public List<SequenceNumber> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<SequenceNumber> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(SequenceNumber sequenceNumber)
    throws HibernateException
  {
    return (String)super.save(sequenceNumber);
  }
  







  public String save(SequenceNumber sequenceNumber, Session s)
    throws HibernateException
  {
    return (String)save(sequenceNumber, s);
  }
  





  public void saveOrUpdate(SequenceNumber sequenceNumber)
    throws HibernateException
  {
    saveOrUpdate(sequenceNumber);
  }
  







  public void saveOrUpdate(SequenceNumber sequenceNumber, Session s)
    throws HibernateException
  {
    saveOrUpdate(sequenceNumber, s);
  }
  




  public void update(SequenceNumber sequenceNumber)
    throws HibernateException
  {
    update(sequenceNumber);
  }
  






  public void update(SequenceNumber sequenceNumber, Session s)
    throws HibernateException
  {
    update(sequenceNumber, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(SequenceNumber sequenceNumber)
    throws HibernateException
  {
    delete(sequenceNumber);
  }
  






  public void delete(SequenceNumber sequenceNumber, Session s)
    throws HibernateException
  {
    delete(sequenceNumber, s);
  }
  









  public void refresh(SequenceNumber sequenceNumber, Session s)
    throws HibernateException
  {
    refresh(sequenceNumber, s);
  }
}
