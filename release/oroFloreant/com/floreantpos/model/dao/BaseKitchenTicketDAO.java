package com.floreantpos.model.dao;

import com.floreantpos.model.KitchenTicket;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseKitchenTicketDAO
  extends _RootDAO
{
  public static KitchenTicketDAO instance;
  
  public BaseKitchenTicketDAO() {}
  
  public static KitchenTicketDAO getInstance()
  {
    if (null == instance) instance = new KitchenTicketDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return KitchenTicket.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public KitchenTicket cast(Object object)
  {
    return (KitchenTicket)object;
  }
  
  public KitchenTicket get(String key) throws HibernateException
  {
    return (KitchenTicket)get(getReferenceClass(), key);
  }
  
  public KitchenTicket get(String key, Session s) throws HibernateException
  {
    return (KitchenTicket)get(getReferenceClass(), key, s);
  }
  
  public KitchenTicket load(String key) throws HibernateException
  {
    return (KitchenTicket)load(getReferenceClass(), key);
  }
  
  public KitchenTicket load(String key, Session s) throws HibernateException
  {
    return (KitchenTicket)load(getReferenceClass(), key, s);
  }
  
  public KitchenTicket loadInitialize(String key, Session s) throws HibernateException
  {
    KitchenTicket obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<KitchenTicket> findAll()
  {
    return super.findAll();
  }
  


  public List<KitchenTicket> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<KitchenTicket> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(KitchenTicket kitchenTicket)
    throws HibernateException
  {
    return (String)super.save(kitchenTicket);
  }
  







  public String save(KitchenTicket kitchenTicket, Session s)
    throws HibernateException
  {
    return (String)save(kitchenTicket, s);
  }
  





  public void saveOrUpdate(KitchenTicket kitchenTicket)
    throws HibernateException
  {
    saveOrUpdate(kitchenTicket);
  }
  







  public void saveOrUpdate(KitchenTicket kitchenTicket, Session s)
    throws HibernateException
  {
    saveOrUpdate(kitchenTicket, s);
  }
  




  public void update(KitchenTicket kitchenTicket)
    throws HibernateException
  {
    update(kitchenTicket);
  }
  






  public void update(KitchenTicket kitchenTicket, Session s)
    throws HibernateException
  {
    update(kitchenTicket, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(KitchenTicket kitchenTicket)
    throws HibernateException
  {
    delete(kitchenTicket);
  }
  






  public void delete(KitchenTicket kitchenTicket, Session s)
    throws HibernateException
  {
    delete(kitchenTicket, s);
  }
  









  public void refresh(KitchenTicket kitchenTicket, Session s)
    throws HibernateException
  {
    refresh(kitchenTicket, s);
  }
}
