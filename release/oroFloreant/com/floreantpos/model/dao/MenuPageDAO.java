package com.floreantpos.model.dao;

import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuPage;
import com.floreantpos.model.MenuPageItem;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Terminal;
import com.floreantpos.swing.PaginatedListModel;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class MenuPageDAO extends BaseMenuPageDAO
{
  public MenuPageDAO() {}
  
  public void saveOrUpdatePages(List<MenuPage> items)
  {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      for (Iterator iterator = items.iterator(); iterator.hasNext();) {
        MenuPage menuPage = (MenuPage)iterator.next();
        session.saveOrUpdate(menuPage);
      }
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      com.floreantpos.PosLog.error(getClass(), e);
    } finally {
      session.close();
    }
  }
  
  public int getRowCount(Terminal terminal, MenuGroup menuGroup, Object selectedOrderType) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      
      if (menuGroup != null) {
        criteria.add(Restrictions.eq(MenuPage.PROP_MENU_GROUP_ID, menuGroup.getId()));
      }
      criteria.add(Restrictions.eq(MenuPage.PROP_VISIBLE, Boolean.TRUE));
      criteria.setProjection(Projections.rowCount());
      
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        return rowCount.intValue();
      }
    }
    finally {
      closeSession(session);
    }
    return 0;
  }
  
  public List<MenuPage> findByGroup(MenuGroup menuGroup) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(MenuPage.PROP_MENU_GROUP_ID, menuGroup.getId()));
      criteria.addOrder(Order.asc(MenuPage.PROP_SORT_ORDER));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public void loadItems(Terminal terminal, MenuGroup menuGroup, OrderType selectedOrderType, Boolean visible, PaginatedListModel listModel) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.addOrder(Order.asc(MenuPage.PROP_SORT_ORDER));
      if (menuGroup != null) {
        criteria.add(Restrictions.eq(MenuPage.PROP_MENU_GROUP_ID, menuGroup.getId()));
      }
      criteria.add(Restrictions.eq(MenuPage.PROP_VISIBLE, Boolean.TRUE));
      criteria.setFirstResult(listModel.getCurrentRowIndex());
      criteria.setMaxResults(1);
      listModel.setData(criteria.list());
    } finally {
      closeSession(session);
    }
  }
  
  public void deleteAll(List<MenuPage> items) {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      deleteAll(items, session);
      tx.commit();
    }
    catch (Exception e) {
      throw e;
    } finally {
      session.close();
    }
  }
  
  public void deleteAll(List<MenuPage> items, Session session)
  {
    for (Iterator iterator = items.iterator(); iterator.hasNext();) {
      MenuPage menuPage = (MenuPage)iterator.next();
      session.delete(menuPage);
    }
  }
  
  public void addToMenuGroupEndPage(MenuItem mnuItem) {
    if (mnuItem.getMenuGroupId() == null)
      return;
    MenuItemDAO.getInstance().initialize(mnuItem);
    MenuPage page = getLastPage(mnuItem.getMenuGroupId());
    MenuPageItem menuPageItem = new MenuPageItem(null, null, mnuItem, page);
    for (int i = 0; i < page.getRows().intValue(); i++) {
      for (int j = 0; j < page.getCols().intValue(); j++) {
        if (page.getItemForCell(j, i) == null) {
          menuPageItem.setCol(Integer.valueOf(j));
          menuPageItem.setRow(Integer.valueOf(i));
          menuPageItem.setMenuPage(page);
          MenuPageItemDAO.getInstance().saveOrUpdate(page);
          return;
        }
      }
    }
  }
  
  private MenuPage getLastPage(String menuGroupId) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(MenuPage.PROP_MENU_GROUP_ID, menuGroupId));
      criteria.addOrder(Order.asc(MenuPage.PROP_SORT_ORDER));
      
      List list = criteria.list();
      MenuPage page = null;
      if ((list != null) && (!list.isEmpty())) {
        page = (MenuPage)list.get(list.size() - 1);
      }
      List<MenuPageItem> pageItems = null;
      if (page != null)
        pageItems = MenuPageItemDAO.getInstance().getPageItems(page);
      int pageNumber;
      if ((page == null) || ((pageItems != null) && (!pageItems.isEmpty()) && (pageItems.size() == page.getRows().intValue() * page.getCols().intValue()))) {
        page = new MenuPage();
        page.setMenuGroupId(menuGroupId);
        pageNumber = list.size() + 1;
        page.setName("Page " + pageNumber);
        page.setSortOrder(Integer.valueOf(pageNumber));
        page.setCols(Integer.valueOf(5));
        page.setRows(Integer.valueOf(5));
        page.setButtonHeight(Integer.valueOf(120));
        page.setButtonWidth(Integer.valueOf(120));
        page.setVisible(Boolean.valueOf(true));
      }
      return page;
    } finally {
      closeSession(session);
    }
  }
  
  public void initialize(MenuPage menuPage) {
    if (Hibernate.isInitialized(menuPage.getPageItems())) {
      return;
    }
    Session session = null;
    try {
      session = createNewSession();
      session.refresh(menuPage);
      Hibernate.initialize(menuPage.getPageItems());
      
      closeSession(session); } finally { closeSession(session);
    }
  }
  


  public int getRowCount(Terminal terminal, List<String> menuGroupIds, Object selectedOrderType)
  {
    if ((menuGroupIds == null) || (menuGroupIds.size() == 0)) {
      return 0;
    }
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      
      criteria.add(Restrictions.in(MenuPage.PROP_MENU_GROUP_ID, menuGroupIds));
      criteria.add(Restrictions.eq(MenuPage.PROP_VISIBLE, Boolean.TRUE));
      criteria.setProjection(Projections.rowCount());
      
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        return rowCount.intValue();
      }
    }
    finally {
      closeSession(session);
    }
    return 0;
  }
  


  public void loadItems(Terminal terminal, List<String> menuGroupIds, OrderType selectedOrderType, Boolean visible, PaginatedListModel listModel)
  {
    if ((menuGroupIds == null) || (menuGroupIds.size() == 0)) {
      return;
    }
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.addOrder(Order.asc(MenuPage.PROP_SORT_ORDER));
      criteria.add(Restrictions.in(MenuPage.PROP_MENU_GROUP_ID, menuGroupIds));
      criteria.add(Restrictions.eq(MenuPage.PROP_VISIBLE, Boolean.TRUE));
      criteria.setFirstResult(listModel.getCurrentRowIndex());
      criteria.setMaxResults(1);
      listModel.setData(criteria.list());
    } finally {
      closeSession(session);
    }
  }
}
