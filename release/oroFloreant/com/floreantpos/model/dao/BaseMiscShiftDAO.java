package com.floreantpos.model.dao;

import com.floreantpos.model.MiscShift;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseMiscShiftDAO
  extends _RootDAO
{
  public static MiscShiftDAO instance;
  
  public BaseMiscShiftDAO() {}
  
  public static MiscShiftDAO getInstance()
  {
    if (null == instance) instance = new MiscShiftDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return MiscShift.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public MiscShift cast(Object object)
  {
    return (MiscShift)object;
  }
  
  public MiscShift get(String key) throws HibernateException
  {
    return (MiscShift)get(getReferenceClass(), key);
  }
  
  public MiscShift get(String key, Session s) throws HibernateException
  {
    return (MiscShift)get(getReferenceClass(), key, s);
  }
  
  public MiscShift load(String key) throws HibernateException
  {
    return (MiscShift)load(getReferenceClass(), key);
  }
  
  public MiscShift load(String key, Session s) throws HibernateException
  {
    return (MiscShift)load(getReferenceClass(), key, s);
  }
  
  public MiscShift loadInitialize(String key, Session s) throws HibernateException
  {
    MiscShift obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<MiscShift> findAll()
  {
    return super.findAll();
  }
  


  public List<MiscShift> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<MiscShift> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(MiscShift miscShift)
    throws HibernateException
  {
    return (String)super.save(miscShift);
  }
  







  public String save(MiscShift miscShift, Session s)
    throws HibernateException
  {
    return (String)save(miscShift, s);
  }
  





  public void saveOrUpdate(MiscShift miscShift)
    throws HibernateException
  {
    saveOrUpdate(miscShift);
  }
  







  public void saveOrUpdate(MiscShift miscShift, Session s)
    throws HibernateException
  {
    saveOrUpdate(miscShift, s);
  }
  




  public void update(MiscShift miscShift)
    throws HibernateException
  {
    update(miscShift);
  }
  






  public void update(MiscShift miscShift, Session s)
    throws HibernateException
  {
    update(miscShift, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(MiscShift miscShift)
    throws HibernateException
  {
    delete(miscShift);
  }
  






  public void delete(MiscShift miscShift, Session s)
    throws HibernateException
  {
    delete(miscShift, s);
  }
  









  public void refresh(MiscShift miscShift, Session s)
    throws HibernateException
  {
    refresh(miscShift, s);
  }
}
