package com.floreantpos.model.dao;

import com.floreantpos.model.DataUpdateInfo;
import java.util.Date;
import org.hibernate.Session;




















public class DataUpdateInfoDAO
  extends BaseDataUpdateInfoDAO
{
  private static DataUpdateInfo lastUpdateInfo;
  
  public DataUpdateInfoDAO() {}
  
  public static synchronized DataUpdateInfo getLastUpdateInfo()
  {
    try
    {
      if (lastUpdateInfo != null) {
        try {
          getInstance().refresh(lastUpdateInfo);
        } catch (Exception x) {
          lastUpdateInfo = null;
        }
        
        return lastUpdateInfo;
      }
      
      lastUpdateInfo = getInstance().get("1");
      
      if (lastUpdateInfo == null) {
        lastUpdateInfo = new DataUpdateInfo();
        lastUpdateInfo.setLastUpdateTime(new Date());
        
        getInstance().save(lastUpdateInfo);
      }
      
      return lastUpdateInfo;
    }
    catch (Exception e) {}
    return null;
  }
  
  public static synchronized DataUpdateInfo getLastUpdateInfo(Session session)
  {
    try {
      if (lastUpdateInfo != null) {
        try {
          getInstance().refresh(lastUpdateInfo, session);
        } catch (Exception x) {
          lastUpdateInfo = null;
        }
        
        return lastUpdateInfo;
      }
      
      lastUpdateInfo = getInstance().get("1", session);
      
      if (lastUpdateInfo == null) {
        lastUpdateInfo = new DataUpdateInfo();
        lastUpdateInfo.setLastUpdateTime(new Date());
        
        getInstance().save(lastUpdateInfo, session);
      }
      
      return lastUpdateInfo;
    }
    catch (Exception e) {}
    return null;
  }
}
