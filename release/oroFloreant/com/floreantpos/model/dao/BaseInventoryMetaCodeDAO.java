package com.floreantpos.model.dao;

import com.floreantpos.model.InventoryMetaCode;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseInventoryMetaCodeDAO
  extends _RootDAO
{
  public static InventoryMetaCodeDAO instance;
  
  public BaseInventoryMetaCodeDAO() {}
  
  public static InventoryMetaCodeDAO getInstance()
  {
    if (null == instance) instance = new InventoryMetaCodeDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return InventoryMetaCode.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public InventoryMetaCode cast(Object object)
  {
    return (InventoryMetaCode)object;
  }
  
  public InventoryMetaCode get(String key) throws HibernateException
  {
    return (InventoryMetaCode)get(getReferenceClass(), key);
  }
  
  public InventoryMetaCode get(String key, Session s) throws HibernateException
  {
    return (InventoryMetaCode)get(getReferenceClass(), key, s);
  }
  
  public InventoryMetaCode load(String key) throws HibernateException
  {
    return (InventoryMetaCode)load(getReferenceClass(), key);
  }
  
  public InventoryMetaCode load(String key, Session s) throws HibernateException
  {
    return (InventoryMetaCode)load(getReferenceClass(), key, s);
  }
  
  public InventoryMetaCode loadInitialize(String key, Session s) throws HibernateException
  {
    InventoryMetaCode obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<InventoryMetaCode> findAll()
  {
    return super.findAll();
  }
  


  public List<InventoryMetaCode> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<InventoryMetaCode> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(InventoryMetaCode inventoryMetaCode)
    throws HibernateException
  {
    return (String)super.save(inventoryMetaCode);
  }
  







  public String save(InventoryMetaCode inventoryMetaCode, Session s)
    throws HibernateException
  {
    return (String)save(inventoryMetaCode, s);
  }
  





  public void saveOrUpdate(InventoryMetaCode inventoryMetaCode)
    throws HibernateException
  {
    saveOrUpdate(inventoryMetaCode);
  }
  







  public void saveOrUpdate(InventoryMetaCode inventoryMetaCode, Session s)
    throws HibernateException
  {
    saveOrUpdate(inventoryMetaCode, s);
  }
  




  public void update(InventoryMetaCode inventoryMetaCode)
    throws HibernateException
  {
    update(inventoryMetaCode);
  }
  






  public void update(InventoryMetaCode inventoryMetaCode, Session s)
    throws HibernateException
  {
    update(inventoryMetaCode, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(InventoryMetaCode inventoryMetaCode)
    throws HibernateException
  {
    delete(inventoryMetaCode);
  }
  






  public void delete(InventoryMetaCode inventoryMetaCode, Session s)
    throws HibernateException
  {
    delete(inventoryMetaCode, s);
  }
  









  public void refresh(InventoryMetaCode inventoryMetaCode, Session s)
    throws HibernateException
  {
    refresh(inventoryMetaCode, s);
  }
}
