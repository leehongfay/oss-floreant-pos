package com.floreantpos.model.dao;

import com.floreantpos.model.MenuModifier;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseMenuModifierDAO
  extends _RootDAO
{
  public static MenuModifierDAO instance;
  
  public BaseMenuModifierDAO() {}
  
  public static MenuModifierDAO getInstance()
  {
    if (null == instance) instance = new MenuModifierDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return MenuModifier.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public MenuModifier cast(Object object)
  {
    return (MenuModifier)object;
  }
  
  public MenuModifier get(String key) throws HibernateException
  {
    return (MenuModifier)get(getReferenceClass(), key);
  }
  
  public MenuModifier get(String key, Session s) throws HibernateException
  {
    return (MenuModifier)get(getReferenceClass(), key, s);
  }
  
  public MenuModifier load(String key) throws HibernateException
  {
    return (MenuModifier)load(getReferenceClass(), key);
  }
  
  public MenuModifier load(String key, Session s) throws HibernateException
  {
    return (MenuModifier)load(getReferenceClass(), key, s);
  }
  
  public MenuModifier loadInitialize(String key, Session s) throws HibernateException
  {
    MenuModifier obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<MenuModifier> findAll()
  {
    return super.findAll();
  }
  


  public List<MenuModifier> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<MenuModifier> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(MenuModifier menuModifier)
    throws HibernateException
  {
    return (String)super.save(menuModifier);
  }
  







  public String save(MenuModifier menuModifier, Session s)
    throws HibernateException
  {
    return (String)save(menuModifier, s);
  }
  





  public void saveOrUpdate(MenuModifier menuModifier)
    throws HibernateException
  {
    saveOrUpdate(menuModifier);
  }
  







  public void saveOrUpdate(MenuModifier menuModifier, Session s)
    throws HibernateException
  {
    saveOrUpdate(menuModifier, s);
  }
  




  public void update(MenuModifier menuModifier)
    throws HibernateException
  {
    update(menuModifier);
  }
  






  public void update(MenuModifier menuModifier, Session s)
    throws HibernateException
  {
    update(menuModifier, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(MenuModifier menuModifier)
    throws HibernateException
  {
    delete(menuModifier);
  }
  






  public void delete(MenuModifier menuModifier, Session s)
    throws HibernateException
  {
    delete(menuModifier, s);
  }
  









  public void refresh(MenuModifier menuModifier, Session s)
    throws HibernateException
  {
    refresh(menuModifier, s);
  }
}
