package com.floreantpos.model.dao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.naming.TimeLimitExceededException;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
















public abstract class _BaseRootDAO
{
  protected static Map<String, SessionFactory> sessionFactoryMap;
  protected static SessionFactory sessionFactory;
  protected static ThreadLocal<Map> mappedSessions;
  protected static ThreadLocal<Session> sessions;
  
  public _BaseRootDAO() {}
  
  public static void initialize()
  {
    _RootDAO.initialize((String)null);
  }
  



  public static void initialize(String configFileName)
  {
    _RootDAO.initialize(configFileName, _RootDAO.getNewConfiguration(null));
  }
  
  public static void initialize(String configFileName, Configuration configuration) {
    if ((null == configFileName) && (null != sessionFactory))
      return;
    if ((null != sessionFactoryMap) && (null != sessionFactoryMap.get(configFileName))) {
      return;
    }
    if (null == configFileName) {
      configuration.configure();
      _RootDAO.setSessionFactory(configuration.buildSessionFactory());
    }
    else {
      configuration.configure(configFileName);
      _RootDAO.setSessionFactory(configFileName, configuration.buildSessionFactory());
    }
  }
  



  public static void setSessionFactory(SessionFactory sessionFactory)
  {
    setSessionFactory((String)null, sessionFactory);
  }
  


  public static void setSessionFactory(String configFileName, SessionFactory sf)
  {
    if (null == configFileName) {
      sessionFactory = sf;
    }
    else {
      if (null == sessionFactoryMap)
        sessionFactoryMap = new HashMap();
      sessionFactoryMap.put(configFileName, sessionFactory);
    }
  }
  




  protected SessionFactory getSessionFactory()
  {
    return getSessionFactory(getConfigurationFileName());
  }
  
  protected SessionFactory getSessionFactory(String configFile) {
    if (null == configFile) {
      if (null == sessionFactory) {
        throw new RuntimeException("The session factory has not been initialized (or an error occured during initialization)");
      }
      return sessionFactory;
    }
    
    if (null == sessionFactoryMap) {
      throw new RuntimeException("The session factory for '" + configFile + "' has not been initialized (or an error occured during initialization)");
    }
    SessionFactory sf = (SessionFactory)sessionFactoryMap.get(configFile);
    if (null == sf) {
      throw new RuntimeException("The session factory for '" + configFile + "' has not been initialized (or an error occured during initialization)");
    }
    
    return sf;
  }
  





  public Session getSession()
  {
    return getSession(getConfigurationFileName(), false);
  }
  



  public Session createNewSession()
  {
    return getSession(getConfigurationFileName(), true);
  }
  




  protected Session getSession(String configFile, boolean createNew)
  {
    if (createNew) {
      return getSessionFactory(configFile).openSession();
    }
    
    if (null == configFile) {
      if (null == sessions)
        sessions = new ThreadLocal();
      Session session = (Session)sessions.get();
      if ((null == session) || (!session.isOpen())) {
        session = getSessionFactory(null).openSession();
        sessions.set(session);
      }
      return session;
    }
    
    if (null == mappedSessions)
      mappedSessions = new ThreadLocal();
    Map<String, Session> map = (Map)mappedSessions.get();
    if (null == map) {
      map = new HashMap(1);
      mappedSessions.set(map);
    }
    Session session = (Session)map.get(configFile);
    if ((null == session) || (!session.isOpen())) {
      session = getSessionFactory(configFile).openSession();
      map.put(configFile, session);
    }
    return session;
  }
  




  public static void closeCurrentThreadSessions()
  {
    if (null != sessions) {
      Session session = (Session)sessions.get();
      if ((null != session) && (session.isOpen())) {
        session.close();
        sessions.set(null);
      }
    }
    if (null != mappedSessions) {
      Map<String, Session> map = (Map)mappedSessions.get();
      if (null != map) {
        HibernateException thrownException = null;
        for (Session session : map.values()) {
          try {
            if ((null != session) && (session.isOpen())) {
              session.close();
            }
          } catch (HibernateException e) {
            thrownException = e;
          }
        }
        map.clear();
        if (null != thrownException) {
          throw thrownException;
        }
      }
    }
  }
  
  public void closeSession(Session session)
  {
    try
    {
      if (null != session) {
        session.close();
      }
    }
    catch (Exception localException) {}
  }
  

  public Transaction beginTransaction(Session s)
  {
    return s.beginTransaction();
  }
  


  public void commitTransaction(Transaction t)
  {
    t.commit();
  }
  





  public static Configuration getNewConfiguration(String configFileName)
  {
    return new Configuration();
  }
  


  public String getConfigurationFileName()
  {
    return null;
  }
  




  protected abstract Class getReferenceClass();
  




  protected Object get(Class refClass, Serializable key)
  {
    Session s = null;
    try {
      s = getSession();
      return get(refClass, key, s);
    } finally {
      closeSession(s);
    }
  }
  



  protected Object get(Class refClass, Serializable key, Session s)
  {
    return s.get(refClass, key);
  }
  



  protected Object load(Class refClass, Serializable key)
  {
    Session s = null;
    try {
      s = getSession();
      return load(refClass, key, s);
    } finally {
      closeSession(s);
    }
  }
  



  protected Object load(Class refClass, Serializable key, Session s)
  {
    return s.load(refClass, key);
  }
  


  public List findAll()
  {
    Session s = null;
    try {
      s = getSession();
      return findAll(s);
    } finally {
      closeSession(s);
    }
  }
  




  public List findAll(Session s)
  {
    return findAll(s, getDefaultOrder());
  }
  


  public List findAll(Order defaultOrder)
  {
    Session s = null;
    try {
      s = getSession();
      return findAll(s, defaultOrder);
    } finally {
      closeSession(s);
    }
  }
  




  public List findAll(Session s, Order defaultOrder)
  {
    Criteria crit = s.createCriteria(getReferenceClass());
    if (null != defaultOrder)
      crit.addOrder(defaultOrder);
    return crit.list();
  }
  





  protected Criteria findFiltered(String propName, Object filter)
  {
    return findFiltered(propName, filter, getDefaultOrder());
  }
  






  protected Criteria findFiltered(String propName, Object filter, Order order)
  {
    Session s = null;
    try {
      s = getSession();
      return findFiltered(s, propName, filter, order);
    } finally {
      closeSession(s);
    }
  }
  







  protected Criteria findFiltered(Session s, String propName, Object filter, Order order)
  {
    Criteria crit = s.createCriteria(getReferenceClass());
    crit.add(Expression.eq(propName, filter));
    if (null != order)
      crit.addOrder(order);
    return crit;
  }
  




  protected Query getNamedQuery(String name)
  {
    Session s = null;
    try {
      s = getSession();
      return getNamedQuery(name, s);
    } finally {
      closeSession(s);
    }
  }
  






  protected Query getNamedQuery(String name, Session s)
  {
    Query q = s.getNamedQuery(name);
    return q;
  }
  





  protected Query getNamedQuery(String name, Serializable param)
  {
    Session s = null;
    try {
      s = getSession();
      return getNamedQuery(name, param, s);
    } finally {
      closeSession(s);
    }
  }
  







  protected Query getNamedQuery(String name, Serializable param, Session s)
  {
    Query q = s.getNamedQuery(name);
    q.setParameter(0, param);
    return q;
  }
  






  protected Query getNamedQuery(String name, Serializable[] params)
  {
    Session s = null;
    try {
      s = getSession();
      return getNamedQuery(name, params, s);
    } finally {
      closeSession(s);
    }
  }
  







  protected Query getNamedQuery(String name, Serializable[] params, Session s)
  {
    Query q = s.getNamedQuery(name);
    if (null != params) {
      for (int i = 0; i < params.length; i++) {
        q.setParameter(i, params[i]);
      }
    }
    return q;
  }
  






  protected Query getNamedQuery(String name, Map params)
  {
    Session s = null;
    try {
      s = getSession();
      return getNamedQuery(name, params, s);
    } finally {
      closeSession(s);
    }
  }
  







  protected Query getNamedQuery(String name, Map params, Session s)
  {
    Query q = s.getNamedQuery(name);
    Iterator i; if (null != params) {
      for (i = params.entrySet().iterator(); i.hasNext();) {
        Map.Entry entry = (Map.Entry)i.next();
        q.setParameter((String)entry.getKey(), entry.getValue());
      }
    }
    return q;
  }
  




  public Query getQuery(String queryStr)
  {
    Session s = null;
    try {
      s = getSession();
      return getQuery(queryStr, s);
    } finally {
      closeSession(s);
    }
  }
  




  public Query getQuery(String queryStr, Session s)
  {
    return s.createQuery(queryStr);
  }
  






  protected Query getQuery(String queryStr, Serializable param)
  {
    Session s = null;
    try {
      s = getSession();
      return getQuery(queryStr, param, s);
    } finally {
      closeSession(s);
    }
  }
  






  protected Query getQuery(String queryStr, Serializable param, Session s)
  {
    Query q = getQuery(queryStr, s);
    q.setParameter(0, param);
    return q;
  }
  





  protected Query getQuery(String queryStr, Serializable[] params)
  {
    Session s = null;
    try {
      s = getSession();
      return getQuery(queryStr, params, s);
    } finally {
      closeSession(s);
    }
  }
  






  protected Query getQuery(String queryStr, Serializable[] params, Session s)
  {
    Query q = getQuery(queryStr, s);
    if (null != params) {
      for (int i = 0; i < params.length; i++) {
        q.setParameter(i, params[i]);
      }
    }
    return q;
  }
  






  protected Query getQuery(String queryStr, Map params)
  {
    Session s = null;
    try {
      s = getSession();
      return getQuery(queryStr, params, s);
    } finally {
      closeSession(s);
    }
  }
  







  protected Query getQuery(String queryStr, Map params, Session s)
  {
    Query q = getQuery(queryStr, s);
    Iterator i; if (null != params) {
      for (i = params.entrySet().iterator(); i.hasNext();) {
        Map.Entry entry = (Map.Entry)i.next();
        q.setParameter((String)entry.getKey(), entry.getValue());
      }
    }
    return q;
  }
  
  protected Order getDefaultOrder() {
    return null;
  }
  




  protected Serializable save(final Object obj)
  {
    (Serializable)run(new TransactionRunnable(obj) {
      public Object run(Session s) {
        return save(obj, s);
      }
    });
  }
  




  protected Serializable save(Object obj, Session s)
  {
    return s.save(obj);
  }
  




  protected void saveOrUpdate(final Object obj)
  {
    run(new TransactionRunnable(obj) {
      public Object run(Session s) {
        saveOrUpdate(obj, s);
        return null;
      }
    });
  }
  




  protected void saveOrUpdate(Object obj, Session s)
  {
    s.saveOrUpdate(obj);
  }
  





  protected void update(final Object obj)
  {
    run(new TransactionRunnable(obj) {
      public Object run(Session s) {
        update(obj, s);
        return null;
      }
    });
  }
  






  protected void update(Object obj, Session s)
  {
    s.update(obj);
  }
  


  protected int delete(final Query query)
  {
    Integer rtn = (Integer)run(new TransactionRunnable(query) {
      public Object run(Session s) {
        return new Integer(delete(query, s));
      }
    });
    return rtn.intValue();
  }
  


  protected int delete(Query query, Session s)
  {
    List list = query.list();
    for (Iterator i = list.iterator(); i.hasNext();) {
      delete(i.next(), s);
    }
    return list.size();
  }
  




  protected void delete(final Object obj)
  {
    run(new TransactionRunnable(obj) {
      public Object run(Session s) {
        delete(obj, s);
        return null;
      }
    });
  }
  




  protected void delete(Object obj, Session s)
  {
    s.delete(obj);
  }
  




  protected void refresh(Object obj, Session s)
  {
    s.refresh(obj);
  }
  
  protected void throwException(Throwable t) {
    if ((t instanceof HibernateException))
      throw ((HibernateException)t);
    if ((t instanceof RuntimeException)) {
      throw ((RuntimeException)t);
    }
    throw new HibernateException(t);
  }
  


  protected Object run(TransactionRunnable transactionRunnable)
  {
    Transaction t = null;
    Session s = null;
    try {
      s = getSession();
      t = beginTransaction(s);
      Object obj = transactionRunnable.run(s);
      commitTransaction(t);
      return obj;
    } catch (Throwable throwable) {
      if (null != t) {
        try {
          t.rollback();
        }
        catch (HibernateException e) {}
      }
      
      if ((transactionRunnable instanceof TransactionFailHandler)) {
        try {
          ((TransactionFailHandler)transactionRunnable).onFail(s);
        }
        catch (Throwable e) {}
      }
      
      throwException(throwable);
      return null;
    } finally {
      closeSession(s);
    }
  }
  


  protected TransactionPointer runAsnyc(TransactionRunnable transactionRunnable)
  {
    TransactionPointer transactionPointer = new TransactionPointer(transactionRunnable);
    ThreadRunner threadRunner = new ThreadRunner(transactionPointer);
    threadRunner.start();
    return transactionPointer;
  }
  
  protected void handleError(Throwable t) {}
  
  public abstract class TransactionRunnable
  {
    public TransactionRunnable() {}
    
    public abstract Object run(Session paramSession) throws Exception;
  }
  
  public static abstract interface TransactionFailHandler
  {
    public abstract void onFail(Session paramSession);
  }
  
  public abstract class TransactionRunnableFailHandler extends _BaseRootDAO.TransactionRunnable
    implements _BaseRootDAO.TransactionFailHandler
  {
    public TransactionRunnableFailHandler() { super(); }
  }
  
  public class TransactionPointer {
    private _BaseRootDAO.TransactionRunnable transactionRunnable;
    private Throwable thrownException;
    private Object returnValue;
    private boolean hasCompleted = false;
    
    public TransactionPointer(_BaseRootDAO.TransactionRunnable transactionRunnable) {
      this.transactionRunnable = transactionRunnable;
    }
    
    public boolean hasCompleted() {
      return hasCompleted;
    }
    
    public void complete() {
      hasCompleted = true;
    }
    
    public Object getReturnValue() {
      return returnValue;
    }
    
    public void setReturnValue(Object returnValue) {
      this.returnValue = returnValue;
    }
    
    public Throwable getThrownException() {
      return thrownException;
    }
    
    public void setThrownException(Throwable thrownException) {
      this.thrownException = thrownException;
    }
    
    public _BaseRootDAO.TransactionRunnable getTransactionRunnable() {
      return transactionRunnable;
    }
    
    public void setTransactionRunnable(_BaseRootDAO.TransactionRunnable transactionRunnable) {
      this.transactionRunnable = transactionRunnable;
    }
    






    public Object waitUntilFinish(long timeout)
      throws Throwable
    {
      long killTime = -1L;
      if (timeout > 0L)
        killTime = System.currentTimeMillis() + timeout;
      do {
        try {
          Thread.sleep(50L);
        }
        catch (InterruptedException localInterruptedException) {}
      } while ((!hasCompleted) && (((killTime > 0L) && (System.currentTimeMillis() < killTime)) || (killTime <= 0L)));
      if (!hasCompleted)
        throw new TimeLimitExceededException();
      if (null != thrownException) {
        throw thrownException;
      }
      return returnValue;
    }
  }
  
  private class ThreadRunner extends Thread {
    private _BaseRootDAO.TransactionPointer transactionPointer;
    
    public ThreadRunner(_BaseRootDAO.TransactionPointer transactionPointer) {
      this.transactionPointer = transactionPointer;
    }
    
    public void run() {
      Transaction t = null;
      Session s = null;
      try {
        s = getSession();
        t = beginTransaction(s);
        Object obj = transactionPointer.getTransactionRunnable().run(s);
        t.commit();
        transactionPointer.setReturnValue(obj); return;
      } catch (Throwable throwable) {
        if (null != t) {
          try {
            t.rollback();
          } catch (HibernateException e) {
            handleError(e);
          }
        }
        if ((transactionPointer.getTransactionRunnable() instanceof _BaseRootDAO.TransactionFailHandler)) {
          try {
            ((_BaseRootDAO.TransactionFailHandler)transactionPointer.getTransactionRunnable()).onFail(s);
          } catch (Throwable e) {
            handleError(e);
          }
        }
        transactionPointer.setThrownException(throwable);
      } finally {
        transactionPointer.complete();
        try {
          closeSession(s);
        } catch (HibernateException e) {
          transactionPointer.setThrownException(e);
        }
      }
    }
  }
}
