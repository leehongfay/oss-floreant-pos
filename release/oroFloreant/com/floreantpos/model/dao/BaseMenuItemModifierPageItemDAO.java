package com.floreantpos.model.dao;

import com.floreantpos.model.MenuItemModifierPageItem;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseMenuItemModifierPageItemDAO
  extends _RootDAO
{
  public static MenuItemModifierPageItemDAO instance;
  
  public BaseMenuItemModifierPageItemDAO() {}
  
  public static MenuItemModifierPageItemDAO getInstance()
  {
    if (null == instance) instance = new MenuItemModifierPageItemDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return MenuItemModifierPageItem.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public MenuItemModifierPageItem cast(Object object)
  {
    return (MenuItemModifierPageItem)object;
  }
  
  public MenuItemModifierPageItem get(String key) throws HibernateException
  {
    return (MenuItemModifierPageItem)get(getReferenceClass(), key);
  }
  
  public MenuItemModifierPageItem get(String key, Session s) throws HibernateException
  {
    return (MenuItemModifierPageItem)get(getReferenceClass(), key, s);
  }
  
  public MenuItemModifierPageItem load(String key) throws HibernateException
  {
    return (MenuItemModifierPageItem)load(getReferenceClass(), key);
  }
  
  public MenuItemModifierPageItem load(String key, Session s) throws HibernateException
  {
    return (MenuItemModifierPageItem)load(getReferenceClass(), key, s);
  }
  
  public MenuItemModifierPageItem loadInitialize(String key, Session s) throws HibernateException
  {
    MenuItemModifierPageItem obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<MenuItemModifierPageItem> findAll()
  {
    return super.findAll();
  }
  


  public List<MenuItemModifierPageItem> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<MenuItemModifierPageItem> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(MenuItemModifierPageItem menuItemModifierPageItem)
    throws HibernateException
  {
    return (String)super.save(menuItemModifierPageItem);
  }
  







  public String save(MenuItemModifierPageItem menuItemModifierPageItem, Session s)
    throws HibernateException
  {
    return (String)save(menuItemModifierPageItem, s);
  }
  





  public void saveOrUpdate(MenuItemModifierPageItem menuItemModifierPageItem)
    throws HibernateException
  {
    saveOrUpdate(menuItemModifierPageItem);
  }
  







  public void saveOrUpdate(MenuItemModifierPageItem menuItemModifierPageItem, Session s)
    throws HibernateException
  {
    saveOrUpdate(menuItemModifierPageItem, s);
  }
  




  public void update(MenuItemModifierPageItem menuItemModifierPageItem)
    throws HibernateException
  {
    update(menuItemModifierPageItem);
  }
  






  public void update(MenuItemModifierPageItem menuItemModifierPageItem, Session s)
    throws HibernateException
  {
    update(menuItemModifierPageItem, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(MenuItemModifierPageItem menuItemModifierPageItem)
    throws HibernateException
  {
    delete(menuItemModifierPageItem);
  }
  






  public void delete(MenuItemModifierPageItem menuItemModifierPageItem, Session s)
    throws HibernateException
  {
    delete(menuItemModifierPageItem, s);
  }
  









  public void refresh(MenuItemModifierPageItem menuItemModifierPageItem, Session s)
    throws HibernateException
  {
    refresh(menuItemModifierPageItem, s);
  }
}
