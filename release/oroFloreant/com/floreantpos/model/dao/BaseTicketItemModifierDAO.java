package com.floreantpos.model.dao;

import com.floreantpos.model.TicketItemModifier;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseTicketItemModifierDAO
  extends _RootDAO
{
  public static TicketItemModifierDAO instance;
  
  public BaseTicketItemModifierDAO() {}
  
  public static TicketItemModifierDAO getInstance()
  {
    if (null == instance) instance = new TicketItemModifierDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return TicketItemModifier.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public TicketItemModifier cast(Object object)
  {
    return (TicketItemModifier)object;
  }
  
  public TicketItemModifier get(String key) throws HibernateException
  {
    return (TicketItemModifier)get(getReferenceClass(), key);
  }
  
  public TicketItemModifier get(String key, Session s) throws HibernateException
  {
    return (TicketItemModifier)get(getReferenceClass(), key, s);
  }
  
  public TicketItemModifier load(String key) throws HibernateException
  {
    return (TicketItemModifier)load(getReferenceClass(), key);
  }
  
  public TicketItemModifier load(String key, Session s) throws HibernateException
  {
    return (TicketItemModifier)load(getReferenceClass(), key, s);
  }
  
  public TicketItemModifier loadInitialize(String key, Session s) throws HibernateException
  {
    TicketItemModifier obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<TicketItemModifier> findAll()
  {
    return super.findAll();
  }
  


  public List<TicketItemModifier> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<TicketItemModifier> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(TicketItemModifier ticketItemModifier)
    throws HibernateException
  {
    return (String)super.save(ticketItemModifier);
  }
  







  public String save(TicketItemModifier ticketItemModifier, Session s)
    throws HibernateException
  {
    return (String)save(ticketItemModifier, s);
  }
  





  public void saveOrUpdate(TicketItemModifier ticketItemModifier)
    throws HibernateException
  {
    saveOrUpdate(ticketItemModifier);
  }
  







  public void saveOrUpdate(TicketItemModifier ticketItemModifier, Session s)
    throws HibernateException
  {
    saveOrUpdate(ticketItemModifier, s);
  }
  




  public void update(TicketItemModifier ticketItemModifier)
    throws HibernateException
  {
    update(ticketItemModifier);
  }
  






  public void update(TicketItemModifier ticketItemModifier, Session s)
    throws HibernateException
  {
    update(ticketItemModifier, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(TicketItemModifier ticketItemModifier)
    throws HibernateException
  {
    delete(ticketItemModifier);
  }
  






  public void delete(TicketItemModifier ticketItemModifier, Session s)
    throws HibernateException
  {
    delete(ticketItemModifier, s);
  }
  









  public void refresh(TicketItemModifier ticketItemModifier, Session s)
    throws HibernateException
  {
    refresh(ticketItemModifier, s);
  }
}
