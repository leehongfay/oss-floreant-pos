package com.floreantpos.model.dao;

import com.floreantpos.model.Tag;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;






public class TagDAO
  extends BaseTagDAO
{
  public TagDAO() {}
  
  public boolean isExists(String tagName)
  {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Tag.PROP_NAME, tagName));
      List list = criteria.list();
      
      return (list != null) && (list.size() > 0);
    } finally {
      closeSession(session);
    }
  }
}
