package com.floreantpos.model.dao;

import com.floreantpos.model.ReversalTransaction;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseReversalTransactionDAO
  extends _RootDAO
{
  public static ReversalTransactionDAO instance;
  
  public BaseReversalTransactionDAO() {}
  
  public static ReversalTransactionDAO getInstance()
  {
    if (null == instance) instance = new ReversalTransactionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return ReversalTransaction.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public ReversalTransaction cast(Object object)
  {
    return (ReversalTransaction)object;
  }
  
  public ReversalTransaction get(String key) throws HibernateException
  {
    return (ReversalTransaction)get(getReferenceClass(), key);
  }
  
  public ReversalTransaction get(String key, Session s) throws HibernateException
  {
    return (ReversalTransaction)get(getReferenceClass(), key, s);
  }
  
  public ReversalTransaction load(String key) throws HibernateException
  {
    return (ReversalTransaction)load(getReferenceClass(), key);
  }
  
  public ReversalTransaction load(String key, Session s) throws HibernateException
  {
    return (ReversalTransaction)load(getReferenceClass(), key, s);
  }
  
  public ReversalTransaction loadInitialize(String key, Session s) throws HibernateException
  {
    ReversalTransaction obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<ReversalTransaction> findAll()
  {
    return super.findAll();
  }
  


  public List<ReversalTransaction> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<ReversalTransaction> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(ReversalTransaction reversalTransaction)
    throws HibernateException
  {
    return (String)super.save(reversalTransaction);
  }
  







  public String save(ReversalTransaction reversalTransaction, Session s)
    throws HibernateException
  {
    return (String)save(reversalTransaction, s);
  }
  





  public void saveOrUpdate(ReversalTransaction reversalTransaction)
    throws HibernateException
  {
    saveOrUpdate(reversalTransaction);
  }
  







  public void saveOrUpdate(ReversalTransaction reversalTransaction, Session s)
    throws HibernateException
  {
    saveOrUpdate(reversalTransaction, s);
  }
  




  public void update(ReversalTransaction reversalTransaction)
    throws HibernateException
  {
    update(reversalTransaction);
  }
  






  public void update(ReversalTransaction reversalTransaction, Session s)
    throws HibernateException
  {
    update(reversalTransaction, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(ReversalTransaction reversalTransaction)
    throws HibernateException
  {
    delete(reversalTransaction);
  }
  






  public void delete(ReversalTransaction reversalTransaction, Session s)
    throws HibernateException
  {
    delete(reversalTransaction, s);
  }
  









  public void refresh(ReversalTransaction reversalTransaction, Session s)
    throws HibernateException
  {
    refresh(reversalTransaction, s);
  }
}
