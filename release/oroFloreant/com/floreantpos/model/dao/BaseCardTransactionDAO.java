package com.floreantpos.model.dao;

import com.floreantpos.model.CardTransaction;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




















public abstract class BaseCardTransactionDAO
  extends _RootDAO
{
  public static CardTransactionDAO instance;
  
  public BaseCardTransactionDAO() {}
  
  public static CardTransactionDAO getInstance()
  {
    if (null == instance) instance = new CardTransactionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return CardTransaction.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public CardTransaction cast(Object object)
  {
    return (CardTransaction)object;
  }
  
  public CardTransaction get(Integer key) throws HibernateException
  {
    return (CardTransaction)get(getReferenceClass(), key);
  }
  
  public CardTransaction get(Integer key, Session s) throws HibernateException
  {
    return (CardTransaction)get(getReferenceClass(), key, s);
  }
  
  public CardTransaction load(Integer key) throws HibernateException
  {
    return (CardTransaction)load(getReferenceClass(), key);
  }
  
  public CardTransaction load(Integer key, Session s) throws HibernateException
  {
    return (CardTransaction)load(getReferenceClass(), key, s);
  }
  
  public CardTransaction loadInitialize(Integer key, Session s) throws HibernateException
  {
    CardTransaction obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<CardTransaction> findAll()
  {
    return super.findAll();
  }
  


  public List<CardTransaction> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<CardTransaction> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public Integer save(CardTransaction cardTransaction)
    throws HibernateException
  {
    return (Integer)super.save(cardTransaction);
  }
  







  public Integer save(CardTransaction cardTransaction, Session s)
    throws HibernateException
  {
    return (Integer)save(cardTransaction, s);
  }
  





  public void saveOrUpdate(CardTransaction cardTransaction)
    throws HibernateException
  {
    saveOrUpdate(cardTransaction);
  }
  







  public void saveOrUpdate(CardTransaction cardTransaction, Session s)
    throws HibernateException
  {
    saveOrUpdate(cardTransaction, s);
  }
  




  public void update(CardTransaction cardTransaction)
    throws HibernateException
  {
    update(cardTransaction);
  }
  






  public void update(CardTransaction cardTransaction, Session s)
    throws HibernateException
  {
    update(cardTransaction, s);
  }
  




  public void delete(Integer id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(Integer id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(CardTransaction cardTransaction)
    throws HibernateException
  {
    delete(cardTransaction);
  }
  






  public void delete(CardTransaction cardTransaction, Session s)
    throws HibernateException
  {
    delete(cardTransaction, s);
  }
  









  public void refresh(CardTransaction cardTransaction, Session s)
    throws HibernateException
  {
    refresh(cardTransaction, s);
  }
}
