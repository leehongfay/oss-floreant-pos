package com.floreantpos.model.dao;

import com.floreantpos.model.ShopFloor;
import com.floreantpos.model.ShopTable;
import java.util.List;
import java.util.Set;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;






















public class ShopFloorDAO
  extends BaseShopFloorDAO
{
  public ShopFloorDAO() {}
  
  public List<ShopFloor> findAll()
  {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.addOrder(Order.asc(ShopFloor.PROP_SORT_ORDER));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public boolean hasFloor() {
    Number result = (Number)getSession().createCriteria(getReferenceClass()).setProjection(Projections.rowCount()).uniqueResult();
    return result.intValue() != 0;
  }
  
  public void initializeTables(ShopFloor shopFloor) {
    if ((shopFloor == null) || (shopFloor.getId() == null)) {
      return;
    }
    Session session = null;
    try {
      session = createNewSession();
      session.refresh(shopFloor);
      Hibernate.initialize(shopFloor.getTables());
      
      closeSession(session); } finally { closeSession(session);
    }
  }
  
  public void delete(ShopFloor shopFloor) throws HibernateException
  {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      Set<ShopTable> tables = shopFloor.getTables();
      
      if ((tables != null) && (!tables.isEmpty())) {
        shopFloor.getTables().removeAll(tables);
        saveOrUpdate(shopFloor);
      }
      
      super.delete(shopFloor, session);
      
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(ShopFloorDAO.class).error(e);
      
      throw new HibernateException(e);
    } finally {
      closeSession(session);
    }
  }
}
