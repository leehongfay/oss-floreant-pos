package com.floreantpos.model.dao;

import com.floreantpos.model.TableBookingInfo;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseTableBookingInfoDAO
  extends _RootDAO
{
  public static TableBookingInfoDAO instance;
  
  public BaseTableBookingInfoDAO() {}
  
  public static TableBookingInfoDAO getInstance()
  {
    if (null == instance) instance = new TableBookingInfoDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return TableBookingInfo.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public TableBookingInfo cast(Object object)
  {
    return (TableBookingInfo)object;
  }
  
  public TableBookingInfo get(String key) throws HibernateException
  {
    return (TableBookingInfo)get(getReferenceClass(), key);
  }
  
  public TableBookingInfo get(String key, Session s) throws HibernateException
  {
    return (TableBookingInfo)get(getReferenceClass(), key, s);
  }
  
  public TableBookingInfo load(String key) throws HibernateException
  {
    return (TableBookingInfo)load(getReferenceClass(), key);
  }
  
  public TableBookingInfo load(String key, Session s) throws HibernateException
  {
    return (TableBookingInfo)load(getReferenceClass(), key, s);
  }
  
  public TableBookingInfo loadInitialize(String key, Session s) throws HibernateException
  {
    TableBookingInfo obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<TableBookingInfo> findAll()
  {
    return super.findAll();
  }
  


  public List<TableBookingInfo> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<TableBookingInfo> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(TableBookingInfo tableBookingInfo)
    throws HibernateException
  {
    return (String)super.save(tableBookingInfo);
  }
  







  public String save(TableBookingInfo tableBookingInfo, Session s)
    throws HibernateException
  {
    return (String)save(tableBookingInfo, s);
  }
  





  public void saveOrUpdate(TableBookingInfo tableBookingInfo)
    throws HibernateException
  {
    saveOrUpdate(tableBookingInfo);
  }
  







  public void saveOrUpdate(TableBookingInfo tableBookingInfo, Session s)
    throws HibernateException
  {
    saveOrUpdate(tableBookingInfo, s);
  }
  




  public void update(TableBookingInfo tableBookingInfo)
    throws HibernateException
  {
    update(tableBookingInfo);
  }
  






  public void update(TableBookingInfo tableBookingInfo, Session s)
    throws HibernateException
  {
    update(tableBookingInfo, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(TableBookingInfo tableBookingInfo)
    throws HibernateException
  {
    delete(tableBookingInfo);
  }
  






  public void delete(TableBookingInfo tableBookingInfo, Session s)
    throws HibernateException
  {
    delete(tableBookingInfo, s);
  }
  









  public void refresh(TableBookingInfo tableBookingInfo, Session s)
    throws HibernateException
  {
    refresh(tableBookingInfo, s);
  }
}
