package com.floreantpos.model.dao;

import com.floreantpos.model.MenuItemModifierPageItem;
import com.floreantpos.model.MenuModifier;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;





































































public class ModifierDAO
  extends BaseModifierDAO
{
  public ModifierDAO() {}
  
  public List<MenuModifier> getPizzaModifiers(String searchString)
  {
    Session session = null;
    Criteria criteria = null;
    try
    {
      session = createNewSession();
      criteria = session.createCriteria(MenuModifier.class);
      criteria.add(Restrictions.eq(MenuModifier.PROP_PIZZA_MODIFIER, Boolean.valueOf(true)));
      if (StringUtils.isNotEmpty(searchString)) {
        criteria.add(Restrictions.ilike(MenuModifier.PROP_NAME, searchString.trim(), MatchMode.ANYWHERE));
      }
      return criteria.list();
    }
    finally {
      session.close();
    }
  }
  
  public void saveMenuModifierFormData(MenuModifier menuModifier) {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      
      saveOrUpdate(menuModifier, session);
      

      MenuItemModifierPageItemDAO menuPageItemDAO = MenuItemModifierPageItemDAO.getInstance();
      List<MenuItemModifierPageItem> menuItemModifierPageItems = menuPageItemDAO.getPageItemFor(menuModifier, session);
      if (menuItemModifierPageItems != null) {
        for (MenuItemModifierPageItem menuItemModifierPageItem : menuItemModifierPageItems) {
          menuItemModifierPageItem.setMenuModifier(menuModifier);
          menuPageItemDAO.saveOrUpdate(menuItemModifierPageItem, session);
        }
      }
      
      tx.commit();
    } finally {
      closeSession(session);
    }
  }
}
