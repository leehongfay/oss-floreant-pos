package com.floreantpos.model.dao;

import com.floreantpos.model.Ticket;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseTicketDAO
  extends _RootDAO
{
  public static TicketDAO instance;
  
  public BaseTicketDAO() {}
  
  public static TicketDAO getInstance()
  {
    if (null == instance) instance = new TicketDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return Ticket.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public Ticket cast(Object object)
  {
    return (Ticket)object;
  }
  
  public Ticket get(String key) throws HibernateException
  {
    return (Ticket)get(getReferenceClass(), key);
  }
  
  public Ticket get(String key, Session s) throws HibernateException
  {
    return (Ticket)get(getReferenceClass(), key, s);
  }
  
  public Ticket load(String key) throws HibernateException
  {
    return (Ticket)load(getReferenceClass(), key);
  }
  
  public Ticket load(String key, Session s) throws HibernateException
  {
    return (Ticket)load(getReferenceClass(), key, s);
  }
  
  public Ticket loadInitialize(String key, Session s) throws HibernateException
  {
    Ticket obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<Ticket> findAll()
  {
    return super.findAll();
  }
  


  public List<Ticket> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<Ticket> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(Ticket ticket)
    throws HibernateException
  {
    return (String)super.save(ticket);
  }
  







  public String save(Ticket ticket, Session s)
    throws HibernateException
  {
    return (String)save(ticket, s);
  }
  





  public void saveOrUpdate(Ticket ticket)
    throws HibernateException
  {
    saveOrUpdate(ticket);
  }
  







  public void saveOrUpdate(Ticket ticket, Session s)
    throws HibernateException
  {
    saveOrUpdate(ticket, s);
  }
  




  public void update(Ticket ticket)
    throws HibernateException
  {
    update(ticket);
  }
  






  public void update(Ticket ticket, Session s)
    throws HibernateException
  {
    update(ticket, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(Ticket ticket)
    throws HibernateException
  {
    delete(ticket);
  }
  






  public void delete(Ticket ticket, Session s)
    throws HibernateException
  {
    delete(ticket, s);
  }
  









  public void refresh(Ticket ticket, Session s)
    throws HibernateException
  {
    refresh(ticket, s);
  }
}
