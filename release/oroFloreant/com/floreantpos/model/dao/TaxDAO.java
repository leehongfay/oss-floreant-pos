package com.floreantpos.model.dao;

import com.floreantpos.model.Tax;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;




















public class TaxDAO
  extends BaseTaxDAO
{
  public TaxDAO() {}
  
  public Tax findByTaxRate(double taxRate)
  {
    Session session = null;
    try {
      session = createNewSession();
      
      Criteria criteria = session.createCriteria(Tax.class);
      criteria.add(Restrictions.eq(Tax.PROP_RATE, Double.valueOf(taxRate)));
      return (Tax)criteria.uniqueResult();
    } finally {
      closeSession(session);
    }
  }
  
  public boolean nameExists(Tax tax, String code) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Tax.PROP_NAME, code).ignoreCase());
      if (tax.getId() != null) {
        criteria.add(Restrictions.ne(Tax.PROP_ID, tax.getId()));
      }
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      boolean bool;
      if (rowCount == null) {
        return false;
      }
      return rowCount.intValue() > 0;
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
}
