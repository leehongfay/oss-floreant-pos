package com.floreantpos.model.dao;

import com.floreantpos.model.MenuItemShift;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseMenuItemShiftDAO
  extends _RootDAO
{
  public static MenuItemShiftDAO instance;
  
  public BaseMenuItemShiftDAO() {}
  
  public static MenuItemShiftDAO getInstance()
  {
    if (null == instance) instance = new MenuItemShiftDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return MenuItemShift.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public MenuItemShift cast(Object object)
  {
    return (MenuItemShift)object;
  }
  
  public MenuItemShift get(String key) throws HibernateException
  {
    return (MenuItemShift)get(getReferenceClass(), key);
  }
  
  public MenuItemShift get(String key, Session s) throws HibernateException
  {
    return (MenuItemShift)get(getReferenceClass(), key, s);
  }
  
  public MenuItemShift load(String key) throws HibernateException
  {
    return (MenuItemShift)load(getReferenceClass(), key);
  }
  
  public MenuItemShift load(String key, Session s) throws HibernateException
  {
    return (MenuItemShift)load(getReferenceClass(), key, s);
  }
  
  public MenuItemShift loadInitialize(String key, Session s) throws HibernateException
  {
    MenuItemShift obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<MenuItemShift> findAll()
  {
    return super.findAll();
  }
  


  public List<MenuItemShift> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<MenuItemShift> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(MenuItemShift menuItemShift)
    throws HibernateException
  {
    return (String)super.save(menuItemShift);
  }
  







  public String save(MenuItemShift menuItemShift, Session s)
    throws HibernateException
  {
    return (String)save(menuItemShift, s);
  }
  





  public void saveOrUpdate(MenuItemShift menuItemShift)
    throws HibernateException
  {
    saveOrUpdate(menuItemShift);
  }
  







  public void saveOrUpdate(MenuItemShift menuItemShift, Session s)
    throws HibernateException
  {
    saveOrUpdate(menuItemShift, s);
  }
  




  public void update(MenuItemShift menuItemShift)
    throws HibernateException
  {
    update(menuItemShift);
  }
  






  public void update(MenuItemShift menuItemShift, Session s)
    throws HibernateException
  {
    update(menuItemShift, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(MenuItemShift menuItemShift)
    throws HibernateException
  {
    delete(menuItemShift);
  }
  






  public void delete(MenuItemShift menuItemShift, Session s)
    throws HibernateException
  {
    delete(menuItemShift, s);
  }
  









  public void refresh(MenuItemShift menuItemShift, Session s)
    throws HibernateException
  {
    refresh(menuItemShift, s);
  }
}
