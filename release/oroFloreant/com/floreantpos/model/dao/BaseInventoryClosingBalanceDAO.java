package com.floreantpos.model.dao;

import com.floreantpos.model.InventoryClosingBalance;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseInventoryClosingBalanceDAO
  extends _RootDAO
{
  public static InventoryClosingBalanceDAO instance;
  
  public BaseInventoryClosingBalanceDAO() {}
  
  public static InventoryClosingBalanceDAO getInstance()
  {
    if (null == instance) instance = new InventoryClosingBalanceDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return InventoryClosingBalance.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public InventoryClosingBalance cast(Object object)
  {
    return (InventoryClosingBalance)object;
  }
  
  public InventoryClosingBalance get(String key) throws HibernateException
  {
    return (InventoryClosingBalance)get(getReferenceClass(), key);
  }
  
  public InventoryClosingBalance get(String key, Session s) throws HibernateException
  {
    return (InventoryClosingBalance)get(getReferenceClass(), key, s);
  }
  
  public InventoryClosingBalance load(String key) throws HibernateException
  {
    return (InventoryClosingBalance)load(getReferenceClass(), key);
  }
  
  public InventoryClosingBalance load(String key, Session s) throws HibernateException
  {
    return (InventoryClosingBalance)load(getReferenceClass(), key, s);
  }
  
  public InventoryClosingBalance loadInitialize(String key, Session s) throws HibernateException
  {
    InventoryClosingBalance obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<InventoryClosingBalance> findAll()
  {
    return super.findAll();
  }
  


  public List<InventoryClosingBalance> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<InventoryClosingBalance> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(InventoryClosingBalance inventoryClosingBalance)
    throws HibernateException
  {
    return (String)super.save(inventoryClosingBalance);
  }
  







  public String save(InventoryClosingBalance inventoryClosingBalance, Session s)
    throws HibernateException
  {
    return (String)save(inventoryClosingBalance, s);
  }
  





  public void saveOrUpdate(InventoryClosingBalance inventoryClosingBalance)
    throws HibernateException
  {
    saveOrUpdate(inventoryClosingBalance);
  }
  







  public void saveOrUpdate(InventoryClosingBalance inventoryClosingBalance, Session s)
    throws HibernateException
  {
    saveOrUpdate(inventoryClosingBalance, s);
  }
  




  public void update(InventoryClosingBalance inventoryClosingBalance)
    throws HibernateException
  {
    update(inventoryClosingBalance);
  }
  






  public void update(InventoryClosingBalance inventoryClosingBalance, Session s)
    throws HibernateException
  {
    update(inventoryClosingBalance, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(InventoryClosingBalance inventoryClosingBalance)
    throws HibernateException
  {
    delete(inventoryClosingBalance);
  }
  






  public void delete(InventoryClosingBalance inventoryClosingBalance, Session s)
    throws HibernateException
  {
    delete(inventoryClosingBalance, s);
  }
  









  public void refresh(InventoryClosingBalance inventoryClosingBalance, Session s)
    throws HibernateException
  {
    refresh(inventoryClosingBalance, s);
  }
}
