package com.floreantpos.model.dao;

import com.floreantpos.model.PrinterGroup;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;






















public class PrinterGroupDAO
  extends BasePrinterGroupDAO
{
  public PrinterGroupDAO() {}
  
  public PrinterGroup findByName(String name, Session session)
  {
    Criteria criteria = session.createCriteria(getReferenceClass());
    criteria.add(Restrictions.eq(PrinterGroup.PROP_NAME, name));
    
    return (PrinterGroup)criteria.uniqueResult();
  }
  
  public PrinterGroup findByName(String name) {
    Session session = null;
    try {
      session = getSession();
      return findByName(name, session);
    } finally {
      closeSession(getSession());
    }
  }
  
  public PrinterGroup getDefaultPrinterGroup() {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(PrinterGroup.PROP_IS_DEFAULT, Boolean.TRUE));
      List list = criteria.list();
      PrinterGroup localPrinterGroup; if ((list != null) && (!list.isEmpty())) {
        return (PrinterGroup)list.get(0);
      }
      return null;
    } finally {
      closeSession(getSession());
    }
  }
}
