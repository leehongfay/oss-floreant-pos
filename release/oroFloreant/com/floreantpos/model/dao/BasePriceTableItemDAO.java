package com.floreantpos.model.dao;

import com.floreantpos.model.PriceTableItem;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BasePriceTableItemDAO
  extends _RootDAO
{
  public static PriceTableItemDAO instance;
  
  public BasePriceTableItemDAO() {}
  
  public static PriceTableItemDAO getInstance()
  {
    if (null == instance) instance = new PriceTableItemDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return PriceTableItem.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public PriceTableItem cast(Object object)
  {
    return (PriceTableItem)object;
  }
  
  public PriceTableItem get(String key) throws HibernateException
  {
    return (PriceTableItem)get(getReferenceClass(), key);
  }
  
  public PriceTableItem get(String key, Session s) throws HibernateException
  {
    return (PriceTableItem)get(getReferenceClass(), key, s);
  }
  
  public PriceTableItem load(String key) throws HibernateException
  {
    return (PriceTableItem)load(getReferenceClass(), key);
  }
  
  public PriceTableItem load(String key, Session s) throws HibernateException
  {
    return (PriceTableItem)load(getReferenceClass(), key, s);
  }
  
  public PriceTableItem loadInitialize(String key, Session s) throws HibernateException
  {
    PriceTableItem obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<PriceTableItem> findAll()
  {
    return super.findAll();
  }
  


  public List<PriceTableItem> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<PriceTableItem> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(PriceTableItem priceTableItem)
    throws HibernateException
  {
    return (String)super.save(priceTableItem);
  }
  







  public String save(PriceTableItem priceTableItem, Session s)
    throws HibernateException
  {
    return (String)save(priceTableItem, s);
  }
  





  public void saveOrUpdate(PriceTableItem priceTableItem)
    throws HibernateException
  {
    saveOrUpdate(priceTableItem);
  }
  







  public void saveOrUpdate(PriceTableItem priceTableItem, Session s)
    throws HibernateException
  {
    saveOrUpdate(priceTableItem, s);
  }
  




  public void update(PriceTableItem priceTableItem)
    throws HibernateException
  {
    update(priceTableItem);
  }
  






  public void update(PriceTableItem priceTableItem, Session s)
    throws HibernateException
  {
    update(priceTableItem, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(PriceTableItem priceTableItem)
    throws HibernateException
  {
    delete(priceTableItem);
  }
  






  public void delete(PriceTableItem priceTableItem, Session s)
    throws HibernateException
  {
    delete(priceTableItem, s);
  }
  









  public void refresh(PriceTableItem priceTableItem, Session s)
    throws HibernateException
  {
    refresh(priceTableItem, s);
  }
}
