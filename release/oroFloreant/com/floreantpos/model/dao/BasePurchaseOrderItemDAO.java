package com.floreantpos.model.dao;

import com.floreantpos.model.PurchaseOrderItem;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BasePurchaseOrderItemDAO
  extends _RootDAO
{
  public static PurchaseOrderItemDAO instance;
  
  public BasePurchaseOrderItemDAO() {}
  
  public static PurchaseOrderItemDAO getInstance()
  {
    if (null == instance) instance = new PurchaseOrderItemDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return PurchaseOrderItem.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public PurchaseOrderItem cast(Object object)
  {
    return (PurchaseOrderItem)object;
  }
  
  public PurchaseOrderItem get(String key) throws HibernateException
  {
    return (PurchaseOrderItem)get(getReferenceClass(), key);
  }
  
  public PurchaseOrderItem get(String key, Session s) throws HibernateException
  {
    return (PurchaseOrderItem)get(getReferenceClass(), key, s);
  }
  
  public PurchaseOrderItem load(String key) throws HibernateException
  {
    return (PurchaseOrderItem)load(getReferenceClass(), key);
  }
  
  public PurchaseOrderItem load(String key, Session s) throws HibernateException
  {
    return (PurchaseOrderItem)load(getReferenceClass(), key, s);
  }
  
  public PurchaseOrderItem loadInitialize(String key, Session s) throws HibernateException
  {
    PurchaseOrderItem obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<PurchaseOrderItem> findAll()
  {
    return super.findAll();
  }
  


  public List<PurchaseOrderItem> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<PurchaseOrderItem> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(PurchaseOrderItem purchaseOrderItem)
    throws HibernateException
  {
    return (String)super.save(purchaseOrderItem);
  }
  







  public String save(PurchaseOrderItem purchaseOrderItem, Session s)
    throws HibernateException
  {
    return (String)save(purchaseOrderItem, s);
  }
  





  public void saveOrUpdate(PurchaseOrderItem purchaseOrderItem)
    throws HibernateException
  {
    saveOrUpdate(purchaseOrderItem);
  }
  







  public void saveOrUpdate(PurchaseOrderItem purchaseOrderItem, Session s)
    throws HibernateException
  {
    saveOrUpdate(purchaseOrderItem, s);
  }
  




  public void update(PurchaseOrderItem purchaseOrderItem)
    throws HibernateException
  {
    update(purchaseOrderItem);
  }
  






  public void update(PurchaseOrderItem purchaseOrderItem, Session s)
    throws HibernateException
  {
    update(purchaseOrderItem, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(PurchaseOrderItem purchaseOrderItem)
    throws HibernateException
  {
    delete(purchaseOrderItem);
  }
  






  public void delete(PurchaseOrderItem purchaseOrderItem, Session s)
    throws HibernateException
  {
    delete(purchaseOrderItem, s);
  }
  









  public void refresh(PurchaseOrderItem purchaseOrderItem, Session s)
    throws HibernateException
  {
    refresh(purchaseOrderItem, s);
  }
}
