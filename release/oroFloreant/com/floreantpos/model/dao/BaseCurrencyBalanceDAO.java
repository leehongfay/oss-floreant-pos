package com.floreantpos.model.dao;

import com.floreantpos.model.CurrencyBalance;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseCurrencyBalanceDAO
  extends _RootDAO
{
  public static CurrencyBalanceDAO instance;
  
  public BaseCurrencyBalanceDAO() {}
  
  public static CurrencyBalanceDAO getInstance()
  {
    if (null == instance) instance = new CurrencyBalanceDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return CurrencyBalance.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public CurrencyBalance cast(Object object)
  {
    return (CurrencyBalance)object;
  }
  
  public CurrencyBalance get(String key) throws HibernateException
  {
    return (CurrencyBalance)get(getReferenceClass(), key);
  }
  
  public CurrencyBalance get(String key, Session s) throws HibernateException
  {
    return (CurrencyBalance)get(getReferenceClass(), key, s);
  }
  
  public CurrencyBalance load(String key) throws HibernateException
  {
    return (CurrencyBalance)load(getReferenceClass(), key);
  }
  
  public CurrencyBalance load(String key, Session s) throws HibernateException
  {
    return (CurrencyBalance)load(getReferenceClass(), key, s);
  }
  
  public CurrencyBalance loadInitialize(String key, Session s) throws HibernateException
  {
    CurrencyBalance obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<CurrencyBalance> findAll()
  {
    return super.findAll();
  }
  


  public List<CurrencyBalance> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<CurrencyBalance> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(CurrencyBalance currencyBalance)
    throws HibernateException
  {
    return (String)super.save(currencyBalance);
  }
  







  public String save(CurrencyBalance currencyBalance, Session s)
    throws HibernateException
  {
    return (String)save(currencyBalance, s);
  }
  





  public void saveOrUpdate(CurrencyBalance currencyBalance)
    throws HibernateException
  {
    saveOrUpdate(currencyBalance);
  }
  







  public void saveOrUpdate(CurrencyBalance currencyBalance, Session s)
    throws HibernateException
  {
    saveOrUpdate(currencyBalance, s);
  }
  




  public void update(CurrencyBalance currencyBalance)
    throws HibernateException
  {
    update(currencyBalance);
  }
  






  public void update(CurrencyBalance currencyBalance, Session s)
    throws HibernateException
  {
    update(currencyBalance, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(CurrencyBalance currencyBalance)
    throws HibernateException
  {
    delete(currencyBalance);
  }
  






  public void delete(CurrencyBalance currencyBalance, Session s)
    throws HibernateException
  {
    delete(currencyBalance, s);
  }
  









  public void refresh(CurrencyBalance currencyBalance, Session s)
    throws HibernateException
  {
    refresh(currencyBalance, s);
  }
}
