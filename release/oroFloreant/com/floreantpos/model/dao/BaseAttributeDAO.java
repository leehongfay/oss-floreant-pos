package com.floreantpos.model.dao;

import com.floreantpos.model.Attribute;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseAttributeDAO
  extends _RootDAO
{
  public static AttributeDAO instance;
  
  public BaseAttributeDAO() {}
  
  public static AttributeDAO getInstance()
  {
    if (null == instance) instance = new AttributeDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return Attribute.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public Attribute cast(Object object)
  {
    return (Attribute)object;
  }
  
  public Attribute get(String key) throws HibernateException
  {
    return (Attribute)get(getReferenceClass(), key);
  }
  
  public Attribute get(String key, Session s) throws HibernateException
  {
    return (Attribute)get(getReferenceClass(), key, s);
  }
  
  public Attribute load(String key) throws HibernateException
  {
    return (Attribute)load(getReferenceClass(), key);
  }
  
  public Attribute load(String key, Session s) throws HibernateException
  {
    return (Attribute)load(getReferenceClass(), key, s);
  }
  
  public Attribute loadInitialize(String key, Session s) throws HibernateException
  {
    Attribute obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<Attribute> findAll()
  {
    return super.findAll();
  }
  


  public List<Attribute> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<Attribute> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(Attribute attribute)
    throws HibernateException
  {
    return (String)super.save(attribute);
  }
  







  public String save(Attribute attribute, Session s)
    throws HibernateException
  {
    return (String)save(attribute, s);
  }
  





  public void saveOrUpdate(Attribute attribute)
    throws HibernateException
  {
    saveOrUpdate(attribute);
  }
  







  public void saveOrUpdate(Attribute attribute, Session s)
    throws HibernateException
  {
    saveOrUpdate(attribute, s);
  }
  




  public void update(Attribute attribute)
    throws HibernateException
  {
    update(attribute);
  }
  






  public void update(Attribute attribute, Session s)
    throws HibernateException
  {
    update(attribute, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(Attribute attribute)
    throws HibernateException
  {
    delete(attribute);
  }
  






  public void delete(Attribute attribute, Session s)
    throws HibernateException
  {
    delete(attribute, s);
  }
  









  public void refresh(Attribute attribute, Session s)
    throws HibernateException
  {
    refresh(attribute, s);
  }
}
