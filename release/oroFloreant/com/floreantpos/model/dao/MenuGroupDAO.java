package com.floreantpos.model.dao;

import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Terminal;
import com.floreantpos.swing.PaginatedListModel;
import com.floreantpos.swing.PaginationSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.authorize.util.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;






















public class MenuGroupDAO
  extends BaseMenuGroupDAO
{
  public MenuGroupDAO() {}
  
  public Serializable save(Object menuGroup, Session s)
    throws HibernateException
  {
    Serializable serializable = super.save(menuGroup, s);
    updateDependentModels((MenuGroup)menuGroup, s);
    return serializable;
  }
  
  public void update(Object menuGroup, Session s) throws HibernateException
  {
    super.update(menuGroup, s);
    updateDependentModels((MenuGroup)menuGroup, s);
  }
  
  public void saveOrUpdate(Object menuGroup, Session s) throws HibernateException
  {
    super.saveOrUpdate(menuGroup, s);
    updateDependentModels((MenuGroup)menuGroup, s);
  }
  
  private void updateDependentModels(MenuGroup menuGroup, Session session) {
    String hqlString = "update MenuItem set %s=:groupName, %s=:categoryId, %s=:categoryName, %s=:beverage where %s=:groupId";
    
    hqlString = String.format(hqlString, new Object[] { MenuItem.PROP_MENU_GROUP_NAME, MenuItem.PROP_MENU_CATEGORY_ID, MenuItem.PROP_MENU_CATEGORY_NAME, MenuItem.PROP_BEVERAGE, MenuItem.PROP_MENU_GROUP_ID });
    







    Query query = session.createQuery(hqlString);
    query.setParameter("groupName", menuGroup.getName());
    query.setParameter("categoryId", menuGroup.getMenuCategoryId());
    query.setParameter("categoryName", menuGroup.getMenuCategoryName());
    query.setParameter("beverage", menuGroup.isBeverage());
    query.setParameter("groupId", menuGroup.getId());
    query.executeUpdate();
    session.saveOrUpdate(menuGroup);
  }
  
  public void initialize(MenuGroup menuGroup) {
    Session session = null;
    try
    {
      session = createNewSession();
      session.refresh(menuGroup);
      
      closeSession(session); } finally { closeSession(session);
    }
  }
  
  public void loadActiveGroups(MenuCategory parentCategory, PaginatedListModel listModel) throws PosException {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(MenuGroup.PROP_VISIBLE, Boolean.TRUE));
      if (parentCategory != null) {
        criteria.add(Restrictions.eq(MenuGroup.PROP_MENU_CATEGORY_ID, parentCategory.getId()));
      }
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        listModel.setNumRows(rowCount.intValue());
      }
      
      criteria.setProjection(null);
      
      criteria.addOrder(Order.asc(MenuGroup.PROP_SORT_ORDER));
      criteria.setFirstResult(listModel.getCurrentRowIndex());
      criteria.setMaxResults(listModel.getPageSize());
      
      List<MenuGroup> list = criteria.list();
      





      listModel.setData(list);
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuGroup> loadActiveGroupsByOrderType(OrderType orderType) throws PosException {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(MenuGroup.PROP_VISIBLE, Boolean.TRUE));
      List<MenuCategory> activeCategories = MenuCategoryDAO.getInstance().findActiveCategories(orderType);
      List<String> activeCatIds = new ArrayList();
      for (Object localObject1 = activeCategories.iterator(); ((Iterator)localObject1).hasNext();) { MenuCategory menuCategory = (MenuCategory)((Iterator)localObject1).next();
        activeCatIds.add(menuCategory.getId());
      }
      if (!activeCatIds.isEmpty()) {
        criteria.add(Restrictions.in(MenuGroup.PROP_MENU_CATEGORY_ID, activeCatIds));
      }
      criteria.setProjection(null);
      criteria.addOrder(Order.asc(MenuGroup.PROP_SORT_ORDER));
      
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuGroup> findEnabledByParent(MenuCategory menuCategory) throws PosException
  {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(MenuGroup.PROP_VISIBLE, Boolean.TRUE));
      
      if (menuCategory != null) {
        criteria.add(Restrictions.eq(MenuGroup.PROP_MENU_CATEGORY_ID, menuCategory.getId()));
      }
      criteria.addOrder(Order.asc(MenuGroup.PROP_SORT_ORDER));
      
      List<MenuGroup> list = criteria.list();
      Object localObject1; if (menuCategory != null) {
        for (localObject1 = list.iterator(); ((Iterator)localObject1).hasNext();) { MenuGroup menuGroup = (MenuGroup)((Iterator)localObject1).next();
          menuGroup.setBeverageCategory(menuCategory.isBeverage().booleanValue());
          menuGroup.setCategoryName(menuCategory.getDisplayName());
        }
      }
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public List<String> findEnabledGroupsIdsByParent(MenuCategory menuCategory) throws PosException
  {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.setProjection(Projections.property(MenuGroup.PROP_ID));
      criteria.add(Restrictions.eq(MenuGroup.PROP_VISIBLE, Boolean.TRUE));
      
      if (menuCategory != null) {
        criteria.add(Restrictions.eq(MenuGroup.PROP_MENU_CATEGORY_ID, menuCategory.getId()));
      }
      criteria.addOrder(Order.asc(MenuGroup.PROP_SORT_ORDER));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuGroup> findByParent(MenuCategory menuCategory) throws PosException {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      if (menuCategory != null) {
        criteria.add(Restrictions.eq(MenuGroup.PROP_MENU_CATEGORY_ID, menuCategory.getId()));
      }
      criteria.addOrder(Order.asc(MenuGroup.PROP_SORT_ORDER));
      
      List<MenuGroup> list = criteria.list();
      Object localObject1; if (menuCategory != null) {
        for (localObject1 = list.iterator(); ((Iterator)localObject1).hasNext();) { MenuGroup menuGroup = (MenuGroup)((Iterator)localObject1).next();
          menuGroup.setBeverageCategory(menuCategory.isBeverage().booleanValue());
          menuGroup.setCategoryName(menuCategory.getDisplayName());
        }
      }
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public boolean hasChildren(Terminal terminal, MenuGroup group, OrderType orderType) throws PosException
  {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(MenuItem.class);
      
      criteria.add(Restrictions.eq(MenuItem.PROP_VISIBLE, Boolean.TRUE));
      



      criteria.setProjection(Projections.rowCount());
      




      Number rowCount = (Number)criteria.uniqueResult();
      return rowCount.intValue() > 0;
    } catch (Exception e) {
      PosLog.error(getClass(), e);
      throw new PosException("");
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public void releaseParent(List<MenuGroup> menuGroupList) {
    if (menuGroupList == null) {
      return;
    }
    
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      for (MenuGroup menuGroup : menuGroupList) {
        menuGroup.setMenuCategoryId(null);
        session.saveOrUpdate(menuGroup);
      }
      
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(ShopTableDAO.class).error(e);
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
  
  public void releaseParentAndDelete(MenuGroup group) {
    if (group == null) {
      return;
    }
    initialize(group);
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      String queryString = "delete from MENUGROUP_DISCOUNT where MENUGROUP_ID='%s'";
      queryString = String.format(queryString, new Object[] { group.getId() });
      Query query = session.createSQLQuery(queryString);
      query.executeUpdate();
      
      String queryString2 = "update MENU_ITEM set GROUP_ID=null where GROUP_ID='%s'";
      queryString2 = String.format(queryString2, new Object[] { group.getId() });
      Query query2 = session.createSQLQuery(queryString2);
      query2.executeUpdate();
      
      session.delete(group);
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(ShopTableDAO.class).error(e);
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
  
  public boolean existsMenuGroups(MenuCategory menuCategory) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.setProjection(Projections.rowCount());
      if (menuCategory != null) {
        criteria.add(Restrictions.eq(MenuGroup.PROP_MENU_CATEGORY_ID, menuCategory.getId()));
      }
      Number rowCount = (Number)criteria.uniqueResult();
      return (rowCount != null) && (rowCount.intValue() > 0);
    } finally {
      closeSession(session);
    }
  }
  
  public void loadMenuGroups(PaginationSupport paginationSupport, String itemName, MenuCategory menuCategory, String... fields) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuGroup.class);
      
      if (StringUtils.isNotEmpty(itemName)) {
        criteria.add(Restrictions.ilike(MenuGroup.PROP_NAME, itemName.trim(), MatchMode.ANYWHERE));
      }
      if (menuCategory != null) {
        criteria.add(Restrictions.eq(MenuGroup.PROP_MENU_CATEGORY_ID, menuCategory.getId()));
      }
      
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        paginationSupport.setNumRows(rowCount.intValue());
      }
      
      criteria.setProjection(null);
      criteria.addOrder(Order.asc(MenuGroup.PROP_SORT_ORDER));
      criteria.addOrder(Order.asc(MenuGroup.PROP_NAME));
      
      if ((fields != null) && (fields.length > 0)) {
        ProjectionList projectionList = Projections.projectionList();
        for (String field : fields) {
          projectionList.add(Projections.property(field), field);
        }
        criteria.setProjection(projectionList);
        criteria.setResultTransformer(Transformers.aliasToBean(MenuGroup.class));
        paginationSupport.setRows(criteria.list());
      }
      else {
        paginationSupport.setRows(criteria.list());
      }
    }
    finally {
      closeSession(session);
    }
  }
  
  public List<MenuGroup> findGroupsWithInventoryItems() throws PosException {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(MenuItem.class);
      criteria.add(Restrictions.eq(MenuItem.PROP_INVENTORY_ITEM, Boolean.TRUE));
      criteria.setProjection(Projections.property(MenuItem.PROP_MENU_GROUP_ID));
      List<String> groupIdList = criteria.list();
      
      criteria = session.createCriteria(MenuGroup.class);
      criteria.add(Restrictions.in(MenuGroup.PROP_ID, groupIdList));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
}
