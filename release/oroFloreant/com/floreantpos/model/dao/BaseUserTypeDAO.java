package com.floreantpos.model.dao;

import com.floreantpos.model.UserType;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseUserTypeDAO
  extends _RootDAO
{
  public static UserTypeDAO instance;
  
  public BaseUserTypeDAO() {}
  
  public static UserTypeDAO getInstance()
  {
    if (null == instance) instance = new UserTypeDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return UserType.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public UserType cast(Object object)
  {
    return (UserType)object;
  }
  
  public UserType get(String key) throws HibernateException
  {
    return (UserType)get(getReferenceClass(), key);
  }
  
  public UserType get(String key, Session s) throws HibernateException
  {
    return (UserType)get(getReferenceClass(), key, s);
  }
  
  public UserType load(String key) throws HibernateException
  {
    return (UserType)load(getReferenceClass(), key);
  }
  
  public UserType load(String key, Session s) throws HibernateException
  {
    return (UserType)load(getReferenceClass(), key, s);
  }
  
  public UserType loadInitialize(String key, Session s) throws HibernateException
  {
    UserType obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<UserType> findAll()
  {
    return super.findAll();
  }
  


  public List<UserType> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<UserType> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(UserType userType)
    throws HibernateException
  {
    return (String)super.save(userType);
  }
  







  public String save(UserType userType, Session s)
    throws HibernateException
  {
    return (String)save(userType, s);
  }
  





  public void saveOrUpdate(UserType userType)
    throws HibernateException
  {
    saveOrUpdate(userType);
  }
  







  public void saveOrUpdate(UserType userType, Session s)
    throws HibernateException
  {
    saveOrUpdate(userType, s);
  }
  




  public void update(UserType userType)
    throws HibernateException
  {
    update(userType);
  }
  






  public void update(UserType userType, Session s)
    throws HibernateException
  {
    update(userType, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(UserType userType)
    throws HibernateException
  {
    delete(userType);
  }
  






  public void delete(UserType userType, Session s)
    throws HibernateException
  {
    delete(userType, s);
  }
  









  public void refresh(UserType userType, Session s)
    throws HibernateException
  {
    refresh(userType, s);
  }
}
