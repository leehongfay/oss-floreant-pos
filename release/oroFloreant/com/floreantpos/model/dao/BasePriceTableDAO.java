package com.floreantpos.model.dao;

import com.floreantpos.model.PriceTable;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BasePriceTableDAO
  extends _RootDAO
{
  public static PriceTableDAO instance;
  
  public BasePriceTableDAO() {}
  
  public static PriceTableDAO getInstance()
  {
    if (null == instance) instance = new PriceTableDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return PriceTable.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public PriceTable cast(Object object)
  {
    return (PriceTable)object;
  }
  
  public PriceTable get(String key) throws HibernateException
  {
    return (PriceTable)get(getReferenceClass(), key);
  }
  
  public PriceTable get(String key, Session s) throws HibernateException
  {
    return (PriceTable)get(getReferenceClass(), key, s);
  }
  
  public PriceTable load(String key) throws HibernateException
  {
    return (PriceTable)load(getReferenceClass(), key);
  }
  
  public PriceTable load(String key, Session s) throws HibernateException
  {
    return (PriceTable)load(getReferenceClass(), key, s);
  }
  
  public PriceTable loadInitialize(String key, Session s) throws HibernateException
  {
    PriceTable obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<PriceTable> findAll()
  {
    return super.findAll();
  }
  


  public List<PriceTable> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<PriceTable> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(PriceTable priceTable)
    throws HibernateException
  {
    return (String)super.save(priceTable);
  }
  







  public String save(PriceTable priceTable, Session s)
    throws HibernateException
  {
    return (String)save(priceTable, s);
  }
  





  public void saveOrUpdate(PriceTable priceTable)
    throws HibernateException
  {
    saveOrUpdate(priceTable);
  }
  







  public void saveOrUpdate(PriceTable priceTable, Session s)
    throws HibernateException
  {
    saveOrUpdate(priceTable, s);
  }
  




  public void update(PriceTable priceTable)
    throws HibernateException
  {
    update(priceTable);
  }
  






  public void update(PriceTable priceTable, Session s)
    throws HibernateException
  {
    update(priceTable, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(PriceTable priceTable)
    throws HibernateException
  {
    delete(priceTable);
  }
  






  public void delete(PriceTable priceTable, Session s)
    throws HibernateException
  {
    delete(priceTable, s);
  }
  









  public void refresh(PriceTable priceTable, Session s)
    throws HibernateException
  {
    refresh(priceTable, s);
  }
}
