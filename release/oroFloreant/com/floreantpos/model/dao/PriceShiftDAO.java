package com.floreantpos.model.dao;

import com.floreantpos.PosException;
import com.floreantpos.model.PriceShift;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;




public class PriceShiftDAO
  extends BasePriceShiftDAO
{
  public PriceShiftDAO() {}
  
  public boolean exists(String shiftName)
    throws PosException
  {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(PriceShift.PROP_NAME, shiftName));
      List list = criteria.list();
      boolean bool; if ((list != null) && (list.size() > 0)) {
        return true;
      }
      return false;
    } catch (Exception e) {
      throw new PosException("An error occured while trying to check price shift duplicacy", e);
    } finally {
      if (session != null) {
        try {
          session.close();
        }
        catch (Exception localException3) {}
      }
    }
  }
  






















  public void releaseParentAndDelete(PriceShift priceShift)
  {
    if (priceShift == null) {
      return;
    }
    
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      String queryString = "update PRICE_RULE set PRICE_SHIFT_ID=null where PRICE_SHIFT_ID='%s'";
      queryString = String.format(queryString, new Object[] { priceShift.getId() });
      Query query = session.createSQLQuery(queryString);
      query.executeUpdate();
      
      session.delete(priceShift);
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
  
  public List<PriceShift> findAllActive() {
    Session session = null;
    try {
      session = createNewSession();
      
      return findAllActive(session);
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public List<PriceShift> findAllActive(Session session) {
    Criteria criteria = session.createCriteria(getReferenceClass());
    criteria.add(Restrictions.eq(PriceShift.PROP_ENABLE, Boolean.TRUE));
    criteria.addOrder(Order.asc(PriceShift.PROP_PRIORITY));
    return criteria.list();
  }
}
