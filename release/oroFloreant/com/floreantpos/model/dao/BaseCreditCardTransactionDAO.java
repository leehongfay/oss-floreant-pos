package com.floreantpos.model.dao;

import com.floreantpos.model.CreditCardTransaction;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseCreditCardTransactionDAO
  extends _RootDAO
{
  public static CreditCardTransactionDAO instance;
  
  public BaseCreditCardTransactionDAO() {}
  
  public static CreditCardTransactionDAO getInstance()
  {
    if (null == instance) instance = new CreditCardTransactionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return CreditCardTransaction.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public CreditCardTransaction cast(Object object)
  {
    return (CreditCardTransaction)object;
  }
  
  public CreditCardTransaction get(String key) throws HibernateException
  {
    return (CreditCardTransaction)get(getReferenceClass(), key);
  }
  
  public CreditCardTransaction get(String key, Session s) throws HibernateException
  {
    return (CreditCardTransaction)get(getReferenceClass(), key, s);
  }
  
  public CreditCardTransaction load(String key) throws HibernateException
  {
    return (CreditCardTransaction)load(getReferenceClass(), key);
  }
  
  public CreditCardTransaction load(String key, Session s) throws HibernateException
  {
    return (CreditCardTransaction)load(getReferenceClass(), key, s);
  }
  
  public CreditCardTransaction loadInitialize(String key, Session s) throws HibernateException
  {
    CreditCardTransaction obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<CreditCardTransaction> findAll()
  {
    return super.findAll();
  }
  


  public List<CreditCardTransaction> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<CreditCardTransaction> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(CreditCardTransaction creditCardTransaction)
    throws HibernateException
  {
    return (String)super.save(creditCardTransaction);
  }
  







  public String save(CreditCardTransaction creditCardTransaction, Session s)
    throws HibernateException
  {
    return (String)save(creditCardTransaction, s);
  }
  





  public void saveOrUpdate(CreditCardTransaction creditCardTransaction)
    throws HibernateException
  {
    saveOrUpdate(creditCardTransaction);
  }
  







  public void saveOrUpdate(CreditCardTransaction creditCardTransaction, Session s)
    throws HibernateException
  {
    saveOrUpdate(creditCardTransaction, s);
  }
  




  public void update(CreditCardTransaction creditCardTransaction)
    throws HibernateException
  {
    update(creditCardTransaction);
  }
  






  public void update(CreditCardTransaction creditCardTransaction, Session s)
    throws HibernateException
  {
    update(creditCardTransaction, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(CreditCardTransaction creditCardTransaction)
    throws HibernateException
  {
    delete(creditCardTransaction);
  }
  






  public void delete(CreditCardTransaction creditCardTransaction, Session s)
    throws HibernateException
  {
    delete(creditCardTransaction, s);
  }
  









  public void refresh(CreditCardTransaction creditCardTransaction, Session s)
    throws HibernateException
  {
    refresh(creditCardTransaction, s);
  }
}
