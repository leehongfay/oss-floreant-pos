package com.floreantpos.model.dao;

import com.floreantpos.model.PizzaCrust;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;






public class PizzaCrustDAO
  extends BasePizzaCrustDAO
{
  public PizzaCrustDAO() {}
  
  public Order getDefaultOrder()
  {
    return Order.asc(PizzaCrust.PROP_SORT_ORDER);
  }
  
  public void saveOrUpdateList(List<PizzaCrust> items) {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      saveOrUpdateCrustList(items, session);
      tx.commit();
    }
    catch (Exception e) {
      throw e;
    } finally {
      session.close();
    }
  }
  
  public void saveOrUpdateCrustList(List<PizzaCrust> items, Session session) {
    for (Iterator iterator = items.iterator(); iterator.hasNext();) {
      PizzaCrust pizzaCrust = (PizzaCrust)iterator.next();
      session.saveOrUpdate(pizzaCrust);
    }
  }
}
