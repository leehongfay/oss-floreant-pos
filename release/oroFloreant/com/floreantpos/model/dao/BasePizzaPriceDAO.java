package com.floreantpos.model.dao;

import com.floreantpos.model.PizzaPrice;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BasePizzaPriceDAO
  extends _RootDAO
{
  public static PizzaPriceDAO instance;
  
  public BasePizzaPriceDAO() {}
  
  public static PizzaPriceDAO getInstance()
  {
    if (null == instance) instance = new PizzaPriceDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return PizzaPrice.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public PizzaPrice cast(Object object)
  {
    return (PizzaPrice)object;
  }
  
  public PizzaPrice get(String key) throws HibernateException
  {
    return (PizzaPrice)get(getReferenceClass(), key);
  }
  
  public PizzaPrice get(String key, Session s) throws HibernateException
  {
    return (PizzaPrice)get(getReferenceClass(), key, s);
  }
  
  public PizzaPrice load(String key) throws HibernateException
  {
    return (PizzaPrice)load(getReferenceClass(), key);
  }
  
  public PizzaPrice load(String key, Session s) throws HibernateException
  {
    return (PizzaPrice)load(getReferenceClass(), key, s);
  }
  
  public PizzaPrice loadInitialize(String key, Session s) throws HibernateException
  {
    PizzaPrice obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<PizzaPrice> findAll()
  {
    return super.findAll();
  }
  


  public List<PizzaPrice> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<PizzaPrice> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(PizzaPrice pizzaPrice)
    throws HibernateException
  {
    return (String)super.save(pizzaPrice);
  }
  







  public String save(PizzaPrice pizzaPrice, Session s)
    throws HibernateException
  {
    return (String)save(pizzaPrice, s);
  }
  





  public void saveOrUpdate(PizzaPrice pizzaPrice)
    throws HibernateException
  {
    saveOrUpdate(pizzaPrice);
  }
  







  public void saveOrUpdate(PizzaPrice pizzaPrice, Session s)
    throws HibernateException
  {
    saveOrUpdate(pizzaPrice, s);
  }
  




  public void update(PizzaPrice pizzaPrice)
    throws HibernateException
  {
    update(pizzaPrice);
  }
  






  public void update(PizzaPrice pizzaPrice, Session s)
    throws HibernateException
  {
    update(pizzaPrice, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(PizzaPrice pizzaPrice)
    throws HibernateException
  {
    delete(pizzaPrice);
  }
  






  public void delete(PizzaPrice pizzaPrice, Session s)
    throws HibernateException
  {
    delete(pizzaPrice, s);
  }
  









  public void refresh(PizzaPrice pizzaPrice, Session s)
    throws HibernateException
  {
    refresh(pizzaPrice, s);
  }
}
