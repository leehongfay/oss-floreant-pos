package com.floreantpos.model.dao;

import com.floreantpos.model.GuestCheckPrint;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseGuestCheckPrintDAO
  extends _RootDAO
{
  public static GuestCheckPrintDAO instance;
  
  public BaseGuestCheckPrintDAO() {}
  
  public static GuestCheckPrintDAO getInstance()
  {
    if (null == instance) instance = new GuestCheckPrintDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return GuestCheckPrint.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public GuestCheckPrint cast(Object object)
  {
    return (GuestCheckPrint)object;
  }
  
  public GuestCheckPrint get(String key) throws HibernateException
  {
    return (GuestCheckPrint)get(getReferenceClass(), key);
  }
  
  public GuestCheckPrint get(String key, Session s) throws HibernateException
  {
    return (GuestCheckPrint)get(getReferenceClass(), key, s);
  }
  
  public GuestCheckPrint load(String key) throws HibernateException
  {
    return (GuestCheckPrint)load(getReferenceClass(), key);
  }
  
  public GuestCheckPrint load(String key, Session s) throws HibernateException
  {
    return (GuestCheckPrint)load(getReferenceClass(), key, s);
  }
  
  public GuestCheckPrint loadInitialize(String key, Session s) throws HibernateException
  {
    GuestCheckPrint obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<GuestCheckPrint> findAll()
  {
    return super.findAll();
  }
  


  public List<GuestCheckPrint> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<GuestCheckPrint> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(GuestCheckPrint guestCheckPrint)
    throws HibernateException
  {
    return (String)super.save(guestCheckPrint);
  }
  







  public String save(GuestCheckPrint guestCheckPrint, Session s)
    throws HibernateException
  {
    return (String)save(guestCheckPrint, s);
  }
  





  public void saveOrUpdate(GuestCheckPrint guestCheckPrint)
    throws HibernateException
  {
    saveOrUpdate(guestCheckPrint);
  }
  







  public void saveOrUpdate(GuestCheckPrint guestCheckPrint, Session s)
    throws HibernateException
  {
    saveOrUpdate(guestCheckPrint, s);
  }
  




  public void update(GuestCheckPrint guestCheckPrint)
    throws HibernateException
  {
    update(guestCheckPrint);
  }
  






  public void update(GuestCheckPrint guestCheckPrint, Session s)
    throws HibernateException
  {
    update(guestCheckPrint, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(GuestCheckPrint guestCheckPrint)
    throws HibernateException
  {
    delete(guestCheckPrint);
  }
  






  public void delete(GuestCheckPrint guestCheckPrint, Session s)
    throws HibernateException
  {
    delete(guestCheckPrint, s);
  }
  









  public void refresh(GuestCheckPrint guestCheckPrint, Session s)
    throws HibernateException
  {
    refresh(guestCheckPrint, s);
  }
}
