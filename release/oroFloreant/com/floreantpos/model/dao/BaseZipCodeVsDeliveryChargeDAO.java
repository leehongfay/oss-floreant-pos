package com.floreantpos.model.dao;

import com.floreantpos.model.ZipCodeVsDeliveryCharge;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseZipCodeVsDeliveryChargeDAO
  extends _RootDAO
{
  public static ZipCodeVsDeliveryChargeDAO instance;
  
  public BaseZipCodeVsDeliveryChargeDAO() {}
  
  public static ZipCodeVsDeliveryChargeDAO getInstance()
  {
    if (null == instance) instance = new ZipCodeVsDeliveryChargeDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return ZipCodeVsDeliveryCharge.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public ZipCodeVsDeliveryCharge cast(Object object)
  {
    return (ZipCodeVsDeliveryCharge)object;
  }
  
  public ZipCodeVsDeliveryCharge get(String key) throws HibernateException
  {
    return (ZipCodeVsDeliveryCharge)get(getReferenceClass(), key);
  }
  
  public ZipCodeVsDeliveryCharge get(String key, Session s) throws HibernateException
  {
    return (ZipCodeVsDeliveryCharge)get(getReferenceClass(), key, s);
  }
  
  public ZipCodeVsDeliveryCharge load(String key) throws HibernateException
  {
    return (ZipCodeVsDeliveryCharge)load(getReferenceClass(), key);
  }
  
  public ZipCodeVsDeliveryCharge load(String key, Session s) throws HibernateException
  {
    return (ZipCodeVsDeliveryCharge)load(getReferenceClass(), key, s);
  }
  
  public ZipCodeVsDeliveryCharge loadInitialize(String key, Session s) throws HibernateException
  {
    ZipCodeVsDeliveryCharge obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<ZipCodeVsDeliveryCharge> findAll()
  {
    return super.findAll();
  }
  


  public List<ZipCodeVsDeliveryCharge> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<ZipCodeVsDeliveryCharge> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(ZipCodeVsDeliveryCharge zipCodeVsDeliveryCharge)
    throws HibernateException
  {
    return (String)super.save(zipCodeVsDeliveryCharge);
  }
  







  public String save(ZipCodeVsDeliveryCharge zipCodeVsDeliveryCharge, Session s)
    throws HibernateException
  {
    return (String)save(zipCodeVsDeliveryCharge, s);
  }
  





  public void saveOrUpdate(ZipCodeVsDeliveryCharge zipCodeVsDeliveryCharge)
    throws HibernateException
  {
    saveOrUpdate(zipCodeVsDeliveryCharge);
  }
  







  public void saveOrUpdate(ZipCodeVsDeliveryCharge zipCodeVsDeliveryCharge, Session s)
    throws HibernateException
  {
    saveOrUpdate(zipCodeVsDeliveryCharge, s);
  }
  




  public void update(ZipCodeVsDeliveryCharge zipCodeVsDeliveryCharge)
    throws HibernateException
  {
    update(zipCodeVsDeliveryCharge);
  }
  






  public void update(ZipCodeVsDeliveryCharge zipCodeVsDeliveryCharge, Session s)
    throws HibernateException
  {
    update(zipCodeVsDeliveryCharge, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(ZipCodeVsDeliveryCharge zipCodeVsDeliveryCharge)
    throws HibernateException
  {
    delete(zipCodeVsDeliveryCharge);
  }
  






  public void delete(ZipCodeVsDeliveryCharge zipCodeVsDeliveryCharge, Session s)
    throws HibernateException
  {
    delete(zipCodeVsDeliveryCharge, s);
  }
  









  public void refresh(ZipCodeVsDeliveryCharge zipCodeVsDeliveryCharge, Session s)
    throws HibernateException
  {
    refresh(zipCodeVsDeliveryCharge, s);
  }
}
