package com.floreantpos.model.dao;

import com.floreantpos.model.MenuItem;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseMenuItemDAO
  extends _RootDAO
{
  public static MenuItemDAO instance;
  
  public BaseMenuItemDAO() {}
  
  public static MenuItemDAO getInstance()
  {
    if (null == instance) instance = new MenuItemDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return MenuItem.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public MenuItem cast(Object object)
  {
    return (MenuItem)object;
  }
  
  public MenuItem get(String key) throws HibernateException
  {
    return (MenuItem)get(getReferenceClass(), key);
  }
  
  public MenuItem get(String key, Session s) throws HibernateException
  {
    return (MenuItem)get(getReferenceClass(), key, s);
  }
  
  public MenuItem load(String key) throws HibernateException
  {
    return (MenuItem)load(getReferenceClass(), key);
  }
  
  public MenuItem load(String key, Session s) throws HibernateException
  {
    return (MenuItem)load(getReferenceClass(), key, s);
  }
  
  public MenuItem loadInitialize(String key, Session s) throws HibernateException
  {
    MenuItem obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<MenuItem> findAll()
  {
    return super.findAll();
  }
  


  public List<MenuItem> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<MenuItem> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(MenuItem menuItem)
    throws HibernateException
  {
    return (String)super.save(menuItem);
  }
  







  public String save(MenuItem menuItem, Session s)
    throws HibernateException
  {
    return (String)save(menuItem, s);
  }
  





  public void saveOrUpdate(MenuItem menuItem)
    throws HibernateException
  {
    saveOrUpdate(menuItem);
  }
  







  public void saveOrUpdate(MenuItem menuItem, Session s)
    throws HibernateException
  {
    saveOrUpdate(menuItem, s);
  }
  




  public void update(MenuItem menuItem)
    throws HibernateException
  {
    update(menuItem);
  }
  






  public void update(MenuItem menuItem, Session s)
    throws HibernateException
  {
    update(menuItem, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(MenuItem menuItem)
    throws HibernateException
  {
    delete(menuItem);
  }
  






  public void delete(MenuItem menuItem, Session s)
    throws HibernateException
  {
    delete(menuItem, s);
  }
  









  public void refresh(MenuItem menuItem, Session s)
    throws HibernateException
  {
    refresh(menuItem, s);
  }
}
