package com.floreantpos.model.dao;

import com.floreantpos.model.PayOutTransaction;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BasePayOutTransactionDAO
  extends _RootDAO
{
  public static PayOutTransactionDAO instance;
  
  public BasePayOutTransactionDAO() {}
  
  public static PayOutTransactionDAO getInstance()
  {
    if (null == instance) instance = new PayOutTransactionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return PayOutTransaction.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public PayOutTransaction cast(Object object)
  {
    return (PayOutTransaction)object;
  }
  
  public PayOutTransaction get(String key) throws HibernateException
  {
    return (PayOutTransaction)get(getReferenceClass(), key);
  }
  
  public PayOutTransaction get(String key, Session s) throws HibernateException
  {
    return (PayOutTransaction)get(getReferenceClass(), key, s);
  }
  
  public PayOutTransaction load(String key) throws HibernateException
  {
    return (PayOutTransaction)load(getReferenceClass(), key);
  }
  
  public PayOutTransaction load(String key, Session s) throws HibernateException
  {
    return (PayOutTransaction)load(getReferenceClass(), key, s);
  }
  
  public PayOutTransaction loadInitialize(String key, Session s) throws HibernateException
  {
    PayOutTransaction obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<PayOutTransaction> findAll()
  {
    return super.findAll();
  }
  


  public List<PayOutTransaction> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<PayOutTransaction> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(PayOutTransaction payOutTransaction)
    throws HibernateException
  {
    return (String)super.save(payOutTransaction);
  }
  







  public String save(PayOutTransaction payOutTransaction, Session s)
    throws HibernateException
  {
    return (String)save(payOutTransaction, s);
  }
  





  public void saveOrUpdate(PayOutTransaction payOutTransaction)
    throws HibernateException
  {
    saveOrUpdate(payOutTransaction);
  }
  







  public void saveOrUpdate(PayOutTransaction payOutTransaction, Session s)
    throws HibernateException
  {
    saveOrUpdate(payOutTransaction, s);
  }
  




  public void update(PayOutTransaction payOutTransaction)
    throws HibernateException
  {
    update(payOutTransaction);
  }
  






  public void update(PayOutTransaction payOutTransaction, Session s)
    throws HibernateException
  {
    update(payOutTransaction, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(PayOutTransaction payOutTransaction)
    throws HibernateException
  {
    delete(payOutTransaction);
  }
  






  public void delete(PayOutTransaction payOutTransaction, Session s)
    throws HibernateException
  {
    delete(payOutTransaction, s);
  }
  









  public void refresh(PayOutTransaction payOutTransaction, Session s)
    throws HibernateException
  {
    refresh(payOutTransaction, s);
  }
}
