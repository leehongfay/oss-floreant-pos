package com.floreantpos.model.dao;

import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.PurchaseOrderItem;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;




public class PurchaseOrderItemDAO
  extends BasePurchaseOrderItemDAO
{
  public PurchaseOrderItemDAO() {}
  
  public List<MenuItem> getMenuItems(InventoryVendor vendor)
  {
    Session session = null;
    Criteria criteria = null;
    try {
      List<MenuItem> menuItems = new ArrayList();
      session = getSession();
      criteria = session.createCriteria(PurchaseOrderItem.class);
      criteria.setProjection(Projections.distinct(Projections.property(PurchaseOrderItem.PROP_MENU_ITEM_ID)));
      criteria.createAlias(PurchaseOrderItem.PROP_PURCHASE_ORDER, "p");
      criteria.add(Restrictions.eq("p.vendor", vendor));
      
      List<Integer> list = criteria.list();
      Object localObject1;
      if (list != null) {
        for (localObject1 = list.iterator(); ((Iterator)localObject1).hasNext();) { Integer item = (Integer)((Iterator)localObject1).next();
          MenuItem menuItem = MenuItemDAO.getInstance().getReplenishedMenuItem(item, session);
          if (menuItem != null) {
            menuItems.add(menuItem);
          }
        }
      }
      return menuItems;
    } finally {
      closeSession(session);
    }
  }
}
