package com.floreantpos.model.dao;

import com.floreantpos.model.ShopTableType;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseShopTableTypeDAO
  extends _RootDAO
{
  public static ShopTableTypeDAO instance;
  
  public BaseShopTableTypeDAO() {}
  
  public static ShopTableTypeDAO getInstance()
  {
    if (null == instance) instance = new ShopTableTypeDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return ShopTableType.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public ShopTableType cast(Object object)
  {
    return (ShopTableType)object;
  }
  
  public ShopTableType get(String key) throws HibernateException
  {
    return (ShopTableType)get(getReferenceClass(), key);
  }
  
  public ShopTableType get(String key, Session s) throws HibernateException
  {
    return (ShopTableType)get(getReferenceClass(), key, s);
  }
  
  public ShopTableType load(String key) throws HibernateException
  {
    return (ShopTableType)load(getReferenceClass(), key);
  }
  
  public ShopTableType load(String key, Session s) throws HibernateException
  {
    return (ShopTableType)load(getReferenceClass(), key, s);
  }
  
  public ShopTableType loadInitialize(String key, Session s) throws HibernateException
  {
    ShopTableType obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<ShopTableType> findAll()
  {
    return super.findAll();
  }
  


  public List<ShopTableType> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<ShopTableType> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(ShopTableType shopTableType)
    throws HibernateException
  {
    return (String)super.save(shopTableType);
  }
  







  public String save(ShopTableType shopTableType, Session s)
    throws HibernateException
  {
    return (String)save(shopTableType, s);
  }
  





  public void saveOrUpdate(ShopTableType shopTableType)
    throws HibernateException
  {
    saveOrUpdate(shopTableType);
  }
  







  public void saveOrUpdate(ShopTableType shopTableType, Session s)
    throws HibernateException
  {
    saveOrUpdate(shopTableType, s);
  }
  




  public void update(ShopTableType shopTableType)
    throws HibernateException
  {
    update(shopTableType);
  }
  






  public void update(ShopTableType shopTableType, Session s)
    throws HibernateException
  {
    update(shopTableType, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(ShopTableType shopTableType)
    throws HibernateException
  {
    delete(shopTableType);
  }
  






  public void delete(ShopTableType shopTableType, Session s)
    throws HibernateException
  {
    delete(shopTableType, s);
  }
  









  public void refresh(ShopTableType shopTableType, Session s)
    throws HibernateException
  {
    refresh(shopTableType, s);
  }
}
