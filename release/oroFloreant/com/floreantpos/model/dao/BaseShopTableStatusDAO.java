package com.floreantpos.model.dao;

import com.floreantpos.model.ShopTableStatus;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseShopTableStatusDAO
  extends _RootDAO
{
  public static ShopTableStatusDAO instance;
  
  public BaseShopTableStatusDAO() {}
  
  public static ShopTableStatusDAO getInstance()
  {
    if (null == instance) instance = new ShopTableStatusDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return ShopTableStatus.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public ShopTableStatus cast(Object object)
  {
    return (ShopTableStatus)object;
  }
  
  public ShopTableStatus get(Integer key) throws HibernateException
  {
    return (ShopTableStatus)get(getReferenceClass(), key);
  }
  
  public ShopTableStatus get(Integer key, Session s) throws HibernateException
  {
    return (ShopTableStatus)get(getReferenceClass(), key, s);
  }
  
  public ShopTableStatus load(Integer key) throws HibernateException
  {
    return (ShopTableStatus)load(getReferenceClass(), key);
  }
  
  public ShopTableStatus load(Integer key, Session s) throws HibernateException
  {
    return (ShopTableStatus)load(getReferenceClass(), key, s);
  }
  
  public ShopTableStatus loadInitialize(Integer key, Session s) throws HibernateException
  {
    ShopTableStatus obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<ShopTableStatus> findAll()
  {
    return super.findAll();
  }
  


  public List<ShopTableStatus> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<ShopTableStatus> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public Integer save(ShopTableStatus shopTableStatus)
    throws HibernateException
  {
    return (Integer)super.save(shopTableStatus);
  }
  







  public Integer save(ShopTableStatus shopTableStatus, Session s)
    throws HibernateException
  {
    return (Integer)save(shopTableStatus, s);
  }
  





  public void saveOrUpdate(ShopTableStatus shopTableStatus)
    throws HibernateException
  {
    saveOrUpdate(shopTableStatus);
  }
  







  public void saveOrUpdate(ShopTableStatus shopTableStatus, Session s)
    throws HibernateException
  {
    saveOrUpdate(shopTableStatus, s);
  }
  




  public void update(ShopTableStatus shopTableStatus)
    throws HibernateException
  {
    update(shopTableStatus);
  }
  






  public void update(ShopTableStatus shopTableStatus, Session s)
    throws HibernateException
  {
    update(shopTableStatus, s);
  }
  




  public void delete(Integer id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(Integer id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(ShopTableStatus shopTableStatus)
    throws HibernateException
  {
    delete(shopTableStatus);
  }
  






  public void delete(ShopTableStatus shopTableStatus, Session s)
    throws HibernateException
  {
    delete(shopTableStatus, s);
  }
  









  public void refresh(ShopTableStatus shopTableStatus, Session s)
    throws HibernateException
  {
    refresh(shopTableStatus, s);
  }
}
