package com.floreantpos.model.dao;

import com.floreantpos.model.StockCount;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseStockCountDAO
  extends _RootDAO
{
  public static StockCountDAO instance;
  
  public BaseStockCountDAO() {}
  
  public static StockCountDAO getInstance()
  {
    if (null == instance) instance = new StockCountDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return StockCount.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public StockCount cast(Object object)
  {
    return (StockCount)object;
  }
  
  public StockCount get(String key) throws HibernateException
  {
    return (StockCount)get(getReferenceClass(), key);
  }
  
  public StockCount get(String key, Session s) throws HibernateException
  {
    return (StockCount)get(getReferenceClass(), key, s);
  }
  
  public StockCount load(String key) throws HibernateException
  {
    return (StockCount)load(getReferenceClass(), key);
  }
  
  public StockCount load(String key, Session s) throws HibernateException
  {
    return (StockCount)load(getReferenceClass(), key, s);
  }
  
  public StockCount loadInitialize(String key, Session s) throws HibernateException
  {
    StockCount obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<StockCount> findAll()
  {
    return super.findAll();
  }
  


  public List<StockCount> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<StockCount> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(StockCount stockCount)
    throws HibernateException
  {
    return (String)super.save(stockCount);
  }
  







  public String save(StockCount stockCount, Session s)
    throws HibernateException
  {
    return (String)save(stockCount, s);
  }
  





  public void saveOrUpdate(StockCount stockCount)
    throws HibernateException
  {
    saveOrUpdate(stockCount);
  }
  







  public void saveOrUpdate(StockCount stockCount, Session s)
    throws HibernateException
  {
    saveOrUpdate(stockCount, s);
  }
  




  public void update(StockCount stockCount)
    throws HibernateException
  {
    update(stockCount);
  }
  






  public void update(StockCount stockCount, Session s)
    throws HibernateException
  {
    update(stockCount, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(StockCount stockCount)
    throws HibernateException
  {
    delete(stockCount);
  }
  






  public void delete(StockCount stockCount, Session s)
    throws HibernateException
  {
    delete(stockCount, s);
  }
  









  public void refresh(StockCount stockCount, Session s)
    throws HibernateException
  {
    refresh(stockCount, s);
  }
}
