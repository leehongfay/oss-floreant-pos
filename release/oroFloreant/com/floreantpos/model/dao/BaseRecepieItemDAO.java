package com.floreantpos.model.dao;

import com.floreantpos.model.RecepieItem;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseRecepieItemDAO
  extends _RootDAO
{
  public static RecepieItemDAO instance;
  
  public BaseRecepieItemDAO() {}
  
  public static RecepieItemDAO getInstance()
  {
    if (null == instance) instance = new RecepieItemDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return RecepieItem.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public RecepieItem cast(Object object)
  {
    return (RecepieItem)object;
  }
  
  public RecepieItem get(String key) throws HibernateException
  {
    return (RecepieItem)get(getReferenceClass(), key);
  }
  
  public RecepieItem get(String key, Session s) throws HibernateException
  {
    return (RecepieItem)get(getReferenceClass(), key, s);
  }
  
  public RecepieItem load(String key) throws HibernateException
  {
    return (RecepieItem)load(getReferenceClass(), key);
  }
  
  public RecepieItem load(String key, Session s) throws HibernateException
  {
    return (RecepieItem)load(getReferenceClass(), key, s);
  }
  
  public RecepieItem loadInitialize(String key, Session s) throws HibernateException
  {
    RecepieItem obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<RecepieItem> findAll()
  {
    return super.findAll();
  }
  


  public List<RecepieItem> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<RecepieItem> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(RecepieItem recepieItem)
    throws HibernateException
  {
    return (String)super.save(recepieItem);
  }
  







  public String save(RecepieItem recepieItem, Session s)
    throws HibernateException
  {
    return (String)save(recepieItem, s);
  }
  





  public void saveOrUpdate(RecepieItem recepieItem)
    throws HibernateException
  {
    saveOrUpdate(recepieItem);
  }
  







  public void saveOrUpdate(RecepieItem recepieItem, Session s)
    throws HibernateException
  {
    saveOrUpdate(recepieItem, s);
  }
  




  public void update(RecepieItem recepieItem)
    throws HibernateException
  {
    update(recepieItem);
  }
  






  public void update(RecepieItem recepieItem, Session s)
    throws HibernateException
  {
    update(recepieItem, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(RecepieItem recepieItem)
    throws HibernateException
  {
    delete(recepieItem);
  }
  






  public void delete(RecepieItem recepieItem, Session s)
    throws HibernateException
  {
    delete(recepieItem, s);
  }
  









  public void refresh(RecepieItem recepieItem, Session s)
    throws HibernateException
  {
    refresh(recepieItem, s);
  }
}
