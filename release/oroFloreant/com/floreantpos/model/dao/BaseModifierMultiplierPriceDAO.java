package com.floreantpos.model.dao;

import com.floreantpos.model.ModifierMultiplierPrice;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseModifierMultiplierPriceDAO
  extends _RootDAO
{
  public static ModifierMultiplierPriceDAO instance;
  
  public BaseModifierMultiplierPriceDAO() {}
  
  public static ModifierMultiplierPriceDAO getInstance()
  {
    if (null == instance) instance = new ModifierMultiplierPriceDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return ModifierMultiplierPrice.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public ModifierMultiplierPrice cast(Object object)
  {
    return (ModifierMultiplierPrice)object;
  }
  
  public ModifierMultiplierPrice get(String key) throws HibernateException
  {
    return (ModifierMultiplierPrice)get(getReferenceClass(), key);
  }
  
  public ModifierMultiplierPrice get(String key, Session s) throws HibernateException
  {
    return (ModifierMultiplierPrice)get(getReferenceClass(), key, s);
  }
  
  public ModifierMultiplierPrice load(String key) throws HibernateException
  {
    return (ModifierMultiplierPrice)load(getReferenceClass(), key);
  }
  
  public ModifierMultiplierPrice load(String key, Session s) throws HibernateException
  {
    return (ModifierMultiplierPrice)load(getReferenceClass(), key, s);
  }
  
  public ModifierMultiplierPrice loadInitialize(String key, Session s) throws HibernateException
  {
    ModifierMultiplierPrice obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<ModifierMultiplierPrice> findAll()
  {
    return super.findAll();
  }
  


  public List<ModifierMultiplierPrice> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<ModifierMultiplierPrice> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(ModifierMultiplierPrice modifierMultiplierPrice)
    throws HibernateException
  {
    return (String)super.save(modifierMultiplierPrice);
  }
  







  public String save(ModifierMultiplierPrice modifierMultiplierPrice, Session s)
    throws HibernateException
  {
    return (String)save(modifierMultiplierPrice, s);
  }
  





  public void saveOrUpdate(ModifierMultiplierPrice modifierMultiplierPrice)
    throws HibernateException
  {
    saveOrUpdate(modifierMultiplierPrice);
  }
  







  public void saveOrUpdate(ModifierMultiplierPrice modifierMultiplierPrice, Session s)
    throws HibernateException
  {
    saveOrUpdate(modifierMultiplierPrice, s);
  }
  




  public void update(ModifierMultiplierPrice modifierMultiplierPrice)
    throws HibernateException
  {
    update(modifierMultiplierPrice);
  }
  






  public void update(ModifierMultiplierPrice modifierMultiplierPrice, Session s)
    throws HibernateException
  {
    update(modifierMultiplierPrice, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(ModifierMultiplierPrice modifierMultiplierPrice)
    throws HibernateException
  {
    delete(modifierMultiplierPrice);
  }
  






  public void delete(ModifierMultiplierPrice modifierMultiplierPrice, Session s)
    throws HibernateException
  {
    delete(modifierMultiplierPrice, s);
  }
  









  public void refresh(ModifierMultiplierPrice modifierMultiplierPrice, Session s)
    throws HibernateException
  {
    refresh(modifierMultiplierPrice, s);
  }
}
