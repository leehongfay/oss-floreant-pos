package com.floreantpos.model.dao;

import com.floreantpos.PosException;
import com.floreantpos.model.CashTransaction;
import com.floreantpos.model.DeclaredTips;
import com.floreantpos.model.Gratuity;
import com.floreantpos.model.GratuityPaymentHistory;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.StoreSession;
import com.floreantpos.model.StoreSessionControl;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TipsCashoutReport;
import com.floreantpos.model.TipsCashoutReportData;
import com.floreantpos.model.User;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.util.StoreUtil;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;




















public class GratuityDAO
  extends BaseGratuityDAO
{
  public GratuityDAO() {}
  
  public List<Gratuity> findByUser(User user)
    throws PosException
  {
    return findByUser(user, false);
  }
  
  public List<Gratuity> findByUser(User user, boolean includePaid) throws PosException {
    Session session = null;
    try
    {
      session = getSession();
      
      Criteria criteria = session.createCriteria(getReferenceClass());
      
      if (user != null) {
        criteria.add(Restrictions.eq(Gratuity.PROP_OWNER_ID, user.getId()));
      }
      if (!includePaid) {
        criteria.add(Restrictions.eq(Gratuity.PROP_PAID, Boolean.FALSE));
      }
      criteria.add(Restrictions.eq(Gratuity.PROP_REFUNDED, Boolean.FALSE));
      return criteria.list();
    } catch (Exception e) {
      throw new PosException("" + user.getFirstName() + " " + user.getLastName());
    } finally {
      closeSession(session);
    }
  }
  
  public void payGratuities(List<Gratuity> gratuities, List<GratuityPaymentHistory> gratuityPaymentList) {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      for (Gratuity gratuity : gratuities) {
        session.saveOrUpdate(gratuity);
      }
      for (GratuityPaymentHistory gratuityPayment : gratuityPaymentList) {
        session.saveOrUpdate(gratuityPayment);
      }
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      throw new PosException("");
    } finally {
      closeSession(session);
    }
  }
  
  public TipsCashoutReport createReport(Date fromDate, Date toDate, User user) {
    return createReport(fromDate, toDate, user, false);
  }
  
  public TipsCashoutReport createReport(Date fromDate, Date toDate, User user, boolean showCurrentSessionHistory) {
    Session session = null;
    try
    {
      session = getSession();
      
      if (fromDate != null)
        fromDate = DateUtil.startOfDay(fromDate);
      if (toDate != null) {
        toDate = DateUtil.endOfDay(toDate);
      }
      Criteria criteria = session.createCriteria(Ticket.class);
      if (user != null) {
        criteria.add(Restrictions.eq(Ticket.PROP_OWNER_ID, user.getId()));
      }
      TipsCashoutReport report = new TipsCashoutReport();
      StoreSession currentData = StoreUtil.getCurrentStoreOperation().getCurrentData();
      criteria.createAlias(Ticket.PROP_GRATUITY, "g");
      criteria.add(Restrictions.isNotNull(Ticket.PROP_GRATUITY));
      criteria.add(Restrictions.gt("g." + Gratuity.PROP_AMOUNT, new Double(0.0D)));
      
      if (showCurrentSessionHistory) {
        criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, currentData.getOpenTime()));
        report.setFromDate(currentData.getOpenTime());
        report.setToDate(new Date());

      }
      else if ((fromDate == null) && (toDate == null)) {
        criteria.add(Restrictions.eq("g." + Gratuity.PROP_PAID, Boolean.FALSE));
        criteria.add(Restrictions.eq("g." + Gratuity.PROP_REFUNDED, Boolean.FALSE));
      }
      else {
        criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, fromDate));
        criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, toDate));
      }
      

      List list = criteria.list();
      report.setServer(user.getId() + "/" + user.toString());
      report.setReportTime(new Date());
      
      if (!showCurrentSessionHistory) {
        report.setFromDate(fromDate);
        report.setToDate(toDate);
      }
      for (Iterator iter = list.iterator(); iter.hasNext();) {
        ticket = (Ticket)iter.next();
        Gratuity gratuity = ticket.getGratuity();
        TipsCashoutReportData data = new TipsCashoutReportData();
        data.setTicketId(String.valueOf(ticket.getTokenNo()));
        data.setTicketTotal(ticket.getTotalAmountWithTips());
        if (gratuity != null) {
          data.setDeclareTipsAmount(gratuity.getDeclareTipsAmount());
        }
        ticket = TicketDAO.getInstance().loadCouponsAndTransactions(ticket.getId());
        if (ticket != null) {
          Set<PosTransaction> transactions = ticket.getTransactions();
          if ((transactions != null) && (transactions.size() > 0)) {
            for (PosTransaction posTransaction : transactions) {
              if ((posTransaction instanceof CashTransaction)) {
                data.setCashTips(Double.valueOf(data.getCashTips().doubleValue() + posTransaction.getTipsAmount().doubleValue()));
              } else
                data.setChargedTips(Double.valueOf(data.getChargedTips().doubleValue() + posTransaction.getTipsAmount().doubleValue()));
            }
          }
        }
        if ((gratuity != null) && (!gratuity.isRefunded().booleanValue())) {
          if ((fromDate == null) && (toDate == null) && 
            (data.getCashTips().doubleValue() + data.getChargedTips().doubleValue() + data.getDeclareTipsAmount().doubleValue() <= 0.0D)) {
            iter.remove();
          }
          else
          {
            if (gratuity.getAmount().doubleValue() > 0.0D)
              data.setSaleType("");
            data.setPaid(gratuity.isPaid().booleanValue());
            data.setTipsPaidAmount(gratuity.getTipsPaidAmount());
          }
        } else report.addReportData(data); }
      Ticket ticket;
      criteria = session.createCriteria(DeclaredTips.class);
      criteria.setProjection(Projections.sum(DeclaredTips.PROP_AMOUNT));
      if ((fromDate == null) && (toDate == null)) {
        criteria.add(Restrictions.eq(DeclaredTips.PROP_SESSION_ID, currentData.getId()));
      }
      else {
        criteria.add(Restrictions.ge(DeclaredTips.PROP_DECLARED_TIME, fromDate));
        criteria.add(Restrictions.le(DeclaredTips.PROP_DECLARED_TIME, toDate));
      }
      if (user != null) {
        criteria.add(Restrictions.eq(DeclaredTips.PROP_OWNER_ID, user.getId()));
      }
      Object uniqueResult = criteria.uniqueResult();
      if (uniqueResult != null)
        report.setDeclaredTipsAmount(((Double)uniqueResult).doubleValue());
      report.calculateOthers();
      return report;
    } catch (Exception e) {
      if (user != null)
        throw new PosException("" + user.getFirstName() + " " + user.getLastName(), e);
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  public double findTotalGratuityAmount(Date fromDate, Date toDate, User user) throws PosException
  {
    Session session = null;
    try {
      session = createNewSession();
      
      Criteria criteria = session.createCriteria(getReferenceClass());
      

      if (user != null) {
        criteria.add(Restrictions.eq(Gratuity.PROP_OWNER_ID, user.getId()));
      }
      criteria.add(Restrictions.eq(Gratuity.PROP_REFUNDED, Boolean.FALSE));
      ProjectionList projectionList = Projections.projectionList();
      projectionList.add(Projections.sum(Gratuity.PROP_AMOUNT), Gratuity.PROP_AMOUNT);
      
      criteria.setProjection(projectionList);
      Number totalAmount = (Number)criteria.uniqueResult();
      if (totalAmount != null) {
        return totalAmount.doubleValue();
      }
    }
    finally {
      closeSession(session);
    }
    return 0.0D;
  }
}
