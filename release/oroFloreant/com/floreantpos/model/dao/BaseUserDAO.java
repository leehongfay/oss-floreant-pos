package com.floreantpos.model.dao;

import com.floreantpos.model.User;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseUserDAO
  extends _RootDAO
{
  public static UserDAO instance;
  
  public BaseUserDAO() {}
  
  public static UserDAO getInstance()
  {
    if (null == instance) instance = new UserDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return User.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("lastName");
  }
  


  public User cast(Object object)
  {
    return (User)object;
  }
  
  public User get(String key) throws HibernateException
  {
    return (User)get(getReferenceClass(), key);
  }
  
  public User get(String key, Session s) throws HibernateException
  {
    return (User)get(getReferenceClass(), key, s);
  }
  
  public User load(String key) throws HibernateException
  {
    return (User)load(getReferenceClass(), key);
  }
  
  public User load(String key, Session s) throws HibernateException
  {
    return (User)load(getReferenceClass(), key, s);
  }
  
  public User loadInitialize(String key, Session s) throws HibernateException
  {
    User obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<User> findAll()
  {
    return super.findAll();
  }
  


  public List<User> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<User> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(User user)
    throws HibernateException
  {
    return (String)super.save(user);
  }
  







  public String save(User user, Session s)
    throws HibernateException
  {
    return (String)save(user, s);
  }
  





  public void saveOrUpdate(User user)
    throws HibernateException
  {
    saveOrUpdate(user);
  }
  







  public void saveOrUpdate(User user, Session s)
    throws HibernateException
  {
    saveOrUpdate(user, s);
  }
  




  public void update(User user)
    throws HibernateException
  {
    update(user);
  }
  






  public void update(User user, Session s)
    throws HibernateException
  {
    update(user, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(User user)
    throws HibernateException
  {
    delete(user);
  }
  






  public void delete(User user, Session s)
    throws HibernateException
  {
    delete(user, s);
  }
  









  public void refresh(User user, Session s)
    throws HibernateException
  {
    refresh(user, s);
  }
}
