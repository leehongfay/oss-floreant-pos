package com.floreantpos.model.dao;

import com.floreantpos.model.InventoryUnitGroup;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseInventoryUnitGroupDAO
  extends _RootDAO
{
  public static InventoryUnitGroupDAO instance;
  
  public BaseInventoryUnitGroupDAO() {}
  
  public static InventoryUnitGroupDAO getInstance()
  {
    if (null == instance) instance = new InventoryUnitGroupDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return InventoryUnitGroup.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public InventoryUnitGroup cast(Object object)
  {
    return (InventoryUnitGroup)object;
  }
  
  public InventoryUnitGroup get(String key) throws HibernateException
  {
    return (InventoryUnitGroup)get(getReferenceClass(), key);
  }
  
  public InventoryUnitGroup get(String key, Session s) throws HibernateException
  {
    return (InventoryUnitGroup)get(getReferenceClass(), key, s);
  }
  
  public InventoryUnitGroup load(String key) throws HibernateException
  {
    return (InventoryUnitGroup)load(getReferenceClass(), key);
  }
  
  public InventoryUnitGroup load(String key, Session s) throws HibernateException
  {
    return (InventoryUnitGroup)load(getReferenceClass(), key, s);
  }
  
  public InventoryUnitGroup loadInitialize(String key, Session s) throws HibernateException
  {
    InventoryUnitGroup obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<InventoryUnitGroup> findAll()
  {
    return super.findAll();
  }
  


  public List<InventoryUnitGroup> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<InventoryUnitGroup> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(InventoryUnitGroup inventoryUnitGroup)
    throws HibernateException
  {
    return (String)super.save(inventoryUnitGroup);
  }
  







  public String save(InventoryUnitGroup inventoryUnitGroup, Session s)
    throws HibernateException
  {
    return (String)save(inventoryUnitGroup, s);
  }
  





  public void saveOrUpdate(InventoryUnitGroup inventoryUnitGroup)
    throws HibernateException
  {
    saveOrUpdate(inventoryUnitGroup);
  }
  







  public void saveOrUpdate(InventoryUnitGroup inventoryUnitGroup, Session s)
    throws HibernateException
  {
    saveOrUpdate(inventoryUnitGroup, s);
  }
  




  public void update(InventoryUnitGroup inventoryUnitGroup)
    throws HibernateException
  {
    update(inventoryUnitGroup);
  }
  






  public void update(InventoryUnitGroup inventoryUnitGroup, Session s)
    throws HibernateException
  {
    update(inventoryUnitGroup, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(InventoryUnitGroup inventoryUnitGroup)
    throws HibernateException
  {
    delete(inventoryUnitGroup);
  }
  






  public void delete(InventoryUnitGroup inventoryUnitGroup, Session s)
    throws HibernateException
  {
    delete(inventoryUnitGroup, s);
  }
  









  public void refresh(InventoryUnitGroup inventoryUnitGroup, Session s)
    throws HibernateException
  {
    refresh(inventoryUnitGroup, s);
  }
}
