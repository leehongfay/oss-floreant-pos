package com.floreantpos.model.dao;

import com.floreantpos.model.PizzaCrust;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BasePizzaCrustDAO
  extends _RootDAO
{
  public static PizzaCrustDAO instance;
  
  public BasePizzaCrustDAO() {}
  
  public static PizzaCrustDAO getInstance()
  {
    if (null == instance) instance = new PizzaCrustDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return PizzaCrust.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public PizzaCrust cast(Object object)
  {
    return (PizzaCrust)object;
  }
  
  public PizzaCrust get(String key) throws HibernateException
  {
    return (PizzaCrust)get(getReferenceClass(), key);
  }
  
  public PizzaCrust get(String key, Session s) throws HibernateException
  {
    return (PizzaCrust)get(getReferenceClass(), key, s);
  }
  
  public PizzaCrust load(String key) throws HibernateException
  {
    return (PizzaCrust)load(getReferenceClass(), key);
  }
  
  public PizzaCrust load(String key, Session s) throws HibernateException
  {
    return (PizzaCrust)load(getReferenceClass(), key, s);
  }
  
  public PizzaCrust loadInitialize(String key, Session s) throws HibernateException
  {
    PizzaCrust obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<PizzaCrust> findAll()
  {
    return super.findAll();
  }
  


  public List<PizzaCrust> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<PizzaCrust> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(PizzaCrust pizzaCrust)
    throws HibernateException
  {
    return (String)super.save(pizzaCrust);
  }
  







  public String save(PizzaCrust pizzaCrust, Session s)
    throws HibernateException
  {
    return (String)save(pizzaCrust, s);
  }
  





  public void saveOrUpdate(PizzaCrust pizzaCrust)
    throws HibernateException
  {
    saveOrUpdate(pizzaCrust);
  }
  







  public void saveOrUpdate(PizzaCrust pizzaCrust, Session s)
    throws HibernateException
  {
    saveOrUpdate(pizzaCrust, s);
  }
  




  public void update(PizzaCrust pizzaCrust)
    throws HibernateException
  {
    update(pizzaCrust);
  }
  






  public void update(PizzaCrust pizzaCrust, Session s)
    throws HibernateException
  {
    update(pizzaCrust, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(PizzaCrust pizzaCrust)
    throws HibernateException
  {
    delete(pizzaCrust);
  }
  






  public void delete(PizzaCrust pizzaCrust, Session s)
    throws HibernateException
  {
    delete(pizzaCrust, s);
  }
  









  public void refresh(PizzaCrust pizzaCrust, Session s)
    throws HibernateException
  {
    refresh(pizzaCrust, s);
  }
}
