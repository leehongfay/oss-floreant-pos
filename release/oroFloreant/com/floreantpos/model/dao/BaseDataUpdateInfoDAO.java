package com.floreantpos.model.dao;

import com.floreantpos.model.DataUpdateInfo;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseDataUpdateInfoDAO
  extends _RootDAO
{
  public static DataUpdateInfoDAO instance;
  
  public BaseDataUpdateInfoDAO() {}
  
  public static DataUpdateInfoDAO getInstance()
  {
    if (null == instance) instance = new DataUpdateInfoDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return DataUpdateInfo.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public DataUpdateInfo cast(Object object)
  {
    return (DataUpdateInfo)object;
  }
  
  public DataUpdateInfo get(String key) throws HibernateException
  {
    return (DataUpdateInfo)get(getReferenceClass(), key);
  }
  
  public DataUpdateInfo get(String key, Session s) throws HibernateException
  {
    return (DataUpdateInfo)get(getReferenceClass(), key, s);
  }
  
  public DataUpdateInfo load(String key) throws HibernateException
  {
    return (DataUpdateInfo)load(getReferenceClass(), key);
  }
  
  public DataUpdateInfo load(String key, Session s) throws HibernateException
  {
    return (DataUpdateInfo)load(getReferenceClass(), key, s);
  }
  
  public DataUpdateInfo loadInitialize(String key, Session s) throws HibernateException
  {
    DataUpdateInfo obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<DataUpdateInfo> findAll()
  {
    return super.findAll();
  }
  


  public List<DataUpdateInfo> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<DataUpdateInfo> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(DataUpdateInfo dataUpdateInfo)
    throws HibernateException
  {
    return (String)super.save(dataUpdateInfo);
  }
  







  public String save(DataUpdateInfo dataUpdateInfo, Session s)
    throws HibernateException
  {
    return (String)save(dataUpdateInfo, s);
  }
  





  public void saveOrUpdate(DataUpdateInfo dataUpdateInfo)
    throws HibernateException
  {
    saveOrUpdate(dataUpdateInfo);
  }
  







  public void saveOrUpdate(DataUpdateInfo dataUpdateInfo, Session s)
    throws HibernateException
  {
    saveOrUpdate(dataUpdateInfo, s);
  }
  




  public void update(DataUpdateInfo dataUpdateInfo)
    throws HibernateException
  {
    update(dataUpdateInfo);
  }
  






  public void update(DataUpdateInfo dataUpdateInfo, Session s)
    throws HibernateException
  {
    update(dataUpdateInfo, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(DataUpdateInfo dataUpdateInfo)
    throws HibernateException
  {
    delete(dataUpdateInfo);
  }
  






  public void delete(DataUpdateInfo dataUpdateInfo, Session s)
    throws HibernateException
  {
    delete(dataUpdateInfo, s);
  }
  









  public void refresh(DataUpdateInfo dataUpdateInfo, Session s)
    throws HibernateException
  {
    refresh(dataUpdateInfo, s);
  }
}
