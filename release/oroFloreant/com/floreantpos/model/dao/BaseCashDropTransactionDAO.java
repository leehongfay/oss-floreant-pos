package com.floreantpos.model.dao;

import com.floreantpos.model.CashDropTransaction;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseCashDropTransactionDAO
  extends _RootDAO
{
  public static CashDropTransactionDAO instance;
  
  public BaseCashDropTransactionDAO() {}
  
  public static CashDropTransactionDAO getInstance()
  {
    if (null == instance) instance = new CashDropTransactionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return CashDropTransaction.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public CashDropTransaction cast(Object object)
  {
    return (CashDropTransaction)object;
  }
  
  public CashDropTransaction get(String key) throws HibernateException
  {
    return (CashDropTransaction)get(getReferenceClass(), key);
  }
  
  public CashDropTransaction get(String key, Session s) throws HibernateException
  {
    return (CashDropTransaction)get(getReferenceClass(), key, s);
  }
  
  public CashDropTransaction load(String key) throws HibernateException
  {
    return (CashDropTransaction)load(getReferenceClass(), key);
  }
  
  public CashDropTransaction load(String key, Session s) throws HibernateException
  {
    return (CashDropTransaction)load(getReferenceClass(), key, s);
  }
  
  public CashDropTransaction loadInitialize(String key, Session s) throws HibernateException
  {
    CashDropTransaction obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<CashDropTransaction> findAll()
  {
    return super.findAll();
  }
  


  public List<CashDropTransaction> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<CashDropTransaction> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(CashDropTransaction cashDropTransaction)
    throws HibernateException
  {
    return (String)super.save(cashDropTransaction);
  }
  







  public String save(CashDropTransaction cashDropTransaction, Session s)
    throws HibernateException
  {
    return (String)save(cashDropTransaction, s);
  }
  





  public void saveOrUpdate(CashDropTransaction cashDropTransaction)
    throws HibernateException
  {
    saveOrUpdate(cashDropTransaction);
  }
  







  public void saveOrUpdate(CashDropTransaction cashDropTransaction, Session s)
    throws HibernateException
  {
    saveOrUpdate(cashDropTransaction, s);
  }
  




  public void update(CashDropTransaction cashDropTransaction)
    throws HibernateException
  {
    update(cashDropTransaction);
  }
  






  public void update(CashDropTransaction cashDropTransaction, Session s)
    throws HibernateException
  {
    update(cashDropTransaction, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(CashDropTransaction cashDropTransaction)
    throws HibernateException
  {
    delete(cashDropTransaction);
  }
  






  public void delete(CashDropTransaction cashDropTransaction, Session s)
    throws HibernateException
  {
    delete(cashDropTransaction, s);
  }
  









  public void refresh(CashDropTransaction cashDropTransaction, Session s)
    throws HibernateException
  {
    refresh(cashDropTransaction, s);
  }
}
