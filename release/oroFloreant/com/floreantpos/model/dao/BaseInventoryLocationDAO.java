package com.floreantpos.model.dao;

import com.floreantpos.model.InventoryLocation;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseInventoryLocationDAO
  extends _RootDAO
{
  public static InventoryLocationDAO instance;
  
  public BaseInventoryLocationDAO() {}
  
  public static InventoryLocationDAO getInstance()
  {
    if (null == instance) instance = new InventoryLocationDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return InventoryLocation.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public InventoryLocation cast(Object object)
  {
    return (InventoryLocation)object;
  }
  
  public InventoryLocation get(String key) throws HibernateException
  {
    return (InventoryLocation)get(getReferenceClass(), key);
  }
  
  public InventoryLocation get(String key, Session s) throws HibernateException
  {
    return (InventoryLocation)get(getReferenceClass(), key, s);
  }
  
  public InventoryLocation load(String key) throws HibernateException
  {
    return (InventoryLocation)load(getReferenceClass(), key);
  }
  
  public InventoryLocation load(String key, Session s) throws HibernateException
  {
    return (InventoryLocation)load(getReferenceClass(), key, s);
  }
  
  public InventoryLocation loadInitialize(String key, Session s) throws HibernateException
  {
    InventoryLocation obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<InventoryLocation> findAll()
  {
    return super.findAll();
  }
  


  public List<InventoryLocation> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<InventoryLocation> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(InventoryLocation inventoryLocation)
    throws HibernateException
  {
    return (String)super.save(inventoryLocation);
  }
  







  public String save(InventoryLocation inventoryLocation, Session s)
    throws HibernateException
  {
    return (String)save(inventoryLocation, s);
  }
  





  public void saveOrUpdate(InventoryLocation inventoryLocation)
    throws HibernateException
  {
    saveOrUpdate(inventoryLocation);
  }
  







  public void saveOrUpdate(InventoryLocation inventoryLocation, Session s)
    throws HibernateException
  {
    saveOrUpdate(inventoryLocation, s);
  }
  




  public void update(InventoryLocation inventoryLocation)
    throws HibernateException
  {
    update(inventoryLocation);
  }
  






  public void update(InventoryLocation inventoryLocation, Session s)
    throws HibernateException
  {
    update(inventoryLocation, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(InventoryLocation inventoryLocation)
    throws HibernateException
  {
    delete(inventoryLocation);
  }
  






  public void delete(InventoryLocation inventoryLocation, Session s)
    throws HibernateException
  {
    delete(inventoryLocation, s);
  }
  









  public void refresh(InventoryLocation inventoryLocation, Session s)
    throws HibernateException
  {
    refresh(inventoryLocation, s);
  }
}
