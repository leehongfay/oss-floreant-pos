package com.floreantpos.model.dao;

import com.floreantpos.model.ReportGroup;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseReportGroupDAO
  extends _RootDAO
{
  public static ReportGroupDAO instance;
  
  public BaseReportGroupDAO() {}
  
  public static ReportGroupDAO getInstance()
  {
    if (null == instance) instance = new ReportGroupDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return ReportGroup.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public ReportGroup cast(Object object)
  {
    return (ReportGroup)object;
  }
  
  public ReportGroup get(String key) throws HibernateException
  {
    return (ReportGroup)get(getReferenceClass(), key);
  }
  
  public ReportGroup get(String key, Session s) throws HibernateException
  {
    return (ReportGroup)get(getReferenceClass(), key, s);
  }
  
  public ReportGroup load(String key) throws HibernateException
  {
    return (ReportGroup)load(getReferenceClass(), key);
  }
  
  public ReportGroup load(String key, Session s) throws HibernateException
  {
    return (ReportGroup)load(getReferenceClass(), key, s);
  }
  
  public ReportGroup loadInitialize(String key, Session s) throws HibernateException
  {
    ReportGroup obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<ReportGroup> findAll()
  {
    return super.findAll();
  }
  


  public List<ReportGroup> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<ReportGroup> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(ReportGroup reportGroup)
    throws HibernateException
  {
    return (String)super.save(reportGroup);
  }
  







  public String save(ReportGroup reportGroup, Session s)
    throws HibernateException
  {
    return (String)save(reportGroup, s);
  }
  





  public void saveOrUpdate(ReportGroup reportGroup)
    throws HibernateException
  {
    saveOrUpdate(reportGroup);
  }
  







  public void saveOrUpdate(ReportGroup reportGroup, Session s)
    throws HibernateException
  {
    saveOrUpdate(reportGroup, s);
  }
  




  public void update(ReportGroup reportGroup)
    throws HibernateException
  {
    update(reportGroup);
  }
  






  public void update(ReportGroup reportGroup, Session s)
    throws HibernateException
  {
    update(reportGroup, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(ReportGroup reportGroup)
    throws HibernateException
  {
    delete(reportGroup);
  }
  






  public void delete(ReportGroup reportGroup, Session s)
    throws HibernateException
  {
    delete(reportGroup, s);
  }
  









  public void refresh(ReportGroup reportGroup, Session s)
    throws HibernateException
  {
    refresh(reportGroup, s);
  }
}
