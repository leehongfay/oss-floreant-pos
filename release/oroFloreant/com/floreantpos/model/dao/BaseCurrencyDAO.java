package com.floreantpos.model.dao;

import com.floreantpos.model.Currency;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseCurrencyDAO
  extends _RootDAO
{
  public static CurrencyDAO instance;
  
  public BaseCurrencyDAO() {}
  
  public static CurrencyDAO getInstance()
  {
    if (null == instance) instance = new CurrencyDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return Currency.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public Currency cast(Object object)
  {
    return (Currency)object;
  }
  
  public Currency get(String key) throws HibernateException
  {
    return (Currency)get(getReferenceClass(), key);
  }
  
  public Currency get(String key, Session s) throws HibernateException
  {
    return (Currency)get(getReferenceClass(), key, s);
  }
  
  public Currency load(String key) throws HibernateException
  {
    return (Currency)load(getReferenceClass(), key);
  }
  
  public Currency load(String key, Session s) throws HibernateException
  {
    return (Currency)load(getReferenceClass(), key, s);
  }
  
  public Currency loadInitialize(String key, Session s) throws HibernateException
  {
    Currency obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<Currency> findAll()
  {
    return super.findAll();
  }
  


  public List<Currency> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<Currency> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(Currency currency)
    throws HibernateException
  {
    return (String)super.save(currency);
  }
  







  public String save(Currency currency, Session s)
    throws HibernateException
  {
    return (String)save(currency, s);
  }
  





  public void saveOrUpdate(Currency currency)
    throws HibernateException
  {
    saveOrUpdate(currency);
  }
  







  public void saveOrUpdate(Currency currency, Session s)
    throws HibernateException
  {
    saveOrUpdate(currency, s);
  }
  




  public void update(Currency currency)
    throws HibernateException
  {
    update(currency);
  }
  






  public void update(Currency currency, Session s)
    throws HibernateException
  {
    update(currency, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(Currency currency)
    throws HibernateException
  {
    delete(currency);
  }
  






  public void delete(Currency currency, Session s)
    throws HibernateException
  {
    delete(currency, s);
  }
  









  public void refresh(Currency currency, Session s)
    throws HibernateException
  {
    refresh(currency, s);
  }
}
