package com.floreantpos.model.dao;

import com.floreantpos.model.MenuItemInventoryStatus;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseMenuItemInventoryStatusDAO
  extends _RootDAO
{
  public static MenuItemInventoryStatusDAO instance;
  
  public BaseMenuItemInventoryStatusDAO() {}
  
  public static MenuItemInventoryStatusDAO getInstance()
  {
    if (null == instance) instance = new MenuItemInventoryStatusDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return MenuItemInventoryStatus.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public MenuItemInventoryStatus cast(Object object)
  {
    return (MenuItemInventoryStatus)object;
  }
  
  public MenuItemInventoryStatus get(String key) throws HibernateException
  {
    return (MenuItemInventoryStatus)get(getReferenceClass(), key);
  }
  
  public MenuItemInventoryStatus get(String key, Session s) throws HibernateException
  {
    return (MenuItemInventoryStatus)get(getReferenceClass(), key, s);
  }
  
  public MenuItemInventoryStatus load(String key) throws HibernateException
  {
    return (MenuItemInventoryStatus)load(getReferenceClass(), key);
  }
  
  public MenuItemInventoryStatus load(String key, Session s) throws HibernateException
  {
    return (MenuItemInventoryStatus)load(getReferenceClass(), key, s);
  }
  
  public MenuItemInventoryStatus loadInitialize(String key, Session s) throws HibernateException
  {
    MenuItemInventoryStatus obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<MenuItemInventoryStatus> findAll()
  {
    return super.findAll();
  }
  


  public List<MenuItemInventoryStatus> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<MenuItemInventoryStatus> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(MenuItemInventoryStatus menuItemInventoryStatus)
    throws HibernateException
  {
    return (String)super.save(menuItemInventoryStatus);
  }
  







  public String save(MenuItemInventoryStatus menuItemInventoryStatus, Session s)
    throws HibernateException
  {
    return (String)save(menuItemInventoryStatus, s);
  }
  





  public void saveOrUpdate(MenuItemInventoryStatus menuItemInventoryStatus)
    throws HibernateException
  {
    saveOrUpdate(menuItemInventoryStatus);
  }
  







  public void saveOrUpdate(MenuItemInventoryStatus menuItemInventoryStatus, Session s)
    throws HibernateException
  {
    saveOrUpdate(menuItemInventoryStatus, s);
  }
  




  public void update(MenuItemInventoryStatus menuItemInventoryStatus)
    throws HibernateException
  {
    update(menuItemInventoryStatus);
  }
  






  public void update(MenuItemInventoryStatus menuItemInventoryStatus, Session s)
    throws HibernateException
  {
    update(menuItemInventoryStatus, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(MenuItemInventoryStatus menuItemInventoryStatus)
    throws HibernateException
  {
    delete(menuItemInventoryStatus);
  }
  






  public void delete(MenuItemInventoryStatus menuItemInventoryStatus, Session s)
    throws HibernateException
  {
    delete(menuItemInventoryStatus, s);
  }
  









  public void refresh(MenuItemInventoryStatus menuItemInventoryStatus, Session s)
    throws HibernateException
  {
    refresh(menuItemInventoryStatus, s);
  }
}
