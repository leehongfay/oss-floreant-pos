package com.floreantpos.model.dao;

import com.floreantpos.model.Recepie;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseRecepieDAO
  extends _RootDAO
{
  public static RecepieDAO instance;
  
  public BaseRecepieDAO() {}
  
  public static RecepieDAO getInstance()
  {
    if (null == instance) instance = new RecepieDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return Recepie.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public Recepie cast(Object object)
  {
    return (Recepie)object;
  }
  
  public Recepie get(String key) throws HibernateException
  {
    return (Recepie)get(getReferenceClass(), key);
  }
  
  public Recepie get(String key, Session s) throws HibernateException
  {
    return (Recepie)get(getReferenceClass(), key, s);
  }
  
  public Recepie load(String key) throws HibernateException
  {
    return (Recepie)load(getReferenceClass(), key);
  }
  
  public Recepie load(String key, Session s) throws HibernateException
  {
    return (Recepie)load(getReferenceClass(), key, s);
  }
  
  public Recepie loadInitialize(String key, Session s) throws HibernateException
  {
    Recepie obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<Recepie> findAll()
  {
    return super.findAll();
  }
  


  public List<Recepie> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<Recepie> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(Recepie recepie)
    throws HibernateException
  {
    return (String)super.save(recepie);
  }
  







  public String save(Recepie recepie, Session s)
    throws HibernateException
  {
    return (String)save(recepie, s);
  }
  





  public void saveOrUpdate(Recepie recepie)
    throws HibernateException
  {
    saveOrUpdate(recepie);
  }
  







  public void saveOrUpdate(Recepie recepie, Session s)
    throws HibernateException
  {
    saveOrUpdate(recepie, s);
  }
  




  public void update(Recepie recepie)
    throws HibernateException
  {
    update(recepie);
  }
  






  public void update(Recepie recepie, Session s)
    throws HibernateException
  {
    update(recepie, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(Recepie recepie)
    throws HibernateException
  {
    delete(recepie);
  }
  






  public void delete(Recepie recepie, Session s)
    throws HibernateException
  {
    delete(recepie, s);
  }
  









  public void refresh(Recepie recepie, Session s)
    throws HibernateException
  {
    refresh(recepie, s);
  }
}
