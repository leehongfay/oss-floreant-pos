package com.floreantpos.model.dao;

import com.floreantpos.model.DeliveryConfiguration;
import java.util.List;
import org.hibernate.HibernateException;





public class DeliveryConfigurationDAO
  extends BaseDeliveryConfigurationDAO
{
  public DeliveryConfigurationDAO() {}
  
  public DeliveryConfiguration get(String key)
    throws HibernateException
  {
    List list = super.findAll();
    if ((list != null) && (!list.isEmpty())) {
      return (DeliveryConfiguration)list.get(0);
    }
    return null;
  }
}
