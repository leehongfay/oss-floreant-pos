package com.floreantpos.model.dao;

import com.floreantpos.model.KitchenTicketItem;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;






















public class KitchenTicketItemDAO
  extends BaseKitchenTicketItemDAO
{
  public KitchenTicketItemDAO() {}
  
  public List<KitchenTicketItem> find(String itemId, boolean modifier)
  {
    Session session = null;
    try {
      session = getSession();
      return find(itemId, modifier, session);
    } finally {
      closeSession(session);
    }
  }
  
  public List<KitchenTicketItem> find(String itemId, boolean modifier, Session session) {
    Criteria criteria = session.createCriteria(getReferenceClass());
    criteria.add(Restrictions.eq(modifier ? KitchenTicketItem.PROP_TICKET_ITEM_MODIFIER_ID : KitchenTicketItem.PROP_TICKET_ITEM_ID, itemId));
    return criteria.list();
  }
}
