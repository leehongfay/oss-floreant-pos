package com.floreantpos.model.dao;

import com.floreantpos.PosLog;
import com.floreantpos.model.ShopFloor;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.ShopTableStatus;
import com.floreantpos.model.ShopTableType;
import com.floreantpos.model.TableStatus;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;






















public class ShopTableDAO
  extends BaseShopTableDAO
{
  public ShopTableDAO() {}
  
  public Order getDefaultOrder()
  {
    return Order.asc(ShopTable.PROP_ID);
  }
  
  public int getNextTableNumber() {
    Session session = null;
    try
    {
      session = createNewSession();
      
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.setProjection(Projections.rowCount());
      
      return ((Number)criteria.uniqueResult()).intValue();
    } finally {
      closeSession(session);
    }
  }
  
  public ShopTable getByNumber(int tableNumber) {
    Session session = null;
    try {
      session = createNewSession();
      
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(ShopTable.PROP_ID, Integer.valueOf(tableNumber)));
      
      return (ShopTable)criteria.uniqueResult();
    } finally {
      closeSession(session);
    }
  }
  
  public List<ShopTable> getByFloor(ShopFloor shopFloor) {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(ShopTable.PROP_FLOOR_ID, shopFloor.getId()));
      criteria.addOrder(Order.asc(ShopTable.PROP_ID));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<ShopTable> getAllUnassigned() {
    Session session = null;
    try
    {
      session = createNewSession();
      
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.isNull(ShopTable.PROP_FLOOR_ID));
      
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<ShopTable> getByNumbers(Collection<Integer> tableNumbers) {
    if ((tableNumbers == null) || (tableNumbers.size() == 0)) {
      return null;
    }
    
    Session session = null;
    try
    {
      session = createNewSession();
      
      Criteria criteria = session.createCriteria(getReferenceClass());
      Disjunction disjunction = Restrictions.disjunction();
      
      for (Object localObject1 = tableNumbers.iterator(); ((Iterator)localObject1).hasNext();) { Integer tableNumber = (Integer)((Iterator)localObject1).next();
        disjunction.add(Restrictions.eq(ShopTable.PROP_ID, tableNumber));
      }
      criteria.add(disjunction);
      
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<ShopTable> getTables(Ticket ticket) {
    return getByNumbers(ticket.getTableNumbers());
  }
  
  public void bookedTables(List<ShopTableStatus> tables) {
    if (tables == null) {
      return;
    }
    
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      for (ShopTableStatus shopTableStatus : tables) {
        shopTableStatus.setTableStatus(TableStatus.Booked);
        session.saveOrUpdate(shopTableStatus);
      }
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(ShopTableDAO.class).error(e);
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
  
  public void occupyTables(Ticket ticket) {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      occupyTables(ticket, session);
      
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(ShopTableDAO.class).error(e);
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
  
  public void occupyTables(Ticket ticket, Session session) {
    List<Integer> tableNumbers = ticket.getTableNumbers();
    List<String> ticketNumbers = new ArrayList();
    ticketNumbers.add(ticket.getId());
    User owner = ticket.getOwner();
    
    for (Integer tableId : tableNumbers) {
      ShopTableStatus shopTableStatus = ShopTableStatusDAO.getInstance().get(tableId, session);
      if (shopTableStatus == null) {
        shopTableStatus = new ShopTableStatus();
        shopTableStatus.setId(tableId);
      }
      shopTableStatus.setTableTicket(ticket.getId(), ticket.getTokenNo(), owner.getId(), owner.getFirstName());
      shopTableStatus.setTableStatus(TableStatus.Seat);
      ShopTableStatusDAO.getInstance().saveOrUpdate(shopTableStatus, session);
    }
  }
  
  public void freeTables(List<ShopTableStatus> tableStatusList) {
    if (tableStatusList == null) {
      return;
    }
    
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      for (ShopTableStatus shopTableStatus : tableStatusList) {
        shopTableStatus.setTableStatus(TableStatus.Available);
        ShopTableStatusDAO.getInstance().saveOrUpdate(shopTableStatus, session);
      }
      
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(ShopTableDAO.class).error(e);
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
  
  public void freeTablesByNumbers(List<Integer> tableNumbers) {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      
      freeTables(tableNumbers, session);
      
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(ShopTableDAO.class).error(e);
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
  
  public void freeTables(List<Integer> tableNumbers, Session session) {
    for (Integer tableId : tableNumbers) {
      ShopTableStatus shopTableStatus = (ShopTableStatus)session.get(ShopTableStatus.class, tableId);
      shopTableStatus.setTicketId(null);
      shopTableStatus.setTableStatus(TableStatus.Available);
      saveOrUpdate(shopTableStatus, session);
    }
  }
  
  public void releaseTables(Ticket ticket) {
    List<ShopTable> tables = getTables(ticket);
    
    if (tables == null) {
      return;
    }
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      for (ShopTable shopTable : tables) {
        shopTable.setTableStatus(TableStatus.Available);
        saveOrUpdate(shopTable, session);
      }
      
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(ShopTableDAO.class).error(e);
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
  
  public void releaseAndDeleteTicketTables(Ticket ticket)
  {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      releaseTables(ticket);
      ticket.setTableNumbers(null);
      TicketDAO.getInstance().saveOrUpdate(ticket);
      
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(ShopTableDAO.class).error(e);
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
  
  public void deleteTables(Collection<ShopTable> tables) {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      for (ShopTable shopTable : tables) {
        super.delete(shopTable, session);
      }
      
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(ShopTableDAO.class).error(e);
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
  
  public List<ShopTableType> getTableByTypes(List<ShopTableType> types) {
    List<String> typeIds = new ArrayList();
    for (ShopTableType shopTableType : types) {
      typeIds.add(shopTableType.getId());
    }
    
    Session session = getSession();
    Criteria criteria = session.createCriteria(ShopTable.class);
    criteria.createAlias("types", "t");
    criteria.add(Restrictions.in("t.id", typeIds));
    
    criteria.addOrder(Order.asc(ShopTable.PROP_ID));
    return criteria.list();
  }
  
  public void createNewTables(int totalNumberOfTableHaveToCreate)
  {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      for (int i = 0; i < totalNumberOfTableHaveToCreate; i++) {
        ShopTable table = new ShopTable();
        table.setId(Integer.valueOf(i + 1));
        table.setCapacity(Integer.valueOf(4));
        super.save(table, session);
      }
      
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(ShopTableDAO.class).error(e);
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
  
  public void updateTableStatus(List tableNumbers, Integer status, Ticket ticket, boolean saveTicket)
  {
    if ((tableNumbers == null) || (tableNumbers.isEmpty()))
      return;
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      updateTableStatus(tableNumbers, status, ticket, session);
      if (saveTicket)
        session.saveOrUpdate(ticket);
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(ShopTableDAO.class).error(e);
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
  
  public void updateTableStatus(List<Integer> tableNumbers, Integer status, Ticket ticket, Session session) {
    String ticketId = null;
    List<String> tickets = new ArrayList();
    String userId = null;
    String userName = null;
    
    if (ticket != null) {
      ticketId = ticket.getId();
      userId = ticket.getOwner().getId();
      userName = ticket.getOwner().getFirstName();
      tickets.add(ticket.getId());
    }
    
    for (Iterator iterator = tableNumbers.iterator(); iterator.hasNext();) {
      Integer integer = (Integer)iterator.next();
      ShopTableStatus tableStatus = ShopTableStatusDAO.getInstance().get(integer);
      if (tableStatus != null) {
        if (ticket != null) {
          tableStatus.setTableTicket(ticketId, ticket.getTokenNo(), userId, userName);
        } else {
          tableStatus.setTicketId(null);
        }
        session.saveOrUpdate(tableStatus);
      }
    }
  }
  















  public List<ShopTable> findBy(String searchSubject)
  {
    Session session = null;
    try
    {
      session = createNewSession();
      
      Criteria criteria = session.createCriteria(getReferenceClass());
      
      if (StringUtils.isEmpty(searchSubject)) {
        return null;
      }
      if (StringUtils.isNumeric(searchSubject)) {
        criteria.add(Restrictions.eq(ShopTable.PROP_CAPACITY, Integer.valueOf(searchSubject)));
      }
      




      List list = criteria.list();
      List localList1; if ((list != null) || (list.size() > 0)) {
        return list;
      }
      return null;
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      closeSession(session);
    }
    return null;
  }
  

  public List<String> getTableNames(List tableNumbers)
  {
    Session session = null;
    try
    {
      session = createNewSession();
      
      String queryString = String.format("select %s, %s from %s table where ", new Object[] { ShopTable.PROP_ID, ShopTable.PROP_NAME, ShopTable.class.getSimpleName() });
      Query query = session.createQuery(queryString + "table.id IN (:tableNumbers)");
      query.setParameterList("tableNumbers", tableNumbers);
      List list = query.list();
      if ((list == null) || (list.isEmpty())) {
        return null;
      }
      Object names = new ArrayList();
      for (int i = 0; i < list.size(); i++) {
        Object[] obj = (Object[])list.get(i);
        if (obj[1] != null) {
          ((List)names).add((String)obj[1]);
        }
        else {
          ((List)names).add(String.valueOf(obj[0]));
        }
      }
      return (int)names;
    } catch (Exception e) {
      PosLog.error(ShopTableDAO.class, e.getMessage(), e);
    } finally {
      closeSession(session);
    }
    return null;
  }
  
  public ShopTable loadWithSeats(Integer tableId)
  {
    if (tableId == null) {
      return null;
    }
    Session session = null;
    try {
      session = createNewSession();
      ShopTable shopTable = get(tableId, session);
      Hibernate.initialize(shopTable.getSeats());
      return shopTable;
    } finally {
      closeSession(session);
    }
  }
  
  public void initializeSeats(ShopTable shopTable) {
    if ((shopTable == null) || (shopTable.getId() == null)) {
      return;
    }
    Session session = null;
    try {
      session = createNewSession();
      session.refresh(shopTable);
      Hibernate.initialize(shopTable.getSeats());
      
      closeSession(session); } finally { closeSession(session);
    }
  }
  
  public void initializeTablesSeats(List<ShopTable> tables) {
    Session session = null;
    try {
      session = createNewSession();
      for (ShopTable table : tables) {
        session.refresh(table);
        Hibernate.initialize(table.getSeats());
      }
    } finally {
      closeSession(session);
    }
  }
  
  public void detachFloor(List<ShopTable> selectedTables)
  {
    if ((selectedTables == null) || (selectedTables.isEmpty()))
      return;
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      for (ShopTable shopTable : selectedTables) {
        shopTable.setFloorId(null);
        saveOrUpdate(shopTable, session);
      }
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(ShopTableDAO.class).error(e);
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
}
