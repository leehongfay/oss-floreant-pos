package com.floreantpos.model.dao;

import com.floreantpos.PosLog;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.ShopTableStatus;
import com.floreantpos.model.TableBookingInfo;
import com.floreantpos.model.util.DateUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;





















public class TableBookingInfoDAO
  extends BaseTableBookingInfoDAO
{
  public TableBookingInfoDAO() {}
  
  public Collection<ShopTable> getBookedTables(Date startDate, Date endDate)
  {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      

      criteria.add(Restrictions.ge(TableBookingInfo.PROP_TO_DATE, startDate))
        .add(Restrictions.ne(TableBookingInfo.PROP_STATUS, "cancel"))
        .add(Restrictions.ne(TableBookingInfo.PROP_STATUS, "no appear"))
        .add(Restrictions.ne(TableBookingInfo.PROP_STATUS, "close"));
      
      List<TableBookingInfo> list = criteria.list();
      List<TableBookingInfo> bookings = new ArrayList();
      
      for (Iterator localIterator = list.iterator(); localIterator.hasNext();) { tableBookingInfo = (TableBookingInfo)localIterator.next();
        if ((DateUtil.between(tableBookingInfo.getFromDate(), tableBookingInfo.getToDate(), startDate)) || 
          (DateUtil.between(tableBookingInfo.getFromDate(), tableBookingInfo.getToDate(), endDate))) {
          bookings.add(tableBookingInfo);
        }
      }
      TableBookingInfo tableBookingInfo;
      Object bookedTables = new HashSet();
      for (TableBookingInfo tableBookingInfo : bookings) {
        List<ShopTable> tables = tableBookingInfo.getTables();
        if (tables != null) {
          ((Set)bookedTables).addAll(tables);
        }
      }
      
      return (TableBookingInfo)bookedTables;
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
    return null;
  }
  
  public List<ShopTable> getFreeTables(Date startDate, Date endDate) {
    Collection<ShopTable> bookedTables = getBookedTables(startDate, endDate);
    List<ShopTable> allTables = ShopTableDAO.getInstance().findAll();
    
    allTables.removeAll(bookedTables);
    
    return allTables;
  }
  
  public List<TableBookingInfo> getAllOpenBooking() {
    Session session = null;
    try {
      session = createNewSession();
      
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.ne(TableBookingInfo.PROP_STATUS, "close"));
      List list = criteria.list();
      return list;
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    } finally {
      try {
        session.close();
      }
      catch (Exception localException3) {}
    }
    return null;
  }
  

  public void setBookingStatus(TableBookingInfo bookingInfo, String bookingStatus, List<ShopTableStatus> tables)
  {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      bookingInfo.setStatus(bookingStatus);
      saveOrUpdate(bookingInfo);
      
      if ((bookingStatus.equals("seat")) || (bookingStatus.equals("delay"))) {
        ShopTableDAO.getInstance().bookedTables(tables);
      }
      
      if ((bookingStatus.equals("cancel")) || (bookingStatus.equals("no appear")) || 
        (bookingStatus.equals("close"))) {
        ShopTableDAO.getInstance().freeTables(tables);
      }
      
      tx.commit();
    }
    catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(TableBookingInfo.class).error(e);
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
  




























  public List getTodaysBooking()
  {
    Session session = null;
    try
    {
      Calendar startDate = Calendar.getInstance();
      startDate.setLenient(false);
      startDate.setTime(new Date());
      startDate.set(11, 0);
      startDate.set(12, 0);
      startDate.set(13, 0);
      startDate.set(14, 0);
      
      Calendar endDate = Calendar.getInstance();
      endDate.setLenient(false);
      endDate.setTime(new Date());
      endDate.set(11, 23);
      endDate.set(12, 59);
      endDate.set(13, 59);
      
      session = createNewSession();
      
      Criteria criteria = session.createCriteria(TableBookingInfo.class);
      
      criteria.add(Restrictions.ge(TableBookingInfo.PROP_FROM_DATE, startDate.getTime()))
        .add(Restrictions.le(TableBookingInfo.PROP_FROM_DATE, endDate.getTime()))
        .add(Restrictions.eq(TableBookingInfo.PROP_STATUS, "open"));
      List list = criteria.list();
      
      return list;
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    } finally {
      session.close();
    }
    return null;
  }
  
  public List getAllBookingByDate(Date startDate, Date endDate)
  {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(TableBookingInfo.class);
      
      criteria.add(Restrictions.ge(TableBookingInfo.PROP_FROM_DATE, startDate)).add(Restrictions.le(TableBookingInfo.PROP_FROM_DATE, endDate))
        .add(Restrictions.ne(TableBookingInfo.PROP_STATUS, "close"));
      
      List list = criteria.list();
      
      return list;
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    } finally {
      session.close();
    }
    return null;
  }
}
