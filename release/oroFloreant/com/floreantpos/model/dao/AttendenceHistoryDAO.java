package com.floreantpos.model.dao;

import com.floreantpos.Messages;
import com.floreantpos.PosException;
import com.floreantpos.model.AttendenceHistory;
import com.floreantpos.model.Shift;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.report.AttendanceReportData;
import com.floreantpos.report.PayrollReportData;
import com.floreantpos.report.WeeklyPayrollReportData;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;






















public class AttendenceHistoryDAO
  extends BaseAttendenceHistoryDAO
{
  public AttendenceHistoryDAO() {}
  
  public List<User> findNumberOfClockedInUserAtHour(Date fromDay, Date toDay, int hour, Terminal terminal)
  {
    Session session = null;
    
    ArrayList<User> users = new ArrayList();
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.ge(AttendenceHistory.PROP_CLOCK_IN_TIME, fromDay));
      criteria.add(Restrictions.le(AttendenceHistory.PROP_CLOCK_IN_TIME, toDay));
      criteria.add(Restrictions.le(AttendenceHistory.PROP_CLOCK_IN_HOUR, new Short((short)hour)));
      
      if (terminal != null) {
        criteria.add(Restrictions.eq(Ticket.PROP_TERMINAL_ID, terminal.getId()));
      }
      
      List list = criteria.list();
      for (Object localObject1 = list.iterator(); ((Iterator)localObject1).hasNext();) { Object object = ((Iterator)localObject1).next();
        AttendenceHistory history = (AttendenceHistory)object;
        
        if (!history.isClockedOut().booleanValue()) {
          users.add(history.getUser());
        }
        else if (history.getClockOutHour().shortValue() >= hour) {
          users.add(history.getUser());
        }
      }
      return users;
    } catch (Exception e) {
      throw new PosException(Messages.getString("AttendenceHistoryDAO.2"), e);
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public List<User> findNumberOfClockedInUserAtShift(Date fromDay, Date toDay, Shift shift, Terminal terminal) {
    Session session = null;
    
    ArrayList<User> users = new ArrayList();
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.ge(AttendenceHistory.PROP_CLOCK_IN_TIME, fromDay));
      criteria.add(Restrictions.le(AttendenceHistory.PROP_CLOCK_IN_TIME, toDay));
      criteria.add(Restrictions.le(AttendenceHistory.PROP_SHIFT, shift));
      
      if (terminal != null) {
        criteria.add(Restrictions.eq(Ticket.PROP_TERMINAL_ID, terminal.getId()));
      }
      
      List list = criteria.list();
      for (Object localObject1 = list.iterator(); ((Iterator)localObject1).hasNext();) { Object object = ((Iterator)localObject1).next();
        AttendenceHistory history = (AttendenceHistory)object;
        






        users.add(history.getUser());
      }
      return users;
    } catch (Exception e) {
      throw new PosException(Messages.getString("AttendenceHistoryDAO.5"), e);
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public AttendenceHistory findHistoryByClockedInTime(User user) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(AttendenceHistory.class);
      criteria.add(Restrictions.eq(AttendenceHistory.PROP_USER, user));
      criteria.add(Restrictions.isNull(AttendenceHistory.PROP_CLOCK_OUT_TIME));
      criteria.addOrder(Order.desc(AttendenceHistory.PROP_CLOCK_IN_TIME));
      criteria.setFirstResult(0);
      criteria.setMaxResults(1);
      
      List<AttendenceHistory> list = criteria.list();
      AttendenceHistory localAttendenceHistory; if (list.size() > 0) {
        return (AttendenceHistory)list.get(0);
      }
      
      return null;
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public List<PayrollReportData> findPayroll(Date from, Date to, User user) {
    Session session = null;
    ArrayList<PayrollReportData> reportDataList = new ArrayList();
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(AttendenceHistory.class);
      criteria.add(Restrictions.ge(AttendenceHistory.PROP_CLOCK_IN_TIME, from));
      criteria.add(Restrictions.le(AttendenceHistory.PROP_CLOCK_OUT_TIME, to));
      criteria.addOrder(Order.asc(AttendenceHistory.PROP_USER));
      criteria.addOrder(Order.asc(AttendenceHistory.PROP_CLOCK_IN_TIME));
      if (user != null) {
        criteria.add(Restrictions.eq(AttendenceHistory.PROP_USER, user));
      }
      
      populateReportDataList(reportDataList, criteria.list());
      List<User> linkedUsers; if (user != null) {
        linkedUsers = user.getLinkedUser();
        for (User linkedUser : linkedUsers)
          if (!linkedUser.getId().equals(user.getId()))
          {

            criteria = session.createCriteria(AttendenceHistory.class);
            criteria.add(Restrictions.ge(AttendenceHistory.PROP_CLOCK_IN_TIME, from));
            criteria.add(Restrictions.le(AttendenceHistory.PROP_CLOCK_OUT_TIME, to));
            criteria.addOrder(Order.asc(AttendenceHistory.PROP_USER));
            criteria.addOrder(Order.asc(AttendenceHistory.PROP_CLOCK_IN_TIME));
            criteria.add(Restrictions.eq(AttendenceHistory.PROP_USER, linkedUser));
            populateReportDataList(reportDataList, criteria.list());
          }
      }
      return reportDataList;
    } catch (Exception e) {
      throw new PosException(Messages.getString("AttendenceHistoryDAO.6"), e);
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  private void populateReportDataList(ArrayList<PayrollReportData> reportDataList, List searchResultList) {
    for (Iterator iterator = searchResultList.iterator(); iterator.hasNext();) {
      AttendenceHistory history = (AttendenceHistory)iterator.next();
      PayrollReportData data = new PayrollReportData();
      data.setFrom(history.getClockInTime());
      data.setTo(history.getClockOutTime());
      data.setDate(history.getClockInTime());
      data.setUser(history.getUser());
      data.calculate();
      
      reportDataList.add(data);
    }
  }
  
  public List<WeeklyPayrollReportData> findWeeklyPayroll(Date from, Date to, User user, int firstDayOfWeek) {
    Session session = null;
    List<WeeklyPayrollReportData> reportDataList = new ArrayList();
    List<Date[]> searchDateList = new ArrayList();
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(AttendenceHistory.class);
      
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(from);
      Date[] dateRange = new Date[2];
      dateRange[0] = from;
      int dayOfWeek; while (!from.after(to)) {
        dayOfWeek = calendar.get(7);
        int difference = dayOfWeek - (firstDayOfWeek - 1);
        calendar.add(5, 1);
        
        if ((difference == 0) || (calendar.getTime().after(to))) {
          calendar.add(5, -1);
          Date toDate = calendar.getTime();
          dateRange[1] = toDate;
          searchDateList.add(dateRange);
          dateRange = new Date[2];
          calendar.add(5, 1);
          from = calendar.getTime();
          dateRange[0] = from;
        }
      }
      
      for (Date[] dateRangeArray : searchDateList) {
        criteria = session.createCriteria(AttendenceHistory.class);
        startOfDay = DateUtil.startOfDay(dateRangeArray[0]);
        endOfDay = DateUtil.endOfDay(dateRangeArray[1]);
        
        criteria.add(Restrictions.ge(AttendenceHistory.PROP_CLOCK_IN_TIME, startOfDay));
        criteria.add(Restrictions.le(AttendenceHistory.PROP_CLOCK_OUT_TIME, endOfDay));
        criteria.addOrder(Order.asc(AttendenceHistory.PROP_USER));
        criteria.addOrder(Order.asc(AttendenceHistory.PROP_CLOCK_IN_TIME));
        if (user != null) {
          criteria.add(Restrictions.eq(AttendenceHistory.PROP_USER, user));
        }
        
        reportDataList.addAll(populateWeeklyPayrollReportDataList(criteria.list(), startOfDay, endOfDay, firstDayOfWeek));
        if (user != null) {
          List<User> linkedUsers = user.getLinkedUser();
          for (User linkedUser : linkedUsers)
            if (!linkedUser.getId().equals(user.getId()))
            {

              criteria = session.createCriteria(AttendenceHistory.class);
              criteria.add(Restrictions.ge(AttendenceHistory.PROP_CLOCK_IN_TIME, from));
              criteria.add(Restrictions.le(AttendenceHistory.PROP_CLOCK_OUT_TIME, to));
              criteria.addOrder(Order.asc(AttendenceHistory.PROP_USER));
              criteria.addOrder(Order.asc(AttendenceHistory.PROP_CLOCK_IN_TIME));
              criteria.add(Restrictions.eq(AttendenceHistory.PROP_USER, linkedUser));
              reportDataList.addAll(populateWeeklyPayrollReportDataList(criteria.list(), startOfDay, endOfDay, firstDayOfWeek));
            } } }
      Date startOfDay;
      Date endOfDay;
      return reportDataList;
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  private List<WeeklyPayrollReportData> populateWeeklyPayrollReportDataList(List searchResultList, Date from, Date to, int firstDayOfWeek) {
    Map<String, WeeklyPayrollReportData> reportDataMap = new HashMap();
    for (Iterator iterator = searchResultList.iterator(); iterator.hasNext();) {
      history = (AttendenceHistory)iterator.next();
      WeeklyPayrollReportData data = (WeeklyPayrollReportData)reportDataMap.get(history.getUser().getId());
      if (data == null) {
        data = new WeeklyPayrollReportData();
        data.setFirstDayOfWeek(firstDayOfWeek);
        reportDataMap.put(history.getUser().getId(), data);
      }
      data.setTotalWorkHourMs(data.getTotalWorkHourMs() + (history.getClockOutTime().getTime() - history.getClockInTime().getTime()));
      data.setFromDateOfWeek(from);
      data.setToDateOfWeek(to);
      data.setUser(history.getUser()); }
    AttendenceHistory history;
    Collection<WeeklyPayrollReportData> values = reportDataMap.values();
    for (WeeklyPayrollReportData data : values) {
      data.calculateTotalHour();
    }
    return new ArrayList(values);
  }
  
  public List<AttendanceReportData> findAttendance(Date from, Date to, User user) {
    Session session = null;
    
    ArrayList<AttendanceReportData> list = new ArrayList();
    
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(AttendenceHistory.class);
      criteria.add(Restrictions.ge(AttendenceHistory.PROP_CLOCK_IN_TIME, from));
      criteria.add(Restrictions.or(Restrictions.isNull(AttendenceHistory.PROP_CLOCK_OUT_TIME), Restrictions.le(AttendenceHistory.PROP_CLOCK_OUT_TIME, to)));
      criteria.addOrder(Order.asc(AttendenceHistory.PROP_USER));
      if (user != null) {
        criteria.add(Restrictions.eq(AttendenceHistory.PROP_USER, user));
      }
      List list2 = criteria.list();
      
      for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
        AttendenceHistory history = (AttendenceHistory)iterator.next();
        AttendanceReportData data = new AttendanceReportData();
        data.setClockIn(history.getClockInTime());
        data.setClockOut(history.getClockOutTime());
        data.setUser(history.getUser());
        data.setName(history.getUser().getFirstName());
        data.calculate();
        
        list.add(data);
      }
      
      return list;
    } catch (Exception e) {
      throw new PosException("Unable to find Attendance", e);
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public List<AttendenceHistory> findHistory(Date from, Date toDate, User user) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(AttendenceHistory.class);
      criteria.add(Restrictions.ge(AttendenceHistory.PROP_CLOCK_IN_TIME, from));
      criteria.addOrder(Order.asc(AttendenceHistory.PROP_ID));
      if (user != null) {
        criteria.add(Restrictions.eq(AttendenceHistory.PROP_USER, user));
      }
      
      List list2 = criteria.list();
      
      return list2;
    } catch (Exception e) {
      throw new PosException("Unable to find History", e);
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public List<AttendenceHistory> findAttendanceHistory(Date from, Date to, User user) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(AttendenceHistory.class);
      criteria.add(Restrictions.ge(AttendenceHistory.PROP_CLOCK_IN_TIME, from));
      criteria.add(Restrictions.le(AttendenceHistory.PROP_CLOCK_OUT_TIME, to));
      criteria.addOrder(Order.asc(AttendenceHistory.PROP_ID));
      if (user != null) {
        criteria.add(Restrictions.eq(AttendenceHistory.PROP_USER, user));
      }
      
      List list2 = criteria.list();
      
      return list2;
    } catch (Exception e) {
      throw new PosException("Unable to find History", e);
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
}
