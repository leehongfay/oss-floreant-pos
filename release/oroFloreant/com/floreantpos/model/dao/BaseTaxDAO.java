package com.floreantpos.model.dao;

import com.floreantpos.model.Tax;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseTaxDAO
  extends _RootDAO
{
  public static TaxDAO instance;
  
  public BaseTaxDAO() {}
  
  public static TaxDAO getInstance()
  {
    if (null == instance) instance = new TaxDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return Tax.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public Tax cast(Object object)
  {
    return (Tax)object;
  }
  
  public Tax get(String key) throws HibernateException
  {
    return (Tax)get(getReferenceClass(), key);
  }
  
  public Tax get(String key, Session s) throws HibernateException
  {
    return (Tax)get(getReferenceClass(), key, s);
  }
  
  public Tax load(String key) throws HibernateException
  {
    return (Tax)load(getReferenceClass(), key);
  }
  
  public Tax load(String key, Session s) throws HibernateException
  {
    return (Tax)load(getReferenceClass(), key, s);
  }
  
  public Tax loadInitialize(String key, Session s) throws HibernateException
  {
    Tax obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<Tax> findAll()
  {
    return super.findAll();
  }
  


  public List<Tax> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<Tax> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(Tax tax)
    throws HibernateException
  {
    return (String)super.save(tax);
  }
  







  public String save(Tax tax, Session s)
    throws HibernateException
  {
    return (String)save(tax, s);
  }
  





  public void saveOrUpdate(Tax tax)
    throws HibernateException
  {
    saveOrUpdate(tax);
  }
  







  public void saveOrUpdate(Tax tax, Session s)
    throws HibernateException
  {
    saveOrUpdate(tax, s);
  }
  




  public void update(Tax tax)
    throws HibernateException
  {
    update(tax);
  }
  






  public void update(Tax tax, Session s)
    throws HibernateException
  {
    update(tax, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(Tax tax)
    throws HibernateException
  {
    delete(tax);
  }
  






  public void delete(Tax tax, Session s)
    throws HibernateException
  {
    delete(tax, s);
  }
  









  public void refresh(Tax tax, Session s)
    throws HibernateException
  {
    refresh(tax, s);
  }
}
