package com.floreantpos.model.dao;

import com.floreantpos.model.ComboItem;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;





public class ComboItemDAO
  extends BaseComboItemDAO
{
  public ComboItemDAO() {}
  
  public List<ComboItem> getByMenuItem(String menuItemId)
  {
    if (StringUtils.isEmpty(menuItemId)) {
      return new ArrayList(0);
    }
    Session session = createNewSession();Throwable localThrowable3 = null;
    try { Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(ComboItem.PROP_MENU_ITEM_ID, menuItemId));
      return criteria.list();
    }
    catch (Throwable localThrowable1)
    {
      localThrowable3 = localThrowable1;throw localThrowable1;
    }
    finally
    {
      if (session != null) if (localThrowable3 != null) try { session.close(); } catch (Throwable localThrowable2) { localThrowable3.addSuppressed(localThrowable2); } else session.close();
    }
  }
}
