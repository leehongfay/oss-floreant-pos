package com.floreantpos.model.dao;

import com.floreantpos.model.CookingInstruction;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseCookingInstructionDAO
  extends _RootDAO
{
  public static CookingInstructionDAO instance;
  
  public BaseCookingInstructionDAO() {}
  
  public static CookingInstructionDAO getInstance()
  {
    if (null == instance) instance = new CookingInstructionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return CookingInstruction.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public CookingInstruction cast(Object object)
  {
    return (CookingInstruction)object;
  }
  
  public CookingInstruction get(String key) throws HibernateException
  {
    return (CookingInstruction)get(getReferenceClass(), key);
  }
  
  public CookingInstruction get(String key, Session s) throws HibernateException
  {
    return (CookingInstruction)get(getReferenceClass(), key, s);
  }
  
  public CookingInstruction load(String key) throws HibernateException
  {
    return (CookingInstruction)load(getReferenceClass(), key);
  }
  
  public CookingInstruction load(String key, Session s) throws HibernateException
  {
    return (CookingInstruction)load(getReferenceClass(), key, s);
  }
  
  public CookingInstruction loadInitialize(String key, Session s) throws HibernateException
  {
    CookingInstruction obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<CookingInstruction> findAll()
  {
    return super.findAll();
  }
  


  public List<CookingInstruction> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<CookingInstruction> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(CookingInstruction cookingInstruction)
    throws HibernateException
  {
    return (String)super.save(cookingInstruction);
  }
  







  public String save(CookingInstruction cookingInstruction, Session s)
    throws HibernateException
  {
    return (String)save(cookingInstruction, s);
  }
  





  public void saveOrUpdate(CookingInstruction cookingInstruction)
    throws HibernateException
  {
    saveOrUpdate(cookingInstruction);
  }
  







  public void saveOrUpdate(CookingInstruction cookingInstruction, Session s)
    throws HibernateException
  {
    saveOrUpdate(cookingInstruction, s);
  }
  




  public void update(CookingInstruction cookingInstruction)
    throws HibernateException
  {
    update(cookingInstruction);
  }
  






  public void update(CookingInstruction cookingInstruction, Session s)
    throws HibernateException
  {
    update(cookingInstruction, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(CookingInstruction cookingInstruction)
    throws HibernateException
  {
    delete(cookingInstruction);
  }
  






  public void delete(CookingInstruction cookingInstruction, Session s)
    throws HibernateException
  {
    delete(cookingInstruction, s);
  }
  









  public void refresh(CookingInstruction cookingInstruction, Session s)
    throws HibernateException
  {
    refresh(cookingInstruction, s);
  }
}
