package com.floreantpos.model.dao;

import com.floreantpos.model.MenuItemModifierSpec;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseMenuItemModifierSpecDAO
  extends _RootDAO
{
  public static MenuItemModifierSpecDAO instance;
  
  public BaseMenuItemModifierSpecDAO() {}
  
  public static MenuItemModifierSpecDAO getInstance()
  {
    if (null == instance) instance = new MenuItemModifierSpecDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return MenuItemModifierSpec.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public MenuItemModifierSpec cast(Object object)
  {
    return (MenuItemModifierSpec)object;
  }
  
  public MenuItemModifierSpec get(String key) throws HibernateException
  {
    return (MenuItemModifierSpec)get(getReferenceClass(), key);
  }
  
  public MenuItemModifierSpec get(String key, Session s) throws HibernateException
  {
    return (MenuItemModifierSpec)get(getReferenceClass(), key, s);
  }
  
  public MenuItemModifierSpec load(String key) throws HibernateException
  {
    return (MenuItemModifierSpec)load(getReferenceClass(), key);
  }
  
  public MenuItemModifierSpec load(String key, Session s) throws HibernateException
  {
    return (MenuItemModifierSpec)load(getReferenceClass(), key, s);
  }
  
  public MenuItemModifierSpec loadInitialize(String key, Session s) throws HibernateException
  {
    MenuItemModifierSpec obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<MenuItemModifierSpec> findAll()
  {
    return super.findAll();
  }
  


  public List<MenuItemModifierSpec> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<MenuItemModifierSpec> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(MenuItemModifierSpec menuItemModifierSpec)
    throws HibernateException
  {
    return (String)super.save(menuItemModifierSpec);
  }
  







  public String save(MenuItemModifierSpec menuItemModifierSpec, Session s)
    throws HibernateException
  {
    return (String)save(menuItemModifierSpec, s);
  }
  





  public void saveOrUpdate(MenuItemModifierSpec menuItemModifierSpec)
    throws HibernateException
  {
    saveOrUpdate(menuItemModifierSpec);
  }
  







  public void saveOrUpdate(MenuItemModifierSpec menuItemModifierSpec, Session s)
    throws HibernateException
  {
    saveOrUpdate(menuItemModifierSpec, s);
  }
  




  public void update(MenuItemModifierSpec menuItemModifierSpec)
    throws HibernateException
  {
    update(menuItemModifierSpec);
  }
  






  public void update(MenuItemModifierSpec menuItemModifierSpec, Session s)
    throws HibernateException
  {
    update(menuItemModifierSpec, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(MenuItemModifierSpec menuItemModifierSpec)
    throws HibernateException
  {
    delete(menuItemModifierSpec);
  }
  






  public void delete(MenuItemModifierSpec menuItemModifierSpec, Session s)
    throws HibernateException
  {
    delete(menuItemModifierSpec, s);
  }
  









  public void refresh(MenuItemModifierSpec menuItemModifierSpec, Session s)
    throws HibernateException
  {
    refresh(menuItemModifierSpec, s);
  }
}
