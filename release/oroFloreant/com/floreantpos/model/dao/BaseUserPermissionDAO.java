package com.floreantpos.model.dao;

import com.floreantpos.model.UserPermission;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseUserPermissionDAO
  extends _RootDAO
{
  public static UserPermissionDAO instance;
  
  public BaseUserPermissionDAO() {}
  
  public static UserPermissionDAO getInstance()
  {
    if (null == instance) instance = new UserPermissionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return UserPermission.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public UserPermission cast(Object object)
  {
    return (UserPermission)object;
  }
  
  public UserPermission get(String key) throws HibernateException
  {
    return (UserPermission)get(getReferenceClass(), key);
  }
  
  public UserPermission get(String key, Session s) throws HibernateException
  {
    return (UserPermission)get(getReferenceClass(), key, s);
  }
  
  public UserPermission load(String key) throws HibernateException
  {
    return (UserPermission)load(getReferenceClass(), key);
  }
  
  public UserPermission load(String key, Session s) throws HibernateException
  {
    return (UserPermission)load(getReferenceClass(), key, s);
  }
  
  public UserPermission loadInitialize(String key, Session s) throws HibernateException
  {
    UserPermission obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<UserPermission> findAll()
  {
    return super.findAll();
  }
  


  public List<UserPermission> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<UserPermission> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(UserPermission userPermission)
    throws HibernateException
  {
    return (String)super.save(userPermission);
  }
  







  public String save(UserPermission userPermission, Session s)
    throws HibernateException
  {
    return (String)save(userPermission, s);
  }
  





  public void saveOrUpdate(UserPermission userPermission)
    throws HibernateException
  {
    saveOrUpdate(userPermission);
  }
  







  public void saveOrUpdate(UserPermission userPermission, Session s)
    throws HibernateException
  {
    saveOrUpdate(userPermission, s);
  }
  




  public void update(UserPermission userPermission)
    throws HibernateException
  {
    update(userPermission);
  }
  






  public void update(UserPermission userPermission, Session s)
    throws HibernateException
  {
    update(userPermission, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(UserPermission userPermission)
    throws HibernateException
  {
    delete(userPermission);
  }
  






  public void delete(UserPermission userPermission, Session s)
    throws HibernateException
  {
    delete(userPermission, s);
  }
  









  public void refresh(UserPermission userPermission, Session s)
    throws HibernateException
  {
    refresh(userPermission, s);
  }
}
