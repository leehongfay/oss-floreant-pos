package com.floreantpos.model.dao;

import com.floreantpos.model.SalesArea;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseSalesAreaDAO
  extends _RootDAO
{
  public static SalesAreaDAO instance;
  
  public BaseSalesAreaDAO() {}
  
  public static SalesAreaDAO getInstance()
  {
    if (null == instance) instance = new SalesAreaDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return SalesArea.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public SalesArea cast(Object object)
  {
    return (SalesArea)object;
  }
  
  public SalesArea get(String key) throws HibernateException
  {
    return (SalesArea)get(getReferenceClass(), key);
  }
  
  public SalesArea get(String key, Session s) throws HibernateException
  {
    return (SalesArea)get(getReferenceClass(), key, s);
  }
  
  public SalesArea load(String key) throws HibernateException
  {
    return (SalesArea)load(getReferenceClass(), key);
  }
  
  public SalesArea load(String key, Session s) throws HibernateException
  {
    return (SalesArea)load(getReferenceClass(), key, s);
  }
  
  public SalesArea loadInitialize(String key, Session s) throws HibernateException
  {
    SalesArea obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<SalesArea> findAll()
  {
    return super.findAll();
  }
  


  public List<SalesArea> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<SalesArea> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(SalesArea salesArea)
    throws HibernateException
  {
    return (String)super.save(salesArea);
  }
  







  public String save(SalesArea salesArea, Session s)
    throws HibernateException
  {
    return (String)save(salesArea, s);
  }
  





  public void saveOrUpdate(SalesArea salesArea)
    throws HibernateException
  {
    saveOrUpdate(salesArea);
  }
  







  public void saveOrUpdate(SalesArea salesArea, Session s)
    throws HibernateException
  {
    saveOrUpdate(salesArea, s);
  }
  




  public void update(SalesArea salesArea)
    throws HibernateException
  {
    update(salesArea);
  }
  






  public void update(SalesArea salesArea, Session s)
    throws HibernateException
  {
    update(salesArea, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(SalesArea salesArea)
    throws HibernateException
  {
    delete(salesArea);
  }
  






  public void delete(SalesArea salesArea, Session s)
    throws HibernateException
  {
    delete(salesArea, s);
  }
  









  public void refresh(SalesArea salesArea, Session s)
    throws HibernateException
  {
    refresh(salesArea, s);
  }
}
