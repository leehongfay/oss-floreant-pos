package com.floreantpos.model.dao;

import com.floreantpos.model.PizzaModifierPrice;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BasePizzaModifierPriceDAO
  extends _RootDAO
{
  public static PizzaModifierPriceDAO instance;
  
  public BasePizzaModifierPriceDAO() {}
  
  public static PizzaModifierPriceDAO getInstance()
  {
    if (null == instance) instance = new PizzaModifierPriceDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return PizzaModifierPrice.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public PizzaModifierPrice cast(Object object)
  {
    return (PizzaModifierPrice)object;
  }
  
  public PizzaModifierPrice get(String key) throws HibernateException
  {
    return (PizzaModifierPrice)get(getReferenceClass(), key);
  }
  
  public PizzaModifierPrice get(String key, Session s) throws HibernateException
  {
    return (PizzaModifierPrice)get(getReferenceClass(), key, s);
  }
  
  public PizzaModifierPrice load(String key) throws HibernateException
  {
    return (PizzaModifierPrice)load(getReferenceClass(), key);
  }
  
  public PizzaModifierPrice load(String key, Session s) throws HibernateException
  {
    return (PizzaModifierPrice)load(getReferenceClass(), key, s);
  }
  
  public PizzaModifierPrice loadInitialize(String key, Session s) throws HibernateException
  {
    PizzaModifierPrice obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<PizzaModifierPrice> findAll()
  {
    return super.findAll();
  }
  


  public List<PizzaModifierPrice> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<PizzaModifierPrice> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(PizzaModifierPrice pizzaModifierPrice)
    throws HibernateException
  {
    return (String)super.save(pizzaModifierPrice);
  }
  







  public String save(PizzaModifierPrice pizzaModifierPrice, Session s)
    throws HibernateException
  {
    return (String)save(pizzaModifierPrice, s);
  }
  





  public void saveOrUpdate(PizzaModifierPrice pizzaModifierPrice)
    throws HibernateException
  {
    saveOrUpdate(pizzaModifierPrice);
  }
  







  public void saveOrUpdate(PizzaModifierPrice pizzaModifierPrice, Session s)
    throws HibernateException
  {
    saveOrUpdate(pizzaModifierPrice, s);
  }
  




  public void update(PizzaModifierPrice pizzaModifierPrice)
    throws HibernateException
  {
    update(pizzaModifierPrice);
  }
  






  public void update(PizzaModifierPrice pizzaModifierPrice, Session s)
    throws HibernateException
  {
    update(pizzaModifierPrice, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(PizzaModifierPrice pizzaModifierPrice)
    throws HibernateException
  {
    delete(pizzaModifierPrice);
  }
  






  public void delete(PizzaModifierPrice pizzaModifierPrice, Session s)
    throws HibernateException
  {
    delete(pizzaModifierPrice, s);
  }
  









  public void refresh(PizzaModifierPrice pizzaModifierPrice, Session s)
    throws HibernateException
  {
    refresh(pizzaModifierPrice, s);
  }
}
