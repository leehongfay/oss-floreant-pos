package com.floreantpos.model.dao;

import com.floreantpos.model.GiftCard;
import com.floreantpos.swing.PaginationSupport;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;





public class GiftCardDAO
  extends BaseGiftCardDAO
{
  public GiftCardDAO() {}
  
  public void saveAsList(List<GiftCard> giftCardList)
  {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      for (GiftCard giftCard : giftCardList) {
        String cardNumber = giftCard.getCardNumber();
        if (cardNumber != null)
        {

          cardNumber = cardNumber.replaceAll("-", "");
          giftCard.setCardNumber(cardNumber);
          GiftCard card = (GiftCard)session.get(GiftCard.class, cardNumber);
          if (card == null) {
            session.save(giftCard);
          }
          else
            session.merge(giftCard);
        }
      }
      tx.commit();
    }
    catch (Exception e) {
      tx.rollback();
      throw e;
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public GiftCard findByCardNumber(String cardNumber) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(GiftCard.class);
      GiftCard localGiftCard;
      if (StringUtils.isNotEmpty(cardNumber)) {
        criteria.add(Restrictions.eq(GiftCard.PROP_CARD_NUMBER, cardNumber));
        
        if (criteria.list().isEmpty()) {
          return null;
        }
        return (GiftCard)criteria.list().get(0);
      }
      
      return null;
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public void searchByCardAndIssueDate(String cardNumber, String batchNumber, Date fromDate, Date toDate, PaginationSupport model) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(GiftCard.class);
      criteria.add(Restrictions.eq(GiftCard.PROP_DISABLE, Boolean.valueOf(false)));
      if ((fromDate != null) && (toDate != null))
        criteria.add(Restrictions.between(GiftCard.PROP_ISSUE_DATE, fromDate, toDate));
      if (StringUtils.isNotEmpty(cardNumber)) {
        criteria.add(Restrictions.eq(GiftCard.PROP_CARD_NUMBER, cardNumber));
      }
      if (StringUtils.isNotEmpty(batchNumber)) {
        criteria.add(Restrictions.eq(GiftCard.PROP_BATCH_NO, batchNumber));
      }
      
      criteria.setProjection(Projections.rowCount());
      Number uniqueResult = (Number)criteria.uniqueResult();
      model.setNumRows(uniqueResult.intValue());
      criteria.setProjection(null);
      
      criteria.addOrder(Order.asc(GiftCard.PROP_ISSUE_DATE));
      criteria.setFirstResult(model.getCurrentRowIndex());
      criteria.setMaxResults(model.getPageSize());
      List<GiftCard> result = criteria.list();
      model.setRows(result);
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public List<Date> findByDate() {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(GiftCard.class);
      
      criteria.setProjection(Projections.distinct(Projections.property(GiftCard.PROP_ISSUE_DATE)));
      Object localObject1; if (criteria.list().isEmpty()) {
        return null;
      }
      return criteria.list();
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public List<GiftCard> findExceptDisable() {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(GiftCard.class);
      criteria.add(Restrictions.eq(GiftCard.PROP_DISABLE, Boolean.valueOf(false)));
      List localList;
      if (!criteria.list().isEmpty()) {
        return criteria.list();
      }
      return null;
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public GiftCard initialize(GiftCard giftCard) {
    if (giftCard == null) {
      return giftCard;
    }
    Session session = null;
    try
    {
      session = createNewSession();
      session.refresh(giftCard);
      
      return giftCard;
    } finally {
      closeSession(session);
    }
  }
  
  public boolean findActiveCardByBatchNumber(String batchNumber) {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(GiftCard.class);
      boolean bool;
      if (StringUtils.isNotEmpty(batchNumber)) {
        criteria.add(Restrictions.eq(GiftCard.PROP_BATCH_NO, batchNumber));
        

        criteria.add(Restrictions.or(Restrictions.eq(GiftCard.PROP_ACTIVE, Boolean.valueOf(true)), Restrictions.and(Restrictions.eq(GiftCard.PROP_ACTIVE, Boolean.valueOf(false)), Restrictions.isNotNull(GiftCard.PROP_ACTIVATION_DATE))));
        if (criteria.list().isEmpty()) {
          return false;
        }
        return true;
      }
      
      return false;
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public boolean deleteCardListByBatchNumber(String batchNumber) {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(GiftCard.class);
      criteria.add(Restrictions.eq(GiftCard.PROP_DISABLE, Boolean.valueOf(false)));
      
      List<GiftCard> giftCardList = null;
      boolean bool; if (StringUtils.isNotEmpty(batchNumber)) {
        criteria.add(Restrictions.eq(GiftCard.PROP_BATCH_NO, batchNumber));
        
        giftCardList = criteria.list();
        
        if (giftCardList != null) {
          for (GiftCard giftCard : giftCardList) {
            delete(giftCard);
          }
          return true;
        }
      }
      

      return false;
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public List<GiftCard> findByBatchNumber(String batchNO) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(GiftCard.class);
      criteria.add(Restrictions.eq(GiftCard.PROP_BATCH_NO, batchNO));
      List localList;
      if (!criteria.list().isEmpty()) {
        return criteria.list();
      }
      
      return null;
    }
    finally {
      closeSession(session);
    }
  }
  
  public boolean hasBatchNo(String batchNO) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(GiftCard.class);
      criteria.add(Restrictions.eq(GiftCard.PROP_BATCH_NO, batchNO));
      criteria.setMaxResults(1);
      
      return criteria.list().size() > 0;
    } finally {
      closeSession(session);
    }
  }
  
  public List<GiftCard> findGiftCards(Date fromDate, Date toDate, boolean isExpired, String sortOption) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(GiftCard.class);
      criteria.add(Restrictions.between(GiftCard.PROP_ACTIVATION_DATE, fromDate, toDate));
      if (isExpired) {
        criteria.add(Restrictions.lt(GiftCard.PROP_EXPIRY_DATE, new Date()));
      }
      else {
        criteria.add(Restrictions.ge(GiftCard.PROP_EXPIRY_DATE, new Date()));
      }
      if ("Owner".equals(sortOption)) {
        criteria.addOrder(Order.asc(GiftCard.PROP_OWNER_NAME));
      } else {
        criteria.addOrder(Order.asc(GiftCard.PROP_ACTIVATION_DATE));
      }
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
}
