package com.floreantpos.model.dao;

import com.floreantpos.PosLog;
import com.floreantpos.model.MenuItemModifierPage;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.ModifierGroup;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;






















public class MenuItemModifierSpecDAO
  extends BaseMenuItemModifierSpecDAO
{
  public MenuItemModifierSpecDAO() {}
  
  public MenuItemModifierSpec initialize(MenuItemModifierSpec menuItemModifierSpec)
  {
    if ((menuItemModifierSpec == null) || (menuItemModifierSpec.getId() == null)) {
      return menuItemModifierSpec;
    }
    Session session = null;
    try
    {
      session = createNewSession();
      session.refresh(menuItemModifierSpec);
      
      Hibernate.initialize(menuItemModifierSpec.getDefaultModifierList());
      Hibernate.initialize(menuItemModifierSpec.getModifierPages());
      
      return menuItemModifierSpec;
    } finally {
      closeSession(session);
    }
  }
  
  public MenuItemModifierSpec findByGroup(ModifierGroup modifierGroup) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(MenuItemModifierSpec.PROP_MODIFIER_GROUP_ID, modifierGroup == null ? null : modifierGroup.getId()));
      List list = criteria.list();
      if ((list != null) && (!list.isEmpty())) {
        return (MenuItemModifierSpec)list.get(0);
      }
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    } finally {
      session.close();
    }
    return null;
  }
  
  public void saveOrUpdate(MenuItemModifierSpec spec, MenuItemModifierPage page) {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      session.saveOrUpdate(page);
      session.saveOrUpdate(spec);
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      PosLog.error(getClass(), e);
    } finally {
      session.close();
    }
  }
}
