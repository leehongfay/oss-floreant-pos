package com.floreantpos.model.dao;

import com.floreantpos.model.Course;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseCourseDAO
  extends _RootDAO
{
  public static CourseDAO instance;
  
  public BaseCourseDAO() {}
  
  public static CourseDAO getInstance()
  {
    if (null == instance) instance = new CourseDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return Course.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public Course cast(Object object)
  {
    return (Course)object;
  }
  
  public Course get(String key) throws HibernateException
  {
    return (Course)get(getReferenceClass(), key);
  }
  
  public Course get(String key, Session s) throws HibernateException
  {
    return (Course)get(getReferenceClass(), key, s);
  }
  
  public Course load(String key) throws HibernateException
  {
    return (Course)load(getReferenceClass(), key);
  }
  
  public Course load(String key, Session s) throws HibernateException
  {
    return (Course)load(getReferenceClass(), key, s);
  }
  
  public Course loadInitialize(String key, Session s) throws HibernateException
  {
    Course obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<Course> findAll()
  {
    return super.findAll();
  }
  


  public List<Course> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<Course> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(Course course)
    throws HibernateException
  {
    return (String)super.save(course);
  }
  







  public String save(Course course, Session s)
    throws HibernateException
  {
    return (String)save(course, s);
  }
  





  public void saveOrUpdate(Course course)
    throws HibernateException
  {
    saveOrUpdate(course);
  }
  







  public void saveOrUpdate(Course course, Session s)
    throws HibernateException
  {
    saveOrUpdate(course, s);
  }
  




  public void update(Course course)
    throws HibernateException
  {
    update(course);
  }
  






  public void update(Course course, Session s)
    throws HibernateException
  {
    update(course, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(Course course)
    throws HibernateException
  {
    delete(course);
  }
  






  public void delete(Course course, Session s)
    throws HibernateException
  {
    delete(course, s);
  }
  









  public void refresh(Course course, Session s)
    throws HibernateException
  {
    refresh(course, s);
  }
}
