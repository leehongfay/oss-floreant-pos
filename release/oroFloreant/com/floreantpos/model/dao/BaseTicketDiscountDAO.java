package com.floreantpos.model.dao;

import com.floreantpos.model.TicketDiscount;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseTicketDiscountDAO
  extends _RootDAO
{
  public static TicketDiscountDAO instance;
  
  public BaseTicketDiscountDAO() {}
  
  public static TicketDiscountDAO getInstance()
  {
    if (null == instance) instance = new TicketDiscountDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return TicketDiscount.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public TicketDiscount cast(Object object)
  {
    return (TicketDiscount)object;
  }
  
  public TicketDiscount get(String key) throws HibernateException
  {
    return (TicketDiscount)get(getReferenceClass(), key);
  }
  
  public TicketDiscount get(String key, Session s) throws HibernateException
  {
    return (TicketDiscount)get(getReferenceClass(), key, s);
  }
  
  public TicketDiscount load(String key) throws HibernateException
  {
    return (TicketDiscount)load(getReferenceClass(), key);
  }
  
  public TicketDiscount load(String key, Session s) throws HibernateException
  {
    return (TicketDiscount)load(getReferenceClass(), key, s);
  }
  
  public TicketDiscount loadInitialize(String key, Session s) throws HibernateException
  {
    TicketDiscount obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<TicketDiscount> findAll()
  {
    return super.findAll();
  }
  


  public List<TicketDiscount> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<TicketDiscount> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(TicketDiscount ticketDiscount)
    throws HibernateException
  {
    return (String)super.save(ticketDiscount);
  }
  







  public String save(TicketDiscount ticketDiscount, Session s)
    throws HibernateException
  {
    return (String)save(ticketDiscount, s);
  }
  





  public void saveOrUpdate(TicketDiscount ticketDiscount)
    throws HibernateException
  {
    saveOrUpdate(ticketDiscount);
  }
  







  public void saveOrUpdate(TicketDiscount ticketDiscount, Session s)
    throws HibernateException
  {
    saveOrUpdate(ticketDiscount, s);
  }
  




  public void update(TicketDiscount ticketDiscount)
    throws HibernateException
  {
    update(ticketDiscount);
  }
  






  public void update(TicketDiscount ticketDiscount, Session s)
    throws HibernateException
  {
    update(ticketDiscount, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(TicketDiscount ticketDiscount)
    throws HibernateException
  {
    delete(ticketDiscount);
  }
  






  public void delete(TicketDiscount ticketDiscount, Session s)
    throws HibernateException
  {
    delete(ticketDiscount, s);
  }
  









  public void refresh(TicketDiscount ticketDiscount, Session s)
    throws HibernateException
  {
    refresh(ticketDiscount, s);
  }
}
