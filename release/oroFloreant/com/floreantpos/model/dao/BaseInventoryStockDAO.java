package com.floreantpos.model.dao;

import com.floreantpos.model.InventoryStock;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseInventoryStockDAO
  extends _RootDAO
{
  public static InventoryStockDAO instance;
  
  public BaseInventoryStockDAO() {}
  
  public static InventoryStockDAO getInstance()
  {
    if (null == instance) instance = new InventoryStockDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return InventoryStock.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public InventoryStock cast(Object object)
  {
    return (InventoryStock)object;
  }
  
  public InventoryStock get(String key) throws HibernateException
  {
    return (InventoryStock)get(getReferenceClass(), key);
  }
  
  public InventoryStock get(String key, Session s) throws HibernateException
  {
    return (InventoryStock)get(getReferenceClass(), key, s);
  }
  
  public InventoryStock load(String key) throws HibernateException
  {
    return (InventoryStock)load(getReferenceClass(), key);
  }
  
  public InventoryStock load(String key, Session s) throws HibernateException
  {
    return (InventoryStock)load(getReferenceClass(), key, s);
  }
  
  public InventoryStock loadInitialize(String key, Session s) throws HibernateException
  {
    InventoryStock obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<InventoryStock> findAll()
  {
    return super.findAll();
  }
  


  public List<InventoryStock> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<InventoryStock> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(InventoryStock inventoryStock)
    throws HibernateException
  {
    return (String)super.save(inventoryStock);
  }
  







  public String save(InventoryStock inventoryStock, Session s)
    throws HibernateException
  {
    return (String)save(inventoryStock, s);
  }
  





  public void saveOrUpdate(InventoryStock inventoryStock)
    throws HibernateException
  {
    saveOrUpdate(inventoryStock);
  }
  







  public void saveOrUpdate(InventoryStock inventoryStock, Session s)
    throws HibernateException
  {
    saveOrUpdate(inventoryStock, s);
  }
  




  public void update(InventoryStock inventoryStock)
    throws HibernateException
  {
    update(inventoryStock);
  }
  






  public void update(InventoryStock inventoryStock, Session s)
    throws HibernateException
  {
    update(inventoryStock, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(InventoryStock inventoryStock)
    throws HibernateException
  {
    delete(inventoryStock);
  }
  






  public void delete(InventoryStock inventoryStock, Session s)
    throws HibernateException
  {
    delete(inventoryStock, s);
  }
  









  public void refresh(InventoryStock inventoryStock, Session s)
    throws HibernateException
  {
    refresh(inventoryStock, s);
  }
}
