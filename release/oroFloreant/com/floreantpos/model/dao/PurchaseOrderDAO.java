package com.floreantpos.model.dao;

import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.InventoryTransactionType;
import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.PurchaseOrder;
import com.floreantpos.model.PurchaseOrderItem;
import java.util.Date;
import java.util.List;
import net.authorize.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
























public class PurchaseOrderDAO
  extends BasePurchaseOrderDAO
{
  public PurchaseOrderDAO() {}
  
  public synchronized void saveOrUpdate(PurchaseOrder order, boolean updateStock)
  {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      
      saveOrUpdate(order, updateStock, session);
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      throwException(e);
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public synchronized void saveOrUpdateWithItemAvailBalance(PurchaseOrder order) {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      updateAvailableUnit(order, session);
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      throwException(e);
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  private void updateAvailableUnit(PurchaseOrder purchaseOrder, Session session) {
    List<PurchaseOrderItem> orderItems = purchaseOrder.getOrderItems();
    if (orderItems == null) {
      return;
    }
    for (PurchaseOrderItem purchaseOrderItem : orderItems)
    {
      MenuItem menuItem = purchaseOrderItem.getMenuItem();
      Double quantityToReceive = purchaseOrderItem.getItemQuantity();
      
      MenuItemDAO.getInstance().updateStockQuantity(menuItem.getId(), quantityToReceive.doubleValue(), true, false, session);
    }
    session.saveOrUpdate(purchaseOrder);
  }
  
  public void saveOrUpdate(PurchaseOrder order, boolean updateStock, Session session) {
    if (order.getId() == null) {
      order.setCreatedDate(new Date());
      order.setStatus(Integer.valueOf(0));
    }
    if (updateStock) {
      adjustInventoryItems(session, order);
    }
    session.saveOrUpdate(order);
  }
  
  public void saveOrUpdate(PurchaseOrder order, boolean updateStock, boolean isAvailableUnitUpdate)
  {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      
      if (order.getId() == null) {
        order.setCreatedDate(new Date());
        order.setStatus(Integer.valueOf(0));
      }
      if (updateStock) {
        adjustInventoryItems(session, order, isAvailableUnitUpdate);
      }
      session.saveOrUpdate(order);
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      throwException(e);
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  private void adjustInventoryItems(Session session, PurchaseOrder order) {
    adjustInventoryItems(session, order, true);
  }
  
  private void adjustInventoryItems(Session session, PurchaseOrder order, boolean isAvailableUnitUpdate) {
    List<PurchaseOrderItem> orderItems = order.getOrderItems();
    if (orderItems == null) {
      return;
    }
    boolean fullyReceived = true;
    for (PurchaseOrderItem orderItem : orderItems) {
      orderItem.setQuantityReceived(Double.valueOf(orderItem.getQuantityReceived().doubleValue() + orderItem.getQuantityToReceive().doubleValue()));
      InventoryTransaction stockInTrans = new InventoryTransaction();
      MenuItem menuItem = orderItem.getMenuItem();
      stockInTrans.setMenuItem(menuItem);
      stockInTrans.setQuantity(orderItem.getQuantityToReceive());
      Double unitPrice = orderItem.getUnitPrice();
      stockInTrans.setUnitCost(unitPrice);
      stockInTrans.setToInventoryLocation(order.getInventoryLocation());
      stockInTrans.setTransactionDate(order.getReceivingDate());
      stockInTrans.setVendor(order.getVendor());
      stockInTrans.setUnit(orderItem.getItemUnitName());
      stockInTrans.setTotal(Double.valueOf(stockInTrans.getQuantity().doubleValue() * unitPrice.doubleValue()));
      stockInTrans.setTransactionDate(new Date());
      if (order.getType().equals("DEBIT")) {
        stockInTrans.setReason("PURCHASE");
        stockInTrans.setTransactionType(InventoryTransactionType.IN);
      }
      else {
        stockInTrans.setReason("RETURN");
        stockInTrans.setTransactionType(InventoryTransactionType.OUT);
      }
      InventoryTransactionDAO.getInstance().adjustInventoryStock(stockInTrans, session, isAvailableUnitUpdate, true);
      if (orderItem.getQuantityReceived().doubleValue() != orderItem.getItemQuantity().doubleValue()) {
        fullyReceived = false;
      }
      orderItem.setQuantityToReceive(Double.valueOf(0.0D));
    }
    if (fullyReceived) {
      order.setStatus(Integer.valueOf(4));
    }
    else {
      order.setStatus(Integer.valueOf(5));
    }
  }
  
  /* Error */
  public String getNextOrderSequenceNumber()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aload_0
    //   3: invokevirtual 64	com/floreantpos/model/dao/PurchaseOrderDAO:getSession	()Lorg/hibernate/Session;
    //   6: astore_1
    //   7: aload_1
    //   8: aload_0
    //   9: invokevirtual 65	com/floreantpos/model/dao/PurchaseOrderDAO:getReferenceClass	()Ljava/lang/Class;
    //   12: invokeinterface 66 2 0
    //   17: astore_2
    //   18: aload_2
    //   19: getstatic 67	com/floreantpos/model/PurchaseOrder:PROP_ORDER_ID	Ljava/lang/String;
    //   22: invokestatic 68	org/hibernate/criterion/Projections:max	(Ljava/lang/String;)Lorg/hibernate/criterion/AggregateProjection;
    //   25: invokeinterface 69 2 0
    //   30: pop
    //   31: aload_2
    //   32: invokeinterface 70 1 0
    //   37: astore_3
    //   38: aload_3
    //   39: ifnonnull +20 -> 59
    //   42: ldc 71
    //   44: astore 4
    //   46: aload_1
    //   47: ifnull +9 -> 56
    //   50: aload_1
    //   51: invokeinterface 6 1 0
    //   56: aload 4
    //   58: areturn
    //   59: aload_3
    //   60: checkcast 72	java/lang/String
    //   63: invokestatic 73	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   66: iconst_1
    //   67: iadd
    //   68: invokestatic 74	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   71: astore 4
    //   73: aload_1
    //   74: ifnull +9 -> 83
    //   77: aload_1
    //   78: invokeinterface 6 1 0
    //   83: aload 4
    //   85: areturn
    //   86: astore 4
    //   88: ldc 75
    //   90: astore 5
    //   92: aload_1
    //   93: ifnull +9 -> 102
    //   96: aload_1
    //   97: invokeinterface 6 1 0
    //   102: aload 5
    //   104: areturn
    //   105: astore 6
    //   107: aload_1
    //   108: ifnull +9 -> 117
    //   111: aload_1
    //   112: invokeinterface 6 1 0
    //   117: aload 6
    //   119: athrow
    // Line number table:
    //   Java source line #190	-> byte code offset #0
    //   Java source line #192	-> byte code offset #2
    //   Java source line #193	-> byte code offset #7
    //   Java source line #194	-> byte code offset #18
    //   Java source line #195	-> byte code offset #31
    //   Java source line #196	-> byte code offset #38
    //   Java source line #197	-> byte code offset #42
    //   Java source line #204	-> byte code offset #46
    //   Java source line #205	-> byte code offset #50
    //   Java source line #199	-> byte code offset #59
    //   Java source line #204	-> byte code offset #73
    //   Java source line #205	-> byte code offset #77
    //   Java source line #200	-> byte code offset #86
    //   Java source line #201	-> byte code offset #88
    //   Java source line #204	-> byte code offset #92
    //   Java source line #205	-> byte code offset #96
    //   Java source line #204	-> byte code offset #105
    //   Java source line #205	-> byte code offset #111
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	120	0	this	PurchaseOrderDAO
    //   1	111	1	session	Session
    //   17	15	2	criteria	Criteria
    //   37	23	3	maxNumber	Object
    //   44	40	4	str1	String
    //   86	3	4	e	Exception
    //   90	13	5	str2	String
    //   105	13	6	localObject1	Object
    // Exception table:
    //   from	to	target	type
    //   59	73	86	java/lang/Exception
    //   2	46	105	finally
    //   59	73	105	finally
    //   86	92	105	finally
    //   105	107	105	finally
  }
  
  public List<PurchaseOrder> findBy(String poNumber, InventoryVendor vendor, Date from, Date to, Boolean showClosed)
  {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      if (StringUtils.isNotEmpty(poNumber)) {
        criteria.add(Restrictions.ilike(PurchaseOrder.PROP_ORDER_ID, poNumber.trim(), MatchMode.START));
      }
      if (vendor != null) {
        criteria.add(Restrictions.eq(PurchaseOrder.PROP_VENDOR, vendor));
      }
      if ((from != null) && (to != null)) {
        criteria.add(Restrictions.ge(PurchaseOrder.PROP_CREATED_DATE, from));
        criteria.add(Restrictions.le(PurchaseOrder.PROP_CREATED_DATE, to));
      }
      if (!showClosed.booleanValue()) {
        criteria.add(Restrictions.ne(PurchaseOrder.PROP_STATUS, Integer.valueOf(4)));
      }
      return criteria.list();
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
}
