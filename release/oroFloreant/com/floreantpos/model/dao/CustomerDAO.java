package com.floreantpos.model.dao;

import com.floreantpos.model.Customer;
import com.floreantpos.model.CustomerGroup;
import com.floreantpos.model.MenuModifier;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PaginatedListModel;
import com.floreantpos.swing.PaginatedTableModel;
import com.floreantpos.swing.PaginationSupport;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;






















public class CustomerDAO
  extends BaseCustomerDAO
{
  public CustomerDAO() {}
  
  public Order getDefaultOrder()
  {
    return Order.asc(Customer.PROP_ID);
  }
  
  public void findBy(String mobile, String loyalty, String name, PaginatedTableModel tableModel) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      Disjunction disjunction = Restrictions.disjunction();
      
      if (StringUtils.isNotEmpty(mobile)) {
        disjunction.add(Restrictions.like(Customer.PROP_MOBILE_NO, "%" + mobile + "%"));
      }
      if (StringUtils.isNotEmpty(loyalty)) {
        disjunction.add(Restrictions.like(Customer.PROP_LOYALTY_NO, "%" + loyalty + "%"));
      }
      if (StringUtils.isNotEmpty(name)) {
        disjunction.add(Restrictions.like(Customer.PROP_FIRST_NAME, "%" + name + "%"));
      }
      criteria.add(disjunction);
      
      criteria.setFirstResult(0);
      criteria.setMaxResults(tableModel.getPageSize());
      tableModel.setNumRows(tableModel.getPageSize());
      tableModel.setRows(criteria.list());
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public int getNumberOfCustomers(String searchString) {
    Session session = null;
    Criteria criteria = null;
    try {
      if (StringUtils.isEmpty(searchString)) {
        return 0;
      }
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      Disjunction disjunction = Restrictions.disjunction();
      disjunction.add(Restrictions.ilike(Customer.PROP_MOBILE_NO, "%" + searchString + "%"));
      disjunction.add(Restrictions.ilike(Customer.PROP_NAME, "%" + searchString + "%"));
      
      criteria.add(disjunction);
      
      List list = criteria.list();
      int j; if (list != null) {
        return list.size();
      }
      return 0;
    } finally {
      closeSession(session);
    }
  }
  
  public int getNumberOfCustomers(String mobile, String loyalty, String name)
  {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      Disjunction disjunction = Restrictions.disjunction();
      
      if (StringUtils.isNotEmpty(mobile)) {
        disjunction.add(Restrictions.ilike(Customer.PROP_MOBILE_NO, "%" + mobile + "%"));
      }
      if (StringUtils.isNotEmpty(loyalty)) {
        disjunction.add(Restrictions.ilike(Customer.PROP_LOYALTY_NO, "%" + loyalty + "%"));
      }
      if (StringUtils.isNotEmpty(name)) {
        disjunction.add(Restrictions.ilike(Customer.PROP_NAME, "%" + name + "%"));
      }
      criteria.add(disjunction);
      
      List list = criteria.list();
      int i; if (list != null) {
        return list.size();
      }
      return 0;
    } finally {
      closeSession(session);
    }
  }
  
  public List<Customer> findBy(String mobile, String loyalty, String name)
  {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      Disjunction disjunction = Restrictions.disjunction();
      
      if (StringUtils.isNotEmpty(mobile)) {
        disjunction.add(Restrictions.ilike(Customer.PROP_MOBILE_NO, "%" + mobile + "%"));
      }
      if (StringUtils.isNotEmpty(loyalty)) {
        disjunction.add(Restrictions.ilike(Customer.PROP_LOYALTY_NO, "%" + loyalty + "%"));
      }
      if (StringUtils.isNotEmpty(name)) {
        disjunction.add(Restrictions.ilike(Customer.PROP_NAME, "%" + name + "%"));
      }
      criteria.add(disjunction);
      
      return criteria.list();
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  

  /**
   * @deprecated
   */
  public void findBy(String searchString, PaginationSupport tableModel)
  {
    findByPhoneOrName(searchString, tableModel);
  }
  
  public void findByPhoneOrName(String searchString, PaginationSupport tableModel) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      Disjunction disjunction = Restrictions.disjunction();
      
      if (StringUtils.isNotEmpty(searchString))
      {


        disjunction.add(Restrictions.or(Restrictions.ilike(Customer.PROP_NAME, searchString, MatchMode.ANYWHERE), 
          Restrictions.ilike(Customer.PROP_MOBILE_NO, searchString, MatchMode.ANYWHERE)));
      }
      
      criteria.add(disjunction);
      
      criteria.setProjection(Projections.rowCount());
      Number uniqueResult = (Number)criteria.uniqueResult();
      tableModel.setNumRows(uniqueResult.intValue());
      criteria.setProjection(null);
      
      criteria.setFirstResult(tableModel.getCurrentRowIndex());
      criteria.setMaxResults(tableModel.getPageSize());
      tableModel.setRows(criteria.list());
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public List<Customer> findByMobileNumber(String mobileNo)
  {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      
      if (StringUtils.isNotEmpty(mobileNo))
        criteria.add(Restrictions.eq(Customer.PROP_MOBILE_NO, mobileNo));
      return criteria.list();
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public List<Customer> findByName(String name) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      
      if (StringUtils.isNotEmpty(name)) {
        criteria.add(Restrictions.ilike(Customer.PROP_FIRST_NAME, name + "%".trim(), MatchMode.ANYWHERE));
      }
      return criteria.list();
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public Customer findById(String customerId)
  {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      
      criteria.add(Restrictions.eq(Customer.PROP_ID, customerId));
      
      return (Customer)criteria.uniqueResult();
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public Customer findByPhoneOrEmail(String mobileNo, String email) {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      
      criteria.add(Restrictions.or(Restrictions.eq(Customer.PROP_MOBILE_NO, mobileNo), 
        Restrictions.eq(Customer.PROP_EMAIL, email)));
      List list = criteria.list();
      Customer localCustomer; if (list.size() > 0) {
        return (Customer)list.get(0);
      }
      return null;
    }
    catch (Exception localException) {}finally {
      if (session != null) {
        closeSession(session);
      }
    }
    return null;
  }
  
  public boolean deleteAll(List<Customer> customerList) {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      String queryDeliveryAddress = "update DELIVERY_ADDRESS set CUSTOMER_ID = null";
      String queryTableBookingInfo = "update TABLE_BOOKING_INFO set customer_id = null";
      String queryTicketItemSeat = "update TICKET_ITEM_SEAT set MEMBER_ID = null";
      
      Query deliveryAddressUpdateQuery = session.createSQLQuery(queryDeliveryAddress);
      Query tableBookingInfoUpdateQuery = session.createSQLQuery(queryTableBookingInfo);
      Query ticketItemSeatUpdateQuery = session.createSQLQuery(queryTicketItemSeat);
      
      deliveryAddressUpdateQuery.executeUpdate();
      tableBookingInfoUpdateQuery.executeUpdate();
      ticketItemSeatUpdateQuery.executeUpdate();
      
      for (Customer customer : customerList) {
        session.delete(customer);
      }
      
      tx.commit();
      return true;
    } catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(CustomerDAO.class).error(e);
      throw new RuntimeException(e);
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public int getNumberOfCustomers()
  {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.setProjection(Projections.rowCount());
      criteria.list();
      Number rowCount = (Number)criteria.uniqueResult();
      int i; if (rowCount != null) {
        return rowCount.intValue();
      }
      
      return 0;
    } finally {
      closeSession(session);
    }
  }
  
  public int getNumberOfCustomers(CustomerGroup customerGroup)
  {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass(), "customer");
      criteria.setProjection(Projections.rowCount());
      if (customerGroup != null) {
        criteria.createAlias("customer.customerGroups", "customerGroup");
        criteria.add(
          Restrictions.in("customerGroup.id", Arrays.asList(new String[] {customerGroup.getId() })));
      }
      Number rowCount = (Number)criteria.uniqueResult();
      int i; if (rowCount != null) {
        return rowCount.intValue();
      }
      return 0;
    } finally {
      closeSession(session);
    }
  }
  
  public void loadCustomers(PaginatedTableModel tableModel) {
    Session session = null;
    Criteria criteria = null;
    try
    {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.addOrder(getDefaultOrder());
      criteria.setFirstResult(tableModel.getCurrentRowIndex());
      criteria.setMaxResults(tableModel.getPageSize());
      tableModel.setRows(criteria.list());
      return;
    }
    finally {
      closeSession(session);
    }
  }
  
  public void loadCustomers(CustomerGroup customerGroup, PaginatedListModel tableModel) {
    Session session = null;
    Criteria criteria = null;
    try
    {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass(), "customer");
      criteria.addOrder(getDefaultOrder());
      if (customerGroup != null) {
        criteria.createAlias("customer.customerGroups", "customerGroup");
        criteria.add(
          Restrictions.in("customerGroup.id", Arrays.asList(new String[] {customerGroup.getId() })));
      }
      criteria.setFirstResult(tableModel.getCurrentRowIndex());
      criteria.setMaxResults(tableModel.getPageSize());
      tableModel.setData(criteria.list());
      
      return;
    }
    finally {
      closeSession(session);
    }
  }
  
  public int getRowCount(String searchString) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      
      criteria.setProjection(Projections.rowCount());
      
      if (StringUtils.isNotEmpty(searchString)) {
        criteria.add(Restrictions.ilike(MenuModifier.PROP_NAME, searchString, MatchMode.START));
      }
      
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        return rowCount.intValue();
      }
    }
    finally {
      closeSession(session);
    }
    return 0;
  }
  
  public void loadCustomers(String searchString, BeanTableModel<Customer> listModel) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.addOrder(Order.asc(Customer.PROP_NAME));
      
      if (StringUtils.isNotEmpty(searchString)) {
        criteria.add(Restrictions.ilike(Customer.PROP_NAME, searchString, MatchMode.START));
      }
      
      criteria.setFirstResult(listModel.getCurrentRowIndex());
      criteria.setMaxResults(listModel.getPageSize());
      listModel.setRows(criteria.list());
    } finally {
      closeSession(session);
    }
  }
  
  public Customer initialize(Customer customer) {
    if ((customer == null) || (customer.getId() == null)) {
      return customer;
    }
    Session session = null;
    try
    {
      session = createNewSession();
      session.refresh(customer);
      
      Hibernate.initialize(customer.getDeliveryAddresses());
      Hibernate.initialize(customer.getDeliveryInstructions());
      
      return customer;
    } finally {
      closeSession(session);
    }
  }
}
