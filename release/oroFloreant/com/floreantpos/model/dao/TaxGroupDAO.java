package com.floreantpos.model.dao;

import com.floreantpos.model.TaxGroup;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;



public class TaxGroupDAO
  extends BaseTaxGroupDAO
{
  public TaxGroupDAO() {}
  
  public boolean nameExists(TaxGroup taxGroup, String name)
  {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(TaxGroup.PROP_NAME, name).ignoreCase());
      if (taxGroup.getId() != null) {
        criteria.add(Restrictions.ne(TaxGroup.PROP_ID, taxGroup.getId()));
      }
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      boolean bool;
      if (rowCount == null) {
        return false;
      }
      return rowCount.intValue() > 0;
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
}
