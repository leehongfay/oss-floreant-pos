package com.floreantpos.model.dao;

import com.floreantpos.model.PrinterGroup;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BasePrinterGroupDAO
  extends _RootDAO
{
  public static PrinterGroupDAO instance;
  
  public BasePrinterGroupDAO() {}
  
  public static PrinterGroupDAO getInstance()
  {
    if (null == instance) instance = new PrinterGroupDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return PrinterGroup.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public PrinterGroup cast(Object object)
  {
    return (PrinterGroup)object;
  }
  
  public PrinterGroup get(String key) throws HibernateException
  {
    return (PrinterGroup)get(getReferenceClass(), key);
  }
  
  public PrinterGroup get(String key, Session s) throws HibernateException
  {
    return (PrinterGroup)get(getReferenceClass(), key, s);
  }
  
  public PrinterGroup load(String key) throws HibernateException
  {
    return (PrinterGroup)load(getReferenceClass(), key);
  }
  
  public PrinterGroup load(String key, Session s) throws HibernateException
  {
    return (PrinterGroup)load(getReferenceClass(), key, s);
  }
  
  public PrinterGroup loadInitialize(String key, Session s) throws HibernateException
  {
    PrinterGroup obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<PrinterGroup> findAll()
  {
    return super.findAll();
  }
  


  public List<PrinterGroup> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<PrinterGroup> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(PrinterGroup printerGroup)
    throws HibernateException
  {
    return (String)super.save(printerGroup);
  }
  







  public String save(PrinterGroup printerGroup, Session s)
    throws HibernateException
  {
    return (String)save(printerGroup, s);
  }
  





  public void saveOrUpdate(PrinterGroup printerGroup)
    throws HibernateException
  {
    saveOrUpdate(printerGroup);
  }
  







  public void saveOrUpdate(PrinterGroup printerGroup, Session s)
    throws HibernateException
  {
    saveOrUpdate(printerGroup, s);
  }
  




  public void update(PrinterGroup printerGroup)
    throws HibernateException
  {
    update(printerGroup);
  }
  






  public void update(PrinterGroup printerGroup, Session s)
    throws HibernateException
  {
    update(printerGroup, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(PrinterGroup printerGroup)
    throws HibernateException
  {
    delete(printerGroup);
  }
  






  public void delete(PrinterGroup printerGroup, Session s)
    throws HibernateException
  {
    delete(printerGroup, s);
  }
  









  public void refresh(PrinterGroup printerGroup, Session s)
    throws HibernateException
  {
    refresh(printerGroup, s);
  }
}
