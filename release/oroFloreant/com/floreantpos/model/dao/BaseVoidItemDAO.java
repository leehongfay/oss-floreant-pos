package com.floreantpos.model.dao;

import com.floreantpos.model.VoidItem;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseVoidItemDAO
  extends _RootDAO
{
  public static VoidItemDAO instance;
  
  public BaseVoidItemDAO() {}
  
  public static VoidItemDAO getInstance()
  {
    if (null == instance) instance = new VoidItemDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return VoidItem.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public VoidItem cast(Object object)
  {
    return (VoidItem)object;
  }
  
  public VoidItem get(String key) throws HibernateException
  {
    return (VoidItem)get(getReferenceClass(), key);
  }
  
  public VoidItem get(String key, Session s) throws HibernateException
  {
    return (VoidItem)get(getReferenceClass(), key, s);
  }
  
  public VoidItem load(String key) throws HibernateException
  {
    return (VoidItem)load(getReferenceClass(), key);
  }
  
  public VoidItem load(String key, Session s) throws HibernateException
  {
    return (VoidItem)load(getReferenceClass(), key, s);
  }
  
  public VoidItem loadInitialize(String key, Session s) throws HibernateException
  {
    VoidItem obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<VoidItem> findAll()
  {
    return super.findAll();
  }
  


  public List<VoidItem> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<VoidItem> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(VoidItem voidItem)
    throws HibernateException
  {
    return (String)super.save(voidItem);
  }
  







  public String save(VoidItem voidItem, Session s)
    throws HibernateException
  {
    return (String)save(voidItem, s);
  }
  





  public void saveOrUpdate(VoidItem voidItem)
    throws HibernateException
  {
    saveOrUpdate(voidItem);
  }
  







  public void saveOrUpdate(VoidItem voidItem, Session s)
    throws HibernateException
  {
    saveOrUpdate(voidItem, s);
  }
  




  public void update(VoidItem voidItem)
    throws HibernateException
  {
    update(voidItem);
  }
  






  public void update(VoidItem voidItem, Session s)
    throws HibernateException
  {
    update(voidItem, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(VoidItem voidItem)
    throws HibernateException
  {
    delete(voidItem);
  }
  






  public void delete(VoidItem voidItem, Session s)
    throws HibernateException
  {
    delete(voidItem, s);
  }
  









  public void refresh(VoidItem voidItem, Session s)
    throws HibernateException
  {
    refresh(voidItem, s);
  }
}
