package com.floreantpos.model.dao;

import com.floreantpos.model.DayPart;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseDayPartDAO
  extends _RootDAO
{
  public static DayPartDAO instance;
  
  public BaseDayPartDAO() {}
  
  public static DayPartDAO getInstance()
  {
    if (null == instance) instance = new DayPartDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return DayPart.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public DayPart cast(Object object)
  {
    return (DayPart)object;
  }
  
  public DayPart get(String key) throws HibernateException
  {
    return (DayPart)get(getReferenceClass(), key);
  }
  
  public DayPart get(String key, Session s) throws HibernateException
  {
    return (DayPart)get(getReferenceClass(), key, s);
  }
  
  public DayPart load(String key) throws HibernateException
  {
    return (DayPart)load(getReferenceClass(), key);
  }
  
  public DayPart load(String key, Session s) throws HibernateException
  {
    return (DayPart)load(getReferenceClass(), key, s);
  }
  
  public DayPart loadInitialize(String key, Session s) throws HibernateException
  {
    DayPart obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<DayPart> findAll()
  {
    return super.findAll();
  }
  


  public List<DayPart> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<DayPart> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(DayPart dayPart)
    throws HibernateException
  {
    return (String)super.save(dayPart);
  }
  







  public String save(DayPart dayPart, Session s)
    throws HibernateException
  {
    return (String)save(dayPart, s);
  }
  





  public void saveOrUpdate(DayPart dayPart)
    throws HibernateException
  {
    saveOrUpdate(dayPart);
  }
  







  public void saveOrUpdate(DayPart dayPart, Session s)
    throws HibernateException
  {
    saveOrUpdate(dayPart, s);
  }
  




  public void update(DayPart dayPart)
    throws HibernateException
  {
    update(dayPart);
  }
  






  public void update(DayPart dayPart, Session s)
    throws HibernateException
  {
    update(dayPart, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(DayPart dayPart)
    throws HibernateException
  {
    delete(dayPart);
  }
  






  public void delete(DayPart dayPart, Session s)
    throws HibernateException
  {
    delete(dayPart, s);
  }
  









  public void refresh(DayPart dayPart, Session s)
    throws HibernateException
  {
    refresh(dayPart, s);
  }
}
