package com.floreantpos.model.dao;

import com.floreantpos.PosException;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.PriceTable;
import com.floreantpos.model.PriceTableItem;
import com.floreantpos.swing.PaginatedListModel;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;




public class PriceTableItemDAO
  extends BasePriceTableItemDAO
{
  public PriceTableItemDAO() {}
  
  public PriceTableItem getItem(PriceTable priceTable, MenuItem menuItem)
    throws PosException
  {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(PriceTableItem.PROP_MENU_ITEM_ID, menuItem.getId()));
      criteria.add(Restrictions.eq(PriceTableItem.PROP_PRICE_TABLE_ID, priceTable.getId()));
      return (PriceTableItem)criteria.uniqueResult();
    } finally {
      if (session != null) {
        try {
          session.close();
        }
        catch (Exception localException1) {}
      }
    }
  }
  
  public PriceTableItem getItemById(String priceTableId, MenuItem menuItem) throws PosException {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(PriceTableItem.PROP_MENU_ITEM_ID, menuItem.getId()));
      criteria.add(Restrictions.eq(PriceTableItem.PROP_PRICE_TABLE_ID, priceTableId));
      return (PriceTableItem)criteria.uniqueResult();
    } finally {
      if (session != null) {
        try {
          session.close();
        }
        catch (Exception localException1) {}
      }
    }
  }
  
  public int rowCount(PriceTable priceTable) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.setProjection(Projections.rowCount());
      criteria.add(Restrictions.eq(PriceTableItem.PROP_PRICE_TABLE_ID, priceTable.getId()));
      Number rowCount = (Number)criteria.uniqueResult();
      int i; if (rowCount != null) {
        return rowCount.intValue();
      }
      return 0;
    } finally {
      closeSession(session);
    }
  }
  
  public void loadItems(PriceTable priceTable, PaginatedListModel listModel) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(PriceTableItem.PROP_PRICE_TABLE_ID, priceTable.getId()));
      criteria.setFirstResult(listModel.getCurrentRowIndex());
      criteria.setMaxResults(listModel.getPageSize());
      listModel.setData(criteria.list());
    } finally {
      closeSession(session);
    }
  }
  
  public List<PriceTableItem> getItemsByPriceTable(PriceTable priceTable) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(PriceTableItem.PROP_PRICE_TABLE_ID, priceTable.getId()));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public void saveOrUpdateItems(PriceTable priceTable, List<PriceTableItem> priceTableItems) {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      session.saveOrUpdate(priceTable);
      for (PriceTableItem item : priceTableItems) {
        item.setPriceTableId(priceTable.getId());
        session.saveOrUpdate(item);
      }
      tx.commit();
    } catch (Exception e) {
      try {
        tx.rollback();
      }
      catch (Exception localException1) {}
      throw e;
    } finally {
      closeSession(session);
    }
  }
}
