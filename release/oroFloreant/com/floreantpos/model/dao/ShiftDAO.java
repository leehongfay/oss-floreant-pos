package com.floreantpos.model.dao;

import com.floreantpos.PosException;
import com.floreantpos.model.Shift;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;





















public class ShiftDAO
  extends BaseShiftDAO
{
  public ShiftDAO() {}
  
  public boolean exists(String shiftName)
    throws PosException
  {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Shift.PROP_NAME, shiftName));
      List list = criteria.list();
      boolean bool; if ((list != null) && (list.size() > 0)) {
        return true;
      }
      return false;
    } catch (Exception e) {
      throw new PosException("An error occured while trying to check Shift duplicacy", e);
    } finally {
      if (session != null) {
        try {
          session.close();
        }
        catch (Exception localException3) {}
      }
    }
  }
  
  public Shift getByName(String shiftName) throws PosException {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Shift.PROP_NAME, shiftName));
      List<Shift> list = criteria.list();
      Shift localShift; if ((list != null) && (list.size() > 0)) {
        return (Shift)list.get(0);
      }
      return null;
    } catch (Exception e) {
      throw new PosException("An error occured while trying to check Shift duplicacy", e);
    } finally {
      if (session != null) {
        try {
          session.close();
        }
        catch (Exception localException3) {}
      }
    }
  }
  
  public void refresh(Shift shift) throws PosException {
    Session session = null;
    try
    {
      session = createNewSession();
      session.refresh(shift); return;
    } catch (Exception e) {
      throw new PosException("An error occured while refreshing Shift state.", e);
    } finally {
      if (session != null) {
        try {
          session.close();
        }
        catch (Exception localException2) {}
      }
    }
  }
  
  public Shift findByName(String name) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Shift.PROP_NAME, name));
      return (Shift)criteria.uniqueResult();
    } catch (Exception e) {
      throw e;
    } finally {
      if (session != null) {
        try {
          session.close();
        }
        catch (Exception localException2) {}
      }
    }
  }
}
