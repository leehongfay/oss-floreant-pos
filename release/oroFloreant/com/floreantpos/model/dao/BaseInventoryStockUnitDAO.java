package com.floreantpos.model.dao;

import com.floreantpos.model.InventoryStockUnit;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseInventoryStockUnitDAO
  extends _RootDAO
{
  public static InventoryStockUnitDAO instance;
  
  public BaseInventoryStockUnitDAO() {}
  
  public static InventoryStockUnitDAO getInstance()
  {
    if (null == instance) instance = new InventoryStockUnitDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return InventoryStockUnit.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public InventoryStockUnit cast(Object object)
  {
    return (InventoryStockUnit)object;
  }
  
  public InventoryStockUnit get(String key) throws HibernateException
  {
    return (InventoryStockUnit)get(getReferenceClass(), key);
  }
  
  public InventoryStockUnit get(String key, Session s) throws HibernateException
  {
    return (InventoryStockUnit)get(getReferenceClass(), key, s);
  }
  
  public InventoryStockUnit load(String key) throws HibernateException
  {
    return (InventoryStockUnit)load(getReferenceClass(), key);
  }
  
  public InventoryStockUnit load(String key, Session s) throws HibernateException
  {
    return (InventoryStockUnit)load(getReferenceClass(), key, s);
  }
  
  public InventoryStockUnit loadInitialize(String key, Session s) throws HibernateException
  {
    InventoryStockUnit obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<InventoryStockUnit> findAll()
  {
    return super.findAll();
  }
  


  public List<InventoryStockUnit> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<InventoryStockUnit> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(InventoryStockUnit inventoryStockUnit)
    throws HibernateException
  {
    return (String)super.save(inventoryStockUnit);
  }
  







  public String save(InventoryStockUnit inventoryStockUnit, Session s)
    throws HibernateException
  {
    return (String)save(inventoryStockUnit, s);
  }
  





  public void saveOrUpdate(InventoryStockUnit inventoryStockUnit)
    throws HibernateException
  {
    saveOrUpdate(inventoryStockUnit);
  }
  







  public void saveOrUpdate(InventoryStockUnit inventoryStockUnit, Session s)
    throws HibernateException
  {
    saveOrUpdate(inventoryStockUnit, s);
  }
  




  public void update(InventoryStockUnit inventoryStockUnit)
    throws HibernateException
  {
    update(inventoryStockUnit);
  }
  






  public void update(InventoryStockUnit inventoryStockUnit, Session s)
    throws HibernateException
  {
    update(inventoryStockUnit, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(InventoryStockUnit inventoryStockUnit)
    throws HibernateException
  {
    delete(inventoryStockUnit);
  }
  






  public void delete(InventoryStockUnit inventoryStockUnit, Session s)
    throws HibernateException
  {
    delete(inventoryStockUnit, s);
  }
  









  public void refresh(InventoryStockUnit inventoryStockUnit, Session s)
    throws HibernateException
  {
    refresh(inventoryStockUnit, s);
  }
}
