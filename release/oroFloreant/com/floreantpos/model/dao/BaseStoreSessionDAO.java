package com.floreantpos.model.dao;

import com.floreantpos.model.StoreSession;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseStoreSessionDAO
  extends _RootDAO
{
  public static StoreSessionDAO instance;
  
  public BaseStoreSessionDAO() {}
  
  public static StoreSessionDAO getInstance()
  {
    if (null == instance) instance = new StoreSessionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return StoreSession.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public StoreSession cast(Object object)
  {
    return (StoreSession)object;
  }
  
  public StoreSession get(String key) throws HibernateException
  {
    return (StoreSession)get(getReferenceClass(), key);
  }
  
  public StoreSession get(String key, Session s) throws HibernateException
  {
    return (StoreSession)get(getReferenceClass(), key, s);
  }
  
  public StoreSession load(String key) throws HibernateException
  {
    return (StoreSession)load(getReferenceClass(), key);
  }
  
  public StoreSession load(String key, Session s) throws HibernateException
  {
    return (StoreSession)load(getReferenceClass(), key, s);
  }
  
  public StoreSession loadInitialize(String key, Session s) throws HibernateException
  {
    StoreSession obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<StoreSession> findAll()
  {
    return super.findAll();
  }
  


  public List<StoreSession> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<StoreSession> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(StoreSession storeSession)
    throws HibernateException
  {
    return (String)super.save(storeSession);
  }
  







  public String save(StoreSession storeSession, Session s)
    throws HibernateException
  {
    return (String)save(storeSession, s);
  }
  





  public void saveOrUpdate(StoreSession storeSession)
    throws HibernateException
  {
    saveOrUpdate(storeSession);
  }
  







  public void saveOrUpdate(StoreSession storeSession, Session s)
    throws HibernateException
  {
    saveOrUpdate(storeSession, s);
  }
  




  public void update(StoreSession storeSession)
    throws HibernateException
  {
    update(storeSession);
  }
  






  public void update(StoreSession storeSession, Session s)
    throws HibernateException
  {
    update(storeSession, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(StoreSession storeSession)
    throws HibernateException
  {
    delete(storeSession);
  }
  






  public void delete(StoreSession storeSession, Session s)
    throws HibernateException
  {
    delete(storeSession, s);
  }
  









  public void refresh(StoreSession storeSession, Session s)
    throws HibernateException
  {
    refresh(storeSession, s);
  }
}
