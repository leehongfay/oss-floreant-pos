package com.floreantpos.model.dao;

import com.floreantpos.model.TerminalType;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseTerminalTypeDAO
  extends _RootDAO
{
  public static TerminalTypeDAO instance;
  
  public BaseTerminalTypeDAO() {}
  
  public static TerminalTypeDAO getInstance()
  {
    if (null == instance) instance = new TerminalTypeDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return TerminalType.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public TerminalType cast(Object object)
  {
    return (TerminalType)object;
  }
  
  public TerminalType get(String key) throws HibernateException
  {
    return (TerminalType)get(getReferenceClass(), key);
  }
  
  public TerminalType get(String key, Session s) throws HibernateException
  {
    return (TerminalType)get(getReferenceClass(), key, s);
  }
  
  public TerminalType load(String key) throws HibernateException
  {
    return (TerminalType)load(getReferenceClass(), key);
  }
  
  public TerminalType load(String key, Session s) throws HibernateException
  {
    return (TerminalType)load(getReferenceClass(), key, s);
  }
  
  public TerminalType loadInitialize(String key, Session s) throws HibernateException
  {
    TerminalType obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<TerminalType> findAll()
  {
    return super.findAll();
  }
  


  public List<TerminalType> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<TerminalType> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(TerminalType terminalType)
    throws HibernateException
  {
    return (String)super.save(terminalType);
  }
  







  public String save(TerminalType terminalType, Session s)
    throws HibernateException
  {
    return (String)save(terminalType, s);
  }
  





  public void saveOrUpdate(TerminalType terminalType)
    throws HibernateException
  {
    saveOrUpdate(terminalType);
  }
  







  public void saveOrUpdate(TerminalType terminalType, Session s)
    throws HibernateException
  {
    saveOrUpdate(terminalType, s);
  }
  




  public void update(TerminalType terminalType)
    throws HibernateException
  {
    update(terminalType);
  }
  






  public void update(TerminalType terminalType, Session s)
    throws HibernateException
  {
    update(terminalType, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(TerminalType terminalType)
    throws HibernateException
  {
    delete(terminalType);
  }
  






  public void delete(TerminalType terminalType, Session s)
    throws HibernateException
  {
    delete(terminalType, s);
  }
  









  public void refresh(TerminalType terminalType, Session s)
    throws HibernateException
  {
    refresh(terminalType, s);
  }
}
