package com.floreantpos.model.dao;

import com.floreantpos.model.ShopTable;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseShopTableDAO
  extends _RootDAO
{
  public static ShopTableDAO instance;
  
  public BaseShopTableDAO() {}
  
  public static ShopTableDAO getInstance()
  {
    if (null == instance) instance = new ShopTableDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return ShopTable.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public ShopTable cast(Object object)
  {
    return (ShopTable)object;
  }
  
  public ShopTable get(Integer key) throws HibernateException
  {
    return (ShopTable)get(getReferenceClass(), key);
  }
  
  public ShopTable get(Integer key, Session s) throws HibernateException
  {
    return (ShopTable)get(getReferenceClass(), key, s);
  }
  
  public ShopTable load(Integer key) throws HibernateException
  {
    return (ShopTable)load(getReferenceClass(), key);
  }
  
  public ShopTable load(Integer key, Session s) throws HibernateException
  {
    return (ShopTable)load(getReferenceClass(), key, s);
  }
  
  public ShopTable loadInitialize(Integer key, Session s) throws HibernateException
  {
    ShopTable obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<ShopTable> findAll()
  {
    return super.findAll();
  }
  


  public List<ShopTable> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<ShopTable> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public Integer save(ShopTable shopTable)
    throws HibernateException
  {
    return (Integer)super.save(shopTable);
  }
  







  public Integer save(ShopTable shopTable, Session s)
    throws HibernateException
  {
    return (Integer)save(shopTable, s);
  }
  





  public void saveOrUpdate(ShopTable shopTable)
    throws HibernateException
  {
    saveOrUpdate(shopTable);
  }
  







  public void saveOrUpdate(ShopTable shopTable, Session s)
    throws HibernateException
  {
    saveOrUpdate(shopTable, s);
  }
  




  public void update(ShopTable shopTable)
    throws HibernateException
  {
    update(shopTable);
  }
  






  public void update(ShopTable shopTable, Session s)
    throws HibernateException
  {
    update(shopTable, s);
  }
  




  public void delete(Integer id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(Integer id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(ShopTable shopTable)
    throws HibernateException
  {
    delete(shopTable);
  }
  






  public void delete(ShopTable shopTable, Session s)
    throws HibernateException
  {
    delete(shopTable, s);
  }
  









  public void refresh(ShopTable shopTable, Session s)
    throws HibernateException
  {
    refresh(shopTable, s);
  }
}
