package com.floreantpos.model.dao;

import com.floreantpos.PosException;
import com.floreantpos.model.MenuShift;
import com.floreantpos.model.Shift;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;




public class MenuShiftDAO
  extends BaseMenuShiftDAO
{
  public MenuShiftDAO() {}
  
  public boolean exists(String shiftName)
    throws PosException
  {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(Shift.class);
      criteria.add(Restrictions.eq(Shift.PROP_NAME, shiftName));
      List list = criteria.list();
      boolean bool; if ((list != null) && (list.size() > 0)) {
        return true;
      }
      return false;
    } catch (Exception e) {
      throw new PosException("An error occured while trying to check price shift duplicacy", e);
    } finally {
      if (session != null) {
        try {
          session.close();
        }
        catch (Exception localException3) {}
      }
    }
  }
  
  public List<MenuShift> findAllActive() {
    Session session = null;
    try {
      session = getSession();
      return findAllActive(session);
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public List<MenuShift> findAllActive(Session session) {
    Criteria criteria = session.createCriteria(getReferenceClass());
    criteria.add(Restrictions.eq(MenuShift.PROP_ENABLE, Boolean.TRUE));
    criteria.addOrder(Order.asc(MenuShift.PROP_PRIORITY));
    return criteria.list();
  }
}
