package com.floreantpos.model.dao;

import com.floreantpos.model.PayoutReason;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BasePayoutReasonDAO
  extends _RootDAO
{
  public static PayoutReasonDAO instance;
  
  public BasePayoutReasonDAO() {}
  
  public static PayoutReasonDAO getInstance()
  {
    if (null == instance) instance = new PayoutReasonDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return PayoutReason.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public PayoutReason cast(Object object)
  {
    return (PayoutReason)object;
  }
  
  public PayoutReason get(String key) throws HibernateException
  {
    return (PayoutReason)get(getReferenceClass(), key);
  }
  
  public PayoutReason get(String key, Session s) throws HibernateException
  {
    return (PayoutReason)get(getReferenceClass(), key, s);
  }
  
  public PayoutReason load(String key) throws HibernateException
  {
    return (PayoutReason)load(getReferenceClass(), key);
  }
  
  public PayoutReason load(String key, Session s) throws HibernateException
  {
    return (PayoutReason)load(getReferenceClass(), key, s);
  }
  
  public PayoutReason loadInitialize(String key, Session s) throws HibernateException
  {
    PayoutReason obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<PayoutReason> findAll()
  {
    return super.findAll();
  }
  


  public List<PayoutReason> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<PayoutReason> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(PayoutReason payoutReason)
    throws HibernateException
  {
    return (String)super.save(payoutReason);
  }
  







  public String save(PayoutReason payoutReason, Session s)
    throws HibernateException
  {
    return (String)save(payoutReason, s);
  }
  





  public void saveOrUpdate(PayoutReason payoutReason)
    throws HibernateException
  {
    saveOrUpdate(payoutReason);
  }
  







  public void saveOrUpdate(PayoutReason payoutReason, Session s)
    throws HibernateException
  {
    saveOrUpdate(payoutReason, s);
  }
  




  public void update(PayoutReason payoutReason)
    throws HibernateException
  {
    update(payoutReason);
  }
  






  public void update(PayoutReason payoutReason, Session s)
    throws HibernateException
  {
    update(payoutReason, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(PayoutReason payoutReason)
    throws HibernateException
  {
    delete(payoutReason);
  }
  






  public void delete(PayoutReason payoutReason, Session s)
    throws HibernateException
  {
    delete(payoutReason, s);
  }
  









  public void refresh(PayoutReason payoutReason, Session s)
    throws HibernateException
  {
    refresh(payoutReason, s);
  }
}
