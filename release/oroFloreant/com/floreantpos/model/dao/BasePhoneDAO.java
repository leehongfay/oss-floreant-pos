package com.floreantpos.model.dao;

import com.floreantpos.model.Phone;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BasePhoneDAO
  extends _RootDAO
{
  public static PhoneDAO instance;
  
  public BasePhoneDAO() {}
  
  public static PhoneDAO getInstance()
  {
    if (null == instance) instance = new PhoneDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return Phone.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public Phone cast(Object object)
  {
    return (Phone)object;
  }
  
  public Phone get(String key) throws HibernateException
  {
    return (Phone)get(getReferenceClass(), key);
  }
  
  public Phone get(String key, Session s) throws HibernateException
  {
    return (Phone)get(getReferenceClass(), key, s);
  }
  
  public Phone load(String key) throws HibernateException
  {
    return (Phone)load(getReferenceClass(), key);
  }
  
  public Phone load(String key, Session s) throws HibernateException
  {
    return (Phone)load(getReferenceClass(), key, s);
  }
  
  public Phone loadInitialize(String key, Session s) throws HibernateException
  {
    Phone obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<Phone> findAll()
  {
    return super.findAll();
  }
  


  public List<Phone> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<Phone> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(Phone phone)
    throws HibernateException
  {
    return (String)super.save(phone);
  }
  







  public String save(Phone phone, Session s)
    throws HibernateException
  {
    return (String)save(phone, s);
  }
  





  public void saveOrUpdate(Phone phone)
    throws HibernateException
  {
    saveOrUpdate(phone);
  }
  







  public void saveOrUpdate(Phone phone, Session s)
    throws HibernateException
  {
    saveOrUpdate(phone, s);
  }
  




  public void update(Phone phone)
    throws HibernateException
  {
    update(phone);
  }
  






  public void update(Phone phone, Session s)
    throws HibernateException
  {
    update(phone, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(Phone phone)
    throws HibernateException
  {
    delete(phone);
  }
  






  public void delete(Phone phone, Session s)
    throws HibernateException
  {
    delete(phone, s);
  }
  









  public void refresh(Phone phone, Session s)
    throws HibernateException
  {
    refresh(phone, s);
  }
}
