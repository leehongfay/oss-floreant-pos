package com.floreantpos.model.dao;

import com.floreantpos.model.DeliveryInstruction;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseDeliveryInstructionDAO
  extends _RootDAO
{
  public static DeliveryInstructionDAO instance;
  
  public BaseDeliveryInstructionDAO() {}
  
  public static DeliveryInstructionDAO getInstance()
  {
    if (null == instance) instance = new DeliveryInstructionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return DeliveryInstruction.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public DeliveryInstruction cast(Object object)
  {
    return (DeliveryInstruction)object;
  }
  
  public DeliveryInstruction get(String key) throws HibernateException
  {
    return (DeliveryInstruction)get(getReferenceClass(), key);
  }
  
  public DeliveryInstruction get(String key, Session s) throws HibernateException
  {
    return (DeliveryInstruction)get(getReferenceClass(), key, s);
  }
  
  public DeliveryInstruction load(String key) throws HibernateException
  {
    return (DeliveryInstruction)load(getReferenceClass(), key);
  }
  
  public DeliveryInstruction load(String key, Session s) throws HibernateException
  {
    return (DeliveryInstruction)load(getReferenceClass(), key, s);
  }
  
  public DeliveryInstruction loadInitialize(String key, Session s) throws HibernateException
  {
    DeliveryInstruction obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<DeliveryInstruction> findAll()
  {
    return super.findAll();
  }
  


  public List<DeliveryInstruction> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<DeliveryInstruction> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(DeliveryInstruction deliveryInstruction)
    throws HibernateException
  {
    return (String)super.save(deliveryInstruction);
  }
  







  public String save(DeliveryInstruction deliveryInstruction, Session s)
    throws HibernateException
  {
    return (String)save(deliveryInstruction, s);
  }
  





  public void saveOrUpdate(DeliveryInstruction deliveryInstruction)
    throws HibernateException
  {
    saveOrUpdate(deliveryInstruction);
  }
  







  public void saveOrUpdate(DeliveryInstruction deliveryInstruction, Session s)
    throws HibernateException
  {
    saveOrUpdate(deliveryInstruction, s);
  }
  




  public void update(DeliveryInstruction deliveryInstruction)
    throws HibernateException
  {
    update(deliveryInstruction);
  }
  






  public void update(DeliveryInstruction deliveryInstruction, Session s)
    throws HibernateException
  {
    update(deliveryInstruction, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(DeliveryInstruction deliveryInstruction)
    throws HibernateException
  {
    delete(deliveryInstruction);
  }
  






  public void delete(DeliveryInstruction deliveryInstruction, Session s)
    throws HibernateException
  {
    delete(deliveryInstruction, s);
  }
  









  public void refresh(DeliveryInstruction deliveryInstruction, Session s)
    throws HibernateException
  {
    refresh(deliveryInstruction, s);
  }
}
