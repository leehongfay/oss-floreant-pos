package com.floreantpos.model.dao;

import com.floreantpos.model.ComboTicketItem;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseComboTicketItemDAO
  extends _RootDAO
{
  public static ComboTicketItemDAO instance;
  
  public BaseComboTicketItemDAO() {}
  
  public static ComboTicketItemDAO getInstance()
  {
    if (null == instance) instance = new ComboTicketItemDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return ComboTicketItem.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public ComboTicketItem cast(Object object)
  {
    return (ComboTicketItem)object;
  }
  
  public ComboTicketItem get(String key) throws HibernateException
  {
    return (ComboTicketItem)get(getReferenceClass(), key);
  }
  
  public ComboTicketItem get(String key, Session s) throws HibernateException
  {
    return (ComboTicketItem)get(getReferenceClass(), key, s);
  }
  
  public ComboTicketItem load(String key) throws HibernateException
  {
    return (ComboTicketItem)load(getReferenceClass(), key);
  }
  
  public ComboTicketItem load(String key, Session s) throws HibernateException
  {
    return (ComboTicketItem)load(getReferenceClass(), key, s);
  }
  
  public ComboTicketItem loadInitialize(String key, Session s) throws HibernateException
  {
    ComboTicketItem obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<ComboTicketItem> findAll()
  {
    return super.findAll();
  }
  


  public List<ComboTicketItem> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<ComboTicketItem> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(ComboTicketItem comboTicketItem)
    throws HibernateException
  {
    return (String)super.save(comboTicketItem);
  }
  







  public String save(ComboTicketItem comboTicketItem, Session s)
    throws HibernateException
  {
    return (String)save(comboTicketItem, s);
  }
  





  public void saveOrUpdate(ComboTicketItem comboTicketItem)
    throws HibernateException
  {
    saveOrUpdate(comboTicketItem);
  }
  







  public void saveOrUpdate(ComboTicketItem comboTicketItem, Session s)
    throws HibernateException
  {
    saveOrUpdate(comboTicketItem, s);
  }
  




  public void update(ComboTicketItem comboTicketItem)
    throws HibernateException
  {
    update(comboTicketItem);
  }
  






  public void update(ComboTicketItem comboTicketItem, Session s)
    throws HibernateException
  {
    update(comboTicketItem, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(ComboTicketItem comboTicketItem)
    throws HibernateException
  {
    delete(comboTicketItem);
  }
  






  public void delete(ComboTicketItem comboTicketItem, Session s)
    throws HibernateException
  {
    delete(comboTicketItem, s);
  }
  









  public void refresh(ComboTicketItem comboTicketItem, Session s)
    throws HibernateException
  {
    refresh(comboTicketItem, s);
  }
}
