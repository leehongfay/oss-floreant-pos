package com.floreantpos.model.dao;

import com.floreantpos.model.ReservationShift;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseReservationShiftDAO
  extends _RootDAO
{
  public static ReservationShiftDAO instance;
  
  public BaseReservationShiftDAO() {}
  
  public static ReservationShiftDAO getInstance()
  {
    if (null == instance) instance = new ReservationShiftDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return ReservationShift.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public ReservationShift cast(Object object)
  {
    return (ReservationShift)object;
  }
  
  public ReservationShift get(String key) throws HibernateException
  {
    return (ReservationShift)get(getReferenceClass(), key);
  }
  
  public ReservationShift get(String key, Session s) throws HibernateException
  {
    return (ReservationShift)get(getReferenceClass(), key, s);
  }
  
  public ReservationShift load(String key) throws HibernateException
  {
    return (ReservationShift)load(getReferenceClass(), key);
  }
  
  public ReservationShift load(String key, Session s) throws HibernateException
  {
    return (ReservationShift)load(getReferenceClass(), key, s);
  }
  
  public ReservationShift loadInitialize(String key, Session s) throws HibernateException
  {
    ReservationShift obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<ReservationShift> findAll()
  {
    return super.findAll();
  }
  


  public List<ReservationShift> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<ReservationShift> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(ReservationShift reservationShift)
    throws HibernateException
  {
    return (String)super.save(reservationShift);
  }
  







  public String save(ReservationShift reservationShift, Session s)
    throws HibernateException
  {
    return (String)save(reservationShift, s);
  }
  





  public void saveOrUpdate(ReservationShift reservationShift)
    throws HibernateException
  {
    saveOrUpdate(reservationShift);
  }
  







  public void saveOrUpdate(ReservationShift reservationShift, Session s)
    throws HibernateException
  {
    saveOrUpdate(reservationShift, s);
  }
  




  public void update(ReservationShift reservationShift)
    throws HibernateException
  {
    update(reservationShift);
  }
  






  public void update(ReservationShift reservationShift, Session s)
    throws HibernateException
  {
    update(reservationShift, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(ReservationShift reservationShift)
    throws HibernateException
  {
    delete(reservationShift);
  }
  






  public void delete(ReservationShift reservationShift, Session s)
    throws HibernateException
  {
    delete(reservationShift, s);
  }
  









  public void refresh(ReservationShift reservationShift, Session s)
    throws HibernateException
  {
    refresh(reservationShift, s);
  }
}
