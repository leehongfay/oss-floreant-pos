package com.floreantpos.model.dao;

import com.floreantpos.model.MenuItemModifierPage;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseMenuItemModifierPageDAO
  extends _RootDAO
{
  public static MenuItemModifierPageDAO instance;
  
  public BaseMenuItemModifierPageDAO() {}
  
  public static MenuItemModifierPageDAO getInstance()
  {
    if (null == instance) instance = new MenuItemModifierPageDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return MenuItemModifierPage.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public MenuItemModifierPage cast(Object object)
  {
    return (MenuItemModifierPage)object;
  }
  
  public MenuItemModifierPage get(String key) throws HibernateException
  {
    return (MenuItemModifierPage)get(getReferenceClass(), key);
  }
  
  public MenuItemModifierPage get(String key, Session s) throws HibernateException
  {
    return (MenuItemModifierPage)get(getReferenceClass(), key, s);
  }
  
  public MenuItemModifierPage load(String key) throws HibernateException
  {
    return (MenuItemModifierPage)load(getReferenceClass(), key);
  }
  
  public MenuItemModifierPage load(String key, Session s) throws HibernateException
  {
    return (MenuItemModifierPage)load(getReferenceClass(), key, s);
  }
  
  public MenuItemModifierPage loadInitialize(String key, Session s) throws HibernateException
  {
    MenuItemModifierPage obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<MenuItemModifierPage> findAll()
  {
    return super.findAll();
  }
  


  public List<MenuItemModifierPage> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<MenuItemModifierPage> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(MenuItemModifierPage menuItemModifierPage)
    throws HibernateException
  {
    return (String)super.save(menuItemModifierPage);
  }
  







  public String save(MenuItemModifierPage menuItemModifierPage, Session s)
    throws HibernateException
  {
    return (String)save(menuItemModifierPage, s);
  }
  





  public void saveOrUpdate(MenuItemModifierPage menuItemModifierPage)
    throws HibernateException
  {
    saveOrUpdate(menuItemModifierPage);
  }
  







  public void saveOrUpdate(MenuItemModifierPage menuItemModifierPage, Session s)
    throws HibernateException
  {
    saveOrUpdate(menuItemModifierPage, s);
  }
  




  public void update(MenuItemModifierPage menuItemModifierPage)
    throws HibernateException
  {
    update(menuItemModifierPage);
  }
  






  public void update(MenuItemModifierPage menuItemModifierPage, Session s)
    throws HibernateException
  {
    update(menuItemModifierPage, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(MenuItemModifierPage menuItemModifierPage)
    throws HibernateException
  {
    delete(menuItemModifierPage);
  }
  






  public void delete(MenuItemModifierPage menuItemModifierPage, Session s)
    throws HibernateException
  {
    delete(menuItemModifierPage, s);
  }
  









  public void refresh(MenuItemModifierPage menuItemModifierPage, Session s)
    throws HibernateException
  {
    refresh(menuItemModifierPage, s);
  }
}
