package com.floreantpos.model.dao;

import com.floreantpos.model.CustomerGroup;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseCustomerGroupDAO
  extends _RootDAO
{
  public static CustomerGroupDAO instance;
  
  public BaseCustomerGroupDAO() {}
  
  public static CustomerGroupDAO getInstance()
  {
    if (null == instance) instance = new CustomerGroupDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return CustomerGroup.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public CustomerGroup cast(Object object)
  {
    return (CustomerGroup)object;
  }
  
  public CustomerGroup get(String key) throws HibernateException
  {
    return (CustomerGroup)get(getReferenceClass(), key);
  }
  
  public CustomerGroup get(String key, Session s) throws HibernateException
  {
    return (CustomerGroup)get(getReferenceClass(), key, s);
  }
  
  public CustomerGroup load(String key) throws HibernateException
  {
    return (CustomerGroup)load(getReferenceClass(), key);
  }
  
  public CustomerGroup load(String key, Session s) throws HibernateException
  {
    return (CustomerGroup)load(getReferenceClass(), key, s);
  }
  
  public CustomerGroup loadInitialize(String key, Session s) throws HibernateException
  {
    CustomerGroup obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<CustomerGroup> findAll()
  {
    return super.findAll();
  }
  


  public List<CustomerGroup> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<CustomerGroup> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(CustomerGroup customerGroup)
    throws HibernateException
  {
    return (String)super.save(customerGroup);
  }
  







  public String save(CustomerGroup customerGroup, Session s)
    throws HibernateException
  {
    return (String)save(customerGroup, s);
  }
  





  public void saveOrUpdate(CustomerGroup customerGroup)
    throws HibernateException
  {
    saveOrUpdate(customerGroup);
  }
  







  public void saveOrUpdate(CustomerGroup customerGroup, Session s)
    throws HibernateException
  {
    saveOrUpdate(customerGroup, s);
  }
  




  public void update(CustomerGroup customerGroup)
    throws HibernateException
  {
    update(customerGroup);
  }
  






  public void update(CustomerGroup customerGroup, Session s)
    throws HibernateException
  {
    update(customerGroup, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(CustomerGroup customerGroup)
    throws HibernateException
  {
    delete(customerGroup);
  }
  






  public void delete(CustomerGroup customerGroup, Session s)
    throws HibernateException
  {
    delete(customerGroup, s);
  }
  









  public void refresh(CustomerGroup customerGroup, Session s)
    throws HibernateException
  {
    refresh(customerGroup, s);
  }
}
