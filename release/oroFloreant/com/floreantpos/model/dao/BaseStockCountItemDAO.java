package com.floreantpos.model.dao;

import com.floreantpos.model.StockCountItem;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseStockCountItemDAO
  extends _RootDAO
{
  public static StockCountItemDAO instance;
  
  public BaseStockCountItemDAO() {}
  
  public static StockCountItemDAO getInstance()
  {
    if (null == instance) instance = new StockCountItemDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return StockCountItem.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public StockCountItem cast(Object object)
  {
    return (StockCountItem)object;
  }
  
  public StockCountItem get(String key) throws HibernateException
  {
    return (StockCountItem)get(getReferenceClass(), key);
  }
  
  public StockCountItem get(String key, Session s) throws HibernateException
  {
    return (StockCountItem)get(getReferenceClass(), key, s);
  }
  
  public StockCountItem load(String key) throws HibernateException
  {
    return (StockCountItem)load(getReferenceClass(), key);
  }
  
  public StockCountItem load(String key, Session s) throws HibernateException
  {
    return (StockCountItem)load(getReferenceClass(), key, s);
  }
  
  public StockCountItem loadInitialize(String key, Session s) throws HibernateException
  {
    StockCountItem obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<StockCountItem> findAll()
  {
    return super.findAll();
  }
  


  public List<StockCountItem> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<StockCountItem> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(StockCountItem stockCountItem)
    throws HibernateException
  {
    return (String)super.save(stockCountItem);
  }
  







  public String save(StockCountItem stockCountItem, Session s)
    throws HibernateException
  {
    return (String)save(stockCountItem, s);
  }
  





  public void saveOrUpdate(StockCountItem stockCountItem)
    throws HibernateException
  {
    saveOrUpdate(stockCountItem);
  }
  







  public void saveOrUpdate(StockCountItem stockCountItem, Session s)
    throws HibernateException
  {
    saveOrUpdate(stockCountItem, s);
  }
  




  public void update(StockCountItem stockCountItem)
    throws HibernateException
  {
    update(stockCountItem);
  }
  






  public void update(StockCountItem stockCountItem, Session s)
    throws HibernateException
  {
    update(stockCountItem, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(StockCountItem stockCountItem)
    throws HibernateException
  {
    delete(stockCountItem);
  }
  






  public void delete(StockCountItem stockCountItem, Session s)
    throws HibernateException
  {
    delete(stockCountItem, s);
  }
  









  public void refresh(StockCountItem stockCountItem, Session s)
    throws HibernateException
  {
    refresh(stockCountItem, s);
  }
}
