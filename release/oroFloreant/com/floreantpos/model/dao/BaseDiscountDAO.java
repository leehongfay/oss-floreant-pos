package com.floreantpos.model.dao;

import com.floreantpos.model.Discount;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseDiscountDAO
  extends _RootDAO
{
  public static DiscountDAO instance;
  
  public BaseDiscountDAO() {}
  
  public static DiscountDAO getInstance()
  {
    if (null == instance) instance = new DiscountDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return Discount.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public Discount cast(Object object)
  {
    return (Discount)object;
  }
  
  public Discount get(String key) throws HibernateException
  {
    return (Discount)get(getReferenceClass(), key);
  }
  
  public Discount get(String key, Session s) throws HibernateException
  {
    return (Discount)get(getReferenceClass(), key, s);
  }
  
  public Discount load(String key) throws HibernateException
  {
    return (Discount)load(getReferenceClass(), key);
  }
  
  public Discount load(String key, Session s) throws HibernateException
  {
    return (Discount)load(getReferenceClass(), key, s);
  }
  
  public Discount loadInitialize(String key, Session s) throws HibernateException
  {
    Discount obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<Discount> findAll()
  {
    return super.findAll();
  }
  


  public List<Discount> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<Discount> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(Discount discount)
    throws HibernateException
  {
    return (String)super.save(discount);
  }
  







  public String save(Discount discount, Session s)
    throws HibernateException
  {
    return (String)save(discount, s);
  }
  





  public void saveOrUpdate(Discount discount)
    throws HibernateException
  {
    saveOrUpdate(discount);
  }
  







  public void saveOrUpdate(Discount discount, Session s)
    throws HibernateException
  {
    saveOrUpdate(discount, s);
  }
  




  public void update(Discount discount)
    throws HibernateException
  {
    update(discount);
  }
  






  public void update(Discount discount, Session s)
    throws HibernateException
  {
    update(discount, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(Discount discount)
    throws HibernateException
  {
    delete(discount);
  }
  






  public void delete(Discount discount, Session s)
    throws HibernateException
  {
    delete(discount, s);
  }
  









  public void refresh(Discount discount, Session s)
    throws HibernateException
  {
    refresh(discount, s);
  }
}
