package com.floreantpos.model.dao;

import com.floreantpos.model.Department;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseDepartmentDAO
  extends _RootDAO
{
  public static DepartmentDAO instance;
  
  public BaseDepartmentDAO() {}
  
  public static DepartmentDAO getInstance()
  {
    if (null == instance) instance = new DepartmentDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return Department.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public Department cast(Object object)
  {
    return (Department)object;
  }
  
  public Department get(String key) throws HibernateException
  {
    return (Department)get(getReferenceClass(), key);
  }
  
  public Department get(String key, Session s) throws HibernateException
  {
    return (Department)get(getReferenceClass(), key, s);
  }
  
  public Department load(String key) throws HibernateException
  {
    return (Department)load(getReferenceClass(), key);
  }
  
  public Department load(String key, Session s) throws HibernateException
  {
    return (Department)load(getReferenceClass(), key, s);
  }
  
  public Department loadInitialize(String key, Session s) throws HibernateException
  {
    Department obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<Department> findAll()
  {
    return super.findAll();
  }
  


  public List<Department> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<Department> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(Department department)
    throws HibernateException
  {
    return (String)super.save(department);
  }
  







  public String save(Department department, Session s)
    throws HibernateException
  {
    return (String)save(department, s);
  }
  





  public void saveOrUpdate(Department department)
    throws HibernateException
  {
    saveOrUpdate(department);
  }
  







  public void saveOrUpdate(Department department, Session s)
    throws HibernateException
  {
    saveOrUpdate(department, s);
  }
  




  public void update(Department department)
    throws HibernateException
  {
    update(department);
  }
  






  public void update(Department department, Session s)
    throws HibernateException
  {
    update(department, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(Department department)
    throws HibernateException
  {
    delete(department);
  }
  






  public void delete(Department department, Session s)
    throws HibernateException
  {
    delete(department, s);
  }
  









  public void refresh(Department department, Session s)
    throws HibernateException
  {
    refresh(department, s);
  }
}
