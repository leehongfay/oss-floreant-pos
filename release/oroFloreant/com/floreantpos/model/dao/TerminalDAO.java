package com.floreantpos.model.dao;

import com.floreantpos.PosLog;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.Outlet;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;





















public class TerminalDAO
  extends BaseTerminalDAO
{
  public TerminalDAO() {}
  
  public boolean isDrawerAssigned(Terminal terminal)
  {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Terminal.PROP_ID, terminal.getId()));
      criteria.setProjection(Projections.property(Terminal.PROP_ASSIGNED_USER_ID));
      Object result = criteria.uniqueResult();
      return result != null;
    } finally {
      closeSession(session);
    }
  }
  
  public Integer createNewTerminalId() {
    Session session = null;
    try
    {
      session = getSession();
      return createNewTerminalId(session);
    } finally {
      closeSession(session);
    }
  }
  
  public Integer createNewTerminalId(Session session) {
    Random random = new Random();
    for (;;)
    {
      Integer terminalId = Integer.valueOf(random.nextInt(10000) + 1);
      Criteria criteria = session.createCriteria(getClass());
      criteria.add(Restrictions.eq(Terminal.PROP_ID, terminalId));
      Object uniqueResult = criteria.uniqueResult();
      if (uniqueResult == null) {
        return terminalId;
      }
    }
  }
  
  public Terminal createNewTerminal(String terminalKey, Integer terminalId, Outlet outlet) {
    return createNewTerminal(terminalKey, terminalId, outlet, null);
  }
  
  public Terminal createNewTerminal(String terminalKey, Integer terminalId, Outlet outlet, Session session) {
    if ((terminalId == null) || (terminalId.intValue() == -1)) {
      terminalId = createNewTerminalId();
    }
    Terminal terminal = new Terminal();
    terminal.setId(terminalId);
    terminal.setTerminalKey(terminalKey);
    terminal.setName(String.valueOf("Terminal " + terminalId));
    if (outlet != null) {
      terminal.setOutletId(outlet.getId());
    }
    if (session == null) {
      save(terminal);
    }
    else {
      save(terminal, session);
    }
    
    return terminal;
  }
  
  public void refresh(Terminal terminal) {
    Session session = null;
    try
    {
      session = getSession();
      session.refresh(terminal);
      
      closeSession(session); } finally { closeSession(session);
    }
  }
  
  public List<Terminal> findOpenTerminals() {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.isNotNull(Terminal.PROP_ASSIGNED_USER_ID));
      return criteria.list();
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public List<Terminal> findCashDrawerTerminals() {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Terminal.PROP_HAS_CASH_DRAWER, Boolean.TRUE));
      return criteria.list();
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  


  public void performBatchSave(Object... objects)
  {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      for (Object object : objects) {
        if (object != null) {
          session.saveOrUpdate(object);
        }
      }
      tx.commit();
    } catch (Exception e) {
      try {
        tx.rollback();
      }
      catch (Exception localException2) {}
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  public void performBatchSave(List<String> customQueryList, Object... objects) {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      for (Object object : objects) {
        if (object != null) {
          session.saveOrUpdate(object);
        }
      }
      if (customQueryList != null) {
        for (??? = customQueryList.iterator(); ((Iterator)???).hasNext();) { String sql = (String)((Iterator)???).next();
          Query query = session.createQuery(sql);
          query.executeUpdate();
        }
      }
      tx.commit();
    } catch (Exception e) {
      try {
        tx.rollback();
      }
      catch (Exception localException2) {}
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  public void resetCashDrawer(CashDrawer report, Terminal terminal, User user, double balance) throws Exception {
    Session session = null;
    Transaction tx = null;
    
    report.setClosedBy(user);
    report.setReportTime(new Date());
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      











      terminal.setAssignedUserId(null);
      

      terminal.setCurrentCashDrawer(null);
      
      update(terminal, session);
      saveOrUpdate(report, session);
      
      tx.commit();
    } catch (Exception e) {
      try {
        tx.rollback();
      }
      catch (Exception localException1) {}
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  public void resetStaffBank(User closedByUser, CashDrawer report) throws Exception {
    Session session = null;
    Transaction tx = null;
    
    report.setClosedBy(closedByUser);
    report.setReportTime(new Date());
    User assignedUser = report.getAssignedUser();
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      











      session.saveOrUpdate(report);
      if (assignedUser != null) {
        assignedUser.setStaffBankStarted(Boolean.valueOf(false));
        assignedUser.setCurrentCashDrawer(null);
        session.saveOrUpdate(assignedUser);
      }
      
      tx.commit();
    } catch (Exception e) {
      try {
        tx.rollback();
      }
      catch (Exception localException1) {}
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  public Terminal initialize(Terminal terminal) {
    if ((terminal == null) || (terminal.getId() == null)) {
      return terminal;
    }
    Session session = null;
    try
    {
      session = createNewSession();
      session.refresh(terminal);
      


      return terminal;
    } finally {
      closeSession(session);
    }
  }
  





  public Terminal getByTerminalKey(String terminalKey)
  {
    Session session = null;
    try {
      session = createNewSession();
      return getByTerminalKey(terminalKey, session);
    } finally {
      closeSession(session);
    }
  }
  
  public Terminal getByTerminalKey(String terminalKey, Session session) {
    Criteria criteria = session.createCriteria(getReferenceClass());
    criteria.add(Restrictions.eq(Terminal.PROP_TERMINAL_KEY, terminalKey));
    List<Terminal> list = criteria.list();
    if (list.size() > 0) {
      return (Terminal)list.get(0);
    }
    return null;
  }
  
  public Terminal findByAssignedUser(User user) {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Terminal.PROP_ASSIGNED_USER_ID, user.getId()));
      List<Terminal> list = criteria.list();
      Terminal localTerminal; if (list.size() > 0) {
        return (Terminal)list.get(0);
      }
      return null;
    } finally {
      closeSession(session);
    }
  }
  
  public boolean isVersionEqual(Class beanClass, Object id, long versionToCompare) {
    Session session = createNewSession();
    try {
      Criteria criteria = session.createCriteria(beanClass);
      criteria.add(Restrictions.eq("id", id));
      criteria.setProjection(Projections.property("version"));
      Object result = criteria.uniqueResult();
      boolean bool; if ((result instanceof Number)) {
        return ((Number)result).longValue() == versionToCompare;
      }
      
      return false;
    } finally {
      closeSession(session);
    }
  }
  
  public long getVersion(Class beanClass, String column, Object id) {
    Session session = createNewSession();
    try {
      Criteria criteria = session.createCriteria(beanClass);
      criteria.add(Restrictions.eq(column, id));
      criteria.setProjection(Projections.property("version"));
      
      return ((Long)criteria.uniqueResult()).longValue();
    } finally {
      closeSession(session);
    }
  }
  
  public void executeSqlQuery(List<String> sqlList) {
    if (sqlList == null)
      return;
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      for (String sql : sqlList) {
        Query query = session.createSQLQuery(sql);
        query.executeUpdate();
      }
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      PosLog.error(getClass(), e);
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public List executeSqlQuery(String sql) {
    return executeSqlQuery(sql, 0L, 0L);
  }
  
  public List executeSqlQuery(String sql, long startIndex, long maxResult) {
    Session session = null;
    try {
      session = createNewSession();
      SQLQuery sqlQuery = session.createSQLQuery(sql);
      if (maxResult > 0L) {
        sqlQuery.setFirstResult((int)startIndex);
        sqlQuery.setMaxResults((int)maxResult);
      }
      return sqlQuery.list();
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
    return null;
  }
}
