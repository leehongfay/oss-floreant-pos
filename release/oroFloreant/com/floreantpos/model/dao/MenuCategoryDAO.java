package com.floreantpos.model.dao;

import com.floreantpos.model.Department;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuShift;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.TerminalType;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.swing.PaginatedListModel;
import com.floreantpos.swing.PaginationSupport;
import com.floreantpos.util.ShiftUtil;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;






















public class MenuCategoryDAO
  extends BaseMenuCategoryDAO
{
  public MenuCategoryDAO() {}
  
  public Order getDefaultOrder()
  {
    return Order.asc(MenuCategory.PROP_SORT_ORDER);
  }
  
  public Serializable save(Object menuCategory, Session session)
    throws HibernateException
  {
    Serializable id = super.save(menuCategory, session);
    

    updateDependentModels((MenuCategory)menuCategory, session);
    return id;
  }
  
  public void update(Object menuCategory, Session session)
    throws HibernateException
  {
    super.update(menuCategory, session);
    

    updateDependentModels((MenuCategory)menuCategory, session);
  }
  
  public void saveOrUpdate(Object menuCategory, Session session)
    throws HibernateException
  {
    session.saveOrUpdate(menuCategory);
    

    updateDependentModels((MenuCategory)menuCategory, session);
  }
  

  private void updateDependentModels(MenuCategory menuCategory, Session session)
  {
    String hqlString = "update MenuGroup set %s=:categoryName, %s=:beverage where %s=:categoryId";
    hqlString = String.format(hqlString, new Object[] { MenuGroup.PROP_MENU_CATEGORY_NAME, MenuGroup.PROP_BEVERAGE, MenuGroup.PROP_MENU_CATEGORY_ID });
    




    Query query = session.createQuery(hqlString);
    query.setParameter("categoryName", menuCategory.getDisplayName());
    query.setParameter("beverage", menuCategory.isBeverage());
    query.setParameter("categoryId", menuCategory.getId());
    query.executeUpdate();
    

    hqlString = "update MenuItem set %s=:categoryName, %s=:beverage where %s=:categoryId";
    hqlString = String.format(hqlString, new Object[] { MenuItem.PROP_MENU_CATEGORY_NAME, MenuItem.PROP_BEVERAGE, MenuItem.PROP_MENU_CATEGORY_ID });
    





    query = session.createQuery(hqlString);
    query.setParameter("categoryName", menuCategory.getDisplayName());
    query.setParameter("beverage", menuCategory.isBeverage());
    query.setParameter("categoryId", menuCategory.getId());
    query.executeUpdate();
  }
  
  public List<MenuCategory> findAllEnable(Department department) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass(), "category");
      criteria.add(Restrictions.eq(MenuCategory.PROP_VISIBLE, Boolean.TRUE));
      criteria.addOrder(Order.asc(MenuCategory.PROP_SORT_ORDER));
      
      if (department != null) {
        criteria.createAlias("category.departments", "department");
        criteria.add(Restrictions.eq("department.id", department.getId()));
      }
      
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuCategory> findActiveCategories(OrderType orderType) {
    Terminal terminal = DataProvider.get().getCurrentTerminal();
    Department department = terminal.getDepartment();
    MenuShift menuShift = ShiftUtil.getCurrentMenuShift();
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = setRestrictions(orderType, terminal, department, menuShift, session);
      criteria.setProjection(null);
      criteria.addOrder(Order.asc(MenuCategory.PROP_SORT_ORDER));
      criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public void findActiveCategories(PaginatedListModel listModel, OrderType orderType) { Terminal terminal = DataProvider.get().getCurrentTerminal();
    Department department = terminal.getDepartment();
    MenuShift menuShift = ShiftUtil.getCurrentMenuShift();
    
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = setRestrictions(orderType, terminal, department, menuShift, session);
      criteria.setProjection(Projections.countDistinct(MenuCategory.PROP_ID));
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        listModel.setNumRows(rowCount.intValue());
      }
      












      criteria.setProjection(null);
      criteria.setFirstResult(listModel.getCurrentRowIndex());
      criteria.setMaxResults(listModel.getPageSize());
      criteria.addOrder(Order.asc(MenuCategory.PROP_SORT_ORDER));
      criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
      listModel.setData(criteria.list());
    } finally {
      closeSession(session);
    }
  }
  
  private Criteria setRestrictions(OrderType orderType, Terminal terminal, Department department, MenuShift menuShift, Session session) {
    Criteria criteria = session.createCriteria(getReferenceClass(), "c");
    
    if (orderType != null) {
      criteria.createAlias("c.orderTypes", "orderType", JoinType.LEFT_OUTER_JOIN);
      criteria.add(
        Restrictions.or(
        Restrictions.isEmpty("c.orderTypes"), 
        Restrictions.in("orderType.id", Arrays.asList(new String[] {orderType.getId() }))));
    }
    

    if (terminal.getTerminalType() != null) {
      criteria.createAlias("c.terminalTypes", "terminalType", JoinType.LEFT_OUTER_JOIN);
      criteria.add(
        Restrictions.or(
        Restrictions.isEmpty("c.terminalTypes"), 
        Restrictions.in("terminalType.id", Arrays.asList(new String[] {terminal.getTerminalType().getId() }))));
    }
    

    if (department != null) {
      criteria.createAlias("c.departments", "department", JoinType.LEFT_OUTER_JOIN);
      criteria.add(
        Restrictions.or(
        Restrictions.isEmpty("c.departments"), 
        Restrictions.in("department.id", Arrays.asList(new String[] {department.getId() }))));
    }
    

    if (menuShift != null) {
      criteria.createAlias("c.menuShifts", "menuShifts", JoinType.LEFT_OUTER_JOIN);
      criteria.add(
        Restrictions.or(
        Restrictions.isEmpty("c.menuShifts"), 
        Restrictions.in("menuShifts.id", Arrays.asList(new String[] {menuShift.getId() }))));
    }
    


    criteria.add(Restrictions.eq(MenuCategory.PROP_VISIBLE, Boolean.TRUE));
    return criteria;
  }
  



























  public MenuCategory initialize(MenuCategory menuCategory)
  {
    if ((menuCategory == null) || (menuCategory.getId() == null)) {
      return menuCategory;
    }
    Session session = null;
    try
    {
      session = createNewSession();
      session.refresh(menuCategory);
      
      Hibernate.initialize(menuCategory.getDepartments());
      Hibernate.initialize(menuCategory.getOrderTypes());
      Hibernate.initialize(menuCategory.getMenuShifts());
      
      return menuCategory;
    } finally {
      closeSession(session);
    }
  }
  
  public void performBatchSaveOrUpdate(MenuCategory menuCategory, List<TerminalType> terminalTypes) {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      saveOrUpdate(menuCategory, session);
      tx.commit();
    } catch (Exception e) {
      try {
        tx.rollback();
      }
      catch (Exception localException1) {}
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  public void releaseParentAndDelete(MenuCategory category) {
    if (category == null) {
      return;
    }
    
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      String queryString = "delete from MENUCATEGORY_DISCOUNT where MENUCATEGORY_ID='%s'";
      queryString = String.format(queryString, new Object[] { category.getId() });
      Query query = session.createSQLQuery(queryString);
      query.executeUpdate();
      
      String queryString2 = "delete from CAT_DEPT where CATEGORY_ID='%s'";
      queryString2 = String.format(queryString2, new Object[] { category.getId() });
      Query query2 = session.createSQLQuery(queryString2);
      query2.executeUpdate();
      
      String queryString3 = "delete from CATEGORY_ORDER_TYPE where CATEGORY_ID='%s'";
      queryString3 = String.format(queryString3, new Object[] { category.getId() });
      Query query3 = session.createSQLQuery(queryString3);
      query3.executeUpdate();
      
      String queryString4 = "delete from TERMINAL_TYPE_CATEGORY where MENU_CATEGORY_ID='%s'";
      queryString4 = String.format(queryString4, new Object[] { category.getId() });
      Query query4 = session.createSQLQuery(queryString4);
      query4.executeUpdate();
      
      String queryString5 = "update MENU_GROUP set CATEGORY_ID=null where CATEGORY_ID='%s'";
      queryString5 = String.format(queryString5, new Object[] { category.getId() });
      Query query5 = session.createSQLQuery(queryString5);
      query5.executeUpdate();
      
      session.delete(category);
      
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(ShopTableDAO.class).error(e);
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
  
  public void findCategories(PaginationSupport paginationSupport, boolean includeHiddenItems, String... fields)
  {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass(), "c");
      if (!includeHiddenItems) {
        criteria.add(Restrictions.eq(MenuCategory.PROP_VISIBLE, Boolean.TRUE));
      }
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        paginationSupport.setNumRows(rowCount.intValue());
      }
      
      criteria.setProjection(null);
      criteria.addOrder(Order.asc(MenuCategory.PROP_SORT_ORDER));
      if ((fields != null) && (fields.length > 0)) {
        ProjectionList projectionList = Projections.projectionList();
        for (String field : fields) {
          projectionList.add(Projections.property(field), field);
        }
        criteria.setProjection(projectionList);
        criteria.setResultTransformer(Transformers.aliasToBean(MenuCategory.class));
        paginationSupport.setRows(criteria.list());
      }
      else {
        paginationSupport.setRows(criteria.list());
      }
    } finally {
      closeSession(session);
    }
  }
}
