package com.floreantpos.model.dao;

import com.floreantpos.model.VoidTransaction;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseVoidTransactionDAO
  extends _RootDAO
{
  public static VoidTransactionDAO instance;
  
  public BaseVoidTransactionDAO() {}
  
  public static VoidTransactionDAO getInstance()
  {
    if (null == instance) instance = new VoidTransactionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return VoidTransaction.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public VoidTransaction cast(Object object)
  {
    return (VoidTransaction)object;
  }
  
  public VoidTransaction get(String key) throws HibernateException
  {
    return (VoidTransaction)get(getReferenceClass(), key);
  }
  
  public VoidTransaction get(String key, Session s) throws HibernateException
  {
    return (VoidTransaction)get(getReferenceClass(), key, s);
  }
  
  public VoidTransaction load(String key) throws HibernateException
  {
    return (VoidTransaction)load(getReferenceClass(), key);
  }
  
  public VoidTransaction load(String key, Session s) throws HibernateException
  {
    return (VoidTransaction)load(getReferenceClass(), key, s);
  }
  
  public VoidTransaction loadInitialize(String key, Session s) throws HibernateException
  {
    VoidTransaction obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<VoidTransaction> findAll()
  {
    return super.findAll();
  }
  


  public List<VoidTransaction> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<VoidTransaction> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(VoidTransaction voidTransaction)
    throws HibernateException
  {
    return (String)super.save(voidTransaction);
  }
  







  public String save(VoidTransaction voidTransaction, Session s)
    throws HibernateException
  {
    return (String)save(voidTransaction, s);
  }
  





  public void saveOrUpdate(VoidTransaction voidTransaction)
    throws HibernateException
  {
    saveOrUpdate(voidTransaction);
  }
  







  public void saveOrUpdate(VoidTransaction voidTransaction, Session s)
    throws HibernateException
  {
    saveOrUpdate(voidTransaction, s);
  }
  




  public void update(VoidTransaction voidTransaction)
    throws HibernateException
  {
    update(voidTransaction);
  }
  






  public void update(VoidTransaction voidTransaction, Session s)
    throws HibernateException
  {
    update(voidTransaction, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(VoidTransaction voidTransaction)
    throws HibernateException
  {
    delete(voidTransaction);
  }
  






  public void delete(VoidTransaction voidTransaction, Session s)
    throws HibernateException
  {
    delete(voidTransaction, s);
  }
  









  public void refresh(VoidTransaction voidTransaction, Session s)
    throws HibernateException
  {
    refresh(voidTransaction, s);
  }
}
