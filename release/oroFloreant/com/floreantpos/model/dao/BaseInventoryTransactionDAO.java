package com.floreantpos.model.dao;

import com.floreantpos.model.InventoryTransaction;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseInventoryTransactionDAO
  extends _RootDAO
{
  public static InventoryTransactionDAO instance;
  
  public BaseInventoryTransactionDAO() {}
  
  public static InventoryTransactionDAO getInstance()
  {
    if (null == instance) instance = new InventoryTransactionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return InventoryTransaction.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public InventoryTransaction cast(Object object)
  {
    return (InventoryTransaction)object;
  }
  
  public InventoryTransaction get(String key) throws HibernateException
  {
    return (InventoryTransaction)get(getReferenceClass(), key);
  }
  
  public InventoryTransaction get(String key, Session s) throws HibernateException
  {
    return (InventoryTransaction)get(getReferenceClass(), key, s);
  }
  
  public InventoryTransaction load(String key) throws HibernateException
  {
    return (InventoryTransaction)load(getReferenceClass(), key);
  }
  
  public InventoryTransaction load(String key, Session s) throws HibernateException
  {
    return (InventoryTransaction)load(getReferenceClass(), key, s);
  }
  
  public InventoryTransaction loadInitialize(String key, Session s) throws HibernateException
  {
    InventoryTransaction obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<InventoryTransaction> findAll()
  {
    return super.findAll();
  }
  


  public List<InventoryTransaction> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<InventoryTransaction> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(InventoryTransaction inventoryTransaction)
    throws HibernateException
  {
    return (String)super.save(inventoryTransaction);
  }
  







  public String save(InventoryTransaction inventoryTransaction, Session s)
    throws HibernateException
  {
    return (String)save(inventoryTransaction, s);
  }
  





  public void saveOrUpdate(InventoryTransaction inventoryTransaction)
    throws HibernateException
  {
    saveOrUpdate(inventoryTransaction);
  }
  







  public void saveOrUpdate(InventoryTransaction inventoryTransaction, Session s)
    throws HibernateException
  {
    saveOrUpdate(inventoryTransaction, s);
  }
  




  public void update(InventoryTransaction inventoryTransaction)
    throws HibernateException
  {
    update(inventoryTransaction);
  }
  






  public void update(InventoryTransaction inventoryTransaction, Session s)
    throws HibernateException
  {
    update(inventoryTransaction, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(InventoryTransaction inventoryTransaction)
    throws HibernateException
  {
    delete(inventoryTransaction);
  }
  






  public void delete(InventoryTransaction inventoryTransaction, Session s)
    throws HibernateException
  {
    delete(inventoryTransaction, s);
  }
  









  public void refresh(InventoryTransaction inventoryTransaction, Session s)
    throws HibernateException
  {
    refresh(inventoryTransaction, s);
  }
}
