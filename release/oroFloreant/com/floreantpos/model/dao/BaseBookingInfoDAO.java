package com.floreantpos.model.dao;

import com.floreantpos.model.BookingInfo;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseBookingInfoDAO
  extends _RootDAO
{
  public static BookingInfoDAO instance;
  
  public BaseBookingInfoDAO() {}
  
  public static BookingInfoDAO getInstance()
  {
    if (null == instance) instance = new BookingInfoDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return BookingInfo.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public BookingInfo cast(Object object)
  {
    return (BookingInfo)object;
  }
  
  public BookingInfo get(String key) throws HibernateException
  {
    return (BookingInfo)get(getReferenceClass(), key);
  }
  
  public BookingInfo get(String key, Session s) throws HibernateException
  {
    return (BookingInfo)get(getReferenceClass(), key, s);
  }
  
  public BookingInfo load(String key) throws HibernateException
  {
    return (BookingInfo)load(getReferenceClass(), key);
  }
  
  public BookingInfo load(String key, Session s) throws HibernateException
  {
    return (BookingInfo)load(getReferenceClass(), key, s);
  }
  
  public BookingInfo loadInitialize(String key, Session s) throws HibernateException
  {
    BookingInfo obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<BookingInfo> findAll()
  {
    return super.findAll();
  }
  


  public List<BookingInfo> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<BookingInfo> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(BookingInfo bookingInfo)
    throws HibernateException
  {
    return (String)super.save(bookingInfo);
  }
  







  public String save(BookingInfo bookingInfo, Session s)
    throws HibernateException
  {
    return (String)save(bookingInfo, s);
  }
  





  public void saveOrUpdate(BookingInfo bookingInfo)
    throws HibernateException
  {
    saveOrUpdate(bookingInfo);
  }
  







  public void saveOrUpdate(BookingInfo bookingInfo, Session s)
    throws HibernateException
  {
    saveOrUpdate(bookingInfo, s);
  }
  




  public void update(BookingInfo bookingInfo)
    throws HibernateException
  {
    update(bookingInfo);
  }
  






  public void update(BookingInfo bookingInfo, Session s)
    throws HibernateException
  {
    update(bookingInfo, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(BookingInfo bookingInfo)
    throws HibernateException
  {
    delete(bookingInfo);
  }
  






  public void delete(BookingInfo bookingInfo, Session s)
    throws HibernateException
  {
    delete(bookingInfo, s);
  }
  









  public void refresh(BookingInfo bookingInfo, Session s)
    throws HibernateException
  {
    refresh(bookingInfo, s);
  }
}
