package com.floreantpos.model.dao;

import com.floreantpos.model.Customer;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseCustomerDAO
  extends _RootDAO
{
  public static CustomerDAO instance;
  
  public BaseCustomerDAO() {}
  
  public static CustomerDAO getInstance()
  {
    if (null == instance) instance = new CustomerDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return Customer.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public Customer cast(Object object)
  {
    return (Customer)object;
  }
  
  public Customer get(String key) throws HibernateException
  {
    return (Customer)get(getReferenceClass(), key);
  }
  
  public Customer get(String key, Session s) throws HibernateException
  {
    return (Customer)get(getReferenceClass(), key, s);
  }
  
  public Customer load(String key) throws HibernateException
  {
    return (Customer)load(getReferenceClass(), key);
  }
  
  public Customer load(String key, Session s) throws HibernateException
  {
    return (Customer)load(getReferenceClass(), key, s);
  }
  
  public Customer loadInitialize(String key, Session s) throws HibernateException
  {
    Customer obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<Customer> findAll()
  {
    return super.findAll();
  }
  


  public List<Customer> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<Customer> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(Customer customer)
    throws HibernateException
  {
    return (String)super.save(customer);
  }
  







  public String save(Customer customer, Session s)
    throws HibernateException
  {
    return (String)save(customer, s);
  }
  





  public void saveOrUpdate(Customer customer)
    throws HibernateException
  {
    saveOrUpdate(customer);
  }
  







  public void saveOrUpdate(Customer customer, Session s)
    throws HibernateException
  {
    saveOrUpdate(customer, s);
  }
  




  public void update(Customer customer)
    throws HibernateException
  {
    update(customer);
  }
  






  public void update(Customer customer, Session s)
    throws HibernateException
  {
    update(customer, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(Customer customer)
    throws HibernateException
  {
    delete(customer);
  }
  






  public void delete(Customer customer, Session s)
    throws HibernateException
  {
    delete(customer, s);
  }
  









  public void refresh(Customer customer, Session s)
    throws HibernateException
  {
    refresh(customer, s);
  }
}
