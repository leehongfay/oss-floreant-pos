package com.floreantpos.model.dao;

import com.floreantpos.model.PosTransaction;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BasePosTransactionDAO
  extends _RootDAO
{
  public static PosTransactionDAO instance;
  
  public BasePosTransactionDAO() {}
  
  public static PosTransactionDAO getInstance()
  {
    if (null == instance) instance = new PosTransactionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return PosTransaction.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public PosTransaction cast(Object object)
  {
    return (PosTransaction)object;
  }
  
  public PosTransaction get(String key) throws HibernateException
  {
    return (PosTransaction)get(getReferenceClass(), key);
  }
  
  public PosTransaction get(String key, Session s) throws HibernateException
  {
    return (PosTransaction)get(getReferenceClass(), key, s);
  }
  
  public PosTransaction load(String key) throws HibernateException
  {
    return (PosTransaction)load(getReferenceClass(), key);
  }
  
  public PosTransaction load(String key, Session s) throws HibernateException
  {
    return (PosTransaction)load(getReferenceClass(), key, s);
  }
  
  public PosTransaction loadInitialize(String key, Session s) throws HibernateException
  {
    PosTransaction obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<PosTransaction> findAll()
  {
    return super.findAll();
  }
  


  public List<PosTransaction> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<PosTransaction> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(PosTransaction posTransaction)
    throws HibernateException
  {
    return (String)super.save(posTransaction);
  }
  







  public String save(PosTransaction posTransaction, Session s)
    throws HibernateException
  {
    return (String)save(posTransaction, s);
  }
  





  public void saveOrUpdate(PosTransaction posTransaction)
    throws HibernateException
  {
    saveOrUpdate(posTransaction);
  }
  







  public void saveOrUpdate(PosTransaction posTransaction, Session s)
    throws HibernateException
  {
    saveOrUpdate(posTransaction, s);
  }
  




  public void update(PosTransaction posTransaction)
    throws HibernateException
  {
    update(posTransaction);
  }
  






  public void update(PosTransaction posTransaction, Session s)
    throws HibernateException
  {
    update(posTransaction, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(PosTransaction posTransaction)
    throws HibernateException
  {
    delete(posTransaction);
  }
  






  public void delete(PosTransaction posTransaction, Session s)
    throws HibernateException
  {
    delete(posTransaction, s);
  }
  









  public void refresh(PosTransaction posTransaction, Session s)
    throws HibernateException
  {
    refresh(posTransaction, s);
  }
}
