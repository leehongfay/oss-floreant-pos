package com.floreantpos.model.dao;

import com.floreantpos.model.EmployeeShift;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseEmployeeShiftDAO
  extends _RootDAO
{
  public static EmployeeShiftDAO instance;
  
  public BaseEmployeeShiftDAO() {}
  
  public static EmployeeShiftDAO getInstance()
  {
    if (null == instance) instance = new EmployeeShiftDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return EmployeeShift.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public EmployeeShift cast(Object object)
  {
    return (EmployeeShift)object;
  }
  
  public EmployeeShift get(String key) throws HibernateException
  {
    return (EmployeeShift)get(getReferenceClass(), key);
  }
  
  public EmployeeShift get(String key, Session s) throws HibernateException
  {
    return (EmployeeShift)get(getReferenceClass(), key, s);
  }
  
  public EmployeeShift load(String key) throws HibernateException
  {
    return (EmployeeShift)load(getReferenceClass(), key);
  }
  
  public EmployeeShift load(String key, Session s) throws HibernateException
  {
    return (EmployeeShift)load(getReferenceClass(), key, s);
  }
  
  public EmployeeShift loadInitialize(String key, Session s) throws HibernateException
  {
    EmployeeShift obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<EmployeeShift> findAll()
  {
    return super.findAll();
  }
  


  public List<EmployeeShift> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<EmployeeShift> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(EmployeeShift employeeShift)
    throws HibernateException
  {
    return (String)super.save(employeeShift);
  }
  







  public String save(EmployeeShift employeeShift, Session s)
    throws HibernateException
  {
    return (String)save(employeeShift, s);
  }
  





  public void saveOrUpdate(EmployeeShift employeeShift)
    throws HibernateException
  {
    saveOrUpdate(employeeShift);
  }
  







  public void saveOrUpdate(EmployeeShift employeeShift, Session s)
    throws HibernateException
  {
    saveOrUpdate(employeeShift, s);
  }
  




  public void update(EmployeeShift employeeShift)
    throws HibernateException
  {
    update(employeeShift);
  }
  






  public void update(EmployeeShift employeeShift, Session s)
    throws HibernateException
  {
    update(employeeShift, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(EmployeeShift employeeShift)
    throws HibernateException
  {
    delete(employeeShift);
  }
  






  public void delete(EmployeeShift employeeShift, Session s)
    throws HibernateException
  {
    delete(employeeShift, s);
  }
  









  public void refresh(EmployeeShift employeeShift, Session s)
    throws HibernateException
  {
    refresh(employeeShift, s);
  }
}
