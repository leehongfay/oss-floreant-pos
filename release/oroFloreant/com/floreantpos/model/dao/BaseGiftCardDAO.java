package com.floreantpos.model.dao;

import com.floreantpos.model.GiftCard;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseGiftCardDAO
  extends _RootDAO
{
  public static GiftCardDAO instance;
  
  public BaseGiftCardDAO() {}
  
  public static GiftCardDAO getInstance()
  {
    if (null == instance) instance = new GiftCardDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return GiftCard.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public GiftCard cast(Object object)
  {
    return (GiftCard)object;
  }
  
  public GiftCard get(String key) throws HibernateException
  {
    return (GiftCard)get(getReferenceClass(), key);
  }
  
  public GiftCard get(String key, Session s) throws HibernateException
  {
    return (GiftCard)get(getReferenceClass(), key, s);
  }
  
  public GiftCard load(String key) throws HibernateException
  {
    return (GiftCard)load(getReferenceClass(), key);
  }
  
  public GiftCard load(String key, Session s) throws HibernateException
  {
    return (GiftCard)load(getReferenceClass(), key, s);
  }
  
  public GiftCard loadInitialize(String key, Session s) throws HibernateException
  {
    GiftCard obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<GiftCard> findAll()
  {
    return super.findAll();
  }
  


  public List<GiftCard> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<GiftCard> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(GiftCard giftCard)
    throws HibernateException
  {
    return (String)super.save(giftCard);
  }
  







  public String save(GiftCard giftCard, Session s)
    throws HibernateException
  {
    return (String)save(giftCard, s);
  }
  





  public void saveOrUpdate(GiftCard giftCard)
    throws HibernateException
  {
    saveOrUpdate(giftCard);
  }
  







  public void saveOrUpdate(GiftCard giftCard, Session s)
    throws HibernateException
  {
    saveOrUpdate(giftCard, s);
  }
  




  public void update(GiftCard giftCard)
    throws HibernateException
  {
    update(giftCard);
  }
  






  public void update(GiftCard giftCard, Session s)
    throws HibernateException
  {
    update(giftCard, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(GiftCard giftCard)
    throws HibernateException
  {
    delete(giftCard);
  }
  






  public void delete(GiftCard giftCard, Session s)
    throws HibernateException
  {
    delete(giftCard, s);
  }
  









  public void refresh(GiftCard giftCard, Session s)
    throws HibernateException
  {
    refresh(giftCard, s);
  }
}
