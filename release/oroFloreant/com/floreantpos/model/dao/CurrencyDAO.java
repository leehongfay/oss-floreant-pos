package com.floreantpos.model.dao;

import com.floreantpos.model.Currency;
import org.hibernate.Session;
import org.hibernate.Transaction;





















public class CurrencyDAO
  extends BaseCurrencyDAO
{
  public CurrencyDAO() {}
  
  public void saveOrUpdate(Currency currency)
  {
    Session session = null;
    Transaction transaction = null;
    try {
      session = createNewSession();
      transaction = session.beginTransaction();
      
      if (currency.isMain().booleanValue()) {
        for (Currency curr : findAll()) {
          if ((!currency.getId().equals(curr.getId())) && (curr.isMain().booleanValue())) {
            curr.setMain(Boolean.valueOf(false));
            session.saveOrUpdate(curr);
          }
        }
      }
      
      session.saveOrUpdate(currency);
      transaction.commit();
    } finally {
      session.close();
    }
  }
}
