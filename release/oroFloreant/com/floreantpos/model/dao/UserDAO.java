package com.floreantpos.model.dao;

import com.floreantpos.Messages;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.model.ActionHistory;
import com.floreantpos.model.AttendenceHistory;
import com.floreantpos.model.EmployeeInOutHistory;
import com.floreantpos.model.Shift;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.swing.PaginationSupport;
import com.floreantpos.util.AESencrp;
import com.floreantpos.util.UserNotFoundException;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.ResultTransformer;

















public class UserDAO
  extends BaseUserDAO
{
  public static final UserDAO instance = new UserDAO();
  


  public UserDAO() {}
  

  public int rowCount(String filterItem)
  {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(User.class);
      criteria.setProjection(Projections.rowCount());
      if (StringUtils.isNotEmpty(filterItem)) {
        criteria.add(Restrictions.or(Restrictions.ilike(User.PROP_ID, filterItem, MatchMode.ANYWHERE), 
          Restrictions.or(Restrictions.ilike(User.PROP_FIRST_NAME, filterItem, MatchMode.ANYWHERE), 
          Restrictions.ilike(User.PROP_LAST_NAME, filterItem, MatchMode.ANYWHERE))));
      }
      criteria.add(Restrictions.or(Restrictions.isNull(User.PROP_ROOT), Restrictions.eq(User.PROP_ROOT, Boolean.valueOf(true))));
      Number rowCount = (Number)criteria.uniqueResult();
      int i; if (rowCount != null) {
        return rowCount.intValue();
      }
      return 0;
    } finally {
      closeSession(session);
    }
  }
  
  public List<User> loadUsers(PaginationSupport model, String filterItem) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(User.class);
      if (StringUtils.isNotEmpty(filterItem)) {
        criteria.add(Restrictions.or(Restrictions.ilike(User.PROP_ID, filterItem, MatchMode.ANYWHERE), 
          Restrictions.or(Restrictions.ilike(User.PROP_FIRST_NAME, filterItem, MatchMode.ANYWHERE), 
          Restrictions.ilike(User.PROP_LAST_NAME, filterItem, MatchMode.ANYWHERE))));
      }
      criteria.add(Restrictions.or(Restrictions.isNull(User.PROP_ROOT), Restrictions.eq(User.PROP_ROOT, Boolean.valueOf(true))));
      criteria.setFirstResult(model.getCurrentRowIndex());
      criteria.setMaxResults(model.getPageSize());
      List<User> result = criteria.list();
      model.setRows(result);
      return result;
    } finally {
      closeSession(session);
    }
  }
  
  public User getRandomUser() {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.or(Restrictions.isNull(User.PROP_ROOT), Restrictions.eq(User.PROP_ROOT, Boolean.valueOf(true))));
      criteria.add(Restrictions.eq(User.PROP_ACTIVE, Boolean.valueOf(true)));
      criteria.setMaxResults(1);
      List<User> users = criteria.list();
      User localUser; if (users.size() > 0) {
        return (User)users.get(0);
      }
      return null;
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public List<User> findAll()
  {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.or(Restrictions.isNull(User.PROP_ROOT), Restrictions.eq(User.PROP_ROOT, Boolean.valueOf(true))));
      return criteria.list();
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public List<User> findClockedInUsers() {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.or(Restrictions.isNull(User.PROP_ACTIVE), Restrictions.eq(User.PROP_ACTIVE, Boolean.valueOf(true))));
      criteria.add(Restrictions.eq(User.PROP_CLOCKED_IN, Boolean.valueOf(true)));
      return criteria.list();
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public List<User> findAllActive() {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      
      Junction activeUserCriteria = Restrictions.disjunction().add(Restrictions.isNull(User.PROP_ACTIVE)).add(Restrictions.eq(User.PROP_ACTIVE, Boolean.TRUE));
      criteria.add(Restrictions.or(Restrictions.isNull(User.PROP_ROOT), Restrictions.eq(User.PROP_ROOT, Boolean.valueOf(true))));
      criteria.add(activeUserCriteria);
      

      return criteria.list();
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public List<User> findActiveUsersForPayroll() {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      
      Junction activeUserCriteria = Restrictions.disjunction().add(Restrictions.isNull(User.PROP_ACTIVE)).add(Restrictions.eq(User.PROP_ACTIVE, Boolean.TRUE));
      criteria.add(Restrictions.or(Restrictions.isNull(User.PROP_ROOT), Restrictions.eq(User.PROP_ROOT, Boolean.valueOf(true))));
      criteria.add(activeUserCriteria);
      criteria.addOrder(Order.asc(User.PROP_FIRST_NAME));
      
      return criteria.list();
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public List<User> findDrivers() {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(User.PROP_DRIVER, Boolean.TRUE));
      criteria.add(Restrictions.or(Restrictions.isNull(User.PROP_ROOT), Restrictions.eq(User.PROP_ROOT, Boolean.valueOf(true))));
      
      return criteria.list();
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public User findUser(String userId) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(User.PROP_ID, userId));
      criteria.add(Restrictions.or(Restrictions.isNull(User.PROP_ROOT), Restrictions.eq(User.PROP_ROOT, Boolean.valueOf(true))));
      
      Object result = criteria.uniqueResult();
      if (result != null) {
        return (User)result;
      }
      

      throw new UserNotFoundException(Messages.getString("UserDAO.0") + userId + Messages.getString("UserDAO.1"));
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public User findUserBySecretKey(String secretKey) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(User.PROP_PASSWORD, getEncrypedPassword(secretKey)));
      criteria.add(Restrictions.or(Restrictions.isNull(User.PROP_ROOT), Restrictions.eq(User.PROP_ROOT, Boolean.valueOf(true))));
      criteria.add(Restrictions.or(Restrictions.isNull(User.PROP_ACTIVE), Restrictions.eq(User.PROP_ACTIVE, Boolean.valueOf(true))));
      
      List list = criteria.list();
      User localUser; if ((list != null) && (list.size() > 0)) {
        return (User)list.get(0);
      }
      return null;
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  private String getEncrypedPassword(String secretKey) {
    if (StringUtils.isNotEmpty(secretKey)) {
      try {
        secretKey = AESencrp.encrypt(secretKey);
      } catch (Exception e) {
        PosLog.error(getClass(), e);
      }
    }
    return secretKey;
  }
  
  public boolean isUserExist(String userId) {
    try {
      User user = findUser(userId);
      
      return user != null;
    }
    catch (UserNotFoundException x) {}
    return false;
  }
  
  public Integer findUserWithMaxId()
  {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.setProjection(Projections.max(User.PROP_ID));
      criteria.add(Restrictions.or(Restrictions.isNull(User.PROP_ROOT), Restrictions.eq(User.PROP_ROOT, Boolean.valueOf(true))));
      
      List list = criteria.list();
      Integer localInteger; if ((list != null) && (list.size() > 0)) {
        return (Integer)list.get(0);
      }
      
      return null;
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public List<User> getClockedInUser(Terminal terminal, boolean includeStaffBank) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(User.PROP_CLOCKED_IN, Boolean.TRUE));
      

      if (!includeStaffBank) {
        criteria.add(Restrictions.eq(User.PROP_STAFF_BANK, Boolean.FALSE));
      }
      
      Junction activeUserCriteria = Restrictions.disjunction().add(Restrictions.isNull(User.PROP_ACTIVE)).add(Restrictions.eq(User.PROP_ACTIVE, Boolean.TRUE));
      criteria.add(activeUserCriteria);
      


      return criteria.list();
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public void saveClockIn(User user, AttendenceHistory attendenceHistory, Shift shift, Calendar currentTime)
  {
    Session session = null;
    Transaction tx = null;
    
    ActionHistory actionHistory = new ActionHistory();
    actionHistory.setActionName("CLOCK IN");
    actionHistory.setActionTime(attendenceHistory.getClockInTime());
    String actionMessage = String.format("User %s clocks in", new Object[] { user.getId() });
    actionHistory.setDescription(actionMessage);
    actionHistory.setPerformer(user);
    try
    {
      session = getSession();
      tx = session.beginTransaction();
      
      session.saveOrUpdate(user);
      session.saveOrUpdate(attendenceHistory);
      session.save(actionHistory);
      
      tx.commit();
    } catch (Exception e) {
      PosLog.error(getClass(), e);
      
      if (tx != null) {
        try {
          tx.rollback();
        }
        catch (Exception localException1) {}
      }
      throw new PosException(Messages.getString("UserDAO.2"), e);
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public void saveClockOut(User user, AttendenceHistory attendenceHistory, Shift shift, Calendar currentTime) {
    ActionHistory actionHistory = new ActionHistory();
    actionHistory.setActionName("CLOCK OUT");
    actionHistory.setActionTime(attendenceHistory.getClockOutTime());
    String actionMessage = String.format("User %s clocks out", new Object[] { user.getId() });
    actionHistory.setDescription(actionMessage);
    actionHistory.setPerformer(user);
    
    Session session = null;
    Transaction tx = null;
    try
    {
      session = getSession();
      tx = session.beginTransaction();
      
      session.saveOrUpdate(user);
      session.saveOrUpdate(attendenceHistory);
      session.save(actionHistory);
      
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        try {
          tx.rollback();
        }
        catch (Exception localException1) {}
      }
      throw new PosException(Messages.getString("UserDAO.3"), e);
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public void saveDriverOut(User user, EmployeeInOutHistory attendenceHistory, Shift shift, Calendar currentTime) {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = getSession();
      tx = session.beginTransaction();
      
      session.saveOrUpdate(user);
      session.saveOrUpdate(attendenceHistory);
      
      tx.commit();
    } catch (Exception e) {
      PosLog.error(getClass(), e);
      
      if (tx != null) {
        try {
          tx.rollback();
        }
        catch (Exception localException1) {}
      }
      throw new PosException(Messages.getString("UserDAO.2"), e);
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public void saveDriverIn(User user, EmployeeInOutHistory attendenceHistory, Shift shift, Calendar currentTime) {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = getSession();
      tx = session.beginTransaction();
      
      session.saveOrUpdate(user);
      session.saveOrUpdate(attendenceHistory);
      
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        try {
          tx.rollback();
        }
        catch (Exception localException1) {}
      }
      throw new PosException(Messages.getString("UserDAO.3"), e);
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  private boolean validate(User user, boolean editMode) throws PosException {
    String hql = "from User u where u." + User.PROP_ID + "=:userId and u." + User.PROP_USER_TYPE_ID + "=:userTypeId";
    
    Session session = getSession();
    Query query = session.createQuery(hql);
    query = query.setParameter("userId", user.getId());
    query = query.setParameter("userTypeId", user.getUserTypeId());
    
    if (query.list().size() > 0) {
      throw new PosException(Messages.getString("UserDAO.7"));
    }
    
    return true;
  }
  
  public void saveOrUpdate(User user, boolean editMode) {
    Session session = null;
    try
    {
      if (!editMode) {
        validate(user, editMode);
      }
      super.saveOrUpdate(user);
    } catch (Exception x) {
      throw new PosException(Messages.getString("UserDAO.8"), x);
    } finally {
      closeSession(session);
    }
  }
  
































  public int findNumberOfOpenTickets(User user)
    throws PosException
  {
    Session session = null;
    Transaction tx = null;
    
    String hql = "select count(*) from Ticket ticket where ticket.owner=:owner and ticket." + Ticket.PROP_CLOSED + "settled=false";
    
    int count = 0;
    try {
      session = getSession();
      tx = session.beginTransaction();
      Query query = session.createQuery(hql);
      query = query.setEntity("owner", user);
      Iterator iterator = query.iterate();
      if (iterator.hasNext()) {
        count = ((Integer)iterator.next()).intValue();
      }
      tx.commit();
      return count;
    }
    catch (Exception e)
    {
      try {}catch (Exception localException1) {}
      


      throw new PosException(Messages.getString("UserDAO.12"), e);
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public boolean isClockedIn(User user) {
    Session session = getSession();
    Criteria criteria = session.createCriteria(getReferenceClass());
    criteria.add(Restrictions.eq(User.PROP_ID, user.getId()));
    
    ProjectionList projectionList = Projections.projectionList();
    projectionList.add(Projections.property(User.PROP_CLOCKED_IN));
    
    ResultTransformer transformer = new ResultTransformer() {
      public Object transformTuple(Object[] row, String[] arg1) {
        return row[0];
      }
      
      public List transformList(List arg0) {
        return arg0;
      }
    };
    criteria.setProjection(projectionList).setResultTransformer(transformer);
    Boolean result = (Boolean)criteria.uniqueResult();
    return result.booleanValue();
  }
  
  public User getRandomUser(Session session) {
    Criteria criteria = session.createCriteria(getReferenceClass());
    criteria.add(Restrictions.eq(User.PROP_ACTIVE, Boolean.TRUE));
    criteria.setMaxResults(1);
    List list = criteria.list();
    if (list.size() > 0) {
      return (User)list.get(0);
    }
    
    return null;
  }
}
