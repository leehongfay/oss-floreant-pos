package com.floreantpos.model.dao;

import com.floreantpos.model.ModifierGroup;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseModifierGroupDAO
  extends _RootDAO
{
  public static ModifierGroupDAO instance;
  
  public BaseModifierGroupDAO() {}
  
  public static ModifierGroupDAO getInstance()
  {
    if (null == instance) instance = new ModifierGroupDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return ModifierGroup.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public ModifierGroup cast(Object object)
  {
    return (ModifierGroup)object;
  }
  
  public ModifierGroup get(String key) throws HibernateException
  {
    return (ModifierGroup)get(getReferenceClass(), key);
  }
  
  public ModifierGroup get(String key, Session s) throws HibernateException
  {
    return (ModifierGroup)get(getReferenceClass(), key, s);
  }
  
  public ModifierGroup load(String key) throws HibernateException
  {
    return (ModifierGroup)load(getReferenceClass(), key);
  }
  
  public ModifierGroup load(String key, Session s) throws HibernateException
  {
    return (ModifierGroup)load(getReferenceClass(), key, s);
  }
  
  public ModifierGroup loadInitialize(String key, Session s) throws HibernateException
  {
    ModifierGroup obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<ModifierGroup> findAll()
  {
    return super.findAll();
  }
  


  public List<ModifierGroup> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<ModifierGroup> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(ModifierGroup modifierGroup)
    throws HibernateException
  {
    return (String)super.save(modifierGroup);
  }
  







  public String save(ModifierGroup modifierGroup, Session s)
    throws HibernateException
  {
    return (String)save(modifierGroup, s);
  }
  





  public void saveOrUpdate(ModifierGroup modifierGroup)
    throws HibernateException
  {
    saveOrUpdate(modifierGroup);
  }
  







  public void saveOrUpdate(ModifierGroup modifierGroup, Session s)
    throws HibernateException
  {
    saveOrUpdate(modifierGroup, s);
  }
  




  public void update(ModifierGroup modifierGroup)
    throws HibernateException
  {
    update(modifierGroup);
  }
  






  public void update(ModifierGroup modifierGroup, Session s)
    throws HibernateException
  {
    update(modifierGroup, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(ModifierGroup modifierGroup)
    throws HibernateException
  {
    delete(modifierGroup);
  }
  






  public void delete(ModifierGroup modifierGroup, Session s)
    throws HibernateException
  {
    delete(modifierGroup, s);
  }
  









  public void refresh(ModifierGroup modifierGroup, Session s)
    throws HibernateException
  {
    refresh(modifierGroup, s);
  }
}
