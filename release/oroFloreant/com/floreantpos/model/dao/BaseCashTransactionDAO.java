package com.floreantpos.model.dao;

import com.floreantpos.model.CashTransaction;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseCashTransactionDAO
  extends _RootDAO
{
  public static CashTransactionDAO instance;
  
  public BaseCashTransactionDAO() {}
  
  public static CashTransactionDAO getInstance()
  {
    if (null == instance) instance = new CashTransactionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return CashTransaction.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public CashTransaction cast(Object object)
  {
    return (CashTransaction)object;
  }
  
  public CashTransaction get(String key) throws HibernateException
  {
    return (CashTransaction)get(getReferenceClass(), key);
  }
  
  public CashTransaction get(String key, Session s) throws HibernateException
  {
    return (CashTransaction)get(getReferenceClass(), key, s);
  }
  
  public CashTransaction load(String key) throws HibernateException
  {
    return (CashTransaction)load(getReferenceClass(), key);
  }
  
  public CashTransaction load(String key, Session s) throws HibernateException
  {
    return (CashTransaction)load(getReferenceClass(), key, s);
  }
  
  public CashTransaction loadInitialize(String key, Session s) throws HibernateException
  {
    CashTransaction obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<CashTransaction> findAll()
  {
    return super.findAll();
  }
  


  public List<CashTransaction> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<CashTransaction> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(CashTransaction cashTransaction)
    throws HibernateException
  {
    return (String)super.save(cashTransaction);
  }
  







  public String save(CashTransaction cashTransaction, Session s)
    throws HibernateException
  {
    return (String)save(cashTransaction, s);
  }
  





  public void saveOrUpdate(CashTransaction cashTransaction)
    throws HibernateException
  {
    saveOrUpdate(cashTransaction);
  }
  







  public void saveOrUpdate(CashTransaction cashTransaction, Session s)
    throws HibernateException
  {
    saveOrUpdate(cashTransaction, s);
  }
  




  public void update(CashTransaction cashTransaction)
    throws HibernateException
  {
    update(cashTransaction);
  }
  






  public void update(CashTransaction cashTransaction, Session s)
    throws HibernateException
  {
    update(cashTransaction, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(CashTransaction cashTransaction)
    throws HibernateException
  {
    delete(cashTransaction);
  }
  






  public void delete(CashTransaction cashTransaction, Session s)
    throws HibernateException
  {
    delete(cashTransaction, s);
  }
  









  public void refresh(CashTransaction cashTransaction, Session s)
    throws HibernateException
  {
    refresh(cashTransaction, s);
  }
}
