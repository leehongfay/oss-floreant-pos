package com.floreantpos.model.dao;

import com.floreantpos.model.InventoryUnit;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseInventoryUnitDAO
  extends _RootDAO
{
  public static InventoryUnitDAO instance;
  
  public BaseInventoryUnitDAO() {}
  
  public static InventoryUnitDAO getInstance()
  {
    if (null == instance) instance = new InventoryUnitDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return InventoryUnit.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public InventoryUnit cast(Object object)
  {
    return (InventoryUnit)object;
  }
  
  public InventoryUnit get(String key) throws HibernateException
  {
    return (InventoryUnit)get(getReferenceClass(), key);
  }
  
  public InventoryUnit get(String key, Session s) throws HibernateException
  {
    return (InventoryUnit)get(getReferenceClass(), key, s);
  }
  
  public InventoryUnit load(String key) throws HibernateException
  {
    return (InventoryUnit)load(getReferenceClass(), key);
  }
  
  public InventoryUnit load(String key, Session s) throws HibernateException
  {
    return (InventoryUnit)load(getReferenceClass(), key, s);
  }
  
  public InventoryUnit loadInitialize(String key, Session s) throws HibernateException
  {
    InventoryUnit obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<InventoryUnit> findAll()
  {
    return super.findAll();
  }
  


  public List<InventoryUnit> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<InventoryUnit> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(InventoryUnit inventoryUnit)
    throws HibernateException
  {
    return (String)super.save(inventoryUnit);
  }
  







  public String save(InventoryUnit inventoryUnit, Session s)
    throws HibernateException
  {
    return (String)save(inventoryUnit, s);
  }
  





  public void saveOrUpdate(InventoryUnit inventoryUnit)
    throws HibernateException
  {
    saveOrUpdate(inventoryUnit);
  }
  







  public void saveOrUpdate(InventoryUnit inventoryUnit, Session s)
    throws HibernateException
  {
    saveOrUpdate(inventoryUnit, s);
  }
  




  public void update(InventoryUnit inventoryUnit)
    throws HibernateException
  {
    update(inventoryUnit);
  }
  






  public void update(InventoryUnit inventoryUnit, Session s)
    throws HibernateException
  {
    update(inventoryUnit, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(InventoryUnit inventoryUnit)
    throws HibernateException
  {
    delete(inventoryUnit);
  }
  






  public void delete(InventoryUnit inventoryUnit, Session s)
    throws HibernateException
  {
    delete(inventoryUnit, s);
  }
  









  public void refresh(InventoryUnit inventoryUnit, Session s)
    throws HibernateException
  {
    refresh(inventoryUnit, s);
  }
}
