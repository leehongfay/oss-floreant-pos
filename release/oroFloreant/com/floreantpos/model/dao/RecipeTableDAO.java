package com.floreantpos.model.dao;

import com.floreantpos.model.MenuItem;
import com.floreantpos.model.Recepie;
import com.floreantpos.model.RecipeTable;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;




public class RecipeTableDAO
  extends BaseRecipeTableDAO
{
  public RecipeTableDAO() {}
  
  public RecipeTable findBy(String menuItemId)
  {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.createAlias(RecipeTable.PROP_MENU_ITEM, "item");
      criteria.add(Restrictions.eq("item.id", menuItemId));
      return (RecipeTable)criteria.uniqueResult();
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuItem> findMenuItems(Recepie recipe) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.setProjection(Projections.distinct(Projections.property(RecipeTable.PROP_MENU_ITEM)));
      criteria.createAlias("recipeList", "r");
      criteria.add(Restrictions.eq("r.id", recipe.getId()));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<RecipeTable> findRecipeTables(Recepie recipe) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.createAlias("recipeList", "r");
      criteria.add(Restrictions.eq("r.id", recipe.getId()));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<RecipeTable> findRecipeTables(String searchString) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      Disjunction disjunction; if (StringUtils.isNotEmpty(searchString)) {
        disjunction = Restrictions.disjunction();
        criteria.createAlias("recipeList", "r");
        disjunction.add(Restrictions.ilike("r.name", searchString, MatchMode.ANYWHERE));
        criteria.createAlias(RecipeTable.PROP_MENU_ITEM, "item");
        disjunction.add(Restrictions.ilike("item", searchString, MatchMode.ANYWHERE));
        criteria.add(disjunction);
      }
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public void saveRecipeTables(List<MenuItem> menuItems, Object... objects) {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      if (objects != null) {
        for (Object object : objects) {
          if (object != null) {
            session.saveOrUpdate(object);
          }
        }
      }
      if (menuItems != null) {
        for (??? = menuItems.iterator(); ((Iterator)???).hasNext();) { MenuItem menuItem = (MenuItem)((Iterator)???).next();
          RecipeTable recipeTable = findBy(menuItem.getId());
          if (recipeTable != null)
            session.delete(recipeTable);
        }
      }
      tx.commit();
    } catch (Exception e) {
      try {
        tx.rollback();
      }
      catch (Exception localException2) {}
      throw e;
    } finally {
      closeSession(session);
    }
  }
}
