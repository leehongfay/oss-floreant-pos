package com.floreantpos.model.dao;

import com.floreantpos.model.AttendenceHistory;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseAttendenceHistoryDAO
  extends _RootDAO
{
  public static AttendenceHistoryDAO instance;
  
  public BaseAttendenceHistoryDAO() {}
  
  public static AttendenceHistoryDAO getInstance()
  {
    if (null == instance) instance = new AttendenceHistoryDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return AttendenceHistory.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public AttendenceHistory cast(Object object)
  {
    return (AttendenceHistory)object;
  }
  
  public AttendenceHistory get(String key) throws HibernateException
  {
    return (AttendenceHistory)get(getReferenceClass(), key);
  }
  
  public AttendenceHistory get(String key, Session s) throws HibernateException
  {
    return (AttendenceHistory)get(getReferenceClass(), key, s);
  }
  
  public AttendenceHistory load(String key) throws HibernateException
  {
    return (AttendenceHistory)load(getReferenceClass(), key);
  }
  
  public AttendenceHistory load(String key, Session s) throws HibernateException
  {
    return (AttendenceHistory)load(getReferenceClass(), key, s);
  }
  
  public AttendenceHistory loadInitialize(String key, Session s) throws HibernateException
  {
    AttendenceHistory obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<AttendenceHistory> findAll()
  {
    return super.findAll();
  }
  


  public List<AttendenceHistory> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<AttendenceHistory> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(AttendenceHistory attendenceHistory)
    throws HibernateException
  {
    return (String)super.save(attendenceHistory);
  }
  







  public String save(AttendenceHistory attendenceHistory, Session s)
    throws HibernateException
  {
    return (String)save(attendenceHistory, s);
  }
  





  public void saveOrUpdate(AttendenceHistory attendenceHistory)
    throws HibernateException
  {
    saveOrUpdate(attendenceHistory);
  }
  







  public void saveOrUpdate(AttendenceHistory attendenceHistory, Session s)
    throws HibernateException
  {
    saveOrUpdate(attendenceHistory, s);
  }
  




  public void update(AttendenceHistory attendenceHistory)
    throws HibernateException
  {
    update(attendenceHistory);
  }
  






  public void update(AttendenceHistory attendenceHistory, Session s)
    throws HibernateException
  {
    update(attendenceHistory, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(AttendenceHistory attendenceHistory)
    throws HibernateException
  {
    delete(attendenceHistory);
  }
  






  public void delete(AttendenceHistory attendenceHistory, Session s)
    throws HibernateException
  {
    delete(attendenceHistory, s);
  }
  









  public void refresh(AttendenceHistory attendenceHistory, Session s)
    throws HibernateException
  {
    refresh(attendenceHistory, s);
  }
}
