package com.floreantpos.model.dao;

import com.floreantpos.model.PriceRule;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BasePriceRuleDAO
  extends _RootDAO
{
  public static PriceRuleDAO instance;
  
  public BasePriceRuleDAO() {}
  
  public static PriceRuleDAO getInstance()
  {
    if (null == instance) instance = new PriceRuleDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return PriceRule.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public PriceRule cast(Object object)
  {
    return (PriceRule)object;
  }
  
  public PriceRule get(String key) throws HibernateException
  {
    return (PriceRule)get(getReferenceClass(), key);
  }
  
  public PriceRule get(String key, Session s) throws HibernateException
  {
    return (PriceRule)get(getReferenceClass(), key, s);
  }
  
  public PriceRule load(String key) throws HibernateException
  {
    return (PriceRule)load(getReferenceClass(), key);
  }
  
  public PriceRule load(String key, Session s) throws HibernateException
  {
    return (PriceRule)load(getReferenceClass(), key, s);
  }
  
  public PriceRule loadInitialize(String key, Session s) throws HibernateException
  {
    PriceRule obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<PriceRule> findAll()
  {
    return super.findAll();
  }
  


  public List<PriceRule> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<PriceRule> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(PriceRule priceRule)
    throws HibernateException
  {
    return (String)super.save(priceRule);
  }
  







  public String save(PriceRule priceRule, Session s)
    throws HibernateException
  {
    return (String)save(priceRule, s);
  }
  





  public void saveOrUpdate(PriceRule priceRule)
    throws HibernateException
  {
    saveOrUpdate(priceRule);
  }
  







  public void saveOrUpdate(PriceRule priceRule, Session s)
    throws HibernateException
  {
    saveOrUpdate(priceRule, s);
  }
  




  public void update(PriceRule priceRule)
    throws HibernateException
  {
    update(priceRule);
  }
  






  public void update(PriceRule priceRule, Session s)
    throws HibernateException
  {
    update(priceRule, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(PriceRule priceRule)
    throws HibernateException
  {
    delete(priceRule);
  }
  






  public void delete(PriceRule priceRule, Session s)
    throws HibernateException
  {
    delete(priceRule, s);
  }
  









  public void refresh(PriceRule priceRule, Session s)
    throws HibernateException
  {
    refresh(priceRule, s);
  }
}
