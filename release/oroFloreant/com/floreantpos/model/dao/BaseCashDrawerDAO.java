package com.floreantpos.model.dao;

import com.floreantpos.model.CashDrawer;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseCashDrawerDAO
  extends _RootDAO
{
  public static CashDrawerDAO instance;
  
  public BaseCashDrawerDAO() {}
  
  public static CashDrawerDAO getInstance()
  {
    if (null == instance) instance = new CashDrawerDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return CashDrawer.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public CashDrawer cast(Object object)
  {
    return (CashDrawer)object;
  }
  
  public CashDrawer get(String key) throws HibernateException
  {
    return (CashDrawer)get(getReferenceClass(), key);
  }
  
  public CashDrawer get(String key, Session s) throws HibernateException
  {
    return (CashDrawer)get(getReferenceClass(), key, s);
  }
  
  public CashDrawer load(String key) throws HibernateException
  {
    return (CashDrawer)load(getReferenceClass(), key);
  }
  
  public CashDrawer load(String key, Session s) throws HibernateException
  {
    return (CashDrawer)load(getReferenceClass(), key, s);
  }
  
  public CashDrawer loadInitialize(String key, Session s) throws HibernateException
  {
    CashDrawer obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<CashDrawer> findAll()
  {
    return super.findAll();
  }
  


  public List<CashDrawer> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<CashDrawer> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(CashDrawer cashDrawer)
    throws HibernateException
  {
    return (String)super.save(cashDrawer);
  }
  







  public String save(CashDrawer cashDrawer, Session s)
    throws HibernateException
  {
    return (String)save(cashDrawer, s);
  }
  





  public void saveOrUpdate(CashDrawer cashDrawer)
    throws HibernateException
  {
    saveOrUpdate(cashDrawer);
  }
  







  public void saveOrUpdate(CashDrawer cashDrawer, Session s)
    throws HibernateException
  {
    saveOrUpdate(cashDrawer, s);
  }
  




  public void update(CashDrawer cashDrawer)
    throws HibernateException
  {
    update(cashDrawer);
  }
  






  public void update(CashDrawer cashDrawer, Session s)
    throws HibernateException
  {
    update(cashDrawer, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(CashDrawer cashDrawer)
    throws HibernateException
  {
    delete(cashDrawer);
  }
  






  public void delete(CashDrawer cashDrawer, Session s)
    throws HibernateException
  {
    delete(cashDrawer, s);
  }
  









  public void refresh(CashDrawer cashDrawer, Session s)
    throws HibernateException
  {
    refresh(cashDrawer, s);
  }
}
