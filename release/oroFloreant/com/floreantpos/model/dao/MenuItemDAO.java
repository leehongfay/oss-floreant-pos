package com.floreantpos.model.dao;

import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.model.ComboItem;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.InventoryStock;
import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.InventoryVendorItems;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuItemInventoryStatus;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.MenuPageItem;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Pagination;
import com.floreantpos.model.PurchaseOrder;
import com.floreantpos.model.PurchaseOrderItem;
import com.floreantpos.model.ReportGroup;
import com.floreantpos.model.Terminal;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PaginatedListModel;
import com.floreantpos.swing.PaginationSupport;
import com.floreantpos.util.NumberUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.transform.Transformers;























public class MenuItemDAO
  extends BaseMenuItemDAO
{
  public MenuItemDAO() {}
  
  protected Serializable save(Object obj, Session s)
  {
    Serializable serializable = super.save(obj, s);
    updateDependentModels((MenuItem)obj, s);
    return serializable;
  }
  
  protected void update(Object obj, Session s)
  {
    super.update(obj, s);
    updateDependentModels((MenuItem)obj, s);
  }
  
  protected void saveOrUpdate(Object obj, Session s)
  {
    super.saveOrUpdate(obj, s);
    updateDependentModels((MenuItem)obj, s);
  }
  
  private void updateDependentModels(MenuItem menuItem, Session s)
  {
    saveInventoryStockStatus(menuItem, s);
    

    MenuPageItemDAO menuPageItemDAO = MenuPageItemDAO.getInstance();
    List<MenuPageItem> pageItems = menuPageItemDAO.getPageItemFor(menuItem, s);
    Iterator localIterator; if (pageItems != null)
      for (localIterator = pageItems.iterator(); localIterator.hasNext();) { menuPageItem = (MenuPageItem)localIterator.next();
        menuPageItem.setMenuItem(menuItem);
        menuPageItemDAO.saveOrUpdate(menuPageItem, s);
      }
    MenuPageItem menuPageItem;
    Object comboItems = ComboItemDAO.getInstance().getByMenuItem(menuItem.getId());
    for (ComboItem comboItem : (List)comboItems) {
      comboItem.setMenuItem(menuItem);
      ComboItemDAO.getInstance().update(comboItem);
    }
  }
  
  public void saveAll(List<MenuItem> items) {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      
      for (MenuItem menuItem : items) {
        session.merge(menuItem);
      }
      tx.commit();
    } finally {
      closeSession(session);
    }
  }
  
  private void saveInventoryStockStatus(MenuItem menuItem, Session session) {
    if (!menuItem.isInventoryItem().booleanValue())
      return;
    MenuItemInventoryStatus stockStatus = menuItem.getStockStatus();
    if (stockStatus == null) {
      stockStatus = new MenuItemInventoryStatus();
      menuItem.setStockStatus(stockStatus);
    }
    if (stockStatus.getId() == null) {
      stockStatus.setId(menuItem.getId());
      MenuItemInventoryStatusDAO.getInstance().save(stockStatus, session);
    }
  }
  
  public void saveOrUpdate(MenuItem menuItem) throws HibernateException
  {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      if (menuItem.getId() == null) {
        save(menuItem, session);
      }
      else {
        update(menuItem, session);
      }
      
      if (!menuItem.isInventoryItem().booleanValue()) {
        tx.commit();
        return;
      }
      
      MenuItemInventoryStatus stockStatus = menuItem.getStockStatus();
      if (stockStatus == null) {
        stockStatus = new MenuItemInventoryStatus();
        stockStatus.setId(menuItem.getId());
        menuItem.setStockStatus(stockStatus);
        MenuItemInventoryStatusDAO.getInstance().save(stockStatus, session);
      }
      
      tx.commit();
    } catch (Exception e) {
      try {
        tx.rollback();
      }
      catch (Exception localException1) {}
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  public void initializeUnits(MenuItem menuItem) {
    Session session = null;
    try {
      session = createNewSession();
      session.refresh(menuItem);
      if (!Hibernate.isInitialized(menuItem.getStockUnits())) {
        Hibernate.initialize(menuItem.getStockUnits());
      }
      
      closeSession(session); } finally { closeSession(session);
    }
  }
  
  public int getRowCount(String searchString) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      
      criteria.setProjection(Projections.rowCount());
      
      if (net.authorize.util.StringUtils.isNotEmpty(searchString)) {
        criteria.add(Restrictions.ilike(MenuItem.PROP_NAME, searchString, MatchMode.START));
      }
      
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        return rowCount.intValue();
      }
    }
    finally {
      closeSession(session);
    }
    return 0;
  }
  
  public void loadInventoryItems(String searchString, BeanTableModel<MenuItem> listModel) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.addOrder(Order.asc(MenuItem.PROP_NAME));
      
      if (net.authorize.util.StringUtils.isNotEmpty(searchString)) {
        criteria.add(Restrictions.ilike(MenuItem.PROP_NAME, searchString, MatchMode.START));
      }
      
      criteria.setFirstResult(listModel.getCurrentRowIndex());
      criteria.setMaxResults(listModel.getPageSize());
      listModel.setRows(criteria.list());
    } finally {
      closeSession(session);
    }
  }
  


















  public int rowCount(Boolean menuItem, MenuGroup menuGroup, String itemName, Object selectedType, boolean variant)
  {
    return rowCount(menuItem, menuGroup, itemName, selectedType, variant, null);
  }
  
  public int rowCount(Boolean menuItem, MenuGroup menuGroup, String itemName, Object selectedType, boolean variant, Boolean pizzaType) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      criteria.setProjection(Projections.rowCount());
      if (!menuItem.booleanValue()) {
        criteria.add(Restrictions.eq(MenuItem.PROP_INVENTORY_ITEM, Boolean.TRUE));
      }
      if ((pizzaType != null) && (pizzaType.booleanValue())) {
        criteria.add(Restrictions.eq(MenuItem.PROP_PIZZA_TYPE, pizzaType));
      }
      if (!variant) {
        criteria.add(Restrictions.or(Restrictions.isNull(MenuItem.PROP_VARIANT), Restrictions.eq(MenuItem.PROP_VARIANT, Boolean.FALSE)));
      } else {
        criteria.add(Restrictions.eq(MenuItem.PROP_VARIANT, Boolean.TRUE));
      }
      if (menuGroup != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_MENU_GROUP_ID, menuGroup.getId()));
      }
      
      if (net.authorize.util.StringUtils.isNotEmpty(itemName)) {
        criteria.add(Restrictions.ilike(MenuItem.PROP_NAME, itemName.trim(), MatchMode.ANYWHERE));
      }
      addOrderTypeFilter(selectedType, session, criteria);
      Number rowCount = (Number)criteria.uniqueResult();
      int i; if (rowCount != null) {
        return rowCount.intValue();
      }
      return 0;
    } finally {
      closeSession(session);
    }
  }
  
  public int rowCount(Boolean menuItem, MenuGroup menuGroup, String itemName) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      criteria.setProjection(Projections.rowCount());
      if (!menuItem.booleanValue()) {
        criteria.add(Restrictions.eq(MenuItem.PROP_INVENTORY_ITEM, Boolean.TRUE));
      }
      if (menuGroup != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_MENU_GROUP_ID, menuGroup.getId()));
      }
      
      if (net.authorize.util.StringUtils.isNotEmpty(itemName)) {
        criteria.add(Restrictions.ilike(MenuItem.PROP_NAME, itemName.trim(), MatchMode.ANYWHERE));
      }
      Number rowCount = (Number)criteria.uniqueResult();
      int i; if (rowCount != null) {
        return rowCount.intValue();
      }
      return 0;
    } finally {
      closeSession(session);
    }
  }
  
  public int rowCount(MenuGroup menuGroup, String itemName, boolean variant, Boolean pizzaType) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      criteria.setProjection(Projections.rowCount());
      
      if ((pizzaType != null) && (pizzaType.booleanValue())) {
        criteria.add(Restrictions.eq(MenuItem.PROP_PIZZA_TYPE, pizzaType));
      }
      if (!variant) {
        criteria.add(Restrictions.or(Restrictions.isNull(MenuItem.PROP_VARIANT), Restrictions.eq(MenuItem.PROP_VARIANT, Boolean.FALSE)));
      } else {
        criteria.add(Restrictions.eq(MenuItem.PROP_VARIANT, Boolean.TRUE));
      }
      if (menuGroup != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_MENU_GROUP_ID, menuGroup.getId()));
      }
      
      if (net.authorize.util.StringUtils.isNotEmpty(itemName)) {
        criteria.add(Restrictions.ilike(MenuItem.PROP_NAME, itemName.trim(), MatchMode.ANYWHERE));
      }
      Number rowCount = (Number)criteria.uniqueResult();
      int i; if (rowCount != null) {
        return rowCount.intValue();
      }
      return 0;
    } finally {
      closeSession(session);
    }
  }
  
  public MenuItem loadInitialized(String menuItemId) throws HibernateException {
    MenuItem menuItem = super.get(menuItemId);
    initialize(menuItem);
    return menuItem;
  }
  
  public MenuItem getInitialized(String menuItemId) {
    if (org.apache.commons.lang.StringUtils.isEmpty(menuItemId)) {
      return null;
    }
    Session session = null;
    try
    {
      session = createNewSession();
      MenuItem menuItem = get(menuItemId, session);
      initialize(menuItem, session);
      return menuItem;
    } finally {
      closeSession(session);
    }
  }
  
  public void initialize(MenuItem menuItem) {
    if ((menuItem == null) || (menuItem.getId() == null)) {
      return;
    }
    Session session = null;
    try
    {
      session = createNewSession();
      session.refresh(menuItem);
      initialize(menuItem, session);
      
      closeSession(session); } finally { closeSession(session);
    }
  }
  
  public void initializeVariants(MenuItem menuItem) {
    if ((menuItem == null) || (menuItem.getId() == null)) {
      return;
    }
    



    Session session = null;
    try
    {
      session = createNewSession();
      session.update(menuItem);
      Hibernate.initialize(menuItem.getVariants());
      Hibernate.initialize(menuItem.getAttributes());
      
      closeSession(session); } finally { closeSession(session);
    }
  }
  
  public void initialize(MenuItem menuItem, Session session) {
    Hibernate.initialize(menuItem.getStockUnits());
    Hibernate.initialize(menuItem.getMenuItemModiferSpecs());
    Hibernate.initialize(menuItem.getPizzaPriceList());
    Hibernate.initialize(menuItem.getDiscounts());
    Hibernate.initialize(menuItem.getComboGroups());
    Hibernate.initialize(menuItem.getComboItems());
    Hibernate.initialize(menuItem.getVariants());
    List<MenuItem> variants = menuItem.getVariants();
    Iterator localIterator; if (variants != null)
      for (localIterator = variants.iterator(); localIterator.hasNext();) { menuItem2 = (MenuItem)localIterator.next();
        Hibernate.initialize(menuItem2.getAttributes());
      }
    MenuItem menuItem2;
    Object modiferSpecs = menuItem.getMenuItemModiferSpecs();
    if (modiferSpecs != null) {
      for (MenuItemModifierSpec modifierSpec : (List)modiferSpecs) {
        Hibernate.initialize(modifierSpec.getDefaultModifierList());
        Hibernate.initialize(modifierSpec.getModifierPages());
      }
    }
  }
  
  public List<MenuItem> findByParent(Terminal terminal, MenuGroup menuGroup, boolean includeInvisibleItems) throws PosException
  {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      
      if (menuGroup != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_MENU_GROUP_ID, menuGroup.getId()));
      }
      
      criteria.addOrder(Order.asc(MenuItem.PROP_SORT_ORDER));
      criteria.addOrder(Order.asc(MenuItem.PROP_NAME));
      
      if (!includeInvisibleItems) {
        criteria.add(Restrictions.eq(MenuItem.PROP_VISIBLE, Boolean.TRUE));
      }
      
      return criteria.list();
    } catch (Exception e) {
      PosLog.error(getClass(), e);
      throw new PosException("");
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public List<MenuItem> getVariants(MenuItem parentItem) throws PosException {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      if (parentItem != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_PARENT_MENU_ITEM_ID, parentItem.getId()));
      }
      criteria.add(Restrictions.eq(MenuItem.PROP_VISIBLE, Boolean.TRUE));
      criteria.addOrder(Order.asc(MenuItem.PROP_SORT_ORDER));
      criteria.addOrder(Order.asc(MenuItem.PROP_NAME));
      
      return criteria.list();
    } catch (Exception e) {
      PosLog.error(getClass(), e);
      throw new PosException("");
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public int getRowCount(Terminal terminal, MenuGroup menuGroup, Object selectedOrderType) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      




      addOrderTypeFilter(selectedOrderType, session, criteria);
      
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        return rowCount.intValue();
      }
    }
    finally {
      closeSession(session);
    }
    return 0;
  }
  
  public void loadItems(Terminal terminal, MenuGroup menuGroup, Object selectedOrderType, boolean includeInvisibleItems, PaginatedListModel listModel, boolean variant) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      



      criteria.addOrder(Order.asc(MenuItem.PROP_SORT_ORDER));
      criteria.addOrder(Order.asc(MenuItem.PROP_NAME));
      
      if (!variant) {
        criteria.add(Restrictions.or(Restrictions.isNull(MenuItem.PROP_VARIANT), Restrictions.eq(MenuItem.PROP_VARIANT, Boolean.FALSE)));
      } else {
        criteria.add(Restrictions.eq(MenuItem.PROP_VARIANT, Boolean.TRUE));
      }
      if (!includeInvisibleItems) {
        criteria.add(Restrictions.eq(MenuItem.PROP_VISIBLE, Boolean.TRUE));
      }
      
      addOrderTypeFilter(selectedOrderType, session, criteria);
      criteria.setFirstResult(listModel.getCurrentRowIndex());
      criteria.setMaxResults(listModel.getPageSize());
      listModel.setData(criteria.list());
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuItemModifierSpec> findModifierGroups(MenuItem item) throws PosException {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(MenuItem.PROP_ID, item.getId()));
      MenuItem newItem = (MenuItem)criteria.uniqueResult();
      Hibernate.initialize(newItem.getMenuItemModiferSpecs());
      
      return newItem.getMenuItemModiferSpecs();
    } catch (Exception e) {
      throw new PosException("");
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public List<MenuItem> getMenuItems(String itemName, MenuGroup menuGroup, Object selectedType, String type) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = getSession();
      criteria = session.createCriteria(MenuItem.class);
      
      if (net.authorize.util.StringUtils.isNotEmpty(itemName)) {
        criteria.add(Restrictions.ilike(MenuItem.PROP_NAME, itemName.trim(), MatchMode.ANYWHERE));
      }
      
      if (type.equals("InventoryItem")) {
        criteria.add(Restrictions.eq(MenuItem.PROP_INVENTORY_ITEM, Boolean.valueOf(true)));
      }
      
      addOrderTypeFilter(selectedType, session, criteria);
      return criteria.list();
    }
    finally {
      closeSession(session);
    }
  }
  
  public List<MenuItem> getPizzaItems(String itemName, MenuGroup menuGroup, Object selectedType) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      criteria.add(Restrictions.eq(MenuItem.PROP_PIZZA_TYPE, Boolean.valueOf(true)));
      
      if (net.authorize.util.StringUtils.isNotEmpty(itemName)) {
        criteria.add(Restrictions.ilike(MenuItem.PROP_NAME, itemName.trim(), MatchMode.ANYWHERE));
      }
      
      addOrderTypeFilter(selectedType, session, criteria);
      
      return criteria.list();
    }
    finally {
      closeSession(session);
    }
  }
  












  public void releaseParent(List<MenuItem> menuItemList) {}
  












  public void releaseParentAndDelete(MenuItem item)
  {
    if (item == null) {
      return;
    }
    
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      String queryStringForDiscount = "delete from MENUITEM_DISCOUNT where MENUITEM_ID='%s'";
      queryStringForDiscount = String.format(queryStringForDiscount, new Object[] { item.getId() });
      Query query = session.createSQLQuery(queryStringForDiscount);
      query.executeUpdate();
      
      String queryString = "delete from MENU_PAGE_ITEM where MENU_ITEM_ID='%s'";
      queryString = String.format(queryString, new Object[] { item.getId() });
      Query queryForPageItem = session.createSQLQuery(queryString);
      queryForPageItem.executeUpdate();
      
      String queryStringReleaseRecipe = "delete from RECIPE_TABLE where MENU_ITEM_ID='%s'";
      queryStringReleaseRecipe = String.format(queryStringReleaseRecipe, new Object[] { item.getId() });
      Query queryReleaseRecipe = session.createSQLQuery(queryStringReleaseRecipe);
      queryReleaseRecipe.executeUpdate();
      
      session.delete(item);
      
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  public MenuItem getMenuItemByBarcodeOrSKU(String barcode) {
    MenuItem menuItem = getMenuItemByBarcode(barcode);
    if (menuItem == null) {
      menuItem = getMenuItemBySKU(barcode);
    }
    return menuItem;
  }
  
  public MenuItem getMenuItemByBarcode(String barcode) {
    return getMenuItemByBarcode(barcode, true);
  }
  
  public MenuItem getMenuItemByBarcode(String barcode, boolean showVisibleItems) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      if (showVisibleItems) {
        criteria.add(Restrictions.eq(MenuItem.PROP_VISIBLE, Boolean.valueOf(showVisibleItems)));
      }
      criteria.add(Restrictions.like(MenuItem.PROP_BARCODE, barcode));
      List<MenuItem> result = criteria.list();
      MenuItem localMenuItem; if ((result == null) || (result.isEmpty())) {
        return null;
      }
      return (MenuItem)result.get(0);
    } finally {
      closeSession(session);
    }
  }
  
  public MenuItem getMenuItemBySKU(String skuNo) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      criteria.add(Restrictions.eq(MenuItem.PROP_VISIBLE, Boolean.TRUE));
      criteria.add(Restrictions.like(MenuItem.PROP_SKU, skuNo));
      List<MenuItem> result = criteria.list();
      MenuItem localMenuItem; if ((result == null) || (result.isEmpty())) {
        return null;
      }
      return (MenuItem)result.get(0);
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuItem> getMenuItemByName(String itemName) {
    return getMenuItemByName(itemName, true);
  }
  
  public List<MenuItem> getMenuItemByName(String itemName, boolean showVisibleItems) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      if (showVisibleItems) {
        criteria.add(Restrictions.eq(MenuItem.PROP_VISIBLE, Boolean.valueOf(showVisibleItems)));
      }
      criteria.add(Restrictions.ilike(MenuItem.PROP_NAME, itemName, MatchMode.ANYWHERE));
      List<MenuItem> result = criteria.list();
      List<MenuItem> localList1; if ((result == null) || (result.isEmpty())) {
        return null;
      }
      return result;
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuItem> getMenuItemByName(MenuGroup menuGroup, String itemName) {
    return getMenuItemByName(menuGroup, itemName, true);
  }
  
  public List<MenuItem> getMenuItemByName(MenuGroup menuGroup, String itemName, boolean showVisibleItems) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      if (showVisibleItems) {
        criteria.add(Restrictions.eq(MenuItem.PROP_VISIBLE, Boolean.TRUE));
      }
      criteria.add(Restrictions.ilike(MenuItem.PROP_NAME, itemName, MatchMode.ANYWHERE));
      if (menuGroup != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_MENU_GROUP_ID, menuGroup.getId()));
      }
      List<MenuItem> result = criteria.list();
      List<MenuItem> localList1; if ((result == null) || (result.isEmpty())) {
        return null;
      }
      return result;
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuItem> getMenuItems(String sku, String name, String barcode, ReportGroup reportGroup) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      
      if (net.authorize.util.StringUtils.isNotEmpty(barcode))
        criteria.add(Restrictions.eq(MenuItem.PROP_BARCODE, barcode));
      if (net.authorize.util.StringUtils.isNotEmpty(sku))
        criteria.add(Restrictions.eq(MenuItem.PROP_SKU, sku));
      if (net.authorize.util.StringUtils.isNotEmpty(name)) {
        criteria.add(Restrictions.ilike(MenuItem.PROP_NAME, name, MatchMode.START));
      }
      if (reportGroup != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_REPORT_GROUP_ID, reportGroup.getId()));
      }
      
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuItem> findMenuItemsForStockCount(String searchString, MenuGroup group, InventoryVendor vendor, InventoryLocation location) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      List<String> commonMenuItemsId = null;
      if (vendor != null) {
        criteria = session.createCriteria(InventoryVendorItems.class);
        criteria.add(Restrictions.eq(InventoryVendorItems.PROP_VENDOR, vendor));
        criteria.createAlias(InventoryVendorItems.PROP_ITEM, "item");
        criteria.setProjection(Projections.property("item.id"));
        commonMenuItemsId = criteria.list();
      }
      
      if (location != null) {
        criteria = session.createCriteria(InventoryStock.class);
        criteria.add(Restrictions.eq(InventoryStock.PROP_LOCATION_ID, location.getId()));
        criteria.setProjection(Projections.property(InventoryStock.PROP_MENU_ITEM_ID));
        if (commonMenuItemsId == null) {
          commonMenuItemsId = criteria.list();
        }
        else {
          commonMenuItemsId.retainAll(criteria.list());
        }
      }
      
      criteria = session.createCriteria(MenuItem.class);
      criteria.add(Restrictions.eq(MenuItem.PROP_INVENTORY_ITEM, Boolean.TRUE));
      criteria.add(Restrictions.eq(MenuItem.PROP_HAS_VARIANT, Boolean.FALSE));
      
      if (net.authorize.util.StringUtils.isNotEmpty(searchString)) {
        criteria.add(Restrictions.or(Restrictions.eq(MenuItem.PROP_BARCODE, searchString), Restrictions.or(Restrictions.eq(MenuItem.PROP_SKU, searchString), Restrictions.ilike(MenuItem.PROP_NAME, searchString, MatchMode.START))));
      }
      
      if (group != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_MENU_GROUP_ID, group.getId()));
      }
      
      if ((commonMenuItemsId != null) && (!commonMenuItemsId.isEmpty())) {
        criteria.add(Restrictions.in(MenuItem.PROP_ID, commonMenuItemsId));
      }
      
      criteria.setMaxResults(200);
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuItem> getPizzaItems() {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      criteria.add(Restrictions.eq(MenuItem.PROP_PIZZA_TYPE, Boolean.valueOf(true)));
      
      List<MenuItem> result = criteria.list();
      
      return result;
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuItem> getMenuItems() {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      criteria.add(Restrictions.or(Restrictions.eq(MenuItem.PROP_PIZZA_TYPE, Boolean.valueOf(false)), Restrictions.isNull(MenuItem.PROP_PIZZA_TYPE)));
      
      List<MenuItem> result = criteria.list();
      
      return result;
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuItem> getInventortItems() {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      criteria.add(Restrictions.eq(MenuItem.PROP_INVENTORY_ITEM, Boolean.valueOf(true)));
      
      List<MenuItem> result = criteria.list();
      
      return result;
    } finally {
      closeSession(session);
    }
  }
  
  public MenuItem getReplenishedMenuItem(Integer id, Session session) {
    Criteria criteria = null;
    




    criteria = session.createCriteria(MenuItem.class);
    criteria.add(Restrictions.eq(MenuItem.PROP_ID, id));
    MenuItem menuItem = (MenuItem)criteria.uniqueResult();
    
    Double reorderLevel = Double.valueOf(0.0D);
    if (menuItem != null) {
      reorderLevel = menuItem.getReorderLevel();
    }
    if (reorderLevel.doubleValue() == 0.0D) {
      return null;
    }
    criteria = session.createCriteria(MenuItemInventoryStatus.class);
    criteria.add(Restrictions.eq(MenuItemInventoryStatus.PROP_ID, id));
    criteria.add(Restrictions.lt(MenuItemInventoryStatus.PROP_AVAILABLE_UNIT, reorderLevel));
    MenuItemInventoryStatus inventoryStatus = (MenuItemInventoryStatus)criteria.uniqueResult();
    if (inventoryStatus == null)
      return null;
    menuItem.setStockStatus(inventoryStatus);
    return menuItem;
  }
  
  public List<MenuItem> getReOrderedMenuItems() {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass(), "item");
      criteria.addOrder(Order.asc(MenuItem.PROP_ID));
      

      criteria.add(Subqueries.exists(getDetachedCriteriaForStockStatus("item.id")));
      criteria.add(Restrictions.eq(MenuItem.PROP_INVENTORY_ITEM, Boolean.valueOf(true)));
      List list = criteria.list();
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  private DetachedCriteria getDetachedCriteriaForStockStatus(String propItemId)
  {
    DetachedCriteria stockStatusSubCriteria = DetachedCriteria.forClass(MenuItemInventoryStatus.class, "status").setProjection(Projections.property(MenuItemInventoryStatus.PROP_ID)).add(Restrictions.eqProperty(MenuItemInventoryStatus.PROP_ID, propItemId)).add(Restrictions.or(Restrictions.isNull("status." + MenuItemInventoryStatus.PROP_UNIT_ON_HAND), Restrictions.leProperty("status." + MenuItemInventoryStatus.PROP_UNIT_ON_HAND, "item." + MenuItem.PROP_REORDER_LEVEL)));
    return stockStatusSubCriteria;
  }
  
  public int rowReOrderedItemCount(MenuGroup menuGroup, String itemName, boolean variant, Boolean pizzaType) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class, "item");
      criteria.setProjection(Projections.rowCount());
      

      criteria.add(Subqueries.exists(getDetachedCriteriaForStockStatus("item.id")));
      criteria.add(Restrictions.eq(MenuItem.PROP_INVENTORY_ITEM, Boolean.valueOf(true)));
      
      if ((pizzaType != null) && (pizzaType.booleanValue())) {
        criteria.add(Restrictions.eq(MenuItem.PROP_PIZZA_TYPE, pizzaType));
      }
      if (!variant) {
        criteria.add(Restrictions.or(Restrictions.isNull(MenuItem.PROP_VARIANT), Restrictions.eq(MenuItem.PROP_VARIANT, Boolean.FALSE)));
      } else {
        criteria.add(Restrictions.eq(MenuItem.PROP_VARIANT, Boolean.TRUE));
      }
      if (menuGroup != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_MENU_GROUP_ID, menuGroup.getId()));
      }
      
      if (net.authorize.util.StringUtils.isNotEmpty(itemName)) {
        criteria.add(Restrictions.ilike(MenuItem.PROP_NAME, itemName.trim(), MatchMode.ANYWHERE));
      }
      
      Number rowCount = (Number)criteria.uniqueResult();
      int i; if (rowCount != null) {
        return rowCount.intValue();
      }
      return 0;
    } finally {
      closeSession(session);
    }
  }
  
  public void loadReorderedMenuItems(PaginationSupport model, MenuGroup menuGroup, String itemName, boolean variant, Boolean pizzaType) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class, "item");
      

      criteria.add(Subqueries.exists(getDetachedCriteriaForStockStatus("item.id")));
      criteria.add(Restrictions.eq(MenuItem.PROP_INVENTORY_ITEM, Boolean.valueOf(true)));
      
      if ((pizzaType != null) && (pizzaType.booleanValue())) {
        criteria.add(Restrictions.eq(MenuItem.PROP_PIZZA_TYPE, pizzaType));
      }
      
      if (!variant) {
        criteria.add(Restrictions.or(Restrictions.isNull(MenuItem.PROP_VARIANT), Restrictions.eq(MenuItem.PROP_VARIANT, Boolean.FALSE)));
      } else {
        criteria.add(Restrictions.eq(MenuItem.PROP_VARIANT, Boolean.TRUE));
      }
      if (net.authorize.util.StringUtils.isNotEmpty(itemName)) {
        criteria.add(Restrictions.ilike(MenuItem.PROP_NAME, itemName.trim(), MatchMode.ANYWHERE));
      }
      if (menuGroup != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_MENU_GROUP_ID, menuGroup.getId()));
      }
      
      criteria.addOrder(Order.asc(MenuItem.PROP_ID));
      criteria.setFirstResult(model.getCurrentRowIndex());
      criteria.setMaxResults(model.getPageSize());
      List<MenuItem> result = criteria.list();
      model.setRows(result);
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuItem> getMenuItems(String sku, String name, String barcode, MenuGroup menuGroup, Pagination pagination) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      if (pagination != null) {
        criteria.setFirstResult(pagination.getCurrentRowIndex());
        criteria.setMaxResults(pagination.getPageSize());
      }
      updateCriteria(criteria, sku, name, barcode, menuGroup);
      List list = criteria.list();
      
      criteria = session.createCriteria(getReferenceClass());
      criteria.setProjection(Projections.rowCount());
      updateCriteria(criteria, sku, name, barcode, menuGroup);
      Number uniqueResult; if (pagination != null) {
        uniqueResult = (Number)criteria.uniqueResult();
        pagination.setNumRows(uniqueResult.intValue());
      }
      
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  private void updateCriteria(Criteria criteria, String sku, String name, String barcode, MenuGroup menuGroup) {
    if (net.authorize.util.StringUtils.isNotEmpty(barcode))
      criteria.add(Restrictions.eq(MenuItem.PROP_BARCODE, barcode));
    if (net.authorize.util.StringUtils.isNotEmpty(sku))
      criteria.add(Restrictions.eq(MenuItem.PROP_SKU, sku));
    if (net.authorize.util.StringUtils.isNotEmpty(name))
      criteria.add(Restrictions.ilike(MenuItem.PROP_NAME, name, MatchMode.ANYWHERE));
    if (menuGroup != null) {
      criteria.add(Restrictions.eq(MenuItem.PROP_MENU_GROUP_ID, menuGroup.getId()));
    }
  }
  
  public void loadMenuItems(PaginationSupport model, Boolean menuItem) {
    loadMenuItems(model, menuItem, null, null, null, false);
  }
  
  public void loadMenuItems(PaginationSupport model, Boolean menuItem, MenuGroup menuGroup, String itemName, MenuCategory selectedType, boolean variant) {
    loadMenuItems(model, menuItem, menuGroup, itemName, selectedType, variant, null);
  }
  
  public List<MenuItem> loadInventoryMenuItem() {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      
      criteria.add(Restrictions.eq(MenuItem.PROP_INVENTORY_ITEM, Boolean.TRUE));
      criteria.addOrder(Order.asc(MenuItem.PROP_ID));
      List result = criteria.list();
      return result;
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuItem> loadInventoryMenuItem(String nameOrSku, MenuGroup menuGroup, MenuCategory menuCategory) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      
      criteria.add(Restrictions.eq(MenuItem.PROP_INVENTORY_ITEM, Boolean.TRUE));
      criteria.addOrder(Order.asc(MenuItem.PROP_MENU_GROUP_ID));
      criteria.addOrder(Order.asc(MenuItem.PROP_NAME));
      if (menuGroup != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_MENU_GROUP_ID, menuGroup.getId()));
      }
      if (menuCategory != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_MENU_CATEGORY_ID, menuCategory.getId()));
      }
      if (net.authorize.util.StringUtils.isNotEmpty(nameOrSku)) {
        criteria.add(Restrictions.or(Restrictions.ilike(MenuItem.PROP_NAME, nameOrSku, MatchMode.ANYWHERE), Restrictions.eq(MenuItem.PROP_SKU, nameOrSku)));
      }
      
      List result = criteria.list();
      return result;
    } finally {
      closeSession(session);
    }
  }
  
  public void loadMenuItems(PaginationSupport model, Boolean menuItem, MenuGroup menuGroup, String itemName, MenuCategory menuCategory, boolean variant, Boolean pizzaType) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      if (!menuItem.booleanValue()) {
        criteria.add(Restrictions.eq(MenuItem.PROP_INVENTORY_ITEM, Boolean.TRUE));
      }
      if ((pizzaType != null) && (pizzaType.booleanValue())) {
        criteria.add(Restrictions.eq(MenuItem.PROP_PIZZA_TYPE, pizzaType));
      }
      
      if (!variant) {
        criteria.add(Restrictions.or(Restrictions.isNull(MenuItem.PROP_VARIANT), Restrictions.eq(MenuItem.PROP_VARIANT, Boolean.FALSE)));
      } else {
        criteria.add(Restrictions.eq(MenuItem.PROP_VARIANT, Boolean.TRUE));
      }
      if (net.authorize.util.StringUtils.isNotEmpty(itemName)) {
        criteria.add(Restrictions.ilike(MenuItem.PROP_NAME, itemName.trim(), MatchMode.ANYWHERE));
      }
      if (menuGroup != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_MENU_GROUP_ID, menuGroup.getId()));
      }
      
      if (menuCategory != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_MENU_CATEGORY_ID, menuCategory.getId()));
      }
      
      criteria.setProjection(Projections.rowCount());
      Number uniqueResult = (Number)criteria.uniqueResult();
      model.setNumRows(uniqueResult.intValue());
      criteria.setProjection(null);
      
      criteria.addOrder(Order.asc(MenuItem.PROP_SORT_ORDER));
      criteria.addOrder(Order.asc(MenuItem.PROP_NAME));
      criteria.setFirstResult(model.getCurrentRowIndex());
      criteria.setMaxResults(model.getPageSize());
      List<MenuItem> result = criteria.list();
      model.setRows(result);
    } finally {
      closeSession(session);
    }
  }
  
  private void addOrderTypeFilter(Object selectedType, Session session, Criteria criteria) {
    if ((selectedType instanceof OrderType)) {
      OrderType orderType = (OrderType)selectedType;
      Criteria criteria2 = session.createCriteria(MenuCategory.class);
      criteria2.createAlias("orderTypes", "ot");
      criteria2.add(Restrictions.in("ot." + OrderType.PROP_ID, Arrays.asList(new String[] { orderType.getId() })));
      List<MenuCategory> list = criteria2.list();
      if ((list != null) && (!list.isEmpty())) {
        List<String> idList = new ArrayList();
        for (MenuCategory object : list) {
          idList.add(object.getId());
        }
        criteria.add(Restrictions.in(MenuItem.PROP_MENU_CATEGORY_ID, idList));
      }
    }
  }
  
  public void findByBarcodeOrName(PaginationSupport paginationSupport, Boolean inventoryItemOnly, MenuGroup menuGroup, String itemNameOrBarcode, Boolean variant, Boolean pizzaType, String... fields) {
    findByBarcodeOrName(paginationSupport, inventoryItemOnly, menuGroup, itemNameOrBarcode, variant, pizzaType, true, fields);
  }
  
  public void findByBarcodeOrName(PaginationSupport paginationSupport, Boolean inventoryItemOnly, MenuGroup menuGroup, String itemNameOrBarcode, Boolean variant, Boolean pizzaType, boolean showVisibleItems, String... fields) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      boolean isFoundByBarcode = findByBarcode(paginationSupport, itemNameOrBarcode, session, criteria);
      if (isFoundByBarcode) {
        return;
      }
      
      criteria = session.createCriteria(MenuItem.class);
      criteria.setFirstResult(paginationSupport.getCurrentRowIndex());
      criteria.setMaxResults(paginationSupport.getPageSize());
      
      if (net.authorize.util.StringUtils.isNotEmpty(itemNameOrBarcode)) {
        criteria.add(Restrictions.ilike(MenuItem.PROP_NAME, itemNameOrBarcode.trim(), MatchMode.ANYWHERE));
      }
      if (inventoryItemOnly.booleanValue()) {
        criteria.add(Restrictions.eq(MenuItem.PROP_INVENTORY_ITEM, Boolean.TRUE));
      }
      if ((pizzaType != null) && (pizzaType.booleanValue())) {
        criteria.add(Restrictions.eq(MenuItem.PROP_PIZZA_TYPE, pizzaType));
      }
      
      if (variant != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_VARIANT, variant));
      }
      
      if (menuGroup != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_MENU_GROUP_ID, menuGroup.getId()));
      }
      if (showVisibleItems) {
        criteria.add(Restrictions.eq(MenuItem.PROP_VISIBLE, Boolean.valueOf(showVisibleItems)));
      }
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        paginationSupport.setNumRows(rowCount.intValue());
      }
      
      criteria.setProjection(null);
      criteria.addOrder(Order.asc(MenuItem.PROP_SORT_ORDER));
      criteria.addOrder(Order.asc(MenuItem.PROP_NAME));
      if ((fields != null) && (fields.length > 0)) {
        ProjectionList projectionList = Projections.projectionList();
        for (String field : fields) {
          projectionList.add(Projections.property(field), field);
        }
        criteria.setProjection(projectionList);
        criteria.setResultTransformer(Transformers.aliasToBean(MenuItem.class));
        paginationSupport.setRows(criteria.list());
      }
      else {
        paginationSupport.setRows(criteria.list());
      }
    }
    finally {
      closeSession(session);
    }
  }
  
  private boolean findByBarcode(PaginationSupport model, String itemNameOrBarcode, Session session, Criteria criteria) {
    return findByBarcode(model, itemNameOrBarcode, session, criteria, true);
  }
  
  private boolean findByBarcode(PaginationSupport model, String itemNameOrBarcode, Session session, Criteria criteria, boolean showVisibleItems) {
    if (net.authorize.util.StringUtils.isEmpty(itemNameOrBarcode)) {
      return false;
    }
    criteria = session.createCriteria(MenuItem.class);
    criteria.setFirstResult(model.getCurrentRowIndex());
    criteria.setMaxResults(model.getPageSize());
    criteria.add(Restrictions.eq(MenuItem.PROP_BARCODE, itemNameOrBarcode.trim()));
    if (showVisibleItems) {
      criteria.add(Restrictions.eq(MenuItem.PROP_VISIBLE, Boolean.valueOf(showVisibleItems)));
    }
    criteria.setProjection(Projections.rowCount());
    Number rowCount = (Number)criteria.uniqueResult();
    if (rowCount.intValue() <= 0) {
      return false;
    }
    model.setNumRows(rowCount.intValue());
    criteria.setProjection(null);
    model.setRows(criteria.list());
    return true;
  }
  
  public MenuItem findByName(String menuItemName)
  {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      
      criteria.add(Restrictions.eq(MenuItem.PROP_NAME, menuItemName));
      List result = criteria.list();
      MenuItem localMenuItem; if (result.size() == 0) {
        return null;
      }
      return (MenuItem)result.get(0);
    } finally {
      closeSession(session);
    }
  }
  
  public MenuItem findByBarcode(String barcode, Session session) {
    if (net.authorize.util.StringUtils.isEmpty(barcode)) {
      return null;
    }
    Criteria criteria = session.createCriteria(MenuItem.class);
    criteria.add(Restrictions.eq(MenuItem.PROP_BARCODE, barcode.trim()));
    List list = criteria.list();
    if (list.size() > 0) {
      return (MenuItem)list.get(0);
    }
    
    return null;
  }
  
  public void loadMenuItems(BeanTableModel model, Boolean menuItem, MenuGroup menuGroup, String itemName)
  {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      if (!menuItem.booleanValue()) {
        criteria.add(Restrictions.eq(MenuItem.PROP_INVENTORY_ITEM, Boolean.TRUE));
      }
      if (net.authorize.util.StringUtils.isNotEmpty(itemName)) {
        criteria.add(Restrictions.ilike(MenuItem.PROP_NAME, itemName.trim(), MatchMode.ANYWHERE));
      }
      if (menuGroup != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_MENU_GROUP_ID, menuGroup.getId()));
      }
      
      String modelSortColumn = model.getSortBy();
      if (org.apache.commons.lang.StringUtils.isNotEmpty(modelSortColumn)) {
        criteria.addOrder(model.isAscOrder() ? Order.asc(modelSortColumn) : Order.desc(modelSortColumn));
      }
      else {
        criteria.addOrder(Order.asc(MenuItem.PROP_SORT_ORDER));
        criteria.addOrder(Order.asc(MenuItem.PROP_NAME));
      }
      criteria.setFirstResult(model.getCurrentRowIndex());
      criteria.setMaxResults(model.getPageSize());
      List<MenuItem> result = criteria.list();
      model.setRows(result);
    } finally {
      closeSession(session);
    }
  }
  
  public void loadMenuItems(PaginationSupport model, MenuGroup menuGroup, String itemName, boolean variant, Boolean pizzaType)
  {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      
      if ((pizzaType != null) && (pizzaType.booleanValue())) {
        criteria.add(Restrictions.eq(MenuItem.PROP_PIZZA_TYPE, pizzaType));
      }
      
      if (!variant) {
        criteria.add(Restrictions.or(Restrictions.isNull(MenuItem.PROP_VARIANT), Restrictions.eq(MenuItem.PROP_VARIANT, Boolean.FALSE)));
      } else {
        criteria.add(Restrictions.eq(MenuItem.PROP_VARIANT, Boolean.TRUE));
      }
      if (net.authorize.util.StringUtils.isNotEmpty(itemName)) {
        criteria.add(Restrictions.ilike(MenuItem.PROP_NAME, itemName.trim(), MatchMode.ANYWHERE));
      }
      if (menuGroup != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_MENU_GROUP_ID, menuGroup.getId()));
      }
      
      criteria.addOrder(Order.asc(MenuItem.PROP_ID));
      criteria.setFirstResult(model.getCurrentRowIndex());
      criteria.setMaxResults(model.getPageSize());
      List<MenuItem> result = criteria.list();
      model.setRows(result);
    } finally {
      closeSession(session);
    }
  }
  
  public void loadInventoryItems(PaginationSupport model, MenuGroup menuGroup, String itemName) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      criteria.add(Restrictions.eq(MenuItem.PROP_INVENTORY_ITEM, Boolean.valueOf(true)));
      if (net.authorize.util.StringUtils.isNotEmpty(itemName)) {
        criteria.add(Restrictions.or(new Criterion[] {
          Restrictions.ilike(MenuItem.PROP_NAME, itemName.trim(), MatchMode.ANYWHERE), 
          Restrictions.ilike(MenuItem.PROP_SKU, itemName.trim(), MatchMode.START), 
          Restrictions.ilike(MenuItem.PROP_BARCODE, itemName.trim(), MatchMode.START) }));
      }
      if (menuGroup != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_MENU_GROUP_ID, menuGroup.getId()));
      }
      
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      model.setNumRows(rowCount.intValue());
      
      criteria.setProjection(null);
      criteria.addOrder(Order.asc(MenuItem.PROP_ID));
      criteria.setFirstResult(model.getCurrentRowIndex());
      criteria.setMaxResults(model.getPageSize());
      List<MenuItem> result = criteria.list();
      model.setRows(result);
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuItem> findAllInventoryItems() {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      criteria.add(Restrictions.eq(MenuItem.PROP_INVENTORY_ITEM, Boolean.valueOf(true)));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  











  public void updateStockQuantity(String menuItemId, double unitQuantity, boolean updateAvailableUnit, boolean updateOnHandUnit, Session session)
  {
    MenuItemInventoryStatus status = MenuItemInventoryStatusDAO.getInstance().get(menuItemId, session);
    if (status == null) {
      status = new MenuItemInventoryStatus();
      status.setId(menuItemId);
    }
    if (updateAvailableUnit) {
      status.setAvailableUnit(Double.valueOf(status.getAvailableUnit().doubleValue() + unitQuantity));
    }
    if (updateOnHandUnit) {
      status.setUnitOnHand(Double.valueOf(status.getUnitOnHand().doubleValue() + unitQuantity));
    }
    session.saveOrUpdate(status);
  }
  
  public void updateStockQuantity(String menuItemId, double unitQuantity, double unitCost, boolean updateAvailableUnit, boolean updateOnHandUnit, Session session) {
    String sql = null;
    if ((updateAvailableUnit) && (updateOnHandUnit)) {
      sql = String.format("update MenuItem set availableUnit = (availableUnit + %s),unitOnHand = (unitOnHand + %s),cost = %s where id = '%s'", new Object[] { Double.valueOf(unitQuantity), Double.valueOf(unitQuantity), Double.valueOf(unitCost), menuItemId });
    }
    else if (updateOnHandUnit) {
      sql = String.format("update MenuItem set unitOnHand = (unitOnHand + %s),cost = %s where id = '%s'", new Object[] { Double.valueOf(unitQuantity), Double.valueOf(unitCost), menuItemId });
    }
    else {
      sql = String.format("update MenuItem set availableUnit = (availableUnit + %s),cost = %s where id = '%s'", new Object[] { Double.valueOf(unitQuantity), Double.valueOf(unitCost), menuItemId });
    }
    session.createQuery(sql).executeUpdate();
  }
  
  public boolean existsMenuItem(MenuGroup menuGroup) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      criteria.setProjection(Projections.rowCount());
      if (menuGroup != null) {
        criteria.add(Restrictions.eq(MenuItem.PROP_MENU_GROUP_ID, menuGroup.getId()));
      }
      
      Number rowCount = (Number)criteria.uniqueResult();
      return (rowCount != null) && (rowCount.intValue() > 0);
    } finally {
      closeSession(session);
    }
  }
  
  public void updateLastPurchaseCost(String menuItemId, double cost, boolean updateLastPurchasePrice, Session session)
  {
    String sql = null;
    double avgCost = InventoryTransactionDAO.getInstance().findItemAvgCost(menuItemId, session);
    double actualCost = updateLastPurchasePrice ? cost : avgCost;
    actualCost = NumberUtil.roundToTwoDigit(actualCost);
    sql = String.format("update MenuItem set lastPurchasedCost = %s,avgCost=%s, cost = %s where id = '%s'", new Object[] { Double.valueOf(cost), Double.valueOf(avgCost), Double.valueOf(actualCost), menuItemId });
    
    session.createQuery(sql).executeUpdate();
  }
  
  public void updateLastPurchaseCost(PurchaseOrder order, boolean updateLastPurchasePrice) {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      List<PurchaseOrderItem> orderItems = order.getOrderItems();
      for (PurchaseOrderItem purchaseOrderItem : orderItems) {
        MenuItem menuItem = purchaseOrderItem.getMenuItem();
        double baseUnitQuantity = menuItem.getBaseUnitQuantity(purchaseOrderItem.getItemUnitName());
        double unitPrice = NumberUtil.roundToTwoDigit(purchaseOrderItem.getUnitPrice().doubleValue() / baseUnitQuantity);
        updateLastPurchaseCost(menuItem.getId(), unitPrice, updateLastPurchasePrice, session);
      }
      
      session.saveOrUpdate(order);
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      throwException(e);
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public MenuItem getMenuItemWithFields(String menuItemId, String... fields)
    throws Exception
  {
    Session session = null;
    try {
      session = createNewSession();
      return getMenuItemWithFields(session, menuItemId, fields);
    } finally {
      closeSession(session);
    }
  }
  
  public MenuItem getMenuItemWithFields(Session session, String menuItemId, String... fields) throws Exception {
    Criteria criteria = null;
    criteria = session.createCriteria(MenuItem.class);
    criteria.add(Restrictions.eq(MenuItem.PROP_ID, menuItemId));
    
    ProjectionList projectionList = Projections.projectionList();
    projectionList.add(Projections.property(MenuItem.PROP_ID), MenuItem.PROP_ID);
    for (String field : fields) {
      projectionList.add(Projections.property(field), field);
    }
    criteria.setProjection(projectionList);
    criteria.setResultTransformer(Transformers.aliasToBean(MenuItem.class));
    MenuItem menuItem = (MenuItem)criteria.uniqueResult();
    if (menuItem != null) {
      Hibernate.initialize(menuItem.getStockUnits());
    }
    
    return menuItem;
  }
  
  public void loadMenuItems(PaginationSupport model, String searchItem) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuItem.class);
      
      if (net.authorize.util.StringUtils.isNotEmpty(searchItem)) {
        criteria.add(Restrictions.ilike(MenuItem.PROP_NAME, searchItem.trim(), MatchMode.ANYWHERE));
      }
      criteria.add(Restrictions.eq(MenuItem.PROP_HAS_VARIANT, Boolean.valueOf(false)));
      criteria.setProjection(Projections.rowCount());
      Number uniqueResult = (Number)criteria.uniqueResult();
      model.setNumRows(uniqueResult.intValue());
      criteria.setProjection(null);
      
      criteria.addOrder(Order.asc(MenuItem.PROP_SORT_ORDER));
      criteria.addOrder(Order.asc(MenuItem.PROP_NAME));
      criteria.setFirstResult(model.getCurrentRowIndex());
      criteria.setMaxResults(model.getPageSize());
      List<MenuItem> result = criteria.list();
      model.setRows(result);
    } finally {
      closeSession(session);
    }
  }
}
