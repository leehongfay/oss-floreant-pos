package com.floreantpos.model.dao;

import com.floreantpos.PosLog;
import com.floreantpos.model.Multiplier;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;




public class MultiplierDAO
  extends BaseMultiplierDAO
{
  public MultiplierDAO() {}
  
  public List<Multiplier> findAll()
  {
    try
    {
      Session session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.addOrder(Order.asc(Multiplier.PROP_SORT_ORDER));
      return criteria.list();
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    }
    return null;
  }
  
  public void saveOrUpdateMultipliers(List<Multiplier> items) {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      for (Iterator iterator = items.iterator(); iterator.hasNext();) {
        Multiplier multiplier = (Multiplier)iterator.next();
        session.saveOrUpdate(multiplier);
      }
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      PosLog.error(getClass(), e);
    } finally {
      session.close();
    }
  }
  
  public Multiplier getDefaultMutltiplier() {
    try {
      Session session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Multiplier.PROP_DEFAULT_MULTIPLIER, Boolean.TRUE));
      return (Multiplier)criteria.uniqueResult();
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    }
    return null;
  }
  
  public boolean nameExists(String name) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Multiplier.PROP_ID, name).ignoreCase());
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      boolean bool;
      if (rowCount == null) {
        return false;
      }
      return rowCount.intValue() > 0;
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public void deleteMultiplier(Multiplier multiplier) {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      String sqlDeleteItem = "delete from MODIFIER_MULTIPLIER_PRICE where MULTIPLIER_ID='%s'";
      sqlDeleteItem = String.format(sqlDeleteItem, new Object[] { multiplier.getId() });
      Query query = session.createSQLQuery(sqlDeleteItem);
      query.executeUpdate();
      session.delete(multiplier);
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      PosLog.error(getClass(), e);
    } finally {
      session.close();
    }
  }
}
