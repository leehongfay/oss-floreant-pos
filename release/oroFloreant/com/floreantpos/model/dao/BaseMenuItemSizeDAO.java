package com.floreantpos.model.dao;

import com.floreantpos.model.MenuItemSize;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseMenuItemSizeDAO
  extends _RootDAO
{
  public static MenuItemSizeDAO instance;
  
  public BaseMenuItemSizeDAO() {}
  
  public static MenuItemSizeDAO getInstance()
  {
    if (null == instance) instance = new MenuItemSizeDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return MenuItemSize.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public MenuItemSize cast(Object object)
  {
    return (MenuItemSize)object;
  }
  
  public MenuItemSize get(String key) throws HibernateException
  {
    return (MenuItemSize)get(getReferenceClass(), key);
  }
  
  public MenuItemSize get(String key, Session s) throws HibernateException
  {
    return (MenuItemSize)get(getReferenceClass(), key, s);
  }
  
  public MenuItemSize load(String key) throws HibernateException
  {
    return (MenuItemSize)load(getReferenceClass(), key);
  }
  
  public MenuItemSize load(String key, Session s) throws HibernateException
  {
    return (MenuItemSize)load(getReferenceClass(), key, s);
  }
  
  public MenuItemSize loadInitialize(String key, Session s) throws HibernateException
  {
    MenuItemSize obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<MenuItemSize> findAll()
  {
    return super.findAll();
  }
  


  public List<MenuItemSize> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<MenuItemSize> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(MenuItemSize menuItemSize)
    throws HibernateException
  {
    return (String)super.save(menuItemSize);
  }
  







  public String save(MenuItemSize menuItemSize, Session s)
    throws HibernateException
  {
    return (String)save(menuItemSize, s);
  }
  





  public void saveOrUpdate(MenuItemSize menuItemSize)
    throws HibernateException
  {
    saveOrUpdate(menuItemSize);
  }
  







  public void saveOrUpdate(MenuItemSize menuItemSize, Session s)
    throws HibernateException
  {
    saveOrUpdate(menuItemSize, s);
  }
  




  public void update(MenuItemSize menuItemSize)
    throws HibernateException
  {
    update(menuItemSize);
  }
  






  public void update(MenuItemSize menuItemSize, Session s)
    throws HibernateException
  {
    update(menuItemSize, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(MenuItemSize menuItemSize)
    throws HibernateException
  {
    delete(menuItemSize);
  }
  






  public void delete(MenuItemSize menuItemSize, Session s)
    throws HibernateException
  {
    delete(menuItemSize, s);
  }
  









  public void refresh(MenuItemSize menuItemSize, Session s)
    throws HibernateException
  {
    refresh(menuItemSize, s);
  }
}
