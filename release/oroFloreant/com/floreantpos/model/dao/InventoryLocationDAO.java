package com.floreantpos.model.dao;

import com.floreantpos.PosLog;
import com.floreantpos.model.InventoryLocation;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;






















public class InventoryLocationDAO
  extends BaseInventoryLocationDAO
{
  public InventoryLocationDAO() {}
  
  public InventoryLocation initialize(InventoryLocation inventoryLocation)
  {
    if ((inventoryLocation == null) || (inventoryLocation.getId() == null)) {
      return inventoryLocation;
    }
    Session session = null;
    try
    {
      session = createNewSession();
      session.refresh(inventoryLocation);
      
      Hibernate.initialize(InventoryLocation.class);
      

      return inventoryLocation;
    } finally {
      closeSession(session);
    }
  }
  
  public InventoryLocation getRootLocation() {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(InventoryLocation.PROP_ROOT, Boolean.valueOf(true)));
      
      List list = criteria.list();
      InventoryLocation localInventoryLocation;
      if ((list != null) && (list.size() > 0)) {
        return (InventoryLocation)list.get(0);
      }
      
      return null;
    }
    finally {
      closeSession(session);
    }
  }
  
  public List<InventoryLocation> getRootLocations() {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(InventoryLocation.PROP_ROOT, Boolean.valueOf(true)));
      
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public InventoryLocation getDefaultInInventoryLocation() {
    Session session = null;
    Criteria criteria = null;
    try {
      session = getSession();
      criteria = session.createCriteria(InventoryLocation.class);
      
      criteria.add(Restrictions.eq(InventoryLocation.PROP_DEFAULT_IN_LOCATION, Boolean.valueOf(true)));
      
      List<InventoryLocation> result = criteria.list();
      InventoryLocation localInventoryLocation; if ((result == null) || (result.isEmpty())) {
        return null;
      }
      return (InventoryLocation)result.get(0);
    }
    catch (Exception e)
    {
      PosLog.info(getClass(), "" + e);
    } finally {
      closeSession(session);
    }
    return null;
  }
  
  public InventoryLocation getDefaultOutInventoryLocation() {
    Session session = null;
    Criteria criteria = null;
    try {
      session = getSession();
      criteria = session.createCriteria(InventoryLocation.class);
      
      criteria.add(Restrictions.eq(InventoryLocation.PROP_DEFAULT_OUT_LOCATION, Boolean.valueOf(true)));
      
      List<InventoryLocation> result = criteria.list();
      InventoryLocation localInventoryLocation; if ((result == null) || (result.isEmpty())) {
        return null;
      }
      return (InventoryLocation)result.get(0);
    }
    catch (Exception e)
    {
      PosLog.info(getClass(), "" + e);
    } finally {
      closeSession(session);
    }
    return null;
  }
  
  public InventoryLocation getDefaultOutInventoryLocation(Session session) {
    Criteria criteria = session.createCriteria(InventoryLocation.class);
    criteria.add(Restrictions.eq(InventoryLocation.PROP_DEFAULT_OUT_LOCATION, Boolean.valueOf(true)));
    List<InventoryLocation> result = criteria.list();
    if ((result == null) || (result.isEmpty())) {
      criteria = session.createCriteria(InventoryLocation.class);
      criteria.setMaxResults(1);
      return (InventoryLocation)criteria.uniqueResult();
    }
    return (InventoryLocation)result.get(0);
  }
  
  public InventoryLocation getDefaultInInventoryLocation(Session session) {
    Criteria criteria = session.createCriteria(InventoryLocation.class);
    criteria.add(Restrictions.eq(InventoryLocation.PROP_DEFAULT_IN_LOCATION, Boolean.valueOf(true)));
    List<InventoryLocation> result = criteria.list();
    if ((result == null) || (result.isEmpty())) {
      criteria = session.createCriteria(InventoryLocation.class);
      criteria.setMaxResults(1);
      return (InventoryLocation)criteria.uniqueResult();
    }
    return (InventoryLocation)result.get(0);
  }
}
