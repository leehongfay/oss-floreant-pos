package com.floreantpos.model.dao;

import com.floreantpos.model.TicketItemSeat;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseTicketItemSeatDAO
  extends _RootDAO
{
  public static TicketItemSeatDAO instance;
  
  public BaseTicketItemSeatDAO() {}
  
  public static TicketItemSeatDAO getInstance()
  {
    if (null == instance) instance = new TicketItemSeatDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return TicketItemSeat.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public TicketItemSeat cast(Object object)
  {
    return (TicketItemSeat)object;
  }
  
  public TicketItemSeat get(String key) throws HibernateException
  {
    return (TicketItemSeat)get(getReferenceClass(), key);
  }
  
  public TicketItemSeat get(String key, Session s) throws HibernateException
  {
    return (TicketItemSeat)get(getReferenceClass(), key, s);
  }
  
  public TicketItemSeat load(String key) throws HibernateException
  {
    return (TicketItemSeat)load(getReferenceClass(), key);
  }
  
  public TicketItemSeat load(String key, Session s) throws HibernateException
  {
    return (TicketItemSeat)load(getReferenceClass(), key, s);
  }
  
  public TicketItemSeat loadInitialize(String key, Session s) throws HibernateException
  {
    TicketItemSeat obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<TicketItemSeat> findAll()
  {
    return super.findAll();
  }
  


  public List<TicketItemSeat> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<TicketItemSeat> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(TicketItemSeat ticketItemSeat)
    throws HibernateException
  {
    return (String)super.save(ticketItemSeat);
  }
  







  public String save(TicketItemSeat ticketItemSeat, Session s)
    throws HibernateException
  {
    return (String)save(ticketItemSeat, s);
  }
  





  public void saveOrUpdate(TicketItemSeat ticketItemSeat)
    throws HibernateException
  {
    saveOrUpdate(ticketItemSeat);
  }
  







  public void saveOrUpdate(TicketItemSeat ticketItemSeat, Session s)
    throws HibernateException
  {
    saveOrUpdate(ticketItemSeat, s);
  }
  




  public void update(TicketItemSeat ticketItemSeat)
    throws HibernateException
  {
    update(ticketItemSeat);
  }
  






  public void update(TicketItemSeat ticketItemSeat, Session s)
    throws HibernateException
  {
    update(ticketItemSeat, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(TicketItemSeat ticketItemSeat)
    throws HibernateException
  {
    delete(ticketItemSeat);
  }
  






  public void delete(TicketItemSeat ticketItemSeat, Session s)
    throws HibernateException
  {
    delete(ticketItemSeat, s);
  }
  









  public void refresh(TicketItemSeat ticketItemSeat, Session s)
    throws HibernateException
  {
    refresh(ticketItemSeat, s);
  }
}
