package com.floreantpos.model.dao;

import com.floreantpos.model.MenuModifier;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PaginatedListModel;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;






















public class MenuModifierDAO
  extends BaseMenuModifierDAO
{
  public MenuModifierDAO() {}
  
  public void initialize(MenuModifier modifier)
  {
    if ((modifier == null) || (modifier.getId() == null)) {
      return;
    }
    if ((Hibernate.isInitialized(modifier.getPizzaModifierPriceList())) && (Hibernate.isInitialized(modifier.getMultiplierPriceList()))) {
      return;
    }
    Session session = null;
    try
    {
      session = createNewSession();
      session.refresh(modifier);
      Hibernate.initialize(modifier.getPizzaModifierPriceList());
      Hibernate.initialize(modifier.getMultiplierPriceList());
      
      closeSession(session); } finally { closeSession(session);
    }
  }
  
  public List<MenuModifier> getModifierList(String tagName) {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      if (StringUtils.isNotEmpty(tagName)) {
        criteria.add(Restrictions.eq(MenuModifier.PROP_TAG, tagName));
      }
      List list = criteria.list();
      
      return list;
    }
    finally {
      closeSession(session);
    }
  }
  
  public List<MenuModifier> getMenuModifiers(String itemName)
  {
    Session session = null;
    Criteria criteria = null;
    try {
      session = getSession();
      criteria = session.createCriteria(MenuModifier.class);
      
      if (StringUtils.isNotEmpty(itemName)) {
        criteria.add(Restrictions.ilike(MenuModifier.PROP_NAME, itemName.trim(), MatchMode.ANYWHERE));
      }
      criteria.add(Restrictions.ne(MenuModifier.PROP_PIZZA_MODIFIER, Boolean.TRUE));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public int getRowCount(String searchString, boolean pizzaModifier) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      
      criteria.setProjection(Projections.rowCount());
      
      if (StringUtils.isNotEmpty(searchString)) {
        criteria.add(Restrictions.ilike(MenuModifier.PROP_NAME, searchString, MatchMode.START));
      }
      if (pizzaModifier) {
        criteria.add(Restrictions.eq(MenuModifier.PROP_PIZZA_MODIFIER, Boolean.TRUE));
      } else {
        criteria.add(Restrictions.ne(MenuModifier.PROP_PIZZA_MODIFIER, Boolean.TRUE));
      }
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        return rowCount.intValue();
      }
    }
    finally {
      closeSession(session);
    }
    return 0;
  }
  
  public void loadItems(String searchString, boolean includeInvisibleItems, PaginatedListModel listModel) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.addOrder(Order.asc(MenuModifier.PROP_SORT_ORDER));
      
      if (StringUtils.isNotEmpty(searchString)) {
        criteria.add(Restrictions.ilike(MenuModifier.PROP_NAME, searchString, MatchMode.START));
      }
      
      if (!includeInvisibleItems) {
        criteria.add(Restrictions.eq(MenuModifier.PROP_ENABLE, Boolean.TRUE));
      }
      criteria.setFirstResult(listModel.getCurrentRowIndex());
      criteria.setMaxResults(listModel.getPageSize());
      listModel.setData(criteria.list());
    } finally {
      closeSession(session);
    }
  }
  
  public void loadItems(String searchString, boolean includeInvisibleItems, boolean pizzaModifier, BeanTableModel<MenuModifier> listModel) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.addOrder(Order.asc(MenuModifier.PROP_SORT_ORDER));
      
      if (StringUtils.isNotEmpty(searchString)) {
        criteria.add(Restrictions.ilike(MenuModifier.PROP_NAME, searchString, MatchMode.START));
      }
      if (pizzaModifier) {
        criteria.add(Restrictions.eq(MenuModifier.PROP_PIZZA_MODIFIER, Boolean.TRUE));
      } else {
        criteria.add(Restrictions.ne(MenuModifier.PROP_PIZZA_MODIFIER, Boolean.TRUE));
      }
      if (!includeInvisibleItems) {
        criteria.add(Restrictions.eq(MenuModifier.PROP_ENABLE, Boolean.TRUE));
      }
      criteria.setFirstResult(listModel.getCurrentRowIndex());
      criteria.setMaxResults(listModel.getPageSize());
      listModel.setRows(criteria.list());
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuModifier> getComboModifiers() {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(MenuModifier.class);
      criteria.add(Restrictions.eq(MenuModifier.PROP_COMBO_MODIFIER, Boolean.TRUE));
      return criteria.list();
    }
    catch (Exception localException) {}
    return null;
  }
}
