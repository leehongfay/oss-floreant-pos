package com.floreantpos.model.dao;

import com.floreantpos.model.CustomerGroup;
import com.floreantpos.model.Department;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PriceRule;
import com.floreantpos.model.PriceShift;
import com.floreantpos.model.PriceTableItem;
import com.floreantpos.model.SalesArea;
import com.floreantpos.util.ShiftUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;





public class PriceRuleDAO
  extends BasePriceRuleDAO
{
  public PriceRuleDAO() {}
  
  public Double getPrice(MenuItem menuItem, OrderType orderType, Department department, SalesArea salesArea, CustomerGroup customerGroup)
  {
    Session session = null;
    try {
      session = createNewSession();
      return getPrice(menuItem, orderType, department, salesArea, customerGroup, session);
    } finally {
      if (session != null) {
        try {
          session.close();
        }
        catch (Exception localException1) {}
      }
    }
  }
  
  public Double getPrice(MenuItem menuItem, OrderType orderType, Department department, SalesArea salesArea, CustomerGroup customerGroup, Session session) {
    List<String> filteredPriceShift = getFilteredPriceShift();
    Criteria criteria = session.createCriteria(getReferenceClass());
    criteria.setProjection(Projections.property(PriceRule.PROP_PRICE_TABLE_ID));
    criteria.add(Restrictions.or(Restrictions.isNull(PriceRule.PROP_ORDER_TYPE_ID), 
      Restrictions.eq(PriceRule.PROP_ORDER_TYPE_ID, orderType == null ? null : orderType.getId())));
    criteria.add(Restrictions.or(Restrictions.isNull(PriceRule.PROP_DEPARTMENT_ID), 
      Restrictions.eq(PriceRule.PROP_DEPARTMENT_ID, department == null ? null : department.getId())));
    criteria.add(Restrictions.or(Restrictions.isNull(PriceRule.PROP_SALES_AREA_ID), 
      Restrictions.eq(PriceRule.PROP_SALES_AREA_ID, salesArea == null ? null : salesArea.getId())));
    criteria.add(Restrictions.or(Restrictions.isNull(PriceRule.PROP_CUSTOMER_GROUP_ID), 
      Restrictions.eq(PriceRule.PROP_CUSTOMER_GROUP_ID, customerGroup == null ? null : customerGroup.getId())));
    
    if (!filteredPriceShift.isEmpty()) {
      criteria.add(
        Restrictions.or(Restrictions.isNull(PriceRule.PROP_PRICE_SHIFT_ID), Restrictions.in(PriceRule.PROP_PRICE_SHIFT_ID, filteredPriceShift)));
    }
    else {
      criteria.add(Restrictions.isNull(PriceRule.PROP_PRICE_SHIFT_ID));
    }
    criteria.add(Restrictions.eq(PriceRule.PROP_ACTIVE, Boolean.TRUE));
    
    List list = criteria.list();
    if ((list == null) || (list.isEmpty())) {
      return null;
    }
    criteria = session.createCriteria(PriceTableItem.class);
    criteria.setProjection(Projections.property(PriceTableItem.PROP_PRICE));
    criteria.add(Restrictions.eq(PriceTableItem.PROP_MENU_ITEM_ID, menuItem.getId()));
    criteria.add(Restrictions.in(PriceTableItem.PROP_PRICE_TABLE_ID, list));
    
    List prices = criteria.list();
    if ((prices != null) && (prices.size() > 0)) {
      if (prices.size() == 1) {
        return (Double)prices.get(0);
      }
      return (Double)Collections.min(prices);
    }
    
    return null;
  }
  
  private List<String> getFilteredPriceShift() {
    int firstPriorityValue = 1;
    List<String> filteredPriceShift = new ArrayList();
    List<PriceShift> priceShifts = ShiftUtil.getCurrentPricShifts();
    if (priceShifts != null) {
      for (PriceShift priceShift : priceShifts) {
        Integer priorityValue = priceShift.getPriority();
        if (firstPriorityValue >= priorityValue.intValue()) {
          firstPriorityValue = priorityValue.intValue();
          filteredPriceShift.add(priceShift.getId());
        }
      }
    }
    return filteredPriceShift;
  }
}
