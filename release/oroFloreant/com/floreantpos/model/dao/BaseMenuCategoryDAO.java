package com.floreantpos.model.dao;

import com.floreantpos.model.MenuCategory;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseMenuCategoryDAO
  extends _RootDAO
{
  public static MenuCategoryDAO instance;
  
  public BaseMenuCategoryDAO() {}
  
  public static MenuCategoryDAO getInstance()
  {
    if (null == instance) instance = new MenuCategoryDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return MenuCategory.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public MenuCategory cast(Object object)
  {
    return (MenuCategory)object;
  }
  
  public MenuCategory get(String key) throws HibernateException
  {
    return (MenuCategory)get(getReferenceClass(), key);
  }
  
  public MenuCategory get(String key, Session s) throws HibernateException
  {
    return (MenuCategory)get(getReferenceClass(), key, s);
  }
  
  public MenuCategory load(String key) throws HibernateException
  {
    return (MenuCategory)load(getReferenceClass(), key);
  }
  
  public MenuCategory load(String key, Session s) throws HibernateException
  {
    return (MenuCategory)load(getReferenceClass(), key, s);
  }
  
  public MenuCategory loadInitialize(String key, Session s) throws HibernateException
  {
    MenuCategory obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<MenuCategory> findAll()
  {
    return super.findAll();
  }
  


  public List<MenuCategory> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<MenuCategory> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(MenuCategory menuCategory)
    throws HibernateException
  {
    return (String)super.save(menuCategory);
  }
  







  public String save(MenuCategory menuCategory, Session s)
    throws HibernateException
  {
    return (String)save(menuCategory, s);
  }
  





  public void saveOrUpdate(MenuCategory menuCategory)
    throws HibernateException
  {
    saveOrUpdate(menuCategory);
  }
  







  public void saveOrUpdate(MenuCategory menuCategory, Session s)
    throws HibernateException
  {
    saveOrUpdate(menuCategory, s);
  }
  




  public void update(MenuCategory menuCategory)
    throws HibernateException
  {
    update(menuCategory);
  }
  






  public void update(MenuCategory menuCategory, Session s)
    throws HibernateException
  {
    update(menuCategory, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(MenuCategory menuCategory)
    throws HibernateException
  {
    delete(menuCategory);
  }
  






  public void delete(MenuCategory menuCategory, Session s)
    throws HibernateException
  {
    delete(menuCategory, s);
  }
  









  public void refresh(MenuCategory menuCategory, Session s)
    throws HibernateException
  {
    refresh(menuCategory, s);
  }
}
