package com.floreantpos.model.dao;

import com.floreantpos.model.Store;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseStoreDAO
  extends _RootDAO
{
  public static StoreDAO instance;
  
  public BaseStoreDAO() {}
  
  public static StoreDAO getInstance()
  {
    if (null == instance) instance = new StoreDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return Store.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public Store cast(Object object)
  {
    return (Store)object;
  }
  
  public Store get(String key) throws HibernateException
  {
    return (Store)get(getReferenceClass(), key);
  }
  
  public Store get(String key, Session s) throws HibernateException
  {
    return (Store)get(getReferenceClass(), key, s);
  }
  
  public Store load(String key) throws HibernateException
  {
    return (Store)load(getReferenceClass(), key);
  }
  
  public Store load(String key, Session s) throws HibernateException
  {
    return (Store)load(getReferenceClass(), key, s);
  }
  
  public Store loadInitialize(String key, Session s) throws HibernateException
  {
    Store obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<Store> findAll()
  {
    return super.findAll();
  }
  


  public List<Store> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<Store> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(Store store)
    throws HibernateException
  {
    return (String)super.save(store);
  }
  







  public String save(Store store, Session s)
    throws HibernateException
  {
    return (String)save(store, s);
  }
  





  public void saveOrUpdate(Store store)
    throws HibernateException
  {
    saveOrUpdate(store);
  }
  







  public void saveOrUpdate(Store store, Session s)
    throws HibernateException
  {
    saveOrUpdate(store, s);
  }
  




  public void update(Store store)
    throws HibernateException
  {
    update(store);
  }
  






  public void update(Store store, Session s)
    throws HibernateException
  {
    update(store, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(Store store)
    throws HibernateException
  {
    delete(store);
  }
  






  public void delete(Store store, Session s)
    throws HibernateException
  {
    delete(store, s);
  }
  









  public void refresh(Store store, Session s)
    throws HibernateException
  {
    refresh(store, s);
  }
}
