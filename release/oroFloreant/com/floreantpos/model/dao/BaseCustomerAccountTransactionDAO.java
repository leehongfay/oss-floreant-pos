package com.floreantpos.model.dao;

import com.floreantpos.model.CustomerAccountTransaction;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseCustomerAccountTransactionDAO
  extends _RootDAO
{
  public static CustomerAccountTransactionDAO instance;
  
  public BaseCustomerAccountTransactionDAO() {}
  
  public static CustomerAccountTransactionDAO getInstance()
  {
    if (null == instance) instance = new CustomerAccountTransactionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return CustomerAccountTransaction.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public CustomerAccountTransaction cast(Object object)
  {
    return (CustomerAccountTransaction)object;
  }
  
  public CustomerAccountTransaction get(String key) throws HibernateException
  {
    return (CustomerAccountTransaction)get(getReferenceClass(), key);
  }
  
  public CustomerAccountTransaction get(String key, Session s) throws HibernateException
  {
    return (CustomerAccountTransaction)get(getReferenceClass(), key, s);
  }
  
  public CustomerAccountTransaction load(String key) throws HibernateException
  {
    return (CustomerAccountTransaction)load(getReferenceClass(), key);
  }
  
  public CustomerAccountTransaction load(String key, Session s) throws HibernateException
  {
    return (CustomerAccountTransaction)load(getReferenceClass(), key, s);
  }
  
  public CustomerAccountTransaction loadInitialize(String key, Session s) throws HibernateException
  {
    CustomerAccountTransaction obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<CustomerAccountTransaction> findAll()
  {
    return super.findAll();
  }
  


  public List<CustomerAccountTransaction> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<CustomerAccountTransaction> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(CustomerAccountTransaction customerAccountTransaction)
    throws HibernateException
  {
    return (String)super.save(customerAccountTransaction);
  }
  







  public String save(CustomerAccountTransaction customerAccountTransaction, Session s)
    throws HibernateException
  {
    return (String)save(customerAccountTransaction, s);
  }
  





  public void saveOrUpdate(CustomerAccountTransaction customerAccountTransaction)
    throws HibernateException
  {
    saveOrUpdate(customerAccountTransaction);
  }
  







  public void saveOrUpdate(CustomerAccountTransaction customerAccountTransaction, Session s)
    throws HibernateException
  {
    saveOrUpdate(customerAccountTransaction, s);
  }
  




  public void update(CustomerAccountTransaction customerAccountTransaction)
    throws HibernateException
  {
    update(customerAccountTransaction);
  }
  






  public void update(CustomerAccountTransaction customerAccountTransaction, Session s)
    throws HibernateException
  {
    update(customerAccountTransaction, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(CustomerAccountTransaction customerAccountTransaction)
    throws HibernateException
  {
    delete(customerAccountTransaction);
  }
  






  public void delete(CustomerAccountTransaction customerAccountTransaction, Session s)
    throws HibernateException
  {
    delete(customerAccountTransaction, s);
  }
  









  public void refresh(CustomerAccountTransaction customerAccountTransaction, Session s)
    throws HibernateException
  {
    refresh(customerAccountTransaction, s);
  }
}
