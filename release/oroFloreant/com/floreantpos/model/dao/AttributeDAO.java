package com.floreantpos.model.dao;

import com.floreantpos.model.Attribute;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;





public class AttributeDAO
  extends BaseAttributeDAO
{
  public AttributeDAO() {}
  
  public List<Attribute> findAll()
  {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.createAlias(Attribute.PROP_GROUP, "g");
      criteria.addOrder(Order.asc("g.name"));
      return criteria.list();
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public boolean nameExists(Attribute attribute, String code) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Attribute.PROP_NAME, code).ignoreCase());
      if (attribute.getId() != null) {
        criteria.add(Restrictions.ne(Attribute.PROP_ID, attribute.getId()));
      }
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      boolean bool;
      if (rowCount == null) {
        return false;
      }
      return rowCount.intValue() > 0;
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
}
