package com.floreantpos.model.dao;

import com.floreantpos.PosLog;
import com.floreantpos.model.Department;
import com.floreantpos.model.SalesArea;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;


public class SalesAreaDAO
  extends BaseSalesAreaDAO
{
  public SalesAreaDAO() {}
  
  public List<SalesArea> findSalesAreaByDept(Department department)
  {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(SalesArea.PROP_DEPARTMENT_ID, department.getId()));
      
      List list = criteria.list();
      Object localObject1; if ((list == null) || (list.size() == 0)) {
        return null;
      }
      return list;
    } catch (Exception e) {
      PosLog.error(SalesAreaDAO.class, e);
    } finally {
      if (session != null) {
        session.close();
      }
    }
    return null;
  }
  
  public SalesArea findById(String id) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      
      criteria.add(Restrictions.eq(SalesArea.PROP_ID, id));
      
      return (SalesArea)criteria.uniqueResult();
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public SalesArea find(SalesArea salesArea) {
    Session session = null;
    try
    {
      if (salesArea == null) {
        return null;
      }
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      
      criteria.add(Restrictions.eq(SalesArea.PROP_ID, salesArea.getId()));
      
      return (SalesArea)criteria.uniqueResult();
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public SalesArea findByName(String name) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      
      criteria.add(Restrictions.eq(SalesArea.PROP_NAME, name));
      
      return (SalesArea)criteria.uniqueResult();
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
}
