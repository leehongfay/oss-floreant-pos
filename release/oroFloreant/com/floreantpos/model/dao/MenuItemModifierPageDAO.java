package com.floreantpos.model.dao;

import com.floreantpos.PosLog;
import com.floreantpos.model.MenuItemModifierPage;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.swing.PaginatedListModel;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;





public class MenuItemModifierPageDAO
  extends BaseMenuItemModifierPageDAO
{
  public MenuItemModifierPageDAO() {}
  
  public void saveOrUpdatePages(List<MenuItemModifierPage> menuPages)
  {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      for (Iterator iterator = menuPages.iterator(); iterator.hasNext();) {
        MenuItemModifierPage menuPage = (MenuItemModifierPage)iterator.next();
        session.saveOrUpdate(menuPage);
      }
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      PosLog.error(getClass(), e);
    } finally {
      session.close();
    }
  }
  
  public int getRowCount(String specId) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      
      if (StringUtils.isNotEmpty(specId)) {
        criteria.add(Restrictions.eq(MenuItemModifierPage.PROP_MODIFIER_SPEC_ID, specId));
      }
      criteria.add(Restrictions.eq(MenuItemModifierPage.PROP_VISIBLE, Boolean.TRUE));
      criteria.setProjection(Projections.rowCount());
      
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        return rowCount.intValue();
      }
    }
    finally {
      closeSession(session);
    }
    return 0;
  }
  
  public void loadItems(MenuItemModifierSpec spec, PaginatedListModel listModel) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.addOrder(Order.asc(MenuItemModifierPage.PROP_SORT_ORDER));
      if (spec != null) {
        criteria.add(Restrictions.eq(MenuItemModifierPage.PROP_MODIFIER_SPEC_ID, spec.getId()));
      }
      criteria.add(Restrictions.eq(MenuItemModifierPage.PROP_VISIBLE, Boolean.TRUE));
      criteria.setFirstResult(listModel.getCurrentRowIndex());
      criteria.setMaxResults(1);
      listModel.setData(criteria.list());
    } finally {
      closeSession(session);
    }
  }
}
