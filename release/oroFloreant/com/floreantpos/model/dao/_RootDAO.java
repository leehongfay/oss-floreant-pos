package com.floreantpos.model.dao;

import com.floreantpos.Database;
import com.floreantpos.config.AppConfig;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataBuilder;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;





























public abstract class _RootDAO
  extends _BaseRootDAO
{
  private static StandardServiceRegistry standardServiceRegistry;
  
  public _RootDAO() {}
  
  public static void initialize()
  {
    Database database = AppConfig.getDefaultDatabase();
    String connectString = AppConfig.getConnectString();
    String databaseUser = AppConfig.getDatabaseUser();
    String databasePassword = AppConfig.getDatabasePassword();
    initialize("oropos.hibernate.cfg.xml", database, connectString, databaseUser, databasePassword);
  }
  
  public static void initialize(String hibernanetCfgFile) {
    Database database = AppConfig.getDefaultDatabase();
    String connectString = AppConfig.getConnectString();
    String databaseUser = AppConfig.getDatabaseUser();
    String databasePassword = AppConfig.getDatabasePassword();
    initialize(hibernanetCfgFile, database, connectString, databaseUser, databasePassword);
  }
  
  public static void initialize(String hibernanetCfgFile, Database database, String connectString, String databaseUser, String databasePassword) {
    initialize(hibernanetCfgFile, database.getHibernateDialect(), database.getHibernateConnectionDriverClass(), connectString, databaseUser, databasePassword);
  }
  
  public static void initialize(String hibernanetCfgFile, String hibernateDialectClass, String driverClass, String connectString, String databaseUser, String databasePassword) {
    Map<String, String> map = new HashMap();
    map.put("hibernate.dialect", hibernateDialectClass);
    map.put("hibernate.connection.driver_class", driverClass);
    map.put("hibernate.connection.url", connectString);
    map.put("hibernate.connection.username", databaseUser);
    map.put("hibernate.connection.password", databasePassword);
    map.put("hibernate.connection.autocommit", "false");
    map.put("hibernate.max_fetch_depth", "3");
    map.put("hibernate.show_sql", "false");
    map.put("hibernate.connection.isolation", String.valueOf(1));
    map.put("hibernate.cache.use_second_level_cache", "false");
    



    initialize(hibernanetCfgFile, map);
  }
  
  public static void initialize(Map<String, String> properties) {
    initialize("oropos.hibernate.cfg.xml", properties);
  }
  
  public static void initialize(String hibernanetCfgFile, Map<String, String> properties) {
    if (sessionFactory != null) {
      sessionFactory.close();
    }
    if (standardServiceRegistry != null) {
      StandardServiceRegistryBuilder.destroy(standardServiceRegistry);
    }
    StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
    URL resource = _RootDAO.class.getResource("/" + hibernanetCfgFile);
    registryBuilder.configure(resource);
    registryBuilder.applySettings(properties);
    
    setupCconnectionPoolSettings(registryBuilder);
    

    standardServiceRegistry = registryBuilder.build();
    Metadata metaData = new MetadataSources(standardServiceRegistry).getMetadataBuilder().build();
    setSessionFactory(metaData.buildSessionFactory());
  }
  
  public static void setupCconnectionPoolSettings(StandardServiceRegistryBuilder builder)
  {
    builder.applySetting("hibernate.c3p0.min_size", "0");
    
    builder.applySetting("hibernate.c3p0.max_size", "5");
    
    builder.applySetting("hibernate.c3p0.timeout", "300");
    
    builder.applySetting("hibernate.c3p0.max_statements", "0");
    



    builder.applySetting("hibernate.c3p0.checkoutTimeout", "5000");
    builder.applySetting("hibernate.c3p0.acquireRetryAttempts", "1");
    builder.applySetting("hibernate.c3p0.acquireRetryDelay", "100");
    builder.applySetting("hibernate.c3p0.acquireIncrement", "1");
    
    builder.applySetting("testConnectionOnCheckout", "true");
    

    builder.applySetting("hibernate.c3p0.breakAfterAcquireFailure", "false");
  }
  
  public static void setupHikariCPSettings(StandardServiceRegistryBuilder builder) {
    builder.applySetting("hibernate.connection.provider_class", "org.hibernate.hikaricp.internal.HikariCPConnectionProvider");
    builder.applySetting("hibernate.hikari.idleTimeout", "30000");
    builder.applySetting("hibernate.hikari.minimumIdle", "0");
    builder.applySetting("hibernate.hikari.maximumPoolSize", "5");
  }
  
  public static void initialize(String configFileName, Configuration configuration) {
    if (standardServiceRegistry != null) {
      StandardServiceRegistryBuilder.destroy(standardServiceRegistry);
    }
    initialize();
  }
  
















































  public static void releaseConnection()
  {
    if (standardServiceRegistry != null) {
      StandardServiceRegistryBuilder.destroy(standardServiceRegistry);
    }
  }
  
  public void refresh(Object obj) {
    Session session = createNewSession();
    super.refresh(obj, session);
    session.close();
  }
  
  public int rowCount() {
    Session session = getSession();
    Criteria criteria = session.createCriteria(getReferenceClass());
    criteria.setProjection(Projections.rowCount());
    return ((Long)criteria.uniqueResult()).intValue();
  }
  
  public int rowCount(Criteria criteria) {
    criteria.setProjection(Projections.rowCount());
    int intValue = ((Long)criteria.uniqueResult()).intValue();
    criteria.setProjection(null);
    return intValue;
  }
  
  public List getPageData(int pageNum, int pageSize) {
    Session session = getSession();
    try {
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.setFirstResult(pageNum * pageSize);
      criteria.setMaxResults(pageSize);
      
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public int rowCount(Map<String, Object> restrictions) {
    Session session = getSession();
    try {
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.setProjection(Projections.rowCount());
      Iterator localIterator;
      if (restrictions != null)
        for (localIterator = restrictions.keySet().iterator(); localIterator.hasNext();) { key = (String)localIterator.next();
          criteria.add(Restrictions.eq(key, restrictions.get(key)));
        }
      String key;
      Number result = (Number)criteria.uniqueResult();
      if (result != null) {
        return result.intValue();
      }
      return 0;
    } finally {
      closeSession(session);
    }
  }
  
  public List getPageData(int pageNum, int pageSize, Map<String, Object> restrictions) {
    Session session = getSession();
    try {
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.setFirstResult(pageNum * pageSize);
      criteria.setMaxResults(pageSize);
      Object localObject1;
      if (restrictions != null) {
        for (localObject1 = restrictions.keySet().iterator(); ((Iterator)localObject1).hasNext();) { String key = (String)((Iterator)localObject1).next();
          criteria.add(Restrictions.eq(key, restrictions.get(key)));
        }
      }
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
}
