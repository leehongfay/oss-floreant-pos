package com.floreantpos.model.dao;

import com.floreantpos.model.MenuItemSize;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;






public class MenuItemSizeDAO
  extends BaseMenuItemSizeDAO
{
  public MenuItemSizeDAO() {}
  
  public Order getDefaultOrder()
  {
    return Order.asc(MenuItemSize.PROP_SORT_ORDER);
  }
  
  public void saveOrUpdateSizeList(List<MenuItemSize> items, Session session) {
    for (Iterator iterator = items.iterator(); iterator.hasNext();) {
      MenuItemSize menuItemSize = (MenuItemSize)iterator.next();
      session.saveOrUpdate(menuItemSize);
    }
  }
  
  public void saveOrUpdateList(List<MenuItemSize> sizeList) {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      saveOrUpdateSizeList(sizeList, session);
      tx.commit();
    }
    catch (Exception e) {
      throw e;
    } finally {
      session.close();
    }
  }
}
