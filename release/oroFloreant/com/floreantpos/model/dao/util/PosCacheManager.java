package com.floreantpos.model.dao.util;

import com.floreantpos.config.TerminalConfig;
import com.floreantpos.model.Address;
import com.floreantpos.model.Course;
import com.floreantpos.model.Currency;
import com.floreantpos.model.Customer;
import com.floreantpos.model.CustomerGroup;
import com.floreantpos.model.DayPart;
import com.floreantpos.model.Department;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuItemInventoryStatus;
import com.floreantpos.model.MenuShift;
import com.floreantpos.model.Multiplier;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Outlet;
import com.floreantpos.model.PosPrinters;
import com.floreantpos.model.PriceShift;
import com.floreantpos.model.PrinterGroup;
import com.floreantpos.model.Recepie;
import com.floreantpos.model.ReportGroup;
import com.floreantpos.model.SalesArea;
import com.floreantpos.model.Shift;
import com.floreantpos.model.Store;
import com.floreantpos.model.TaxGroup;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.TerminalType;
import com.floreantpos.model.User;
import com.floreantpos.model.UserType;
import com.floreantpos.model.dao.AddressDAO;
import com.floreantpos.model.dao.CourseDAO;
import com.floreantpos.model.dao.CurrencyDAO;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.dao.DayPartDAO;
import com.floreantpos.model.dao.DepartmentDAO;
import com.floreantpos.model.dao.InventoryLocationDAO;
import com.floreantpos.model.dao.InventoryUnitDAO;
import com.floreantpos.model.dao.MenuItemInventoryStatusDAO;
import com.floreantpos.model.dao.MenuShiftDAO;
import com.floreantpos.model.dao.MultiplierDAO;
import com.floreantpos.model.dao.OrderTypeDAO;
import com.floreantpos.model.dao.OutletDAO;
import com.floreantpos.model.dao.PriceRuleDAO;
import com.floreantpos.model.dao.PriceShiftDAO;
import com.floreantpos.model.dao.PrinterGroupDAO;
import com.floreantpos.model.dao.RecepieDAO;
import com.floreantpos.model.dao.ReportGroupDAO;
import com.floreantpos.model.dao.SalesAreaDAO;
import com.floreantpos.model.dao.ShiftDAO;
import com.floreantpos.model.dao.StoreDAO;
import com.floreantpos.model.dao.TaxGroupDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.dao.TerminalTypeDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.model.dao.UserTypeDAO;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.DatabaseConnectionException;
import com.orocube.common.util.TerminalUtil;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.hibernate.Session;


public class PosCacheManager
  extends DataProvider
{
  private Store store;
  private Outlet outlet;
  private Terminal terminal;
  private InventoryLocation defaultInLocation;
  private InventoryLocation defaultOutLocation;
  private Map<String, OrderType> orderTypes = null;
  private Map<String, UserType> userTypeMap = new LinkedHashMap();
  private Map<String, Shift> shiftMap = new LinkedHashMap();
  private Map<String, Currency> currencyMap = new LinkedHashMap();
  private Map<String, TerminalType> terminalTypeMap = new LinkedHashMap();
  
  private Map<String, Multiplier> multiplierMap = new LinkedHashMap();
  private Map<String, Department> departmentMap = new LinkedHashMap();
  private Map<String, InventoryLocation> inventoryLocationMap = new LinkedHashMap();
  private Map<String, InventoryUnit> inventoryUnitMap = new HashMap();
  private Map<String, TaxGroup> taxGroupMap = new HashMap();
  private Map<String, PrinterGroup> printerGroupMap = new HashMap();
  private Map<String, ReportGroup> reportGroupMap = new HashMap();
  private Map<String, Course> courseMap = new HashMap();
  
  private static List<DayPart> daypartShifts;
  
  private static List<PriceShift> priceShifts;
  
  private static List<MenuShift> menuShifts;
  private Map<Integer, Terminal> terminalMap = new LinkedHashMap();
  private Map<String, User> userMap = new LinkedHashMap();
  
  private List<OrderType> orderTypeList = new ArrayList();
  private List<Multiplier> multiplierList = new ArrayList();
  private String defaultCourseId;
  
  public PosCacheManager() {}
  
  public void initialize() { terminalMap.clear();
    userMap.clear();
    multiplierList.clear();
    orderTypeList.clear();
    
    loadStore();
    initializeShifts();
    loadTerminalTypes();
    loadUserTypes();
    loadTerminal();
    
    loadOrderTypes();
    
    loadCurrencies();
    
    loadCourses();
    
    loadMultipliers();
    loadInventoryLocations();
    loadPrinterGroups();
    loadInventoryUnits();
    loadReportGroups();
    loadTaxGroups();
    loadDepartments();
    loadPrinters();
  }
  
  private PosPrinters printers;
  private void initializeShifts() { List<Shift> dataList = ShiftDAO.getInstance().findAll();
    clear(shiftMap);
    if ((dataList != null) && (dataList.size() > 0)) {
      for (Shift obj : dataList) {
        shiftMap.put(obj.getId(), obj);
      }
    }
    
    daypartShifts = DayPartDAO.getInstance().findAll();
    for (Iterator iterator = daypartShifts.iterator(); iterator.hasNext();) {
      Shift shift = (Shift)iterator.next();
      if ("DEFAULT SHIFT".equalsIgnoreCase(shift.getName())) {
        iterator.remove();
        break;
      }
    }
    
    priceShifts = PriceShiftDAO.getInstance().findAllActive();
    menuShifts = MenuShiftDAO.getInstance().findAllActive();
  }
  
  private void loadMultipliers() {
    List<Multiplier> dataList = MultiplierDAO.getInstance().findAll();
    clear(multiplierMap);
    if ((dataList != null) && (dataList.size() > 0)) {
      for (Multiplier obj : dataList) {
        multiplierMap.put(obj.getId(), obj);
        multiplierList.add(obj);
      }
    }
  }
  
  private void loadTerminalTypes() {
    List<TerminalType> dataList = TerminalTypeDAO.getInstance().findAll();
    clear(terminalTypeMap);
    if ((dataList != null) && (dataList.size() > 0)) {
      for (TerminalType obj : dataList) {
        terminalTypeMap.put(obj.getId(), obj);
      }
    }
  }
  
  private void loadCurrencies() {
    List<Currency> dataList = CurrencyDAO.getInstance().findAll();
    clear(currencyMap);
    if ((dataList != null) && (dataList.size() > 0)) {
      for (Currency obj : dataList) {
        currencyMap.put(obj.getId(), obj);
      }
    }
    CurrencyUtil.populateCurrency();
  }
  
  private void loadCourses() {
    List<Course> dataList = CourseDAO.getInstance().findAll();
    clear(courseMap);
    if ((dataList != null) && (dataList.size() > 0)) {
      defaultCourseId = ((Course)dataList.get(0)).getId();
      for (Course obj : dataList) {
        courseMap.put(obj.getId(), obj);
      }
    }
  }
  
  private void loadUserTypes() {
    List<UserType> dataList = UserTypeDAO.getInstance().findAll();
    clear(userTypeMap);
    if ((dataList != null) && (dataList.size() > 0)) {
      for (UserType obj : dataList) {
        userTypeMap.put(obj.getId(), obj);
      }
    }
  }
  
  private void loadDepartments() {
    List<Department> dataList = DepartmentDAO.getInstance().findAll();
    clear(departmentMap);
    if ((dataList != null) && (dataList.size() > 0)) {
      for (Department obj : dataList) {
        departmentMap.put(obj.getId(), obj);
      }
    }
  }
  
  public void loadCloudCacheData() {
    loadStore();
    loadTerminal();
    loadPrinterGroups();
  }
  
  public void refreshStore()
  {
    store = StoreDAO.getRestaurant();
  }
  
  public void refreshCurrentTerminal()
  {
    TerminalDAO.getInstance().refresh(terminal);
  }
  
  private void loadStore() {
    store = StoreDAO.getRestaurant();
    if ((store.getUniqueId() == null) || (store.getUniqueId().intValue() == 0)) {
      store.setUniqueId(Integer.valueOf(RandomUtils.nextInt()));
      StoreDAO.getInstance().saveOrUpdate(store);
    }
    outlet = OutletDAO.getInstance().get(String.valueOf(store.getUniqueId()));
    if (outlet == null) {
      outlet = new Outlet();
      outlet.setId(String.valueOf(store.getUniqueId()));
      outlet.setName(store.getOutletName());
      Address address = new Address();
      address.setAddressLine(store.getAddressLine1());
      outlet.setAddress(address);
      AddressDAO.getInstance().saveOrUpdate(address);
      OutletDAO.getInstance().save(outlet);
    }
  }
  
  private void loadOrderTypes() {
    orderTypes = new LinkedHashMap();
    List<OrderType> dataList = OrderTypeDAO.getInstance().findEnabledOrderTypesForTerminal(terminal);
    orderTypeList.clear();
    
    if ((dataList != null) && (dataList.size() > 0)) {
      for (OrderType obj : dataList) {
        orderTypes.put(obj.getId(), obj);
        orderTypeList.add(obj);
      }
    }
  }
  
  private void loadTerminal() {
    try {
      int terminalId = 0;
      String terminalKey = TerminalUtil.getSystemUID();
      TerminalDAO terminalDAO = TerminalDAO.getInstance();
      terminal = terminalDAO.getByTerminalKey(terminalKey);
      if (terminal != null) {
        TerminalConfig.setTerminalId(terminal.getId().intValue());
        return;
      }
      terminalId = TerminalConfig.getTerminalId();
      
      terminal = terminalDAO.get(Integer.valueOf(terminalId));
      Outlet outlet = DataProvider.get().getOutlet();
      if (terminal == null) {
        terminal = terminalDAO.createNewTerminal(terminalKey, Integer.valueOf(terminalId), outlet);
      }
      else if (!terminalKey.equals(terminal.getTerminalKey())) {
        terminal = terminalDAO.createNewTerminal(terminalKey, null, outlet);
      }
      
      if (terminal.getOutlet() == null) {
        terminal.setOutlet(outlet);
        terminalDAO.saveOrUpdate(terminal);
      }
      TerminalConfig.setTerminalId(terminalId);
    } catch (Exception e) {
      throw new DatabaseConnectionException(e);
    }
  }
  
  private void loadTaxGroups() {
    List<TaxGroup> dataList = TaxGroupDAO.getInstance().findAll();
    clear(taxGroupMap);
    if ((dataList != null) && (dataList.size() > 0)) {
      for (TaxGroup obj : dataList) {
        taxGroupMap.put(obj.getId(), obj);
      }
    }
  }
  
  private void loadReportGroups() {
    List<ReportGroup> dataList = ReportGroupDAO.getInstance().findAll();
    clear(reportGroupMap);
    if ((dataList != null) && (dataList.size() > 0)) {
      for (ReportGroup obj : dataList) {
        reportGroupMap.put(obj.getId(), obj);
      }
    }
  }
  
  private void loadInventoryUnits() {
    List<InventoryUnit> dataList = InventoryUnitDAO.getInstance().findAll();
    clear(inventoryUnitMap);
    if ((dataList != null) && (dataList.size() > 0)) {
      for (InventoryUnit obj : dataList) {
        inventoryUnitMap.put(obj.getId(), obj);
      }
    }
  }
  
  private void loadPrinterGroups() {
    List<PrinterGroup> dataList = PrinterGroupDAO.getInstance().findAll();
    clear(printerGroupMap);
    if ((dataList != null) && (dataList.size() > 0)) {
      for (PrinterGroup obj : dataList) {
        printerGroupMap.put(obj.getId(), obj);
      }
    }
  }
  
  private void loadInventoryLocations() {
    defaultInLocation = null;
    defaultOutLocation = null;
    
    Terminal currentTerminal = getCurrentTerminal();
    String inventoryInLocationId = currentTerminal.getInventoryInLocationId();
    String inventoryOutLocationId = currentTerminal.getInventoryOutLocationId();
    if (StringUtils.isNotEmpty(inventoryInLocationId)) {
      defaultInLocation = InventoryLocationDAO.getInstance().get(inventoryInLocationId);
    }
    if (StringUtils.isNotEmpty(inventoryOutLocationId)) {
      defaultOutLocation = InventoryLocationDAO.getInstance().get(inventoryOutLocationId);
    }
    
    List<InventoryLocation> dataList = InventoryLocationDAO.getInstance().findAll();
    clear(inventoryLocationMap);
    if ((dataList != null) && (dataList.size() > 0)) {
      for (InventoryLocation location : dataList) {
        inventoryLocationMap.put(location.getId(), location);
        if ((defaultInLocation == null) && (location.isDefaultInLocation().booleanValue())) {
          defaultInLocation = location;
        }
        if ((defaultOutLocation == null) && (location.isDefaultOutLocation().booleanValue())) {
          defaultOutLocation = location;
        }
      }
    }
  }
  
  private void clear(Map map) {
    if (map.size() > 0) {
      map.clear();
    }
  }
  
  public Terminal getCurrentTerminal() {
    return terminal;
  }
  
  public void setCloudTerminal(Terminal terminal) {
    terminalMap.put(terminal.getId(), terminal);
    this.terminal = terminal;
  }
  
  public OrderType getOrderType(String orderTypeId) {
    return (OrderType)orderTypes.get(orderTypeId);
  }
  
  public List<OrderType> getOrderTypes() {
    return orderTypeList;
  }
  
  public UserType getUserType(String userTypeId) {
    UserType userType = (UserType)userTypeMap.get(userTypeId);
    if (userType == null) {
      return UserTypeDAO.getInstance().get(userTypeId);
    }
    
    return userType;
  }
  
  public Currency getCurrency(String currencyId) {
    return (Currency)currencyMap.get(currencyId);
  }
  
  public Course getCourse(String courseId) {
    return (Course)courseMap.get(courseId);
  }
  
  public TerminalType getTerminalType(String terminalTypeId) {
    return (TerminalType)terminalTypeMap.get(terminalTypeId);
  }
  
  public Multiplier getMultiplierById(String id) {
    return (Multiplier)multiplierMap.get(id);
  }
  
  public List<Multiplier> getMultiplierList() {
    return multiplierList;
  }
  
  public InventoryLocation getDefaultInLocation() {
    return defaultInLocation;
  }
  
  public InventoryLocation getDefaultOutLocation() {
    return defaultOutLocation;
  }
  
  public List<Course> getCourses() {
    if (courseMap.size() > 0)
      return new ArrayList(courseMap.values());
    return new ArrayList();
  }
  
  public String getDefaultCourseId() {
    return defaultCourseId;
  }
  
  public String getRecipeMenuItemName(Recepie recipe) {
    return RecepieDAO.getInstance().getMenuItemName(recipe);
  }
  
  public MenuItemInventoryStatus getMenuItemStockStatus(MenuItem menuItem)
  {
    MenuItemInventoryStatusDAO dao = MenuItemInventoryStatusDAO.getInstance();
    
    Session session = dao.createNewSession();Throwable localThrowable3 = null;
    try { MenuItemInventoryStatus stockStatus = dao.get(menuItem.getId(), session);
      if (stockStatus == null) {
        stockStatus = new MenuItemInventoryStatus(menuItem.getId());
        dao.save(stockStatus, session);
      }
      return stockStatus;
    }
    catch (Throwable localThrowable1)
    {
      localThrowable3 = localThrowable1;throw localThrowable1;


    }
    finally
    {

      if (session != null) if (localThrowable3 != null) try { session.close(); } catch (Throwable localThrowable2) { localThrowable3.addSuppressed(localThrowable2); } else session.close();
    }
  }
  
  public SalesArea getSalesArea(String salesAreaId) {
    if (StringUtils.isEmpty(salesAreaId)) {
      return null;
    }
    return SalesAreaDAO.getInstance().get(salesAreaId);
  }
  
  public Customer getCustomer(String id)
  {
    if (StringUtils.isEmpty(id)) {
      return null;
    }
    return CustomerDAO.getInstance().get(id);
  }
  
  public double getPriceFromPriceRule(MenuItem menuItem, OrderType orderType, Department department, SalesArea salesArea, CustomerGroup customerGroup)
  {
    Double price = PriceRuleDAO.getInstance().getPrice(menuItem, orderType, department, salesArea, customerGroup);
    if (price == null)
      return menuItem.getPrice().doubleValue();
    return price.doubleValue();
  }
  
  public PosPrinters getPrinters()
  {
    return printers;
  }
  
  private void loadPrinters() {
    printers = PosPrinters.load();
    if (printers == null) {
      printers = new PosPrinters();
    }
  }
  
  public Store getStore() {
    return store;
  }
  
  public Outlet getOutlet() {
    return outlet;
  }
  
  public InventoryUnit getInventoryUnitById(String unitId) {
    if (StringUtils.isEmpty(unitId))
      return null;
    return (InventoryUnit)inventoryUnitMap.get(unitId);
  }
  
  public TaxGroup getTaxGroupById(String taxGroupId) {
    if (StringUtils.isEmpty(taxGroupId))
      return null;
    return (TaxGroup)taxGroupMap.get(taxGroupId);
  }
  
  public ReportGroup getReportGroupById(String reportGroupId) {
    if (StringUtils.isEmpty(reportGroupId))
      return null;
    return (ReportGroup)reportGroupMap.get(reportGroupId);
  }
  
  public PrinterGroup getPrinterGroupById(String printerGroupId) {
    if (StringUtils.isEmpty(printerGroupId))
      return null;
    return (PrinterGroup)printerGroupMap.get(printerGroupId);
  }
  
  public Department getDepartmentById(String departmentId) {
    if (StringUtils.isEmpty(departmentId))
      return null;
    return (Department)departmentMap.get(departmentId);
  }
  
  public OrderType getOrderTypeById(String orderTypeId) {
    if (StringUtils.isEmpty(orderTypeId))
      return null;
    if (orderTypes != null)
      return (OrderType)orderTypes.get(orderTypeId);
    return OrderTypeDAO.getInstance().get(orderTypeId);
  }
  
  public InventoryLocation getInventoryLocationById(String inventoryLocationId) {
    if (StringUtils.isEmpty(inventoryLocationId))
      return null;
    return (InventoryLocation)inventoryLocationMap.get(inventoryLocationId);
  }
  
  public User getUserById(String userId) {
    if (StringUtils.isEmpty(userId))
      return null;
    User user = (User)userMap.get(userId);
    if (user == null) {
      user = UserDAO.getInstance().get(userId);
      userMap.put(userId, user);
    }
    return user;
  }
  
  public Terminal getTerminalById(Integer terminalId) {
    if (terminalId == null)
      return null;
    Terminal terminal2 = (Terminal)terminalMap.get(terminalId);
    if (terminal2 == null) {
      terminal2 = TerminalDAO.getInstance().get(terminalId);
      terminalMap.put(terminalId, terminal2);
    }
    return terminal2;
  }
  
  public Object getObjectOf(Class clazz, Serializable id)
  {
    Session session = null;
    try
    {
      session = StoreDAO.getInstance().createNewSession();
      return session.get(clazz, id);
    } finally {
      StoreDAO.getInstance().closeSession(session);
    }
  }
  
  public Shift getShiftById(String shiftId) {
    if (StringUtils.isEmpty(shiftId))
      return null;
    return (Shift)shiftMap.get(shiftId);
  }
  
  public List<DayPart> getDaryPartShifts()
  {
    return daypartShifts;
  }
  
  public List<PriceShift> getPriceShifts()
  {
    return priceShifts;
  }
  
  public List<MenuShift> getMenuShifts()
  {
    return menuShifts;
  }
  
  public File getAppConfigFileLocation()
  {
    return new File(".");
  }
}
