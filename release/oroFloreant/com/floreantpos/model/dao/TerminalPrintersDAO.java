package com.floreantpos.model.dao;

import com.floreantpos.main.Application;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.TerminalPrinters;
import com.floreantpos.model.VirtualPrinter;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;





public class TerminalPrintersDAO
  extends BaseTerminalPrintersDAO
{
  public TerminalPrintersDAO() {}
  
  public List<TerminalPrinters> findTerminalPrinters()
  {
    return findTerminalPrinters(Application.getInstance().getTerminal());
  }
  
  public List<TerminalPrinters> findTerminalPrinters(Terminal terminal) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(TerminalPrinters.PROP_TERMINAL, terminal));
      
      List list = criteria.list();
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public TerminalPrinters findPrinters(VirtualPrinter virtualPrinter) {
    return findPrinters(virtualPrinter, Application.getInstance().getTerminal());
  }
  
  public TerminalPrinters findPrinters(VirtualPrinter virtualPrinter, Terminal terminal) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.and(Restrictions.eq(TerminalPrinters.PROP_TERMINAL, terminal), 
        Restrictions.eq(TerminalPrinters.PROP_VIRTUAL_PRINTER, virtualPrinter)));
      criteria.setMaxResults(1);
      return (TerminalPrinters)criteria.uniqueResult();
    } finally {
      closeSession(session);
    }
  }
}
