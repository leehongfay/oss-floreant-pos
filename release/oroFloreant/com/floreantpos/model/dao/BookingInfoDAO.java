package com.floreantpos.model.dao;

import com.floreantpos.PosLog;
import com.floreantpos.model.BookingInfo;
import com.floreantpos.model.ShopFloor;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.ShopTableStatus;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;






















public class BookingInfoDAO
  extends BaseBookingInfoDAO
{
  public BookingInfoDAO() {}
  
  public List<BookingInfo> getBookedTables(Date startDate, Date endDate)
  {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      






















      criteria.add(Restrictions.or(Restrictions.ge(BookingInfo.PROP_FROM_DATE, startDate), Restrictions.le(BookingInfo.PROP_TO_DATE, endDate)));
      
      List<BookingInfo> list = criteria.list();
      List<BookingInfo> localList1; if (list != null) {
        return list;
      }
      return null;
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
    return null;
  }
  
  public List<ShopTable> getAllTableForBookingOfFloor(Date startDate, Date endDate, ShopFloor shopFloor) {
    List<BookingInfo> bookedTables = getBookedTables(startDate, endDate);
    List<ShopTable> allTables = ShopTableDAO.getInstance().getByFloor(shopFloor);
    



    return null;
  }
  
  public List<BookingInfo> getAllOpenBooking() {
    Session session = null;
    try {
      session = createNewSession();
      
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.ne(BookingInfo.PROP_STATUS, "close"));
      List list = criteria.list();
      return list;
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    } finally {
      try {
        session.close();
      }
      catch (Exception localException3) {}
    }
    return null;
  }
  

  public void setBookingStatus(BookingInfo bookingInfo, String bookingStatus, List<ShopTableStatus> tableStatusList)
  {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      bookingInfo.setStatus(bookingStatus);
      saveOrUpdate(bookingInfo);
      
      if ((bookingStatus.equals("seat")) || (bookingStatus.equals("delay"))) {
        ShopTableDAO.getInstance().bookedTables(tableStatusList);
      }
      
      if ((bookingStatus.equals("cancel")) || (bookingStatus.equals("no appear")) || 
        (bookingStatus.equals("close"))) {
        ShopTableDAO.getInstance().freeTables(tableStatusList);
      }
      
      tx.commit();
    }
    catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(BookingInfo.class).error(e);
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
  




























  public List getTodaysBooking()
  {
    Session session = null;
    try
    {
      Calendar startDate = Calendar.getInstance();
      startDate.setLenient(false);
      startDate.setTime(new Date());
      startDate.set(11, 0);
      startDate.set(12, 0);
      startDate.set(13, 0);
      startDate.set(14, 0);
      
      Calendar endDate = Calendar.getInstance();
      endDate.setLenient(false);
      endDate.setTime(new Date());
      endDate.set(11, 23);
      endDate.set(12, 59);
      endDate.set(13, 59);
      
      session = createNewSession();
      
      Criteria criteria = session.createCriteria(BookingInfo.class);
      
      criteria.add(Restrictions.ge(BookingInfo.PROP_FROM_DATE, startDate.getTime())).add(Restrictions.le(BookingInfo.PROP_FROM_DATE, endDate.getTime()))
        .add(Restrictions.eq(BookingInfo.PROP_STATUS, "open"));
      List list = criteria.list();
      
      return list;
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    } finally {
      session.close();
    }
    return null;
  }
  
  public List<ShopTable> getAllBookedTablesByDate(Date startDate, Date endDate)
  {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(BookingInfo.class);
      
      criteria.add(Restrictions.ge(BookingInfo.PROP_FROM_DATE, startDate)).add(Restrictions.le(BookingInfo.PROP_FROM_DATE, endDate))
        .add(Restrictions.ne(BookingInfo.PROP_STATUS, "close"));
      
      List list = criteria.list();
      List<ShopTable> bookedTableList = new ArrayList();
      for (Iterator iterator = list.iterator(); iterator.hasNext();) {
        BookingInfo bookingInfo = (BookingInfo)iterator.next();
        for (ShopTable shopTable : bookingInfo.getTables()) {
          bookedTableList.add(shopTable);
        }
      }
      
      return bookedTableList;
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    } finally {
      session.close();
    }
    return null;
  }
  
  public List<BookingInfo> getAllBookingInfoByDate(Date startDate, Date endDate)
  {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(BookingInfo.class);
      
      criteria.add(Restrictions.ge(BookingInfo.PROP_FROM_DATE, startDate)).add(Restrictions.le(BookingInfo.PROP_FROM_DATE, endDate))
        .add(Restrictions.ne(BookingInfo.PROP_STATUS, "close"));
      
      List list = criteria.list();
      List localList1; if (list != null) {
        return list;
      }
      return null;
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    } finally {
      session.close();
    }
    return null;
  }
}
