package com.floreantpos.model.dao;

import com.floreantpos.model.KitchenTicketItem;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseKitchenTicketItemDAO
  extends _RootDAO
{
  public static KitchenTicketItemDAO instance;
  
  public BaseKitchenTicketItemDAO() {}
  
  public static KitchenTicketItemDAO getInstance()
  {
    if (null == instance) instance = new KitchenTicketItemDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return KitchenTicketItem.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public KitchenTicketItem cast(Object object)
  {
    return (KitchenTicketItem)object;
  }
  
  public KitchenTicketItem get(String key) throws HibernateException
  {
    return (KitchenTicketItem)get(getReferenceClass(), key);
  }
  
  public KitchenTicketItem get(String key, Session s) throws HibernateException
  {
    return (KitchenTicketItem)get(getReferenceClass(), key, s);
  }
  
  public KitchenTicketItem load(String key) throws HibernateException
  {
    return (KitchenTicketItem)load(getReferenceClass(), key);
  }
  
  public KitchenTicketItem load(String key, Session s) throws HibernateException
  {
    return (KitchenTicketItem)load(getReferenceClass(), key, s);
  }
  
  public KitchenTicketItem loadInitialize(String key, Session s) throws HibernateException
  {
    KitchenTicketItem obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<KitchenTicketItem> findAll()
  {
    return super.findAll();
  }
  


  public List<KitchenTicketItem> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<KitchenTicketItem> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(KitchenTicketItem kitchenTicketItem)
    throws HibernateException
  {
    return (String)super.save(kitchenTicketItem);
  }
  







  public String save(KitchenTicketItem kitchenTicketItem, Session s)
    throws HibernateException
  {
    return (String)save(kitchenTicketItem, s);
  }
  





  public void saveOrUpdate(KitchenTicketItem kitchenTicketItem)
    throws HibernateException
  {
    saveOrUpdate(kitchenTicketItem);
  }
  







  public void saveOrUpdate(KitchenTicketItem kitchenTicketItem, Session s)
    throws HibernateException
  {
    saveOrUpdate(kitchenTicketItem, s);
  }
  




  public void update(KitchenTicketItem kitchenTicketItem)
    throws HibernateException
  {
    update(kitchenTicketItem);
  }
  






  public void update(KitchenTicketItem kitchenTicketItem, Session s)
    throws HibernateException
  {
    update(kitchenTicketItem, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(KitchenTicketItem kitchenTicketItem)
    throws HibernateException
  {
    delete(kitchenTicketItem);
  }
  






  public void delete(KitchenTicketItem kitchenTicketItem, Session s)
    throws HibernateException
  {
    delete(kitchenTicketItem, s);
  }
  









  public void refresh(KitchenTicketItem kitchenTicketItem, Session s)
    throws HibernateException
  {
    refresh(kitchenTicketItem, s);
  }
}
