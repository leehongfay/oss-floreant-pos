package com.floreantpos.model.dao;

import com.floreantpos.model.ShopFloor;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseShopFloorDAO
  extends _RootDAO
{
  public static ShopFloorDAO instance;
  
  public BaseShopFloorDAO() {}
  
  public static ShopFloorDAO getInstance()
  {
    if (null == instance) instance = new ShopFloorDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return ShopFloor.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public ShopFloor cast(Object object)
  {
    return (ShopFloor)object;
  }
  
  public ShopFloor get(String key) throws HibernateException
  {
    return (ShopFloor)get(getReferenceClass(), key);
  }
  
  public ShopFloor get(String key, Session s) throws HibernateException
  {
    return (ShopFloor)get(getReferenceClass(), key, s);
  }
  
  public ShopFloor load(String key) throws HibernateException
  {
    return (ShopFloor)load(getReferenceClass(), key);
  }
  
  public ShopFloor load(String key, Session s) throws HibernateException
  {
    return (ShopFloor)load(getReferenceClass(), key, s);
  }
  
  public ShopFloor loadInitialize(String key, Session s) throws HibernateException
  {
    ShopFloor obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<ShopFloor> findAll()
  {
    return super.findAll();
  }
  


  public List<ShopFloor> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<ShopFloor> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(ShopFloor shopFloor)
    throws HibernateException
  {
    return (String)super.save(shopFloor);
  }
  







  public String save(ShopFloor shopFloor, Session s)
    throws HibernateException
  {
    return (String)save(shopFloor, s);
  }
  





  public void saveOrUpdate(ShopFloor shopFloor)
    throws HibernateException
  {
    saveOrUpdate(shopFloor);
  }
  







  public void saveOrUpdate(ShopFloor shopFloor, Session s)
    throws HibernateException
  {
    saveOrUpdate(shopFloor, s);
  }
  




  public void update(ShopFloor shopFloor)
    throws HibernateException
  {
    update(shopFloor);
  }
  






  public void update(ShopFloor shopFloor, Session s)
    throws HibernateException
  {
    update(shopFloor, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(ShopFloor shopFloor)
    throws HibernateException
  {
    delete(shopFloor);
  }
  






  public void delete(ShopFloor shopFloor, Session s)
    throws HibernateException
  {
    delete(shopFloor, s);
  }
  









  public void refresh(ShopFloor shopFloor, Session s)
    throws HibernateException
  {
    refresh(shopFloor, s);
  }
}
