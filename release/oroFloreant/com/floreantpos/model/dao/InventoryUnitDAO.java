package com.floreantpos.model.dao;

import com.floreantpos.model.InventoryUnit;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;





















public class InventoryUnitDAO
  extends BaseInventoryUnitDAO
{
  public InventoryUnitDAO() {}
  
  public boolean nameExists(InventoryUnit unit, String code)
  {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(InventoryUnit.PROP_NAME, code).ignoreCase());
      if (unit.getId() != null) {
        criteria.add(Restrictions.ne(InventoryUnit.PROP_ID, unit.getId()));
      }
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      boolean bool;
      if (rowCount == null) {
        return false;
      }
      return rowCount.intValue() > 0;
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public List<InventoryUnit> findByGroupId(String unitGroupId) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(InventoryUnit.PROP_UNIT_GROUP_ID, unitGroupId));
      return criteria.list();
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
}
