package com.floreantpos.model.dao;

import com.floreantpos.model.VoidReason;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseVoidReasonDAO
  extends _RootDAO
{
  public static VoidReasonDAO instance;
  
  public BaseVoidReasonDAO() {}
  
  public static VoidReasonDAO getInstance()
  {
    if (null == instance) instance = new VoidReasonDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return VoidReason.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public VoidReason cast(Object object)
  {
    return (VoidReason)object;
  }
  
  public VoidReason get(String key) throws HibernateException
  {
    return (VoidReason)get(getReferenceClass(), key);
  }
  
  public VoidReason get(String key, Session s) throws HibernateException
  {
    return (VoidReason)get(getReferenceClass(), key, s);
  }
  
  public VoidReason load(String key) throws HibernateException
  {
    return (VoidReason)load(getReferenceClass(), key);
  }
  
  public VoidReason load(String key, Session s) throws HibernateException
  {
    return (VoidReason)load(getReferenceClass(), key, s);
  }
  
  public VoidReason loadInitialize(String key, Session s) throws HibernateException
  {
    VoidReason obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<VoidReason> findAll()
  {
    return super.findAll();
  }
  


  public List<VoidReason> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<VoidReason> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(VoidReason voidReason)
    throws HibernateException
  {
    return (String)super.save(voidReason);
  }
  







  public String save(VoidReason voidReason, Session s)
    throws HibernateException
  {
    return (String)save(voidReason, s);
  }
  





  public void saveOrUpdate(VoidReason voidReason)
    throws HibernateException
  {
    saveOrUpdate(voidReason);
  }
  







  public void saveOrUpdate(VoidReason voidReason, Session s)
    throws HibernateException
  {
    saveOrUpdate(voidReason, s);
  }
  




  public void update(VoidReason voidReason)
    throws HibernateException
  {
    update(voidReason);
  }
  






  public void update(VoidReason voidReason, Session s)
    throws HibernateException
  {
    update(voidReason, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(VoidReason voidReason)
    throws HibernateException
  {
    delete(voidReason);
  }
  






  public void delete(VoidReason voidReason, Session s)
    throws HibernateException
  {
    delete(voidReason, s);
  }
  









  public void refresh(VoidReason voidReason, Session s)
    throws HibernateException
  {
    refresh(voidReason, s);
  }
}
