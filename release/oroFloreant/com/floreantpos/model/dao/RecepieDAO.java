package com.floreantpos.model.dao;

import com.floreantpos.main.Application;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.InventoryTransactionType;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.Recepie;
import com.floreantpos.model.RecipeTable;
import com.floreantpos.model.Store;
import com.floreantpos.swing.BeanTableModel;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import net.authorize.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
























public class RecepieDAO
  extends BaseRecepieDAO
{
  public RecepieDAO() {}
  
  public int rowCount(String name, boolean subRecipeOnly)
  {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(Recepie.class);
      criteria.setProjection(Projections.rowCount());
      
      if (StringUtils.isNotEmpty(name)) {
        criteria.add(Restrictions.ilike(Recepie.PROP_NAME, name.trim(), MatchMode.ANYWHERE));
      }
      Number rowCount = (Number)criteria.uniqueResult();
      int i; if (rowCount != null) {
        return rowCount.intValue();
      }
      return 0;
    } finally {
      closeSession(session);
    }
  }
  
  public void loadRecepies(BeanTableModel<Recepie> model, String name, boolean subRecipeOnly) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(Recepie.class);
      
      if (StringUtils.isNotEmpty(name)) {
        Disjunction disjunction = Restrictions.disjunction();
        disjunction.add(Restrictions.ilike(Recepie.PROP_NAME, name.trim(), MatchMode.ANYWHERE));
        criteria.createAlias(Recepie.PROP_MENU_ITEM, "item");
        disjunction.add(Restrictions.ilike("item.name", name.trim(), MatchMode.START));
        criteria.add(disjunction);
      }
      criteria.addOrder(Order.asc(Recepie.PROP_NAME));
      criteria.setFirstResult(model.getCurrentRowIndex());
      criteria.setMaxResults(model.getPageSize());
      List<Recepie> result = criteria.list();
      model.setRows(result);
    } finally {
      closeSession(session);
    }
  }
  
  public List<Recepie> findBy(MenuItem menuItem) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Recepie.PROP_MENU_ITEM, menuItem));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public void adjustInventory(List<Recepie> recipes) throws Exception {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      adjustInventory(recipes, session);
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  public void adjustInventory(List<Recepie> recipes, Session session) throws Exception {
    HashMap<String, Double> itemMap = new HashMap();
    HashMap<String, Double> recipeMap = new HashMap();
    for (Recepie recepie : recipes) {
      Double actualYieldValue = recepie.getYield();
      Double cookingYield = recepie.getCookingYield();
      


      MenuItem menuItem = recepie.getMenuItem();
      Double previousValue = (Double)itemMap.get(menuItem.getId());
      if (previousValue == null) {
        previousValue = Double.valueOf(0.0D);
      }
      Double toBeAdjustQty = recepie.getPortion();
      toBeAdjustQty = Double.valueOf(toBeAdjustQty.doubleValue() + previousValue.doubleValue());
      if (toBeAdjustQty.doubleValue() > 0.0D)
      {
        itemMap.put(menuItem.getId(), toBeAdjustQty);
        recepie.populateRecipeItemQuantity(recipeMap, cookingYield.doubleValue() / actualYieldValue.doubleValue());
      } }
    InventoryLocation defaultOutInventoryLocation = InventoryLocationDAO.getInstance().getDefaultOutInventoryLocation(session);
    InventoryLocation defaultInInventoryLocation = InventoryLocationDAO.getInstance().getDefaultInInventoryLocation(session);
    adjustInventory(itemMap, InventoryTransactionType.IN, "PREPARE IN", defaultInInventoryLocation, session);
    adjustInventory(recipeMap, InventoryTransactionType.OUT, "PREPARE OUT", defaultOutInventoryLocation, session);
  }
  
  public void adjustRecipeItemsFromInventory(List<Recepie> recipes, Session session) throws Exception {
    HashMap<String, Double> itemMap = new HashMap();
    HashMap<String, Double> recipeMap = new HashMap();
    for (Recepie recepie : recipes) {
      Double actualYieldValue = recepie.getYield();
      Double cookingYield = recepie.getCookingYield();
      


      MenuItem menuItem = recepie.getMenuItem();
      Double previousValue = (Double)itemMap.get(menuItem.getId());
      if (previousValue == null) {
        previousValue = Double.valueOf(0.0D);
      }
      Double toBeAdjustQty = recepie.getPortion();
      toBeAdjustQty = Double.valueOf(toBeAdjustQty.doubleValue() + previousValue.doubleValue());
      if (toBeAdjustQty.doubleValue() > 0.0D)
      {
        itemMap.put(menuItem.getId(), toBeAdjustQty);
        recepie.populateRecipeItemQuantity(recipeMap, cookingYield.doubleValue() / actualYieldValue.doubleValue());
      } }
    InventoryLocation defaultOutInventoryLocation = InventoryLocationDAO.getInstance().getDefaultOutInventoryLocation(session);
    adjustInventory(recipeMap, InventoryTransactionType.OUT, "TICKET SALES", defaultOutInventoryLocation, session);
  }
  
  private void adjustInventory(HashMap<String, Double> itemMap, InventoryTransactionType transactionType, String reason, InventoryLocation location, Session session) throws Exception
  {
    Store store = Application.getInstance().getStore();
    boolean isUpdateOnHandBlncForSale = store.isUpdateOnHandBlncForSale();
    boolean isUpdateAvailBlncForSale = store.isUpdateAvlBlncForSale();
    for (String menuItemId : itemMap.keySet()) {
      Double unitQuantity = (Double)itemMap.get(menuItemId);
      MenuItem menuItem = MenuItemDAO.getInstance().getMenuItemWithFields(menuItemId, new String[] { MenuItem.PROP_NAME, MenuItem.PROP_PRICE, MenuItem.PROP_SKU, MenuItem.PROP_BARCODE, MenuItem.PROP_UNIT_ID, MenuItem.PROP_COST, MenuItem.PROP_AVERAGE_UNIT_PURCHASE_PRICE, MenuItem.PROP_AVG_COST });
      
      if (menuItem != null)
      {

        menuItem.setId(menuItemId);
        InventoryTransaction trans = new InventoryTransaction();
        trans.setReason(reason);
        trans.setTransactionDate(new Date());
        trans.setMenuItem(menuItem);
        trans.setType(Integer.valueOf(transactionType.getType()));
        trans.setUser(Application.getCurrentUser());
        InventoryUnit unit = menuItem.getUnit();
        trans.setUnitPrice(menuItem.getPrice());
        trans.setQuantity(unitQuantity);
        if (unit != null)
          trans.setUnit(unit.getCode());
        trans.setUnitCost(menuItem.getAverageUnitPurchasePrice());
        
        if (transactionType == InventoryTransactionType.IN) {
          trans.setToInventoryLocation(location);
        }
        else {
          trans.setFromInventoryLocation(location);
        }
        trans.setTotal(Double.valueOf(trans.getUnitPrice().doubleValue() * trans.getQuantity().doubleValue()));
        InventoryTransactionDAO.getInstance().adjustInventoryStock(trans, session, isUpdateAvailBlncForSale, isUpdateOnHandBlncForSale);
      }
    }
  }
  
  public String getMenuItemName(Recepie recepie) { Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.createAlias("menuItem", "item");
      criteria.setProjection(Projections.property("item.name"));
      criteria.add(Restrictions.eq(RecipeTable.PROP_ID, recepie.getId()));
      return (String)criteria.uniqueResult();
    } finally {
      closeSession(session);
    }
  }
}
