package com.floreantpos.model.dao;

import com.floreantpos.model.DataSync;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseDataSyncDAO
  extends _RootDAO
{
  public static DataSyncDAO instance;
  
  public BaseDataSyncDAO() {}
  
  public static DataSyncDAO getInstance()
  {
    if (null == instance) instance = new DataSyncDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return DataSync.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public DataSync cast(Object object)
  {
    return (DataSync)object;
  }
  
  public DataSync get(String key) throws HibernateException
  {
    return (DataSync)get(getReferenceClass(), key);
  }
  
  public DataSync get(String key, Session s) throws HibernateException
  {
    return (DataSync)get(getReferenceClass(), key, s);
  }
  
  public DataSync load(String key) throws HibernateException
  {
    return (DataSync)load(getReferenceClass(), key);
  }
  
  public DataSync load(String key, Session s) throws HibernateException
  {
    return (DataSync)load(getReferenceClass(), key, s);
  }
  
  public DataSync loadInitialize(String key, Session s) throws HibernateException
  {
    DataSync obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<DataSync> findAll()
  {
    return super.findAll();
  }
  


  public List<DataSync> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<DataSync> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(DataSync dataSync)
    throws HibernateException
  {
    return (String)super.save(dataSync);
  }
  







  public String save(DataSync dataSync, Session s)
    throws HibernateException
  {
    return (String)save(dataSync, s);
  }
  





  public void saveOrUpdate(DataSync dataSync)
    throws HibernateException
  {
    saveOrUpdate(dataSync);
  }
  







  public void saveOrUpdate(DataSync dataSync, Session s)
    throws HibernateException
  {
    saveOrUpdate(dataSync, s);
  }
  




  public void update(DataSync dataSync)
    throws HibernateException
  {
    update(dataSync);
  }
  






  public void update(DataSync dataSync, Session s)
    throws HibernateException
  {
    update(dataSync, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(DataSync dataSync)
    throws HibernateException
  {
    delete(dataSync);
  }
  






  public void delete(DataSync dataSync, Session s)
    throws HibernateException
  {
    delete(dataSync, s);
  }
  









  public void refresh(DataSync dataSync, Session s)
    throws HibernateException
  {
    refresh(dataSync, s);
  }
}
