package com.floreantpos.model.dao;

import com.floreantpos.model.InventoryWarehouse;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseInventoryWarehouseDAO
  extends _RootDAO
{
  public static InventoryWarehouseDAO instance;
  
  public BaseInventoryWarehouseDAO() {}
  
  public static InventoryWarehouseDAO getInstance()
  {
    if (null == instance) instance = new InventoryWarehouseDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return InventoryWarehouse.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public InventoryWarehouse cast(Object object)
  {
    return (InventoryWarehouse)object;
  }
  
  public InventoryWarehouse get(String key) throws HibernateException
  {
    return (InventoryWarehouse)get(getReferenceClass(), key);
  }
  
  public InventoryWarehouse get(String key, Session s) throws HibernateException
  {
    return (InventoryWarehouse)get(getReferenceClass(), key, s);
  }
  
  public InventoryWarehouse load(String key) throws HibernateException
  {
    return (InventoryWarehouse)load(getReferenceClass(), key);
  }
  
  public InventoryWarehouse load(String key, Session s) throws HibernateException
  {
    return (InventoryWarehouse)load(getReferenceClass(), key, s);
  }
  
  public InventoryWarehouse loadInitialize(String key, Session s) throws HibernateException
  {
    InventoryWarehouse obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<InventoryWarehouse> findAll()
  {
    return super.findAll();
  }
  


  public List<InventoryWarehouse> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<InventoryWarehouse> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(InventoryWarehouse inventoryWarehouse)
    throws HibernateException
  {
    return (String)super.save(inventoryWarehouse);
  }
  







  public String save(InventoryWarehouse inventoryWarehouse, Session s)
    throws HibernateException
  {
    return (String)save(inventoryWarehouse, s);
  }
  





  public void saveOrUpdate(InventoryWarehouse inventoryWarehouse)
    throws HibernateException
  {
    saveOrUpdate(inventoryWarehouse);
  }
  







  public void saveOrUpdate(InventoryWarehouse inventoryWarehouse, Session s)
    throws HibernateException
  {
    saveOrUpdate(inventoryWarehouse, s);
  }
  




  public void update(InventoryWarehouse inventoryWarehouse)
    throws HibernateException
  {
    update(inventoryWarehouse);
  }
  






  public void update(InventoryWarehouse inventoryWarehouse, Session s)
    throws HibernateException
  {
    update(inventoryWarehouse, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(InventoryWarehouse inventoryWarehouse)
    throws HibernateException
  {
    delete(inventoryWarehouse);
  }
  






  public void delete(InventoryWarehouse inventoryWarehouse, Session s)
    throws HibernateException
  {
    delete(inventoryWarehouse, s);
  }
  









  public void refresh(InventoryWarehouse inventoryWarehouse, Session s)
    throws HibernateException
  {
    refresh(inventoryWarehouse, s);
  }
}
