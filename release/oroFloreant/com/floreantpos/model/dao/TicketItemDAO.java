package com.floreantpos.model.dao;

import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemModifier;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;






















public class TicketItemDAO
  extends BaseTicketItemDAO
{
  public TicketItemDAO() {}
  
  public boolean deleteTicketItemWithTicket(Integer itemID)
  {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(TicketItem.class);
      criteria.add(Restrictions.eq(TicketItem.PROP_MENU_ITEM_ID, itemID));
      List<TicketItem> result = criteria.list();
      if ((result == null) || (result.isEmpty())) {
        return false;
      }
      
      for (TicketItem ticketItem : result) {
        ticketItem.setTicket(null);
        super.delete(ticketItem, session);
      }
      
      return true;
    } finally {
      closeSession(session);
    }
  }
  
  public List<TicketItem> findTicketItemWithinDate(Date startDate, Date endDate, Terminal terminal, List<MenuGroup> groups, boolean isInventoryItem) {
    Session session = null;
    Criteria criteria = null;
    try {
      List groupIdList = new ArrayList();
      if (groups != null) {
        for (MenuGroup menuGroup : groups) {
          groupIdList.add(menuGroup.getId());
        }
      }
      
      session = createNewSession();
      criteria = session.createCriteria(TicketItem.class, "item").createCriteria(TicketItem.PROP_TICKET, "ticket", JoinType.INNER_JOIN);
      criteria.add(Restrictions.between("ticket." + Ticket.PROP_CREATE_DATE, startDate, endDate));
      

      if (terminal != null) {
        criteria.add(Restrictions.eq("ticket.terminalId", terminal.getId()));
      }
      
      if (!groupIdList.isEmpty()) {
        criteria.add(Restrictions.in("item." + TicketItem.PROP_GROUP_ID, groupIdList));
      }
      criteria.add(Restrictions.eq("item." + TicketItem.PROP_INVENTORY_ITEM, Boolean.valueOf(isInventoryItem)));
      
      ProjectionList pList = Projections.projectionList();
      pList.add(Projections.property("item." + TicketItem.PROP_NAME), TicketItem.PROP_NAME);
      pList.add(Projections.property("item." + TicketItem.PROP_UNIT_PRICE), TicketItem.PROP_UNIT_PRICE);
      pList.add(Projections.property("item." + TicketItem.PROP_ADJUSTED_UNIT_PRICE), TicketItem.PROP_ADJUSTED_UNIT_PRICE);
      pList.add(Projections.property("item." + TicketItem.PROP_UNIT_COST), TicketItem.PROP_UNIT_COST);
      pList.add(Projections.property("item." + TicketItem.PROP_GROUP_NAME), TicketItem.PROP_GROUP_NAME);
      pList.add(Projections.property("item." + TicketItem.PROP_MENU_ITEM_ID), TicketItem.PROP_MENU_ITEM_ID);
      pList.add(Projections.property("item." + TicketItem.PROP_QUANTITY), TicketItem.PROP_QUANTITY);
      pList.add(Projections.property("item." + TicketItem.PROP_ADJUSTED_DISCOUNT), TicketItem.PROP_ADJUSTED_DISCOUNT);
      pList.add(Projections.property("item." + TicketItem.PROP_ADJUSTED_DISCOUNT_WITHOUT_MODIFIERS), TicketItem.PROP_ADJUSTED_DISCOUNT_WITHOUT_MODIFIERS);
      pList.add(Projections.property("item." + TicketItem.PROP_ADJUSTED_SUBTOTAL_WITHOUT_MODIFIERS), TicketItem.PROP_ADJUSTED_SUBTOTAL_WITHOUT_MODIFIERS);
      pList.add(Projections.property("item." + TicketItem.PROP_ADJUSTED_SUBTOTAL), TicketItem.PROP_ADJUSTED_SUBTOTAL);
      pList.add(Projections.property("item." + TicketItem.PROP_ADJUSTED_TOTAL_WITHOUT_MODIFIERS), TicketItem.PROP_ADJUSTED_TOTAL_WITHOUT_MODIFIERS);
      pList.add(Projections.property("item." + TicketItem.PROP_ADJUSTED_TOTAL), TicketItem.PROP_ADJUSTED_TOTAL);
      pList.add(Projections.property("item." + TicketItem.PROP_ADJUSTED_TAX), TicketItem.PROP_ADJUSTED_TAX);
      pList.add(Projections.property("item." + TicketItem.PROP_ADJUSTED_TAX_WITHOUT_MODIFIERS), TicketItem.PROP_ADJUSTED_TAX_WITHOUT_MODIFIERS);
      pList.add(Projections.property("item." + TicketItem.PROP_TAX_INCLUDED), TicketItem.PROP_TAX_INCLUDED);
      criteria.setProjection(pList);
      
      criteria.setResultTransformer(Transformers.aliasToBean(TicketItem.class));
      
      criteria.addOrder(Order.asc(TicketItem.PROP_GROUP_NAME));
      criteria.addOrder(Order.asc(TicketItem.PROP_NAME));
      List<TicketItem> list = criteria.list();
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public List<TicketItemModifier> findTicketItemModifierWithinDate(Date startDate, Date endDate, Terminal terminal, List<MenuGroup> groups, boolean isInventoryItem)
  {
    Session session = null;
    Criteria criteria = null;
    try {
      List groupIdList = new ArrayList();
      if (groups != null) {
        for (MenuGroup menuGroup : groups) {
          groupIdList.add(menuGroup.getId());
        }
      }
      
      session = createNewSession();
      
      criteria = session.createCriteria(TicketItemModifier.class, "item").createCriteria(TicketItemModifier.PROP_TICKET_ITEM, "ticketItem").createCriteria(TicketItem.PROP_TICKET, "ticket", JoinType.INNER_JOIN);
      criteria.add(Restrictions.between("ticket." + Ticket.PROP_CREATE_DATE, startDate, endDate));
      criteria.add(Restrictions.eq("ticketItem." + TicketItem.PROP_INVENTORY_ITEM, Boolean.valueOf(isInventoryItem)));
      if (!groupIdList.isEmpty()) {
        criteria.add(Restrictions.in("ticketItem." + TicketItem.PROP_GROUP_ID, groupIdList));
      }
      ProjectionList pList = Projections.projectionList();
      pList.add(Projections.property("item." + TicketItemModifier.PROP_ITEM_ID), TicketItemModifier.PROP_ITEM_ID);
      pList.add(Projections.property("item." + TicketItemModifier.PROP_NAME), TicketItemModifier.PROP_NAME);
      pList.add(Projections.property("item." + TicketItemModifier.PROP_MODIFIER_TYPE), TicketItemModifier.PROP_MODIFIER_TYPE);
      pList.add(Projections.property("item." + TicketItemModifier.PROP_UNIT_PRICE), TicketItemModifier.PROP_UNIT_PRICE);
      pList.add(Projections.property("item." + TicketItemModifier.PROP_ITEM_QUANTITY), TicketItemModifier.PROP_ITEM_QUANTITY);
      pList.add(Projections.property("item." + TicketItemModifier.PROP_ADJUSTED_UNIT_PRICE), TicketItemModifier.PROP_ADJUSTED_UNIT_PRICE);
      pList.add(Projections.property("item." + TicketItemModifier.PROP_ADJUSTED_DISCOUNT), TicketItemModifier.PROP_ADJUSTED_DISCOUNT);
      pList.add(Projections.property("item." + TicketItemModifier.PROP_ADJUSTED_SUBTOTAL), TicketItemModifier.PROP_ADJUSTED_SUBTOTAL);
      pList.add(Projections.property("item." + TicketItemModifier.PROP_ADJUSTED_TOTAL), TicketItemModifier.PROP_ADJUSTED_TOTAL);
      pList.add(Projections.property("item." + TicketItemModifier.PROP_ADJUSTED_TAX), TicketItemModifier.PROP_ADJUSTED_TAX);
      pList.add(Projections.property("item." + TicketItemModifier.PROP_TAX_INCLUDED), TicketItemModifier.PROP_TAX_INCLUDED);
      pList.add(Projections.property("ticketItem." + TicketItem.PROP_QUANTITY), "ticketItemQuantity");
      criteria.setProjection(pList);
      
      criteria.setResultTransformer(Transformers.aliasToBean(TicketItemModifier.class));
      criteria.addOrder(Order.asc(TicketItemModifier.PROP_NAME));
      List<TicketItemModifier> list = criteria.list();
      return list;
    }
    finally {
      closeSession(session);
    }
  }
}
