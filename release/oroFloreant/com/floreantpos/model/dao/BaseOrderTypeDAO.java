package com.floreantpos.model.dao;

import com.floreantpos.model.OrderType;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseOrderTypeDAO
  extends _RootDAO
{
  public static OrderTypeDAO instance;
  
  public BaseOrderTypeDAO() {}
  
  public static OrderTypeDAO getInstance()
  {
    if (null == instance) instance = new OrderTypeDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return OrderType.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public OrderType cast(Object object)
  {
    return (OrderType)object;
  }
  
  public OrderType get(String key) throws HibernateException
  {
    return (OrderType)get(getReferenceClass(), key);
  }
  
  public OrderType get(String key, Session s) throws HibernateException
  {
    return (OrderType)get(getReferenceClass(), key, s);
  }
  
  public OrderType load(String key) throws HibernateException
  {
    return (OrderType)load(getReferenceClass(), key);
  }
  
  public OrderType load(String key, Session s) throws HibernateException
  {
    return (OrderType)load(getReferenceClass(), key, s);
  }
  
  public OrderType loadInitialize(String key, Session s) throws HibernateException
  {
    OrderType obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<OrderType> findAll()
  {
    return super.findAll();
  }
  


  public List<OrderType> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<OrderType> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(OrderType orderType)
    throws HibernateException
  {
    return (String)super.save(orderType);
  }
  







  public String save(OrderType orderType, Session s)
    throws HibernateException
  {
    return (String)save(orderType, s);
  }
  





  public void saveOrUpdate(OrderType orderType)
    throws HibernateException
  {
    saveOrUpdate(orderType);
  }
  







  public void saveOrUpdate(OrderType orderType, Session s)
    throws HibernateException
  {
    saveOrUpdate(orderType, s);
  }
  




  public void update(OrderType orderType)
    throws HibernateException
  {
    update(orderType);
  }
  






  public void update(OrderType orderType, Session s)
    throws HibernateException
  {
    update(orderType, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(OrderType orderType)
    throws HibernateException
  {
    delete(orderType);
  }
  






  public void delete(OrderType orderType, Session s)
    throws HibernateException
  {
    delete(orderType, s);
  }
  









  public void refresh(OrderType orderType, Session s)
    throws HibernateException
  {
    refresh(orderType, s);
  }
}
