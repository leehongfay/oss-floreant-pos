package com.floreantpos.model.dao;

import com.floreantpos.PosException;
import com.floreantpos.model.InventoryClosingBalance;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.util.DateUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DoubleType;
import org.hibernate.type.Type;



public class InventoryClosingBalanceDAO
  extends BaseInventoryClosingBalanceDAO
{
  public InventoryClosingBalanceDAO() {}
  
  public void closeStock(Date closingMonth, Date closingDate)
    throws Exception
  {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      List<InventoryClosingBalance> dataList = closeInventoryStock(session, DateUtil.endOfMonth(closingMonth), closingDate, true);
      if ((dataList == null) || (dataList.isEmpty())) {
        throw new PosException("No data found");
      }
      closingDate = DateUtil.startOfDay(closingDate);
      for (InventoryClosingBalance data : dataList) {
        data.setClosingDate(closingDate);
        session.saveOrUpdate(data);
      }
      tx.commit();
    }
    catch (Exception e)
    {
      throw e;
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public List<InventoryClosingBalance> closeInventoryStock(Session session, Date closingUpToDate, Date closingDate, boolean includeAllItems) {
    Criteria criteria = null;
    Map<String, InventoryClosingBalance> itemMap = getInventoryItems(session);
    Map<String, InventoryClosingBalance> dataMap = new HashMap();
    
    Date existingClosingDate = getExistingClosingDate(closingUpToDate, closingDate);
    Iterator iterator; if (existingClosingDate != null) {
      criteria = session.createCriteria(InventoryClosingBalance.class);
      if (existingClosingDate != null) {
        existingClosingDate = DateUtil.startOfDay(existingClosingDate);
        criteria.add(Restrictions.ge(InventoryClosingBalance.PROP_CLOSING_DATE, existingClosingDate));
        criteria.add(Restrictions.lt(InventoryClosingBalance.PROP_CLOSING_DATE, DateUtil.endOfDay(existingClosingDate)));
      }
      List dataList = criteria.list();
      if ((dataList != null) && (dataList.size() > 0)) {
        for (iterator = dataList.iterator(); iterator.hasNext();) {
          InventoryClosingBalance inventoryStockData = (InventoryClosingBalance)iterator.next();
          inventoryStockData.setBalance(Double.valueOf(0.0D));
          dataMap.put(generateKey(inventoryStockData), inventoryStockData);
        }
      }
    }
    Date lastClosingDate = getLastClosingDate(closingUpToDate);
    Iterator iterator; if (lastClosingDate != null) {
      lastClosingDate = DateUtil.startOfDay(lastClosingDate);
      criteria = session.createCriteria(InventoryClosingBalance.class);
      if (lastClosingDate != null) {
        criteria.add(Restrictions.ge(InventoryClosingBalance.PROP_CLOSING_DATE, lastClosingDate));
        criteria.add(Restrictions.lt(InventoryClosingBalance.PROP_CLOSING_DATE, DateUtil.endOfDay(lastClosingDate)));
      }
      List dataList = criteria.list();
      if ((dataList != null) && (dataList.size() > 0)) {
        for (iterator = dataList.iterator(); iterator.hasNext();) {
          InventoryClosingBalance inventoryStockData = (InventoryClosingBalance)iterator.next();
          String key = generateKey(inventoryStockData);
          InventoryClosingBalance stockData = (InventoryClosingBalance)dataMap.get(key);
          if (stockData != null) {
            stockData.setBalance(Double.valueOf(stockData.getBalance().doubleValue() + inventoryStockData.getBalance().doubleValue()));
          }
          else {
            InventoryClosingBalance nextClosingBalance = new InventoryClosingBalance();
            nextClosingBalance.setMenuItemId(inventoryStockData.getMenuItemId());
            nextClosingBalance.setLocationId(inventoryStockData.getLocationId());
            nextClosingBalance.setUnit(inventoryStockData.getUnit());
            nextClosingBalance.setBalance(inventoryStockData.getBalance());
            dataMap.put(key, nextClosingBalance);
          }
        }
      }
    }
    populateInventoryTransactions(session, dataMap, closingDate, lastClosingDate);
    List stockDataList = new ArrayList(dataMap.values());
    
    for (Iterator iterator = stockDataList.iterator(); iterator.hasNext();) {
      InventoryClosingBalance data = (InventoryClosingBalance)iterator.next();
      if ((!includeAllItems) && (data.getBalance().doubleValue() == 0.0D))
        iterator.remove();
      InventoryClosingBalance itemData = (InventoryClosingBalance)itemMap.get(data.getMenuItemId());
      if (itemData != null)
      {
        if ((includeAllItems) && (itemData != null))
          itemMap.remove(data.getMenuItemId()); }
    }
    if ((includeAllItems) && (itemMap.values().size() > 0))
      stockDataList.addAll(itemMap.values());
    return stockDataList;
  }
  
  private String generateKey(InventoryClosingBalance inventoryStockData) {
    return inventoryStockData.getMenuItemId() + inventoryStockData.getUnit() + inventoryStockData.getLocationId();
  }
  






  private void populateInventoryTransactions(Session session, Map<String, InventoryClosingBalance> dataMap, Date closingDate, Date lastClosingDate)
  {
    Criteria criteria = session.createCriteria(InventoryLocation.class);
    criteria.setProjection(Projections.property(InventoryLocation.PROP_ID));
    List locationIds = criteria.list();
    Iterator locIterator; if (locationIds != null) {
      for (locIterator = locationIds.iterator(); locIterator.hasNext();) {
        locationId = (String)locIterator.next();
        
        criteria = session.createCriteria(InventoryTransaction.class);
        criteria.createAlias(InventoryTransaction.PROP_MENU_ITEM, "item");
        
        ProjectionList projections = Projections.projectionList();
        projections.add(Projections.property("item.id"), InventoryClosingBalance.PROP_MENU_ITEM_ID);
        projections.add(Projections.property(InventoryTransaction.PROP_UNIT), InventoryClosingBalance.PROP_UNIT);
        projections.add(Projections.sqlProjection("sum(quantity*tran_type) AS " + InventoryClosingBalance.PROP_BALANCE, new String[] { InventoryClosingBalance.PROP_BALANCE }, new Type[] { new DoubleType() }));
        
        projections.add(Projections.groupProperty(InventoryTransaction.PROP_UNIT));
        projections.add(Projections.groupProperty("item.id"));
        criteria.setProjection(projections);
        
        if (lastClosingDate != null) {
          criteria.add(Restrictions.gt(InventoryTransaction.PROP_TRANSACTION_DATE, lastClosingDate));
        }
        criteria.add(Restrictions.lt(InventoryTransaction.PROP_TRANSACTION_DATE, closingDate));
        if (locationId != null)
          criteria.add(Restrictions.or(Restrictions.eq(InventoryTransaction.PROP_FROM_LOCATION_ID, locationId), 
            Restrictions.eq(InventoryTransaction.PROP_TO_LOCATION_ID, locationId)));
        criteria.setResultTransformer(Transformers.aliasToBean(InventoryClosingBalance.class));
        List dataList = criteria.list();
        if ((dataList != null) && (dataList.size() > 0))
          for (iterator = dataList.iterator(); iterator.hasNext();) {
            InventoryClosingBalance tData = (InventoryClosingBalance)iterator.next();
            tData.setLocationId(locationId);
            String key = generateKey(tData);
            InventoryClosingBalance stockData = (InventoryClosingBalance)dataMap.get(key);
            if (stockData != null) {
              stockData.setBalance(Double.valueOf(stockData.getBalance().doubleValue() + tData.getBalance().doubleValue()));
            }
            else
              dataMap.put(key, tData);
          }
      }
    }
    String locationId;
    Iterator iterator;
  }
  
  private Map<String, InventoryClosingBalance> getInventoryItems(Session session) {
    Criteria criteria = session.createCriteria(MenuItem.class);
    ProjectionList list = Projections.projectionList();
    list.add(Projections.property(MenuItem.PROP_ID), InventoryClosingBalance.PROP_MENU_ITEM_ID);
    list.add(Projections.property(MenuItem.PROP_UNIT_NAME), InventoryClosingBalance.PROP_UNIT);
    criteria.setProjection(list);
    criteria.add(Restrictions.eqOrIsNull(MenuItem.PROP_INVENTORY_ITEM, Boolean.TRUE));
    criteria.setResultTransformer(Transformers.aliasToBean(InventoryClosingBalance.class));
    List dataList = criteria.list();
    Map<String, InventoryClosingBalance> itemMap = new HashMap();
    Iterator iterator; if ((dataList != null) && (dataList.size() > 0)) {
      for (iterator = dataList.iterator(); iterator.hasNext();) {
        InventoryClosingBalance inventoryStockData = (InventoryClosingBalance)iterator.next();
        itemMap.put(inventoryStockData.getMenuItemId(), inventoryStockData);
      }
    }
    return itemMap;
  }
  
  public Date getExistingClosingDate(Date previousMonthEndDate, Date closingDate) {
    Criteria criteria = getSession().createCriteria(getReferenceClass());
    Date from = DateUtil.startOfDay(previousMonthEndDate);
    Date to = DateUtil.endOfDay(closingDate);
    criteria.setProjection(Projections.property(InventoryClosingBalance.PROP_CLOSING_DATE));
    criteria.add(Restrictions.gt(InventoryClosingBalance.PROP_CLOSING_DATE, from));
    criteria.add(Restrictions.le(InventoryClosingBalance.PROP_CLOSING_DATE, to));
    criteria.setMaxResults(1);
    return (Date)criteria.uniqueResult();
  }
  
  public Date getLastClosingDate(Date closingDate) {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.setProjection(Projections.distinct(Projections.max(InventoryClosingBalance.PROP_CLOSING_DATE)));
      criteria.add(Restrictions.lt(InventoryClosingBalance.PROP_CLOSING_DATE, closingDate));
      return (Date)criteria.uniqueResult();
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public Date getFirstInventoryTransactionDate() {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(InventoryTransaction.class);
      criteria.setProjection(Projections.distinct(Projections.min(InventoryTransaction.PROP_TRANSACTION_DATE)));
      return (Date)criteria.uniqueResult();
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
}
