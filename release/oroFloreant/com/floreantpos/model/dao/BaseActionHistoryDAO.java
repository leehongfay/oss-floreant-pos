package com.floreantpos.model.dao;

import com.floreantpos.model.ActionHistory;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseActionHistoryDAO
  extends _RootDAO
{
  public static ActionHistoryDAO instance;
  
  public BaseActionHistoryDAO() {}
  
  public static ActionHistoryDAO getInstance()
  {
    if (null == instance) instance = new ActionHistoryDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return ActionHistory.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public ActionHistory cast(Object object)
  {
    return (ActionHistory)object;
  }
  
  public ActionHistory get(String key) throws HibernateException
  {
    return (ActionHistory)get(getReferenceClass(), key);
  }
  
  public ActionHistory get(String key, Session s) throws HibernateException
  {
    return (ActionHistory)get(getReferenceClass(), key, s);
  }
  
  public ActionHistory load(String key) throws HibernateException
  {
    return (ActionHistory)load(getReferenceClass(), key);
  }
  
  public ActionHistory load(String key, Session s) throws HibernateException
  {
    return (ActionHistory)load(getReferenceClass(), key, s);
  }
  
  public ActionHistory loadInitialize(String key, Session s) throws HibernateException
  {
    ActionHistory obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<ActionHistory> findAll()
  {
    return super.findAll();
  }
  


  public List<ActionHistory> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<ActionHistory> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(ActionHistory actionHistory)
    throws HibernateException
  {
    return (String)super.save(actionHistory);
  }
  







  public String save(ActionHistory actionHistory, Session s)
    throws HibernateException
  {
    return (String)save(actionHistory, s);
  }
  





  public void saveOrUpdate(ActionHistory actionHistory)
    throws HibernateException
  {
    saveOrUpdate(actionHistory);
  }
  







  public void saveOrUpdate(ActionHistory actionHistory, Session s)
    throws HibernateException
  {
    saveOrUpdate(actionHistory, s);
  }
  




  public void update(ActionHistory actionHistory)
    throws HibernateException
  {
    update(actionHistory);
  }
  






  public void update(ActionHistory actionHistory, Session s)
    throws HibernateException
  {
    update(actionHistory, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(ActionHistory actionHistory)
    throws HibernateException
  {
    delete(actionHistory);
  }
  






  public void delete(ActionHistory actionHistory, Session s)
    throws HibernateException
  {
    delete(actionHistory, s);
  }
  









  public void refresh(ActionHistory actionHistory, Session s)
    throws HibernateException
  {
    refresh(actionHistory, s);
  }
}
