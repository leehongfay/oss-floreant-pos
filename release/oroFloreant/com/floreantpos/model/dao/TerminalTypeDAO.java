package com.floreantpos.model.dao;

import com.floreantpos.main.Application;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.TerminalType;
import com.floreantpos.teminaltype.TerminalCategory;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;



public class TerminalTypeDAO
  extends BaseTerminalTypeDAO
{
  public TerminalTypeDAO() {}
  
  public void initialize(TerminalType terminalType)
  {
    if ((terminalType == null) || (terminalType.getId() == null)) {
      return;
    }
    if ((Hibernate.isInitialized(terminalType.getOrderTypes())) && (Hibernate.isInitialized(terminalType.getCategories()))) {
      return;
    }
    Session session = null;
    try
    {
      session = createNewSession();
      session.refresh(terminalType);
      
      Hibernate.initialize(terminalType.getOrderTypes());
      Hibernate.initialize(terminalType.getCategories());
      
      closeSession(session); } finally { closeSession(session);
    }
  }
  
  public boolean nameExists(TerminalType terminalType, String name) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(TerminalType.PROP_NAME, name).ignoreCase());
      if (terminalType.getId() != null) {
        criteria.add(Restrictions.ne(TerminalType.PROP_ID, terminalType.getId()));
      }
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      boolean bool;
      if (rowCount == null) {
        return false;
      }
      return rowCount.intValue() > 0;
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public TerminalType getTerminalType(TerminalCategory category) {
    if (category == null) {
      TerminalType thisTerminalType = Application.getInstance().getTerminal().getTerminalType();
      if (thisTerminalType != null) {
        return thisTerminalType;
      }
      category = TerminalCategory.REGULAR;
    }
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(TerminalType.PROP_TYPE, category.getType()));
      
      List list = criteria.list();
      TerminalType localTerminalType1; if ((list != null) && (list.size() > 0)) {
        return (TerminalType)list.get(0);
      }
      return null;
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
}
