package com.floreantpos.model.dao;

import com.floreantpos.model.RefundTransaction;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseRefundTransactionDAO
  extends _RootDAO
{
  public static RefundTransactionDAO instance;
  
  public BaseRefundTransactionDAO() {}
  
  public static RefundTransactionDAO getInstance()
  {
    if (null == instance) instance = new RefundTransactionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return RefundTransaction.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public RefundTransaction cast(Object object)
  {
    return (RefundTransaction)object;
  }
  
  public RefundTransaction get(String key) throws HibernateException
  {
    return (RefundTransaction)get(getReferenceClass(), key);
  }
  
  public RefundTransaction get(String key, Session s) throws HibernateException
  {
    return (RefundTransaction)get(getReferenceClass(), key, s);
  }
  
  public RefundTransaction load(String key) throws HibernateException
  {
    return (RefundTransaction)load(getReferenceClass(), key);
  }
  
  public RefundTransaction load(String key, Session s) throws HibernateException
  {
    return (RefundTransaction)load(getReferenceClass(), key, s);
  }
  
  public RefundTransaction loadInitialize(String key, Session s) throws HibernateException
  {
    RefundTransaction obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<RefundTransaction> findAll()
  {
    return super.findAll();
  }
  


  public List<RefundTransaction> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<RefundTransaction> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(RefundTransaction refundTransaction)
    throws HibernateException
  {
    return (String)super.save(refundTransaction);
  }
  







  public String save(RefundTransaction refundTransaction, Session s)
    throws HibernateException
  {
    return (String)save(refundTransaction, s);
  }
  





  public void saveOrUpdate(RefundTransaction refundTransaction)
    throws HibernateException
  {
    saveOrUpdate(refundTransaction);
  }
  







  public void saveOrUpdate(RefundTransaction refundTransaction, Session s)
    throws HibernateException
  {
    saveOrUpdate(refundTransaction, s);
  }
  




  public void update(RefundTransaction refundTransaction)
    throws HibernateException
  {
    update(refundTransaction);
  }
  






  public void update(RefundTransaction refundTransaction, Session s)
    throws HibernateException
  {
    update(refundTransaction, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(RefundTransaction refundTransaction)
    throws HibernateException
  {
    delete(refundTransaction);
  }
  






  public void delete(RefundTransaction refundTransaction, Session s)
    throws HibernateException
  {
    delete(refundTransaction, s);
  }
  









  public void refresh(RefundTransaction refundTransaction, Session s)
    throws HibernateException
  {
    refresh(refundTransaction, s);
  }
}
