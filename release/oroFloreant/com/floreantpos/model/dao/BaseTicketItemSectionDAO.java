package com.floreantpos.model.dao;

import com.floreantpos.model.TicketItemSection;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseTicketItemSectionDAO
  extends _RootDAO
{
  public static TicketItemSectionDAO instance;
  
  public BaseTicketItemSectionDAO() {}
  
  public static TicketItemSectionDAO getInstance()
  {
    if (null == instance) instance = new TicketItemSectionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return TicketItemSection.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public TicketItemSection cast(Object object)
  {
    return (TicketItemSection)object;
  }
  
  public TicketItemSection get(String key) throws HibernateException
  {
    return (TicketItemSection)get(getReferenceClass(), key);
  }
  
  public TicketItemSection get(String key, Session s) throws HibernateException
  {
    return (TicketItemSection)get(getReferenceClass(), key, s);
  }
  
  public TicketItemSection load(String key) throws HibernateException
  {
    return (TicketItemSection)load(getReferenceClass(), key);
  }
  
  public TicketItemSection load(String key, Session s) throws HibernateException
  {
    return (TicketItemSection)load(getReferenceClass(), key, s);
  }
  
  public TicketItemSection loadInitialize(String key, Session s) throws HibernateException
  {
    TicketItemSection obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<TicketItemSection> findAll()
  {
    return super.findAll();
  }
  


  public List<TicketItemSection> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<TicketItemSection> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(TicketItemSection ticketItemSection)
    throws HibernateException
  {
    return (String)super.save(ticketItemSection);
  }
  







  public String save(TicketItemSection ticketItemSection, Session s)
    throws HibernateException
  {
    return (String)save(ticketItemSection, s);
  }
  





  public void saveOrUpdate(TicketItemSection ticketItemSection)
    throws HibernateException
  {
    saveOrUpdate(ticketItemSection);
  }
  







  public void saveOrUpdate(TicketItemSection ticketItemSection, Session s)
    throws HibernateException
  {
    saveOrUpdate(ticketItemSection, s);
  }
  




  public void update(TicketItemSection ticketItemSection)
    throws HibernateException
  {
    update(ticketItemSection);
  }
  






  public void update(TicketItemSection ticketItemSection, Session s)
    throws HibernateException
  {
    update(ticketItemSection, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(TicketItemSection ticketItemSection)
    throws HibernateException
  {
    delete(ticketItemSection);
  }
  






  public void delete(TicketItemSection ticketItemSection, Session s)
    throws HibernateException
  {
    delete(ticketItemSection, s);
  }
  









  public void refresh(TicketItemSection ticketItemSection, Session s)
    throws HibernateException
  {
    refresh(ticketItemSection, s);
  }
}
