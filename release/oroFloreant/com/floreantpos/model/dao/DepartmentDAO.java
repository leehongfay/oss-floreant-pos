package com.floreantpos.model.dao;

import com.floreantpos.PosLog;
import com.floreantpos.model.Department;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Outlet;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;





public class DepartmentDAO
  extends BaseDepartmentDAO
{
  public DepartmentDAO() {}
  
  public Department initialize(Department dept)
  {
    if ((dept == null) || (dept.getId() == null)) {
      return dept;
    }
    Session session = null;
    try
    {
      session = createNewSession();
      session.refresh(dept);
      
      Hibernate.initialize(dept.getOutlets());
      
      return dept;
    } finally {
      closeSession(session);
    }
  }
  
  public List<Department> findByOrderType(OrderType orderType) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = getSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.createAlias("orderTypes", "o");
      criteria.add(Restrictions.eq("o.id", orderType.getId()));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public boolean existsDepartment(OrderType orderType) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.setProjection(Projections.rowCount());
      
      if (orderType != null) {
        criteria.createAlias("orderTypes", "o");
        criteria.add(Restrictions.eq("o.id", orderType.getId()));
      }
      Number rowCount = (Number)criteria.uniqueResult();
      return (rowCount != null) && (rowCount.intValue() > 0);
    } finally {
      closeSession(session);
    }
  }
  
  public List<Department> findDepartmentbyOutlet(Outlet outlet) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      
      List list = criteria.list();
      Object localObject1; if ((list == null) || (list.size() == 0)) {
        return null;
      }
      return list;
    }
    catch (Exception e) {
      PosLog.error(DepartmentDAO.class, e);
    } finally {
      if (session != null) {
        session.close();
      }
    }
    return null;
  }
}
