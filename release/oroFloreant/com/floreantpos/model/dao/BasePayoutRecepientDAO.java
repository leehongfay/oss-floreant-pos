package com.floreantpos.model.dao;

import com.floreantpos.model.PayoutRecepient;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BasePayoutRecepientDAO
  extends _RootDAO
{
  public static PayoutRecepientDAO instance;
  
  public BasePayoutRecepientDAO() {}
  
  public static PayoutRecepientDAO getInstance()
  {
    if (null == instance) instance = new PayoutRecepientDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return PayoutRecepient.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public PayoutRecepient cast(Object object)
  {
    return (PayoutRecepient)object;
  }
  
  public PayoutRecepient get(String key) throws HibernateException
  {
    return (PayoutRecepient)get(getReferenceClass(), key);
  }
  
  public PayoutRecepient get(String key, Session s) throws HibernateException
  {
    return (PayoutRecepient)get(getReferenceClass(), key, s);
  }
  
  public PayoutRecepient load(String key) throws HibernateException
  {
    return (PayoutRecepient)load(getReferenceClass(), key);
  }
  
  public PayoutRecepient load(String key, Session s) throws HibernateException
  {
    return (PayoutRecepient)load(getReferenceClass(), key, s);
  }
  
  public PayoutRecepient loadInitialize(String key, Session s) throws HibernateException
  {
    PayoutRecepient obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<PayoutRecepient> findAll()
  {
    return super.findAll();
  }
  


  public List<PayoutRecepient> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<PayoutRecepient> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(PayoutRecepient payoutRecepient)
    throws HibernateException
  {
    return (String)super.save(payoutRecepient);
  }
  







  public String save(PayoutRecepient payoutRecepient, Session s)
    throws HibernateException
  {
    return (String)save(payoutRecepient, s);
  }
  





  public void saveOrUpdate(PayoutRecepient payoutRecepient)
    throws HibernateException
  {
    saveOrUpdate(payoutRecepient);
  }
  







  public void saveOrUpdate(PayoutRecepient payoutRecepient, Session s)
    throws HibernateException
  {
    saveOrUpdate(payoutRecepient, s);
  }
  




  public void update(PayoutRecepient payoutRecepient)
    throws HibernateException
  {
    update(payoutRecepient);
  }
  






  public void update(PayoutRecepient payoutRecepient, Session s)
    throws HibernateException
  {
    update(payoutRecepient, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(PayoutRecepient payoutRecepient)
    throws HibernateException
  {
    delete(payoutRecepient);
  }
  






  public void delete(PayoutRecepient payoutRecepient, Session s)
    throws HibernateException
  {
    delete(payoutRecepient, s);
  }
  









  public void refresh(PayoutRecepient payoutRecepient, Session s)
    throws HibernateException
  {
    refresh(payoutRecepient, s);
  }
}
