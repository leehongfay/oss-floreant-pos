package com.floreantpos.model.dao;

import com.floreantpos.model.TaxGroup;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseTaxGroupDAO
  extends _RootDAO
{
  public static TaxGroupDAO instance;
  
  public BaseTaxGroupDAO() {}
  
  public static TaxGroupDAO getInstance()
  {
    if (null == instance) instance = new TaxGroupDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return TaxGroup.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public TaxGroup cast(Object object)
  {
    return (TaxGroup)object;
  }
  
  public TaxGroup get(String key) throws HibernateException
  {
    return (TaxGroup)get(getReferenceClass(), key);
  }
  
  public TaxGroup get(String key, Session s) throws HibernateException
  {
    return (TaxGroup)get(getReferenceClass(), key, s);
  }
  
  public TaxGroup load(String key) throws HibernateException
  {
    return (TaxGroup)load(getReferenceClass(), key);
  }
  
  public TaxGroup load(String key, Session s) throws HibernateException
  {
    return (TaxGroup)load(getReferenceClass(), key, s);
  }
  
  public TaxGroup loadInitialize(String key, Session s) throws HibernateException
  {
    TaxGroup obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<TaxGroup> findAll()
  {
    return super.findAll();
  }
  


  public List<TaxGroup> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<TaxGroup> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(TaxGroup taxGroup)
    throws HibernateException
  {
    return (String)super.save(taxGroup);
  }
  







  public String save(TaxGroup taxGroup, Session s)
    throws HibernateException
  {
    return (String)save(taxGroup, s);
  }
  





  public void saveOrUpdate(TaxGroup taxGroup)
    throws HibernateException
  {
    saveOrUpdate(taxGroup);
  }
  







  public void saveOrUpdate(TaxGroup taxGroup, Session s)
    throws HibernateException
  {
    saveOrUpdate(taxGroup, s);
  }
  




  public void update(TaxGroup taxGroup)
    throws HibernateException
  {
    update(taxGroup);
  }
  






  public void update(TaxGroup taxGroup, Session s)
    throws HibernateException
  {
    update(taxGroup, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(TaxGroup taxGroup)
    throws HibernateException
  {
    delete(taxGroup);
  }
  






  public void delete(TaxGroup taxGroup, Session s)
    throws HibernateException
  {
    delete(taxGroup, s);
  }
  









  public void refresh(TaxGroup taxGroup, Session s)
    throws HibernateException
  {
    refresh(taxGroup, s);
  }
}
