package com.floreantpos.model.dao;

import com.floreantpos.model.Gratuity;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseGratuityDAO
  extends _RootDAO
{
  public static GratuityDAO instance;
  
  public BaseGratuityDAO() {}
  
  public static GratuityDAO getInstance()
  {
    if (null == instance) instance = new GratuityDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return Gratuity.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public Gratuity cast(Object object)
  {
    return (Gratuity)object;
  }
  
  public Gratuity get(String key) throws HibernateException
  {
    return (Gratuity)get(getReferenceClass(), key);
  }
  
  public Gratuity get(String key, Session s) throws HibernateException
  {
    return (Gratuity)get(getReferenceClass(), key, s);
  }
  
  public Gratuity load(String key) throws HibernateException
  {
    return (Gratuity)load(getReferenceClass(), key);
  }
  
  public Gratuity load(String key, Session s) throws HibernateException
  {
    return (Gratuity)load(getReferenceClass(), key, s);
  }
  
  public Gratuity loadInitialize(String key, Session s) throws HibernateException
  {
    Gratuity obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<Gratuity> findAll()
  {
    return super.findAll();
  }
  


  public List<Gratuity> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<Gratuity> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(Gratuity gratuity)
    throws HibernateException
  {
    return (String)super.save(gratuity);
  }
  







  public String save(Gratuity gratuity, Session s)
    throws HibernateException
  {
    return (String)save(gratuity, s);
  }
  





  public void saveOrUpdate(Gratuity gratuity)
    throws HibernateException
  {
    saveOrUpdate(gratuity);
  }
  







  public void saveOrUpdate(Gratuity gratuity, Session s)
    throws HibernateException
  {
    saveOrUpdate(gratuity, s);
  }
  




  public void update(Gratuity gratuity)
    throws HibernateException
  {
    update(gratuity);
  }
  






  public void update(Gratuity gratuity, Session s)
    throws HibernateException
  {
    update(gratuity, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(Gratuity gratuity)
    throws HibernateException
  {
    delete(gratuity);
  }
  






  public void delete(Gratuity gratuity, Session s)
    throws HibernateException
  {
    delete(gratuity, s);
  }
  









  public void refresh(Gratuity gratuity, Session s)
    throws HibernateException
  {
    refresh(gratuity, s);
  }
}
