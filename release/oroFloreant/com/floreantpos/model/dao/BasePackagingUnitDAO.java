package com.floreantpos.model.dao;

import com.floreantpos.model.PackagingUnit;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BasePackagingUnitDAO
  extends _RootDAO
{
  public static PackagingUnitDAO instance;
  
  public BasePackagingUnitDAO() {}
  
  public static PackagingUnitDAO getInstance()
  {
    if (null == instance) instance = new PackagingUnitDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return PackagingUnit.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public PackagingUnit cast(Object object)
  {
    return (PackagingUnit)object;
  }
  
  public PackagingUnit get(String key) throws HibernateException
  {
    return (PackagingUnit)get(getReferenceClass(), key);
  }
  
  public PackagingUnit get(String key, Session s) throws HibernateException
  {
    return (PackagingUnit)get(getReferenceClass(), key, s);
  }
  
  public PackagingUnit load(String key) throws HibernateException
  {
    return (PackagingUnit)load(getReferenceClass(), key);
  }
  
  public PackagingUnit load(String key, Session s) throws HibernateException
  {
    return (PackagingUnit)load(getReferenceClass(), key, s);
  }
  
  public PackagingUnit loadInitialize(String key, Session s) throws HibernateException
  {
    PackagingUnit obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<PackagingUnit> findAll()
  {
    return super.findAll();
  }
  


  public List<PackagingUnit> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<PackagingUnit> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(PackagingUnit packagingUnit)
    throws HibernateException
  {
    return (String)super.save(packagingUnit);
  }
  







  public String save(PackagingUnit packagingUnit, Session s)
    throws HibernateException
  {
    return (String)save(packagingUnit, s);
  }
  





  public void saveOrUpdate(PackagingUnit packagingUnit)
    throws HibernateException
  {
    saveOrUpdate(packagingUnit);
  }
  







  public void saveOrUpdate(PackagingUnit packagingUnit, Session s)
    throws HibernateException
  {
    saveOrUpdate(packagingUnit, s);
  }
  




  public void update(PackagingUnit packagingUnit)
    throws HibernateException
  {
    update(packagingUnit);
  }
  






  public void update(PackagingUnit packagingUnit, Session s)
    throws HibernateException
  {
    update(packagingUnit, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(PackagingUnit packagingUnit)
    throws HibernateException
  {
    delete(packagingUnit);
  }
  






  public void delete(PackagingUnit packagingUnit, Session s)
    throws HibernateException
  {
    delete(packagingUnit, s);
  }
  









  public void refresh(PackagingUnit packagingUnit, Session s)
    throws HibernateException
  {
    refresh(packagingUnit, s);
  }
}
