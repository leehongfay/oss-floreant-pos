package com.floreantpos.model.dao;

import com.floreantpos.model.ComboGroup;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseComboGroupDAO
  extends _RootDAO
{
  public static ComboGroupDAO instance;
  
  public BaseComboGroupDAO() {}
  
  public static ComboGroupDAO getInstance()
  {
    if (null == instance) instance = new ComboGroupDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return ComboGroup.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public ComboGroup cast(Object object)
  {
    return (ComboGroup)object;
  }
  
  public ComboGroup get(String key) throws HibernateException
  {
    return (ComboGroup)get(getReferenceClass(), key);
  }
  
  public ComboGroup get(String key, Session s) throws HibernateException
  {
    return (ComboGroup)get(getReferenceClass(), key, s);
  }
  
  public ComboGroup load(String key) throws HibernateException
  {
    return (ComboGroup)load(getReferenceClass(), key);
  }
  
  public ComboGroup load(String key, Session s) throws HibernateException
  {
    return (ComboGroup)load(getReferenceClass(), key, s);
  }
  
  public ComboGroup loadInitialize(String key, Session s) throws HibernateException
  {
    ComboGroup obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<ComboGroup> findAll()
  {
    return super.findAll();
  }
  


  public List<ComboGroup> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<ComboGroup> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(ComboGroup comboGroup)
    throws HibernateException
  {
    return (String)super.save(comboGroup);
  }
  







  public String save(ComboGroup comboGroup, Session s)
    throws HibernateException
  {
    return (String)save(comboGroup, s);
  }
  





  public void saveOrUpdate(ComboGroup comboGroup)
    throws HibernateException
  {
    saveOrUpdate(comboGroup);
  }
  







  public void saveOrUpdate(ComboGroup comboGroup, Session s)
    throws HibernateException
  {
    saveOrUpdate(comboGroup, s);
  }
  




  public void update(ComboGroup comboGroup)
    throws HibernateException
  {
    update(comboGroup);
  }
  






  public void update(ComboGroup comboGroup, Session s)
    throws HibernateException
  {
    update(comboGroup, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(ComboGroup comboGroup)
    throws HibernateException
  {
    delete(comboGroup);
  }
  






  public void delete(ComboGroup comboGroup, Session s)
    throws HibernateException
  {
    delete(comboGroup, s);
  }
  









  public void refresh(ComboGroup comboGroup, Session s)
    throws HibernateException
  {
    refresh(comboGroup, s);
  }
}
