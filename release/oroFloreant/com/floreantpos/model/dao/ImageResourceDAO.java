package com.floreantpos.model.dao;

import com.floreantpos.model.ImageResource;
import com.floreantpos.model.ImageResource.IMAGE_CATEGORY;
import com.floreantpos.swing.PaginatedTableModel;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.serial.SerialBlob;
import net.authorize.util.StringUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;





public class ImageResourceDAO
  extends BaseImageResourceDAO
{
  public ImageResourceDAO() {}
  
  public int rowCount()
  {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(ImageResource.class);
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      int i; if (rowCount != null) {
        return rowCount.intValue();
      }
      
      return 0;
    } finally {
      closeSession(session);
    }
  }
  
  public void getImages(PaginatedTableModel model, ImageResource.IMAGE_CATEGORY imgCategory, String description) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(ImageResource.class);
      criteria.addOrder(Order.asc(ImageResource.PROP_ID));
      if (imgCategory != null) {
        criteria.add(Restrictions.eq(ImageResource.PROP_IMAGE_CATEGORY_NUM, Integer.valueOf(imgCategory.getType())));
      }
      if (StringUtils.isNotEmpty(description)) {
        criteria.add(Restrictions.ilike(ImageResource.PROP_DESCRIPTION, description, MatchMode.START));
      }
      if (imgCategory != ImageResource.IMAGE_CATEGORY.DELETED) {
        criteria.add(Restrictions.ne(ImageResource.PROP_IMAGE_CATEGORY_NUM, Integer.valueOf(ImageResource.IMAGE_CATEGORY.DELETED.getType())));
      }
      int currentRowIndex = model.getCurrentRowIndex();
      criteria.setFirstResult(currentRowIndex);
      criteria.setMaxResults(model.getPageSize());
      List<ImageResource> result = criteria.list();
      model.setRows(result);
    } catch (Exception e) {
      model.setRows(new ArrayList());
    } finally {
      closeSession(session);
    }
  }
  
  public ImageResource getDefaultFloorImage(InputStream inputStream) throws Exception {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(ImageResource.class);
      criteria.addOrder(Order.asc(ImageResource.PROP_ID));
      criteria.add(Restrictions.eq(ImageResource.PROP_IMAGE_CATEGORY_NUM, Integer.valueOf(ImageResource.IMAGE_CATEGORY.FLOORPLAN.getType())));
      criteria.add(Restrictions.eq(ImageResource.PROP_DESCRIPTION, "default_floor_image"));
      List list = criteria.list();
      if (list.size() > 0) {
        return (ImageResource)list.get(0);
      }
      
      if (inputStream != null) {
        ImageResource imageResource = new ImageResource();
        byte[] byteArray = IOUtils.toByteArray(inputStream);
        imageResource.setImageData(new SerialBlob(byteArray));
        imageResource.setDescription("default_floor_image");
        imageResource.setImageCategoryNum(Integer.valueOf(ImageResource.IMAGE_CATEGORY.FLOORPLAN.getType()));
        save(imageResource);
        return imageResource;
      }
    }
    finally {
      closeSession(session);
      if (inputStream != null) {
        inputStream.close();
      }
    }
    return null;
  }
  
  public ImageResource findById(String id) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(ImageResource.class);
      criteria.add(Restrictions.eq(ImageResource.PROP_ID, id));
      List<ImageResource> result = criteria.list();
      if ((result == null) || (result.isEmpty())) {
        return null;
      }
      return (ImageResource)result.get(0);
    } catch (Exception e) { ImageResource localImageResource;
      return null;
    } finally {
      closeSession(session);
    }
  }
  
  public void saveOrUpdate(List<ImageResource> resourceList) {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      
      for (ImageResource resource : resourceList) {
        session.saveOrUpdate(resource);
      }
      
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(ImageResourceDAO.class).error(e);
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
}
