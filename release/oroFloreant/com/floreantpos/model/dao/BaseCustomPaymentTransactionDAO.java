package com.floreantpos.model.dao;

import com.floreantpos.model.CustomPaymentTransaction;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseCustomPaymentTransactionDAO
  extends _RootDAO
{
  public static CustomPaymentTransactionDAO instance;
  
  public BaseCustomPaymentTransactionDAO() {}
  
  public static CustomPaymentTransactionDAO getInstance()
  {
    if (null == instance) instance = new CustomPaymentTransactionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return CustomPaymentTransaction.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public CustomPaymentTransaction cast(Object object)
  {
    return (CustomPaymentTransaction)object;
  }
  
  public CustomPaymentTransaction get(String key) throws HibernateException
  {
    return (CustomPaymentTransaction)get(getReferenceClass(), key);
  }
  
  public CustomPaymentTransaction get(String key, Session s) throws HibernateException
  {
    return (CustomPaymentTransaction)get(getReferenceClass(), key, s);
  }
  
  public CustomPaymentTransaction load(String key) throws HibernateException
  {
    return (CustomPaymentTransaction)load(getReferenceClass(), key);
  }
  
  public CustomPaymentTransaction load(String key, Session s) throws HibernateException
  {
    return (CustomPaymentTransaction)load(getReferenceClass(), key, s);
  }
  
  public CustomPaymentTransaction loadInitialize(String key, Session s) throws HibernateException
  {
    CustomPaymentTransaction obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<CustomPaymentTransaction> findAll()
  {
    return super.findAll();
  }
  


  public List<CustomPaymentTransaction> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<CustomPaymentTransaction> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(CustomPaymentTransaction customPaymentTransaction)
    throws HibernateException
  {
    return (String)super.save(customPaymentTransaction);
  }
  







  public String save(CustomPaymentTransaction customPaymentTransaction, Session s)
    throws HibernateException
  {
    return (String)save(customPaymentTransaction, s);
  }
  





  public void saveOrUpdate(CustomPaymentTransaction customPaymentTransaction)
    throws HibernateException
  {
    saveOrUpdate(customPaymentTransaction);
  }
  







  public void saveOrUpdate(CustomPaymentTransaction customPaymentTransaction, Session s)
    throws HibernateException
  {
    saveOrUpdate(customPaymentTransaction, s);
  }
  




  public void update(CustomPaymentTransaction customPaymentTransaction)
    throws HibernateException
  {
    update(customPaymentTransaction);
  }
  






  public void update(CustomPaymentTransaction customPaymentTransaction, Session s)
    throws HibernateException
  {
    update(customPaymentTransaction, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(CustomPaymentTransaction customPaymentTransaction)
    throws HibernateException
  {
    delete(customPaymentTransaction);
  }
  






  public void delete(CustomPaymentTransaction customPaymentTransaction, Session s)
    throws HibernateException
  {
    delete(customPaymentTransaction, s);
  }
  









  public void refresh(CustomPaymentTransaction customPaymentTransaction, Session s)
    throws HibernateException
  {
    refresh(customPaymentTransaction, s);
  }
}
