package com.floreantpos.model.dao;

import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.DrawerPullVoidEntry;
import com.floreantpos.model.KitchenTicket;
import com.floreantpos.model.KitchenTicketItem;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.VoidItem;
import com.floreantpos.model.ext.KitchenStatus;
import com.floreantpos.util.CopyUtil;
import com.floreantpos.util.NumberUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.ResultTransformer;



public class VoidItemDAO
  extends BaseVoidItemDAO
{
  public VoidItemDAO() {}
  
  public void saveVoidItems(List<VoidItem> voidItems, Ticket ticket)
  {
    Session session = null;
    Transaction tx = null;
    try
    {
      CashDrawer activeDrawer = Application.getCurrentUser().getActiveDrawerPullReport();
      session = createNewSession();
      tx = session.beginTransaction();
      for (VoidItem voidItem : voidItems) {
        Double totalPrice = Double.valueOf(Math.abs(voidItem.getTotalPrice().doubleValue()));
        ticket.setVoidAmount(Double.valueOf(ticket.getVoidAmount().doubleValue() + totalPrice.doubleValue()));
        
        voidItem.setTicketId(ticket.getId());
        voidItem.setCashDrawerId(activeDrawer.getId());
        if (voidItem.getItemId() != null) {
          List<KitchenTicketItem> kitchenTicketItems = KitchenTicketItemDAO.getInstance().find(voidItem.getItemId(), voidItem.isModifier().booleanValue(), session);
          if (kitchenTicketItems != null) {
            for (KitchenTicketItem kitchenItem : kitchenTicketItems) {
              KitchenTicket kitchenTicket = kitchenItem.getKitchenTicket();
              if (voidItem.getQuantity().doubleValue() < kitchenItem.getQuantity().doubleValue()) {
                KitchenTicketItem newKitchenTicketItem = (KitchenTicketItem)CopyUtil.deepCopy(kitchenItem);
                newKitchenTicketItem.setId(null);
                newKitchenTicketItem.setQuantity(Double.valueOf(kitchenItem.getQuantity().doubleValue() - voidItem.getQuantity().doubleValue()));
                kitchenTicket.addToticketItems(newKitchenTicketItem);
                session.save(newKitchenTicketItem);
              }
              if (kitchenItem.getStatus().equalsIgnoreCase(KitchenStatus.BUMP.name())) {
                voidItem.setCooked(true);
              }
              kitchenItem.setVoided(Boolean.valueOf(true));
              session.update(kitchenItem);
            }
          }
        }
        session.saveOrUpdate(voidItem);
      }
      TicketDAO.getInstance().updateStock(ticket, session);
      session.saveOrUpdate(ticket);
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      
      throwException(e);
    }
    finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public void saveVoidItems(List<VoidItem> voidItems, Session session) {
    for (VoidItem voidItem : voidItems) {
      session.saveOrUpdate(voidItem);
    }
  }
  
  public List<VoidItem> findByDate(Date startDate, Date endDate, Terminal terminal) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.ge(VoidItem.PROP_VOID_DATE, startDate));
      criteria.add(Restrictions.le(VoidItem.PROP_VOID_DATE, endDate));
      
      if (terminal != null) {
        criteria.add(Restrictions.eq(VoidItem.PROP_TERMINAL, terminal));
      }
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<DrawerPullVoidEntry> getVoidEntries(CashDrawer cashDrawer) {
    List<String> cashDrawerIds = new ArrayList();
    cashDrawerIds.add(cashDrawer.getId());
    return getVoidEntries(cashDrawerIds);
  }
  
  public List<DrawerPullVoidEntry> getVoidEntries(List<String> cashDrawerIds) {
    if (cashDrawerIds.isEmpty())
      return new ArrayList();
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      ProjectionList projectionList = Projections.projectionList();
      projectionList.add(Projections.countDistinct(VoidItem.PROP_TICKET_ID));
      projectionList.add(Projections.sum(VoidItem.PROP_QUANTITY));
      projectionList.add(Projections.sum(VoidItem.PROP_TOTAL_PRICE));
      projectionList.add(Projections.groupProperty(VoidItem.PROP_ITEM_WASTED));
      Object localObject1; if (cashDrawerIds.size() == 1) {
        criteria.add(Restrictions.eq(VoidItem.PROP_CASH_DRAWER_ID, cashDrawerIds.get(0)));
      }
      else {
        Disjunction disjunction = Restrictions.disjunction();
        for (localObject1 = cashDrawerIds.iterator(); ((Iterator)localObject1).hasNext();) { String cashDrawerId = (String)((Iterator)localObject1).next();
          disjunction.add(Restrictions.eq(VoidItem.PROP_CASH_DRAWER_ID, cashDrawerId));
        }
        criteria.add(Restrictions.and(Restrictions.isNotNull(VoidItem.PROP_CASH_DRAWER_ID), disjunction));
      }
      ResultTransformer transformer = new ResultTransformer()
      {
        public Object transformTuple(Object[] row, String[] arg1) {
          DrawerPullVoidEntry voidEntry = new DrawerPullVoidEntry();
          voidEntry.setCheckCount(Double.valueOf("" + row[0]));
          voidEntry.setQuantity(Double.valueOf("" + row[1]).doubleValue());
          voidEntry.setAmount(Double.valueOf("" + row[2]));
          voidEntry.setReason((Boolean.valueOf("" + row[3]).booleanValue() == true ? "TOTAL WASTE" : "TOTAL VOID") + (voidEntry
            .getCheckCount().doubleValue() > 0.0D ? " (" + NumberUtil.trimDecilamIfNotNeeded(voidEntry.getCheckCount()) + ")" : ""));
          return voidEntry;
        }
        
        public List transformList(List arg0) {
          return arg0;
        }
      };
      criteria.setProjection(projectionList).setResultTransformer(transformer);
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<VoidItem> getVoidItems(String ticketId)
  {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(VoidItem.PROP_TICKET_ID, ticketId));
      
      List list = criteria.list();
      Object localObject1; if (list.isEmpty()) {
        return null;
      }
      return list;
    }
    finally {
      closeSession(session);
    }
  }
  
  public void saveAndSentToKitchenIfNeeded(VoidItem voidItem, boolean printedToKitchen, Ticket ticket, Session session) {
    Double totalPrice = Double.valueOf(Math.abs(voidItem.getTotalPrice().doubleValue()));
    ticket.setVoidAmount(Double.valueOf(ticket.getVoidAmount().doubleValue() + totalPrice.doubleValue()));
    ticket.setRefundableAmount(Double.valueOf(ticket.getRefundableAmount().doubleValue() + totalPrice.doubleValue()));
    voidItem.setTicketId(ticket.getId());
    if (voidItem.getItemId() != null) {
      List<KitchenTicketItem> kitchenTicketItems = KitchenTicketItemDAO.getInstance().find(voidItem.getItemId(), voidItem.isModifier().booleanValue(), session);
      if (kitchenTicketItems != null) {
        Set<KitchenTicket> kitchenTickets = new HashSet();
        for (KitchenTicketItem kitchenItem : kitchenTicketItems) {
          KitchenTicket kitchenTicket = kitchenItem.getKitchenTicket();
          kitchenTickets.add(kitchenTicket);
          
          if (voidItem.getQuantity().doubleValue() < kitchenItem.getQuantity().doubleValue()) {
            try {
              KitchenTicketItem newKitchenTicketItem = (KitchenTicketItem)CopyUtil.deepCopy(kitchenItem);
              newKitchenTicketItem.setId(null);
              newKitchenTicketItem.setQuantity(Double.valueOf(kitchenItem.getQuantity().doubleValue() - voidItem.getQuantity().doubleValue()));
              kitchenTicket.addToticketItems(newKitchenTicketItem);
            }
            catch (Exception e) {
              PosLog.error(getClass(), e);
            }
          }
          if (kitchenItem.getStatus().equalsIgnoreCase(KitchenStatus.BUMP.name())) {
            voidItem.setCooked(true);
          }
          kitchenItem.setVoided(Boolean.valueOf(true));
        }
        
        for (KitchenTicket kitchenTicket : kitchenTickets) {
          KitchenTicketDAO.getInstance().update(kitchenTicket, session);
        }
      }
    }
    saveOrUpdate(voidItem, session);
  }
}
