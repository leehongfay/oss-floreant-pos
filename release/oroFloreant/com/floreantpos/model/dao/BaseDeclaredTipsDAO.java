package com.floreantpos.model.dao;

import com.floreantpos.model.DeclaredTips;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseDeclaredTipsDAO
  extends _RootDAO
{
  public static DeclaredTipsDAO instance;
  
  public BaseDeclaredTipsDAO() {}
  
  public static DeclaredTipsDAO getInstance()
  {
    if (null == instance) instance = new DeclaredTipsDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return DeclaredTips.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public DeclaredTips cast(Object object)
  {
    return (DeclaredTips)object;
  }
  
  public DeclaredTips get(String key) throws HibernateException
  {
    return (DeclaredTips)get(getReferenceClass(), key);
  }
  
  public DeclaredTips get(String key, Session s) throws HibernateException
  {
    return (DeclaredTips)get(getReferenceClass(), key, s);
  }
  
  public DeclaredTips load(String key) throws HibernateException
  {
    return (DeclaredTips)load(getReferenceClass(), key);
  }
  
  public DeclaredTips load(String key, Session s) throws HibernateException
  {
    return (DeclaredTips)load(getReferenceClass(), key, s);
  }
  
  public DeclaredTips loadInitialize(String key, Session s) throws HibernateException
  {
    DeclaredTips obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<DeclaredTips> findAll()
  {
    return super.findAll();
  }
  


  public List<DeclaredTips> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<DeclaredTips> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(DeclaredTips declaredTips)
    throws HibernateException
  {
    return (String)super.save(declaredTips);
  }
  







  public String save(DeclaredTips declaredTips, Session s)
    throws HibernateException
  {
    return (String)save(declaredTips, s);
  }
  





  public void saveOrUpdate(DeclaredTips declaredTips)
    throws HibernateException
  {
    saveOrUpdate(declaredTips);
  }
  







  public void saveOrUpdate(DeclaredTips declaredTips, Session s)
    throws HibernateException
  {
    saveOrUpdate(declaredTips, s);
  }
  




  public void update(DeclaredTips declaredTips)
    throws HibernateException
  {
    update(declaredTips);
  }
  






  public void update(DeclaredTips declaredTips, Session s)
    throws HibernateException
  {
    update(declaredTips, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(DeclaredTips declaredTips)
    throws HibernateException
  {
    delete(declaredTips);
  }
  






  public void delete(DeclaredTips declaredTips, Session s)
    throws HibernateException
  {
    delete(declaredTips, s);
  }
  









  public void refresh(DeclaredTips declaredTips, Session s)
    throws HibernateException
  {
    refresh(declaredTips, s);
  }
}
