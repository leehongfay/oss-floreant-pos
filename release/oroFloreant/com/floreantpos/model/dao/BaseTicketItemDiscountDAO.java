package com.floreantpos.model.dao;

import com.floreantpos.model.TicketItemDiscount;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseTicketItemDiscountDAO
  extends _RootDAO
{
  public static TicketItemDiscountDAO instance;
  
  public BaseTicketItemDiscountDAO() {}
  
  public static TicketItemDiscountDAO getInstance()
  {
    if (null == instance) instance = new TicketItemDiscountDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return TicketItemDiscount.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public TicketItemDiscount cast(Object object)
  {
    return (TicketItemDiscount)object;
  }
  
  public TicketItemDiscount get(String key) throws HibernateException
  {
    return (TicketItemDiscount)get(getReferenceClass(), key);
  }
  
  public TicketItemDiscount get(String key, Session s) throws HibernateException
  {
    return (TicketItemDiscount)get(getReferenceClass(), key, s);
  }
  
  public TicketItemDiscount load(String key) throws HibernateException
  {
    return (TicketItemDiscount)load(getReferenceClass(), key);
  }
  
  public TicketItemDiscount load(String key, Session s) throws HibernateException
  {
    return (TicketItemDiscount)load(getReferenceClass(), key, s);
  }
  
  public TicketItemDiscount loadInitialize(String key, Session s) throws HibernateException
  {
    TicketItemDiscount obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<TicketItemDiscount> findAll()
  {
    return super.findAll();
  }
  


  public List<TicketItemDiscount> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<TicketItemDiscount> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(TicketItemDiscount ticketItemDiscount)
    throws HibernateException
  {
    return (String)super.save(ticketItemDiscount);
  }
  







  public String save(TicketItemDiscount ticketItemDiscount, Session s)
    throws HibernateException
  {
    return (String)save(ticketItemDiscount, s);
  }
  





  public void saveOrUpdate(TicketItemDiscount ticketItemDiscount)
    throws HibernateException
  {
    saveOrUpdate(ticketItemDiscount);
  }
  







  public void saveOrUpdate(TicketItemDiscount ticketItemDiscount, Session s)
    throws HibernateException
  {
    saveOrUpdate(ticketItemDiscount, s);
  }
  




  public void update(TicketItemDiscount ticketItemDiscount)
    throws HibernateException
  {
    update(ticketItemDiscount);
  }
  






  public void update(TicketItemDiscount ticketItemDiscount, Session s)
    throws HibernateException
  {
    update(ticketItemDiscount, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(TicketItemDiscount ticketItemDiscount)
    throws HibernateException
  {
    delete(ticketItemDiscount);
  }
  






  public void delete(TicketItemDiscount ticketItemDiscount, Session s)
    throws HibernateException
  {
    delete(ticketItemDiscount, s);
  }
  









  public void refresh(TicketItemDiscount ticketItemDiscount, Session s)
    throws HibernateException
  {
    refresh(ticketItemDiscount, s);
  }
}
