package com.floreantpos.model.dao;

import com.floreantpos.model.Outlet;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.StoreSession;
import com.floreantpos.swing.PaginationSupport;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;





public class StoreSessionDAO
  extends BaseStoreSessionDAO
{
  public StoreSessionDAO() {}
  
  public void loadStoreSession(PaginationSupport model)
  {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(StoreSession.class);
      criteria.addOrder(Order.asc(StoreSession.PROP_ID));
      criteria.setFirstResult(model.getCurrentRowIndex());
      criteria.setMaxResults(model.getPageSize());
      List<StoreSession> result = criteria.list();
      model.setRows(result);
    } finally {
      closeSession(session);
    }
  }
  
  public List<StoreSession> findSessions(Date startDate, Date endDate) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      
      criteria.add(Restrictions.and(Restrictions.ge(StoreSession.PROP_OPEN_TIME, startDate), Restrictions.le(StoreSession.PROP_OPEN_TIME, endDate)));
      List openTimeList = criteria.list();
      criteria.add(Restrictions.and(Restrictions.ge(StoreSession.PROP_CLOSE_TIME, startDate), Restrictions.le(StoreSession.PROP_CLOSE_TIME, endDate)));
      List closeTimeList = criteria.list();
      Iterator iterator;
      if (!closeTimeList.isEmpty()) {
        for (iterator = closeTimeList.iterator(); iterator.hasNext();) {
          StoreSession storeSession = (StoreSession)iterator.next();
          if (!openTimeList.contains(storeSession)) {
            openTimeList.add(storeSession);
          }
        }
      }
      
      return openTimeList;
    } finally {
      closeSession(session);
    }
  }
  
  public List<StoreSession> findSessions(Outlet outlet, Date startDate, Date endDate) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.and(Restrictions.ge(StoreSession.PROP_OPEN_TIME, startDate), Restrictions.le(StoreSession.PROP_OPEN_TIME, endDate)));
      if (outlet != null) {
        criteria.add(Restrictions.eq(StoreSession.PROP_OUTLET_ID, outlet.getId()));
      }
      List openTimeList = criteria.list();
      criteria.add(Restrictions.and(Restrictions.ge(StoreSession.PROP_CLOSE_TIME, startDate), Restrictions.le(StoreSession.PROP_CLOSE_TIME, endDate)));
      List closeTimeList = criteria.list();
      Iterator iterator;
      if (!closeTimeList.isEmpty()) {
        for (iterator = closeTimeList.iterator(); iterator.hasNext();) {
          StoreSession storeSession = (StoreSession)iterator.next();
          if (!openTimeList.contains(storeSession)) {
            openTimeList.add(storeSession);
          }
        }
      }
      
      return openTimeList;
    } finally {
      closeSession(session);
    }
  }
  
  public StoreSession findOpenSession(Outlet outlet) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.isNull(StoreSession.PROP_CLOSE_TIME));
      if (outlet != null) {
        criteria.add(Restrictions.eq(StoreSession.PROP_OUTLET_ID, outlet.getId()));
      }
      criteria.addOrder(Order.desc(StoreSession.PROP_OPEN_TIME));
      List openSessions = criteria.list();
      if (openSessions != null) {
        return (StoreSession)openSessions.get(0);
      }
    } finally {
      closeSession(session);
    }
    return null;
  }
  



















  public Integer getStoreSessionTotalGuest(StoreSession storeOperationData)
  {
    Session session = null;
    try {
      List<String> cashDrawerIds = CashDrawerDAO.getInstance().getCashDrawerIds(storeOperationData);
      if ((cashDrawerIds == null) || (cashDrawerIds.isEmpty()))
        return Integer.valueOf(0);
      session = getSession();
      Criteria criteria = session.createCriteria(PosTransaction.class, "t");
      

      criteria.createAlias(PosTransaction.PROP_TICKET, "ticket");
      
      criteria.add(Restrictions.in(PosTransaction.PROP_CASH_DRAWER_ID, cashDrawerIds));
      
      criteria.setProjection(Projections.sum("ticket.numberOfGuests"));
      Long guestNumber = (Long)criteria.uniqueResult();
      Integer localInteger2; if (guestNumber != null) {
        return Integer.valueOf(guestNumber.intValue());
      }
      return Integer.valueOf(0);
    } finally {
      closeSession(session);
    }
  }
}
