package com.floreantpos.model.dao;

import com.floreantpos.model.TicketItemTax;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseTicketItemTaxDAO
  extends _RootDAO
{
  public static TicketItemTaxDAO instance;
  
  public BaseTicketItemTaxDAO() {}
  
  public static TicketItemTaxDAO getInstance()
  {
    if (null == instance) instance = new TicketItemTaxDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return TicketItemTax.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public TicketItemTax cast(Object object)
  {
    return (TicketItemTax)object;
  }
  
  public TicketItemTax get(String key) throws HibernateException
  {
    return (TicketItemTax)get(getReferenceClass(), key);
  }
  
  public TicketItemTax get(String key, Session s) throws HibernateException
  {
    return (TicketItemTax)get(getReferenceClass(), key, s);
  }
  
  public TicketItemTax load(String key) throws HibernateException
  {
    return (TicketItemTax)load(getReferenceClass(), key);
  }
  
  public TicketItemTax load(String key, Session s) throws HibernateException
  {
    return (TicketItemTax)load(getReferenceClass(), key, s);
  }
  
  public TicketItemTax loadInitialize(String key, Session s) throws HibernateException
  {
    TicketItemTax obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<TicketItemTax> findAll()
  {
    return super.findAll();
  }
  


  public List<TicketItemTax> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<TicketItemTax> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(TicketItemTax ticketItemTax)
    throws HibernateException
  {
    return (String)super.save(ticketItemTax);
  }
  







  public String save(TicketItemTax ticketItemTax, Session s)
    throws HibernateException
  {
    return (String)save(ticketItemTax, s);
  }
  





  public void saveOrUpdate(TicketItemTax ticketItemTax)
    throws HibernateException
  {
    saveOrUpdate(ticketItemTax);
  }
  







  public void saveOrUpdate(TicketItemTax ticketItemTax, Session s)
    throws HibernateException
  {
    saveOrUpdate(ticketItemTax, s);
  }
  




  public void update(TicketItemTax ticketItemTax)
    throws HibernateException
  {
    update(ticketItemTax);
  }
  






  public void update(TicketItemTax ticketItemTax, Session s)
    throws HibernateException
  {
    update(ticketItemTax, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(TicketItemTax ticketItemTax)
    throws HibernateException
  {
    delete(ticketItemTax);
  }
  






  public void delete(TicketItemTax ticketItemTax, Session s)
    throws HibernateException
  {
    delete(ticketItemTax, s);
  }
  









  public void refresh(TicketItemTax ticketItemTax, Session s)
    throws HibernateException
  {
    refresh(ticketItemTax, s);
  }
}
