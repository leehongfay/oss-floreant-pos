package com.floreantpos.model.dao;

import com.floreantpos.model.GratuityPaymentHistory;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseGratuityPaymentHistoryDAO
  extends _RootDAO
{
  public static GratuityPaymentHistoryDAO instance;
  
  public BaseGratuityPaymentHistoryDAO() {}
  
  public static GratuityPaymentHistoryDAO getInstance()
  {
    if (null == instance) instance = new GratuityPaymentHistoryDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return GratuityPaymentHistory.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public GratuityPaymentHistory cast(Object object)
  {
    return (GratuityPaymentHistory)object;
  }
  
  public GratuityPaymentHistory get(String key) throws HibernateException
  {
    return (GratuityPaymentHistory)get(getReferenceClass(), key);
  }
  
  public GratuityPaymentHistory get(String key, Session s) throws HibernateException
  {
    return (GratuityPaymentHistory)get(getReferenceClass(), key, s);
  }
  
  public GratuityPaymentHistory load(String key) throws HibernateException
  {
    return (GratuityPaymentHistory)load(getReferenceClass(), key);
  }
  
  public GratuityPaymentHistory load(String key, Session s) throws HibernateException
  {
    return (GratuityPaymentHistory)load(getReferenceClass(), key, s);
  }
  
  public GratuityPaymentHistory loadInitialize(String key, Session s) throws HibernateException
  {
    GratuityPaymentHistory obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<GratuityPaymentHistory> findAll()
  {
    return super.findAll();
  }
  


  public List<GratuityPaymentHistory> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<GratuityPaymentHistory> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(GratuityPaymentHistory gratuityPaymentHistory)
    throws HibernateException
  {
    return (String)super.save(gratuityPaymentHistory);
  }
  







  public String save(GratuityPaymentHistory gratuityPaymentHistory, Session s)
    throws HibernateException
  {
    return (String)save(gratuityPaymentHistory, s);
  }
  





  public void saveOrUpdate(GratuityPaymentHistory gratuityPaymentHistory)
    throws HibernateException
  {
    saveOrUpdate(gratuityPaymentHistory);
  }
  







  public void saveOrUpdate(GratuityPaymentHistory gratuityPaymentHistory, Session s)
    throws HibernateException
  {
    saveOrUpdate(gratuityPaymentHistory, s);
  }
  




  public void update(GratuityPaymentHistory gratuityPaymentHistory)
    throws HibernateException
  {
    update(gratuityPaymentHistory);
  }
  






  public void update(GratuityPaymentHistory gratuityPaymentHistory, Session s)
    throws HibernateException
  {
    update(gratuityPaymentHistory, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(GratuityPaymentHistory gratuityPaymentHistory)
    throws HibernateException
  {
    delete(gratuityPaymentHistory);
  }
  






  public void delete(GratuityPaymentHistory gratuityPaymentHistory, Session s)
    throws HibernateException
  {
    delete(gratuityPaymentHistory, s);
  }
  









  public void refresh(GratuityPaymentHistory gratuityPaymentHistory, Session s)
    throws HibernateException
  {
    refresh(gratuityPaymentHistory, s);
  }
}
