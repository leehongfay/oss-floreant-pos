package com.floreantpos.model.dao;

import com.floreantpos.model.Multiplier;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseMultiplierDAO
  extends _RootDAO
{
  public static MultiplierDAO instance;
  
  public BaseMultiplierDAO() {}
  
  public static MultiplierDAO getInstance()
  {
    if (null == instance) instance = new MultiplierDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return Multiplier.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public Multiplier cast(Object object)
  {
    return (Multiplier)object;
  }
  
  public Multiplier get(String key) throws HibernateException
  {
    return (Multiplier)get(getReferenceClass(), key);
  }
  
  public Multiplier get(String key, Session s) throws HibernateException
  {
    return (Multiplier)get(getReferenceClass(), key, s);
  }
  
  public Multiplier load(String key) throws HibernateException
  {
    return (Multiplier)load(getReferenceClass(), key);
  }
  
  public Multiplier load(String key, Session s) throws HibernateException
  {
    return (Multiplier)load(getReferenceClass(), key, s);
  }
  
  public Multiplier loadInitialize(String key, Session s) throws HibernateException
  {
    Multiplier obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<Multiplier> findAll()
  {
    return super.findAll();
  }
  


  public List<Multiplier> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<Multiplier> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(Multiplier multiplier)
    throws HibernateException
  {
    return (String)super.save(multiplier);
  }
  







  public String save(Multiplier multiplier, Session s)
    throws HibernateException
  {
    return (String)save(multiplier, s);
  }
  





  public void saveOrUpdate(Multiplier multiplier)
    throws HibernateException
  {
    saveOrUpdate(multiplier);
  }
  







  public void saveOrUpdate(Multiplier multiplier, Session s)
    throws HibernateException
  {
    saveOrUpdate(multiplier, s);
  }
  




  public void update(Multiplier multiplier)
    throws HibernateException
  {
    update(multiplier);
  }
  






  public void update(Multiplier multiplier, Session s)
    throws HibernateException
  {
    update(multiplier, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(Multiplier multiplier)
    throws HibernateException
  {
    delete(multiplier);
  }
  






  public void delete(Multiplier multiplier, Session s)
    throws HibernateException
  {
    delete(multiplier, s);
  }
  









  public void refresh(Multiplier multiplier, Session s)
    throws HibernateException
  {
    refresh(multiplier, s);
  }
}
