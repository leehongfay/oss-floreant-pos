package com.floreantpos.model.dao;

import com.floreantpos.model.MenuPageItem;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseMenuPageItemDAO
  extends _RootDAO
{
  public static MenuPageItemDAO instance;
  
  public BaseMenuPageItemDAO() {}
  
  public static MenuPageItemDAO getInstance()
  {
    if (null == instance) instance = new MenuPageItemDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return MenuPageItem.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public MenuPageItem cast(Object object)
  {
    return (MenuPageItem)object;
  }
  
  public MenuPageItem get(String key) throws HibernateException
  {
    return (MenuPageItem)get(getReferenceClass(), key);
  }
  
  public MenuPageItem get(String key, Session s) throws HibernateException
  {
    return (MenuPageItem)get(getReferenceClass(), key, s);
  }
  
  public MenuPageItem load(String key) throws HibernateException
  {
    return (MenuPageItem)load(getReferenceClass(), key);
  }
  
  public MenuPageItem load(String key, Session s) throws HibernateException
  {
    return (MenuPageItem)load(getReferenceClass(), key, s);
  }
  
  public MenuPageItem loadInitialize(String key, Session s) throws HibernateException
  {
    MenuPageItem obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<MenuPageItem> findAll()
  {
    return super.findAll();
  }
  


  public List<MenuPageItem> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<MenuPageItem> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(MenuPageItem menuPageItem)
    throws HibernateException
  {
    return (String)super.save(menuPageItem);
  }
  







  public String save(MenuPageItem menuPageItem, Session s)
    throws HibernateException
  {
    return (String)save(menuPageItem, s);
  }
  





  public void saveOrUpdate(MenuPageItem menuPageItem)
    throws HibernateException
  {
    saveOrUpdate(menuPageItem);
  }
  







  public void saveOrUpdate(MenuPageItem menuPageItem, Session s)
    throws HibernateException
  {
    saveOrUpdate(menuPageItem, s);
  }
  




  public void update(MenuPageItem menuPageItem)
    throws HibernateException
  {
    update(menuPageItem);
  }
  






  public void update(MenuPageItem menuPageItem, Session s)
    throws HibernateException
  {
    update(menuPageItem, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(MenuPageItem menuPageItem)
    throws HibernateException
  {
    delete(menuPageItem);
  }
  






  public void delete(MenuPageItem menuPageItem, Session s)
    throws HibernateException
  {
    delete(menuPageItem, s);
  }
  









  public void refresh(MenuPageItem menuPageItem, Session s)
    throws HibernateException
  {
    refresh(menuPageItem, s);
  }
}
