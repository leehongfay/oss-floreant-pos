package com.floreantpos.model.dao;

import com.floreantpos.model.DeliveryAddress;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseDeliveryAddressDAO
  extends _RootDAO
{
  public static DeliveryAddressDAO instance;
  
  public BaseDeliveryAddressDAO() {}
  
  public static DeliveryAddressDAO getInstance()
  {
    if (null == instance) instance = new DeliveryAddressDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return DeliveryAddress.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public DeliveryAddress cast(Object object)
  {
    return (DeliveryAddress)object;
  }
  
  public DeliveryAddress get(String key) throws HibernateException
  {
    return (DeliveryAddress)get(getReferenceClass(), key);
  }
  
  public DeliveryAddress get(String key, Session s) throws HibernateException
  {
    return (DeliveryAddress)get(getReferenceClass(), key, s);
  }
  
  public DeliveryAddress load(String key) throws HibernateException
  {
    return (DeliveryAddress)load(getReferenceClass(), key);
  }
  
  public DeliveryAddress load(String key, Session s) throws HibernateException
  {
    return (DeliveryAddress)load(getReferenceClass(), key, s);
  }
  
  public DeliveryAddress loadInitialize(String key, Session s) throws HibernateException
  {
    DeliveryAddress obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<DeliveryAddress> findAll()
  {
    return super.findAll();
  }
  


  public List<DeliveryAddress> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<DeliveryAddress> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(DeliveryAddress deliveryAddress)
    throws HibernateException
  {
    return (String)super.save(deliveryAddress);
  }
  







  public String save(DeliveryAddress deliveryAddress, Session s)
    throws HibernateException
  {
    return (String)save(deliveryAddress, s);
  }
  





  public void saveOrUpdate(DeliveryAddress deliveryAddress)
    throws HibernateException
  {
    saveOrUpdate(deliveryAddress);
  }
  







  public void saveOrUpdate(DeliveryAddress deliveryAddress, Session s)
    throws HibernateException
  {
    saveOrUpdate(deliveryAddress, s);
  }
  




  public void update(DeliveryAddress deliveryAddress)
    throws HibernateException
  {
    update(deliveryAddress);
  }
  






  public void update(DeliveryAddress deliveryAddress, Session s)
    throws HibernateException
  {
    update(deliveryAddress, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(DeliveryAddress deliveryAddress)
    throws HibernateException
  {
    delete(deliveryAddress);
  }
  






  public void delete(DeliveryAddress deliveryAddress, Session s)
    throws HibernateException
  {
    delete(deliveryAddress, s);
  }
  









  public void refresh(DeliveryAddress deliveryAddress, Session s)
    throws HibernateException
  {
    refresh(deliveryAddress, s);
  }
}
