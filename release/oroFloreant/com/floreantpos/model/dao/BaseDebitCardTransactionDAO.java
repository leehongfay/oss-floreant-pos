package com.floreantpos.model.dao;

import com.floreantpos.model.DebitCardTransaction;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseDebitCardTransactionDAO
  extends _RootDAO
{
  public static DebitCardTransactionDAO instance;
  
  public BaseDebitCardTransactionDAO() {}
  
  public static DebitCardTransactionDAO getInstance()
  {
    if (null == instance) instance = new DebitCardTransactionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return DebitCardTransaction.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public DebitCardTransaction cast(Object object)
  {
    return (DebitCardTransaction)object;
  }
  
  public DebitCardTransaction get(String key) throws HibernateException
  {
    return (DebitCardTransaction)get(getReferenceClass(), key);
  }
  
  public DebitCardTransaction get(String key, Session s) throws HibernateException
  {
    return (DebitCardTransaction)get(getReferenceClass(), key, s);
  }
  
  public DebitCardTransaction load(String key) throws HibernateException
  {
    return (DebitCardTransaction)load(getReferenceClass(), key);
  }
  
  public DebitCardTransaction load(String key, Session s) throws HibernateException
  {
    return (DebitCardTransaction)load(getReferenceClass(), key, s);
  }
  
  public DebitCardTransaction loadInitialize(String key, Session s) throws HibernateException
  {
    DebitCardTransaction obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<DebitCardTransaction> findAll()
  {
    return super.findAll();
  }
  


  public List<DebitCardTransaction> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<DebitCardTransaction> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(DebitCardTransaction debitCardTransaction)
    throws HibernateException
  {
    return (String)super.save(debitCardTransaction);
  }
  







  public String save(DebitCardTransaction debitCardTransaction, Session s)
    throws HibernateException
  {
    return (String)save(debitCardTransaction, s);
  }
  





  public void saveOrUpdate(DebitCardTransaction debitCardTransaction)
    throws HibernateException
  {
    saveOrUpdate(debitCardTransaction);
  }
  







  public void saveOrUpdate(DebitCardTransaction debitCardTransaction, Session s)
    throws HibernateException
  {
    saveOrUpdate(debitCardTransaction, s);
  }
  




  public void update(DebitCardTransaction debitCardTransaction)
    throws HibernateException
  {
    update(debitCardTransaction);
  }
  






  public void update(DebitCardTransaction debitCardTransaction, Session s)
    throws HibernateException
  {
    update(debitCardTransaction, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(DebitCardTransaction debitCardTransaction)
    throws HibernateException
  {
    delete(debitCardTransaction);
  }
  






  public void delete(DebitCardTransaction debitCardTransaction, Session s)
    throws HibernateException
  {
    delete(debitCardTransaction, s);
  }
  









  public void refresh(DebitCardTransaction debitCardTransaction, Session s)
    throws HibernateException
  {
    refresh(debitCardTransaction, s);
  }
}
