package com.floreantpos.model.dao;

import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import com.floreantpos.model.AttendenceHistory;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Shift;
import com.floreantpos.model.Store;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.User;
import com.floreantpos.model.UserType;
import com.floreantpos.report.SalesStatistics;
import com.floreantpos.report.SalesStatistics.ShiftwiseSalesTableData;
import com.floreantpos.report.ShiftwiseSalesSummaryReportModel.ShiftwiseSalesSummaryData;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;




















public class SalesSummaryDAO
  extends _RootDAO
{
  public SalesSummaryDAO() {}
  
  protected Class getReferenceClass()
  {
    return null;
  }
  
  public Serializable save(Object obj)
  {
    return super.save(obj);
  }
  
  public void saveOrUpdate(Object obj)
  {
    super.saveOrUpdate(obj);
  }
  
  public List<ShiftwiseSalesSummaryReportModel.ShiftwiseSalesSummaryData> findSalesAnalysis(Date start, Date end, UserType userType, Terminal terminal) {
    Session session = null;
    try
    {
      ArrayList<ShiftwiseSalesSummaryReportModel.ShiftwiseSalesSummaryData> list = new ArrayList();
      
      session = getSession();
      
      Criteria criteria = session.createCriteria(Shift.class);
      List<Shift> shifts = criteria.list();
      
      criteria = session.createCriteria(MenuCategory.class);
      List<MenuCategory> categories = criteria.list();
      MenuCategory miscCategory = new MenuCategory();
      miscCategory.setName(Messages.getString("SalesSummaryDAO.0"));
      categories.add(miscCategory);
      

      for (Object localObject1 = shifts.iterator(); ((Iterator)localObject1).hasNext();) { shift = (Shift)((Iterator)localObject1).next();
        
        for (MenuCategory category : categories)
        {
          criteria = session.createCriteria(TicketItem.class, "item");
          criteria.createCriteria("ticket", "t");
          ProjectionList projectionList = Projections.projectionList();
          projectionList.add(Projections.sum(TicketItem.PROP_QUANTITY));
          projectionList.add(Projections.sum(TicketItem.PROP_SUBTOTAL_AMOUNT));
          projectionList.add(Projections.sum(TicketItem.PROP_DISCOUNT_AMOUNT));
          criteria.setProjection(projectionList);
          criteria.add(Restrictions.eq("item." + TicketItem.PROP_CATEGORY_NAME, category.getName()));
          criteria.add(Restrictions.eq("t." + Ticket.PROP_SHIFT_ID, shift.getId()));
          criteria.add(Restrictions.ge("t." + Ticket.PROP_ACTIVE_DATE, start));
          criteria.add(Restrictions.le("t." + Ticket.PROP_ACTIVE_DATE, end));
          
          if (userType != null) {
            criteria.add(Restrictions.eq("t." + Ticket.PROP_OWNER_TYPE_ID, userType.getId()));
          }
          if (terminal != null) {
            criteria.add(Restrictions.eq("t." + Ticket.PROP_TERMINAL_ID, terminal.getId()));
          }
          List datas = criteria.list();
          if (datas.size() > 0) {
            Object[] objects = (Object[])datas.get(0);
            
            ShiftwiseSalesSummaryReportModel.ShiftwiseSalesSummaryData data = new ShiftwiseSalesSummaryReportModel.ShiftwiseSalesSummaryData();
            data.setShiftName(shift.getName());
            data.setCategoryName(category.getName());
            
            if ((objects.length > 0) && (objects[0] != null)) {
              data.setCount(((Number)objects[0]).intValue());
            }
            if ((objects.length > 1) && (objects[1] != null)) {
              data.setGross(((Number)objects[1]).doubleValue());
            }
            if ((objects.length > 2) && (objects[2] != null)) {
              data.setDiscount(((Number)objects[2]).doubleValue());
            }
            data.calculate();
            list.add(data);
          }
        }
      }
      
      Shift shift;
      for (localObject1 = categories.iterator(); ((Iterator)localObject1).hasNext();) { MenuCategory category = (MenuCategory)((Iterator)localObject1).next();
        
        criteria = session.createCriteria(TicketItem.class, "item");
        criteria.createCriteria("ticket", "t");
        ProjectionList projectionList = Projections.projectionList();
        projectionList.add(Projections.sum(TicketItem.PROP_QUANTITY));
        projectionList.add(Projections.sum(TicketItem.PROP_SUBTOTAL_AMOUNT));
        projectionList.add(Projections.sum(TicketItem.PROP_DISCOUNT_AMOUNT));
        criteria.setProjection(projectionList);
        criteria.add(Restrictions.eq("item." + TicketItem.PROP_CATEGORY_NAME, category.getName()));
        criteria.add(Restrictions.ge("t." + Ticket.PROP_ACTIVE_DATE, start));
        criteria.add(Restrictions.le("t." + Ticket.PROP_ACTIVE_DATE, end));
        
        if (userType != null) {
          criteria.add(Restrictions.eq("t." + Ticket.PROP_OWNER_TYPE_ID, userType.getId()));
        }
        if (terminal != null) {
          criteria.add(Restrictions.eq("t." + Ticket.PROP_TERMINAL_ID, terminal.getId()));
        }
        List datas = criteria.list();
        if (datas.size() > 0) {
          Object[] objects = (Object[])datas.get(0);
          
          ShiftwiseSalesSummaryReportModel.ShiftwiseSalesSummaryData data = new ShiftwiseSalesSummaryReportModel.ShiftwiseSalesSummaryData();
          data.setShiftName("ALL DAY");
          data.setCategoryName(category.getName());
          
          if ((objects.length > 0) && (objects[0] != null)) {
            data.setCount(((Number)objects[0]).intValue());
          }
          if ((objects.length > 1) && (objects[1] != null)) {
            data.setGross(((Number)objects[1]).doubleValue());
          }
          if ((objects.length > 2) && (objects[2] != null)) {
            data.setDiscount(((Number)objects[2]).doubleValue());
          }
          data.calculate();
          list.add(data);
        }
      }
      return list;
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public SalesStatistics findKeyStatistics(Date start, Date end, UserType userType, Terminal terminal) {
    Session session = null;
    try
    {
      SalesStatistics salesSummary = new SalesStatistics();
      
      session = getSession();
      

      Store store = (Store)get(Store.class, "1", session);
      if (store != null) {
        salesSummary.setCapacity(store.getCapacity() != null ? store.getCapacity().intValue() : 0);
        salesSummary.setTables(store.getTables() != null ? store.getTables().intValue() : 0);
      }
      



      Criteria criteria = session.createCriteria(Ticket.class, "ticket");
      

      ProjectionList projectionList = Projections.projectionList();
      projectionList.add(Projections.rowCount());
      projectionList.add(Projections.sum(Ticket.PROP_SUBTOTAL_AMOUNT));
      projectionList.add(Projections.sum(Ticket.PROP_DISCOUNT_AMOUNT));
      projectionList.add(Projections.sum(Ticket.PROP_TAX_AMOUNT));
      criteria.setProjection(projectionList);
      criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, start));
      criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, end));
      

      criteria.add(Restrictions.eq(Ticket.PROP_VOIDED, Boolean.FALSE));
      criteria.add(Restrictions.eq(Ticket.PROP_REFUNDED, Boolean.FALSE));
      
      if (userType != null)
      {
        criteria.add(Restrictions.eq(Ticket.PROP_OWNER_TYPE_ID, userType.getId()));
      }
      
      if (terminal != null) {
        criteria.add(Restrictions.eq(Ticket.PROP_TERMINAL_ID, terminal.getId()));
      }
      List list = criteria.list();
      if (list.size() > 0) {
        Object[] objects = (Object[])list.get(0);
        
        if ((objects.length > 1) && (objects[1] != null)) {
          salesSummary.setGrossSale(((Number)objects[1]).doubleValue());
        }
        if ((objects.length > 2) && (objects[2] != null)) {
          salesSummary.setDiscount(((Number)objects[2]).intValue());
        }
        if ((objects.length > 3) && (objects[3] != null)) {
          salesSummary.setTax(((Number)objects[3]).intValue());
        }
      }
      



      Criteria criteria = session.createCriteria(Ticket.class, "ticket");
      

      ProjectionList projectionList = Projections.projectionList();
      projectionList.add(Projections.rowCount());
      projectionList.add(Projections.sum(Ticket.PROP_NUMBER_OF_GUESTS));
      criteria.setProjection(projectionList);
      criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, start));
      criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, end));
      
      if (userType != null)
      {
        criteria.add(Restrictions.eq(Ticket.PROP_OWNER_TYPE_ID, userType.getId()));
      }
      
      if (terminal != null) {
        criteria.add(Restrictions.eq(Ticket.PROP_TERMINAL_ID, terminal.getId()));
      }
      List list = criteria.list();
      if (list.size() > 0) {
        Object[] objects = (Object[])list.get(0);
        salesSummary.setCheckCount(((Number)objects[0]).intValue());
        
        if ((objects.length > 1) && (objects[1] != null)) {
          salesSummary.setGuestCount(((Number)objects[1]).intValue());
        }
      }
      



      Criteria criteria = session.createCriteria(Ticket.class, "ticket");
      

      ProjectionList projectionList = Projections.projectionList();
      projectionList.add(Projections.rowCount());
      projectionList.add(Projections.sum(Ticket.PROP_TOTAL_AMOUNT));
      criteria.setProjection(projectionList);
      criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, start));
      criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, end));
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.FALSE));
      
      if (userType != null)
      {
        criteria.add(Restrictions.eq(Ticket.PROP_OWNER_TYPE_ID, userType.getId()));
      }
      if (terminal != null) {
        criteria.add(Restrictions.eq(Ticket.PROP_TERMINAL_ID, terminal.getId()));
      }
      List list = criteria.list();
      if (list.size() > 0) {
        Object[] objects = (Object[])list.get(0);
        salesSummary.setOpenChecks(((Number)objects[0]).intValue());
        
        if ((objects.length > 1) && (objects[1] != null)) {
          salesSummary.setOpenAmount(((Number)objects[1]).doubleValue());
        }
      }
      



      Criteria criteria = session.createCriteria(Ticket.class, "ticket");
      
      ProjectionList projectionList = Projections.projectionList();
      projectionList.add(Projections.rowCount());
      projectionList.add(Projections.sum(Ticket.PROP_TOTAL_AMOUNT));
      criteria.setProjection(projectionList);
      criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, start));
      criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, end));
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.TRUE));
      criteria.add(Restrictions.eq(Ticket.PROP_VOIDED, Boolean.TRUE));
      
      if (userType != null)
      {
        criteria.add(Restrictions.eq(Ticket.PROP_OWNER_TYPE_ID, userType.getId()));
      }
      if (terminal != null) {
        criteria.add(Restrictions.eq(Ticket.PROP_TERMINAL_ID, terminal.getId()));
      }
      List list = criteria.list();
      if (list.size() > 0) {
        Object[] objects = (Object[])list.get(0);
        salesSummary.setVoidChecks(((Number)objects[0]).intValue());
        
        if ((objects.length > 1) && (objects[1] != null)) {
          salesSummary.setVoidAmount(((Number)objects[1]).doubleValue());
        }
      }
      



      Criteria criteria = session.createCriteria(Ticket.class, "ticket");
      
      ProjectionList projectionList = Projections.projectionList();
      projectionList.add(Projections.rowCount());
      projectionList.add(Projections.sum(Ticket.PROP_TOTAL_AMOUNT));
      criteria.setProjection(projectionList);
      criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, start));
      criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, end));
      criteria.add(Restrictions.eq(Ticket.PROP_VOIDED, Boolean.FALSE));
      criteria.add(Restrictions.eq(Ticket.PROP_REFUNDED, Boolean.FALSE));
      criteria.add(Restrictions.eq(Ticket.PROP_TAX_EXEMPT, Boolean.TRUE));
      
      if (userType != null)
      {
        criteria.add(Restrictions.eq(Ticket.PROP_OWNER_TYPE_ID, userType.getId()));
      }
      if (terminal != null) {
        criteria.add(Restrictions.eq(Ticket.PROP_TERMINAL_ID, terminal.getId()));
      }
      List list = criteria.list();
      if (list.size() > 0) {
        Object[] objects = (Object[])list.get(0);
        salesSummary.setNtaxChecks(((Number)objects[0]).intValue());
        
        if ((objects.length > 1) && (objects[1] != null)) {
          salesSummary.setNtaxAmount(((Number)objects[1]).doubleValue());
        }
      }
      


      Criteria criteria = session.createCriteria(Ticket.class, "ticket");
      
      ProjectionList projectionList = Projections.projectionList();
      projectionList.add(Projections.rowCount());
      projectionList.add(Projections.sum(Ticket.PROP_TOTAL_AMOUNT));
      criteria.setProjection(projectionList);
      criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, start));
      criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, end));
      criteria.add(Restrictions.eq(Ticket.PROP_RE_OPENED, Boolean.TRUE));
      
      if (userType != null)
      {
        criteria.add(Restrictions.eq(Ticket.PROP_OWNER_TYPE_ID, userType.getId()));
      }
      if (terminal != null) {
        criteria.add(Restrictions.eq(Ticket.PROP_TERMINAL_ID, terminal.getId()));
      }
      List list = criteria.list();
      if (list.size() > 0) {
        Object[] objects = (Object[])list.get(0);
        salesSummary.setRopnChecks(((Number)objects[0]).intValue());
        
        if ((objects.length > 1) && (objects[1] != null)) {
          salesSummary.setRopnAmount(((Number)objects[1]).doubleValue());
        }
      }
      



      Criteria criteria = session.createCriteria(AttendenceHistory.class, "history");
      criteria.createCriteria(AttendenceHistory.PROP_USER, "u");
      criteria.add(Restrictions.ge(AttendenceHistory.PROP_CLOCK_IN_TIME, start));
      criteria.add(Restrictions.le(AttendenceHistory.PROP_CLOCK_IN_TIME, end));
      
      if (userType != null) {
        criteria.add(Restrictions.eq("u." + User.PROP_USER_TYPE_ID, userType.getId()));
      }
      if (terminal != null) {
        criteria.add(Restrictions.eq(AttendenceHistory.PROP_TERMINAL, terminal));
      }
      List list = criteria.list();
      
      double laborHours = 0.0D;
      double laborCost = 0.0D;
      for (Object object : list) {
        AttendenceHistory attendenceHistory = (AttendenceHistory)object;
        double laborHourInMillisecond = 0.0D;
        if ((!attendenceHistory.isClockedOut().booleanValue()) || (attendenceHistory.getClockOutTime() == null)) {
          Shift attendenceShift = attendenceHistory.getShift();
          laborHourInMillisecond = Math.abs(end.getTime() - attendenceHistory.getClockInTime().getTime());
          if (laborHourInMillisecond > attendenceShift.getShiftLength().longValue()) {
            laborHourInMillisecond = attendenceShift.getShiftLength().longValue();
          }
        }
        else {
          laborHourInMillisecond = Math.abs(attendenceHistory.getClockInTime().getTime() - attendenceHistory.getClockInTime().getTime());
        }
        double hour = laborHourInMillisecond * (2.77777778D * Math.pow(10.0D, -7.0D));
        laborHours += hour;
        laborCost += hour * (attendenceHistory.getUser().getCostPerHour() == null ? 0.0D : attendenceHistory.getUser().getCostPerHour().doubleValue());
      }
      salesSummary.setLaborHour(laborHours);
      salesSummary.setLaborCost(laborCost);
      



      Criteria criteria = session.createCriteria(Shift.class);
      List shifts = criteria.list();
      for (Object object : shifts) {
        shift = (Shift)object;
        
        List<OrderType> values = Application.getInstance().getOrderTypes();
        for (OrderType ticketType : values) {
          findRecordByProfitCenter(start, end, userType, terminal, session, salesSummary, shift, ticketType);
        }
      }
      
      Shift shift;
      
      salesSummary.calculateOthers();
      return salesSummary;
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  

  private void findRecordByProfitCenter(Date start, Date end, UserType userType, Terminal terminal, Session session, SalesStatistics salesSummary, Shift shift, OrderType ticketType)
  {
    Criteria criteria = session.createCriteria(Ticket.class, "ticket");
    
    ProjectionList projectionList = Projections.projectionList();
    projectionList.add(Projections.rowCount());
    projectionList.add(Projections.sum(Ticket.PROP_NUMBER_OF_GUESTS));
    projectionList.add(Projections.sum(Ticket.PROP_SUBTOTAL_AMOUNT));
    criteria.setProjection(projectionList);
    criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, start));
    criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, end));
    criteria.add(Restrictions.eq(Ticket.PROP_SHIFT_ID, shift.getId()));
    criteria.add(Restrictions.eq(Ticket.PROP_ORDER_TYPE_ID, ticketType == null ? null : ticketType.getId()));
    
    if (userType != null)
    {
      criteria.add(Restrictions.eq(Ticket.PROP_OWNER_TYPE_ID, userType.getId()));
    }
    if (terminal != null) {
      criteria.add(Restrictions.eq(Ticket.PROP_TERMINAL_ID, terminal.getId()));
    }
    List list = criteria.list();
    if (list.size() > 0) {
      SalesStatistics.ShiftwiseSalesTableData data = new SalesStatistics.ShiftwiseSalesTableData();
      data.setProfitCenter(ticketType.toString());
      Object[] objects = (Object[])list.get(0);
      
      data.setShiftName(shift.getName());
      data.setCheckCount(((Number)objects[0]).intValue());
      
      if ((objects.length > 1) && (objects[1] != null)) {
        data.setGuestCount(((Number)objects[1]).intValue());
      }
      if ((objects.length > 2) && (objects[2] != null)) {
        data.setTotalSales(((Number)objects[2]).doubleValue());
      }
      data.setPercentage(data.getTotalSales() * 100.0D / salesSummary.getGrossSale());
      data.calculateOthers();
      salesSummary.addSalesTableData(data);
    }
  }
}
