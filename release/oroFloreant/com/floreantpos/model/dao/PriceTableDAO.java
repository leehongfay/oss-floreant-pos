package com.floreantpos.model.dao;

import com.floreantpos.model.PriceTable;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;




public class PriceTableDAO
  extends BasePriceTableDAO
{
  public PriceTableDAO() {}
  
  public void releaseParentAndDelete(PriceTable priceTable)
  {
    if (priceTable == null) {
      return;
    }
    
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      String queryString = "update PRICE_RULE set PRICE_TABLE_ID=null where PRICE_TABLE_ID='%s'";
      queryString = String.format(queryString, new Object[] { priceTable.getId() });
      Query query = session.createSQLQuery(queryString);
      query.executeUpdate();
      
      String queryString2 = "delete from PRICE_TABLE_ITEM where PRICE_TABLE_ID='%s'";
      queryString2 = String.format(queryString2, new Object[] { priceTable.getId() });
      Query query2 = session.createSQLQuery(queryString2);
      query2.executeUpdate();
      
      session.delete(priceTable);
      
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
}
