package com.floreantpos.model.dao;

import com.floreantpos.model.UserPermission;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;




















public class UserPermissionDAO
  extends BaseUserPermissionDAO
{
  public UserPermissionDAO() {}
  
  public boolean hasPermission(UserPermission permission)
  {
    Criteria criteria = getSession().createCriteria(getReferenceClass());
    criteria.add(Restrictions.eq(UserPermission.PROP_NAME, permission.getName()));
    List list = criteria.list();
    return list.size() > 0;
  }
}
