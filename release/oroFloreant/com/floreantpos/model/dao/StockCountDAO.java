package com.floreantpos.model.dao;

import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.InventoryTransactionType;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.StockCount;
import com.floreantpos.model.StockCountItem;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;






public class StockCountDAO
  extends BaseStockCountDAO
{
  public StockCountDAO() {}
  
  public void saveOrUpdate(StockCount stockCount, boolean updateStock)
  {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      
      saveOrUpdate(stockCount, updateStock, session);
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      throwException(e);
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public void saveOrUpdate(StockCount stockCount, boolean updateStock, Session session) {
    if (stockCount.getId() == null) {
      stockCount.setCreatedDate(new Date());
    }
    stockCount.setLastModifiedDate(new Date());
    if (updateStock) {
      adjustInventoryItems(session, stockCount);
    }
    session.saveOrUpdate(stockCount);
  }
  
  private void adjustInventoryItems(Session session, StockCount stockCount) {
    List<StockCountItem> orderItems = stockCount.getCountItems();
    if (orderItems == null) {
      return;
    }
    for (StockCountItem orderItem : orderItems) {
      if (!orderItem.isAdjusted().booleanValue())
      {
        orderItem.setAdjusted(Boolean.TRUE);
        
        Double stockDifference = Double.valueOf(orderItem.getActualUnit().doubleValue() - orderItem.getUnitOnHand().doubleValue());
        
        if (stockDifference.doubleValue() != 0.0D)
        {

          InventoryTransaction stockInTrans = new InventoryTransaction();
          MenuItem menuItem = MenuItemDAO.getInstance().get(orderItem.getItemId(), session);
          Hibernate.initialize(menuItem.getStockUnits());
          stockInTrans.setMenuItem(menuItem);
          stockInTrans.setQuantity(Double.valueOf(Math.abs(stockDifference.doubleValue())));
          stockInTrans.setUnit(orderItem.getUnit());
          double baseUnitQuantity = menuItem.getBaseUnitQuantity(orderItem.getUnit());
          stockInTrans.setUnitCost(Double.valueOf(baseUnitQuantity * menuItem.getCost().doubleValue()));
          stockInTrans.setTotal(Double.valueOf(baseUnitQuantity * stockInTrans.getQuantity().doubleValue() * menuItem.getCost().doubleValue()));
          stockInTrans.setTransactionDate(new Date());
          String reason = orderItem.getReason();
          
          if (StringUtils.isEmpty(reason)) {
            String strReason = stockDifference.doubleValue() > 0.0D ? "ADJUST_IN" : "ADJUST_OUT";
            stockInTrans.setReason(strReason);
          }
          else
          {
            stockInTrans.setReason(reason);
          }
          
          if (stockDifference.doubleValue() > 0.0D) {
            stockInTrans.setType(Integer.valueOf(InventoryTransactionType.IN.getType()));
            stockInTrans.setToInventoryLocation(orderItem.getInventoryLocation());
          }
          else {
            stockInTrans.setType(Integer.valueOf(InventoryTransactionType.OUT.getType()));
            stockInTrans.setFromInventoryLocation(orderItem.getInventoryLocation());
          }
          InventoryTransactionDAO.getInstance().adjustInventoryStock(stockInTrans, session);
        }
      }
    }
  }
  
  /* Error */
  public String getNextStockCountSequenceNumber()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aload_0
    //   3: invokevirtual 63	com/floreantpos/model/dao/StockCountDAO:getSession	()Lorg/hibernate/Session;
    //   6: astore_1
    //   7: aload_1
    //   8: aload_0
    //   9: invokevirtual 64	com/floreantpos/model/dao/StockCountDAO:getReferenceClass	()Ljava/lang/Class;
    //   12: invokeinterface 65 2 0
    //   17: astore_2
    //   18: aload_2
    //   19: getstatic 66	com/floreantpos/model/StockCount:PROP_REF_NUMBER	Ljava/lang/String;
    //   22: invokestatic 67	org/hibernate/criterion/Projections:max	(Ljava/lang/String;)Lorg/hibernate/criterion/AggregateProjection;
    //   25: invokeinterface 68 2 0
    //   30: pop
    //   31: aload_2
    //   32: invokeinterface 69 1 0
    //   37: astore_3
    //   38: aload_3
    //   39: ifnonnull +20 -> 59
    //   42: ldc 70
    //   44: astore 4
    //   46: aload_1
    //   47: ifnull +9 -> 56
    //   50: aload_1
    //   51: invokeinterface 6 1 0
    //   56: aload 4
    //   58: areturn
    //   59: aload_3
    //   60: checkcast 71	java/lang/String
    //   63: invokestatic 72	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   66: iconst_1
    //   67: iadd
    //   68: invokestatic 73	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   71: astore 4
    //   73: aload_1
    //   74: ifnull +9 -> 83
    //   77: aload_1
    //   78: invokeinterface 6 1 0
    //   83: aload 4
    //   85: areturn
    //   86: astore 4
    //   88: ldc 74
    //   90: astore 5
    //   92: aload_1
    //   93: ifnull +9 -> 102
    //   96: aload_1
    //   97: invokeinterface 6 1 0
    //   102: aload 5
    //   104: areturn
    //   105: astore 6
    //   107: aload_1
    //   108: ifnull +9 -> 117
    //   111: aload_1
    //   112: invokeinterface 6 1 0
    //   117: aload 6
    //   119: athrow
    // Line number table:
    //   Java source line #109	-> byte code offset #0
    //   Java source line #111	-> byte code offset #2
    //   Java source line #112	-> byte code offset #7
    //   Java source line #113	-> byte code offset #18
    //   Java source line #114	-> byte code offset #31
    //   Java source line #115	-> byte code offset #38
    //   Java source line #116	-> byte code offset #42
    //   Java source line #123	-> byte code offset #46
    //   Java source line #124	-> byte code offset #50
    //   Java source line #118	-> byte code offset #59
    //   Java source line #123	-> byte code offset #73
    //   Java source line #124	-> byte code offset #77
    //   Java source line #119	-> byte code offset #86
    //   Java source line #120	-> byte code offset #88
    //   Java source line #123	-> byte code offset #92
    //   Java source line #124	-> byte code offset #96
    //   Java source line #123	-> byte code offset #105
    //   Java source line #124	-> byte code offset #111
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	120	0	this	StockCountDAO
    //   1	111	1	session	Session
    //   17	15	2	criteria	Criteria
    //   37	23	3	maxNumber	Object
    //   44	40	4	str1	String
    //   86	3	4	e	Exception
    //   90	13	5	str2	String
    //   105	13	6	localObject1	Object
    // Exception table:
    //   from	to	target	type
    //   59	73	86	java/lang/Exception
    //   2	46	105	finally
    //   59	73	105	finally
    //   86	92	105	finally
    //   105	107	105	finally
  }
  
  public List findBy(String ref, Date from, Date to, Boolean showVerified)
  {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      if (StringUtils.isNotEmpty(ref)) {
        criteria.add(Restrictions.ilike(StockCount.PROP_REF_NUMBER, ref));
      }
      if ((from != null) && (to != null)) {
        criteria.add(Restrictions.ge(StockCount.PROP_CREATED_DATE, from));
        criteria.add(Restrictions.le(StockCount.PROP_CREATED_DATE, to));
      }
      if (!showVerified.booleanValue()) {
        criteria.add(Restrictions.isNull(StockCount.PROP_VERIFIED_BY));
      }
      return criteria.list();
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
}
