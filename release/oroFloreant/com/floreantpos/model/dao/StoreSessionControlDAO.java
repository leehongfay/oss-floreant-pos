package com.floreantpos.model.dao;

import com.floreantpos.model.StoreSessionControl;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;





public class StoreSessionControlDAO
  extends BaseStoreSessionControlDAO
{
  public StoreSessionControlDAO() {}
  
  public StoreSessionControl getCurrent()
  {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.addOrder(Order.asc(StoreSessionControl.PROP_ID));
      List list = criteria.list();
      StoreSessionControl storeSessionControl; if ((list == null) || (list.size() == 0)) {
        storeSessionControl = new StoreSessionControl();
        save(storeSessionControl);
        return storeSessionControl;
      }
      return (StoreSessionControl)list.get(0);
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
}
