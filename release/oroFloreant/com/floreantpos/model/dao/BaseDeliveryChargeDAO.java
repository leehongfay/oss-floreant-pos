package com.floreantpos.model.dao;

import com.floreantpos.model.DeliveryCharge;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseDeliveryChargeDAO
  extends _RootDAO
{
  public static DeliveryChargeDAO instance;
  
  public BaseDeliveryChargeDAO() {}
  
  public static DeliveryChargeDAO getInstance()
  {
    if (null == instance) instance = new DeliveryChargeDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return DeliveryCharge.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public DeliveryCharge cast(Object object)
  {
    return (DeliveryCharge)object;
  }
  
  public DeliveryCharge get(String key) throws HibernateException
  {
    return (DeliveryCharge)get(getReferenceClass(), key);
  }
  
  public DeliveryCharge get(String key, Session s) throws HibernateException
  {
    return (DeliveryCharge)get(getReferenceClass(), key, s);
  }
  
  public DeliveryCharge load(String key) throws HibernateException
  {
    return (DeliveryCharge)load(getReferenceClass(), key);
  }
  
  public DeliveryCharge load(String key, Session s) throws HibernateException
  {
    return (DeliveryCharge)load(getReferenceClass(), key, s);
  }
  
  public DeliveryCharge loadInitialize(String key, Session s) throws HibernateException
  {
    DeliveryCharge obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<DeliveryCharge> findAll()
  {
    return super.findAll();
  }
  


  public List<DeliveryCharge> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<DeliveryCharge> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(DeliveryCharge deliveryCharge)
    throws HibernateException
  {
    return (String)super.save(deliveryCharge);
  }
  







  public String save(DeliveryCharge deliveryCharge, Session s)
    throws HibernateException
  {
    return (String)save(deliveryCharge, s);
  }
  





  public void saveOrUpdate(DeliveryCharge deliveryCharge)
    throws HibernateException
  {
    saveOrUpdate(deliveryCharge);
  }
  







  public void saveOrUpdate(DeliveryCharge deliveryCharge, Session s)
    throws HibernateException
  {
    saveOrUpdate(deliveryCharge, s);
  }
  




  public void update(DeliveryCharge deliveryCharge)
    throws HibernateException
  {
    update(deliveryCharge);
  }
  






  public void update(DeliveryCharge deliveryCharge, Session s)
    throws HibernateException
  {
    update(deliveryCharge, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(DeliveryCharge deliveryCharge)
    throws HibernateException
  {
    delete(deliveryCharge);
  }
  






  public void delete(DeliveryCharge deliveryCharge, Session s)
    throws HibernateException
  {
    delete(deliveryCharge, s);
  }
  









  public void refresh(DeliveryCharge deliveryCharge, Session s)
    throws HibernateException
  {
    refresh(deliveryCharge, s);
  }
}
