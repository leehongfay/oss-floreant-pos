package com.floreantpos.model.dao;

import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.config.CardConfig;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.extension.PaymentGatewayPlugin;
import com.floreantpos.main.Application;
import com.floreantpos.model.ActionHistory;
import com.floreantpos.model.CreditCardTransaction;
import com.floreantpos.model.DataUpdateInfo;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.InventoryTransactionType;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.KitchenTicket;
import com.floreantpos.model.KitchenTicketItem;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuItemInventoryStatus;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PaymentStatusFilter;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.Recepie;
import com.floreantpos.model.Shift;
import com.floreantpos.model.Store;
import com.floreantpos.model.StoreSession;
import com.floreantpos.model.StoreSessionControl;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketType;
import com.floreantpos.model.User;
import com.floreantpos.model.UserType;
import com.floreantpos.model.VoidItem;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.PaginatedTableModel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.OrderController;
import com.floreantpos.ui.views.payment.CardProcessor;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.NumericGlobalIdGenerator;
import com.floreantpos.util.StoreUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

















public class TicketDAO
  extends BaseTicketDAO
{
  private static final TicketDAO instance = new TicketDAO();
  


  public TicketDAO() {}
  


  public Order getDefaultOrder()
  {
    return Order.desc(Ticket.PROP_CREATE_DATE);
  }
  
  protected Serializable save(Object obj, Session session)
  {
    Serializable save = super.save(obj, session);
    Ticket ticket = (Ticket)obj;
    
    performPostSaveOperations(session, ticket);
    
    return save;
  }
  
  private void performPostSaveOperations(Session session, Ticket ticket) {
    updateStock(ticket, session);
    
    ticket.clearDeletedItems();
    clearVoidedItems(ticket);
    
    DataUpdateInfo lastUpdateInfo = DataUpdateInfoDAO.getLastUpdateInfo(session);
    if (lastUpdateInfo != null) {
      lastUpdateInfo.setLastUpdateTime(new Date());
      DataUpdateInfoDAO.getInstance().update(lastUpdateInfo, session);
    }
  }
  
  protected void update(Object obj, Session session)
  {
    super.update(obj, session);
    
    Ticket ticket = (Ticket)obj;
    
    performPostSaveOperations(session, ticket);
  }
  
  public synchronized void saveOrUpdate(Ticket ticket)
  {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      
      saveOrUpdate(ticket, session);
      
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  public synchronized void saveOrUpdateSplitTickets(List<Ticket> tickets, List<Integer> tableNumbers) {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      
      Ticket parentTicket = (Ticket)tickets.get(0);
      for (Ticket ticket : tickets) {
        ticket.addProperty("split_ticket_id", parentTicket.getId());
        ticket.addProperty("split", "true");
        saveOrUpdate(ticket, session, false);
      }
      ShopTableStatusDAO.getInstance().addTicketsToShopTableStatus(tableNumbers, tickets, session);
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  public void saveOrUpdate(Ticket ticket, Session session)
  {
    saveOrUpdate(ticket, session, true);
  }
  
  public void saveOrUpdate(Ticket ticket, Session session, boolean updateTableStatus) {
    boolean newTicket = StringUtils.isEmpty(ticket.getId());
    if (newTicket) {
      ticket.setId(NumericGlobalIdGenerator.generateGlobalId());
      ticket.setShortId(RandomStringUtils.randomNumeric(7));
      ticket.setTokenNo(Integer.valueOf(SequenceNumberDAO.getInstance().getNextSequenceNumber("TICKET_TOKEN_NUMBER", session)));
    }
    
    ticket.setActiveDate(StoreDAO.geServerTimestamp());
    ticket.updateGratuityInfo();
    
    if ((ticket.isPaid().booleanValue()) && (ticket.getDueAmount().doubleValue() > 0.0D)) {
      ticket.setPaid(Boolean.valueOf(false));
    }
    if (newTicket) {
      session.save(ticket);
    }
    else {
      session.update(ticket);
    }
    
    performPostSaveOperations(session, ticket);
    
    if (updateTableStatus) {
      updateShopTableStatus(ticket, session);
    }
  }
  
  private void clearVoidedItems(Ticket ticket) {
    for (TicketItem ticketItem : ticket.getTicketItems()) {
      ticketItem.setVoidItem(null);
    }
  }
  
  public synchronized void saveKitchenPrintStatus(Ticket ticket, List<KitchenTicket> kitchenTickets) throws Exception {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      
      for (KitchenTicket kitchenTicket : kitchenTickets)
      {

        List<KitchenTicketItem> ticketItems = kitchenTicket.getTicketItems();
        for (Iterator iterator = ticketItems.iterator(); iterator.hasNext();) {
          KitchenTicketItem kitchenTicketItem = (KitchenTicketItem)iterator.next();
          if (kitchenTicketItem.isVoided().booleanValue()) {
            String ticketItemId = kitchenTicketItem.getVoidedItemId();
            List<KitchenTicketItem> list = KitchenTicketItemDAO.getInstance().find(ticketItemId, kitchenTicketItem.isModifierItem(), session);
            if ((list != null) && (list.size() > 0)) {
              iterator.remove();
            }
          }
        }
        if (ticketItems.size() != 0)
        {

          saveOrUpdate(kitchenTicket, session); }
      }
      ticket.clearDeletedItems();
      if (ticket.getId() == null) {
        ticket.setTokenNo(Integer.valueOf(SequenceNumberDAO.getInstance().getNextSequenceNumber("TICKET_TOKEN_NUMBER", session)));
      }
      saveOrUpdate(ticket, session);
      updateStock(ticket, session);
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  private void updateShopTableStatus(Ticket ticket, Session session) {
    List<Integer> tableNumbers = ticket.getTableNumbers();
    if ((tableNumbers == null) || (tableNumbers.isEmpty())) {
      return;
    }
    if (ticket.isClosed().booleanValue()) {
      ShopTableStatusDAO.getInstance().removeTicketFromShopTableStatus(ticket, session);
    }
    else {
      ShopTableDAO.getInstance().occupyTables(ticket, session);
    }
  }
  
  public void voidTicket(Ticket ticket) throws Exception {
    Session session = null;
    Transaction tx = null;
    try {
      populateVoidItems(ticket);
      ticket.calculatePrice();
      
      session = createNewSession();
      tx = session.beginTransaction();
      
      List<KitchenTicket> kitchenTickets = KitchenTicketDAO.getInstance().findByParentId(ticket.getId());
      Date serverTimestamp = StoreDAO.geServerTimestamp();
      if (kitchenTickets != null) {
        for (KitchenTicket kitchenTicket : kitchenTickets) {
          kitchenTicket.setCreateDate(serverTimestamp);
          kitchenTicket.setVoided(Boolean.valueOf(true));
          session.saveOrUpdate(kitchenTicket);
        }
      }
      ticket.setVoided(Boolean.valueOf(true));
      ticket.setClosed(Boolean.valueOf(true));
      ticket.setClosingDate(StoreDAO.geServerTimestamp());
      getInstance().saveOrUpdate(ticket, session);
      

      String actionDescription = POSConstants.RECEIPT_REPORT_TICKET_NO_LABEL + ":" + ticket.getId() + "; Total" + ": " + NumberUtil.formatNumber(ticket.getTotalAmountWithTips());
      ActionHistoryDAO.getInstance().saveHistory(Application.getCurrentUser(), ActionHistory.VOID_CHECK, actionDescription, session);
      tx.commit();
    } catch (Exception x) {
      try {
        tx.rollback();
      }
      catch (Exception localException1) {}
      throw x;
    } finally {
      closeSession(session);
    }
  }
  
  private void populateVoidItems(Ticket ticket) {
    Map<String, Double> voidedItemQuantityMap = new HashMap();
    for (TicketItem ticketItem : ticket.getTicketItems()) {
      if (ticketItem.isVoided().booleanValue())
      {
        Double previousValue = (Double)voidedItemQuantityMap.get(ticketItem.getMenuItemId());
        if (previousValue == null) {
          previousValue = Double.valueOf(0.0D);
        }
        double voidedQuantity = 0.0D;
        voidedQuantity = Math.abs(ticketItem.getQuantity().doubleValue());
        voidedQuantity += previousValue.doubleValue();
        if (voidedQuantity != 0.0D)
        {

          voidedItemQuantityMap.put(ticketItem.getMenuItemId(), Double.valueOf(voidedQuantity)); }
      }
    }
    Object toBeVoidedItemsMap = new HashMap();
    
    for (Iterator iterator = ticket.getTicketItems().iterator(); iterator.hasNext();) {
      ticketItem = (TicketItem)iterator.next();
      if ((ticketItem.getId() != null) && (!ticketItem.isVoided().booleanValue()) && (ticketItem.getVoidItem() == null))
      {


        Double voidedQuantity = (Double)voidedItemQuantityMap.get(ticketItem.getMenuItemId());
        double toBeVoidQuantity = ticketItem.getQuantity().doubleValue();
        if ((voidedQuantity != null) && (voidedQuantity.doubleValue() > 0.0D)) {
          if (voidedQuantity.doubleValue() >= toBeVoidQuantity) {
            voidedItemQuantityMap.put(ticketItem.getMenuItemId(), Double.valueOf(voidedQuantity.doubleValue() - toBeVoidQuantity));
          }
          else
          {
            toBeVoidQuantity -= voidedQuantity.doubleValue();
          }
        } else
          ((Map)toBeVoidedItemsMap).put(ticketItem, new VoidItem(ticket.getVoidReason(), ticket.isWasted().booleanValue(), toBeVoidQuantity)); } }
    TicketItem ticketItem;
    Set<TicketItem> keys = ((Map)toBeVoidedItemsMap).keySet();
    for (TicketItem ticketItem : keys) {
      VoidItem voidItem = (VoidItem)((Map)toBeVoidedItemsMap).get(ticketItem);
      ticket.voidItem(ticketItem, voidItem.getVoidReason(), voidItem.isItemWasted().booleanValue(), voidItem.getQuantity().doubleValue());
    }
  }
  
  public void loadFullTicket(Ticket ticket) {
    if ((Hibernate.isInitialized(ticket.getTicketItems())) && (Hibernate.isInitialized(ticket.getDiscounts())) && 
      (Hibernate.isInitialized(ticket.getTransactions()))) {
      return;
    }
    

    Session session = null;
    try {
      session = createNewSession();
      session.refresh(ticket);
      
      Hibernate.initialize(ticket.getTicketItems());
      Hibernate.initialize(ticket.getDiscounts());
      Hibernate.initialize(ticket.getTransactions());
      














      closeSession(session); } finally { closeSession(session);
    }
  }
  
  public Ticket loadFullTicket(String id) {
    Session session = createNewSession();
    
    Ticket ticket = (Ticket)session.get(getReferenceClass(), id);
    
    if (ticket == null) {
      return null;
    }
    Hibernate.initialize(ticket.getTicketItems());
    Hibernate.initialize(ticket.getDiscounts());
    Hibernate.initialize(ticket.getTransactions());
    














    session.close();
    
    return ticket;
  }
  
  public Ticket loadCouponsAndTransactions(String ticketId) {
    Session session = createNewSession();
    
    Ticket ticket = (Ticket)session.get(getReferenceClass(), ticketId);
    
    Hibernate.initialize(ticket.getDiscounts());
    Hibernate.initialize(ticket.getTransactions());
    
    session.close();
    
    return ticket;
  }
  
  public List<Ticket> findOpenTickets() {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.FALSE));
      criteria.addOrder(getDefaultOrder());
      
      List list = criteria.list();
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public List<Ticket> findOpenTickets(Integer customerId) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.FALSE));
      criteria.add(Restrictions.eq(Ticket.PROP_PAID, Boolean.FALSE));
      criteria.add(Restrictions.eq(Ticket.PROP_CUSTOMER_ID, customerId));
      criteria.addOrder(getDefaultOrder());
      
      List list = criteria.list();
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public List<Ticket> findOpenTickets(Terminal terminal, UserType userType) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.FALSE));
      
      if (userType != null)
      {

        criteria.add(Restrictions.eq(Ticket.PROP_OWNER_TYPE_ID, userType.getId()));
      }
      if (terminal != null) {
        criteria.add(Restrictions.eq(Ticket.PROP_TERMINAL_ID, terminal.getId()));
      }
      criteria.addOrder(getDefaultOrder());
      
      List list = criteria.list();
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public int getNumTickets() {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      updateCriteriaFilters(criteria);
      
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      int i; if (rowCount != null) {
        return rowCount.intValue();
      }
      
      return 0;
    } finally {
      closeSession(session);
    }
  }
  
  public void loadTickets(PaginatedTableModel tableModel) {
    Session session = null;
    Criteria criteria = null;
    try
    {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      updateCriteriaFilters(criteria);
      criteria.addOrder(getDefaultOrder());
      criteria.setFirstResult(tableModel.getCurrentRowIndex());
      criteria.setMaxResults(tableModel.getPageSize());
      tableModel.setRows(criteria.list());
      return;
    }
    finally {
      closeSession(session);
    }
  }
  
  public List<Ticket> findCustomerTickets(String customerId, PaginatedTableModel tableModel) {
    Session session = null;
    Criteria criteria = null;
    try
    {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Ticket.PROP_CUSTOMER_ID, customerId));
      





      criteria.setFirstResult(0);
      criteria.setMaxResults(tableModel.getPageSize());
      
      List ticketList = criteria.list();
      
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        tableModel.setNumRows(rowCount.intValue());
      }
      

      tableModel.setCurrentRowIndex(0);
      
      return ticketList;
    }
    finally {
      closeSession(session);
    }
  }
  
  public List<Ticket> findNextCustomerTickets(Integer customerId, PaginatedTableModel tableModel, String filter) {
    Session session = null;
    Criteria criteria = null;
    try
    {
      int nextIndex = tableModel.getNextRowIndex();
      
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Ticket.PROP_CUSTOMER_ID, customerId));
      
      if (filter.equals(PaymentStatusFilter.OPEN)) {
        criteria.add(Restrictions.eq(Ticket.PROP_PAID, Boolean.FALSE));
        criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.FALSE));
      }
      
      criteria.setFirstResult(nextIndex);
      criteria.setMaxResults(tableModel.getPageSize());
      
      List ticketList = criteria.list();
      
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        tableModel.setNumRows(rowCount.intValue());
      }
      

      tableModel.setCurrentRowIndex(nextIndex);
      
      return ticketList;
    }
    finally {
      closeSession(session);
    }
  }
  
  public List<Ticket> findPreviousCustomerTickets(Integer customerId, PaginatedTableModel tableModel, String filter) {
    Session session = null;
    Criteria criteria = null;
    try
    {
      int previousIndex = tableModel.getPreviousRowIndex();
      
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Ticket.PROP_CUSTOMER_ID, customerId));
      
      if (filter.equals(PaymentStatusFilter.OPEN)) {
        criteria.add(Restrictions.eq(Ticket.PROP_PAID, Boolean.FALSE));
        criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.FALSE));
      }
      
      criteria.setFirstResult(previousIndex);
      criteria.setMaxResults(tableModel.getPageSize());
      
      List ticketList = criteria.list();
      
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        tableModel.setNumRows(rowCount.intValue());
      }
      

      tableModel.setCurrentRowIndex(previousIndex);
      
      return ticketList;
    }
    finally {
      closeSession(session);
    }
  }
  
  public List<Ticket> findTicketByCustomer(Integer customerId) {
    Session session = null;
    Criteria criteria = null;
    try
    {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Ticket.PROP_CUSTOMER_ID, customerId));
      
      List ticketList = criteria.list();
      return ticketList;
    }
    finally {
      closeSession(session);
    }
  }
  
  public List<Ticket> findTickets(PaymentStatusFilter psFilter, OrderType orderType) {
    return findTicketsForUser(psFilter, orderType, null);
  }
  
  public List<Ticket> findTicketsForUser(PaymentStatusFilter psFilter, OrderType orderType, User user) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      
      if (psFilter == PaymentStatusFilter.OPEN) {
        criteria.add(Restrictions.eq(Ticket.PROP_PAID, Boolean.FALSE));
        criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.FALSE));
      }
      else if (psFilter == PaymentStatusFilter.PAID) {
        criteria.add(Restrictions.eq(Ticket.PROP_PAID, Boolean.TRUE));
        criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.FALSE));
      }
      else if (psFilter == PaymentStatusFilter.CLOSED) {
        criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.TRUE));
        
        Calendar currentTime = Calendar.getInstance();
        currentTime.add(11, -24);
        
        criteria.add(Restrictions.ge(Ticket.PROP_CLOSING_DATE, currentTime.getTime()));
      }
      
      if (orderType != null) {
        criteria.add(Restrictions.eq(Ticket.PROP_ORDER_TYPE_ID, orderType.getId()));
      }
      
      if (user != null) {
        criteria.add(Restrictions.eq(Ticket.PROP_OWNER_ID, user.getId()));
      }
      
      List list = criteria.list();
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public boolean hasOpenTickets(User user) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.setProjection(Projections.rowCount());
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.FALSE));
      if (user != null)
        criteria.add(Restrictions.eq(Ticket.PROP_OWNER_ID, user.getId()));
      Number result = (Number)criteria.uniqueResult();
      boolean bool; if (result != null) {
        return result.intValue() > 0;
      }
      return false;
    } finally {
      closeSession(session);
    }
  }
  
  public List<Ticket> findOpenTicketsForUser(User user) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.FALSE));
      

      criteria.add(Restrictions.eq(Ticket.PROP_OWNER_ID, user == null ? null : user.getId()));
      List list = criteria.list();
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public List<Ticket> findOpenTickets(Date startDate, Date endDate) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.FALSE));
      criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, startDate));
      criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, endDate));
      List list = criteria.list();
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public List<Ticket> findClosedTickets(Date startDate, Date endDate) {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.TRUE));
      if ((startDate != null) && (endDate != null)) {
        criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, startDate));
        criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, endDate));
      }
      List list = criteria.list();
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public void closeOrder(Ticket ticket)
  {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      saveOrUpdate(ticket);
      
      User driver = ticket.getAssignedDriver();
      if (driver != null) {
        driver.setAvailableForDelivery(Boolean.valueOf(true));
        UserDAO.getInstance().saveOrUpdate(driver);
      }
      
      ShopTableDAO.getInstance().releaseTables(ticket);
      
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(TicketDAO.class).error(e);
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
  
  public List<Ticket> findTickets(Date startDate, Date endDate, boolean closed, Terminal terminal) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, startDate));
      criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, endDate));
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.TRUE));
      criteria.add(Restrictions.eq(Ticket.PROP_VOIDED, Boolean.FALSE));
      criteria.add(Restrictions.eq(Ticket.PROP_REFUNDED, Boolean.FALSE));
      
      if (terminal != null) {
        criteria.add(Restrictions.eq(Ticket.PROP_TERMINAL_ID, terminal.getId()));
      }
      
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<Ticket> findTicketsForLaborHour(Date startDate, Date endDate, int hour, Terminal terminal) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.ge(Ticket.PROP_ACTIVE_DATE, startDate));
      criteria.add(Restrictions.le(Ticket.PROP_ACTIVE_DATE, endDate));
      criteria.add(Restrictions.eq(Ticket.PROP_CREATION_HOUR, Integer.valueOf(hour)));
      


      if (terminal != null) {
        criteria.add(Restrictions.eq(Ticket.PROP_TERMINAL_ID, terminal.getId()));
      }
      
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<Ticket> findTicketsForShift(Date startDate, Date endDate, Shift shit, Terminal terminal) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, startDate));
      criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, endDate));
      criteria.add(Restrictions.eq(Ticket.PROP_SHIFT_ID, shit == null ? null : shit.getId()));
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.TRUE));
      criteria.add(Restrictions.eq(Ticket.PROP_VOIDED, Boolean.FALSE));
      criteria.add(Restrictions.eq(Ticket.PROP_REFUNDED, Boolean.FALSE));
      
      if (terminal != null) {
        criteria.add(Restrictions.eq(Ticket.PROP_TERMINAL_ID, terminal.getId()));
      }
      
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public static TicketDAO getInstance() {
    return instance;
  }
  
  private void updateCriteriaFilters(Criteria criteria) {
    User user = Application.getCurrentUser();
    PaymentStatusFilter paymentStatusFilter = TerminalConfig.getPaymentStatusFilter();
    String orderTypeFilter = TerminalConfig.getOrderTypeFilter();
    boolean filterByMyTicket = TerminalConfig.isFilterByOwner();
    OrderType orderType = null;
    if (!"ALL".equals(orderTypeFilter)) {
      orderType = OrderTypeDAO.getInstance().findByName(orderTypeFilter);
    }
    if (paymentStatusFilter == PaymentStatusFilter.OPEN) {
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.FALSE));
      
      if ((!user.canViewAllOpenTickets()) || (filterByMyTicket)) {
        criteria.add(Restrictions.eq(Ticket.PROP_OWNER_ID, user.getId()));
      }
    }
    else if (paymentStatusFilter == PaymentStatusFilter.CLOSED) {
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.TRUE));
      StoreSessionControl currentStoreOperation = StoreUtil.getCurrentStoreOperation();
      if ((currentStoreOperation != null) && (currentStoreOperation.getCurrentData() != null)) {
        Date openTime = currentStoreOperation.getCurrentData().getOpenTime();
        criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, openTime));
      }
      
      if ((!user.canViewAllCloseTickets()) || (filterByMyTicket)) {
        criteria.add(Restrictions.eq(Ticket.PROP_OWNER_ID, user.getId()));
      }
    }
    
    if (!orderTypeFilter.equals(POSConstants.ALL)) {
      criteria.add(Restrictions.eq(Ticket.PROP_ORDER_TYPE_ID, orderType.getId()));
    }
  }
  
  public void deleteTickets(List<Ticket> tickets)
  {
    deleteTickets(tickets, false);
  }
  
  public void deleteTickets(List<Ticket> tickets, boolean releaseTables) {
    deleteTickets(tickets, releaseTables, false);
  }
  
  public void deleteTickets(List<Ticket> tickets, boolean releaseTables, boolean updateStock) {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      deleteTickets(session, tickets, releaseTables, updateStock);
      
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      LogFactory.getLog(TicketDAO.class).error(e);
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
  
  public void deleteTickets(Session session, List<Ticket> tickets, boolean releaseTables) throws Exception {
    deleteTickets(session, tickets, releaseTables, false);
  }
  
  public void deleteTickets(Session session, List<Ticket> tickets, boolean releaseTables, boolean updateStock) throws Exception {
    for (Ticket ticket : tickets) {
      if (!ticket.isClosed().booleanValue()) {
        List<TicketItem> removedTicketItems = new ArrayList();
        removedTicketItems.addAll(ticket.getTicketItems());
        for (Iterator iterator = removedTicketItems.iterator(); iterator.hasNext();) {
          TicketItem ticketItem = (TicketItem)iterator.next();
          ticket.addDeletedItems(ticketItem);
        }
        



        if ((updateStock) && (ticket.getPaidAmount().doubleValue() > 0.0D))
          getInstance().updateStock(ticket, session);
      }
      super.delete(ticket, session);
      if (releaseTables) {
        List<Integer> tableNumbers = ticket.getTableNumbers();
        if ((tableNumbers != null) && (!tableNumbers.isEmpty())) {
          ShopTableDAO.getInstance().freeTables(tableNumbers, session);
        }
      }
    }
  }
  




































































  protected void updateStock(Ticket ticket, Session session)
  {
    try
    {
      if (!shouldUpdateStock(ticket)) {
        return;
      }
      InventoryLocation defaultOutInventoryLocation = null;
      InventoryLocation defaultInInventoryLocation = null;
      defaultOutInventoryLocation = DataProvider.get().getDefaultOutLocation();
      if (defaultOutInventoryLocation == null) {
        return;
      }
      HashMap<String, Double> itemMap = buildItemMapForInventoryAdjustment(ticket);
      HashMap<String, Double> voidedItemsMap = buildItemMapForVoidItems(ticket, session);
      adjustInventory(ticket, itemMap, InventoryTransactionType.OUT, "TICKET SALES", defaultOutInventoryLocation, session);
      if ((voidedItemsMap != null) && (voidedItemsMap.size() > 0)) {
        defaultInInventoryLocation = DataProvider.get().getDefaultInLocation();
        adjustInventory(ticket, voidedItemsMap, InventoryTransactionType.IN, "VOID", defaultInInventoryLocation, session);
      }
    } catch (Exception e) {
      PosLog.error(getClass(), "Failed to update stock balance for ticket: " + ticket.getId());
    }
  }
  
  private void adjustInventory(Ticket ticket, HashMap<String, Double> itemMap, InventoryTransactionType transactionType, String reason, InventoryLocation location, Session session) throws Exception
  {
    Store store = Application.getInstance().getStore();
    boolean isUpdateOnHandBlncForSale = store.isUpdateOnHandBlncForSale();
    boolean isUpdateAvailBlncForSale = store.isUpdateAvlBlncForSale();
    for (String menuItemId : itemMap.keySet()) {
      Double unitQuantity = (Double)itemMap.get(menuItemId);
      MenuItem menuItem = MenuItemDAO.getInstance().getMenuItemWithFields(session, menuItemId, new String[] { MenuItem.PROP_NAME, MenuItem.PROP_PRICE, MenuItem.PROP_SKU, MenuItem.PROP_BARCODE, MenuItem.PROP_UNIT_ID, MenuItem.PROP_COST, MenuItem.PROP_AVERAGE_UNIT_PURCHASE_PRICE, MenuItem.PROP_AVG_COST, MenuItem.PROP_DEFAULT_RECIPE_ID });
      

      if (menuItem != null)
      {

        InventoryTransaction outTrans = new InventoryTransaction();
        outTrans.setReason(reason);
        outTrans.setTransactionDate(new Date());
        outTrans.setMenuItem(menuItem);
        outTrans.setType(Integer.valueOf(transactionType.getType()));
        outTrans.setTicketId(ticket.getId());
        outTrans.setUser(ticket.getOwner());
        InventoryUnit unit = menuItem.getUnit();
        outTrans.setUnitPrice(menuItem.getPrice());
        outTrans.setQuantity(unitQuantity);
        if (unit != null)
          outTrans.setUnit(unit.getCode());
        outTrans.setUnitCost(menuItem.getAverageUnitPurchasePrice());
        
        if (transactionType == InventoryTransactionType.IN) {
          outTrans.setToInventoryLocation(location);
        }
        else {
          outTrans.setFromInventoryLocation(location);
        }
        outTrans.setTotal(Double.valueOf(outTrans.getUnitCost().doubleValue() * outTrans.getQuantity().doubleValue()));
        InventoryTransactionDAO.getInstance().adjustInventoryStock(outTrans, session, isUpdateAvailBlncForSale, isUpdateOnHandBlncForSale);
        
        MenuItemInventoryStatus stockStatus = MenuItemInventoryStatusDAO.getInstance().get(menuItemId, session);
        if ((stockStatus != null) && (stockStatus.getAvailableUnit().doubleValue() <= 0.0D) && (StringUtils.isNotEmpty(menuItem.getDefaultRecipeId()))) {
          Recepie recepie = RecepieDAO.getInstance().get(menuItem.getDefaultRecipeId(), session);
          RecepieDAO.getInstance().adjustRecipeItemsFromInventory(Arrays.asList(new Recepie[] { recepie }), session);
        }
      }
    }
  }
  
  private HashMap<String, Double> buildItemMapForInventoryAdjustment(Ticket ticket) { List<TicketItem> ticketItems = ticket.getTicketItems();
    HashMap<String, Double> itemMap = new HashMap();
    populateItemToMap(ticketItems, itemMap);
    return itemMap;
  }
  
  private void populateItemToMap(List<TicketItem> ticketItems, HashMap<String, Double> itemMap) {
    for (TicketItem ticketItem : ticketItems)
      if ((ticketItem.isInventoryItem().booleanValue()) && (ticketItem.getMenuItemId() != null) && (!ticketItem.isVoided().booleanValue()) && (!ticketItem.isInventoryAdjusted()))
      {

        Double previousValue = (Double)itemMap.get(ticketItem.getMenuItemId());
        if (previousValue == null) {
          previousValue = Double.valueOf(0.0D);
        }
        Double toBeAdjustQty = ticketItem.getQuantity();
        toBeAdjustQty = Double.valueOf(toBeAdjustQty.doubleValue() - ticketItem.getInventoryAdjustQty().doubleValue());
        toBeAdjustQty = Double.valueOf(toBeAdjustQty.doubleValue() + previousValue.doubleValue());
        if (toBeAdjustQty.doubleValue() > 0.0D)
        {











          itemMap.put(ticketItem.getMenuItemId(), toBeAdjustQty);
          ticketItem.setInventoryAdjustQty(ticketItem.getQuantity());
          if (ticketItem.isComboItem().booleanValue())
            populateItemToMap(ticketItem.getComboItems(), itemMap);
        }
      }
  }
  
  private HashMap<String, Double> buildItemMapForVoidItems(Ticket ticket, Session session) { List<TicketItem> ticketItems = ticket.getTicketItems();
    HashMap<String, Double> voidItemMap = new HashMap();
    populateVoidItemToMap(ticket, session, ticketItems, voidItemMap);
    return voidItemMap;
  }
  
  private void populateVoidItemToMap(Ticket ticket, Session session, List<TicketItem> ticketItems, HashMap<String, Double> voidItemMap) {
    for (TicketItem voidTicketItem : ticketItems) {
      VoidItem voidItem = voidTicketItem.getVoidItem();
      if ((voidTicketItem.isInventoryItem().booleanValue()) && (voidTicketItem.getMenuItemId() != null) && (voidTicketItem.isVoided().booleanValue()) && (voidItem != null))
      {

        Double previousValue = (Double)voidItemMap.get(voidTicketItem.getMenuItemId());
        if (previousValue == null) {
          previousValue = Double.valueOf(0.0D);
        }
        double toBeAdjustQty = 0.0D;
        toBeAdjustQty = voidItem.getQuantity().doubleValue();
        toBeAdjustQty += previousValue.doubleValue();
        if (toBeAdjustQty != 0.0D)
        {

          if (!voidTicketItem.isInventoryAdjusted()) {
            voidItemMap.put(voidTicketItem.getMenuItemId(), Double.valueOf(toBeAdjustQty));
            voidTicketItem.setInventoryAdjustQty(voidTicketItem.getQuantity());
          }
          Boolean isPrintedToKitchen = voidTicketItem.isPrintedToKitchen();
          VoidItemDAO.getInstance().saveAndSentToKitchenIfNeeded(voidItem, isPrintedToKitchen.booleanValue(), ticket, session);
          
          List<VoidItem> voidedModifiers = voidItem.getVoidedModifiers();
          if ((voidedModifiers != null) && (voidedModifiers.size() > 0)) {
            for (VoidItem voidedModifier : voidedModifiers) {
              VoidItemDAO.getInstance().saveAndSentToKitchenIfNeeded(voidedModifier, isPrintedToKitchen.booleanValue(), ticket, session);
            }
          }
          if (voidTicketItem.isComboItem().booleanValue())
            populateVoidItemToMap(ticket, session, voidTicketItem.getComboItems(), voidItemMap);
        }
      }
    } }
  
  public int getNumTickets(Date start, Date end) { Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.FALSE));
      if (start != null) {
        criteria.add(Restrictions.ge(Ticket.PROP_DELIVERY_DATE, start));
      }
      if (end != null) {
        criteria.add(Restrictions.le(Ticket.PROP_DELIVERY_DATE, end));
      }
      criteria.add(Restrictions.isNotNull(Ticket.PROP_DELIVERY_DATE));
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      int i; if (rowCount != null) {
        return rowCount.intValue();
      }
      return 0;
    } finally {
      closeSession(session);
    }
  }
  
  public void loadTickets(PaginatedTableModel tableModel, Date beginingDeliveryDate, Date endingDeliveryDate) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.addOrder(getDefaultOrder());
      
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.FALSE));
      if (beginingDeliveryDate != null) {
        criteria.add(Restrictions.ge(Ticket.PROP_DELIVERY_DATE, beginingDeliveryDate));
      }
      if (endingDeliveryDate != null) {
        criteria.add(Restrictions.le(Ticket.PROP_DELIVERY_DATE, endingDeliveryDate));
      }
      
      criteria.setFirstResult(tableModel.getCurrentRowIndex());
      criteria.setMaxResults(tableModel.getPageSize());
      tableModel.setRows(criteria.list());
      return;
    }
    finally {
      closeSession(session);
    }
  }
  
  public List<Ticket> getTicketsWithSpecificFields(String... fields) {
    Session session = null;
    Criteria criteria = null;
    User currentUser = Application.getCurrentUser();
    boolean filterUser = (!currentUser.isAdministrator()) || (!currentUser.isManager());
    try {
      session = createNewSession();
      criteria = session.createCriteria(Ticket.class);
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.FALSE));
      if (filterUser)
      {

        criteria.add(Restrictions.eq(Ticket.PROP_OWNER_ID, currentUser.getId()));
      }
      
      ProjectionList projectionList = Projections.projectionList();
      for (String field : fields) {
        projectionList.add(Projections.property(field), field);
      }
      
      criteria.setProjection(projectionList);
      criteria.setResultTransformer(Transformers.aliasToBean(Ticket.class));
      Object list = criteria.list();
      for (Object localObject1 = ((List)list).iterator(); ((Iterator)localObject1).hasNext();) { Ticket ticket = (Ticket)((Iterator)localObject1).next();
        String query = "select TABLE_ID from TICKET_TABLE_NUM where TICKET_ID ='" + ticket.getId() + "'";
        
        SQLQuery createSQLQuery = session.createSQLQuery(query);
        List tableList = createSQLQuery.list();
        ticket.setTableNumbers(tableList);
      }
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public boolean hasTicketByOnlineOrderId(String onlineOrderId) {
    Session session = null;
    try {
      session = createNewSession();
      String sql = "select * from TICKET_PROPERTIES where PROPERTY_VALUE='%s' and PROPERTY_NAME='%s'";
      sql = String.format(sql, new Object[] { onlineOrderId, "onlineOrderId" });
      SQLQuery sqlQuery = session.createSQLQuery(sql);
      List list = sqlQuery.list();
      return list.size() > 0;
    } finally {
      closeSession(session);
    }
  }
  
  public boolean hasTicketByReservationId(String reservationId) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Ticket.PROP_ID, reservationId));
      criteria.add(Restrictions.eq(Ticket.PROP_TYPE, Integer.valueOf(TicketType.RESERVATION.getTypeNo())));
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        return rowCount.intValue() > 0;
      }
    }
    catch (Exception e) {
      PosLog.error(getReferenceClass(), e);
    } finally {
      closeSession(session);
    }
    return false;
  }
  
  public List<Ticket> findOnlineTickets(PaginatedTableModel tableModel) {
    Session session = null;
    try
    {
      session = createNewSession();
      String sql = "select TICKET_ID from TICKET_PROPERTIES where PROPERTY_VALUE='online' and PROPERTY_NAME='source'";
      SQLQuery sqlQuery = session.createSQLQuery(sql);
      List<String> list = sqlQuery.list();
      List<Ticket> ticketList = new ArrayList();
      for (Object localObject1 = list.iterator(); ((Iterator)localObject1).hasNext();) { String ticketId = (String)((Iterator)localObject1).next();
        if (ticketId != null)
        {

          Ticket ticket = get(ticketId);
          ticketList.add(ticket);
        }
      }
      return ticketList;
    }
    finally {
      closeSession(session);
    }
  }
  


















  public void doDriverOut(List<Ticket> tickets, List<User> drivers)
  {
    Session session = null;
    Transaction transaction = null;
    try {
      session = getInstance().createNewSession();
      transaction = session.beginTransaction();
      
      for (Ticket ticket : tickets) {
        ticket.addProperty("OUT_AT", DateUtil.getReportDate());
        User driver = ticket.getAssignedDriver();
        if ((driver != null) && (!drivers.contains(driver))) {
          drivers.add(driver);
        }
        session.saveOrUpdate(ticket);
      }
      transaction.commit();
    } catch (Exception e) {
      POSMessageDialog.showError(e.getMessage());
      if (transaction != null)
        transaction.rollback();
    } finally {
      closeSession(session);
    }
  }
  

  public void findTicketForDeliveryDispath(PaginatedTableModel model, OrderType orderType, String customerId, Date startDate, Date endDate, boolean onlineOrderOnly, boolean isPickupOnly, boolean isDeliveryOnly, boolean filterUnassigned)
  {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.addOrder(getDefaultOrder());
      criteria.add(Restrictions.eq(Ticket.PROP_ORDER_TYPE_ID, orderType.getId()));
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.FALSE));
      
      if (StringUtils.isNotEmpty(customerId)) {
        criteria.add(Restrictions.eq(Ticket.PROP_CUSTOMER_ID, customerId));
      }
      
      if ((startDate != null) && (endDate != null)) {
        criteria.add(Restrictions.and(Restrictions.ge(Ticket.PROP_DELIVERY_DATE, startDate), Restrictions.le(Ticket.PROP_DELIVERY_DATE, endDate)));
      }
      if (isPickupOnly) {
        criteria.add(Restrictions.eq(Ticket.PROP_CUSTOMER_WILL_PICKUP, Boolean.TRUE));
      }
      if (isDeliveryOnly) {
        criteria.add(Restrictions.eq(Ticket.PROP_CUSTOMER_WILL_PICKUP, Boolean.FALSE));
      }
      if (filterUnassigned) {
        criteria.add(Restrictions.isNull(Ticket.PROP_ASSIGNED_DRIVER_ID));
      }
      int currentRowIndex = model.getCurrentRowIndex();
      criteria.setFirstResult(currentRowIndex);
      criteria.setMaxResults(model.getPageSize());
      
      List<Ticket> list = criteria.list();
      Iterator iterator; if (onlineOrderOnly) {
        for (iterator = list.iterator(); iterator.hasNext();) {
          Ticket ticket = (Ticket)iterator.next();
          if (!ticket.isSourceOnline()) {
            iterator.remove();
          }
        }
      }
      
      model.setRows(list);
    } finally {
      closeSession(session);
    }
  }
  
  public static synchronized void closeOrders(Ticket... tickets) {
    Session session = getInstance().createNewSession();
    Transaction transaction = null;
    if (tickets == null) {
      return;
    }
    try {
      transaction = session.beginTransaction();
      Date serverTimestamp = StoreDAO.geServerTimestamp();
      for (Ticket ticket : tickets) {
        ticket = getInstance().loadFullTicket(ticket.getId());
        ticket.setClosed(Boolean.valueOf(true));
        ticket.setClosingDate(serverTimestamp);
        
        session.saveOrUpdate(ticket);
      }
      transaction.commit();
    } catch (Exception e) {
      PosLog.error(OrderController.class, e);
      POSMessageDialog.showError(e.getMessage());
      
      if (transaction != null)
        transaction.rollback();
    } finally {
      session.close();
    }
  }
  
  public List<Ticket> findBarTabOpenTickets(OrderType orderType) {
    Session session = null;
    try
    {
      List<String> barTabOrderTypes = new ArrayList();
      List<OrderType> orderTypes = DataProvider.get().getOrderTypes();
      if (orderTypes != null) {
        for (OrderType ot : orderTypes) {
          if (ot.isBarTab().booleanValue())
            barTabOrderTypes.add(ot.getId());
        }
      }
      if ((barTabOrderTypes == null) || (barTabOrderTypes.isEmpty())) {
        return null;
      }
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      User user = Application.getCurrentUser();
      if ((user != null) && (!user.canViewAllOpenTickets())) {
        criteria.add(Restrictions.eq(Ticket.PROP_OWNER_ID, user.getId()));
      }
      criteria.add(Restrictions.eq(Ticket.PROP_ORDER_TYPE_ID, orderType == null ? null : orderType.getId()));
      

      criteria.add(Restrictions.in(Ticket.PROP_ORDER_TYPE_ID, barTabOrderTypes));
      criteria.add(Restrictions.or(Restrictions.isEmpty("tableNumbers"), Restrictions.isNull("tableNumbers")));
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.FALSE));
      criteria.addOrder(getDefaultOrder());
      
      List list = criteria.list();
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public List<Ticket> findVoidTicketByDate(Date startDate, Date endDate, Terminal terminal) {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Ticket.PROP_VOIDED, Boolean.valueOf(true)));
      criteria.add(Restrictions.ge(Ticket.PROP_CREATE_DATE, startDate));
      criteria.add(Restrictions.le(Ticket.PROP_CLOSING_DATE, endDate));
      
      if (terminal != null) {
        criteria.add(Restrictions.eq(VoidItem.PROP_TERMINAL, terminal));
      }
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public User findOwner(String ticketId) {
    if (ticketId == null) {
      return null;
    }
    Session session = getSession();
    Criteria criteria = session.createCriteria(Ticket.class);
    
    criteria.add(Restrictions.eq(Ticket.PROP_ID, ticketId));
    
    ProjectionList projectionList = Projections.projectionList();
    
    projectionList.add(Projections.property(Ticket.PROP_OWNER_ID));
    criteria.setProjection(projectionList);
    if (criteria.list().isEmpty()) {
      return null;
    }
    
    User user = UserDAO.getInstance().get((String)criteria.list().get(0));
    return user;
  }
  
  public Ticket findByCustomerAndDeliveryDate(String customerId, Date deliveryDate) {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(Ticket.class);
      
      criteria.add(Restrictions.eq(Ticket.PROP_CUSTOMER_ID, customerId));
      criteria.add(Restrictions.between(Ticket.PROP_DELIVERY_DATE, DateUtil.startOfDay(deliveryDate), DateUtil.endOfDay(deliveryDate)));
      criteria.add(Restrictions.eq(Ticket.PROP_CLOSED, Boolean.valueOf(false)));
      List list = criteria.list();
      Ticket localTicket; if (!list.isEmpty()) {
        return (Ticket)list.get(0);
      }
      return null;
    } finally {
      closeSession(session);
    }
  }
  
  public void saveMergedTickets(List<Ticket> tickets, Ticket rootTicket) throws Exception
  {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      
      for (Ticket ticket : tickets) {
        update(ticket, session);
      }
      deleteTickets(session, tickets, true);
      saveOrUpdate(rootTicket, session, true);
      
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  public void saveOrUpdateTransferedTicketsList(List<Ticket> ticketsToDelete, List<Ticket> ticketsToUpdate) throws Exception {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      
      if ((ticketsToDelete != null) || (ticketsToDelete.size() > 0)) {
        deleteTickets(session, ticketsToDelete, true);
      }
      
      for (Ticket ticket : ticketsToUpdate) {
        saveOrUpdate(ticket, session);
      }
      
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  public void reversePayment(Ticket ticket, PosTransaction transaction, boolean forceVoid) {
    String errorMessage = "Failed to reverse payment.";
    if ((!forceVoid) && ((transaction instanceof CreditCardTransaction))) {
      CardProcessor cardProcessor = CardConfig.getPaymentGateway().getProcessor();
      try {
        cardProcessor.voidTransaction(transaction);
        if (!transaction.isVoided().booleanValue()) {
          cardProcessor.refundTransaction(transaction, transaction.getAmount().doubleValue());
          if (!transaction.isRefunded()) {
            throw new PosException(errorMessage);
          }
        }
      } catch (Exception e2) {
        try {
          cardProcessor.refundTransaction(transaction, transaction.getAmount().doubleValue());
          if (!transaction.isRefunded()) {
            throw new PosException(errorMessage);
          }
        } catch (Exception e3) {
          throw new PosException(errorMessage);
        }
      }
    }
    
    transaction.setVoided(Boolean.valueOf(true));
    ticket.setPaidAmount(Double.valueOf(ticket.getPaidAmount().doubleValue() - transaction.getAmount().doubleValue()));
    ticket.setDueAmount(Double.valueOf(ticket.getDueAmount().doubleValue() + transaction.getAmount().doubleValue()));
    ticket.setClosed(Boolean.valueOf(false));
    ticket.setPaid(Boolean.valueOf(false));
    
    update(ticket);
  }
  
  public void loadTicketsForUser(User user, Date from, Date to, PaginatedTableModel listModel) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.setProjection(Projections.rowCount());
      if (user != null)
        criteria.add(Restrictions.eq(Ticket.PROP_OWNER_ID, user.getId()));
      if (from != null)
        criteria.add(Restrictions.or(Restrictions.ge(Ticket.PROP_CREATE_DATE, from), Restrictions.eq(Ticket.PROP_CLOSED, Boolean.valueOf(false))));
      if (to != null) {
        criteria.add(Restrictions.le(Ticket.PROP_CREATE_DATE, to));
      }
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        listModel.setNumRows(rowCount.intValue());
      }
      else {
        listModel.setNumRows(0);
      }
      criteria.setProjection(null);
      criteria.setFirstResult(listModel.getCurrentRowIndex());
      criteria.setMaxResults(listModel.getPageSize());
      criteria.addOrder(Order.desc(Ticket.PROP_CREATE_DATE));
      listModel.setRows(criteria.list());
    } finally {
      closeSession(session);
    }
  }
  
  public List<Ticket> findUnsyncedTickets() {
    Session session = null;
    Criteria criteria = null;
    try
    {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Ticket.PROP_CLOUD_SYNCED, Boolean.FALSE));
      criteria.addOrder(Order.asc(Ticket.PROP_CREATE_DATE));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  private boolean shouldUpdateStock(Ticket ticket) {
    if (ticket.isReservation()) {
      return true;
    }
    if (((ticket.getPaidAmount().doubleValue() > 0.0D) || (ticket.hasRefundableItem().booleanValue())) && (!ticket.isSourceOnline())) {
      return true;
    }
    return false;
  }
  
  public List<Ticket> getTicketsOfCurrentSession(StoreSession storeSession) {
    Session session = null;
    Criteria criteria = null;
    try
    {
      List<String> cashDrawerIds = CashDrawerDAO.getInstance().getCashDrawerIds(storeSession);
      Object localObject1; if ((cashDrawerIds == null) || (cashDrawerIds.isEmpty())) {
        return null;
      }
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Ticket.PROP_CLOUD_SYNCED, Boolean.FALSE));
      
      criteria.createAlias("transactions", "tx");
      criteria.add(Restrictions.in("tx." + PosTransaction.PROP_CASH_DRAWER_ID, cashDrawerIds));
      criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
}
