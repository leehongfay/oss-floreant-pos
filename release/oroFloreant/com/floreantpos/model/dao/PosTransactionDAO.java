package com.floreantpos.model.dao;

import com.floreantpos.PosLog;
import com.floreantpos.model.CreditCardTransaction;
import com.floreantpos.model.Customer;
import com.floreantpos.model.CustomerAccountTransaction;
import com.floreantpos.model.Outlet;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.ReversalTransaction;
import com.floreantpos.model.StoreSession;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TransactionType;
import com.floreantpos.model.User;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.model.util.TransactionSummary;
import com.floreantpos.report.CustomerPaymentReportView.CustomerAccountTransactionItem;
import com.floreantpos.services.PosTransactionService;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;























public class PosTransactionDAO
  extends BasePosTransactionDAO
{
  public PosTransactionDAO() {}
  
  public List<PosTransaction> findUnauthorizedTransactions()
  {
    return findUnauthorizedTransactions(null);
  }
  
  public List<PosTransaction> findUnauthorizedTransactions(User owner) {
    Session session = null;
    try
    {
      session = getSession();
      
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(PosTransaction.PROP_CAPTURED, Boolean.FALSE));
      criteria.add(Restrictions.eq(PosTransaction.PROP_AUTHORIZABLE, Boolean.TRUE));
      criteria.add(Restrictions.eq(PosTransaction.PROP_TRANSACTION_TYPE, TransactionType.CREDIT.name()));
      criteria.add(Restrictions.or(Restrictions.isNull(PosTransaction.PROP_VOIDED), Restrictions.eq(PosTransaction.PROP_VOIDED, Boolean.FALSE)));
      criteria.add(Restrictions.isNotNull(PosTransaction.PROP_TICKET));
      
      if (owner != null) {
        criteria.add(Restrictions.eq(PosTransaction.PROP_USER_ID, owner.getId()));
      }
      
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public Boolean hasUnauthorizedTransactions(User owner) {
    Session session = null;
    try {
      session = getSession();
      
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.setProjection(Projections.rowCount());
      criteria.add(Restrictions.eq(PosTransaction.PROP_CAPTURED, Boolean.FALSE));
      criteria.add(Restrictions.eq(PosTransaction.PROP_AUTHORIZABLE, Boolean.TRUE));
      criteria.add(Restrictions.eq(PosTransaction.PROP_TRANSACTION_TYPE, TransactionType.CREDIT.name()));
      criteria.add(Restrictions.isNotNull(PosTransaction.PROP_TICKET));
      if (owner != null) {
        criteria.add(Restrictions.eq(PosTransaction.PROP_USER_ID, owner.getId()));
      }
      Number rowCount = (Number)criteria.uniqueResult();
      return Boolean.valueOf((rowCount != null) && (rowCount.intValue() > 0));
    } finally {
      closeSession(session);
    }
  }
  
  public List<? extends PosTransaction> findTransactions(Terminal terminal, Class transactionClass, Date from, Date to) {
    return findTransactions(terminal, transactionClass, from, to, true);
  }
  
  public List<? extends PosTransaction> findTransactions(Terminal terminal, Class transactionClass, Date from, Date to, boolean excludeNullTicket) {
    return findTransactions(terminal, null, transactionClass, from, to, excludeNullTicket);
  }
  
  public List<? extends PosTransaction> findTransactions(Terminal terminal, Outlet outlet, Class transactionClass, Date from, Date to, boolean excludeNullTicket) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(transactionClass);
      if (excludeNullTicket) {
        criteria.add(Restrictions.isNotNull(PosTransaction.PROP_TICKET));
      }
      if (terminal != null) {
        criteria.add(Restrictions.eq(PosTransaction.PROP_TERMINAL_ID, terminal.getId()));
      }
      
      if ((outlet == null) || (
      



        (from != null) && (to != null))) {
        criteria.add(Restrictions.between(PosTransaction.PROP_TRANSACTION_TIME, from, to));
      }
      ProjectionList pList = Projections.projectionList();
      pList.add(Projections.property(PosTransaction.PROP_TICKET), PosTransaction.PROP_TICKET);
      pList.add(Projections.property(PosTransaction.PROP_PAYMENT_TYPE), PosTransaction.PROP_PAYMENT_TYPE);
      pList.add(Projections.property(PosTransaction.PROP_CARD_TYPE), PosTransaction.PROP_CARD_TYPE);
      pList.add(Projections.property(PosTransaction.PROP_CARD_READER), PosTransaction.PROP_CARD_READER);
      pList.add(Projections.property(PosTransaction.PROP_TRANSACTION_TIME), PosTransaction.PROP_TRANSACTION_TIME);
      pList.add(Projections.property(PosTransaction.PROP_USER_ID), PosTransaction.PROP_USER_ID);
      pList.add(Projections.property(PosTransaction.PROP_CARD_AUTH_CODE), PosTransaction.PROP_CARD_AUTH_CODE);
      pList.add(Projections.property(PosTransaction.PROP_TIPS_AMOUNT), PosTransaction.PROP_TIPS_AMOUNT);
      pList.add(Projections.property(PosTransaction.PROP_AMOUNT), PosTransaction.PROP_AMOUNT);
      pList.add(Projections.property(PosTransaction.PROP_TERMINAL_ID), PosTransaction.PROP_TERMINAL_ID);
      
      pList.add(Projections.property(CustomerAccountTransaction.PROP_CUSTOMER_ID), CustomerAccountTransaction.PROP_CUSTOMER_ID);
      criteria.setProjection(pList);
      
      criteria.setResultTransformer(Transformers.aliasToBean(PosTransaction.class));
      criteria.addOrder(Order.asc(PosTransaction.PROP_TRANSACTION_TIME));
      
      List<PosTransaction> list = criteria.list();
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public TransactionSummary getTransactionSummary(Terminal terminal, Class transactionClass, Date from, Date to) {
    Session session = null;
    TransactionSummary summary = new TransactionSummary();
    try {
      session = getSession();
      
      Criteria criteria = session.createCriteria(transactionClass);
      criteria.add(Restrictions.eq(PosTransaction.PROP_DRAWER_RESETTED, Boolean.FALSE));
      
      if (terminal != null) {
        criteria.add(Restrictions.eq(PosTransaction.PROP_TERMINAL_ID, terminal.getId()));
      }
      
      if ((from != null) && (to != null)) {
        criteria.add(Restrictions.ge(PosTransaction.PROP_TRANSACTION_TIME, from));
        criteria.add(Restrictions.le(PosTransaction.PROP_TRANSACTION_TIME, to));
      }
      
      ProjectionList projectionList = Projections.projectionList();
      projectionList.add(Projections.count(PosTransaction.PROP_ID));
      projectionList.add(Projections.sum(PosTransaction.PROP_AMOUNT));
      projectionList.add(Projections.sum(PosTransaction.PROP_TIPS_AMOUNT));
      
      criteria.setProjection(projectionList);
      
      List list = criteria.list();
      
      if ((list == null) || (list.size() == 0)) {
        return summary;
      }
      Object[] o = (Object[])list.get(0);
      int index = 0;
      
      summary.setCount(HibernateProjectionsUtil.getInt(o, index++));
      summary.setAmount(HibernateProjectionsUtil.getDouble(o, index++));
      summary.setTipsAmount(HibernateProjectionsUtil.getDouble(o, index++));
      
      return summary;
    } finally {
      closeSession(session);
    }
  }
  
  public List<PosTransaction> findTransactionListByGiftCardNumber(String giftCardNumber, Date fromDate, Date toDate) {
    Session session = null;
    try
    {
      session = getSession();
      
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.between(PosTransaction.PROP_TRANSACTION_TIME, DateUtil.startOfDay(fromDate), DateUtil.endOfDay(toDate)));
      criteria.add(Restrictions.eq(PosTransaction.PROP_GIFT_CERT_NUMBER, giftCardNumber));
      
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<PosTransaction> findCapturedTransactions(User owner) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(CreditCardTransaction.class);
      criteria.add(Restrictions.eq(PosTransaction.PROP_CAPTURED, Boolean.TRUE));
      criteria.add(Restrictions.or(Restrictions.isNull(PosTransaction.PROP_VOIDED), Restrictions.eq(PosTransaction.PROP_VOIDED, Boolean.FALSE)));
      criteria.add(Restrictions.isNotNull(PosTransaction.PROP_TICKET));
      if (owner != null) {
        criteria.add(Restrictions.eq(PosTransaction.PROP_USER_ID, owner.getId()));
      }
      Calendar calendar = Calendar.getInstance();
      calendar.add(5, -1);
      Date startOfDay = DateUtil.startOfDay(calendar.getTime());
      Date endOfDay = DateUtil.endOfDay(new Date());
      criteria.add(Restrictions.between(PosTransaction.PROP_TRANSACTION_TIME, startOfDay, endOfDay));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<PosTransaction> getStoreSessionTransactions(StoreSession storeOperationData) {
    Session session = null;
    try {
      List<String> cashDrawerIds = CashDrawerDAO.getInstance().getCashDrawerIds(storeOperationData);
      if ((cashDrawerIds == null) || (cashDrawerIds.isEmpty()))
        return null;
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass(), "t");
      


      criteria.createAlias(PosTransaction.PROP_TICKET, "ticket");
      criteria.add(Restrictions.eq("ticket.closed", Boolean.valueOf(true)));
      criteria.add(Restrictions.eq("ticket.voided", Boolean.valueOf(false)));
      criteria.add(Restrictions.in(PosTransaction.PROP_CASH_DRAWER_ID, cashDrawerIds));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public void saveReversalTransaction(Ticket ticket, PosTransaction transaction, ReversalTransaction reversalTransaction) {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      delete(transaction, session);
      saveOrUpdate(reversalTransaction, session);
      TicketDAO.getInstance().update(ticket, session);
      
      tx.commit();
    } catch (Exception e) {
      try {
        tx.rollback();
      } catch (Exception x) {
        PosLog.error(PosTransactionService.class, x);
        x.printStackTrace();
      }
      throw e;
    } finally {
      TerminalDAO.getInstance().closeSession(session);
    }
  }
  
  public List<PosTransaction> findCreditTransactions(Date fromDate, Date toDate, User user) {
    Session session = null;
    Criteria criteria = null;
    try
    {
      session = createNewSession();
      
      criteria = session.createCriteria(PosTransaction.class);
      criteria.add(Restrictions.eq(PosTransaction.PROP_TRANSACTION_TYPE, TransactionType.CREDIT.name()));
      criteria.add(Restrictions.between(PosTransaction.PROP_TRANSACTION_TIME, fromDate, toDate));
      addMultiUserFilter(user, criteria);
      
      ProjectionList pList = Projections.projectionList();
      pList.add(Projections.property(PosTransaction.PROP_PAYMENT_TYPE), PosTransaction.PROP_PAYMENT_TYPE);
      pList.add(Projections.property(PosTransaction.PROP_USER_ID), PosTransaction.PROP_USER_ID);
      pList.add(Projections.property(PosTransaction.PROP_ID), PosTransaction.PROP_ID);
      pList.add(Projections.property(PosTransaction.PROP_TRANSACTION_TIME), PosTransaction.PROP_TRANSACTION_TIME);
      pList.add(Projections.property(PosTransaction.PROP_AMOUNT), PosTransaction.PROP_AMOUNT);
      pList.add(Projections.property(PosTransaction.PROP_TICKET), PosTransaction.PROP_TICKET);
      criteria.setProjection(pList);
      criteria.setResultTransformer(Transformers.aliasToBean(PosTransaction.class));
      criteria.addOrder(Order.asc(PosTransaction.PROP_PAYMENT_TYPE));
      criteria.addOrder(Order.asc(PosTransaction.PROP_TRANSACTION_TIME));
      
      List<PosTransaction> list = criteria.list();
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public List<PosTransaction> getCloudStoreSessionTransactions(StoreSession storeOperationData) {
    Session session = null;
    try {
      List<String> cashDrawerIds = CashDrawerDAO.getInstance().getCashDrawerIds(storeOperationData);
      if ((cashDrawerIds == null) || (cashDrawerIds.isEmpty()))
        return null;
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass(), "t");
      


      criteria.add(Restrictions.in(PosTransaction.PROP_CASH_DRAWER_ID, cashDrawerIds));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  private void addMultiUserFilter(User user, Criteria criteria) {
    if (user != null) {
      PosLog.info(getClass(), "setting multi user filter for root user '" + user.getFullName() + "', id: " + user.getId());
      Disjunction disjunction = Restrictions.disjunction();
      disjunction.add(Restrictions.eq(PosTransaction.PROP_USER_ID, user == null ? null : user.getId()));
      List<User> linkedUsers = user.getLinkedUser();
      if (linkedUsers != null) {
        for (User linkedUser : linkedUsers)
          if (!linkedUser.getId().equals(user.getId()))
          {

            PosLog.info(getClass(), "linked user '" + linkedUser.getFullName() + "', id: " + linkedUser.getId());
            disjunction.add(Restrictions.eq(PosTransaction.PROP_USER_ID, linkedUser.getId()));
          }
      }
      criteria.add(disjunction);
    }
  }
  
  public List<CustomerPaymentReportView.CustomerAccountTransactionItem> findCustomerAccountTransactions(Date fromDate, Date toDate, Customer customer) {
    Session session = null;
    try
    {
      session = getSession();
      String hql = "select t.%s, t.%s, t.%s, t.%s, t.%s, t.%s, c.%s from PosTransaction as t, Customer as c where t.%s=c.%s ";
      hql = String.format(hql, new Object[] { PosTransaction.PROP_ID, PosTransaction.PROP_TICKET, PosTransaction.PROP_TRANSACTION_TIME, PosTransaction.PROP_TIPS_AMOUNT, PosTransaction.PROP_AMOUNT, PosTransaction.PROP_CUSTOMER_ID, Customer.PROP_NAME, PosTransaction.PROP_CUSTOMER_ID, Customer.PROP_ID });
      
      if (customer != null) {
        hql = hql + "and c.id= '" + customer.getId() + "'";
      }
      Query query = session.createQuery(hql);
      List<Object[]> list = query.list();
      List<CustomerPaymentReportView.CustomerAccountTransactionItem> items = new ArrayList();
      for (Object localObject1 = list.iterator(); ((Iterator)localObject1).hasNext();) { Object[] object = (Object[])((Iterator)localObject1).next();
        CustomerPaymentReportView.CustomerAccountTransactionItem accountTransactionItem = new CustomerPaymentReportView.CustomerAccountTransactionItem();
        accountTransactionItem.setTransactionNo(String.valueOf(object[0]));
        Ticket ticket = (Ticket)object[1];
        accountTransactionItem.setTicketNo(ticket.getId());
        accountTransactionItem.setDate((Date)object[2]);
        accountTransactionItem.setTips(Double.valueOf(((Double)object[3]).doubleValue()));
        accountTransactionItem.setTotalAmount(Double.valueOf(((Double)object[4]).doubleValue()));
        accountTransactionItem.setCustomerId(String.valueOf(object[5]));
        accountTransactionItem.setCustomerName(String.valueOf(object[6]));
        items.add(accountTransactionItem);
      }
      
      return items;
    }
    finally {
      closeSession(session);
    }
  }
}
