package com.floreantpos.model.dao;

import com.floreantpos.model.Tag;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseTagDAO
  extends _RootDAO
{
  public static TagDAO instance;
  
  public BaseTagDAO() {}
  
  public static TagDAO getInstance()
  {
    if (null == instance) instance = new TagDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return Tag.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public Tag cast(Object object)
  {
    return (Tag)object;
  }
  
  public Tag get(String key) throws HibernateException
  {
    return (Tag)get(getReferenceClass(), key);
  }
  
  public Tag get(String key, Session s) throws HibernateException
  {
    return (Tag)get(getReferenceClass(), key, s);
  }
  
  public Tag load(String key) throws HibernateException
  {
    return (Tag)load(getReferenceClass(), key);
  }
  
  public Tag load(String key, Session s) throws HibernateException
  {
    return (Tag)load(getReferenceClass(), key, s);
  }
  
  public Tag loadInitialize(String key, Session s) throws HibernateException
  {
    Tag obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<Tag> findAll()
  {
    return super.findAll();
  }
  


  public List<Tag> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<Tag> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(Tag tag)
    throws HibernateException
  {
    return (String)super.save(tag);
  }
  







  public String save(Tag tag, Session s)
    throws HibernateException
  {
    return (String)save(tag, s);
  }
  





  public void saveOrUpdate(Tag tag)
    throws HibernateException
  {
    saveOrUpdate(tag);
  }
  







  public void saveOrUpdate(Tag tag, Session s)
    throws HibernateException
  {
    saveOrUpdate(tag, s);
  }
  




  public void update(Tag tag)
    throws HibernateException
  {
    update(tag);
  }
  






  public void update(Tag tag, Session s)
    throws HibernateException
  {
    update(tag, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(Tag tag)
    throws HibernateException
  {
    delete(tag);
  }
  






  public void delete(Tag tag, Session s)
    throws HibernateException
  {
    delete(tag, s);
  }
  









  public void refresh(Tag tag, Session s)
    throws HibernateException
  {
    refresh(tag, s);
  }
}
