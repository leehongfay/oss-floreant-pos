package com.floreantpos.model.dao;

import com.floreantpos.model.EmployeeInOutHistory;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseEmployeeInOutHistoryDAO
  extends _RootDAO
{
  public static EmployeeInOutHistoryDAO instance;
  
  public BaseEmployeeInOutHistoryDAO() {}
  
  public static EmployeeInOutHistoryDAO getInstance()
  {
    if (null == instance) instance = new EmployeeInOutHistoryDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return EmployeeInOutHistory.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public EmployeeInOutHistory cast(Object object)
  {
    return (EmployeeInOutHistory)object;
  }
  
  public EmployeeInOutHistory get(String key) throws HibernateException
  {
    return (EmployeeInOutHistory)get(getReferenceClass(), key);
  }
  
  public EmployeeInOutHistory get(String key, Session s) throws HibernateException
  {
    return (EmployeeInOutHistory)get(getReferenceClass(), key, s);
  }
  
  public EmployeeInOutHistory load(String key) throws HibernateException
  {
    return (EmployeeInOutHistory)load(getReferenceClass(), key);
  }
  
  public EmployeeInOutHistory load(String key, Session s) throws HibernateException
  {
    return (EmployeeInOutHistory)load(getReferenceClass(), key, s);
  }
  
  public EmployeeInOutHistory loadInitialize(String key, Session s) throws HibernateException
  {
    EmployeeInOutHistory obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<EmployeeInOutHistory> findAll()
  {
    return super.findAll();
  }
  


  public List<EmployeeInOutHistory> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<EmployeeInOutHistory> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(EmployeeInOutHistory employeeInOutHistory)
    throws HibernateException
  {
    return (String)super.save(employeeInOutHistory);
  }
  







  public String save(EmployeeInOutHistory employeeInOutHistory, Session s)
    throws HibernateException
  {
    return (String)save(employeeInOutHistory, s);
  }
  





  public void saveOrUpdate(EmployeeInOutHistory employeeInOutHistory)
    throws HibernateException
  {
    saveOrUpdate(employeeInOutHistory);
  }
  







  public void saveOrUpdate(EmployeeInOutHistory employeeInOutHistory, Session s)
    throws HibernateException
  {
    saveOrUpdate(employeeInOutHistory, s);
  }
  




  public void update(EmployeeInOutHistory employeeInOutHistory)
    throws HibernateException
  {
    update(employeeInOutHistory);
  }
  






  public void update(EmployeeInOutHistory employeeInOutHistory, Session s)
    throws HibernateException
  {
    update(employeeInOutHistory, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(EmployeeInOutHistory employeeInOutHistory)
    throws HibernateException
  {
    delete(employeeInOutHistory);
  }
  






  public void delete(EmployeeInOutHistory employeeInOutHistory, Session s)
    throws HibernateException
  {
    delete(employeeInOutHistory, s);
  }
  









  public void refresh(EmployeeInOutHistory employeeInOutHistory, Session s)
    throws HibernateException
  {
    refresh(employeeInOutHistory, s);
  }
}
