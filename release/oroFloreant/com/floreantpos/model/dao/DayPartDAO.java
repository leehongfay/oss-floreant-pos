package com.floreantpos.model.dao;

import com.floreantpos.model.DayPart;
import com.floreantpos.model.PriceShift;
import com.floreantpos.model.Shift;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;










public class DayPartDAO
  extends BaseDayPartDAO
{
  public DayPartDAO() {}
  
  public List<Shift> convertShiftToDayPart()
  {
    Session session = null;
    Transaction tx = null;
    try
    {
      List<Shift> existingShift = ShiftDAO.getInstance().findAll();
      if (existingShift.isEmpty()) {
        return existingShift;
      }
      String whereClause = "";
      for (Iterator iterator = existingShift.iterator(); iterator.hasNext();) {
        Shift shift = (Shift)iterator.next();
        if ((shift instanceof PriceShift)) {
          iterator.remove();
        }
        else {
          whereClause = whereClause + "ID = '" + shift.getId() + "'";
          if (iterator.hasNext()) {
            whereClause = whereClause + " or ";
          }
        }
      }
      session = createNewSession();
      tx = session.beginTransaction();
      String queryString = "update SHIFT set TYPE='%s' where %s";
      queryString = String.format(queryString, new Object[] { "2", whereClause });
      Query query = session.createSQLQuery(queryString);
      query.executeUpdate();
      tx.commit();
      return existingShift;
    } catch (Exception e) {
      try {
        tx.rollback();
      }
      catch (Exception localException2) {}
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  public DayPart findByName(String name) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Shift.PROP_NAME, name));
      return (DayPart)criteria.uniqueResult();
    } catch (Exception e) {
      throw e;
    } finally {
      if (session != null) {
        try {
          session.close();
        }
        catch (Exception localException2) {}
      }
    }
  }
}
