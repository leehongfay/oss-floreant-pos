package com.floreantpos.model.dao;

import com.floreantpos.model.Terminal;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseTerminalDAO
  extends _RootDAO
{
  public static TerminalDAO instance;
  
  public BaseTerminalDAO() {}
  
  public static TerminalDAO getInstance()
  {
    if (null == instance) instance = new TerminalDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return Terminal.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public Terminal cast(Object object)
  {
    return (Terminal)object;
  }
  
  public Terminal get(Integer key) throws HibernateException
  {
    return (Terminal)get(getReferenceClass(), key);
  }
  
  public Terminal get(Integer key, Session s) throws HibernateException
  {
    return (Terminal)get(getReferenceClass(), key, s);
  }
  
  public Terminal load(Integer key) throws HibernateException
  {
    return (Terminal)load(getReferenceClass(), key);
  }
  
  public Terminal load(Integer key, Session s) throws HibernateException
  {
    return (Terminal)load(getReferenceClass(), key, s);
  }
  
  public Terminal loadInitialize(Integer key, Session s) throws HibernateException
  {
    Terminal obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<Terminal> findAll()
  {
    return super.findAll();
  }
  


  public List<Terminal> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<Terminal> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public Integer save(Terminal terminal)
    throws HibernateException
  {
    return (Integer)super.save(terminal);
  }
  







  public Integer save(Terminal terminal, Session s)
    throws HibernateException
  {
    return (Integer)save(terminal, s);
  }
  





  public void saveOrUpdate(Terminal terminal)
    throws HibernateException
  {
    saveOrUpdate(terminal);
  }
  







  public void saveOrUpdate(Terminal terminal, Session s)
    throws HibernateException
  {
    saveOrUpdate(terminal, s);
  }
  




  public void update(Terminal terminal)
    throws HibernateException
  {
    update(terminal);
  }
  






  public void update(Terminal terminal, Session s)
    throws HibernateException
  {
    update(terminal, s);
  }
  




  public void delete(Integer id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(Integer id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(Terminal terminal)
    throws HibernateException
  {
    delete(terminal);
  }
  






  public void delete(Terminal terminal, Session s)
    throws HibernateException
  {
    delete(terminal, s);
  }
  









  public void refresh(Terminal terminal, Session s)
    throws HibernateException
  {
    refresh(terminal, s);
  }
}
