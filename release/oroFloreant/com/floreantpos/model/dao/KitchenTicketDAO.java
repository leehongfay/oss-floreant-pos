package com.floreantpos.model.dao;

import com.floreantpos.PosException;
import com.floreantpos.model.KitchenTicket;
import com.floreantpos.model.KitchenTicketItem;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Printer;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.VirtualPrinter;
import com.floreantpos.model.ext.KitchenStatus;
import com.floreantpos.swing.PaginatedListModel;
import com.floreantpos.swing.PaginatedTableModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;




















public class KitchenTicketDAO
  extends BaseKitchenTicketDAO
{
  public KitchenTicketDAO() {}
  
  public Date getLastUpdateDate()
  {
    Session session = null;
    Criteria criteria = null;
    try {
      session = getSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.setProjection(Projections.max(KitchenTicket.PROP_LAST_UPDATE_DATE));
      criteria.setMaxResults(1);
      return (Date)criteria.uniqueResult();
    } finally {
      closeSession(session);
    }
  }
  
  public void loadItems(Terminal terminal, List<Printer> selectedKDSPrinters, List<OrderType> orderTypes, boolean kitchenDispatchMode, PaginatedListModel listModel) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(KitchenTicket.PROP_VOIDED, Boolean.FALSE));
      if (kitchenDispatchMode) {
        criteria.add(Restrictions.eq(KitchenTicket.PROP_STATUS, KitchenStatus.BUMP.name()));
      }
      else {
        criteria.add(Restrictions.eq(KitchenTicket.PROP_STATUS, KitchenStatus.WAITING.name()));
      }
      
      if (orderTypes != null) {
        List<String> orderTypesId = new ArrayList();
        for (int i = 0; i < orderTypes.size(); i++) {
          orderTypesId.add(((OrderType)orderTypes.get(i)).getId());
        }
        
        criteria.add(Restrictions.in(KitchenTicket.PROP_ORDER_TYPE_ID, orderTypesId));
      }
      

      if (selectedKDSPrinters != null) {
        List<String> printerName = new ArrayList();
        for (int i = 0; i < selectedKDSPrinters.size(); i++) {
          printerName.add(((Printer)selectedKDSPrinters.get(i)).getVirtualPrinter().getName());
        }
        criteria.add(Restrictions.in(KitchenTicket.PROP_PRINTER_NAME, printerName));
      }
      criteria.add(Restrictions.isNotEmpty("ticketItems"));
      
      listModel.setNumRows(rowCount(criteria));
      
      criteria.setFirstResult(listModel.getCurrentRowIndex());
      criteria.setMaxResults(listModel.getPageSize());
      criteria.addOrder(Order.desc(KitchenTicket.PROP_CREATE_DATE));
      List list = criteria.list();
      if (list == null) {
        list = new ArrayList();
      }
      listModel.setData(list);
    } finally {
      closeSession(session);
    }
  }
  
  public List<KitchenTicket> findAllOpen() {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(KitchenTicket.PROP_STATUS, KitchenStatus.WAITING.name()));
      List list = criteria.list();
      
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public void dispatchAll() {
    Session session = null;
    Transaction tx = null;
    try {
      session = getSession();
      tx = session.beginTransaction();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(KitchenTicket.PROP_STATUS, KitchenStatus.BUMP.name()));
      List<KitchenTicket> KitchenTicketList = criteria.list();
      
      if (KitchenTicketList.isEmpty()) {
        tx.rollback();
        throw new PosException("No data to dispatch");
      }
      
      for (KitchenTicket kitchenTicket : KitchenTicketList) {
        delete(kitchenTicket, session);
      }
      tx.commit();
    }
    catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  public List<KitchenTicket> findByParentId(String ticketId) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(KitchenTicket.PROP_TICKET_ID, ticketId));
      List list = criteria.list();
      
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public KitchenTicket findByLastOrderParentId(String ticketId) {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(KitchenTicket.PROP_TICKET_ID, ticketId));
      criteria.addOrder(Order.desc(KitchenTicket.PROP_CREATE_DATE));
      List list = criteria.list();
      KitchenTicket localKitchenTicket; if (list.size() > 0) {
        return (KitchenTicket)list.get(0);
      }
      return null;
    } finally {
      closeSession(session);
    }
  }
  
  public List<Ticket> findNextKitchenTickets(PaginatedTableModel tableModel) {
    Session session = null;
    Criteria criteria = null;
    try
    {
      int nextIndex = tableModel.getNextRowIndex();
      
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      
      criteria.setFirstResult(nextIndex);
      criteria.setMaxResults(tableModel.getPageSize());
      
      List kitchenTicketList = criteria.list();
      
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        tableModel.setNumRows(rowCount.intValue());
      }
      
      tableModel.setCurrentRowIndex(nextIndex);
      
      return kitchenTicketList;
    }
    finally {
      closeSession(session);
    }
  }
  
  public List<Ticket> findPreviousKitchenTickets(PaginatedTableModel tableModel) {
    Session session = null;
    Criteria criteria = null;
    try
    {
      int previousIndex = tableModel.getPreviousRowIndex();
      
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      
      criteria.setFirstResult(previousIndex);
      criteria.setMaxResults(tableModel.getPageSize());
      
      List kitchenTicketList = criteria.list();
      
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      if (rowCount != null) {
        tableModel.setNumRows(rowCount.intValue());
      }
      

      tableModel.setCurrentRowIndex(previousIndex);
      
      return kitchenTicketList;
    }
    finally {
      closeSession(session);
    }
  }
  





  public synchronized void save(List<KitchenTicket> kitchenTickets)
  {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      
      for (KitchenTicket kitchenTicket : kitchenTickets) {
        session.saveOrUpdate(kitchenTicket);
      }
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      throwException(e);
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public Date isDataUpdated(Date modifiedDate) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.setProjection(Projections.max(KitchenTicket.PROP_CREATE_DATE));
      if (modifiedDate != null) {
        criteria.add(Restrictions.gt(KitchenTicket.PROP_CREATE_DATE, modifiedDate));
      }
      Object data = criteria.uniqueResult();
      Date localDate; if (data == null) {
        return null;
      }
      return (Date)data;
    } finally {
      closeSession(session);
    }
  }
  
  protected void saveOrUpdate(Object obj, Session s)
  {
    Date currentTime = StoreDAO.geServerTimestamp();
    KitchenTicket kitchenTicket = (KitchenTicket)obj;
    kitchenTicket.setLastUpdateDate(currentTime);
    s.saveOrUpdate(kitchenTicket);
  }
  
  public void bumpOrUnbump(KitchenTicket kitchenTicket, KitchenStatus kitchenTicketStatus, KitchenStatus ticketItemStatus, boolean bump) {
    Ticket parentTicket = TicketDAO.getInstance().loadFullTicket(kitchenTicket.getTicketId());
    
    Transaction tx = null;
    Session session = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      for (Iterator localIterator1 = kitchenTicket.getTicketItems().iterator(); localIterator1.hasNext();) { kitchenTicketItem = (KitchenTicketItem)localIterator1.next();
        kitchenTicketItem.setStatus(kitchenTicketStatus.name());
        itemQuantity = kitchenTicketItem.getQuantity().doubleValue();
        
        if (parentTicket != null)
          for (TicketItem item : parentTicket.getTicketItems())
            if ((kitchenTicketItem.getMenuItemCode() != null) && (kitchenTicketItem.getMenuItemCode().equals(item.getMenuItemId())))
              if (item.getKitchenStatusValue() != ticketItemStatus)
              {

                if (itemQuantity == 0.0D) {
                  break;
                }
                item.setKitchenStatusValue(ticketItemStatus);
                itemQuantity -= item.getQuantity().doubleValue();
              }
      }
      KitchenTicketItem kitchenTicketItem;
      double itemQuantity;
      kitchenTicket.setStatus(kitchenTicketStatus.name());
      Date currentTime = StoreDAO.geServerTimestamp();
      if (bump) {
        kitchenTicket.setClosingDate(currentTime);
      } else {
        kitchenTicket.setClosingDate(null);
      }
      saveOrUpdate(kitchenTicket, session);
      if (parentTicket != null) {
        TicketDAO.getInstance().saveOrUpdate(parentTicket, session);
      }
      tx.commit();
    }
    catch (Exception ex) {
      throw ex;
    } finally {
      session.close();
    }
  }
}
