package com.floreantpos.model.dao;

import com.floreantpos.model.ModifierGroup;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;





public class ModifierGroupDAO
  extends BaseModifierGroupDAO
{
  public ModifierGroupDAO() {}
  
  public List<ModifierGroup> findPizzaModifierGroups()
  {
    Session session = null;
    Criteria criteria = null;
    try
    {
      session = createNewSession();
      criteria = session.createCriteria(ModifierGroup.class);
      criteria.add(Restrictions.eq(ModifierGroup.PROP_PIZZA_MODIFIER_GROUP, Boolean.TRUE));
      
      return criteria.list();
    } finally {
      session.close();
    }
  }
  
  public List<ModifierGroup> findNormalModifierGroups() {
    Session session = null;
    Criteria criteria = null;
    try
    {
      session = createNewSession();
      criteria = session.createCriteria(ModifierGroup.class);
      criteria.add(Restrictions.or(Restrictions.isNull(ModifierGroup.PROP_PIZZA_MODIFIER_GROUP), 
        Restrictions.eq(ModifierGroup.PROP_PIZZA_MODIFIER_GROUP, Boolean.FALSE)));
      
      return criteria.list();
    } finally {
      session.close();
    }
  }
  






  public boolean hasPizzaModifierGroup()
  {
    Session session = null;
    Criteria criteria = null;
    try
    {
      session = createNewSession();
      criteria = session.createCriteria(ModifierGroup.class);
      criteria.setProjection(Projections.rowCount());
      criteria.add(Restrictions.eq(ModifierGroup.PROP_PIZZA_MODIFIER_GROUP, Boolean.TRUE));
      Number rowCount = (Number)criteria.uniqueResult();
      return (rowCount != null) && (rowCount.intValue() > 0);
    } finally {
      session.close();
    }
  }
  






  public void updateModifierGroupBooleanPropertyValue(boolean pizzaGroup)
  {
    if (hasPizzaModifierGroup()) {
      return;
    }
    Session session = null;
    Criteria criteria = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      criteria = session.createCriteria(ModifierGroup.class);
      criteria.createAlias("modifiers", "m");
      
      if (pizzaGroup) {
        criteria.add(Restrictions.eq("m.pizzaModifier", Boolean.TRUE));
      } else {
        criteria.add(Restrictions.ne("m.pizzaModifier", Boolean.TRUE));
      }
      List list = criteria.list();
      Iterator iterator; if ((list != null) && (list.size() > 0)) {
        for (iterator = list.iterator(); iterator.hasNext();) {
          ModifierGroup group = (ModifierGroup)iterator.next();
          group.setPizzaModifierGroup(Boolean.valueOf(pizzaGroup));
          saveOrUpdate(group, session);
        }
      }
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
    } finally {
      session.close();
    }
  }
}
