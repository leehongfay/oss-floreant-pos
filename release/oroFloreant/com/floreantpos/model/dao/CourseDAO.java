package com.floreantpos.model.dao;

import com.floreantpos.model.Course;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;





public class CourseDAO
  extends BaseCourseDAO
{
  public CourseDAO() {}
  
  public List<Course> findAll()
  {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.addOrder(Order.asc(Course.PROP_SORT_ORDER));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
}
