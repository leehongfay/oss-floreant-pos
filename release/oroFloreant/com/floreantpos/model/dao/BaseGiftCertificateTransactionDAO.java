package com.floreantpos.model.dao;

import com.floreantpos.model.GiftCertificateTransaction;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseGiftCertificateTransactionDAO
  extends _RootDAO
{
  public static GiftCertificateTransactionDAO instance;
  
  public BaseGiftCertificateTransactionDAO() {}
  
  public static GiftCertificateTransactionDAO getInstance()
  {
    if (null == instance) instance = new GiftCertificateTransactionDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return GiftCertificateTransaction.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public GiftCertificateTransaction cast(Object object)
  {
    return (GiftCertificateTransaction)object;
  }
  
  public GiftCertificateTransaction get(String key) throws HibernateException
  {
    return (GiftCertificateTransaction)get(getReferenceClass(), key);
  }
  
  public GiftCertificateTransaction get(String key, Session s) throws HibernateException
  {
    return (GiftCertificateTransaction)get(getReferenceClass(), key, s);
  }
  
  public GiftCertificateTransaction load(String key) throws HibernateException
  {
    return (GiftCertificateTransaction)load(getReferenceClass(), key);
  }
  
  public GiftCertificateTransaction load(String key, Session s) throws HibernateException
  {
    return (GiftCertificateTransaction)load(getReferenceClass(), key, s);
  }
  
  public GiftCertificateTransaction loadInitialize(String key, Session s) throws HibernateException
  {
    GiftCertificateTransaction obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<GiftCertificateTransaction> findAll()
  {
    return super.findAll();
  }
  


  public List<GiftCertificateTransaction> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<GiftCertificateTransaction> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(GiftCertificateTransaction giftCertificateTransaction)
    throws HibernateException
  {
    return (String)super.save(giftCertificateTransaction);
  }
  







  public String save(GiftCertificateTransaction giftCertificateTransaction, Session s)
    throws HibernateException
  {
    return (String)save(giftCertificateTransaction, s);
  }
  





  public void saveOrUpdate(GiftCertificateTransaction giftCertificateTransaction)
    throws HibernateException
  {
    saveOrUpdate(giftCertificateTransaction);
  }
  







  public void saveOrUpdate(GiftCertificateTransaction giftCertificateTransaction, Session s)
    throws HibernateException
  {
    saveOrUpdate(giftCertificateTransaction, s);
  }
  




  public void update(GiftCertificateTransaction giftCertificateTransaction)
    throws HibernateException
  {
    update(giftCertificateTransaction);
  }
  






  public void update(GiftCertificateTransaction giftCertificateTransaction, Session s)
    throws HibernateException
  {
    update(giftCertificateTransaction, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(GiftCertificateTransaction giftCertificateTransaction)
    throws HibernateException
  {
    delete(giftCertificateTransaction);
  }
  






  public void delete(GiftCertificateTransaction giftCertificateTransaction, Session s)
    throws HibernateException
  {
    delete(giftCertificateTransaction, s);
  }
  









  public void refresh(GiftCertificateTransaction giftCertificateTransaction, Session s)
    throws HibernateException
  {
    refresh(giftCertificateTransaction, s);
  }
}
