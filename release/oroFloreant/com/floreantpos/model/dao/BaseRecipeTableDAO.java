package com.floreantpos.model.dao;

import com.floreantpos.model.RecipeTable;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseRecipeTableDAO
  extends _RootDAO
{
  public static RecipeTableDAO instance;
  
  public BaseRecipeTableDAO() {}
  
  public static RecipeTableDAO getInstance()
  {
    if (null == instance) instance = new RecipeTableDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return RecipeTable.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public RecipeTable cast(Object object)
  {
    return (RecipeTable)object;
  }
  
  public RecipeTable get(String key) throws HibernateException
  {
    return (RecipeTable)get(getReferenceClass(), key);
  }
  
  public RecipeTable get(String key, Session s) throws HibernateException
  {
    return (RecipeTable)get(getReferenceClass(), key, s);
  }
  
  public RecipeTable load(String key) throws HibernateException
  {
    return (RecipeTable)load(getReferenceClass(), key);
  }
  
  public RecipeTable load(String key, Session s) throws HibernateException
  {
    return (RecipeTable)load(getReferenceClass(), key, s);
  }
  
  public RecipeTable loadInitialize(String key, Session s) throws HibernateException
  {
    RecipeTable obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<RecipeTable> findAll()
  {
    return super.findAll();
  }
  


  public List<RecipeTable> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<RecipeTable> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(RecipeTable recipeTable)
    throws HibernateException
  {
    return (String)super.save(recipeTable);
  }
  







  public String save(RecipeTable recipeTable, Session s)
    throws HibernateException
  {
    return (String)save(recipeTable, s);
  }
  





  public void saveOrUpdate(RecipeTable recipeTable)
    throws HibernateException
  {
    saveOrUpdate(recipeTable);
  }
  







  public void saveOrUpdate(RecipeTable recipeTable, Session s)
    throws HibernateException
  {
    saveOrUpdate(recipeTable, s);
  }
  




  public void update(RecipeTable recipeTable)
    throws HibernateException
  {
    update(recipeTable);
  }
  






  public void update(RecipeTable recipeTable, Session s)
    throws HibernateException
  {
    update(recipeTable, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(RecipeTable recipeTable)
    throws HibernateException
  {
    delete(recipeTable);
  }
  






  public void delete(RecipeTable recipeTable, Session s)
    throws HibernateException
  {
    delete(recipeTable, s);
  }
  









  public void refresh(RecipeTable recipeTable, Session s)
    throws HibernateException
  {
    refresh(recipeTable, s);
  }
}
