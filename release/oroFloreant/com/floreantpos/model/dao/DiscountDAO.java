package com.floreantpos.model.dao;

import com.floreantpos.model.Discount;
import com.floreantpos.model.MenuItem;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;






















public class DiscountDAO
  extends BaseDiscountDAO
{
  public DiscountDAO() {}
  
  public Discount getInitialized(String discountId)
  {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Discount.PROP_ID, discountId));
      
      Discount discount = (Discount)criteria.uniqueResult();
      Hibernate.initialize(discount.getMenuItems());
      Hibernate.initialize(discount.getMenuGroups());
      Hibernate.initialize(discount.getMenuCategories());
      
      return discount;
    } finally {
      closeSession(session);
    }
  }
  
  public List<Discount> findAllValidCoupons() {
    Session session = null;
    
    Date currentDate = new Date();
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Discount.PROP_ENABLED, Boolean.TRUE));
      criteria.add(Restrictions.or(Restrictions.eq(Discount.PROP_NEVER_EXPIRE, Boolean.TRUE), Restrictions.ge(Discount.PROP_EXPIRY_DATE, currentDate)));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<Discount> getValidItemCoupons()
  {
    Session session = null;
    
    Date currentDate = new Date();
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Discount.PROP_ENABLED, Boolean.TRUE));
      criteria.add(Restrictions.or(Restrictions.eq(Discount.PROP_QUALIFICATION_TYPE, Integer.valueOf(0)), 
        Restrictions.eq(Discount.PROP_TYPE, Integer.valueOf(2))));
      criteria.add(Restrictions.or(Restrictions.eq(Discount.PROP_NEVER_EXPIRE, Boolean.TRUE), Restrictions.ge(Discount.PROP_EXPIRY_DATE, currentDate)));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<Discount> getValidCoupon(MenuItem menuItem)
  {
    Session session = null;
    Date currentDate = new Date();
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Discount.PROP_ENABLED, Boolean.TRUE));
      criteria.createAlias("menuItems", "item");
      criteria.add(Restrictions.eq("item.id", menuItem.getId()));
      criteria.add(Restrictions.or(Restrictions.eq(Discount.PROP_QUALIFICATION_TYPE, Integer.valueOf(0)), 
        Restrictions.eq(Discount.PROP_TYPE, Integer.valueOf(2))));
      criteria.add(Restrictions.or(Restrictions.eq(Discount.PROP_NEVER_EXPIRE, Boolean.TRUE), Restrictions.ge(Discount.PROP_EXPIRY_DATE, currentDate)));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<Discount> getTicketValidCoupon() {
    Session session = null;
    
    Date currentDate = new Date();
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Discount.PROP_ENABLED, Boolean.TRUE));
      criteria.add(Restrictions.or(Restrictions.eq(Discount.PROP_QUALIFICATION_TYPE, Integer.valueOf(1)), 
        Restrictions.eq(Discount.PROP_TYPE, Integer.valueOf(2))));
      criteria.add(Restrictions.or(Restrictions.eq(Discount.PROP_NEVER_EXPIRE, Boolean.TRUE), Restrictions.ge(Discount.PROP_EXPIRY_DATE, currentDate)));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public boolean isApplicable(String itemId, String discountId) {
    Session session = null;
    Date currentDate = new Date();
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.setProjection(Projections.rowCount());
      criteria.createAlias("menuItems", "item");
      criteria.add(Restrictions.eq("item.id", itemId));
      criteria.add(Restrictions.eq(Discount.PROP_ID, discountId));
      criteria.add(Restrictions.or(Restrictions.eq(Discount.PROP_NEVER_EXPIRE, Boolean.TRUE), Restrictions.ge(Discount.PROP_EXPIRY_DATE, currentDate)));
      Number rowCount = (Number)criteria.uniqueResult();
      return (rowCount != null) && (rowCount.intValue() > 0);
    } finally {
      closeSession(session);
    }
  }
  
  public Discount getDiscountByBarcode(String barcode, int couponType) {
    Session session = null;
    Criteria criteria = null;
    Date currentDate = new Date();
    try {
      session = createNewSession();
      criteria = session.createCriteria(Discount.class);
      criteria.add(Restrictions.like(Discount.PROP_BARCODE, barcode));
      criteria.add(Restrictions.eq(Discount.PROP_ENABLED, Boolean.TRUE));
      criteria.add(Restrictions.or(Restrictions.eq(Discount.PROP_QUALIFICATION_TYPE, 
        Integer.valueOf(couponType == 0 ? 0 : 1)), Restrictions.eq(Discount.PROP_TYPE, 
        Integer.valueOf(2))));
      criteria.add(Restrictions.or(Restrictions.eq(Discount.PROP_NEVER_EXPIRE, Boolean.TRUE), Restrictions.ge(Discount.PROP_EXPIRY_DATE, currentDate)));
      List<Discount> result = criteria.list();
      Discount localDiscount; if ((result == null) || (result.isEmpty())) {
        return null;
      }
      return (Discount)result.get(0);
    } finally {
      closeSession(session);
    }
  }
  
  public String getCurrentDayPropertyName(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    
    String propertyName = "";
    
    int dayOfWeek = c.get(7);
    switch (dayOfWeek) {
    case 7: 
      propertyName = Discount.PROP_APPLY_TO_SATURDAY_ONLY;
      break;
    case 1: 
      propertyName = Discount.PROP_APPLY_TO_SUNDAY_ONLY;
      break;
    case 2: 
      propertyName = Discount.PROP_APPLY_TO_MONDAY_ONLY;
      break;
    case 3: 
      propertyName = Discount.PROP_APPLY_TO_TUESDAY_ONLY;
      break;
    case 4: 
      propertyName = Discount.PROP_APPLY_TO_WEDNESDAY_ONLY;
      break;
    case 5: 
      propertyName = Discount.PROP_APPLY_TO_THURSDAY_ONLY;
      break;
    case 6: 
      propertyName = Discount.PROP_APPLY_TO_FRIDAY_ONLY;
      break;
    }
    
    

    return propertyName;
  }
  
  public List<Discount> getValidCoupons() {
    Session session = null;
    Date currentDate = new Date();
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(Discount.PROP_ENABLED, Boolean.TRUE));
      criteria.add(Restrictions.or(Restrictions.eq(Discount.PROP_NEVER_EXPIRE, Boolean.TRUE), Restrictions.ge(Discount.PROP_EXPIRY_DATE, currentDate)));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
}
