package com.floreantpos.model.dao;

import com.floreantpos.model.InventoryVendor;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseInventoryVendorDAO
  extends _RootDAO
{
  public static InventoryVendorDAO instance;
  
  public BaseInventoryVendorDAO() {}
  
  public static InventoryVendorDAO getInstance()
  {
    if (null == instance) instance = new InventoryVendorDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return InventoryVendor.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public InventoryVendor cast(Object object)
  {
    return (InventoryVendor)object;
  }
  
  public InventoryVendor get(String key) throws HibernateException
  {
    return (InventoryVendor)get(getReferenceClass(), key);
  }
  
  public InventoryVendor get(String key, Session s) throws HibernateException
  {
    return (InventoryVendor)get(getReferenceClass(), key, s);
  }
  
  public InventoryVendor load(String key) throws HibernateException
  {
    return (InventoryVendor)load(getReferenceClass(), key);
  }
  
  public InventoryVendor load(String key, Session s) throws HibernateException
  {
    return (InventoryVendor)load(getReferenceClass(), key, s);
  }
  
  public InventoryVendor loadInitialize(String key, Session s) throws HibernateException
  {
    InventoryVendor obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<InventoryVendor> findAll()
  {
    return super.findAll();
  }
  


  public List<InventoryVendor> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<InventoryVendor> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(InventoryVendor inventoryVendor)
    throws HibernateException
  {
    return (String)super.save(inventoryVendor);
  }
  







  public String save(InventoryVendor inventoryVendor, Session s)
    throws HibernateException
  {
    return (String)save(inventoryVendor, s);
  }
  





  public void saveOrUpdate(InventoryVendor inventoryVendor)
    throws HibernateException
  {
    saveOrUpdate(inventoryVendor);
  }
  







  public void saveOrUpdate(InventoryVendor inventoryVendor, Session s)
    throws HibernateException
  {
    saveOrUpdate(inventoryVendor, s);
  }
  




  public void update(InventoryVendor inventoryVendor)
    throws HibernateException
  {
    update(inventoryVendor);
  }
  






  public void update(InventoryVendor inventoryVendor, Session s)
    throws HibernateException
  {
    update(inventoryVendor, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(InventoryVendor inventoryVendor)
    throws HibernateException
  {
    delete(inventoryVendor);
  }
  






  public void delete(InventoryVendor inventoryVendor, Session s)
    throws HibernateException
  {
    delete(inventoryVendor, s);
  }
  









  public void refresh(InventoryVendor inventoryVendor, Session s)
    throws HibernateException
  {
    refresh(inventoryVendor, s);
  }
}
