package com.floreantpos.model.dao;

import com.floreantpos.model.Address;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseAddressDAO
  extends _RootDAO
{
  public static AddressDAO instance;
  
  public BaseAddressDAO() {}
  
  public static AddressDAO getInstance()
  {
    if (null == instance) instance = new AddressDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return Address.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public Address cast(Object object)
  {
    return (Address)object;
  }
  
  public Address get(String key) throws HibernateException
  {
    return (Address)get(getReferenceClass(), key);
  }
  
  public Address get(String key, Session s) throws HibernateException
  {
    return (Address)get(getReferenceClass(), key, s);
  }
  
  public Address load(String key) throws HibernateException
  {
    return (Address)load(getReferenceClass(), key);
  }
  
  public Address load(String key, Session s) throws HibernateException
  {
    return (Address)load(getReferenceClass(), key, s);
  }
  
  public Address loadInitialize(String key, Session s) throws HibernateException
  {
    Address obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<Address> findAll()
  {
    return super.findAll();
  }
  


  public List<Address> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<Address> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(Address address)
    throws HibernateException
  {
    return (String)super.save(address);
  }
  







  public String save(Address address, Session s)
    throws HibernateException
  {
    return (String)save(address, s);
  }
  





  public void saveOrUpdate(Address address)
    throws HibernateException
  {
    saveOrUpdate(address);
  }
  







  public void saveOrUpdate(Address address, Session s)
    throws HibernateException
  {
    saveOrUpdate(address, s);
  }
  




  public void update(Address address)
    throws HibernateException
  {
    update(address);
  }
  






  public void update(Address address, Session s)
    throws HibernateException
  {
    update(address, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(Address address)
    throws HibernateException
  {
    delete(address);
  }
  






  public void delete(Address address, Session s)
    throws HibernateException
  {
    delete(address, s);
  }
  









  public void refresh(Address address, Session s)
    throws HibernateException
  {
    refresh(address, s);
  }
}
