package com.floreantpos.model.dao;

import com.floreantpos.model.InventoryDepartment;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseInventoryDepartmentDAO
  extends _RootDAO
{
  public static InventoryDepartmentDAO instance;
  
  public BaseInventoryDepartmentDAO() {}
  
  public static InventoryDepartmentDAO getInstance()
  {
    if (null == instance) instance = new InventoryDepartmentDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return InventoryDepartment.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public InventoryDepartment cast(Object object)
  {
    return (InventoryDepartment)object;
  }
  
  public InventoryDepartment get(String key) throws HibernateException
  {
    return (InventoryDepartment)get(getReferenceClass(), key);
  }
  
  public InventoryDepartment get(String key, Session s) throws HibernateException
  {
    return (InventoryDepartment)get(getReferenceClass(), key, s);
  }
  
  public InventoryDepartment load(String key) throws HibernateException
  {
    return (InventoryDepartment)load(getReferenceClass(), key);
  }
  
  public InventoryDepartment load(String key, Session s) throws HibernateException
  {
    return (InventoryDepartment)load(getReferenceClass(), key, s);
  }
  
  public InventoryDepartment loadInitialize(String key, Session s) throws HibernateException
  {
    InventoryDepartment obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<InventoryDepartment> findAll()
  {
    return super.findAll();
  }
  


  public List<InventoryDepartment> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<InventoryDepartment> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(InventoryDepartment inventoryDepartment)
    throws HibernateException
  {
    return (String)super.save(inventoryDepartment);
  }
  







  public String save(InventoryDepartment inventoryDepartment, Session s)
    throws HibernateException
  {
    return (String)save(inventoryDepartment, s);
  }
  





  public void saveOrUpdate(InventoryDepartment inventoryDepartment)
    throws HibernateException
  {
    saveOrUpdate(inventoryDepartment);
  }
  







  public void saveOrUpdate(InventoryDepartment inventoryDepartment, Session s)
    throws HibernateException
  {
    saveOrUpdate(inventoryDepartment, s);
  }
  




  public void update(InventoryDepartment inventoryDepartment)
    throws HibernateException
  {
    update(inventoryDepartment);
  }
  






  public void update(InventoryDepartment inventoryDepartment, Session s)
    throws HibernateException
  {
    update(inventoryDepartment, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(InventoryDepartment inventoryDepartment)
    throws HibernateException
  {
    delete(inventoryDepartment);
  }
  






  public void delete(InventoryDepartment inventoryDepartment, Session s)
    throws HibernateException
  {
    delete(inventoryDepartment, s);
  }
  









  public void refresh(InventoryDepartment inventoryDepartment, Session s)
    throws HibernateException
  {
    refresh(inventoryDepartment, s);
  }
}
