package com.floreantpos.model.dao;

import com.floreantpos.model.SequenceNumber;
import java.util.List;
import org.hibernate.Session;





public class SequenceNumberDAO
  extends BaseSequenceNumberDAO
{
  public SequenceNumberDAO() {}
  
  public int getNextSequenceNumber(String type, Session session)
  {
    SequenceNumber ref = get(type, session);
    if (ref == null) {
      ref = new SequenceNumber(type, Integer.valueOf(1));
    }
    int refNumber = ref.getNextSequenceNumber().intValue();
    ref.setNextSequenceNumber(Integer.valueOf(refNumber + 1));
    session.saveOrUpdate(ref);
    return refNumber;
  }
  
  public void resetAll(Session session) {
    List<SequenceNumber> refList = findAll(session);
    if (refList == null)
      return;
    for (SequenceNumber ref : refList) {
      ref.setNextSequenceNumber(Integer.valueOf(0));
      session.saveOrUpdate(ref);
    }
  }
}
