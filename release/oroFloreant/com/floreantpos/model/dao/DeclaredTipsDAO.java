package com.floreantpos.model.dao;

import com.floreantpos.model.DeclaredTips;
import com.floreantpos.model.StoreSession;
import com.floreantpos.model.User;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;





public class DeclaredTipsDAO
  extends BaseDeclaredTipsDAO
{
  public DeclaredTipsDAO() {}
  
  public List<DeclaredTips> findBy(StoreSession currentData, User user)
  {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(DeclaredTips.PROP_SESSION_ID, currentData.getId()));
      criteria.add(Restrictions.eq(DeclaredTips.PROP_OWNER_ID, user.getId()));
      List list = criteria.list();
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public double findTotalAmount(Date fromDate, Date toDate, User user) {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.between(DeclaredTips.PROP_DECLARED_TIME, fromDate, toDate));
      criteria.add(Restrictions.eq(DeclaredTips.PROP_OWNER_ID, user.getId()));
      
      ProjectionList projectionList = Projections.projectionList();
      projectionList.add(Projections.sum(DeclaredTips.PROP_AMOUNT), DeclaredTips.PROP_AMOUNT);
      
      criteria.setProjection(projectionList);
      Number totalAmount = (Number)criteria.uniqueResult();
      if (totalAmount != null) {
        return totalAmount.doubleValue();
      }
    }
    finally {
      closeSession(session);
    }
    return 0.0D;
  }
}
