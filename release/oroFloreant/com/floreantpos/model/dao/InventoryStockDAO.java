package com.floreantpos.model.dao;

import com.floreantpos.PosLog;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.InventoryStock;
import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.MenuItem;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.authorize.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;






public class InventoryStockDAO
  extends BaseInventoryStockDAO
{
  public InventoryStockDAO() {}
  
  public InventoryStock initialize(InventoryStock inventoryStock)
  {
    if ((inventoryStock == null) || (inventoryStock.getId() == null)) {
      return inventoryStock;
    }
    Session session = null;
    try
    {
      session = createNewSession();
      session.refresh(inventoryStock);
      


      return inventoryStock;
    } finally {
      closeSession(session);
    }
  }
  
  public List<InventoryStock> getInventoryStock(String itemName, Object selectedType) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = getSession();
      criteria = session.createCriteria(InventoryStock.class);
      
      if (StringUtils.isNotEmpty(itemName)) {
        criteria.add(Restrictions.ilike(InventoryStock.PROP_ITEM_NAME, "%" + itemName + "%"));
      }
      
      List<InventoryStock> similarItems = criteria.list();
      
      if ((selectedType instanceof InventoryLocation)) {
        InventoryLocation location = (InventoryLocation)selectedType;
        criteria.add(Restrictions.eq(InventoryStock.PROP_LOCATION_ID, location.getId()));
        
        similarItems = criteria.list();
      }
      


      Iterator iterator;
      

      if (similarItems != null) {
        for (iterator = similarItems.iterator(); iterator.hasNext();) {
          InventoryStock inventoryStock = (InventoryStock)iterator.next();
          MenuItem menuItem = MenuItemDAO.getInstance().get(inventoryStock.getMenuItemId());
          if ((menuItem != null) && 
            (!menuItem.isInventoryItem().booleanValue())) {
            iterator.remove();
          }
        }
      }
      
      return similarItems;
    }
    catch (Exception localException) {}
    
    return criteria.list();
  }
  
  public void addNewStock(InventoryStock stock, InventoryTransaction transaction, MenuItem menuItem) {
    Session session = null;
    Transaction tx = null;
    
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      session.saveOrUpdate(stock);
      session.saveOrUpdate(transaction);
      session.saveOrUpdate(menuItem);
      
      tx.commit();


    }
    catch (Exception e)
    {

      throw e;
    }
    finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public InventoryStock getInventoryStock(MenuItem menuItem, InventoryLocation location, String unitCode) {
    if (location == null) {
      return getInventoryStock(menuItem, (String)null, unitCode);
    }
    return getInventoryStock(menuItem, location.getId(), unitCode);
  }
  
  public InventoryStock getInventoryStock(MenuItem menuItem, String locationId, String unitCode) {
    Session session = null;
    try {
      session = createNewSession();
      return getInventoryStock(menuItem, locationId, unitCode, session);
    } catch (Exception e) {
      PosLog.error(InventoryStockDAO.class, e.getMessage(), e);
    } finally {
      closeSession(session);
    }
    return null;
  }
  
  public InventoryStock getInventoryStock(MenuItem menuItem, String locationId, String unitCode, Session session) {
    Criteria criteria = session.createCriteria(InventoryStock.class);
    if (menuItem != null) {
      criteria.add(Restrictions.eq(InventoryStock.PROP_MENU_ITEM_ID, menuItem.getId()));
    }
    if (locationId != null) {
      criteria.add(Restrictions.eq(InventoryStock.PROP_LOCATION_ID, locationId));
    }
    if (unitCode != null)
      criteria.add(Restrictions.eq(InventoryStock.PROP_UNIT, unitCode));
    List list = criteria.list();
    if ((list != null) && (list.size() > 0)) {
      return (InventoryStock)list.get(0);
    }
    return null;
  }
  
  public List<InventoryStock> getInventoryStocks(MenuItem menuItem) {
    return getInventoryStocks(menuItem, null);
  }
  
  public List<InventoryStock> getInventoryStocks(MenuItem menuItem, InventoryLocation location) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = getSession();
      criteria = session.createCriteria(InventoryStock.class);
      
      criteria.add(Restrictions.eq(InventoryStock.PROP_MENU_ITEM_ID, menuItem.getId()));
      if (location != null) {
        criteria.add(Restrictions.eq(InventoryStock.PROP_LOCATION_ID, location.getId()));
      }
      return criteria.list();
    }
    catch (Exception localException1) {}finally
    {
      closeSession(session);
    }
    return null;
  }
  
  public void addTransfer(InventoryTransaction inTransaction, InventoryTransaction outTransaction, InventoryStock inStock, InventoryStock outStock, MenuItem menuItem) {
    Session session = null;
    Transaction tx = null;
    
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      session.saveOrUpdate(inStock);
      session.saveOrUpdate(outStock);
      session.saveOrUpdate(inTransaction);
      session.saveOrUpdate(outTransaction);
      session.saveOrUpdate(menuItem);
      
      tx.commit();


    }
    catch (Exception e)
    {

      throw e;
    }
    finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public List<InventoryStock> getInventoryOnhandReprotData(String nameOrSku, InventoryLocation location) {
    Session session = null;
    Criteria criteria = null;
    Map<String, String> inventoryLocMap = new HashMap();
    Map<String, Double> menuItemMap = new HashMap();
    session = getSession();
    criteria = session.createCriteria(InventoryStock.class);
    criteria.addOrder(Order.asc(InventoryStock.PROP_ITEM_NAME));
    if (StringUtils.isNotEmpty(nameOrSku)) {
      criteria.add(Restrictions.or(Restrictions.ilike(InventoryStock.PROP_ITEM_NAME, nameOrSku, MatchMode.ANYWHERE), Restrictions.eq(InventoryStock.PROP_SKU, nameOrSku)));
    }
    if (location != null) {
      criteria.add(Restrictions.eq(InventoryStock.PROP_LOCATION_ID, location.getId()));
    }
    List<InventoryStock> stockList = criteria.list();
    for (Iterator<InventoryStock> iterator = stockList.iterator(); iterator.hasNext();) {
      InventoryStock inventoryStock = (InventoryStock)iterator.next();
      String menuItemId = inventoryStock.getMenuItemId();
      String locationId = inventoryStock.getLocationId();
      Double cost = (Double)menuItemMap.get(menuItemId);
      String locationName = (String)inventoryLocMap.get(locationId);
      if (cost == null) {
        cost = getMenuItemCost(menuItemId, session);
      }
      if (locationName == null) {
        locationName = getLocationName(locationId, session);
      }
      inventoryStock.setMenuItemCost(cost.doubleValue());
      inventoryStock.setLocationName(locationName);
    }
    return stockList;
  }
  
  private String getLocationName(String locationId, Session session) {
    Criteria criteria = session.createCriteria(InventoryLocation.class);
    criteria.add(Restrictions.eq(InventoryLocation.PROP_ID, locationId));
    criteria.setProjection(Projections.property(InventoryLocation.PROP_NAME));
    Object result = criteria.uniqueResult();
    if ((result instanceof String)) {
      return ((String)result).toString();
    }
    return null;
  }
  
  private Double getMenuItemCost(String menuItemId, Session session) {
    Criteria criteria = session.createCriteria(MenuItem.class);
    criteria.add(Restrictions.eq(MenuItem.PROP_ID, menuItemId));
    criteria.setProjection(Projections.property(MenuItem.PROP_COST));
    Object result = criteria.uniqueResult();
    if ((result instanceof Number)) {
      return Double.valueOf(((Number)result).doubleValue());
    }
    return Double.valueOf(0.0D);
  }
}
