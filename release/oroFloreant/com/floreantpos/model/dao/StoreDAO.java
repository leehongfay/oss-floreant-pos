package com.floreantpos.model.dao;

import com.floreantpos.StoreAlreadyCloseException;
import com.floreantpos.model.Store;
import com.floreantpos.model.StoreSession;
import com.floreantpos.model.StoreSessionControl;
import com.floreantpos.model.User;
import com.floreantpos.util.StoreUtil;
import java.util.Date;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;






















public class StoreDAO
  extends BaseStoreDAO
{
  public StoreDAO() {}
  
  public static Store getRestaurant()
  {
    return getInstance().get("1");
  }
  



  public static Date geServerTimestamp()
  {
    return new Date();
  }
  










  public void closeStore(User closedBy)
  {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      StoreSessionControl currentStoreOperation = StoreUtil.getCurrentStoreOperation();
      if (currentStoreOperation.getCurrentData() == null) {
        throw new StoreAlreadyCloseException("Store is already closed.");
      }
      StoreSession currentData = currentStoreOperation.getCurrentData();
      currentData.setClosedBy(closedBy);
      currentData.setCloseTime(new Date());
      currentStoreOperation.setCurrentData(null);
      
      session.saveOrUpdate(currentData);
      session.saveOrUpdate(currentStoreOperation);
      
      String sqlDeleteItem = "delete from KITCHEN_TICKET_ITEM";
      String sqlDeleteTicketTables = "delete from KIT_TICKET_TABLE_NUM";
      String sqlKitchenTicket = "delete from KITCHEN_TICKET";
      
      Query query = session.createSQLQuery(sqlDeleteItem);
      query.executeUpdate();
      Query query2 = session.createSQLQuery(sqlDeleteTicketTables);
      query2.executeUpdate();
      Query query3 = session.createSQLQuery(sqlKitchenTicket);
      query3.executeUpdate();
      tx.commit();
    } catch (Exception e) {
      try {
        tx.rollback();
      }
      catch (Exception localException1) {}
      throw e;
    } finally {
      closeSession(session);
    }
  }
}
