package com.floreantpos.model.dao;

import com.floreantpos.model.ShopSeat;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseShopSeatDAO
  extends _RootDAO
{
  public static ShopSeatDAO instance;
  
  public BaseShopSeatDAO() {}
  
  public static ShopSeatDAO getInstance()
  {
    if (null == instance) instance = new ShopSeatDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return ShopSeat.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public ShopSeat cast(Object object)
  {
    return (ShopSeat)object;
  }
  
  public ShopSeat get(String key) throws HibernateException
  {
    return (ShopSeat)get(getReferenceClass(), key);
  }
  
  public ShopSeat get(String key, Session s) throws HibernateException
  {
    return (ShopSeat)get(getReferenceClass(), key, s);
  }
  
  public ShopSeat load(String key) throws HibernateException
  {
    return (ShopSeat)load(getReferenceClass(), key);
  }
  
  public ShopSeat load(String key, Session s) throws HibernateException
  {
    return (ShopSeat)load(getReferenceClass(), key, s);
  }
  
  public ShopSeat loadInitialize(String key, Session s) throws HibernateException
  {
    ShopSeat obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<ShopSeat> findAll()
  {
    return super.findAll();
  }
  


  public List<ShopSeat> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<ShopSeat> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(ShopSeat shopSeat)
    throws HibernateException
  {
    return (String)super.save(shopSeat);
  }
  







  public String save(ShopSeat shopSeat, Session s)
    throws HibernateException
  {
    return (String)save(shopSeat, s);
  }
  





  public void saveOrUpdate(ShopSeat shopSeat)
    throws HibernateException
  {
    saveOrUpdate(shopSeat);
  }
  







  public void saveOrUpdate(ShopSeat shopSeat, Session s)
    throws HibernateException
  {
    saveOrUpdate(shopSeat, s);
  }
  




  public void update(ShopSeat shopSeat)
    throws HibernateException
  {
    update(shopSeat);
  }
  






  public void update(ShopSeat shopSeat, Session s)
    throws HibernateException
  {
    update(shopSeat, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(ShopSeat shopSeat)
    throws HibernateException
  {
    delete(shopSeat);
  }
  






  public void delete(ShopSeat shopSeat, Session s)
    throws HibernateException
  {
    delete(shopSeat, s);
  }
  









  public void refresh(ShopSeat shopSeat, Session s)
    throws HibernateException
  {
    refresh(shopSeat, s);
  }
}
