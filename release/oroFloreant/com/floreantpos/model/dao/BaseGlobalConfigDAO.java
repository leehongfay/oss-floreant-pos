package com.floreantpos.model.dao;

import com.floreantpos.model.GlobalConfig;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseGlobalConfigDAO
  extends _RootDAO
{
  public static GlobalConfigDAO instance;
  
  public BaseGlobalConfigDAO() {}
  
  public static GlobalConfigDAO getInstance()
  {
    if (null == instance) instance = new GlobalConfigDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return GlobalConfig.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public GlobalConfig cast(Object object)
  {
    return (GlobalConfig)object;
  }
  
  public GlobalConfig get(String key) throws HibernateException
  {
    return (GlobalConfig)get(getReferenceClass(), key);
  }
  
  public GlobalConfig get(String key, Session s) throws HibernateException
  {
    return (GlobalConfig)get(getReferenceClass(), key, s);
  }
  
  public GlobalConfig load(String key) throws HibernateException
  {
    return (GlobalConfig)load(getReferenceClass(), key);
  }
  
  public GlobalConfig load(String key, Session s) throws HibernateException
  {
    return (GlobalConfig)load(getReferenceClass(), key, s);
  }
  
  public GlobalConfig loadInitialize(String key, Session s) throws HibernateException
  {
    GlobalConfig obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<GlobalConfig> findAll()
  {
    return super.findAll();
  }
  


  public List<GlobalConfig> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<GlobalConfig> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(GlobalConfig globalConfig)
    throws HibernateException
  {
    return (String)super.save(globalConfig);
  }
  







  public String save(GlobalConfig globalConfig, Session s)
    throws HibernateException
  {
    return (String)save(globalConfig, s);
  }
  





  public void saveOrUpdate(GlobalConfig globalConfig)
    throws HibernateException
  {
    saveOrUpdate(globalConfig);
  }
  







  public void saveOrUpdate(GlobalConfig globalConfig, Session s)
    throws HibernateException
  {
    saveOrUpdate(globalConfig, s);
  }
  




  public void update(GlobalConfig globalConfig)
    throws HibernateException
  {
    update(globalConfig);
  }
  






  public void update(GlobalConfig globalConfig, Session s)
    throws HibernateException
  {
    update(globalConfig, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(GlobalConfig globalConfig)
    throws HibernateException
  {
    delete(globalConfig);
  }
  






  public void delete(GlobalConfig globalConfig, Session s)
    throws HibernateException
  {
    delete(globalConfig, s);
  }
  









  public void refresh(GlobalConfig globalConfig, Session s)
    throws HibernateException
  {
    refresh(globalConfig, s);
  }
}
