package com.floreantpos.model.dao;

import com.floreantpos.model.CustomPayment;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseCustomPaymentDAO
  extends _RootDAO
{
  public static CustomPaymentDAO instance;
  
  public BaseCustomPaymentDAO() {}
  
  public static CustomPaymentDAO getInstance()
  {
    if (null == instance) instance = new CustomPaymentDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return CustomPayment.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public CustomPayment cast(Object object)
  {
    return (CustomPayment)object;
  }
  
  public CustomPayment get(String key) throws HibernateException
  {
    return (CustomPayment)get(getReferenceClass(), key);
  }
  
  public CustomPayment get(String key, Session s) throws HibernateException
  {
    return (CustomPayment)get(getReferenceClass(), key, s);
  }
  
  public CustomPayment load(String key) throws HibernateException
  {
    return (CustomPayment)load(getReferenceClass(), key);
  }
  
  public CustomPayment load(String key, Session s) throws HibernateException
  {
    return (CustomPayment)load(getReferenceClass(), key, s);
  }
  
  public CustomPayment loadInitialize(String key, Session s) throws HibernateException
  {
    CustomPayment obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<CustomPayment> findAll()
  {
    return super.findAll();
  }
  


  public List<CustomPayment> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<CustomPayment> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(CustomPayment customPayment)
    throws HibernateException
  {
    return (String)super.save(customPayment);
  }
  







  public String save(CustomPayment customPayment, Session s)
    throws HibernateException
  {
    return (String)save(customPayment, s);
  }
  





  public void saveOrUpdate(CustomPayment customPayment)
    throws HibernateException
  {
    saveOrUpdate(customPayment);
  }
  







  public void saveOrUpdate(CustomPayment customPayment, Session s)
    throws HibernateException
  {
    saveOrUpdate(customPayment, s);
  }
  




  public void update(CustomPayment customPayment)
    throws HibernateException
  {
    update(customPayment);
  }
  






  public void update(CustomPayment customPayment, Session s)
    throws HibernateException
  {
    update(customPayment, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(CustomPayment customPayment)
    throws HibernateException
  {
    delete(customPayment);
  }
  






  public void delete(CustomPayment customPayment, Session s)
    throws HibernateException
  {
    delete(customPayment, s);
  }
  









  public void refresh(CustomPayment customPayment, Session s)
    throws HibernateException
  {
    refresh(customPayment, s);
  }
}
