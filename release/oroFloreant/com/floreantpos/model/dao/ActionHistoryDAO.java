package com.floreantpos.model.dao;

import com.floreantpos.model.ActionHistory;
import com.floreantpos.model.User;
import java.util.Date;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;




















public class ActionHistoryDAO
  extends BaseActionHistoryDAO
{
  public ActionHistoryDAO() {}
  
  public void saveHistory(User performer, String actionName, String description)
  {
    try
    {
      saveHistory(performer, actionName, description, getSession());
    } catch (Exception e) {
      LogFactory.getLog(ActionHistoryDAO.class).error("Error occured while trying to save action history", e);
    }
  }
  
  public void saveHistory(User performer, String actionName, String description, Session session) {
    ActionHistory history = new ActionHistory();
    history.setActionName(actionName);
    history.setDescription(description);
    history.setPerformer(performer);
    history.setActionTime(new Date());
    save(history, session);
  }
}
