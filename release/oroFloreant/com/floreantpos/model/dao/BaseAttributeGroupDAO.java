package com.floreantpos.model.dao;

import com.floreantpos.model.AttributeGroup;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseAttributeGroupDAO
  extends _RootDAO
{
  public static AttributeGroupDAO instance;
  
  public BaseAttributeGroupDAO() {}
  
  public static AttributeGroupDAO getInstance()
  {
    if (null == instance) instance = new AttributeGroupDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return AttributeGroup.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public AttributeGroup cast(Object object)
  {
    return (AttributeGroup)object;
  }
  
  public AttributeGroup get(String key) throws HibernateException
  {
    return (AttributeGroup)get(getReferenceClass(), key);
  }
  
  public AttributeGroup get(String key, Session s) throws HibernateException
  {
    return (AttributeGroup)get(getReferenceClass(), key, s);
  }
  
  public AttributeGroup load(String key) throws HibernateException
  {
    return (AttributeGroup)load(getReferenceClass(), key);
  }
  
  public AttributeGroup load(String key, Session s) throws HibernateException
  {
    return (AttributeGroup)load(getReferenceClass(), key, s);
  }
  
  public AttributeGroup loadInitialize(String key, Session s) throws HibernateException
  {
    AttributeGroup obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<AttributeGroup> findAll()
  {
    return super.findAll();
  }
  


  public List<AttributeGroup> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<AttributeGroup> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(AttributeGroup attributeGroup)
    throws HibernateException
  {
    return (String)super.save(attributeGroup);
  }
  







  public String save(AttributeGroup attributeGroup, Session s)
    throws HibernateException
  {
    return (String)save(attributeGroup, s);
  }
  





  public void saveOrUpdate(AttributeGroup attributeGroup)
    throws HibernateException
  {
    saveOrUpdate(attributeGroup);
  }
  







  public void saveOrUpdate(AttributeGroup attributeGroup, Session s)
    throws HibernateException
  {
    saveOrUpdate(attributeGroup, s);
  }
  




  public void update(AttributeGroup attributeGroup)
    throws HibernateException
  {
    update(attributeGroup);
  }
  






  public void update(AttributeGroup attributeGroup, Session s)
    throws HibernateException
  {
    update(attributeGroup, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(AttributeGroup attributeGroup)
    throws HibernateException
  {
    delete(attributeGroup);
  }
  






  public void delete(AttributeGroup attributeGroup, Session s)
    throws HibernateException
  {
    delete(attributeGroup, s);
  }
  









  public void refresh(AttributeGroup attributeGroup, Session s)
    throws HibernateException
  {
    refresh(attributeGroup, s);
  }
}
