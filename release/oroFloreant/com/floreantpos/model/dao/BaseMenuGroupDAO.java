package com.floreantpos.model.dao;

import com.floreantpos.model.MenuGroup;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseMenuGroupDAO
  extends _RootDAO
{
  public static MenuGroupDAO instance;
  
  public BaseMenuGroupDAO() {}
  
  public static MenuGroupDAO getInstance()
  {
    if (null == instance) instance = new MenuGroupDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return MenuGroup.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public MenuGroup cast(Object object)
  {
    return (MenuGroup)object;
  }
  
  public MenuGroup get(String key) throws HibernateException
  {
    return (MenuGroup)get(getReferenceClass(), key);
  }
  
  public MenuGroup get(String key, Session s) throws HibernateException
  {
    return (MenuGroup)get(getReferenceClass(), key, s);
  }
  
  public MenuGroup load(String key) throws HibernateException
  {
    return (MenuGroup)load(getReferenceClass(), key);
  }
  
  public MenuGroup load(String key, Session s) throws HibernateException
  {
    return (MenuGroup)load(getReferenceClass(), key, s);
  }
  
  public MenuGroup loadInitialize(String key, Session s) throws HibernateException
  {
    MenuGroup obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<MenuGroup> findAll()
  {
    return super.findAll();
  }
  


  public List<MenuGroup> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<MenuGroup> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(MenuGroup menuGroup)
    throws HibernateException
  {
    return (String)super.save(menuGroup);
  }
  







  public String save(MenuGroup menuGroup, Session s)
    throws HibernateException
  {
    return (String)save(menuGroup, s);
  }
  





  public void saveOrUpdate(MenuGroup menuGroup)
    throws HibernateException
  {
    saveOrUpdate(menuGroup);
  }
  







  public void saveOrUpdate(MenuGroup menuGroup, Session s)
    throws HibernateException
  {
    saveOrUpdate(menuGroup, s);
  }
  




  public void update(MenuGroup menuGroup)
    throws HibernateException
  {
    update(menuGroup);
  }
  






  public void update(MenuGroup menuGroup, Session s)
    throws HibernateException
  {
    update(menuGroup, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(MenuGroup menuGroup)
    throws HibernateException
  {
    delete(menuGroup);
  }
  






  public void delete(MenuGroup menuGroup, Session s)
    throws HibernateException
  {
    delete(menuGroup, s);
  }
  









  public void refresh(MenuGroup menuGroup, Session s)
    throws HibernateException
  {
    refresh(menuGroup, s);
  }
}
