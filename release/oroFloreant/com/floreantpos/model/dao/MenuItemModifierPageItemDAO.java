package com.floreantpos.model.dao;

import com.floreantpos.model.MenuItemModifierPageItem;
import com.floreantpos.model.MenuModifier;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;





public class MenuItemModifierPageItemDAO
  extends BaseMenuItemModifierPageItemDAO
{
  public MenuItemModifierPageItemDAO() {}
  
  public List<MenuItemModifierPageItem> getPageItemFor(MenuModifier menuModifier, Session session)
  {
    Criteria criteria = session.createCriteria(getReferenceClass());
    criteria.add(Restrictions.eq(MenuItemModifierPageItem.PROP_MENU_MODIFIER_ID, menuModifier.getId()));
    
    return criteria.list();
  }
}
