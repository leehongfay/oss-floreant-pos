package com.floreantpos.model.dao;

import com.floreantpos.model.VirtualPrinter;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;





















public class VirtualPrinterDAO
  extends BaseVirtualPrinterDAO
{
  public VirtualPrinterDAO() {}
  
  public VirtualPrinter findPrinterByName(String name)
  {
    Session session = getSession();
    Criteria criteria = session.createCriteria(getReferenceClass());
    criteria.add(Restrictions.eq(VirtualPrinter.PROP_NAME, name));
    
    return (VirtualPrinter)criteria.uniqueResult();
  }
}
