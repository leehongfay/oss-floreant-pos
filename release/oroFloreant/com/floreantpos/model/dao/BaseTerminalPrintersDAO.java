package com.floreantpos.model.dao;

import com.floreantpos.model.TerminalPrinters;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseTerminalPrintersDAO
  extends _RootDAO
{
  public static TerminalPrintersDAO instance;
  
  public BaseTerminalPrintersDAO() {}
  
  public static TerminalPrintersDAO getInstance()
  {
    if (null == instance) instance = new TerminalPrintersDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return TerminalPrinters.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public TerminalPrinters cast(Object object)
  {
    return (TerminalPrinters)object;
  }
  
  public TerminalPrinters get(String key) throws HibernateException
  {
    return (TerminalPrinters)get(getReferenceClass(), key);
  }
  
  public TerminalPrinters get(String key, Session s) throws HibernateException
  {
    return (TerminalPrinters)get(getReferenceClass(), key, s);
  }
  
  public TerminalPrinters load(String key) throws HibernateException
  {
    return (TerminalPrinters)load(getReferenceClass(), key);
  }
  
  public TerminalPrinters load(String key, Session s) throws HibernateException
  {
    return (TerminalPrinters)load(getReferenceClass(), key, s);
  }
  
  public TerminalPrinters loadInitialize(String key, Session s) throws HibernateException
  {
    TerminalPrinters obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<TerminalPrinters> findAll()
  {
    return super.findAll();
  }
  


  public List<TerminalPrinters> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<TerminalPrinters> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(TerminalPrinters terminalPrinters)
    throws HibernateException
  {
    return (String)super.save(terminalPrinters);
  }
  







  public String save(TerminalPrinters terminalPrinters, Session s)
    throws HibernateException
  {
    return (String)save(terminalPrinters, s);
  }
  





  public void saveOrUpdate(TerminalPrinters terminalPrinters)
    throws HibernateException
  {
    saveOrUpdate(terminalPrinters);
  }
  







  public void saveOrUpdate(TerminalPrinters terminalPrinters, Session s)
    throws HibernateException
  {
    saveOrUpdate(terminalPrinters, s);
  }
  




  public void update(TerminalPrinters terminalPrinters)
    throws HibernateException
  {
    update(terminalPrinters);
  }
  






  public void update(TerminalPrinters terminalPrinters, Session s)
    throws HibernateException
  {
    update(terminalPrinters, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(TerminalPrinters terminalPrinters)
    throws HibernateException
  {
    delete(terminalPrinters);
  }
  






  public void delete(TerminalPrinters terminalPrinters, Session s)
    throws HibernateException
  {
    delete(terminalPrinters, s);
  }
  









  public void refresh(TerminalPrinters terminalPrinters, Session s)
    throws HibernateException
  {
    refresh(terminalPrinters, s);
  }
}
