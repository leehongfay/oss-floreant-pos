package com.floreantpos.model.dao;

import com.floreantpos.model.DeliveryConfiguration;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseDeliveryConfigurationDAO
  extends _RootDAO
{
  public static DeliveryConfigurationDAO instance;
  
  public BaseDeliveryConfigurationDAO() {}
  
  public static DeliveryConfigurationDAO getInstance()
  {
    if (null == instance) instance = new DeliveryConfigurationDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return DeliveryConfiguration.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public DeliveryConfiguration cast(Object object)
  {
    return (DeliveryConfiguration)object;
  }
  
  public DeliveryConfiguration get(String key) throws HibernateException
  {
    return (DeliveryConfiguration)get(getReferenceClass(), key);
  }
  
  public DeliveryConfiguration get(String key, Session s) throws HibernateException
  {
    return (DeliveryConfiguration)get(getReferenceClass(), key, s);
  }
  
  public DeliveryConfiguration load(String key) throws HibernateException
  {
    return (DeliveryConfiguration)load(getReferenceClass(), key);
  }
  
  public DeliveryConfiguration load(String key, Session s) throws HibernateException
  {
    return (DeliveryConfiguration)load(getReferenceClass(), key, s);
  }
  
  public DeliveryConfiguration loadInitialize(String key, Session s) throws HibernateException
  {
    DeliveryConfiguration obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<DeliveryConfiguration> findAll()
  {
    return super.findAll();
  }
  


  public List<DeliveryConfiguration> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<DeliveryConfiguration> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(DeliveryConfiguration deliveryConfiguration)
    throws HibernateException
  {
    return (String)super.save(deliveryConfiguration);
  }
  







  public String save(DeliveryConfiguration deliveryConfiguration, Session s)
    throws HibernateException
  {
    return (String)save(deliveryConfiguration, s);
  }
  





  public void saveOrUpdate(DeliveryConfiguration deliveryConfiguration)
    throws HibernateException
  {
    saveOrUpdate(deliveryConfiguration);
  }
  







  public void saveOrUpdate(DeliveryConfiguration deliveryConfiguration, Session s)
    throws HibernateException
  {
    saveOrUpdate(deliveryConfiguration, s);
  }
  




  public void update(DeliveryConfiguration deliveryConfiguration)
    throws HibernateException
  {
    update(deliveryConfiguration);
  }
  






  public void update(DeliveryConfiguration deliveryConfiguration, Session s)
    throws HibernateException
  {
    update(deliveryConfiguration, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(DeliveryConfiguration deliveryConfiguration)
    throws HibernateException
  {
    delete(deliveryConfiguration);
  }
  






  public void delete(DeliveryConfiguration deliveryConfiguration, Session s)
    throws HibernateException
  {
    delete(deliveryConfiguration, s);
  }
  









  public void refresh(DeliveryConfiguration deliveryConfiguration, Session s)
    throws HibernateException
  {
    refresh(deliveryConfiguration, s);
  }
}
