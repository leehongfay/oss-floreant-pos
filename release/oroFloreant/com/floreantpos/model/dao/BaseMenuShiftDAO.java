package com.floreantpos.model.dao;

import com.floreantpos.model.MenuShift;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseMenuShiftDAO
  extends _RootDAO
{
  public static MenuShiftDAO instance;
  
  public BaseMenuShiftDAO() {}
  
  public static MenuShiftDAO getInstance()
  {
    if (null == instance) instance = new MenuShiftDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return MenuShift.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public MenuShift cast(Object object)
  {
    return (MenuShift)object;
  }
  
  public MenuShift get(String key) throws HibernateException
  {
    return (MenuShift)get(getReferenceClass(), key);
  }
  
  public MenuShift get(String key, Session s) throws HibernateException
  {
    return (MenuShift)get(getReferenceClass(), key, s);
  }
  
  public MenuShift load(String key) throws HibernateException
  {
    return (MenuShift)load(getReferenceClass(), key);
  }
  
  public MenuShift load(String key, Session s) throws HibernateException
  {
    return (MenuShift)load(getReferenceClass(), key, s);
  }
  
  public MenuShift loadInitialize(String key, Session s) throws HibernateException
  {
    MenuShift obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<MenuShift> findAll()
  {
    return super.findAll();
  }
  


  public List<MenuShift> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<MenuShift> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(MenuShift menuShift)
    throws HibernateException
  {
    return (String)super.save(menuShift);
  }
  







  public String save(MenuShift menuShift, Session s)
    throws HibernateException
  {
    return (String)save(menuShift, s);
  }
  





  public void saveOrUpdate(MenuShift menuShift)
    throws HibernateException
  {
    saveOrUpdate(menuShift);
  }
  







  public void saveOrUpdate(MenuShift menuShift, Session s)
    throws HibernateException
  {
    saveOrUpdate(menuShift, s);
  }
  




  public void update(MenuShift menuShift)
    throws HibernateException
  {
    update(menuShift);
  }
  






  public void update(MenuShift menuShift, Session s)
    throws HibernateException
  {
    update(menuShift, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(MenuShift menuShift)
    throws HibernateException
  {
    delete(menuShift);
  }
  






  public void delete(MenuShift menuShift, Session s)
    throws HibernateException
  {
    delete(menuShift, s);
  }
  









  public void refresh(MenuShift menuShift, Session s)
    throws HibernateException
  {
    refresh(menuShift, s);
  }
}
