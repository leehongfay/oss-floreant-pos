package com.floreantpos.model.dao;

import com.floreantpos.model.CustomerGroup;
import org.hibernate.Hibernate;
import org.hibernate.Session;





public class CustomerGroupDAO
  extends BaseCustomerGroupDAO
{
  public CustomerGroupDAO() {}
  
  public void initialize(CustomerGroup customerGroup)
  {
    if ((customerGroup == null) || (customerGroup.getId() == null)) {
      return;
    }
    if (Hibernate.isInitialized(customerGroup.getCustomers())) {
      return;
    }
    Session session = null;
    try
    {
      session = createNewSession();
      session.refresh(customerGroup);
      
      Hibernate.initialize(customerGroup.getCustomers());
      
      closeSession(session); } finally { closeSession(session);
    }
  }
}
