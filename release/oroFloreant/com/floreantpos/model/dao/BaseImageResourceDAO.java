package com.floreantpos.model.dao;

import com.floreantpos.model.ImageResource;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseImageResourceDAO
  extends _RootDAO
{
  public static ImageResourceDAO instance;
  
  public BaseImageResourceDAO() {}
  
  public static ImageResourceDAO getInstance()
  {
    if (null == instance) instance = new ImageResourceDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return ImageResource.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public ImageResource cast(Object object)
  {
    return (ImageResource)object;
  }
  
  public ImageResource get(String key) throws HibernateException
  {
    return (ImageResource)get(getReferenceClass(), key);
  }
  
  public ImageResource get(String key, Session s) throws HibernateException
  {
    return (ImageResource)get(getReferenceClass(), key, s);
  }
  
  public ImageResource load(String key) throws HibernateException
  {
    return (ImageResource)load(getReferenceClass(), key);
  }
  
  public ImageResource load(String key, Session s) throws HibernateException
  {
    return (ImageResource)load(getReferenceClass(), key, s);
  }
  
  public ImageResource loadInitialize(String key, Session s) throws HibernateException
  {
    ImageResource obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<ImageResource> findAll()
  {
    return super.findAll();
  }
  


  public List<ImageResource> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<ImageResource> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(ImageResource imageResource)
    throws HibernateException
  {
    return (String)super.save(imageResource);
  }
  







  public String save(ImageResource imageResource, Session s)
    throws HibernateException
  {
    return (String)save(imageResource, s);
  }
  





  public void saveOrUpdate(ImageResource imageResource)
    throws HibernateException
  {
    saveOrUpdate(imageResource);
  }
  







  public void saveOrUpdate(ImageResource imageResource, Session s)
    throws HibernateException
  {
    saveOrUpdate(imageResource, s);
  }
  




  public void update(ImageResource imageResource)
    throws HibernateException
  {
    update(imageResource);
  }
  






  public void update(ImageResource imageResource, Session s)
    throws HibernateException
  {
    update(imageResource, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(ImageResource imageResource)
    throws HibernateException
  {
    delete(imageResource);
  }
  






  public void delete(ImageResource imageResource, Session s)
    throws HibernateException
  {
    delete(imageResource, s);
  }
  









  public void refresh(ImageResource imageResource, Session s)
    throws HibernateException
  {
    refresh(imageResource, s);
  }
}
