package com.floreantpos.model.dao;

import com.floreantpos.model.ShopFloorTemplate;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseShopFloorTemplateDAO
  extends _RootDAO
{
  public static ShopFloorTemplateDAO instance;
  
  public BaseShopFloorTemplateDAO() {}
  
  public static ShopFloorTemplateDAO getInstance()
  {
    if (null == instance) instance = new ShopFloorTemplateDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return ShopFloorTemplate.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public ShopFloorTemplate cast(Object object)
  {
    return (ShopFloorTemplate)object;
  }
  
  public ShopFloorTemplate get(String key) throws HibernateException
  {
    return (ShopFloorTemplate)get(getReferenceClass(), key);
  }
  
  public ShopFloorTemplate get(String key, Session s) throws HibernateException
  {
    return (ShopFloorTemplate)get(getReferenceClass(), key, s);
  }
  
  public ShopFloorTemplate load(String key) throws HibernateException
  {
    return (ShopFloorTemplate)load(getReferenceClass(), key);
  }
  
  public ShopFloorTemplate load(String key, Session s) throws HibernateException
  {
    return (ShopFloorTemplate)load(getReferenceClass(), key, s);
  }
  
  public ShopFloorTemplate loadInitialize(String key, Session s) throws HibernateException
  {
    ShopFloorTemplate obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<ShopFloorTemplate> findAll()
  {
    return super.findAll();
  }
  


  public List<ShopFloorTemplate> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<ShopFloorTemplate> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(ShopFloorTemplate shopFloorTemplate)
    throws HibernateException
  {
    return (String)super.save(shopFloorTemplate);
  }
  







  public String save(ShopFloorTemplate shopFloorTemplate, Session s)
    throws HibernateException
  {
    return (String)save(shopFloorTemplate, s);
  }
  





  public void saveOrUpdate(ShopFloorTemplate shopFloorTemplate)
    throws HibernateException
  {
    saveOrUpdate(shopFloorTemplate);
  }
  







  public void saveOrUpdate(ShopFloorTemplate shopFloorTemplate, Session s)
    throws HibernateException
  {
    saveOrUpdate(shopFloorTemplate, s);
  }
  




  public void update(ShopFloorTemplate shopFloorTemplate)
    throws HibernateException
  {
    update(shopFloorTemplate);
  }
  






  public void update(ShopFloorTemplate shopFloorTemplate, Session s)
    throws HibernateException
  {
    update(shopFloorTemplate, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(ShopFloorTemplate shopFloorTemplate)
    throws HibernateException
  {
    delete(shopFloorTemplate);
  }
  






  public void delete(ShopFloorTemplate shopFloorTemplate, Session s)
    throws HibernateException
  {
    delete(shopFloorTemplate, s);
  }
  









  public void refresh(ShopFloorTemplate shopFloorTemplate, Session s)
    throws HibernateException
  {
    refresh(shopFloorTemplate, s);
  }
}
