package com.floreantpos.model.dao;

import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuPage;
import com.floreantpos.model.MenuPageItem;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;





public class MenuPageItemDAO
  extends BaseMenuPageItemDAO
{
  public MenuPageItemDAO() {}
  
  public List<MenuPageItem> getPageItems(MenuPage menuPage)
  {
    Session session = null;
    if (menuPage == null) {
      return null;
    }
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      if (menuPage != null) {
        criteria.add(Restrictions.eq(MenuPageItem.PROP_MENU_PAGE_ID, menuPage.getId()));
      }
      
      List list = criteria.list();
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuPageItem> getPageItemFor(MenuItem menuItem) {
    Session session = null;
    try {
      session = createNewSession();
      return getPageItemFor(menuItem, session);
    } finally {
      closeSession(session);
    }
  }
  
  public List<MenuPageItem> getPageItemFor(MenuItem menuItem, Session session) {
    Criteria criteria = session.createCriteria(getReferenceClass());
    criteria.add(Restrictions.eq(MenuPageItem.PROP_MENU_ITEM_ID, menuItem.getId()));
    
    return criteria.list();
  }
  


  public List<MenuItem> getMenuItemsForGroup(MenuGroup menuGroup)
  {
    Session session = null;
    try
    {
      session = getSession();
      List menuGroupIds = null;
      Criteria criteria = session.createCriteria(MenuGroup.class);
      
      if (menuGroup.getId() == null) {
        criteria.setProjection(Projections.property(MenuGroup.PROP_ID));
        criteria.add(Restrictions.eq(MenuGroup.PROP_MENU_CATEGORY_ID, menuGroup.getMenuCategoryId()));
        menuGroupIds = criteria.list();
        if ((menuGroupIds == null) || (menuGroupIds.isEmpty())) {
          return null;
        }
      }
      criteria = session.createCriteria(MenuPage.class);
      criteria.setProjection(Projections.property(MenuPage.PROP_ID));
      criteria.add(menuGroupIds != null ? Restrictions.in(MenuPage.PROP_MENU_GROUP_ID, menuGroupIds) : Restrictions.eq(MenuPage.PROP_MENU_GROUP_ID, menuGroup
        .getId()));
      
      List menuPageIds = criteria.list();
      Object localObject1; if ((menuPageIds == null) || (menuPageIds.isEmpty())) {
        return null;
      }
      criteria = session.createCriteria(MenuPageItem.class);
      criteria.setProjection(Projections.property(MenuPageItem.PROP_MENU_ITEM_ID));
      criteria.add(Restrictions.in(MenuPageItem.PROP_MENU_PAGE_ID, menuPageIds));
      return getItemWithNameAndPrice(criteria.list(), session);
    } finally {
      closeSession(session);
    }
  }
  
  private List<MenuItem> getItemWithNameAndPrice(List menuItemIds, Session session) {
    if ((menuItemIds == null) || (menuItemIds.isEmpty())) {
      return null;
    }
    Criteria criteria = session.createCriteria(MenuItem.class);
    ProjectionList projections = Projections.projectionList();
    projections.add(Projections.property(MenuItem.PROP_ID), MenuItem.PROP_ID);
    projections.add(Projections.property(MenuItem.PROP_NAME), MenuItem.PROP_NAME);
    projections.add(Projections.property(MenuItem.PROP_PRICE), MenuItem.PROP_PRICE);
    projections.add(Projections.property(MenuItem.PROP_IMAGE_ID), MenuItem.PROP_IMAGE_ID);
    projections.add(Projections.property(MenuItem.PROP_PROPERTIES_JSON), MenuItem.PROP_PROPERTIES_JSON);
    criteria.add(Restrictions.in(MenuItem.PROP_ID, menuItemIds));
    criteria.setProjection(projections);
    criteria.setResultTransformer(Transformers.aliasToBean(MenuItem.class));
    return criteria.list();
  }
}
