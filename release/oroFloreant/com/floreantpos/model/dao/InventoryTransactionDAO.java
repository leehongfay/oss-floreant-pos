package com.floreantpos.model.dao;

import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.InventoryStock;
import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.InventoryTransactionType;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.PurchaseOrder;
import com.floreantpos.model.Store;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.PaginationSupport;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import net.authorize.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;




















public class InventoryTransactionDAO
  extends BaseInventoryTransactionDAO
{
  public InventoryTransactionDAO() {}
  
  public InventoryTransaction initialize(InventoryTransaction inventoryTransaction)
  {
    if ((inventoryTransaction == null) || (inventoryTransaction.getId() == null)) {
      return inventoryTransaction;
    }
    Session session = null;
    try
    {
      session = createNewSession();
      session.refresh(inventoryTransaction);
      


      return inventoryTransaction;
    } finally {
      closeSession(session);
    }
  }
  
  public void adjustInventoryStock(InventoryTransaction trans, Session session) {
    adjustInventoryStock(trans, session, true, true);
  }
  
  public void adjustInventoryStock(InventoryTransaction trans, Session session, boolean updateAvailableUnit, boolean updateOnHandUnit) {
    try {
      MenuItem menuItem = trans.getMenuItem();
      double baseUnitQuantity = menuItem.getBaseUnitQuantity(trans.getUnit());
      String baseUnit = "";
      if (menuItem.getUnit() != null)
        baseUnit = menuItem.getUnit().getCode();
      trans.setBaseUnit(baseUnit);
      trans.setBaseUnitQuantity(Double.valueOf(baseUnitQuantity));
      InventoryLocation inventoryFromLocation = null;
      InventoryLocation inventoryToLocation = null;
      InventoryStock stockItem = null;
      double unitQuantity = 0.0D;
      
      PosLog.info(getClass(), menuItem.getDisplayName() + " |" + trans.getTransactionType() + "| |" + trans.getQuantity() + "| " + trans.getReason());
      if (trans.getTransactionType() == InventoryTransactionType.IN) {
        inventoryToLocation = trans.getToInventoryLocation();
        stockItem = InventoryStockDAO.getInstance().getInventoryStock(menuItem, trans.getToLocationId(), trans.getUnit(), session);
        unitQuantity = createStockItem(trans, session, menuItem, baseUnitQuantity, stockItem, inventoryToLocation);
        session.saveOrUpdate(trans);
        MenuItemDAO.getInstance().updateStockQuantity(menuItem.getId(), unitQuantity, updateAvailableUnit, updateOnHandUnit, session);
      }
      if (trans.getTransactionType() == InventoryTransactionType.OUT) {
        inventoryFromLocation = trans.getFromInventoryLocation();
        stockItem = InventoryStockDAO.getInstance().getInventoryStock(menuItem, trans.getFromLocationId(), trans.getUnit(), session);
        unitQuantity = createStockItem(trans, session, menuItem, baseUnitQuantity, stockItem, inventoryFromLocation);
        session.saveOrUpdate(trans);
        MenuItemDAO.getInstance().updateStockQuantity(menuItem.getId(), unitQuantity, updateAvailableUnit, updateOnHandUnit, session);
      }
      if (trans.getTransactionType() == InventoryTransactionType.UNCHANGED) {
        inventoryFromLocation = trans.getFromInventoryLocation();
        inventoryToLocation = trans.getToInventoryLocation();
        createStockItemForTransfer(trans, session, menuItem, baseUnitQuantity, trans.getFromLocationId(), trans.getToLocationId());
        session.saveOrUpdate(trans);
      }
    } catch (Exception e) {
      PosLog.error(InventoryTransactionDAO.class, e.getMessage(), e);
    }
  }
  

  private void createStockItemForTransfer(InventoryTransaction trans, Session session, MenuItem menuItem, double baseUnitQuantity, String fromLocationId, String toLocationId)
  {
    InventoryStock stockItemFromLocation = InventoryStockDAO.getInstance().getInventoryStock(menuItem, fromLocationId, trans.getUnit(), session);
    InventoryStock stockItemToLocation = InventoryStockDAO.getInstance().getInventoryStock(menuItem, toLocationId, trans.getUnit(), session);
    if (stockItemFromLocation == null) {
      return;
    }
    if (stockItemToLocation == null) {
      stockItemToLocation = new InventoryStock();
      stockItemToLocation.setMenuItem(menuItem);
      stockItemToLocation.setLocationId(toLocationId);
      stockItemToLocation.setUnit(trans.getUnit());
    }
    stockItemFromLocation.setQuantityInHand(Double.valueOf(stockItemFromLocation.getQuantityInHand().doubleValue() - trans.getQuantity().doubleValue()));
    stockItemToLocation.setQuantityInHand(Double.valueOf(stockItemToLocation.getQuantityInHand().doubleValue() + trans.getQuantity().doubleValue()));
    
    if (stockItemFromLocation.getQuantityInHand().doubleValue() == 0.0D) {
      session.delete(stockItemFromLocation);
    }
    else {
      session.saveOrUpdate(stockItemFromLocation);
    }
    
    session.saveOrUpdate(stockItemToLocation);
  }
  
  private double createStockItem(InventoryTransaction trans, Session session, MenuItem menuItem, double baseUnitQuantity, InventoryStock stockItem, InventoryLocation inventoryLocation)
  {
    if (stockItem == null) {
      stockItem = new InventoryStock();
      stockItem.setMenuItem(menuItem);
      stockItem.setLocationId(inventoryLocation.getId());
      stockItem.setUnit(trans.getUnit());
    }
    if (trans.getTransactionType() == InventoryTransactionType.IN) {
      stockItem.setQuantityInHand(Double.valueOf(stockItem.getQuantityInHand().doubleValue() + trans.getQuantity().doubleValue()));
    }
    if (trans.getTransactionType() == InventoryTransactionType.OUT) {
      stockItem.setQuantityInHand(Double.valueOf(stockItem.getQuantityInHand().doubleValue() - trans.getQuantity().doubleValue()));
    }
    
    double unitQuantity = 0.0D;
    if (trans.getTransactionType() == InventoryTransactionType.IN) {
      unitQuantity += baseUnitQuantity * trans.getQuantity().doubleValue();
    }
    if (trans.getTransactionType() == InventoryTransactionType.OUT) {
      unitQuantity -= baseUnitQuantity * trans.getQuantity().doubleValue();
    }
    if (stockItem.getQuantityInHand().doubleValue() == 0.0D) {
      session.delete(stockItem);
    } else
      session.saveOrUpdate(stockItem);
    return unitQuantity;
  }
  
  public void performInventoryTransaction(InventoryTransaction trans) {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      adjustInventoryStock(trans, session);
      tx.commit();
    } catch (Exception ex) {
      PosLog.error(getClass(), ex);
      tx.rollback();
    } finally {
      closeSession(session);
    }
  }
  
  public void saveInventoryTransactionList(List<InventoryTransaction> inventoryTransactions) {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      for (InventoryTransaction inventoryTransaction : inventoryTransactions) {
        adjustInventoryStock(inventoryTransaction, session);
        if (inventoryTransaction.getReason().equals("PURCHASE")) {
          MenuItem menuItem = inventoryTransaction.getMenuItem();
          MenuItemDAO.getInstance().updateLastPurchaseCost(menuItem.getId(), inventoryTransaction.getUnitCost().doubleValue(), 
            !Application.getInstance().getStore().isInventoryAvgPricingMethod(), session);
          InventoryVendorItemsDAO.getInstance().saveItems(inventoryTransaction, session);
        }
        else if (inventoryTransaction.getReason().equals("NEW STOCK")) {
          InventoryVendorItemsDAO.getInstance().saveItems(inventoryTransaction, session);
        }
      }
      tx.commit();








    }
    catch (Exception ex)
    {








      PosLog.error(getClass(), ex);
      tx.rollback();
    } finally {
      closeSession(session);
    }
  }
  
  public void performInventoryTransfer(InventoryTransaction inTrans, InventoryTransaction outTrans) {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      InventoryStock stockIn = InventoryStockDAO.getInstance().getInventoryStock(inTrans.getMenuItem(), inTrans.getToLocationId(), inTrans.getUnit());
      if (stockIn == null) {
        stockIn = new InventoryStock();
        stockIn.setMenuItem(inTrans.getMenuItem());
        stockIn.setLocationId(inTrans.getToLocationId());
        stockIn.setQuantityInHand(inTrans.getQuantity());
        stockIn.setUnit(inTrans.getUnit());
      }
      else {
        stockIn.setQuantityInHand(Double.valueOf(stockIn.getQuantityInHand().doubleValue() + inTrans.getQuantity().doubleValue()));
      }
      
      InventoryStock stockOut = InventoryStockDAO.getInstance().getInventoryStock(outTrans.getMenuItem(), outTrans.getFromLocationId(), outTrans
        .getUnit());
      if (stockOut == null) {
        stockOut = new InventoryStock();
        stockOut.setMenuItem(outTrans.getMenuItem());
        stockOut.setLocationId(outTrans.getFromLocationId());
        stockOut.setQuantityInHand(outTrans.getQuantity());
        stockOut.setUnit(outTrans.getUnit());
      }
      else {
        stockOut.setQuantityInHand(Double.valueOf(stockOut.getQuantityInHand().doubleValue() - outTrans.getQuantity().doubleValue()));
      }
      session.saveOrUpdate(inTrans);
      session.saveOrUpdate(outTrans);
      session.saveOrUpdate(stockIn);
      session.saveOrUpdate(stockOut);
      tx.commit();
    } catch (Exception ex) {
      PosLog.error(getClass(), ex);
      tx.rollback();
    } finally {
      closeSession(session);
    }
  }
  
  public void delete(String id) {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      InventoryTransaction transaction = get(id, session);
      MenuItem menuItem = transaction.getMenuItem();
      menuItem.setAvailableUnit(Double.valueOf(menuItem.getAvailableUnit().doubleValue() - transaction.getQuantity().doubleValue()));
      
      session.saveOrUpdate(menuItem);
      session.delete(transaction);
      tx.commit();
    } catch (Exception ex) {
      PosLog.error(getClass(), ex);
      tx.rollback();
    } finally {
      closeSession(session);
    }
  }
  
  public InventoryTransaction getInventoryTransaction(int referenceId) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = getSession();
      criteria = session.createCriteria(InventoryTransaction.class);
      






      criteria.createAlias(InventoryTransaction.PROP_REFERENCE_NO, "item");
      criteria.add(Restrictions.eq("item.id", Integer.valueOf(referenceId)));
      






      List<InventoryTransaction> result = criteria.list();
      InventoryTransaction localInventoryTransaction; if ((result == null) || (result.isEmpty())) {
        return null;
      }
      return (InventoryTransaction)result.get(0);
    }
    catch (Exception e)
    {
      PosLog.info(getClass(), "" + e);
    } finally {
      closeSession(session);
    }
    return null;
  }
  
  public PurchaseOrder getPurchaseOrderId(String referenceId) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = getSession();
      criteria = session.createCriteria(PurchaseOrder.class);
      


      criteria.add(Restrictions.eq(PurchaseOrder.PROP_ORDER_ID, referenceId));
      
      List<PurchaseOrder> result = criteria.list();
      PurchaseOrder localPurchaseOrder; if ((result == null) || (result.isEmpty())) {
        return null;
      }
      return (PurchaseOrder)result.get(0);
    }
    catch (Exception e) {
      PosLog.info(getClass(), "" + e);
    } finally {
      closeSession(session);
    }
    return null;
  }
  
  public List<InventoryTransaction> findTransactionsByType(String nameOrSku, InventoryTransactionType transactionType, MenuGroup menuGroup, Date fromDate, Date toDate)
  {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.createAlias(InventoryTransaction.PROP_MENU_ITEM, "menuItem");
      
      criteria.add(Restrictions.between(InventoryTransaction.PROP_TRANSACTION_DATE, fromDate, toDate));
      
      if (transactionType != null) {
        criteria.add(Restrictions.eq(InventoryTransaction.PROP_TYPE, Integer.valueOf(transactionType.getType())));
      }
      if (StringUtils.isNotEmpty(nameOrSku)) {
        criteria.add(Restrictions.or(Restrictions.ilike("menuItem.name", nameOrSku, MatchMode.ANYWHERE), Restrictions.eq("menuItem.sku", nameOrSku)));
      }
      if (menuGroup != null) {}
      



      criteria.addOrder(Order.asc("menuItem.name"));
      criteria.addOrder(Order.asc(InventoryTransaction.PROP_TRANSACTION_DATE));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public InventoryTransaction findOpeningBalance(MenuItem menuItem, Date fromdate, Date toDate) {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(InventoryTransaction.PROP_MENU_ITEM, menuItem));
      criteria.add(Restrictions.eq(InventoryTransaction.PROP_TYPE, Integer.valueOf(InventoryTransactionType.IN.getType())));
      criteria.add(Restrictions.or(Restrictions.eq(InventoryTransaction.PROP_REASON, "NEW STOCK"), 
        Restrictions.eq(InventoryTransaction.PROP_REASON, "PURCHASE")));
      criteria.add(Restrictions.ge(InventoryTransaction.PROP_TRANSACTION_DATE, fromdate));
      criteria.add(Restrictions.le(InventoryTransaction.PROP_TRANSACTION_DATE, toDate));
      
      List<InventoryTransaction> list = criteria.list();
      InventoryTransaction sum = new InventoryTransaction();
      double openingQty = 0.0D;
      double openingCost = 0.0D;
      double openingTotalCost = 0.0D;
      for (Object localObject1 = list.iterator(); ((Iterator)localObject1).hasNext();) { InventoryTransaction inventoryTransaction = (InventoryTransaction)((Iterator)localObject1).next();
        openingQty += inventoryTransaction.getQuantity().doubleValue();
        openingTotalCost += inventoryTransaction.getTotal().doubleValue();
      }
      openingCost = openingTotalCost / openingQty;
      if (Double.isNaN(openingCost)) {
        openingCost = 0.0D;
      }
      sum.setQuantity(Double.valueOf(openingQty));
      sum.setTotal(Double.valueOf(openingTotalCost));
      sum.setUnitCost(Double.valueOf(openingCost));
      
      return sum;
    } finally {
      closeSession(session);
    }
  }
  
  public List<InventoryTransaction> findTransactionsForAvgCosting(MenuGroup menuGroup, Date fromdate, Date toDate) {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      

      criteria.add(Restrictions.eq(InventoryTransaction.PROP_TYPE, Integer.valueOf(InventoryTransactionType.IN.getType())));
      criteria.add(Restrictions.or(Restrictions.eq(InventoryTransaction.PROP_REASON, "NEW STOCK"), 
        Restrictions.eq(InventoryTransaction.PROP_REASON, "PURCHASE")));
      criteria.add(Restrictions.ge(InventoryTransaction.PROP_TRANSACTION_DATE, fromdate));
      criteria.add(Restrictions.le(InventoryTransaction.PROP_TRANSACTION_DATE, toDate));
      criteria.createAlias(InventoryTransaction.PROP_MENU_ITEM, "menuItem");
      




      criteria.addOrder(Order.asc(InventoryTransaction.PROP_MENU_ITEM));
      criteria.addOrder(Order.asc(InventoryTransaction.PROP_TRANSACTION_DATE));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public int rowCount(String itemName, Object selectedType, Date fromDate, Date toDate) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(InventoryTransaction.class);
      criteria.setProjection(Projections.rowCount());
      criteria.add(Restrictions.between(InventoryTransaction.PROP_TRANSACTION_DATE, fromDate, toDate));
      if (StringUtils.isNotEmpty(itemName)) {
        criteria.createAlias(InventoryTransaction.PROP_MENU_ITEM, "item");
        criteria.add(Restrictions.ilike("item.name", "%" + itemName + "%"));
      }
      if ((selectedType instanceof InventoryLocation)) {
        criteria.add(Restrictions.eq(InventoryTransaction.PROP_FROM_LOCATION_ID, ((InventoryLocation)selectedType).getId()));
      }
      
      Number rowCount = (Number)criteria.uniqueResult();
      int i; if (rowCount != null) {
        return rowCount.intValue();
      }
      return 0;
    } finally {
      closeSession(session);
    }
  }
  
  public void getInventoryTransactions(PaginationSupport model, String itemName, Object selectedType, Date fromDate, Date toDate) {
    Session session = null;
    Criteria criteria = null;
    
    session = getSession();
    criteria = session.createCriteria(InventoryTransaction.class);
    criteria.add(Restrictions.between(InventoryTransaction.PROP_TRANSACTION_DATE, fromDate, toDate));
    if (StringUtils.isNotEmpty(itemName)) {
      criteria.createAlias(InventoryTransaction.PROP_MENU_ITEM, "item");
      criteria.add(Restrictions.ilike("item.name", "%" + itemName + "%"));
    }
    
    if ((selectedType instanceof InventoryLocation)) {
      criteria.add(Restrictions.eq(InventoryTransaction.PROP_FROM_LOCATION_ID, ((InventoryLocation)selectedType).getId()));
    }
    


    criteria.setFirstResult(model.getCurrentRowIndex());
    criteria.setMaxResults(model.getPageSize());
    List<InventoryTransaction> list = criteria.list();
    model.setRows(list);
    
    criteria.setProjection(Projections.rowCount());
    
    Number rowCount = (Number)criteria.uniqueResult();
    if (rowCount != null) {
      model.setNumRows(rowCount.intValue());
    }
  }
  
  public List<InventoryTransaction> getInventoryTransactionsByItemName(String itemName) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = getSession();
      criteria = session.createCriteria(InventoryTransaction.class);
      
      if (StringUtils.isNotEmpty(itemName)) {
        criteria.createAlias(InventoryTransaction.PROP_MENU_ITEM, "item");
        criteria.add(Restrictions.ilike("item.name", "%" + itemName + "%"));
      }
      
      List<InventoryTransaction> similarItems = criteria.list();
      if ((similarItems == null) || (similarItems.size() == 0)) {
        return null;
      }
      return similarItems;
    }
    catch (Exception localException) {}
    
    return criteria.list();
  }
  
  public Ticket getTicketId(int referenceId) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = getSession();
      criteria = session.createCriteria(Ticket.class);
      
      criteria.add(Restrictions.eq(Ticket.PROP_ID, Integer.valueOf(referenceId)));
      
      List<Ticket> result = criteria.list();
      Ticket localTicket; if ((result == null) || (result.isEmpty())) {
        return null;
      }
      return (Ticket)result.get(0);
    }
    catch (Exception e) {
      PosLog.info(getClass(), "" + e);
    } finally {
      closeSession(session);
    }
    return null;
  }
  
  public double findItemAvgCost(MenuItem menuItem) {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(InventoryTransaction.PROP_MENU_ITEM, menuItem));
      criteria.add(Restrictions.eq(InventoryTransaction.PROP_TYPE, Integer.valueOf(InventoryTransactionType.IN.getType())));
      criteria.add(Restrictions.or(Restrictions.eq(InventoryTransaction.PROP_REASON, "NEW STOCK"), 
        Restrictions.eq(InventoryTransaction.PROP_REASON, "PURCHASE")));
      
      Calendar calendar = Calendar.getInstance();
      calendar.add(2, -1);
      calendar.set(5, 1);
      Date lastMonthStart = DateUtil.startOfDay(calendar.getTime());
      
      calendar = Calendar.getInstance();
      calendar.set(5, calendar.getActualMaximum(5));
      Date thisMonthEnd = DateUtil.endOfDay(calendar.getTime());
      criteria.add(Restrictions.ge(InventoryTransaction.PROP_TRANSACTION_DATE, lastMonthStart));
      criteria.add(Restrictions.le(InventoryTransaction.PROP_TRANSACTION_DATE, thisMonthEnd));
      
      ProjectionList projectionList = Projections.projectionList();
      projectionList.add(Projections.sum(InventoryTransaction.PROP_QUANTITY));
      projectionList.add(Projections.sum(InventoryTransaction.PROP_TOTAL));
      criteria.setProjection(projectionList);
      
      List list = criteria.list();
      Object[] object = (Object[])list.get(0);
      
      double quantity = 0.0D;
      if (object[0] != null) {
        quantity = ((Number)object[0]).doubleValue();
      }
      double total = 0.0D;
      if (object[1] != null) {
        total = ((Number)object[1]).doubleValue();
      }
      double d1;
      if (quantity == 0.0D) {
        return 0.0D;
      }
      
      return total / quantity;
    } finally {
      closeSession(session);
    }
  }
  
  public double findItemAvgCost(String menuItemId, Session session) {
    Criteria criteria = session.createCriteria(getReferenceClass());
    criteria.createAlias(InventoryTransaction.PROP_MENU_ITEM, "item");
    criteria.add(Restrictions.eq("item.id", menuItemId));
    criteria.add(Restrictions.eq(InventoryTransaction.PROP_TYPE, Integer.valueOf(InventoryTransactionType.IN.getType())));
    criteria.add(Restrictions.or(Restrictions.eq(InventoryTransaction.PROP_REASON, "NEW STOCK"), 
      Restrictions.eq(InventoryTransaction.PROP_REASON, "PURCHASE")));
    
    Calendar calendar = Calendar.getInstance();
    calendar.add(2, -1);
    calendar.set(5, 1);
    Date lastMonthStart = DateUtil.startOfDay(calendar.getTime());
    
    calendar = Calendar.getInstance();
    calendar.set(5, calendar.getActualMaximum(5));
    Date thisMonthEnd = DateUtil.endOfDay(calendar.getTime());
    criteria.add(Restrictions.ge(InventoryTransaction.PROP_TRANSACTION_DATE, lastMonthStart));
    criteria.add(Restrictions.le(InventoryTransaction.PROP_TRANSACTION_DATE, thisMonthEnd));
    
    ProjectionList projectionList = Projections.projectionList();
    projectionList.add(Projections.sum(InventoryTransaction.PROP_QUANTITY));
    projectionList.add(Projections.sum(InventoryTransaction.PROP_TOTAL));
    criteria.setProjection(projectionList);
    
    List list = criteria.list();
    Object[] object = (Object[])list.get(0);
    
    double quantity = 0.0D;
    if (object[0] != null) {
      quantity = ((Number)object[0]).doubleValue();
    }
    double total = 0.0D;
    if (object[1] != null) {
      total = ((Number)object[1]).doubleValue();
    }
    
    if (quantity == 0.0D) {
      return 0.0D;
    }
    
    return total / quantity;
  }
  
  public List<InventoryTransaction> findTransactions(String reason, InventoryTransactionType transactionType) {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      if (transactionType != null) {
        criteria.add(Restrictions.eq(InventoryTransaction.PROP_TYPE, Integer.valueOf(transactionType.getType())));
      }
      if (reason != null) {
        criteria.add(Restrictions.eq(InventoryTransaction.PROP_REASON, reason));
      }
      criteria.addOrder(Order.asc(InventoryTransaction.PROP_TRANSACTION_DATE));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
}
