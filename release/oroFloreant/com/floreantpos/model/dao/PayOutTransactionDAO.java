package com.floreantpos.model.dao;

import com.floreantpos.model.PayOutTransaction;
import com.floreantpos.model.PaymentType;
import com.floreantpos.model.PayoutReason;
import com.floreantpos.model.PayoutRecepient;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.TransactionType;
import com.floreantpos.model.User;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;





















public class PayOutTransactionDAO
  extends BasePayOutTransactionDAO
{
  public PayOutTransactionDAO() {}
  
  public void saveTransaction(PayOutTransaction t, Terminal terminal)
    throws Exception
  {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      session.saveOrUpdate(t);
      session.saveOrUpdate(terminal);
      tx.commit();
    }
    catch (Exception e) {
      try {
        tx.rollback();
      }
      catch (Exception localException1) {}
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  public void createPayoutTransaction(PayoutReason reason, PayoutRecepient recepient, String note, double payoutAmount, User currentUser, Terminal drawerTerminal)
    throws Exception
  {
    PayOutTransaction payOutTransaction = new PayOutTransaction();
    payOutTransaction.setPaymentType(PaymentType.CASH.name());
    payOutTransaction.setTransactionType(TransactionType.DEBIT.name());
    
    payOutTransaction.setReason(reason);
    payOutTransaction.setRecepient(recepient);
    payOutTransaction.setNote(note);
    payOutTransaction.setAmount(Double.valueOf(payoutAmount));
    
    payOutTransaction.setCashDrawer(currentUser.getActiveDrawerPullReport());
    
    payOutTransaction.setUser(currentUser);
    payOutTransaction.setTransactionTime(new Date());
    payOutTransaction.setTerminal(drawerTerminal);
    saveTransaction(payOutTransaction, drawerTerminal);
  }
  
  public List<PayOutTransaction> getUnsettled(Terminal terminal, User cashier)
  {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(PayOutTransaction.PROP_DRAWER_RESETTED, Boolean.FALSE));
      if (cashier == null) {
        criteria.add(Restrictions.eq(PayOutTransaction.PROP_TERMINAL_ID, terminal == null ? null : terminal.getId()));
      }
      else
      {
        criteria.add(Restrictions.eq(PayOutTransaction.PROP_USER_ID, cashier.getId()));
      }
      

      List list = criteria.list();
      return list;
    } finally {
      closeSession(session);
    }
  }
}
