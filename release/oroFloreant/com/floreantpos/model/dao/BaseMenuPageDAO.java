package com.floreantpos.model.dao;

import com.floreantpos.model.MenuPage;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseMenuPageDAO
  extends _RootDAO
{
  public static MenuPageDAO instance;
  
  public BaseMenuPageDAO() {}
  
  public static MenuPageDAO getInstance()
  {
    if (null == instance) instance = new MenuPageDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return MenuPage.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public MenuPage cast(Object object)
  {
    return (MenuPage)object;
  }
  
  public MenuPage get(String key) throws HibernateException
  {
    return (MenuPage)get(getReferenceClass(), key);
  }
  
  public MenuPage get(String key, Session s) throws HibernateException
  {
    return (MenuPage)get(getReferenceClass(), key, s);
  }
  
  public MenuPage load(String key) throws HibernateException
  {
    return (MenuPage)load(getReferenceClass(), key);
  }
  
  public MenuPage load(String key, Session s) throws HibernateException
  {
    return (MenuPage)load(getReferenceClass(), key, s);
  }
  
  public MenuPage loadInitialize(String key, Session s) throws HibernateException
  {
    MenuPage obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<MenuPage> findAll()
  {
    return super.findAll();
  }
  


  public List<MenuPage> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<MenuPage> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(MenuPage menuPage)
    throws HibernateException
  {
    return (String)super.save(menuPage);
  }
  







  public String save(MenuPage menuPage, Session s)
    throws HibernateException
  {
    return (String)save(menuPage, s);
  }
  





  public void saveOrUpdate(MenuPage menuPage)
    throws HibernateException
  {
    saveOrUpdate(menuPage);
  }
  







  public void saveOrUpdate(MenuPage menuPage, Session s)
    throws HibernateException
  {
    saveOrUpdate(menuPage, s);
  }
  




  public void update(MenuPage menuPage)
    throws HibernateException
  {
    update(menuPage);
  }
  






  public void update(MenuPage menuPage, Session s)
    throws HibernateException
  {
    update(menuPage, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(MenuPage menuPage)
    throws HibernateException
  {
    delete(menuPage);
  }
  






  public void delete(MenuPage menuPage, Session s)
    throws HibernateException
  {
    delete(menuPage, s);
  }
  









  public void refresh(MenuPage menuPage, Session s)
    throws HibernateException
  {
    refresh(menuPage, s);
  }
}
