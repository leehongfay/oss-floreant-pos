package com.floreantpos.model.dao;

import com.floreantpos.model.Shift;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseShiftDAO
  extends _RootDAO
{
  public static ShiftDAO instance;
  
  public BaseShiftDAO() {}
  
  public static ShiftDAO getInstance()
  {
    if (null == instance) instance = new ShiftDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return Shift.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public Shift cast(Object object)
  {
    return (Shift)object;
  }
  
  public Shift get(String key) throws HibernateException
  {
    return (Shift)get(getReferenceClass(), key);
  }
  
  public Shift get(String key, Session s) throws HibernateException
  {
    return (Shift)get(getReferenceClass(), key, s);
  }
  
  public Shift load(String key) throws HibernateException
  {
    return (Shift)load(getReferenceClass(), key);
  }
  
  public Shift load(String key, Session s) throws HibernateException
  {
    return (Shift)load(getReferenceClass(), key, s);
  }
  
  public Shift loadInitialize(String key, Session s) throws HibernateException
  {
    Shift obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<Shift> findAll()
  {
    return super.findAll();
  }
  


  public List<Shift> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<Shift> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(Shift shift)
    throws HibernateException
  {
    return (String)super.save(shift);
  }
  







  public String save(Shift shift, Session s)
    throws HibernateException
  {
    return (String)save(shift, s);
  }
  





  public void saveOrUpdate(Shift shift)
    throws HibernateException
  {
    saveOrUpdate(shift);
  }
  







  public void saveOrUpdate(Shift shift, Session s)
    throws HibernateException
  {
    saveOrUpdate(shift, s);
  }
  




  public void update(Shift shift)
    throws HibernateException
  {
    update(shift);
  }
  






  public void update(Shift shift, Session s)
    throws HibernateException
  {
    update(shift, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(Shift shift)
    throws HibernateException
  {
    delete(shift);
  }
  






  public void delete(Shift shift, Session s)
    throws HibernateException
  {
    delete(shift, s);
  }
  









  public void refresh(Shift shift, Session s)
    throws HibernateException
  {
    refresh(shift, s);
  }
}
