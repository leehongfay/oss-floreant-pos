package com.floreantpos.model.dao;

import com.floreantpos.model.PackagingUnit;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;





















public class PackagingUnitDAO
  extends BasePackagingUnitDAO
{
  public PackagingUnitDAO() {}
  
  public boolean nameExists(String name)
  {
    Session session = null;
    
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(PackagingUnit.PROP_NAME, name).ignoreCase());
      
      criteria.setProjection(Projections.rowCount());
      Number rowCount = (Number)criteria.uniqueResult();
      boolean bool;
      if (rowCount == null) {
        return false;
      }
      return rowCount.intValue() > 0;
    }
    finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  public List<PackagingUnit> findAll()
  {
    return findAll(false);
  }
  
  public List findAll(boolean recipeUnit) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      if (recipeUnit) {
        criteria.add(Restrictions.eq(PackagingUnit.PROP_RECIPE_UNIT, Boolean.valueOf(true)));
      }
      else {
        criteria.add(Restrictions.or(Restrictions.isNull(PackagingUnit.PROP_RECIPE_UNIT), Restrictions.eq(PackagingUnit.PROP_RECIPE_UNIT, Boolean.valueOf(recipeUnit))));
      }
      return criteria.list();
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
}
