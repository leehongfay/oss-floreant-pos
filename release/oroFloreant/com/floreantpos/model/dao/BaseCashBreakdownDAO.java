package com.floreantpos.model.dao;

import com.floreantpos.model.CashBreakdown;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseCashBreakdownDAO
  extends _RootDAO
{
  public static CashBreakdownDAO instance;
  
  public BaseCashBreakdownDAO() {}
  
  public static CashBreakdownDAO getInstance()
  {
    if (null == instance) instance = new CashBreakdownDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return CashBreakdown.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public CashBreakdown cast(Object object)
  {
    return (CashBreakdown)object;
  }
  
  public CashBreakdown get(String key) throws HibernateException
  {
    return (CashBreakdown)get(getReferenceClass(), key);
  }
  
  public CashBreakdown get(String key, Session s) throws HibernateException
  {
    return (CashBreakdown)get(getReferenceClass(), key, s);
  }
  
  public CashBreakdown load(String key) throws HibernateException
  {
    return (CashBreakdown)load(getReferenceClass(), key);
  }
  
  public CashBreakdown load(String key, Session s) throws HibernateException
  {
    return (CashBreakdown)load(getReferenceClass(), key, s);
  }
  
  public CashBreakdown loadInitialize(String key, Session s) throws HibernateException
  {
    CashBreakdown obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<CashBreakdown> findAll()
  {
    return super.findAll();
  }
  


  public List<CashBreakdown> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<CashBreakdown> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(CashBreakdown cashBreakdown)
    throws HibernateException
  {
    return (String)super.save(cashBreakdown);
  }
  







  public String save(CashBreakdown cashBreakdown, Session s)
    throws HibernateException
  {
    return (String)save(cashBreakdown, s);
  }
  





  public void saveOrUpdate(CashBreakdown cashBreakdown)
    throws HibernateException
  {
    saveOrUpdate(cashBreakdown);
  }
  







  public void saveOrUpdate(CashBreakdown cashBreakdown, Session s)
    throws HibernateException
  {
    saveOrUpdate(cashBreakdown, s);
  }
  




  public void update(CashBreakdown cashBreakdown)
    throws HibernateException
  {
    update(cashBreakdown);
  }
  






  public void update(CashBreakdown cashBreakdown, Session s)
    throws HibernateException
  {
    update(cashBreakdown, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(CashBreakdown cashBreakdown)
    throws HibernateException
  {
    delete(cashBreakdown);
  }
  






  public void delete(CashBreakdown cashBreakdown, Session s)
    throws HibernateException
  {
    delete(cashBreakdown, s);
  }
  









  public void refresh(CashBreakdown cashBreakdown, Session s)
    throws HibernateException
  {
    refresh(cashBreakdown, s);
  }
}
