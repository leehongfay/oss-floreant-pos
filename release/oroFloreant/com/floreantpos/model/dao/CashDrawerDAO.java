package com.floreantpos.model.dao;

import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.StoreSession;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;






















public class CashDrawerDAO
  extends BaseCashDrawerDAO
{
  public CashDrawerDAO() {}
  
  public List<CashDrawer> findReports(Date start, Date end)
  {
    Session session = null;
    try
    {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.ge(CashDrawer.PROP_REPORT_TIME, start));
      criteria.add(Restrictions.le(CashDrawer.PROP_REPORT_TIME, end));
      
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<CashDrawer> findByStoreOperationData(StoreSession data, Boolean openDrawer) {
    if (data == null)
      return null;
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(CashDrawer.PROP_STORE_OPERATION_DATA, data));
      if (openDrawer != null)
        criteria.add(openDrawer.booleanValue() ? Restrictions.isNull(CashDrawer.PROP_REPORT_TIME) : Restrictions.isNotNull(CashDrawer.PROP_REPORT_TIME));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<CashDrawer> findByStoreOperationData(StoreSession data) {
    if (data == null)
      return null;
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(CashDrawer.PROP_STORE_OPERATION_DATA, data));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<String> getCashDrawerIds(StoreSession data) {
    if (data == null)
      return null;
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.setProjection(Projections.property(CashDrawer.PROP_ID));
      criteria.add(Restrictions.eq(CashDrawer.PROP_STORE_OPERATION_DATA, data));
      List list = criteria.list();
      return list;
    } finally {
      closeSession(session);
    }
  }
  
  public CashDrawer loadFullCashDrawer(String id) {
    Session session = null;
    try {
      session = createNewSession();
      CashDrawer cashDrawer = (CashDrawer)session.get(getReferenceClass(), id);
      Hibernate.initialize(cashDrawer.getTransactions());
      return cashDrawer;
    } finally {
      closeSession(session);
    }
  }
  
  public CashDrawer loadFullCashDrawer(CashDrawer cashDrawer) {
    Session session = null;
    try {
      session = createNewSession();
      session.refresh(cashDrawer);
      Hibernate.initialize(cashDrawer.getTransactions());
      return cashDrawer;
    } finally {
      closeSession(session);
    }
  }
  
  public double getSumOfOpeningBalance(StoreSession storeSession) {
    Session session = null;
    try {
      session = getSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(CashDrawer.PROP_STORE_OPERATION_DATA, storeSession));
      criteria.setProjection(Projections.sum(CashDrawer.PROP_BEGIN_CASH));
      
      Double openingBalance = (Double)criteria.uniqueResult();
      double d; if (openingBalance == null) {
        return 0.0D;
      }
      return openingBalance.doubleValue();
    } finally {
      closeSession(session);
    }
  }
}
