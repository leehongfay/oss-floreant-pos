package com.floreantpos.model.dao;

import com.floreantpos.model.MenuModifier;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Order;





















public abstract class BaseModifierDAO
  extends _RootDAO
{
  public static ModifierDAO instance;
  
  public BaseModifierDAO() {}
  
  public static ModifierDAO getInstance()
  {
    if (null == instance) instance = new ModifierDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return MenuModifier.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public MenuModifier cast(Object object)
  {
    return (MenuModifier)object;
  }
  
  public MenuModifier get(Integer key)
  {
    return (MenuModifier)get(getReferenceClass(), key);
  }
  
  public MenuModifier get(Integer key, Session s)
  {
    return (MenuModifier)get(getReferenceClass(), key, s);
  }
  
  public MenuModifier load(Integer key)
  {
    return (MenuModifier)load(getReferenceClass(), key);
  }
  
  public MenuModifier load(Integer key, Session s)
  {
    return (MenuModifier)load(getReferenceClass(), key, s);
  }
  
  public MenuModifier loadInitialize(Integer key, Session s)
  {
    MenuModifier obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<MenuModifier> findAll()
  {
    return super.findAll();
  }
  


  public List<MenuModifier> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<MenuModifier> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  






  public Integer save(MenuModifier modifier)
  {
    return (Integer)super.save(modifier);
  }
  








  public Integer save(MenuModifier modifier, Session s)
  {
    return (Integer)save(modifier, s);
  }
  






  public void saveOrUpdate(MenuModifier modifier)
  {
    saveOrUpdate(modifier);
  }
  








  public void saveOrUpdate(MenuModifier modifier, Session s)
  {
    saveOrUpdate(modifier, s);
  }
  





  public void update(MenuModifier modifier)
  {
    update(modifier);
  }
  







  public void update(MenuModifier modifier, Session s)
  {
    update(modifier, s);
  }
  





  public void delete(Integer id)
  {
    delete(load(id));
  }
  







  public void delete(Integer id, Session s)
  {
    delete(load(id, s), s);
  }
  





  public void delete(MenuModifier modifier)
  {
    delete(modifier);
  }
  







  public void delete(MenuModifier modifier, Session s)
  {
    delete(modifier, s);
  }
  










  public void refresh(MenuModifier modifier, Session s)
  {
    refresh(modifier, s);
  }
}
