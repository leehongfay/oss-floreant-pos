package com.floreantpos.model.dao;

import com.floreantpos.model.PrinterConfiguration;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BasePrinterConfigurationDAO
  extends _RootDAO
{
  public static PrinterConfigurationDAO instance;
  
  public BasePrinterConfigurationDAO() {}
  
  public static PrinterConfigurationDAO getInstance()
  {
    if (null == instance) instance = new PrinterConfigurationDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return PrinterConfiguration.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public PrinterConfiguration cast(Object object)
  {
    return (PrinterConfiguration)object;
  }
  
  public PrinterConfiguration get(String key) throws HibernateException
  {
    return (PrinterConfiguration)get(getReferenceClass(), key);
  }
  
  public PrinterConfiguration get(String key, Session s) throws HibernateException
  {
    return (PrinterConfiguration)get(getReferenceClass(), key, s);
  }
  
  public PrinterConfiguration load(String key) throws HibernateException
  {
    return (PrinterConfiguration)load(getReferenceClass(), key);
  }
  
  public PrinterConfiguration load(String key, Session s) throws HibernateException
  {
    return (PrinterConfiguration)load(getReferenceClass(), key, s);
  }
  
  public PrinterConfiguration loadInitialize(String key, Session s) throws HibernateException
  {
    PrinterConfiguration obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<PrinterConfiguration> findAll()
  {
    return super.findAll();
  }
  


  public List<PrinterConfiguration> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<PrinterConfiguration> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(PrinterConfiguration printerConfiguration)
    throws HibernateException
  {
    return (String)super.save(printerConfiguration);
  }
  







  public String save(PrinterConfiguration printerConfiguration, Session s)
    throws HibernateException
  {
    return (String)save(printerConfiguration, s);
  }
  





  public void saveOrUpdate(PrinterConfiguration printerConfiguration)
    throws HibernateException
  {
    saveOrUpdate(printerConfiguration);
  }
  







  public void saveOrUpdate(PrinterConfiguration printerConfiguration, Session s)
    throws HibernateException
  {
    saveOrUpdate(printerConfiguration, s);
  }
  




  public void update(PrinterConfiguration printerConfiguration)
    throws HibernateException
  {
    update(printerConfiguration);
  }
  






  public void update(PrinterConfiguration printerConfiguration, Session s)
    throws HibernateException
  {
    update(printerConfiguration, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(PrinterConfiguration printerConfiguration)
    throws HibernateException
  {
    delete(printerConfiguration);
  }
  






  public void delete(PrinterConfiguration printerConfiguration, Session s)
    throws HibernateException
  {
    delete(printerConfiguration, s);
  }
  









  public void refresh(PrinterConfiguration printerConfiguration, Session s)
    throws HibernateException
  {
    refresh(printerConfiguration, s);
  }
}
