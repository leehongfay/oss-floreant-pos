package com.floreantpos.model.dao;

import com.floreantpos.model.Outlet;
import org.hibernate.Session;





public class OutletDAO
  extends BaseOutletDAO
{
  public OutletDAO() {}
  
  public Outlet initialize(Outlet outlet)
  {
    if ((outlet == null) || (outlet.getId() == null)) {
      return outlet;
    }
    Session session = null;
    try
    {
      session = createNewSession();
      session.refresh(outlet);
      


      return outlet;
    } finally {
      closeSession(session);
    }
  }
}
