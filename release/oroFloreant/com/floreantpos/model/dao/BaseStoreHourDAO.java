package com.floreantpos.model.dao;

import com.floreantpos.model.StoreHour;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseStoreHourDAO
  extends _RootDAO
{
  public static StoreHourDAO instance;
  
  public BaseStoreHourDAO() {}
  
  public static StoreHourDAO getInstance()
  {
    if (null == instance) instance = new StoreHourDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return StoreHour.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public StoreHour cast(Object object)
  {
    return (StoreHour)object;
  }
  
  public StoreHour get(String key) throws HibernateException
  {
    return (StoreHour)get(getReferenceClass(), key);
  }
  
  public StoreHour get(String key, Session s) throws HibernateException
  {
    return (StoreHour)get(getReferenceClass(), key, s);
  }
  
  public StoreHour load(String key) throws HibernateException
  {
    return (StoreHour)load(getReferenceClass(), key);
  }
  
  public StoreHour load(String key, Session s) throws HibernateException
  {
    return (StoreHour)load(getReferenceClass(), key, s);
  }
  
  public StoreHour loadInitialize(String key, Session s) throws HibernateException
  {
    StoreHour obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<StoreHour> findAll()
  {
    return super.findAll();
  }
  


  public List<StoreHour> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<StoreHour> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(StoreHour storeHour)
    throws HibernateException
  {
    return (String)super.save(storeHour);
  }
  







  public String save(StoreHour storeHour, Session s)
    throws HibernateException
  {
    return (String)save(storeHour, s);
  }
  





  public void saveOrUpdate(StoreHour storeHour)
    throws HibernateException
  {
    saveOrUpdate(storeHour);
  }
  







  public void saveOrUpdate(StoreHour storeHour, Session s)
    throws HibernateException
  {
    saveOrUpdate(storeHour, s);
  }
  




  public void update(StoreHour storeHour)
    throws HibernateException
  {
    update(storeHour);
  }
  






  public void update(StoreHour storeHour, Session s)
    throws HibernateException
  {
    update(storeHour, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(StoreHour storeHour)
    throws HibernateException
  {
    delete(storeHour);
  }
  






  public void delete(StoreHour storeHour, Session s)
    throws HibernateException
  {
    delete(storeHour, s);
  }
  









  public void refresh(StoreHour storeHour, Session s)
    throws HibernateException
  {
    refresh(storeHour, s);
  }
}
