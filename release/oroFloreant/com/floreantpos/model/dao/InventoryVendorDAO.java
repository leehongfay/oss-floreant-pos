package com.floreantpos.model.dao;

import com.floreantpos.model.InventoryVendor;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;






















public class InventoryVendorDAO
  extends BaseInventoryVendorDAO
{
  public InventoryVendorDAO() {}
  
  public void saveAll(List<InventoryVendor> vendorList)
  {
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      for (InventoryVendor vendor : vendorList) {
        session.saveOrUpdate(vendor);
      }
      
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
}
