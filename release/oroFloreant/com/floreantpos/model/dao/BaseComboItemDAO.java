package com.floreantpos.model.dao;

import com.floreantpos.model.ComboItem;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseComboItemDAO
  extends _RootDAO
{
  public static ComboItemDAO instance;
  
  public BaseComboItemDAO() {}
  
  public static ComboItemDAO getInstance()
  {
    if (null == instance) instance = new ComboItemDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return ComboItem.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public ComboItem cast(Object object)
  {
    return (ComboItem)object;
  }
  
  public ComboItem get(String key) throws HibernateException
  {
    return (ComboItem)get(getReferenceClass(), key);
  }
  
  public ComboItem get(String key, Session s) throws HibernateException
  {
    return (ComboItem)get(getReferenceClass(), key, s);
  }
  
  public ComboItem load(String key) throws HibernateException
  {
    return (ComboItem)load(getReferenceClass(), key);
  }
  
  public ComboItem load(String key, Session s) throws HibernateException
  {
    return (ComboItem)load(getReferenceClass(), key, s);
  }
  
  public ComboItem loadInitialize(String key, Session s) throws HibernateException
  {
    ComboItem obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<ComboItem> findAll()
  {
    return super.findAll();
  }
  


  public List<ComboItem> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<ComboItem> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(ComboItem comboItem)
    throws HibernateException
  {
    return (String)super.save(comboItem);
  }
  







  public String save(ComboItem comboItem, Session s)
    throws HibernateException
  {
    return (String)save(comboItem, s);
  }
  





  public void saveOrUpdate(ComboItem comboItem)
    throws HibernateException
  {
    saveOrUpdate(comboItem);
  }
  







  public void saveOrUpdate(ComboItem comboItem, Session s)
    throws HibernateException
  {
    saveOrUpdate(comboItem, s);
  }
  




  public void update(ComboItem comboItem)
    throws HibernateException
  {
    update(comboItem);
  }
  






  public void update(ComboItem comboItem, Session s)
    throws HibernateException
  {
    update(comboItem, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(ComboItem comboItem)
    throws HibernateException
  {
    delete(comboItem);
  }
  






  public void delete(ComboItem comboItem, Session s)
    throws HibernateException
  {
    delete(comboItem, s);
  }
  









  public void refresh(ComboItem comboItem, Session s)
    throws HibernateException
  {
    refresh(comboItem, s);
  }
}
