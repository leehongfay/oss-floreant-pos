package com.floreantpos.model.dao;

import com.floreantpos.model.ReservationShift;
import org.hibernate.Session;
import org.hibernate.Transaction;







public class ReservationShiftDAO
  extends BaseReservationShiftDAO
{
  public ReservationShiftDAO() {}
  
  public void releaseParentAndDelete(ReservationShift reservationShift)
  {
    if (reservationShift == null) {
      return;
    }
    
    Session session = null;
    Transaction tx = null;
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      session.delete(reservationShift);
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
}
