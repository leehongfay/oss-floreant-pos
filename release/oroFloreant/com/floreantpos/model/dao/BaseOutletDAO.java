package com.floreantpos.model.dao;

import com.floreantpos.model.Outlet;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseOutletDAO
  extends _RootDAO
{
  public static OutletDAO instance;
  
  public BaseOutletDAO() {}
  
  public static OutletDAO getInstance()
  {
    if (null == instance) instance = new OutletDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return Outlet.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public Outlet cast(Object object)
  {
    return (Outlet)object;
  }
  
  public Outlet get(String key) throws HibernateException
  {
    return (Outlet)get(getReferenceClass(), key);
  }
  
  public Outlet get(String key, Session s) throws HibernateException
  {
    return (Outlet)get(getReferenceClass(), key, s);
  }
  
  public Outlet load(String key) throws HibernateException
  {
    return (Outlet)load(getReferenceClass(), key);
  }
  
  public Outlet load(String key, Session s) throws HibernateException
  {
    return (Outlet)load(getReferenceClass(), key, s);
  }
  
  public Outlet loadInitialize(String key, Session s) throws HibernateException
  {
    Outlet obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<Outlet> findAll()
  {
    return super.findAll();
  }
  


  public List<Outlet> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<Outlet> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(Outlet outlet)
    throws HibernateException
  {
    return (String)super.save(outlet);
  }
  







  public String save(Outlet outlet, Session s)
    throws HibernateException
  {
    return (String)save(outlet, s);
  }
  





  public void saveOrUpdate(Outlet outlet)
    throws HibernateException
  {
    saveOrUpdate(outlet);
  }
  







  public void saveOrUpdate(Outlet outlet, Session s)
    throws HibernateException
  {
    saveOrUpdate(outlet, s);
  }
  




  public void update(Outlet outlet)
    throws HibernateException
  {
    update(outlet);
  }
  






  public void update(Outlet outlet, Session s)
    throws HibernateException
  {
    update(outlet, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(Outlet outlet)
    throws HibernateException
  {
    delete(outlet);
  }
  






  public void delete(Outlet outlet, Session s)
    throws HibernateException
  {
    delete(outlet, s);
  }
  









  public void refresh(Outlet outlet, Session s)
    throws HibernateException
  {
    refresh(outlet, s);
  }
}
