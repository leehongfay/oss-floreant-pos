package com.floreantpos.model.dao;

import com.floreantpos.model.TicketItem;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseTicketItemDAO
  extends _RootDAO
{
  public static TicketItemDAO instance;
  
  public BaseTicketItemDAO() {}
  
  public static TicketItemDAO getInstance()
  {
    if (null == instance) instance = new TicketItemDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return TicketItem.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public TicketItem cast(Object object)
  {
    return (TicketItem)object;
  }
  
  public TicketItem get(String key) throws HibernateException
  {
    return (TicketItem)get(getReferenceClass(), key);
  }
  
  public TicketItem get(String key, Session s) throws HibernateException
  {
    return (TicketItem)get(getReferenceClass(), key, s);
  }
  
  public TicketItem load(String key) throws HibernateException
  {
    return (TicketItem)load(getReferenceClass(), key);
  }
  
  public TicketItem load(String key, Session s) throws HibernateException
  {
    return (TicketItem)load(getReferenceClass(), key, s);
  }
  
  public TicketItem loadInitialize(String key, Session s) throws HibernateException
  {
    TicketItem obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<TicketItem> findAll()
  {
    return super.findAll();
  }
  


  public List<TicketItem> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<TicketItem> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(TicketItem ticketItem)
    throws HibernateException
  {
    return (String)super.save(ticketItem);
  }
  







  public String save(TicketItem ticketItem, Session s)
    throws HibernateException
  {
    return (String)save(ticketItem, s);
  }
  





  public void saveOrUpdate(TicketItem ticketItem)
    throws HibernateException
  {
    saveOrUpdate(ticketItem);
  }
  







  public void saveOrUpdate(TicketItem ticketItem, Session s)
    throws HibernateException
  {
    saveOrUpdate(ticketItem, s);
  }
  




  public void update(TicketItem ticketItem)
    throws HibernateException
  {
    update(ticketItem);
  }
  






  public void update(TicketItem ticketItem, Session s)
    throws HibernateException
  {
    update(ticketItem, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(TicketItem ticketItem)
    throws HibernateException
  {
    delete(ticketItem);
  }
  






  public void delete(TicketItem ticketItem, Session s)
    throws HibernateException
  {
    delete(ticketItem, s);
  }
  









  public void refresh(TicketItem ticketItem, Session s)
    throws HibernateException
  {
    refresh(ticketItem, s);
  }
}
