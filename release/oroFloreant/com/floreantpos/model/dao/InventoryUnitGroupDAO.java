package com.floreantpos.model.dao;

import com.floreantpos.model.InventoryUnitGroup;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;




public class InventoryUnitGroupDAO
  extends BaseInventoryUnitGroupDAO
{
  public InventoryUnitGroupDAO() {}
  
  public InventoryUnitGroup findByName(String unitGroupName)
  {
    Session session = null;
    try {
      session = createNewSession();
      return findByName(unitGroupName, session);
    } finally {
      closeSession(session);
    }
  }
  
  public InventoryUnitGroup findByName(String unitGroupName, Session session) {
    if (session == null) {
      session = getSession();
    }
    Criteria criteria = session.createCriteria(getReferenceClass());
    criteria.add(Restrictions.eq(InventoryUnitGroup.PROP_NAME, unitGroupName));
    
    return (InventoryUnitGroup)criteria.uniqueResult();
  }
}
