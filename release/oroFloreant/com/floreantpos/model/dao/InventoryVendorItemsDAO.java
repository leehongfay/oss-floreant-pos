package com.floreantpos.model.dao;

import com.floreantpos.PosLog;
import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.InventoryVendorItems;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.PurchaseOrder;
import com.floreantpos.model.PurchaseOrderItem;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;





public class InventoryVendorItemsDAO
  extends BaseInventoryVendorItemsDAO
{
  public InventoryVendorItemsDAO() {}
  
  public List<InventoryVendorItems> findByItem(MenuItem item)
  {
    if (item.getId() == null) {
      return null;
    }
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      if (item != null) {
        criteria.add(Restrictions.eq(InventoryVendorItems.PROP_ITEM, item));
      }
      
      List list = criteria.list();
      Object localObject1; if (list.isEmpty()) {
        return null;
      }
      
      return list;
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public InventoryVendorItems findByItemAndVendor(MenuItem item, InventoryVendor vendor) {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      if ((item != null) && (vendor != null)) {
        criteria.add(Restrictions.eq(InventoryVendorItems.PROP_ITEM, item));
        criteria.add(Restrictions.eq(InventoryVendorItems.PROP_VENDOR, vendor));
      }
      
      List list = criteria.list();
      InventoryVendorItems localInventoryVendorItems; if (list.isEmpty()) {
        return null;
      }
      
      return (InventoryVendorItems)list.get(0);
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public boolean vendorHasItem(InventoryVendor vendor, MenuItem item, Session session) {
    if (item.getId() == null) {
      return false;
    }
    boolean isNewSession = false;
    try {
      if (session == null) {
        session = createNewSession();
        isNewSession = true;
      }
      Criteria criteria = session.createCriteria(getReferenceClass());
      if ((item != null) && (vendor != null)) {
        criteria.add(Restrictions.eq(InventoryVendorItems.PROP_ITEM, item));
        criteria.add(Restrictions.eq(InventoryVendorItems.PROP_VENDOR, vendor));
      }
      
      List list = criteria.list();
      boolean bool1; if (list.isEmpty()) {
        return false;
      }
      
      return true;
    } catch (Exception e) {
      PosLog.error(InventoryVendorDAO.class, e.getMessage(), e);
    } finally {
      if ((isNewSession) && (session != null)) {
        session.close();
      }
    }
    return false;
  }
  
  public void saveItems(PurchaseOrder order) {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      List<PurchaseOrderItem> orderItems = order.getOrderItems();
      for (PurchaseOrderItem purchaseOrderItem : orderItems) {
        MenuItem menuItem = purchaseOrderItem.getMenuItem();
        InventoryVendor vendor = order.getVendor();
        if (!vendorHasItem(vendor, menuItem, null)) {
          InventoryVendorItems inventoryVendorItems = new InventoryVendorItems();
          inventoryVendorItems.setItem(menuItem);
          inventoryVendorItems.setVendor(vendor);
          session.saveOrUpdate(inventoryVendorItems);
        }
      }
      
      session.saveOrUpdate(order);
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      throwException(e);
    } finally {
      if (session != null) {
        session.close();
      }
    }
  }
  
  public void saveItems(InventoryTransaction transaction, Session session) {
    try {
      MenuItem menuItem = transaction.getMenuItem();
      InventoryVendor vendor = null;
      if (transaction.getVendorId() != null) {
        vendor = InventoryVendorDAO.getInstance().get(transaction.getId());
      }
      if (!vendorHasItem(vendor, menuItem, session)) {
        InventoryVendorItems inventoryVendorItems = new InventoryVendorItems();
        inventoryVendorItems.setItem(menuItem);
        inventoryVendorItems.setVendor(vendor);
        session.saveOrUpdate(inventoryVendorItems);
      }
    } catch (Exception e) {
      PosLog.error(InventoryVendorDAO.class, e.getMessage(), e);
    }
  }
  
  public void saveAll(List<InventoryVendorItems> vendorItemsList) {
    Transaction tx = null;
    Session session = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      saveAll(vendorItemsList, session);
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
      throw new RuntimeException(e);
    } finally {
      closeSession(session);
    }
  }
  
  public void saveAll(List<InventoryVendorItems> vendorItemsList, Session session) {
    for (InventoryVendorItems inventoryVendorItems : vendorItemsList) {
      saveOrUpdate(inventoryVendorItems, session);
    }
  }
}
