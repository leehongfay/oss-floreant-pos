package com.floreantpos.model.dao;

import com.floreantpos.model.PurchaseOrder;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BasePurchaseOrderDAO
  extends _RootDAO
{
  public static PurchaseOrderDAO instance;
  
  public BasePurchaseOrderDAO() {}
  
  public static PurchaseOrderDAO getInstance()
  {
    if (null == instance) instance = new PurchaseOrderDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return PurchaseOrder.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public PurchaseOrder cast(Object object)
  {
    return (PurchaseOrder)object;
  }
  
  public PurchaseOrder get(String key) throws HibernateException
  {
    return (PurchaseOrder)get(getReferenceClass(), key);
  }
  
  public PurchaseOrder get(String key, Session s) throws HibernateException
  {
    return (PurchaseOrder)get(getReferenceClass(), key, s);
  }
  
  public PurchaseOrder load(String key) throws HibernateException
  {
    return (PurchaseOrder)load(getReferenceClass(), key);
  }
  
  public PurchaseOrder load(String key, Session s) throws HibernateException
  {
    return (PurchaseOrder)load(getReferenceClass(), key, s);
  }
  
  public PurchaseOrder loadInitialize(String key, Session s) throws HibernateException
  {
    PurchaseOrder obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<PurchaseOrder> findAll()
  {
    return super.findAll();
  }
  


  public List<PurchaseOrder> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<PurchaseOrder> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(PurchaseOrder purchaseOrder)
    throws HibernateException
  {
    return (String)super.save(purchaseOrder);
  }
  







  public String save(PurchaseOrder purchaseOrder, Session s)
    throws HibernateException
  {
    return (String)save(purchaseOrder, s);
  }
  





  public void saveOrUpdate(PurchaseOrder purchaseOrder)
    throws HibernateException
  {
    saveOrUpdate(purchaseOrder);
  }
  







  public void saveOrUpdate(PurchaseOrder purchaseOrder, Session s)
    throws HibernateException
  {
    saveOrUpdate(purchaseOrder, s);
  }
  




  public void update(PurchaseOrder purchaseOrder)
    throws HibernateException
  {
    update(purchaseOrder);
  }
  






  public void update(PurchaseOrder purchaseOrder, Session s)
    throws HibernateException
  {
    update(purchaseOrder, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(PurchaseOrder purchaseOrder)
    throws HibernateException
  {
    delete(purchaseOrder);
  }
  






  public void delete(PurchaseOrder purchaseOrder, Session s)
    throws HibernateException
  {
    delete(purchaseOrder, s);
  }
  









  public void refresh(PurchaseOrder purchaseOrder, Session s)
    throws HibernateException
  {
    refresh(purchaseOrder, s);
  }
}
