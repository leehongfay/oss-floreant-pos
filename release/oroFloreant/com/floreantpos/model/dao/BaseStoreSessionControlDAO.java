package com.floreantpos.model.dao;

import com.floreantpos.model.StoreSessionControl;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseStoreSessionControlDAO
  extends _RootDAO
{
  public static StoreSessionControlDAO instance;
  
  public BaseStoreSessionControlDAO() {}
  
  public static StoreSessionControlDAO getInstance()
  {
    if (null == instance) instance = new StoreSessionControlDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return StoreSessionControl.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public StoreSessionControl cast(Object object)
  {
    return (StoreSessionControl)object;
  }
  
  public StoreSessionControl get(String key) throws HibernateException
  {
    return (StoreSessionControl)get(getReferenceClass(), key);
  }
  
  public StoreSessionControl get(String key, Session s) throws HibernateException
  {
    return (StoreSessionControl)get(getReferenceClass(), key, s);
  }
  
  public StoreSessionControl load(String key) throws HibernateException
  {
    return (StoreSessionControl)load(getReferenceClass(), key);
  }
  
  public StoreSessionControl load(String key, Session s) throws HibernateException
  {
    return (StoreSessionControl)load(getReferenceClass(), key, s);
  }
  
  public StoreSessionControl loadInitialize(String key, Session s) throws HibernateException
  {
    StoreSessionControl obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<StoreSessionControl> findAll()
  {
    return super.findAll();
  }
  


  public List<StoreSessionControl> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<StoreSessionControl> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(StoreSessionControl storeSessionControl)
    throws HibernateException
  {
    return (String)super.save(storeSessionControl);
  }
  







  public String save(StoreSessionControl storeSessionControl, Session s)
    throws HibernateException
  {
    return (String)save(storeSessionControl, s);
  }
  





  public void saveOrUpdate(StoreSessionControl storeSessionControl)
    throws HibernateException
  {
    saveOrUpdate(storeSessionControl);
  }
  







  public void saveOrUpdate(StoreSessionControl storeSessionControl, Session s)
    throws HibernateException
  {
    saveOrUpdate(storeSessionControl, s);
  }
  




  public void update(StoreSessionControl storeSessionControl)
    throws HibernateException
  {
    update(storeSessionControl);
  }
  






  public void update(StoreSessionControl storeSessionControl, Session s)
    throws HibernateException
  {
    update(storeSessionControl, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(StoreSessionControl storeSessionControl)
    throws HibernateException
  {
    delete(storeSessionControl);
  }
  






  public void delete(StoreSessionControl storeSessionControl, Session s)
    throws HibernateException
  {
    delete(storeSessionControl, s);
  }
  









  public void refresh(StoreSessionControl storeSessionControl, Session s)
    throws HibernateException
  {
    refresh(storeSessionControl, s);
  }
}
