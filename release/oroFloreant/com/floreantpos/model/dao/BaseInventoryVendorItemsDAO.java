package com.floreantpos.model.dao;

import com.floreantpos.model.InventoryVendorItems;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseInventoryVendorItemsDAO
  extends _RootDAO
{
  public static InventoryVendorItemsDAO instance;
  
  public BaseInventoryVendorItemsDAO() {}
  
  public static InventoryVendorItemsDAO getInstance()
  {
    if (null == instance) instance = new InventoryVendorItemsDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return InventoryVendorItems.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public InventoryVendorItems cast(Object object)
  {
    return (InventoryVendorItems)object;
  }
  
  public InventoryVendorItems get(String key) throws HibernateException
  {
    return (InventoryVendorItems)get(getReferenceClass(), key);
  }
  
  public InventoryVendorItems get(String key, Session s) throws HibernateException
  {
    return (InventoryVendorItems)get(getReferenceClass(), key, s);
  }
  
  public InventoryVendorItems load(String key) throws HibernateException
  {
    return (InventoryVendorItems)load(getReferenceClass(), key);
  }
  
  public InventoryVendorItems load(String key, Session s) throws HibernateException
  {
    return (InventoryVendorItems)load(getReferenceClass(), key, s);
  }
  
  public InventoryVendorItems loadInitialize(String key, Session s) throws HibernateException
  {
    InventoryVendorItems obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<InventoryVendorItems> findAll()
  {
    return super.findAll();
  }
  


  public List<InventoryVendorItems> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<InventoryVendorItems> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(InventoryVendorItems inventoryVendorItems)
    throws HibernateException
  {
    return (String)super.save(inventoryVendorItems);
  }
  







  public String save(InventoryVendorItems inventoryVendorItems, Session s)
    throws HibernateException
  {
    return (String)save(inventoryVendorItems, s);
  }
  





  public void saveOrUpdate(InventoryVendorItems inventoryVendorItems)
    throws HibernateException
  {
    saveOrUpdate(inventoryVendorItems);
  }
  







  public void saveOrUpdate(InventoryVendorItems inventoryVendorItems, Session s)
    throws HibernateException
  {
    saveOrUpdate(inventoryVendorItems, s);
  }
  




  public void update(InventoryVendorItems inventoryVendorItems)
    throws HibernateException
  {
    update(inventoryVendorItems);
  }
  






  public void update(InventoryVendorItems inventoryVendorItems, Session s)
    throws HibernateException
  {
    update(inventoryVendorItems, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(InventoryVendorItems inventoryVendorItems)
    throws HibernateException
  {
    delete(inventoryVendorItems);
  }
  






  public void delete(InventoryVendorItems inventoryVendorItems, Session s)
    throws HibernateException
  {
    delete(inventoryVendorItems, s);
  }
  









  public void refresh(InventoryVendorItems inventoryVendorItems, Session s)
    throws HibernateException
  {
    refresh(inventoryVendorItems, s);
  }
}
