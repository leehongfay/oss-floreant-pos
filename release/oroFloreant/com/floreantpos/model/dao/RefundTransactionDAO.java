package com.floreantpos.model.dao;

import com.floreantpos.model.RefundTransaction;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.util.RefundSummary;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;






















public class RefundTransactionDAO
  extends BaseRefundTransactionDAO
{
  public RefundTransactionDAO() {}
  
  public RefundSummary getTotalRefundForTerminal(Terminal terminal, User cashier)
  {
    Session session = null;
    RefundSummary refundSummary = new RefundSummary();
    try
    {
      session = getSession();
      
      Criteria criteria = session.createCriteria(getReferenceClass());
      
      ProjectionList projectionList = Projections.projectionList();
      projectionList.add(Projections.rowCount());
      projectionList.add(Projections.sum(RefundTransaction.PROP_AMOUNT));
      criteria.setProjection(projectionList);
      
      if (cashier == null) {
        criteria.add(Restrictions.eq(RefundTransaction.PROP_TERMINAL_ID, terminal == null ? null : terminal.getId()));
      }
      else
      {
        criteria.add(Restrictions.eq(RefundTransaction.PROP_USER_ID, cashier.getId()));
      }
      

      criteria.add(Restrictions.eq(RefundTransaction.PROP_DRAWER_RESETTED, Boolean.FALSE));
      
      List list = criteria.list();
      Object[] objects; if (list.size() > 0) {
        objects = (Object[])list.get(0);
        
        if ((objects.length > 0) && (objects[0] != null)) {
          refundSummary.setCount(((Number)objects[0]).intValue());
        }
        
        if ((objects.length > 1) && (objects[1] != null)) {
          refundSummary.setAmount(((Number)objects[1]).doubleValue());
        }
      }
      return refundSummary;
    } finally {
      closeSession(session);
    }
  }
}
