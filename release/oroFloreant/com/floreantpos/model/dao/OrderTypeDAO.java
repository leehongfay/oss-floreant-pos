package com.floreantpos.model.dao;

import com.floreantpos.model.Department;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.SalesArea;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.TerminalType;
import java.util.Arrays;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;





public class OrderTypeDAO
  extends BaseOrderTypeDAO
{
  public OrderTypeDAO() {}
  
  public List<OrderType> findAll()
  {
    Session session = null;
    try {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.addOrder(Order.asc(OrderType.PROP_SORT_ORDER));
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public List<OrderType> findEnabledOrderTypesForTerminal(Terminal terminal) {
    Session session = null;
    try
    {
      TerminalType terminalType = terminal.getTerminalType();
      Department department = terminal.getDepartment();
      SalesArea salesArea = terminal.getSalesArea();
      

      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass(), "c");
      criteria.add(Restrictions.eq(OrderType.PROP_ENABLED, Boolean.valueOf(true)));
      
      if (terminalType != null) {
        criteria.createAlias("c.terminalTypes", "terminalType", JoinType.LEFT_OUTER_JOIN);
        criteria.add(
          Restrictions.or(
          Restrictions.isEmpty("c.terminalTypes"), 
          Restrictions.in("terminalType.id", Arrays.asList(new String[] {terminalType.getId() }))));
      }
      

      if (department != null) {
        criteria.createAlias("c.departments", "department", JoinType.LEFT_OUTER_JOIN);
        criteria.add(
          Restrictions.or(
          Restrictions.isEmpty("c.departments"), 
          Restrictions.in("department.id", Arrays.asList(new String[] {department.getId() }))));
      }
      

      if (salesArea != null) {
        criteria.add(Restrictions.eqOrIsNull(OrderType.PROP_SALES_AREA_ID, salesArea.getId()));
      }
      
      criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
      return criteria.list();
















    }
    finally
    {















      closeSession(session);
    }
  }
  


































  public List<OrderType> findLoginScreenViewOrderTypes()
  {
    Session session = null;
    try {
      session = createNewSession();
      
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(OrderType.PROP_ENABLED, Boolean.valueOf(true)));
      criteria.add(Restrictions.eq(OrderType.PROP_SHOW_IN_LOGIN_SCREEN, Boolean.valueOf(true)));
      
      return criteria.list();
    } finally {
      closeSession(session);
    }
  }
  
  public OrderType findByName(String orderType) {
    Session session = null;
    try {
      session = createNewSession();
      return findByName(orderType, session);
    } finally {
      closeSession(session);
    }
  }
  
  public OrderType findByName(String orderType, Session session) {
    if (session == null) {
      session = getSession();
    }
    Criteria criteria = session.createCriteria(getReferenceClass());
    criteria.add(Restrictions.eq(OrderType.PROP_NAME, orderType));
    
    return (OrderType)criteria.uniqueResult();
  }
  




  public boolean containsOrderTypeObj()
  {
    Session session = null;
    try {
      session = createNewSession();
      Query query = session.createSQLQuery("select count(s.MENU_ITEM_ID), count(s.ORDER_TYPE_ID) from ITEM_ORDER_TYPE s");
      List result = query.list();
      Object[] object = (Object[])result.get(0);
      Integer menuItemCount = getInt(object, 0);
      Integer orderTypeCount = getInt(object, 1);
      boolean bool;
      if (menuItemCount.intValue() < 1) {
        return true;
      }
      return orderTypeCount.intValue() > 0;
    }
    catch (Exception localException) {}finally
    {
      if (session != null) {
        closeSession(session);
      }
    }
    return false;
  }
  



  public void updateMenuItemOrderType()
  {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      Query query = session.createSQLQuery("Update ITEM_ORDER_TYPE t SET t.ORDER_TYPE_ID=(Select o.id from ORDER_TYPE o where o.NAME=t.ORDER_TYPE)");
      query.executeUpdate();
      tx.commit();
    } catch (Exception e) {
      tx.rollback();
    } finally {
      if (session != null) {
        closeSession(session);
      }
    }
  }
  
  private Integer getInt(Object[] array, int index) {
    if (array.length < index + 1) {
      return null;
    }
    if ((array[index] instanceof Number)) {
      return Integer.valueOf(((Number)array[index]).intValue());
    }
    
    return null;
  }
  
  public OrderType getFirstOrderType() {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.setFirstResult(0);
      criteria.setMaxResults(1);
      List result = criteria.list();
      OrderType localOrderType; if (result.size() > 0) {
        return (OrderType)result.get(0);
      }
      return null;
    } finally {
      closeSession(session);
    }
  }
  
  public OrderType getOrderType(boolean dineIn) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(OrderType.PROP_ENABLED, Boolean.valueOf(true)));
      criteria.add(Restrictions.eq(OrderType.PROP_SHOW_TABLE_SELECTION, Boolean.valueOf(dineIn)));
      if (!dineIn) {
        criteria.add(Restrictions.eq(OrderType.PROP_DELIVERY, Boolean.FALSE));
      }
      List result = criteria.list();
      OrderType localOrderType; if (result.size() > 0) {
        return (OrderType)result.get(0);
      }
      return null;
    } finally {
      closeSession(session);
    }
  }
  
  public OrderType getHomeDeliveryOrderType() {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(OrderType.PROP_ENABLED, Boolean.valueOf(true)));
      criteria.add(Restrictions.eq(OrderType.PROP_DELIVERY, Boolean.TRUE));
      List result = criteria.list();
      OrderType localOrderType; if (result.size() > 0) {
        return (OrderType)result.get(0);
      }
      return null;
    } finally {
      closeSession(session);
    }
  }
  
  public void saveOrUpdateOrderType(OrderType ordersType, List<Department> checkedValues) {
    Session session = null;
    Transaction tx = null;
    try {
      session = createNewSession();
      tx = session.beginTransaction();
      session.saveOrUpdate(ordersType);
      for (Department object : checkedValues) {
        session.saveOrUpdate(object);
      }
      tx.commit();
    } catch (Exception e) {
      try {
        tx.rollback();
      }
      catch (Exception localException1) {}
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  public OrderType initialize(OrderType orderType) {
    if ((orderType == null) || (orderType.getId() == null)) {
      return orderType;
    }
    Session session = null;
    try
    {
      session = createNewSession();
      session.refresh(orderType);
      
      Hibernate.initialize(orderType.getDepartments());
      Hibernate.initialize(orderType.getTerminalTypes());
      Hibernate.initialize(orderType.getCategories());
      
      return orderType;
    } finally {
      closeSession(session);
    }
  }
}
