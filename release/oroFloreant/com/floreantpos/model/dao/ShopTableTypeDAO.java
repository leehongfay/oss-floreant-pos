package com.floreantpos.model.dao;

import com.floreantpos.model.ShopTableType;
import org.hibernate.criterion.Order;



























public class ShopTableTypeDAO
  extends BaseShopTableTypeDAO
{
  public ShopTableTypeDAO() {}
  
  public Order getDefaultOrder()
  {
    return Order.asc(ShopTableType.PROP_ID);
  }
}
