package com.floreantpos.model.dao;

import com.floreantpos.model.CustomPayment;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;






public class CustomPaymentDAO
  extends BaseCustomPaymentDAO
{
  public CustomPaymentDAO() {}
  
  public Order getDefaultOrder()
  {
    return Order.asc(CustomPayment.PROP_ID);
  }
  
  public CustomPayment getByName(String name) {
    Session session = null;
    try {
      session = createNewSession();
      
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.like(CustomPayment.PROP_NAME, name));
      
      return (CustomPayment)criteria.uniqueResult();
    } finally {
      closeSession(session);
    }
  }
}
