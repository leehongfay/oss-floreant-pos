package com.floreantpos.model.dao;

import com.floreantpos.model.PriceShift;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BasePriceShiftDAO
  extends _RootDAO
{
  public static PriceShiftDAO instance;
  
  public BasePriceShiftDAO() {}
  
  public static PriceShiftDAO getInstance()
  {
    if (null == instance) instance = new PriceShiftDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return PriceShift.class;
  }
  
  public Order getDefaultOrder() {
    return null;
  }
  


  public PriceShift cast(Object object)
  {
    return (PriceShift)object;
  }
  
  public PriceShift get(String key) throws HibernateException
  {
    return (PriceShift)get(getReferenceClass(), key);
  }
  
  public PriceShift get(String key, Session s) throws HibernateException
  {
    return (PriceShift)get(getReferenceClass(), key, s);
  }
  
  public PriceShift load(String key) throws HibernateException
  {
    return (PriceShift)load(getReferenceClass(), key);
  }
  
  public PriceShift load(String key, Session s) throws HibernateException
  {
    return (PriceShift)load(getReferenceClass(), key, s);
  }
  
  public PriceShift loadInitialize(String key, Session s) throws HibernateException
  {
    PriceShift obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<PriceShift> findAll()
  {
    return super.findAll();
  }
  


  public List<PriceShift> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<PriceShift> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(PriceShift priceShift)
    throws HibernateException
  {
    return (String)super.save(priceShift);
  }
  







  public String save(PriceShift priceShift, Session s)
    throws HibernateException
  {
    return (String)save(priceShift, s);
  }
  





  public void saveOrUpdate(PriceShift priceShift)
    throws HibernateException
  {
    saveOrUpdate(priceShift);
  }
  







  public void saveOrUpdate(PriceShift priceShift, Session s)
    throws HibernateException
  {
    saveOrUpdate(priceShift, s);
  }
  




  public void update(PriceShift priceShift)
    throws HibernateException
  {
    update(priceShift);
  }
  






  public void update(PriceShift priceShift, Session s)
    throws HibernateException
  {
    update(priceShift, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(PriceShift priceShift)
    throws HibernateException
  {
    delete(priceShift);
  }
  






  public void delete(PriceShift priceShift, Session s)
    throws HibernateException
  {
    delete(priceShift, s);
  }
  









  public void refresh(PriceShift priceShift, Session s)
    throws HibernateException
  {
    refresh(priceShift, s);
  }
}
