package com.floreantpos.model.dao;

import com.floreantpos.model.CashDropTransaction;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;





















public class CashDropTransactionDAO
  extends BaseCashDropTransactionDAO
{
  public CashDropTransactionDAO() {}
  
  public List<CashDropTransaction> findUnsettled(Terminal terminal, User cashier)
    throws Exception
  {
    Session session = null;
    try
    {
      session = createNewSession();
      Criteria criteria = session.createCriteria(getReferenceClass());
      criteria.add(Restrictions.eq(CashDropTransaction.PROP_DRAWER_RESETTED, Boolean.FALSE));
      if (cashier == null) {
        criteria.add(Restrictions.eq(CashDropTransaction.PROP_TERMINAL_ID, terminal == null ? null : terminal.getId()));
      }
      else
      {
        criteria.add(Restrictions.eq(CashDropTransaction.PROP_USER_ID, cashier.getId()));
      }
      

      return criteria.list();
    } catch (Exception e) {
      throw e;
    } finally {
      closeSession(session);
    }
  }
  
  public void deleteCashDrop(CashDropTransaction transaction, Terminal terminal) {
    Session session = null;
    Transaction tx = null;
    
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      session.delete(transaction);
      
      tx.commit();
    } catch (Exception e) {
      try {
        tx.rollback();
      }
      catch (Exception localException1) {}
    } finally {
      closeSession(session);
    }
  }
  
  public void saveNewCashDrop(CashDropTransaction transaction, Terminal terminal)
  {
    Session session = null;
    Transaction tx = null;
    
    try
    {
      session = createNewSession();
      tx = session.beginTransaction();
      
      session.save(transaction);
      
      tx.commit();
    } catch (Exception e) {
      try {
        tx.rollback();
      }
      catch (Exception localException1) {}
    } finally {
      closeSession(session);
    }
  }
}
