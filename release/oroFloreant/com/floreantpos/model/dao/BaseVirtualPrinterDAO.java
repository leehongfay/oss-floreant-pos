package com.floreantpos.model.dao;

import com.floreantpos.model.VirtualPrinter;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;




public abstract class BaseVirtualPrinterDAO
  extends _RootDAO
{
  public static VirtualPrinterDAO instance;
  
  public BaseVirtualPrinterDAO() {}
  
  public static VirtualPrinterDAO getInstance()
  {
    if (null == instance) instance = new VirtualPrinterDAO();
    return instance;
  }
  
  public Class getReferenceClass() {
    return VirtualPrinter.class;
  }
  
  public Order getDefaultOrder() {
    return Order.asc("name");
  }
  


  public VirtualPrinter cast(Object object)
  {
    return (VirtualPrinter)object;
  }
  
  public VirtualPrinter get(String key) throws HibernateException
  {
    return (VirtualPrinter)get(getReferenceClass(), key);
  }
  
  public VirtualPrinter get(String key, Session s) throws HibernateException
  {
    return (VirtualPrinter)get(getReferenceClass(), key, s);
  }
  
  public VirtualPrinter load(String key) throws HibernateException
  {
    return (VirtualPrinter)load(getReferenceClass(), key);
  }
  
  public VirtualPrinter load(String key, Session s) throws HibernateException
  {
    return (VirtualPrinter)load(getReferenceClass(), key, s);
  }
  
  public VirtualPrinter loadInitialize(String key, Session s) throws HibernateException
  {
    VirtualPrinter obj = load(key, s);
    if (!Hibernate.isInitialized(obj)) {
      Hibernate.initialize(obj);
    }
    return obj;
  }
  




  public List<VirtualPrinter> findAll()
  {
    return super.findAll();
  }
  


  public List<VirtualPrinter> findAll(Order defaultOrder)
  {
    return super.findAll(defaultOrder);
  }
  




  public List<VirtualPrinter> findAll(Session s, Order defaultOrder)
  {
    return super.findAll(s, defaultOrder);
  }
  





  public String save(VirtualPrinter virtualPrinter)
    throws HibernateException
  {
    return (String)super.save(virtualPrinter);
  }
  







  public String save(VirtualPrinter virtualPrinter, Session s)
    throws HibernateException
  {
    return (String)save(virtualPrinter, s);
  }
  





  public void saveOrUpdate(VirtualPrinter virtualPrinter)
    throws HibernateException
  {
    saveOrUpdate(virtualPrinter);
  }
  







  public void saveOrUpdate(VirtualPrinter virtualPrinter, Session s)
    throws HibernateException
  {
    saveOrUpdate(virtualPrinter, s);
  }
  




  public void update(VirtualPrinter virtualPrinter)
    throws HibernateException
  {
    update(virtualPrinter);
  }
  






  public void update(VirtualPrinter virtualPrinter, Session s)
    throws HibernateException
  {
    update(virtualPrinter, s);
  }
  




  public void delete(String id)
    throws HibernateException
  {
    delete(load(id));
  }
  






  public void delete(String id, Session s)
    throws HibernateException
  {
    delete(load(id, s), s);
  }
  




  public void delete(VirtualPrinter virtualPrinter)
    throws HibernateException
  {
    delete(virtualPrinter);
  }
  






  public void delete(VirtualPrinter virtualPrinter, Session s)
    throws HibernateException
  {
    delete(virtualPrinter, s);
  }
  









  public void refresh(VirtualPrinter virtualPrinter, Session s)
    throws HibernateException
  {
    refresh(virtualPrinter, s);
  }
}
