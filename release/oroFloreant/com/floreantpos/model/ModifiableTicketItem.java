package com.floreantpos.model;

import com.floreantpos.model.base.BaseModifiableTicketItem;



public class ModifiableTicketItem
  extends BaseModifiableTicketItem
{
  private static final long serialVersionUID = 1L;
  
  public ModifiableTicketItem() {}
  
  public ModifiableTicketItem(String id)
  {
    super(id);
  }
  

  public ModifiableTicketItem(Ticket ticket, String name, double unitPrice, double quantity, TicketItemTax tax)
  {
    setName(name);
    setUnitPrice(Double.valueOf(unitPrice));
    setQuantity(Double.valueOf(quantity));
    addTotaxes(tax);
    setTicket(ticket);
  }
}
