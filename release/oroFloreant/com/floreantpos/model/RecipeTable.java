package com.floreantpos.model;

import com.floreantpos.model.base.BaseRecipeTable;



public class RecipeTable
  extends BaseRecipeTable
{
  private static final long serialVersionUID = 1L;
  
  public RecipeTable() {}
  
  public RecipeTable(String id)
  {
    super(id);
  }
  

  public Double getPortion()
  {
    return Double.valueOf(1.0D);
  }
  
  public void setPortion(Double portion) {}
}
