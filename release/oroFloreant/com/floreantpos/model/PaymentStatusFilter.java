package com.floreantpos.model;

import com.floreantpos.Messages;
import org.apache.commons.lang.StringUtils;


















public enum PaymentStatusFilter
{
  OPEN(Messages.getString("PaymentStatusFilter.0")),  PAID(Messages.getString("PaymentStatusFilter.1")),  CLOSED(Messages.getString("PaymentStatusFilter.2"));
  
  private String displayString;
  
  private PaymentStatusFilter(String displayString) {
    this.displayString = displayString;
  }
  
  public static PaymentStatusFilter fromString(String s) {
    if (StringUtils.isEmpty(s)) {
      return OPEN;
    }
    try
    {
      return valueOf(s);
    }
    catch (Exception e) {}
    return OPEN;
  }
  
  public String toString()
  {
    return name().replaceAll("_", " ");
  }
  
  public String getDisplayString() {
    return displayString;
  }
}
