package com.floreantpos.model;

import com.floreantpos.model.base.BaseReportGroup;



public class ReportGroup
  extends BaseReportGroup
{
  private static final long serialVersionUID = 1L;
  
  public ReportGroup() {}
  
  public ReportGroup(String id)
  {
    super(id);
  }
  

  public String toString()
  {
    return super.getName();
  }
}
