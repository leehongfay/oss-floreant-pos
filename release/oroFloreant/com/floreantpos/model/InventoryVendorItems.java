package com.floreantpos.model;

import com.floreantpos.model.base.BaseInventoryVendorItems;



public class InventoryVendorItems
  extends BaseInventoryVendorItems
{
  private static final long serialVersionUID = 1L;
  
  public InventoryVendorItems() {}
  
  public InventoryVendorItems(String id)
  {
    super(id);
  }
  

  public String toString()
  {
    if (getVendor() != null) {
      return getVendor().getName();
    }
    return null;
  }
}
