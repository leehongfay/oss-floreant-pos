package com.floreantpos.model;

import com.floreantpos.model.base.BasePizzaPrice;





public class PizzaPrice
  extends BasePizzaPrice
{
  private static final long serialVersionUID = 1L;
  
  public PizzaPrice() {}
  
  public PizzaPrice(String id)
  {
    super(id);
  }
  
  public Double getPrice(int defaultSellPortion)
  {
    return Double.valueOf(super.getPrice().doubleValue() * defaultSellPortion / 100.0D);
  }
}
