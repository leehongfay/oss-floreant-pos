package com.floreantpos.model;

import com.floreantpos.model.base.BaseRefundTransaction;






















public class RefundTransaction
  extends BaseRefundTransaction
{
  private static final long serialVersionUID = 1L;
  
  public RefundTransaction() {}
  
  public RefundTransaction(String id)
  {
    super(id);
  }
  






  public RefundTransaction(String id, String transactionType, String paymentType)
  {
    super(id, transactionType, paymentType);
  }
}
