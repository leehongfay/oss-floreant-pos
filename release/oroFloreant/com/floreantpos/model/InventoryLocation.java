package com.floreantpos.model;

import com.floreantpos.model.base.BaseInventoryLocation;



















public class InventoryLocation
  extends BaseInventoryLocation
  implements IdContainer
{
  private static final long serialVersionUID = 1L;
  
  public InventoryLocation() {}
  
  public InventoryLocation(String id)
  {
    super(id);
  }
  





  public InventoryLocation(String id, String name)
  {
    super(id, name);
  }
  




  public String toString()
  {
    String name = getName();
    if (getParentLocation() != null) {
      name = getParentLocation().toString() + "->" + getName();
    }
    return name;
  }
}
