package com.floreantpos.model;

import com.floreantpos.config.CardConfig;


















public enum PaymentType
{
  CUSTOM_PAYMENT("CUSTOM PAYMENT"),  CASH("CASH"), 
  CREDIT_CARD("CREDIT CARD"), 
  DEBIT_CARD("DEBIT CARD"), 
  DEBIT_VISA("VISA", "visa_card.png"), 
  DEBIT_MASTER_CARD("MASTER CARD", "master_card.png"), 
  CREDIT_VISA("VISA", "visa_card.png"), 
  CREDIT_MASTER_CARD("MASTER CARD", "master_card.png"), 
  CREDIT_AMEX("AMEX", "am_ex_card.png"), 
  CREDIT_DISCOVERY("DISCOVER", "discover_card.png"), 
  GIFT_CERTIFICATE("GIFT CERTIFICATE"), 
  CUSTOMER_ACCOUNT("CUSTOMER ACCOUNT");
  
  private String displayString;
  private String imageFile;
  
  private PaymentType(String display)
  {
    displayString = display;
  }
  
  private PaymentType(String display, String image) {
    displayString = display;
    imageFile = image;
  }
  
  public String toString()
  {
    return displayString;
  }
  
  public String getDisplayString() {
    return displayString;
  }
  
  public void setDisplayString(String displayString) {
    this.displayString = displayString;
  }
  
  public String getImageFile() {
    return imageFile;
  }
  
  public void setImageFile(String imageFile) {
    this.imageFile = imageFile;
  }
  
  public boolean isSupported() {
    switch (1.$SwitchMap$com$floreantpos$model$PaymentType[ordinal()]) {
    case 1: 
      return true;
    }
    
    return (CardConfig.isSwipeCardSupported()) || (CardConfig.isManualEntrySupported()) || (CardConfig.isExtTerminalSupported());
  }
  
  public PosTransaction createTransaction()
  {
    PosTransaction transaction = null;
    switch (1.$SwitchMap$com$floreantpos$model$PaymentType[ordinal()]) {
    case 2: 
    case 3: 
    case 4: 
    case 5: 
    case 6: 
      transaction = new CreditCardTransaction();
      transaction.setAuthorizable(Boolean.valueOf(true));
      break;
    
    case 7: 
    case 8: 
      transaction = new DebitCardTransaction();
      transaction.setAuthorizable(Boolean.valueOf(true));
      break;
    
    case 9: 
      transaction = new GiftCertificateTransaction();
      break;
    
    case 10: 
      transaction = new CustomPaymentTransaction();
      break;
    
    case 11: 
      transaction = new CustomerAccountTransaction();
      break;
    
    default: 
      transaction = new CashTransaction();
    }
    
    
    transaction.setPaymentType(getDisplayString());
    return transaction;
  }
}
