package com.floreantpos.model;

import com.floreantpos.model.base.BaseTicketDiscount;
import com.floreantpos.model.ext.KitchenStatus;
import com.floreantpos.util.NumberUtil;
import org.json.JSONObject;



















public class TicketDiscount
  extends BaseTicketDiscount
  implements ITicketItem
{
  private static final long serialVersionUID = 1L;
  private String ticketId;
  
  public TicketDiscount() {}
  
  public TicketDiscount(String id)
  {
    super(id);
  }
  

  public TicketDiscount(String id, String name, double quantity, double value, int type, Ticket ticket)
  {
    setId(id);
    setDiscountId(id);
    setName(name);
    setCouponQuantity(Double.valueOf(quantity));
    setValue(Double.valueOf(value));
    setType(Integer.valueOf(type));
    setTicket(ticket);
    setTicketId(ticket.getId());
  }
  
  public Double getCouponQuantity()
  {
    Double couponQuantity = super.getCouponQuantity();
    if (couponQuantity.doubleValue() == 0.0D)
      return Double.valueOf(1.0D);
    return couponQuantity;
  }
  
  public String getNameDisplay() {
    double couponQuantity = getCouponQuantity().doubleValue();
    String display;
    String display; if (couponQuantity > 1.0D) {
      display = NumberUtil.trimDecilamIfNotNeeded(Double.valueOf(couponQuantity)) + "x " + getName();
    }
    else {
      display = getName().trim();
    }
    return display;
  }
  

  public String getItemCode()
  {
    return null;
  }
  
  public boolean canAddCookingInstruction()
  {
    return false;
  }
  
  public boolean canAddDiscount()
  {
    return false;
  }
  
  public boolean canVoid()
  {
    return false;
  }
  
  public boolean canAddAdOn()
  {
    return false;
  }
  
  public Boolean isPrintedToKitchen()
  {
    return Boolean.valueOf(false);
  }
  
  public Double getUnitPriceDisplay()
  {
    return null;
  }
  
  public String getItemQuantityDisplay()
  {
    return null;
  }
  
  public Double getTaxAmountWithoutModifiersDisplay()
  {
    return null;
  }
  
  public Double getTotalAmountWithoutModifiersDisplay()
  {
    return null;
  }
  
  public Double getSubTotalAmountDisplay()
  {
    return Double.valueOf(-getTotalDiscountAmount().doubleValue());
  }
  
  public Double getSubTotalAmountWithoutModifiersDisplay()
  {
    return null;
  }
  

  public void setDiscountAmount(Double amount) {}
  

  public Double getDiscountAmount()
  {
    return null;
  }
  
  public KitchenStatus getKitchenStatusValue()
  {
    return null;
  }
  
  public boolean isSaved()
  {
    return getId() != null;
  }
  
  public JSONObject toJson() {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put(PROP_DISCOUNT_ID, getDiscountId());
    jsonObject.put(PROP_NAME, getName());
    jsonObject.put(PROP_TYPE, getType());
    jsonObject.put(PROP_AUTO_APPLY, isAutoApply());
    jsonObject.put(PROP_COUPON_QUANTITY, getCouponQuantity());
    jsonObject.put(PROP_MINIMUM_AMOUNT, getMinimumAmount());
    jsonObject.put(PROP_VALUE, getValue());
    jsonObject.put(PROP_TOTAL_DISCOUNT_AMOUNT, getTotalDiscountAmount());
    
    return jsonObject;
  }
  
  public String getTicketId() {
    return ticketId;
  }
  
  public void setTicketId(String ticketId) {
    this.ticketId = ticketId;
  }
}
