package com.floreantpos.model;

import com.floreantpos.model.base.BaseMenuPage;
import com.floreantpos.model.dao.MenuPageDAO;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MenuPage
  extends BaseMenuPage
{
  private static final long serialVersionUID = 1L;
  public static final int MAX_SIZE = 16;
  private Map<String, MenuPageItem> cellItemMap;
  
  public MenuPage() {}
  
  public MenuPage(String id)
  {
    super(id);
  }
  
  public Integer getButtonWidth()
  {
    Integer buttonWidth = super.getButtonWidth();
    if (buttonWidth.intValue() < 30) {
      return Integer.valueOf(30);
    }
    return buttonWidth;
  }
  
  public Integer getButtonHeight()
  {
    Integer buttonHeight = super.getButtonHeight();
    if (buttonHeight.intValue() < 30) {
      return Integer.valueOf(30);
    }
    return buttonHeight;
  }
  
  public MenuPageItem getItemForCell(int col, int row) {
    MenuPageDAO.getInstance().initialize(this);
    List<MenuPageItem> pageItems = getPageItems();
    if ((pageItems == null) || (pageItems.size() == 0)) {
      return null;
    }
    if ((cellItemMap == null) || (cellItemMap.size() != pageItems.size())) {
      cellItemMap = new HashMap();
      for (MenuPageItem menuPageItem : pageItems) {
        String cellKey = String.valueOf(menuPageItem.getCol()) + String.valueOf(menuPageItem.getRow());
        cellItemMap.put(cellKey, menuPageItem);
      }
    }
    
    String cellKey = String.valueOf(col) + String.valueOf(row);
    return (MenuPageItem)cellItemMap.get(cellKey);
  }
  
  public String toString()
  {
    return super.getName();
  }
}
