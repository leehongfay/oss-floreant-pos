package com.floreantpos.model;

import com.floreantpos.swing.PaginatedListModel;
import com.floreantpos.swing.PaginationSupport;
import java.util.List;

public class Pagination extends PaginatedListModel implements PaginationSupport
{
  public Pagination(int currentRowIndex, int pageSize)
  {
    setCurrentRowIndex(currentRowIndex);
    setPageSize(pageSize);
  }
  
  public void setRows(List rows)
  {
    super.setData(rows);
  }
}
