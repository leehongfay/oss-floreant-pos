package com.floreantpos.model;

import com.floreantpos.model.base.BaseCustomPaymentTransaction;



public class CustomPaymentTransaction
  extends BaseCustomPaymentTransaction
{
  private static final long serialVersionUID = 1L;
  
  public CustomPaymentTransaction() {}
  
  public CustomPaymentTransaction(String id)
  {
    super(id);
  }
  






  public CustomPaymentTransaction(String id, String transactionType, String paymentType)
  {
    super(id, transactionType, paymentType);
  }
}
