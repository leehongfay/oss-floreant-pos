package com.floreantpos.model;

import com.floreantpos.IconFactory;
import com.floreantpos.model.base.BaseShopFloor;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.xml.bind.annotation.XmlTransient;




















public class ShopFloor
  extends BaseShopFloor
{
  private static final long serialVersionUID = 1L;
  private Color foregroundColor;
  private Color backgroundColor;
  
  public ShopFloor() {}
  
  public ShopFloor(String id)
  {
    super(id);
  }
  

  public ImageIcon getImage()
  {
    return IconFactory.getIconFromImageResource(getImageId());
  }
  
  public String toString()
  {
    return getName();
  }
  
  public boolean hasTableWithNumber(String number) {
    Set<ShopTable> tables = getTables();
    if (tables == null) {
      return false;
    }
    
    for (ShopTable shopTable : tables) {
      if (shopTable.getTableNumber().equals(number)) {
        return true;
      }
    }
    
    return false;
  }
  


  @XmlTransient
  public Color getForegroundColor()
  {
    if (getForegroundColorCode() == null) {
      return null;
    }
    if (foregroundColor != null) {
      return foregroundColor;
    }
    return this.foregroundColor = new Color(getForegroundColorCode().intValue());
  }
  
  public void setForegroundColor(Color foregroundColor) {
    this.foregroundColor = foregroundColor;
    if (foregroundColor != null) {
      setForegroundColorCode(Integer.valueOf(foregroundColor.getRGB()));
    }
  }
  
  @XmlTransient
  public Color getBackgroundColor() {
    if (getBackgroundColorCode() == null) {
      return null;
    }
    if (backgroundColor != null) {
      return backgroundColor;
    }
    return this.backgroundColor = new Color(getBackgroundColorCode().intValue());
  }
  
  public void setBackgroundColor(Color backgroundColor) {
    this.backgroundColor = backgroundColor;
    if (backgroundColor != null) {
      setBackgroundColorCode(Integer.valueOf(backgroundColor.getRGB()));
    }
  }
  
  @XmlTransient
  public Dimension getFloorSize() {
    Integer height = getHeight();
    Integer width = getWidth();
    if (width.intValue() <= 0) {
      width = Integer.valueOf(850);
    }
    if (height.intValue() <= 0) {
      height = Integer.valueOf(500);
    }
    return new Dimension(width.intValue(), height.intValue());
  }
}
