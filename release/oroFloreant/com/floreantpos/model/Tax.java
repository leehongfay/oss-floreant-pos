package com.floreantpos.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.floreantpos.model.base.BaseTax;
import javax.xml.bind.annotation.XmlRootElement;





















@JsonIgnoreProperties(ignoreUnknown=true, value={"enable"})
@XmlRootElement
public class Tax
  extends BaseTax
{
  private static final long serialVersionUID = 1L;
  private Boolean enable;
  
  public Tax() {}
  
  public Tax(String id)
  {
    super(id);
  }
  





  public Tax(String id, String name)
  {
    super(id, name);
  }
  



  public Tax(String id, String name, double rate)
  {
    super(id, name);
    setRate(Double.valueOf(rate));
  }
  
  public String getUniqueId() {
    return ("tax_" + getName() + "_" + getId()).replaceAll("\\s+", "_");
  }
  
  public String toString()
  {
    return getName() + " (" + getRate() + "%)";
  }
  
  public Boolean isEnable() {
    return Boolean.valueOf(enable == null ? false : enable.booleanValue());
  }
  
  public void setEnable(Boolean enable) {
    this.enable = enable;
  }
}
