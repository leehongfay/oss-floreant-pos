package com.floreantpos.model;

public enum TipsReceivedBy
{
  Driver,  Server,  Cashier;
  
  private TipsReceivedBy() {}
}
