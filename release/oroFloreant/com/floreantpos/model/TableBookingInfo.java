package com.floreantpos.model;

import com.floreantpos.model.base.BaseTableBookingInfo;
import java.util.Iterator;
import java.util.List;





















public class TableBookingInfo
  extends BaseTableBookingInfo
{
  private static final long serialVersionUID = 1L;
  public static final String STATUS_CANCEL = "cancel";
  public static final String STATUS_CLOSE = "close";
  public static final String STATUS_NO_APR = "no appear";
  public static final String STATUS_SEAT = "seat";
  public static final String STATUS_DELAY = "delay";
  public static final String STATUS_OPEN = "open";
  public static final String STATUS_CONFIRM = "confirm";
  private String customerInfo;
  private String bookedTableNumbers;
  
  public TableBookingInfo() {}
  
  public TableBookingInfo(String id)
  {
    super(id);
  }
  
  public String toString()
  {
    return getId().toString();
  }
  





  public String getCustomerInfo()
  {
    Customer customer = getCustomer();
    
    if (customer == null) {
      return customerInfo;
    }
    
    if (!customer.getFirstName().equals("")) {
      return this.customerInfo = customer.getFirstName();
    }
    
    if (!customer.getMobileNo().equals("")) {
      return this.customerInfo = customer.getMobileNo();
    }
    
    if (!customer.getLoyaltyNo().equals("")) {
      return this.customerInfo = customer.getLoyaltyNo();
    }
    
    return customerInfo;
  }
  


  public void setCustomerInfo(String customerInfo)
  {
    this.customerInfo = customerInfo;
  }
  


  public String getBookedTableNumbers()
  {
    if (bookedTableNumbers != null) {
      return bookedTableNumbers;
    }
    
    List<ShopTable> shopTables = getTables();
    if ((shopTables == null) || (shopTables.isEmpty())) {
      return null;
    }
    String tableNumbers = "";
    
    for (Iterator iterator = shopTables.iterator(); iterator.hasNext();) {
      ShopTable shopTable = (ShopTable)iterator.next();
      tableNumbers = tableNumbers + shopTable.getTableNumber();
      
      if (iterator.hasNext()) {
        tableNumbers = tableNumbers + ", ";
      }
    }
    
    return tableNumbers;
  }
  
  public void setBookedTableNumbers(String bookTableNumbers) {
    bookedTableNumbers = bookTableNumbers;
  }
}
