package com.floreantpos.model;

import com.floreantpos.model.base.BaseZipCodeVsDeliveryCharge;






















public class ZipCodeVsDeliveryCharge
  extends BaseZipCodeVsDeliveryCharge
{
  private static final long serialVersionUID = 1L;
  
  public ZipCodeVsDeliveryCharge() {}
  
  public ZipCodeVsDeliveryCharge(String id)
  {
    super(id);
  }
  






  public ZipCodeVsDeliveryCharge(String id, String zipCode, double deliveryCharge)
  {
    super(id, zipCode, deliveryCharge);
  }
}
