package com.floreantpos.model.ext;

import com.floreantpos.PosLog;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.util.DataProvider;

public enum PaperSize
{
  A4, 
  LETTER;
  
  private PaperSize() {}
  
  public static String getReportNameAccording2Size(String reportName) { String paperSize = DataProvider.get().getCurrentTerminal().getProperty("report.paper_size");
    if (LETTER.name().equalsIgnoreCase(paperSize)) {
      reportName = reportName + "-letter";
    }
    PosLog.debug(PaperSize.class, "report name: " + reportName);
    return reportName;
  }
}
