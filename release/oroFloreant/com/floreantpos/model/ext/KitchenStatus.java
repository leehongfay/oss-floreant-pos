package com.floreantpos.model.ext;

public enum KitchenStatus {
  BUMP, 
  WAITING, 
  NOT_SENT, 
  VOID, 
  DISPATCHED, 
  UNKNOWN;
  
  private KitchenStatus() {}
  
  public static KitchenStatus fromString(String str) { KitchenStatus[] values = values();
    for (KitchenStatus kitchenStatus : values) {
      if (kitchenStatus.name().equals(str)) {
        return kitchenStatus;
      }
    }
    return UNKNOWN;
  }
}
