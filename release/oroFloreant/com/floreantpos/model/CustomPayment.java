package com.floreantpos.model;

import com.floreantpos.model.base.BaseCustomPayment;



public class CustomPayment
  extends BaseCustomPayment
{
  private static final long serialVersionUID = 1L;
  
  public CustomPayment() {}
  
  public CustomPayment(String id)
  {
    super(id);
  }
}
