package com.floreantpos.model;

public abstract interface IUnit
{
  public abstract String getUniqueCode();
}
