package com.floreantpos.model;

import com.floreantpos.model.base.BaseInventoryVendor;





















public class InventoryVendor
  extends BaseInventoryVendor
  implements IdContainer
{
  private static final long serialVersionUID = 1L;
  
  public InventoryVendor() {}
  
  public InventoryVendor(String id)
  {
    super(id);
  }
  





  public InventoryVendor(String id, String name)
  {
    super(id, name);
  }
  




  public Boolean isVisible()
  {
    return Boolean.valueOf(visible == null ? true : visible.booleanValue());
  }
  
  public String toString()
  {
    return getName();
  }
}
