package com.floreantpos.model;

import com.floreantpos.model.base.BaseDeliveryAddress;
import com.floreantpos.model.dao.CustomerDAO;
import org.apache.commons.lang.StringUtils;






public class DeliveryAddress
  extends BaseDeliveryAddress
{
  private static final long serialVersionUID = 1L;
  
  public DeliveryAddress() {}
  
  public DeliveryAddress(String id)
  {
    super(id);
  }
  

  public String toString()
  {
    return super.getAddress();
  }
  
  public Customer getCustomer() {
    if (StringUtils.isNotEmpty(getCustomerId())) {
      return CustomerDAO.getInstance().get(getCustomerId());
    }
    return null;
  }
  
  public void setCustomer(Customer customer) {
    String customerId = null;
    if (customer != null) {
      customerId = customer.getId();
    }
    super.setCustomerId(customerId);
  }
}
