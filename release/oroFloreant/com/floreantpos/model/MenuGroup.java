package com.floreantpos.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.floreantpos.main.Application;
import com.floreantpos.model.base.BaseMenuGroup;
import com.floreantpos.model.dao.MenuCategoryDAO;
import java.awt.Color;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.lang.StringUtils;






















@JsonIgnoreProperties(ignoreUnknown=true, value={"buttonColor", "textColor", "discounts", "menuPages"})
@XmlRootElement(name="menu-group")
public class MenuGroup
  extends BaseMenuGroup
  implements IdContainer
{
  private static final long serialVersionUID = 1L;
  private transient boolean beverageCategory;
  private transient String categoryName;
  private Color buttonColor;
  private Color textColor;
  
  public MenuGroup() {}
  
  public MenuGroup(String id)
  {
    super(id);
  }
  



  public MenuGroup(String id, String name)
  {
    super(id, name);
  }
  





  public Integer getSortOrder()
  {
    return Integer.valueOf(sortOrder == null ? 9999 : sortOrder.intValue());
  }
  
  @XmlTransient
  public Color getButtonColor() {
    if (buttonColor != null) {
      return buttonColor;
    }
    
    if ((getButtonColorCode() == null) || (getButtonColorCode().intValue() == 0)) {
      return null;
    }
    
    return this.buttonColor = new Color(getButtonColorCode().intValue());
  }
  
  public void setButtonColor(Color buttonColor) {
    this.buttonColor = buttonColor;
  }
  
  @XmlTransient
  public Color getTextColor() {
    if (textColor != null) {
      return textColor;
    }
    
    if (getTextColorCode() == null) {
      return null;
    }
    
    return this.textColor = new Color(getTextColorCode().intValue());
  }
  
  public void setTextColor(Color textColor) {
    this.textColor = textColor;
  }
  
  @JsonIgnore
  public String getDisplayName() {
    Terminal terminal = Application.getInstance().getTerminal();
    if ((terminal != null) && (terminal.isTranslatedName().booleanValue()) && (StringUtils.isNotEmpty(getTranslatedName()))) {
      return getTranslatedName();
    }
    
    return super.getName();
  }
  
  public void setMenuCategory(MenuCategory category) {
    if (category == null) {
      setMenuCategoryId(null);
      setMenuCategoryName(null);
      setBeverage(Boolean.valueOf(false));
    }
    else {
      setMenuCategoryId(category.getId());
      setMenuCategoryName(category.getDisplayName());
      setBeverage(category.isBeverage());
    }
  }
  
  public void setParent(MenuCategory category) {
    setMenuCategory(category);
  }
  
  public MenuCategory getParent() {
    String categoryId = getMenuCategoryId();
    if (StringUtils.isNotEmpty(categoryId)) {
      return MenuCategoryDAO.getInstance().get(categoryId);
    }
    return null;
  }
  
  public String toString()
  {
    return getName();
  }
  
  @JsonIgnore
  public String getUniqueId() {
    return ("menu_group_" + getName() + "_" + getId()).replaceAll("\\s+", "_");
  }
  
  public boolean isBeverageCategory() {
    return beverageCategory;
  }
  
  public void setBeverageCategory(boolean beverageCategory) {
    this.beverageCategory = beverageCategory;
  }
  
  public String getCategoryName() {
    return categoryName;
  }
  
  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }
}
