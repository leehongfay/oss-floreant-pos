package com.floreantpos.model;

import com.floreantpos.model.base.BaseInventoryClosingBalance;



public class InventoryClosingBalance
  extends BaseInventoryClosingBalance
{
  private static final long serialVersionUID = 1L;
  
  public InventoryClosingBalance() {}
  
  public InventoryClosingBalance(String id)
  {
    super(id);
  }
}
