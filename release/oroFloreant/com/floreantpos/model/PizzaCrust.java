package com.floreantpos.model;

import com.floreantpos.model.base.BasePizzaCrust;
import org.apache.commons.lang.StringUtils;






public class PizzaCrust
  extends BasePizzaCrust
{
  private static final long serialVersionUID = 1L;
  
  public PizzaCrust() {}
  
  public PizzaCrust(String id)
  {
    super(id);
  }
  


  public String getTranslatedName()
  {
    String translatedName = super.getTranslatedName();
    if (StringUtils.isEmpty(translatedName)) {
      return getName();
    }
    
    return translatedName;
  }
  
  public String toString()
  {
    return getName();
  }
}
