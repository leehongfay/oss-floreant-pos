package com.floreantpos.model;

import com.floreantpos.model.base.BaseDefaultMenuModifier;



public class DefaultMenuModifier
  extends BaseDefaultMenuModifier
{
  private static final long serialVersionUID = 1L;
  private Boolean enable;
  
  public DefaultMenuModifier() {}
  
  public void setEnable(Boolean enable)
  {
    this.enable = enable;
  }
  
  public Boolean isEnable() {
    return Boolean.valueOf(enable == null ? false : enable.booleanValue());
  }
  
  public String getId() {
    MenuModifier modifier = getModifier();
    if (modifier == null)
      return "";
    return modifier.getId();
  }
}
