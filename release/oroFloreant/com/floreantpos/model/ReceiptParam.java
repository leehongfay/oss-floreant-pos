package com.floreantpos.model;

public enum ReceiptParam {
  STORE_NAME("storeName"), 
  STORE_LOGO("storeLogo"), 
  STORE_ADDRESS1("storeAddress1"), 
  STORE_ADDRESS2("storeAddress2"), 
  STORE_ADDRESS3("storeAddress3"), 
  STORE_PHONE_NO("storePhoneNo"), 
  RECEIPT_TYPE("receiptType"), 
  TICKET_ID("ticketId"), 
  TICKET_SHORT_ID("ticketShortId"), 
  SPLIT_TICKET_ID("splitTicketId"), 
  TOKEN_NO("tokenNo"), 
  ORDER_DATE("orderDate"), 
  CURRENCY_SYMBOL("currencySymbol"), 
  ORDER_TYPE("orderType"), 
  TERMINAL_NAME("terminalName"), 
  TERMINAL_ID("terminalId"), 
  SERVER_NAME("serverName"), 
  SERVER_ID("serverId"), 
  GUEST_COUNT("guestCount"), 
  TABLE_NO("tableNo"), 
  BARCODE("barcode"), 
  CUSTOMER_NAME("customerName"), 
  CUSTOMER_ID("customerId"), 
  CUSTOMER_PHONE("customerPhone"), 
  DELIVERY_ADDRESS("deliveryAddress"), 
  ONLINE_ORDER_ID("onlineOrderId"), 
  DRIVER_NAME("driverName"), 
  DRIVER_ID("driverId"), 
  PRINT_DATE("printDate"), 
  DELIVERY_DATE("deliveryDate"), 
  CUSTOMER_NO("customerNo"), 
  CUSTOMER_SIGNATURE("customerSignature"), 
  SALES_AREA("salesArea"), 
  PAYMENT_TYPE("paymentType");
  

  private String paramName;
  
  private ReceiptParam(String paramName)
  {
    this.paramName = paramName;
  }
  
  public String getParamName() {
    return paramName;
  }
  
  public String toString()
  {
    return getParamName();
  }
}
