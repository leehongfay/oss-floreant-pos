package com.floreantpos.model;

import com.floreantpos.model.base.BaseGuestCheckPrint;
import com.floreantpos.model.util.DateUtil;
import java.util.Date;




public class GuestCheckPrint
  extends BaseGuestCheckPrint
{
  private static final long serialVersionUID = 1L;
  private String diffInBillPrint;
  
  public GuestCheckPrint() {}
  
  public GuestCheckPrint(String id)
  {
    super(id);
  }
  
  public String getDiffInBillPrint()
  {
    return DateUtil.getElapsedTime(getPrintTime(), new Date());
  }
  
  public void setDiffInBillPrint(String diffInBillPrint) {
    this.diffInBillPrint = diffInBillPrint;
  }
}
