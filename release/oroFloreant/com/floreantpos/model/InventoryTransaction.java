package com.floreantpos.model;

import com.floreantpos.model.base.BaseInventoryTransaction;
import com.floreantpos.model.dao.InventoryVendorDAO;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.util.NumberUtil;
import java.text.SimpleDateFormat;





public class InventoryTransaction
  extends BaseInventoryTransaction
{
  private static final long serialVersionUID = 1L;
  public static final String PROPERTY_VENDOR = "vendor";
  public static final String PROPERTY_FROM_LOC = "fromInventoryLocation";
  public static final String PROPERTY_TO_LOC = "toInventoryLocation";
  public static final String REASON_NEW_STOCK = "NEW STOCK";
  public static final String REASON_RETURN = "RETURN";
  public static final String REASON_PURCHASE = "PURCHASE";
  public static final String REASON_DAMAGED = "DAMAGED";
  public static final String REASON_TICKET_SALES = "TICKET SALES";
  public static final String REASON_TRANSFER = "TRANSFER";
  public static final String REASON_ADJUST = "ADJUST";
  public static final String REASON_ADJUST_IN = "ADJUST_IN";
  public static final String REASON_ADJUST_OUT = "ADJUST_OUT";
  public static final String REASON_PREPARE_IN = "PREPARE IN";
  public static final String REASON_PREPARE_OUT = "PREPARE OUT";
  public static final String REASON_VOID = "VOID";
  public static final String VENDOR_RETURN = "VENDOR RETURN";
  
  public InventoryTransaction() {}
  
  public InventoryTransaction(String id)
  {
    super(id);
  }
  





















  public static final String[] REASON_IN = { "NEW STOCK", "RETURN", "PURCHASE", "ADJUST_IN", "PREPARE IN" };
  public static final String[] REASON_OUT = { "TICKET SALES", "DAMAGED", "ADJUST_OUT", "PREPARE OUT", "VENDOR RETURN" };
  public static final String[] REASON_TRANS = { "TRANSFER" };
  
  public static InventoryTransactionType transactionType;
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, hh:mm a");
  
  private double openingQty;
  private double openingCost;
  private double openingTotalCost;
  private transient InventoryLocation inventoryToLocation;
  private transient InventoryLocation inventoryFromLocation;
  
  public InventoryTransactionType getTransactionType()
  {
    return InventoryTransaction.transactionType = InventoryTransactionType.fromInt(super.getType().intValue());
  }
  
  public void setTransactionType(InventoryTransactionType type) {
    super.setType(Integer.valueOf(type.getType()));
  }
  
  public String getTransactionDateAsString() {
    if (super.getTransactionDate() == null)
      return "";
    return dateFormat.format(super.getTransactionDate());
  }
  
  public void setTransactionDateAsString(String transactionDateAsString) {}
  
  public double getOpeningQty()
  {
    return openingQty;
  }
  
  public void setOpeningQty(double openingQty) {
    this.openingQty = openingQty;
  }
  
  public double getOpeningCost() {
    return openingCost;
  }
  
  public void setOpeningCost(double openingCost) {
    this.openingCost = openingCost;
  }
  
  public double getOpeningTotalCost() {
    return openingTotalCost;
  }
  
  public void setOpeningTotalCost(double openingTotalCost) {
    this.openingTotalCost = openingTotalCost;
  }
  

  public void setUnitCostDisplay(String s) {}
  

  public String getUnitCostDisplay()
  {
    return NumberUtil.getCurrencyFormat(getUnitCost());
  }
  

  public void setUnitPriceDisplay(String s) {}
  

  public String getUnitPriceDisplay()
  {
    return NumberUtil.getCurrencyFormat(getUnitPrice());
  }
  
  public String getSku() {
    return getMenuItem().getSku();
  }
  
  public void setSku(String sku) {}
  
  public String getItemName()
  {
    return getMenuItem().getName();
  }
  
  public void setItemName(String itemName) {}
  
  public void setToInventoryLocation(InventoryLocation location)
  {
    inventoryToLocation = location;
    setToLocationId(location == null ? null : location.getId());
  }
  
  public InventoryLocation getToInventoryLocation() {
    if (inventoryToLocation != null) {
      return inventoryToLocation;
    }
    return DataProvider.get().getInventoryLocationById(getToLocationId());
  }
  
  public void setFromInventoryLocation(InventoryLocation location) {
    inventoryFromLocation = location;
    setFromLocationId(location == null ? null : location.getId());
  }
  
  public InventoryLocation getFromInventoryLocation() {
    if (inventoryFromLocation != null) {
      return inventoryFromLocation;
    }
    return DataProvider.get().getInventoryLocationById(getFromLocationId());
  }
  
  public void setUser(User user) {
    setUserId(user == null ? null : user.getId());
  }
  
  public void setVendor(InventoryVendor vendor) {
    setVendorId(vendor == null ? null : vendor.getId());
  }
  
  public InventoryVendor getVendor() {
    if (getVendorId() == null) {
      return null;
    }
    
    return InventoryVendorDAO.getInstance().get(getVendorId());
  }
}
