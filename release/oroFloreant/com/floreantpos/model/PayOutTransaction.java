package com.floreantpos.model;

import com.floreantpos.model.base.BasePayOutTransaction;
import com.floreantpos.model.dao.PayoutReasonDAO;
import com.floreantpos.model.dao.PayoutRecepientDAO;
import org.apache.commons.lang.StringUtils;



















public class PayOutTransaction
  extends BasePayOutTransaction
{
  private static final long serialVersionUID = 1L;
  private PayoutReason reason;
  private PayoutRecepient recepient;
  
  public PayOutTransaction() {}
  
  public PayOutTransaction(String id)
  {
    super(id);
  }
  






  public PayOutTransaction(String id, String transactionType, String paymentType)
  {
    super(id, transactionType, paymentType);
  }
  





  public String getTransactionType()
  {
    String type = super.getTransactionType();
    
    if (StringUtils.isEmpty(type)) {
      return TransactionType.DEBIT.name();
    }
    
    return type;
  }
  


  public PayoutReason getReason()
  {
    String payoutReason = getReasonId();
    if ((reason != null) && (reason.getId().equals(payoutReason)))
      return reason;
    if (StringUtils.isNotEmpty(payoutReason)) {
      reason = PayoutReasonDAO.getInstance().get(payoutReason);
    }
    return reason;
  }
  
  public void setReason(PayoutReason payoutReason) {
    reason = payoutReason;
    String payoutReasonId = null;
    if (payoutReason != null) {
      payoutReasonId = payoutReason.getId();
    }
    super.setReasonId(payoutReasonId);
  }
  
  public PayoutRecepient getRecepient() {
    String payoutPayoutRecepient = getRecepientId();
    if ((recepient != null) && (recepient.getId().equals(payoutPayoutRecepient)))
      return recepient;
    if (StringUtils.isNotEmpty(payoutPayoutRecepient)) {
      recepient = PayoutRecepientDAO.getInstance().get(payoutPayoutRecepient);
    }
    return recepient;
  }
  
  public void setRecepient(PayoutRecepient payoutPayoutRecepient) {
    recepient = payoutPayoutRecepient;
    String recepientId = null;
    if (payoutPayoutRecepient != null) {
      recepientId = payoutPayoutRecepient.getId();
    }
    super.setRecepientId(recepientId);
  }
}
