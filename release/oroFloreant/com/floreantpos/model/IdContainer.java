package com.floreantpos.model;

public abstract interface IdContainer
{
  public abstract String getId();
}
