package com.floreantpos.model;

import com.floreantpos.model.base.BaseStoreSessionControl;
import javax.xml.bind.annotation.XmlRootElement;





@XmlRootElement
public class StoreSessionControl
  extends BaseStoreSessionControl
{
  private static final long serialVersionUID = 1L;
  
  public StoreSessionControl() {}
  
  public StoreSessionControl(String id)
  {
    super(id);
  }
}
