package com.floreantpos.model;

import com.floreantpos.model.base.BasePrinterConfiguration;



















public class PrinterConfiguration
  extends BasePrinterConfiguration
{
  private static final long serialVersionUID = 1L;
  public static final String ID = "1";
  
  public PrinterConfiguration() {}
  
  public PrinterConfiguration(String id)
  {
    super(id);
  }
  




  public String getReceiptPrinterName()
  {
    if (super.getReceiptPrinterName() == null) {
      return "";
    }
    return super.getReceiptPrinterName();
  }
  
  public String getKitchenPrinterName()
  {
    if (super.getKitchenPrinterName() == null) {
      return "";
    }
    return super.getKitchenPrinterName();
  }
}
