package com.floreantpos.model;

import com.floreantpos.model.ext.KitchenStatus;

public abstract interface ITicketItem
{
  public abstract boolean isSaved();
  
  public abstract String getItemCode();
  
  public abstract boolean canAddCookingInstruction();
  
  public abstract boolean canAddDiscount();
  
  public abstract boolean canVoid();
  
  public abstract boolean canAddAdOn();
  
  public abstract Boolean isPrintedToKitchen();
  
  public abstract String getNameDisplay();
  
  public abstract Double getUnitPriceDisplay();
  
  public abstract String getItemQuantityDisplay();
  
  public abstract Double getTaxAmountWithoutModifiersDisplay();
  
  public abstract Double getTotalAmountWithoutModifiersDisplay();
  
  public abstract Double getSubTotalAmountDisplay();
  
  public abstract Double getSubTotalAmountWithoutModifiersDisplay();
  
  public abstract void setDiscountAmount(Double paramDouble);
  
  public abstract Double getDiscountAmount();
  
  public abstract KitchenStatus getKitchenStatusValue();
}
