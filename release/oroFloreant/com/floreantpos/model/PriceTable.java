package com.floreantpos.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.floreantpos.model.base.BasePriceTable;
import java.util.List;




@JsonIgnoreProperties(ignoreUnknown=true, value={"lastUpdatedBy"})
public class PriceTable
  extends BasePriceTable
{
  private static final long serialVersionUID = 1L;
  private List<PriceTableItem> priceTableItems;
  
  public PriceTable() {}
  
  public PriceTable(String id)
  {
    super(id);
  }
  


  public String toString()
  {
    return super.getName();
  }
  
  public void setPriceTableItems(List<PriceTableItem> items) {
    priceTableItems = items;
  }
  
  public List<PriceTableItem> getPriceTableItems() {
    return priceTableItems;
  }
}
