package com.floreantpos.model;

import com.floreantpos.model.base.BaseTerminal;
import com.floreantpos.model.dao.DepartmentDAO;
import com.floreantpos.model.dao.OutletDAO;
import com.floreantpos.model.dao.SalesAreaDAO;
import com.floreantpos.model.util.DataProvider;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;





















@XmlRootElement(name="terminal")
public class Terminal
  extends BaseTerminal
{
  private static final long serialVersionUID = 1L;
  
  public Terminal() {}
  
  public Terminal(Integer id)
  {
    super(id);
  }
  





  public Boolean isHasCashDrawer()
  {
    return hasCashDrawer == null ? Boolean.TRUE : hasCashDrawer;
  }
  
  public User getAssignedUser() {
    return DataProvider.get().getUserById(getAssignedUserId());
  }
  



  public void setAssignedUser(User assignedUser)
  {
    setAssignedUserId(assignedUser == null ? null : assignedUser.getId());
  }
  


  public Outlet getOutlet()
  {
    if (getOutletId() == null) {
      return null;
    }
    return OutletDAO.getInstance().get(getOutletId());
  }
  



  public void setOutlet(Outlet outlet)
  {
    setOutletId(outlet == null ? null : outlet.getId());
  }
  


  public Department getDepartment()
  {
    if (getDepartmentId() == null) {
      return null;
    }
    return DepartmentDAO.getInstance().get(getDepartmentId());
  }
  



  public void setDepartment(Department department)
  {
    setDepartmentId(department == null ? null : department.getId());
  }
  


  public SalesArea getSalesArea()
  {
    if (getSalesAreaId() == null) {
      return null;
    }
    return SalesAreaDAO.getInstance().get(getSalesAreaId());
  }
  



  public void setSalesArea(SalesArea salesArea)
  {
    setSalesAreaId(salesArea == null ? null : salesArea.getId());
  }
  


  public TerminalType getTerminalType()
  {
    return DataProvider.get().getTerminalType(getTerminalTypeId());
  }
  



  public void setTerminalType(TerminalType terminalType)
  {
    setTerminalTypeId(terminalType == null ? null : terminalType.getId());
  }
  
  public boolean isCashDrawerAssigned() {
    return getAssignedUser() != null;
  }
  
  public String getProperty(String key) {
    if (getProperties() == null) {
      return null;
    }
    return (String)getProperties().get(key);
  }
  
  public void addProperty(String key, String value) {
    getProperties().put(key, value);
  }
  
  public String toString()
  {
    return getName();
  }
  
  public boolean isShowTableNumber() {
    return getProperty("floorplan.showTableNumber") == null ? true : 
      Boolean.valueOf(getProperty("floorplan.showTableNumber")).booleanValue();
  }
  
  public boolean isShowServerName()
  {
    return getProperty("floorplan.showServerName") == null ? true : 
      Boolean.valueOf(getProperty("floorplan.showServerName")).booleanValue();
  }
  
  public boolean isShowTokenNum()
  {
    return getProperty("floorplan.showTokenName") == null ? true : 
      Boolean.valueOf(getProperty("floorplan.showTokenName")).booleanValue();
  }
}
