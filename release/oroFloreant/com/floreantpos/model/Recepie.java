package com.floreantpos.model;

import com.floreantpos.model.base.BaseRecepie;
import com.floreantpos.model.dao.RecipeTableDAO;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.util.NumberUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

















public class Recepie
  extends BaseRecepie
  implements IdContainer
{
  private static final long serialVersionUID = 1L;
  private Double cost;
  private Boolean visible;
  private Double cookingYield;
  
  public Recepie() {}
  
  public Recepie(String id)
  {
    super(id);
  }
  




  public void addRecepieItem(RecepieItem recepieItem)
  {
    List<RecepieItem> recepieItems = getRecepieItems();
    if (recepieItems == null) {
      recepieItems = new ArrayList(3);
      setRecepieItems(recepieItems);
    }
    

    recepieItems.add(recepieItem);
  }
  
  public String toString()
  {
    return super.getName();
  }
  
  public Double getCost() {
    return Double.valueOf(cost == null ? 0.0D : cost.doubleValue());
  }
  
  public void setCost(Double cost) {
    this.cost = cost;
  }
  
  public void calculateCost() {
    double portionCost = 0.0D;
    double recipeCost = 0.0D;
    List<RecepieItem> items = getRecepieItems();
    if ((items != null) && (items.size() > 0)) {
      for (RecepieItem recepieItem : items) {
        recepieItem.calculatePercentage();
        recipeCost += recepieItem.getCost();
      }
    }
    portionCost = recipeCost * getPortion().doubleValue() / getYield().doubleValue();
    setCost(Double.valueOf(portionCost));
  }
  
  public Double getPortion()
  {
    return Double.valueOf(super.getPortion().doubleValue() == 0.0D ? 1.0D : super.getPortion().doubleValue());
  }
  
  public Double getYield()
  {
    return Double.valueOf(super.getYield().doubleValue() == 0.0D ? 1.0D : super.getYield().doubleValue());
  }
  
  public void setVisible(Boolean visible) {
    this.visible = visible;
  }
  
  public Boolean isVisible() {
    return Boolean.valueOf(visible == null ? false : visible.booleanValue());
  }
  

  public void populateRecipeItemQuantity(HashMap<String, Double> recepieItemMap, double itemQuantity)
  {
    List<RecepieItem> recepieItems = getRecepieItems();
    if ((recepieItems == null) || (recepieItems.size() == 0)) {
      return;
    }
    for (RecepieItem recepieItem : recepieItems) {
      MenuItem inventoryItem = recepieItem.getInventoryItem();
      if (inventoryItem != null)
      {

        Double previousValue = (Double)recepieItemMap.get(inventoryItem.getId());
        if (previousValue == null) {
          previousValue = Double.valueOf(0.0D);
        }
        Double percentage = Double.valueOf(recepieItem.getPercentage().doubleValue() / 100.0D);
        Double toBeAdjustQty = Double.valueOf(NumberUtil.roundToTwoDigit(previousValue.doubleValue() + itemQuantity * percentage.doubleValue()));
        recepieItemMap.put(inventoryItem.getId(), toBeAdjustQty);
      }
    }
  }
  
  public Double getCookingYield() { return Double.valueOf(cookingYield == null ? 1.0D : cookingYield.doubleValue()); }
  
  public void setCookingYield(Double cookingYield)
  {
    this.cookingYield = cookingYield;
  }
  
  public String getMenuItemName() {
    return DataProvider.get().getRecipeMenuItemName(this);
  }
  


  public void setMenuItemName(String menuItemName) {}
  


  public void populateRecipeItems(HashMap<String, RecepieItem> recepieItemMap, double itemQuantity, Recepie recipe, boolean group)
  {
    List<RecepieItem> recepieItems = getRecepieItems();
    if ((recepieItems == null) || (recepieItems.size() == 0)) {
      return;
    }
    for (RecepieItem recepieItem : recepieItems) {
      MenuItem inventoryItem = recepieItem.getInventoryItem();
      if (inventoryItem != null)
      {

        Double percentage = Double.valueOf(0.0D);
        String key = inventoryItem.getId();
        if (!group) {
          key = recipe.getId() + "-" + key;
        }
        RecepieItem item = (RecepieItem)recepieItemMap.get(key);
        RecipeTable recipeTable = RecipeTableDAO.getInstance().findBy(key);
        if (recipeTable == null) {
          if (item == null) {
            item = new RecepieItem();
            item.setRecepie(recipe);
            item.setInventoryItem(recepieItem.getInventoryItem());
            item.setUnit(recepieItem.getUnit());
            item.setUnitCode(recepieItem.getUnitCode());
            if (!group) {
              recepieItemMap.put(recipe.getId() + "-" + key, item);
            }
            else {
              recepieItemMap.put(key, item);
            }
          }
          item.setGroupName(recipe.getName());
          item.setGroupId(recipe.getId());
          percentage = Double.valueOf(recepieItem.getPercentage().doubleValue() / 100.0D);
          Double toBeAdjustQty = Double.valueOf(NumberUtil.roundToTwoDigit(item.getQuantity().doubleValue() + itemQuantity * percentage.doubleValue()));
          item.setQuantity(toBeAdjustQty);
        }
      }
    }
  }
  
  public int getCookingMin() { int timeInSeconds = getCookingTime().intValue();
    int hours = timeInSeconds / 3600;
    int secondsLeft = timeInSeconds - hours * 3600;
    int minutes = secondsLeft / 60;
    return minutes;
  }
}
