package com.floreantpos.model;

public enum TransactionType
{
  DEBIT,  CREDIT;
  
  private TransactionType() {}
}
