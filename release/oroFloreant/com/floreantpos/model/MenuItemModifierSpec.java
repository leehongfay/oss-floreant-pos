package com.floreantpos.model;

import com.floreantpos.model.base.BaseMenuItemModifierSpec;
import com.floreantpos.model.dao.ModifierGroupDAO;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang.StringUtils;






















@XmlRootElement(name="menuItemModifierSpec")
public class MenuItemModifierSpec
  extends BaseMenuItemModifierSpec
{
  private static final long serialVersionUID = 1L;
  private ModifierGroup modifierGroup;
  
  public MenuItemModifierSpec() {}
  
  public MenuItemModifierSpec(String id)
  {
    super(id);
  }
  




  public String toString()
  {
    return getName();
  }
  
  public String getUniqueId() {
    return ("menuitem_modifiergroup_" + toString() + "_" + getId()).replaceAll("\\s+", "_");
  }
  
  public Set<MenuModifier> getModifiers() {
    Set<MenuItemModifierPage> modifierPages = getModifierPages();
    Set<MenuModifier> menuModifiers = new HashSet();
    if (modifierPages != null) {
      for (MenuItemModifierPage page : modifierPages) {
        if (page.getPageItems() != null) {
          for (MenuItemModifierPageItem pageItem : page.getPageItems()) {
            MenuModifier modifier = pageItem.getMenuModifier();
            if (modifier != null) {
              modifier.setPageItemId(pageItem.getId());
              menuModifiers.add(modifier);
            }
          }
        }
      }
    }
    return menuModifiers;
  }
  
  public boolean hasMenuItemModifierPageItem() {
    Set<MenuItemModifierPage> modifierPages = getModifierPages();
    if ((modifierPages == null) || (modifierPages.isEmpty())) {
      return false;
    }
    for (MenuItemModifierPage menuItemModifierPage : modifierPages) {
      List<MenuItemModifierPageItem> pageItems = menuItemModifierPage.getPageItems();
      if ((pageItems != null) || (!pageItems.isEmpty())) {
        return true;
      }
    }
    return false;
  }
  
  public String getDisplayName() {
    return super.getName();
  }
  
  public Set<MenuModifier> getDefaultModifiers() {
    Set<MenuModifier> defaultModifiers = new HashSet();
    List<DefaultMenuModifier> defaultModifierList = super.getDefaultModifierList();
    if (defaultModifierList != null) {
      for (DefaultMenuModifier defaultMenuModifier : defaultModifierList) {
        defaultModifiers.add(defaultMenuModifier.getModifier());
      }
    }
    return defaultModifiers;
  }
  
  public ModifierGroup getModifierGroup() {
    if (modifierGroup != null)
      return modifierGroup;
    String modifierGroupId = super.getModifierGroupId();
    if (StringUtils.isEmpty(modifierGroupId))
      return null;
    return this.modifierGroup = ModifierGroupDAO.getInstance().get(modifierGroupId);
  }
  
  public void setModifierGroup(ModifierGroup modifierGroup) {
    this.modifierGroup = modifierGroup;
    String modifierGroupId = null;
    if (modifierGroup != null) {
      modifierGroupId = modifierGroup.getId();
    }
    setModifierGroupId(modifierGroupId);
  }
}
