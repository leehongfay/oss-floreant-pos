package com.floreantpos.model;

import com.floreantpos.PosLog;
import com.floreantpos.model.base.BaseImageResource;
import java.awt.Image;
import java.sql.Blob;
import java.sql.SQLException;
import javax.swing.ImageIcon;






public class ImageResource
  extends BaseImageResource
{
  private static final long serialVersionUID = 1L;
  private Image image;
  
  public ImageResource() {}
  
  public ImageResource(String id)
  {
    super(id);
  }
  
  public static enum IMAGE_CATEGORY
  {
    UNLISTED(0),  FLOORPLAN(1),  PRODUCTS(2),  PEOPLE(3),  DELETED(4);
    
    private int type;
    
    private IMAGE_CATEGORY(int type) { this.type = type; }
    
    public int getType()
    {
      return type;
    }
    
    public void setType(int type) {
      this.type = type;
    }
    
    public static IMAGE_CATEGORY fromInt(int type) {
      IMAGE_CATEGORY[] values = values();
      
      for (IMAGE_CATEGORY cat : values) {
        if (type == type) {
          return cat;
        }
      }
      
      return UNLISTED;
    }
  }
  
  public IMAGE_CATEGORY getImageCategory() {
    return IMAGE_CATEGORY.fromInt(super.getImageCategoryNum().intValue());
  }
  
  public void setImageCategory(IMAGE_CATEGORY imageCategory) {
    super.setImageCategoryNum(Integer.valueOf(imageCategory.getType()));
  }
  
  public void setImageData(Blob imageData)
  {
    super.setImageData(imageData);
    
    if (imageData != null) {
      try {
        int blobLength = (int)imageData.length();
        image = new ImageIcon(imageData.getBytes(1L, blobLength)).getImage();
      } catch (SQLException e) {
        PosLog.error(getClass(), e);
      }
    }
  }
  
  public ImageIcon getScaledImage(int w, int h)
  {
    Image scaledInstance = null;
    if (image != null) {
      scaledInstance = image.getScaledInstance(w, h, 4);
      return new ImageIcon(scaledInstance);
    }
    
    return null;
  }
  
  public Image getImage() {
    return image;
  }
  
  public ImageIcon getAsIcon() {
    if (image == null) {
      return null;
    }
    return new ImageIcon(image.getScaledInstance(80, 80, 1));
  }
}
