package com.floreantpos.model;

import com.floreantpos.model.base.BasePackagingUnit;
import org.apache.commons.lang.StringUtils;




















public class PackagingUnit
  extends BasePackagingUnit
  implements IUnit
{
  private static final long serialVersionUID = 1L;
  
  public PackagingUnit() {}
  
  public PackagingUnit(String id)
  {
    super(id);
  }
  

  public void setPackagingDimension(PackagingDimension dimension)
  {
    setDimension(dimension.name());
  }
  
  public PackagingDimension getPackagingDimension() {
    String dimension2 = getDimension();
    
    if (StringUtils.isEmpty(dimension2)) {
      return null;
    }
    
    return PackagingDimension.valueOf(dimension2);
  }
  
  public String toString()
  {
    return getCode();
  }
  
  public String getUniqueCode()
  {
    return super.getCode();
  }
}
