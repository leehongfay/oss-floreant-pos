package com.floreantpos.model;

import com.floreantpos.model.base.BaseInventoryStock;
import com.floreantpos.model.dao.InventoryLocationDAO;
import org.apache.commons.lang.StringUtils;

public class InventoryStock
  extends BaseInventoryStock
{
  private static final long serialVersionUID = 1L;
  private double menuItemCost;
  private String locationName;
  private InventoryLocation inventoryLocation;
  
  public InventoryStock() {}
  
  public InventoryStock(String id)
  {
    super(id);
  }
  






  public String getSku()
  {
    String skuDisplay = super.getSku();
    return StringUtils.isNotEmpty(skuDisplay) ? skuDisplay : "";
  }
  
  public void setMenuItem(MenuItem menuItem) {
    if (menuItem == null)
      return;
    setMenuItemId(menuItem.getId());
    setItemName(menuItem.getName());
    setSku(menuItem.getSku());
  }
  
  public InventoryLocation getInventoryLocation()
  {
    String locationId = getLocationId();
    if (locationId == null)
      return null;
    if ((inventoryLocation != null) && (inventoryLocation.getId().equals(locationId)))
      return inventoryLocation;
    return InventoryLocationDAO.getInstance().get(locationId);
  }
  
  public void setInventoryLocation(InventoryLocation inventoryLocation) {
    this.inventoryLocation = inventoryLocation;
  }
  
  public double getMenuItemCost() {
    return menuItemCost;
  }
  
  public void setMenuItemCost(double menuItemCost) {
    this.menuItemCost = menuItemCost;
  }
  
  public String getLocationName() {
    return locationName;
  }
  
  public void setLocationName(String locationName) {
    this.locationName = locationName;
  }
}
