package com.floreantpos.model;

import com.floreantpos.model.base.BaseVirtualPrinter;
import java.util.Iterator;
import java.util.List;



















public class VirtualPrinter
  extends BaseVirtualPrinter
{
  private static final long serialVersionUID = 1L;
  public static final int REPORT = 0;
  public static final int RECEIPT = 1;
  public static final int KITCHEN = 2;
  public static final int PACKING = 3;
  public static final int KITCHEN_DISPLAY = 4;
  public static final int LABEL = 5;
  public static final int STICKER = 6;
  public static final String[] PRINTER_TYPE_NAMES = { "Report", "Receipt", "Kitchen", "Packing", "KDS", "Label", "Sticker" };
  


  public VirtualPrinter() {}
  


  public VirtualPrinter(String id)
  {
    super(id);
  }
  





  public VirtualPrinter(String id, String name)
  {
    super(id, name);
  }
  




  public int hashCode()
  {
    return name.hashCode();
  }
  
  public boolean equals(Object obj)
  {
    if (!(obj instanceof VirtualPrinter)) {
      return false;
    }
    
    VirtualPrinter other = (VirtualPrinter)obj;
    
    return name.equalsIgnoreCase(name);
  }
  
  public String getDisplayName() {
    return PRINTER_TYPE_NAMES[getType().intValue()];
  }
  
  public String toString()
  {
    String name = getName();
    
    List<String> typeNames = getOrderTypeNames();
    if ((typeNames != null) && (typeNames.size() > 0)) {
      name = name + " (";
      
      for (Iterator iterator = typeNames.iterator(); iterator.hasNext();) {
        String string = (String)iterator.next();
        name = name + string;
        if (iterator.hasNext()) {
          name = name + ", ";
        }
      }
      name = name + ")";
    }
    
    return name;
  }
}
