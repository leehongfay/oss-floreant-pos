package com.floreantpos.model;

import com.floreantpos.model.base.BaseStockCountItem;
import com.floreantpos.model.dao.MenuItemDAO;
import org.apache.commons.lang.StringUtils;

public class StockCountItem extends BaseStockCountItem
{
  private static final long serialVersionUID = 1L;
  private double countVariance;
  private double costVariance;
  private double cost;
  private MenuItem menuItem;
  
  public StockCountItem() {}
  
  public StockCountItem(String id)
  {
    super(id);
  }
  
  public StockCountItem(String id, StockCount stockCount)
  {
    super(id, stockCount);
  }
  

  public MenuItem getMenuItem()
  {
    if (menuItem == null) {
      String itemId = getItemId();
      if (StringUtils.isEmpty(itemId)) {
        return null;
      }
      menuItem = MenuItemDAO.getInstance().loadInitialized(itemId);
    }
    
    return menuItem;
  }
  
  public void setMenuItem(MenuItem menuItem) {
    this.menuItem = menuItem;
  }
  
  public double getCountVariance() {
    countVariance = (getUnitOnHand().doubleValue() - getActualUnit().doubleValue());
    return countVariance;
  }
  
  public void setCountVariance(double countVariance) {
    this.countVariance = countVariance;
  }
  
  public double getCostVariance() {
    costVariance = (getCountVariance() * getMenuItem().getCost().doubleValue());
    return costVariance;
  }
  
  public void setCostVariance(double costVariance) {
    this.costVariance = costVariance;
  }
  
  public double getCost() {
    return getMenuItem().getCost().doubleValue();
  }
  
  public void setCost(double cost) {
    this.cost = cost;
  }
}
