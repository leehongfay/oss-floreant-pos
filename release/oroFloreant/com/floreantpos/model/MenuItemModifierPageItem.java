package com.floreantpos.model;

import com.floreantpos.IconFactory;
import com.floreantpos.model.base.BaseMenuItemModifierPageItem;
import com.floreantpos.model.dao.ImageResourceDAO;
import com.floreantpos.model.dao.MenuModifierDAO;
import java.awt.Color;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.lang.StringUtils;



public class MenuItemModifierPageItem
  extends BaseMenuItemModifierPageItem
{
  private static final long serialVersionUID = 1L;
  @XmlTransient
  private transient ImageIcon image;
  @XmlTransient
  private transient Color buttonColor;
  @XmlTransient
  private transient Color textColor;
  
  public MenuItemModifierPageItem() {}
  
  public MenuItemModifierPageItem(String id)
  {
    super(id);
  }
  
  public MenuItemModifierPageItem(Integer col, Integer row)
  {
    super.setCol(col);
    super.setRow(row);
  }
  
  public MenuItemModifierPageItem(Integer col, Integer row, MenuModifier menuModifier, MenuItemModifierPage modifierPage)
  {
    super.setCol(col);
    super.setRow(row);
    
    setMenuModifier(menuModifier);
    setParentPage(modifierPage);
  }
  
  public void setParentPage(MenuItemModifierPage modifierPage) {
    if (modifierPage != null) {
      setParentPageId(modifierPage.getId());
    }
    else {
      setParentPageId(null);
    }
  }
  
  public MenuModifier getMenuModifier() {
    if (StringUtils.isNotEmpty(getMenuModifierId())) {
      return MenuModifierDAO.getInstance().get(getMenuModifierId());
    }
    return null;
  }
  
  public void setMenuModifier(MenuModifier menuModifier)
  {
    if (menuModifier != null) {
      setMenuModifierId(menuModifier.getId());
      setMenuModifierName(menuModifier.getDisplayName());
      setButtonColorCode(menuModifier.getButtonColor());
      setTextColorCode(menuModifier.getTextColor());
      setImageId(menuModifier.getImageId());
      setShowImageOnly(menuModifier.isShowImageOnly());
    }
    else {
      setMenuModifierId(null);
    }
  }
  
  public void setImage(ImageIcon image) {
    this.image = image;
  }
  
  public ImageIcon getImage() {
    return IconFactory.getIconFromImageResource(getImageId(), 80, 80);
  }
  
  public ImageIcon getImage(int w, int h) {
    return IconFactory.getIconFromImageResource(getImageId(), w - 20, h - 20);
  }
  
  public ImageIcon getScaledImageIcon(int w, int h) {
    ImageResource imageResource = ImageResourceDAO.getInstance().findById(getImageId());
    if (imageResource != null) {
      image = new ImageIcon(imageResource.getImage().getScaledInstance(w, h, 1));
    }
    return image;
  }
  
  @XmlTransient
  public Color getButtonColor() {
    if (buttonColor != null) {
      return buttonColor;
    }
    
    if (getButtonColorCode() == null) {
      return null;
    }
    
    return this.buttonColor = new Color(getButtonColorCode().intValue());
  }
  
  public void setButtonColor(Color buttonColor) {
    this.buttonColor = buttonColor;
  }
  
  @XmlTransient
  public Color getTextColor() {
    if (textColor != null) {
      return textColor;
    }
    
    if (getTextColorCode() == null) {
      return null;
    }
    
    return this.textColor = new Color(getTextColorCode().intValue());
  }
  
  public void setTextColor(Color textColor) {
    this.textColor = textColor;
  }
}
