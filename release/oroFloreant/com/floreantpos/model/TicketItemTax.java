package com.floreantpos.model;

import com.floreantpos.model.base.BaseTicketItemTax;
import java.io.PrintStream;
import org.json.JSONObject;



public class TicketItemTax
  extends BaseTicketItemTax
{
  private static final long serialVersionUID = 1L;
  
  public TicketItemTax() {}
  
  public TicketItemTax(String id)
  {
    super(id);
  }
  



  public TicketItemTax(String id, String name)
  {
    super(id, name);
  }
  

  public TicketItemTax(String id, String name, double rate)
  {
    super(id, name);
    setRate(Double.valueOf(rate));
  }
  
  public JSONObject toJson() {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put(PROP_NAME, getName());
    jsonObject.put(PROP_RATE, getRate());
    jsonObject.put(PROP_TAX_AMOUNT, getTaxAmount());
    return jsonObject;
  }
  
  public static void main(String[] args) {
    TicketItemTax tax = new TicketItemTax();
    System.out.println(tax.toJson());
  }
}
