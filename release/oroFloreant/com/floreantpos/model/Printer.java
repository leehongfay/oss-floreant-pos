package com.floreantpos.model;




public class Printer
{
  private VirtualPrinter virtualPrinter;
  


  private String deviceName;
  


  private boolean defaultPrinter;
  


  private String type;
  



  public Printer() {}
  



  public Printer(VirtualPrinter virtualPrinter, String deviceName)
  {
    this.virtualPrinter = virtualPrinter;
    this.deviceName = deviceName;
  }
  
  public Printer(VirtualPrinter virtualPrinter, String deviceName, boolean defaultPrinter)
  {
    this.virtualPrinter = virtualPrinter;
    this.deviceName = deviceName;
    this.defaultPrinter = defaultPrinter;
  }
  
  public VirtualPrinter getVirtualPrinter() {
    return virtualPrinter;
  }
  
  public void setVirtualPrinter(VirtualPrinter virtualPrinter) {
    this.virtualPrinter = virtualPrinter;
  }
  
  public String getDeviceName() {
    if (deviceName == null) {
      return "No Print";
    }
    return deviceName;
  }
  
  public void setDeviceName(String deviceName) {
    this.deviceName = deviceName;
  }
  
  public boolean isDefaultPrinter() {
    return defaultPrinter;
  }
  
  public void setDefaultPrinter(boolean defaultPrinter) {
    this.defaultPrinter = defaultPrinter;
  }
  
  public int hashCode()
  {
    return virtualPrinter.hashCode();
  }
  
  public boolean equals(Object obj)
  {
    if (!(obj instanceof Printer)) {
      return false;
    }
    
    Printer that = (Printer)obj;
    
    return virtualPrinter.equals(virtualPrinter);
  }
  
  public String toString()
  {
    return virtualPrinter.toString();
  }
  
  public String getDisplayName() {
    return virtualPrinter.toString() + " -    " + getDeviceName();
  }
  
  public String getType() {
    type = VirtualPrinter.PRINTER_TYPE_NAMES[virtualPrinter.getType().intValue()];
    return type;
  }
  
  public void setType(String type) {
    this.type = type;
  }
}
