package com.floreantpos.model;

import com.floreantpos.model.base.BaseDayPart;





public class DayPart
  extends BaseDayPart
{
  private static final long serialVersionUID = 1L;
  
  public DayPart() {}
  
  public DayPart(String id)
  {
    super(id);
  }
  





  public DayPart(String id, String name)
  {
    super(id, name);
  }
}
