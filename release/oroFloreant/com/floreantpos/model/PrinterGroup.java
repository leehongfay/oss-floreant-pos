package com.floreantpos.model;

import com.floreantpos.model.base.BasePrinterGroup;
import java.util.Iterator;
import java.util.List;























public class PrinterGroup
  extends BasePrinterGroup
{
  private static final long serialVersionUID = 1L;
  
  public PrinterGroup() {}
  
  public PrinterGroup(String id)
  {
    super(id);
  }
  





  public PrinterGroup(String id, String name)
  {
    super(id, name);
  }
  




  public String toString()
  {
    String name = getName();
    
    List<String> list = getPrinterNames();
    if ((list != null) && (list.size() > 0)) {
      name = name + " (";
      for (Iterator iterator = list.iterator(); iterator.hasNext();) {
        String string = (String)iterator.next();
        name = name + string;
        
        if (iterator.hasNext()) {
          name = name + ", ";
        }
      }
      name = name + ")";
      
      if (isDefault) {
        name = name + "   -  Default";
      }
    }
    
    return name;
  }
  
  public String getUniqueId() {
    return ("pg_" + getName() + "_" + getId()).replaceAll("\\s+", "_");
  }
}
