package com.floreantpos.model;

import com.floreantpos.model.base.BasePayoutRecepient;






















public class PayoutRecepient
  extends BasePayoutRecepient
{
  private static final long serialVersionUID = 1L;
  
  public PayoutRecepient() {}
  
  public PayoutRecepient(String id)
  {
    super(id);
  }
  


  public String toString()
  {
    return getName();
  }
}
