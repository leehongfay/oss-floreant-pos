package com.floreantpos.model;

import com.floreantpos.model.base.BaseOutlet;
import com.floreantpos.model.dao.AddressDAO;
import com.floreantpos.model.util.DataProvider;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang.StringUtils;





@XmlRootElement
public class Outlet
  extends BaseOutlet
{
  private static final long serialVersionUID = 1L;
  
  public Outlet() {}
  
  public Outlet(String id)
  {
    super(id);
  }
  


  public String toString()
  {
    return super.getName();
  }
  
  public TaxGroup getTaxGroup() {
    return DataProvider.get().getTaxGroupById(getTaxGroupId());
  }
  
  public void setTaxGroup(TaxGroup taxGroup) {
    String taxGroupId = null;
    if (taxGroup != null) {
      taxGroupId = taxGroup.getId();
    }
    super.setTaxGroupId(taxGroupId);
  }
  
  public Address getAddress() {
    if (StringUtils.isNotEmpty(getAddressId())) {
      return AddressDAO.getInstance().get(getAddressId());
    }
    return null;
  }
  
  public void setAddress(Address address) {
    String addressId = null;
    if (address != null) {
      addressId = address.getId();
    }
    super.setAddressId(addressId);
  }
  
  public Currency getCurrency() {
    return DataProvider.get().getCurrency(getCurrencyId());
  }
  
  public void setCurrency(Currency currency) {
    String currencyId = null;
    if (currency != null) {
      currencyId = currency.getId();
    }
    super.setCurrencyId(currencyId);
  }
}
