package com.floreantpos.model.util;

import com.floreantpos.Messages;
import com.floreantpos.model.dao.StoreDAO;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;
















public class DateUtil
{
  public DateUtil() {}
  
  private static final SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm a");
  private static final SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd MMM yy, hh:mm a");
  private static final SimpleDateFormat dateFormat3 = new SimpleDateFormat("MMM d h:mm:ss a");
  private static final SimpleDateFormat dateFormat4 = new SimpleDateFormat("MM/dd/YYYY");
  private static final SimpleDateFormat dateFormat5 = new SimpleDateFormat("dd MMM yy");
  private static final SimpleDateFormat dateFormat6 = new SimpleDateFormat("MMM dd, hh:mm a");
  private static final SimpleDateFormat dateFormat7 = new SimpleDateFormat("dd MMM, hh:mm:ss a");
  private static final SimpleDateFormat usFormat = new SimpleDateFormat("MM/dd/yy hh:mm a");
  private static final SimpleDateFormat dateFormat9 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss ");
  
  private static final DateFormat localeFormat = DateFormat.getDateInstance(3);
  
  public static String formatDateAsLocale(Date date) {
    String dateString = localeFormat.format(date);
    return dateString;
  }
  
  public static String formatReportDateAsString(Date date) {
    String dateString = usFormat.format(date);
    return dateString;
  }
  
  public static String formatDateAsString(Date date) {
    String dateString = dateFormat9.format(date);
    return dateString;
  }
  
  public static Date parseByFullDate(String date) throws Exception {
    return dateFormat9.parse(date);
  }
  
  public static String formatFullDateAsString(Date date) {
    String dateString = dateFormat5.format(date);
    return dateString;
  }
  
  public static Date startOfDay(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.set(11, 0);
    cal.set(12, 0);
    cal.set(13, 0);
    
    return new Date(cal.getTimeInMillis());
  }
  
  public static Date endOfDay(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.set(11, 23);
    cal.set(12, 59);
    cal.set(13, 59);
    
    return new Date(cal.getTimeInMillis());
  }
  
  public static Date startOfMonth(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.set(11, 0);
    cal.set(12, 0);
    cal.set(13, 0);
    cal.set(5, 1);
    
    return new Date(cal.getTimeInMillis());
  }
  
  public static Date endOfMonth(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.set(11, 23);
    cal.set(12, 59);
    cal.set(13, 59);
    cal.set(5, cal.getActualMaximum(5));
    
    return new Date(cal.getTimeInMillis());
  }
  
  public static Date copyTime(Date copyTo, Date copyFrom)
  {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(copyFrom);
    int hour = calendar.get(11);
    int min = calendar.get(12);
    
    calendar.setTime(copyTo);
    calendar.set(11, hour);
    calendar.set(12, min);
    return calendar.getTime();
  }
  
  public static boolean between(Date startDate, Date endDate, Date guniping)
  {
    if ((startDate == null) || (endDate == null)) {
      return false;
    }
    
    return ((guniping.equals(startDate)) || (guniping.after(startDate))) && ((guniping.equals(endDate)) || (guniping.before(endDate)));
  }
  
  public static String formatSmall(Date date) {
    return dateFormat6.format(date);
  }
  
  public static String getReportDate() {
    String date = dateFormat3.format(new Date());
    return date;
  }
  
  public static boolean isToday(Date date) {
    return isSameDay(date, Calendar.getInstance().getTime());
  }
  
  public static boolean isToday(Calendar cal) {
    return isSameDay(cal, Calendar.getInstance());
  }
  
  public static String formatAsTodayDate(Date date) {
    String dateString = dateFormat1.format(date);
    return Messages.getString("DateUtil.2") + " " + dateString;
  }
  
  public static String formatFullDateAndTimeAsString(Date date) {
    String dateString = dateFormat2.format(date);
    return dateString;
  }
  
  public static String formatFullDateAndTimeWithoutYearAsString(Date date) {
    String dateString = dateFormat7.format(date);
    return dateString;
  }
  
  public static boolean isSameDay(Date date1, Date date2) {
    Calendar cal1 = Calendar.getInstance();
    cal1.setTime(date1);
    Calendar cal2 = Calendar.getInstance();
    cal2.setTime(date2);
    return isSameDay(cal1, cal2);
  }
  
  public static boolean isSameDay(Calendar cal1, Calendar cal2) {
    return (cal1.get(0) == cal2.get(0)) && (cal1.get(1) == cal2.get(1)) && (cal1.get(6) == cal2.get(6));
  }
  
  public static String getOnlyFormattedTime(Date date) {
    String dateString = dateFormat1.format(date);
    return dateString;
  }
  
  public static String getOnlyFormattedDate(Date date) {
    String dateString = dateFormat4.format(date);
    return dateString;
  }
  
  public static String getElapsedTime(Date oldTime, Date newTime) {
    DateTime startDate = new DateTime(oldTime);
    DateTime endDate = new DateTime(newTime);
    Interval interval = new Interval(startDate, endDate);
    long days = interval.toDuration().getStandardDays();
    long hours = interval.toDuration().getStandardHours();
    long minutes = interval.toDuration().getStandardMinutes();
    long seconds = interval.toDuration().getStandardSeconds();
    
    hours %= 24L;
    minutes %= 60L;
    seconds %= 60L;
    
    String strDays = days + "d, ";
    String strHours = hours + "hr, ";
    String strMins = minutes + "mn";
    String strSec = seconds + "secs";
    String strAgo = "";
    
    String fullTime = strDays + strHours + strMins + strAgo;
    String timeWithoutDay = strHours + strMins + strAgo;
    String timeWithoutHour = strMins + strAgo;
    String timeWithoutMin = strSec + strAgo;
    
    if (days != 0L) {
      return fullTime;
    }
    if (hours != 0L) {
      return timeWithoutDay;
    }
    if (minutes != 0L) {
      return timeWithoutHour;
    }
    if (seconds != 0L) {
      return timeWithoutMin;
    }
    
    return "not printed yet";
  }
  
  public static Calendar getLocalTimeCalendar()
  {
    return Calendar.getInstance();
  }
  
  public static Calendar getServerTimeCalendar() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(StoreDAO.geServerTimestamp());
    return calendar;
  }
}
