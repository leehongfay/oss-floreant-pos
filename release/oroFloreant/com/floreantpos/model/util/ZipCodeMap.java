package com.floreantpos.model.util;

public class ZipCodeMap
{
  private String zipCode;
  private String city;
  private String state;
  
  public ZipCodeMap() {}
  
  public String getZipCode()
  {
    return zipCode;
  }
  


  public void setZipCode(String zipCode)
  {
    this.zipCode = zipCode;
  }
  


  public String getCity()
  {
    return city;
  }
  


  public void setCity(String city)
  {
    this.city = city;
  }
  


  public String getState()
  {
    return state;
  }
  


  public void setState(String state)
  {
    this.state = state;
  }
}
