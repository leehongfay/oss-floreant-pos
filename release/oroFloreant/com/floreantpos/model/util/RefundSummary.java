package com.floreantpos.model.util;





public class RefundSummary
{
  private int count;
  


  private double amount;
  



  public RefundSummary() {}
  



  public double getAmount()
  {
    return amount;
  }
  
  public void setAmount(double amount) {
    this.amount = amount;
  }
  
  public int getCount() {
    return count;
  }
  
  public void setCount(int count) {
    this.count = count;
  }
}
