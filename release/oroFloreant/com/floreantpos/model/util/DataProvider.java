package com.floreantpos.model.util;

import com.floreantpos.model.Course;
import com.floreantpos.model.Currency;
import com.floreantpos.model.CustomerGroup;
import com.floreantpos.model.Department;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuItemInventoryStatus;
import com.floreantpos.model.MenuShift;
import com.floreantpos.model.Multiplier;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PosPrinters;
import com.floreantpos.model.PriceShift;
import com.floreantpos.model.PrinterGroup;
import com.floreantpos.model.Recepie;
import com.floreantpos.model.ReportGroup;
import com.floreantpos.model.SalesArea;
import com.floreantpos.model.Shift;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.TerminalType;
import com.floreantpos.model.User;
import java.io.File;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;

public abstract class DataProvider
{
  public static final String DATA_PROVIDER_CLASS_NAME = "data.provider.class";
  private static DataProvider instance;
  
  public DataProvider() {}
  
  public abstract void initialize();
  
  public abstract com.floreantpos.model.Store getStore();
  
  public abstract com.floreantpos.model.Outlet getOutlet();
  
  public abstract com.floreantpos.model.InventoryUnit getInventoryUnitById(String paramString);
  
  public abstract com.floreantpos.model.TaxGroup getTaxGroupById(String paramString);
  
  public abstract ReportGroup getReportGroupById(String paramString);
  
  public abstract PrinterGroup getPrinterGroupById(String paramString);
  
  public abstract Department getDepartmentById(String paramString);
  
  public abstract OrderType getOrderTypeById(String paramString);
  
  public abstract InventoryLocation getInventoryLocationById(String paramString);
  
  public abstract User getUserById(String paramString);
  
  public abstract Terminal getTerminalById(Integer paramInteger);
  
  public abstract Shift getShiftById(String paramString);
  
  public abstract Terminal getCurrentTerminal();
  
  public abstract void setCloudTerminal(Terminal paramTerminal);
  
  public abstract OrderType getOrderType(String paramString);
  
  public abstract List<OrderType> getOrderTypes();
  
  public abstract com.floreantpos.model.UserType getUserType(String paramString);
  
  public abstract Currency getCurrency(String paramString);
  
  public abstract Course getCourse(String paramString);
  
  public abstract TerminalType getTerminalType(String paramString);
  
  public abstract Multiplier getMultiplierById(String paramString);
  
  public abstract List<Multiplier> getMultiplierList();
  
  public abstract InventoryLocation getDefaultInLocation();
  
  public abstract InventoryLocation getDefaultOutLocation();
  
  public abstract List<Course> getCourses();
  
  public abstract String getDefaultCourseId();
  
  public abstract void refreshStore();
  
  public abstract void refreshCurrentTerminal();
  
  public abstract SalesArea getSalesArea(String paramString);
  
  public abstract String getRecipeMenuItemName(Recepie paramRecepie);
  
  public abstract PosPrinters getPrinters();
  
  public abstract MenuItemInventoryStatus getMenuItemStockStatus(MenuItem paramMenuItem);
  
  public abstract com.floreantpos.model.Customer getCustomer(String paramString);
  
  public abstract double getPriceFromPriceRule(MenuItem paramMenuItem, OrderType paramOrderType, Department paramDepartment, SalesArea paramSalesArea, CustomerGroup paramCustomerGroup);
  
  public abstract List<com.floreantpos.model.DayPart> getDaryPartShifts();
  
  public abstract List<PriceShift> getPriceShifts();
  
  public abstract List<MenuShift> getMenuShifts();
  
  public abstract Object getObjectOf(Class paramClass, Serializable paramSerializable);
  
  public abstract File getAppConfigFileLocation();
  
  public static DataProvider get()
  {
    if (instance == null) {
      try {
        String className = System.getProperty("data.provider.class");
        if (StringUtils.isEmpty(className)) {
          className = "com.floreantpos.model.dao.util.PosCacheManager";
        }
        instance = (DataProvider)Class.forName(className).newInstance();
      } catch (Exception e) {
        LogFactory.getLog(DataProvider.class).error("Could not initialize data provider");
      }
    }
    return instance;
  }
}
