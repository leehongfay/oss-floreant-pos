package com.floreantpos.model;

import com.floreantpos.model.base.BaseReservationShift;



public class ReservationShift
  extends BaseReservationShift
{
  private static final long serialVersionUID = 1L;
  
  public ReservationShift() {}
  
  public ReservationShift(String id)
  {
    super(id);
  }
  



  public ReservationShift(String id, String name)
  {
    super(id, name);
  }
  

  public static enum DayOfWeek
  {
    Sunday(1),  Monday(2),  Tuesday(3),  Wednesday(4),  Thursday(5),  Friday(6),  Saturday(7);
    
    private final int value;
    
    private DayOfWeek(int value) {
      this.value = value;
    }
    
    public int getValue() {
      return value;
    }
    
    public static DayOfWeek getDayOfWeek(int value) {
      for (DayOfWeek day : ) {
        if (day.getValue() == value) {
          return day;
        }
      }
      return null;
    }
  }
  
  public String getDayOfWeekAsString() {
    String daysDisplay = "";
    String daysString = getDaysOfWeek();
    if (daysString != null) {
      String[] split = daysString.split(",");
      if (split.length == 7)
        return "All";
      if (split.length > 0) {
        for (int i = 0; i < split.length; i++) {
          int day = Integer.valueOf(split[i]).intValue();
          DayOfWeek week = DayOfWeek.getDayOfWeek(day);
          daysDisplay = daysDisplay + week.name();
          if (i != split.length - 1) {
            daysDisplay = daysDisplay + ",";
          }
        }
      }
    }
    return daysDisplay;
  }
}
