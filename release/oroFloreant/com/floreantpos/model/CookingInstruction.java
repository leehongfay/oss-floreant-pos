package com.floreantpos.model;

import com.floreantpos.model.base.BaseCookingInstruction;






















public class CookingInstruction
  extends BaseCookingInstruction
{
  private static final long serialVersionUID = 1L;
  
  public CookingInstruction() {}
  
  public CookingInstruction(String id)
  {
    super(id);
  }
  


  public String toString()
  {
    return getDescription();
  }
}
