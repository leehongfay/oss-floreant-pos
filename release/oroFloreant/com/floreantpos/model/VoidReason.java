package com.floreantpos.model;

import com.floreantpos.model.base.BaseVoidReason;




















public class VoidReason
  extends BaseVoidReason
{
  private static final long serialVersionUID = 1L;
  
  public VoidReason() {}
  
  public VoidReason(String id)
  {
    super(id);
  }
  


  public String toString()
  {
    return getReasonText();
  }
}
