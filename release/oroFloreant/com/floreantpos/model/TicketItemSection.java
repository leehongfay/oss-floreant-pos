package com.floreantpos.model;

import com.floreantpos.model.base.BaseTicketItemSection;





public class TicketItemSection
  extends BaseTicketItemSection
{
  private static final long serialVersionUID = 1L;
  
  public TicketItemSection() {}
  
  public TicketItemSection(String id)
  {
    super(id);
  }
}
