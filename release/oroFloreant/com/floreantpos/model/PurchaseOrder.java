package com.floreantpos.model;

import com.floreantpos.model.base.BasePurchaseOrder;
import com.floreantpos.util.NumberUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;













public class PurchaseOrder
  extends BasePurchaseOrder
{
  private static final long serialVersionUID = 1L;
  public static final int ORDER_PENDING = 0;
  public static final int ORDER_VERIFIED = 1;
  public static final int ORDER_FULLY_INVOICED = 2;
  public static final int ORDER_PARTIALLY_INVOICED = 3;
  public static final int ORDER_FULLY_RECEIVED = 4;
  public static final int ORDER_PARTIALLY_RECEIVED = 5;
  public static final int ORDER_PARTIALLY_RECEIVED_AND_INVOICED = 6;
  public static final int ORDER_CLOSED = 7;
  public static final int ORDER_CANCELLED = 8;
  
  public PurchaseOrder() {}
  
  public PurchaseOrder(String id)
  {
    super(id);
  }
  












  public static final String[] ORDER_STATUS = { "Pending", "Verified", "Fully Invoiced", "Partially Invoiced", "Fully Received", "Partially Received", "Partially Received and Invoiced", "Closed", "Cancelled" };
  
  public static final String PO_VERIFIED_BY = "Verified_By";
  
  public static final String PO_SENT_BY = "Sent_By";
  
  public static final String PO_RECEIVED_BY = "Received_By";
  
  public static final String PO_INVOICED_BY = "Invoiced_By";
  public static final String PO_CLOSED_BY = "Closed_By";
  public String orderStatusDisplay;
  public static final String DEBIT = "DEBIT";
  public static final String CREDIT = "CREDIT";
  public String statusDisplay;
  
  public String getOrderStatusDisplay()
  {
    return ORDER_STATUS[getStatus().intValue()];
  }
  
  public void setOrderStatusDisplay(String orderStatusDisplay) {
    this.orderStatusDisplay = ORDER_STATUS[getStatus().intValue()];
  }
  
  public List<PurchaseOrderItem> getOrderItems()
  {
    List<PurchaseOrderItem> items = super.getOrderItems();
    
    if (items == null) {
      items = new ArrayList();
      super.setOrderItems(items);
    }
    return items;
  }
  
  public void calculatePrice() {
    List<PurchaseOrderItem> items = getOrderItems();
    if (items == null) {
      return;
    }
    
    double subtotalAmount = 0.0D;
    double discountAmount = 0.0D;
    double taxAmount = 0.0D;
    
    for (PurchaseOrderItem item : items) {
      item.calculatePrice();
      subtotalAmount += item.getSubtotalAmount().doubleValue();
      discountAmount += item.getDiscountAmount().doubleValue();
      taxAmount += item.getTaxAmount().doubleValue();
    }
    
    setSubtotalAmount(Double.valueOf(subtotalAmount));
    setDiscountAmount(Double.valueOf(discountAmount));
    setTaxAmount(Double.valueOf(taxAmount));
    
    double totalAmount = subtotalAmount - discountAmount + taxAmount;
    
    totalAmount = fixInvalidAmount(totalAmount);
    setTotalAmount(Double.valueOf(NumberUtil.roundToTwoDigit(totalAmount)));
    
    double dueAmount = totalAmount - getPaidAmount().doubleValue();
    setDueAmount(Double.valueOf(NumberUtil.roundToTwoDigit(dueAmount)));
  }
  
  private double fixInvalidAmount(double tax) {
    if ((tax < 0.0D) || (Double.isNaN(tax))) {
      tax = 0.0D;
    }
    return tax;
  }
  
  public void addProperty(String name, String value) {
    if (getProperties() == null) {
      setProperties(new HashMap());
    }
    
    getProperties().put(name, value);
  }
  
  public boolean hasProperty(String key) {
    return getProperty(key) != null;
  }
  
  public String getProperty(String key) {
    if (getProperties() == null) {
      return null;
    }
    
    return (String)getProperties().get(key);
  }
  
  public String getProperty(String key, String defaultValue) {
    if (getProperties() == null) {
      return null;
    }
    
    String string = (String)getProperties().get(key);
    if (StringUtils.isEmpty(string)) {
      return defaultValue;
    }
    
    return string;
  }
  
  public void removeProperty(String propertyName) {
    Map<String, String> properties = getProperties();
    if (properties == null) {
      return;
    }
    
    properties.remove(propertyName);
  }
  
  public String getStatusDisplay() {
    int status = getStatus().intValue();
    if (status == 0)
      return "Verify";
    if ((status == 1) || (status == 5)) {
      return "Receive";
    }
    return "";
  }
  
  public void setStatusDisplay(String statusDisplay) {
    this.statusDisplay = statusDisplay;
  }
}
