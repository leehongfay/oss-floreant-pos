package com.floreantpos.model;

import com.floreantpos.model.base.BaseSequenceNumber;

public class SequenceNumber extends BaseSequenceNumber
{
  private static final long serialVersionUID = 1L;
  public static final String TICKET_TOKEN = "TICKET_TOKEN_NUMBER";
  
  public SequenceNumber() {}
  
  public SequenceNumber(String type) {
    super(type);
  }
  
  public SequenceNumber(String type, Integer nextReference) {
    super(type);
    setNextSequenceNumber(nextReference);
  }
}
