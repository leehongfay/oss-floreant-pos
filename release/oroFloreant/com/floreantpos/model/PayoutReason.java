package com.floreantpos.model;

import com.floreantpos.model.base.BasePayoutReason;






















public class PayoutReason
  extends BasePayoutReason
{
  private static final long serialVersionUID = 1L;
  
  public PayoutReason() {}
  
  public PayoutReason(String id)
  {
    super(id);
  }
  


  public String toString()
  {
    return getReason();
  }
}
