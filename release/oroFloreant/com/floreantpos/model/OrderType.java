package com.floreantpos.model;

import com.floreantpos.PosLog;
import com.floreantpos.model.base.BaseOrderType;
import com.floreantpos.model.util.DataProvider;
import java.awt.Color;
import java.awt.Image;
import java.sql.Blob;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.xml.bind.annotation.XmlTransient;





public class OrderType
  extends BaseOrderType
{
  private static final long serialVersionUID = 1L;
  public static final String BAR_TAB = "BAR_TAB";
  public static final String FOR_HERE = "FOR HERE";
  public static final String TO_GO = "TO GO";
  private ImageIcon image;
  private Color buttonColor;
  private Color textColor;
  
  public OrderType() {}
  
  public TaxGroup getDefaultTaxGroup()
  {
    return DataProvider.get().getTaxGroupById(getDefaultTaxGroupId());
  }
  



  public void setDefaultTaxGroup(TaxGroup defaultTaxGroup)
  {
    setDefaultTaxGroupId(defaultTaxGroup == null ? null : defaultTaxGroup.getId());
  }
  


  public TaxGroup getForHereTaxGroup()
  {
    return DataProvider.get().getTaxGroupById(getForHereTaxGroupId());
  }
  



  public void setForHereTaxGroup(TaxGroup forHereTaxGroup)
  {
    setForHereTaxGroupId(forHereTaxGroup == null ? null : forHereTaxGroup.getId());
  }
  


  public TaxGroup getToGoTaxGroup()
  {
    return DataProvider.get().getTaxGroupById(getToGoTaxGroupId());
  }
  



  public void setToGoTaxGroup(TaxGroup toGoTaxGroup)
  {
    setToGoTaxGroupId(toGoTaxGroup == null ? null : toGoTaxGroup.getId());
  }
  
  @XmlTransient
  public Color getTextColor() {
    if (textColor != null) {
      return textColor;
    }
    
    if (getTextColorCode() == null) {
      return null;
    }
    return this.textColor = new Color(getTextColorCode().intValue());
  }
  
  public void setTextColor(Color textColor) {
    this.textColor = textColor;
  }
  
  @XmlTransient
  public Color getButtonColor() {
    if (buttonColor != null) {
      return buttonColor;
    }
    
    if (getButtonColorCode() == null) {
      return null;
    }
    
    return this.buttonColor = new Color(getButtonColorCode().intValue());
  }
  
  public OrderType(String id) {
    super(id);
  }
  
  public String name() {
    return super.getName();
  }
  
  public OrderType valueOf() {
    return this;
  }
  
  public String toString()
  {
    return getName().replaceAll("_", " ");
  }
  
  public ImageIcon getImage()
  {
    return image;
  }
  
  public Image getStoredImage() {
    ImageIcon tempImageIcon = getImage();
    Image tempImage;
    if (tempImageIcon != null) {
      tempImage = tempImageIcon.getImage();
    }
    else
      return null;
    Image tempImage;
    return tempImage;
  }
  
  public void setImageData(Blob imageData)
  {
    super.setImageData(imageData);
    
    if (imageData != null) {
      try {
        int blobLength = (int)imageData.length();
        image = new ImageIcon(imageData.getBytes(1L, blobLength));
      } catch (SQLException e) {
        PosLog.error(getClass(), e);
      }
    }
  }
  
  public ImageIcon getScaledImage(int width, int height)
  {
    Image scaledInstance = null;
    if (image != null) {
      scaledInstance = image.getImage().getScaledInstance(width, height, 4);
      return new ImageIcon(scaledInstance);
    }
    
    return null;
  }
  
  public String getUniqueId() {
    return ("order_type_" + getName() + "_" + getId()).replaceAll("\\s+", "_");
  }
}
