package com.floreantpos.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.floreantpos.model.base.BaseInventoryUnit;
import com.floreantpos.model.dao.InventoryUnitGroupDAO;
import java.util.List;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.lang.StringUtils;





















@JsonIgnoreProperties(ignoreUnknown=true, value={})
public class InventoryUnit
  extends BaseInventoryUnit
  implements IUnit
{
  private static final long serialVersionUID = 1L;
  private InventoryUnitGroup unitGroup;
  
  public InventoryUnit() {}
  
  public InventoryUnit(String id)
  {
    super(id);
  }
  



  public InventoryUnit(String id, String code)
  {
    super(id, code);
  }
  


  public String toString()
  {
    return getCode();
  }
  
  @JsonIgnore
  public String getUniqueCode()
  {
    return super.getCode();
  }
  

  @XmlTransient
  public InventoryUnitGroup getUnitGroup()
  {
    if (StringUtils.isEmpty(unitGroupId))
      return null;
    if ((unitGroup != null) && (unitGroupId.equals(unitGroup.getId())))
      return unitGroup;
    return this.unitGroup = InventoryUnitGroupDAO.getInstance().get(unitGroupId);
  }
  
  public void setUnitGroup(InventoryUnitGroup unitGroup) {
    this.unitGroup = unitGroup;
  }
  
  public String getUniqueId() {
    return ("unit_" + getName() + "_" + getId()).replaceAll("\\s+", "_");
  }
  
  public double getBaseUnitConversionValue() {
    double baseConversionValue = 0.0D;
    List<InventoryUnit> units = getUnitGroup().getUnits();
    if ((units != null) && (units.size() > 0)) {
      for (InventoryUnit groupUnit : units) {
        if (groupUnit.isBaseUnit().booleanValue()) {
          baseConversionValue = groupUnit.getConversionRate().doubleValue();
          break;
        }
      }
    }
    return baseConversionValue * getConversionRate().doubleValue();
  }
}
