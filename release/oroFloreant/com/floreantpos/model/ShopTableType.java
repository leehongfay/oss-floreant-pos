package com.floreantpos.model;

import com.floreantpos.model.base.BaseShopTableType;






















public class ShopTableType
  extends BaseShopTableType
{
  private static final long serialVersionUID = 1L;
  
  public ShopTableType() {}
  
  public ShopTableType(String id)
  {
    super(id);
  }
  

  public String toString()
  {
    return getName();
  }
}
