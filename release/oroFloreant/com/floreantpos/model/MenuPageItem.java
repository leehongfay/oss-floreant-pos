package com.floreantpos.model;

import com.floreantpos.IconFactory;
import com.floreantpos.model.base.BaseMenuPageItem;
import com.floreantpos.model.dao.ImageResourceDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import java.awt.Color;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.xml.bind.annotation.XmlTransient;




public class MenuPageItem
  extends BaseMenuPageItem
{
  private static final long serialVersionUID = 1L;
  private transient MenuItem menuItem;
  @XmlTransient
  private transient ImageIcon image;
  @XmlTransient
  private transient Color buttonColor;
  @XmlTransient
  private transient Color textColor;
  
  public MenuPageItem() {}
  
  public MenuPageItem(String id)
  {
    super(id);
  }
  
  public MenuPageItem(Integer col, Integer row)
  {
    super.setCol(col);
    super.setRow(row);
  }
  
  public MenuPageItem(Integer col, Integer row, MenuItem menuItem, MenuPage menuPage)
  {
    super.setCol(col);
    super.setRow(row);
    
    setMenuItem(menuItem);
    setMenuPage(menuPage);
  }
  
  public void setMenuPage(MenuPage menuPage) {
    if (menuPage != null) {
      setMenuPageId(menuPage.getId());
    }
    else {
      setMenuPageId(null);
    }
  }
  
  public MenuItem getMenuItem() {
    if ((menuItem == null) && (getMenuItemId() != null)) {
      menuItem = MenuItemDAO.getInstance().get(getMenuItemId());
    }
    return menuItem;
  }
  
  public void setMenuItem(MenuItem menuItem) {
    this.menuItem = menuItem;
    if (menuItem != null) {
      setMenuItemId(menuItem.getId());
      setMenuItemName(menuItem.getDisplayName());
      setButtonColorCode(menuItem.getButtonColorCode());
      setTextColorCode(menuItem.getTextColorCode());
      setImageId(menuItem.getImageId());
      setShowImageOnly(menuItem.isShowImageOnly());
    }
    else {
      setMenuItemId(null);
    }
  }
  
  public void setImage(ImageIcon image) {
    this.image = image;
  }
  
  public ImageIcon getImage() {
    return IconFactory.getIconFromImageResource(getImageId(), 80, 80);
  }
  
  public ImageIcon getImage(int w, int h) {
    return IconFactory.getIconFromImageResource(getImageId(), w - 20, h - 20);
  }
  
  public ImageIcon getScaledImageIcon(int w, int h) {
    ImageResource imageResource = ImageResourceDAO.getInstance().findById(getImageId());
    if (imageResource != null) {
      image = new ImageIcon(imageResource.getImage().getScaledInstance(w, h, 1));
    }
    return image;
  }
  
  @XmlTransient
  public Color getButtonColor() {
    if (buttonColor != null) {
      return buttonColor;
    }
    
    if (getButtonColorCode() == null) {
      return null;
    }
    
    return this.buttonColor = new Color(getButtonColorCode().intValue());
  }
  
  public void setButtonColor(Color buttonColor) {
    this.buttonColor = buttonColor;
  }
  
  @XmlTransient
  public Color getTextColor() {
    if (textColor != null) {
      return textColor;
    }
    
    if (getTextColorCode() == null) {
      return null;
    }
    
    return this.textColor = new Color(getTextColorCode().intValue());
  }
  
  public void setTextColor(Color textColor) {
    this.textColor = textColor;
  }
}
