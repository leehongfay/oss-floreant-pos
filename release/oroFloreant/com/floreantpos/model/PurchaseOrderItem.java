package com.floreantpos.model;

import com.floreantpos.model.base.BasePurchaseOrderItem;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.util.NumberUtil;
import org.apache.commons.lang.StringUtils;


public class PurchaseOrderItem
  extends BasePurchaseOrderItem
{
  private static final long serialVersionUID = 1L;
  private Double quantityToReceive;
  private MenuItem menuItem;
  
  public PurchaseOrderItem() {}
  
  public PurchaseOrderItem(String id)
  {
    super(id);
  }
  



  public PurchaseOrderItem(String id, PurchaseOrder purchaseOrder)
  {
    super(id, purchaseOrder);
  }
  




  public void calculatePrice()
  {
    setSubtotalAmount(Double.valueOf(NumberUtil.roundToTwoDigit(getUnitPrice().doubleValue() * getItemQuantity().doubleValue())));
    setTotalAmount(Double.valueOf(NumberUtil.roundToTwoDigit(getSubtotalAmount().doubleValue() + getTaxAmount().doubleValue() - getDiscountAmount().doubleValue())));
  }
  
  public String getQuantityDisplay() {
    return NumberUtil.formatNumber(super.getItemQuantity());
  }
  
  public String getPriceDisplay() {
    return NumberUtil.formatNumber(super.getUnitPrice());
  }
  
  public String getTotalDisplay() {
    return NumberUtil.formatNumber(super.getTotalAmount());
  }
  
  public Double getQuantityToReceive() {
    return quantityToReceive == null ? Double.valueOf(0.0D) : quantityToReceive;
  }
  
  public void setQuantityToReceive(Double quantityToReceive) {
    this.quantityToReceive = quantityToReceive;
  }
  
  public MenuItem getMenuItem() {
    if (menuItem == null) {
      String itemId = getMenuItemId();
      if (StringUtils.isEmpty(itemId)) {
        return null;
      }
      menuItem = MenuItemDAO.getInstance().loadInitialized(itemId);
    }
    
    return menuItem;
  }
  
  public void setMenuItem(MenuItem menuItem) {
    this.menuItem = menuItem;
  }
}
