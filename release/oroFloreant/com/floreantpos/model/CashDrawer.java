package com.floreantpos.model;

import com.floreantpos.model.base.BaseCashDrawer;
import com.floreantpos.model.dao.VoidItemDAO;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.util.NumberUtil;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.xml.bind.annotation.XmlRootElement;



















@XmlRootElement
public class CashDrawer
  extends BaseCashDrawer
{
  private static final long serialVersionUID = 1L;
  private double giftCardAddBalance;
  protected DrawerType drawerType;
  private Double cardVoidAmount;
  private Set<DrawerPullVoidEntry> voidEntries;
  
  public CashDrawer() {}
  
  public User getAssignedUser()
  {
    return DataProvider.get().getUserById(getAssignedUserId());
  }
  
  public void setAssignedUser(User user) {
    if (user == null) {
      setAssignedUserId(null);
    }
    else {
      setAssignedUserId(user.getId());
    }
  }
  
  public User getAssignedBy() {
    return DataProvider.get().getUserById(getAssignedByUserId());
  }
  
  public void setAssignedBy(User user) {
    if (user == null) {
      setAssignedByUserId(null);
    }
    else {
      setAssignedByUserId(user.getId());
    }
  }
  
  public User getClosedBy() {
    return DataProvider.get().getUserById(getClosedByUserId());
  }
  
  public void setClosedBy(User user) {
    if (user == null) {
      setClosedByUserId(null);
    }
    else {
      setClosedByUserId(user.getId());
    }
  }
  
  public Terminal getTerminal() {
    return DataProvider.get().getTerminalById(getTerminalId());
  }
  
  public void setTerminal(Terminal terminal) {
    if (terminal == null) {
      setTerminalId(null);
    }
    else {
      setTerminalId(terminal.getId());
    }
  }
  
  public DrawerType getDrawerType() {
    return DrawerType.fromInt(super.getType().intValue());
  }
  
  public void setDrawerType(DrawerType drawerType) {
    super.setType(Integer.valueOf(drawerType.getTypeNumber()));
  }
  
  public double getGiftCardAddBalance() {
    Map<String, Double> revenueCategory = super.getOtherRevenueCategory();
    if (revenueCategory == null) {
      return 0.0D;
    }
    Set<String> keys = revenueCategory.keySet();
    double total = 0.0D;
    for (String key : keys) {
      total += ((Double)revenueCategory.get(key)).doubleValue();
    }
    return total;
  }
  
  public CashBreakdown getCurrencyBalance(Currency currency) {
    List<CashBreakdown> list = getCashBreakdownList();
    if (list == null) {
      return null;
    }
    
    for (CashBreakdown cashBreakdown : list) {
      if (currency.equals(cashBreakdown.getCurrency())) {
        return cashBreakdown;
      }
    }
    
    return null;
  }
  
  public void setGiftCardAddBalance(double giftCardAddBalance) {
    this.giftCardAddBalance = giftCardAddBalance;
  }
  

  public void setPayOutNumber(Integer i) {}
  
  public String getCashReceiptNumber()
  {
    return "";
  }
  

  public void setCashReceiptNumber(String s) {}
  
  public String getCreditCardReceiptNumber()
  {
    return "";
  }
  

  public void setCreditCardReceiptNumber(String s) {}
  
  public String getDrawerBleedNumber()
  {
    return "";
  }
  

  public void setDebitCardReceiptNumber(String s) {}
  
  public String getDebitCardReceiptNumber()
  {
    return "";
  }
  

  public void setDrawerBleedNumber(String s) {}
  
  public Integer getPayOutNumber()
  {
    return Integer.valueOf(0);
  }
  
  public void addCurrencyBalances(List<CashBreakdown> cashBreakdown) {
    getCashBreakdownList().addAll(cashBreakdown);
  }
  
  public List<CashBreakdown> getCashBreakdownList()
  {
    List<CashBreakdown> curBalanceList = super.getCashBreakdownList();
    
    if (curBalanceList == null) {
      curBalanceList = new ArrayList();
      super.setCashBreakdownList(curBalanceList);
    }
    return curBalanceList;
  }
  
  public void calculate()
  {
    setTotalRevenue(Double.valueOf(getNetSales().doubleValue() + getSalesTax().doubleValue() - getTotalDiscountAmount().doubleValue()));
    setGrossReceipts(Double.valueOf(getTotalRevenue().doubleValue() + getCashTips().doubleValue() + getChargedTips().doubleValue()));
    

    double total = getCashReceiptAmount().doubleValue() + getCreditCardReceiptAmount().doubleValue() + getDebitCardReceiptAmount().doubleValue() + getGiftCertReturnAmount().doubleValue() + getCustomPaymentAmount().doubleValue() - getGiftCertChangeAmount().doubleValue() - getRefundAmount().doubleValue() - getCardVoidAmount().doubleValue() - getCustomerPaymentAmount().doubleValue();
    setReceiptDifferential(Double.valueOf(NumberUtil.roundToTwoDigit(getGrossReceipts().doubleValue() - total)));
    
    setTipsDifferential(Double.valueOf(getCashTips().doubleValue() + getChargedTips().doubleValue() - getTipsPaid().doubleValue()));
    
    double totalCash = getCashReceiptAmount().doubleValue();
    double tips = getTipsPaid().doubleValue();
    double totalPayout = getPayOutAmount().doubleValue();
    double beginCash = getBeginCash().doubleValue();
    double refundAmount = getRefundAmount().doubleValue();
    double drawerBleed = getDrawerBleedAmount().doubleValue();
    
    double accountable = beginCash + totalCash - tips - totalPayout - refundAmount - drawerBleed;
    setDrawerAccountable(Double.valueOf(accountable));
  }
  




  public Set<DrawerPullVoidEntry> getVoidEntries()
  {
    if (getId() == null)
      return voidEntries;
    voidEntries = new HashSet(VoidItemDAO.getInstance().getVoidEntries(this));
    if (voidEntries != null) {
      double totalVoidAmount = 0.0D;
      for (DrawerPullVoidEntry drawerPullVoidEntry : voidEntries) {
        totalVoidAmount += drawerPullVoidEntry.getAmount().doubleValue();
      }
      setTotalVoid(Double.valueOf(totalVoidAmount));
    }
    return voidEntries;
  }
  
  public void setVoidEntries(Set<DrawerPullVoidEntry> voidEntries) {
    this.voidEntries = voidEntries;
    if (voidEntries != null) {
      double totalVoidAmount = 0.0D;
      for (DrawerPullVoidEntry drawerPullVoidEntry : voidEntries) {
        totalVoidAmount += drawerPullVoidEntry.getAmount().doubleValue();
      }
      setTotalVoid(Double.valueOf(totalVoidAmount));
    }
  }
  
  public boolean isOpen() {
    return getReportTime() == null;
  }
  
  public boolean isClosed() {
    return !isOpen();
  }
  
  public Double getCardVoidAmount() {
    return cardVoidAmount == null ? new Double(0.0D) : cardVoidAmount;
  }
  
  public void setCardVoidAmount(Double cardVoidAmount) {
    this.cardVoidAmount = cardVoidAmount;
  }
}
