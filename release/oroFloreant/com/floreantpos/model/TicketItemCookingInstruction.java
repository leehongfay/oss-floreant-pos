package com.floreantpos.model;

import com.floreantpos.model.base.BaseTicketItemCookingInstruction;
import com.floreantpos.model.ext.KitchenStatus;
import org.json.JSONObject;




















public class TicketItemCookingInstruction
  extends BaseTicketItemCookingInstruction
  implements ITicketItem
{
  private static final long serialVersionUID = 1L;
  private int tableRowNum;
  
  public TicketItemCookingInstruction() {}
  
  private int index = 1;
  
  public int getTableRowNum() {
    return tableRowNum;
  }
  
  public void setTableRowNum(int tableRowNum) {
    this.tableRowNum = tableRowNum;
  }
  
  public boolean canAddCookingInstruction() {
    return false;
  }
  
  public String toString()
  {
    return getDescription();
  }
  
  public String getNameDisplay()
  {
    return "   * " + getDescription();
  }
  
  public Double getUnitPriceDisplay()
  {
    return null;
  }
  
  public String getItemQuantityDisplay()
  {
    return null;
  }
  
  public Double getTaxAmountWithoutModifiersDisplay()
  {
    return null;
  }
  
  public Double getTotalAmountWithoutModifiersDisplay()
  {
    return null;
  }
  
  public Double getSubTotalAmountWithoutModifiersDisplay()
  {
    return null;
  }
  
  public String getItemCode()
  {
    return "";
  }
  
  public boolean canAddDiscount()
  {
    return false;
  }
  
  public boolean canVoid()
  {
    return false;
  }
  
  public boolean canAddAdOn()
  {
    return false;
  }
  

  private String ticketItemId;
  public void setDiscountAmount(Double amount) {}
  
  public Double getDiscountAmount()
  {
    return null;
  }
  
  public KitchenStatus getKitchenStatusValue()
  {
    return null;
  }
  
  public Double getSubTotalAmountDisplay()
  {
    return null;
  }
  

  public String getTicketItemId()
  {
    return ticketItemId;
  }
  
  public void setTicketItemId(String ticketItemId) {
    this.ticketItemId = ticketItemId;
  }
  
  public void setPrintedToKitchen(Boolean printedToKitchen) {
    super.setPrintedToKitchen(printedToKitchen);
  }
  
  public int getIndex() {
    return index;
  }
  
  public void setIndex(int index) {
    this.index = index;
  }
  
  public JSONObject toJson() {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put(PROP_DESCRIPTION, getDescription());
    jsonObject.put(PROP_PRINTED_TO_KITCHEN, isPrintedToKitchen());
    jsonObject.put(PROP_SAVED, isSaved());
    return jsonObject;
  }
}
