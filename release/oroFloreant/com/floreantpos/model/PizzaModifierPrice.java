package com.floreantpos.model;

import com.floreantpos.model.base.BasePizzaModifierPrice;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;




public class PizzaModifierPrice
  extends BasePizzaModifierPrice
{
  private static final long serialVersionUID = 1L;
  
  public PizzaModifierPrice() {}
  
  public PizzaModifierPrice(String id)
  {
    super(id);
  }
  


  private Map<String, ModifierMultiplierPrice> priceMap = new HashMap();
  
  public double getPrice() {
    return 0.0D;
  }
  
  public double getExtraPrice() {
    return 0.0D;
  }
  

  public void setPrice(double price) {}
  

  public void setExtraPrice(double price) {}
  

  public void initializeSizeAndPriceList(List<Multiplier> multipliers)
  {
    List<ModifierMultiplierPrice> priceList = getMultiplierPriceList();
    if (priceList == null) {
      priceList = new ArrayList();
    }
    for (ModifierMultiplierPrice price : priceList) {
      priceMap.put(price.getMultiplier().getId(), price);
    }
    for (Multiplier multiplier : multipliers) {
      ModifierMultiplierPrice priceItem = (ModifierMultiplierPrice)priceMap.get(multiplier.getId());
      if (priceItem == null) {
        priceItem = new ModifierMultiplierPrice();
        priceItem.setMultiplier(multiplier);
        priceList.add(priceItem);
        priceMap.put(multiplier.getId(), priceItem);
      }
    }
    setMultiplierPriceList(priceList);
  }
  
  public ModifierMultiplierPrice getMultiplier(String columnName) {
    return (ModifierMultiplierPrice)priceMap.get(columnName);
  }
  
  public void populateMultiplierPriceListRowValue(MenuModifier modifier) {
    for (Iterator iterator = getMultiplierPriceList().iterator(); iterator.hasNext();) {
      ModifierMultiplierPrice price = (ModifierMultiplierPrice)iterator.next();
      if (price.getPrice() == null) {
        iterator.remove();
      }
      else {
        price.setModifierId(modifier.getId());
        price.setPizzaModifierPriceId(getId());
      }
    }
  }
  
  public String toString()
  {
    return getId();
  }
}
