package com.floreantpos.model;

import com.floreantpos.model.base.BaseShopSeat;


public class ShopSeat
  extends BaseShopSeat
{
  private static final long serialVersionUID = 1L;
  private Customer member;
  
  public ShopSeat() {}
  
  public ShopSeat(String id)
  {
    super(id);
  }
  


  public ShopSeat(ShopTable table, int seatNumber, int x, int y)
  {
    setTableId(table.getId());
    setSeatNumber(Integer.valueOf(seatNumber));
    setPosX(Integer.valueOf(x));
    setPosY(Integer.valueOf(y));
  }
  
  public String toString()
  {
    return String.valueOf(super.getSeatNumber());
  }
  
  public TicketItemSeat convertTicketItemSeat() {
    TicketItemSeat ticketItemSeat = new TicketItemSeat();
    ticketItemSeat.setShopSeat(this);
    return ticketItemSeat;
  }
  
  public Customer getMember()
  {
    return member;
  }
  
  public void setMember(Customer member) {
    this.member = member;
  }
}
