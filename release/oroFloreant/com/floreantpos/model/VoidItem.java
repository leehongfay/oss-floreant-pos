package com.floreantpos.model;

import com.floreantpos.model.base.BaseVoidItem;
import com.floreantpos.util.NumberUtil;
import java.util.List;




public class VoidItem
  extends BaseVoidItem
{
  private static final long serialVersionUID = 1L;
  private String itemId;
  private boolean cooked;
  private Double taxAmount;
  private List<VoidItem> voidedModifiers;
  
  public VoidItem() {}
  
  public VoidItem(String id)
  {
    super(id);
  }
  

  public VoidItem(String voidReason, boolean wasted, double qty)
  {
    setVoidReason(voidReason);
    setItemWasted(Boolean.valueOf(wasted));
    setQuantity(Double.valueOf(qty));
  }
  
  public String getItemId() {
    return itemId;
  }
  
  public void setItemId(String itemId) {
    this.itemId = itemId;
  }
  
  public boolean isCooked() {
    return cooked;
  }
  
  public void setCooked(boolean b) {
    cooked = b;
  }
  
  public Double getTaxAmount() {
    return taxAmount == null ? new Double(0.0D) : taxAmount;
  }
  
  public void setTaxAmount(Double taxAmount) {
    this.taxAmount = taxAmount;
  }
  
  public void setTotalPrice(Double totalPrice)
  {
    super.setTotalPrice(Double.valueOf(NumberUtil.roundToTwoDigit(totalPrice.doubleValue())));
  }
  
  public void setVoidModifiers(List<VoidItem> modifierVoidItems) {
    voidedModifiers = modifierVoidItems;
  }
  
  public List<VoidItem> getVoidedModifiers() {
    return voidedModifiers;
  }
}
