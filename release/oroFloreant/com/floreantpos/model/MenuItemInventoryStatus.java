package com.floreantpos.model;

import com.floreantpos.model.base.BaseMenuItemInventoryStatus;





public class MenuItemInventoryStatus
  extends BaseMenuItemInventoryStatus
{
  private static final long serialVersionUID = 1L;
  
  public MenuItemInventoryStatus() {}
  
  public MenuItemInventoryStatus(String id)
  {
    super(id);
  }
}
