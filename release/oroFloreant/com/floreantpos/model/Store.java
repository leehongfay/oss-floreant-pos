package com.floreantpos.model;

import com.floreantpos.PosException;
import com.floreantpos.main.Application;
import com.floreantpos.model.base.BaseStore;
import com.floreantpos.model.dao.ImageResourceDAO;
import com.floreantpos.util.POSUtil;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang.StringUtils;























@XmlRootElement(name="restaurant")
public class Store
  extends BaseStore
{
  private static final long serialVersionUID = 1L;
  public static final String STORE_PROP_DEFAULT_RESERVATION_OWNER = "Reservation.owner";
  public static final String STORE_PROP_DEFAULT_RESERVATION_ORDERTYPE = "Reservation.ordertype";
  public static final String STORE_PROP_RESERVATION_MERGE_IF_SAME_USER = "Reservation.Merge_If_Same_Member";
  public static final String STORE_PROP_RESERVATION_LOGGED_IN_USER = "Reservation.Logged_In_User_As_Owner";
  public static final String STORE_PROP_RESERVATION_DEFAULT_SALES_AREA = "Reservation.Default_Sales_Area";
  public static final String STORE_PROP_AUTOMATICALLY_SYNC_CHARGES_WHEN_DAY_CLOSED = "Charges.Automatically_Sync_Day";
  private ImageIcon storeLogo;
  private ImageIcon loginScreenBackground;
  
  public Store() {}
  
  public Store(String id)
  {
    super(id);
  }
  
  public boolean hasProperty(String key)
  {
    return getProperty(key) != null;
  }
  
  public String getProperty(String key) {
    return (String)getProperties().get(key);
  }
  
  public String getProperty(String key, String defaultValue) {
    String string = (String)getProperties().get(key);
    if (StringUtils.isEmpty(string)) {
      string = defaultValue;
    }
    return string;
  }
  
  public boolean getBooleanProperty(String key, boolean defaultValue) {
    String string = (String)getProperties().get(key);
    if (StringUtils.isEmpty(string)) {
      return defaultValue;
    }
    return Boolean.valueOf(string).booleanValue();
  }
  
  public void addProperty(String key, String value) {
    getProperties().put(key, value);
  }
  
  public String getCurrencyName()
  {
    String currencyName = super.getCurrencyName();
    if (StringUtils.isEmpty(currencyName)) {
      return "Sample Currency";
    }
    return currencyName;
  }
  
  public String getCurrencySymbol()
  {
    String currencySymbol = super.getCurrencySymbol();
    if (StringUtils.isEmpty(currencySymbol)) {
      currencySymbol = "$";
    }
    return currencySymbol;
  }
  
  public boolean isUpdateOnHandBlncForSale() {
    String update = getProperty("inventory.updateOnHandBlnceForSale");
    if (StringUtils.isEmpty(update)) {
      return true;
    }
    return Boolean.valueOf(update).booleanValue();
  }
  
  public boolean isUpdateAvlBlncForSale() {
    String update = getProperty("inventory.updateAvlBlnceForSale");
    if (StringUtils.isEmpty(update)) {
      return true;
    }
    return Boolean.valueOf(update).booleanValue();
  }
  
  public boolean isAllwNegOnHandBlnce() {
    String update = getProperty("inventory.allowNegetiveOnHandBalance");
    if (StringUtils.isEmpty(update)) {
      return false;
    }
    return Boolean.valueOf(update).booleanValue();
  }
  
  public boolean isUpdateAvlBlncForPOCreated() {
    String update = getProperty("inventory.updateAvailBalanceForPurchaseOrderCreated");
    if (StringUtils.isEmpty(update)) {
      return false;
    }
    return Boolean.valueOf(update).booleanValue();
  }
  
  public boolean isUpdateOnHandBlncForPORec() {
    String update = getProperty("inventory.updateOnHandBalanceForPurchaseOrderReceived");
    if (StringUtils.isEmpty(update)) {
      return true;
    }
    return Boolean.valueOf(update).booleanValue();
  }
  
  public ImageIcon getStoreLogo() {
    if (storeLogo == null) {
      String storeLogoResourceId = getProperty("ticket.header.logo.imageid");
      if (storeLogoResourceId == null)
        return null;
      ImageResource logoResource = ImageResourceDAO.getInstance().findById(storeLogoResourceId);
      if (logoResource != null) {
        storeLogo = new ImageIcon(logoResource.getImage());
        if ((storeLogo.getIconWidth() > 128) || (storeLogo.getIconHeight() > 128)) {
          storeLogo = new ImageIcon(POSUtil.getScaledImage(logoResource.getImage(), 128, 128));
        }
      }
    }
    return storeLogo;
  }
  
  public boolean isInventoryAvgPricingMethod() {
    String property = getProperty("inventory.pricing.method");
    if (property == null) {
      return false;
    }
    return property.equals("avg");
  }
  
  public void setStoreLogo(ImageIcon storeLogo) {
    this.storeLogo = storeLogo;
  }
  
  public ImageIcon getLoginScreenBackground() {
    if (loginScreenBackground == null) {
      String resourceId = getProperty("loginscreen.background");
      if (resourceId == null)
        return null;
      ImageResource resource = ImageResourceDAO.getInstance().findById(resourceId);
      if (resource != null) {
        loginScreenBackground = new ImageIcon(resource.getImage());
      }
    }
    return loginScreenBackground;
  }
  
  public void setLoginScreenBackground(ImageIcon loginScreenBackground) {
    this.loginScreenBackground = loginScreenBackground;
  }
  
  public static String getWebServiceUrl() {
    Store store = Application.getInstance().getStore();
    return store.getProperty("web.service.url") + "/service/data/store/" + store.getProperty("web.service.schema");
  }
  
  public TipsReceivedBy getTipsReceivedByForDeliveryOrder() {
    try {
      String tipsReceiver = getProperty("tips.receiver.delivery");
      if (StringUtils.isNotEmpty(tipsReceiver)) {
        return TipsReceivedBy.valueOf(tipsReceiver);
      }
      return TipsReceivedBy.Server;
    } catch (Exception e) {
      throw new PosException("Tips received by configuration is wrong.");
    }
  }
  
  public TipsReceivedBy getTipsReceivedByForNonDeliveryOrder() {
    try {
      String tipsReceiver = getProperty("tips.receiver.nonDelivery");
      if (StringUtils.isNotEmpty(tipsReceiver)) {
        return TipsReceivedBy.valueOf(tipsReceiver);
      }
      return TipsReceivedBy.Server;
    } catch (Exception e) {
      throw new PosException("Tips received by configuration is wrong.");
    }
  }
  
  public double getOvertimeMarkup() {
    try {
      String overtimeMarkup = getProperty("overtime.markup");
      if (StringUtils.isNotEmpty(overtimeMarkup)) {
        return Double.parseDouble(overtimeMarkup);
      }
    }
    catch (Exception localException) {}
    
    return 0.0D;
  }
  
  public boolean hasCreateMemberPermission() {
    try {
      String memberCreate = getProperty("member.create.permission");
      if (StringUtils.isNotEmpty(memberCreate)) {
        return Boolean.valueOf(memberCreate).booleanValue();
      }
    }
    catch (Exception localException) {}
    
    return true;
  }
  
  public Integer getDatabaseVersion() {
    String property = getProperty("database.version");
    try {
      return Integer.valueOf(Integer.parseInt(property));
    } catch (Exception e) {}
    return null;
  }
  
  public void setDatabaseVersion(int verson)
  {
    addProperty("database.version", String.valueOf(verson));
  }
  
  public boolean isAutoSyncCharges() {
    return getBooleanProperty("Charges.Automatically_Sync_Day", Boolean.FALSE.booleanValue());
  }
}
