package com.floreantpos.model;

import com.floreantpos.swing.ListTableModel;
import com.floreantpos.util.NumberUtil;
import java.util.List;
















public class TipsCashoutReportTableModel
  extends ListTableModel
{
  public TipsCashoutReportTableModel()
  {
    super(new String[] { "TICKET ID", "TYPE", "TICKET AMOUNT", "CASH TIPS", "CHAREGD TIPS", "TIPS PAID" });
  }
  
  public TipsCashoutReportTableModel(List<TipsCashoutReportData> datas) {
    super(new String[] { "TICKET ID", "TYPE", "TICKET AMOUNT", "CASH TIPS", "CHAREGD TIPS", "TIPS PAID" }, datas);
  }
  
  public TipsCashoutReportTableModel(List<TipsCashoutReportData> datas, String[] columnNames) {
    super(columnNames, datas);
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    TipsCashoutReportData data = (TipsCashoutReportData)rows.get(rowIndex);
    
    switch (columnIndex) {
    case 0: 
      return data.getTicketId();
    
    case 1: 
      return data.getSaleType();
    
    case 2: 
      return Double.valueOf(NumberUtil.roundToTwoDigit(data.getTicketTotal().doubleValue()));
    
    case 3: 
      return Double.valueOf(NumberUtil.roundToTwoDigit(data.getCashTips().doubleValue()));
    case 4: 
      return Double.valueOf(NumberUtil.roundToTwoDigit(data.getChargedTips().doubleValue()));
    case 5: 
      return Double.valueOf(NumberUtil.roundToTwoDigit(data.getTipsPaidAmount().doubleValue()));
    }
    
    return null;
  }
}
