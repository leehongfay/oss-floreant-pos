package com.floreantpos.model;

import com.floreantpos.model.base.BaseGiftCard;


public class GiftCard
  extends BaseGiftCard
{
  private static final long serialVersionUID = 1L;
  public static final String DURATION_TYPE_DAY = "DAY";
  public static final String DURATION_TYPE_MONTH = "MONTH";
  public static final String DURATION_TYPE_YEAR = "YEAR";
  public String customCardNumber;
  
  public GiftCard() {}
  
  public GiftCard(String cardNumber)
  {
    super(cardNumber);
  }
  


  public void setCustomCardNumber(String customCardNumber)
  {
    this.customCardNumber = customCardNumber;
  }
  
  public String getCustomCardNumber() {
    StringBuilder gcn = new StringBuilder(super.getCardNumber());
    for (int i = 0; i < gcn.length(); i++) {
      if ((i == 4) || (i == 9) || (i == 14)) {
        gcn.insert(i, "-");
      }
    }
    
    return gcn.toString();
  }
  
  public String toString()
  {
    return super.getCardNumber();
  }
}
