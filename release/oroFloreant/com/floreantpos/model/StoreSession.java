package com.floreantpos.model;

import com.floreantpos.model.base.BaseStoreSession;
import com.floreantpos.model.util.DataProvider;
import javax.xml.bind.annotation.XmlRootElement;





@XmlRootElement
public class StoreSession
  extends BaseStoreSession
{
  private static final long serialVersionUID = 1L;
  
  public StoreSession() {}
  
  public StoreSession(String id)
  {
    super(id);
  }
  

  public User getClosedBy()
  {
    return DataProvider.get().getUserById(getClosedByUserId());
  }
  
  public void setClosedBy(User user) {
    if (user == null) {
      setClosedByUserId(null);
    }
    else {
      setClosedByUserId(user.getId());
    }
  }
  
  public User getOpenedBy() {
    return DataProvider.get().getUserById(getOpenedByUserId());
  }
  
  public void setOpenedBy(User user) {
    if (user == null) {
      setOpenedByUserId(null);
    }
    else {
      setOpenedByUserId(user.getId());
    }
  }
}
