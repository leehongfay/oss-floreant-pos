package com.floreantpos.model;

import com.floreantpos.model.base.BaseSalesArea;
import com.floreantpos.model.dao.DepartmentDAO;
import org.apache.commons.lang.StringUtils;



public class SalesArea
  extends BaseSalesArea
  implements IdContainer
{
  private static final long serialVersionUID = 1L;
  
  public SalesArea() {}
  
  public SalesArea(String id)
  {
    super(id);
  }
  


  public String toString()
  {
    return super.getName();
  }
  
  public Department getDepartment() {
    if (StringUtils.isNotEmpty(getDepartmentId())) {
      return DepartmentDAO.getInstance().get(getDepartmentId());
    }
    return null;
  }
  
  public void setDepartment(Department department) {
    String deptId = null;
    if (department != null) {
      deptId = department.getId();
    }
    super.setDepartmentId(deptId);
  }
}
