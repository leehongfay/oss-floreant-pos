package com.floreantpos.model;

import com.floreantpos.model.base.BaseKitchenTicket;
import com.floreantpos.model.dao.KitchenTicketDAO;
import com.floreantpos.model.dao.StoreDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.ext.KitchenStatus;
import com.floreantpos.model.util.DataProvider;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.StringUtils;
















public class KitchenTicket
  extends BaseKitchenTicket
{
  private static final long serialVersionUID = 1L;
  private String customerName;
  private Printer printer;
  private Ticket parentTicket;
  private Boolean filterItem;
  private Integer sortOrder;
  
  public KitchenTicket() {}
  
  public KitchenTicket(String id)
  {
    super(id);
  }
  







  public KitchenStatus getKitchenStatusValue()
  {
    return KitchenStatus.fromString(super.getStatus());
  }
  
  public void setKitchenStatusValue(KitchenStatus kitchenStatus) {
    super.setStatus(kitchenStatus.name());
  }
  
  public List<KitchenTicketItem> getModifiersForTicketItem(String ticketItemId) {
    List<KitchenTicketItem> modifiers = new ArrayList();
    List<KitchenTicketItem> ticketItems = getTicketItems();
    if (ticketItems != null) {
      for (KitchenTicketItem kitchenTicketItem : ticketItems) {
        if ((kitchenTicketItem.isModifierItem()) && (ticketItemId.equals(kitchenTicketItem.getTicketItemId()))) {
          modifiers.add(kitchenTicketItem);
        }
      }
    }
    return modifiers;
  }
  
  public List<KitchenTicketItem> getCookingInstructionForTicketItem(String ticketItemId) {
    List<KitchenTicketItem> instructions = new ArrayList();
    List<KitchenTicketItem> ticketItems = getTicketItems();
    if (ticketItems != null) {
      for (KitchenTicketItem kitchenTicketItem : ticketItems) {
        if ((kitchenTicketItem.isCookingInstruction()) && (ticketItemId.equals(kitchenTicketItem.getTicketItemId()))) {
          instructions.add(kitchenTicketItem);
        }
      }
    }
    return instructions;
  }
  
  public void setPrinter(Printer printer) {
    this.printer = printer;
  }
  
  public Printer getPrinter() {
    return printer;
  }
  
  public List<Printer> getPrinters() {
    List<Printer> printers = new ArrayList();
    
    PosPrinters posPrinters = PosPrinters.load();
    PrinterGroup virtualPrinter = getPrinterGroup();
    
    if (virtualPrinter == null) {
      printers.add(posPrinters.getDefaultKitchenPrinter());
      return printers;
    }
    
    List<String> printerNames = virtualPrinter.getPrinterNames();
    List<Printer> kitchenPrinters = posPrinters.getKitchenPrinters();
    for (Printer printer : kitchenPrinters) {
      if (printerNames.contains(printer.getVirtualPrinter().getName())) {
        printers.add(printer);
      }
    }
    
    if (printers.size() == 0) {
      printers.add(posPrinters.getDefaultKitchenPrinter());
    }
    
    return printers;
  }
  
  public List<KitchenTicketItem> getTicketItems()
  {
    List<KitchenTicketItem> items = super.getTicketItems();
    
    if (items == null) {
      items = new ArrayList();
      super.setTicketItems(items);
    }
    return items;
  }
  
  public static List<KitchenTicket> fromTicket(Ticket ticket, boolean isFilterKitchenPrintedItems) {
    return fromTicket(ticket, isFilterKitchenPrintedItems, null);
  }
  
  public static List<KitchenTicket> fromTicket(Ticket ticket, boolean isFilterKitchenPrintedItems, List<TicketItem> ticketItems) {
    Map<Printer, KitchenTicket> itemMap = new HashMap();
    List<KitchenTicket> kitchenTickets = new ArrayList(4);
    
    Ticket clonedTicket = (Ticket)SerializationUtils.clone(ticket);
    boolean isItemBasedKitPrint = ticketItems == null;
    
    if (ticketItems != null) {
      clonedTicket.consolidateKitchenTicketItems(isFilterKitchenPrintedItems, ticketItems);
    }
    else {
      ticketItems = clonedTicket.getTicketItems();
      clonedTicket.consolidateKitchenTicketItems(isFilterKitchenPrintedItems);
    }
    
    if (ticketItems == null) {
      return kitchenTickets;
    }
    OrderType orderType = ticket.getOrderType();
    if (orderType.isEnableCourse().booleanValue()) {
      Collections.sort(ticketItems, new Comparator()
      {
        public int compare(TicketItem o1, TicketItem o2) {
          if ((o1.getCourseId() == null) || (o2.getCourseId() == null))
            return 0;
          return o1.getCourseId().compareTo(o2.getCourseId());
        }
      });
      Collections.sort(ticketItems, new Comparator()
      {
        public int compare(TicketItem o1, TicketItem o2) {
          return o1.getSortOrder().compareTo(o2.getSortOrder());
        }
      });
    }
    if (orderType.isAllowSeatBasedOrder().booleanValue()) {
      Collections.sort(ticketItems, new Comparator()
      {
        public int compare(TicketItem o1, TicketItem o2) {
          if ((o1.getId() == null) || (o2.getId() == null))
            return 0;
          return o1.getId().compareTo(o2.getId());
        }
      });
      Collections.sort(ticketItems, new Comparator()
      {
        public int compare(TicketItem o1, TicketItem o2) {
          return o1.getSeatNumber().intValue() - o2.getSeatNumber().intValue();
        }
      });
    }
    
    for (Iterator localIterator1 = ticketItems.iterator(); localIterator1.hasNext();) { ticketItem = (TicketItem)localIterator1.next();
      List<Printer> printers = ticketItem.getPrinters(orderType);
      if (printers != null)
      {


        for (Printer printer : printers) {
          KitchenTicket kitchenTicket = (KitchenTicket)itemMap.get(printer);
          if (kitchenTicket == null) {
            kitchenTicket = new KitchenTicket();
            kitchenTicket.setPrinterGroup(ticketItem.getPrinterGroup());
            kitchenTicket.setTicketId(ticket.getId());
            kitchenTicket.setTokenNo(ticket.getTokenNo());
            kitchenTicket.setCreateDate(StoreDAO.geServerTimestamp());
            kitchenTicket.setOrderType(orderType);
            
            if (ticket.getTableNumbers() != null) {
              kitchenTicket.setTableNumbers(new ArrayList(ticket.getTableNumbers()));
            }
            
            kitchenTicket.setServerName(ticket.getOwner().getFirstName());
            kitchenTicket.setStatus(KitchenStatus.WAITING.name());
            
            if (StringUtils.isNotEmpty(ticket.getProperty("CUSTOMER_NAME"))) {
              kitchenTicket.setCustomerName(ticket.getProperty("CUSTOMER_NAME"));
            }
            KitchenTicketDAO.getInstance().saveOrUpdate(kitchenTicket);
            
            kitchenTicket.setPrinter(printer);
            VirtualPrinter virtualPrinter = printer.getVirtualPrinter();
            if (virtualPrinter != null)
              kitchenTicket.setPrinterName(virtualPrinter.getName());
            itemMap.put(printer, kitchenTicket);
          }
          
          KitchenTicketItem item = new KitchenTicketItem();
          item.setTicketItemId(ticketItem.getId());
          item.setMenuItemCode(ticketItem.getItemCode());
          item.setVoided(ticketItem.isVoided());
          item.setVoidedItemId(ticketItem.getVoidedItemId());
          item.setMenuItemName(ticketItem.getName());
          item.setPrintKitchenSticker(ticketItem.isPrintKitchenSticker());
          if (ticketItem.getMenuItem() == null) {
            item.setMenuItemGroupName("MISC.");
            
            item.setSortOrder(Integer.valueOf(10001));
          }
          else {
            item.setMenuItemGroupName(ticketItem.getGroupName());
            
            item.setSortOrder(Integer.valueOf(ticketItem.getTableRowNum()));
          }
          item.setCourseId(ticketItem.getCourseId());
          item.setUnitName(ticketItem.getUnitName());
          item.setQuantity(ticketItem.getQuantity());
          item.setKitchenStatusValue(KitchenStatus.WAITING);
          item.setKitchenTicket(kitchenTicket);
          
          kitchenTicket.addToticketItems(item);
          
          ticketItem.setPrintedToKitchen(Boolean.valueOf(true));
          
          includeModifiers(ticketItem, kitchenTicket);
          includeCookintInstructions(ticketItem, kitchenTicket);
        }
      }
    }
    
    TicketItem ticketItem;
    Object values = itemMap.values();
    
    for (KitchenTicket kitchenTicket : (Collection)values) {
      kitchenTicket.setParentTicket(ticket);
      kitchenTicket.setFilterItem(Boolean.valueOf(isFilterKitchenPrintedItems));
      kitchenTickets.add(kitchenTicket);
      String kitchenTicketNumber = ticket.getProperty("KITCHEN_TICKET_NUMBER");
      if (kitchenTicketNumber == null) {
        kitchenTicketNumber = "1";
      }
      else {
        kitchenTicketNumber = String.valueOf(Integer.valueOf(kitchenTicketNumber).intValue() + 1);
      }
      ticket.addProperty("KITCHEN_TICKET_NUMBER", kitchenTicketNumber);
      kitchenTicket.setSequenceNumber(Integer.valueOf(kitchenTicketNumber));
    }
    if (!isItemBasedKitPrint) {
      ticket.markPrintedToKitchen(ticketItems);
    }
    else {
      ticket.markPrintedToKitchen();
    }
    return kitchenTickets;
  }
  
  private static void includeCookintInstructions(TicketItem ticketItem, KitchenTicket kitchenTicket) {
    List<TicketItemCookingInstruction> cookingInstructions = ticketItem.getCookingInstructions();
    if (cookingInstructions != null) {
      for (TicketItemCookingInstruction ticketItemCookingInstruction : cookingInstructions) {
        KitchenTicketItem item = new KitchenTicketItem();
        item.setCookable(Boolean.valueOf(false));
        item.setTicketItemId(ticketItem.getId());
        item.setMenuItemName(ticketItemCookingInstruction.getNameDisplay());
        if (ticketItem.getMenuItem() == null) {
          item.setMenuItemGroupName("MISC.");
          
          item.setSortOrder(Integer.valueOf(10001));
        }
        else {
          item.setMenuItemGroupName(ticketItem.getGroupName());
        }
        

        item.setKitchenTicket(kitchenTicket);
        kitchenTicket.addToticketItems(item);
      }
    }
  }
  
  private static void includeModifiers(TicketItem ticketItem, KitchenTicket kitchenTicket) {
    List<TicketItemModifier> ticketItemModifiers = ticketItem.getTicketItemModifiers();
    if (ticketItemModifiers != null) {
      for (TicketItemModifier itemModifier : ticketItemModifiers) {
        if (itemModifier.isShouldPrintToKitchen().booleanValue())
        {


          KitchenTicketItem item = new KitchenTicketItem();
          item.setTicketItemId(ticketItem.getId());
          item.setTicketItemModifierId(itemModifier.getId());
          item.setMenuItemName(itemModifier.getNameDisplay(true));
          if (ticketItem.getMenuItem() == null) {
            item.setMenuItemGroupName("MISC.");
            
            item.setSortOrder(Integer.valueOf(10001));
          }
          else {
            item.setMenuItemGroupName(ticketItem.getGroupName());
          }
          


          item.setQuantity(itemModifier.getItemQuantity());
          item.setKitchenStatusValue(KitchenStatus.WAITING);
          item.setKitchenTicket(kitchenTicket);
          kitchenTicket.addToticketItems(item);
          
          itemModifier.setPrintedToKitchen(Boolean.valueOf(true));
        }
      }
    }
  }
  


  public String getCustomerName()
  {
    return customerName;
  }
  
  public void setCustomerName(String customerName) {
    this.customerName = customerName;
  }
  
  public Ticket getParentTicket() {
    if (parentTicket == null) {
      parentTicket = TicketDAO.getInstance().get(getTicketId());
    }
    return parentTicket;
  }
  
  public void setParentTicket(Ticket parentTicket) {
    this.parentTicket = parentTicket;
  }
  
  public Boolean isFilterItem() {
    return filterItem;
  }
  
  public void setFilterItem(Boolean filterItem) {
    this.filterItem = filterItem;
  }
  
  public void setOrderType(OrderType orderType) {
    if (orderType != null) {
      super.setOrderTypeId(orderType.getId());
    } else
      super.setOrderTypeId(null);
  }
  
  public OrderType getOrderType() {
    if (StringUtils.isEmpty(getOrderTypeId()))
      return null;
    return DataProvider.get().getOrderType(getOrderTypeId());
  }
  
  public void setSortOrder(Integer sortOrder) {
    this.sortOrder = sortOrder;
  }
  
  public Integer getSortOrder() {
    return Integer.valueOf(sortOrder == null ? 0 : sortOrder.intValue());
  }
}
