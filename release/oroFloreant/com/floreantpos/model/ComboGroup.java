package com.floreantpos.model;

import com.floreantpos.model.base.BaseComboGroup;
import java.util.List;



public class ComboGroup
  extends BaseComboGroup
{
  private static final long serialVersionUID = 1L;
  private List<String> itemsId;
  
  public ComboGroup() {}
  
  public ComboGroup(String id)
  {
    super(id);
  }
  


  public List<String> getItemsId()
  {
    return itemsId;
  }
  
  public void setItemsId(List<String> itemsId) {
    this.itemsId = itemsId;
  }
}
