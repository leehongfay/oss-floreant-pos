package com.floreantpos.model;

import com.floreantpos.model.base.BaseEmployeeShift;





public class EmployeeShift
  extends BaseEmployeeShift
{
  private static final long serialVersionUID = 1L;
  
  public EmployeeShift() {}
  
  public EmployeeShift(String id)
  {
    super(id);
  }
  





  public EmployeeShift(String id, String name)
  {
    super(id, name);
  }
}
