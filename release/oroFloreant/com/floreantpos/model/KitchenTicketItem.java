package com.floreantpos.model;

import com.floreantpos.model.base.BaseKitchenTicketItem;
import com.floreantpos.model.ext.KitchenStatus;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.util.NumberUtil;
import java.awt.Image;
import javax.swing.ImageIcon;
import org.apache.commons.lang.StringUtils;
























public class KitchenTicketItem
  extends BaseKitchenTicketItem
{
  private static final long serialVersionUID = 1L;
  private String voidedItemId;
  
  public KitchenTicketItem() {}
  
  public KitchenTicketItem(String id)
  {
    super(id);
  }
  

  public KitchenStatus getKitchenStatusValue()
  {
    return KitchenStatus.fromString(super.getStatus());
  }
  
  public void setKitchenStatusValue(KitchenStatus kitchenStatus) {
    super.setStatus(kitchenStatus.name());
  }
  
  public boolean isModifierItem() {
    return getTicketItemModifierId() != null;
  }
  
  public boolean isCookingInstruction() {
    return (!isCookable().booleanValue()) && (getTicketItemModifierId() == null);
  }
  


  public Boolean isCookable()
  {
    return cookable == null ? Boolean.TRUE : cookable;
  }
  
  public String getMenuItemGroupName() {
    if (super.getMenuItemGroupName() == null) {
      return "";
    }
    return super.getMenuItemGroupName();
  }
  
  public Image getCourseIcon() {
    if (StringUtils.isEmpty(getCourseId()))
      return null;
    Course course = DataProvider.get().getCourse(getCourseId());
    ImageIcon imageIcon = course == null ? null : course.getIcon();
    if (imageIcon != null)
      return imageIcon.getImage();
    return null;
  }
  
  public String getVoidedItemId() {
    return voidedItemId;
  }
  
  public void setVoidedItemId(String voidedItemId) {
    this.voidedItemId = voidedItemId;
  }
  
  public String getMenuItemNameDisplay() {
    String displayName = NumberUtil.trimDecilamIfNotNeeded(getQuantity()) + "x ";
    return displayName + getMenuItemName();
  }
}
