package com.floreantpos.model;

import com.floreantpos.PosLog;
import com.floreantpos.model.base.BaseGratuity;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.util.DataProvider;
import java.util.Set;
import org.apache.commons.lang.StringUtils;





















public class Gratuity
  extends BaseGratuity
{
  private static final long serialVersionUID = 1L;
  private transient boolean syncEdited;
  private transient Ticket ticket;
  private Double transactionTipsAmount;
  
  public Gratuity() {}
  
  public Gratuity(String id)
  {
    super(id);
  }
  











  public void calculateTransactionTips()
  {
    try
    {
      transactionTipsAmount = Double.valueOf(0.0D);
      String ticketId2 = getTicketId();
      if (StringUtils.isEmpty(ticketId2))
        return;
      Ticket ticket = TicketDAO.getInstance().loadCouponsAndTransactions(ticketId2);
      if (ticket == null)
        return;
      Set<PosTransaction> transactions = ticket.getTransactions();
      if ((transactions != null) && (transactions.size() > 0)) {
        for (PosTransaction posTransaction : transactions) {
          if (!posTransaction.isVoided().booleanValue())
          {
            if ((posTransaction instanceof RefundTransaction)) {
              transactionTipsAmount = Double.valueOf(transactionTipsAmount.doubleValue() - posTransaction.getTipsAmount().doubleValue());
            }
            else
              transactionTipsAmount = Double.valueOf(transactionTipsAmount.doubleValue() + posTransaction.getTipsAmount().doubleValue());
          }
        }
      }
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    }
  }
  
  public Double getTransactionTipsAmount() {
    if (transactionTipsAmount == null) {
      calculateTransactionTips();
    }
    return transactionTipsAmount;
  }
  
  public Ticket getTicket() {
    if ((ticket == null) && (StringUtils.isNotEmpty(getTicketId()))) {
      ticket = ((Ticket)DataProvider.get().getObjectOf(Ticket.class, getTicketId()));
    }
    return ticket;
  }
  
  public boolean isSyncEdited() {
    return syncEdited;
  }
  
  public void setSyncEdited(boolean syncEdited) {
    this.syncEdited = syncEdited;
  }
}
