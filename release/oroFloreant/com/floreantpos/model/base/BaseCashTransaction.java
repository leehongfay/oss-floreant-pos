package com.floreantpos.model.base;

import com.floreantpos.model.CashTransaction;
import com.floreantpos.model.PosTransaction;
import java.io.Serializable;








public abstract class BaseCashTransaction
  extends PosTransaction
  implements Comparable, Serializable
{
  public static String REF = "CashTransaction";
  public static String PROP_ID = "id";
  

  public BaseCashTransaction()
  {
    initialize();
  }
  


  public BaseCashTransaction(String id)
  {
    super(id);
  }
  






  public BaseCashTransaction(String id, String transactionType, String paymentType)
  {
    super(id, transactionType, paymentType);
  }
  





  private int hashCode = Integer.MIN_VALUE;
  







  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof CashTransaction)) { return false;
    }
    CashTransaction cashTransaction = (CashTransaction)obj;
    if ((null == getId()) || (null == cashTransaction.getId())) return this == obj;
    return getId().equals(cashTransaction.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
