package com.floreantpos.model.base;

import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.PurchaseOrder;
import com.floreantpos.model.PurchaseOrderItem;
import com.floreantpos.model.Terminal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


public abstract class BasePurchaseOrder
  implements Comparable, Serializable
{
  public static String REF = "PurchaseOrder";
  public static String PROP_SENT_DATE = "sentDate";
  public static String PROP_EXPECTED_DATE = "expectedDate";
  public static String PROP_TERMINAL = "terminal";
  public static String PROP_TYPE = "type";
  public static String PROP_CLOSING_DATE = "closingDate";
  public static String PROP_ORDER_ID = "orderId";
  public static String PROP_LAST_MODIFIED_DATE = "lastModifiedDate";
  public static String PROP_DUE_AMOUNT = "dueAmount";
  public static String PROP_TAX_AMOUNT = "taxAmount";
  public static String PROP_DISCOUNT_AMOUNT = "discountAmount";
  public static String PROP_INVENTORY_LOCATION = "inventoryLocation";
  public static String PROP_RECEIVING_DATE = "receivingDate";
  public static String PROP_NAME = "name";
  public static String PROP_STATUS = "status";
  public static String PROP_SUBTOTAL_AMOUNT = "subtotalAmount";
  public static String PROP_ADVANCE_AMOUNT = "advanceAmount";
  public static String PROP_VENDOR = "vendor";
  public static String PROP_PICK_UP_ADDRESS = "pickUpAddress";
  public static String PROP_CREATED_DATE = "createdDate";
  public static String PROP_ID = "id";
  public static String PROP_TOTAL_AMOUNT = "totalAmount";
  public static String PROP_PAID_AMOUNT = "paidAmount";
  public static String PROP_VARIFICATION_DATE = "varificationDate";
  

  public BasePurchaseOrder()
  {
    initialize();
  }
  


  public BasePurchaseOrder(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  private long version;
  
  protected String type;
  
  protected Date createdDate;
  
  protected Date expectedDate;
  
  protected Date lastModifiedDate;
  
  protected Date varificationDate;
  
  protected Date sentDate;
  
  protected Date receivingDate;
  
  protected Date closingDate;
  
  protected String orderId;
  
  protected String name;
  
  protected Double subtotalAmount;
  
  protected Double discountAmount;
  protected Double taxAmount;
  protected Double totalAmount;
  protected Double paidAmount;
  protected Double dueAmount;
  protected Double advanceAmount;
  protected Integer status;
  protected String pickUpAddress;
  private InventoryLocation inventoryLocation;
  private InventoryVendor vendor;
  private Terminal terminal;
  private List<PurchaseOrderItem> orderItems;
  private Map<String, String> properties;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getType()
  {
    return type;
  }
  



  public void setType(String type)
  {
    this.type = type;
  }
  




  public Date getCreatedDate()
  {
    return createdDate;
  }
  



  public void setCreatedDate(Date createdDate)
  {
    this.createdDate = createdDate;
  }
  




  public Date getExpectedDate()
  {
    return expectedDate;
  }
  



  public void setExpectedDate(Date expectedDate)
  {
    this.expectedDate = expectedDate;
  }
  




  public Date getLastModifiedDate()
  {
    return lastModifiedDate;
  }
  



  public void setLastModifiedDate(Date lastModifiedDate)
  {
    this.lastModifiedDate = lastModifiedDate;
  }
  




  public Date getVarificationDate()
  {
    return varificationDate;
  }
  



  public void setVarificationDate(Date varificationDate)
  {
    this.varificationDate = varificationDate;
  }
  




  public Date getSentDate()
  {
    return sentDate;
  }
  



  public void setSentDate(Date sentDate)
  {
    this.sentDate = sentDate;
  }
  




  public Date getReceivingDate()
  {
    return receivingDate;
  }
  



  public void setReceivingDate(Date receivingDate)
  {
    this.receivingDate = receivingDate;
  }
  




  public Date getClosingDate()
  {
    return closingDate;
  }
  



  public void setClosingDate(Date closingDate)
  {
    this.closingDate = closingDate;
  }
  




  public String getOrderId()
  {
    return orderId;
  }
  



  public void setOrderId(String orderId)
  {
    this.orderId = orderId;
  }
  




  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Double getSubtotalAmount()
  {
    return subtotalAmount == null ? Double.valueOf(0.0D) : subtotalAmount;
  }
  



  public void setSubtotalAmount(Double subtotalAmount)
  {
    this.subtotalAmount = subtotalAmount;
  }
  




  public Double getDiscountAmount()
  {
    return discountAmount == null ? Double.valueOf(0.0D) : discountAmount;
  }
  



  public void setDiscountAmount(Double discountAmount)
  {
    this.discountAmount = discountAmount;
  }
  




  public Double getTaxAmount()
  {
    return taxAmount == null ? Double.valueOf(0.0D) : taxAmount;
  }
  



  public void setTaxAmount(Double taxAmount)
  {
    this.taxAmount = taxAmount;
  }
  




  public Double getTotalAmount()
  {
    return totalAmount == null ? Double.valueOf(0.0D) : totalAmount;
  }
  



  public void setTotalAmount(Double totalAmount)
  {
    this.totalAmount = totalAmount;
  }
  




  public Double getPaidAmount()
  {
    return paidAmount == null ? Double.valueOf(0.0D) : paidAmount;
  }
  



  public void setPaidAmount(Double paidAmount)
  {
    this.paidAmount = paidAmount;
  }
  




  public Double getDueAmount()
  {
    return dueAmount == null ? Double.valueOf(0.0D) : dueAmount;
  }
  



  public void setDueAmount(Double dueAmount)
  {
    this.dueAmount = dueAmount;
  }
  




  public Double getAdvanceAmount()
  {
    return advanceAmount == null ? Double.valueOf(0.0D) : advanceAmount;
  }
  



  public void setAdvanceAmount(Double advanceAmount)
  {
    this.advanceAmount = advanceAmount;
  }
  




  public Integer getStatus()
  {
    return status == null ? Integer.valueOf(0) : status;
  }
  



  public void setStatus(Integer status)
  {
    this.status = status;
  }
  




  public String getPickUpAddress()
  {
    return pickUpAddress;
  }
  



  public void setPickUpAddress(String pickUpAddress)
  {
    this.pickUpAddress = pickUpAddress;
  }
  




  public InventoryLocation getInventoryLocation()
  {
    return inventoryLocation;
  }
  



  public void setInventoryLocation(InventoryLocation inventoryLocation)
  {
    this.inventoryLocation = inventoryLocation;
  }
  




  public InventoryVendor getVendor()
  {
    return vendor;
  }
  



  public void setVendor(InventoryVendor vendor)
  {
    this.vendor = vendor;
  }
  




  public Terminal getTerminal()
  {
    return terminal;
  }
  



  public void setTerminal(Terminal terminal)
  {
    this.terminal = terminal;
  }
  




  public List<PurchaseOrderItem> getOrderItems()
  {
    return orderItems;
  }
  



  public void setOrderItems(List<PurchaseOrderItem> orderItems)
  {
    this.orderItems = orderItems;
  }
  
  public void addToorderItems(PurchaseOrderItem purchaseOrderItem) {
    if (null == getOrderItems()) setOrderItems(new ArrayList());
    getOrderItems().add(purchaseOrderItem);
  }
  




  public Map<String, String> getProperties()
  {
    return properties;
  }
  



  public void setProperties(Map<String, String> properties)
  {
    this.properties = properties;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof PurchaseOrder)) { return false;
    }
    PurchaseOrder purchaseOrder = (PurchaseOrder)obj;
    if ((null == getId()) || (null == purchaseOrder.getId())) return this == obj;
    return getId().equals(purchaseOrder.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
