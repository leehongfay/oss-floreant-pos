package com.floreantpos.model.base;

import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.MenuItem;
import java.io.Serializable;
import java.util.Date;








public abstract class BaseInventoryTransaction
  implements Comparable, Serializable
{
  public static String REF = "InventoryTransaction";
  public static String PROP_FROM_LOCATION_ID = "fromLocationId";
  public static String PROP_QUANTITY = "quantity";
  public static String PROP_BASE_UNIT_QUANTITY = "baseUnitQuantity";
  public static String PROP_TRANSACTION_DATE = "transactionDate";
  public static String PROP_TYPE = "type";
  public static String PROP_USER_ID = "userId";
  public static String PROP_UNIT_PRICE = "unitPrice";
  public static String PROP_TICKET_ID = "ticketId";
  public static String PROP_REFERENCE_NO = "referenceNo";
  public static String PROP_UNIT = "unit";
  public static String PROP_BASE_UNIT = "baseUnit";
  public static String PROP_MENU_ITEM = "menuItem";
  public static String PROP_VENDOR_ID = "vendorId";
  public static String PROP_ID = "id";
  public static String PROP_REMARK = "remark";
  public static String PROP_TO_LOCATION_ID = "toLocationId";
  public static String PROP_UNIT_COST = "unitCost";
  public static String PROP_TOTAL = "total";
  public static String PROP_REASON = "reason";
  

  public BaseInventoryTransaction()
  {
    initialize();
  }
  


  public BaseInventoryTransaction(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected Date transactionDate;
  
  protected String reason;
  
  protected Double quantity;
  
  protected String unit;
  
  protected Double unitPrice;
  
  protected Double unitCost;
  
  protected Double baseUnitQuantity;
  
  protected String baseUnit;
  
  protected String remark;
  
  protected Double total;
  protected String ticketId;
  protected String referenceNo;
  protected String fromLocationId;
  protected String toLocationId;
  protected String vendorId;
  protected String userId;
  protected Integer type;
  private MenuItem menuItem;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Date getTransactionDate()
  {
    return transactionDate;
  }
  



  public void setTransactionDate(Date transactionDate)
  {
    this.transactionDate = transactionDate;
  }
  




  public String getReason()
  {
    return reason;
  }
  



  public void setReason(String reason)
  {
    this.reason = reason;
  }
  




  public Double getQuantity()
  {
    return quantity == null ? Double.valueOf(0.0D) : quantity;
  }
  



  public void setQuantity(Double quantity)
  {
    this.quantity = quantity;
  }
  




  public String getUnit()
  {
    return unit;
  }
  



  public void setUnit(String unit)
  {
    this.unit = unit;
  }
  




  public Double getUnitPrice()
  {
    return unitPrice == null ? Double.valueOf(0.0D) : unitPrice;
  }
  



  public void setUnitPrice(Double unitPrice)
  {
    this.unitPrice = unitPrice;
  }
  




  public Double getUnitCost()
  {
    return unitCost == null ? Double.valueOf(0.0D) : unitCost;
  }
  



  public void setUnitCost(Double unitCost)
  {
    this.unitCost = unitCost;
  }
  




  public Double getBaseUnitQuantity()
  {
    return baseUnitQuantity == null ? Double.valueOf(0.0D) : baseUnitQuantity;
  }
  



  public void setBaseUnitQuantity(Double baseUnitQuantity)
  {
    this.baseUnitQuantity = baseUnitQuantity;
  }
  




  public String getBaseUnit()
  {
    return baseUnit;
  }
  



  public void setBaseUnit(String baseUnit)
  {
    this.baseUnit = baseUnit;
  }
  




  public String getRemark()
  {
    return remark;
  }
  



  public void setRemark(String remark)
  {
    this.remark = remark;
  }
  




  public Double getTotal()
  {
    return total == null ? Double.valueOf(0.0D) : total;
  }
  



  public void setTotal(Double total)
  {
    this.total = total;
  }
  




  public String getTicketId()
  {
    return ticketId;
  }
  



  public void setTicketId(String ticketId)
  {
    this.ticketId = ticketId;
  }
  




  public String getReferenceNo()
  {
    return referenceNo;
  }
  



  public void setReferenceNo(String referenceNo)
  {
    this.referenceNo = referenceNo;
  }
  




  public String getFromLocationId()
  {
    return fromLocationId;
  }
  



  public void setFromLocationId(String fromLocationId)
  {
    this.fromLocationId = fromLocationId;
  }
  




  public String getToLocationId()
  {
    return toLocationId;
  }
  



  public void setToLocationId(String toLocationId)
  {
    this.toLocationId = toLocationId;
  }
  




  public String getVendorId()
  {
    return vendorId;
  }
  



  public void setVendorId(String vendorId)
  {
    this.vendorId = vendorId;
  }
  




  public String getUserId()
  {
    return userId;
  }
  



  public void setUserId(String userId)
  {
    this.userId = userId;
  }
  




  public Integer getType()
  {
    return type == null ? Integer.valueOf(0) : type;
  }
  



  public void setType(Integer type)
  {
    this.type = type;
  }
  




  public MenuItem getMenuItem()
  {
    return menuItem;
  }
  



  public void setMenuItem(MenuItem menuItem)
  {
    this.menuItem = menuItem;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof InventoryTransaction)) { return false;
    }
    InventoryTransaction inventoryTransaction = (InventoryTransaction)obj;
    if ((null == getId()) || (null == inventoryTransaction.getId())) return this == obj;
    return getId().equals(inventoryTransaction.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
