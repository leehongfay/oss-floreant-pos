package com.floreantpos.model.base;

import com.floreantpos.model.InventoryVendor;
import java.io.Serializable;










public abstract class BaseInventoryVendor
  implements Comparable, Serializable
{
  public static String REF = "InventoryVendor";
  public static String PROP_ZIP = "zip";
  public static String PROP_EMAIL = "email";
  public static String PROP_ADDRESS = "address";
  public static String PROP_STATE = "state";
  public static String PROP_PHONE = "phone";
  public static String PROP_VISIBLE = "visible";
  public static String PROP_COUNTRY = "country";
  public static String PROP_CITY = "city";
  public static String PROP_ID = "id";
  public static String PROP_FAX = "fax";
  public static String PROP_NAME = "name";
  

  public BaseInventoryVendor()
  {
    initialize();
  }
  


  public BaseInventoryVendor(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseInventoryVendor(String id, String name)
  {
    setId(id);
    setName(name);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected String name;
  
  protected Boolean visible;
  
  protected String address;
  
  protected String city;
  
  protected String state;
  
  protected String zip;
  
  protected String country;
  
  protected String email;
  protected String phone;
  protected String fax;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Boolean isVisible()
  {
    return visible == null ? Boolean.FALSE : visible;
  }
  



  public void setVisible(Boolean visible)
  {
    this.visible = visible;
  }
  




  public String getAddress()
  {
    return address;
  }
  



  public void setAddress(String address)
  {
    this.address = address;
  }
  




  public String getCity()
  {
    return city;
  }
  



  public void setCity(String city)
  {
    this.city = city;
  }
  




  public String getState()
  {
    return state;
  }
  



  public void setState(String state)
  {
    this.state = state;
  }
  




  public String getZip()
  {
    return zip;
  }
  



  public void setZip(String zip)
  {
    this.zip = zip;
  }
  




  public String getCountry()
  {
    return country;
  }
  



  public void setCountry(String country)
  {
    this.country = country;
  }
  




  public String getEmail()
  {
    return email;
  }
  



  public void setEmail(String email)
  {
    this.email = email;
  }
  




  public String getPhone()
  {
    return phone;
  }
  



  public void setPhone(String phone)
  {
    this.phone = phone;
  }
  




  public String getFax()
  {
    return fax;
  }
  



  public void setFax(String fax)
  {
    this.fax = fax;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof InventoryVendor)) { return false;
    }
    InventoryVendor inventoryVendor = (InventoryVendor)obj;
    if ((null == getId()) || (null == inventoryVendor.getId())) return this == obj;
    return getId().equals(inventoryVendor.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
