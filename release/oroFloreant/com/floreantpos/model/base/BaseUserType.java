package com.floreantpos.model.base;

import com.floreantpos.model.UserPermission;
import com.floreantpos.model.UserType;
import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;







public abstract class BaseUserType
  implements Comparable, Serializable
{
  public static String REF = "UserType";
  public static String PROP_NAME = "name";
  public static String PROP_ID = "id";
  

  public BaseUserType()
  {
    initialize();
  }
  


  public BaseUserType(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String name;
  

  private Set<UserPermission> permissions;
  


  protected void initialize() {}
  


  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Set<UserPermission> getPermissions()
  {
    return permissions;
  }
  



  public void setPermissions(Set<UserPermission> permissions)
  {
    this.permissions = permissions;
  }
  
  public void addTopermissions(UserPermission userPermission) {
    if (null == getPermissions()) setPermissions(new TreeSet());
    getPermissions().add(userPermission);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof UserType)) { return false;
    }
    UserType userType = (UserType)obj;
    if ((null == getId()) || (null == userType.getId())) return this == obj;
    return getId().equals(userType.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
