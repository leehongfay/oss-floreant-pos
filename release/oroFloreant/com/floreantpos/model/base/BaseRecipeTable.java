package com.floreantpos.model.base;

import com.floreantpos.model.MenuItem;
import com.floreantpos.model.Recepie;
import com.floreantpos.model.RecipeTable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;






public abstract class BaseRecipeTable
  implements Comparable, Serializable
{
  public static String REF = "RecipeTable";
  public static String PROP_MENU_ITEM = "menuItem";
  public static String PROP_ID = "id";
  

  public BaseRecipeTable()
  {
    initialize();
  }
  


  public BaseRecipeTable(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  long version;
  

  private MenuItem menuItem;
  

  private List<Recepie> recipeList;
  


  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public MenuItem getMenuItem()
  {
    return menuItem;
  }
  



  public void setMenuItem(MenuItem menuItem)
  {
    this.menuItem = menuItem;
  }
  




  public List<Recepie> getRecipeList()
  {
    return recipeList;
  }
  



  public void setRecipeList(List<Recepie> recipeList)
  {
    this.recipeList = recipeList;
  }
  
  public void addTorecipeList(Recepie recepie) {
    if (null == getRecipeList())
      setRecipeList(new ArrayList());
    getRecipeList().add(recepie);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof RecipeTable)) { return false;
    }
    RecipeTable recipeTable = (RecipeTable)obj;
    if ((null == getId()) || (null == recipeTable.getId())) return this == obj;
    return getId().equals(recipeTable.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
