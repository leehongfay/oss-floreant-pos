package com.floreantpos.model.base;

import com.floreantpos.model.Course;
import java.io.Serializable;










public abstract class BaseCourse
  implements Comparable, Serializable
{
  public static String REF = "Course";
  public static String PROP_SORT_ORDER = "sortOrder";
  public static String PROP_SHORT_NAME = "shortName";
  public static String PROP_ID = "id";
  public static String PROP_ICON_ID = "iconId";
  public static String PROP_NAME = "name";
  

  public BaseCourse()
  {
    initialize();
  }
  


  public BaseCourse(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  protected String name;
  
  protected String shortName;
  
  protected Integer sortOrder;
  
  protected String iconId;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getShortName()
  {
    return shortName;
  }
  



  public void setShortName(String shortName)
  {
    this.shortName = shortName;
  }
  




  public Integer getSortOrder()
  {
    return sortOrder == null ? Integer.valueOf(0) : sortOrder;
  }
  



  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  




  public String getIconId()
  {
    return iconId;
  }
  



  public void setIconId(String iconId)
  {
    this.iconId = iconId;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof Course)) { return false;
    }
    Course course = (Course)obj;
    if ((null == getId()) || (null == course.getId())) return this == obj;
    return getId().equals(course.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
