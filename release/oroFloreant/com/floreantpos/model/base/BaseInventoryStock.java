package com.floreantpos.model.base;

import com.floreantpos.model.InventoryStock;
import java.io.Serializable;










public abstract class BaseInventoryStock
  implements Comparable, Serializable
{
  public static String REF = "InventoryStock";
  public static String PROP_QUANTITY_IN_HAND = "quantityInHand";
  public static String PROP_MENU_ITEM_ID = "menuItemId";
  public static String PROP_LOCATION_ID = "locationId";
  public static String PROP_ITEM_NAME = "itemName";
  public static String PROP_ID = "id";
  public static String PROP_SKU = "sku";
  public static String PROP_UNIT = "unit";
  

  public BaseInventoryStock()
  {
    initialize();
  }
  


  public BaseInventoryStock(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected Double quantityInHand;
  
  protected String menuItemId;
  
  protected String itemName;
  
  protected String sku;
  
  protected String unit;
  
  protected String locationId;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Double getQuantityInHand()
  {
    return quantityInHand == null ? Double.valueOf(0.0D) : quantityInHand;
  }
  



  public void setQuantityInHand(Double quantityInHand)
  {
    this.quantityInHand = quantityInHand;
  }
  




  public String getMenuItemId()
  {
    return menuItemId;
  }
  



  public void setMenuItemId(String menuItemId)
  {
    this.menuItemId = menuItemId;
  }
  




  public String getItemName()
  {
    return itemName;
  }
  



  public void setItemName(String itemName)
  {
    this.itemName = itemName;
  }
  




  public String getSku()
  {
    return sku;
  }
  



  public void setSku(String sku)
  {
    this.sku = sku;
  }
  




  public String getUnit()
  {
    return unit;
  }
  



  public void setUnit(String unit)
  {
    this.unit = unit;
  }
  




  public String getLocationId()
  {
    return locationId;
  }
  



  public void setLocationId(String locationId)
  {
    this.locationId = locationId;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof InventoryStock)) { return false;
    }
    InventoryStock inventoryStock = (InventoryStock)obj;
    if ((null == getId()) || (null == inventoryStock.getId())) return this == obj;
    return getId().equals(inventoryStock.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
