package com.floreantpos.model.base;

import com.floreantpos.model.MenuItemSize;
import com.floreantpos.model.ModifierMultiplierPrice;
import com.floreantpos.model.PizzaModifierPrice;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;






public abstract class BasePizzaModifierPrice
  implements Comparable, Serializable
{
  public static String REF = "PizzaModifierPrice";
  public static String PROP_ID = "id";
  public static String PROP_SIZE = "size";
  

  public BasePizzaModifierPrice()
  {
    initialize();
  }
  


  public BasePizzaModifierPrice(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  private MenuItemSize size;
  

  private List<ModifierMultiplierPrice> multiplierPriceList;
  


  protected void initialize() {}
  


  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public MenuItemSize getSize()
  {
    return size;
  }
  



  public void setSize(MenuItemSize size)
  {
    this.size = size;
  }
  




  public List<ModifierMultiplierPrice> getMultiplierPriceList()
  {
    return multiplierPriceList;
  }
  



  public void setMultiplierPriceList(List<ModifierMultiplierPrice> multiplierPriceList)
  {
    this.multiplierPriceList = multiplierPriceList;
  }
  
  public void addTomultiplierPriceList(ModifierMultiplierPrice modifierMultiplierPrice) {
    if (null == getMultiplierPriceList()) setMultiplierPriceList(new ArrayList());
    getMultiplierPriceList().add(modifierMultiplierPrice);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof PizzaModifierPrice)) { return false;
    }
    PizzaModifierPrice pizzaModifierPrice = (PizzaModifierPrice)obj;
    if ((null == getId()) || (null == pizzaModifierPrice.getId())) return this == obj;
    return getId().equals(pizzaModifierPrice.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
