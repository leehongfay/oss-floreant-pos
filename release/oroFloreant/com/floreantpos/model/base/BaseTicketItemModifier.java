package com.floreantpos.model.base;

import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemModifier;
import java.io.Serializable;









public abstract class BaseTicketItemModifier
  implements Comparable, Serializable
{
  public static String REF = "TicketItemModifier";
  public static String PROP_ADJUSTED_DISCOUNT = "adjustedDiscount";
  public static String PROP_IS_DEFAULT = "isDefault";
  public static String PROP_INFO_ONLY = "infoOnly";
  public static String PROP_DISCOUNT_AMOUNT = "discountAmount";
  public static String PROP_SHOULD_SECTION_WISE_PRICE = "shouldSectionWisePrice";
  public static String PROP_GROUP_ID = "groupId";
  public static String PROP_ADJUSTED_UNIT_PRICE = "adjustedUnitPrice";
  public static String PROP_SHOULD_PRINT_TO_KITCHEN = "shouldPrintToKitchen";
  public static String PROP_IS_DEFAULT_MODIFIER = "isDefaultModifier";
  public static String PROP_SECTION_NAME = "sectionName";
  public static String PROP_TICKET_ITEM = "ticketItem";
  public static String PROP_MODIFIER_TYPE = "modifierType";
  public static String PROP_ITEM_QUANTITY = "itemQuantity";
  public static String PROP_ADJUSTED_TOTAL = "adjustedTotal";
  public static String PROP_SUB_TOTAL_AMOUNT = "subTotalAmount";
  public static String PROP_ITEM_ID = "itemId";
  public static String PROP_MULTIPLIER_NAME = "multiplierName";
  public static String PROP_ADJUSTED_SUBTOTAL = "adjustedSubtotal";
  public static String PROP_ITEM_COUNT = "itemCount";
  public static String PROP_TAXES_PROPERTY = "taxesProperty";
  public static String PROP_ADJUSTED_TAX = "adjustedTax";
  public static String PROP_TAX_AMOUNT = "taxAmount";
  public static String PROP_UNIT_PRICE = "unitPrice";
  public static String PROP_FRACTIONAL_UNIT = "fractionalUnit";
  public static String PROP_NAME = "name";
  public static String PROP_PAGE_ITEM_ID = "pageItemId";
  public static String PROP_PRINTED_TO_KITCHEN = "printedToKitchen";
  public static String PROP_STATUS = "status";
  public static String PROP_TAX_INCLUDED = "taxIncluded";
  public static String PROP_ID = "id";
  public static String PROP_TOTAL_AMOUNT = "totalAmount";
  

  public BaseTicketItemModifier()
  {
    initialize();
  }
  


  public BaseTicketItemModifier(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  private String itemId;
  
  private String groupId;
  
  private String pageItemId;
  
  private Integer itemCount;
  
  private Double itemQuantity;
  
  private String name;
  
  private Double unitPrice;
  
  private Integer modifierType;
  
  private Double subTotalAmount;
  
  private Double discountAmount;
  private Double taxAmount;
  private Double totalAmount;
  private Boolean taxIncluded;
  private Double adjustedUnitPrice;
  private Double adjustedDiscount;
  private Double adjustedSubtotal;
  private Double adjustedTax;
  private Double adjustedTotal;
  private Boolean infoOnly;
  private Boolean isDefault;
  private String sectionName;
  private String multiplierName;
  private Boolean shouldPrintToKitchen;
  private String status;
  private Boolean shouldSectionWisePrice;
  private Boolean fractionalUnit;
  private Boolean printedToKitchen;
  private Boolean isDefaultModifier;
  private String taxesProperty;
  private TicketItem ticketItem;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getItemId()
  {
    return itemId;
  }
  



  public void setItemId(String itemId)
  {
    this.itemId = itemId;
  }
  




  public String getGroupId()
  {
    return groupId;
  }
  



  public void setGroupId(String groupId)
  {
    this.groupId = groupId;
  }
  




  public String getPageItemId()
  {
    return pageItemId;
  }
  



  public void setPageItemId(String pageItemId)
  {
    this.pageItemId = pageItemId;
  }
  




  public Integer getItemCount()
  {
    return itemCount == null ? Integer.valueOf(0) : itemCount;
  }
  



  public void setItemCount(Integer itemCount)
  {
    this.itemCount = itemCount;
  }
  




  public Double getItemQuantity()
  {
    return itemQuantity == null ? Double.valueOf(0.0D) : itemQuantity;
  }
  



  public void setItemQuantity(Double itemQuantity)
  {
    this.itemQuantity = itemQuantity;
  }
  




  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Double getUnitPrice()
  {
    return unitPrice == null ? Double.valueOf(0.0D) : unitPrice;
  }
  



  public void setUnitPrice(Double unitPrice)
  {
    this.unitPrice = unitPrice;
  }
  




  public Integer getModifierType()
  {
    return modifierType == null ? Integer.valueOf(0) : modifierType;
  }
  



  public void setModifierType(Integer modifierType)
  {
    this.modifierType = modifierType;
  }
  




  public Double getSubTotalAmount()
  {
    return subTotalAmount == null ? Double.valueOf(0.0D) : subTotalAmount;
  }
  



  public void setSubTotalAmount(Double subTotalAmount)
  {
    this.subTotalAmount = subTotalAmount;
  }
  




  public Double getDiscountAmount()
  {
    return discountAmount == null ? Double.valueOf(0.0D) : discountAmount;
  }
  



  public void setDiscountAmount(Double discountAmount)
  {
    this.discountAmount = discountAmount;
  }
  




  public Double getTaxAmount()
  {
    return taxAmount == null ? Double.valueOf(0.0D) : taxAmount;
  }
  



  public void setTaxAmount(Double taxAmount)
  {
    this.taxAmount = taxAmount;
  }
  




  public Double getTotalAmount()
  {
    return totalAmount == null ? Double.valueOf(0.0D) : totalAmount;
  }
  



  public void setTotalAmount(Double totalAmount)
  {
    this.totalAmount = totalAmount;
  }
  




  public Boolean isTaxIncluded()
  {
    return taxIncluded == null ? Boolean.FALSE : taxIncluded;
  }
  



  public void setTaxIncluded(Boolean taxIncluded)
  {
    this.taxIncluded = taxIncluded;
  }
  




  public Double getAdjustedUnitPrice()
  {
    return adjustedUnitPrice == null ? Double.valueOf(0.0D) : adjustedUnitPrice;
  }
  



  public void setAdjustedUnitPrice(Double adjustedUnitPrice)
  {
    this.adjustedUnitPrice = adjustedUnitPrice;
  }
  




  public Double getAdjustedDiscount()
  {
    return adjustedDiscount == null ? Double.valueOf(0.0D) : adjustedDiscount;
  }
  



  public void setAdjustedDiscount(Double adjustedDiscount)
  {
    this.adjustedDiscount = adjustedDiscount;
  }
  




  public Double getAdjustedSubtotal()
  {
    return adjustedSubtotal == null ? Double.valueOf(0.0D) : adjustedSubtotal;
  }
  



  public void setAdjustedSubtotal(Double adjustedSubtotal)
  {
    this.adjustedSubtotal = adjustedSubtotal;
  }
  




  public Double getAdjustedTax()
  {
    return adjustedTax == null ? Double.valueOf(0.0D) : adjustedTax;
  }
  



  public void setAdjustedTax(Double adjustedTax)
  {
    this.adjustedTax = adjustedTax;
  }
  




  public Double getAdjustedTotal()
  {
    return adjustedTotal == null ? Double.valueOf(0.0D) : adjustedTotal;
  }
  



  public void setAdjustedTotal(Double adjustedTotal)
  {
    this.adjustedTotal = adjustedTotal;
  }
  




  public Boolean isInfoOnly()
  {
    return infoOnly == null ? Boolean.FALSE : infoOnly;
  }
  



  public void setInfoOnly(Boolean infoOnly)
  {
    this.infoOnly = infoOnly;
  }
  




  public Boolean isIsDefault()
  {
    return isDefault == null ? Boolean.FALSE : isDefault;
  }
  



  public void setIsDefault(Boolean isDefault)
  {
    this.isDefault = isDefault;
  }
  




  public String getSectionName()
  {
    return sectionName;
  }
  



  public void setSectionName(String sectionName)
  {
    this.sectionName = sectionName;
  }
  




  public String getMultiplierName()
  {
    return multiplierName;
  }
  



  public void setMultiplierName(String multiplierName)
  {
    this.multiplierName = multiplierName;
  }
  




  public Boolean isShouldPrintToKitchen()
  {
    return shouldPrintToKitchen == null ? Boolean.valueOf(true) : shouldPrintToKitchen;
  }
  



  public void setShouldPrintToKitchen(Boolean shouldPrintToKitchen)
  {
    this.shouldPrintToKitchen = shouldPrintToKitchen;
  }
  



  public static String getShouldPrintToKitchenDefaultValue()
  {
    return "true";
  }
  



  public String getStatus()
  {
    return status;
  }
  



  public void setStatus(String status)
  {
    this.status = status;
  }
  




  public Boolean isShouldSectionWisePrice()
  {
    return shouldSectionWisePrice == null ? Boolean.FALSE : shouldSectionWisePrice;
  }
  



  public void setShouldSectionWisePrice(Boolean shouldSectionWisePrice)
  {
    this.shouldSectionWisePrice = shouldSectionWisePrice;
  }
  




  public Boolean isFractionalUnit()
  {
    return fractionalUnit == null ? Boolean.FALSE : fractionalUnit;
  }
  



  public void setFractionalUnit(Boolean fractionalUnit)
  {
    this.fractionalUnit = fractionalUnit;
  }
  




  public Boolean isPrintedToKitchen()
  {
    return printedToKitchen == null ? Boolean.FALSE : printedToKitchen;
  }
  



  public void setPrintedToKitchen(Boolean printedToKitchen)
  {
    this.printedToKitchen = printedToKitchen;
  }
  




  public Boolean isIsDefaultModifier()
  {
    return isDefaultModifier == null ? Boolean.FALSE : isDefaultModifier;
  }
  



  public void setIsDefaultModifier(Boolean isDefaultModifier)
  {
    this.isDefaultModifier = isDefaultModifier;
  }
  




  public String getTaxesProperty()
  {
    return taxesProperty;
  }
  



  public void setTaxesProperty(String taxesProperty)
  {
    this.taxesProperty = taxesProperty;
  }
  




  public TicketItem getTicketItem()
  {
    return ticketItem;
  }
  



  public void setTicketItem(TicketItem ticketItem)
  {
    this.ticketItem = ticketItem;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof TicketItemModifier)) { return false;
    }
    TicketItemModifier ticketItemModifier = (TicketItemModifier)obj;
    if ((null == getId()) || (null == ticketItemModifier.getId())) return this == obj;
    return getId().equals(ticketItemModifier.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
