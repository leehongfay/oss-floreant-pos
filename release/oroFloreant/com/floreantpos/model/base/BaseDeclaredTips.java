package com.floreantpos.model.base;

import com.floreantpos.model.DeclaredTips;
import java.io.Serializable;
import java.util.Date;









public abstract class BaseDeclaredTips
  implements Comparable, Serializable
{
  public static String REF = "DeclaredTips";
  public static String PROP_OWNER_ID = "ownerId";
  public static String PROP_AMOUNT = "amount";
  public static String PROP_ID = "id";
  public static String PROP_DECLARED_TIME = "declaredTime";
  public static String PROP_SESSION_ID = "sessionId";
  

  public BaseDeclaredTips()
  {
    initialize();
  }
  


  public BaseDeclaredTips(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  long version;
  
  protected Double amount;
  
  protected Date declaredTime;
  
  protected String ownerId;
  
  protected String sessionId;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Double getAmount()
  {
    return amount == null ? Double.valueOf(0.0D) : amount;
  }
  



  public void setAmount(Double amount)
  {
    this.amount = amount;
  }
  




  public Date getDeclaredTime()
  {
    return declaredTime;
  }
  



  public void setDeclaredTime(Date declaredTime)
  {
    this.declaredTime = declaredTime;
  }
  




  public String getOwnerId()
  {
    return ownerId;
  }
  



  public void setOwnerId(String ownerId)
  {
    this.ownerId = ownerId;
  }
  




  public String getSessionId()
  {
    return sessionId;
  }
  



  public void setSessionId(String sessionId)
  {
    this.sessionId = sessionId;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof DeclaredTips)) { return false;
    }
    DeclaredTips declaredTips = (DeclaredTips)obj;
    if ((null == getId()) || (null == declaredTips.getId())) return this == obj;
    return getId().equals(declaredTips.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
