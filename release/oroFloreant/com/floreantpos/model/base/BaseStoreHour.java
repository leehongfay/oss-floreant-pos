package com.floreantpos.model.base;

import com.floreantpos.model.Shift;
import com.floreantpos.model.StoreHour;
import java.io.Serializable;








public abstract class BaseStoreHour
  extends Shift
  implements Comparable, Serializable
{
  public static String REF = "StoreHour";
  public static String PROP_ID = "id";
  

  public BaseStoreHour()
  {
    initialize();
  }
  


  public BaseStoreHour(String id)
  {
    super(id);
  }
  





  public BaseStoreHour(String id, String name)
  {
    super(id, name);
  }
  




  private int hashCode = Integer.MIN_VALUE;
  







  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof StoreHour)) { return false;
    }
    StoreHour storeHour = (StoreHour)obj;
    if ((null == getId()) || (null == storeHour.getId())) return this == obj;
    return getId().equals(storeHour.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
