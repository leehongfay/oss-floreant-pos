package com.floreantpos.model.base;

import com.floreantpos.model.Gratuity;
import java.io.Serializable;










public abstract class BaseGratuity
  implements Comparable, Serializable
{
  public static String REF = "Gratuity";
  public static String PROP_TICKET_ID = "ticketId";
  public static String PROP_OWNER_ID = "ownerId";
  public static String PROP_CLOUD_SYNCED = "cloudSynced";
  public static String PROP_DECLARE_TIPS_AMOUNT = "declareTipsAmount";
  public static String PROP_AMOUNT = "amount";
  public static String PROP_PAID = "paid";
  public static String PROP_TERMINAL_ID = "terminalId";
  public static String PROP_HAS_SYNC_ERROR = "hasSyncError";
  public static String PROP_ID = "id";
  public static String PROP_TIPS_PAID_AMOUNT = "tipsPaidAmount";
  public static String PROP_REFUNDED = "refunded";
  

  public BaseGratuity()
  {
    initialize();
  }
  


  public BaseGratuity(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  private Double amount;
  
  private Double declareTipsAmount;
  
  private Double tipsPaidAmount;
  
  private Boolean paid;
  
  private Boolean refunded;
  
  private String ticketId;
  
  private String ownerId;
  
  private Integer terminalId;
  private Boolean cloudSynced;
  private Boolean hasSyncError;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Double getAmount()
  {
    return amount == null ? Double.valueOf(0.0D) : amount;
  }
  



  public void setAmount(Double amount)
  {
    this.amount = amount;
  }
  




  public Double getDeclareTipsAmount()
  {
    return declareTipsAmount == null ? Double.valueOf(0.0D) : declareTipsAmount;
  }
  



  public void setDeclareTipsAmount(Double declareTipsAmount)
  {
    this.declareTipsAmount = declareTipsAmount;
  }
  




  public Double getTipsPaidAmount()
  {
    return tipsPaidAmount == null ? Double.valueOf(0.0D) : tipsPaidAmount;
  }
  



  public void setTipsPaidAmount(Double tipsPaidAmount)
  {
    this.tipsPaidAmount = tipsPaidAmount;
  }
  




  public Boolean isPaid()
  {
    return paid == null ? Boolean.FALSE : paid;
  }
  



  public void setPaid(Boolean paid)
  {
    this.paid = paid;
  }
  




  public Boolean isRefunded()
  {
    return refunded == null ? Boolean.FALSE : refunded;
  }
  



  public void setRefunded(Boolean refunded)
  {
    this.refunded = refunded;
  }
  




  public String getTicketId()
  {
    return ticketId;
  }
  



  public void setTicketId(String ticketId)
  {
    this.ticketId = ticketId;
  }
  




  public String getOwnerId()
  {
    return ownerId;
  }
  



  public void setOwnerId(String ownerId)
  {
    this.ownerId = ownerId;
  }
  




  public Integer getTerminalId()
  {
    return terminalId == null ? Integer.valueOf(0) : terminalId;
  }
  



  public void setTerminalId(Integer terminalId)
  {
    this.terminalId = terminalId;
  }
  




  public Boolean isCloudSynced()
  {
    return cloudSynced == null ? Boolean.FALSE : cloudSynced;
  }
  



  public void setCloudSynced(Boolean cloudSynced)
  {
    this.cloudSynced = cloudSynced;
  }
  




  public Boolean isHasSyncError()
  {
    return hasSyncError == null ? Boolean.FALSE : hasSyncError;
  }
  



  public void setHasSyncError(Boolean hasSyncError)
  {
    this.hasSyncError = hasSyncError;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof Gratuity)) { return false;
    }
    Gratuity gratuity = (Gratuity)obj;
    if ((null == getId()) || (null == gratuity.getId())) return this == obj;
    return getId().equals(gratuity.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
