package com.floreantpos.model.base;

import com.floreantpos.model.Tax;
import com.floreantpos.model.TaxGroup;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;







public abstract class BaseTaxGroup
  implements Comparable, Serializable
{
  public static String REF = "TaxGroup";
  public static String PROP_NAME = "name";
  public static String PROP_ID = "id";
  

  public BaseTaxGroup()
  {
    initialize();
  }
  


  public BaseTaxGroup(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseTaxGroup(String id, String name)
  {
    setId(id);
    setName(name);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String name;
  

  private List<Tax> taxes;
  


  protected void initialize() {}
  


  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public List<Tax> getTaxes()
  {
    return taxes;
  }
  



  public void setTaxes(List<Tax> taxes)
  {
    this.taxes = taxes;
  }
  
  public void addTotaxes(Tax tax) {
    if (null == getTaxes()) setTaxes(new ArrayList());
    getTaxes().add(tax);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof TaxGroup)) { return false;
    }
    TaxGroup taxGroup = (TaxGroup)obj;
    if ((null == getId()) || (null == taxGroup.getId())) return this == obj;
    return getId().equals(taxGroup.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
