package com.floreantpos.model.base;

import com.floreantpos.model.UserPermission;
import java.io.Serializable;










public abstract class BaseUserPermission
  implements Comparable, Serializable
{
  public static String REF = "UserPermission";
  public static String PROP_NAME = "name";
  

  public BaseUserPermission()
  {
    initialize();
  }
  


  public BaseUserPermission(String name)
  {
    setName(name);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String name;
  

  private long version;
  


  protected void initialize() {}
  


  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  




  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof UserPermission)) { return false;
    }
    UserPermission userPermission = (UserPermission)obj;
    if ((null == getName()) || (null == userPermission.getName())) return this == obj;
    return getName().equals(userPermission.getName());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getName()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getName().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
