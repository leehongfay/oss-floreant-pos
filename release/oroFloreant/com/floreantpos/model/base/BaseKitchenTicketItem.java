package com.floreantpos.model.base;

import com.floreantpos.model.KitchenTicket;
import com.floreantpos.model.KitchenTicketItem;
import java.io.Serializable;









public abstract class BaseKitchenTicketItem
  implements Comparable, Serializable
{
  public static String REF = "KitchenTicketItem";
  public static String PROP_STATUS = "status";
  public static String PROP_PRINT_KITCHEN_STICKER = "printKitchenSticker";
  public static String PROP_QUANTITY = "quantity";
  public static String PROP_SORT_ORDER = "sortOrder";
  public static String PROP_UNIT_NAME = "unitName";
  public static String PROP_TICKET_ITEM_MODIFIER_ID = "ticketItemModifierId";
  public static String PROP_COURSE_ID = "courseId";
  public static String PROP_KITCHEN_TICKET = "kitchenTicket";
  public static String PROP_MENU_ITEM_GROUP_NAME = "menuItemGroupName";
  public static String PROP_MENU_ITEM_NAME = "menuItemName";
  public static String PROP_MENU_ITEM_GROUP_ID = "menuItemGroupId";
  public static String PROP_ID = "id";
  public static String PROP_TICKET_ITEM_ID = "ticketItemId";
  public static String PROP_VOIDED = "voided";
  public static String PROP_COOKABLE = "cookable";
  public static String PROP_MENU_ITEM_CODE = "menuItemCode";
  

  public BaseKitchenTicketItem()
  {
    initialize();
  }
  


  public BaseKitchenTicketItem(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected Boolean cookable;
  
  protected String ticketItemId;
  
  protected String ticketItemModifierId;
  
  protected String menuItemCode;
  
  protected String menuItemName;
  
  protected String menuItemGroupId;
  
  protected String menuItemGroupName;
  
  protected String courseId;
  
  protected Double quantity;
  
  protected String unitName;
  protected Integer sortOrder;
  protected Boolean voided;
  protected Boolean printKitchenSticker;
  protected String status;
  private KitchenTicket kitchenTicket;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Boolean isCookable()
  {
    return cookable == null ? Boolean.FALSE : cookable;
  }
  



  public void setCookable(Boolean cookable)
  {
    this.cookable = cookable;
  }
  




  public String getTicketItemId()
  {
    return ticketItemId;
  }
  



  public void setTicketItemId(String ticketItemId)
  {
    this.ticketItemId = ticketItemId;
  }
  




  public String getTicketItemModifierId()
  {
    return ticketItemModifierId;
  }
  



  public void setTicketItemModifierId(String ticketItemModifierId)
  {
    this.ticketItemModifierId = ticketItemModifierId;
  }
  




  public String getMenuItemCode()
  {
    return menuItemCode;
  }
  



  public void setMenuItemCode(String menuItemCode)
  {
    this.menuItemCode = menuItemCode;
  }
  




  public String getMenuItemName()
  {
    return menuItemName;
  }
  



  public void setMenuItemName(String menuItemName)
  {
    this.menuItemName = menuItemName;
  }
  




  public String getMenuItemGroupId()
  {
    return menuItemGroupId;
  }
  



  public void setMenuItemGroupId(String menuItemGroupId)
  {
    this.menuItemGroupId = menuItemGroupId;
  }
  




  public String getMenuItemGroupName()
  {
    return menuItemGroupName;
  }
  



  public void setMenuItemGroupName(String menuItemGroupName)
  {
    this.menuItemGroupName = menuItemGroupName;
  }
  




  public String getCourseId()
  {
    return courseId;
  }
  



  public void setCourseId(String courseId)
  {
    this.courseId = courseId;
  }
  




  public Double getQuantity()
  {
    return quantity == null ? Double.valueOf(0.0D) : quantity;
  }
  



  public void setQuantity(Double quantity)
  {
    this.quantity = quantity;
  }
  




  public String getUnitName()
  {
    return unitName;
  }
  



  public void setUnitName(String unitName)
  {
    this.unitName = unitName;
  }
  




  public Integer getSortOrder()
  {
    return sortOrder == null ? Integer.valueOf(0) : sortOrder;
  }
  



  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  




  public Boolean isVoided()
  {
    return voided == null ? Boolean.FALSE : voided;
  }
  



  public void setVoided(Boolean voided)
  {
    this.voided = voided;
  }
  




  public Boolean isPrintKitchenSticker()
  {
    return printKitchenSticker == null ? Boolean.valueOf(false) : printKitchenSticker;
  }
  



  public void setPrintKitchenSticker(Boolean printKitchenSticker)
  {
    this.printKitchenSticker = printKitchenSticker;
  }
  



  public static String getPrintKitchenStickerDefaultValue()
  {
    return "false";
  }
  



  public String getStatus()
  {
    return status;
  }
  



  public void setStatus(String status)
  {
    this.status = status;
  }
  




  public KitchenTicket getKitchenTicket()
  {
    return kitchenTicket;
  }
  



  public void setKitchenTicket(KitchenTicket kitchenTicket)
  {
    this.kitchenTicket = kitchenTicket;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof KitchenTicketItem)) { return false;
    }
    KitchenTicketItem kitchenTicketItem = (KitchenTicketItem)obj;
    if ((null == getId()) || (null == kitchenTicketItem.getId())) return this == obj;
    return getId().equals(kitchenTicketItem.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
