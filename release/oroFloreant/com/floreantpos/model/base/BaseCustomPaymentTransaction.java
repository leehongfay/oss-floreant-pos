package com.floreantpos.model.base;

import com.floreantpos.model.CustomPaymentTransaction;
import com.floreantpos.model.PosTransaction;
import java.io.Serializable;








public abstract class BaseCustomPaymentTransaction
  extends PosTransaction
  implements Comparable, Serializable
{
  public static String REF = "CustomPaymentTransaction";
  public static String PROP_CUSTOM_PAYMENT_FIELD_NAME = "customPaymentFieldName";
  public static String PROP_CUSTOM_PAYMENT_NAME = "customPaymentName";
  public static String PROP_ID = "id";
  public static String PROP_CUSTOM_PAYMENT_REF = "customPaymentRef";
  

  public BaseCustomPaymentTransaction()
  {
    initialize();
  }
  


  public BaseCustomPaymentTransaction(String id)
  {
    super(id);
  }
  






  public BaseCustomPaymentTransaction(String id, String transactionType, String paymentType)
  {
    super(id, transactionType, paymentType);
  }
  





  private int hashCode = Integer.MIN_VALUE;
  


  private String customPaymentName;
  


  private String customPaymentRef;
  

  private String customPaymentFieldName;
  


  public String getCustomPaymentName()
  {
    return customPaymentName;
  }
  



  public void setCustomPaymentName(String customPaymentName)
  {
    this.customPaymentName = customPaymentName;
  }
  




  public String getCustomPaymentRef()
  {
    return customPaymentRef;
  }
  



  public void setCustomPaymentRef(String customPaymentRef)
  {
    this.customPaymentRef = customPaymentRef;
  }
  




  public String getCustomPaymentFieldName()
  {
    return customPaymentFieldName;
  }
  



  public void setCustomPaymentFieldName(String customPaymentFieldName)
  {
    this.customPaymentFieldName = customPaymentFieldName;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof CustomPaymentTransaction)) { return false;
    }
    CustomPaymentTransaction customPaymentTransaction = (CustomPaymentTransaction)obj;
    if ((null == getId()) || (null == customPaymentTransaction.getId())) return this == obj;
    return getId().equals(customPaymentTransaction.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
