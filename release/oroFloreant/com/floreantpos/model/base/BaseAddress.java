package com.floreantpos.model.base;

import com.floreantpos.model.Address;
import java.io.Serializable;
import java.util.List;









public abstract class BaseAddress
  implements Comparable, Serializable
{
  public static String REF = "Address";
  public static String PROP_TYPE = "type";
  public static String PROP_ADDRESS_LINE = "addressLine";
  public static String PROP_STATE = "state";
  public static String PROP_ZIP_CODE = "zipCode";
  public static String PROP_FLAT_NO = "flatNo";
  public static String PROP_STREET = "street";
  public static String PROP_COUNTRY = "country";
  public static String PROP_HOUSE = "house";
  public static String PROP_CITY = "city";
  public static String PROP_ID = "id";
  public static String PROP_COUNTRY_CODE = "countryCode";
  public static String PROP_NAME = "name";
  

  public BaseAddress()
  {
    initialize();
  }
  


  public BaseAddress(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected String name;
  
  protected String addressLine;
  
  protected String house;
  
  protected String flatNo;
  
  protected String type;
  
  protected String street;
  
  protected String city;
  
  protected String state;
  
  protected String zipCode;
  
  protected String country;
  protected String countryCode;
  private List<String> phoneNumbers;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getAddressLine()
  {
    return addressLine;
  }
  



  public void setAddressLine(String addressLine)
  {
    this.addressLine = addressLine;
  }
  




  public String getHouse()
  {
    return house;
  }
  



  public void setHouse(String house)
  {
    this.house = house;
  }
  




  public String getFlatNo()
  {
    return flatNo;
  }
  



  public void setFlatNo(String flatNo)
  {
    this.flatNo = flatNo;
  }
  




  public String getType()
  {
    return type;
  }
  



  public void setType(String type)
  {
    this.type = type;
  }
  




  public String getStreet()
  {
    return street;
  }
  



  public void setStreet(String street)
  {
    this.street = street;
  }
  




  public String getCity()
  {
    return city;
  }
  



  public void setCity(String city)
  {
    this.city = city;
  }
  




  public String getState()
  {
    return state;
  }
  



  public void setState(String state)
  {
    this.state = state;
  }
  




  public String getZipCode()
  {
    return zipCode;
  }
  



  public void setZipCode(String zipCode)
  {
    this.zipCode = zipCode;
  }
  




  public String getCountry()
  {
    return country;
  }
  



  public void setCountry(String country)
  {
    this.country = country;
  }
  




  public String getCountryCode()
  {
    return countryCode;
  }
  



  public void setCountryCode(String countryCode)
  {
    this.countryCode = countryCode;
  }
  




  public List<String> getPhoneNumbers()
  {
    return phoneNumbers;
  }
  



  public void setPhoneNumbers(List<String> phoneNumbers)
  {
    this.phoneNumbers = phoneNumbers;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof Address)) { return false;
    }
    Address address = (Address)obj;
    if ((null == getId()) || (null == address.getId())) return this == obj;
    return getId().equals(address.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
