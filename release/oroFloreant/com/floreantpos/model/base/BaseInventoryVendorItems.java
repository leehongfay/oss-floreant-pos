package com.floreantpos.model.base;

import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.InventoryVendorItems;
import com.floreantpos.model.MenuItem;
import java.io.Serializable;








public abstract class BaseInventoryVendorItems
  implements Comparable, Serializable
{
  public static String REF = "InventoryVendorItems";
  public static String PROP_ITEM = "item";
  public static String PROP_VENDOR = "vendor";
  public static String PROP_ID = "id";
  

  public BaseInventoryVendorItems()
  {
    initialize();
  }
  


  public BaseInventoryVendorItems(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  long version;
  

  private InventoryVendor vendor;
  

  private MenuItem item;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public InventoryVendor getVendor()
  {
    return vendor;
  }
  



  public void setVendor(InventoryVendor vendor)
  {
    this.vendor = vendor;
  }
  




  public MenuItem getItem()
  {
    return item;
  }
  



  public void setItem(MenuItem item)
  {
    this.item = item;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof InventoryVendorItems)) { return false;
    }
    InventoryVendorItems inventoryVendorItems = (InventoryVendorItems)obj;
    if ((null == getId()) || (null == inventoryVendorItems.getId())) return this == obj;
    return getId().equals(inventoryVendorItems.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
