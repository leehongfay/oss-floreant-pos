package com.floreantpos.model.base;

import com.floreantpos.model.StockCount;
import com.floreantpos.model.StockCountItem;
import com.floreantpos.model.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;





public abstract class BaseStockCount
  implements Comparable, Serializable
{
  public static String REF = "StockCount";
  public static String PROP_USER = "user";
  public static String PROP_REF_NUMBER = "refNumber";
  public static String PROP_CREATED_DATE = "createdDate";
  public static String PROP_LAST_MODIFIED_DATE = "lastModifiedDate";
  public static String PROP_ID = "id";
  public static String PROP_VERIFIED_BY = "verifiedBy";
  public static String PROP_VARIFICATION_DATE = "varificationDate";
  

  public BaseStockCount()
  {
    initialize();
  }
  


  public BaseStockCount(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String refNumber;
  
  protected Date createdDate;
  
  protected Date lastModifiedDate;
  
  protected Date varificationDate;
  
  private User user;
  
  private User verifiedBy;
  
  private List<StockCountItem> countItems;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getRefNumber()
  {
    return refNumber;
  }
  



  public void setRefNumber(String refNumber)
  {
    this.refNumber = refNumber;
  }
  




  public Date getCreatedDate()
  {
    return createdDate;
  }
  



  public void setCreatedDate(Date createdDate)
  {
    this.createdDate = createdDate;
  }
  




  public Date getLastModifiedDate()
  {
    return lastModifiedDate;
  }
  



  public void setLastModifiedDate(Date lastModifiedDate)
  {
    this.lastModifiedDate = lastModifiedDate;
  }
  




  public Date getVarificationDate()
  {
    return varificationDate;
  }
  



  public void setVarificationDate(Date varificationDate)
  {
    this.varificationDate = varificationDate;
  }
  




  public User getUser()
  {
    return user;
  }
  



  public void setUser(User user)
  {
    this.user = user;
  }
  




  public User getVerifiedBy()
  {
    return verifiedBy;
  }
  



  public void setVerifiedBy(User verifiedBy)
  {
    this.verifiedBy = verifiedBy;
  }
  




  public List<StockCountItem> getCountItems()
  {
    return countItems;
  }
  



  public void setCountItems(List<StockCountItem> countItems)
  {
    this.countItems = countItems;
  }
  
  public void addTocountItems(StockCountItem stockCountItem) {
    if (null == getCountItems()) setCountItems(new ArrayList());
    getCountItems().add(stockCountItem);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof StockCount)) { return false;
    }
    StockCount stockCount = (StockCount)obj;
    if ((null == getId()) || (null == stockCount.getId())) return this == obj;
    return getId().equals(stockCount.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
