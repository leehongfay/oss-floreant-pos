package com.floreantpos.model.base;

import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemSeat;
import java.io.Serializable;








public abstract class BaseTicketItem
  implements Comparable, Serializable
{
  public static String REF = "TicketItem";
  public static String PROP_GROUP_NAME = "groupName";
  public static String PROP_SEAT = "seat";
  public static String PROP_PIZZA_SECTION_MODE_TYPE = "pizzaSectionModeType";
  public static String PROP_SHOULD_PRINT_TO_KITCHEN = "shouldPrintToKitchen";
  public static String PROP_TAX_ON_SERVICE_CHARGE = "taxOnServiceCharge";
  public static String PROP_NAME = "name";
  public static String PROP_TAXES_PROPERTY = "taxesProperty";
  public static String PROP_TAX_AMOUNT = "taxAmount";
  public static String PROP_TOTAL_AMOUNT_WITHOUT_MODIFIERS = "totalAmountWithoutModifiers";
  public static String PROP_ADJUSTED_SUBTOTAL = "adjustedSubtotal";
  public static String PROP_BEVERAGE = "beverage";
  public static String PROP_DISCOUNTS_PROPERTY = "discountsProperty";
  public static String PROP_KITCHEN_STATUS = "kitchenStatus";
  public static String PROP_ADJUSTED_TAX = "adjustedTax";
  public static String PROP_PRINT_KITCHEN_STICKER = "printKitchenSticker";
  public static String PROP_QUANTITY_SHIPPED = "quantityShipped";
  public static String PROP_ADJUSTED_UNIT_PRICE = "adjustedUnitPrice";
  public static String PROP_COOKING_INSTRUCTIONS_PROPERTY = "cookingInstructionsProperty";
  public static String PROP_UNIT_NAME = "unitName";
  public static String PROP_PRINTER_GROUP_ID = "printerGroupId";
  public static String PROP_COMBO_ITEM = "comboItem";
  public static String PROP_DISCOUNT_AMOUNT = "discountAmount";
  public static String PROP_CLOUD_SYNCED = "cloudSynced";
  public static String PROP_FRACTIONAL_UNIT = "fractionalUnit";
  public static String PROP_SUBTOTAL_AMOUNT_WITHOUT_MODIFIERS = "subtotalAmountWithoutModifiers";
  public static String PROP_TREAT_AS_SEAT = "treatAsSeat";
  public static String PROP_SERVICE_CHARGE_RATE = "serviceChargeRate";
  public static String PROP_ID = "id";
  public static String PROP_CATEGORY_ID = "categoryId";
  public static String PROP_ADJUSTED_TAX_WITHOUT_MODIFIERS = "adjustedTaxWithoutModifiers";
  public static String PROP_ADJUSTED_TOTAL_WITHOUT_MODIFIERS = "adjustedTotalWithoutModifiers";
  public static String PROP_INVENTORY_ITEM = "inventoryItem";
  public static String PROP_SUBTOTAL_AMOUNT = "subtotalAmount";
  public static String PROP_TAX_INCLUDED = "taxIncluded";
  public static String PROP_HAS_SYNC_ERROR = "hasSyncError";
  public static String PROP_ADJUSTED_SUBTOTAL_WITHOUT_MODIFIERS = "adjustedSubtotalWithoutModifiers";
  public static String PROP_INVENTORY_ADJUST_QTY = "inventoryAdjustQty";
  public static String PROP_TAX_AMOUNT_WITHOUT_MODIFIERS = "taxAmountWithoutModifiers";
  public static String PROP_COURSE_ID = "courseId";
  public static String PROP_ADJUSTED_DISCOUNT = "adjustedDiscount";
  public static String PROP_SERVICE_CHARGE = "serviceCharge";
  public static String PROP_ADJUSTED_TOTAL = "adjustedTotal";
  public static String PROP_MENU_ITEM_ID = "menuItemId";
  public static String PROP_VOIDED_ITEM_ID = "voidedItemId";
  public static String PROP_PRINTED_TO_KITCHEN = "printedToKitchen";
  public static String PROP_VOIDED = "voided";
  public static String PROP_TICKET = "ticket";
  public static String PROP_SERVICE_CHARGE_APPLICABLE = "serviceChargeApplicable";
  public static String PROP_PIZZA_TYPE = "pizzaType";
  public static String PROP_QUANTITY = "quantity";
  public static String PROP_TOTAL_AMOUNT = "totalAmount";
  public static String PROP_GROUP_ID = "groupId";
  public static String PROP_UNIT_COST = "unitCost";
  public static String PROP_UNIT_PRICE = "unitPrice";
  public static String PROP_CATEGORY_NAME = "categoryName";
  public static String PROP_DISCOUNT_WITHOUT_MODIFIERS = "discountWithoutModifiers";
  public static String PROP_SEAT_NUMBER = "seatNumber";
  public static String PROP_ADJUSTED_DISCOUNT_WITHOUT_MODIFIERS = "adjustedDiscountWithoutModifiers";
  

  public BaseTicketItem()
  {
    initialize();
  }
  


  public BaseTicketItem(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  private String menuItemId;
  
  private Double quantity;
  
  private Double quantityShipped;
  
  private String name;
  
  private String unitName;
  
  private Boolean taxIncluded;
  
  private String courseId;
  
  private String groupId;
  
  private String groupName;
  
  private String categoryId;
  private String categoryName;
  private Double unitPrice;
  private Double unitCost;
  private Double subtotalAmount;
  private Double subtotalAmountWithoutModifiers;
  private Double discountAmount;
  private Double discountWithoutModifiers;
  private Double taxAmount;
  private Double taxAmountWithoutModifiers;
  private Double totalAmount;
  private Double totalAmountWithoutModifiers;
  private Double adjustedUnitPrice;
  private Double adjustedSubtotal;
  private Double adjustedSubtotalWithoutModifiers;
  private Double adjustedDiscount;
  private Double adjustedDiscountWithoutModifiers;
  private Double adjustedTax;
  private Double adjustedTaxWithoutModifiers;
  private Double adjustedTotal;
  private Double adjustedTotalWithoutModifiers;
  private Boolean beverage;
  private Boolean voided;
  private String voidedItemId;
  private Boolean inventoryItem;
  private Double inventoryAdjustQty;
  private Boolean shouldPrintToKitchen;
  private Boolean printKitchenSticker;
  private Boolean treatAsSeat;
  private Integer seatNumber;
  private Boolean fractionalUnit;
  private Boolean printedToKitchen;
  private String kitchenStatus;
  private Boolean pizzaType;
  private Integer pizzaSectionModeType;
  private Boolean comboItem;
  private Double serviceChargeRate;
  private Double serviceCharge;
  private Boolean serviceChargeApplicable;
  private Boolean taxOnServiceCharge;
  private String printerGroupId;
  private String taxesProperty;
  private String discountsProperty;
  private String cookingInstructionsProperty;
  private Boolean cloudSynced;
  private Boolean hasSyncError;
  private Ticket ticket;
  private TicketItemSeat seat;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getMenuItemId()
  {
    return menuItemId;
  }
  



  public void setMenuItemId(String menuItemId)
  {
    this.menuItemId = menuItemId;
  }
  




  public Double getQuantity()
  {
    return quantity == null ? Double.valueOf(0.0D) : quantity;
  }
  



  public void setQuantity(Double quantity)
  {
    this.quantity = quantity;
  }
  




  public Double getQuantityShipped()
  {
    return quantityShipped == null ? Double.valueOf(0.0D) : quantityShipped;
  }
  



  public void setQuantityShipped(Double quantityShipped)
  {
    this.quantityShipped = quantityShipped;
  }
  




  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getUnitName()
  {
    return unitName;
  }
  



  public void setUnitName(String unitName)
  {
    this.unitName = unitName;
  }
  




  public Boolean isTaxIncluded()
  {
    return taxIncluded == null ? Boolean.FALSE : taxIncluded;
  }
  



  public void setTaxIncluded(Boolean taxIncluded)
  {
    this.taxIncluded = taxIncluded;
  }
  




  public String getCourseId()
  {
    return courseId;
  }
  



  public void setCourseId(String courseId)
  {
    this.courseId = courseId;
  }
  




  public String getGroupId()
  {
    return groupId;
  }
  



  public void setGroupId(String groupId)
  {
    this.groupId = groupId;
  }
  




  public String getGroupName()
  {
    return groupName;
  }
  



  public void setGroupName(String groupName)
  {
    this.groupName = groupName;
  }
  




  public String getCategoryId()
  {
    return categoryId;
  }
  



  public void setCategoryId(String categoryId)
  {
    this.categoryId = categoryId;
  }
  




  public String getCategoryName()
  {
    return categoryName;
  }
  



  public void setCategoryName(String categoryName)
  {
    this.categoryName = categoryName;
  }
  




  public Double getUnitPrice()
  {
    return unitPrice == null ? Double.valueOf(0.0D) : unitPrice;
  }
  



  public void setUnitPrice(Double unitPrice)
  {
    this.unitPrice = unitPrice;
  }
  




  public Double getUnitCost()
  {
    return unitCost == null ? Double.valueOf(0.0D) : unitCost;
  }
  



  public void setUnitCost(Double unitCost)
  {
    this.unitCost = unitCost;
  }
  




  public Double getSubtotalAmount()
  {
    return subtotalAmount == null ? Double.valueOf(0.0D) : subtotalAmount;
  }
  



  public void setSubtotalAmount(Double subtotalAmount)
  {
    this.subtotalAmount = subtotalAmount;
  }
  




  public Double getSubtotalAmountWithoutModifiers()
  {
    return subtotalAmountWithoutModifiers == null ? Double.valueOf(0.0D) : subtotalAmountWithoutModifiers;
  }
  



  public void setSubtotalAmountWithoutModifiers(Double subtotalAmountWithoutModifiers)
  {
    this.subtotalAmountWithoutModifiers = subtotalAmountWithoutModifiers;
  }
  




  public Double getDiscountAmount()
  {
    return discountAmount == null ? Double.valueOf(0.0D) : discountAmount;
  }
  



  public void setDiscountAmount(Double discountAmount)
  {
    this.discountAmount = discountAmount;
  }
  




  public Double getDiscountWithoutModifiers()
  {
    return discountWithoutModifiers == null ? Double.valueOf(0.0D) : discountWithoutModifiers;
  }
  



  public void setDiscountWithoutModifiers(Double discountWithoutModifiers)
  {
    this.discountWithoutModifiers = discountWithoutModifiers;
  }
  




  public Double getTaxAmount()
  {
    return taxAmount == null ? Double.valueOf(0.0D) : taxAmount;
  }
  



  public void setTaxAmount(Double taxAmount)
  {
    this.taxAmount = taxAmount;
  }
  




  public Double getTaxAmountWithoutModifiers()
  {
    return taxAmountWithoutModifiers == null ? Double.valueOf(0.0D) : taxAmountWithoutModifiers;
  }
  



  public void setTaxAmountWithoutModifiers(Double taxAmountWithoutModifiers)
  {
    this.taxAmountWithoutModifiers = taxAmountWithoutModifiers;
  }
  




  public Double getTotalAmount()
  {
    return totalAmount == null ? Double.valueOf(0.0D) : totalAmount;
  }
  



  public void setTotalAmount(Double totalAmount)
  {
    this.totalAmount = totalAmount;
  }
  




  public Double getTotalAmountWithoutModifiers()
  {
    return totalAmountWithoutModifiers == null ? Double.valueOf(0.0D) : totalAmountWithoutModifiers;
  }
  



  public void setTotalAmountWithoutModifiers(Double totalAmountWithoutModifiers)
  {
    this.totalAmountWithoutModifiers = totalAmountWithoutModifiers;
  }
  




  public Double getAdjustedUnitPrice()
  {
    return adjustedUnitPrice == null ? Double.valueOf(0.0D) : adjustedUnitPrice;
  }
  



  public void setAdjustedUnitPrice(Double adjustedUnitPrice)
  {
    this.adjustedUnitPrice = adjustedUnitPrice;
  }
  




  public Double getAdjustedSubtotal()
  {
    return adjustedSubtotal == null ? Double.valueOf(0.0D) : adjustedSubtotal;
  }
  



  public void setAdjustedSubtotal(Double adjustedSubtotal)
  {
    this.adjustedSubtotal = adjustedSubtotal;
  }
  




  public Double getAdjustedSubtotalWithoutModifiers()
  {
    return adjustedSubtotalWithoutModifiers == null ? Double.valueOf(0.0D) : adjustedSubtotalWithoutModifiers;
  }
  



  public void setAdjustedSubtotalWithoutModifiers(Double adjustedSubtotalWithoutModifiers)
  {
    this.adjustedSubtotalWithoutModifiers = adjustedSubtotalWithoutModifiers;
  }
  




  public Double getAdjustedDiscount()
  {
    return adjustedDiscount == null ? Double.valueOf(0.0D) : adjustedDiscount;
  }
  



  public void setAdjustedDiscount(Double adjustedDiscount)
  {
    this.adjustedDiscount = adjustedDiscount;
  }
  




  public Double getAdjustedDiscountWithoutModifiers()
  {
    return adjustedDiscountWithoutModifiers == null ? Double.valueOf(0.0D) : adjustedDiscountWithoutModifiers;
  }
  



  public void setAdjustedDiscountWithoutModifiers(Double adjustedDiscountWithoutModifiers)
  {
    this.adjustedDiscountWithoutModifiers = adjustedDiscountWithoutModifiers;
  }
  




  public Double getAdjustedTax()
  {
    return adjustedTax == null ? Double.valueOf(0.0D) : adjustedTax;
  }
  



  public void setAdjustedTax(Double adjustedTax)
  {
    this.adjustedTax = adjustedTax;
  }
  




  public Double getAdjustedTaxWithoutModifiers()
  {
    return adjustedTaxWithoutModifiers == null ? Double.valueOf(0.0D) : adjustedTaxWithoutModifiers;
  }
  



  public void setAdjustedTaxWithoutModifiers(Double adjustedTaxWithoutModifiers)
  {
    this.adjustedTaxWithoutModifiers = adjustedTaxWithoutModifiers;
  }
  




  public Double getAdjustedTotal()
  {
    return adjustedTotal == null ? Double.valueOf(0.0D) : adjustedTotal;
  }
  



  public void setAdjustedTotal(Double adjustedTotal)
  {
    this.adjustedTotal = adjustedTotal;
  }
  




  public Double getAdjustedTotalWithoutModifiers()
  {
    return adjustedTotalWithoutModifiers == null ? Double.valueOf(0.0D) : adjustedTotalWithoutModifiers;
  }
  



  public void setAdjustedTotalWithoutModifiers(Double adjustedTotalWithoutModifiers)
  {
    this.adjustedTotalWithoutModifiers = adjustedTotalWithoutModifiers;
  }
  




  public Boolean isBeverage()
  {
    return beverage == null ? Boolean.FALSE : beverage;
  }
  



  public void setBeverage(Boolean beverage)
  {
    this.beverage = beverage;
  }
  




  public Boolean isVoided()
  {
    return voided == null ? Boolean.FALSE : voided;
  }
  



  public void setVoided(Boolean voided)
  {
    this.voided = voided;
  }
  




  public String getVoidedItemId()
  {
    return voidedItemId;
  }
  



  public void setVoidedItemId(String voidedItemId)
  {
    this.voidedItemId = voidedItemId;
  }
  




  public Boolean isInventoryItem()
  {
    return inventoryItem == null ? Boolean.valueOf(true) : inventoryItem;
  }
  



  public void setInventoryItem(Boolean inventoryItem)
  {
    this.inventoryItem = inventoryItem;
  }
  



  public static String getInventoryItemDefaultValue()
  {
    return "true";
  }
  



  public Double getInventoryAdjustQty()
  {
    return inventoryAdjustQty == null ? Double.valueOf(0.0D) : inventoryAdjustQty;
  }
  



  public void setInventoryAdjustQty(Double inventoryAdjustQty)
  {
    this.inventoryAdjustQty = inventoryAdjustQty;
  }
  




  public Boolean isShouldPrintToKitchen()
  {
    return shouldPrintToKitchen == null ? Boolean.valueOf(true) : shouldPrintToKitchen;
  }
  



  public void setShouldPrintToKitchen(Boolean shouldPrintToKitchen)
  {
    this.shouldPrintToKitchen = shouldPrintToKitchen;
  }
  



  public static String getShouldPrintToKitchenDefaultValue()
  {
    return "true";
  }
  



  public Boolean isPrintKitchenSticker()
  {
    return printKitchenSticker == null ? Boolean.valueOf(false) : printKitchenSticker;
  }
  



  public void setPrintKitchenSticker(Boolean printKitchenSticker)
  {
    this.printKitchenSticker = printKitchenSticker;
  }
  



  public static String getPrintKitchenStickerDefaultValue()
  {
    return "false";
  }
  



  public Boolean isTreatAsSeat()
  {
    return treatAsSeat == null ? Boolean.FALSE : treatAsSeat;
  }
  



  public void setTreatAsSeat(Boolean treatAsSeat)
  {
    this.treatAsSeat = treatAsSeat;
  }
  




  public Integer getSeatNumber()
  {
    return seatNumber == null ? Integer.valueOf(0) : seatNumber;
  }
  



  public void setSeatNumber(Integer seatNumber)
  {
    this.seatNumber = seatNumber;
  }
  




  public Boolean isFractionalUnit()
  {
    return fractionalUnit == null ? Boolean.FALSE : fractionalUnit;
  }
  



  public void setFractionalUnit(Boolean fractionalUnit)
  {
    this.fractionalUnit = fractionalUnit;
  }
  




  public Boolean isPrintedToKitchen()
  {
    return printedToKitchen == null ? Boolean.FALSE : printedToKitchen;
  }
  



  public void setPrintedToKitchen(Boolean printedToKitchen)
  {
    this.printedToKitchen = printedToKitchen;
  }
  




  public String getKitchenStatus()
  {
    return kitchenStatus;
  }
  



  public void setKitchenStatus(String kitchenStatus)
  {
    this.kitchenStatus = kitchenStatus;
  }
  




  public Boolean isPizzaType()
  {
    return pizzaType == null ? Boolean.FALSE : pizzaType;
  }
  



  public void setPizzaType(Boolean pizzaType)
  {
    this.pizzaType = pizzaType;
  }
  




  public Integer getPizzaSectionModeType()
  {
    return pizzaSectionModeType == null ? Integer.valueOf(0) : pizzaSectionModeType;
  }
  



  public void setPizzaSectionModeType(Integer pizzaSectionModeType)
  {
    this.pizzaSectionModeType = pizzaSectionModeType;
  }
  




  public Boolean isComboItem()
  {
    return comboItem == null ? Boolean.FALSE : comboItem;
  }
  



  public void setComboItem(Boolean comboItem)
  {
    this.comboItem = comboItem;
  }
  




  public Double getServiceChargeRate()
  {
    return serviceChargeRate == null ? Double.valueOf(0.0D) : serviceChargeRate;
  }
  



  public void setServiceChargeRate(Double serviceChargeRate)
  {
    this.serviceChargeRate = serviceChargeRate;
  }
  




  public Double getServiceCharge()
  {
    return serviceCharge == null ? Double.valueOf(0.0D) : serviceCharge;
  }
  



  public void setServiceCharge(Double serviceCharge)
  {
    this.serviceCharge = serviceCharge;
  }
  




  public Boolean isServiceChargeApplicable()
  {
    return serviceChargeApplicable == null ? Boolean.FALSE : serviceChargeApplicable;
  }
  



  public void setServiceChargeApplicable(Boolean serviceChargeApplicable)
  {
    this.serviceChargeApplicable = serviceChargeApplicable;
  }
  




  public Boolean isTaxOnServiceCharge()
  {
    return taxOnServiceCharge == null ? Boolean.FALSE : taxOnServiceCharge;
  }
  



  public void setTaxOnServiceCharge(Boolean taxOnServiceCharge)
  {
    this.taxOnServiceCharge = taxOnServiceCharge;
  }
  




  public String getPrinterGroupId()
  {
    return printerGroupId;
  }
  



  public void setPrinterGroupId(String printerGroupId)
  {
    this.printerGroupId = printerGroupId;
  }
  




  public String getTaxesProperty()
  {
    return taxesProperty;
  }
  



  public void setTaxesProperty(String taxesProperty)
  {
    this.taxesProperty = taxesProperty;
  }
  




  public String getDiscountsProperty()
  {
    return discountsProperty;
  }
  



  public void setDiscountsProperty(String discountsProperty)
  {
    this.discountsProperty = discountsProperty;
  }
  




  public String getCookingInstructionsProperty()
  {
    return cookingInstructionsProperty;
  }
  



  public void setCookingInstructionsProperty(String cookingInstructionsProperty)
  {
    this.cookingInstructionsProperty = cookingInstructionsProperty;
  }
  




  public Boolean isCloudSynced()
  {
    return cloudSynced == null ? Boolean.FALSE : cloudSynced;
  }
  



  public void setCloudSynced(Boolean cloudSynced)
  {
    this.cloudSynced = cloudSynced;
  }
  




  public Boolean isHasSyncError()
  {
    return hasSyncError == null ? Boolean.FALSE : hasSyncError;
  }
  



  public void setHasSyncError(Boolean hasSyncError)
  {
    this.hasSyncError = hasSyncError;
  }
  




  public Ticket getTicket()
  {
    return ticket;
  }
  



  public void setTicket(Ticket ticket)
  {
    this.ticket = ticket;
  }
  




  public TicketItemSeat getSeat()
  {
    return seat;
  }
  



  public void setSeat(TicketItemSeat seat)
  {
    this.seat = seat;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof TicketItem)) { return false;
    }
    TicketItem ticketItem = (TicketItem)obj;
    if ((null == getId()) || (null == ticketItem.getId())) return this == obj;
    return getId().equals(ticketItem.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
