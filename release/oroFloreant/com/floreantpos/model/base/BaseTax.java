package com.floreantpos.model.base;

import com.floreantpos.model.Tax;
import java.io.Serializable;










public abstract class BaseTax
  implements Comparable, Serializable
{
  public static String REF = "Tax";
  public static String PROP_NAME = "name";
  public static String PROP_ID = "id";
  public static String PROP_RATE = "rate";
  

  public BaseTax()
  {
    initialize();
  }
  


  public BaseTax(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseTax(String id, String name)
  {
    setId(id);
    setName(name);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String name;
  

  protected Double rate;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Double getRate()
  {
    return rate == null ? Double.valueOf(0.0D) : rate;
  }
  



  public void setRate(Double rate)
  {
    this.rate = rate;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof Tax)) { return false;
    }
    Tax tax = (Tax)obj;
    if ((null == getId()) || (null == tax.getId())) return this == obj;
    return getId().equals(tax.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
