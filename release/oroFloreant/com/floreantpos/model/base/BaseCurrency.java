package com.floreantpos.model.base;

import com.floreantpos.model.Currency;
import java.io.Serializable;










public abstract class BaseCurrency
  implements Comparable, Serializable
{
  public static String REF = "Currency";
  public static String PROP_SALES_PRICE = "salesPrice";
  public static String PROP_BUY_PRICE = "buyPrice";
  public static String PROP_NAME = "name";
  public static String PROP_MAIN = "main";
  public static String PROP_TOLERANCE = "tolerance";
  public static String PROP_EXCHANGE_RATE = "exchangeRate";
  public static String PROP_SYMBOL = "symbol";
  public static String PROP_ID = "id";
  public static String PROP_CODE = "code";
  

  public BaseCurrency()
  {
    initialize();
  }
  


  public BaseCurrency(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  private long version;
  
  protected String code;
  
  protected String name;
  
  protected String symbol;
  
  protected Double exchangeRate;
  
  protected Double tolerance;
  
  protected Double buyPrice;
  
  protected Double salesPrice;
  
  protected Boolean main;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getCode()
  {
    return code;
  }
  



  public void setCode(String code)
  {
    this.code = code;
  }
  




  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getSymbol()
  {
    return symbol;
  }
  



  public void setSymbol(String symbol)
  {
    this.symbol = symbol;
  }
  




  public Double getExchangeRate()
  {
    return exchangeRate == null ? Double.valueOf(1.0D) : exchangeRate;
  }
  



  public void setExchangeRate(Double exchangeRate)
  {
    this.exchangeRate = exchangeRate;
  }
  



  public static String getExchangeRateDefaultValue()
  {
    return "1";
  }
  



  public Double getTolerance()
  {
    return tolerance == null ? Double.valueOf(0.0D) : tolerance;
  }
  



  public void setTolerance(Double tolerance)
  {
    this.tolerance = tolerance;
  }
  




  public Double getBuyPrice()
  {
    return buyPrice == null ? Double.valueOf(0.0D) : buyPrice;
  }
  



  public void setBuyPrice(Double buyPrice)
  {
    this.buyPrice = buyPrice;
  }
  




  public Double getSalesPrice()
  {
    return salesPrice == null ? Double.valueOf(0.0D) : salesPrice;
  }
  



  public void setSalesPrice(Double salesPrice)
  {
    this.salesPrice = salesPrice;
  }
  




  public Boolean isMain()
  {
    return main == null ? Boolean.FALSE : main;
  }
  



  public void setMain(Boolean main)
  {
    this.main = main;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof Currency)) { return false;
    }
    Currency currency = (Currency)obj;
    if ((null == getId()) || (null == currency.getId())) return this == obj;
    return getId().equals(currency.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
