package com.floreantpos.model.base;

import com.floreantpos.model.PriceRule;
import java.io.Serializable;










public abstract class BasePriceRule
  implements Comparable, Serializable
{
  public static String REF = "PriceRule";
  public static String PROP_OUTLET_ID = "outletId";
  public static String PROP_PRICE_TABLE_ID = "priceTableId";
  public static String PROP_ACTIVE = "active";
  public static String PROP_DESCRIPTION = "description";
  public static String PROP_CUSTOMER_GROUP_ID = "customerGroupId";
  public static String PROP_ORDER_TYPE_ID = "orderTypeId";
  public static String PROP_SALES_AREA_ID = "salesAreaId";
  public static String PROP_PRICE_SHIFT_ID = "priceShiftId";
  public static String PROP_DEPARTMENT_ID = "departmentId";
  public static String PROP_ID = "id";
  public static String PROP_CODE = "code";
  public static String PROP_NAME = "name";
  

  public BasePriceRule()
  {
    initialize();
  }
  


  public BasePriceRule(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected String name;
  
  protected String description;
  
  protected String code;
  
  protected Boolean active;
  
  protected String outletId;
  
  protected String departmentId;
  
  protected String salesAreaId;
  
  protected String orderTypeId;
  protected String customerGroupId;
  protected String priceShiftId;
  protected String priceTableId;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getDescription()
  {
    return description;
  }
  



  public void setDescription(String description)
  {
    this.description = description;
  }
  




  public String getCode()
  {
    return code;
  }
  



  public void setCode(String code)
  {
    this.code = code;
  }
  




  public Boolean isActive()
  {
    return active == null ? Boolean.FALSE : active;
  }
  



  public void setActive(Boolean active)
  {
    this.active = active;
  }
  




  public String getOutletId()
  {
    return outletId;
  }
  



  public void setOutletId(String outletId)
  {
    this.outletId = outletId;
  }
  




  public String getDepartmentId()
  {
    return departmentId;
  }
  



  public void setDepartmentId(String departmentId)
  {
    this.departmentId = departmentId;
  }
  




  public String getSalesAreaId()
  {
    return salesAreaId;
  }
  



  public void setSalesAreaId(String salesAreaId)
  {
    this.salesAreaId = salesAreaId;
  }
  




  public String getOrderTypeId()
  {
    return orderTypeId;
  }
  



  public void setOrderTypeId(String orderTypeId)
  {
    this.orderTypeId = orderTypeId;
  }
  




  public String getCustomerGroupId()
  {
    return customerGroupId;
  }
  



  public void setCustomerGroupId(String customerGroupId)
  {
    this.customerGroupId = customerGroupId;
  }
  




  public String getPriceShiftId()
  {
    return priceShiftId;
  }
  



  public void setPriceShiftId(String priceShiftId)
  {
    this.priceShiftId = priceShiftId;
  }
  




  public String getPriceTableId()
  {
    return priceTableId;
  }
  



  public void setPriceTableId(String priceTableId)
  {
    this.priceTableId = priceTableId;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof PriceRule)) { return false;
    }
    PriceRule priceRule = (PriceRule)obj;
    if ((null == getId()) || (null == priceRule.getId())) return this == obj;
    return getId().equals(priceRule.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
