package com.floreantpos.model.base;

import com.floreantpos.model.Department;
import com.floreantpos.model.Outlet;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;







public abstract class BaseOutlet
  implements Comparable, Serializable
{
  public static String REF = "Outlet";
  public static String PROP_DESCRIPTION = "description";
  public static String PROP_DEFAULT_GRATUITY_PERCENTAGE = "defaultGratuityPercentage";
  public static String PROP_TAX_GROUP_ID = "taxGroupId";
  public static String PROP_SERVICE_CHARGE_PERCENTAGE = "serviceChargePercentage";
  public static String PROP_ID = "id";
  public static String PROP_ADDRESS_ID = "addressId";
  public static String PROP_CURRENCY_ID = "currencyId";
  public static String PROP_NAME = "name";
  

  public BaseOutlet()
  {
    initialize();
  }
  


  public BaseOutlet(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected String name;
  
  protected String description;
  
  protected Double serviceChargePercentage;
  
  protected Double defaultGratuityPercentage;
  
  protected String addressId;
  
  protected String taxGroupId;
  
  protected String currencyId;
  
  private List<Department> departments;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getDescription()
  {
    return description;
  }
  



  public void setDescription(String description)
  {
    this.description = description;
  }
  




  public Double getServiceChargePercentage()
  {
    return serviceChargePercentage == null ? Double.valueOf(0.0D) : serviceChargePercentage;
  }
  



  public void setServiceChargePercentage(Double serviceChargePercentage)
  {
    this.serviceChargePercentage = serviceChargePercentage;
  }
  




  public Double getDefaultGratuityPercentage()
  {
    return defaultGratuityPercentage == null ? Double.valueOf(0.0D) : defaultGratuityPercentage;
  }
  



  public void setDefaultGratuityPercentage(Double defaultGratuityPercentage)
  {
    this.defaultGratuityPercentage = defaultGratuityPercentage;
  }
  




  public String getAddressId()
  {
    return addressId;
  }
  



  public void setAddressId(String addressId)
  {
    this.addressId = addressId;
  }
  




  public String getTaxGroupId()
  {
    return taxGroupId;
  }
  



  public void setTaxGroupId(String taxGroupId)
  {
    this.taxGroupId = taxGroupId;
  }
  




  public String getCurrencyId()
  {
    return currencyId;
  }
  



  public void setCurrencyId(String currencyId)
  {
    this.currencyId = currencyId;
  }
  




  public List<Department> getDepartments()
  {
    return departments;
  }
  



  public void setDepartments(List<Department> departments)
  {
    this.departments = departments;
  }
  
  public void addTodepartments(Department department) {
    if (null == getDepartments()) setDepartments(new ArrayList());
    getDepartments().add(department);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof Outlet)) { return false;
    }
    Outlet outlet = (Outlet)obj;
    if ((null == getId()) || (null == outlet.getId())) return this == obj;
    return getId().equals(outlet.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
