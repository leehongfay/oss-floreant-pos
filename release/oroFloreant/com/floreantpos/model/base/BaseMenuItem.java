package com.floreantpos.model.base;

import com.floreantpos.model.Attribute;
import com.floreantpos.model.ComboGroup;
import com.floreantpos.model.ComboItem;
import com.floreantpos.model.Discount;
import com.floreantpos.model.InventoryStockUnit;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.MenuItemModifierSpec;
import com.floreantpos.model.PizzaPrice;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseMenuItem
  implements Comparable, Serializable
{
  public static String REF = "MenuItem";
  public static String PROP_SHOW_IMAGE_ONLY = "showImageOnly";
  public static String PROP_AVG_COST = "avgCost";
  public static String PROP_SHOULD_PRINT_TO_KITCHEN = "shouldPrintToKitchen";
  public static String PROP_TAX_ON_SERVICE_CHARGE = "taxOnServiceCharge";
  public static String PROP_MENU_CATEGORY_NAME = "menuCategoryName";
  public static String PROP_NAME = "name";
  public static String PROP_UNIT_ID = "unitId";
  public static String PROP_TRANSLATED_NAME = "translatedName";
  public static String PROP_IMAGE_ID = "imageId";
  public static String PROP_BEVERAGE = "beverage";
  public static String PROP_HAS_MODIFIERS = "hasModifiers";
  public static String PROP_HAS_MANDATORY_MODIFIERS = "hasMandatoryModifiers";
  public static String PROP_PRINT_KITCHEN_STICKER = "printKitchenSticker";
  public static String PROP_AVERAGE_UNIT_PURCHASE_PRICE = "averageUnitPurchasePrice";
  public static String PROP_TAX_GROUP_ID = "taxGroupId";
  public static String PROP_EDITABLE_PRICE = "editablePrice";
  public static String PROP_UNIT_NAME = "unitName";
  public static String PROP_REPORT_GROUP_ID = "reportGroupId";
  public static String PROP_DEFAULT_SELL_PORTION = "defaultSellPortion";
  public static String PROP_PRINTER_GROUP_ID = "printerGroupId";
  public static String PROP_PROPERTIES_JSON = "propertiesJson";
  public static String PROP_DISABLE_WHEN_STOCK_AMOUNT_IS_ZERO = "disableWhenStockAmountIsZero";
  public static String PROP_COMBO_ITEM = "comboItem";
  public static String PROP_TEXT_COLOR_CODE = "textColorCode";
  public static String PROP_VARIANT = "variant";
  public static String PROP_MENU_GROUP_ID = "menuGroupId";
  public static String PROP_MENU_GROUP_NAME = "menuGroupName";
  public static String PROP_PRICE = "price";
  public static String PROP_BARCODE = "barcode";
  public static String PROP_FRACTIONAL_UNIT = "fractionalUnit";
  public static String PROP_DEFAULT_RECIPE_ID = "defaultRecipeId";
  public static String PROP_HAS_VARIANT = "hasVariant";
  public static String PROP_ID = "id";
  public static String PROP_BUTTON_COLOR_CODE = "buttonColorCode";
  public static String PROP_INVENTORY_ITEM = "inventoryItem";
  public static String PROP_DESCRIPTION = "description";
  public static String PROP_MENU_CATEGORY_ID = "menuCategoryId";
  public static String PROP_REORDER_LEVEL = "reorderLevel";
  public static String PROP_COST = "cost";
  public static String PROP_SERVICE_CHARGE = "serviceCharge";
  public static String PROP_COURSE_ID = "courseId";
  public static String PROP_VISIBLE = "visible";
  public static String PROP_ENABLE = "enable";
  public static String PROP_RAW_MATERIAL = "rawMaterial";
  public static String PROP_SERVICE_CHARGE_APPLICABLE = "serviceChargeApplicable";
  public static String PROP_PIZZA_TYPE = "pizzaType";
  public static String PROP_SORT_ORDER = "sortOrder";
  public static String PROP_DISCOUNT_RATE = "discountRate";
  public static String PROP_PARENT_MENU_ITEM_ID = "parentMenuItemId";
  public static String PROP_LAST_PURCHASED_COST = "lastPurchasedCost";
  public static String PROP_REPLENISH_LEVEL = "replenishLevel";
  public static String PROP_SHORT_ID = "shortId";
  public static String PROP_SKU = "sku";
  

  public BaseMenuItem()
  {
    initialize();
  }
  


  public BaseMenuItem(String id)
  {
    setId(id);
    initialize();
  }
  






  public BaseMenuItem(String id, String name, Double price)
  {
    setId(id);
    setName(name);
    setPrice(price);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  private String shortId;
  
  private String name;
  
  private String description;
  
  private String unitName;
  
  private String translatedName;
  
  private String barcode;
  
  private String sku;
  
  private Double price;
  
  private Double cost;
  
  private Double discountRate;
  private Boolean visible;
  private Boolean beverage;
  private Boolean enable;
  private Boolean rawMaterial;
  private Boolean inventoryItem;
  private Double averageUnitPurchasePrice;
  private Double reorderLevel;
  private Double replenishLevel;
  private Boolean disableWhenStockAmountIsZero;
  private Boolean shouldPrintToKitchen;
  private Boolean printKitchenSticker;
  private Boolean hasVariant;
  private Boolean variant;
  private Boolean comboItem;
  private Integer sortOrder;
  private Integer buttonColorCode;
  private Integer textColorCode;
  private String imageId;
  private Boolean showImageOnly;
  private Boolean fractionalUnit;
  private Boolean pizzaType;
  private Integer defaultSellPortion;
  private Double lastPurchasedCost;
  private Double avgCost;
  private Double serviceCharge;
  private Boolean serviceChargeApplicable;
  private Boolean taxOnServiceCharge;
  private String menuGroupId;
  private String menuGroupName;
  private String menuCategoryId;
  private String menuCategoryName;
  private String courseId;
  private String unitId;
  private String taxGroupId;
  private String printerGroupId;
  private String reportGroupId;
  private String parentMenuItemId;
  private Boolean hasModifiers;
  private Boolean hasMandatoryModifiers;
  private String propertiesJson;
  private String defaultRecipeId;
  private Boolean editablePrice;
  private List<Attribute> attributes;
  private List<Discount> discounts;
  private List<MenuItemModifierSpec> menuItemModiferSpecs;
  private List<ComboGroup> comboGroups;
  private List<ComboItem> comboItems;
  private List<InventoryStockUnit> stockUnits;
  private List<PizzaPrice> pizzaPriceList;
  private List<MenuItem> variants;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getShortId()
  {
    return shortId;
  }
  



  public void setShortId(String shortId)
  {
    this.shortId = shortId;
  }
  




  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getDescription()
  {
    return description;
  }
  



  public void setDescription(String description)
  {
    this.description = description;
  }
  




  public String getUnitName()
  {
    return unitName;
  }
  



  public void setUnitName(String unitName)
  {
    this.unitName = unitName;
  }
  




  public String getTranslatedName()
  {
    return translatedName;
  }
  



  public void setTranslatedName(String translatedName)
  {
    this.translatedName = translatedName;
  }
  




  public String getBarcode()
  {
    return barcode;
  }
  



  public void setBarcode(String barcode)
  {
    this.barcode = barcode;
  }
  




  public String getSku()
  {
    return sku;
  }
  



  public void setSku(String sku)
  {
    this.sku = sku;
  }
  




  public Double getPrice()
  {
    return price == null ? Double.valueOf(0.0D) : price;
  }
  



  public void setPrice(Double price)
  {
    this.price = price;
  }
  




  public Double getCost()
  {
    return cost == null ? Double.valueOf(0.0D) : cost;
  }
  



  public void setCost(Double cost)
  {
    this.cost = cost;
  }
  




  public Double getDiscountRate()
  {
    return discountRate == null ? Double.valueOf(0.0D) : discountRate;
  }
  



  public void setDiscountRate(Double discountRate)
  {
    this.discountRate = discountRate;
  }
  




  public Boolean isVisible()
  {
    return visible == null ? Boolean.valueOf(true) : visible;
  }
  



  public void setVisible(Boolean visible)
  {
    this.visible = visible;
  }
  



  public static String getVisibleDefaultValue()
  {
    return "true";
  }
  



  public Boolean isBeverage()
  {
    return beverage == null ? Boolean.FALSE : beverage;
  }
  



  public void setBeverage(Boolean beverage)
  {
    this.beverage = beverage;
  }
  




  public Boolean isEnable()
  {
    return enable == null ? Boolean.valueOf(true) : enable;
  }
  



  public void setEnable(Boolean enable)
  {
    this.enable = enable;
  }
  



  public static String getEnableDefaultValue()
  {
    return "true";
  }
  



  public Boolean isRawMaterial()
  {
    return rawMaterial == null ? Boolean.FALSE : rawMaterial;
  }
  



  public void setRawMaterial(Boolean rawMaterial)
  {
    this.rawMaterial = rawMaterial;
  }
  




  public Boolean isInventoryItem()
  {
    return inventoryItem == null ? Boolean.valueOf(true) : inventoryItem;
  }
  



  public void setInventoryItem(Boolean inventoryItem)
  {
    this.inventoryItem = inventoryItem;
  }
  



  public static String getInventoryItemDefaultValue()
  {
    return "true";
  }
  



  public Double getAverageUnitPurchasePrice()
  {
    return averageUnitPurchasePrice == null ? Double.valueOf(0.0D) : averageUnitPurchasePrice;
  }
  



  public void setAverageUnitPurchasePrice(Double averageUnitPurchasePrice)
  {
    this.averageUnitPurchasePrice = averageUnitPurchasePrice;
  }
  




  public Double getReorderLevel()
  {
    return reorderLevel == null ? Double.valueOf(0.0D) : reorderLevel;
  }
  



  public void setReorderLevel(Double reorderLevel)
  {
    this.reorderLevel = reorderLevel;
  }
  




  public Double getReplenishLevel()
  {
    return replenishLevel == null ? Double.valueOf(0.0D) : replenishLevel;
  }
  



  public void setReplenishLevel(Double replenishLevel)
  {
    this.replenishLevel = replenishLevel;
  }
  




  public Boolean isDisableWhenStockAmountIsZero()
  {
    return disableWhenStockAmountIsZero == null ? Boolean.valueOf(false) : disableWhenStockAmountIsZero;
  }
  



  public void setDisableWhenStockAmountIsZero(Boolean disableWhenStockAmountIsZero)
  {
    this.disableWhenStockAmountIsZero = disableWhenStockAmountIsZero;
  }
  



  public static String getDisableWhenStockAmountIsZeroDefaultValue()
  {
    return "false";
  }
  



  public Boolean isShouldPrintToKitchen()
  {
    return shouldPrintToKitchen == null ? Boolean.valueOf(true) : shouldPrintToKitchen;
  }
  



  public void setShouldPrintToKitchen(Boolean shouldPrintToKitchen)
  {
    this.shouldPrintToKitchen = shouldPrintToKitchen;
  }
  



  public static String getShouldPrintToKitchenDefaultValue()
  {
    return "true";
  }
  



  public Boolean isPrintKitchenSticker()
  {
    return printKitchenSticker == null ? Boolean.valueOf(false) : printKitchenSticker;
  }
  



  public void setPrintKitchenSticker(Boolean printKitchenSticker)
  {
    this.printKitchenSticker = printKitchenSticker;
  }
  



  public static String getPrintKitchenStickerDefaultValue()
  {
    return "false";
  }
  



  public Boolean isHasVariant()
  {
    return hasVariant == null ? Boolean.FALSE : hasVariant;
  }
  



  public void setHasVariant(Boolean hasVariant)
  {
    this.hasVariant = hasVariant;
  }
  




  public Boolean isVariant()
  {
    return variant == null ? Boolean.FALSE : variant;
  }
  



  public void setVariant(Boolean variant)
  {
    this.variant = variant;
  }
  




  public Boolean isComboItem()
  {
    return comboItem == null ? Boolean.FALSE : comboItem;
  }
  



  public void setComboItem(Boolean comboItem)
  {
    this.comboItem = comboItem;
  }
  




  public Integer getSortOrder()
  {
    return sortOrder == null ? Integer.valueOf(0) : sortOrder;
  }
  



  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  




  public Integer getButtonColorCode()
  {
    return buttonColorCode == null ? null : buttonColorCode;
  }
  



  public void setButtonColorCode(Integer buttonColorCode)
  {
    this.buttonColorCode = buttonColorCode;
  }
  



  public static String getButtonColorCodeDefaultValue()
  {
    return "null";
  }
  



  public Integer getTextColorCode()
  {
    return textColorCode == null ? null : textColorCode;
  }
  



  public void setTextColorCode(Integer textColorCode)
  {
    this.textColorCode = textColorCode;
  }
  



  public static String getTextColorCodeDefaultValue()
  {
    return "null";
  }
  



  public String getImageId()
  {
    return imageId;
  }
  



  public void setImageId(String imageId)
  {
    this.imageId = imageId;
  }
  




  public Boolean isShowImageOnly()
  {
    return showImageOnly == null ? Boolean.FALSE : showImageOnly;
  }
  



  public void setShowImageOnly(Boolean showImageOnly)
  {
    this.showImageOnly = showImageOnly;
  }
  




  public Boolean isFractionalUnit()
  {
    return fractionalUnit == null ? Boolean.FALSE : fractionalUnit;
  }
  



  public void setFractionalUnit(Boolean fractionalUnit)
  {
    this.fractionalUnit = fractionalUnit;
  }
  




  public Boolean isPizzaType()
  {
    return pizzaType == null ? Boolean.FALSE : pizzaType;
  }
  



  public void setPizzaType(Boolean pizzaType)
  {
    this.pizzaType = pizzaType;
  }
  




  public Integer getDefaultSellPortion()
  {
    return defaultSellPortion == null ? Integer.valueOf(0) : defaultSellPortion;
  }
  



  public void setDefaultSellPortion(Integer defaultSellPortion)
  {
    this.defaultSellPortion = defaultSellPortion;
  }
  




  public Double getLastPurchasedCost()
  {
    return lastPurchasedCost == null ? Double.valueOf(0.0D) : lastPurchasedCost;
  }
  



  public void setLastPurchasedCost(Double lastPurchasedCost)
  {
    this.lastPurchasedCost = lastPurchasedCost;
  }
  




  public Double getAvgCost()
  {
    return avgCost == null ? Double.valueOf(0.0D) : avgCost;
  }
  



  public void setAvgCost(Double avgCost)
  {
    this.avgCost = avgCost;
  }
  




  public Double getServiceCharge()
  {
    return serviceCharge == null ? Double.valueOf(0.0D) : serviceCharge;
  }
  



  public void setServiceCharge(Double serviceCharge)
  {
    this.serviceCharge = serviceCharge;
  }
  




  public Boolean isServiceChargeApplicable()
  {
    return serviceChargeApplicable == null ? Boolean.FALSE : serviceChargeApplicable;
  }
  



  public void setServiceChargeApplicable(Boolean serviceChargeApplicable)
  {
    this.serviceChargeApplicable = serviceChargeApplicable;
  }
  




  public Boolean isTaxOnServiceCharge()
  {
    return taxOnServiceCharge == null ? Boolean.FALSE : taxOnServiceCharge;
  }
  



  public void setTaxOnServiceCharge(Boolean taxOnServiceCharge)
  {
    this.taxOnServiceCharge = taxOnServiceCharge;
  }
  




  public String getMenuGroupId()
  {
    return menuGroupId;
  }
  



  public void setMenuGroupId(String menuGroupId)
  {
    this.menuGroupId = menuGroupId;
  }
  




  public String getMenuGroupName()
  {
    return menuGroupName;
  }
  



  public void setMenuGroupName(String menuGroupName)
  {
    this.menuGroupName = menuGroupName;
  }
  




  public String getMenuCategoryId()
  {
    return menuCategoryId;
  }
  



  public void setMenuCategoryId(String menuCategoryId)
  {
    this.menuCategoryId = menuCategoryId;
  }
  




  public String getMenuCategoryName()
  {
    return menuCategoryName;
  }
  



  public void setMenuCategoryName(String menuCategoryName)
  {
    this.menuCategoryName = menuCategoryName;
  }
  




  public String getCourseId()
  {
    return courseId;
  }
  



  public void setCourseId(String courseId)
  {
    this.courseId = courseId;
  }
  




  public String getUnitId()
  {
    return unitId;
  }
  



  public void setUnitId(String unitId)
  {
    this.unitId = unitId;
  }
  




  public String getTaxGroupId()
  {
    return taxGroupId;
  }
  



  public void setTaxGroupId(String taxGroupId)
  {
    this.taxGroupId = taxGroupId;
  }
  




  public String getPrinterGroupId()
  {
    return printerGroupId;
  }
  



  public void setPrinterGroupId(String printerGroupId)
  {
    this.printerGroupId = printerGroupId;
  }
  




  public String getReportGroupId()
  {
    return reportGroupId;
  }
  



  public void setReportGroupId(String reportGroupId)
  {
    this.reportGroupId = reportGroupId;
  }
  




  public String getParentMenuItemId()
  {
    return parentMenuItemId;
  }
  



  public void setParentMenuItemId(String parentMenuItemId)
  {
    this.parentMenuItemId = parentMenuItemId;
  }
  




  public Boolean isHasModifiers()
  {
    return hasModifiers == null ? Boolean.FALSE : hasModifiers;
  }
  



  public void setHasModifiers(Boolean hasModifiers)
  {
    this.hasModifiers = hasModifiers;
  }
  




  public Boolean isHasMandatoryModifiers()
  {
    return hasMandatoryModifiers == null ? Boolean.FALSE : hasMandatoryModifiers;
  }
  



  public void setHasMandatoryModifiers(Boolean hasMandatoryModifiers)
  {
    this.hasMandatoryModifiers = hasMandatoryModifiers;
  }
  




  public String getPropertiesJson()
  {
    return propertiesJson;
  }
  



  public void setPropertiesJson(String propertiesJson)
  {
    this.propertiesJson = propertiesJson;
  }
  




  public String getDefaultRecipeId()
  {
    return defaultRecipeId;
  }
  



  public void setDefaultRecipeId(String defaultRecipeId)
  {
    this.defaultRecipeId = defaultRecipeId;
  }
  




  public Boolean isEditablePrice()
  {
    return editablePrice == null ? Boolean.FALSE : editablePrice;
  }
  



  public void setEditablePrice(Boolean editablePrice)
  {
    this.editablePrice = editablePrice;
  }
  




  public List<Attribute> getAttributes()
  {
    return attributes;
  }
  



  public void setAttributes(List<Attribute> attributes)
  {
    this.attributes = attributes;
  }
  
  public void addToattributes(Attribute attribute) {
    if (null == getAttributes()) setAttributes(new ArrayList());
    getAttributes().add(attribute);
  }
  




  public List<Discount> getDiscounts()
  {
    return discounts;
  }
  



  public void setDiscounts(List<Discount> discounts)
  {
    this.discounts = discounts;
  }
  
  public void addTodiscounts(Discount discount) {
    if (null == getDiscounts()) setDiscounts(new ArrayList());
    getDiscounts().add(discount);
  }
  




  public List<MenuItemModifierSpec> getMenuItemModiferSpecs()
  {
    return menuItemModiferSpecs;
  }
  



  public void setMenuItemModiferSpecs(List<MenuItemModifierSpec> menuItemModiferSpecs)
  {
    this.menuItemModiferSpecs = menuItemModiferSpecs;
  }
  
  public void addTomenuItemModiferSpecs(MenuItemModifierSpec menuItemModifierSpec) {
    if (null == getMenuItemModiferSpecs()) setMenuItemModiferSpecs(new ArrayList());
    getMenuItemModiferSpecs().add(menuItemModifierSpec);
  }
  




  public List<ComboGroup> getComboGroups()
  {
    return comboGroups;
  }
  



  public void setComboGroups(List<ComboGroup> comboGroups)
  {
    this.comboGroups = comboGroups;
  }
  
  public void addTocomboGroups(ComboGroup comboGroup) {
    if (null == getComboGroups()) setComboGroups(new ArrayList());
    getComboGroups().add(comboGroup);
  }
  




  public List<ComboItem> getComboItems()
  {
    return comboItems;
  }
  



  public void setComboItems(List<ComboItem> comboItems)
  {
    this.comboItems = comboItems;
  }
  
  public void addTocomboItems(ComboItem comboItem) {
    if (null == getComboItems()) setComboItems(new ArrayList());
    getComboItems().add(comboItem);
  }
  




  public List<InventoryStockUnit> getStockUnits()
  {
    return stockUnits;
  }
  



  public void setStockUnits(List<InventoryStockUnit> stockUnits)
  {
    this.stockUnits = stockUnits;
  }
  
  public void addTostockUnits(InventoryStockUnit inventoryStockUnit) {
    if (null == getStockUnits()) setStockUnits(new ArrayList());
    getStockUnits().add(inventoryStockUnit);
  }
  




  public List<PizzaPrice> getPizzaPriceList()
  {
    return pizzaPriceList;
  }
  



  public void setPizzaPriceList(List<PizzaPrice> pizzaPriceList)
  {
    this.pizzaPriceList = pizzaPriceList;
  }
  
  public void addTopizzaPriceList(PizzaPrice pizzaPrice) {
    if (null == getPizzaPriceList()) setPizzaPriceList(new ArrayList());
    getPizzaPriceList().add(pizzaPrice);
  }
  




  public List<MenuItem> getVariants()
  {
    return variants;
  }
  



  public void setVariants(List<MenuItem> variants)
  {
    this.variants = variants;
  }
  
  public void addTovariants(MenuItem menuItem) {
    if (null == getVariants()) setVariants(new ArrayList());
    getVariants().add(menuItem);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof MenuItem)) { return false;
    }
    MenuItem menuItem = (MenuItem)obj;
    if ((null == getId()) || (null == menuItem.getId())) return this == obj;
    return getId().equals(menuItem.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
