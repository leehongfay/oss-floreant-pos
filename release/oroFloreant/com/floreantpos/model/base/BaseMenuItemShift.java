package com.floreantpos.model.base;

import com.floreantpos.model.MenuItemShift;
import com.floreantpos.model.Shift;
import java.io.Serializable;









public abstract class BaseMenuItemShift
  implements Comparable, Serializable
{
  public static String REF = "MenuItemShift";
  public static String PROP_SHIFT_PRICE = "shiftPrice";
  public static String PROP_SHIFT = "shift";
  public static String PROP_ID = "id";
  

  public BaseMenuItemShift()
  {
    initialize();
  }
  


  public BaseMenuItemShift(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected Double shiftPrice;
  

  private Shift shift;
  


  protected void initialize() {}
  


  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Double getShiftPrice()
  {
    return shiftPrice == null ? Double.valueOf(0.0D) : shiftPrice;
  }
  



  public void setShiftPrice(Double shiftPrice)
  {
    this.shiftPrice = shiftPrice;
  }
  




  public Shift getShift()
  {
    return shift;
  }
  



  public void setShift(Shift shift)
  {
    this.shift = shift;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof MenuItemShift)) { return false;
    }
    MenuItemShift menuItemShift = (MenuItemShift)obj;
    if ((null == getId()) || (null == menuItemShift.getId())) return this == obj;
    return getId().equals(menuItemShift.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
