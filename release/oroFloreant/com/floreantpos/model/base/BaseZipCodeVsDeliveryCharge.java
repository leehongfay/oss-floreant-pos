package com.floreantpos.model.base;

import com.floreantpos.model.ZipCodeVsDeliveryCharge;
import java.io.Serializable;










public abstract class BaseZipCodeVsDeliveryCharge
  implements Comparable, Serializable
{
  public static String REF = "ZipCodeVsDeliveryCharge";
  public static String PROP_DELIVERY_CHARGE = "deliveryCharge";
  public static String PROP_ID = "id";
  public static String PROP_ZIP_CODE = "zipCode";
  

  public BaseZipCodeVsDeliveryCharge()
  {
    initialize();
  }
  


  public BaseZipCodeVsDeliveryCharge(String id)
  {
    setId(id);
    initialize();
  }
  






  public BaseZipCodeVsDeliveryCharge(String id, String zipCode, double deliveryCharge)
  {
    setId(id);
    setZipCode(zipCode);
    setDeliveryCharge(deliveryCharge);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String zipCode;
  

  protected double deliveryCharge;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getZipCode()
  {
    return zipCode;
  }
  



  public void setZipCode(String zipCode)
  {
    this.zipCode = zipCode;
  }
  




  public double getDeliveryCharge()
  {
    return deliveryCharge;
  }
  



  public void setDeliveryCharge(double deliveryCharge)
  {
    this.deliveryCharge = deliveryCharge;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof ZipCodeVsDeliveryCharge)) { return false;
    }
    ZipCodeVsDeliveryCharge zipCodeVsDeliveryCharge = (ZipCodeVsDeliveryCharge)obj;
    if ((null == getId()) || (null == zipCodeVsDeliveryCharge.getId())) return this == obj;
    return getId().equals(zipCodeVsDeliveryCharge.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
