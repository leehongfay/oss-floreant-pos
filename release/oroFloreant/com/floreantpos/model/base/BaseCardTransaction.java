package com.floreantpos.model.base;

import com.floreantpos.model.CardTransaction;
import com.floreantpos.model.PosTransaction;
import java.io.Serializable;
























public abstract class BaseCardTransaction
  extends PosTransaction
  implements Comparable, Serializable
{
  public static String REF = "CardTransaction";
  public static String PROP_CARD_NUMBER = "cardNumber";
  public static String PROP_AUTHORIZATION_CODE = "authorizationCode";
  public static String PROP_ID = "id";
  public static String PROP_CARD_TYPE = "cardType";
  

  public BaseCardTransaction()
  {
    initialize();
  }
  


  public BaseCardTransaction(String id)
  {
    super(id);
  }
  


  private int hashCode = Integer.MIN_VALUE;
  


  protected String cardNumber;
  


  protected String authorizationCode;
  

  protected String cardType;
  


  public String getCardNumber()
  {
    return cardNumber;
  }
  



  public void setCardNumber(String cardNumber)
  {
    this.cardNumber = cardNumber;
  }
  




  public String getAuthorizationCode()
  {
    return authorizationCode;
  }
  



  public void setAuthorizationCode(String authorizationCode)
  {
    this.authorizationCode = authorizationCode;
  }
  




  public String getCardType()
  {
    return cardType;
  }
  



  public void setCardType(String cardType)
  {
    this.cardType = cardType;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof CardTransaction)) { return false;
    }
    CardTransaction cardTransaction = (CardTransaction)obj;
    if ((null == getId()) || (null == cardTransaction.getId())) return false;
    return getId().equals(cardTransaction.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
