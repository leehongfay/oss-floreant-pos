package com.floreantpos.model.base;

import com.floreantpos.model.EmployeeInOutHistory;
import com.floreantpos.model.Shift;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import java.io.Serializable;
import java.util.Date;






public abstract class BaseEmployeeInOutHistory
  implements Comparable, Serializable
{
  public static String REF = "EmployeeInOutHistory";
  public static String PROP_USER = "user";
  public static String PROP_OUT_TIME = "outTime";
  public static String PROP_IN_HOUR = "inHour";
  public static String PROP_TERMINAL = "terminal";
  public static String PROP_CLOCK_OUT = "clockOut";
  public static String PROP_SHIFT = "shift";
  public static String PROP_OUT_HOUR = "outHour";
  public static String PROP_IN_TIME = "inTime";
  public static String PROP_ID = "id";
  

  public BaseEmployeeInOutHistory()
  {
    initialize();
  }
  


  public BaseEmployeeInOutHistory(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  private long version;
  
  protected Date outTime;
  
  protected Date inTime;
  
  protected Short outHour;
  
  protected Short inHour;
  
  protected Boolean clockOut;
  
  private User user;
  
  private Shift shift;
  
  private Terminal terminal;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Date getOutTime()
  {
    return outTime;
  }
  



  public void setOutTime(Date outTime)
  {
    this.outTime = outTime;
  }
  




  public Date getInTime()
  {
    return inTime;
  }
  



  public void setInTime(Date inTime)
  {
    this.inTime = inTime;
  }
  




  public Short getOutHour()
  {
    return outHour;
  }
  



  public void setOutHour(Short outHour)
  {
    this.outHour = outHour;
  }
  




  public Short getInHour()
  {
    return inHour;
  }
  



  public void setInHour(Short inHour)
  {
    this.inHour = inHour;
  }
  




  public Boolean isClockOut()
  {
    return clockOut == null ? Boolean.FALSE : clockOut;
  }
  



  public void setClockOut(Boolean clockOut)
  {
    this.clockOut = clockOut;
  }
  




  public User getUser()
  {
    return user;
  }
  



  public void setUser(User user)
  {
    this.user = user;
  }
  




  public Shift getShift()
  {
    return shift;
  }
  



  public void setShift(Shift shift)
  {
    this.shift = shift;
  }
  




  public Terminal getTerminal()
  {
    return terminal;
  }
  



  public void setTerminal(Terminal terminal)
  {
    this.terminal = terminal;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof EmployeeInOutHistory)) { return false;
    }
    EmployeeInOutHistory employeeInOutHistory = (EmployeeInOutHistory)obj;
    if ((null == getId()) || (null == employeeInOutHistory.getId())) return this == obj;
    return getId().equals(employeeInOutHistory.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
