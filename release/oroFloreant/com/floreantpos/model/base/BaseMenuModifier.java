package com.floreantpos.model.base;

import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.ModifierMultiplierPrice;
import com.floreantpos.model.PizzaModifierPrice;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;






public abstract class BaseMenuModifier
  implements Comparable, Serializable
{
  public static String REF = "MenuModifier";
  public static String PROP_FIXED_PRICE = "fixedPrice";
  public static String PROP_EXTRA_PRICE = "extraPrice";
  public static String PROP_IMAGE_ID = "imageId";
  public static String PROP_PIZZA_MODIFIER = "pizzaModifier";
  public static String PROP_SHOW_IMAGE_ONLY = "showImageOnly";
  public static String PROP_SORT_ORDER = "sortOrder";
  public static String PROP_TEXT_COLOR = "textColor";
  public static String PROP_SHOULD_SECTION_WISE_PRICE = "shouldSectionWisePrice";
  public static String PROP_NAME = "name";
  public static String PROP_SHOULD_PRINT_TO_KITCHEN = "shouldPrintToKitchen";
  public static String PROP_BUTTON_COLOR = "buttonColor";
  public static String PROP_TAX_GROUP_ID = "taxGroupId";
  public static String PROP_ENABLE = "enable";
  public static String PROP_TAG = "tag";
  public static String PROP_COMBO_MODIFIER = "comboModifier";
  public static String PROP_PRICE = "price";
  public static String PROP_ID = "id";
  public static String PROP_TRANSLATED_NAME = "translatedName";
  

  public BaseMenuModifier()
  {
    initialize();
  }
  


  public BaseMenuModifier(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected String name;
  
  protected String translatedName;
  
  protected Double price;
  
  protected Double extraPrice;
  
  protected Integer sortOrder;
  
  protected Integer buttonColor;
  
  protected Integer textColor;
  
  protected Boolean enable;
  
  protected Boolean shouldPrintToKitchen;
  
  protected String imageId;
  protected Boolean showImageOnly;
  protected Boolean fixedPrice;
  protected Boolean shouldSectionWisePrice;
  protected Boolean pizzaModifier;
  protected Boolean comboModifier;
  protected String tag;
  protected String taxGroupId;
  private List<PizzaModifierPrice> pizzaModifierPriceList;
  private List<ModifierMultiplierPrice> multiplierPriceList;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getTranslatedName()
  {
    return translatedName;
  }
  



  public void setTranslatedName(String translatedName)
  {
    this.translatedName = translatedName;
  }
  




  public Double getPrice()
  {
    return price == null ? Double.valueOf(0.0D) : price;
  }
  



  public void setPrice(Double price)
  {
    this.price = price;
  }
  




  public Double getExtraPrice()
  {
    return extraPrice == null ? Double.valueOf(0.0D) : extraPrice;
  }
  



  public void setExtraPrice(Double extraPrice)
  {
    this.extraPrice = extraPrice;
  }
  




  public Integer getSortOrder()
  {
    return sortOrder == null ? Integer.valueOf(0) : sortOrder;
  }
  



  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  




  public Integer getButtonColor()
  {
    return buttonColor == null ? Integer.valueOf(0) : buttonColor;
  }
  



  public void setButtonColor(Integer buttonColor)
  {
    this.buttonColor = buttonColor;
  }
  




  public Integer getTextColor()
  {
    return textColor == null ? Integer.valueOf(0) : textColor;
  }
  



  public void setTextColor(Integer textColor)
  {
    this.textColor = textColor;
  }
  




  public Boolean isEnable()
  {
    return enable == null ? Boolean.FALSE : enable;
  }
  



  public void setEnable(Boolean enable)
  {
    this.enable = enable;
  }
  




  public Boolean isShouldPrintToKitchen()
  {
    return shouldPrintToKitchen == null ? Boolean.valueOf(true) : shouldPrintToKitchen;
  }
  



  public void setShouldPrintToKitchen(Boolean shouldPrintToKitchen)
  {
    this.shouldPrintToKitchen = shouldPrintToKitchen;
  }
  



  public static String getShouldPrintToKitchenDefaultValue()
  {
    return "true";
  }
  



  public String getImageId()
  {
    return imageId;
  }
  



  public void setImageId(String imageId)
  {
    this.imageId = imageId;
  }
  




  public Boolean isShowImageOnly()
  {
    return showImageOnly == null ? Boolean.FALSE : showImageOnly;
  }
  



  public void setShowImageOnly(Boolean showImageOnly)
  {
    this.showImageOnly = showImageOnly;
  }
  




  public Boolean isFixedPrice()
  {
    return fixedPrice == null ? Boolean.FALSE : fixedPrice;
  }
  



  public void setFixedPrice(Boolean fixedPrice)
  {
    this.fixedPrice = fixedPrice;
  }
  




  public Boolean isShouldSectionWisePrice()
  {
    return shouldSectionWisePrice == null ? Boolean.FALSE : shouldSectionWisePrice;
  }
  



  public void setShouldSectionWisePrice(Boolean shouldSectionWisePrice)
  {
    this.shouldSectionWisePrice = shouldSectionWisePrice;
  }
  




  public Boolean isPizzaModifier()
  {
    return pizzaModifier == null ? Boolean.FALSE : pizzaModifier;
  }
  



  public void setPizzaModifier(Boolean pizzaModifier)
  {
    this.pizzaModifier = pizzaModifier;
  }
  




  public Boolean isComboModifier()
  {
    return comboModifier == null ? Boolean.FALSE : comboModifier;
  }
  



  public void setComboModifier(Boolean comboModifier)
  {
    this.comboModifier = comboModifier;
  }
  




  public String getTag()
  {
    return tag;
  }
  



  public void setTag(String tag)
  {
    this.tag = tag;
  }
  




  public String getTaxGroupId()
  {
    return taxGroupId;
  }
  



  public void setTaxGroupId(String taxGroupId)
  {
    this.taxGroupId = taxGroupId;
  }
  




  public List<PizzaModifierPrice> getPizzaModifierPriceList()
  {
    return pizzaModifierPriceList;
  }
  



  public void setPizzaModifierPriceList(List<PizzaModifierPrice> pizzaModifierPriceList)
  {
    this.pizzaModifierPriceList = pizzaModifierPriceList;
  }
  
  public void addTopizzaModifierPriceList(PizzaModifierPrice pizzaModifierPrice) {
    if (null == getPizzaModifierPriceList()) setPizzaModifierPriceList(new ArrayList());
    getPizzaModifierPriceList().add(pizzaModifierPrice);
  }
  




  public List<ModifierMultiplierPrice> getMultiplierPriceList()
  {
    return multiplierPriceList;
  }
  



  public void setMultiplierPriceList(List<ModifierMultiplierPrice> multiplierPriceList)
  {
    this.multiplierPriceList = multiplierPriceList;
  }
  
  public void addTomultiplierPriceList(ModifierMultiplierPrice modifierMultiplierPrice) {
    if (null == getMultiplierPriceList()) setMultiplierPriceList(new ArrayList());
    getMultiplierPriceList().add(modifierMultiplierPrice);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof MenuModifier)) { return false;
    }
    MenuModifier menuModifier = (MenuModifier)obj;
    if ((null == getId()) || (null == menuModifier.getId())) return this == obj;
    return getId().equals(menuModifier.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
