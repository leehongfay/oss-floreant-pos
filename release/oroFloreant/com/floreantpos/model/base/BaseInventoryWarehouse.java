package com.floreantpos.model.base;

import com.floreantpos.model.InventoryWarehouse;
import java.io.Serializable;










public abstract class BaseInventoryWarehouse
  implements Comparable, Serializable
{
  public static String REF = "InventoryWarehouse";
  public static String PROP_NAME = "name";
  public static String PROP_VISIBLE = "visible";
  public static String PROP_ID = "id";
  

  public BaseInventoryWarehouse()
  {
    initialize();
  }
  


  public BaseInventoryWarehouse(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseInventoryWarehouse(String id, String name)
  {
    setId(id);
    setName(name);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String name;
  

  protected Boolean visible;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Boolean isVisible()
  {
    return visible == null ? Boolean.FALSE : visible;
  }
  



  public void setVisible(Boolean visible)
  {
    this.visible = visible;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof InventoryWarehouse)) { return false;
    }
    InventoryWarehouse inventoryWarehouse = (InventoryWarehouse)obj;
    if ((null == getId()) || (null == inventoryWarehouse.getId())) return this == obj;
    return getId().equals(inventoryWarehouse.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
