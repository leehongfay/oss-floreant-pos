package com.floreantpos.model.base;

import com.floreantpos.model.InventoryUnit;
import java.io.Serializable;










public abstract class BaseInventoryUnit
  implements Comparable, Serializable
{
  public static String REF = "InventoryUnit";
  public static String PROP_CONVERSION_RATE = "conversionRate";
  public static String PROP_BASE_UNIT = "baseUnit";
  public static String PROP_UNIT_GROUP_ID = "unitGroupId";
  public static String PROP_ID = "id";
  public static String PROP_CODE = "code";
  public static String PROP_NAME = "name";
  

  public BaseInventoryUnit()
  {
    initialize();
  }
  


  public BaseInventoryUnit(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseInventoryUnit(String id, String code)
  {
    setId(id);
    setCode(code);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  
  long version;
  
  protected String code;
  
  protected String name;
  
  protected Boolean baseUnit;
  
  protected Double conversionRate;
  
  protected String unitGroupId;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getCode()
  {
    return code;
  }
  



  public void setCode(String code)
  {
    this.code = code;
  }
  




  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Boolean isBaseUnit()
  {
    return baseUnit == null ? Boolean.FALSE : baseUnit;
  }
  



  public void setBaseUnit(Boolean baseUnit)
  {
    this.baseUnit = baseUnit;
  }
  




  public Double getConversionRate()
  {
    return conversionRate == null ? Double.valueOf(0.0D) : conversionRate;
  }
  



  public void setConversionRate(Double conversionRate)
  {
    this.conversionRate = conversionRate;
  }
  




  public String getUnitGroupId()
  {
    return unitGroupId;
  }
  



  public void setUnitGroupId(String unitGroupId)
  {
    this.unitGroupId = unitGroupId;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof InventoryUnit)) { return false;
    }
    InventoryUnit inventoryUnit = (InventoryUnit)obj;
    if ((null == getId()) || (null == inventoryUnit.getId())) return this == obj;
    return getId().equals(inventoryUnit.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
