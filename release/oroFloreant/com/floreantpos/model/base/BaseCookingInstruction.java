package com.floreantpos.model.base;

import com.floreantpos.model.CookingInstruction;
import java.io.Serializable;










public abstract class BaseCookingInstruction
  implements Comparable, Serializable
{
  public static String REF = "CookingInstruction";
  public static String PROP_DESCRIPTION = "description";
  public static String PROP_ID = "id";
  

  public BaseCookingInstruction()
  {
    initialize();
  }
  


  public BaseCookingInstruction(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String description;
  


  protected void initialize() {}
  


  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getDescription()
  {
    return description;
  }
  



  public void setDescription(String description)
  {
    this.description = description;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof CookingInstruction)) { return false;
    }
    CookingInstruction cookingInstruction = (CookingInstruction)obj;
    if ((null == getId()) || (null == cookingInstruction.getId())) return this == obj;
    return getId().equals(cookingInstruction.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
