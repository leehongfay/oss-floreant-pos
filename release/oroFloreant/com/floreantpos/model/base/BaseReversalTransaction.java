package com.floreantpos.model.base;

import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.ReversalTransaction;
import java.io.Serializable;








public abstract class BaseReversalTransaction
  extends PosTransaction
  implements Comparable, Serializable
{
  public static String REF = "ReversalTransaction";
  public static String PROP_ID = "id";
  

  public BaseReversalTransaction()
  {
    initialize();
  }
  


  public BaseReversalTransaction(String id)
  {
    super(id);
  }
  






  public BaseReversalTransaction(String id, String transactionType, String paymentType)
  {
    super(id, transactionType, paymentType);
  }
  





  private int hashCode = Integer.MIN_VALUE;
  







  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof ReversalTransaction)) { return false;
    }
    ReversalTransaction reversalTransaction = (ReversalTransaction)obj;
    if ((null == getId()) || (null == reversalTransaction.getId())) return this == obj;
    return getId().equals(reversalTransaction.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
