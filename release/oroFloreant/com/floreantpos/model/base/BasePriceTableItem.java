package com.floreantpos.model.base;

import com.floreantpos.model.PriceTableItem;
import java.io.Serializable;










public abstract class BasePriceTableItem
  implements Comparable, Serializable
{
  public static String REF = "PriceTableItem";
  public static String PROP_PRICE_TABLE_ID = "priceTableId";
  public static String PROP_PRICE = "price";
  public static String PROP_MENU_ITEM_ID = "menuItemId";
  public static String PROP_ITEM_BARCODE = "itemBarcode";
  public static String PROP_ID = "id";
  

  public BasePriceTableItem()
  {
    initialize();
  }
  


  public BasePriceTableItem(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  long version;
  
  protected String menuItemId;
  
  protected String itemBarcode;
  
  protected Double price;
  
  protected String priceTableId;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getMenuItemId()
  {
    return menuItemId;
  }
  



  public void setMenuItemId(String menuItemId)
  {
    this.menuItemId = menuItemId;
  }
  




  public String getItemBarcode()
  {
    return itemBarcode;
  }
  



  public void setItemBarcode(String itemBarcode)
  {
    this.itemBarcode = itemBarcode;
  }
  




  public Double getPrice()
  {
    return price == null ? Double.valueOf(0.0D) : price;
  }
  



  public void setPrice(Double price)
  {
    this.price = price;
  }
  




  public String getPriceTableId()
  {
    return priceTableId;
  }
  



  public void setPriceTableId(String priceTableId)
  {
    this.priceTableId = priceTableId;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof PriceTableItem)) { return false;
    }
    PriceTableItem priceTableItem = (PriceTableItem)obj;
    if ((null == getId()) || (null == priceTableItem.getId())) return this == obj;
    return getId().equals(priceTableItem.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
