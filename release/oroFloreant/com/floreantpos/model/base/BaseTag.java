package com.floreantpos.model.base;

import com.floreantpos.model.Tag;
import java.io.Serializable;










public abstract class BaseTag
  implements Comparable, Serializable
{
  public static String REF = "Tag";
  public static String PROP_NAME = "name";
  public static String PROP_ID = "id";
  

  public BaseTag()
  {
    initialize();
  }
  


  public BaseTag(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseTag(String id, String name)
  {
    setId(id);
    setName(name);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String name;
  


  protected void initialize() {}
  


  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof Tag)) { return false;
    }
    Tag tag = (Tag)obj;
    if ((null == getId()) || (null == tag.getId())) return this == obj;
    return getId().equals(tag.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
