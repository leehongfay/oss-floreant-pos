package com.floreantpos.model.base;

import com.floreantpos.model.DefaultMenuModifier;
import com.floreantpos.model.MenuItemModifierPage;
import com.floreantpos.model.MenuItemModifierSpec;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;







public abstract class BaseMenuItemModifierSpec
  implements Comparable, Serializable
{
  public static String REF = "MenuItemModifierSpec";
  public static String PROP_JUMP_GROUP = "jumpGroup";
  public static String PROP_EXCLUSIVE = "exclusive";
  public static String PROP_SORT_ORDER = "sortOrder";
  public static String PROP_INSTRUCTION = "instruction";
  public static String PROP_MODIFIER_GROUP_ID = "modifierGroupId";
  public static String PROP_MIN_QUANTITY = "minQuantity";
  public static String PROP_NAME = "name";
  public static String PROP_REQUIRED = "required";
  public static String PROP_TRANSLATED_NAME = "translatedName";
  public static String PROP_AUTO_SHOW = "autoShow";
  public static String PROP_ENABLE = "enable";
  public static String PROP_MAX_QUANTITY = "maxQuantity";
  public static String PROP_ID = "id";
  

  public BaseMenuItemModifierSpec()
  {
    initialize();
  }
  


  public BaseMenuItemModifierSpec(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected String name;
  
  protected String translatedName;
  
  protected String instruction;
  
  protected Boolean enable;
  
  protected Boolean exclusive;
  
  protected Boolean required;
  
  protected Integer minQuantity;
  
  protected Integer maxQuantity;
  
  protected Integer sortOrder;
  
  protected Boolean jumpGroup;
  protected Boolean autoShow;
  protected String modifierGroupId;
  private Set<MenuItemModifierPage> modifierPages;
  private List<DefaultMenuModifier> defaultModifierList;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getTranslatedName()
  {
    return translatedName;
  }
  



  public void setTranslatedName(String translatedName)
  {
    this.translatedName = translatedName;
  }
  




  public String getInstruction()
  {
    return instruction;
  }
  



  public void setInstruction(String instruction)
  {
    this.instruction = instruction;
  }
  




  public Boolean isEnable()
  {
    return enable == null ? Boolean.FALSE : enable;
  }
  



  public void setEnable(Boolean enable)
  {
    this.enable = enable;
  }
  




  public Boolean isExclusive()
  {
    return exclusive == null ? Boolean.FALSE : exclusive;
  }
  



  public void setExclusive(Boolean exclusive)
  {
    this.exclusive = exclusive;
  }
  




  public Boolean isRequired()
  {
    return required == null ? Boolean.FALSE : required;
  }
  



  public void setRequired(Boolean required)
  {
    this.required = required;
  }
  




  public Integer getMinQuantity()
  {
    return minQuantity == null ? Integer.valueOf(0) : minQuantity;
  }
  



  public void setMinQuantity(Integer minQuantity)
  {
    this.minQuantity = minQuantity;
  }
  




  public Integer getMaxQuantity()
  {
    return maxQuantity == null ? Integer.valueOf(0) : maxQuantity;
  }
  



  public void setMaxQuantity(Integer maxQuantity)
  {
    this.maxQuantity = maxQuantity;
  }
  




  public Integer getSortOrder()
  {
    return sortOrder == null ? Integer.valueOf(0) : sortOrder;
  }
  



  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  




  public Boolean isJumpGroup()
  {
    return jumpGroup == null ? Boolean.FALSE : jumpGroup;
  }
  



  public void setJumpGroup(Boolean jumpGroup)
  {
    this.jumpGroup = jumpGroup;
  }
  




  public Boolean isAutoShow()
  {
    return autoShow == null ? Boolean.FALSE : autoShow;
  }
  



  public void setAutoShow(Boolean autoShow)
  {
    this.autoShow = autoShow;
  }
  




  public String getModifierGroupId()
  {
    return modifierGroupId;
  }
  



  public void setModifierGroupId(String modifierGroupId)
  {
    this.modifierGroupId = modifierGroupId;
  }
  




  public Set<MenuItemModifierPage> getModifierPages()
  {
    return modifierPages;
  }
  



  public void setModifierPages(Set<MenuItemModifierPage> modifierPages)
  {
    this.modifierPages = modifierPages;
  }
  
  public void addTomodifierPages(MenuItemModifierPage menuItemModifierPage) {
    if (null == getModifierPages()) setModifierPages(new TreeSet());
    getModifierPages().add(menuItemModifierPage);
  }
  




  public List<DefaultMenuModifier> getDefaultModifierList()
  {
    return defaultModifierList;
  }
  



  public void setDefaultModifierList(List<DefaultMenuModifier> defaultModifierList)
  {
    this.defaultModifierList = defaultModifierList;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof MenuItemModifierSpec)) { return false;
    }
    MenuItemModifierSpec menuItemModifierSpec = (MenuItemModifierSpec)obj;
    if ((null == getId()) || (null == menuItemModifierSpec.getId())) return this == obj;
    return getId().equals(menuItemModifierSpec.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
