package com.floreantpos.model.base;

import com.floreantpos.model.ComboTicketItem;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemModifier;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;





public abstract class BaseComboTicketItem
  extends TicketItem
  implements Comparable, Serializable
{
  public static String REF = "ComboTicketItem";
  public static String PROP_SIZE_MODIFIER = "sizeModifier";
  public static String PROP_ID = "id";
  

  public BaseComboTicketItem()
  {
    initialize();
  }
  


  public BaseComboTicketItem(String id)
  {
    super(id);
  }
  


  private int hashCode = Integer.MIN_VALUE;
  


  private TicketItemModifier sizeModifier;
  


  private List<TicketItem> comboItems;
  


  private List<TicketItemModifier> ticketItemModifiers;
  



  public TicketItemModifier getSizeModifier()
  {
    return sizeModifier;
  }
  



  public void setSizeModifier(TicketItemModifier sizeModifier)
  {
    this.sizeModifier = sizeModifier;
  }
  




  public List<TicketItem> getComboItems()
  {
    return comboItems;
  }
  



  public void setComboItems(List<TicketItem> comboItems)
  {
    this.comboItems = comboItems;
  }
  
  public void addTocomboItems(TicketItem ticketItem) {
    if (null == getComboItems()) setComboItems(new ArrayList());
    getComboItems().add(ticketItem);
  }
  




  public List<TicketItemModifier> getTicketItemModifiers()
  {
    return ticketItemModifiers;
  }
  



  public void setTicketItemModifiers(List<TicketItemModifier> ticketItemModifiers)
  {
    this.ticketItemModifiers = ticketItemModifiers;
  }
  
  public void addToticketItemModifiers(TicketItemModifier ticketItemModifier) {
    if (null == getTicketItemModifiers()) setTicketItemModifiers(new ArrayList());
    getTicketItemModifiers().add(ticketItemModifier);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof ComboTicketItem)) { return false;
    }
    ComboTicketItem comboTicketItem = (ComboTicketItem)obj;
    if ((null == getId()) || (null == comboTicketItem.getId())) return this == obj;
    return getId().equals(comboTicketItem.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
