package com.floreantpos.model.base;

import com.floreantpos.model.CashBreakdown;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.StoreSession;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;



public abstract class BaseCashDrawer
  implements Comparable, Serializable
{
  public static String REF = "CashDrawer";
  public static String PROP_STORE_OPERATION_DATA = "storeOperationData";
  public static String PROP_TOTAL_DISCOUNT_CHECK_SIZE = "totalDiscountCheckSize";
  public static String PROP_CUSTOMER_PAYMENT_COUNT = "customerPaymentCount";
  public static String PROP_CUSTOM_PAYMENT_AMOUNT = "customPaymentAmount";
  public static String PROP_PAY_OUT_COUNT = "payOutCount";
  public static String PROP_DRAWER_BLEED_COUNT = "drawerBleedCount";
  public static String PROP_DRAWER_ACCOUNTABLE = "drawerAccountable";
  public static String PROP_GIFT_CERT_CHANGE_AMOUNT = "giftCertChangeAmount";
  public static String PROP_TOTAL_DISCOUNT_GUEST = "totalDiscountGuest";
  public static String PROP_NET_SALES = "netSales";
  public static String PROP_CASH_BACK = "cashBack";
  public static String PROP_CLOSED_BY_USER_ID = "closedByUserId";
  public static String PROP_DRAWER_BLEED_AMOUNT = "drawerBleedAmount";
  public static String PROP_CREDIT_CARD_RECEIPT_COUNT = "creditCardReceiptCount";
  public static String PROP_REFUND_RECEIPT_COUNT = "refundReceiptCount";
  public static String PROP_CREDIT_CARD_RECEIPT_AMOUNT = "creditCardReceiptAmount";
  public static String PROP_BEGIN_CASH = "beginCash";
  public static String PROP_TIPS_PAID = "tipsPaid";
  public static String PROP_TOTAL_DISCOUNT_COUNT = "totalDiscountCount";
  public static String PROP_TOTAL_DISCOUNT_AMOUNT = "totalDiscountAmount";
  public static String PROP_TOTAL_REVENUE = "totalRevenue";
  public static String PROP_ID = "id";
  public static String PROP_GIFT_CERT_RETURN_COUNT = "giftCertReturnCount";
  public static String PROP_TOTAL_DISCOUNT_SALES = "totalDiscountSales";
  public static String PROP_VARIANCE = "variance";
  public static String PROP_PAY_OUT_AMOUNT = "payOutAmount";
  public static String PROP_ASSIGNED_BY_USER_ID = "assignedByUserId";
  public static String PROP_DEBIT_CARD_RECEIPT_AMOUNT = "debitCardReceiptAmount";
  public static String PROP_TYPE = "type";
  public static String PROP_SALES_TAX = "salesTax";
  public static String PROP_CUSTOM_PAYMENT_COUNT = "customPaymentCount";
  public static String PROP_CASH_RECEIPT_COUNT = "cashReceiptCount";
  public static String PROP_TOTAL_VOID = "totalVoid";
  public static String PROP_TIPS_DIFFERENTIAL = "tipsDifferential";
  public static String PROP_TERMINAL_ID = "terminalId";
  public static String PROP_CASH_TAX = "cashTax";
  public static String PROP_RECEIPT_DIFFERENTIAL = "receiptDifferential";
  public static String PROP_START_TIME = "startTime";
  public static String PROP_REFUND_AMOUNT = "refundAmount";
  public static String PROP_SALES_DELIVERY_CHARGE = "salesDeliveryCharge";
  public static String PROP_CASH_RECEIPT_AMOUNT = "cashReceiptAmount";
  public static String PROP_TOTAL_DISCOUNT_PARTY_SIZE = "totalDiscountPartySize";
  public static String PROP_REG = "reg";
  public static String PROP_TOTAL_VOID_WST = "totalVoidWst";
  public static String PROP_DEBIT_CARD_RECEIPT_COUNT = "debitCardReceiptCount";
  public static String PROP_TOTAL_DISCOUNT_PERCENTAGE = "totalDiscountPercentage";
  public static String PROP_GIFT_CERT_RETURN_AMOUNT = "giftCertReturnAmount";
  public static String PROP_ASSIGNED_USER_ID = "assignedUserId";
  public static String PROP_TOTAL_DISCOUNT_RATIO = "totalDiscountRatio";
  public static String PROP_CASH_TO_DEPOSIT = "cashToDeposit";
  public static String PROP_CUSTOMER_PAYMENT_AMOUNT = "customerPaymentAmount";
  public static String PROP_GROSS_RECEIPTS = "grossReceipts";
  public static String PROP_CHARGED_TIPS = "chargedTips";
  public static String PROP_DECLARED_TIPS = "declaredTips";
  public static String PROP_REPORT_TIME = "reportTime";
  public static String PROP_TICKET_COUNT = "ticketCount";
  public static String PROP_CASH_TIPS = "cashTips";
  

  public BaseCashDrawer()
  {
    initialize();
  }
  


  public BaseCashDrawer(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseCashDrawer(String id, StoreSession storeOperationData)
  {
    setId(id);
    setStoreOperationData(storeOperationData);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  private Date startTime;
  
  private Date reportTime;
  
  private Integer type;
  
  private String reg;
  
  private Integer ticketCount;
  
  private Double beginCash;
  
  private Double netSales;
  
  private Double salesTax;
  
  private Double cashTax;
  
  private Double totalRevenue;
  
  private Double grossReceipts;
  
  private Integer giftCertReturnCount;
  private Double giftCertReturnAmount;
  private Double giftCertChangeAmount;
  private Integer cashReceiptCount;
  private Double cashReceiptAmount;
  private Integer customPaymentCount;
  private Double customPaymentAmount;
  private Integer creditCardReceiptCount;
  private Double creditCardReceiptAmount;
  private Integer debitCardReceiptCount;
  private Double debitCardReceiptAmount;
  private Integer refundReceiptCount;
  private Double refundAmount;
  private Double receiptDifferential;
  private Double cashBack;
  private Double cashTips;
  private Double chargedTips;
  private Double tipsPaid;
  private Double tipsDifferential;
  private Integer payOutCount;
  private Double payOutAmount;
  private Integer drawerBleedCount;
  private Double drawerBleedAmount;
  private Double drawerAccountable;
  private Double declaredTips;
  private Double cashToDeposit;
  private Double variance;
  private Double salesDeliveryCharge;
  private Integer customerPaymentCount;
  private Double customerPaymentAmount;
  private Double totalVoidWst;
  private Double totalVoid;
  private Integer totalDiscountCount;
  private Double totalDiscountAmount;
  private Double totalDiscountSales;
  private Integer totalDiscountGuest;
  private Integer totalDiscountPartySize;
  private Integer totalDiscountCheckSize;
  private Double totalDiscountPercentage;
  private Double totalDiscountRatio;
  private String assignedUserId;
  private String assignedByUserId;
  private String closedByUserId;
  private Integer terminalId;
  private StoreSession storeOperationData;
  private List<PosTransaction> transactions;
  private Map<String, Double> otherRevenueCategory;
  private List<CashBreakdown> cashBreakdownList;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Date getStartTime()
  {
    return startTime;
  }
  



  public void setStartTime(Date startTime)
  {
    this.startTime = startTime;
  }
  




  public Date getReportTime()
  {
    return reportTime;
  }
  



  public void setReportTime(Date reportTime)
  {
    this.reportTime = reportTime;
  }
  




  public Integer getType()
  {
    return type == null ? Integer.valueOf(0) : type;
  }
  



  public void setType(Integer type)
  {
    this.type = type;
  }
  




  public String getReg()
  {
    return reg;
  }
  



  public void setReg(String reg)
  {
    this.reg = reg;
  }
  




  public Integer getTicketCount()
  {
    return ticketCount == null ? Integer.valueOf(0) : ticketCount;
  }
  



  public void setTicketCount(Integer ticketCount)
  {
    this.ticketCount = ticketCount;
  }
  




  public Double getBeginCash()
  {
    return beginCash == null ? Double.valueOf(0.0D) : beginCash;
  }
  



  public void setBeginCash(Double beginCash)
  {
    this.beginCash = beginCash;
  }
  




  public Double getNetSales()
  {
    return netSales == null ? Double.valueOf(0.0D) : netSales;
  }
  



  public void setNetSales(Double netSales)
  {
    this.netSales = netSales;
  }
  




  public Double getSalesTax()
  {
    return salesTax == null ? Double.valueOf(0.0D) : salesTax;
  }
  



  public void setSalesTax(Double salesTax)
  {
    this.salesTax = salesTax;
  }
  




  public Double getCashTax()
  {
    return cashTax == null ? Double.valueOf(0.0D) : cashTax;
  }
  



  public void setCashTax(Double cashTax)
  {
    this.cashTax = cashTax;
  }
  




  public Double getTotalRevenue()
  {
    return totalRevenue == null ? Double.valueOf(0.0D) : totalRevenue;
  }
  



  public void setTotalRevenue(Double totalRevenue)
  {
    this.totalRevenue = totalRevenue;
  }
  




  public Double getGrossReceipts()
  {
    return grossReceipts == null ? Double.valueOf(0.0D) : grossReceipts;
  }
  



  public void setGrossReceipts(Double grossReceipts)
  {
    this.grossReceipts = grossReceipts;
  }
  




  public Integer getGiftCertReturnCount()
  {
    return giftCertReturnCount == null ? Integer.valueOf(0) : giftCertReturnCount;
  }
  



  public void setGiftCertReturnCount(Integer giftCertReturnCount)
  {
    this.giftCertReturnCount = giftCertReturnCount;
  }
  




  public Double getGiftCertReturnAmount()
  {
    return giftCertReturnAmount == null ? Double.valueOf(0.0D) : giftCertReturnAmount;
  }
  



  public void setGiftCertReturnAmount(Double giftCertReturnAmount)
  {
    this.giftCertReturnAmount = giftCertReturnAmount;
  }
  




  public Double getGiftCertChangeAmount()
  {
    return giftCertChangeAmount == null ? Double.valueOf(0.0D) : giftCertChangeAmount;
  }
  



  public void setGiftCertChangeAmount(Double giftCertChangeAmount)
  {
    this.giftCertChangeAmount = giftCertChangeAmount;
  }
  




  public Integer getCashReceiptCount()
  {
    return cashReceiptCount == null ? Integer.valueOf(0) : cashReceiptCount;
  }
  



  public void setCashReceiptCount(Integer cashReceiptCount)
  {
    this.cashReceiptCount = cashReceiptCount;
  }
  




  public Double getCashReceiptAmount()
  {
    return cashReceiptAmount == null ? Double.valueOf(0.0D) : cashReceiptAmount;
  }
  



  public void setCashReceiptAmount(Double cashReceiptAmount)
  {
    this.cashReceiptAmount = cashReceiptAmount;
  }
  




  public Integer getCustomPaymentCount()
  {
    return customPaymentCount == null ? Integer.valueOf(0) : customPaymentCount;
  }
  



  public void setCustomPaymentCount(Integer customPaymentCount)
  {
    this.customPaymentCount = customPaymentCount;
  }
  




  public Double getCustomPaymentAmount()
  {
    return customPaymentAmount == null ? Double.valueOf(0.0D) : customPaymentAmount;
  }
  



  public void setCustomPaymentAmount(Double customPaymentAmount)
  {
    this.customPaymentAmount = customPaymentAmount;
  }
  




  public Integer getCreditCardReceiptCount()
  {
    return creditCardReceiptCount == null ? Integer.valueOf(0) : creditCardReceiptCount;
  }
  



  public void setCreditCardReceiptCount(Integer creditCardReceiptCount)
  {
    this.creditCardReceiptCount = creditCardReceiptCount;
  }
  




  public Double getCreditCardReceiptAmount()
  {
    return creditCardReceiptAmount == null ? Double.valueOf(0.0D) : creditCardReceiptAmount;
  }
  



  public void setCreditCardReceiptAmount(Double creditCardReceiptAmount)
  {
    this.creditCardReceiptAmount = creditCardReceiptAmount;
  }
  




  public Integer getDebitCardReceiptCount()
  {
    return debitCardReceiptCount == null ? Integer.valueOf(0) : debitCardReceiptCount;
  }
  



  public void setDebitCardReceiptCount(Integer debitCardReceiptCount)
  {
    this.debitCardReceiptCount = debitCardReceiptCount;
  }
  




  public Double getDebitCardReceiptAmount()
  {
    return debitCardReceiptAmount == null ? Double.valueOf(0.0D) : debitCardReceiptAmount;
  }
  



  public void setDebitCardReceiptAmount(Double debitCardReceiptAmount)
  {
    this.debitCardReceiptAmount = debitCardReceiptAmount;
  }
  




  public Integer getRefundReceiptCount()
  {
    return refundReceiptCount == null ? Integer.valueOf(0) : refundReceiptCount;
  }
  



  public void setRefundReceiptCount(Integer refundReceiptCount)
  {
    this.refundReceiptCount = refundReceiptCount;
  }
  




  public Double getRefundAmount()
  {
    return refundAmount == null ? Double.valueOf(0.0D) : refundAmount;
  }
  



  public void setRefundAmount(Double refundAmount)
  {
    this.refundAmount = refundAmount;
  }
  




  public Double getReceiptDifferential()
  {
    return receiptDifferential == null ? Double.valueOf(0.0D) : receiptDifferential;
  }
  



  public void setReceiptDifferential(Double receiptDifferential)
  {
    this.receiptDifferential = receiptDifferential;
  }
  




  public Double getCashBack()
  {
    return cashBack == null ? Double.valueOf(0.0D) : cashBack;
  }
  



  public void setCashBack(Double cashBack)
  {
    this.cashBack = cashBack;
  }
  




  public Double getCashTips()
  {
    return cashTips == null ? Double.valueOf(0.0D) : cashTips;
  }
  



  public void setCashTips(Double cashTips)
  {
    this.cashTips = cashTips;
  }
  




  public Double getChargedTips()
  {
    return chargedTips == null ? Double.valueOf(0.0D) : chargedTips;
  }
  



  public void setChargedTips(Double chargedTips)
  {
    this.chargedTips = chargedTips;
  }
  




  public Double getTipsPaid()
  {
    return tipsPaid == null ? Double.valueOf(0.0D) : tipsPaid;
  }
  



  public void setTipsPaid(Double tipsPaid)
  {
    this.tipsPaid = tipsPaid;
  }
  




  public Double getTipsDifferential()
  {
    return tipsDifferential == null ? Double.valueOf(0.0D) : tipsDifferential;
  }
  



  public void setTipsDifferential(Double tipsDifferential)
  {
    this.tipsDifferential = tipsDifferential;
  }
  




  public Integer getPayOutCount()
  {
    return payOutCount == null ? Integer.valueOf(0) : payOutCount;
  }
  



  public void setPayOutCount(Integer payOutCount)
  {
    this.payOutCount = payOutCount;
  }
  




  public Double getPayOutAmount()
  {
    return payOutAmount == null ? Double.valueOf(0.0D) : payOutAmount;
  }
  



  public void setPayOutAmount(Double payOutAmount)
  {
    this.payOutAmount = payOutAmount;
  }
  




  public Integer getDrawerBleedCount()
  {
    return drawerBleedCount == null ? Integer.valueOf(0) : drawerBleedCount;
  }
  



  public void setDrawerBleedCount(Integer drawerBleedCount)
  {
    this.drawerBleedCount = drawerBleedCount;
  }
  




  public Double getDrawerBleedAmount()
  {
    return drawerBleedAmount == null ? Double.valueOf(0.0D) : drawerBleedAmount;
  }
  



  public void setDrawerBleedAmount(Double drawerBleedAmount)
  {
    this.drawerBleedAmount = drawerBleedAmount;
  }
  




  public Double getDrawerAccountable()
  {
    return drawerAccountable == null ? Double.valueOf(0.0D) : drawerAccountable;
  }
  



  public void setDrawerAccountable(Double drawerAccountable)
  {
    this.drawerAccountable = drawerAccountable;
  }
  




  public Double getDeclaredTips()
  {
    return declaredTips == null ? Double.valueOf(0.0D) : declaredTips;
  }
  



  public void setDeclaredTips(Double declaredTips)
  {
    this.declaredTips = declaredTips;
  }
  




  public Double getCashToDeposit()
  {
    return cashToDeposit == null ? Double.valueOf(0.0D) : cashToDeposit;
  }
  



  public void setCashToDeposit(Double cashToDeposit)
  {
    this.cashToDeposit = cashToDeposit;
  }
  




  public Double getVariance()
  {
    return variance == null ? Double.valueOf(0.0D) : variance;
  }
  



  public void setVariance(Double variance)
  {
    this.variance = variance;
  }
  




  public Double getSalesDeliveryCharge()
  {
    return salesDeliveryCharge == null ? Double.valueOf(0.0D) : salesDeliveryCharge;
  }
  



  public void setSalesDeliveryCharge(Double salesDeliveryCharge)
  {
    this.salesDeliveryCharge = salesDeliveryCharge;
  }
  




  public Integer getCustomerPaymentCount()
  {
    return customerPaymentCount == null ? Integer.valueOf(0) : customerPaymentCount;
  }
  



  public void setCustomerPaymentCount(Integer customerPaymentCount)
  {
    this.customerPaymentCount = customerPaymentCount;
  }
  




  public Double getCustomerPaymentAmount()
  {
    return customerPaymentAmount == null ? Double.valueOf(0.0D) : customerPaymentAmount;
  }
  



  public void setCustomerPaymentAmount(Double customerPaymentAmount)
  {
    this.customerPaymentAmount = customerPaymentAmount;
  }
  




  public Double getTotalVoidWst()
  {
    return totalVoidWst == null ? Double.valueOf(0.0D) : totalVoidWst;
  }
  



  public void setTotalVoidWst(Double totalVoidWst)
  {
    this.totalVoidWst = totalVoidWst;
  }
  




  public Double getTotalVoid()
  {
    return totalVoid == null ? Double.valueOf(0.0D) : totalVoid;
  }
  



  public void setTotalVoid(Double totalVoid)
  {
    this.totalVoid = totalVoid;
  }
  




  public Integer getTotalDiscountCount()
  {
    return totalDiscountCount == null ? Integer.valueOf(0) : totalDiscountCount;
  }
  



  public void setTotalDiscountCount(Integer totalDiscountCount)
  {
    this.totalDiscountCount = totalDiscountCount;
  }
  




  public Double getTotalDiscountAmount()
  {
    return totalDiscountAmount == null ? Double.valueOf(0.0D) : totalDiscountAmount;
  }
  



  public void setTotalDiscountAmount(Double totalDiscountAmount)
  {
    this.totalDiscountAmount = totalDiscountAmount;
  }
  




  public Double getTotalDiscountSales()
  {
    return totalDiscountSales == null ? Double.valueOf(0.0D) : totalDiscountSales;
  }
  



  public void setTotalDiscountSales(Double totalDiscountSales)
  {
    this.totalDiscountSales = totalDiscountSales;
  }
  




  public Integer getTotalDiscountGuest()
  {
    return totalDiscountGuest == null ? Integer.valueOf(0) : totalDiscountGuest;
  }
  



  public void setTotalDiscountGuest(Integer totalDiscountGuest)
  {
    this.totalDiscountGuest = totalDiscountGuest;
  }
  




  public Integer getTotalDiscountPartySize()
  {
    return totalDiscountPartySize == null ? Integer.valueOf(0) : totalDiscountPartySize;
  }
  



  public void setTotalDiscountPartySize(Integer totalDiscountPartySize)
  {
    this.totalDiscountPartySize = totalDiscountPartySize;
  }
  




  public Integer getTotalDiscountCheckSize()
  {
    return totalDiscountCheckSize == null ? Integer.valueOf(0) : totalDiscountCheckSize;
  }
  



  public void setTotalDiscountCheckSize(Integer totalDiscountCheckSize)
  {
    this.totalDiscountCheckSize = totalDiscountCheckSize;
  }
  




  public Double getTotalDiscountPercentage()
  {
    return totalDiscountPercentage == null ? Double.valueOf(0.0D) : totalDiscountPercentage;
  }
  



  public void setTotalDiscountPercentage(Double totalDiscountPercentage)
  {
    this.totalDiscountPercentage = totalDiscountPercentage;
  }
  




  public Double getTotalDiscountRatio()
  {
    return totalDiscountRatio == null ? Double.valueOf(0.0D) : totalDiscountRatio;
  }
  



  public void setTotalDiscountRatio(Double totalDiscountRatio)
  {
    this.totalDiscountRatio = totalDiscountRatio;
  }
  




  public String getAssignedUserId()
  {
    return assignedUserId;
  }
  



  public void setAssignedUserId(String assignedUserId)
  {
    this.assignedUserId = assignedUserId;
  }
  




  public String getAssignedByUserId()
  {
    return assignedByUserId;
  }
  



  public void setAssignedByUserId(String assignedByUserId)
  {
    this.assignedByUserId = assignedByUserId;
  }
  




  public String getClosedByUserId()
  {
    return closedByUserId;
  }
  



  public void setClosedByUserId(String closedByUserId)
  {
    this.closedByUserId = closedByUserId;
  }
  




  public Integer getTerminalId()
  {
    return terminalId == null ? Integer.valueOf(0) : terminalId;
  }
  



  public void setTerminalId(Integer terminalId)
  {
    this.terminalId = terminalId;
  }
  




  public StoreSession getStoreOperationData()
  {
    return storeOperationData;
  }
  



  public void setStoreOperationData(StoreSession storeOperationData)
  {
    this.storeOperationData = storeOperationData;
  }
  




  public List<PosTransaction> getTransactions()
  {
    return transactions;
  }
  



  public void setTransactions(List<PosTransaction> transactions)
  {
    this.transactions = transactions;
  }
  
  public void addTotransactions(PosTransaction posTransaction) {
    if (null == getTransactions()) setTransactions(new ArrayList());
    getTransactions().add(posTransaction);
  }
  




  public Map<String, Double> getOtherRevenueCategory()
  {
    return otherRevenueCategory;
  }
  



  public void setOtherRevenueCategory(Map<String, Double> otherRevenueCategory)
  {
    this.otherRevenueCategory = otherRevenueCategory;
  }
  




  public List<CashBreakdown> getCashBreakdownList()
  {
    return cashBreakdownList;
  }
  



  public void setCashBreakdownList(List<CashBreakdown> cashBreakdownList)
  {
    this.cashBreakdownList = cashBreakdownList;
  }
  
  public void addTocashBreakdownList(CashBreakdown cashBreakdown) {
    if (null == getCashBreakdownList()) setCashBreakdownList(new ArrayList());
    getCashBreakdownList().add(cashBreakdown);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof CashDrawer)) { return false;
    }
    CashDrawer cashDrawer = (CashDrawer)obj;
    if ((null == getId()) || (null == cashDrawer.getId())) return this == obj;
    return getId().equals(cashDrawer.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
