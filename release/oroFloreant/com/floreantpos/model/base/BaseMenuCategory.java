package com.floreantpos.model.base;

import com.floreantpos.model.Department;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuShift;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.TerminalType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public abstract class BaseMenuCategory
  implements Comparable, Serializable
{
  public static String REF = "MenuCategory";
  public static String PROP_NAME = "name";
  public static String PROP_TEXT_COLOR_CODE = "textColorCode";
  public static String PROP_BEVERAGE = "beverage";
  public static String PROP_VISIBLE = "visible";
  public static String PROP_SORT_ORDER = "sortOrder";
  public static String PROP_BUTTON_COLOR_CODE = "buttonColorCode";
  public static String PROP_ID = "id";
  public static String PROP_TRANSLATED_NAME = "translatedName";
  

  public BaseMenuCategory()
  {
    initialize();
  }
  


  public BaseMenuCategory(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseMenuCategory(String id, String name)
  {
    setId(id);
    setName(name);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected String name;
  
  protected String translatedName;
  
  protected Boolean visible;
  
  protected Boolean beverage;
  
  protected Integer sortOrder;
  
  protected Integer buttonColorCode;
  
  protected Integer textColorCode;
  
  private List<MenuGroup> menuGroups;
  
  private Set<OrderType> orderTypes;
  
  private Set<Department> departments;
  private Set<TerminalType> terminalTypes;
  private Set<MenuShift> menuShifts;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getTranslatedName()
  {
    return translatedName;
  }
  



  public void setTranslatedName(String translatedName)
  {
    this.translatedName = translatedName;
  }
  




  public Boolean isVisible()
  {
    return visible == null ? Boolean.valueOf(true) : visible;
  }
  



  public void setVisible(Boolean visible)
  {
    this.visible = visible;
  }
  



  public static String getVisibleDefaultValue()
  {
    return "true";
  }
  



  public Boolean isBeverage()
  {
    return beverage == null ? Boolean.FALSE : beverage;
  }
  



  public void setBeverage(Boolean beverage)
  {
    this.beverage = beverage;
  }
  




  public Integer getSortOrder()
  {
    return sortOrder == null ? Integer.valueOf(0) : sortOrder;
  }
  



  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  




  public Integer getButtonColorCode()
  {
    return buttonColorCode == null ? Integer.valueOf(0) : buttonColorCode;
  }
  



  public void setButtonColorCode(Integer buttonColorCode)
  {
    this.buttonColorCode = buttonColorCode;
  }
  




  public Integer getTextColorCode()
  {
    return textColorCode == null ? Integer.valueOf(0) : textColorCode;
  }
  



  public void setTextColorCode(Integer textColorCode)
  {
    this.textColorCode = textColorCode;
  }
  




  public List<MenuGroup> getMenuGroups()
  {
    return menuGroups;
  }
  



  public void setMenuGroups(List<MenuGroup> menuGroups)
  {
    this.menuGroups = menuGroups;
  }
  
  public void addTomenuGroups(MenuGroup menuGroup) {
    if (null == getMenuGroups()) setMenuGroups(new ArrayList());
    getMenuGroups().add(menuGroup);
  }
  




  public Set<OrderType> getOrderTypes()
  {
    return orderTypes;
  }
  



  public void setOrderTypes(Set<OrderType> orderTypes)
  {
    this.orderTypes = orderTypes;
  }
  
  public void addToorderTypes(OrderType orderType) {
    if (null == getOrderTypes()) setOrderTypes(new TreeSet());
    getOrderTypes().add(orderType);
  }
  




  public Set<Department> getDepartments()
  {
    return departments;
  }
  



  public void setDepartments(Set<Department> departments)
  {
    this.departments = departments;
  }
  
  public void addTodepartments(Department department) {
    if (null == getDepartments()) setDepartments(new TreeSet());
    getDepartments().add(department);
  }
  




  public Set<TerminalType> getTerminalTypes()
  {
    return terminalTypes;
  }
  



  public void setTerminalTypes(Set<TerminalType> terminalTypes)
  {
    this.terminalTypes = terminalTypes;
  }
  
  public void addToterminalTypes(TerminalType terminalType) {
    if (null == getTerminalTypes()) setTerminalTypes(new TreeSet());
    getTerminalTypes().add(terminalType);
  }
  




  public Set<MenuShift> getMenuShifts()
  {
    return menuShifts;
  }
  



  public void setMenuShifts(Set<MenuShift> menuShifts)
  {
    this.menuShifts = menuShifts;
  }
  
  public void addTomenuShifts(MenuShift menuShift) {
    if (null == getMenuShifts()) setMenuShifts(new TreeSet());
    getMenuShifts().add(menuShift);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof MenuCategory)) { return false;
    }
    MenuCategory menuCategory = (MenuCategory)obj;
    if ((null == getId()) || (null == menuCategory.getId())) return this == obj;
    return getId().equals(menuCategory.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
