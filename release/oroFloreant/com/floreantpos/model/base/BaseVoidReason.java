package com.floreantpos.model.base;

import com.floreantpos.model.VoidReason;
import java.io.Serializable;










public abstract class BaseVoidReason
  implements Comparable, Serializable
{
  public static String REF = "VoidReason";
  public static String PROP_REASON_TEXT = "reasonText";
  public static String PROP_ID = "id";
  

  public BaseVoidReason()
  {
    initialize();
  }
  


  public BaseVoidReason(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String reasonText;
  


  protected void initialize() {}
  


  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getReasonText()
  {
    return reasonText;
  }
  



  public void setReasonText(String reasonText)
  {
    this.reasonText = reasonText;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof VoidReason)) { return false;
    }
    VoidReason voidReason = (VoidReason)obj;
    if ((null == getId()) || (null == voidReason.getId())) return this == obj;
    return getId().equals(voidReason.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
