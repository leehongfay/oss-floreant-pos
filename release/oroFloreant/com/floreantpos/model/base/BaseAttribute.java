package com.floreantpos.model.base;

import com.floreantpos.model.Attribute;
import com.floreantpos.model.AttributeGroup;
import java.io.Serializable;









public abstract class BaseAttribute
  implements Comparable, Serializable
{
  public static String REF = "Attribute";
  public static String PROP_DEFAULT_ATTRIBUTE = "defaultAttribute";
  public static String PROP_NAME = "name";
  public static String PROP_SORT_ORDER = "sortOrder";
  public static String PROP_ID = "id";
  public static String PROP_GROUP = "group";
  

  public BaseAttribute()
  {
    initialize();
  }
  


  public BaseAttribute(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String name;
  

  protected Integer sortOrder;
  
  protected Boolean defaultAttribute;
  
  private AttributeGroup group;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Integer getSortOrder()
  {
    return sortOrder == null ? Integer.valueOf(0) : sortOrder;
  }
  



  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  




  public Boolean isDefaultAttribute()
  {
    return defaultAttribute == null ? Boolean.FALSE : defaultAttribute;
  }
  



  public void setDefaultAttribute(Boolean defaultAttribute)
  {
    this.defaultAttribute = defaultAttribute;
  }
  




  public AttributeGroup getGroup()
  {
    return group;
  }
  



  public void setGroup(AttributeGroup group)
  {
    this.group = group;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof Attribute)) { return false;
    }
    Attribute attribute = (Attribute)obj;
    if ((null == getId()) || (null == attribute.getId())) return this == obj;
    return getId().equals(attribute.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
