package com.floreantpos.model.base;

import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;






public abstract class BaseUser
  implements Comparable, Serializable
{
  public static String REF = "User";
  public static String PROP_IMAGE_ID = "imageId";
  public static String PROP_BLIND_ACCOUNTABLE_AMOUNT = "blindAccountableAmount";
  public static String PROP_STAFF_BANK_STARTED = "staffBankStarted";
  public static String PROP_PASSWORD = "password";
  public static String PROP_LAST_NAME = "lastName";
  public static String PROP_CURRENT_CASH_DRAWER = "currentCashDrawer";
  public static String PROP_PHONE_NO = "phoneNo";
  public static String PROP_USER_TYPE_ID = "userTypeId";
  public static String PROP_DRIVER = "driver";
  public static String PROP_ALLOW_RECEIVE_TIPS = "allowReceiveTips";
  public static String PROP_AVAILABLE_FOR_DELIVERY = "availableForDelivery";
  public static String PROP_OUTLET_ID = "outletId";
  public static String PROP_FIRST_NAME = "firstName";
  public static String PROP_CURRENT_SHIFT_ID = "currentShiftId";
  public static String PROP_CLOCKED_IN = "clockedIn";
  public static String PROP_LAST_CLOCK_OUT_TIME = "lastClockOutTime";
  public static String PROP_LAST_CLOCK_IN_TIME = "lastClockInTime";
  public static String PROP_PARENT_USER = "parentUser";
  public static String PROP_AUTO_START_STAFF_BANK = "autoStartStaffBank";
  public static String PROP_SSN = "ssn";
  public static String PROP_ACTIVE = "active";
  public static String PROP_OVERTIME_RATE_PER_HOUR = "overtimeRatePerHour";
  public static String PROP_ROOT = "root";
  public static String PROP_ID = "id";
  public static String PROP_COST_PER_HOUR = "costPerHour";
  public static String PROP_STAFF_BANK = "staffBank";
  

  public BaseUser()
  {
    initialize();
  }
  


  public BaseUser(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected String password;
  
  protected String firstName;
  
  protected String lastName;
  
  protected String ssn;
  
  protected Double costPerHour;
  
  protected Boolean clockedIn;
  
  protected Date lastClockInTime;
  
  protected Date lastClockOutTime;
  
  protected String phoneNo;
  
  protected Boolean driver;
  
  protected Boolean allowReceiveTips;
  
  protected Boolean staffBank;
  protected Boolean autoStartStaffBank;
  protected Boolean staffBankStarted;
  protected Boolean blindAccountableAmount;
  protected Boolean availableForDelivery;
  protected Boolean active;
  protected String imageId;
  protected Boolean root;
  protected String outletId;
  protected Double overtimeRatePerHour;
  protected String currentShiftId;
  protected String userTypeId;
  private User parentUser;
  private CashDrawer currentCashDrawer;
  private List<User> linkedUser;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getPassword()
  {
    return password;
  }
  



  public void setPassword(String password)
  {
    this.password = password;
  }
  




  public String getFirstName()
  {
    return firstName;
  }
  



  public void setFirstName(String firstName)
  {
    this.firstName = firstName;
  }
  




  public String getLastName()
  {
    return lastName;
  }
  



  public void setLastName(String lastName)
  {
    this.lastName = lastName;
  }
  




  public String getSsn()
  {
    return ssn;
  }
  



  public void setSsn(String ssn)
  {
    this.ssn = ssn;
  }
  




  public Double getCostPerHour()
  {
    return costPerHour == null ? Double.valueOf(0.0D) : costPerHour;
  }
  



  public void setCostPerHour(Double costPerHour)
  {
    this.costPerHour = costPerHour;
  }
  




  public Boolean isClockedIn()
  {
    return clockedIn == null ? Boolean.FALSE : clockedIn;
  }
  



  public void setClockedIn(Boolean clockedIn)
  {
    this.clockedIn = clockedIn;
  }
  




  public Date getLastClockInTime()
  {
    return lastClockInTime;
  }
  



  public void setLastClockInTime(Date lastClockInTime)
  {
    this.lastClockInTime = lastClockInTime;
  }
  




  public Date getLastClockOutTime()
  {
    return lastClockOutTime;
  }
  



  public void setLastClockOutTime(Date lastClockOutTime)
  {
    this.lastClockOutTime = lastClockOutTime;
  }
  




  public String getPhoneNo()
  {
    return phoneNo;
  }
  



  public void setPhoneNo(String phoneNo)
  {
    this.phoneNo = phoneNo;
  }
  




  public Boolean isDriver()
  {
    return driver == null ? Boolean.FALSE : driver;
  }
  



  public void setDriver(Boolean driver)
  {
    this.driver = driver;
  }
  




  public Boolean isAllowReceiveTips()
  {
    return allowReceiveTips == null ? Boolean.FALSE : allowReceiveTips;
  }
  



  public void setAllowReceiveTips(Boolean allowReceiveTips)
  {
    this.allowReceiveTips = allowReceiveTips;
  }
  




  public Boolean isStaffBank()
  {
    return staffBank == null ? Boolean.FALSE : staffBank;
  }
  



  public void setStaffBank(Boolean staffBank)
  {
    this.staffBank = staffBank;
  }
  




  public Boolean isAutoStartStaffBank()
  {
    return autoStartStaffBank == null ? Boolean.FALSE : autoStartStaffBank;
  }
  



  public void setAutoStartStaffBank(Boolean autoStartStaffBank)
  {
    this.autoStartStaffBank = autoStartStaffBank;
  }
  




  public Boolean isStaffBankStarted()
  {
    return staffBankStarted == null ? Boolean.FALSE : staffBankStarted;
  }
  



  public void setStaffBankStarted(Boolean staffBankStarted)
  {
    this.staffBankStarted = staffBankStarted;
  }
  




  public Boolean isBlindAccountableAmount()
  {
    return blindAccountableAmount == null ? Boolean.FALSE : blindAccountableAmount;
  }
  



  public void setBlindAccountableAmount(Boolean blindAccountableAmount)
  {
    this.blindAccountableAmount = blindAccountableAmount;
  }
  




  public Boolean isAvailableForDelivery()
  {
    return availableForDelivery == null ? Boolean.FALSE : availableForDelivery;
  }
  



  public void setAvailableForDelivery(Boolean availableForDelivery)
  {
    this.availableForDelivery = availableForDelivery;
  }
  




  public Boolean isActive()
  {
    return active == null ? Boolean.FALSE : active;
  }
  



  public void setActive(Boolean active)
  {
    this.active = active;
  }
  




  public String getImageId()
  {
    return imageId;
  }
  



  public void setImageId(String imageId)
  {
    this.imageId = imageId;
  }
  




  public Boolean isRoot()
  {
    return root == null ? Boolean.FALSE : root;
  }
  



  public void setRoot(Boolean root)
  {
    this.root = root;
  }
  




  public String getOutletId()
  {
    return outletId;
  }
  



  public void setOutletId(String outletId)
  {
    this.outletId = outletId;
  }
  




  public Double getOvertimeRatePerHour()
  {
    return overtimeRatePerHour == null ? Double.valueOf(0.0D) : overtimeRatePerHour;
  }
  



  public void setOvertimeRatePerHour(Double overtimeRatePerHour)
  {
    this.overtimeRatePerHour = overtimeRatePerHour;
  }
  




  public String getCurrentShiftId()
  {
    return currentShiftId;
  }
  



  public void setCurrentShiftId(String currentShiftId)
  {
    this.currentShiftId = currentShiftId;
  }
  




  public String getUserTypeId()
  {
    return userTypeId;
  }
  



  public void setUserTypeId(String userTypeId)
  {
    this.userTypeId = userTypeId;
  }
  




  public User getParentUser()
  {
    return parentUser;
  }
  



  public void setParentUser(User parentUser)
  {
    this.parentUser = parentUser;
  }
  




  public CashDrawer getCurrentCashDrawer()
  {
    return currentCashDrawer;
  }
  



  public void setCurrentCashDrawer(CashDrawer currentCashDrawer)
  {
    this.currentCashDrawer = currentCashDrawer;
  }
  




  public List<User> getLinkedUser()
  {
    return linkedUser;
  }
  



  public void setLinkedUser(List<User> linkedUser)
  {
    this.linkedUser = linkedUser;
  }
  
  public void addTolinkedUser(User user) {
    if (null == getLinkedUser()) setLinkedUser(new ArrayList());
    getLinkedUser().add(user);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof User)) { return false;
    }
    User user = (User)obj;
    if ((null == getId()) || (null == user.getId())) return this == obj;
    return getId().equals(user.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
