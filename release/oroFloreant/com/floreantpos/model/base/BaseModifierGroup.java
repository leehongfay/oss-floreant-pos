package com.floreantpos.model.base;

import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.ModifierGroup;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;







public abstract class BaseModifierGroup
  implements Comparable, Serializable
{
  public static String REF = "ModifierGroup";
  public static String PROP_NAME = "name";
  public static String PROP_PIZZA_MODIFIER_GROUP = "pizzaModifierGroup";
  public static String PROP_ENABLE = "enable";
  public static String PROP_SORT_ORDER = "sortOrder";
  public static String PROP_ID = "id";
  public static String PROP_TRANSLATED_NAME = "translatedName";
  

  public BaseModifierGroup()
  {
    initialize();
  }
  


  public BaseModifierGroup(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  
  protected String name;
  
  protected String translatedName;
  
  protected Integer sortOrder;
  
  protected Boolean enable;
  
  protected Boolean pizzaModifierGroup;
  
  private List<MenuModifier> modifiers;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getTranslatedName()
  {
    return translatedName;
  }
  



  public void setTranslatedName(String translatedName)
  {
    this.translatedName = translatedName;
  }
  




  public Integer getSortOrder()
  {
    return sortOrder == null ? Integer.valueOf(0) : sortOrder;
  }
  



  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  




  public Boolean isEnable()
  {
    return enable == null ? Boolean.FALSE : enable;
  }
  



  public void setEnable(Boolean enable)
  {
    this.enable = enable;
  }
  




  public Boolean isPizzaModifierGroup()
  {
    return pizzaModifierGroup == null ? Boolean.FALSE : pizzaModifierGroup;
  }
  



  public void setPizzaModifierGroup(Boolean pizzaModifierGroup)
  {
    this.pizzaModifierGroup = pizzaModifierGroup;
  }
  




  public List<MenuModifier> getModifiers()
  {
    return modifiers;
  }
  



  public void setModifiers(List<MenuModifier> modifiers)
  {
    this.modifiers = modifiers;
  }
  
  public void addTomodifiers(MenuModifier menuModifier) {
    if (null == getModifiers()) setModifiers(new ArrayList());
    getModifiers().add(menuModifier);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof ModifierGroup)) { return false;
    }
    ModifierGroup modifierGroup = (ModifierGroup)obj;
    if ((null == getId()) || (null == modifierGroup.getId())) return this == obj;
    return getId().equals(modifierGroup.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
