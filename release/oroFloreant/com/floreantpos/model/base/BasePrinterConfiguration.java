package com.floreantpos.model.base;

import com.floreantpos.model.PrinterConfiguration;
import java.io.Serializable;










public abstract class BasePrinterConfiguration
  implements Comparable, Serializable
{
  public static String REF = "PrinterConfiguration";
  public static String PROP_PRINT_KITCHEN_WHEN_TICKET_PAID = "printKitchenWhenTicketPaid";
  public static String PROP_PRINT_KITCHEN_WHEN_TICKET_SETTLED = "printKitchenWhenTicketSettled";
  public static String PROP_KITCHEN_PRINTER_NAME = "kitchenPrinterName";
  public static String PROP_USE_NORMAL_PRINTER_FOR_TICKET = "useNormalPrinterForTicket";
  public static String PROP_RECEIPT_PRINTER_NAME = "receiptPrinterName";
  public static String PROP_PRINT_RECREIPT_WHEN_TICKET_SETTLED = "printRecreiptWhenTicketSettled";
  public static String PROP_ID = "id";
  public static String PROP_USE_NORMAL_PRINTER_FOR_KITCHEN = "useNormalPrinterForKitchen";
  public static String PROP_PRINT_RECEIPT_WHEN_TICKET_PAID = "printReceiptWhenTicketPaid";
  

  public BasePrinterConfiguration()
  {
    initialize();
  }
  


  public BasePrinterConfiguration(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  private long version;
  
  protected String receiptPrinterName;
  
  protected String kitchenPrinterName;
  
  protected Boolean printRecreiptWhenTicketSettled;
  
  protected Boolean printKitchenWhenTicketSettled;
  
  protected Boolean printReceiptWhenTicketPaid;
  
  protected Boolean printKitchenWhenTicketPaid;
  
  protected Boolean useNormalPrinterForTicket;
  
  protected Boolean useNormalPrinterForKitchen;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getReceiptPrinterName()
  {
    return receiptPrinterName;
  }
  



  public void setReceiptPrinterName(String receiptPrinterName)
  {
    this.receiptPrinterName = receiptPrinterName;
  }
  




  public String getKitchenPrinterName()
  {
    return kitchenPrinterName;
  }
  



  public void setKitchenPrinterName(String kitchenPrinterName)
  {
    this.kitchenPrinterName = kitchenPrinterName;
  }
  




  public Boolean isPrintRecreiptWhenTicketSettled()
  {
    return printRecreiptWhenTicketSettled == null ? Boolean.valueOf(true) : printRecreiptWhenTicketSettled;
  }
  



  public void setPrintRecreiptWhenTicketSettled(Boolean printRecreiptWhenTicketSettled)
  {
    this.printRecreiptWhenTicketSettled = printRecreiptWhenTicketSettled;
  }
  



  public static String getPrintRecreiptWhenTicketSettledDefaultValue()
  {
    return "true";
  }
  



  public Boolean isPrintKitchenWhenTicketSettled()
  {
    return printKitchenWhenTicketSettled == null ? Boolean.valueOf(true) : printKitchenWhenTicketSettled;
  }
  



  public void setPrintKitchenWhenTicketSettled(Boolean printKitchenWhenTicketSettled)
  {
    this.printKitchenWhenTicketSettled = printKitchenWhenTicketSettled;
  }
  



  public static String getPrintKitchenWhenTicketSettledDefaultValue()
  {
    return "true";
  }
  



  public Boolean isPrintReceiptWhenTicketPaid()
  {
    return printReceiptWhenTicketPaid == null ? Boolean.valueOf(true) : printReceiptWhenTicketPaid;
  }
  



  public void setPrintReceiptWhenTicketPaid(Boolean printReceiptWhenTicketPaid)
  {
    this.printReceiptWhenTicketPaid = printReceiptWhenTicketPaid;
  }
  



  public static String getPrintReceiptWhenTicketPaidDefaultValue()
  {
    return "true";
  }
  



  public Boolean isPrintKitchenWhenTicketPaid()
  {
    return printKitchenWhenTicketPaid == null ? Boolean.valueOf(true) : printKitchenWhenTicketPaid;
  }
  



  public void setPrintKitchenWhenTicketPaid(Boolean printKitchenWhenTicketPaid)
  {
    this.printKitchenWhenTicketPaid = printKitchenWhenTicketPaid;
  }
  



  public static String getPrintKitchenWhenTicketPaidDefaultValue()
  {
    return "true";
  }
  



  public Boolean isUseNormalPrinterForTicket()
  {
    return useNormalPrinterForTicket == null ? Boolean.valueOf(false) : useNormalPrinterForTicket;
  }
  



  public void setUseNormalPrinterForTicket(Boolean useNormalPrinterForTicket)
  {
    this.useNormalPrinterForTicket = useNormalPrinterForTicket;
  }
  



  public static String getUseNormalPrinterForTicketDefaultValue()
  {
    return "false";
  }
  



  public Boolean isUseNormalPrinterForKitchen()
  {
    return useNormalPrinterForKitchen == null ? Boolean.valueOf(false) : useNormalPrinterForKitchen;
  }
  



  public void setUseNormalPrinterForKitchen(Boolean useNormalPrinterForKitchen)
  {
    this.useNormalPrinterForKitchen = useNormalPrinterForKitchen;
  }
  



  public static String getUseNormalPrinterForKitchenDefaultValue()
  {
    return "false";
  }
  


  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof PrinterConfiguration)) { return false;
    }
    PrinterConfiguration printerConfiguration = (PrinterConfiguration)obj;
    if ((null == getId()) || (null == printerConfiguration.getId())) return this == obj;
    return getId().equals(printerConfiguration.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
