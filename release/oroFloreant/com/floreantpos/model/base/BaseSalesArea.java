package com.floreantpos.model.base;

import com.floreantpos.model.SalesArea;
import java.io.Serializable;










public abstract class BaseSalesArea
  implements Comparable, Serializable
{
  public static String REF = "SalesArea";
  public static String PROP_DEPARTMENT_ID = "departmentId";
  public static String PROP_ID = "id";
  public static String PROP_NAME = "name";
  

  public BaseSalesArea()
  {
    initialize();
  }
  


  public BaseSalesArea(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  long version;
  

  protected String name;
  

  protected String departmentId;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getDepartmentId()
  {
    return departmentId;
  }
  



  public void setDepartmentId(String departmentId)
  {
    this.departmentId = departmentId;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof SalesArea)) { return false;
    }
    SalesArea salesArea = (SalesArea)obj;
    if ((null == getId()) || (null == salesArea.getId())) return this == obj;
    return getId().equals(salesArea.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
