package com.floreantpos.model.base;

import com.floreantpos.model.GlobalConfig;
import java.io.Serializable;










public abstract class BaseGlobalConfig
  implements Comparable, Serializable
{
  public static String REF = "GlobalConfig";
  public static String PROP_VALUE = "value";
  public static String PROP_KEY = "key";
  public static String PROP_ID = "id";
  

  public BaseGlobalConfig()
  {
    initialize();
  }
  


  public BaseGlobalConfig(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String key;
  

  protected String value;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getKey()
  {
    return key;
  }
  



  public void setKey(String key)
  {
    this.key = key;
  }
  




  public String getValue()
  {
    return value;
  }
  



  public void setValue(String value)
  {
    this.value = value;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof GlobalConfig)) { return false;
    }
    GlobalConfig globalConfig = (GlobalConfig)obj;
    if ((null == getId()) || (null == globalConfig.getId())) return this == obj;
    return getId().equals(globalConfig.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
