package com.floreantpos.model.base;

import com.floreantpos.model.TicketItemModifier;
import com.floreantpos.model.TicketItemSection;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;







public abstract class BaseTicketItemSection
  implements Comparable, Serializable
{
  public static String REF = "TicketItemSection";
  public static String PROP_NAME = "name";
  public static String PROP_MAIN = "main";
  public static String PROP_SORT_ORDER = "sortOrder";
  public static String PROP_ID = "id";
  public static String PROP_SHOW_NAME = "showName";
  

  public BaseTicketItemSection()
  {
    initialize();
  }
  


  public BaseTicketItemSection(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String name;
  
  protected Integer sortOrder;
  
  protected Boolean main;
  
  protected Boolean showName;
  
  private List<TicketItemModifier> toppings;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Integer getSortOrder()
  {
    return sortOrder == null ? Integer.valueOf(0) : sortOrder;
  }
  



  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  




  public Boolean isMain()
  {
    return main == null ? Boolean.FALSE : main;
  }
  



  public void setMain(Boolean main)
  {
    this.main = main;
  }
  




  public Boolean isShowName()
  {
    return showName == null ? Boolean.FALSE : showName;
  }
  



  public void setShowName(Boolean showName)
  {
    this.showName = showName;
  }
  




  public List<TicketItemModifier> getToppings()
  {
    return toppings;
  }
  



  public void setToppings(List<TicketItemModifier> toppings)
  {
    this.toppings = toppings;
  }
  
  public void addTotoppings(TicketItemModifier ticketItemModifier) {
    if (null == getToppings()) setToppings(new ArrayList());
    getToppings().add(ticketItemModifier);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof TicketItemSection)) { return false;
    }
    TicketItemSection ticketItemSection = (TicketItemSection)obj;
    if ((null == getId()) || (null == ticketItemSection.getId())) return this == obj;
    return getId().equals(ticketItemSection.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
