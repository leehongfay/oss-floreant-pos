package com.floreantpos.model.base;

import com.floreantpos.model.AttendenceHistory;
import com.floreantpos.model.Shift;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import java.io.Serializable;
import java.util.Date;






public abstract class BaseAttendenceHistory
  implements Comparable, Serializable
{
  public static String REF = "AttendenceHistory";
  public static String PROP_USER = "user";
  public static String PROP_CLOCK_IN_TIME = "clockInTime";
  public static String PROP_CLOCK_OUT_TIME = "clockOutTime";
  public static String PROP_TERMINAL = "terminal";
  public static String PROP_CLOCK_IN_HOUR = "clockInHour";
  public static String PROP_CLOCKED_OUT = "clockedOut";
  public static String PROP_SHIFT = "shift";
  public static String PROP_ID = "id";
  public static String PROP_CLOCK_OUT_HOUR = "clockOutHour";
  

  public BaseAttendenceHistory()
  {
    initialize();
  }
  


  public BaseAttendenceHistory(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  private long version;
  
  protected Date clockInTime;
  
  protected Date clockOutTime;
  
  protected Short clockInHour;
  
  protected Short clockOutHour;
  
  protected Boolean clockedOut;
  
  private User user;
  
  private Shift shift;
  
  private Terminal terminal;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Date getClockInTime()
  {
    return clockInTime;
  }
  



  public void setClockInTime(Date clockInTime)
  {
    this.clockInTime = clockInTime;
  }
  




  public Date getClockOutTime()
  {
    return clockOutTime;
  }
  



  public void setClockOutTime(Date clockOutTime)
  {
    this.clockOutTime = clockOutTime;
  }
  




  public Short getClockInHour()
  {
    return clockInHour;
  }
  



  public void setClockInHour(Short clockInHour)
  {
    this.clockInHour = clockInHour;
  }
  




  public Short getClockOutHour()
  {
    return clockOutHour;
  }
  



  public void setClockOutHour(Short clockOutHour)
  {
    this.clockOutHour = clockOutHour;
  }
  




  public Boolean isClockedOut()
  {
    return clockedOut == null ? Boolean.FALSE : clockedOut;
  }
  



  public void setClockedOut(Boolean clockedOut)
  {
    this.clockedOut = clockedOut;
  }
  




  public User getUser()
  {
    return user;
  }
  



  public void setUser(User user)
  {
    this.user = user;
  }
  




  public Shift getShift()
  {
    return shift;
  }
  



  public void setShift(Shift shift)
  {
    this.shift = shift;
  }
  




  public Terminal getTerminal()
  {
    return terminal;
  }
  



  public void setTerminal(Terminal terminal)
  {
    this.terminal = terminal;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof AttendenceHistory)) { return false;
    }
    AttendenceHistory attendenceHistory = (AttendenceHistory)obj;
    if ((null == getId()) || (null == attendenceHistory.getId())) return this == obj;
    return getId().equals(attendenceHistory.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
