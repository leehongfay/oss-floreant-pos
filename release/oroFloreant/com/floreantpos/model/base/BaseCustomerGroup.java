package com.floreantpos.model.base;

import com.floreantpos.model.Customer;
import com.floreantpos.model.CustomerGroup;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;







public abstract class BaseCustomerGroup
  implements Comparable, Serializable
{
  public static String REF = "CustomerGroup";
  public static String PROP_NAME = "name";
  public static String PROP_DESCRIPTION = "description";
  public static String PROP_ID = "id";
  public static String PROP_CODE = "code";
  

  public BaseCustomerGroup()
  {
    initialize();
  }
  


  public BaseCustomerGroup(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String name;
  

  protected String code;
  
  protected String description;
  
  private List<Customer> customers;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getCode()
  {
    return code;
  }
  



  public void setCode(String code)
  {
    this.code = code;
  }
  




  public String getDescription()
  {
    return description;
  }
  



  public void setDescription(String description)
  {
    this.description = description;
  }
  




  public List<Customer> getCustomers()
  {
    return customers;
  }
  



  public void setCustomers(List<Customer> customers)
  {
    this.customers = customers;
  }
  
  public void addTocustomers(Customer customer) {
    if (null == getCustomers()) setCustomers(new ArrayList());
    getCustomers().add(customer);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof CustomerGroup)) { return false;
    }
    CustomerGroup customerGroup = (CustomerGroup)obj;
    if ((null == getId()) || (null == customerGroup.getId())) return this == obj;
    return getId().equals(customerGroup.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
