package com.floreantpos.model.base;

import com.floreantpos.model.ShopTableStatus;
import java.io.Serializable;










public abstract class BaseShopTableStatus
  implements Comparable, Serializable
{
  public static String REF = "ShopTableStatus";
  public static String PROP_TICKET_INFORMATIONS = "ticketInformations";
  public static String PROP_ID = "id";
  public static String PROP_TABLE_STATUS_NUM = "tableStatusNum";
  

  public BaseShopTableStatus()
  {
    initialize();
  }
  


  public BaseShopTableStatus(Integer id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private Integer id;
  

  long version;
  

  protected Integer tableStatusNum;
  

  protected String ticketInformations;
  

  protected void initialize() {}
  

  public Integer getId()
  {
    return id;
  }
  



  public void setId(Integer id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Integer getTableStatusNum()
  {
    return tableStatusNum == null ? Integer.valueOf(0) : tableStatusNum;
  }
  



  public void setTableStatusNum(Integer tableStatusNum)
  {
    this.tableStatusNum = tableStatusNum;
  }
  




  public String getTicketInformations()
  {
    return ticketInformations;
  }
  



  public void setTicketInformations(String ticketInformations)
  {
    this.ticketInformations = ticketInformations;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof ShopTableStatus)) { return false;
    }
    ShopTableStatus shopTableStatus = (ShopTableStatus)obj;
    if ((null == getId()) || (null == shopTableStatus.getId())) return this == obj;
    return getId().equals(shopTableStatus.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
