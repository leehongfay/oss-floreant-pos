package com.floreantpos.model.base;

import com.floreantpos.model.ModifiableTicketItem;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemModifier;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;





public abstract class BaseModifiableTicketItem
  extends TicketItem
  implements Comparable, Serializable
{
  public static String REF = "ModifiableTicketItem";
  public static String PROP_SIZE_MODIFIER = "sizeModifier";
  public static String PROP_ID = "id";
  

  public BaseModifiableTicketItem()
  {
    initialize();
  }
  


  public BaseModifiableTicketItem(String id)
  {
    super(id);
  }
  


  private int hashCode = Integer.MIN_VALUE;
  



  private TicketItemModifier sizeModifier;
  



  private List<TicketItemModifier> ticketItemModifiers;
  




  public TicketItemModifier getSizeModifier()
  {
    return sizeModifier;
  }
  



  public void setSizeModifier(TicketItemModifier sizeModifier)
  {
    this.sizeModifier = sizeModifier;
  }
  




  public List<TicketItemModifier> getTicketItemModifiers()
  {
    return ticketItemModifiers;
  }
  



  public void setTicketItemModifiers(List<TicketItemModifier> ticketItemModifiers)
  {
    this.ticketItemModifiers = ticketItemModifiers;
  }
  
  public void addToticketItemModifiers(TicketItemModifier ticketItemModifier) {
    if (null == getTicketItemModifiers()) setTicketItemModifiers(new ArrayList());
    getTicketItemModifiers().add(ticketItemModifier);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof ModifiableTicketItem)) { return false;
    }
    ModifiableTicketItem modifiableTicketItem = (ModifiableTicketItem)obj;
    if ((null == getId()) || (null == modifiableTicketItem.getId())) return this == obj;
    return getId().equals(modifiableTicketItem.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
