package com.floreantpos.model.base;

import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.StockCount;
import com.floreantpos.model.StockCountItem;
import java.io.Serializable;








public abstract class BaseStockCountItem
  implements Comparable, Serializable
{
  public static String REF = "StockCountItem";
  public static String PROP_ADJUSTED = "adjusted";
  public static String PROP_NAME = "name";
  public static String PROP_UNIT_ON_HAND = "unitOnHand";
  public static String PROP_ITEM_ID = "itemId";
  public static String PROP_STOCK_COUNT = "stockCount";
  public static String PROP_ID = "id";
  public static String PROP_NOTE = "note";
  public static String PROP_ACTUAL_UNIT = "actualUnit";
  public static String PROP_UNIT = "unit";
  public static String PROP_SKU = "sku";
  public static String PROP_INVENTORY_LOCATION = "inventoryLocation";
  public static String PROP_REASON = "reason";
  

  public BaseStockCountItem()
  {
    initialize();
  }
  


  public BaseStockCountItem(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseStockCountItem(String id, StockCount stockCount)
  {
    setId(id);
    setStockCount(stockCount);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  private long version;
  
  protected String sku;
  
  protected String itemId;
  
  protected String name;
  
  protected Double unitOnHand;
  
  protected Double actualUnit;
  
  protected String unit;
  
  protected Boolean adjusted;
  
  protected String reason;
  
  protected String note;
  
  private InventoryLocation inventoryLocation;
  private StockCount stockCount;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getSku()
  {
    return sku;
  }
  



  public void setSku(String sku)
  {
    this.sku = sku;
  }
  




  public String getItemId()
  {
    return itemId;
  }
  



  public void setItemId(String itemId)
  {
    this.itemId = itemId;
  }
  




  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Double getUnitOnHand()
  {
    return unitOnHand == null ? Double.valueOf(0.0D) : unitOnHand;
  }
  



  public void setUnitOnHand(Double unitOnHand)
  {
    this.unitOnHand = unitOnHand;
  }
  




  public Double getActualUnit()
  {
    return actualUnit == null ? Double.valueOf(0.0D) : actualUnit;
  }
  



  public void setActualUnit(Double actualUnit)
  {
    this.actualUnit = actualUnit;
  }
  




  public String getUnit()
  {
    return unit;
  }
  



  public void setUnit(String unit)
  {
    this.unit = unit;
  }
  




  public Boolean isAdjusted()
  {
    return adjusted == null ? Boolean.FALSE : adjusted;
  }
  



  public void setAdjusted(Boolean adjusted)
  {
    this.adjusted = adjusted;
  }
  




  public String getReason()
  {
    return reason;
  }
  



  public void setReason(String reason)
  {
    this.reason = reason;
  }
  




  public String getNote()
  {
    return note;
  }
  



  public void setNote(String note)
  {
    this.note = note;
  }
  




  public InventoryLocation getInventoryLocation()
  {
    return inventoryLocation;
  }
  



  public void setInventoryLocation(InventoryLocation inventoryLocation)
  {
    this.inventoryLocation = inventoryLocation;
  }
  




  public StockCount getStockCount()
  {
    return stockCount;
  }
  



  public void setStockCount(StockCount stockCount)
  {
    this.stockCount = stockCount;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof StockCountItem)) { return false;
    }
    StockCountItem stockCountItem = (StockCountItem)obj;
    if ((null == getId()) || (null == stockCountItem.getId())) return this == obj;
    return getId().equals(stockCountItem.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
