package com.floreantpos.model.base;

import com.floreantpos.model.Recepie;
import java.io.Serializable;










public abstract class BaseSubRecipe
  implements Comparable, Serializable
{
  public static String REF = "SubRecipe";
  public static String PROP_RECIPE = "recipe";
  public static String PROP_PORTION = "portion";
  protected Double portion;
  private Recepie recipe;
  
  public BaseSubRecipe() {
    initialize();
  }
  








  protected void initialize() {}
  







  public Double getPortion()
  {
    return portion == null ? Double.valueOf(0.0D) : portion;
  }
  



  public void setPortion(Double portion)
  {
    this.portion = portion;
  }
  




  public Recepie getRecipe()
  {
    return recipe;
  }
  



  public void setRecipe(Recepie recipe)
  {
    this.recipe = recipe;
  }
  





  public int compareTo(Object obj)
  {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
