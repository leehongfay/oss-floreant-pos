package com.floreantpos.model.base;

import com.floreantpos.model.MenuItemSize;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.PizzaCrust;
import com.floreantpos.model.PizzaPrice;
import java.io.Serializable;







public abstract class BasePizzaPrice
  implements Comparable, Serializable
{
  public static String REF = "PizzaPrice";
  public static String PROP_ORDER_TYPE = "orderType";
  public static String PROP_PRICE = "price";
  public static String PROP_ID = "id";
  public static String PROP_CRUST = "crust";
  public static String PROP_SIZE = "size";
  

  public BasePizzaPrice()
  {
    initialize();
  }
  


  public BasePizzaPrice(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected Double price;
  

  private MenuItemSize size;
  
  private PizzaCrust crust;
  
  private OrderType orderType;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Double getPrice()
  {
    return price == null ? Double.valueOf(0.0D) : price;
  }
  



  public void setPrice(Double price)
  {
    this.price = price;
  }
  




  public MenuItemSize getSize()
  {
    return size;
  }
  



  public void setSize(MenuItemSize size)
  {
    this.size = size;
  }
  




  public PizzaCrust getCrust()
  {
    return crust;
  }
  



  public void setCrust(PizzaCrust crust)
  {
    this.crust = crust;
  }
  




  public OrderType getOrderType()
  {
    return orderType;
  }
  



  public void setOrderType(OrderType orderType)
  {
    this.orderType = orderType;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof PizzaPrice)) { return false;
    }
    PizzaPrice pizzaPrice = (PizzaPrice)obj;
    if ((null == getId()) || (null == pizzaPrice.getId())) return this == obj;
    return getId().equals(pizzaPrice.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
