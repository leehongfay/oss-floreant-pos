package com.floreantpos.model.base;

import com.floreantpos.model.ComboGroup;
import com.floreantpos.model.MenuItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;







public abstract class BaseComboGroup
  implements Comparable, Serializable
{
  public static String REF = "ComboGroup";
  public static String PROP_MIN_QUANTITY = "minQuantity";
  public static String PROP_NAME = "name";
  public static String PROP_ID = "id";
  public static String PROP_MAX_QUANTITY = "maxQuantity";
  

  public BaseComboGroup()
  {
    initialize();
  }
  


  public BaseComboGroup(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String name;
  

  protected Integer minQuantity;
  
  protected Integer maxQuantity;
  
  private List<MenuItem> items;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Integer getMinQuantity()
  {
    return minQuantity == null ? Integer.valueOf(0) : minQuantity;
  }
  



  public void setMinQuantity(Integer minQuantity)
  {
    this.minQuantity = minQuantity;
  }
  




  public Integer getMaxQuantity()
  {
    return maxQuantity == null ? Integer.valueOf(0) : maxQuantity;
  }
  



  public void setMaxQuantity(Integer maxQuantity)
  {
    this.maxQuantity = maxQuantity;
  }
  




  public List<MenuItem> getItems()
  {
    return items;
  }
  



  public void setItems(List<MenuItem> items)
  {
    this.items = items;
  }
  
  public void addToitems(MenuItem menuItem) {
    if (null == getItems()) setItems(new ArrayList());
    getItems().add(menuItem);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof ComboGroup)) { return false;
    }
    ComboGroup comboGroup = (ComboGroup)obj;
    if ((null == getId()) || (null == comboGroup.getId())) return this == obj;
    return getId().equals(comboGroup.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
