package com.floreantpos.model.base;

import com.floreantpos.model.ShopFloor;
import com.floreantpos.model.ShopTable;
import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;







public abstract class BaseShopFloor
  implements Comparable, Serializable
{
  public static String REF = "ShopFloor";
  public static String PROP_NAME = "name";
  public static String PROP_IMAGE_ID = "imageId";
  public static String PROP_FOREGROUND_COLOR_CODE = "foregroundColorCode";
  public static String PROP_SORT_ORDER = "sortOrder";
  public static String PROP_HEIGHT = "height";
  public static String PROP_OCCUPIED = "occupied";
  public static String PROP_ID = "id";
  public static String PROP_WIDTH = "width";
  public static String PROP_BACKGROUND_COLOR_CODE = "backgroundColorCode";
  

  public BaseShopFloor()
  {
    initialize();
  }
  


  public BaseShopFloor(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  private long version;
  
  protected String name;
  
  protected Boolean occupied;
  
  protected String imageId;
  
  protected Integer sortOrder;
  
  protected Integer width;
  
  protected Integer height;
  
  protected Integer foregroundColorCode;
  
  protected Integer backgroundColorCode;
  
  private Set<ShopTable> tables;
  

  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Boolean isOccupied()
  {
    return occupied == null ? Boolean.FALSE : occupied;
  }
  



  public void setOccupied(Boolean occupied)
  {
    this.occupied = occupied;
  }
  




  public String getImageId()
  {
    return imageId;
  }
  



  public void setImageId(String imageId)
  {
    this.imageId = imageId;
  }
  




  public Integer getSortOrder()
  {
    return sortOrder == null ? Integer.valueOf(0) : sortOrder;
  }
  



  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  




  public Integer getWidth()
  {
    return width == null ? Integer.valueOf(0) : width;
  }
  



  public void setWidth(Integer width)
  {
    this.width = width;
  }
  




  public Integer getHeight()
  {
    return height == null ? Integer.valueOf(0) : height;
  }
  



  public void setHeight(Integer height)
  {
    this.height = height;
  }
  




  public Integer getForegroundColorCode()
  {
    return foregroundColorCode == null ? null : foregroundColorCode;
  }
  



  public void setForegroundColorCode(Integer foregroundColorCode)
  {
    this.foregroundColorCode = foregroundColorCode;
  }
  



  public static String getForegroundColorCodeDefaultValue()
  {
    return "null";
  }
  



  public Integer getBackgroundColorCode()
  {
    return backgroundColorCode == null ? null : backgroundColorCode;
  }
  



  public void setBackgroundColorCode(Integer backgroundColorCode)
  {
    this.backgroundColorCode = backgroundColorCode;
  }
  



  public static String getBackgroundColorCodeDefaultValue()
  {
    return "null";
  }
  



  public Set<ShopTable> getTables()
  {
    return tables;
  }
  



  public void setTables(Set<ShopTable> tables)
  {
    this.tables = tables;
  }
  
  public void addTotables(ShopTable shopTable) {
    if (null == getTables()) setTables(new TreeSet());
    getTables().add(shopTable);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof ShopFloor)) { return false;
    }
    ShopFloor shopFloor = (ShopFloor)obj;
    if ((null == getId()) || (null == shopFloor.getId())) return this == obj;
    return getId().equals(shopFloor.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
