package com.floreantpos.model.base;

import com.floreantpos.model.Gratuity;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public abstract class BaseTicket
  implements Comparable, Serializable
{
  public static String REF = "Ticket";
  public static String PROP_VOIDED_BY_ID = "voidedById";
  public static String PROP_REVENUE_PURPOSE = "revenue_purpose";
  public static String PROP_ORDER_STATUS = "orderStatus";
  public static String PROP_RE_OPENED = "reOpened";
  public static String PROP_VOID_REASON = "voidReason";
  public static String PROP_DISCOUNTS_PROPERTY = "discountsProperty";
  public static String PROP_CASHIER_ID = "cashierId";
  public static String PROP_DISCOUNT_AMOUNT = "discountAmount";
  public static String PROP_CREATE_DATE = "createDate";
  public static String PROP_NUMBER_OF_GUESTS = "numberOfGuests";
  public static String PROP_DELIVERY_CHARGE = "deliveryCharge";
  public static String PROP_OWNER_TYPE_ID = "ownerTypeId";
  public static String PROP_ADVANCE_AMOUNT = "advanceAmount";
  public static String PROP_CUSTOMER_ID = "customerId";
  public static String PROP_SHIFT_ID = "shiftId";
  public static String PROP_DEPARTMENT_ID = "departmentId";
  public static String PROP_CUSTOMER_WILL_PICKUP = "customerWillPickup";
  public static String PROP_DELIVERY_DATE = "deliveryDate";
  public static String PROP_GRATUITY = "gratuity";
  public static String PROP_DELIVERY_ADDRESS = "deliveryAddress";
  public static String PROP_TAX_AMOUNT = "taxAmount";
  public static String PROP_SUBTOTAL_AMOUNT = "subtotalAmount";
  public static String PROP_TAX_EXEMPT = "taxExempt";
  public static String PROP_ID = "id";
  public static String PROP_TICKET_TABLE_NUMBERS = "ticketTableNumbers";
  public static String PROP_TOTAL_AMOUNT = "totalAmount";
  public static String PROP_VOIDED = "voided";
  public static String PROP_WASTED = "wasted";
  public static String PROP_EXTRA_DELIVERY_INFO = "extraDeliveryInfo";
  public static String PROP_SERVICE_CHARGE = "serviceCharge";
  public static String PROP_TYPE = "type";
  public static String PROP_OWNER_ID = "ownerId";
  public static String PROP_SHOULD_CALCULATE_PRICE = "shouldCalculatePrice";
  public static String PROP_DUE_AMOUNT = "dueAmount";
  public static String PROP_SALES_AREA_ID = "salesAreaId";
  public static String PROP_TERMINAL_ID = "terminalId";
  public static String PROP_PAID = "paid";
  public static String PROP_INVENTORY_LOCATION_ID = "inventoryLocationId";
  public static String PROP_VOID_AMOUNT = "voidAmount";
  public static String PROP_OUTLET_ID = "outletId";
  public static String PROP_ACTIVE_DATE = "activeDate";
  public static String PROP_SHOULD_INCLUDE_IN_SALES = "shouldIncludeInSales";
  public static String PROP_CREATION_HOUR = "creationHour";
  public static String PROP_REFUND_AMOUNT = "refundAmount";
  public static String PROP_ORDER_TYPE_ID = "orderTypeId";
  public static String PROP_CLOUD_SYNCED = "cloudSynced";
  public static String PROP_CLOSED = "closed";
  public static String PROP_TOKEN_NO = "tokenNo";
  public static String PROP_CLOSING_DATE = "closingDate";
  public static String PROP_NOTE = "note";
  public static String PROP_REFUNDABLE_AMOUNT = "refundableAmount";
  public static String PROP_REFUNDED = "refunded";
  public static String PROP_STATUS = "status";
  public static String PROP_TAX_INCLUDED = "taxIncluded";
  public static String PROP_ASSIGNED_DRIVER_ID = "assignedDriverId";
  public static String PROP_PAID_AMOUNT = "paidAmount";
  public static String PROP_SHORT_ID = "shortId";
  

  public BaseTicket()
  {
    initialize();
  }
  


  public BaseTicket(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  private String shortId;
  
  private Integer tokenNo;
  
  private Date createDate;
  
  private Date closingDate;
  
  private Date activeDate;
  
  private Date deliveryDate;
  
  private Integer creationHour;
  
  private Boolean paid;
  
  private Boolean voided;
  
  private String voidReason;
  
  private Boolean wasted;
  
  private Boolean refunded;
  private Boolean closed;
  private Boolean taxIncluded;
  private Double subtotalAmount;
  private Double discountAmount;
  private Double taxAmount;
  private Double totalAmount;
  private Double paidAmount;
  private Double voidAmount;
  private Double refundAmount;
  private Double refundableAmount;
  private Double dueAmount;
  private Double advanceAmount;
  private Integer numberOfGuests;
  private String status;
  private Integer orderStatus;
  private Integer type;
  private String outletId;
  private String departmentId;
  private String salesAreaId;
  private Boolean shouldCalculatePrice;
  private Boolean taxExempt;
  private Boolean reOpened;
  private Double serviceCharge;
  private Double deliveryCharge;
  private String customerId;
  private String deliveryAddress;
  private Boolean customerWillPickup;
  private String extraDeliveryInfo;
  private Boolean shouldIncludeInSales;
  private String revenue_purpose;
  private String orderTypeId;
  private String inventoryLocationId;
  private String shiftId;
  private String ownerId;
  private String ownerTypeId;
  private String cashierId;
  private String assignedDriverId;
  private String voidedById;
  private Integer terminalId;
  private Boolean cloudSynced;
  private String ticketTableNumbers;
  private String discountsProperty;
  private String note;
  private Gratuity gratuity;
  private Map<String, String> properties;
  private List<TicketItem> ticketItems;
  private Set<PosTransaction> transactions;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getShortId()
  {
    return shortId;
  }
  



  public void setShortId(String shortId)
  {
    this.shortId = shortId;
  }
  




  public Integer getTokenNo()
  {
    return tokenNo == null ? Integer.valueOf(0) : tokenNo;
  }
  



  public void setTokenNo(Integer tokenNo)
  {
    this.tokenNo = tokenNo;
  }
  




  public Date getCreateDate()
  {
    return createDate;
  }
  



  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  




  public Date getClosingDate()
  {
    return closingDate;
  }
  



  public void setClosingDate(Date closingDate)
  {
    this.closingDate = closingDate;
  }
  




  public Date getActiveDate()
  {
    return activeDate;
  }
  



  public void setActiveDate(Date activeDate)
  {
    this.activeDate = activeDate;
  }
  




  public Date getDeliveryDate()
  {
    return deliveryDate;
  }
  



  public void setDeliveryDate(Date deliveryDate)
  {
    this.deliveryDate = deliveryDate;
  }
  




  public Integer getCreationHour()
  {
    return creationHour == null ? Integer.valueOf(0) : creationHour;
  }
  



  public void setCreationHour(Integer creationHour)
  {
    this.creationHour = creationHour;
  }
  




  public Boolean isPaid()
  {
    return paid == null ? Boolean.FALSE : paid;
  }
  



  public void setPaid(Boolean paid)
  {
    this.paid = paid;
  }
  




  public Boolean isVoided()
  {
    return voided == null ? Boolean.FALSE : voided;
  }
  



  public void setVoided(Boolean voided)
  {
    this.voided = voided;
  }
  




  public String getVoidReason()
  {
    return voidReason;
  }
  



  public void setVoidReason(String voidReason)
  {
    this.voidReason = voidReason;
  }
  




  public Boolean isWasted()
  {
    return wasted == null ? Boolean.FALSE : wasted;
  }
  



  public void setWasted(Boolean wasted)
  {
    this.wasted = wasted;
  }
  




  public Boolean isRefunded()
  {
    return refunded == null ? Boolean.FALSE : refunded;
  }
  



  public void setRefunded(Boolean refunded)
  {
    this.refunded = refunded;
  }
  




  public Boolean isClosed()
  {
    return closed == null ? Boolean.FALSE : closed;
  }
  



  public void setClosed(Boolean closed)
  {
    this.closed = closed;
  }
  




  public Boolean isTaxIncluded()
  {
    return taxIncluded == null ? Boolean.FALSE : taxIncluded;
  }
  



  public void setTaxIncluded(Boolean taxIncluded)
  {
    this.taxIncluded = taxIncluded;
  }
  




  public Double getSubtotalAmount()
  {
    return subtotalAmount == null ? Double.valueOf(0.0D) : subtotalAmount;
  }
  



  public void setSubtotalAmount(Double subtotalAmount)
  {
    this.subtotalAmount = subtotalAmount;
  }
  




  public Double getDiscountAmount()
  {
    return discountAmount == null ? Double.valueOf(0.0D) : discountAmount;
  }
  



  public void setDiscountAmount(Double discountAmount)
  {
    this.discountAmount = discountAmount;
  }
  




  public Double getTaxAmount()
  {
    return taxAmount == null ? Double.valueOf(0.0D) : taxAmount;
  }
  



  public void setTaxAmount(Double taxAmount)
  {
    this.taxAmount = taxAmount;
  }
  




  public Double getTotalAmount()
  {
    return totalAmount == null ? Double.valueOf(0.0D) : totalAmount;
  }
  



  public void setTotalAmount(Double totalAmount)
  {
    this.totalAmount = totalAmount;
  }
  




  public Double getPaidAmount()
  {
    return paidAmount == null ? Double.valueOf(0.0D) : paidAmount;
  }
  



  public void setPaidAmount(Double paidAmount)
  {
    this.paidAmount = paidAmount;
  }
  




  public Double getVoidAmount()
  {
    return voidAmount == null ? Double.valueOf(0.0D) : voidAmount;
  }
  



  public void setVoidAmount(Double voidAmount)
  {
    this.voidAmount = voidAmount;
  }
  




  public Double getRefundAmount()
  {
    return refundAmount == null ? Double.valueOf(0.0D) : refundAmount;
  }
  



  public void setRefundAmount(Double refundAmount)
  {
    this.refundAmount = refundAmount;
  }
  




  public Double getRefundableAmount()
  {
    return refundableAmount == null ? Double.valueOf(0.0D) : refundableAmount;
  }
  



  public void setRefundableAmount(Double refundableAmount)
  {
    this.refundableAmount = refundableAmount;
  }
  




  public Double getDueAmount()
  {
    return dueAmount == null ? Double.valueOf(0.0D) : dueAmount;
  }
  



  public void setDueAmount(Double dueAmount)
  {
    this.dueAmount = dueAmount;
  }
  




  public Double getAdvanceAmount()
  {
    return advanceAmount == null ? Double.valueOf(0.0D) : advanceAmount;
  }
  



  public void setAdvanceAmount(Double advanceAmount)
  {
    this.advanceAmount = advanceAmount;
  }
  




  public Integer getNumberOfGuests()
  {
    return numberOfGuests == null ? Integer.valueOf(0) : numberOfGuests;
  }
  



  public void setNumberOfGuests(Integer numberOfGuests)
  {
    this.numberOfGuests = numberOfGuests;
  }
  




  public String getStatus()
  {
    return status;
  }
  



  public void setStatus(String status)
  {
    this.status = status;
  }
  




  public Integer getOrderStatus()
  {
    return orderStatus == null ? Integer.valueOf(0) : orderStatus;
  }
  



  public void setOrderStatus(Integer orderStatus)
  {
    this.orderStatus = orderStatus;
  }
  




  public Integer getType()
  {
    return type == null ? Integer.valueOf(0) : type;
  }
  



  public void setType(Integer type)
  {
    this.type = type;
  }
  




  public String getOutletId()
  {
    return outletId;
  }
  



  public void setOutletId(String outletId)
  {
    this.outletId = outletId;
  }
  




  public String getDepartmentId()
  {
    return departmentId;
  }
  



  public void setDepartmentId(String departmentId)
  {
    this.departmentId = departmentId;
  }
  




  public String getSalesAreaId()
  {
    return salesAreaId;
  }
  



  public void setSalesAreaId(String salesAreaId)
  {
    this.salesAreaId = salesAreaId;
  }
  




  public Boolean isShouldCalculatePrice()
  {
    return shouldCalculatePrice == null ? Boolean.valueOf(true) : shouldCalculatePrice;
  }
  



  public void setShouldCalculatePrice(Boolean shouldCalculatePrice)
  {
    this.shouldCalculatePrice = shouldCalculatePrice;
  }
  



  public static String getShouldCalculatePriceDefaultValue()
  {
    return "true";
  }
  



  public Boolean isTaxExempt()
  {
    return taxExempt == null ? Boolean.FALSE : taxExempt;
  }
  



  public void setTaxExempt(Boolean taxExempt)
  {
    this.taxExempt = taxExempt;
  }
  




  public Boolean isReOpened()
  {
    return reOpened == null ? Boolean.FALSE : reOpened;
  }
  



  public void setReOpened(Boolean reOpened)
  {
    this.reOpened = reOpened;
  }
  




  public Double getServiceCharge()
  {
    return serviceCharge == null ? Double.valueOf(0.0D) : serviceCharge;
  }
  



  public void setServiceCharge(Double serviceCharge)
  {
    this.serviceCharge = serviceCharge;
  }
  




  public Double getDeliveryCharge()
  {
    return deliveryCharge == null ? Double.valueOf(0.0D) : deliveryCharge;
  }
  



  public void setDeliveryCharge(Double deliveryCharge)
  {
    this.deliveryCharge = deliveryCharge;
  }
  




  public String getCustomerId()
  {
    return customerId;
  }
  



  public void setCustomerId(String customerId)
  {
    this.customerId = customerId;
  }
  




  public String getDeliveryAddress()
  {
    return deliveryAddress;
  }
  



  public void setDeliveryAddress(String deliveryAddress)
  {
    this.deliveryAddress = deliveryAddress;
  }
  




  public Boolean isCustomerWillPickup()
  {
    return customerWillPickup == null ? Boolean.FALSE : customerWillPickup;
  }
  



  public void setCustomerWillPickup(Boolean customerWillPickup)
  {
    this.customerWillPickup = customerWillPickup;
  }
  




  public String getExtraDeliveryInfo()
  {
    return extraDeliveryInfo;
  }
  



  public void setExtraDeliveryInfo(String extraDeliveryInfo)
  {
    this.extraDeliveryInfo = extraDeliveryInfo;
  }
  




  public Boolean isShouldIncludeInSales()
  {
    return shouldIncludeInSales == null ? Boolean.valueOf(true) : shouldIncludeInSales;
  }
  



  public void setShouldIncludeInSales(Boolean shouldIncludeInSales)
  {
    this.shouldIncludeInSales = shouldIncludeInSales;
  }
  



  public static String getShouldIncludeInSalesDefaultValue()
  {
    return "true";
  }
  



  public String getRevenue_purpose()
  {
    return revenue_purpose;
  }
  



  public void setRevenue_purpose(String revenue_purpose)
  {
    this.revenue_purpose = revenue_purpose;
  }
  




  public String getOrderTypeId()
  {
    return orderTypeId;
  }
  



  public void setOrderTypeId(String orderTypeId)
  {
    this.orderTypeId = orderTypeId;
  }
  




  public String getInventoryLocationId()
  {
    return inventoryLocationId;
  }
  



  public void setInventoryLocationId(String inventoryLocationId)
  {
    this.inventoryLocationId = inventoryLocationId;
  }
  




  public String getShiftId()
  {
    return shiftId;
  }
  



  public void setShiftId(String shiftId)
  {
    this.shiftId = shiftId;
  }
  




  public String getOwnerId()
  {
    return ownerId;
  }
  



  public void setOwnerId(String ownerId)
  {
    this.ownerId = ownerId;
  }
  




  public String getOwnerTypeId()
  {
    return ownerTypeId;
  }
  



  public void setOwnerTypeId(String ownerTypeId)
  {
    this.ownerTypeId = ownerTypeId;
  }
  




  public String getCashierId()
  {
    return cashierId;
  }
  



  public void setCashierId(String cashierId)
  {
    this.cashierId = cashierId;
  }
  




  public String getAssignedDriverId()
  {
    return assignedDriverId;
  }
  



  public void setAssignedDriverId(String assignedDriverId)
  {
    this.assignedDriverId = assignedDriverId;
  }
  




  public String getVoidedById()
  {
    return voidedById;
  }
  



  public void setVoidedById(String voidedById)
  {
    this.voidedById = voidedById;
  }
  




  public Integer getTerminalId()
  {
    return terminalId == null ? Integer.valueOf(0) : terminalId;
  }
  



  public void setTerminalId(Integer terminalId)
  {
    this.terminalId = terminalId;
  }
  




  public Boolean isCloudSynced()
  {
    return cloudSynced == null ? Boolean.FALSE : cloudSynced;
  }
  



  public void setCloudSynced(Boolean cloudSynced)
  {
    this.cloudSynced = cloudSynced;
  }
  




  public String getTicketTableNumbers()
  {
    return ticketTableNumbers;
  }
  



  public void setTicketTableNumbers(String ticketTableNumbers)
  {
    this.ticketTableNumbers = ticketTableNumbers;
  }
  




  public String getDiscountsProperty()
  {
    return discountsProperty;
  }
  



  public void setDiscountsProperty(String discountsProperty)
  {
    this.discountsProperty = discountsProperty;
  }
  




  public String getNote()
  {
    return note;
  }
  



  public void setNote(String note)
  {
    this.note = note;
  }
  




  public Gratuity getGratuity()
  {
    return gratuity;
  }
  



  public void setGratuity(Gratuity gratuity)
  {
    this.gratuity = gratuity;
  }
  




  public Map<String, String> getProperties()
  {
    return properties;
  }
  



  public void setProperties(Map<String, String> properties)
  {
    this.properties = properties;
  }
  




  public List<TicketItem> getTicketItems()
  {
    return ticketItems;
  }
  



  public void setTicketItems(List<TicketItem> ticketItems)
  {
    this.ticketItems = ticketItems;
  }
  
  public void addToticketItems(TicketItem ticketItem) {
    if (null == getTicketItems()) setTicketItems(new ArrayList());
    getTicketItems().add(ticketItem);
  }
  




  public Set<PosTransaction> getTransactions()
  {
    return transactions;
  }
  



  public void setTransactions(Set<PosTransaction> transactions)
  {
    this.transactions = transactions;
  }
  
  public void addTotransactions(PosTransaction posTransaction) {
    if (null == getTransactions()) setTransactions(new TreeSet());
    getTransactions().add(posTransaction);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof Ticket)) { return false;
    }
    Ticket ticket = (Ticket)obj;
    if ((null == getId()) || (null == ticket.getId())) return this == obj;
    return getId().equals(ticket.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
