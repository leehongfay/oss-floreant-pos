package com.floreantpos.model.base;

import com.floreantpos.model.MiscShift;
import com.floreantpos.model.Shift;
import java.io.Serializable;








public abstract class BaseMiscShift
  extends Shift
  implements Comparable, Serializable
{
  public static String REF = "MiscShift";
  public static String PROP_ID = "id";
  

  public BaseMiscShift()
  {
    initialize();
  }
  


  public BaseMiscShift(String id)
  {
    super(id);
  }
  





  public BaseMiscShift(String id, String name)
  {
    super(id, name);
  }
  




  private int hashCode = Integer.MIN_VALUE;
  







  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof MiscShift)) { return false;
    }
    MiscShift miscShift = (MiscShift)obj;
    if ((null == getId()) || (null == miscShift.getId())) return this == obj;
    return getId().equals(miscShift.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
