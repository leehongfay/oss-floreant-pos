package com.floreantpos.model.base;

import com.floreantpos.model.BlobXmlAdapter;
import com.floreantpos.model.ImageResource;
import java.io.Serializable;
import java.sql.Blob;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;







public abstract class BaseImageResource
  implements Comparable, Serializable
{
  public static String REF = "ImageResource";
  public static String PROP_DESCRIPTION = "description";
  public static String PROP_IMAGE_DATA = "imageData";
  public static String PROP_ID = "id";
  public static String PROP_IMAGE_CATEGORY_NUM = "imageCategoryNum";
  

  public BaseImageResource()
  {
    initialize();
  }
  


  public BaseImageResource(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String description;
  
  protected Blob imageData;
  
  protected Integer imageCategoryNum;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getDescription()
  {
    return description;
  }
  



  public void setDescription(String description)
  {
    this.description = description;
  }
  




  @XmlJavaTypeAdapter(BlobXmlAdapter.class)
  public Blob getImageData()
  {
    return imageData;
  }
  



  public void setImageData(Blob imageData)
  {
    this.imageData = imageData;
  }
  



  public static String getImageDataXmlTransientAnnotation()
  {
    return "@javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter(com.floreantpos.model.BlobXmlAdapter.class)";
  }
  



  public Integer getImageCategoryNum()
  {
    return imageCategoryNum == null ? Integer.valueOf(0) : imageCategoryNum;
  }
  



  public void setImageCategoryNum(Integer imageCategoryNum)
  {
    this.imageCategoryNum = imageCategoryNum;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof ImageResource)) { return false;
    }
    ImageResource imageResource = (ImageResource)obj;
    if ((null == getId()) || (null == imageResource.getId())) return this == obj;
    return getId().equals(imageResource.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
