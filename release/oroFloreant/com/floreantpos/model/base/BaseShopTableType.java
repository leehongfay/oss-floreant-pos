package com.floreantpos.model.base;

import com.floreantpos.model.ShopTableType;
import java.io.Serializable;










public abstract class BaseShopTableType
  implements Comparable, Serializable
{
  public static String REF = "ShopTableType";
  public static String PROP_NAME = "name";
  public static String PROP_DESCRIPTION = "description";
  public static String PROP_ID = "id";
  

  public BaseShopTableType()
  {
    initialize();
  }
  


  public BaseShopTableType(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String description;
  

  protected String name;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getDescription()
  {
    return description;
  }
  



  public void setDescription(String description)
  {
    this.description = description;
  }
  




  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof ShopTableType)) { return false;
    }
    ShopTableType shopTableType = (ShopTableType)obj;
    if ((null == getId()) || (null == shopTableType.getId())) return this == obj;
    return getId().equals(shopTableType.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
