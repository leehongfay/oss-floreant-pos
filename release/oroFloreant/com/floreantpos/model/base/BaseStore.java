package com.floreantpos.model.base;

import com.floreantpos.model.Address;
import com.floreantpos.model.Store;
import java.io.Serializable;
import java.util.Map;








public abstract class BaseStore
  implements Comparable, Serializable
{
  public static String REF = "Store";
  public static String PROP_UNIQUE_ID = "uniqueId";
  public static String PROP_OUTLET_NAME = "outletName";
  public static String PROP_TELEPHONE = "telephone";
  public static String PROP_ADDRESS = "address";
  public static String PROP_ZIP_CODE = "zipCode";
  public static String PROP_SERVICE_CHARGE_PERCENTAGE = "serviceChargePercentage";
  public static String PROP_ALLOW_MODIFIER_MAX_EXCEED = "allowModifierMaxExceed";
  public static String PROP_TICKET_FOOTER_MESSAGE = "ticketFooterMessage";
  public static String PROP_CURRENCY_SYMBOL = "currencySymbol";
  public static String PROP_NAME = "name";
  public static String PROP_CURRENCY_NAME = "currencyName";
  public static String PROP_ADDRESS_LINE3 = "addressLine3";
  public static String PROP_ADDRESS_LINE2 = "addressLine2";
  public static String PROP_ADDRESS_LINE1 = "addressLine1";
  public static String PROP_DEFAULT_GRATUITY_PERCENTAGE = "defaultGratuityPercentage";
  public static String PROP_ITEM_PRICE_INCLUDES_TAX = "itemPriceIncludesTax";
  public static String PROP_CAPACITY = "capacity";
  public static String PROP_TABLES = "tables";
  public static String PROP_ID = "id";
  public static String PROP_USE_DETAILED_RECONCILIATION = "useDetailedReconciliation";
  

  public BaseStore()
  {
    initialize();
  }
  


  public BaseStore(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected Integer uniqueId;
  
  protected String name;
  
  protected String outletName;
  
  protected String addressLine1;
  
  protected String addressLine2;
  
  protected String addressLine3;
  
  protected String zipCode;
  
  protected String telephone;
  
  protected Integer capacity;
  
  protected Integer tables;
  
  protected String currencyName;
  
  protected String currencySymbol;
  protected Double serviceChargePercentage;
  protected Double defaultGratuityPercentage;
  protected String ticketFooterMessage;
  protected Boolean itemPriceIncludesTax;
  protected Boolean allowModifierMaxExceed;
  protected Boolean useDetailedReconciliation;
  private Address address;
  private Map<String, String> properties;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Integer getUniqueId()
  {
    return uniqueId == null ? Integer.valueOf(0) : uniqueId;
  }
  



  public void setUniqueId(Integer uniqueId)
  {
    this.uniqueId = uniqueId;
  }
  




  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getOutletName()
  {
    return outletName;
  }
  



  public void setOutletName(String outletName)
  {
    this.outletName = outletName;
  }
  




  public String getAddressLine1()
  {
    return addressLine1;
  }
  



  public void setAddressLine1(String addressLine1)
  {
    this.addressLine1 = addressLine1;
  }
  




  public String getAddressLine2()
  {
    return addressLine2;
  }
  



  public void setAddressLine2(String addressLine2)
  {
    this.addressLine2 = addressLine2;
  }
  




  public String getAddressLine3()
  {
    return addressLine3;
  }
  



  public void setAddressLine3(String addressLine3)
  {
    this.addressLine3 = addressLine3;
  }
  




  public String getZipCode()
  {
    return zipCode;
  }
  



  public void setZipCode(String zipCode)
  {
    this.zipCode = zipCode;
  }
  




  public String getTelephone()
  {
    return telephone;
  }
  



  public void setTelephone(String telephone)
  {
    this.telephone = telephone;
  }
  




  public Integer getCapacity()
  {
    return capacity == null ? Integer.valueOf(0) : capacity;
  }
  



  public void setCapacity(Integer capacity)
  {
    this.capacity = capacity;
  }
  




  public Integer getTables()
  {
    return tables == null ? Integer.valueOf(0) : tables;
  }
  



  public void setTables(Integer tables)
  {
    this.tables = tables;
  }
  




  public String getCurrencyName()
  {
    return currencyName;
  }
  



  public void setCurrencyName(String currencyName)
  {
    this.currencyName = currencyName;
  }
  




  public String getCurrencySymbol()
  {
    return currencySymbol;
  }
  



  public void setCurrencySymbol(String currencySymbol)
  {
    this.currencySymbol = currencySymbol;
  }
  




  public Double getServiceChargePercentage()
  {
    return serviceChargePercentage == null ? Double.valueOf(0.0D) : serviceChargePercentage;
  }
  



  public void setServiceChargePercentage(Double serviceChargePercentage)
  {
    this.serviceChargePercentage = serviceChargePercentage;
  }
  




  public Double getDefaultGratuityPercentage()
  {
    return defaultGratuityPercentage == null ? Double.valueOf(0.0D) : defaultGratuityPercentage;
  }
  



  public void setDefaultGratuityPercentage(Double defaultGratuityPercentage)
  {
    this.defaultGratuityPercentage = defaultGratuityPercentage;
  }
  




  public String getTicketFooterMessage()
  {
    return ticketFooterMessage;
  }
  



  public void setTicketFooterMessage(String ticketFooterMessage)
  {
    this.ticketFooterMessage = ticketFooterMessage;
  }
  




  public Boolean isItemPriceIncludesTax()
  {
    return itemPriceIncludesTax == null ? Boolean.FALSE : itemPriceIncludesTax;
  }
  



  public void setItemPriceIncludesTax(Boolean itemPriceIncludesTax)
  {
    this.itemPriceIncludesTax = itemPriceIncludesTax;
  }
  




  public Boolean isAllowModifierMaxExceed()
  {
    return allowModifierMaxExceed == null ? Boolean.FALSE : allowModifierMaxExceed;
  }
  



  public void setAllowModifierMaxExceed(Boolean allowModifierMaxExceed)
  {
    this.allowModifierMaxExceed = allowModifierMaxExceed;
  }
  




  public Boolean isUseDetailedReconciliation()
  {
    return useDetailedReconciliation == null ? Boolean.FALSE : useDetailedReconciliation;
  }
  



  public void setUseDetailedReconciliation(Boolean useDetailedReconciliation)
  {
    this.useDetailedReconciliation = useDetailedReconciliation;
  }
  




  public Address getAddress()
  {
    return address;
  }
  



  public void setAddress(Address address)
  {
    this.address = address;
  }
  




  public Map<String, String> getProperties()
  {
    return properties;
  }
  



  public void setProperties(Map<String, String> properties)
  {
    this.properties = properties;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof Store)) { return false;
    }
    Store store = (Store)obj;
    if ((null == getId()) || (null == store.getId())) return this == obj;
    return getId().equals(store.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
