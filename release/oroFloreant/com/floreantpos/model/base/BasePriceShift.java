package com.floreantpos.model.base;

import com.floreantpos.model.PriceShift;
import com.floreantpos.model.Shift;
import java.io.Serializable;








public abstract class BasePriceShift
  extends Shift
  implements Comparable, Serializable
{
  public static String REF = "PriceShift";
  public static String PROP_ID = "id";
  

  public BasePriceShift()
  {
    initialize();
  }
  


  public BasePriceShift(String id)
  {
    super(id);
  }
  





  public BasePriceShift(String id, String name)
  {
    super(id, name);
  }
  




  private int hashCode = Integer.MIN_VALUE;
  







  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof PriceShift)) { return false;
    }
    PriceShift priceShift = (PriceShift)obj;
    if ((null == getId()) || (null == priceShift.getId())) return this == obj;
    return getId().equals(priceShift.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
