package com.floreantpos.model.base;

import com.floreantpos.model.MenuPageItem;
import java.io.Serializable;










public abstract class BaseMenuPageItem
  implements Comparable, Serializable
{
  public static String REF = "MenuPageItem";
  public static String PROP_MENU_PAGE_ID = "menuPageId";
  public static String PROP_IMAGE_ID = "imageId";
  public static String PROP_TEXT_COLOR_CODE = "textColorCode";
  public static String PROP_SHOW_IMAGE_ONLY = "showImageOnly";
  public static String PROP_COL = "col";
  public static String PROP_SORT_ORDER = "sortOrder";
  public static String PROP_ROW = "row";
  public static String PROP_BUTTON_COLOR_CODE = "buttonColorCode";
  public static String PROP_HEIGHT = "height";
  public static String PROP_ID = "id";
  public static String PROP_MENU_ITEM_NAME = "menuItemName";
  public static String PROP_WIDTH = "width";
  public static String PROP_MENU_ITEM_ID = "menuItemId";
  

  public BaseMenuPageItem()
  {
    initialize();
  }
  


  public BaseMenuPageItem(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected Integer col;
  
  protected Integer row;
  
  protected Integer width;
  
  protected Integer height;
  
  protected String menuItemName;
  
  protected Integer sortOrder;
  
  protected Integer buttonColorCode;
  
  protected Integer textColorCode;
  protected String imageId;
  protected Boolean showImageOnly;
  protected String menuPageId;
  protected String menuItemId;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Integer getCol()
  {
    return col == null ? Integer.valueOf(0) : col;
  }
  



  public void setCol(Integer col)
  {
    this.col = col;
  }
  




  public Integer getRow()
  {
    return row == null ? Integer.valueOf(0) : row;
  }
  



  public void setRow(Integer row)
  {
    this.row = row;
  }
  




  public Integer getWidth()
  {
    return width == null ? Integer.valueOf(0) : width;
  }
  



  public void setWidth(Integer width)
  {
    this.width = width;
  }
  




  public Integer getHeight()
  {
    return height == null ? Integer.valueOf(0) : height;
  }
  



  public void setHeight(Integer height)
  {
    this.height = height;
  }
  




  public String getMenuItemName()
  {
    return menuItemName;
  }
  



  public void setMenuItemName(String menuItemName)
  {
    this.menuItemName = menuItemName;
  }
  




  public Integer getSortOrder()
  {
    return sortOrder == null ? Integer.valueOf(0) : sortOrder;
  }
  



  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  




  public Integer getButtonColorCode()
  {
    return buttonColorCode == null ? null : buttonColorCode;
  }
  



  public void setButtonColorCode(Integer buttonColorCode)
  {
    this.buttonColorCode = buttonColorCode;
  }
  



  public static String getButtonColorCodeDefaultValue()
  {
    return "null";
  }
  



  public Integer getTextColorCode()
  {
    return textColorCode == null ? null : textColorCode;
  }
  



  public void setTextColorCode(Integer textColorCode)
  {
    this.textColorCode = textColorCode;
  }
  



  public static String getTextColorCodeDefaultValue()
  {
    return "null";
  }
  



  public String getImageId()
  {
    return imageId;
  }
  



  public void setImageId(String imageId)
  {
    this.imageId = imageId;
  }
  




  public Boolean isShowImageOnly()
  {
    return showImageOnly == null ? Boolean.FALSE : showImageOnly;
  }
  



  public void setShowImageOnly(Boolean showImageOnly)
  {
    this.showImageOnly = showImageOnly;
  }
  




  public String getMenuPageId()
  {
    return menuPageId;
  }
  



  public void setMenuPageId(String menuPageId)
  {
    this.menuPageId = menuPageId;
  }
  




  public String getMenuItemId()
  {
    return menuItemId;
  }
  



  public void setMenuItemId(String menuItemId)
  {
    this.menuItemId = menuItemId;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof MenuPageItem)) { return false;
    }
    MenuPageItem menuPageItem = (MenuPageItem)obj;
    if ((null == getId()) || (null == menuPageItem.getId())) return this == obj;
    return getId().equals(menuPageItem.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
