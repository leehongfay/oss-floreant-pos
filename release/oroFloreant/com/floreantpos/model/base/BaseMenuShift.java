package com.floreantpos.model.base;

import com.floreantpos.model.MenuShift;
import com.floreantpos.model.Shift;
import java.io.Serializable;








public abstract class BaseMenuShift
  extends Shift
  implements Comparable, Serializable
{
  public static String REF = "MenuShift";
  public static String PROP_ID = "id";
  

  public BaseMenuShift()
  {
    initialize();
  }
  


  public BaseMenuShift(String id)
  {
    super(id);
  }
  





  public BaseMenuShift(String id, String name)
  {
    super(id, name);
  }
  




  private int hashCode = Integer.MIN_VALUE;
  







  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof MenuShift)) { return false;
    }
    MenuShift menuShift = (MenuShift)obj;
    if ((null == getId()) || (null == menuShift.getId())) return this == obj;
    return getId().equals(menuShift.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
