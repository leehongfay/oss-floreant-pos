package com.floreantpos.model.base;

import com.floreantpos.model.Phone;
import java.io.Serializable;










public abstract class BasePhone
  implements Comparable, Serializable
{
  public static String REF = "Phone";
  public static String PROP_NAME = "name";
  public static String PROP_EXTENSION = "extension";
  public static String PROP_NUMBER = "number";
  public static String PROP_ID = "id";
  public static String PROP_COUNTRY_CODE = "countryCode";
  

  public BasePhone()
  {
    initialize();
  }
  


  public BasePhone(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  
  protected String name;
  
  protected String countryCode;
  
  protected String number;
  
  protected String extension;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getCountryCode()
  {
    return countryCode;
  }
  



  public void setCountryCode(String countryCode)
  {
    this.countryCode = countryCode;
  }
  




  public String getNumber()
  {
    return number;
  }
  



  public void setNumber(String number)
  {
    this.number = number;
  }
  




  public String getExtension()
  {
    return extension;
  }
  



  public void setExtension(String extension)
  {
    this.extension = extension;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof Phone)) { return false;
    }
    Phone phone = (Phone)obj;
    if ((null == getId()) || (null == phone.getId())) return this == obj;
    return getId().equals(phone.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
