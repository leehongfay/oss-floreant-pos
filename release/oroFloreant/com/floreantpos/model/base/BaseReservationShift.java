package com.floreantpos.model.base;

import com.floreantpos.model.ReservationShift;
import com.floreantpos.model.Shift;
import java.io.Serializable;








public abstract class BaseReservationShift
  extends Shift
  implements Comparable, Serializable
{
  public static String REF = "ReservationShift";
  public static String PROP_ID = "id";
  

  public BaseReservationShift()
  {
    initialize();
  }
  


  public BaseReservationShift(String id)
  {
    super(id);
  }
  





  public BaseReservationShift(String id, String name)
  {
    super(id, name);
  }
  




  private int hashCode = Integer.MIN_VALUE;
  







  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof ReservationShift)) { return false;
    }
    ReservationShift reservationShift = (ReservationShift)obj;
    if ((null == getId()) || (null == reservationShift.getId())) return this == obj;
    return getId().equals(reservationShift.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
