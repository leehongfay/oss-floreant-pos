package com.floreantpos.model.base;

import com.floreantpos.model.MenuItem;
import com.floreantpos.model.Recepie;
import com.floreantpos.model.RecepieItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;





public abstract class BaseRecepie
  implements Comparable, Serializable
{
  public static String REF = "Recepie";
  public static String PROP_YIELD_UNIT = "yieldUnit";
  public static String PROP_DESCRIPTION = "description";
  public static String PROP_BATCH_PROCESS = "batchProcess";
  public static String PROP_MENU_ITEM = "menuItem";
  public static String PROP_PORTION_UNIT = "portionUnit";
  public static String PROP_LABOR_COST = "laborCost";
  public static String PROP_CODE = "code";
  public static String PROP_NAME = "name";
  public static String PROP_COOKING_TIME = "cookingTime";
  public static String PROP_ADJUSTMENT_AMOUNT = "adjustmentAmount";
  public static String PROP_YIELD = "yield";
  public static String PROP_ID = "id";
  public static String PROP_PORTION = "portion";
  

  public BaseRecepie()
  {
    initialize();
  }
  


  public BaseRecepie(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected String code;
  
  protected String name;
  
  protected String description;
  
  protected Double yield;
  
  protected String yieldUnit;
  
  protected Double portion;
  
  protected String portionUnit;
  
  protected Double adjustmentAmount;
  
  protected Double laborCost;
  
  protected Boolean batchProcess;
  
  protected Integer cookingTime;
  
  private MenuItem menuItem;
  private List<RecepieItem> recepieItems;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getCode()
  {
    return code;
  }
  



  public void setCode(String code)
  {
    this.code = code;
  }
  




  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getDescription()
  {
    return description;
  }
  



  public void setDescription(String description)
  {
    this.description = description;
  }
  




  public Double getYield()
  {
    return yield == null ? Double.valueOf(0.0D) : yield;
  }
  



  public void setYield(Double yield)
  {
    this.yield = yield;
  }
  




  public String getYieldUnit()
  {
    return yieldUnit;
  }
  



  public void setYieldUnit(String yieldUnit)
  {
    this.yieldUnit = yieldUnit;
  }
  




  public Double getPortion()
  {
    return portion == null ? Double.valueOf(0.0D) : portion;
  }
  



  public void setPortion(Double portion)
  {
    this.portion = portion;
  }
  




  public String getPortionUnit()
  {
    return portionUnit;
  }
  



  public void setPortionUnit(String portionUnit)
  {
    this.portionUnit = portionUnit;
  }
  




  public Double getAdjustmentAmount()
  {
    return adjustmentAmount == null ? Double.valueOf(0.0D) : adjustmentAmount;
  }
  



  public void setAdjustmentAmount(Double adjustmentAmount)
  {
    this.adjustmentAmount = adjustmentAmount;
  }
  




  public Double getLaborCost()
  {
    return laborCost == null ? Double.valueOf(0.0D) : laborCost;
  }
  



  public void setLaborCost(Double laborCost)
  {
    this.laborCost = laborCost;
  }
  




  public Boolean isBatchProcess()
  {
    return batchProcess == null ? Boolean.FALSE : batchProcess;
  }
  



  public void setBatchProcess(Boolean batchProcess)
  {
    this.batchProcess = batchProcess;
  }
  




  public Integer getCookingTime()
  {
    return cookingTime == null ? Integer.valueOf(0) : cookingTime;
  }
  



  public void setCookingTime(Integer cookingTime)
  {
    this.cookingTime = cookingTime;
  }
  




  public MenuItem getMenuItem()
  {
    return menuItem;
  }
  



  public void setMenuItem(MenuItem menuItem)
  {
    this.menuItem = menuItem;
  }
  




  public List<RecepieItem> getRecepieItems()
  {
    return recepieItems;
  }
  



  public void setRecepieItems(List<RecepieItem> recepieItems)
  {
    this.recepieItems = recepieItems;
  }
  
  public void addTorecepieItems(RecepieItem recepieItem) {
    if (null == getRecepieItems()) setRecepieItems(new ArrayList());
    getRecepieItems().add(recepieItem);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof Recepie)) { return false;
    }
    Recepie recepie = (Recepie)obj;
    if ((null == getId()) || (null == recepie.getId())) return this == obj;
    return getId().equals(recepie.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
