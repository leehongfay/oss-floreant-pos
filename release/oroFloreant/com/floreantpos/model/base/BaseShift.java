package com.floreantpos.model.base;

import com.floreantpos.model.Shift;
import java.io.Serializable;
import java.util.Date;









public abstract class BaseShift
  implements Comparable, Serializable
{
  public static String REF = "Shift";
  public static String PROP_DAYS_OF_WEEK = "daysOfWeek";
  public static String PROP_DESCRIPTION = "description";
  public static String PROP_SHIFT_LENGTH = "shiftLength";
  public static String PROP_END_TIME = "endTime";
  public static String PROP_PRIORITY = "priority";
  public static String PROP_START_TIME = "startTime";
  public static String PROP_ENABLE = "enable";
  public static String PROP_ID = "id";
  public static String PROP_NAME = "name";
  

  public BaseShift()
  {
    initialize();
  }
  


  public BaseShift(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseShift(String id, String name)
  {
    setId(id);
    setName(name);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected String name;
  
  protected String description;
  
  protected Date startTime;
  
  protected Date endTime;
  
  protected String daysOfWeek;
  
  protected Long shiftLength;
  
  protected Integer priority;
  
  protected Boolean enable;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getDescription()
  {
    return description;
  }
  



  public void setDescription(String description)
  {
    this.description = description;
  }
  




  public Date getStartTime()
  {
    return startTime;
  }
  



  public void setStartTime(Date startTime)
  {
    this.startTime = startTime;
  }
  




  public Date getEndTime()
  {
    return endTime;
  }
  



  public void setEndTime(Date endTime)
  {
    this.endTime = endTime;
  }
  




  public String getDaysOfWeek()
  {
    return daysOfWeek;
  }
  



  public void setDaysOfWeek(String daysOfWeek)
  {
    this.daysOfWeek = daysOfWeek;
  }
  




  public Long getShiftLength()
  {
    return shiftLength;
  }
  



  public void setShiftLength(Long shiftLength)
  {
    this.shiftLength = shiftLength;
  }
  




  public Integer getPriority()
  {
    return priority == null ? Integer.valueOf(0) : priority;
  }
  



  public void setPriority(Integer priority)
  {
    this.priority = priority;
  }
  




  public Boolean isEnable()
  {
    return enable == null ? Boolean.valueOf(true) : enable;
  }
  



  public void setEnable(Boolean enable)
  {
    this.enable = enable;
  }
  



  public static String getEnableDefaultValue()
  {
    return "true";
  }
  


  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof Shift)) { return false;
    }
    Shift shift = (Shift)obj;
    if ((null == getId()) || (null == shift.getId())) return this == obj;
    return getId().equals(shift.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
