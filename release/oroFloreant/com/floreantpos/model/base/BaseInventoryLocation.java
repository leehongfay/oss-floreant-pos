package com.floreantpos.model.base;

import com.floreantpos.model.InventoryLocation;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;








public abstract class BaseInventoryLocation
  implements Comparable, Serializable
{
  public static String REF = "InventoryLocation";
  public static String PROP_NAME = "name";
  public static String PROP_DEFAULT_IN_LOCATION = "defaultInLocation";
  public static String PROP_DESCRIPTION = "description";
  public static String PROP_VISIBLE = "visible";
  public static String PROP_SORT_ORDER = "sortOrder";
  public static String PROP_ROOT = "root";
  public static String PROP_ADDRESS = "address";
  public static String PROP_ID = "id";
  public static String PROP_DEFAULT_OUT_LOCATION = "defaultOutLocation";
  public static String PROP_TRANSLATED_NAME = "translatedName";
  public static String PROP_PARENT_LOCATION = "parentLocation";
  public static String PROP_CODE = "code";
  

  public BaseInventoryLocation()
  {
    initialize();
  }
  


  public BaseInventoryLocation(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseInventoryLocation(String id, String name)
  {
    setId(id);
    setName(name);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  private long version;
  
  protected String name;
  
  protected String translatedName;
  
  protected String address;
  
  protected Integer sortOrder;
  
  protected Boolean visible;
  
  protected Boolean root;
  
  protected Boolean defaultInLocation;
  
  protected Boolean defaultOutLocation;
  
  protected String code;
  
  protected String description;
  
  private InventoryLocation parentLocation;
  
  private List<InventoryLocation> children;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getTranslatedName()
  {
    return translatedName;
  }
  



  public void setTranslatedName(String translatedName)
  {
    this.translatedName = translatedName;
  }
  




  public String getAddress()
  {
    return address;
  }
  



  public void setAddress(String address)
  {
    this.address = address;
  }
  




  public Integer getSortOrder()
  {
    return sortOrder == null ? Integer.valueOf(0) : sortOrder;
  }
  



  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  




  public Boolean isVisible()
  {
    return visible == null ? Boolean.FALSE : visible;
  }
  



  public void setVisible(Boolean visible)
  {
    this.visible = visible;
  }
  




  public Boolean isRoot()
  {
    return root == null ? Boolean.FALSE : root;
  }
  



  public void setRoot(Boolean root)
  {
    this.root = root;
  }
  




  public Boolean isDefaultInLocation()
  {
    return defaultInLocation == null ? Boolean.FALSE : defaultInLocation;
  }
  



  public void setDefaultInLocation(Boolean defaultInLocation)
  {
    this.defaultInLocation = defaultInLocation;
  }
  




  public Boolean isDefaultOutLocation()
  {
    return defaultOutLocation == null ? Boolean.FALSE : defaultOutLocation;
  }
  



  public void setDefaultOutLocation(Boolean defaultOutLocation)
  {
    this.defaultOutLocation = defaultOutLocation;
  }
  




  public String getCode()
  {
    return code;
  }
  



  public void setCode(String code)
  {
    this.code = code;
  }
  




  public String getDescription()
  {
    return description;
  }
  



  public void setDescription(String description)
  {
    this.description = description;
  }
  




  public InventoryLocation getParentLocation()
  {
    return parentLocation;
  }
  



  public void setParentLocation(InventoryLocation parentLocation)
  {
    this.parentLocation = parentLocation;
  }
  




  public List<InventoryLocation> getChildren()
  {
    return children;
  }
  



  public void setChildren(List<InventoryLocation> children)
  {
    this.children = children;
  }
  
  public void addTochildren(InventoryLocation inventoryLocation) {
    if (null == getChildren()) setChildren(new ArrayList());
    getChildren().add(inventoryLocation);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof InventoryLocation)) { return false;
    }
    InventoryLocation inventoryLocation = (InventoryLocation)obj;
    if ((null == getId()) || (null == inventoryLocation.getId())) return this == obj;
    return getId().equals(inventoryLocation.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
