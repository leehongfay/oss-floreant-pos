package com.floreantpos.model.base;

import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketDiscount;
import java.io.Serializable;









public abstract class BaseTicketDiscount
  implements Comparable, Serializable
{
  public static String REF = "TicketDiscount";
  public static String PROP_MINIMUM_AMOUNT = "minimumAmount";
  public static String PROP_NAME = "name";
  public static String PROP_TICKET = "ticket";
  public static String PROP_TOTAL_DISCOUNT_AMOUNT = "totalDiscountAmount";
  public static String PROP_VALUE = "value";
  public static String PROP_COUPON_QUANTITY = "couponQuantity";
  public static String PROP_DISCOUNT_ID = "discountId";
  public static String PROP_TYPE = "type";
  public static String PROP_ID = "id";
  public static String PROP_AUTO_APPLY = "autoApply";
  

  public BaseTicketDiscount()
  {
    initialize();
  }
  


  public BaseTicketDiscount(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  private long version;
  
  protected String discountId;
  
  protected String name;
  
  protected Integer type;
  
  protected Boolean autoApply;
  
  protected Double minimumAmount;
  
  protected Double couponQuantity;
  
  protected Double value;
  
  protected Double totalDiscountAmount;
  
  private Ticket ticket;
  

  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getDiscountId()
  {
    return discountId;
  }
  



  public void setDiscountId(String discountId)
  {
    this.discountId = discountId;
  }
  




  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Integer getType()
  {
    return type == null ? Integer.valueOf(0) : type;
  }
  



  public void setType(Integer type)
  {
    this.type = type;
  }
  




  public Boolean isAutoApply()
  {
    return autoApply == null ? Boolean.FALSE : autoApply;
  }
  



  public void setAutoApply(Boolean autoApply)
  {
    this.autoApply = autoApply;
  }
  




  public Double getMinimumAmount()
  {
    return minimumAmount == null ? Double.valueOf(0.0D) : minimumAmount;
  }
  



  public void setMinimumAmount(Double minimumAmount)
  {
    this.minimumAmount = minimumAmount;
  }
  




  public Double getCouponQuantity()
  {
    return couponQuantity == null ? Double.valueOf(0.0D) : couponQuantity;
  }
  



  public void setCouponQuantity(Double couponQuantity)
  {
    this.couponQuantity = couponQuantity;
  }
  




  public Double getValue()
  {
    return value == null ? Double.valueOf(0.0D) : value;
  }
  



  public void setValue(Double value)
  {
    this.value = value;
  }
  




  public Double getTotalDiscountAmount()
  {
    return totalDiscountAmount == null ? Double.valueOf(0.0D) : totalDiscountAmount;
  }
  



  public void setTotalDiscountAmount(Double totalDiscountAmount)
  {
    this.totalDiscountAmount = totalDiscountAmount;
  }
  




  public Ticket getTicket()
  {
    return ticket;
  }
  



  public void setTicket(Ticket ticket)
  {
    this.ticket = ticket;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof TicketDiscount)) { return false;
    }
    TicketDiscount ticketDiscount = (TicketDiscount)obj;
    if ((null == getId()) || (null == ticketDiscount.getId())) return this == obj;
    return getId().equals(ticketDiscount.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
