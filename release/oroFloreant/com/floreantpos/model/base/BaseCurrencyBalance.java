package com.floreantpos.model.base;

import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.Currency;
import com.floreantpos.model.CurrencyBalance;
import java.io.Serializable;








public abstract class BaseCurrencyBalance
  implements Comparable, Serializable
{
  public static String REF = "CurrencyBalance";
  public static String PROP_ID = "id";
  public static String PROP_CASH_DRAWER = "cashDrawer";
  public static String PROP_CURRENCY = "currency";
  public static String PROP_BALANCE = "balance";
  

  public BaseCurrencyBalance()
  {
    initialize();
  }
  


  public BaseCurrencyBalance(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected Double balance;
  

  private Currency currency;
  

  private CashDrawer cashDrawer;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Double getBalance()
  {
    return balance == null ? Double.valueOf(0.0D) : balance;
  }
  



  public void setBalance(Double balance)
  {
    this.balance = balance;
  }
  




  public Currency getCurrency()
  {
    return currency;
  }
  



  public void setCurrency(Currency currency)
  {
    this.currency = currency;
  }
  




  public CashDrawer getCashDrawer()
  {
    return cashDrawer;
  }
  



  public void setCashDrawer(CashDrawer cashDrawer)
  {
    this.cashDrawer = cashDrawer;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof CurrencyBalance)) { return false;
    }
    CurrencyBalance currencyBalance = (CurrencyBalance)obj;
    if ((null == getId()) || (null == currencyBalance.getId())) return this == obj;
    return getId().equals(currencyBalance.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
