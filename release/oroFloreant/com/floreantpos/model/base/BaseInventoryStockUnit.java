package com.floreantpos.model.base;

import com.floreantpos.model.InventoryStockUnit;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.PackagingUnit;
import java.io.Serializable;







public abstract class BaseInventoryStockUnit
  implements Comparable, Serializable
{
  public static String REF = "InventoryStockUnit";
  public static String PROP_PACKAGING_UNIT = "packagingUnit";
  public static String PROP_MENU_ITEM = "menuItem";
  public static String PROP_CONVERSION_VALUE = "conversionValue";
  public static String PROP_ID = "id";
  public static String PROP_UNIT = "unit";
  public static String PROP_BASE_UNIT_VALUE = "baseUnitValue";
  public static String PROP_RECIPE_UNIT = "recipeUnit";
  

  public BaseInventoryStockUnit()
  {
    initialize();
  }
  


  public BaseInventoryStockUnit(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  long version;
  
  protected Double conversionValue;
  
  protected Double baseUnitValue;
  
  protected Boolean recipeUnit;
  
  private InventoryUnit unit;
  
  private PackagingUnit packagingUnit;
  
  private MenuItem menuItem;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Double getConversionValue()
  {
    return conversionValue == null ? Double.valueOf(0.0D) : conversionValue;
  }
  



  public void setConversionValue(Double conversionValue)
  {
    this.conversionValue = conversionValue;
  }
  




  public Double getBaseUnitValue()
  {
    return baseUnitValue == null ? Double.valueOf(0.0D) : baseUnitValue;
  }
  



  public void setBaseUnitValue(Double baseUnitValue)
  {
    this.baseUnitValue = baseUnitValue;
  }
  




  public Boolean isRecipeUnit()
  {
    return recipeUnit == null ? Boolean.FALSE : recipeUnit;
  }
  



  public void setRecipeUnit(Boolean recipeUnit)
  {
    this.recipeUnit = recipeUnit;
  }
  




  public InventoryUnit getUnit()
  {
    return unit;
  }
  



  public void setUnit(InventoryUnit unit)
  {
    this.unit = unit;
  }
  




  public PackagingUnit getPackagingUnit()
  {
    return packagingUnit;
  }
  



  public void setPackagingUnit(PackagingUnit packagingUnit)
  {
    this.packagingUnit = packagingUnit;
  }
  




  public MenuItem getMenuItem()
  {
    return menuItem;
  }
  



  public void setMenuItem(MenuItem menuItem)
  {
    this.menuItem = menuItem;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof InventoryStockUnit)) { return false;
    }
    InventoryStockUnit inventoryStockUnit = (InventoryStockUnit)obj;
    if ((null == getId()) || (null == inventoryStockUnit.getId())) return this == obj;
    return getId().equals(inventoryStockUnit.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
