package com.floreantpos.model.base;

import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.InventoryUnitGroup;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;







public abstract class BaseInventoryUnitGroup
  implements Comparable, Serializable
{
  public static String REF = "InventoryUnitGroup";
  public static String PROP_NAME = "name";
  public static String PROP_ID = "id";
  

  public BaseInventoryUnitGroup()
  {
    initialize();
  }
  


  public BaseInventoryUnitGroup(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseInventoryUnitGroup(String id, String name)
  {
    setId(id);
    setName(name);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String name;
  

  private List<InventoryUnit> units;
  


  protected void initialize() {}
  


  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public List<InventoryUnit> getUnits()
  {
    return units;
  }
  



  public void setUnits(List<InventoryUnit> units)
  {
    this.units = units;
  }
  
  public void addTounits(InventoryUnit inventoryUnit) {
    if (null == getUnits()) setUnits(new ArrayList());
    getUnits().add(inventoryUnit);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof InventoryUnitGroup)) { return false;
    }
    InventoryUnitGroup inventoryUnitGroup = (InventoryUnitGroup)obj;
    if ((null == getId()) || (null == inventoryUnitGroup.getId())) return this == obj;
    return getId().equals(inventoryUnitGroup.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
