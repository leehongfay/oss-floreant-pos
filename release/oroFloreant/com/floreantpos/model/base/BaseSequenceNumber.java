package com.floreantpos.model.base;

import com.floreantpos.model.SequenceNumber;
import java.io.Serializable;










public abstract class BaseSequenceNumber
  implements Comparable, Serializable
{
  public static String REF = "SequenceNumber";
  public static String PROP_NEXT_SEQUENCE_NUMBER = "nextSequenceNumber";
  public static String PROP_TYPE = "type";
  

  public BaseSequenceNumber()
  {
    initialize();
  }
  


  public BaseSequenceNumber(String type)
  {
    setType(type);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String type;
  

  private long version;
  

  protected Integer nextSequenceNumber;
  


  protected void initialize() {}
  


  public String getType()
  {
    return type;
  }
  



  public void setType(String type)
  {
    this.type = type;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Integer getNextSequenceNumber()
  {
    return nextSequenceNumber == null ? Integer.valueOf(0) : nextSequenceNumber;
  }
  



  public void setNextSequenceNumber(Integer nextSequenceNumber)
  {
    this.nextSequenceNumber = nextSequenceNumber;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof SequenceNumber)) { return false;
    }
    SequenceNumber sequenceNumber = (SequenceNumber)obj;
    if ((null == getType()) || (null == sequenceNumber.getType())) return this == obj;
    return getType().equals(sequenceNumber.getType());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getType()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getType().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
