package com.floreantpos.model.base;

import com.floreantpos.model.TicketItemTax;
import java.io.Serializable;










public abstract class BaseTicketItemTax
  implements Comparable, Serializable
{
  public static String REF = "TicketItemTax";
  public static String PROP_NAME = "name";
  public static String PROP_ID = "id";
  public static String PROP_TAX_AMOUNT = "taxAmount";
  public static String PROP_RATE = "rate";
  

  public BaseTicketItemTax()
  {
    initialize();
  }
  


  public BaseTicketItemTax(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseTicketItemTax(String id, String name)
  {
    setId(id);
    setName(name);
    initialize();
  }
  




  private transient int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private transient long version;
  

  protected String name;
  
  protected Double rate;
  
  protected Double taxAmount;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Double getRate()
  {
    return rate == null ? Double.valueOf(0.0D) : rate;
  }
  



  public void setRate(Double rate)
  {
    this.rate = rate;
  }
  




  public Double getTaxAmount()
  {
    return taxAmount == null ? Double.valueOf(0.0D) : taxAmount;
  }
  



  public void setTaxAmount(Double taxAmount)
  {
    this.taxAmount = taxAmount;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof TicketItemTax)) { return false;
    }
    TicketItemTax ticketItemTax = (TicketItemTax)obj;
    if ((null == getId()) || (null == ticketItemTax.getId())) return this == obj;
    return getId().equals(ticketItemTax.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
