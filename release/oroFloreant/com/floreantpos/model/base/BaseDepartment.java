package com.floreantpos.model.base;

import com.floreantpos.model.Department;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Outlet;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;






public abstract class BaseDepartment
  implements Comparable, Serializable
{
  public static String REF = "Department";
  public static String PROP_NAME = "name";
  public static String PROP_DESCRIPTION = "description";
  public static String PROP_ADDRESS = "address";
  public static String PROP_ID = "id";
  

  public BaseDepartment()
  {
    initialize();
  }
  


  public BaseDepartment(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String name;
  
  protected String description;
  
  protected String address;
  
  private List<OrderType> orderTypes;
  
  private List<Outlet> outlets;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getDescription()
  {
    return description;
  }
  



  public void setDescription(String description)
  {
    this.description = description;
  }
  




  public String getAddress()
  {
    return address;
  }
  



  public void setAddress(String address)
  {
    this.address = address;
  }
  




  public List<OrderType> getOrderTypes()
  {
    return orderTypes;
  }
  



  public void setOrderTypes(List<OrderType> orderTypes)
  {
    this.orderTypes = orderTypes;
  }
  
  public void addToorderTypes(OrderType orderType) {
    if (null == getOrderTypes()) setOrderTypes(new ArrayList());
    getOrderTypes().add(orderType);
  }
  




  public List<Outlet> getOutlets()
  {
    return outlets;
  }
  



  public void setOutlets(List<Outlet> outlets)
  {
    this.outlets = outlets;
  }
  
  public void addTooutlets(Outlet outlet) {
    if (null == getOutlets()) setOutlets(new ArrayList());
    getOutlets().add(outlet);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof Department)) { return false;
    }
    Department department = (Department)obj;
    if ((null == getId()) || (null == department.getId())) return this == obj;
    return getId().equals(department.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
