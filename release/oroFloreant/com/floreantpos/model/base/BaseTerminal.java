package com.floreantpos.model.base;

import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.Terminal;
import java.io.Serializable;
import java.util.Map;








public abstract class BaseTerminal
  implements Comparable, Serializable
{
  public static String REF = "Terminal";
  public static String PROP_OPENING_BALANCE = "openingBalance";
  public static String PROP_ASSIGNED_USER_ID = "assignedUserId";
  public static String PROP_SHOW_PRNT_BTN = "showPrntBtn";
  public static String PROP_FIXED_SALES_AREA = "fixedSalesArea";
  public static String PROP_BARCODE_ON_RECEIPT = "barcodeOnReceipt";
  public static String PROP_CURRENT_BALANCE = "currentBalance";
  public static String PROP_KIOSK_MODE = "kioskMode";
  public static String PROP_NAME = "name";
  public static String PROP_GROUP_BY_CATAGORY_KIT_RECEIPT = "groupByCatagoryKitReceipt";
  public static String PROP_SMTP_SENDER = "smtpSender";
  public static String PROP_TERMINAL_TYPE_ID = "terminalTypeId";
  public static String PROP_INVENTORY_OUT_LOCATION_ID = "inventoryOutLocationId";
  public static String PROP_TRANSLATED_NAME = "translatedName";
  public static String PROP_DB_CONFIG = "dbConfig";
  public static String PROP_SMTP_PORT = "smtpPort";
  public static String PROP_INVENTORY_IN_LOCATION_ID = "inventoryInLocationId";
  public static String PROP_OUTLET_ID = "outletId";
  public static String PROP_SMTP_PASSWORD = "smtpPassword";
  public static String PROP_TICKET_SETTLEMENT = "ticketSettlement";
  public static String PROP_DEFAULT_PASS_LENGTH = "defaultPassLength";
  public static String PROP_HAS_CASH_DRAWER = "hasCashDrawer";
  public static String PROP_SALES_AREA_ID = "salesAreaId";
  public static String PROP_FLOOR_ID = "floorId";
  public static String PROP_AUTO_LOG_OFF_SEC = "autoLogOffSec";
  public static String PROP_TERMINAL_KEY = "terminalKey";
  public static String PROP_CURRENT_CASH_DRAWER = "currentCashDrawer";
  public static String PROP_ACTIVE = "active";
  public static String PROP_ENABLE_MULTI_CURRENCY = "enableMultiCurrency";
  public static String PROP_SMTP_HOST = "smtpHost";
  public static String PROP_DEPARTMENT_ID = "departmentId";
  public static String PROP_ID = "id";
  public static String PROP_LOCATION = "location";
  public static String PROP_AUTO_LOG_OFF = "autoLogOff";
  

  public BaseTerminal()
  {
    initialize();
  }
  


  public BaseTerminal(Integer id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private Integer id;
  
  long version;
  
  protected String name;
  
  protected String terminalKey;
  
  protected Double openingBalance;
  
  protected Double currentBalance;
  
  protected Boolean hasCashDrawer;
  
  protected String location;
  
  protected String assignedUserId;
  
  protected String outletId;
  
  protected String departmentId;
  
  protected String salesAreaId;
  
  protected String terminalTypeId;
  
  protected String inventoryInLocationId;
  protected String inventoryOutLocationId;
  protected Boolean ticketSettlement;
  protected Boolean dbConfig;
  protected Boolean autoLogOff;
  protected Boolean translatedName;
  protected Boolean barcodeOnReceipt;
  protected Boolean groupByCatagoryKitReceipt;
  protected Boolean enableMultiCurrency;
  protected Integer floorId;
  protected Integer autoLogOffSec;
  protected Integer defaultPassLength;
  protected String smtpHost;
  protected Integer smtpPort;
  protected String smtpSender;
  protected String smtpPassword;
  protected Boolean kioskMode;
  protected Boolean active;
  protected Boolean showPrntBtn;
  protected Boolean fixedSalesArea;
  private CashDrawer currentCashDrawer;
  private Map<String, String> properties;
  
  protected void initialize() {}
  
  public Integer getId()
  {
    return id;
  }
  



  public void setId(Integer id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getTerminalKey()
  {
    return terminalKey;
  }
  



  public void setTerminalKey(String terminalKey)
  {
    this.terminalKey = terminalKey;
  }
  




  public Double getOpeningBalance()
  {
    return openingBalance == null ? Double.valueOf(0.0D) : openingBalance;
  }
  



  public void setOpeningBalance(Double openingBalance)
  {
    this.openingBalance = openingBalance;
  }
  




  public Double getCurrentBalance()
  {
    return currentBalance == null ? Double.valueOf(0.0D) : currentBalance;
  }
  



  public void setCurrentBalance(Double currentBalance)
  {
    this.currentBalance = currentBalance;
  }
  




  public Boolean isHasCashDrawer()
  {
    return hasCashDrawer == null ? Boolean.valueOf(true) : hasCashDrawer;
  }
  



  public void setHasCashDrawer(Boolean hasCashDrawer)
  {
    this.hasCashDrawer = hasCashDrawer;
  }
  



  public static String getHasCashDrawerDefaultValue()
  {
    return "true";
  }
  



  public String getLocation()
  {
    return location;
  }
  



  public void setLocation(String location)
  {
    this.location = location;
  }
  




  public String getAssignedUserId()
  {
    return assignedUserId;
  }
  



  public void setAssignedUserId(String assignedUserId)
  {
    this.assignedUserId = assignedUserId;
  }
  




  public String getOutletId()
  {
    return outletId;
  }
  



  public void setOutletId(String outletId)
  {
    this.outletId = outletId;
  }
  




  public String getDepartmentId()
  {
    return departmentId;
  }
  



  public void setDepartmentId(String departmentId)
  {
    this.departmentId = departmentId;
  }
  




  public String getSalesAreaId()
  {
    return salesAreaId;
  }
  



  public void setSalesAreaId(String salesAreaId)
  {
    this.salesAreaId = salesAreaId;
  }
  




  public String getTerminalTypeId()
  {
    return terminalTypeId;
  }
  



  public void setTerminalTypeId(String terminalTypeId)
  {
    this.terminalTypeId = terminalTypeId;
  }
  




  public String getInventoryInLocationId()
  {
    return inventoryInLocationId;
  }
  



  public void setInventoryInLocationId(String inventoryInLocationId)
  {
    this.inventoryInLocationId = inventoryInLocationId;
  }
  




  public String getInventoryOutLocationId()
  {
    return inventoryOutLocationId;
  }
  



  public void setInventoryOutLocationId(String inventoryOutLocationId)
  {
    this.inventoryOutLocationId = inventoryOutLocationId;
  }
  




  public Boolean isTicketSettlement()
  {
    return ticketSettlement == null ? Boolean.FALSE : ticketSettlement;
  }
  



  public void setTicketSettlement(Boolean ticketSettlement)
  {
    this.ticketSettlement = ticketSettlement;
  }
  




  public Boolean isDbConfig()
  {
    return dbConfig == null ? Boolean.FALSE : dbConfig;
  }
  



  public void setDbConfig(Boolean dbConfig)
  {
    this.dbConfig = dbConfig;
  }
  




  public Boolean isAutoLogOff()
  {
    return autoLogOff == null ? Boolean.FALSE : autoLogOff;
  }
  



  public void setAutoLogOff(Boolean autoLogOff)
  {
    this.autoLogOff = autoLogOff;
  }
  




  public Boolean isTranslatedName()
  {
    return translatedName == null ? Boolean.FALSE : translatedName;
  }
  



  public void setTranslatedName(Boolean translatedName)
  {
    this.translatedName = translatedName;
  }
  




  public Boolean isBarcodeOnReceipt()
  {
    return barcodeOnReceipt == null ? Boolean.FALSE : barcodeOnReceipt;
  }
  



  public void setBarcodeOnReceipt(Boolean barcodeOnReceipt)
  {
    this.barcodeOnReceipt = barcodeOnReceipt;
  }
  




  public Boolean isGroupByCatagoryKitReceipt()
  {
    return groupByCatagoryKitReceipt == null ? Boolean.FALSE : groupByCatagoryKitReceipt;
  }
  



  public void setGroupByCatagoryKitReceipt(Boolean groupByCatagoryKitReceipt)
  {
    this.groupByCatagoryKitReceipt = groupByCatagoryKitReceipt;
  }
  




  public Boolean isEnableMultiCurrency()
  {
    return enableMultiCurrency == null ? Boolean.FALSE : enableMultiCurrency;
  }
  



  public void setEnableMultiCurrency(Boolean enableMultiCurrency)
  {
    this.enableMultiCurrency = enableMultiCurrency;
  }
  




  public Integer getFloorId()
  {
    return floorId == null ? Integer.valueOf(0) : floorId;
  }
  



  public void setFloorId(Integer floorId)
  {
    this.floorId = floorId;
  }
  




  public Integer getAutoLogOffSec()
  {
    return autoLogOffSec == null ? Integer.valueOf(0) : autoLogOffSec;
  }
  



  public void setAutoLogOffSec(Integer autoLogOffSec)
  {
    this.autoLogOffSec = autoLogOffSec;
  }
  




  public Integer getDefaultPassLength()
  {
    return defaultPassLength == null ? Integer.valueOf(0) : defaultPassLength;
  }
  



  public void setDefaultPassLength(Integer defaultPassLength)
  {
    this.defaultPassLength = defaultPassLength;
  }
  




  public String getSmtpHost()
  {
    return smtpHost;
  }
  



  public void setSmtpHost(String smtpHost)
  {
    this.smtpHost = smtpHost;
  }
  




  public Integer getSmtpPort()
  {
    return smtpPort == null ? Integer.valueOf(0) : smtpPort;
  }
  



  public void setSmtpPort(Integer smtpPort)
  {
    this.smtpPort = smtpPort;
  }
  




  public String getSmtpSender()
  {
    return smtpSender;
  }
  



  public void setSmtpSender(String smtpSender)
  {
    this.smtpSender = smtpSender;
  }
  




  public String getSmtpPassword()
  {
    return smtpPassword;
  }
  



  public void setSmtpPassword(String smtpPassword)
  {
    this.smtpPassword = smtpPassword;
  }
  




  public Boolean isKioskMode()
  {
    return kioskMode == null ? Boolean.FALSE : kioskMode;
  }
  



  public void setKioskMode(Boolean kioskMode)
  {
    this.kioskMode = kioskMode;
  }
  




  public Boolean isActive()
  {
    return active == null ? Boolean.valueOf(true) : active;
  }
  



  public void setActive(Boolean active)
  {
    this.active = active;
  }
  



  public static String getActiveDefaultValue()
  {
    return "true";
  }
  



  public Boolean isShowPrntBtn()
  {
    return showPrntBtn == null ? Boolean.valueOf(true) : showPrntBtn;
  }
  



  public void setShowPrntBtn(Boolean showPrntBtn)
  {
    this.showPrntBtn = showPrntBtn;
  }
  



  public static String getShowPrntBtnDefaultValue()
  {
    return "true";
  }
  



  public Boolean isFixedSalesArea()
  {
    return fixedSalesArea == null ? Boolean.FALSE : fixedSalesArea;
  }
  



  public void setFixedSalesArea(Boolean fixedSalesArea)
  {
    this.fixedSalesArea = fixedSalesArea;
  }
  




  public CashDrawer getCurrentCashDrawer()
  {
    return currentCashDrawer;
  }
  



  public void setCurrentCashDrawer(CashDrawer currentCashDrawer)
  {
    this.currentCashDrawer = currentCashDrawer;
  }
  




  public Map<String, String> getProperties()
  {
    return properties;
  }
  



  public void setProperties(Map<String, String> properties)
  {
    this.properties = properties;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof Terminal)) { return false;
    }
    Terminal terminal = (Terminal)obj;
    if ((null == getId()) || (null == terminal.getId())) return this == obj;
    return getId().equals(terminal.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
