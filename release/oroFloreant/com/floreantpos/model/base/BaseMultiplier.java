package com.floreantpos.model.base;

import com.floreantpos.model.Multiplier;
import java.io.Serializable;










public abstract class BaseMultiplier
  implements Comparable, Serializable
{
  public static String REF = "Multiplier";
  public static String PROP_GLOBAL_ID = "globalId";
  public static String PROP_MAIN = "main";
  public static String PROP_BUTTON_COLOR = "buttonColor";
  public static String PROP_DEFAULT_MULTIPLIER = "defaultMultiplier";
  public static String PROP_SORT_ORDER = "sortOrder";
  public static String PROP_TICKET_PREFIX = "ticketPrefix";
  public static String PROP_ID = "id";
  public static String PROP_TEXT_COLOR = "textColor";
  public static String PROP_RATE = "rate";
  

  public BaseMultiplier()
  {
    initialize();
  }
  


  public BaseMultiplier(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseMultiplier(String id, String globalId)
  {
    setId(id);
    setGlobalId(globalId);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  private long version;
  
  protected String globalId;
  
  protected String ticketPrefix;
  
  protected Double rate;
  
  protected Integer sortOrder;
  
  protected Boolean defaultMultiplier;
  
  protected Boolean main;
  
  protected Integer buttonColor;
  
  protected Integer textColor;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getGlobalId()
  {
    return globalId;
  }
  



  public void setGlobalId(String globalId)
  {
    this.globalId = globalId;
  }
  




  public String getTicketPrefix()
  {
    return ticketPrefix;
  }
  



  public void setTicketPrefix(String ticketPrefix)
  {
    this.ticketPrefix = ticketPrefix;
  }
  




  public Double getRate()
  {
    return rate == null ? Double.valueOf(0.0D) : rate;
  }
  



  public void setRate(Double rate)
  {
    this.rate = rate;
  }
  




  public Integer getSortOrder()
  {
    return sortOrder == null ? Integer.valueOf(0) : sortOrder;
  }
  



  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  




  public Boolean isDefaultMultiplier()
  {
    return defaultMultiplier == null ? Boolean.FALSE : defaultMultiplier;
  }
  



  public void setDefaultMultiplier(Boolean defaultMultiplier)
  {
    this.defaultMultiplier = defaultMultiplier;
  }
  




  public Boolean isMain()
  {
    return main == null ? Boolean.FALSE : main;
  }
  



  public void setMain(Boolean main)
  {
    this.main = main;
  }
  




  public Integer getButtonColor()
  {
    return buttonColor == null ? Integer.valueOf(0) : buttonColor;
  }
  



  public void setButtonColor(Integer buttonColor)
  {
    this.buttonColor = buttonColor;
  }
  




  public Integer getTextColor()
  {
    return textColor == null ? Integer.valueOf(0) : textColor;
  }
  



  public void setTextColor(Integer textColor)
  {
    this.textColor = textColor;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof Multiplier)) { return false;
    }
    Multiplier multiplier = (Multiplier)obj;
    if ((null == getId()) || (null == multiplier.getId())) return this == obj;
    return getId().equals(multiplier.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
