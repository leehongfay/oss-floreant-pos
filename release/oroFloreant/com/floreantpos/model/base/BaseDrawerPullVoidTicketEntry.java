package com.floreantpos.model.base;

import java.io.Serializable;










public abstract class BaseDrawerPullVoidTicketEntry
  implements Comparable, Serializable
{
  public static String REF = "DrawerPullVoidTicketEntry";
  public static String PROP_AMOUNT = "amount";
  public static String PROP_HAST = "hast";
  public static String PROP_QUANTITY = "quantity";
  public static String PROP_CODE = "code";
  public static String PROP_REASON = "reason";
  protected Integer code;
  protected String reason;
  
  public BaseDrawerPullVoidTicketEntry() {
    initialize();
  }
  



  protected Double checkCount;
  


  protected Double quantity;
  


  protected Double amount;
  

  protected void initialize() {}
  


  public Integer getCode()
  {
    return code == null ? Integer.valueOf(0) : code;
  }
  



  public void setCode(Integer code)
  {
    this.code = code;
  }
  




  public String getReason()
  {
    return reason;
  }
  



  public void setReason(String reason)
  {
    this.reason = reason;
  }
  




  public Double getCheckCount()
  {
    return checkCount;
  }
  



  public void setCheckCount(Double checkCount)
  {
    this.checkCount = checkCount;
  }
  




  public Double getQuantity()
  {
    return quantity == null ? Double.valueOf(0.0D) : quantity;
  }
  



  public void setQuantity(double totalWasteQuantity)
  {
    quantity = Double.valueOf(totalWasteQuantity);
  }
  




  public Double getAmount()
  {
    return amount == null ? Double.valueOf(0.0D) : amount;
  }
  



  public void setAmount(Double amount)
  {
    this.amount = amount;
  }
  





  public int compareTo(Object obj)
  {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
