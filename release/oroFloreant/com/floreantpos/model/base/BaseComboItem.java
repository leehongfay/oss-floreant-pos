package com.floreantpos.model.base;

import com.floreantpos.model.ComboItem;
import java.io.Serializable;










public abstract class BaseComboItem
  implements Comparable, Serializable
{
  public static String REF = "ComboItem";
  public static String PROP_PRICE = "price";
  public static String PROP_MENU_ITEM_ID = "menuItemId";
  public static String PROP_QUANTITY = "quantity";
  public static String PROP_ID = "id";
  public static String PROP_NAME = "name";
  

  public BaseComboItem()
  {
    initialize();
  }
  


  public BaseComboItem(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  long version;
  
  protected String menuItemId;
  
  protected String name;
  
  protected Double price;
  
  protected Double quantity;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getMenuItemId()
  {
    return menuItemId;
  }
  



  public void setMenuItemId(String menuItemId)
  {
    this.menuItemId = menuItemId;
  }
  




  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Double getPrice()
  {
    return price == null ? Double.valueOf(0.0D) : price;
  }
  



  public void setPrice(Double price)
  {
    this.price = price;
  }
  




  public Double getQuantity()
  {
    return quantity == null ? Double.valueOf(0.0D) : quantity;
  }
  



  public void setQuantity(Double quantity)
  {
    this.quantity = quantity;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof ComboItem)) { return false;
    }
    ComboItem comboItem = (ComboItem)obj;
    if ((null == getId()) || (null == comboItem.getId())) return this == obj;
    return getId().equals(comboItem.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
