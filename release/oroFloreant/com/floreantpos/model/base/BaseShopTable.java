package com.floreantpos.model.base;

import com.floreantpos.model.ShopSeat;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.ShopTableStatus;
import com.floreantpos.model.ShopTableType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;



public abstract class BaseShopTable
  implements Comparable, Serializable
{
  public static String REF = "ShopTable";
  public static String PROP_DESCRIPTION = "description";
  public static String PROP_SALES_AREA_ID = "salesAreaId";
  public static String PROP_FLOOR_ID = "floorId";
  public static String PROP_STATE_TABLE_TYPE = "stateTableType";
  public static String PROP_MIN_CAPACITY = "minCapacity";
  public static String PROP_GLOBAL_ID = "globalId";
  public static String PROP_NAME = "name";
  public static String PROP_RESERVABLE = "reservable";
  public static String PROP_CAPACITY = "capacity";
  public static String PROP_SHOP_TABLE_STATUS = "shopTableStatus";
  public static String PROP_X = "x";
  public static String PROP_Y = "y";
  public static String PROP_HEIGHT = "height";
  public static String PROP_ID = "id";
  public static String PROP_WIDTH = "width";
  

  public BaseShopTable()
  {
    initialize();
  }
  


  public BaseShopTable(Integer id)
  {
    setId(id);
    initialize();
  }
  





  public BaseShopTable(Integer id, String globalId)
  {
    setId(id);
    setGlobalId(globalId);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private Integer id;
  
  long version;
  
  protected String globalId;
  
  protected String name;
  
  protected String description;
  
  protected Integer capacity;
  
  protected Integer minCapacity;
  
  protected Integer x;
  
  protected Integer y;
  
  protected Integer width;
  
  protected Integer height;
  
  protected Integer stateTableType;
  
  protected Boolean reservable;
  
  protected String salesAreaId;
  protected String floorId;
  private ShopTableStatus shopTableStatus;
  private List<ShopTableType> types;
  private Set<ShopSeat> seats;
  
  protected void initialize() {}
  
  public Integer getId()
  {
    return id;
  }
  



  public void setId(Integer id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getGlobalId()
  {
    return globalId;
  }
  



  public void setGlobalId(String globalId)
  {
    this.globalId = globalId;
  }
  




  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getDescription()
  {
    return description;
  }
  



  public void setDescription(String description)
  {
    this.description = description;
  }
  




  public Integer getCapacity()
  {
    return capacity == null ? Integer.valueOf(0) : capacity;
  }
  



  public void setCapacity(Integer capacity)
  {
    this.capacity = capacity;
  }
  




  public Integer getMinCapacity()
  {
    return minCapacity == null ? Integer.valueOf(0) : minCapacity;
  }
  



  public void setMinCapacity(Integer minCapacity)
  {
    this.minCapacity = minCapacity;
  }
  




  public Integer getX()
  {
    return x == null ? Integer.valueOf(0) : x;
  }
  



  public void setX(Integer x)
  {
    this.x = x;
  }
  




  public Integer getY()
  {
    return y == null ? Integer.valueOf(0) : y;
  }
  



  public void setY(Integer y)
  {
    this.y = y;
  }
  




  public Integer getWidth()
  {
    return width == null ? Integer.valueOf(0) : width;
  }
  



  public void setWidth(Integer width)
  {
    this.width = width;
  }
  




  public Integer getHeight()
  {
    return height == null ? Integer.valueOf(0) : height;
  }
  



  public void setHeight(Integer height)
  {
    this.height = height;
  }
  




  public Integer getStateTableType()
  {
    return stateTableType == null ? Integer.valueOf(0) : stateTableType;
  }
  



  public void setStateTableType(Integer stateTableType)
  {
    this.stateTableType = stateTableType;
  }
  




  public Boolean isReservable()
  {
    return reservable == null ? Boolean.FALSE : reservable;
  }
  



  public void setReservable(Boolean reservable)
  {
    this.reservable = reservable;
  }
  




  public String getSalesAreaId()
  {
    return salesAreaId;
  }
  



  public void setSalesAreaId(String salesAreaId)
  {
    this.salesAreaId = salesAreaId;
  }
  




  public String getFloorId()
  {
    return floorId;
  }
  



  public void setFloorId(String floorId)
  {
    this.floorId = floorId;
  }
  




  public ShopTableStatus getShopTableStatus()
  {
    return shopTableStatus;
  }
  



  public void setShopTableStatus(ShopTableStatus shopTableStatus)
  {
    this.shopTableStatus = shopTableStatus;
  }
  




  public List<ShopTableType> getTypes()
  {
    return types;
  }
  



  public void setTypes(List<ShopTableType> types)
  {
    this.types = types;
  }
  
  public void addTotypes(ShopTableType shopTableType) {
    if (null == getTypes()) setTypes(new ArrayList());
    getTypes().add(shopTableType);
  }
  




  public Set<ShopSeat> getSeats()
  {
    return seats;
  }
  



  public void setSeats(Set<ShopSeat> seats)
  {
    this.seats = seats;
  }
  
  public void addToseats(ShopSeat shopSeat) {
    if (null == getSeats()) setSeats(new TreeSet());
    getSeats().add(shopSeat);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof ShopTable)) { return false;
    }
    ShopTable shopTable = (ShopTable)obj;
    if ((null == getId()) || (null == shopTable.getId())) return this == obj;
    return getId().equals(shopTable.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
