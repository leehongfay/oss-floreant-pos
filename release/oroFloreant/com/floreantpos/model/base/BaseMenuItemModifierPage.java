package com.floreantpos.model.base;

import com.floreantpos.model.MenuItemModifierPage;
import com.floreantpos.model.MenuItemModifierPageItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;







public abstract class BaseMenuItemModifierPage
  implements Comparable, Serializable
{
  public static String REF = "MenuItemModifierPage";
  public static String PROP_BUTTON_HEIGHT = "buttonHeight";
  public static String PROP_BUTTON_WIDTH = "buttonWidth";
  public static String PROP_FLIXIBLE_BUTTON_SIZE = "flixibleButtonSize";
  public static String PROP_COLS = "cols";
  public static String PROP_VISIBLE = "visible";
  public static String PROP_SORT_ORDER = "sortOrder";
  public static String PROP_MODIFIER_SPEC_ID = "modifierSpecId";
  public static String PROP_ID = "id";
  public static String PROP_NAME = "name";
  public static String PROP_ROWS = "rows";
  

  public BaseMenuItemModifierPage()
  {
    initialize();
  }
  


  public BaseMenuItemModifierPage(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseMenuItemModifierPage(String id, String name)
  {
    setId(id);
    setName(name);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected String name;
  
  protected Integer sortOrder;
  
  protected Integer cols;
  
  protected Integer buttonWidth;
  
  protected Integer buttonHeight;
  
  protected Integer rows;
  
  protected Boolean flixibleButtonSize;
  
  protected Boolean visible;
  
  protected String modifierSpecId;
  
  private List<MenuItemModifierPageItem> pageItems;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Integer getSortOrder()
  {
    return sortOrder == null ? Integer.valueOf(0) : sortOrder;
  }
  



  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  




  public Integer getCols()
  {
    return cols == null ? Integer.valueOf(0) : cols;
  }
  



  public void setCols(Integer cols)
  {
    this.cols = cols;
  }
  




  public Integer getButtonWidth()
  {
    return Integer.valueOf(buttonWidth == null ? 100 : buttonWidth.intValue());
  }
  



  public void setButtonWidth(Integer buttonWidth)
  {
    this.buttonWidth = buttonWidth;
  }
  



  public static String getButtonWidthDefaultValue()
  {
    return "100";
  }
  



  public Integer getButtonHeight()
  {
    return Integer.valueOf(buttonHeight == null ? 100 : buttonHeight.intValue());
  }
  



  public void setButtonHeight(Integer buttonHeight)
  {
    this.buttonHeight = buttonHeight;
  }
  



  public static String getButtonHeightDefaultValue()
  {
    return "100";
  }
  



  public Integer getRows()
  {
    return rows == null ? Integer.valueOf(0) : rows;
  }
  



  public void setRows(Integer rows)
  {
    this.rows = rows;
  }
  




  public Boolean isFlixibleButtonSize()
  {
    return flixibleButtonSize == null ? Boolean.FALSE : flixibleButtonSize;
  }
  



  public void setFlixibleButtonSize(Boolean flixibleButtonSize)
  {
    this.flixibleButtonSize = flixibleButtonSize;
  }
  




  public Boolean isVisible()
  {
    return visible == null ? Boolean.valueOf(true) : visible;
  }
  



  public void setVisible(Boolean visible)
  {
    this.visible = visible;
  }
  



  public static String getVisibleDefaultValue()
  {
    return "true";
  }
  



  public String getModifierSpecId()
  {
    return modifierSpecId;
  }
  



  public void setModifierSpecId(String modifierSpecId)
  {
    this.modifierSpecId = modifierSpecId;
  }
  




  public List<MenuItemModifierPageItem> getPageItems()
  {
    return pageItems;
  }
  



  public void setPageItems(List<MenuItemModifierPageItem> pageItems)
  {
    this.pageItems = pageItems;
  }
  
  public void addTopageItems(MenuItemModifierPageItem menuItemModifierPageItem) {
    if (null == getPageItems()) setPageItems(new ArrayList());
    getPageItems().add(menuItemModifierPageItem);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof MenuItemModifierPage)) { return false;
    }
    MenuItemModifierPage menuItemModifierPage = (MenuItemModifierPage)obj;
    if ((null == getId()) || (null == menuItemModifierPage.getId())) return this == obj;
    return getId().equals(menuItemModifierPage.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
