package com.floreantpos.model.base;

import com.floreantpos.model.ShopSeat;
import java.io.Serializable;










public abstract class BaseShopSeat
  implements Comparable, Serializable
{
  public static String REF = "ShopSeat";
  public static String PROP_STATUS = "status";
  public static String PROP_DISABLE = "disable";
  public static String PROP_POS_Y = "posY";
  public static String PROP_SEAT_NUMBER = "seatNumber";
  public static String PROP_POS_X = "posX";
  public static String PROP_ID = "id";
  public static String PROP_DIRTY = "dirty";
  public static String PROP_BOOKED = "booked";
  public static String PROP_SERVING = "serving";
  public static String PROP_FREE = "free";
  public static String PROP_TABLE_ID = "tableId";
  

  public BaseShopSeat()
  {
    initialize();
  }
  


  public BaseShopSeat(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected Integer seatNumber;
  
  protected Integer posX;
  
  protected Integer posY;
  
  protected String status;
  
  protected Boolean free;
  
  protected Boolean serving;
  
  protected Boolean booked;
  
  protected Boolean dirty;
  protected Boolean disable;
  protected Integer tableId;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Integer getSeatNumber()
  {
    return seatNumber == null ? Integer.valueOf(0) : seatNumber;
  }
  



  public void setSeatNumber(Integer seatNumber)
  {
    this.seatNumber = seatNumber;
  }
  




  public Integer getPosX()
  {
    return posX == null ? Integer.valueOf(0) : posX;
  }
  



  public void setPosX(Integer posX)
  {
    this.posX = posX;
  }
  




  public Integer getPosY()
  {
    return posY == null ? Integer.valueOf(0) : posY;
  }
  



  public void setPosY(Integer posY)
  {
    this.posY = posY;
  }
  




  public String getStatus()
  {
    return status;
  }
  



  public void setStatus(String status)
  {
    this.status = status;
  }
  




  public Boolean isFree()
  {
    return free == null ? Boolean.FALSE : free;
  }
  



  public void setFree(Boolean free)
  {
    this.free = free;
  }
  




  public Boolean isServing()
  {
    return serving == null ? Boolean.FALSE : serving;
  }
  



  public void setServing(Boolean serving)
  {
    this.serving = serving;
  }
  




  public Boolean isBooked()
  {
    return booked == null ? Boolean.FALSE : booked;
  }
  



  public void setBooked(Boolean booked)
  {
    this.booked = booked;
  }
  




  public Boolean isDirty()
  {
    return dirty == null ? Boolean.FALSE : dirty;
  }
  



  public void setDirty(Boolean dirty)
  {
    this.dirty = dirty;
  }
  




  public Boolean isDisable()
  {
    return disable == null ? Boolean.FALSE : disable;
  }
  



  public void setDisable(Boolean disable)
  {
    this.disable = disable;
  }
  




  public Integer getTableId()
  {
    return tableId == null ? Integer.valueOf(0) : tableId;
  }
  



  public void setTableId(Integer tableId)
  {
    this.tableId = tableId;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof ShopSeat)) { return false;
    }
    ShopSeat shopSeat = (ShopSeat)obj;
    if ((null == getId()) || (null == shopSeat.getId())) return this == obj;
    return getId().equals(shopSeat.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
