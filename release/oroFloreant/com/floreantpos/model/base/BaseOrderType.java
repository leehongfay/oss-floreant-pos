package com.floreantpos.model.base;

import com.floreantpos.model.Department;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.TerminalType;
import java.io.Serializable;
import java.sql.Blob;
import java.util.Set;
import java.util.TreeSet;
import javax.xml.bind.annotation.XmlTransient;



public abstract class BaseOrderType
  implements Comparable, Serializable
{
  public static String REF = "OrderType";
  public static String PROP_SHOW_IMAGE_ONLY = "showImageOnly";
  public static String PROP_CLOSE_ON_PAID = "closeOnPaid";
  public static String PROP_SHOW_TABLE_SELECTION = "showTableSelection";
  public static String PROP_SHOULD_PRINT_TO_KITCHEN = "shouldPrintToKitchen";
  public static String PROP_ALLOW_SEAT_BASED_ORDER = "allowSeatBasedOrder";
  public static String PROP_PRE_AUTH_CREDIT_CARD = "preAuthCreditCard";
  public static String PROP_BAR_TAB = "barTab";
  public static String PROP_NAME = "name";
  public static String PROP_ENABLE_REORDER = "enableReorder";
  public static String PROP_IMAGE_DATA = "imageData";
  public static String PROP_TO_GO_TAX_GROUP_ID = "toGoTaxGroupId";
  public static String PROP_SHOW_GUEST_SELECTION = "showGuestSelection";
  public static String PROP_PICKUP = "pickup";
  public static String PROP_REQUIRED_DELIVERY_DATA = "requiredDeliveryData";
  public static String PROP_HAS_FOR_HERE_AND_TO_GO = "hasForHereAndToGo";
  public static String PROP_SALES_AREA_ID = "salesAreaId";
  public static String PROP_SORT_ORDER = "sortOrder";
  public static String PROP_ENABLED = "enabled";
  public static String PROP_SHOW_IN_LOGIN_SCREEN = "showInLoginScreen";
  public static String PROP_TEXT_COLOR_CODE = "textColorCode";
  public static String PROP_DEFAULT_TAX_GROUP_ID = "defaultTaxGroupId";
  public static String PROP_PREPAID = "prepaid";
  public static String PROP_FOR_HERE_TAX_GROUP_ID = "forHereTaxGroupId";
  public static String PROP_ENABLE_COURSE = "enableCourse";
  public static String PROP_ASSIGN_DRIVER = "assignDriver";
  public static String PROP_REQUIRED_CUSTOMER_DATA = "requiredCustomerData";
  public static String PROP_DELIVERY = "delivery";
  public static String PROP_HIDE_ITEM_WITH_EMPTY_INVENTORY = "hideItemWithEmptyInventory";
  public static String PROP_ID = "id";
  public static String PROP_RETAIL_ORDER = "retailOrder";
  public static String PROP_CONSOLIDATE_ITEMS_IN_RECEIPT = "consolidateItemsInReceipt";
  public static String PROP_BUTTON_COLOR_CODE = "buttonColorCode";
  

  public BaseOrderType()
  {
    initialize();
  }
  


  public BaseOrderType(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseOrderType(String id, String name)
  {
    setId(id);
    setName(name);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected String name;
  
  protected Integer sortOrder;
  
  protected Boolean enabled;
  
  protected Boolean showTableSelection;
  
  protected Boolean showGuestSelection;
  
  protected Boolean shouldPrintToKitchen;
  
  protected Boolean prepaid;
  
  protected Boolean closeOnPaid;
  
  protected Boolean requiredCustomerData;
  
  protected Boolean delivery;
  protected Boolean showInLoginScreen;
  protected Boolean consolidateItemsInReceipt;
  protected Boolean allowSeatBasedOrder;
  protected Boolean hideItemWithEmptyInventory;
  protected Boolean hasForHereAndToGo;
  protected Boolean preAuthCreditCard;
  protected Boolean barTab;
  protected Boolean enableReorder;
  protected Boolean enableCourse;
  protected Boolean pickup;
  protected Boolean retailOrder;
  protected Boolean requiredDeliveryData;
  protected Boolean assignDriver;
  protected Blob imageData;
  protected Integer textColorCode;
  protected Integer buttonColorCode;
  protected Boolean showImageOnly;
  protected String salesAreaId;
  protected String defaultTaxGroupId;
  protected String forHereTaxGroupId;
  protected String toGoTaxGroupId;
  private Set<TerminalType> terminalTypes;
  private Set<MenuCategory> categories;
  private Set<Department> departments;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Integer getSortOrder()
  {
    return sortOrder == null ? Integer.valueOf(0) : sortOrder;
  }
  



  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  




  public Boolean isEnabled()
  {
    return enabled == null ? Boolean.FALSE : enabled;
  }
  



  public void setEnabled(Boolean enabled)
  {
    this.enabled = enabled;
  }
  




  public Boolean isShowTableSelection()
  {
    return showTableSelection == null ? Boolean.FALSE : showTableSelection;
  }
  



  public void setShowTableSelection(Boolean showTableSelection)
  {
    this.showTableSelection = showTableSelection;
  }
  




  public Boolean isShowGuestSelection()
  {
    return showGuestSelection == null ? Boolean.FALSE : showGuestSelection;
  }
  



  public void setShowGuestSelection(Boolean showGuestSelection)
  {
    this.showGuestSelection = showGuestSelection;
  }
  




  public Boolean isShouldPrintToKitchen()
  {
    return shouldPrintToKitchen == null ? Boolean.FALSE : shouldPrintToKitchen;
  }
  



  public void setShouldPrintToKitchen(Boolean shouldPrintToKitchen)
  {
    this.shouldPrintToKitchen = shouldPrintToKitchen;
  }
  




  public Boolean isPrepaid()
  {
    return prepaid == null ? Boolean.FALSE : prepaid;
  }
  



  public void setPrepaid(Boolean prepaid)
  {
    this.prepaid = prepaid;
  }
  




  public Boolean isCloseOnPaid()
  {
    return closeOnPaid == null ? Boolean.FALSE : closeOnPaid;
  }
  



  public void setCloseOnPaid(Boolean closeOnPaid)
  {
    this.closeOnPaid = closeOnPaid;
  }
  




  public Boolean isRequiredCustomerData()
  {
    return requiredCustomerData == null ? Boolean.FALSE : requiredCustomerData;
  }
  



  public void setRequiredCustomerData(Boolean requiredCustomerData)
  {
    this.requiredCustomerData = requiredCustomerData;
  }
  




  public Boolean isDelivery()
  {
    return delivery == null ? Boolean.FALSE : delivery;
  }
  



  public void setDelivery(Boolean delivery)
  {
    this.delivery = delivery;
  }
  




  public Boolean isShowInLoginScreen()
  {
    return showInLoginScreen == null ? Boolean.FALSE : showInLoginScreen;
  }
  



  public void setShowInLoginScreen(Boolean showInLoginScreen)
  {
    this.showInLoginScreen = showInLoginScreen;
  }
  




  public Boolean isConsolidateItemsInReceipt()
  {
    return consolidateItemsInReceipt == null ? Boolean.FALSE : consolidateItemsInReceipt;
  }
  



  public void setConsolidateItemsInReceipt(Boolean consolidateItemsInReceipt)
  {
    this.consolidateItemsInReceipt = consolidateItemsInReceipt;
  }
  




  public Boolean isAllowSeatBasedOrder()
  {
    return allowSeatBasedOrder == null ? Boolean.FALSE : allowSeatBasedOrder;
  }
  



  public void setAllowSeatBasedOrder(Boolean allowSeatBasedOrder)
  {
    this.allowSeatBasedOrder = allowSeatBasedOrder;
  }
  




  public Boolean isHideItemWithEmptyInventory()
  {
    return hideItemWithEmptyInventory == null ? Boolean.FALSE : hideItemWithEmptyInventory;
  }
  



  public void setHideItemWithEmptyInventory(Boolean hideItemWithEmptyInventory)
  {
    this.hideItemWithEmptyInventory = hideItemWithEmptyInventory;
  }
  




  public Boolean isHasForHereAndToGo()
  {
    return hasForHereAndToGo == null ? Boolean.FALSE : hasForHereAndToGo;
  }
  



  public void setHasForHereAndToGo(Boolean hasForHereAndToGo)
  {
    this.hasForHereAndToGo = hasForHereAndToGo;
  }
  




  public Boolean isPreAuthCreditCard()
  {
    return preAuthCreditCard == null ? Boolean.FALSE : preAuthCreditCard;
  }
  



  public void setPreAuthCreditCard(Boolean preAuthCreditCard)
  {
    this.preAuthCreditCard = preAuthCreditCard;
  }
  




  public Boolean isBarTab()
  {
    return barTab == null ? Boolean.FALSE : barTab;
  }
  



  public void setBarTab(Boolean barTab)
  {
    this.barTab = barTab;
  }
  




  public Boolean isEnableReorder()
  {
    return enableReorder == null ? Boolean.FALSE : enableReorder;
  }
  



  public void setEnableReorder(Boolean enableReorder)
  {
    this.enableReorder = enableReorder;
  }
  




  public Boolean isEnableCourse()
  {
    return enableCourse == null ? Boolean.FALSE : enableCourse;
  }
  



  public void setEnableCourse(Boolean enableCourse)
  {
    this.enableCourse = enableCourse;
  }
  




  public Boolean isPickup()
  {
    return pickup == null ? Boolean.FALSE : pickup;
  }
  



  public void setPickup(Boolean pickup)
  {
    this.pickup = pickup;
  }
  




  public Boolean isRetailOrder()
  {
    return retailOrder == null ? Boolean.FALSE : retailOrder;
  }
  



  public void setRetailOrder(Boolean retailOrder)
  {
    this.retailOrder = retailOrder;
  }
  




  public Boolean isRequiredDeliveryData()
  {
    return requiredDeliveryData == null ? Boolean.FALSE : requiredDeliveryData;
  }
  



  public void setRequiredDeliveryData(Boolean requiredDeliveryData)
  {
    this.requiredDeliveryData = requiredDeliveryData;
  }
  




  public Boolean isAssignDriver()
  {
    return assignDriver == null ? Boolean.FALSE : assignDriver;
  }
  



  public void setAssignDriver(Boolean assignDriver)
  {
    this.assignDriver = assignDriver;
  }
  




  @XmlTransient
  public Blob getImageData()
  {
    return imageData;
  }
  



  public void setImageData(Blob imageData)
  {
    this.imageData = imageData;
  }
  



  public static String getImageDataXmlTransientAnnotation()
  {
    return "@javax.xml.bind.annotation.XmlTransient";
  }
  



  public Integer getTextColorCode()
  {
    return textColorCode == null ? null : textColorCode;
  }
  



  public void setTextColorCode(Integer textColorCode)
  {
    this.textColorCode = textColorCode;
  }
  



  public static String getTextColorCodeDefaultValue()
  {
    return "null";
  }
  



  public Integer getButtonColorCode()
  {
    return buttonColorCode == null ? null : buttonColorCode;
  }
  



  public void setButtonColorCode(Integer buttonColorCode)
  {
    this.buttonColorCode = buttonColorCode;
  }
  



  public static String getButtonColorCodeDefaultValue()
  {
    return "null";
  }
  



  public Boolean isShowImageOnly()
  {
    return showImageOnly == null ? Boolean.FALSE : showImageOnly;
  }
  



  public void setShowImageOnly(Boolean showImageOnly)
  {
    this.showImageOnly = showImageOnly;
  }
  




  public String getSalesAreaId()
  {
    return salesAreaId;
  }
  



  public void setSalesAreaId(String salesAreaId)
  {
    this.salesAreaId = salesAreaId;
  }
  




  public String getDefaultTaxGroupId()
  {
    return defaultTaxGroupId;
  }
  



  public void setDefaultTaxGroupId(String defaultTaxGroupId)
  {
    this.defaultTaxGroupId = defaultTaxGroupId;
  }
  




  public String getForHereTaxGroupId()
  {
    return forHereTaxGroupId;
  }
  



  public void setForHereTaxGroupId(String forHereTaxGroupId)
  {
    this.forHereTaxGroupId = forHereTaxGroupId;
  }
  




  public String getToGoTaxGroupId()
  {
    return toGoTaxGroupId;
  }
  



  public void setToGoTaxGroupId(String toGoTaxGroupId)
  {
    this.toGoTaxGroupId = toGoTaxGroupId;
  }
  




  public Set<TerminalType> getTerminalTypes()
  {
    return terminalTypes;
  }
  



  public void setTerminalTypes(Set<TerminalType> terminalTypes)
  {
    this.terminalTypes = terminalTypes;
  }
  
  public void addToterminalTypes(TerminalType terminalType) {
    if (null == getTerminalTypes()) setTerminalTypes(new TreeSet());
    getTerminalTypes().add(terminalType);
  }
  




  public Set<MenuCategory> getCategories()
  {
    return categories;
  }
  



  public void setCategories(Set<MenuCategory> categories)
  {
    this.categories = categories;
  }
  
  public void addTocategories(MenuCategory menuCategory) {
    if (null == getCategories()) setCategories(new TreeSet());
    getCategories().add(menuCategory);
  }
  




  public Set<Department> getDepartments()
  {
    return departments;
  }
  



  public void setDepartments(Set<Department> departments)
  {
    this.departments = departments;
  }
  
  public void addTodepartments(Department department) {
    if (null == getDepartments()) setDepartments(new TreeSet());
    getDepartments().add(department);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof OrderType)) { return false;
    }
    OrderType orderType = (OrderType)obj;
    if ((null == getId()) || (null == orderType.getId())) return this == obj;
    return getId().equals(orderType.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
