package com.floreantpos.model.base;

import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.Ticket;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;







public abstract class BasePosTransaction
  implements Comparable, Serializable
{
  public static String REF = "PosTransaction";
  public static String PROP_CARD_NUMBER = "cardNumber";
  public static String PROP_TRANSACTION_TYPE = "transactionType";
  public static String PROP_CARD_AUTH_CODE = "cardAuthCode";
  public static String PROP_GIFT_CERT_NUMBER = "giftCertNumber";
  public static String PROP_GIFT_CERT_PAID_AMOUNT = "giftCertPaidAmount";
  public static String PROP_RECEPIENT_ID = "recepientId";
  public static String PROP_HAS_SYNC_ERROR = "hasSyncError";
  public static String PROP_CARD_HOLDER_NAME = "cardHolderName";
  public static String PROP_CUSTOMER_BALANCE_BEFORE = "customerBalanceBefore";
  public static String PROP_GIFT_CERT_FACE_VALUE = "giftCertFaceValue";
  public static String PROP_TAX_AMOUNT = "taxAmount";
  public static String PROP_CARD_READER = "cardReader";
  public static String PROP_CARD_TYPE = "cardType";
  public static String PROP_CASH_DRAWER_ID = "cashDrawerId";
  public static String PROP_DRAWER_RESETTED = "drawerResetted";
  public static String PROP_PAYMENT_TYPE = "paymentType";
  public static String PROP_AUTHORIZABLE = "authorizable";
  public static String PROP_TERMINAL_ID = "terminalId";
  public static String PROP_REASON_ID = "reasonId";
  public static String PROP_VOIDED = "voided";
  public static String PROP_CAPTURED = "captured";
  public static String PROP_CARD_TRANSACTION_ID = "cardTransactionId";
  public static String PROP_TICKET = "ticket";
  public static String PROP_CARD_MERCHANT_GATEWAY = "cardMerchantGateway";
  public static String PROP_AMOUNT = "amount";
  public static String PROP_TENDER_AMOUNT = "tenderAmount";
  public static String PROP_TIPS_AMOUNT = "tipsAmount";
  public static String PROP_CUSTOMER_ID = "customerId";
  public static String PROP_GIFT_CERT_CASH_BACK_AMOUNT = "giftCertCashBackAmount";
  public static String PROP_TRANSACTION_TIME = "transactionTime";
  public static String PROP_CLOUD_SYNCED = "cloudSynced";
  public static String PROP_USER_ID = "userId";
  public static String PROP_NOTE = "note";
  public static String PROP_CUSTOMER_BALANCE_AFTER = "customerBalanceAfter";
  public static String PROP_DEPARTMENT_ID = "departmentId";
  public static String PROP_ID = "id";
  

  public BasePosTransaction()
  {
    initialize();
  }
  


  public BasePosTransaction(String id)
  {
    setId(id);
    initialize();
  }
  






  public BasePosTransaction(String id, String transactionType, String paymentType)
  {
    setId(id);
    setTransactionType(transactionType);
    setPaymentType(paymentType);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  private Date transactionTime;
  
  private Double amount;
  
  private Double taxAmount;
  
  private Double tipsAmount;
  
  private Double tenderAmount;
  
  private String transactionType;
  
  private Boolean voided;
  
  private String paymentType;
  
  private Boolean captured;
  
  private Boolean authorizable;
  
  private String cardHolderName;
  
  private String cardNumber;
  private String cardAuthCode;
  private String cardType;
  private String cardTransactionId;
  private String cardMerchantGateway;
  private String cardReader;
  private String giftCertNumber;
  private Double giftCertFaceValue;
  private Double giftCertPaidAmount;
  private Double giftCertCashBackAmount;
  private String customerId;
  private Double customerBalanceBefore;
  private Double customerBalanceAfter;
  private Boolean drawerResetted;
  private String note;
  private String departmentId;
  private Integer terminalId;
  private String userId;
  private String cashDrawerId;
  private String reasonId;
  private String recepientId;
  private Boolean cloudSynced;
  private Boolean hasSyncError;
  private Ticket ticket;
  private Map<String, String> properties;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Date getTransactionTime()
  {
    return transactionTime;
  }
  



  public void setTransactionTime(Date transactionTime)
  {
    this.transactionTime = transactionTime;
  }
  




  public Double getAmount()
  {
    return amount == null ? Double.valueOf(0.0D) : amount;
  }
  



  public void setAmount(Double amount)
  {
    this.amount = amount;
  }
  




  public Double getTaxAmount()
  {
    return taxAmount == null ? Double.valueOf(0.0D) : taxAmount;
  }
  



  public void setTaxAmount(Double taxAmount)
  {
    this.taxAmount = taxAmount;
  }
  




  public Double getTipsAmount()
  {
    return tipsAmount == null ? Double.valueOf(0.0D) : tipsAmount;
  }
  



  public void setTipsAmount(Double tipsAmount)
  {
    this.tipsAmount = tipsAmount;
  }
  




  public Double getTenderAmount()
  {
    return tenderAmount == null ? Double.valueOf(0.0D) : tenderAmount;
  }
  



  public void setTenderAmount(Double tenderAmount)
  {
    this.tenderAmount = tenderAmount;
  }
  




  public String getTransactionType()
  {
    return transactionType;
  }
  



  public void setTransactionType(String transactionType)
  {
    this.transactionType = transactionType;
  }
  




  public Boolean isVoided()
  {
    return voided == null ? Boolean.FALSE : voided;
  }
  



  public void setVoided(Boolean voided)
  {
    this.voided = voided;
  }
  




  public String getPaymentType()
  {
    return paymentType;
  }
  



  public void setPaymentType(String paymentType)
  {
    this.paymentType = paymentType;
  }
  




  public Boolean isCaptured()
  {
    return captured == null ? Boolean.FALSE : captured;
  }
  



  public void setCaptured(Boolean captured)
  {
    this.captured = captured;
  }
  




  public Boolean isAuthorizable()
  {
    return authorizable == null ? Boolean.FALSE : authorizable;
  }
  



  public void setAuthorizable(Boolean authorizable)
  {
    this.authorizable = authorizable;
  }
  




  public String getCardHolderName()
  {
    return cardHolderName;
  }
  



  public void setCardHolderName(String cardHolderName)
  {
    this.cardHolderName = cardHolderName;
  }
  




  public String getCardNumber()
  {
    return cardNumber;
  }
  



  public void setCardNumber(String cardNumber)
  {
    this.cardNumber = cardNumber;
  }
  




  public String getCardAuthCode()
  {
    return cardAuthCode;
  }
  



  public void setCardAuthCode(String cardAuthCode)
  {
    this.cardAuthCode = cardAuthCode;
  }
  




  public String getCardType()
  {
    return cardType;
  }
  



  public void setCardType(String cardType)
  {
    this.cardType = cardType;
  }
  




  public String getCardTransactionId()
  {
    return cardTransactionId;
  }
  



  public void setCardTransactionId(String cardTransactionId)
  {
    this.cardTransactionId = cardTransactionId;
  }
  




  public String getCardMerchantGateway()
  {
    return cardMerchantGateway;
  }
  



  public void setCardMerchantGateway(String cardMerchantGateway)
  {
    this.cardMerchantGateway = cardMerchantGateway;
  }
  




  public String getCardReader()
  {
    return cardReader;
  }
  



  public void setCardReader(String cardReader)
  {
    this.cardReader = cardReader;
  }
  




  public String getGiftCertNumber()
  {
    return giftCertNumber;
  }
  



  public void setGiftCertNumber(String giftCertNumber)
  {
    this.giftCertNumber = giftCertNumber;
  }
  




  public Double getGiftCertFaceValue()
  {
    return giftCertFaceValue == null ? Double.valueOf(0.0D) : giftCertFaceValue;
  }
  



  public void setGiftCertFaceValue(Double giftCertFaceValue)
  {
    this.giftCertFaceValue = giftCertFaceValue;
  }
  




  public Double getGiftCertPaidAmount()
  {
    return giftCertPaidAmount == null ? Double.valueOf(0.0D) : giftCertPaidAmount;
  }
  



  public void setGiftCertPaidAmount(Double giftCertPaidAmount)
  {
    this.giftCertPaidAmount = giftCertPaidAmount;
  }
  




  public Double getGiftCertCashBackAmount()
  {
    return giftCertCashBackAmount == null ? Double.valueOf(0.0D) : giftCertCashBackAmount;
  }
  



  public void setGiftCertCashBackAmount(Double giftCertCashBackAmount)
  {
    this.giftCertCashBackAmount = giftCertCashBackAmount;
  }
  




  public String getCustomerId()
  {
    return customerId;
  }
  



  public void setCustomerId(String customerId)
  {
    this.customerId = customerId;
  }
  




  public Double getCustomerBalanceBefore()
  {
    return customerBalanceBefore == null ? Double.valueOf(0.0D) : customerBalanceBefore;
  }
  



  public void setCustomerBalanceBefore(Double customerBalanceBefore)
  {
    this.customerBalanceBefore = customerBalanceBefore;
  }
  




  public Double getCustomerBalanceAfter()
  {
    return customerBalanceAfter == null ? Double.valueOf(0.0D) : customerBalanceAfter;
  }
  



  public void setCustomerBalanceAfter(Double customerBalanceAfter)
  {
    this.customerBalanceAfter = customerBalanceAfter;
  }
  




  public Boolean isDrawerResetted()
  {
    return drawerResetted == null ? Boolean.FALSE : drawerResetted;
  }
  



  public void setDrawerResetted(Boolean drawerResetted)
  {
    this.drawerResetted = drawerResetted;
  }
  




  public String getNote()
  {
    return note;
  }
  



  public void setNote(String note)
  {
    this.note = note;
  }
  




  public String getDepartmentId()
  {
    return departmentId;
  }
  



  public void setDepartmentId(String departmentId)
  {
    this.departmentId = departmentId;
  }
  




  public Integer getTerminalId()
  {
    return terminalId == null ? Integer.valueOf(0) : terminalId;
  }
  



  public void setTerminalId(Integer terminalId)
  {
    this.terminalId = terminalId;
  }
  




  public String getUserId()
  {
    return userId;
  }
  



  public void setUserId(String userId)
  {
    this.userId = userId;
  }
  




  public String getCashDrawerId()
  {
    return cashDrawerId;
  }
  



  public void setCashDrawerId(String cashDrawerId)
  {
    this.cashDrawerId = cashDrawerId;
  }
  




  public String getReasonId()
  {
    return reasonId;
  }
  



  public void setReasonId(String reasonId)
  {
    this.reasonId = reasonId;
  }
  




  public String getRecepientId()
  {
    return recepientId;
  }
  



  public void setRecepientId(String recepientId)
  {
    this.recepientId = recepientId;
  }
  




  public Boolean isCloudSynced()
  {
    return cloudSynced == null ? Boolean.FALSE : cloudSynced;
  }
  



  public void setCloudSynced(Boolean cloudSynced)
  {
    this.cloudSynced = cloudSynced;
  }
  




  public Boolean isHasSyncError()
  {
    return hasSyncError == null ? Boolean.FALSE : hasSyncError;
  }
  



  public void setHasSyncError(Boolean hasSyncError)
  {
    this.hasSyncError = hasSyncError;
  }
  




  public Ticket getTicket()
  {
    return ticket;
  }
  



  public void setTicket(Ticket ticket)
  {
    this.ticket = ticket;
  }
  




  public Map<String, String> getProperties()
  {
    return properties;
  }
  



  public void setProperties(Map<String, String> properties)
  {
    this.properties = properties;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof PosTransaction)) { return false;
    }
    PosTransaction posTransaction = (PosTransaction)obj;
    if ((null == getId()) || (null == posTransaction.getId())) return this == obj;
    return getId().equals(posTransaction.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
