package com.floreantpos.model.base;

import com.floreantpos.model.InventoryDepartment;
import com.floreantpos.model.InventoryLocation;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;







public abstract class BaseInventoryDepartment
  implements Comparable, Serializable
{
  public static String REF = "InventoryDepartment";
  public static String PROP_NAME = "name";
  public static String PROP_ID = "id";
  

  public BaseInventoryDepartment()
  {
    initialize();
  }
  


  public BaseInventoryDepartment(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String name;
  

  private List<InventoryLocation> locations;
  


  protected void initialize() {}
  


  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public List<InventoryLocation> getLocations()
  {
    return locations;
  }
  



  public void setLocations(List<InventoryLocation> locations)
  {
    this.locations = locations;
  }
  
  public void addTolocations(InventoryLocation inventoryLocation) {
    if (null == getLocations()) setLocations(new ArrayList());
    getLocations().add(inventoryLocation);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof InventoryDepartment)) { return false;
    }
    InventoryDepartment inventoryDepartment = (InventoryDepartment)obj;
    if ((null == getId()) || (null == inventoryDepartment.getId())) return this == obj;
    return getId().equals(inventoryDepartment.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
