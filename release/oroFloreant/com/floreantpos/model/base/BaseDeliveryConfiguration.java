package com.floreantpos.model.base;

import com.floreantpos.model.DeliveryConfiguration;
import java.io.Serializable;










public abstract class BaseDeliveryConfiguration
  implements Comparable, Serializable
{
  public static String REF = "DeliveryConfiguration";
  public static String PROP_ID = "id";
  

  public BaseDeliveryConfiguration()
  {
    initialize();
  }
  


  public BaseDeliveryConfiguration(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  


  protected void initialize() {}
  


  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  




  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof DeliveryConfiguration)) { return false;
    }
    DeliveryConfiguration deliveryConfiguration = (DeliveryConfiguration)obj;
    if ((null == getId()) || (null == deliveryConfiguration.getId())) return this == obj;
    return getId().equals(deliveryConfiguration.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
