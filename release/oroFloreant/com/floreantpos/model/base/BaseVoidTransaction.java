package com.floreantpos.model.base;

import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.VoidTransaction;
import java.io.Serializable;








public abstract class BaseVoidTransaction
  extends PosTransaction
  implements Comparable, Serializable
{
  public static String REF = "VoidTransaction";
  public static String PROP_ID = "id";
  

  public BaseVoidTransaction()
  {
    initialize();
  }
  


  public BaseVoidTransaction(String id)
  {
    super(id);
  }
  






  public BaseVoidTransaction(String id, String transactionType, String paymentType)
  {
    super(id, transactionType, paymentType);
  }
  





  private int hashCode = Integer.MIN_VALUE;
  







  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof VoidTransaction)) { return false;
    }
    VoidTransaction voidTransaction = (VoidTransaction)obj;
    if ((null == getId()) || (null == voidTransaction.getId())) return this == obj;
    return getId().equals(voidTransaction.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
