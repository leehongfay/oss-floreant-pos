package com.floreantpos.model.base;

import com.floreantpos.model.GuestCheckPrint;
import com.floreantpos.model.User;
import java.io.Serializable;
import java.util.Date;








public abstract class BaseGuestCheckPrint
  implements Comparable, Serializable
{
  public static String REF = "GuestCheckPrint";
  public static String PROP_TICKET_TOTAL = "ticketTotal";
  public static String PROP_TICKET_ID = "ticketId";
  public static String PROP_USER = "user";
  public static String PROP_TABLE_NO = "tableNo";
  public static String PROP_ID = "id";
  public static String PROP_PRINT_TIME = "printTime";
  

  public BaseGuestCheckPrint()
  {
    initialize();
  }
  


  public BaseGuestCheckPrint(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  protected String ticketId;
  

  protected String tableNo;
  
  protected Double ticketTotal;
  
  protected Date printTime;
  
  private User user;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  





  public String getTicketId()
  {
    return ticketId;
  }
  



  public void setTicketId(String ticketId)
  {
    this.ticketId = ticketId;
  }
  




  public String getTableNo()
  {
    return tableNo;
  }
  



  public void setTableNo(String tableNo)
  {
    this.tableNo = tableNo;
  }
  




  public Double getTicketTotal()
  {
    return ticketTotal == null ? Double.valueOf(0.0D) : ticketTotal;
  }
  



  public void setTicketTotal(Double ticketTotal)
  {
    this.ticketTotal = ticketTotal;
  }
  




  public Date getPrintTime()
  {
    return printTime;
  }
  



  public void setPrintTime(Date printTime)
  {
    this.printTime = printTime;
  }
  




  public User getUser()
  {
    return user;
  }
  



  public void setUser(User user)
  {
    this.user = user;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof GuestCheckPrint)) { return false;
    }
    GuestCheckPrint guestCheckPrint = (GuestCheckPrint)obj;
    if ((null == getId()) || (null == guestCheckPrint.getId())) return this == obj;
    return getId().equals(guestCheckPrint.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
