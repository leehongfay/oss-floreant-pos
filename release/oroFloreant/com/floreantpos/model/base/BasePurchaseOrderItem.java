package com.floreantpos.model.base;

import com.floreantpos.model.PurchaseOrder;
import com.floreantpos.model.PurchaseOrderItem;
import java.io.Serializable;









public abstract class BasePurchaseOrderItem
  implements Comparable, Serializable
{
  public static String REF = "PurchaseOrderItem";
  public static String PROP_BEVERAGE = "beverage";
  public static String PROP_CATEGORY_NAME = "categoryName";
  public static String PROP_GROUP_NAME = "groupName";
  public static String PROP_QUANTITY_RECEIVED = "quantityReceived";
  public static String PROP_ITEM_UNIT_NAME = "itemUnitName";
  public static String PROP_TAX_RATE = "taxRate";
  public static String PROP_TAX_AMOUNT = "taxAmount";
  public static String PROP_UNIT_PRICE = "unitPrice";
  public static String PROP_DISCOUNT_AMOUNT = "discountAmount";
  public static String PROP_PURCHASE_ORDER = "purchaseOrder";
  public static String PROP_SKU = "sku";
  public static String PROP_NAME = "name";
  public static String PROP_STATUS = "status";
  public static String PROP_SUBTOTAL_AMOUNT = "subtotalAmount";
  public static String PROP_ID = "id";
  public static String PROP_TOTAL_AMOUNT = "totalAmount";
  public static String PROP_ITEM_QUANTITY = "itemQuantity";
  public static String PROP_MENU_ITEM_ID = "menuItemId";
  

  public BasePurchaseOrderItem()
  {
    initialize();
  }
  


  public BasePurchaseOrderItem(String id)
  {
    setId(id);
    initialize();
  }
  





  public BasePurchaseOrderItem(String id, PurchaseOrder purchaseOrder)
  {
    setId(id);
    setPurchaseOrder(purchaseOrder);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  private long version;
  
  protected String sku;
  
  protected String menuItemId;
  
  protected Double itemQuantity;
  
  protected Double quantityReceived;
  
  protected String name;
  
  protected String itemUnitName;
  
  protected String groupName;
  
  protected String categoryName;
  
  protected Double unitPrice;
  
  protected Double taxRate;
  protected Double subtotalAmount;
  protected Double discountAmount;
  protected Double taxAmount;
  protected Double totalAmount;
  protected Boolean beverage;
  protected String status;
  private PurchaseOrder purchaseOrder;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getSku()
  {
    return sku;
  }
  



  public void setSku(String sku)
  {
    this.sku = sku;
  }
  




  public String getMenuItemId()
  {
    return menuItemId;
  }
  



  public void setMenuItemId(String menuItemId)
  {
    this.menuItemId = menuItemId;
  }
  




  public Double getItemQuantity()
  {
    return itemQuantity == null ? Double.valueOf(0.0D) : itemQuantity;
  }
  



  public void setItemQuantity(Double itemQuantity)
  {
    this.itemQuantity = itemQuantity;
  }
  




  public Double getQuantityReceived()
  {
    return quantityReceived == null ? Double.valueOf(0.0D) : quantityReceived;
  }
  



  public void setQuantityReceived(Double quantityReceived)
  {
    this.quantityReceived = quantityReceived;
  }
  




  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getItemUnitName()
  {
    return itemUnitName;
  }
  



  public void setItemUnitName(String itemUnitName)
  {
    this.itemUnitName = itemUnitName;
  }
  




  public String getGroupName()
  {
    return groupName;
  }
  



  public void setGroupName(String groupName)
  {
    this.groupName = groupName;
  }
  




  public String getCategoryName()
  {
    return categoryName;
  }
  



  public void setCategoryName(String categoryName)
  {
    this.categoryName = categoryName;
  }
  




  public Double getUnitPrice()
  {
    return unitPrice == null ? Double.valueOf(0.0D) : unitPrice;
  }
  



  public void setUnitPrice(Double unitPrice)
  {
    this.unitPrice = unitPrice;
  }
  




  public Double getTaxRate()
  {
    return taxRate == null ? Double.valueOf(0.0D) : taxRate;
  }
  



  public void setTaxRate(Double taxRate)
  {
    this.taxRate = taxRate;
  }
  




  public Double getSubtotalAmount()
  {
    return subtotalAmount == null ? Double.valueOf(0.0D) : subtotalAmount;
  }
  



  public void setSubtotalAmount(Double subtotalAmount)
  {
    this.subtotalAmount = subtotalAmount;
  }
  




  public Double getDiscountAmount()
  {
    return discountAmount == null ? Double.valueOf(0.0D) : discountAmount;
  }
  



  public void setDiscountAmount(Double discountAmount)
  {
    this.discountAmount = discountAmount;
  }
  




  public Double getTaxAmount()
  {
    return taxAmount == null ? Double.valueOf(0.0D) : taxAmount;
  }
  



  public void setTaxAmount(Double taxAmount)
  {
    this.taxAmount = taxAmount;
  }
  




  public Double getTotalAmount()
  {
    return totalAmount == null ? Double.valueOf(0.0D) : totalAmount;
  }
  



  public void setTotalAmount(Double totalAmount)
  {
    this.totalAmount = totalAmount;
  }
  




  public Boolean isBeverage()
  {
    return beverage == null ? Boolean.FALSE : beverage;
  }
  



  public void setBeverage(Boolean beverage)
  {
    this.beverage = beverage;
  }
  




  public String getStatus()
  {
    return status;
  }
  



  public void setStatus(String status)
  {
    this.status = status;
  }
  




  public PurchaseOrder getPurchaseOrder()
  {
    return purchaseOrder;
  }
  



  public void setPurchaseOrder(PurchaseOrder purchaseOrder)
  {
    this.purchaseOrder = purchaseOrder;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof PurchaseOrderItem)) { return false;
    }
    PurchaseOrderItem purchaseOrderItem = (PurchaseOrderItem)obj;
    if ((null == getId()) || (null == purchaseOrderItem.getId())) return this == obj;
    return getId().equals(purchaseOrderItem.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
