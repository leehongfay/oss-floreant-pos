package com.floreantpos.model.base;

import com.floreantpos.model.VirtualPrinter;
import java.io.Serializable;
import java.util.List;









public abstract class BaseVirtualPrinter
  implements Comparable, Serializable
{
  public static String REF = "VirtualPrinter";
  public static String PROP_NAME = "name";
  public static String PROP_ENABLED = "enabled";
  public static String PROP_TYPE = "type";
  public static String PROP_ID = "id";
  public static String PROP_PRIORITY = "priority";
  

  public BaseVirtualPrinter()
  {
    initialize();
  }
  


  public BaseVirtualPrinter(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseVirtualPrinter(String id, String name)
  {
    setId(id);
    setName(name);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String name;
  
  protected Integer type;
  
  protected Integer priority;
  
  protected Boolean enabled;
  
  private List<String> orderTypeNames;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Integer getType()
  {
    return type == null ? Integer.valueOf(0) : type;
  }
  



  public void setType(Integer type)
  {
    this.type = type;
  }
  




  public Integer getPriority()
  {
    return priority == null ? Integer.valueOf(0) : priority;
  }
  



  public void setPriority(Integer priority)
  {
    this.priority = priority;
  }
  




  public Boolean isEnabled()
  {
    return enabled == null ? Boolean.FALSE : enabled;
  }
  



  public void setEnabled(Boolean enabled)
  {
    this.enabled = enabled;
  }
  




  public List<String> getOrderTypeNames()
  {
    return orderTypeNames;
  }
  



  public void setOrderTypeNames(List<String> orderTypeNames)
  {
    this.orderTypeNames = orderTypeNames;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof VirtualPrinter)) { return false;
    }
    VirtualPrinter virtualPrinter = (VirtualPrinter)obj;
    if ((null == getId()) || (null == virtualPrinter.getId())) return this == obj;
    return getId().equals(virtualPrinter.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
