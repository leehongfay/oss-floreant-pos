package com.floreantpos.model.base;

import com.floreantpos.model.InventoryMetaCode;
import java.io.Serializable;










public abstract class BaseInventoryMetaCode
  implements Comparable, Serializable
{
  public static String REF = "InventoryMetaCode";
  public static String PROP_DESCRIPTION = "description";
  public static String PROP_CODE_NO = "codeNo";
  public static String PROP_TYPE = "type";
  public static String PROP_CODE_TEXT = "codeText";
  public static String PROP_ID = "id";
  

  public BaseInventoryMetaCode()
  {
    initialize();
  }
  


  public BaseInventoryMetaCode(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  
  protected String type;
  
  protected String codeText;
  
  protected Integer codeNo;
  
  protected String description;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getType()
  {
    return type;
  }
  



  public void setType(String type)
  {
    this.type = type;
  }
  




  public String getCodeText()
  {
    return codeText;
  }
  



  public void setCodeText(String codeText)
  {
    this.codeText = codeText;
  }
  




  public Integer getCodeNo()
  {
    return codeNo == null ? Integer.valueOf(0) : codeNo;
  }
  



  public void setCodeNo(Integer codeNo)
  {
    this.codeNo = codeNo;
  }
  




  public String getDescription()
  {
    return description;
  }
  



  public void setDescription(String description)
  {
    this.description = description;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof InventoryMetaCode)) { return false;
    }
    InventoryMetaCode inventoryMetaCode = (InventoryMetaCode)obj;
    if ((null == getId()) || (null == inventoryMetaCode.getId())) return this == obj;
    return getId().equals(inventoryMetaCode.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
