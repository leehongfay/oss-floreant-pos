package com.floreantpos.model.base;

import com.floreantpos.model.InventoryClosingBalance;
import java.io.Serializable;
import java.util.Date;









public abstract class BaseInventoryClosingBalance
  implements Comparable, Serializable
{
  public static String REF = "InventoryClosingBalance";
  public static String PROP_MENU_ITEM_ID = "menuItemId";
  public static String PROP_LOCATION_ID = "locationId";
  public static String PROP_CLOSING_DATE = "closingDate";
  public static String PROP_ID = "id";
  public static String PROP_UNIT = "unit";
  public static String PROP_BALANCE = "balance";
  

  public BaseInventoryClosingBalance()
  {
    initialize();
  }
  


  public BaseInventoryClosingBalance(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  
  long version;
  
  protected String menuItemId;
  
  protected Date closingDate;
  
  protected String unit;
  
  protected String locationId;
  
  protected Double balance;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getMenuItemId()
  {
    return menuItemId;
  }
  



  public void setMenuItemId(String menuItemId)
  {
    this.menuItemId = menuItemId;
  }
  




  public Date getClosingDate()
  {
    return closingDate;
  }
  



  public void setClosingDate(Date closingDate)
  {
    this.closingDate = closingDate;
  }
  




  public String getUnit()
  {
    return unit;
  }
  



  public void setUnit(String unit)
  {
    this.unit = unit;
  }
  




  public String getLocationId()
  {
    return locationId;
  }
  



  public void setLocationId(String locationId)
  {
    this.locationId = locationId;
  }
  




  public Double getBalance()
  {
    return balance == null ? Double.valueOf(0.0D) : balance;
  }
  



  public void setBalance(Double balance)
  {
    this.balance = balance;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof InventoryClosingBalance)) { return false;
    }
    InventoryClosingBalance inventoryClosingBalance = (InventoryClosingBalance)obj;
    if ((null == getId()) || (null == inventoryClosingBalance.getId())) return this == obj;
    return getId().equals(inventoryClosingBalance.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
