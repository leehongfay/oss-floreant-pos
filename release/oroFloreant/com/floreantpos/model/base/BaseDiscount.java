package com.floreantpos.model.base;

import com.floreantpos.model.Discount;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;




public abstract class BaseDiscount
  implements Comparable, Serializable
{
  public static String REF = "Discount";
  public static String PROP_APPLY_TO_WEDNESDAY_ONLY = "applyToWednesdayOnly";
  public static String PROP_APPLY_TO_FRIDAY_ONLY = "applyToFridayOnly";
  public static String PROP_BARCODE = "barcode";
  public static String PROP_ENABLED = "enabled";
  public static String PROP_VALUE = "value";
  public static String PROP_TYPE = "type";
  public static String PROP_MINIMUM_BUY = "minimumBuy";
  public static String PROP_APPLY_TO_SUNDAY_ONLY = "applyToSundayOnly";
  public static String PROP_AUTO_APPLY = "autoApply";
  public static String PROP_NAME = "name";
  public static String PROP_MODIFIABLE = "modifiable";
  public static String PROP_EXPIRY_DATE = "expiryDate";
  public static String PROP_NEVER_EXPIRE = "neverExpire";
  public static String PROP_START_DATE = "startDate";
  public static String PROP_APPLY_TO_MONDAY_ONLY = "applyToMondayOnly";
  public static String PROP_MAXIMUM_OFF = "maximumOff";
  public static String PROP_APPLY_TO_ALL = "applyToAll";
  public static String PROP_ID = "id";
  public static String PROP_QUALIFICATION_TYPE = "qualificationType";
  public static String PROP_APPLY_TO_THURSDAY_ONLY = "applyToThursdayOnly";
  public static String PROP_APPLY_TO_TUESDAY_ONLY = "applyToTuesdayOnly";
  public static String PROP_APPLY_TO_SATURDAY_ONLY = "applyToSaturdayOnly";
  

  public BaseDiscount()
  {
    initialize();
  }
  


  public BaseDiscount(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  private long version;
  
  protected String name;
  
  protected Integer type;
  
  protected String barcode;
  
  protected Integer qualificationType;
  
  protected Boolean applyToAll;
  
  protected Double minimumBuy;
  
  protected Double maximumOff;
  
  protected Double value;
  
  protected Date expiryDate;
  
  protected Boolean enabled;
  protected Boolean autoApply;
  protected Boolean modifiable;
  protected Boolean neverExpire;
  protected Date startDate;
  protected Boolean applyToSundayOnly;
  protected Boolean applyToMondayOnly;
  protected Boolean applyToTuesdayOnly;
  protected Boolean applyToWednesdayOnly;
  protected Boolean applyToThursdayOnly;
  protected Boolean applyToFridayOnly;
  protected Boolean applyToSaturdayOnly;
  private List<MenuItem> menuItems;
  private List<MenuGroup> menuGroups;
  private List<MenuCategory> menuCategories;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Integer getType()
  {
    return type == null ? Integer.valueOf(0) : type;
  }
  



  public void setType(Integer type)
  {
    this.type = type;
  }
  




  public String getBarcode()
  {
    return barcode;
  }
  



  public void setBarcode(String barcode)
  {
    this.barcode = barcode;
  }
  




  public Integer getQualificationType()
  {
    return qualificationType == null ? Integer.valueOf(0) : qualificationType;
  }
  



  public void setQualificationType(Integer qualificationType)
  {
    this.qualificationType = qualificationType;
  }
  




  public Boolean isApplyToAll()
  {
    return applyToAll == null ? Boolean.FALSE : applyToAll;
  }
  



  public void setApplyToAll(Boolean applyToAll)
  {
    this.applyToAll = applyToAll;
  }
  




  public Double getMinimumBuy()
  {
    return minimumBuy == null ? Double.valueOf(0.0D) : minimumBuy;
  }
  



  public void setMinimumBuy(Double minimumBuy)
  {
    this.minimumBuy = minimumBuy;
  }
  




  public Double getMaximumOff()
  {
    return maximumOff == null ? Double.valueOf(0.0D) : maximumOff;
  }
  



  public void setMaximumOff(Double maximumOff)
  {
    this.maximumOff = maximumOff;
  }
  




  public Double getValue()
  {
    return value == null ? Double.valueOf(0.0D) : value;
  }
  



  public void setValue(Double value)
  {
    this.value = value;
  }
  




  public Date getExpiryDate()
  {
    return expiryDate;
  }
  



  public void setExpiryDate(Date expiryDate)
  {
    this.expiryDate = expiryDate;
  }
  




  public Boolean isEnabled()
  {
    return enabled == null ? Boolean.FALSE : enabled;
  }
  



  public void setEnabled(Boolean enabled)
  {
    this.enabled = enabled;
  }
  




  public Boolean isAutoApply()
  {
    return autoApply == null ? Boolean.FALSE : autoApply;
  }
  



  public void setAutoApply(Boolean autoApply)
  {
    this.autoApply = autoApply;
  }
  




  public Boolean isModifiable()
  {
    return modifiable == null ? Boolean.FALSE : modifiable;
  }
  



  public void setModifiable(Boolean modifiable)
  {
    this.modifiable = modifiable;
  }
  




  public Boolean isNeverExpire()
  {
    return neverExpire == null ? Boolean.FALSE : neverExpire;
  }
  



  public void setNeverExpire(Boolean neverExpire)
  {
    this.neverExpire = neverExpire;
  }
  




  public Date getStartDate()
  {
    return startDate;
  }
  



  public void setStartDate(Date startDate)
  {
    this.startDate = startDate;
  }
  




  public Boolean isApplyToSundayOnly()
  {
    return applyToSundayOnly == null ? Boolean.FALSE : applyToSundayOnly;
  }
  



  public void setApplyToSundayOnly(Boolean applyToSundayOnly)
  {
    this.applyToSundayOnly = applyToSundayOnly;
  }
  




  public Boolean isApplyToMondayOnly()
  {
    return applyToMondayOnly == null ? Boolean.FALSE : applyToMondayOnly;
  }
  



  public void setApplyToMondayOnly(Boolean applyToMondayOnly)
  {
    this.applyToMondayOnly = applyToMondayOnly;
  }
  




  public Boolean isApplyToTuesdayOnly()
  {
    return applyToTuesdayOnly == null ? Boolean.FALSE : applyToTuesdayOnly;
  }
  



  public void setApplyToTuesdayOnly(Boolean applyToTuesdayOnly)
  {
    this.applyToTuesdayOnly = applyToTuesdayOnly;
  }
  




  public Boolean isApplyToWednesdayOnly()
  {
    return applyToWednesdayOnly == null ? Boolean.FALSE : applyToWednesdayOnly;
  }
  



  public void setApplyToWednesdayOnly(Boolean applyToWednesdayOnly)
  {
    this.applyToWednesdayOnly = applyToWednesdayOnly;
  }
  




  public Boolean isApplyToThursdayOnly()
  {
    return applyToThursdayOnly == null ? Boolean.FALSE : applyToThursdayOnly;
  }
  



  public void setApplyToThursdayOnly(Boolean applyToThursdayOnly)
  {
    this.applyToThursdayOnly = applyToThursdayOnly;
  }
  




  public Boolean isApplyToFridayOnly()
  {
    return applyToFridayOnly == null ? Boolean.FALSE : applyToFridayOnly;
  }
  



  public void setApplyToFridayOnly(Boolean applyToFridayOnly)
  {
    this.applyToFridayOnly = applyToFridayOnly;
  }
  




  public Boolean isApplyToSaturdayOnly()
  {
    return applyToSaturdayOnly == null ? Boolean.FALSE : applyToSaturdayOnly;
  }
  



  public void setApplyToSaturdayOnly(Boolean applyToSaturdayOnly)
  {
    this.applyToSaturdayOnly = applyToSaturdayOnly;
  }
  




  public List<MenuItem> getMenuItems()
  {
    return menuItems;
  }
  



  public void setMenuItems(List<MenuItem> menuItems)
  {
    this.menuItems = menuItems;
  }
  
  public void addTomenuItems(MenuItem menuItem) {
    if (null == getMenuItems()) setMenuItems(new ArrayList());
    getMenuItems().add(menuItem);
  }
  




  public List<MenuGroup> getMenuGroups()
  {
    return menuGroups;
  }
  



  public void setMenuGroups(List<MenuGroup> menuGroups)
  {
    this.menuGroups = menuGroups;
  }
  
  public void addTomenuGroups(MenuGroup menuGroup) {
    if (null == getMenuGroups()) setMenuGroups(new ArrayList());
    getMenuGroups().add(menuGroup);
  }
  




  public List<MenuCategory> getMenuCategories()
  {
    return menuCategories;
  }
  



  public void setMenuCategories(List<MenuCategory> menuCategories)
  {
    this.menuCategories = menuCategories;
  }
  
  public void addTomenuCategories(MenuCategory menuCategory) {
    if (null == getMenuCategories()) setMenuCategories(new ArrayList());
    getMenuCategories().add(menuCategory);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof Discount)) { return false;
    }
    Discount discount = (Discount)obj;
    if ((null == getId()) || (null == discount.getId())) return this == obj;
    return getId().equals(discount.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
