package com.floreantpos.model.base;

import java.io.Serializable;











public abstract class BaseTicketItemCookingInstruction
  implements Comparable, Serializable
{
  public static String REF = "TicketItemCookingInstruction";
  public static String PROP_PRINTED_TO_KITCHEN = "printedToKitchen";
  public static String PROP_DESCRIPTION = "description";
  public static String PROP_SAVED = "saved";
  protected String description;
  protected Boolean printedToKitchen;
  protected Boolean saved;
  
  public BaseTicketItemCookingInstruction() { initialize(); }
  








  protected void initialize() {}
  







  public String getDescription()
  {
    return description;
  }
  



  public void setDescription(String description)
  {
    this.description = description;
  }
  




  public Boolean isPrintedToKitchen()
  {
    return printedToKitchen == null ? Boolean.FALSE : printedToKitchen;
  }
  



  public void setPrintedToKitchen(Boolean printedToKitchen)
  {
    this.printedToKitchen = printedToKitchen;
  }
  




  public boolean isSaved()
  {
    return saved == null ? false : saved.booleanValue();
  }
  



  public void setSaved(Boolean saved)
  {
    this.saved = saved;
  }
  





  public int compareTo(Object obj)
  {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
