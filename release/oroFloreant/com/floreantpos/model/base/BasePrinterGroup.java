package com.floreantpos.model.base;

import com.floreantpos.model.PrinterGroup;
import java.io.Serializable;
import java.util.List;









public abstract class BasePrinterGroup
  implements Comparable, Serializable
{
  public static String REF = "PrinterGroup";
  public static String PROP_NAME = "name";
  public static String PROP_IS_DEFAULT = "isDefault";
  public static String PROP_ID = "id";
  

  public BasePrinterGroup()
  {
    initialize();
  }
  


  public BasePrinterGroup(String id)
  {
    setId(id);
    initialize();
  }
  





  public BasePrinterGroup(String id, String name)
  {
    setId(id);
    setName(name);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String name;
  

  protected boolean isDefault;
  

  private List<String> printerNames;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public boolean isIsDefault()
  {
    return isDefault;
  }
  



  public void setIsDefault(boolean isDefault)
  {
    this.isDefault = isDefault;
  }
  




  public List<String> getPrinterNames()
  {
    return printerNames;
  }
  



  public void setPrinterNames(List<String> printerNames)
  {
    this.printerNames = printerNames;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof PrinterGroup)) { return false;
    }
    PrinterGroup printerGroup = (PrinterGroup)obj;
    if ((null == getId()) || (null == printerGroup.getId())) return this == obj;
    return getId().equals(printerGroup.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
