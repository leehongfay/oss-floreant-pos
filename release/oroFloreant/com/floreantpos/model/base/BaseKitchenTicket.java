package com.floreantpos.model.base;

import com.floreantpos.model.KitchenTicket;
import com.floreantpos.model.KitchenTicketItem;
import com.floreantpos.model.PrinterGroup;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;





public abstract class BaseKitchenTicket
  implements Comparable, Serializable
{
  public static String REF = "KitchenTicket";
  public static String PROP_STATUS = "status";
  public static String PROP_PRINTER_GROUP = "printerGroup";
  public static String PROP_CREATE_DATE = "createDate";
  public static String PROP_TOKEN_NO = "tokenNo";
  public static String PROP_TICKET_ID = "ticketId";
  public static String PROP_PRINTER_NAME = "printerName";
  public static String PROP_SERVER_NAME = "serverName";
  public static String PROP_ORDER_TYPE_ID = "orderTypeId";
  public static String PROP_SEQUENCE_NUMBER = "sequenceNumber";
  public static String PROP_CLOSING_DATE = "closingDate";
  public static String PROP_SHOW_ON_KIT_DISPLAY = "showOnKitDisplay";
  public static String PROP_ID = "id";
  public static String PROP_VOIDED = "voided";
  public static String PROP_LAST_UPDATE_DATE = "lastUpdateDate";
  

  public BaseKitchenTicket()
  {
    initialize();
  }
  


  public BaseKitchenTicket(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected String ticketId;
  
  protected Integer tokenNo;
  
  protected Date createDate;
  
  protected Date lastUpdateDate;
  
  protected Date closingDate;
  
  protected Boolean voided;
  
  protected Integer sequenceNumber;
  
  protected String status;
  
  protected String serverName;
  
  protected String orderTypeId;
  
  protected String printerName;
  
  protected Boolean showOnKitDisplay;
  private PrinterGroup printerGroup;
  private List<Integer> tableNumbers;
  private List<KitchenTicketItem> ticketItems;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getTicketId()
  {
    return ticketId;
  }
  



  public void setTicketId(String ticketId)
  {
    this.ticketId = ticketId;
  }
  




  public Integer getTokenNo()
  {
    return tokenNo == null ? Integer.valueOf(0) : tokenNo;
  }
  



  public void setTokenNo(Integer tokenNo)
  {
    this.tokenNo = tokenNo;
  }
  




  public Date getCreateDate()
  {
    return createDate;
  }
  



  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  




  public Date getLastUpdateDate()
  {
    return lastUpdateDate;
  }
  



  public void setLastUpdateDate(Date lastUpdateDate)
  {
    this.lastUpdateDate = lastUpdateDate;
  }
  




  public Date getClosingDate()
  {
    return closingDate;
  }
  



  public void setClosingDate(Date closingDate)
  {
    this.closingDate = closingDate;
  }
  




  public Boolean isVoided()
  {
    return voided == null ? Boolean.FALSE : voided;
  }
  



  public void setVoided(Boolean voided)
  {
    this.voided = voided;
  }
  




  public Integer getSequenceNumber()
  {
    return sequenceNumber == null ? Integer.valueOf(0) : sequenceNumber;
  }
  



  public void setSequenceNumber(Integer sequenceNumber)
  {
    this.sequenceNumber = sequenceNumber;
  }
  




  public String getStatus()
  {
    return status;
  }
  



  public void setStatus(String status)
  {
    this.status = status;
  }
  




  public String getServerName()
  {
    return serverName;
  }
  



  public void setServerName(String serverName)
  {
    this.serverName = serverName;
  }
  




  public String getOrderTypeId()
  {
    return orderTypeId;
  }
  



  public void setOrderTypeId(String orderTypeId)
  {
    this.orderTypeId = orderTypeId;
  }
  




  public String getPrinterName()
  {
    return printerName;
  }
  



  public void setPrinterName(String printerName)
  {
    this.printerName = printerName;
  }
  




  public Boolean isShowOnKitDisplay()
  {
    return showOnKitDisplay == null ? Boolean.valueOf(true) : showOnKitDisplay;
  }
  



  public void setShowOnKitDisplay(Boolean showOnKitDisplay)
  {
    this.showOnKitDisplay = showOnKitDisplay;
  }
  



  public static String getShowOnKitDisplayDefaultValue()
  {
    return "true";
  }
  



  public PrinterGroup getPrinterGroup()
  {
    return printerGroup;
  }
  



  public void setPrinterGroup(PrinterGroup printerGroup)
  {
    this.printerGroup = printerGroup;
  }
  




  public List<Integer> getTableNumbers()
  {
    return tableNumbers;
  }
  



  public void setTableNumbers(List<Integer> tableNumbers)
  {
    this.tableNumbers = tableNumbers;
  }
  




  public List<KitchenTicketItem> getTicketItems()
  {
    return ticketItems;
  }
  



  public void setTicketItems(List<KitchenTicketItem> ticketItems)
  {
    this.ticketItems = ticketItems;
  }
  
  public void addToticketItems(KitchenTicketItem kitchenTicketItem) {
    if (null == getTicketItems()) setTicketItems(new ArrayList());
    getTicketItems().add(kitchenTicketItem);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof KitchenTicket)) { return false;
    }
    KitchenTicket kitchenTicket = (KitchenTicket)obj;
    if ((null == getId()) || (null == kitchenTicket.getId())) return this == obj;
    return getId().equals(kitchenTicket.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
