package com.floreantpos.model.base;

import com.floreantpos.model.PriceTable;
import java.io.Serializable;
import java.util.Date;









public abstract class BasePriceTable
  implements Comparable, Serializable
{
  public static String REF = "PriceTable";
  public static String PROP_LAST_UPDATED_BY = "lastUpdatedBy";
  public static String PROP_DESCRIPTION = "description";
  public static String PROP_LAST_UPDATED_TIME = "lastUpdatedTime";
  public static String PROP_ID = "id";
  public static String PROP_NAME = "name";
  

  public BasePriceTable()
  {
    initialize();
  }
  


  public BasePriceTable(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  long version;
  
  protected String name;
  
  protected String description;
  
  protected Date lastUpdatedTime;
  
  protected String lastUpdatedBy;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getDescription()
  {
    return description;
  }
  



  public void setDescription(String description)
  {
    this.description = description;
  }
  




  public Date getLastUpdatedTime()
  {
    return lastUpdatedTime;
  }
  



  public void setLastUpdatedTime(Date lastUpdatedTime)
  {
    this.lastUpdatedTime = lastUpdatedTime;
  }
  




  public String getLastUpdatedBy()
  {
    return lastUpdatedBy;
  }
  



  public void setLastUpdatedBy(String lastUpdatedBy)
  {
    this.lastUpdatedBy = lastUpdatedBy;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof PriceTable)) { return false;
    }
    PriceTable priceTable = (PriceTable)obj;
    if ((null == getId()) || (null == priceTable.getId())) return this == obj;
    return getId().equals(priceTable.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
