package com.floreantpos.model.base;

import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.Gratuity;
import com.floreantpos.model.GratuityPaymentHistory;
import com.floreantpos.model.User;
import java.io.Serializable;
import java.util.Date;






public abstract class BaseGratuityPaymentHistory
  implements Comparable, Serializable
{
  public static String REF = "GratuityPaymentHistory";
  public static String PROP_AMOUNT = "amount";
  public static String PROP_GRATUITY = "gratuity";
  public static String PROP_PAID_BY = "paidBy";
  public static String PROP_ID = "id";
  public static String PROP_CASH_DRAWER = "cashDrawer";
  public static String PROP_TRANSACTION_TIME = "transactionTime";
  public static String PROP_TENDERED_AMOUNT = "tenderedAmount";
  

  public BaseGratuityPaymentHistory()
  {
    initialize();
  }
  


  public BaseGratuityPaymentHistory(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseGratuityPaymentHistory(String id, CashDrawer cashDrawer)
  {
    setId(id);
    setCashDrawer(cashDrawer);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  long version;
  
  protected Date transactionTime;
  
  protected Double amount;
  
  protected Double tenderedAmount;
  
  private Gratuity gratuity;
  
  private CashDrawer cashDrawer;
  
  private User paidBy;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Date getTransactionTime()
  {
    return transactionTime;
  }
  



  public void setTransactionTime(Date transactionTime)
  {
    this.transactionTime = transactionTime;
  }
  




  public Double getAmount()
  {
    return amount == null ? Double.valueOf(0.0D) : amount;
  }
  



  public void setAmount(Double amount)
  {
    this.amount = amount;
  }
  




  public Double getTenderedAmount()
  {
    return tenderedAmount == null ? Double.valueOf(0.0D) : tenderedAmount;
  }
  



  public void setTenderedAmount(Double tenderedAmount)
  {
    this.tenderedAmount = tenderedAmount;
  }
  




  public Gratuity getGratuity()
  {
    return gratuity;
  }
  



  public void setGratuity(Gratuity gratuity)
  {
    this.gratuity = gratuity;
  }
  




  public CashDrawer getCashDrawer()
  {
    return cashDrawer;
  }
  



  public void setCashDrawer(CashDrawer cashDrawer)
  {
    this.cashDrawer = cashDrawer;
  }
  




  public User getPaidBy()
  {
    return paidBy;
  }
  



  public void setPaidBy(User paidBy)
  {
    this.paidBy = paidBy;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof GratuityPaymentHistory)) { return false;
    }
    GratuityPaymentHistory gratuityPaymentHistory = (GratuityPaymentHistory)obj;
    if ((null == getId()) || (null == gratuityPaymentHistory.getId())) return this == obj;
    return getId().equals(gratuityPaymentHistory.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
