package com.floreantpos.model.base;

import com.floreantpos.model.PrinterGroup;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.User;
import com.floreantpos.model.VoidItem;
import java.io.Serializable;
import java.util.Date;






public abstract class BaseVoidItem
  implements Comparable, Serializable
{
  public static String REF = "VoidItem";
  public static String PROP_VOID_BY_USER = "voidByUser";
  public static String PROP_PRINTER_GROUP = "printerGroup";
  public static String PROP_VOID_DATE = "voidDate";
  public static String PROP_QUANTITY = "quantity";
  public static String PROP_ITEM_WASTED = "itemWasted";
  public static String PROP_TERMINAL = "terminal";
  public static String PROP_UNIT_PRICE = "unitPrice";
  public static String PROP_TICKET_ID = "ticketId";
  public static String PROP_VOID_REASON = "voidReason";
  public static String PROP_CASH_DRAWER_ID = "cashDrawerId";
  public static String PROP_TOTAL_PRICE = "totalPrice";
  public static String PROP_MENU_ITEM_NAME = "menuItemName";
  public static String PROP_MENU_ITEM_ID = "menuItemId";
  public static String PROP_ID = "id";
  public static String PROP_MODIFIER = "modifier";
  public static String PROP_MODIFIER_ID = "modifierId";
  

  public BaseVoidItem()
  {
    initialize();
  }
  


  public BaseVoidItem(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected String menuItemName;
  
  protected String menuItemId;
  
  protected String modifierId;
  
  protected Double quantity;
  
  protected String voidReason;
  
  protected Date voidDate;
  
  protected Double unitPrice;
  
  protected Double totalPrice;
  
  protected Boolean itemWasted;
  
  protected Boolean modifier;
  protected String ticketId;
  protected String cashDrawerId;
  private User voidByUser;
  private Terminal terminal;
  private PrinterGroup printerGroup;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getMenuItemName()
  {
    return menuItemName;
  }
  



  public void setMenuItemName(String menuItemName)
  {
    this.menuItemName = menuItemName;
  }
  




  public String getMenuItemId()
  {
    return menuItemId;
  }
  



  public void setMenuItemId(String menuItemId)
  {
    this.menuItemId = menuItemId;
  }
  




  public String getModifierId()
  {
    return modifierId;
  }
  



  public void setModifierId(String modifierId)
  {
    this.modifierId = modifierId;
  }
  




  public Double getQuantity()
  {
    return quantity == null ? Double.valueOf(0.0D) : quantity;
  }
  



  public void setQuantity(Double quantity)
  {
    this.quantity = quantity;
  }
  




  public String getVoidReason()
  {
    return voidReason;
  }
  



  public void setVoidReason(String voidReason)
  {
    this.voidReason = voidReason;
  }
  




  public Date getVoidDate()
  {
    return voidDate;
  }
  



  public void setVoidDate(Date voidDate)
  {
    this.voidDate = voidDate;
  }
  




  public Double getUnitPrice()
  {
    return unitPrice == null ? Double.valueOf(0.0D) : unitPrice;
  }
  



  public void setUnitPrice(Double unitPrice)
  {
    this.unitPrice = unitPrice;
  }
  




  public Double getTotalPrice()
  {
    return totalPrice == null ? Double.valueOf(0.0D) : totalPrice;
  }
  



  public void setTotalPrice(Double totalPrice)
  {
    this.totalPrice = totalPrice;
  }
  




  public Boolean isItemWasted()
  {
    return itemWasted == null ? Boolean.FALSE : itemWasted;
  }
  



  public void setItemWasted(Boolean itemWasted)
  {
    this.itemWasted = itemWasted;
  }
  




  public Boolean isModifier()
  {
    return modifier == null ? Boolean.FALSE : modifier;
  }
  



  public void setModifier(Boolean modifier)
  {
    this.modifier = modifier;
  }
  




  public String getTicketId()
  {
    return ticketId;
  }
  



  public void setTicketId(String ticketId)
  {
    this.ticketId = ticketId;
  }
  




  public String getCashDrawerId()
  {
    return cashDrawerId;
  }
  



  public void setCashDrawerId(String cashDrawerId)
  {
    this.cashDrawerId = cashDrawerId;
  }
  




  public User getVoidByUser()
  {
    return voidByUser;
  }
  



  public void setVoidByUser(User voidByUser)
  {
    this.voidByUser = voidByUser;
  }
  




  public Terminal getTerminal()
  {
    return terminal;
  }
  



  public void setTerminal(Terminal terminal)
  {
    this.terminal = terminal;
  }
  




  public PrinterGroup getPrinterGroup()
  {
    return printerGroup;
  }
  



  public void setPrinterGroup(PrinterGroup printerGroup)
  {
    this.printerGroup = printerGroup;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof VoidItem)) { return false;
    }
    VoidItem voidItem = (VoidItem)obj;
    if ((null == getId()) || (null == voidItem.getId())) return this == obj;
    return getId().equals(voidItem.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
