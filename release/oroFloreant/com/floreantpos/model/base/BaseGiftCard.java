package com.floreantpos.model.base;

import com.floreantpos.model.GiftCard;
import java.io.Serializable;
import java.util.Date;









public abstract class BaseGiftCard
  implements Comparable, Serializable
{
  public static String REF = "GiftCard";
  public static String PROP_DURATION_TYPE = "durationType";
  public static String PROP_CARD_NUMBER = "cardNumber";
  public static String PROP_OWNER_NAME = "ownerName";
  public static String PROP_EXPIRY_DATE = "expiryDate";
  public static String PROP_EMAIL = "email";
  public static String PROP_ISSUE_DATE = "issueDate";
  public static String PROP_BATCH_NO = "batchNo";
  public static String PROP_DE_ACTIVATION_DATE = "deActivationDate";
  public static String PROP_DURATION = "duration";
  public static String PROP_POINT = "point";
  public static String PROP_ACTIVE = "active";
  public static String PROP_TYPE = "type";
  public static String PROP_PIN_NUMBER = "pinNumber";
  public static String PROP_ACTIVATION_DATE = "activationDate";
  public static String PROP_BALANCE = "balance";
  public static String PROP_DISABLE = "disable";
  

  public BaseGiftCard()
  {
    initialize();
  }
  


  public BaseGiftCard(String cardNumber)
  {
    setCardNumber(cardNumber);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String cardNumber;
  
  long version;
  
  protected String ownerName;
  
  protected Double balance;
  
  protected Date issueDate;
  
  protected Date activationDate;
  
  protected Date deActivationDate;
  
  protected Date expiryDate;
  
  protected Boolean active;
  
  protected Boolean disable;
  protected String durationType;
  protected Integer duration;
  protected String pinNumber;
  protected Integer point;
  protected String batchNo;
  protected String email;
  protected String type;
  
  protected void initialize() {}
  
  public String getCardNumber()
  {
    return cardNumber;
  }
  



  public void setCardNumber(String cardNumber)
  {
    this.cardNumber = cardNumber;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getOwnerName()
  {
    return ownerName;
  }
  



  public void setOwnerName(String ownerName)
  {
    this.ownerName = ownerName;
  }
  




  public Double getBalance()
  {
    return balance == null ? Double.valueOf(0.0D) : balance;
  }
  



  public void setBalance(Double balance)
  {
    this.balance = balance;
  }
  




  public Date getIssueDate()
  {
    return issueDate;
  }
  



  public void setIssueDate(Date issueDate)
  {
    this.issueDate = issueDate;
  }
  




  public Date getActivationDate()
  {
    return activationDate;
  }
  



  public void setActivationDate(Date activationDate)
  {
    this.activationDate = activationDate;
  }
  




  public Date getDeActivationDate()
  {
    return deActivationDate;
  }
  



  public void setDeActivationDate(Date deActivationDate)
  {
    this.deActivationDate = deActivationDate;
  }
  




  public Date getExpiryDate()
  {
    return expiryDate;
  }
  



  public void setExpiryDate(Date expiryDate)
  {
    this.expiryDate = expiryDate;
  }
  




  public Boolean isActive()
  {
    return active == null ? Boolean.FALSE : active;
  }
  



  public void setActive(Boolean active)
  {
    this.active = active;
  }
  




  public Boolean isDisable()
  {
    return disable == null ? Boolean.FALSE : disable;
  }
  



  public void setDisable(Boolean disable)
  {
    this.disable = disable;
  }
  




  public String getDurationType()
  {
    return durationType;
  }
  



  public void setDurationType(String durationType)
  {
    this.durationType = durationType;
  }
  




  public Integer getDuration()
  {
    return duration == null ? Integer.valueOf(0) : duration;
  }
  



  public void setDuration(Integer duration)
  {
    this.duration = duration;
  }
  




  public String getPinNumber()
  {
    return pinNumber;
  }
  



  public void setPinNumber(String pinNumber)
  {
    this.pinNumber = pinNumber;
  }
  




  public Integer getPoint()
  {
    return point == null ? Integer.valueOf(0) : point;
  }
  



  public void setPoint(Integer point)
  {
    this.point = point;
  }
  




  public String getBatchNo()
  {
    return batchNo;
  }
  



  public void setBatchNo(String batchNo)
  {
    this.batchNo = batchNo;
  }
  




  public String getEmail()
  {
    return email;
  }
  



  public void setEmail(String email)
  {
    this.email = email;
  }
  




  public String getType()
  {
    return type;
  }
  



  public void setType(String type)
  {
    this.type = type;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof GiftCard)) { return false;
    }
    GiftCard giftCard = (GiftCard)obj;
    if ((null == getCardNumber()) || (null == giftCard.getCardNumber())) return this == obj;
    return getCardNumber().equals(giftCard.getCardNumber());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getCardNumber()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getCardNumber().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
