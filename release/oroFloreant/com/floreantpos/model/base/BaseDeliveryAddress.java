package com.floreantpos.model.base;

import com.floreantpos.model.DeliveryAddress;
import java.io.Serializable;










public abstract class BaseDeliveryAddress
  implements Comparable, Serializable
{
  public static String REF = "DeliveryAddress";
  public static String PROP_ROOM_NO = "roomNo";
  public static String PROP_ADDRESS = "address";
  public static String PROP_CUSTOMER_ID = "customerId";
  public static String PROP_ID = "id";
  public static String PROP_DISTANCE = "distance";
  public static String PROP_PHONE_EXTENSION = "phoneExtension";
  

  public BaseDeliveryAddress()
  {
    initialize();
  }
  


  public BaseDeliveryAddress(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  
  long version;
  
  protected String address;
  
  protected String phoneExtension;
  
  protected String roomNo;
  
  protected Double distance;
  
  protected String customerId;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getAddress()
  {
    return address;
  }
  



  public void setAddress(String address)
  {
    this.address = address;
  }
  




  public String getPhoneExtension()
  {
    return phoneExtension;
  }
  



  public void setPhoneExtension(String phoneExtension)
  {
    this.phoneExtension = phoneExtension;
  }
  




  public String getRoomNo()
  {
    return roomNo;
  }
  



  public void setRoomNo(String roomNo)
  {
    this.roomNo = roomNo;
  }
  




  public Double getDistance()
  {
    return distance == null ? Double.valueOf(0.0D) : distance;
  }
  



  public void setDistance(Double distance)
  {
    this.distance = distance;
  }
  




  public String getCustomerId()
  {
    return customerId;
  }
  



  public void setCustomerId(String customerId)
  {
    this.customerId = customerId;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof DeliveryAddress)) { return false;
    }
    DeliveryAddress deliveryAddress = (DeliveryAddress)obj;
    if ((null == getId()) || (null == deliveryAddress.getId())) return this == obj;
    return getId().equals(deliveryAddress.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
