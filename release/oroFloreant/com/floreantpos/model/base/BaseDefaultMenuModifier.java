package com.floreantpos.model.base;

import com.floreantpos.model.MenuModifier;
import com.floreantpos.model.Multiplier;
import java.io.Serializable;









public abstract class BaseDefaultMenuModifier
  implements Comparable, Serializable
{
  public static String REF = "DefaultMenuModifier";
  public static String PROP_MULTIPLIER = "multiplier";
  public static String PROP_QUANTITY = "quantity";
  public static String PROP_MODIFIER = "modifier";
  protected Double quantity;
  private Multiplier multiplier;
  private MenuModifier modifier;
  
  public BaseDefaultMenuModifier() { initialize(); }
  









  protected void initialize() {}
  








  public Double getQuantity()
  {
    return quantity == null ? Double.valueOf(0.0D) : quantity;
  }
  



  public void setQuantity(Double quantity)
  {
    this.quantity = quantity;
  }
  




  public Multiplier getMultiplier()
  {
    return multiplier;
  }
  



  public void setMultiplier(Multiplier multiplier)
  {
    this.multiplier = multiplier;
  }
  




  public MenuModifier getModifier()
  {
    return modifier;
  }
  



  public void setModifier(MenuModifier modifier)
  {
    this.modifier = modifier;
  }
  





  public int compareTo(Object obj)
  {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
