package com.floreantpos.model.base;

import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.TerminalType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;






public abstract class BaseTerminalType
  implements Comparable, Serializable
{
  public static String REF = "TerminalType";
  public static String PROP_NAME = "name";
  public static String PROP_TYPE = "type";
  public static String PROP_ID = "id";
  

  public BaseTerminalType()
  {
    initialize();
  }
  


  public BaseTerminalType(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseTerminalType(String id, String name)
  {
    setId(id);
    setName(name);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String name;
  

  protected Integer type;
  
  private List<OrderType> orderTypes;
  
  private List<MenuCategory> categories;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Integer getType()
  {
    return type == null ? Integer.valueOf(0) : type;
  }
  



  public void setType(Integer type)
  {
    this.type = type;
  }
  




  public List<OrderType> getOrderTypes()
  {
    return orderTypes;
  }
  



  public void setOrderTypes(List<OrderType> orderTypes)
  {
    this.orderTypes = orderTypes;
  }
  
  public void addToorderTypes(OrderType orderType) {
    if (null == getOrderTypes()) setOrderTypes(new ArrayList());
    getOrderTypes().add(orderType);
  }
  




  public List<MenuCategory> getCategories()
  {
    return categories;
  }
  



  public void setCategories(List<MenuCategory> categories)
  {
    this.categories = categories;
  }
  
  public void addTocategories(MenuCategory menuCategory) {
    if (null == getCategories()) setCategories(new ArrayList());
    getCategories().add(menuCategory);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof TerminalType)) { return false;
    }
    TerminalType terminalType = (TerminalType)obj;
    if ((null == getId()) || (null == terminalType.getId())) return this == obj;
    return getId().equals(terminalType.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
