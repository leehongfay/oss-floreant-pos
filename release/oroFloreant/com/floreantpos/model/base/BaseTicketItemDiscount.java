package com.floreantpos.model.base;

import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemDiscount;
import java.io.Serializable;









public abstract class BaseTicketItemDiscount
  implements Comparable, Serializable
{
  public static String REF = "TicketItemDiscount";
  public static String PROP_MINIMUM_AMOUNT = "minimumAmount";
  public static String PROP_NAME = "name";
  public static String PROP_AMOUNT = "amount";
  public static String PROP_VALUE = "value";
  public static String PROP_COUPON_QUANTITY = "couponQuantity";
  public static String PROP_DISCOUNT_ID = "discountId";
  public static String PROP_TYPE = "type";
  public static String PROP_ID = "id";
  public static String PROP_TICKET_ITEM = "ticketItem";
  public static String PROP_AUTO_APPLY = "autoApply";
  

  public BaseTicketItemDiscount()
  {
    initialize();
  }
  


  public BaseTicketItemDiscount(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  private long version;
  
  protected String discountId;
  
  protected String name;
  
  protected Integer type;
  
  protected Boolean autoApply;
  
  protected Double couponQuantity;
  
  protected Double minimumAmount;
  
  protected Double value;
  
  protected Double amount;
  
  private TicketItem ticketItem;
  

  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getDiscountId()
  {
    return discountId;
  }
  



  public void setDiscountId(String discountId)
  {
    this.discountId = discountId;
  }
  




  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Integer getType()
  {
    return type == null ? Integer.valueOf(0) : type;
  }
  



  public void setType(Integer type)
  {
    this.type = type;
  }
  




  public Boolean isAutoApply()
  {
    return autoApply == null ? Boolean.FALSE : autoApply;
  }
  



  public void setAutoApply(Boolean autoApply)
  {
    this.autoApply = autoApply;
  }
  




  public Double getCouponQuantity()
  {
    return couponQuantity == null ? Double.valueOf(0.0D) : couponQuantity;
  }
  



  public void setCouponQuantity(Double couponQuantity)
  {
    this.couponQuantity = couponQuantity;
  }
  




  public Double getMinimumAmount()
  {
    return minimumAmount == null ? Double.valueOf(0.0D) : minimumAmount;
  }
  



  public void setMinimumAmount(Double minimumAmount)
  {
    this.minimumAmount = minimumAmount;
  }
  




  public Double getValue()
  {
    return value == null ? Double.valueOf(0.0D) : value;
  }
  



  public void setValue(Double value)
  {
    this.value = value;
  }
  




  public Double getAmount()
  {
    return amount == null ? Double.valueOf(0.0D) : amount;
  }
  



  public void setAmount(Double amount)
  {
    this.amount = amount;
  }
  




  public TicketItem getTicketItem()
  {
    return ticketItem;
  }
  



  public void setTicketItem(TicketItem ticketItem)
  {
    this.ticketItem = ticketItem;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof TicketItemDiscount)) { return false;
    }
    TicketItemDiscount ticketItemDiscount = (TicketItemDiscount)obj;
    if ((null == getId()) || (null == ticketItemDiscount.getId())) return this == obj;
    return getId().equals(ticketItemDiscount.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
