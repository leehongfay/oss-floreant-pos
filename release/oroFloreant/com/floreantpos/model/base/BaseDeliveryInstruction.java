package com.floreantpos.model.base;

import com.floreantpos.model.Customer;
import com.floreantpos.model.DeliveryInstruction;
import java.io.Serializable;









public abstract class BaseDeliveryInstruction
  implements Comparable, Serializable
{
  public static String REF = "DeliveryInstruction";
  public static String PROP_CUSTOMER = "customer";
  public static String PROP_NOTES = "notes";
  public static String PROP_ID = "id";
  

  public BaseDeliveryInstruction()
  {
    initialize();
  }
  


  public BaseDeliveryInstruction(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String notes;
  

  private Customer customer;
  


  protected void initialize() {}
  


  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getNotes()
  {
    return notes;
  }
  



  public void setNotes(String notes)
  {
    this.notes = notes;
  }
  




  public Customer getCustomer()
  {
    return customer;
  }
  



  public void setCustomer(Customer customer)
  {
    this.customer = customer;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof DeliveryInstruction)) { return false;
    }
    DeliveryInstruction deliveryInstruction = (DeliveryInstruction)obj;
    if ((null == getId()) || (null == deliveryInstruction.getId())) return this == obj;
    return getId().equals(deliveryInstruction.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
