package com.floreantpos.model.base;

import com.floreantpos.model.CustomPayment;
import java.io.Serializable;










public abstract class BaseCustomPayment
  implements Comparable, Serializable
{
  public static String REF = "CustomPayment";
  public static String PROP_NAME = "name";
  public static String PROP_ENABLE = "enable";
  public static String PROP_REQUIRED_REF_NUMBER = "requiredRefNumber";
  public static String PROP_REF_NUMBER_FIELD_NAME = "refNumberFieldName";
  public static String PROP_ID = "id";
  

  public BaseCustomPayment()
  {
    initialize();
  }
  


  public BaseCustomPayment(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  
  protected String name;
  
  protected Boolean requiredRefNumber;
  
  protected String refNumberFieldName;
  
  protected Boolean enable;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Boolean isRequiredRefNumber()
  {
    return requiredRefNumber == null ? Boolean.FALSE : requiredRefNumber;
  }
  



  public void setRequiredRefNumber(Boolean requiredRefNumber)
  {
    this.requiredRefNumber = requiredRefNumber;
  }
  




  public String getRefNumberFieldName()
  {
    return refNumberFieldName;
  }
  



  public void setRefNumberFieldName(String refNumberFieldName)
  {
    this.refNumberFieldName = refNumberFieldName;
  }
  




  public Boolean isEnable()
  {
    return enable == null ? Boolean.FALSE : enable;
  }
  



  public void setEnable(Boolean enable)
  {
    this.enable = enable;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof CustomPayment)) { return false;
    }
    CustomPayment customPayment = (CustomPayment)obj;
    if ((null == getId()) || (null == customPayment.getId())) return this == obj;
    return getId().equals(customPayment.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
