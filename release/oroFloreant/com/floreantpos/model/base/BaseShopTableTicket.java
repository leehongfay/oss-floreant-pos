package com.floreantpos.model.base;

import java.io.Serializable;











public abstract class BaseShopTableTicket
  implements Comparable, Serializable
{
  public static String REF = "ShopTableTicket";
  public static String PROP_TOKEN_NO = "tokenNo";
  public static String PROP_TICKET_ID = "ticketId";
  public static String PROP_USER_NAME = "userName";
  public static String PROP_USER_ID = "userId";
  protected String ticketId;
  protected Integer tokenNo;
  
  public BaseShopTableTicket() {
    initialize();
  }
  




  protected String userId;
  


  protected String userName;
  


  protected void initialize() {}
  



  public String getTicketId()
  {
    return ticketId;
  }
  



  public void setTicketId(String ticketId)
  {
    this.ticketId = ticketId;
  }
  




  public Integer getTokenNo()
  {
    return tokenNo == null ? Integer.valueOf(0) : tokenNo;
  }
  



  public void setTokenNo(Integer tokenNo)
  {
    this.tokenNo = tokenNo;
  }
  




  public String getUserId()
  {
    return userId;
  }
  



  public void setUserId(String userId)
  {
    this.userId = userId;
  }
  




  public String getUserName()
  {
    return userName;
  }
  



  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  





  public int compareTo(Object obj)
  {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
