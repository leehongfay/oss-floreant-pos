package com.floreantpos.model.base;

import com.floreantpos.model.CashBreakdown;
import java.io.Serializable;










public abstract class BaseCashBreakdown
  implements Comparable, Serializable
{
  public static String REF = "CashBreakdown";
  public static String PROP_NO_OF_ONE_DOLLER_COIN = "noOfOneDollerCoin";
  public static String PROP_NO_OF_TENS = "noOfTens";
  public static String PROP_NO_OF_ONES = "noOfOnes";
  public static String PROP_CURRENCY_ID = "currencyId";
  public static String PROP_NO_OF_FIVE_CENT_COIN = "noOfFiveCentCoin";
  public static String PROP_NO_OF_FIVES = "noOfFives";
  public static String PROP_NO_OF_FIFTY_CENT_COIN = "noOfFiftyCentCoin";
  public static String PROP_NO_OF_FIVE_THOUSANDS = "noOfFiveThousands";
  public static String PROP_NO_OF_HUNDREDS = "noOfHundreds";
  public static String PROP_CASH_DRAWER_ID = "cashDrawerId";
  public static String PROP_NO_OF_THOUSANDS = "noOfThousands";
  public static String PROP_NO_OF_TWENTIES = "noOfTwenties";
  public static String PROP_NO_OF_FIVE_HUNDREDS = "noOfFiveHundreds";
  public static String PROP_NO_OF_FIFTIES = "noOfFifties";
  public static String PROP_NO_OF_TWO_THOUSANDS = "noOfTwoThousands";
  public static String PROP_NO_OF_TWOS = "noOfTwos";
  public static String PROP_NO_OF_FIFTEEN_CENT_COIN = "noOfFifteenCentCoin";
  public static String PROP_ID = "id";
  public static String PROP_TOTAL_AMOUNT = "totalAmount";
  public static String PROP_BALANCE = "balance";
  public static String PROP_NO_OF_ONE_CENT_COIN = "noOfOneCentCoin";
  public static String PROP_NO_OF_TEN_CENT_COIN = "noOfTenCentCoin";
  public static String PROP_NO_OF_TWENTY_FIVE_CENT_COIN = "noOfTwentyFiveCentCoin";
  

  public BaseCashBreakdown()
  {
    initialize();
  }
  


  public BaseCashBreakdown(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected Double balance;
  
  protected Integer noOfFiveThousands;
  
  protected Integer noOfTwoThousands;
  
  protected Integer noOfThousands;
  
  protected Integer noOfFiveHundreds;
  
  protected Integer noOfHundreds;
  
  protected Integer noOfFifties;
  
  protected Integer noOfTwenties;
  protected Integer noOfTens;
  protected Integer noOfFives;
  protected Integer noOfTwos;
  protected Integer noOfOnes;
  protected Integer noOfOneDollerCoin;
  protected Integer noOfFiftyCentCoin;
  protected Integer noOfTwentyFiveCentCoin;
  protected Integer noOfFifteenCentCoin;
  protected Integer noOfTenCentCoin;
  protected Integer noOfFiveCentCoin;
  protected Integer noOfOneCentCoin;
  protected Double totalAmount;
  protected String currencyId;
  protected String cashDrawerId;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Double getBalance()
  {
    return balance == null ? Double.valueOf(0.0D) : balance;
  }
  



  public void setBalance(Double balance)
  {
    this.balance = balance;
  }
  




  public Integer getNoOfFiveThousands()
  {
    return noOfFiveThousands == null ? Integer.valueOf(0) : noOfFiveThousands;
  }
  



  public void setNoOfFiveThousands(Integer noOfFiveThousands)
  {
    this.noOfFiveThousands = noOfFiveThousands;
  }
  




  public Integer getNoOfTwoThousands()
  {
    return noOfTwoThousands == null ? Integer.valueOf(0) : noOfTwoThousands;
  }
  



  public void setNoOfTwoThousands(Integer noOfTwoThousands)
  {
    this.noOfTwoThousands = noOfTwoThousands;
  }
  




  public Integer getNoOfThousands()
  {
    return noOfThousands == null ? Integer.valueOf(0) : noOfThousands;
  }
  



  public void setNoOfThousands(Integer noOfThousands)
  {
    this.noOfThousands = noOfThousands;
  }
  




  public Integer getNoOfFiveHundreds()
  {
    return noOfFiveHundreds == null ? Integer.valueOf(0) : noOfFiveHundreds;
  }
  



  public void setNoOfFiveHundreds(Integer noOfFiveHundreds)
  {
    this.noOfFiveHundreds = noOfFiveHundreds;
  }
  




  public Integer getNoOfHundreds()
  {
    return noOfHundreds == null ? Integer.valueOf(0) : noOfHundreds;
  }
  



  public void setNoOfHundreds(Integer noOfHundreds)
  {
    this.noOfHundreds = noOfHundreds;
  }
  




  public Integer getNoOfFifties()
  {
    return noOfFifties == null ? Integer.valueOf(0) : noOfFifties;
  }
  



  public void setNoOfFifties(Integer noOfFifties)
  {
    this.noOfFifties = noOfFifties;
  }
  




  public Integer getNoOfTwenties()
  {
    return noOfTwenties == null ? Integer.valueOf(0) : noOfTwenties;
  }
  



  public void setNoOfTwenties(Integer noOfTwenties)
  {
    this.noOfTwenties = noOfTwenties;
  }
  




  public Integer getNoOfTens()
  {
    return noOfTens == null ? Integer.valueOf(0) : noOfTens;
  }
  



  public void setNoOfTens(Integer noOfTens)
  {
    this.noOfTens = noOfTens;
  }
  




  public Integer getNoOfFives()
  {
    return noOfFives == null ? Integer.valueOf(0) : noOfFives;
  }
  



  public void setNoOfFives(Integer noOfFives)
  {
    this.noOfFives = noOfFives;
  }
  




  public Integer getNoOfTwos()
  {
    return noOfTwos == null ? Integer.valueOf(0) : noOfTwos;
  }
  



  public void setNoOfTwos(Integer noOfTwos)
  {
    this.noOfTwos = noOfTwos;
  }
  




  public Integer getNoOfOnes()
  {
    return noOfOnes == null ? Integer.valueOf(0) : noOfOnes;
  }
  



  public void setNoOfOnes(Integer noOfOnes)
  {
    this.noOfOnes = noOfOnes;
  }
  




  public Integer getNoOfOneDollerCoin()
  {
    return noOfOneDollerCoin == null ? Integer.valueOf(0) : noOfOneDollerCoin;
  }
  



  public void setNoOfOneDollerCoin(Integer noOfOneDollerCoin)
  {
    this.noOfOneDollerCoin = noOfOneDollerCoin;
  }
  




  public Integer getNoOfFiftyCentCoin()
  {
    return noOfFiftyCentCoin == null ? Integer.valueOf(0) : noOfFiftyCentCoin;
  }
  



  public void setNoOfFiftyCentCoin(Integer noOfFiftyCentCoin)
  {
    this.noOfFiftyCentCoin = noOfFiftyCentCoin;
  }
  




  public Integer getNoOfTwentyFiveCentCoin()
  {
    return noOfTwentyFiveCentCoin == null ? Integer.valueOf(0) : noOfTwentyFiveCentCoin;
  }
  



  public void setNoOfTwentyFiveCentCoin(Integer noOfTwentyFiveCentCoin)
  {
    this.noOfTwentyFiveCentCoin = noOfTwentyFiveCentCoin;
  }
  




  public Integer getNoOfFifteenCentCoin()
  {
    return noOfFifteenCentCoin == null ? Integer.valueOf(0) : noOfFifteenCentCoin;
  }
  



  public void setNoOfFifteenCentCoin(Integer noOfFifteenCentCoin)
  {
    this.noOfFifteenCentCoin = noOfFifteenCentCoin;
  }
  




  public Integer getNoOfTenCentCoin()
  {
    return noOfTenCentCoin == null ? Integer.valueOf(0) : noOfTenCentCoin;
  }
  



  public void setNoOfTenCentCoin(Integer noOfTenCentCoin)
  {
    this.noOfTenCentCoin = noOfTenCentCoin;
  }
  




  public Integer getNoOfFiveCentCoin()
  {
    return noOfFiveCentCoin == null ? Integer.valueOf(0) : noOfFiveCentCoin;
  }
  



  public void setNoOfFiveCentCoin(Integer noOfFiveCentCoin)
  {
    this.noOfFiveCentCoin = noOfFiveCentCoin;
  }
  




  public Integer getNoOfOneCentCoin()
  {
    return noOfOneCentCoin == null ? Integer.valueOf(0) : noOfOneCentCoin;
  }
  



  public void setNoOfOneCentCoin(Integer noOfOneCentCoin)
  {
    this.noOfOneCentCoin = noOfOneCentCoin;
  }
  




  public Double getTotalAmount()
  {
    return totalAmount == null ? Double.valueOf(0.0D) : totalAmount;
  }
  



  public void setTotalAmount(Double totalAmount)
  {
    this.totalAmount = totalAmount;
  }
  




  public String getCurrencyId()
  {
    return currencyId;
  }
  



  public void setCurrencyId(String currencyId)
  {
    this.currencyId = currencyId;
  }
  




  public String getCashDrawerId()
  {
    return cashDrawerId;
  }
  



  public void setCashDrawerId(String cashDrawerId)
  {
    this.cashDrawerId = cashDrawerId;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof CashBreakdown)) { return false;
    }
    CashBreakdown cashBreakdown = (CashBreakdown)obj;
    if ((null == getId()) || (null == cashBreakdown.getId())) return this == obj;
    return getId().equals(cashBreakdown.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
