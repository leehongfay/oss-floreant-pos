package com.floreantpos.model.base;

import com.floreantpos.model.TicketItemSeat;
import java.io.Serializable;










public abstract class BaseTicketItemSeat
  implements Comparable, Serializable
{
  public static String REF = "TicketItemSeat";
  public static String PROP_MEMBER_ID = "memberId";
  public static String PROP_ID = "id";
  public static String PROP_SEAT_NUMBER = "seatNumber";
  public static String PROP_SEAT_ID = "seatId";
  

  public BaseTicketItemSeat()
  {
    initialize();
  }
  


  public BaseTicketItemSeat(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  long version;
  

  protected String seatId;
  
  protected Integer seatNumber;
  
  protected String memberId;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getSeatId()
  {
    return seatId;
  }
  



  public void setSeatId(String seatId)
  {
    this.seatId = seatId;
  }
  




  public Integer getSeatNumber()
  {
    return seatNumber == null ? Integer.valueOf(0) : seatNumber;
  }
  



  public void setSeatNumber(Integer seatNumber)
  {
    this.seatNumber = seatNumber;
  }
  




  public String getMemberId()
  {
    return memberId;
  }
  



  public void setMemberId(String memberId)
  {
    this.memberId = memberId;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof TicketItemSeat)) { return false;
    }
    TicketItemSeat ticketItemSeat = (TicketItemSeat)obj;
    if ((null == getId()) || (null == ticketItemSeat.getId())) return this == obj;
    return getId().equals(ticketItemSeat.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
