package com.floreantpos.model.base;

import com.floreantpos.model.Terminal;
import com.floreantpos.model.TerminalPrinters;
import com.floreantpos.model.VirtualPrinter;
import java.io.Serializable;








public abstract class BaseTerminalPrinters
  implements Comparable, Serializable
{
  public static String REF = "TerminalPrinters";
  public static String PROP_TERMINAL = "terminal";
  public static String PROP_PRINTER_NAME = "printerName";
  public static String PROP_ID = "id";
  public static String PROP_VIRTUAL_PRINTER = "virtualPrinter";
  

  public BaseTerminalPrinters()
  {
    initialize();
  }
  


  public BaseTerminalPrinters(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String printerName;
  

  private Terminal terminal;
  

  private VirtualPrinter virtualPrinter;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getPrinterName()
  {
    return printerName;
  }
  



  public void setPrinterName(String printerName)
  {
    this.printerName = printerName;
  }
  




  public Terminal getTerminal()
  {
    return terminal;
  }
  



  public void setTerminal(Terminal terminal)
  {
    this.terminal = terminal;
  }
  




  public VirtualPrinter getVirtualPrinter()
  {
    return virtualPrinter;
  }
  



  public void setVirtualPrinter(VirtualPrinter virtualPrinter)
  {
    this.virtualPrinter = virtualPrinter;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof TerminalPrinters)) { return false;
    }
    TerminalPrinters terminalPrinters = (TerminalPrinters)obj;
    if ((null == getId()) || (null == terminalPrinters.getId())) return this == obj;
    return getId().equals(terminalPrinters.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
