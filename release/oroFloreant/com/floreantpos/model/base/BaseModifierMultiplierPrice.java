package com.floreantpos.model.base;

import com.floreantpos.model.ModifierMultiplierPrice;
import java.io.Serializable;










public abstract class BaseModifierMultiplierPrice
  implements Comparable, Serializable
{
  public static String REF = "ModifierMultiplierPrice";
  public static String PROP_MODIFIER_ID = "modifierId";
  public static String PROP_MULTIPLIER_ID = "multiplierId";
  public static String PROP_PRICE = "price";
  public static String PROP_ID = "id";
  public static String PROP_PIZZA_MODIFIER_PRICE_ID = "pizzaModifierPriceId";
  

  public BaseModifierMultiplierPrice()
  {
    initialize();
  }
  


  public BaseModifierMultiplierPrice(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  long version;
  
  protected Double price;
  
  protected String modifierId;
  
  protected String pizzaModifierPriceId;
  
  protected String multiplierId;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Double getPrice()
  {
    return price == null ? Double.valueOf(0.0D) : price;
  }
  



  public void setPrice(Double price)
  {
    this.price = price;
  }
  




  public String getModifierId()
  {
    return modifierId;
  }
  



  public void setModifierId(String modifierId)
  {
    this.modifierId = modifierId;
  }
  




  public String getPizzaModifierPriceId()
  {
    return pizzaModifierPriceId;
  }
  



  public void setPizzaModifierPriceId(String pizzaModifierPriceId)
  {
    this.pizzaModifierPriceId = pizzaModifierPriceId;
  }
  




  public String getMultiplierId()
  {
    return multiplierId;
  }
  



  public void setMultiplierId(String multiplierId)
  {
    this.multiplierId = multiplierId;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof ModifierMultiplierPrice)) { return false;
    }
    ModifierMultiplierPrice modifierMultiplierPrice = (ModifierMultiplierPrice)obj;
    if ((null == getId()) || (null == modifierMultiplierPrice.getId())) return this == obj;
    return getId().equals(modifierMultiplierPrice.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
