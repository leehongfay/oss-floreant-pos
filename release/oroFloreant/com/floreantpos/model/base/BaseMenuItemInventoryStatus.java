package com.floreantpos.model.base;

import com.floreantpos.model.MenuItemInventoryStatus;
import java.io.Serializable;










public abstract class BaseMenuItemInventoryStatus
  implements Comparable, Serializable
{
  public static String REF = "MenuItemInventoryStatus";
  public static String PROP_UNIT_ON_HAND = "unitOnHand";
  public static String PROP_AVAILABLE_UNIT = "availableUnit";
  public static String PROP_STOCK_AMOUNT = "stockAmount";
  public static String PROP_ID = "id";
  

  public BaseMenuItemInventoryStatus()
  {
    initialize();
  }
  


  public BaseMenuItemInventoryStatus(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  long version;
  

  protected Double availableUnit;
  
  protected Double stockAmount;
  
  protected Double unitOnHand;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Double getAvailableUnit()
  {
    return availableUnit == null ? Double.valueOf(0.0D) : availableUnit;
  }
  



  public void setAvailableUnit(Double availableUnit)
  {
    this.availableUnit = availableUnit;
  }
  




  public Double getStockAmount()
  {
    return stockAmount == null ? Double.valueOf(0.0D) : stockAmount;
  }
  



  public void setStockAmount(Double stockAmount)
  {
    this.stockAmount = stockAmount;
  }
  




  public Double getUnitOnHand()
  {
    return unitOnHand == null ? Double.valueOf(0.0D) : unitOnHand;
  }
  



  public void setUnitOnHand(Double unitOnHand)
  {
    this.unitOnHand = unitOnHand;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof MenuItemInventoryStatus)) { return false;
    }
    MenuItemInventoryStatus menuItemInventoryStatus = (MenuItemInventoryStatus)obj;
    if ((null == getId()) || (null == menuItemInventoryStatus.getId())) return this == obj;
    return getId().equals(menuItemInventoryStatus.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
