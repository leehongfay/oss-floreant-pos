package com.floreantpos.model.base;

import com.floreantpos.model.Customer;
import com.floreantpos.model.DeliveryAddress;
import com.floreantpos.model.DeliveryInstruction;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;






public abstract class BaseCustomer
  implements Comparable, Serializable
{
  public static String REF = "Customer";
  public static String PROP_EMAIL = "email";
  public static String PROP_ADDRESS = "address";
  public static String PROP_SIGNATURE_IMAGE_ID = "signatureImageId";
  public static String PROP_SALUTATION = "salutation";
  public static String PROP_HOME_PHONE_NO = "homePhoneNo";
  public static String PROP_NAME = "name";
  public static String PROP_MEMBER_ID = "memberId";
  public static String PROP_CUSTOMER_GROUP_ID = "customerGroupId";
  public static String PROP_DOB = "dob";
  public static String PROP_IMAGE_ID = "imageId";
  public static String PROP_EMAIL2 = "email2";
  public static String PROP_MOBILE_NO = "mobileNo";
  public static String PROP_FIRST_NAME = "firstName";
  public static String PROP_ZIP_CODE = "zipCode";
  public static String PROP_CREDIT_LIMIT = "creditLimit";
  public static String PROP_CITY = "city";
  public static String PROP_SSN = "ssn";
  public static String PROP_CREDIT_CARD_NO = "creditCardNo";
  public static String PROP_TAX_EXEMPT = "taxExempt";
  public static String PROP_LOYALTY_NO = "loyaltyNo";
  public static String PROP_PIN = "pin";
  public static String PROP_MEMBER_TYPE = "memberType";
  public static String PROP_STATE = "state";
  public static String PROP_NOTE = "note";
  public static String PROP_LOYALTY_POINT = "loyaltyPoint";
  public static String PROP_COUNTRY = "country";
  public static String PROP_LAST_NAME = "lastName";
  public static String PROP_ID = "id";
  public static String PROP_WORK_PHONE_NO = "workPhoneNo";
  public static String PROP_VIP = "vip";
  public static String PROP_BALANCE = "balance";
  public static String PROP_CREDIT_SPENT = "creditSpent";
  

  public BaseCustomer()
  {
    initialize();
  }
  


  public BaseCustomer(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected String memberId;
  
  protected String firstName;
  
  protected String lastName;
  
  protected String name;
  
  protected String loyaltyNo;
  
  protected Integer loyaltyPoint;
  
  protected String imageId;
  
  protected String signatureImageId;
  
  protected String homePhoneNo;
  
  protected String mobileNo;
  protected String workPhoneNo;
  protected String email;
  protected String email2;
  protected String salutation;
  protected String dob;
  protected String ssn;
  protected String address;
  protected String city;
  protected String state;
  protected String zipCode;
  protected String country;
  protected Boolean vip;
  protected Double balance;
  protected Double creditLimit;
  protected Double creditSpent;
  protected String creditCardNo;
  protected String note;
  protected Boolean taxExempt;
  protected String pin;
  protected String customerGroupId;
  protected String memberType;
  private List<DeliveryAddress> deliveryAddresses;
  private List<DeliveryInstruction> deliveryInstructions;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getMemberId()
  {
    return memberId;
  }
  



  public void setMemberId(String memberId)
  {
    this.memberId = memberId;
  }
  




  public String getFirstName()
  {
    return firstName;
  }
  



  public void setFirstName(String firstName)
  {
    this.firstName = firstName;
  }
  




  public String getLastName()
  {
    return lastName;
  }
  



  public void setLastName(String lastName)
  {
    this.lastName = lastName;
  }
  




  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getLoyaltyNo()
  {
    return loyaltyNo;
  }
  



  public void setLoyaltyNo(String loyaltyNo)
  {
    this.loyaltyNo = loyaltyNo;
  }
  




  public Integer getLoyaltyPoint()
  {
    return loyaltyPoint == null ? Integer.valueOf(0) : loyaltyPoint;
  }
  



  public void setLoyaltyPoint(Integer loyaltyPoint)
  {
    this.loyaltyPoint = loyaltyPoint;
  }
  




  public String getImageId()
  {
    return imageId;
  }
  



  public void setImageId(String imageId)
  {
    this.imageId = imageId;
  }
  




  public String getSignatureImageId()
  {
    return signatureImageId;
  }
  



  public void setSignatureImageId(String signatureImageId)
  {
    this.signatureImageId = signatureImageId;
  }
  




  public String getHomePhoneNo()
  {
    return homePhoneNo;
  }
  



  public void setHomePhoneNo(String homePhoneNo)
  {
    this.homePhoneNo = homePhoneNo;
  }
  




  public String getMobileNo()
  {
    return mobileNo;
  }
  



  public void setMobileNo(String mobileNo)
  {
    this.mobileNo = mobileNo;
  }
  




  public String getWorkPhoneNo()
  {
    return workPhoneNo;
  }
  



  public void setWorkPhoneNo(String workPhoneNo)
  {
    this.workPhoneNo = workPhoneNo;
  }
  




  public String getEmail()
  {
    return email;
  }
  



  public void setEmail(String email)
  {
    this.email = email;
  }
  




  public String getEmail2()
  {
    return email2;
  }
  



  public void setEmail2(String email2)
  {
    this.email2 = email2;
  }
  




  public String getSalutation()
  {
    return salutation;
  }
  



  public void setSalutation(String salutation)
  {
    this.salutation = salutation;
  }
  




  public String getDob()
  {
    return dob;
  }
  



  public void setDob(String dob)
  {
    this.dob = dob;
  }
  




  public String getSsn()
  {
    return ssn;
  }
  



  public void setSsn(String ssn)
  {
    this.ssn = ssn;
  }
  




  public String getAddress()
  {
    return address;
  }
  



  public void setAddress(String address)
  {
    this.address = address;
  }
  




  public String getCity()
  {
    return city;
  }
  



  public void setCity(String city)
  {
    this.city = city;
  }
  




  public String getState()
  {
    return state;
  }
  



  public void setState(String state)
  {
    this.state = state;
  }
  




  public String getZipCode()
  {
    return zipCode;
  }
  



  public void setZipCode(String zipCode)
  {
    this.zipCode = zipCode;
  }
  




  public String getCountry()
  {
    return country;
  }
  



  public void setCountry(String country)
  {
    this.country = country;
  }
  




  public Boolean isVip()
  {
    return vip == null ? Boolean.FALSE : vip;
  }
  



  public void setVip(Boolean vip)
  {
    this.vip = vip;
  }
  




  public Double getBalance()
  {
    return balance == null ? Double.valueOf(0.0D) : balance;
  }
  



  public void setBalance(Double balance)
  {
    this.balance = balance;
  }
  




  public Double getCreditLimit()
  {
    return creditLimit == null ? Double.valueOf(0.0D) : creditLimit;
  }
  



  public void setCreditLimit(Double creditLimit)
  {
    this.creditLimit = creditLimit;
  }
  




  public Double getCreditSpent()
  {
    return creditSpent == null ? Double.valueOf(0.0D) : creditSpent;
  }
  



  public void setCreditSpent(Double creditSpent)
  {
    this.creditSpent = creditSpent;
  }
  




  public String getCreditCardNo()
  {
    return creditCardNo;
  }
  



  public void setCreditCardNo(String creditCardNo)
  {
    this.creditCardNo = creditCardNo;
  }
  




  public String getNote()
  {
    return note;
  }
  



  public void setNote(String note)
  {
    this.note = note;
  }
  




  public Boolean isTaxExempt()
  {
    return taxExempt == null ? Boolean.FALSE : taxExempt;
  }
  



  public void setTaxExempt(Boolean taxExempt)
  {
    this.taxExempt = taxExempt;
  }
  




  public String getPin()
  {
    return pin;
  }
  



  public void setPin(String pin)
  {
    this.pin = pin;
  }
  




  public String getCustomerGroupId()
  {
    return customerGroupId;
  }
  



  public void setCustomerGroupId(String customerGroupId)
  {
    this.customerGroupId = customerGroupId;
  }
  




  public String getMemberType()
  {
    return memberType;
  }
  



  public void setMemberType(String memberType)
  {
    this.memberType = memberType;
  }
  




  public List<DeliveryAddress> getDeliveryAddresses()
  {
    return deliveryAddresses;
  }
  



  public void setDeliveryAddresses(List<DeliveryAddress> deliveryAddresses)
  {
    this.deliveryAddresses = deliveryAddresses;
  }
  
  public void addTodeliveryAddresses(DeliveryAddress deliveryAddress) {
    if (null == getDeliveryAddresses()) setDeliveryAddresses(new ArrayList());
    getDeliveryAddresses().add(deliveryAddress);
  }
  




  public List<DeliveryInstruction> getDeliveryInstructions()
  {
    return deliveryInstructions;
  }
  



  public void setDeliveryInstructions(List<DeliveryInstruction> deliveryInstructions)
  {
    this.deliveryInstructions = deliveryInstructions;
  }
  
  public void addTodeliveryInstructions(DeliveryInstruction deliveryInstruction) {
    if (null == getDeliveryInstructions()) setDeliveryInstructions(new ArrayList());
    getDeliveryInstructions().add(deliveryInstruction);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof Customer)) { return false;
    }
    Customer customer = (Customer)obj;
    if ((null == getId()) || (null == customer.getId())) return this == obj;
    return getId().equals(customer.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
