package com.floreantpos.model.base;

import com.floreantpos.model.StoreSession;
import java.io.Serializable;
import java.util.Date;









public abstract class BaseStoreSession
  implements Comparable, Serializable
{
  public static String REF = "StoreSession";
  public static String PROP_OPEN_TIME = "openTime";
  public static String PROP_CLOSE_TIME = "closeTime";
  public static String PROP_OUTLET_ID = "outletId";
  public static String PROP_ID = "id";
  public static String PROP_CLOSED_BY_USER_ID = "closedByUserId";
  public static String PROP_OPENED_BY_USER_ID = "openedByUserId";
  

  public BaseStoreSession()
  {
    initialize();
  }
  


  public BaseStoreSession(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  
  long version;
  
  protected Date openTime;
  
  protected Date closeTime;
  
  protected String outletId;
  
  protected String openedByUserId;
  
  protected String closedByUserId;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Date getOpenTime()
  {
    return openTime;
  }
  



  public void setOpenTime(Date openTime)
  {
    this.openTime = openTime;
  }
  




  public Date getCloseTime()
  {
    return closeTime;
  }
  



  public void setCloseTime(Date closeTime)
  {
    this.closeTime = closeTime;
  }
  




  public String getOutletId()
  {
    return outletId;
  }
  



  public void setOutletId(String outletId)
  {
    this.outletId = outletId;
  }
  




  public String getOpenedByUserId()
  {
    return openedByUserId;
  }
  



  public void setOpenedByUserId(String openedByUserId)
  {
    this.openedByUserId = openedByUserId;
  }
  




  public String getClosedByUserId()
  {
    return closedByUserId;
  }
  



  public void setClosedByUserId(String closedByUserId)
  {
    this.closedByUserId = closedByUserId;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof StoreSession)) { return false;
    }
    StoreSession storeSession = (StoreSession)obj;
    if ((null == getId()) || (null == storeSession.getId())) return this == obj;
    return getId().equals(storeSession.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
