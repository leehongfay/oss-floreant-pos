package com.floreantpos.model.base;

import com.floreantpos.model.DataSync;
import com.floreantpos.model.Shift;
import java.io.Serializable;








public abstract class BaseDataSync
  extends Shift
  implements Comparable, Serializable
{
  public static String REF = "DataSync";
  public static String PROP_ID = "id";
  

  public BaseDataSync()
  {
    initialize();
  }
  


  public BaseDataSync(String id)
  {
    super(id);
  }
  





  public BaseDataSync(String id, String name)
  {
    super(id, name);
  }
  




  private int hashCode = Integer.MIN_VALUE;
  







  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof DataSync)) { return false;
    }
    DataSync dataSync = (DataSync)obj;
    if ((null == getId()) || (null == dataSync.getId())) return this == obj;
    return getId().equals(dataSync.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
