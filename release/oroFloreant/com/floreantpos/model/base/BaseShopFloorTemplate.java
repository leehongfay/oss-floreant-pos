package com.floreantpos.model.base;

import com.floreantpos.model.ShopFloor;
import com.floreantpos.model.ShopFloorTemplate;
import java.io.Serializable;
import java.util.Map;








public abstract class BaseShopFloorTemplate
  implements Comparable, Serializable
{
  public static String REF = "ShopFloorTemplate";
  public static String PROP_NAME = "name";
  public static String PROP_MAIN = "main";
  public static String PROP_DEFAULT_FLOOR = "defaultFloor";
  public static String PROP_ID = "id";
  public static String PROP_FLOOR = "floor";
  

  public BaseShopFloorTemplate()
  {
    initialize();
  }
  


  public BaseShopFloorTemplate(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String name;
  

  protected Boolean defaultFloor;
  

  protected Boolean main;
  
  private ShopFloor floor;
  
  private Map<String, String> properties;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Boolean isDefaultFloor()
  {
    return defaultFloor == null ? Boolean.FALSE : defaultFloor;
  }
  



  public void setDefaultFloor(Boolean defaultFloor)
  {
    this.defaultFloor = defaultFloor;
  }
  




  public Boolean isMain()
  {
    return main == null ? Boolean.FALSE : main;
  }
  



  public void setMain(Boolean main)
  {
    this.main = main;
  }
  




  public ShopFloor getFloor()
  {
    return floor;
  }
  



  public void setFloor(ShopFloor floor)
  {
    this.floor = floor;
  }
  




  public Map<String, String> getProperties()
  {
    return properties;
  }
  



  public void setProperties(Map<String, String> properties)
  {
    this.properties = properties;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof ShopFloorTemplate)) { return false;
    }
    ShopFloorTemplate shopFloorTemplate = (ShopFloorTemplate)obj;
    if ((null == getId()) || (null == shopFloorTemplate.getId())) return this == obj;
    return getId().equals(shopFloorTemplate.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
