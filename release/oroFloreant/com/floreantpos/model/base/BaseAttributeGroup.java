package com.floreantpos.model.base;

import com.floreantpos.model.Attribute;
import com.floreantpos.model.AttributeGroup;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;







public abstract class BaseAttributeGroup
  implements Comparable, Serializable
{
  public static String REF = "AttributeGroup";
  public static String PROP_NAME = "name";
  public static String PROP_SORT_ORDER = "sortOrder";
  public static String PROP_ID = "id";
  

  public BaseAttributeGroup()
  {
    initialize();
  }
  


  public BaseAttributeGroup(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseAttributeGroup(String id, String name)
  {
    setId(id);
    setName(name);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected String name;
  

  protected Integer sortOrder;
  

  private List<Attribute> attributes;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public Integer getSortOrder()
  {
    return sortOrder == null ? Integer.valueOf(0) : sortOrder;
  }
  



  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  




  public List<Attribute> getAttributes()
  {
    return attributes;
  }
  



  public void setAttributes(List<Attribute> attributes)
  {
    this.attributes = attributes;
  }
  
  public void addToattributes(Attribute attribute) {
    if (null == getAttributes()) setAttributes(new ArrayList());
    getAttributes().add(attribute);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof AttributeGroup)) { return false;
    }
    AttributeGroup attributeGroup = (AttributeGroup)obj;
    if ((null == getId()) || (null == attributeGroup.getId())) return this == obj;
    return getId().equals(attributeGroup.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
