package com.floreantpos.model.base;

import com.floreantpos.model.BookingInfo;
import com.floreantpos.model.Customer;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;




public abstract class BaseBookingInfo
  implements Comparable, Serializable
{
  public static String REF = "BookingInfo";
  public static String PROP_CUSTOMER = "customer";
  public static String PROP_USER = "user";
  public static String PROP_STATUS = "status";
  public static String PROP_BOOKING_ID = "bookingId";
  public static String PROP_TO_DATE = "toDate";
  public static String PROP_BOOKING_CHARGE = "bookingCharge";
  public static String PROP_GUEST_COUNT = "guestCount";
  public static String PROP_FROM_DATE = "fromDate";
  public static String PROP_ID = "id";
  public static String PROP_PAID_AMOUNT = "paidAmount";
  public static String PROP_REMAINING_BALANCE = "remainingBalance";
  public static String PROP_PAYMENT_STATUS = "paymentStatus";
  public static String PROP_BOOKING_TYPE = "bookingType";
  

  public BaseBookingInfo()
  {
    initialize();
  }
  


  public BaseBookingInfo(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  private long version;
  
  protected Date fromDate;
  
  protected Date toDate;
  
  protected Integer guestCount;
  
  protected String status;
  
  protected String paymentStatus;
  
  protected Double bookingCharge;
  
  protected Double remainingBalance;
  
  protected Double paidAmount;
  
  protected String bookingType;
  
  protected String bookingId;
  
  private User user;
  
  private Customer customer;
  private List<ShopTable> tables;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Date getFromDate()
  {
    return fromDate;
  }
  



  public void setFromDate(Date fromDate)
  {
    this.fromDate = fromDate;
  }
  




  public Date getToDate()
  {
    return toDate;
  }
  



  public void setToDate(Date toDate)
  {
    this.toDate = toDate;
  }
  




  public Integer getGuestCount()
  {
    return guestCount == null ? Integer.valueOf(0) : guestCount;
  }
  



  public void setGuestCount(Integer guestCount)
  {
    this.guestCount = guestCount;
  }
  




  public String getStatus()
  {
    return status;
  }
  



  public void setStatus(String status)
  {
    this.status = status;
  }
  




  public String getPaymentStatus()
  {
    return paymentStatus;
  }
  



  public void setPaymentStatus(String paymentStatus)
  {
    this.paymentStatus = paymentStatus;
  }
  




  public Double getBookingCharge()
  {
    return bookingCharge == null ? Double.valueOf(0.0D) : bookingCharge;
  }
  



  public void setBookingCharge(Double bookingCharge)
  {
    this.bookingCharge = bookingCharge;
  }
  




  public Double getRemainingBalance()
  {
    return remainingBalance == null ? Double.valueOf(0.0D) : remainingBalance;
  }
  



  public void setRemainingBalance(Double remainingBalance)
  {
    this.remainingBalance = remainingBalance;
  }
  




  public Double getPaidAmount()
  {
    return paidAmount == null ? Double.valueOf(0.0D) : paidAmount;
  }
  



  public void setPaidAmount(Double paidAmount)
  {
    this.paidAmount = paidAmount;
  }
  




  public String getBookingType()
  {
    return bookingType;
  }
  



  public void setBookingType(String bookingType)
  {
    this.bookingType = bookingType;
  }
  




  public String getBookingId()
  {
    return bookingId;
  }
  



  public void setBookingId(String bookingId)
  {
    this.bookingId = bookingId;
  }
  




  public User getUser()
  {
    return user;
  }
  



  public void setUser(User user)
  {
    this.user = user;
  }
  




  public Customer getCustomer()
  {
    return customer;
  }
  



  public void setCustomer(Customer customer)
  {
    this.customer = customer;
  }
  




  public List<ShopTable> getTables()
  {
    return tables;
  }
  



  public void setTables(List<ShopTable> tables)
  {
    this.tables = tables;
  }
  
  public void addTotables(ShopTable shopTable) {
    if (null == getTables()) setTables(new ArrayList());
    getTables().add(shopTable);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof BookingInfo)) { return false;
    }
    BookingInfo bookingInfo = (BookingInfo)obj;
    if ((null == getId()) || (null == bookingInfo.getId())) return this == obj;
    return getId().equals(bookingInfo.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
