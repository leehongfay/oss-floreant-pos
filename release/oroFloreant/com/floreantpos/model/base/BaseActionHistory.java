package com.floreantpos.model.base;

import com.floreantpos.model.ActionHistory;
import com.floreantpos.model.User;
import java.io.Serializable;
import java.util.Date;








public abstract class BaseActionHistory
  implements Comparable, Serializable
{
  public static String REF = "ActionHistory";
  public static String PROP_DESCRIPTION = "description";
  public static String PROP_ACTION_NAME = "actionName";
  public static String PROP_ACTION_TIME = "actionTime";
  public static String PROP_ID = "id";
  public static String PROP_PERFORMER = "performer";
  

  public BaseActionHistory()
  {
    initialize();
  }
  


  public BaseActionHistory(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  long version;
  

  protected Date actionTime;
  

  protected String actionName;
  
  protected String description;
  
  private User performer;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Date getActionTime()
  {
    return actionTime;
  }
  



  public void setActionTime(Date actionTime)
  {
    this.actionTime = actionTime;
  }
  




  public String getActionName()
  {
    return actionName;
  }
  



  public void setActionName(String actionName)
  {
    this.actionName = actionName;
  }
  




  public String getDescription()
  {
    return description;
  }
  



  public void setDescription(String description)
  {
    this.description = description;
  }
  




  public User getPerformer()
  {
    return performer;
  }
  



  public void setPerformer(User performer)
  {
    this.performer = performer;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof ActionHistory)) { return false;
    }
    ActionHistory actionHistory = (ActionHistory)obj;
    if ((null == getId()) || (null == actionHistory.getId())) return this == obj;
    return getId().equals(actionHistory.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
