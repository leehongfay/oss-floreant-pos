package com.floreantpos.model.base;

import com.floreantpos.model.MenuItem;
import com.floreantpos.model.Recepie;
import com.floreantpos.model.RecepieItem;
import java.io.Serializable;








public abstract class BaseRecepieItem
  implements Comparable, Serializable
{
  public static String REF = "RecepieItem";
  public static String PROP_INVENTORY_DEDUCTABLE = "inventoryDeductable";
  public static String PROP_UNIT_CODE = "unitCode";
  public static String PROP_INVENTORY_ITEM = "inventoryItem";
  public static String PROP_PERCENTAGE = "percentage";
  public static String PROP_QUANTITY = "quantity";
  public static String PROP_ID = "id";
  public static String PROP_RECEPIE = "recepie";
  

  public BaseRecepieItem()
  {
    initialize();
  }
  


  public BaseRecepieItem(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  long version;
  
  protected Double quantity;
  
  protected String unitCode;
  
  protected Double percentage;
  
  protected Boolean inventoryDeductable;
  
  private MenuItem inventoryItem;
  
  private Recepie recepie;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Double getQuantity()
  {
    return quantity == null ? Double.valueOf(0.0D) : quantity;
  }
  



  public void setQuantity(Double quantity)
  {
    this.quantity = quantity;
  }
  




  public String getUnitCode()
  {
    return unitCode;
  }
  



  public void setUnitCode(String unitCode)
  {
    this.unitCode = unitCode;
  }
  




  public Double getPercentage()
  {
    return percentage == null ? Double.valueOf(0.0D) : percentage;
  }
  



  public void setPercentage(Double percentage)
  {
    this.percentage = percentage;
  }
  




  public Boolean isInventoryDeductable()
  {
    return inventoryDeductable == null ? Boolean.FALSE : inventoryDeductable;
  }
  



  public void setInventoryDeductable(Boolean inventoryDeductable)
  {
    this.inventoryDeductable = inventoryDeductable;
  }
  




  public MenuItem getInventoryItem()
  {
    return inventoryItem;
  }
  



  public void setInventoryItem(MenuItem inventoryItem)
  {
    this.inventoryItem = inventoryItem;
  }
  




  public Recepie getRecepie()
  {
    return recepie;
  }
  



  public void setRecepie(Recepie recepie)
  {
    this.recepie = recepie;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof RecepieItem)) { return false;
    }
    RecepieItem recepieItem = (RecepieItem)obj;
    if ((null == getId()) || (null == recepieItem.getId())) return this == obj;
    return getId().equals(recepieItem.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
