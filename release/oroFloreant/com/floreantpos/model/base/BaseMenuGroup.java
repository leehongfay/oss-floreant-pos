package com.floreantpos.model.base;

import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuPage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;







public abstract class BaseMenuGroup
  implements Comparable, Serializable
{
  public static String REF = "MenuGroup";
  public static String PROP_NAME = "name";
  public static String PROP_BEVERAGE = "beverage";
  public static String PROP_MENU_CATEGORY_NAME = "menuCategoryName";
  public static String PROP_TEXT_COLOR_CODE = "textColorCode";
  public static String PROP_VISIBLE = "visible";
  public static String PROP_SORT_ORDER = "sortOrder";
  public static String PROP_BUTTON_COLOR_CODE = "buttonColorCode";
  public static String PROP_ID = "id";
  public static String PROP_TRANSLATED_NAME = "translatedName";
  public static String PROP_MENU_CATEGORY_ID = "menuCategoryId";
  

  public BaseMenuGroup()
  {
    initialize();
  }
  


  public BaseMenuGroup(String id)
  {
    setId(id);
    initialize();
  }
  





  public BaseMenuGroup(String id, String name)
  {
    setId(id);
    setName(name);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected String name;
  
  protected String translatedName;
  
  protected Boolean visible;
  
  protected Integer sortOrder;
  
  protected Integer buttonColorCode;
  
  protected Integer textColorCode;
  
  protected String menuCategoryId;
  
  protected String menuCategoryName;
  
  protected Boolean beverage;
  
  private List<MenuPage> menuPages;
  
  protected void initialize() {}
  
  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getTranslatedName()
  {
    return translatedName;
  }
  



  public void setTranslatedName(String translatedName)
  {
    this.translatedName = translatedName;
  }
  




  public Boolean isVisible()
  {
    return visible == null ? Boolean.FALSE : visible;
  }
  



  public void setVisible(Boolean visible)
  {
    this.visible = visible;
  }
  




  public Integer getSortOrder()
  {
    return sortOrder == null ? Integer.valueOf(0) : sortOrder;
  }
  



  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  




  public Integer getButtonColorCode()
  {
    return buttonColorCode == null ? Integer.valueOf(0) : buttonColorCode;
  }
  



  public void setButtonColorCode(Integer buttonColorCode)
  {
    this.buttonColorCode = buttonColorCode;
  }
  




  public Integer getTextColorCode()
  {
    return textColorCode == null ? Integer.valueOf(0) : textColorCode;
  }
  



  public void setTextColorCode(Integer textColorCode)
  {
    this.textColorCode = textColorCode;
  }
  




  public String getMenuCategoryId()
  {
    return menuCategoryId;
  }
  



  public void setMenuCategoryId(String menuCategoryId)
  {
    this.menuCategoryId = menuCategoryId;
  }
  




  public String getMenuCategoryName()
  {
    return menuCategoryName;
  }
  



  public void setMenuCategoryName(String menuCategoryName)
  {
    this.menuCategoryName = menuCategoryName;
  }
  




  public Boolean isBeverage()
  {
    return beverage == null ? Boolean.FALSE : beverage;
  }
  



  public void setBeverage(Boolean beverage)
  {
    this.beverage = beverage;
  }
  




  public List<MenuPage> getMenuPages()
  {
    return menuPages;
  }
  



  public void setMenuPages(List<MenuPage> menuPages)
  {
    this.menuPages = menuPages;
  }
  
  public void addTomenuPages(MenuPage menuPage) {
    if (null == getMenuPages()) setMenuPages(new ArrayList());
    getMenuPages().add(menuPage);
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof MenuGroup)) { return false;
    }
    MenuGroup menuGroup = (MenuGroup)obj;
    if ((null == getId()) || (null == menuGroup.getId())) return this == obj;
    return getId().equals(menuGroup.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
