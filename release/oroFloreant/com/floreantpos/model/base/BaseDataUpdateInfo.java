package com.floreantpos.model.base;

import com.floreantpos.model.DataUpdateInfo;
import java.io.Serializable;
import java.util.Date;









public abstract class BaseDataUpdateInfo
  implements Comparable, Serializable
{
  public static String REF = "DataUpdateInfo";
  public static String PROP_ID = "id";
  public static String PROP_LAST_UPDATE_TIME = "lastUpdateTime";
  

  public BaseDataUpdateInfo()
  {
    initialize();
  }
  


  public BaseDataUpdateInfo(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  protected Date lastUpdateTime;
  


  protected void initialize() {}
  


  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public Date getLastUpdateTime()
  {
    return lastUpdateTime;
  }
  



  public void setLastUpdateTime(Date lastUpdateTime)
  {
    this.lastUpdateTime = lastUpdateTime;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof DataUpdateInfo)) { return false;
    }
    DataUpdateInfo dataUpdateInfo = (DataUpdateInfo)obj;
    if ((null == getId()) || (null == dataUpdateInfo.getId())) return this == obj;
    return getId().equals(dataUpdateInfo.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
