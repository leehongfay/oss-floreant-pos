package com.floreantpos.model.base;

import com.floreantpos.model.MenuItemSize;
import java.io.Serializable;










public abstract class BaseMenuItemSize
  implements Comparable, Serializable
{
  public static String REF = "MenuItemSize";
  public static String PROP_NAME = "name";
  public static String PROP_DEFAULT_SIZE = "defaultSize";
  public static String PROP_DESCRIPTION = "description";
  public static String PROP_SORT_ORDER = "sortOrder";
  public static String PROP_SIZE_IN_INCH = "sizeInInch";
  public static String PROP_ID = "id";
  public static String PROP_TRANSLATED_NAME = "translatedName";
  

  public BaseMenuItemSize()
  {
    initialize();
  }
  


  public BaseMenuItemSize(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  private long version;
  
  protected String name;
  
  protected String translatedName;
  
  protected String description;
  
  protected Integer sortOrder;
  
  protected Double sizeInInch;
  
  protected Boolean defaultSize;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getTranslatedName()
  {
    return translatedName;
  }
  



  public void setTranslatedName(String translatedName)
  {
    this.translatedName = translatedName;
  }
  




  public String getDescription()
  {
    return description;
  }
  



  public void setDescription(String description)
  {
    this.description = description;
  }
  




  public Integer getSortOrder()
  {
    return sortOrder == null ? Integer.valueOf(0) : sortOrder;
  }
  



  public void setSortOrder(Integer sortOrder)
  {
    this.sortOrder = sortOrder;
  }
  




  public Double getSizeInInch()
  {
    return sizeInInch == null ? Double.valueOf(0.0D) : sizeInInch;
  }
  



  public void setSizeInInch(Double sizeInInch)
  {
    this.sizeInInch = sizeInInch;
  }
  




  public Boolean isDefaultSize()
  {
    return defaultSize == null ? Boolean.FALSE : defaultSize;
  }
  



  public void setDefaultSize(Boolean defaultSize)
  {
    this.defaultSize = defaultSize;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof MenuItemSize)) { return false;
    }
    MenuItemSize menuItemSize = (MenuItemSize)obj;
    if ((null == getId()) || (null == menuItemSize.getId())) return this == obj;
    return getId().equals(menuItemSize.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
