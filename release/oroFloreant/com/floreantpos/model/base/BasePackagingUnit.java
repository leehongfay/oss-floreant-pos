package com.floreantpos.model.base;

import com.floreantpos.model.PackagingUnit;
import java.io.Serializable;










public abstract class BasePackagingUnit
  implements Comparable, Serializable
{
  public static String REF = "PackagingUnit";
  public static String PROP_FACTOR = "factor";
  public static String PROP_SHORT_NAME = "shortName";
  public static String PROP_DIMENSION = "dimension";
  public static String PROP_ID = "id";
  public static String PROP_CODE = "code";
  public static String PROP_RECIPE_UNIT = "recipeUnit";
  public static String PROP_NAME = "name";
  

  public BasePackagingUnit()
  {
    initialize();
  }
  


  public BasePackagingUnit(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  
  private String id;
  
  long version;
  
  protected String code;
  
  protected String name;
  
  protected String shortName;
  
  protected Double factor;
  
  protected String dimension;
  
  protected Boolean recipeUnit;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getCode()
  {
    return code;
  }
  



  public void setCode(String code)
  {
    this.code = code;
  }
  




  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getShortName()
  {
    return shortName;
  }
  



  public void setShortName(String shortName)
  {
    this.shortName = shortName;
  }
  




  public Double getFactor()
  {
    return factor == null ? Double.valueOf(0.0D) : factor;
  }
  



  public void setFactor(Double factor)
  {
    this.factor = factor;
  }
  




  public String getDimension()
  {
    return dimension;
  }
  



  public void setDimension(String dimension)
  {
    this.dimension = dimension;
  }
  




  public Boolean isRecipeUnit()
  {
    return recipeUnit == null ? Boolean.FALSE : recipeUnit;
  }
  



  public void setRecipeUnit(Boolean recipeUnit)
  {
    this.recipeUnit = recipeUnit;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof PackagingUnit)) { return false;
    }
    PackagingUnit packagingUnit = (PackagingUnit)obj;
    if ((null == getId()) || (null == packagingUnit.getId())) return this == obj;
    return getId().equals(packagingUnit.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
