package com.floreantpos.model.base;

import com.floreantpos.model.StoreSession;
import com.floreantpos.model.StoreSessionControl;
import java.io.Serializable;









public abstract class BaseStoreSessionControl
  implements Comparable, Serializable
{
  public static String REF = "StoreSessionControl";
  public static String PROP_ID = "id";
  public static String PROP_CURRENT_DATA = "currentData";
  

  public BaseStoreSessionControl()
  {
    initialize();
  }
  


  public BaseStoreSessionControl(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  

  private long version;
  

  private StoreSession currentData;
  


  protected void initialize() {}
  


  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public StoreSession getCurrentData()
  {
    return currentData;
  }
  



  public void setCurrentData(StoreSession currentData)
  {
    this.currentData = currentData;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof StoreSessionControl)) { return false;
    }
    StoreSessionControl storeSessionControl = (StoreSessionControl)obj;
    if ((null == getId()) || (null == storeSessionControl.getId())) return this == obj;
    return getId().equals(storeSessionControl.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
