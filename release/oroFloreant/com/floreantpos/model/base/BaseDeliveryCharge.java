package com.floreantpos.model.base;

import com.floreantpos.model.DeliveryCharge;
import java.io.Serializable;










public abstract class BaseDeliveryCharge
  implements Comparable, Serializable
{
  public static String REF = "DeliveryCharge";
  public static String PROP_CHARGE_AMOUNT = "chargeAmount";
  public static String PROP_NAME = "name";
  public static String PROP_START_RANGE = "startRange";
  public static String PROP_ID = "id";
  public static String PROP_END_RANGE = "endRange";
  public static String PROP_ZIP_CODE = "zipCode";
  

  public BaseDeliveryCharge()
  {
    initialize();
  }
  


  public BaseDeliveryCharge(String id)
  {
    setId(id);
    initialize();
  }
  




  private int hashCode = Integer.MIN_VALUE;
  

  private String id;
  
  private long version;
  
  protected String name;
  
  protected String zipCode;
  
  protected Double startRange;
  
  protected Double endRange;
  
  protected Double chargeAmount;
  

  protected void initialize() {}
  

  public String getId()
  {
    return id;
  }
  



  public void setId(String id)
  {
    this.id = id;
    hashCode = Integer.MIN_VALUE;
  }
  




  public long getVersion()
  {
    return version;
  }
  



  public void setVersion(long version)
  {
    this.version = version;
  }
  





  public String getName()
  {
    return name;
  }
  



  public void setName(String name)
  {
    this.name = name;
  }
  




  public String getZipCode()
  {
    return zipCode;
  }
  



  public void setZipCode(String zipCode)
  {
    this.zipCode = zipCode;
  }
  




  public Double getStartRange()
  {
    return startRange == null ? Double.valueOf(0.0D) : startRange;
  }
  



  public void setStartRange(Double startRange)
  {
    this.startRange = startRange;
  }
  




  public Double getEndRange()
  {
    return endRange == null ? Double.valueOf(0.0D) : endRange;
  }
  



  public void setEndRange(Double endRange)
  {
    this.endRange = endRange;
  }
  




  public Double getChargeAmount()
  {
    return chargeAmount == null ? Double.valueOf(0.0D) : chargeAmount;
  }
  



  public void setChargeAmount(Double chargeAmount)
  {
    this.chargeAmount = chargeAmount;
  }
  



  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof DeliveryCharge)) { return false;
    }
    DeliveryCharge deliveryCharge = (DeliveryCharge)obj;
    if ((null == getId()) || (null == deliveryCharge.getId())) return this == obj;
    return getId().equals(deliveryCharge.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
