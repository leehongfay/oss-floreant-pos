package com.floreantpos.model.base;

import com.floreantpos.model.CreditCardTransaction;
import com.floreantpos.model.PosTransaction;
import java.io.Serializable;








public abstract class BaseCreditCardTransaction
  extends PosTransaction
  implements Comparable, Serializable
{
  public static String REF = "CreditCardTransaction";
  public static String PROP_ID = "id";
  

  public BaseCreditCardTransaction()
  {
    initialize();
  }
  


  public BaseCreditCardTransaction(String id)
  {
    super(id);
  }
  






  public BaseCreditCardTransaction(String id, String transactionType, String paymentType)
  {
    super(id, transactionType, paymentType);
  }
  





  private int hashCode = Integer.MIN_VALUE;
  







  public boolean equals(Object obj)
  {
    if (null == obj) return false;
    if (!(obj instanceof CreditCardTransaction)) { return false;
    }
    CreditCardTransaction creditCardTransaction = (CreditCardTransaction)obj;
    if ((null == getId()) || (null == creditCardTransaction.getId())) return this == obj;
    return getId().equals(creditCardTransaction.getId());
  }
  
  public int hashCode()
  {
    if (Integer.MIN_VALUE == hashCode) {
      if (null == getId()) { return super.hashCode();
      }
      String hashStr = getClass().getName() + ":" + getId().hashCode();
      hashCode = hashStr.hashCode();
    }
    
    return hashCode;
  }
  
  public int compareTo(Object obj) {
    if (obj.hashCode() > hashCode()) return 1;
    if (obj.hashCode() < hashCode()) return -1;
    return 0;
  }
  
  public String toString() {
    return super.toString();
  }
}
