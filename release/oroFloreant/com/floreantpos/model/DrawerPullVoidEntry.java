package com.floreantpos.model;

import com.floreantpos.model.base.BaseDrawerPullVoidTicketEntry;

public class DrawerPullVoidEntry
  extends BaseDrawerPullVoidTicketEntry
{
  private static final long serialVersionUID = 1L;
  
  public DrawerPullVoidEntry() {}
}
