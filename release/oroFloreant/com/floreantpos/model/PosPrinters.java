package com.floreantpos.model;

import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.dao.TerminalPrintersDAO;
import java.io.File;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.io.FileUtils;



















@XmlRootElement(name="printers")
public class PosPrinters
{
  private static String reportPrinter;
  private static String receiptPrinter;
  private static String labelPrinter;
  private static String stickerPrinter;
  private Printer defaultKitchenPrinter;
  private static List<Printer> kitchenPrinters;
  
  public PosPrinters() {}
  
  private Map<VirtualPrinter, Printer> kitchePrinterMap = new HashMap();
  
  public String getReportPrinter() {
    return reportPrinter;
  }
  
  public void setReportPrinter(String reportPrinter) {
    reportPrinter = reportPrinter;
  }
  
  public String getReceiptPrinter() {
    return receiptPrinter;
  }
  
  public void setReceiptPrinter(String receiptPrinter) {
    receiptPrinter = receiptPrinter;
  }
  
  public List<Printer> getKitchenPrinters() {
    if (kitchenPrinters == null) {
      kitchenPrinters = new ArrayList(4);
    }
    
    return kitchenPrinters;
  }
  
  public void setKitchenPrinters(List<Printer> kitchenPrinters) {
    kitchenPrinters = kitchenPrinters;
  }
  
  public void addKitchenPrinter(Printer printer) {
    getKitchenPrinters().add(printer);
  }
  
  public void setDefaultKitchenPrinter(Printer defaultKitchenPrinter) {
    this.defaultKitchenPrinter = defaultKitchenPrinter;
  }
  
  public Printer getDefaultKitchenPrinter() {
    if (getKitchenPrinters().size() > 0) {
      defaultKitchenPrinter = ((Printer)kitchenPrinters.get(0));
      
      for (Printer printer : kitchenPrinters) {
        if (printer.isDefaultPrinter()) {
          defaultKitchenPrinter = printer;
          break;
        }
      }
    }
    
    return defaultKitchenPrinter;
  }
  














  public Printer getKitchenPrinterFor(VirtualPrinter vp)
  {
    return (Printer)kitchePrinterMap.get(vp);
  }
  
  private void populatePrinterMaps() {
    kitchePrinterMap.clear();
    
    for (Printer printer : getKitchenPrinters()) {
      kitchePrinterMap.put(printer.getVirtualPrinter(), printer);
    }
  }
  
  public void save() {
    try {
      getDefaultKitchenPrinter();
      
      populatePrinterMaps();
      
      File file = new File("config", "printers.xml");
      
      JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] { PosPrinters.class });
      Marshaller m = jaxbContext.createMarshaller();
      m.setProperty("jaxb.formatted.output", Boolean.TRUE);
      
      StringWriter writer = new StringWriter();
      m.marshal(this, writer);
      
      FileUtils.write(file, writer.toString());
    }
    catch (Exception e) {
      PosLog.error(getClass(), e);
    }
  }
  
  public static PosPrinters load() {
    return load(Application.getInstance().getTerminal());
  }
  
  public static PosPrinters load(Terminal terminal) {
    try {
      List<TerminalPrinters> terminalPrinters = TerminalPrintersDAO.getInstance().findTerminalPrinters(terminal);
      
      PosPrinters printers = new PosPrinters();
      
      List<Printer> terminalActivePrinters = new ArrayList();
      for (TerminalPrinters terminalPrinter : terminalPrinters) {
        int printerType = terminalPrinter.getVirtualPrinter().getType().intValue();
        if (printerType == 0) {
          if (terminalPrinter.getPrinterName() != null) {
            reportPrinter = terminalPrinter.getPrinterName();
          }
        }
        else if (printerType == 5) {
          labelPrinter = terminalPrinter.getPrinterName();
        }
        else if (printerType == 1) {
          receiptPrinter = terminalPrinter.getPrinterName();
        }
        else if (printerType != 4)
        {
          if (printerType == 6) {
            stickerPrinter = terminalPrinter.getPrinterName();
          }
          else {
            Printer printer = new Printer(terminalPrinter.getVirtualPrinter(), terminalPrinter.getPrinterName());
            terminalActivePrinters.add(printer);
          } }
      }
      kitchenPrinters = terminalActivePrinters;
      
      if (receiptPrinter == null) {
        receiptPrinter = getDefaultPrinterName();
      }
      if (reportPrinter == null) {
        reportPrinter = getDefaultPrinterName();
      }
      if ((kitchenPrinters == null) || (kitchenPrinters.isEmpty())) {
        Printer printer = new Printer(new VirtualPrinter("1", "kitchen"), getDefaultPrinterName());
        kitchenPrinters.add(printer);
      }
      printers.populatePrinterMaps();
      
      return printers;
    }
    catch (Exception e) {
      PosLog.error(PosPrinters.class, e);
    }
    
    return null;
  }
  
  public static String getDefaultPrinterName() {
    PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();
    if (defaultPrintService != null) {
      return defaultPrintService.getName();
    }
    
    PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);
    if (services.length > 0) {
      return services[0].getName();
    }
    return null;
  }
  





















  public String getLabelPrinter()
  {
    return labelPrinter;
  }
  
  public void setLabelPrinter(String labelPrinter) {
    labelPrinter = labelPrinter;
  }
  
  public static String getStickerPrinter() {
    return stickerPrinter;
  }
  
  public static void setStickerPrinter(String stickerPrinter) {
    stickerPrinter = stickerPrinter;
  }
}
