package com.floreantpos.model;

import com.floreantpos.model.base.BaseMenuItemModifierPage;
import com.floreantpos.model.dao.MenuItemModifierSpecDAO;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;







public class MenuItemModifierPage
  extends BaseMenuItemModifierPage
{
  private static final long serialVersionUID = 1L;
  private Map<String, MenuItemModifierPageItem> cellItemMap;
  private MenuItemModifierSpec modifierSpec;
  
  public MenuItemModifierPage() {}
  
  public MenuItemModifierPage(String id)
  {
    super(id);
  }
  



  public MenuItemModifierPage(String id, String name)
  {
    super(id, name);
  }
  


  public Integer getButtonWidth()
  {
    Integer buttonWidth = super.getButtonWidth();
    if (buttonWidth.intValue() < 30) {
      return Integer.valueOf(30);
    }
    return buttonWidth;
  }
  
  public Integer getButtonHeight()
  {
    Integer buttonHeight = super.getButtonHeight();
    if (buttonHeight.intValue() < 30) {
      return Integer.valueOf(30);
    }
    return buttonHeight;
  }
  
  public MenuItemModifierPageItem getItemForCell(int col, int row) {
    List<MenuItemModifierPageItem> pageItems = getPageItems();
    if ((pageItems == null) || (pageItems.size() == 0)) {
      return null;
    }
    if (cellItemMap == null) {
      cellItemMap = new HashMap();
      for (MenuItemModifierPageItem menuPageItem : pageItems) {
        String cellKey = String.valueOf(menuPageItem.getCol()) + String.valueOf(menuPageItem.getRow());
        cellItemMap.put(cellKey, menuPageItem);
      }
    }
    String cellKey = String.valueOf(col) + String.valueOf(row);
    return (MenuItemModifierPageItem)cellItemMap.get(cellKey);
  }
  
  public String toString()
  {
    return super.getName();
  }
  
  public void setModifierSpec(MenuItemModifierSpec modifierSpec) {
    this.modifierSpec = modifierSpec;
    setModifierSpecId(modifierSpec == null ? null : modifierSpec.getId());
  }
  
  public MenuItemModifierSpec getModifierSpec() {
    if (modifierSpec != null) {
      return modifierSpec;
    }
    if (StringUtils.isNotEmpty(getModifierSpecId())) {
      return MenuItemModifierSpecDAO.getInstance().get(getModifierSpecId());
    }
    return null;
  }
}
