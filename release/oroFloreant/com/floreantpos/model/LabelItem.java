package com.floreantpos.model;

public class LabelItem
{
  private MenuItem menuItem;
  private int printQuantity;
  private String name;
  private String barcode;
  private double memberPrice;
  private double retailPrice;
  
  public LabelItem() {}
  
  public String getName() {
    return menuItem.getName();
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getBarcode() {
    return menuItem.getBarcode();
  }
  
  public void setBarcode(String barcode) {
    this.barcode = barcode;
  }
  
  public double getMemberPrice() {
    return menuItem.getPrice().doubleValue();
  }
  
  public void setMemberPrice(double memberPrice) {
    this.memberPrice = memberPrice;
  }
  
  public double getRetailPrice() {
    return menuItem.getRetailPrice().doubleValue();
  }
  
  public void setRetailPrice(double retailPrice) {
    this.retailPrice = retailPrice;
  }
  
  public MenuItem getMenuItem() {
    return menuItem;
  }
  
  public void setMenuItem(MenuItem menuItem) {
    this.menuItem = menuItem;
  }
  
  public int getPrintQuantity() {
    return printQuantity;
  }
  
  public void setPrintQuantity(int printQuantity) {
    this.printQuantity = printQuantity;
  }
}
