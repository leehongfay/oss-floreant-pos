package com.floreantpos.model;

import com.floreantpos.model.base.BaseInventoryStockUnit;


public class InventoryStockUnit
  extends BaseInventoryStockUnit
  implements IUnit
{
  private static final long serialVersionUID = 1L;
  
  public InventoryStockUnit() {}
  
  public InventoryStockUnit(String id)
  {
    super(id);
  }
  

  public void calculateBaseUnitValue()
  {
    double conversionValue = getConversionValue().doubleValue();
    double unitFactor = getUnit().getConversionRate().doubleValue();
    setBaseUnitValue(Double.valueOf(conversionValue / unitFactor));
  }
  
  public String getUniqueCode()
  {
    return super.getPackagingUnit().getCode();
  }
  
  public String toString()
  {
    return getUniqueCode();
  }
}
