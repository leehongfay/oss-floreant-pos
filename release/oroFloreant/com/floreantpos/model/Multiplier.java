package com.floreantpos.model;

import com.floreantpos.model.base.BaseMultiplier;

public class Multiplier extends BaseMultiplier {
  private static final long serialVersionUID = 1L;
  public static final String REGULAR = "Regular";
  
  public Multiplier() {
    setGlobalId(com.floreantpos.util.GlobalIdGenerator.generateGlobalId());
  }
  
  public Multiplier(String name) {
    super(name);
    setGlobalId(com.floreantpos.util.GlobalIdGenerator.generateGlobalId());
  }
  
  public Integer getButtonColor()
  {
    return buttonColor;
  }
  
  public Integer getTextColor()
  {
    return textColor;
  }
  
  public String toString()
  {
    return super.getId();
  }
}
