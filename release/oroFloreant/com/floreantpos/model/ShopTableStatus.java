package com.floreantpos.model;

import com.floreantpos.model.base.BaseShopTableStatus;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang.StringUtils;




public class ShopTableStatus
  extends BaseShopTableStatus
{
  private static final long serialVersionUID = 1L;
  
  public ShopTableStatus() {}
  
  public ShopTableStatus(Integer id)
  {
    super(id);
  }
  

  public TableStatus getTableStatus()
  {
    Integer tableStatus = super.getTableStatusNum();
    return TableStatus.get(tableStatus.intValue());
  }
  
  public void setTableStatus(TableStatus tableStatus) {
    super.setTableStatusNum(Integer.valueOf(tableStatus.getValue()));
  }
  
  public List<ShopTableTicket> getTicketNumbers() {
    String ticketInformations = getTicketInformations();
    if (StringUtils.isEmpty(ticketInformations)) {
      return null;
    }
    Gson gson = new Gson();
    List<ShopTableTicket> fromJson = (List)gson.fromJson(ticketInformations, new TypeToken() {}.getType());
    return fromJson;
  }
  
  public void setTicketNumbers(List<ShopTableTicket> ticketNumbers) {
    if ((ticketNumbers == null) || (ticketNumbers.size() == 0)) {
      setTicketInformations(null);
      return;
    }
    JsonArray jsonArray = new JsonArray();
    for (ShopTableTicket shopTableTicket : ticketNumbers) {
      jsonArray.add(shopTableTicket.toJson());
    }
    setTicketInformations(jsonArray.toString());
  }
  
  public String getTicketId()
  {
    List<String> ticketNumbers = getListOfTicketNumbers();
    if ((ticketNumbers != null) && (ticketNumbers.size() > 0))
      return (String)ticketNumbers.get(0);
    return null;
  }
  
  public List<String> getListOfTicketNumbers() {
    List<ShopTableTicket> shopTableTickets = getTicketNumbers();
    List<String> listOfTicketNumbers = new ArrayList();
    if (shopTableTickets != null) {
      for (ShopTableTicket shopTableTicket : shopTableTickets) {
        listOfTicketNumbers.add(shopTableTicket.getTicketId());
      }
    }
    return listOfTicketNumbers;
  }
  
  public boolean hasMultipleTickets()
  {
    List<ShopTableTicket> ticketNumbers = getTicketNumbers();
    if ((ticketNumbers != null) && (ticketNumbers.size() > 0))
      return true;
    return false;
  }
  
  public void setTicketId(String ticketId) {
    setTableTicket(ticketId, null, null, null);
  }
  
  public void setTableTicket(String ticketId, Integer tokenNo, String userId, String userFirstName) {
    if (ticketId == null) {
      setTableStatus(TableStatus.Available);
      setTicketNumbers(null);
    }
    else {
      ShopTableTicket shopTableTicket = null;
      List<ShopTableTicket> shopTableTickets = getTicketNumbers();
      if ((shopTableTickets != null) && (!shopTableTickets.isEmpty())) {
        for (ShopTableTicket shopT : shopTableTickets) {
          if (shopT.getTicketId().equals(ticketId)) {
            shopTableTicket = shopT;
          }
        }
      } else {
        shopTableTickets = new ArrayList();
      }
      if (shopTableTicket == null) {
        shopTableTicket = new ShopTableTicket();
        shopTableTickets.add(shopTableTicket);
      }
      shopTableTicket.setTicketId(ticketId);
      shopTableTicket.setTokenNo(tokenNo);
      shopTableTicket.setUserId(userId);
      shopTableTicket.setUserName(userFirstName);
      setTicketNumbers(shopTableTickets);
    }
  }
  
  public void addToTableTickets(List<Ticket> tickets) {
    if (tickets == null)
      return;
    List<String> existingTicketIds = new ArrayList();
    List<ShopTableTicket> shopTableTickets = getTicketNumbers();
    if (shopTableTickets == null) {
      shopTableTickets = new ArrayList();
      setTicketNumbers(shopTableTickets);
    }
    for (ShopTableTicket shopTableTicket : shopTableTickets) {
      String ticketId = shopTableTicket.getTicketId();
      if (ticketId != null)
        existingTicketIds.add(ticketId);
    }
    for (Ticket ticket : tickets)
      if (!existingTicketIds.contains(ticket.getId()))
      {
        ShopTableTicket shopTableTicket = new ShopTableTicket();
        shopTableTicket.setTicketId(ticket.getId());
        shopTableTicket.setTokenNo(ticket.getTokenNo());
        shopTableTicket.setUserId(ticket.getOwner().getId());
        shopTableTicket.setUserName(ticket.getOwner().getFirstName());
        shopTableTickets.add(shopTableTicket);
      }
  }
  
  public String getUserId() {
    List<ShopTableTicket> shopTableTickets = getTicketNumbers();
    if ((shopTableTickets == null) || (shopTableTickets.isEmpty()))
      return null;
    return ((ShopTableTicket)shopTableTickets.get(0)).getUserId();
  }
  
  public String getUserName() {
    List<ShopTableTicket> shopTableTickets = getTicketNumbers();
    if ((shopTableTickets == null) || (shopTableTickets.isEmpty()))
      return "";
    int size = shopTableTickets.size();
    if (size > 1) {
      List<String> userIds = new ArrayList();
      for (Iterator iterator = shopTableTickets.iterator(); iterator.hasNext();) {
        ShopTableTicket shopTableTicket = (ShopTableTicket)iterator.next();
        if (!userIds.contains(shopTableTicket.getUserId()))
        {
          userIds.add(shopTableTicket.getUserId()); }
      }
      if (userIds.size() > 1)
        return "Multi owner";
    }
    return ((ShopTableTicket)shopTableTickets.get(0)).getUserName();
  }
  
  public String getTokenNo() {
    List<ShopTableTicket> shopTableTickets = getTicketNumbers();
    if ((shopTableTickets == null) || (shopTableTickets.isEmpty()))
      return "";
    int size = shopTableTickets.size();
    if (size == 1) {
      return String.valueOf(((ShopTableTicket)shopTableTickets.get(0)).getTokenNo());
    }
    String displayString = "";
    int count = 1;
    for (Iterator iterator = shopTableTickets.iterator(); iterator.hasNext();) {
      ShopTableTicket shopTableTicket = (ShopTableTicket)iterator.next();
      displayString = displayString + String.valueOf(shopTableTicket.getTokenNo());
      if (count == 4)
        break;
      count++;
      if (iterator.hasNext()) {
        displayString = displayString + ",";
      }
    }
    return displayString;
  }
}
