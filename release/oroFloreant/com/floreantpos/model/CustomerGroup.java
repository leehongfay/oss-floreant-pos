package com.floreantpos.model;

import com.floreantpos.model.base.BaseCustomerGroup;



public class CustomerGroup
  extends BaseCustomerGroup
{
  private static final long serialVersionUID = 1L;
  
  public CustomerGroup() {}
  
  public CustomerGroup(String id)
  {
    super(id);
  }
}
