package com.floreantpos.model;

import java.sql.Blob;
import javax.sql.rowset.serial.SerialBlob;
import org.apache.commons.codec.binary.Base64;

public class BlobXmlAdapter extends javax.xml.bind.annotation.adapters.XmlAdapter<String, Blob>
{
  public BlobXmlAdapter() {}
  
  public Blob unmarshal(String v) throws Exception
  {
    if (v == null)
      v = "";
    byte[] bytes = Base64.decodeBase64(v);
    return new SerialBlob(bytes);
  }
  
  public String marshal(Blob v) throws Exception
  {
    if (v == null)
      return "";
    return v.getBytes(1L, (int)v.length()).toString();
  }
}
