package com.floreantpos.model;

import com.floreantpos.model.base.BaseTicketItemSeat;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.util.DataProvider;
import org.apache.commons.lang.StringUtils;


public class TicketItemSeat
  extends BaseTicketItemSeat
{
  private static final long serialVersionUID = 1L;
  private ShopSeat shopSeat;
  private Customer member;
  
  public TicketItemSeat() {}
  
  public TicketItemSeat(String id)
  {
    super(id);
  }
  




  public void setShopSeat(ShopSeat shopSeat)
  {
    this.shopSeat = shopSeat;
    setSeatId(shopSeat == null ? null : shopSeat.getId());
    setSeatNumber(shopSeat == null ? null : shopSeat.getSeatNumber());
  }
  
  public ShopSeat getShopSeat() {
    if (shopSeat != null) {
      return shopSeat;
    }
    if (getSeatId() != null) {
      shopSeat = ((ShopSeat)DataProvider.get().getObjectOf(ShopSeat.class, getSeatId()));
    }
    return shopSeat;
  }
  
  public void setMember(Customer member) {
    this.member = member;
    setMemberId(member == null ? null : member.getId());
  }
  
  public Customer getMember() {
    String customerId = getMemberId();
    if (StringUtils.isEmpty(customerId)) {
      return null;
    }
    if ((member != null) && (member.getId().equals(customerId))) {
      return member;
    }
    member = CustomerDAO.getInstance().get(customerId);
    return member;
  }
}
