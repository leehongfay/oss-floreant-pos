package com.floreantpos.model;

import com.floreantpos.model.base.BaseSubRecipe;
import com.floreantpos.util.NumberUtil;
import java.util.List;




public class SubRecipe
  extends BaseSubRecipe
{
  private static final long serialVersionUID = 1L;
  private Double cost;
  
  public SubRecipe() {}
  
  public Double getCost()
  {
    calculateCost();
    cost = Double.valueOf(NumberUtil.roundToTwoDigit(cost.doubleValue()));
    return Double.valueOf(cost == null ? 0.0D : cost.doubleValue());
  }
  
  public void setCost(Double cost) {
    this.cost = cost;
  }
  
  private void calculateCost() {
    double portionCost = 0.0D;
    double recipeCost = 0.0D;
    Recepie recipe = getRecipe();
    if (recipe != null) {
      List<RecepieItem> items = recipe.getRecepieItems();
      if ((items != null) && (items.size() > 0)) {
        for (RecepieItem recepieItem : items) {
          recepieItem.calculatePercentage();
          recipeCost += recepieItem.getCost();
        }
      }
    }
    portionCost = recipeCost * getPortion().doubleValue() / recipe.getYield().doubleValue();
    setCost(Double.valueOf(portionCost));
  }
  
  public String getName() {
    Recepie recipe = getRecipe();
    if (recipe != null)
      return recipe.getName();
    return "";
  }
  
  public Double getYield() {
    Recepie recipe = getRecipe();
    if (recipe != null)
      return recipe.getYield();
    return Double.valueOf(1.0D);
  }
}
