package com.floreantpos.model;

import com.floreantpos.model.base.BaseDeliveryConfiguration;

public class DeliveryConfiguration
  extends BaseDeliveryConfiguration
{
  private static final long serialVersionUID = 1L;
  public static final String UNIT_KM = "KM";
  public static final String UNIT_MILE = "MILE";
  public static final String DELIVERY_CONFIG_LENGTH_UNIT_NAME = "deliveryConfig.unitName";
  public static final String DELIVERY_CONFIG_UNIT_SYMBOL = "deliveryConfig.unitSymbol";
  public static final String DELIVERY_CONFIG_CHARGE_BY_ZIP_CODE = "deliveryConfig.zipcode";
  public static final String DELIVERY_CONFIG_HIGHLIGHT_BEFORE_MINUTES = "deliveryConfig.highlightBeforeMinute";
  public static final String DELIVERY_CONFIG_PREPERATION_TIME = "deliveryConfig.preperationTime";
  public static final String GOOGLE_MAP_API_KEY = "google.map.api.key";
  
  public DeliveryConfiguration() {}
}
