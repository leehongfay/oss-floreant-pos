package com.floreantpos.model;

import com.floreantpos.model.base.BaseMenuItemShift;
import javax.xml.bind.annotation.XmlRootElement;





















@XmlRootElement(name="menu-item-shift")
public class MenuItemShift
  extends BaseMenuItemShift
{
  private static final long serialVersionUID = 1L;
  
  public MenuItemShift() {}
  
  public MenuItemShift(String id)
  {
    super(id);
  }
}
