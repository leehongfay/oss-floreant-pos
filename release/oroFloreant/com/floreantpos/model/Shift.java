package com.floreantpos.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.floreantpos.model.base.BaseShift;
import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;




















@JsonIgnoreProperties(ignoreUnknown=true, value={"dayOfWeekAsString"})
@XmlRootElement(name="shift")
public class Shift
  extends BaseShift
{
  private static final long serialVersionUID = 1L;
  
  public Shift() {}
  
  public Shift(String id)
  {
    super(id);
  }
  





  public Shift(String id, String name)
  {
    super(id, name);
  }
  




  public Long getShiftLength()
  {
    Long shiftLength2 = super.getShiftLength();
    if (shiftLength2 == null) {
      return Long.valueOf(Math.abs(getStartTime().getTime() - getEndTime().getTime()));
    }
    return shiftLength2;
  }
  
  public boolean equals(Object obj)
  {
    if (!(obj instanceof Shift)) {
      return false;
    }
    return getName().equalsIgnoreCase(((Shift)obj).getName());
  }
  
  public String toString()
  {
    return getName();
  }
}
