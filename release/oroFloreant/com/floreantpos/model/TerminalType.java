package com.floreantpos.model;

import com.floreantpos.model.base.BaseTerminalType;




public class TerminalType
  extends BaseTerminalType
  implements IdContainer
{
  private static final long serialVersionUID = 1L;
  
  public TerminalType() {}
  
  public TerminalType(String id)
  {
    super(id);
  }
  





  public TerminalType(String id, String name)
  {
    super(id, name);
  }
  




  public String toString()
  {
    return super.getName();
  }
}
