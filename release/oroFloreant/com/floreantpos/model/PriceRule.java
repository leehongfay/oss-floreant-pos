package com.floreantpos.model;

import com.floreantpos.model.base.BasePriceRule;
import com.floreantpos.model.dao.DepartmentDAO;
import com.floreantpos.model.dao.PriceShiftDAO;
import com.floreantpos.model.dao.PriceTableDAO;
import com.floreantpos.model.dao.SalesAreaDAO;
import org.apache.commons.lang.StringUtils;

public class PriceRule extends BasePriceRule
{
  private static final long serialVersionUID = 1L;
  private Department department;
  private Outlet outlet;
  private SalesArea salesArea;
  private OrderType orderType;
  private CustomerGroup customerGroup;
  private PriceShift priceShift;
  private PriceTable priceTable;
  
  public PriceRule() {}
  
  public PriceRule(String id)
  {
    super(id);
  }
  


  public String toString()
  {
    return super.getName();
  }
  







  public Department getDepartment()
  {
    String departmentId = getDepartmentId();
    if (StringUtils.isEmpty(departmentId))
      return null;
    if ((department == null) || (!department.getId().equals(departmentId))) {
      department = DepartmentDAO.getInstance().get(departmentId);
    }
    return department;
  }
  
  public void setDepartment(Department department) {
    this.department = department;
    String departmentId = null;
    if (department != null) {
      departmentId = department.getId();
    }
    super.setDepartmentId(departmentId);
  }
  
  public Outlet getOutlet() {
    String outletId = getOutletId();
    if (StringUtils.isEmpty(outletId))
      return null;
    if ((outlet == null) || (!outlet.getId().equals(outletId))) {
      outlet = com.floreantpos.model.dao.OutletDAO.getInstance().get(outletId);
    }
    return outlet;
  }
  
  public void setOutlet(Outlet outlet) {
    this.outlet = outlet;
    String outletId = null;
    if (outlet != null) {
      outletId = outlet.getId();
    }
    super.setOutletId(outletId);
  }
  
  public SalesArea getSalesArea() {
    String salesAreaId = getSalesAreaId();
    if (StringUtils.isEmpty(salesAreaId))
      return null;
    if ((salesArea == null) || (!salesArea.getId().equals(salesAreaId))) {
      salesArea = SalesAreaDAO.getInstance().get(salesAreaId);
    }
    return salesArea;
  }
  
  public void setSalesArea(SalesArea salesArea) {
    this.salesArea = salesArea;
    String salesAreaId = null;
    if (salesArea != null) {
      salesAreaId = salesArea.getId();
    }
    super.setSalesAreaId(salesAreaId);
  }
  
  public CustomerGroup getCustomerGroup() {
    String customerGroupId = getCustomerGroupId();
    if (StringUtils.isEmpty(customerGroupId))
      return null;
    if ((customerGroup == null) || (!customerGroup.getId().equals(customerGroupId))) {
      customerGroup = com.floreantpos.model.dao.CustomerGroupDAO.getInstance().get(customerGroupId);
    }
    return customerGroup;
  }
  
  public void setCustomerGroup(CustomerGroup customerGroup) {
    this.customerGroup = customerGroup;
    String customerGroupId = null;
    if (customerGroup != null) {
      customerGroupId = customerGroup.getId();
    }
    super.setCustomerGroupId(customerGroupId);
  }
  
  public PriceShift getPriceShift() {
    String priceShiftId = getPriceShiftId();
    if (StringUtils.isEmpty(priceShiftId))
      return null;
    if ((priceShift == null) || (!priceShift.getId().equals(priceShiftId))) {
      priceShift = PriceShiftDAO.getInstance().get(priceShiftId);
    }
    return priceShift;
  }
  
  public void setPriceShift(PriceShift priceShift) {
    this.priceShift = priceShift;
    String priceShiftId = null;
    if (priceShift != null) {
      priceShiftId = priceShift.getId();
    }
    super.setPriceShiftId(priceShiftId);
  }
  
  public OrderType getOrderType() {
    String orderTypeId = getOrderTypeId();
    if (StringUtils.isEmpty(orderTypeId))
      return null;
    if ((orderType == null) || (!orderType.getId().equals(orderTypeId))) {
      orderType = com.floreantpos.model.dao.OrderTypeDAO.getInstance().get(orderTypeId);
    }
    return orderType;
  }
  
  public void setOrderType(OrderType orderType) {
    this.orderType = orderType;
    String orderTypeId = null;
    if (orderType != null) {
      orderTypeId = orderType.getId();
    }
    super.setOrderTypeId(orderTypeId);
  }
  
  public PriceTable getPriceTable() {
    String priceTableId = getPriceTableId();
    if (StringUtils.isEmpty(priceTableId))
      return null;
    if ((priceTable == null) || (!priceTable.getId().equals(priceTableId))) {
      priceTable = PriceTableDAO.getInstance().get(priceTableId);
    }
    return priceTable;
  }
  
  public void setPriceTable(PriceTable priceTable) {
    this.priceTable = priceTable;
    String priceTableId = null;
    if (priceTable != null) {
      priceTableId = priceTable.getId();
    }
    super.setPriceTableId(priceTableId);
  }
}
