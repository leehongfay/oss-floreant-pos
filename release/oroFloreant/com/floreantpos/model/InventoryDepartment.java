package com.floreantpos.model;

import com.floreantpos.model.base.BaseInventoryDepartment;





public class InventoryDepartment
  extends BaseInventoryDepartment
{
  private static final long serialVersionUID = 1L;
  
  public InventoryDepartment() {}
  
  public InventoryDepartment(String id)
  {
    super(id);
  }
}
