package com.floreantpos.model;

import java.awt.Color;

public enum BookingStatus {
  Confirm(1),  Request(2),  Complete(3),  Cancel(4),  NoAppear(5);
  
  private final int value;
  private Color bgColor;
  private Color textColor;
  private Boolean enabled;
  
  private BookingStatus(int value) {
    this.value = value;
  }
  
  private BookingStatus(int value, Color bgColor, Color textColor) {
    this.value = value;
    this.bgColor = bgColor;
    this.textColor = textColor;
  }
  
  private BookingStatus(int value, Color bgColor, Color textColor, Boolean enabled) {
    this.value = value;
    this.bgColor = bgColor;
    this.textColor = textColor;
    this.enabled = enabled;
  }
  
  public int getValue() {
    return value;
  }
  
  public static BookingStatus get(int value) {
    switch (value) {
    case 1: 
      return Confirm;
    case 2: 
      return Request;
    case 3: 
      return Complete;
    case 4: 
      return Cancel;
    }
    
    return null;
  }
  

  public String toString()
  {
    return name();
  }
  
  public Color getBgColor() {
    return bgColor == null ? Color.WHITE : bgColor;
  }
  
  public Color getTextColor() {
    return textColor == null ? Color.BLACK : textColor;
  }
  
  public Boolean getEnabled() {
    return enabled;
  }
}
