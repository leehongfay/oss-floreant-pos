package com.floreantpos.model;

import com.floreantpos.Messages;
import org.apache.commons.lang.StringUtils;


















public enum CardReader
{
  SWIPE(Messages.getString("CardReader.0")), 
  MANUAL(Messages.getString("CardReader.1")), 
  EXTERNAL_TERMINAL(Messages.getString("CardReader.2")), 
  PAX("PAX"), 
  CHIP("CHIP");
  
  private String type;
  
  private CardReader(String typeString)
  {
    type = typeString;
  }
  
  public String getType() {
    return type;
  }
  
  public static CardReader fromString(String name) {
    if (StringUtils.isEmpty(name)) {
      return null;
    }
    
    return valueOf(name);
  }
  
  public String toString()
  {
    return type;
  }
}
