package com.floreantpos.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.floreantpos.IconFactory;
import com.floreantpos.main.Application;
import com.floreantpos.model.base.BaseMenuModifier;
import com.floreantpos.model.util.DataProvider;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.lang.StringUtils;





















@JsonIgnoreProperties(ignoreUnknown=true, value={"buttonColor", "textColor", "image", "properties", "pizzaModifierPriceList", "multiplierPriceList"})
@XmlRootElement
public class MenuModifier
  extends BaseMenuModifier
{
  private static final long serialVersionUID = 1L;
  private transient MenuItemModifierSpec menuItemModifierGroup;
  private String pageItemId;
  
  public MenuModifier() {}
  
  public MenuModifier(String id)
  {
    super(id);
  }
  




  public TaxGroup getTaxGroup()
  {
    return DataProvider.get().getTaxGroupById(getTaxGroupId());
  }
  
  public void setTaxGroup(TaxGroup taxGroup) {
    String taxGroupId = null;
    if (taxGroup != null) {
      taxGroupId = taxGroup.getId();
    }
    super.setTaxGroupId(taxGroupId);
  }
  
  public MenuItemModifierSpec getMenuItemModifierGroup() {
    return menuItemModifierGroup;
  }
  
  public void setMenuItemModifierGroup(MenuItemModifierSpec menuItemModifierGroup) {
    this.menuItemModifierGroup = menuItemModifierGroup;
  }
  
  public Integer getSortOrder()
  {
    return Integer.valueOf(sortOrder == null ? 9999 : sortOrder.intValue());
  }
  
  public Integer getButtonColor()
  {
    return buttonColor;
  }
  
  public Integer getTextColor()
  {
    return textColor;
  }
  
  @JsonIgnore
  public String getDisplayName() {
    Terminal terminal = Application.getInstance().getTerminal();
    if ((terminal != null) && (terminal.isTranslatedName().booleanValue()) && (StringUtils.isNotEmpty(getTranslatedName()))) {
      return getTranslatedName();
    }
    
    return super.getName();
  }
  
  public String toString()
  {
    return getName();
  }
  
  public String getUniqueId() {
    return ("menu_modifier_" + getName() + "_" + getId()).replaceAll("\\s+", "_");
  }
  










































  public double getPriceForSize(MenuItemSize size, boolean extra)
  {
    double defaultPrice = getPrice().doubleValue();
    if (size == null) {
      return defaultPrice;
    }
    
    List<PizzaModifierPrice> priceList = getPizzaModifierPriceList();
    if (priceList == null) {
      return defaultPrice;
    }
    
    for (PizzaModifierPrice pizzaModifierPrice : priceList) {
      if (size.equals(pizzaModifierPrice.getSize())) {
        if (extra) {
          return pizzaModifierPrice.getExtraPrice();
        }
        return pizzaModifierPrice.getPrice();
      }
    }
    
    return defaultPrice;
  }
  
  public double getPriceForMultiplier(Multiplier multiplier) {
    double defaultPrice = getPrice().doubleValue();
    if ((multiplier == null) || (multiplier.isMain().booleanValue())) {
      return defaultPrice;
    }
    
    List<ModifierMultiplierPrice> priceList = getMultiplierPriceList();
    if ((priceList == null) || (priceList.isEmpty())) {
      return defaultPrice * multiplier.getRate().doubleValue() / 100.0D;
    }
    for (ModifierMultiplierPrice multiplierPrice : priceList) {
      if (multiplier.getId().equals(multiplierPrice.getMultiplier().getId())) {
        return multiplierPrice.getPrice().doubleValue();
      }
    }
    return defaultPrice * multiplier.getRate().doubleValue() / 100.0D;
  }
  
  public double getPriceForSizeAndMultiplier(MenuItemSize size, boolean extra, Multiplier multiplier) {
    List<PizzaModifierPrice> priceList = getPizzaModifierPriceList();
    double regularPrice = 0.0D;
    if ((isPizzaModifier().booleanValue()) && (priceList != null)) {
      for (PizzaModifierPrice pizzaModifierPrice : priceList) {
        if (size.equals(pizzaModifierPrice.getSize())) {
          List<ModifierMultiplierPrice> multiplierPriceList = pizzaModifierPrice.getMultiplierPriceList();
          if (multiplierPriceList != null) {
            Double multiplierPrice = null;
            for (ModifierMultiplierPrice price : multiplierPriceList) {
              String priceTableMultiplierName = price.getMultiplier().getId();
              if (priceTableMultiplierName.equals("Regular")) {
                regularPrice = price.getPrice().doubleValue();
                if (multiplier.getId().equals("Regular")) {
                  return regularPrice;
                }
              }
              else if (priceTableMultiplierName.equals(multiplier.getId())) {
                multiplierPrice = price.getPrice();
              }
            }
            if (multiplierPrice != null) {
              return multiplierPrice.doubleValue();
            }
          }
        }
      }
    }
    return regularPrice * multiplier.getRate().doubleValue() / 100.0D;
  }
  
  public List<TicketItemTax> getTaxByOrderType(OrderType type)
  {
    TaxGroup taxGroup = getTaxGroup();
    if (taxGroup == null) {
      return null;
    }
    List<TicketItemTax> ticketItemTaxes = new ArrayList();
    List<Tax> taxes = taxGroup.getTaxes();
    if (taxes != null) {
      for (Tax tax : taxes) {
        TicketItemTax ticketItemTax = new TicketItemTax();
        ticketItemTax.setId(tax.getId());
        ticketItemTax.setName(tax.getName());
        ticketItemTax.setRate(tax.getRate());
        ticketItemTaxes.add(ticketItemTax);
      }
    }
    







    return ticketItemTaxes;
  }
  
  @XmlTransient
  public ImageIcon getImage() {
    return IconFactory.getIconFromImageResource(getImageId(), 80, 80);
  }
  
  public void setPageItemId(String id) {
    pageItemId = id;
  }
  
  public String getPageItemId() {
    return pageItemId;
  }
}
