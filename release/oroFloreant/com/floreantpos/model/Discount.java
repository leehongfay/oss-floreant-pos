package com.floreantpos.model;

import com.floreantpos.model.base.BaseDiscount;







public class Discount
  extends BaseDiscount
{
  private static final long serialVersionUID = 1L;
  public static final int FREE_AMOUNT = 0;
  public static final int FIXED_PER_CATEGORY = 1;
  public static final int FIXED_PER_ITEM = 2;
  public static final int FIXED_PER_ORDER = 3;
  public static final int PERCENTAGE_PER_CATEGORY = 4;
  public static final int PERCENTAGE_PER_ITEM = 5;
  public static final int PERCENTAGE_PER_ORDER = 6;
  public static final int DISCOUNT_TYPE_AMOUNT = 0;
  public static final int DISCOUNT_TYPE_PERCENTAGE = 1;
  public static final int DISCOUNT_TYPE_REPRICE = 2;
  public static final int DISCOUNT_TYPE_ALT_PRICE = 4;
  public static final int QUALIFICATION_TYPE_ITEM = 0;
  public static final int QUALIFICATION_TYPE_ORDER = 1;
  
  public Discount() {}
  
  public Discount(String id)
  {
    super(id);
  }
  





















  public static final String[] COUPON_TYPE_NAMES = { "AMOUNT", "PERCENTAGE", "REPRICE" };
  
  public static final String[] COUPON_QUALIFICATION_NAMES = { "ITEM", "ORDER" };
  
  public String toString()
  {
    return super.getName();
  }
  
  public double calculateDiscount(ITicketItem ticketItem)
  {
    switch (getType().intValue()) {
    case 0: 
      return ticketItem.getUnitPriceDisplay().doubleValue();
    
    case 1: 
      return getValue().doubleValue() * ticketItem.getUnitPriceDisplay().doubleValue() / 100.0D;
    }
    
    


    return 0.0D;
  }
  
  public double getAmountByType(double price) {
    switch (getType().intValue()) {
    case 0: 
      return price - getValue().doubleValue();
    
    case 1: 
      return price - getValue().doubleValue() / 100.0D;
    }
    
    

    return 0.0D;
  }
}
