package com.floreantpos.model;

import com.floreantpos.model.base.BaseComboTicketItem;





public class ComboTicketItem
  extends BaseComboTicketItem
{
  private static final long serialVersionUID = 1L;
  
  public ComboTicketItem() {}
  
  public ComboTicketItem(String id)
  {
    super(id);
  }
}
