package com.floreantpos.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.floreantpos.model.base.BasePriceShift;
import java.text.SimpleDateFormat;
import org.apache.commons.lang.StringUtils;


@JsonIgnoreProperties(ignoreUnknown=true, value={"dayOfWeekAsString", "formattedStartTime", "formattedEndTime"})
public class PriceShift
  extends BasePriceShift
{
  private static final long serialVersionUID = 1L;
  private String dayOfWeekAsString;
  private String formattedStartTime;
  private String formattedEndTime;
  
  public PriceShift() {}
  
  public PriceShift(String id)
  {
    super(id);
  }
  





  public PriceShift(String id, String name)
  {
    super(id, name);
  }
  




  public boolean equals(Object obj)
  {
    if (!(obj instanceof PriceShift)) {
      return false;
    }
    return getName().equalsIgnoreCase(((PriceShift)obj).getName());
  }
  
  public String toString()
  {
    return getName();
  }
  

  public String getDayOfWeekAsString()
  {
    String daysDisplay = "";
    String daysString = getDaysOfWeek();
    if (StringUtils.isNotEmpty(daysString)) {
      String[] split = daysString.split(",");
      if (split.length == 7)
        return "All";
      if (split.length > 0) {
        for (int i = 0; i < split.length; i++) {
          int day = Integer.valueOf(split[i]).intValue();
          DayOfWeek week = DayOfWeek.getDayOfWeek(day);
          daysDisplay = daysDisplay + week.name();
          if (i != split.length - 1) {
            daysDisplay = daysDisplay + ",";
          }
        }
      }
    }
    return daysDisplay;
  }
  
  public boolean isAnyDay() {
    String daysString = getDaysOfWeek();
    if (daysString != null) {
      String[] split = daysString.split(",");
      if (split.length == 7) {
        return true;
      }
    }
    return false;
  }
  
  public void setAnyDay(boolean selected) {
    if (selected) {
      setDaysOfWeek("1,2,3,4,5,6,7");
    }
  }
  


  public String getFormattedStartTime()
  {
    return this.formattedStartTime = new SimpleDateFormat("hh:mm a").format(getStartTime());
  }
  
  public String getFormattedEndTime() {
    return this.formattedEndTime = new SimpleDateFormat("hh:mm a").format(getEndTime());
  }
}
