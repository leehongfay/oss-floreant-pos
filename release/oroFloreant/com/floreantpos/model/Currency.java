package com.floreantpos.model;

import com.floreantpos.model.base.BaseCurrency;




















public class Currency
  extends BaseCurrency
{
  private static final long serialVersionUID = 1L;
  
  public Currency() {}
  
  public Currency(String id)
  {
    super(id);
  }
  


  public String toString()
  {
    return getName();
  }
}
