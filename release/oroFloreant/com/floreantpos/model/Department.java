package com.floreantpos.model;

import com.floreantpos.model.base.BaseDepartment;
import javax.xml.bind.annotation.XmlRootElement;




@XmlRootElement
public class Department
  extends BaseDepartment
  implements IdContainer
{
  private static final long serialVersionUID = 1L;
  
  public Department() {}
  
  public Department(String id)
  {
    super(id);
  }
  

  public String toString()
  {
    return super.getName();
  }
}
