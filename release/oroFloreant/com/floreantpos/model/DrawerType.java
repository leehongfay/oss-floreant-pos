package com.floreantpos.model;

















public enum DrawerType
{
  DRAWER(0),  STAFF_BANK(1);
  
  private int typeNumber;
  
  private DrawerType(int typeNumber) {
    this.typeNumber = typeNumber;
  }
  
  public int getTypeNumber() {
    return typeNumber;
  }
  
  public static DrawerType fromInt(int typeNumber) {
    DrawerType[] values = values();
    
    for (DrawerType transLocEnum : values) {
      if (typeNumber == typeNumber) {
        return transLocEnum;
      }
    }
    return DRAWER;
  }
  
  public String toString()
  {
    return name();
  }
  
  public static DrawerType getUserDrawerType(User user) {
    return user.isStaffBank().booleanValue() ? STAFF_BANK : DRAWER;
  }
}
