package com.floreantpos.model;

















public enum PackagingDimension
{
  Quantity,  Weight,  Length,  Volume;
  
  private PackagingDimension() {}
  public String toString() { return name(); }
}
