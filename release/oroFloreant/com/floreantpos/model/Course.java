package com.floreantpos.model;

import com.floreantpos.IconFactory;
import com.floreantpos.model.base.BaseCourse;
import javax.swing.ImageIcon;
import javax.xml.bind.annotation.XmlTransient;



public class Course
  extends BaseCourse
  implements IdContainer
{
  private static final long serialVersionUID = 1L;
  
  public Course() {}
  
  public Course(String id)
  {
    super(id);
  }
  

  public void setIcon(ImageIcon image) {}
  

  @XmlTransient
  public ImageIcon getIcon()
  {
    return IconFactory.getIconFromImageResource(getIconId(), 16, 16);
  }
  
  public String toString()
  {
    return super.getName();
  }
}
