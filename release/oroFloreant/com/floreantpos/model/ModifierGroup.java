package com.floreantpos.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.floreantpos.model.base.BaseModifierGroup;
import javax.xml.bind.annotation.XmlRootElement;

@JsonIgnoreProperties(ignoreUnknown=true, value={"modifiers"})
@XmlRootElement
public class ModifierGroup extends BaseModifierGroup
{
  private static final long serialVersionUID = 1L;
  
  public ModifierGroup() {}
  
  public ModifierGroup(String id)
  {
    super(id);
  }
  
  public String toString()
  {
    return super.getName();
  }
  
  public String getDisplayName() {
    return super.getName();
  }
  
  public String getUniqueId() {
    return ("modifiergroup_" + getName() + "_" + getId()).replaceAll("\\s+", "_");
  }
}
