package com.floreantpos.model;

public enum TicketType {
  DEFAULT(0), 
  ONLINE(1), 
  RESERVATION(2);
  
  private int typeNo;
  
  private TicketType(int typeNo)
  {
    this.typeNo = typeNo;
  }
  
  public static TicketType getByTypeNo(Integer no) {
    if (no == null) {
      return DEFAULT;
    }
    TicketType[] values = values();
    for (TicketType ticketType : values) {
      if (typeNo == no.intValue()) {
        return ticketType;
      }
    }
    return DEFAULT;
  }
  
  public int getTypeNo() {
    return typeNo;
  }
}
