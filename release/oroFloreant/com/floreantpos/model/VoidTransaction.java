package com.floreantpos.model;

import com.floreantpos.model.base.BaseVoidTransaction;
import org.apache.commons.lang.StringUtils;























public class VoidTransaction
  extends BaseVoidTransaction
{
  private static final long serialVersionUID = 1L;
  
  public VoidTransaction() {}
  
  public VoidTransaction(String id)
  {
    super(id);
  }
  






  public VoidTransaction(String id, String transactionType, String paymentType)
  {
    super(id, transactionType, paymentType);
  }
  





  public String getTransactionType()
  {
    String type = super.getTransactionType();
    
    if (StringUtils.isEmpty(type)) {
      return TransactionType.DEBIT.name();
    }
    
    return type;
  }
}
