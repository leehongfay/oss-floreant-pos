package com.floreantpos.model;

import com.floreantpos.model.base.BaseGratuityPaymentHistory;





public class GratuityPaymentHistory
  extends BaseGratuityPaymentHistory
{
  private static final long serialVersionUID = 1L;
  
  public GratuityPaymentHistory() {}
  
  public GratuityPaymentHistory(String id)
  {
    super(id);
  }
  





  public GratuityPaymentHistory(String id, CashDrawer cashDrawer)
  {
    super(id, cashDrawer);
  }
}
