package com.floreantpos.model;

import com.floreantpos.model.base.BaseTag;





public class Tag
  extends BaseTag
{
  private static final long serialVersionUID = 1L;
  
  public Tag() {}
  
  public Tag(String id)
  {
    super(id);
  }
  





  public Tag(String id, String name)
  {
    super(id, name);
  }
  


  public String toString()
  {
    return name;
  }
}
