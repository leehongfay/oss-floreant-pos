package com.floreantpos.model;

import com.floreantpos.model.base.BaseRecepieItem;
import com.floreantpos.model.dao.MenuItemDAO;
import java.util.List;

















public class RecepieItem
  extends BaseRecepieItem
{
  private static final long serialVersionUID = 1L;
  private double cost;
  private IUnit unit;
  private String groupName;
  private String groupId;
  
  public RecepieItem() {}
  
  public RecepieItem(String id)
  {
    super(id);
  }
  



  public double getCost()
  {
    return cost;
  }
  
  public void setCost(double cost) {
    this.cost = cost;
  }
  

  public void setUnit(IUnit unit)
  {
    this.unit = unit;
  }
  
  public IUnit getUnit() {
    MenuItem inventoryItem = getInventoryItem();
    MenuItemDAO.getInstance().initialize(inventoryItem);
    if (inventoryItem != null) {
      List<IUnit> units = inventoryItem.getUnits();
      if ((units != null) && (units.size() > 0)) {
        for (IUnit iUnit : units) {
          if ((getUnitCode() != null) && (getUnitCode().equals(iUnit.getUniqueCode()))) {
            unit = iUnit;
          }
        }
      }
    }
    return unit;
  }
  
  public void calculatePercentage() {
    MenuItem inventoryItem = getInventoryItem();
    if (inventoryItem != null) {
      double baseUnitQuantity = inventoryItem.getBaseUnitQuantity(getUnitCode());
      setPercentage(Double.valueOf(getQuantity().doubleValue() * baseUnitQuantity * 100.0D));
      setCost(inventoryItem.getCost().doubleValue() * getQuantity().doubleValue() * baseUnitQuantity);
    }
  }
  
  public String toString()
  {
    return super.getInventoryItem().toString();
  }
  


  public void setGroupName(String groupName)
  {
    this.groupName = groupName;
  }
  
  public String getGroupName() {
    return groupName;
  }
  
  public String getGroupId() {
    return groupId;
  }
  
  public void setGroupId(String groupId) { this.groupId = groupId; }
}
