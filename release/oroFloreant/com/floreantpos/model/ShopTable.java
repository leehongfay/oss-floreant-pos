package com.floreantpos.model;

import com.floreantpos.model.base.BaseShopTable;
import com.floreantpos.model.dao.SalesAreaDAO;
import com.floreantpos.model.dao.ShopFloorDAO;
import com.floreantpos.model.dao.ShopTableStatusDAO;
import com.floreantpos.util.GlobalIdGenerator;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;


















public class ShopTable
  extends BaseShopTable
{
  private static final long serialVersionUID = 1L;
  public static final int TYPE_RECTANGLE = 0;
  public static final int TYPE_ROUNDED_RECTANGLE = 1;
  public static final int TYPE_ROUND = 2;
  public static final int TYPE_POLYGON = 3;
  public static final int TYPE_CUSTOM = 4;
  public String typesAsString;
  private Date ticketCreateTime;
  private boolean showStatus;
  private boolean reArrange;
  private int guestNumber;
  private boolean isTemporary;
  
  public TableStatus getTableStatus()
  {
    ShopTableStatus status = getStatus();
    if (status == null)
      return null;
    return status.getTableStatus();
  }
  
  public void setTableStatus(TableStatus tableStatus) {
    setTableStatusNum(tableStatus.getValue());
  }
  
  private void setTableStatusNum(int value) {
    ShopTableStatus status = getStatus();
    status.setTableStatusNum(Integer.valueOf(value));
  }
  
  public ShopTable()
  {
    setGlobalId(GlobalIdGenerator.generateGlobalId());
  }
  


  public ShopTable(Integer id)
  {
    super(id);
    setGlobalId(GlobalIdGenerator.generateGlobalId());
  }
  


  public ShopTable(Integer id, String globalId)
  {
    super(id, globalId);
    setGlobalId(GlobalIdGenerator.generateGlobalId());
  }
  


  public ShopTable(Integer x, Integer y)
  {
    setGlobalId(GlobalIdGenerator.generateGlobalId());
    setX(x);
    setY(y);
  }
  
  public ShopTable(ShopFloor floor, Integer x, Integer y, Integer id)
  {
    setGlobalId(GlobalIdGenerator.generateGlobalId());
    setCapacity(Integer.valueOf(4));
    setId(id);
    setFloorId(floor.getId());
    setX(x);
    setY(y);
  }
  
  public Integer getTableNumber() {
    return getId();
  }
  
  public String toString()
  {
    return String.valueOf(getTableNumber());
  }
  
  public boolean isTemporary() {
    return isTemporary;
  }
  
  public void setTemporary(boolean isTemporary) {
    this.isTemporary = isTemporary;
  }
  
  public String getTypesAsString() {
    List<ShopTableType> tableTypes = getTypes();
    String strTableTypes = "";
    if ((tableTypes == null) || (tableTypes.size() == 0)) {
      return "";
    }
    boolean getComma = false;
    for (ShopTableType shopTableType : tableTypes) {
      if (getComma) {
        strTableTypes = strTableTypes + ", ";
      }
      strTableTypes = strTableTypes + shopTableType.getName();
      getComma = true;
    }
    return strTableTypes;
  }
  
  public void setTypesAsString(String typesAsString) {
    this.typesAsString = typesAsString;
  }
  
  public String getTicketId() {
    ShopTableStatus status = getStatus();
    if (status == null)
      return null;
    List<String> ticketNumbers = status.getListOfTicketNumbers();
    if ((ticketNumbers != null) && (ticketNumbers.size() > 0))
      return (String)ticketNumbers.get(0);
    return null;
  }
  
  public String getUserId() {
    ShopTableStatus status = getStatus();
    if (status == null)
      return null;
    return status.getUserId();
  }
  
  public String getUserName() {
    ShopTableStatus status = getStatus();
    if (status == null)
      return null;
    return status.getUserName();
  }
  
  public String getTicketShortId() {
    ShopTableStatus status = getStatus();
    if (status == null)
      return null;
    return status.getTokenNo();
  }
  
  public ShopTableStatus getStatus() {
    ShopTableStatus shopTableStatus = super.getShopTableStatus();
    if (shopTableStatus != null)
      return shopTableStatus;
    return saveAndGetNewStatus();
  }
  
  public ShopTableStatus saveAndGetNewStatus() {
    ShopTableStatus shopTableStatus = new ShopTableStatus();
    Integer tableId = getId();
    shopTableStatus.setId(tableId);
    shopTableStatus.setTableStatus(TableStatus.Available);
    if (tableId != null)
      ShopTableStatusDAO.getInstance().save(shopTableStatus);
    setShopTableStatus(shopTableStatus);
    return shopTableStatus;
  }
  
  public Date getTicketCreateTime() {
    return ticketCreateTime;
  }
  
  public void setTicketCreateTime(Date ticketCreateTime) {
    this.ticketCreateTime = ticketCreateTime;
  }
  
  public boolean isShowStatus() {
    return showStatus;
  }
  
  public void setShowStatus(boolean showStatus) {
    this.showStatus = showStatus;
  }
  
  public boolean isReArrange() {
    return reArrange;
  }
  
  public void setReArrange(boolean reArrange) {
    this.reArrange = reArrange;
  }
  
  public int getGuestNumber() {
    return guestNumber;
  }
  
  public void setGuestNumber(int guestNumber) {
    this.guestNumber = guestNumber;
  }
  
  public ShopFloor getFloor() {
    if (StringUtils.isNotEmpty(getFloorId())) {
      return ShopFloorDAO.getInstance().get(getFloorId());
    }
    return null;
  }
  
  public void setFloor(ShopFloor shopFloor) {
    String shopFloorId = null;
    if (shopFloor != null) {
      shopFloorId = shopFloor.getId();
    }
    super.setFloorId(shopFloorId);
  }
  
  public SalesArea getSalesArea() {
    if (StringUtils.isNotEmpty(getSalesAreaId())) {
      return SalesAreaDAO.getInstance().get(getSalesAreaId());
    }
    return null;
  }
  
  public void setSalesArea(SalesArea salesArea) {
    String salesAreaId = null;
    if (salesArea != null) {
      salesAreaId = salesArea.getId();
    }
    super.setSalesAreaId(salesAreaId);
  }
}
