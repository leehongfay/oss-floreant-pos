package com.floreantpos.model;

import com.floreantpos.model.base.BaseInventoryUnitGroup;



public class InventoryUnitGroup
  extends BaseInventoryUnitGroup
{
  private static final long serialVersionUID = 1L;
  
  public InventoryUnitGroup() {}
  
  public InventoryUnitGroup(String id)
  {
    super(id);
  }
  



  public InventoryUnitGroup(String id, String name)
  {
    super(id, name);
  }
  

  public String toString()
  {
    return super.getName();
  }
  
  public String getUniqueId() {
    return ("unitgroup_" + getName() + "_" + getId()).replaceAll("\\s+", "_");
  }
}
