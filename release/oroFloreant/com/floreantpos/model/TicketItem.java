package com.floreantpos.model;

import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.base.BaseTicketItem;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.model.ext.KitchenStatus;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.util.CopyUtil;
import com.floreantpos.util.JsonUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.OrgJsonUtil;
import com.floreantpos.util.PrintServiceUtil;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.swing.ImageIcon;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;












public class TicketItem
  extends BaseTicketItem
  implements ITicketItem
{
  public static final String PROPERTY_COOKING_INSTRUCTION = "COOKING_INSTRUCTION";
  public static final String PROPERTY_TAX = "TAX";
  public static final String PROPERTY_DISCOUNT = "DISCOUNT";
  private static final long serialVersionUID = 1L;
  private transient boolean syncEdited;
  private MenuItem menuItem;
  private int tableRowNum;
  private Double quantityToShip;
  public static final int HALF = 0;
  public static final int FULL = 1;
  public static final int SHARED_FULL = 2;
  public static final int SHARED_CUSTOM = 3;
  private Boolean includeVoidQuantity;
  private VoidItem voidItem;
  private List<TicketItemCookingInstruction> cookingInstructions;
  private List<TicketItemTax> taxes;
  private List<TicketItemDiscount> discounts;
  private TicketItem parentTicketItem;
  private Integer sortOrder;
  
  public TicketItem() {}
  
  public TicketItem(String id)
  {
    super(id);
  }
  

  public TicketItem(Ticket ticket, String name, double unitPrice, double quantity, TicketItemTax tax)
  {
    setName(name);
    setUnitPrice(Double.valueOf(unitPrice));
    setQuantity(Double.valueOf(quantity));
    addTotaxes(tax);
    setTicket(ticket);
  }
  















  public TicketItem createNew()
  {
    if ((this instanceof ModifiableTicketItem)) {
      return new ModifiableTicketItem();
    }
    
    return new TicketItem();
  }
  



  public Boolean isHasModifiers()
  {
    List<TicketItemModifier> ticketItemModifiers = getTicketItemModifiers();
    return Boolean.valueOf((ticketItemModifiers != null) && (ticketItemModifiers.size() > 0));
  }
  


  public TicketItemModifier getSizeModifier()
  {
    return null;
  }
  




  public void setSizeModifier(TicketItemModifier sizeModifier) {}
  



  public List<TicketItemModifier> getTicketItemModifiers()
  {
    return null;
  }
  


  public void setTicketItemModifiers(List<TicketItemModifier> ticketItemModifiers) {}
  


  public void addToticketItemModifiers(TicketItemModifier ticketItemModifier) {}
  

  public TicketItem clone()
  {
    return (TicketItem)SerializationUtils.clone(this);
  }
  
  public TicketItem cloneAsNew() {
    try {
      TicketItem newTicketItem = (TicketItem)CopyUtil.deepCopy(this);
      newTicketItem.setId(null);
      newTicketItem.setVersion(0L);
      newTicketItem.setPrintedToKitchen(Boolean.valueOf(false));
      newTicketItem.setSeat(null);
      if (newTicketItem.getSizeModifier() != null) {
        newTicketItem.getSizeModifier().setId(null);
        newTicketItem.getSizeModifier().setPrintedToKitchen(Boolean.valueOf(false));
      }
      List<TicketItemModifier> ticketItemModifiers = newTicketItem.getTicketItemModifiers();
      newTicketItem.setTicketItemModifiers(null);
      if ((ticketItemModifiers != null) && (ticketItemModifiers.size() > 0)) {
        for (TicketItemModifier ticketItemModifier : ticketItemModifiers) {
          ticketItemModifier.setId(null);
          ticketItemModifier.setPrintedToKitchen(Boolean.valueOf(false));
          newTicketItem.addToticketItemModifiers(ticketItemModifier);
        }
      }
      

















      newTicketItem.setPrintedToKitchen(isPrintedToKitchen());
      Object comboItems = newTicketItem.getComboItems();
      List<TicketItem> newComboItems = new ArrayList();
      if (isComboItem().booleanValue()) {
        ComboTicketItem comboTicketItem = (ComboTicketItem)newTicketItem;
        if (comboItems != null) {
          for (Iterator iterator = ((List)comboItems).iterator(); iterator.hasNext();) {
            TicketItem oldComboItem = (TicketItem)iterator.next();
            TicketItem clonedCombo = oldComboItem.cloneAsNew();
            clonedCombo.setId(null);
            clonedCombo.setParentTicketItem(comboTicketItem);
            newComboItems.add(clonedCombo);
          }
          comboTicketItem.setComboItems(newComboItems);
        }
        return comboTicketItem;
      }
      
      newTicketItem.setComboItems(null);
      
      return newTicketItem;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private void setComboItems(Object object) {}
  
  public void setTaxes(List<TicketItemTax> taxes)
  {
    this.taxes = taxes;
  }
  
  public List<TicketItemTax> getTaxes() {
    if (taxes == null) {
      taxes = new ArrayList();
      
      String property = super.getTaxesProperty();
      if (StringUtils.isNotEmpty(property)) {
        JsonReader jsonParser = Json.createReader(new StringReader(property));
        JsonArray jsonArray = jsonParser.readArray();
        jsonParser.close();
        for (int i = 0; i < jsonArray.size(); i++) {
          JsonObject jsonObject = (JsonObject)jsonArray.get(i);
          TicketItemTax tit = new TicketItemTax();
          tit.setId(JsonUtil.getString(jsonObject, TicketItemTax.PROP_ID));
          tit.setName(JsonUtil.getString(jsonObject, TicketItemTax.PROP_NAME));
          tit.setRate(JsonUtil.getDouble(jsonObject, TicketItemTax.PROP_RATE));
          tit.setTaxAmount(JsonUtil.getDouble(jsonObject, TicketItemTax.PROP_TAX_AMOUNT));
          taxes.add(tit);
        }
      }
    }
    return taxes;
  }
  
  public void addTotaxes(TicketItemTax tax) {
    taxes = getTaxes();
    if (taxes == null) {
      taxes = new ArrayList();
    }
    taxes.add(tax);
  }
  
  public void buildTaxes() {
    if ((taxes == null) || (taxes.isEmpty())) {
      setTaxesProperty(null);
      return;
    }
    JSONArray jsonArray = new JSONArray();
    for (TicketItemTax ticketItemTax : taxes) {
      jsonArray.put(ticketItemTax.toJson());
    }
    setTaxesProperty(jsonArray.toString());
  }
  
  public void setDiscounts(List<TicketItemDiscount> discounts) {
    this.discounts = discounts;
  }
  
  public List<TicketItemDiscount> getDiscounts() {
    if (discounts == null) {
      discounts = new ArrayList();
      
      String property = super.getDiscountsProperty();
      if (StringUtils.isNotEmpty(property)) {
        JSONArray jsonArray = new JSONArray(property);
        for (int i = 0; i < jsonArray.length(); i++) {
          JSONObject jsonObject = jsonArray.getJSONObject(i);
          TicketItemDiscount tid = new TicketItemDiscount();
          tid.setTicketItem(this);
          tid.setDiscountId(OrgJsonUtil.getString(jsonObject, TicketItemDiscount.PROP_DISCOUNT_ID));
          tid.setName(OrgJsonUtil.getString(jsonObject, TicketItemDiscount.PROP_NAME));
          tid.setType(OrgJsonUtil.getInt(jsonObject, TicketItemDiscount.PROP_TYPE));
          tid.setAutoApply(OrgJsonUtil.getBoolean(jsonObject, TicketItemDiscount.PROP_AUTO_APPLY));
          tid.setCouponQuantity(OrgJsonUtil.getDouble(jsonObject, TicketItemDiscount.PROP_COUPON_QUANTITY));
          tid.setMinimumAmount(OrgJsonUtil.getDouble(jsonObject, TicketItemDiscount.PROP_MINIMUM_AMOUNT));
          tid.setValue(OrgJsonUtil.getDouble(jsonObject, TicketItemDiscount.PROP_VALUE));
          tid.setAmount(OrgJsonUtil.getDouble(jsonObject, TicketItemDiscount.PROP_AMOUNT));
          discounts.add(tid);
        }
      }
    }
    return discounts;
  }
  
  public void addTodiscounts(TicketItemDiscount tax) {
    discounts = getDiscounts();
    if (discounts == null) {
      discounts = new ArrayList();
    }
    discounts.add(tax);
  }
  
  public void buildDiscounts() {
    if ((discounts == null) || (discounts.isEmpty())) {
      setDiscountsProperty(null);
      return;
    }
    JSONArray jsonArray = new JSONArray();
    for (TicketItemDiscount ticketItemDiscount : discounts) {
      jsonArray.put(ticketItemDiscount.toJson());
    }
    setDiscountsProperty(jsonArray.toString());
  }
  
  public int getTableRowNum() {
    return tableRowNum;
  }
  
  public void setTableRowNum(int tableRowNum) {
    this.tableRowNum = tableRowNum;
  }
  
  public boolean canAddCookingInstruction() {
    if (isPrintedToKitchen().booleanValue()) {
      return false;
    }
    return true;
  }
  
  public String toString()
  {
    return getName();
  }
  
  public void setCookingInstructions(List<TicketItemCookingInstruction> cookingInstructions) {
    this.cookingInstructions = cookingInstructions;
    buildCoookingInstructions();
  }
  
  public void addCookingInstructions(List<TicketItemCookingInstruction> instructions) {
    cookingInstructions = getCookingInstructions();
    if (cookingInstructions == null) {
      cookingInstructions = new ArrayList(2);
    }
    for (TicketItemCookingInstruction ticketItemCookingInstruction : instructions) {
      addCookingInstruction(ticketItemCookingInstruction);
    }
  }
  
  public void addCookingInstruction(TicketItemCookingInstruction cookingInstruction) {
    cookingInstructions = getCookingInstructions();
    if (cookingInstructions == null) {
      cookingInstructions = new ArrayList();
      return;
    }
    cookingInstructions.add(cookingInstruction);
    buildCoookingInstructions();
  }
  
  public void buildCoookingInstructions() {
    if ((cookingInstructions == null) || (cookingInstructions.isEmpty())) {
      setCookingInstructionsProperty(null);
      return;
    }
    JSONArray jsonArray = new JSONArray();
    for (TicketItemCookingInstruction cookingIns : cookingInstructions) {
      jsonArray.put(cookingIns.toJson());
    }
    setCookingInstructionsProperty(jsonArray.toString());
  }
  
  public void removeCookingInstruction(TicketItemCookingInstruction itemCookingInstruction) {
    List<TicketItemCookingInstruction> cookingInstructions2 = getCookingInstructions();
    if ((cookingInstructions2 == null) || (cookingInstructions2.size() == 0)) {
      return;
    }
    for (Iterator iterator = cookingInstructions2.iterator(); iterator.hasNext();) {
      TicketItemCookingInstruction ticketItemCookingInstruction = (TicketItemCookingInstruction)iterator.next();
      if (ticketItemCookingInstruction.getTableRowNum() == itemCookingInstruction.getTableRowNum()) {
        iterator.remove();
        buildCoookingInstructions();
        return;
      }
    }
  }
  
  public TicketItemModifier findTicketItemModifierFor(MenuModifier menuModifier) {
    List<TicketItemModifier> modifiers = getTicketItemModifiers();
    if (modifiers == null) {
      return null;
    }
    for (TicketItemModifier ticketItemModifier : modifiers) {
      String itemId = ticketItemModifier.getItemId();
      if ((itemId != null) && (itemId.equals(menuModifier.getId()))) {
        return ticketItemModifier;
      }
    }
    return null;
  }
  
  public TicketItemModifier addTicketItemModifier(MenuModifier menuModifier, int modifierType, OrderType type, Multiplier multiplier) {
    return addTicketItemModifier(menuModifier, modifierType, type, multiplier, 1.0D);
  }
  
  public TicketItemModifier addTicketItemModifier(MenuModifier menuModifier, int modifierType, OrderType type, Multiplier multiplier, double quantity) {
    TicketItemModifier ticketItemModifier = new TicketItemModifier();
    ticketItemModifier.setTaxIncluded(Boolean.valueOf(Application.getInstance().isPriceIncludesTax()));
    ticketItemModifier.setItemId(menuModifier.getId());
    ticketItemModifier.setPageItemId(menuModifier.getPageItemId());
    MenuItemModifierSpec menuItemModifierGroup = menuModifier.getMenuItemModifierGroup();
    if (menuItemModifierGroup != null) {
      ticketItemModifier.setGroupId(menuItemModifierGroup.getId());
    }
    ticketItemModifier.setItemQuantity(Double.valueOf(quantity));
    ticketItemModifier.setName(menuModifier.getDisplayName());
    double price = menuModifier.getPriceForMultiplier(multiplier);
    if (multiplier != null) {
      ticketItemModifier.setMultiplierName(multiplier.getId());
      ticketItemModifier.setName(multiplier.getTicketPrefix() + " " + menuModifier.getDisplayName());
    }
    ticketItemModifier.setUnitPrice(Double.valueOf(price));
    ticketItemModifier.setTaxes(menuModifier.getTaxByOrderType(type));
    
    ticketItemModifier.setModifierType(Integer.valueOf(modifierType));
    ticketItemModifier.setShouldPrintToKitchen(menuModifier.isShouldPrintToKitchen());
    ticketItemModifier.setTicketItem(this);
    
    addToticketItemModifiers(ticketItemModifier);
    
    return ticketItemModifier;
  }
  
  public TicketItemModifier addTicketItemModifier(MenuModifier menuModifier, boolean addOn) {
    TicketItemModifier ticketItemModifier = new TicketItemModifier();
    
    ticketItemModifier.setTaxIncluded(Boolean.valueOf(Application.getInstance().isPriceIncludesTax()));
    ticketItemModifier.setItemId(menuModifier.getId());
    MenuItemModifierSpec menuItemModifierGroup = menuModifier.getMenuItemModifierGroup();
    if (menuItemModifierGroup != null) {
      ticketItemModifier.setGroupId(menuItemModifierGroup.getId());
    }
    ticketItemModifier.setItemCount(Integer.valueOf(1));
    ticketItemModifier.setName(menuModifier.getDisplayName());
    
    if (addOn) {
      ticketItemModifier.setUnitPrice(menuModifier.getExtraPrice());
      ticketItemModifier.setModifierType(Integer.valueOf(3));
    }
    else {
      ticketItemModifier.setUnitPrice(menuModifier.getPrice());
      ticketItemModifier.setModifierType(Integer.valueOf(1));
    }
    ticketItemModifier.setTaxes(menuModifier.getTaxByOrderType(ticketItemModifier.getTicketItem().getTicket().getOrderType()));
    ticketItemModifier.setShouldPrintToKitchen(menuModifier.isShouldPrintToKitchen());
    ticketItemModifier.setTicketItem(this);
    
    addToticketItemModifiers(ticketItemModifier);
    
    return ticketItemModifier;
  }
  
  public void updateModifiersUnitPrice(double defaultSellPortion) {
    List<TicketItemModifier> ticketItemModifiers = getTicketItemModifiers();
    if (ticketItemModifiers != null) {
      for (TicketItemModifier ticketItemModifier : ticketItemModifiers) {
        if (!ticketItemModifier.isInfoOnly().booleanValue()) {
          ticketItemModifier.setUnitPrice(Double.valueOf(ticketItemModifier.getUnitPrice().doubleValue() * defaultSellPortion / 100.0D));
        }
      }
    }
  }
  
  public boolean contains(TicketItemModifier ticketItemModifier) {
    List<TicketItemModifier> ticketItemModifiers = getTicketItemModifiers();
    int count = 0;
    if (ticketItemModifiers != null) {
      for (TicketItemModifier ticketItemModifier2 : ticketItemModifiers) {
        if ((!ticketItemModifier2.isInfoOnly().booleanValue()) && 
          (ticketItemModifier.getName().trim().equals(ticketItemModifier2.getName().trim()))) {
          count++;
        }
      }
    }
    
    return count > 1;
  }
  
  public TicketItemModifier removeTicketItemModifier(TicketItemModifier ticketItemModifier) {
    List<TicketItemModifier> ticketItemModifiers = getTicketItemModifiers();
    if (ticketItemModifiers == null) {
      return ticketItemModifier;
    }
    for (Iterator iter = ticketItemModifiers.iterator(); iter.hasNext();) {
      TicketItemModifier oldTicketItemModifier = (TicketItemModifier)iter.next();
      if ((oldTicketItemModifier.getItemId().equals(ticketItemModifier.getItemId())) && 
        (oldTicketItemModifier.getModifierType() == ticketItemModifier.getModifierType())) {
        iter.remove();
        return oldTicketItemModifier;
      }
    }
    return ticketItemModifier;
  }
  
  public void calculatePrice()
  {
    BigDecimal unitPrice = NumberUtil.convertToBigDecimal(getUnitPrice().doubleValue());
    
    BigDecimal itemQuantity = NumberUtil.convertToBigDecimal(getQuantity().doubleValue());
    BigDecimal subtotalWithoutModifiers = NumberUtil.round(unitPrice.multiply(itemQuantity));
    BigDecimal discountWithoutModifiers = calculateDiscount(subtotalWithoutModifiers);
    BigDecimal serviceCharge = calculateServiceCharge(subtotalWithoutModifiers);
    BigDecimal taxWithoutModifiers = null;
    if (isTaxOnServiceCharge().booleanValue()) {
      taxWithoutModifiers = calculateTax(subtotalWithoutModifiers.subtract(discountWithoutModifiers).add(serviceCharge));
    }
    else {
      taxWithoutModifiers = calculateTax(subtotalWithoutModifiers.subtract(discountWithoutModifiers));
    }
    
    PosLog.debug(getClass(), String.format("Item %s, unit price: %s, quantity: %s, tax rate: %s", new Object[] { getName(), unitPrice, getQuantity(), Double.valueOf(getTotalTaxRate()) }));
    PosLog.debug(getClass(), String.format("Subtotal without modifiers: %s, discount without modifiers: %s, tax without modifiers: %s", new Object[] { subtotalWithoutModifiers, discountWithoutModifiers, taxWithoutModifiers }));
    

    BigDecimal modifierSubtotal = NumberUtil.convertToBigDecimal("0");
    BigDecimal modifierDiscount = NumberUtil.convertToBigDecimal("0");
    BigDecimal modifierTax = NumberUtil.convertToBigDecimal("0");
    
    List<TicketItemModifier> ticketItemModifiers = getTicketItemModifiers();
    Set<String> averagePricedModifierList; if (ticketItemModifiers != null) {
      averagePricedModifierList = new HashSet();
      for (TicketItemModifier modifier : ticketItemModifiers)
        if (!modifier.isInfoOnly().booleanValue())
        {

          modifier.calculatePrice();
          if ((modifier.isShouldSectionWisePrice().booleanValue() != true) || (!averagePricedModifierList.contains(modifier.getItemId())))
          {

            modifierSubtotal = modifierSubtotal.add(NumberUtil.convertToBigDecimal(modifier.getSubTotalAmount().doubleValue()).abs());
            modifierDiscount = modifierDiscount.add(NumberUtil.convertToBigDecimal(modifier.getDiscountAmount().doubleValue()).abs());
            modifierTax = modifierTax.add(NumberUtil.convertToBigDecimal(modifier.getTaxAmount().doubleValue()).abs());
            
            averagePricedModifierList.add(modifier.getItemId());
          }
        }
    }
    BigDecimal subtotal = NumberUtil.round(subtotalWithoutModifiers.add(modifierSubtotal));
    BigDecimal discount = NumberUtil.round(discountWithoutModifiers.add(modifierDiscount));
    BigDecimal tax = NumberUtil.round(taxWithoutModifiers.add(modifierTax));
    
    BigDecimal total = NumberUtil.convertToBigDecimal("0");
    BigDecimal totalWithoutModifiers = NumberUtil.convertToBigDecimal("0");
    
    if (isTaxIncluded().booleanValue()) {
      total = subtotal.subtract(discount).add(serviceCharge);
      totalWithoutModifiers = subtotalWithoutModifiers.subtract(discountWithoutModifiers).add(serviceCharge);
    }
    else {
      total = subtotal.subtract(discount).add(tax).add(serviceCharge);
      totalWithoutModifiers = subtotalWithoutModifiers.subtract(discountWithoutModifiers).add(taxWithoutModifiers).add(serviceCharge);
    }
    total = NumberUtil.round(total);
    
    PosLog.debug(getClass(), String.format("Subtotal with modifiers: %s, discount with modifiers: %s, tax with modifiers: %s", new Object[] { subtotal, discount, tax }));
    
    setSubtotalAmount(Double.valueOf(subtotal.doubleValue()));
    setSubtotalAmountWithoutModifiers(Double.valueOf(subtotalWithoutModifiers.doubleValue()));
    setDiscountAmount(Double.valueOf(discount.doubleValue()));
    setDiscountWithoutModifiers(Double.valueOf(discountWithoutModifiers.doubleValue()));
    
    setTaxAmount(Double.valueOf(tax.doubleValue()));
    setTaxAmountWithoutModifiers(Double.valueOf(taxWithoutModifiers.doubleValue()));
    
    setServiceCharge(Double.valueOf(serviceCharge.doubleValue()));
    setTotalAmount(Double.valueOf(total.doubleValue()));
    setTotalAmountWithoutModifiers(Double.valueOf(totalWithoutModifiers.doubleValue()));
    
    setAdjustedUnitPrice(Double.valueOf(unitPrice.doubleValue()));
    setAdjustedDiscount(Double.valueOf(discount.doubleValue()));
    setAdjustedDiscountWithoutModifiers(Double.valueOf(discountWithoutModifiers.doubleValue()));
    setAdjustedSubtotal(Double.valueOf(subtotal.doubleValue()));
    setAdjustedSubtotalWithoutModifiers(Double.valueOf(subtotalWithoutModifiers.doubleValue()));
    
    setAdjustedTax(Double.valueOf(tax.doubleValue()));
    setAdjustedTaxWithoutModifiers(Double.valueOf(taxWithoutModifiers.doubleValue()));
    
    setAdjustedTotal(Double.valueOf(total.doubleValue()));
    setAdjustedTotalWithoutModifiers(Double.valueOf(totalWithoutModifiers.doubleValue()));
  }
  
  public void calculateAdjustedPrice() {
    Ticket ticket = getTicket();
    if (ticket == null) {
      return;
    }
    

    BigDecimal ticketSubtotalAmount = NumberUtil.convertToBigDecimal(ticket.getSubtotalAmountWithVoidItems());
    BigDecimal ticketDiscountAmount = NumberUtil.convertToBigDecimal(ticket.getTicketDiscountAmount());
    BigDecimal itemDiscountAmount = NumberUtil.convertToBigDecimal(ticket.getItemDiscountAmount());
    BigDecimal ticketSubtotalAfterItemDiscount = ticketSubtotalAmount.subtract(itemDiscountAmount);
    BigDecimal ticketSubtotalAfterAllDiscount = ticketSubtotalAfterItemDiscount.subtract(ticketDiscountAmount);
    
    BigDecimal subtotalAfterDiscount = NumberUtil.convertToBigDecimal(getSubtotalAmountWithoutModifiers().doubleValue()).subtract(NumberUtil.convertToBigDecimal(getDiscountWithoutModifiers().doubleValue()));
    BigDecimal adjustedSubtotalWithoutModifiers = subtotalAfterDiscount;
    if (ticketSubtotalAfterItemDiscount.compareTo(BigDecimal.ZERO) != 0) {
      adjustedSubtotalWithoutModifiers = subtotalAfterDiscount.multiply(ticketSubtotalAfterAllDiscount).divide(ticketSubtotalAfterItemDiscount, 4, RoundingMode.HALF_UP);
    }
    














    BigDecimal adjustedTaxWithoutModifiers = NumberUtil.round(calculateTax(adjustedSubtotalWithoutModifiers));
    BigDecimal itemQuantity = NumberUtil.convertToBigDecimal(getQuantity().doubleValue());
    BigDecimal adjustedUnitPrice = adjustedSubtotalWithoutModifiers.divide(itemQuantity, 4, RoundingMode.HALF_UP);
    
    BigDecimal modifierAdjustedSubtotal = NumberUtil.convertToBigDecimal("0");
    BigDecimal modifierAdjustedDiscount = NumberUtil.convertToBigDecimal("0");
    BigDecimal modifierAdjustedTax = NumberUtil.convertToBigDecimal("0");
    
    List<TicketItemModifier> ticketItemModifiers = getTicketItemModifiers();
    Set<String> averagePricedModifierList; if (ticketItemModifiers != null) {
      averagePricedModifierList = new HashSet();
      for (TicketItemModifier modifier : ticketItemModifiers)
        if (!modifier.isInfoOnly().booleanValue())
        {

          modifier.calculateAdjustedPrice();
          if ((modifier.isShouldSectionWisePrice().booleanValue()) || (!averagePricedModifierList.contains(modifier.getItemId())))
          {

            modifierAdjustedSubtotal = modifierAdjustedSubtotal.add(itemQuantity.multiply(NumberUtil.convertToBigDecimal(modifier.getAdjustedSubtotal().doubleValue()).abs()));
            modifierAdjustedDiscount = modifierAdjustedDiscount.add(itemQuantity.multiply(NumberUtil.convertToBigDecimal(modifier.getAdjustedDiscount().doubleValue()).abs()));
            modifierAdjustedTax = modifierAdjustedTax.add(itemQuantity.multiply(NumberUtil.convertToBigDecimal(modifier.getAdjustedTax().doubleValue()).abs()));
            
            averagePricedModifierList.add(modifier.getItemId());
          }
        }
    }
    BigDecimal adjustedSubtotal = NumberUtil.round(adjustedSubtotalWithoutModifiers.add(modifierAdjustedSubtotal));
    BigDecimal myAdjustedDiscountWithoutModifiers = NumberUtil.round(NumberUtil.convertToBigDecimal(getSubtotalAmountWithoutModifiers().doubleValue()).subtract(adjustedSubtotalWithoutModifiers));
    BigDecimal adjustedDiscount = NumberUtil.round(myAdjustedDiscountWithoutModifiers.add(modifierAdjustedDiscount));
    BigDecimal adjustedTax = NumberUtil.round(adjustedTaxWithoutModifiers.add(modifierAdjustedTax));
    BigDecimal adjustedTotal = NumberUtil.convertToBigDecimal(0.0D);
    BigDecimal adjustedTotalWithoutModifiers = NumberUtil.convertToBigDecimal(0.0D);
    if (isTaxIncluded().booleanValue()) {
      adjustedTotal = adjustedSubtotal.add(NumberUtil.convertToBigDecimal(getServiceCharge().doubleValue()));
      adjustedTotalWithoutModifiers = adjustedSubtotalWithoutModifiers.add(NumberUtil.convertToBigDecimal(getServiceCharge().doubleValue()));
    }
    else {
      adjustedTotal = adjustedSubtotal.add(adjustedTax).add(NumberUtil.convertToBigDecimal(getServiceCharge().doubleValue()));
      adjustedTotalWithoutModifiers = adjustedSubtotalWithoutModifiers.add(adjustedTaxWithoutModifiers).add(NumberUtil.convertToBigDecimal(getServiceCharge().doubleValue()));
    }
    
    adjustedTotal = NumberUtil.round(adjustedTotal);
    
    setAdjustedUnitPrice(Double.valueOf(adjustedUnitPrice.doubleValue()));
    setAdjustedDiscount(Double.valueOf(adjustedDiscount.doubleValue()));
    setAdjustedDiscountWithoutModifiers(Double.valueOf(myAdjustedDiscountWithoutModifiers.doubleValue()));
    setAdjustedSubtotal(Double.valueOf(adjustedSubtotal.doubleValue()));
    setAdjustedSubtotalWithoutModifiers(Double.valueOf(adjustedSubtotalWithoutModifiers.doubleValue()));
    setAdjustedTax(Double.valueOf(adjustedTax.doubleValue()));
    setAdjustedTaxWithoutModifiers(Double.valueOf(adjustedTaxWithoutModifiers.doubleValue()));
    setAdjustedTotal(Double.valueOf(adjustedTotal.doubleValue()));
    setAdjustedTotalWithoutModifiers(Double.valueOf(adjustedTotalWithoutModifiers.doubleValue()));
  }
  
  private BigDecimal calculateServiceCharge(BigDecimal subtotalAmount)
  {
    if ((!isServiceChargeApplicable().booleanValue()) || (getServiceChargeRate().doubleValue() == 0.0D)) {
      return new BigDecimal("0");
    }
    
    BigDecimal serviceChargeRate = NumberUtil.convertToBigDecimal(getServiceChargeRate().doubleValue() / 100.0D);
    return NumberUtil.round(subtotalAmount.multiply(serviceChargeRate));
  }
  
  public boolean isMergable(TicketItem otherItem, boolean merge) {
    if ((isTreatAsSeat().booleanValue()) || (otherItem.isTreatAsSeat().booleanValue())) {
      return false;
    }
    if ((isFractionalUnit().booleanValue()) || (otherItem.isFractionalUnit().booleanValue())) {
      return false;
    }
    if ((StringUtils.isEmpty(getMenuItemId())) || (StringUtils.isEmpty(otherItem.getMenuItemId()))) {
      return false;
    }
    if ((hasCookingInstructions()) || (otherItem.hasCookingInstructions())) {
      return false;
    }
    if (!getMenuItemId().equals(otherItem.getMenuItemId())) {
      return false;
    }
    if (!getUnitPrice().equals(otherItem.getUnitPrice())) {
      return false;
    }
    if (getSeatNumber() != otherItem.getSeatNumber()) {
      return false;
    }
    if ((!isHasModifiers().booleanValue()) && (!otherItem.isHasModifiers().booleanValue())) {
      return true;
    }
    if (!isMergableModifiers(getTicketItemModifiers(), otherItem.getTicketItemModifiers(), merge)) {
      return false;
    }
    return true;
  }
  
  public boolean isMergableModifiers(List<TicketItemModifier> thisModifiers, List<TicketItemModifier> thatModifiers, boolean merge) {
    if (thatModifiers == null) {
      return true;
    }
    if (thisModifiers.size() != thatModifiers.size()) {
      return false;
    }
    
    Comparator<TicketItemModifier> comparator = new Comparator()
    {
      public int compare(TicketItemModifier o1, TicketItemModifier o2) {
        return o1.getItemId().compareTo(o2.getItemId());
      }
      
    };
    Collections.sort(thisModifiers, comparator);
    Collections.sort(thatModifiers, comparator);
    
    Iterator<TicketItemModifier> thisIterator = thisModifiers.iterator();
    Iterator<TicketItemModifier> thatIterator = thatModifiers.iterator();
    
    while (thisIterator.hasNext()) {
      TicketItemModifier next1 = (TicketItemModifier)thisIterator.next();
      TicketItemModifier next2 = (TicketItemModifier)thatIterator.next();
      
      if (comparator.compare(next1, next2) != 0) {
        return false;
      }
      
      if (merge) {
        next1.merge(next2);
      }
    }
    return true;
  }
  
  public void merge(TicketItem otherItem) {
    if ((!isHasModifiers().booleanValue()) && (!otherItem.isHasModifiers().booleanValue())) {
      setQuantity(Double.valueOf(getQuantity().doubleValue() + otherItem.getQuantity().doubleValue()));
      return;
    }
    if (isMergable(otherItem, true)) {
      setQuantity(Double.valueOf(getQuantity().doubleValue() + otherItem.getQuantity().doubleValue()));
    }
  }
  
  public boolean hasCookingInstructions() {
    return (getCookingInstructions() != null) && (getCookingInstructions().size() > 0);
  }
  
  public BigDecimal calculateDiscount(BigDecimal subtotalAmount) {
    BigDecimal discount = NumberUtil.convertToBigDecimal(0.0D);
    
    List<TicketItemDiscount> discounts = getDiscounts();
    if (discounts != null) {
      for (TicketItemDiscount ticketItemDiscount : discounts) {
        if (ticketItemDiscount.getType().intValue() != 2)
        {

          if (ticketItemDiscount.getType().intValue() == 1) {
            discount = discount.add(NumberUtil.convertToBigDecimal(ticketItemDiscount.calculateDiscount(subtotalAmount.doubleValue())));
          }
          else if (ticketItemDiscount.getType().intValue() == 0) {
            double discountPercentage = Math.abs(ticketItemDiscount.getValue().doubleValue() * 100.0D / subtotalAmount.doubleValue());
            TicketItemDiscount percentDiscount = new TicketItemDiscount();
            percentDiscount.setType(Integer.valueOf(1));
            percentDiscount.setValue(Double.valueOf(discountPercentage));
            percentDiscount.setTicketItem(this);
            percentDiscount.setCouponQuantity(ticketItemDiscount.getCouponQuantity());
            discount = discount.add(NumberUtil.convertToBigDecimal(percentDiscount.calculateDiscount(subtotalAmount.doubleValue())));
          }
        }
      }
    }
    buildDiscounts();
    if (discount.compareTo(subtotalAmount.abs()) > 0)
      return subtotalAmount;
    return NumberUtil.round(discount);
  }
  
  public double getAmountByType(TicketItemDiscount discount)
  {
    switch (discount.getType().intValue()) {
    case 0: 
      return discount.getValue().doubleValue();
    
    case 1: 
      return discount.getValue().doubleValue() * getUnitPrice().doubleValue() / 100.0D;
    }
    
    


    return 0.0D;
  }
  
  private BigDecimal calculateTax(BigDecimal subtotal) {
    BigDecimal totalTaxAmount = NumberUtil.convertToBigDecimal("0");
    List<TicketItemTax> taxList = getTaxes();
    BigDecimal taxRatePercentage; if (taxList != null) { BigDecimal actualPrice;
      if (isTaxIncluded().booleanValue()) {
        taxRatePercentage = NumberUtil.convertToBigDecimal(0.0D);
        for (TicketItemTax ticketItemTax : taxList) {
          taxRatePercentage = taxRatePercentage.add(NumberUtil.convertToBigDecimal(ticketItemTax.getRate().doubleValue()).divide(NumberUtil.convertToBigDecimal("100"), 4, RoundingMode.HALF_EVEN));
        }
        
        if (taxRatePercentage.compareTo(NumberUtil.convertToBigDecimal("0")) == 0) {
          return totalTaxAmount;
        }
        
        BigDecimal totalTaxRatePercentage = taxRatePercentage.add(NumberUtil.convertToBigDecimal(1.0D));
        actualPrice = subtotal.divide(totalTaxRatePercentage, 4, RoundingMode.HALF_EVEN);
        
        for (TicketItemTax ticketItemTax : taxList) {
          taxRatePercentage = NumberUtil.convertToBigDecimal(ticketItemTax.getRate().doubleValue()).divide(NumberUtil.convertToBigDecimal("100"), 4, RoundingMode.HALF_EVEN);
          BigDecimal tax = actualPrice.multiply(taxRatePercentage);
          ticketItemTax.setTaxAmount(Double.valueOf(tax.doubleValue()));
          totalTaxAmount = totalTaxAmount.add(tax);
        }
      }
      else
      {
        for (TicketItemTax ticketItemTax : taxList) {
          BigDecimal taxRatePercentage = NumberUtil.convertToBigDecimal(ticketItemTax.getRate().doubleValue()).divide(NumberUtil.convertToBigDecimal("100"), 4, RoundingMode.HALF_EVEN);
          if (taxRatePercentage.compareTo(NumberUtil.convertToBigDecimal("0")) == 0) {
            return totalTaxAmount;
          }
          
          BigDecimal tax = subtotal.multiply(taxRatePercentage);
          ticketItemTax.setTaxAmount(Double.valueOf(tax.doubleValue()));
          totalTaxAmount = totalTaxAmount.add(tax);
        }
      }
    }
    

    buildTaxes();
    return NumberUtil.round(totalTaxAmount);
  }
  
  public double getTotalTaxRate() {
    List<TicketItemTax> ticketItemTaxes = getTaxes();
    if ((ticketItemTaxes == null) || (ticketItemTaxes.isEmpty())) {
      return 0.0D;
    }
    double totalTaxRate = 0.0D;
    for (TicketItemTax tax : ticketItemTaxes) {
      totalTaxRate += tax.getRate().doubleValue();
    }
    return totalTaxRate;
  }
  
  public String getNameDisplay()
  {
    String displayName = getItemQuantityDisplay() + "x ";
    if (isComboItem().booleanValue()) {
      List<TicketItem> comboItems = getComboItems();
      if ((comboItems != null) && (!comboItems.isEmpty())) {
        displayName = displayName + getName();
        displayName = displayName + "\n";
        for (Iterator iterator = comboItems.iterator(); iterator.hasNext();) {
          TicketItem item = (TicketItem)iterator.next();
          displayName = displayName + "#" + NumberUtil.trimDecilamIfNotNeeded(item.getQuantity(), true) + " " + item.getName();
          List<TicketItemModifier> ticketItemModifiers = item.getTicketItemModifiers();
          if ((iterator.hasNext()) || (ticketItemModifiers != null)) {
            displayName = displayName + "\n";
          }
          if ((ticketItemModifiers != null) && (!ticketItemModifiers.isEmpty())) {
            for (iterator2 = ticketItemModifiers.iterator(); iterator2.hasNext();) {
              TicketItemModifier modifier = (TicketItemModifier)iterator2.next();
              displayName = displayName + modifier.getNameDisplay();
              if (iterator.hasNext())
                displayName = displayName + "\n";
            }
          }
        }
        Iterator iterator2;
        return displayName;
      }
    }
    else if (getSizeModifier() != null) {
      displayName = displayName + getSizeModifier().getNameDisplay().replaceAll(" -- ", "") + " " + getName();
      return displayName;
    }
    
    return displayName += getName();
  }
  
  public Double getUnitPriceDisplay()
  {
    if (isTreatAsSeat().booleanValue()) {
      return null;
    }
    return getUnitPrice();
  }
  
  public String getItemQuantityDisplay()
  {
    if (isTreatAsSeat().booleanValue()) {
      return "";
    }
    String itemQuantity = NumberUtil.trimDecilamIfNotNeeded(getQuantity(), true);
    if (isFractionalUnit().booleanValue()) {
      return itemQuantity + getUnitName();
    }
    
    return itemQuantity;
  }
  
  public Double getTaxAmountWithoutModifiersDisplay()
  {
    return getTaxAmountWithoutModifiers();
  }
  
  public Double getTotalAmountWithoutModifiersDisplay()
  {
    return getTotalAmountWithoutModifiers();
  }
  
  public Double getSubTotalAmountWithoutModifiersDisplay()
  {
    if (isTreatAsSeat().booleanValue())
      return null;
    return getSubtotalAmountWithoutModifiers();
  }
  
  public String getItemCode()
  {
    return String.valueOf(getMenuItemId());
  }
  
  public List<Printer> getPrinters(OrderType orderType) {
    PosPrinters printers = PosPrinters.load();
    PrinterGroup printerGroup = getPrinterGroup();
    
    List<Printer> printerAll = new ArrayList();
    
    if (printerGroup == null) {
      printerAll.add(printers.getDefaultKitchenPrinter());
      return printerAll;
    }
    
    List<String> printerNames = printerGroup.getPrinterNames();
    List<Printer> kitchenPrinters = printers.getKitchenPrinters();
    for (Printer printer : kitchenPrinters) {
      if (printerNames.contains(printer.getVirtualPrinter().getName())) {
        printerAll.add(printer);
      }
    }
    if ((printerAll.isEmpty()) && (PrintServiceUtil.getFallBackPrinter() != null)) {
      printerAll.add(PrintServiceUtil.getFallBackPrinter());
    }
    return printerAll;
  }
  
  public boolean canAddDiscount()
  {
    return true;
  }
  
  public boolean canVoid()
  {
    return true;
  }
  
  public boolean canAddAdOn()
  {
    return true;
  }
  
  public MenuItem getMenuItem() {
    if (menuItem == null) {
      String itemId = getMenuItemId();
      if (StringUtils.isEmpty(itemId)) {
        return null;
      }
      menuItem = MenuItemDAO.getInstance().loadInitialized(itemId);
    }
    
    return menuItem;
  }
  
  public void setMenuItem(MenuItem menuItem) {
    this.menuItem = menuItem;
    setMenuItemId(menuItem.getId());
  }
  
  public KitchenStatus getKitchenStatusValue() {
    return KitchenStatus.fromString(super.getKitchenStatus());
  }
  
  public void setKitchenStatusValue(KitchenStatus kitchenStatus) {
    super.setKitchenStatus(kitchenStatus.name());
  }
  
  public String getUnitName()
  {
    if (super.getUnitName() == null) {
      return "";
    }
    return super.getUnitName();
  }
  
  public Double getQuantityToShip() {
    return Double.valueOf(quantityToShip == null ? 0.0D : quantityToShip.doubleValue());
  }
  
  public void setQuantityToShip(Double shipQuantity) {
    quantityToShip = shipQuantity;
  }
  
  public Double getSubTotalAmountDisplay()
  {
    if (isTreatAsSeat().booleanValue())
      return null;
    return getSubtotalAmount();
  }
  
  public TicketItemModifier findTicketItemModifierFor(MenuModifier menuModifier, Multiplier multiplier) {
    List<TicketItemModifier> modifiers = getTicketItemModifiers();
    if (modifiers == null) {
      return null;
    }
    for (TicketItemModifier ticketItemModifier : modifiers) {
      String itemId = ticketItemModifier.getItemId();
      if ((itemId != null) && (itemId.equals(menuModifier.getId())) && (multiplier.getId().equals(ticketItemModifier.getMultiplierName()))) {
        return ticketItemModifier;
      }
    }
    return null;
  }
  
  public TicketItemModifier findTicketItemModifierFor(MenuModifier menuModifier, String sectionName) {
    return findTicketItemModifierFor(menuModifier, sectionName, null);
  }
  
  public TicketItemModifier findTicketItemModifierFor(MenuModifier menuModifier, String sectionName, Multiplier multiplier) {
    List<TicketItemModifier> modifiers = getTicketItemModifiers();
    if (modifiers == null) {
      return null;
    }
    for (TicketItemModifier ticketItemModifier : modifiers) {
      String itemId = ticketItemModifier.getItemId();
      if ((multiplier != null) && 
        (itemId != null) && (itemId.equals(menuModifier.getId())) && (sectionName != null) && (sectionName.equals(ticketItemModifier.getSectionName())) && (multiplier != null) && 
        (multiplier.getId().equals(ticketItemModifier.getMultiplierName()))) {
        return ticketItemModifier;
      }
      

      if ((itemId != null) && (itemId.equals(menuModifier.getId())) && (sectionName != null) && (sectionName.equals(ticketItemModifier.getSectionName()))) {
        return ticketItemModifier;
      }
    }
    return null;
  }
  
  public int countModifierFromGroup(MenuItemModifierSpec menuItemModifierGroup) {
    List<TicketItemModifier> modifiers = getTicketItemModifiers();
    if (modifiers == null) {
      return 0;
    }
    int modifierFromGroupCount = 0;
    for (TicketItemModifier ticketItemModifier : modifiers) {
      String groupId = ticketItemModifier.getGroupId();
      if ((groupId != null) && (groupId.equals(menuItemModifierGroup.getId()))) {
        modifierFromGroupCount = (int)(modifierFromGroupCount + ticketItemModifier.getItemQuantity().doubleValue());
      }
    }
    return modifierFromGroupCount;
  }
  
  public boolean requiredModifiersAdded(MenuItemModifierSpec menuItemModifierGroup) {
    int minQuantity = menuItemModifierGroup.getMinQuantity().intValue();
    if (minQuantity == 0) {
      return true;
    }
    return countModifierFromGroup(menuItemModifierGroup) >= minQuantity;
  }
  
  public boolean deleteTicketItemModifier(TicketItemModifier ticketItemModifierToRemove) {
    List<TicketItemModifier> modifiers = getTicketItemModifiers();
    if (modifiers == null) {
      return false;
    }
    for (Iterator iterator = modifiers.iterator(); iterator.hasNext();) {
      TicketItemModifier ticketItemModifier = (TicketItemModifier)iterator.next();
      if (ticketItemModifier == ticketItemModifierToRemove) {
        iterator.remove();
        return true;
      }
    }
    
    return false;
  }
  
  public boolean deleteTicketItemModifierByName(TicketItemModifier ticketItemModifierToRemove) {
    List<TicketItemModifier> modifiers = getTicketItemModifiers();
    if (modifiers == null) {
      return false;
    }
    for (Iterator iterator = modifiers.iterator(); iterator.hasNext();) {
      TicketItemModifier ticketItemModifier = (TicketItemModifier)iterator.next();
      if (ticketItemModifier.getName().equals(ticketItemModifierToRemove.getName())) {
        iterator.remove();
        return true;
      }
    }
    
    return false;
  }
  
  public static enum PIZZA_SECTION_MODE {
    FULL(1),  HALF(2),  QUARTER(3);
    
    private final int value;
    
    private PIZZA_SECTION_MODE(int value) {
      this.value = value;
    }
    
    public int getValue() {
      return value;
    }
    
    public static PIZZA_SECTION_MODE from(int value) {
      if (value == 2) {
        return HALF;
      }
      if (value == 3) {
        return QUARTER;
      }
      return FULL;
    }
    
    public String toString()
    {
      return name();
    }
  }
  
  public void setPizzaSectionMode(PIZZA_SECTION_MODE pizzaSectionMode) {
    setPizzaSectionModeType(Integer.valueOf(pizzaSectionMode.getValue()));
  }
  
  public PIZZA_SECTION_MODE getPizzaSectionMode() {
    return PIZZA_SECTION_MODE.from(getPizzaSectionModeType().intValue());
  }
  
  public TicketItemModifier findTicketItemModifierByPageItem(MenuModifier menuModifier) {
    List<TicketItemModifier> modifiers = getTicketItemModifiers();
    if (modifiers == null) {
      return null;
    }
    for (TicketItemModifier ticketItemModifier : modifiers) {
      String itemId = ticketItemModifier.getPageItemId();
      if ((itemId != null) && (itemId.equals(menuModifier.getPageItemId()))) {
        return ticketItemModifier;
      }
    }
    return null;
  }
  
  public TicketItemModifier findTicketItemComboModifierFor(MenuModifier menuModifier) {
    List<TicketItemModifier> modifiers = getTicketItemModifiers();
    if (modifiers == null) {
      return null;
    }
    for (TicketItemModifier ticketItemModifier : modifiers) {
      String itemId = ticketItemModifier.getItemId();
      if ((itemId != null) && (itemId.equals(menuModifier.getId())) && (ticketItemModifier.getModifierType().intValue() == 3)) {
        return ticketItemModifier;
      }
    }
    return null;
  }
  
  public TicketItemModifier removeTicketItemModifierByPageItem(TicketItemModifier ticketItemModifier) {
    List<TicketItemModifier> ticketItemModifiers = getTicketItemModifiers();
    if (ticketItemModifiers == null) {
      return ticketItemModifier;
    }
    for (Iterator iter = ticketItemModifiers.iterator(); iter.hasNext();) {
      TicketItemModifier oldTicketItemModifier = (TicketItemModifier)iter.next();
      String pageItemId = oldTicketItemModifier.getPageItemId();
      if ((pageItemId != null) && (pageItemId.equals(ticketItemModifier.getPageItemId())) && 
        (oldTicketItemModifier.getModifierType() == ticketItemModifier.getModifierType())) {
        iter.remove();
        return oldTicketItemModifier;
      }
    }
    return ticketItemModifier;
  }
  
  public void removeTicketItemDiscount(TicketItemDiscount itemDiscount) {
    List<TicketItemDiscount> discounts = getDiscounts();
    if (discounts == null) {
      return;
    }
    
    for (Iterator iterator = discounts.iterator(); iterator.hasNext();) {
      TicketItemDiscount ticketItemDiscount = (TicketItemDiscount)iterator.next();
      if (ticketItemDiscount.getTableRowNum() == itemDiscount.getTableRowNum()) {
        iterator.remove();
        return;
      }
    }
  }
  
  public boolean isInventoryAdjusted() {
    return getQuantity().doubleValue() == getInventoryAdjustQty().doubleValue();
  }
  
  public Boolean isIncludeVoidQuantity() {
    return Boolean.valueOf(includeVoidQuantity == null ? false : includeVoidQuantity.booleanValue());
  }
  
  public void setIncludeVoidQuantity(Boolean includeVoidQuantity) {
    this.includeVoidQuantity = includeVoidQuantity;
  }
  
  public void setVoidItem(VoidItem voidItem) {
    this.voidItem = voidItem;
  }
  
  public VoidItem getVoidItem() {
    return voidItem;
  }
  
  public void markVoided(String voidReason, boolean itemWasted, double quantity) {
    setVoided(Boolean.valueOf(true));
    setQuantity(Double.valueOf(-quantity));
    setVoidItem(createVoidItem(voidReason, itemWasted, quantity));
  }
  
  public VoidItem createVoidItem(String voidReason, boolean wasted, double quantity) {
    VoidItem newVoidItem = new VoidItem();
    if (getTicket() != null) {
      newVoidItem.setTicketId(getTicket().getId());
    }
    newVoidItem.setVoidReason(voidReason);
    newVoidItem.setItemId(getId());
    newVoidItem.setMenuItemId(getMenuItemId());
    newVoidItem.setMenuItemName(getName());
    newVoidItem.setUnitPrice(getUnitPrice());
    newVoidItem.setQuantity(Double.valueOf(quantity));
    newVoidItem.setItemWasted(Boolean.valueOf(wasted));
    newVoidItem.setVoidDate(new Date());
    newVoidItem.setModifier(Boolean.valueOf(false));
    newVoidItem.setPrinterGroup(getPrinterGroup());
    User currentUser = Application.getCurrentUser();
    newVoidItem.setVoidByUser(currentUser);
    newVoidItem.setTerminal(Application.getInstance().getTerminal());
    if (currentUser.getActiveDrawerPullReport() != null) {
      newVoidItem.setCashDrawerId(currentUser.getActiveDrawerPullReport().getId());
    }
    
    double subtotal = getUnitPrice().doubleValue() * newVoidItem.getQuantity().doubleValue();
    double taxAmount = subtotal * (getTotalTaxRate() / 100.0D);
    newVoidItem.setTaxAmount(Double.valueOf(taxAmount));
    newVoidItem.setTotalPrice(Double.valueOf(subtotal + taxAmount));
    
    double ticketItemQuantity = getQuantity().doubleValue();
    double voidItemQuantity = newVoidItem.getQuantity().doubleValue();
    List<VoidItem> modifierVoidItems = new ArrayList();
    if (isHasModifiers().booleanValue()) {
      for (TicketItemModifier ticketItemModifier : getTicketItemModifiers()) {
        VoidItem modifierVoidItem = new VoidItem();
        double voidModifierquantity = ticketItemModifier.getItemQuantity().doubleValue() * voidItemQuantity / ticketItemQuantity;
        if (voidModifierquantity > 0.0D) {
          String tfvoidItemReason = newVoidItem.getVoidReason();
          if (!StringUtils.isEmpty(tfvoidItemReason)) {
            modifierVoidItem.setVoidReason(tfvoidItemReason);
          }
          
          modifierVoidItem.setModifierId(ticketItemModifier.getItemId());
          modifierVoidItem.setItemId(ticketItemModifier.getId());
          modifierVoidItem.setMenuItemName(ticketItemModifier.getNameDisplay());
          modifierVoidItem.setUnitPrice(ticketItemModifier.getUnitPrice());
          modifierVoidItem.setQuantity(Double.valueOf(voidModifierquantity));
          modifierVoidItem.setItemWasted(newVoidItem.isItemWasted());
          modifierVoidItem.setVoidDate(new Date());
          modifierVoidItem.setModifier(Boolean.valueOf(true));
          modifierVoidItem.setVoidByUser(currentUser);
          modifierVoidItem.setTerminal(Application.getInstance().getTerminal());
          modifierVoidItem.setCashDrawerId(currentUser.getActiveDrawerPullReport().getId());
          double modifierSubtotal = voidModifierquantity * ticketItemModifier.getUnitPrice().doubleValue();
          double modifierTaxAmount = subtotal * (ticketItemModifier.getTotalTaxRate() / 100.0D);
          modifierVoidItem.setTaxAmount(Double.valueOf(modifierSubtotal));
          modifierVoidItem.setTotalPrice(Double.valueOf(modifierSubtotal + modifierTaxAmount));
          modifierVoidItem.setTicketId(newVoidItem.getTicketId());
          modifierVoidItems.add(modifierVoidItem);
        }
      }
    }
    newVoidItem.setVoidModifiers(modifierVoidItems);
    return newVoidItem;
  }
  
  public boolean isSaved()
  {
    return getId() != null;
  }
  










  public boolean isRefundable()
  {
    return true;
  }
  
  public List<TicketItemCookingInstruction> getCookingInstructions() {
    if (cookingInstructions == null) {
      cookingInstructions = new ArrayList();
      
      String property = super.getCookingInstructionsProperty();
      if (StringUtils.isNotEmpty(property)) {
        JsonReader jsonParser = Json.createReader(new StringReader(property));
        JsonArray jsonArray = jsonParser.readArray();
        jsonParser.close();
        for (int i = 0; i < jsonArray.size(); i++) {
          JsonObject jsonObject = (JsonObject)jsonArray.get(i);
          TicketItemCookingInstruction tic = new TicketItemCookingInstruction();
          tic.setDescription(JsonUtil.getString(jsonObject, TicketItemCookingInstruction.PROP_DESCRIPTION));
          tic.setPrintedToKitchen(JsonUtil.getBoolean(jsonObject, TicketItemCookingInstruction.PROP_PRINTED_TO_KITCHEN));
          tic.setSaved(JsonUtil.getBoolean(jsonObject, TicketItemCookingInstruction.PROP_SAVED));
          cookingInstructions.add(tic);
        }
      }
    }
    return cookingInstructions;
  }
  
  public PrinterGroup getPrinterGroup() {
    return DataProvider.get().getPrinterGroupById(getPrinterGroupId());
  }
  
  public void setPrinterGroup(PrinterGroup printerGroup) {
    String printerGroupId = null;
    if (printerGroup != null) {
      printerGroupId = printerGroup.getId();
    }
    super.setPrinterGroupId(printerGroupId);
  }
  
  public TicketItem getParentTicketItem() {
    return parentTicketItem;
  }
  
  public void setParentTicketItem(TicketItem parentTicketItem) {
    this.parentTicketItem = parentTicketItem;
  }
  
  public List<TicketItem> getComboItems() {
    return null;
  }
  

  public void addTocomboItems(TicketItem item) {}
  
  public ImageIcon getCourseIcon()
  {
    if (StringUtils.isEmpty(getCourseId()))
      return null;
    Course course = DataProvider.get().getCourse(getCourseId());
    return course == null ? null : course.getIcon();
  }
  
  public Integer getSortOrder() {
    if (sortOrder != null)
      return sortOrder;
    return Integer.valueOf(StringUtils.isEmpty(getCourseId()) ? 0 : DataProvider.get().getCourse(getCourseId()).getSortOrder().intValue());
  }
  
  public void setSortOrder(Integer sortOrder) {
    this.sortOrder = sortOrder;
  }
  
  public boolean isSyncEdited() {
    return syncEdited;
  }
  
  public void setSyncEdited(boolean syncEdited) {
    this.syncEdited = syncEdited;
  }
}
