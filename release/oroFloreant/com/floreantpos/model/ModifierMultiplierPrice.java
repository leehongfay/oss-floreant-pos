package com.floreantpos.model;

import com.floreantpos.model.base.BaseModifierMultiplierPrice;
import com.floreantpos.model.util.DataProvider;



public class ModifierMultiplierPrice
  extends BaseModifierMultiplierPrice
{
  private static final long serialVersionUID = 1L;
  
  public ModifierMultiplierPrice() {}
  
  public ModifierMultiplierPrice(String id)
  {
    super(id);
  }
  




  public Multiplier getMultiplier()
  {
    return DataProvider.get().getMultiplierById(getMultiplierId());
  }
  



  public void setMultiplier(Multiplier multiplier)
  {
    setMultiplierId(multiplier == null ? null : multiplier.getId());
  }
  
  public Double getPrice()
  {
    return price;
  }
}
