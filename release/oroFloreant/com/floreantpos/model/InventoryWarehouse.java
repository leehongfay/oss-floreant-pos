package com.floreantpos.model;

import com.floreantpos.model.base.BaseInventoryWarehouse;






















public class InventoryWarehouse
  extends BaseInventoryWarehouse
{
  private static final long serialVersionUID = 1L;
  
  public InventoryWarehouse() {}
  
  public InventoryWarehouse(String id)
  {
    super(id);
  }
  





  public InventoryWarehouse(String id, String name)
  {
    super(id, name);
  }
  




  public Boolean isVisible()
  {
    return Boolean.valueOf(visible == null ? true : visible.booleanValue());
  }
  
  public String toString()
  {
    return getName();
  }
}
