package com.floreantpos.model;

import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.base.BaseTicket;
import com.floreantpos.model.dao.ShopTableDAO;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.util.DiscountUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;






















@XmlRootElement(name="ticket")
public class Ticket
  extends BaseTicket
{
  private static SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy, h:m a");
  
  private static final String CUSTOMER_TAX_EXEMPT = "customer.taxExempt";
  
  private static final long serialVersionUID = 1L;
  
  public static final String PROPERTY_ONLINE_ID = "onlineOrderId";
  
  public static final String PROPERTY_CARD_TRANSACTION_ID = "card_transaction_id";
  
  public static final String PROPERTY_CARD_TRACKS = "card_tracks";
  
  public static final String PROPERTY_CARD_NAME = "card_name";
  
  public static final String PROPERTY_PAYMENT_METHOD = "payment_method";
  
  public static final String PROPERTY_CARD_READER = "card_reader";
  
  public static final String PROPERTY_CARD_NUMBER = "card_number";
  public static final String PROPERTY_CARD_EXP_YEAR = "card_exp_year";
  public static final String PROPERTY_CARD_EXP_MONTH = "card_exp_month";
  public static final String PROPERTY_ADVANCE_PAYMENT = "advance_payment";
  public static final String PROPERTY_CARD_AUTH_CODE = "card_auth_code";
  public static final String PROPERTY_FOR_HERE_AND_TO_GO = "ticket.ForHereToGo";
  public static final String STATUS_WAITING = "Waiting (Kitchen)";
  public static final String STATUS_READY = "Ready";
  public static final String STATUS_NOT_SENT = "Not Sent";
  public static final String STATUS_DRIVING = "Driving";
  public static final String STATUS_VOID = "Void";
  public static final String CUSTOMER_PHONE = "CUSTOMER_MOBILE";
  public static final String CUSTOMER_NAME = "CUSTOMER_NAME";
  public static final String CUSTOMER_LAST_NAME = "CUSTOMER_LAST_NAME";
  public static final String CUSTOMER_ID = "CUSTOMER_ID";
  public static final String CUSTOMER_ZIP_CODE = "CUSTOMER_ZIP_CODE";
  public static final String MANAGER_INSTRUCTION = "MANAGER_INSTRUCTION";
  public static final String PHONE_EXTENSION = "PHONE_EXTENSION";
  public static final String DRIVER_OUT_TIME = "OUT_AT";
  public static final String PROPERTY_DISCOUNT = "DISCOUNT";
  public static final int ORDER_PENDING = 0;
  public static final int ORDER_VERIFIED = 1;
  public static final int ORDER_FULLY_INVOICED = 2;
  public static final int ORDER_PARTIALLY_INVOICED = 3;
  public static final int ORDER_FULLY_SHIPMENT = 4;
  public static final int ORDER_PARTIALLY_SHIPMENT = 5;
  public static final int ORDER_PARTIALLY_SHIPMENT_AND_INVOICED = 6;
  public static final int ORDER_CLOSED = 7;
  public static final int ORDER_CANCELLED = 8;
  public static final String[] ORDER_STATUS = { "Pending", "Verified", "Fully Invoiced", "Partially Invoiced", "Fully Shipped", "Partially Shipped", "Partially Shipped and Invoiced", "Closed", "Cancelled" };
  
  public static final String ORDER_VERIFIED_BY = "Verified_By";
  
  public static final String ORDER_SENT_BY = "Sent_By";
  public static final String ORDER_SHIPMENT_BY = "Shipping_By";
  public static final String ORDER_INVOICED_BY = "Invoiced_By";
  public static final String ORDER_CLOSED_BY = "Closed_By";
  public static final String DEBIT = "DEBIT";
  public static final String CREDIT = "CREDIT";
  public static final String SPLIT = "split";
  public static final String SPLIT_TICKET_ID = "split_ticket_id";
  public static final String SPLIT_TYPE = "SPLIT_TYPE";
  public static final int SPLIT_EQUALLY = 0;
  public static final int SPLIT_BY_SEAT = 1;
  public static final int SPLIT_MANUALLY = 2;
  private double ticketDiscountAmount;
  private double itemDiscountAmount;
  private double voidSubtotal;
  private double voidTotal;
  private List deletedItems;
  private String sortOrder;
  private boolean needSpecialAttention;
  private Customer customer;
  private Outlet outlet;
  private Department department;
  private SalesArea salesArea;
  private List<TicketDiscount> discounts;
  private List<Integer> tableNumbers;
  private List<ShopTable> tables;
  private Boolean refundableItem;
  
  public Ticket() {}
  
  public Ticket(String id)
  {
    super(id);
  }
  




















  public TicketType getTicketType()
  {
    return TicketType.getByTypeNo(super.getType());
  }
  
  public void setTicketType(TicketType type) {
    if (type != null) {
      super.setType(Integer.valueOf(type.getTypeNo()));
    }
  }
  
  public void addTable(int tableNumber) {
    List<Integer> numbers = getTableNumbers();
    if (numbers == null) {
      numbers = new ArrayList();
      setTableNumbers(numbers);
    }
    
    numbers.add(Integer.valueOf(tableNumber));
  }
  
  public void setClosed(Boolean closed)
  {
    super.setClosed(closed);
  }
  
  public double getGratuityAmount() {
    Gratuity gratuity = getGratuity();
    if (gratuity != null) {
      return gratuity.getAmount().doubleValue();
    }
    return 0.0D;
  }
  
  public void updateGratuityInfo() {
    Gratuity gratuity = getGratuity();
    if (gratuity == null) {
      return;
    }
    Store store = Application.getInstance().getStore();
    if (getAssignedDriver() != null) {
      TipsReceivedBy tipsReceiverForDelivery = store.getTipsReceivedByForDeliveryOrder();
      if (tipsReceiverForDelivery == TipsReceivedBy.Driver) {
        gratuity.setOwnerId(getAssignedDriver().getId());
      }
      else if (tipsReceiverForDelivery == TipsReceivedBy.Cashier) {
        gratuity.setOwnerId(getCashier() != null ? getCashier().getId() : getOwner().getId());
      }
      else {
        gratuity.setOwnerId(getOwner().getId());
      }
    }
    else
    {
      TipsReceivedBy tipsReceiverForNonDelivery = store.getTipsReceivedByForNonDeliveryOrder();
      
      if (tipsReceiverForNonDelivery == TipsReceivedBy.Cashier) {
        gratuity.setOwnerId(getCashier().getId());
      }
      else {
        gratuity.setOwnerId(getOwner().getId());
      }
    }
    gratuity.setTicketId(getId());
  }
  
  public void setGratuityAmount(double amount) {
    Gratuity gratuity = getGratuity();
    if (gratuity == null) {
      gratuity = createGratuity();
      setGratuity(gratuity);
    }
    
    gratuity.setAmount(Double.valueOf(amount));
  }
  
  public Gratuity createGratuity() {
    Gratuity gratuity = getGratuity();
    if (gratuity == null) {
      gratuity = new Gratuity();
      gratuity.setTicketId(getId());
      gratuity.setTerminalId(Application.getInstance().getTerminal().getId());
      gratuity.setPaid(Boolean.valueOf(false));
    }
    
    return gratuity;
  }
  
  public boolean hasGratuity() {
    return getGratuity() != null;
  }
  
  public void setCreateDate(Date createDate)
  {
    super.setCreateDate(createDate);
    super.setActiveDate(createDate);
  }
  
  public List<TicketItem> getTicketItems()
  {
    List<TicketItem> items = super.getTicketItems();
    
    if (items == null) {
      items = new ArrayList();
      super.setTicketItems(items);
    }
    return items;
  }
  
  public Integer getNumberOfGuests()
  {
    Integer guests = super.getNumberOfGuests();
    if ((guests == null) || (guests.intValue() == 0)) {
      return Integer.valueOf(1);
    }
    return guests;
  }
  
  public String getCreateDateFormatted() {
    return dateFormat.format(getCreateDate());
  }
  
  public String getTitle() {
    String title = "";
    if (getId() != null) {
      title = title + "#" + getId();
    }
    title = title + ": " + getOwner();
    title = title + ":" + getCreateDateFormatted();
    title = title + ": " + NumberUtil.formatNumber(getTotalAmountWithTips());
    
    return title;
  }
  
  public int getBeverageCount() {
    List<TicketItem> ticketItems = getTicketItems();
    if (ticketItems == null) {
      return 0;
    }
    int count = 0;
    for (TicketItem ticketItem : ticketItems) {
      if (ticketItem.isBeverage().booleanValue()) {
        count = (int)(count + ticketItem.getQuantity().doubleValue());
      }
    }
    return count;
  }
  
  public void calculatePrice() {
    List<TicketItem> ticketItems = getTicketItems();
    if (ticketItems == null) {
      return;
    }
    if (!isShouldCalculatePrice().booleanValue()) {
      setDiscountAmount(Double.valueOf(calculateTicketDiscount(getSubtotalAmount().doubleValue(), 0.0D, 0.0D)));
      return;
    }
    PosLog.debug(getClass(), "Price calculation start----------------");
    
    BigDecimal subtotal = NumberUtil.convertToBigDecimal(0.0D);
    BigDecimal discount = NumberUtil.convertToBigDecimal(0.0D);
    BigDecimal itemsServiceChargeAmount = NumberUtil.convertToBigDecimal(0.0D);
    BigDecimal taxAmount = NumberUtil.convertToBigDecimal(0.0D);
    BigDecimal itemDiscountAmountWithVoidItems = NumberUtil.convertToBigDecimal(0.0D);
    voidSubtotal = 0.0D;
    voidTotal = 0.0D;
    itemDiscountAmount = 0.0D;
    ticketDiscountAmount = 0.0D;
    double gratuityAmount = getGratuityAmount();
    
    setRefundableItem(false);
    
    for (TicketItem ticketItem : ticketItems) {
      ticketItem.calculatePrice();
      
      subtotal = NumberUtil.round(subtotal.add(NumberUtil.convertToBigDecimal(ticketItem.getSubtotalAmount().doubleValue())));
      itemsServiceChargeAmount = NumberUtil.round(itemsServiceChargeAmount.add(NumberUtil.convertToBigDecimal(ticketItem.getServiceCharge().doubleValue())));
      
      if (!isTaxIncluded().booleanValue()) {
        taxAmount = NumberUtil.round(taxAmount.add(NumberUtil.convertToBigDecimal(ticketItem.getTaxAmount().doubleValue())));
      }
      
      itemDiscountAmountWithVoidItems = NumberUtil.round(itemDiscountAmountWithVoidItems.add(NumberUtil.convertToBigDecimal(ticketItem.getDiscountAmount().doubleValue())));
      
      if (ticketItem.isVoided().booleanValue()) {
        setRefundableItem(true);
        if (ticketItem.getVoidedItemId() != null) {
          voidSubtotal += Math.abs(ticketItem.getSubtotalAmount().doubleValue());
          voidTotal += Math.abs(ticketItem.getSubtotalAmount().doubleValue()) + Math.abs(ticketItem.getTaxAmount().doubleValue());
        }
      }
      else {
        itemDiscountAmount += ticketItem.getDiscountAmount().doubleValue();
      }
    }
    ticketDiscountAmount = calculateTicketDiscount(subtotal.doubleValue(), voidSubtotal, itemDiscountAmount);
    discount = NumberUtil.convertToBigDecimal(calculateTotalDiscountAmount(subtotal.doubleValue(), voidSubtotal, ticketDiscountAmount, itemDiscountAmountWithVoidItems.doubleValue()));
    
    PosLog.debug(getClass(), String.format("Ticket info------------------------------", new Object[0]));
    PosLog.debug(getClass(), String.format("subtoal: %s", new Object[] { subtotal }));
    PosLog.debug(getClass(), String.format("void subtoal: %s", new Object[] { Double.valueOf(voidSubtotal) }));
    
    BigDecimal totalAmount = NumberUtil.convertToBigDecimal(0.0D);
    
    if (ticketDiscountAmount > 0.0D) {
      BigDecimal itemDiscountedSubtotal = subtotal.subtract(NumberUtil.convertToBigDecimal(itemDiscountAmount));
      BigDecimal discountSubtotal = itemDiscountedSubtotal.subtract(NumberUtil.convertToBigDecimal(ticketDiscountAmount));
      BigDecimal newTax = NumberUtil.convertToBigDecimal(0.0D);
      if (itemDiscountedSubtotal.compareTo(BigDecimal.ZERO) > 0) {
        newTax = discountSubtotal.multiply(taxAmount).divide(itemDiscountedSubtotal, 4, RoundingMode.HALF_UP);
      }
      
      if (isTaxIncluded().booleanValue()) {
        totalAmount = totalAmount.add(discountSubtotal);
        taxAmount = NumberUtil.convertToBigDecimal(0.0D);
      }
      else {
        totalAmount = totalAmount.add(discountSubtotal).add(NumberUtil.convertToBigDecimal(newTax.doubleValue()));
        taxAmount = NumberUtil.round(newTax);
      }
      
    }
    else if (isTaxIncluded().booleanValue()) {
      totalAmount = totalAmount.add(subtotal).subtract(discount).add(itemsServiceChargeAmount);
    }
    else {
      totalAmount = totalAmount.add(subtotal).subtract(discount).add(itemsServiceChargeAmount).add(taxAmount);
    }
    
    totalAmount = totalAmount.add(NumberUtil.convertToBigDecimal(getDeliveryCharge().doubleValue()));
    totalAmount = NumberUtil.round(totalAmount);
    





















    double deliveryChargeAmount = getDeliveryCharge().doubleValue();
    double ticketServiceChargeAmount = calculateServiceCharge(subtotal.doubleValue(), discount.doubleValue());
    
    BigDecimal dueAmount = NumberUtil.round(totalAmount.add(NumberUtil.convertToBigDecimal(gratuityAmount))
      .subtract(NumberUtil.convertToBigDecimal(getPaidAmount().doubleValue()))
      .add(NumberUtil.convertToBigDecimal(getRefundAmount().doubleValue())));
    

    setSubtotalAmount(Double.valueOf(subtotal.doubleValue()));
    setDiscountAmount(Double.valueOf(discount.doubleValue()));
    setServiceCharge(Double.valueOf(NumberUtil.roundToTwoDigit(itemsServiceChargeAmount.add(NumberUtil.convertToBigDecimal(ticketServiceChargeAmount)).doubleValue())));
    setTaxAmount(Double.valueOf(taxAmount.doubleValue()));
    setTotalAmount(Double.valueOf(totalAmount.doubleValue()));
    setDueAmount(Double.valueOf(dueAmount.doubleValue()));
    
    if ((ticketDiscountAmount <= 0.0D) && (getRefundableAmount().doubleValue() <= 0.0D) && (getRefundAmount().doubleValue() <= 0.0D)) {
      setSubtotalAmount(Double.valueOf(NumberUtil.roundToTwoDigit(subtotal.doubleValue())));
      setDiscountAmount(Double.valueOf(NumberUtil.roundToTwoDigit(discount.doubleValue())));
      setServiceCharge(Double.valueOf(NumberUtil.roundToTwoDigit(getServiceCharge().doubleValue())));
      setTaxAmount(Double.valueOf(NumberUtil.roundToTwoDigit(taxAmount.doubleValue())));
      return;
    }
    
    BigDecimal adjustedTotal = NumberUtil.convertToBigDecimal(0.0D);
    
    double adjustedDiscount = 0.0D;
    for (TicketItem ticketItem : ticketItems) {
      ticketItem.calculateAdjustedPrice();
      adjustedTotal = adjustedTotal.add(NumberUtil.convertToBigDecimal(ticketItem.getAdjustedTotal().doubleValue()));
      adjustedDiscount += ticketItem.getDiscountAmount().doubleValue();
    }
    double originalTicketPrice = getTotalAmount().doubleValue() + getVoidTotal();
    ticketDiscountAmount = (ticketDiscountAmount * (originalTicketPrice - getRefundableAmount().doubleValue() - getRefundAmount().doubleValue()) / originalTicketPrice);
    adjustedDiscount += ticketDiscountAmount;
    adjustedTotal = NumberUtil.round(adjustedTotal.add(NumberUtil.convertToBigDecimal(deliveryChargeAmount))
      .add(NumberUtil.convertToBigDecimal(ticketServiceChargeAmount)));
    
    dueAmount = NumberUtil.round(adjustedTotal.add(NumberUtil.convertToBigDecimal(gratuityAmount)).subtract(NumberUtil.convertToBigDecimal(getPaidAmount().doubleValue()))
      .add(NumberUtil.convertToBigDecimal(getRefundAmount().doubleValue())));
    
    setSubtotalAmount(Double.valueOf(subtotal.doubleValue()));
    setDiscountAmount(Double.valueOf(adjustedDiscount));
    setServiceCharge(getServiceCharge());
    
    setDueAmount(Double.valueOf(dueAmount.doubleValue()));
  }
  
  public double getSubtotalAmountWithVoidItems()
  {
    return getSubtotalAmount().doubleValue() + getVoidSubtotal();
  }
  
  private double calculateTotalDiscountAmount(double subtotal, double voidSubtotal, double ticketDiscountAmount, double itemDiscountAmountWithVoidItems) {
    if ((getDiscounts() == null) || (getDiscounts().size() == 0)) {
      return itemDiscountAmountWithVoidItems;
    }
    double subtotalAmountWithVoidItems = subtotal + voidSubtotal;
    double totalDiscountAmount = 0.0D;
    totalDiscountAmount += itemDiscountAmountWithVoidItems;
    totalDiscountAmount += ticketDiscountAmount;
    

    if (subtotalAmountWithVoidItems != 0.0D) {
      totalDiscountAmount -= ticketDiscountAmount * Math.abs(voidSubtotal) / subtotalAmountWithVoidItems;
    }
    
    if ((subtotal > 0.0D) && (totalDiscountAmount > subtotal)) {
      totalDiscountAmount = subtotal;
    }
    if (ticketDiscountAmount > subtotal) {
      ticketDiscountAmount = subtotal;
    }
    if (totalDiscountAmount < 0.0D) {
      totalDiscountAmount = 0.0D;
    }
    
    return NumberUtil.round(totalDiscountAmount);
  }
  
  private double calculateTicketDiscount(double subtotal, double voidSubtotal, double itemDiscount) {
    double subtotalWithVoidItems = subtotal + voidSubtotal;
    double subtotalAfterItemDiscount = subtotal - itemDiscount;
    double ticketDisount = 0.0D;
    List<TicketDiscount> ticketCouponAndDiscounts = getDiscounts();
    if (ticketCouponAndDiscounts != null) {
      for (TicketDiscount ticketDiscount : ticketCouponAndDiscounts) {
        double discountForThisCoupon = 0.0D;
        if (ticketDiscount.getType().intValue() == 2) {
          discountForThisCoupon = DiscountUtil.calculateRepriceDiscount(this, ticketDiscount.getValue().doubleValue()).doubleValue();
        }
        else if (ticketDiscount.getType().intValue() == 1) {
          Double calculateDiscountAmount = DiscountUtil.calculateDiscountAmount(subtotalWithVoidItems - itemDiscountAmount, ticketDiscount);
          discountForThisCoupon = calculateDiscountAmount.doubleValue() * ticketDiscount.getCouponQuantity().doubleValue();
        }
        else {
          Double calculateDiscountAmount = DiscountUtil.calculateDiscountAmount(subtotalAfterItemDiscount, ticketDiscount);
          discountForThisCoupon = calculateDiscountAmount.doubleValue() * ticketDiscount.getCouponQuantity().doubleValue();
        }
        
        ticketDiscount.setTotalDiscountAmount(Double.valueOf(discountForThisCoupon));
        ticketDisount += discountForThisCoupon;
      }
    }
    buildDiscounts();
    return NumberUtil.round(ticketDisount);
  }
  
  public double getAmountByType(TicketDiscount discount) {
    switch (discount.getType().intValue()) {
    case 0: 
      return discount.getValue().doubleValue();
    
    case 1: 
      return discount.getValue().doubleValue() * getSubtotalAmount().doubleValue() / 100.0D;
    }
    
    

    return 0.0D;
  }
  
  public static TicketDiscount convertToTicketDiscount(Discount discount, Ticket ticket) {
    TicketDiscount ticketDiscount = new TicketDiscount();
    ticketDiscount.setDiscountId(discount.getId());
    ticketDiscount.setName(discount.getName());
    ticketDiscount.setType(discount.getType());
    ticketDiscount.setMinimumAmount(discount.getMinimumBuy());
    ticketDiscount.setValue(discount.getValue());
    ticketDiscount.setTicket(ticket);
    return ticketDiscount;
  }
  
  private double fixInvalidAmount(double amount) {
    if (Double.isNaN(amount)) {
      amount = 0.0D;
    }
    return amount;
  }
  
  public double calculateDiscountFromType(TicketDiscount coupon, double subtotal) {
    List<TicketItem> ticketItems = getTicketItems();
    
    double discount = 0.0D;
    int type = coupon.getType().intValue();
    double couponValue = coupon.getValue().doubleValue();
    HashSet<String> categoryIds;
    HashSet<String> categoryIds; switch (type) {
    case 3: 
      discount += couponValue;
      break;
    
    case 1: 
      categoryIds = new HashSet();
      for (TicketItem item : ticketItems) {
        String itemId = item.getMenuItemId();
        if (!categoryIds.contains(itemId)) {
          discount += couponValue;
          categoryIds.add(itemId);
        }
      }
      break;
    
    case 2: 
      for (TicketItem item : ticketItems) {
        discount += couponValue * item.getQuantity().doubleValue();
      }
      break;
    
    case 6: 
      discount += subtotal * couponValue / 100.0D;
      break;
    
    case 4: 
      categoryIds = new HashSet();
      for (TicketItem item : ticketItems) {
        String itemId = item.getMenuItemId();
        if (!categoryIds.contains(itemId)) {
          discount += item.getUnitPrice().doubleValue() * couponValue / 100.0D;
          categoryIds.add(itemId);
        }
      }
      break;
    
    case 5: 
      for (TicketItem item : ticketItems) {
        discount += item.getSubtotalAmountWithoutModifiers().doubleValue() * couponValue / 100.0D;
      }
      break;
    
    case 0: 
      discount += couponValue;
    }
    
    return discount;
  }
  
  public void addDeletedItems(Object o) {
    if (deletedItems == null) {
      deletedItems = new ArrayList();
    }
    
    deletedItems.add(o);
  }
  
  public List getDeletedItems() {
    return deletedItems;
  }
  
  public void clearDeletedItems() {
    if (deletedItems != null) {
      deletedItems.clear();
    }
    
    deletedItems = null;
  }
  
  public int countItem(TicketItem ticketItem) {
    List<TicketItem> items = getTicketItems();
    if (items == null) {
      return 0;
    }
    
    int count = 0;
    for (TicketItem ticketItem2 : items) {
      if (ticketItem.getMenuItemId().equals(ticketItem2.getMenuItemId())) {
        count++;
      }
    }
    
    return count;
  }
  
  public boolean needsKitchenPrint() {
    if ((getDeletedItems() != null) && (getDeletedItems().size() > 0)) {
      return true;
    }
    
    List<TicketItem> ticketItems = getTicketItems();
    for (TicketItem item : ticketItems) {
      if ((item.isShouldPrintToKitchen().booleanValue()) && (!item.isPrintedToKitchen().booleanValue())) {
        return true;
      }
      
      List<TicketItemModifier> ticketItemModifiers = item.getTicketItemModifiers();
      Iterator localIterator2; if (ticketItemModifiers != null) {
        for (localIterator2 = ticketItemModifiers.iterator(); localIterator2.hasNext();) { modifier = (TicketItemModifier)localIterator2.next();
          if ((modifier.isShouldPrintToKitchen().booleanValue()) && (!modifier.isPrintedToKitchen().booleanValue())) {
            return true;
          }
        }
      }
      
      Object cookingInstructions = item.getCookingInstructions();
      if (cookingInstructions != null) {
        for (TicketItemCookingInstruction ticketItemCookingInstruction : (List)cookingInstructions) {
          if (!ticketItemCookingInstruction.isPrintedToKitchen().booleanValue()) {
            return true;
          }
        }
      }
    }
    TicketItemModifier modifier;
    return false;
  }
  
  public double calculateServiceCharge(double subtotalAmount, double discountAmount) {
    Outlet store = DataProvider.get().getOutlet();
    if (store == null) {
      return 0.0D;
    }
    double serviceChargePercentage = 0.0D;
    if (store.getServiceChargePercentage() != null) {
      serviceChargePercentage = store.getServiceChargePercentage().doubleValue();
    }
    
    double serviceCharge = 0.0D;
    
    if (serviceChargePercentage > 0.0D) {
      serviceCharge = (subtotalAmount - discountAmount) * (serviceChargePercentage / 100.0D);
    }
    
    return fixInvalidAmount(serviceCharge);
  }
  
  public void addProperty(String name, String value) {
    if (getProperties() == null) {
      setProperties(new HashMap());
    }
    
    getProperties().put(name, value);
  }
  
  public boolean hasProperty(String key) {
    return getProperty(key) != null;
  }
  
  public String getProperty(String key) {
    if (getProperties() == null) {
      return null;
    }
    
    return (String)getProperties().get(key);
  }
  
  public String getProperty(String key, String defaultValue) {
    if (getProperties() == null) {
      return null;
    }
    
    String string = (String)getProperties().get(key);
    if (StringUtils.isEmpty(string)) {
      return defaultValue;
    }
    
    return string;
  }
  
  public void removeProperty(String propertyName) {
    Map<String, String> properties = getProperties();
    if (properties == null) {
      return;
    }
    
    properties.remove(propertyName);
  }
  
  public boolean isPropertyValueTrue(String propertyName) {
    String property = getProperty(propertyName);
    
    return POSUtil.getBoolean(property);
  }
  
  public String toURLForm() {
    String s = "ticket_id=" + getId();
    
    List<TicketItem> items = getTicketItems();
    if ((items == null) || (items.size() == 0)) {
      return s;
    }
    
    for (int i = 0; i < items.size(); i++) {
      TicketItem ticketItem = (TicketItem)items.get(i);
      s = s + "&items[" + i + "][id]=" + ticketItem.getId();
      s = s + "&items[" + i + "][name]=" + POSUtil.encodeURLString(ticketItem.getName());
      s = s + "&items[" + i + "][price]=" + ticketItem.getSubtotalAmount();
    }
    
    s = s + "&tax=" + getTaxAmount();
    s = s + "&subtotal=" + getSubtotalAmount();
    s = s + "&grandtotal=" + getTotalAmountWithTips();
    
    return s;
  }
  
  public void setCustomer(Customer customer) {
    this.customer = customer;
    if (customer != null) {
      setCustomerId(customer.getId());
      addProperty("CUSTOMER_ID", String.valueOf(customer.getId()));
      addProperty("CUSTOMER_NAME", customer.getFirstName());
      addProperty("CUSTOMER_LAST_NAME", customer.getLastName());
      addProperty("CUSTOMER_MOBILE", customer.getMobileNo());
      addProperty("CUSTOMER_ZIP_CODE", customer.getZipCode());
      addProperty("customer.taxExempt", customer.isTaxExempt().toString());
    }
    else {
      removeCustomer();
    }
  }
  
  public Customer getCustomer() {
    if (customer != null) {
      return customer;
    }
    String customerId = getCustomerId();
    if (StringUtils.isEmpty(customerId)) {
      return null;
    }
    customer = DataProvider.get().getCustomer(customerId);
    return customer;
  }
  
  public void removeCustomer() {
    removeProperty("CUSTOMER_ID");
    removeProperty("CUSTOMER_NAME");
    removeProperty("CUSTOMER_MOBILE");
    removeProperty("CUSTOMER_ZIP_CODE");
    removeProperty("customer.taxExempt");
  }
  
  public String getSortOrder() {
    if (sortOrder == null) {
      return "";
    }
    return sortOrder;
  }
  
  public void setSortOrder(String sortOrder) {
    this.sortOrder = sortOrder;
  }
  
  public String getStatus() {
    if (super.getStatus() == null) {
      return "";
    }
    return super.getStatus();
  }
  
  public String getOrderStatusDisplay() {
    return ORDER_STATUS[getOrderStatus().intValue()];
  }
  

  public void setOrderStatusDisplay(String orderStatusDisplay) {}
  
  public void consolidateTicketItems()
  {
    List<TicketItem> ticketItems = getTicketItems();
    
    Map<String, List<TicketItem>> itemMap = new LinkedHashMap();
    
    for (Iterator iterator = ticketItems.iterator(); iterator.hasNext();) {
      newItem = (TicketItem)iterator.next();
      String menuItemId = newItem.getMenuItemId();
      if (!StringUtils.isEmpty(menuItemId))
      {

        List<TicketItem> itemListInMap = (List)itemMap.get(menuItemId);
        
        if (itemListInMap == null) {
          List<TicketItem> list = new ArrayList();
          list.add(newItem);
          
          itemMap.put(menuItemId.toString(), list);
        }
        else {
          boolean merged = false;
          for (TicketItem itemInMap : itemListInMap) {
            if (itemInMap.isMergable(newItem, false)) {
              itemInMap.merge(newItem);
              merged = true;
              break;
            }
          }
          
          if (!merged)
            itemListInMap.add(newItem);
        }
      }
    }
    TicketItem newItem;
    getTicketItems().clear();
    Collection<List<TicketItem>> values = itemMap.values();
    for (List<TicketItem> list : values) {
      if (list != null) {
        getTicketItems().addAll(list);
      }
    }
    List<TicketItem> ticketItemList = getTicketItems();
    if (getOrderType().isAllowSeatBasedOrder().booleanValue()) {
      Collections.sort(ticketItemList, new Comparator()
      {
        public int compare(TicketItem o1, TicketItem o2)
        {
          if ((o1.getId() == null) || (o2.getId() == null)) {
            return 0;
          }
          return o1.getId().compareTo(o2.getId());
        }
      });
      Collections.sort(ticketItemList, new Comparator()
      {
        public int compare(TicketItem o1, TicketItem o2)
        {
          return o1.getSeatNumber().intValue() - o2.getSeatNumber().intValue();
        }
      });
    }
    
    calculatePrice();
  }
  
  public void consolidateKitchenTicketItems() {
    boolean isFilterKitchenPrintedItems = true;
    consolidateKitchenTicketItems(isFilterKitchenPrintedItems);
  }
  
  public void consolidateKitchenTicketItems(boolean isFilterTicket) {
    consolidateKitchenTicketItems(isFilterTicket, null);
  }
  
  public void consolidateKitchenTicketItems(boolean isFilterTicket, List<TicketItem> ticketItems)
  {
    if (isSourceOnline()) {
      return;
    }
    if (ticketItems == null) {
      ticketItems = getTicketItems();
    }
    Map<String, List<TicketItem>> itemMap = new LinkedHashMap();
    
    for (Iterator iterator = ticketItems.iterator(); iterator.hasNext();) {
      newItem = (TicketItem)iterator.next();
      if (((newItem.isShouldPrintToKitchen().booleanValue()) || (newItem.isPrintKitchenSticker().booleanValue())) && (
      

        (!isFilterTicket) || (!newItem.isPrintedToKitchen().booleanValue())))
      {

        String menuItemId = newItem.getMenuItemId();
        if (StringUtils.isEmpty(menuItemId)) {
          menuItemId = newItem.getName();
        }
        List<TicketItem> itemListInMap = (List)itemMap.get(menuItemId);
        
        if (itemListInMap == null) {
          List<TicketItem> list = new ArrayList();
          list.add(newItem);
          
          itemMap.put(menuItemId.toString(), list);





        }
        else
        {




          itemListInMap.add(newItem);
        }
      }
    }
    TicketItem newItem;
    ticketItems.clear();
    Collection<List<TicketItem>> values = itemMap.values();
    for (List<TicketItem> list : values) {
      if (list != null) {
        ticketItems.addAll(list);
      }
    }
    List<TicketItem> ticketItemList = ticketItems;
    if (getOrderType().isAllowSeatBasedOrder().booleanValue()) {
      Collections.sort(ticketItemList, new Comparator()
      {
        public int compare(TicketItem o1, TicketItem o2)
        {
          if ((o1.getId() == null) || (o2.getId() == null))
            return 0;
          return o1.getId().compareTo(o2.getId());
        }
      });
      Collections.sort(ticketItemList, new Comparator()
      {
        public int compare(TicketItem o1, TicketItem o2)
        {
          return o1.getSeatNumber().intValue() - o2.getSeatNumber().intValue();
        }
      });
    }
    
    calculatePrice();
  }
  


  public void markPrintedToKitchen()
  {
    List<TicketItem> ticketItems = getTicketItems();
    markPrintedToKitchen(ticketItems);
  }
  
  public void markPrintedToKitchen(List<TicketItem> ticketItems) {
    for (TicketItem ticketItem : ticketItems)
      if ((!ticketItem.isPrintedToKitchen().booleanValue()) && (ticketItem.isShouldPrintToKitchen().booleanValue()))
      {


        List<Printer> printers = ticketItem.getPrinters(getOrderType());
        if (printers != null)
        {

          ticketItem.setPrintedToKitchen(Boolean.valueOf(true));
          List<TicketItemModifier> ticketItemModifiers = ticketItem.getTicketItemModifiers();
          Iterator localIterator2; if (ticketItemModifiers != null)
            for (localIterator2 = ticketItemModifiers.iterator(); localIterator2.hasNext();) { itemModifier = (TicketItemModifier)localIterator2.next();
              itemModifier.setPrintedToKitchen(Boolean.valueOf(true));
            }
          TicketItemModifier itemModifier;
          Object cookingInstructions = ticketItem.getCookingInstructions();
          if (cookingInstructions != null) {
            for (TicketItemCookingInstruction ticketItemCookingInstruction : (List)cookingInstructions) {
              ticketItemCookingInstruction.setPrintedToKitchen(Boolean.valueOf(true));
            }
            ticketItem.buildCoookingInstructions();
          }
        }
      }
  }
  
  public boolean isSourceOnline() { String property = getProperty("source");
    if ("online".equals(property)) {
      return true;
    }
    return false;
  }
  
  public boolean isReservation() {
    if (getTicketType() != null) {
      TicketType ticketType = getTicketType();
      if (ticketType.equals(TicketType.RESERVATION)) {
        return true;
      }
    }
    return false;
  }
  
  public Ticket clone(Ticket source) {
    return (Ticket)SerializationUtils.clone(source);
  }
  
  public boolean isNeedSpecialAttention() {
    return needSpecialAttention;
  }
  
  public void setNeedSpecialAttention(boolean needSpecialAttention) {
    this.needSpecialAttention = needSpecialAttention;
  }
  
  public String getNumberToDisplay() {
    return "TOKEN# " + getTokenNo();
  }
  
  public Outlet getOutlet() {
    if (outlet != null) {
      return outlet;
    }
    String outletId = getOutletId();
    if (outletId == null)
      return null;
    outlet = ((Outlet)DataProvider.get().getObjectOf(Outlet.class, outletId));
    return outlet;
  }
  
  public void setOutlet(Outlet outlet) {
    this.outlet = outlet;
    if (outlet == null) {
      setOutletId(null);
    }
    else {
      setOutletId(outlet.getId());
    }
  }
  
  public Department getDepartment() {
    String departmentId = getDepartmentId();
    if (departmentId == null)
      return null;
    if (department == null)
      department = ((Department)DataProvider.get().getObjectOf(Department.class, getOutletId()));
    return department;
  }
  
  public void setDepartment(Department department) {
    this.department = department;
    if (department == null) {
      setDepartmentId(null);
    }
    else {
      setDepartmentId(department.getId());
    }
  }
  
  public SalesArea getSalesArea() {
    if (salesArea != null) {
      return salesArea;
    }
    String salesAreaId = getSalesAreaId();
    if (salesAreaId == null)
      return null;
    salesArea = DataProvider.get().getSalesArea(salesAreaId);
    return salesArea;
  }
  
  public void setSalesArea(SalesArea salesArea) {
    this.salesArea = salesArea;
    if (salesArea == null) {
      setSalesAreaId(null);
    }
    else {
      setSalesAreaId(salesArea.getId());
    }
  }
  
  public boolean isSplited() {
    return Boolean.valueOf(getProperty("split")).booleanValue();
  }
  
  public void updateTicketItemPriceByOrderType(String orderTypeOption) {
    List<TicketItem> ticketItems = getTicketItems();
    OrderType orderType = getOrderType();
    TaxGroup taxGroup = null;
    
    if (ticketItems == null) {
      return;
    }
    if (orderTypeOption == "FOR HERE") {
      taxGroup = orderType.getForHereTaxGroup();
      addProperty("ticket.ForHereToGo", "FOR HERE");
    }
    else if (orderTypeOption == "TO GO") {
      taxGroup = orderType.getToGoTaxGroup();
      addProperty("ticket.ForHereToGo", "TO GO");
    }
    
    for (TicketItem ticketItem : ticketItems) {
      MenuItem.setItemTaxes(ticketItem, taxGroup, getOrderType());
    }
    calculatePrice();
  }
  
  public String getSplitId() {
    return getProperty("split_ticket_id");
  }
  
  public void setSplitOrder(int order) {
    addProperty("SPLIT_ORDER", String.valueOf(order));
  }
  
  public String getDiffWithCrntTime() {
    return DateUtil.getElapsedTime(getCreateDate(), new Date());
  }
  
  public void setDiffWithCrntTime(String diffWithCrntTime) {}
  
  public String getTableNames()
  {
    List<Integer> tableNumbers = getTableNumbers();
    if ((tableNumbers == null) || (tableNumbers.size() <= 0)) {
      return "";
    }
    List<String> tableNames = ShopTableDAO.getInstance().getTableNames(tableNumbers);
    if ((tableNames != null) && (!tableNames.isEmpty())) {
      return tableNames.toString().replaceAll("[\\[\\]]", "");
    }
    
    String tabNum = "";
    int count; if ((tableNumbers != null) && (tableNumbers.size() > 0)) {
      count = 0;
      for (Integer tableNumber : tableNumbers) {
        tabNum = tabNum + tableNumber;
        if (count < tableNumbers.size() - 1) {
          tabNum = tabNum + ", ";
        }
        count++;
      }
    }
    return tabNum;
  }
  
  public Double getTotalAmountWithTips()
  {
    return Double.valueOf(super.getTotalAmount().doubleValue() + getGratuityAmount());
  }
  
  public void calculateRefundAmount() {
    double refundAmount = 0.0D;
    double voidAmount = 0.0D;
    Set<PosTransaction> transactions = getTransactions();
    if (transactions != null) {
      for (PosTransaction t : transactions) {
        if ((t instanceof RefundTransaction))
          refundAmount += t.getAmount().doubleValue();
        if (t.isVoided().booleanValue())
          voidAmount += t.getAmount().doubleValue();
      }
    }
    setRefundAmount(Double.valueOf(refundAmount + voidAmount));
  }
  
  public String getTableNumbersDisplay() {
    List<Integer> numbers = getTableNumbers();
    if ((numbers == null) || (numbers.size() <= 0)) {
      return "";
    }
    String tableNumbers = numbers.toString().replaceAll("[\\[\\]]", "");
    return tableNumbers;
  }
  
  public boolean isPrintedToKitchenOrInventoryAdjusted() {
    for (TicketItem ticketItem : getTicketItems()) {
      if ((ticketItem.isPrintedToKitchen().booleanValue()) || (ticketItem.isInventoryAdjusted()))
        return true;
      if (ticketItem.isHasModifiers().booleanValue()) {
        for (TicketItemModifier modifier : ticketItem.getTicketItemModifiers()) {
          if (modifier.isPrintedToKitchen().booleanValue())
            return true;
        }
      }
    }
    return false;
  }
  

  public Boolean hasRefundableItem()
  {
    return Boolean.valueOf(refundableItem == null ? false : refundableItem.booleanValue());
  }
  
  public void setRefundableItem(boolean refundableItem) {
    this.refundableItem = Boolean.valueOf(refundableItem);
  }
  
  public double getTicketDiscountAmount() {
    return ticketDiscountAmount;
  }
  
  public double getItemDiscountAmount() {
    return itemDiscountAmount;
  }
  
  public double getVoidSubtotal() {
    return voidSubtotal;
  }
  
  public double getVoidTotal() {
    return voidTotal;
  }
  
  public void setTotalAmount(Double totalAmount)
  {
    super.setTotalAmount(Double.valueOf(totalAmount.doubleValue() + 0.0D));
  }
  
  public void setDueAmount(Double dueAmount)
  {
    super.setDueAmount(Double.valueOf(dueAmount.doubleValue() + 0.0D));
  }
  
  public void setDiscountAmount(Double discountAmount)
  {
    super.setDiscountAmount(Double.valueOf(discountAmount.doubleValue() + 0.0D));
  }
  
  public void setTaxAmount(Double taxAmount)
  {
    super.setTaxAmount(Double.valueOf(taxAmount.doubleValue() + 0.0D));
  }
  
  public OrderType getOrderType() {
    return DataProvider.get().getOrderTypeById(getOrderTypeId());
  }
  
  public void setOrderType(OrderType orderType) {
    String orderTypeId = null;
    if (orderType != null) {
      orderTypeId = orderType.getId();
    }
    super.setOrderTypeId(orderTypeId);
  }
  
  public Shift getShift() {
    return DataProvider.get().getShiftById(getShiftId());
  }
  
  public void setShift(Shift shift) {
    String shiftId = null;
    if (shift != null) {
      shiftId = shift.getId();
    }
    super.setShiftId(shiftId);
  }
  
  public User getOwner() {
    return DataProvider.get().getUserById(getOwnerId());
  }
  
  public void setOwner(User owner) {
    String ownerId = null;
    String ownerTypeId = null;
    if (owner != null) {
      ownerId = owner.getId();
      ownerTypeId = owner.getUserTypeId();
    }
    super.setOwnerId(ownerId);
    super.setOwnerTypeId(ownerTypeId);
  }
  
  public User getCashier() {
    return DataProvider.get().getUserById(getCashierId());
  }
  
  public void setCashier(User cashier) {
    String cashierId = null;
    if (cashier != null) {
      cashierId = cashier.getId();
    }
    super.setCashierId(cashierId);
  }
  
  public User getAssignedDriver() {
    return DataProvider.get().getUserById(getAssignedDriverId());
  }
  
  public void setAssignedDriver(User assignedDriver) {
    String assignedDriverId = null;
    if (assignedDriver != null) {
      assignedDriverId = assignedDriver.getId();
    }
    super.setAssignedDriverId(assignedDriverId);
  }
  
  public User getVoidedBy() {
    return DataProvider.get().getUserById(getVoidedById());
  }
  
  public void setVoidedBy(User voidBy) {
    String voidById = null;
    if (voidBy != null) {
      voidById = voidBy.getId();
    }
    super.setVoidedById(voidById);
  }
  
  public Terminal getTerminal() {
    return DataProvider.get().getTerminalById(getTerminalId());
  }
  
  public void setTerminal(Terminal terminal) {
    Integer terminalId = null;
    if (terminal != null) {
      terminalId = terminal.getId();
    }
    super.setTerminalId(terminalId);
  }
  


  public void setDiscounts(List<TicketDiscount> discounts)
  {
    this.discounts = discounts;
    if (this.discounts == null) {
      this.discounts = new ArrayList();
    }
  }
  
  public List<TicketDiscount> getDiscounts() {
    if (discounts == null) {
      discounts = new ArrayList();
      
      String property = super.getDiscountsProperty();
      if (StringUtils.isNotEmpty(property)) {
        JsonReader jsonParser = Json.createReader(new StringReader(property));
        JsonArray jsonArray = jsonParser.readArray();
        jsonParser.close();
        for (int i = 0; i < jsonArray.size(); i++) {
          JsonObject dObj = (JsonObject)jsonArray.get(i);
          TicketDiscount td = new TicketDiscount();
          td.setTicket(this);
          td.setDiscountId(dObj.getString(TicketDiscount.PROP_DISCOUNT_ID));
          td.setName(dObj.getString(TicketDiscount.PROP_NAME));
          td.setType(Integer.valueOf(dObj.getInt(TicketDiscount.PROP_TYPE)));
          td.setAutoApply(Boolean.valueOf(dObj.getBoolean(TicketDiscount.PROP_AUTO_APPLY)));
          td.setCouponQuantity(Double.valueOf(POSUtil.parseDouble("" + dObj.get(TicketDiscount.PROP_COUPON_QUANTITY))));
          td.setMinimumAmount(Double.valueOf(POSUtil.parseDouble("" + dObj.get(TicketDiscount.PROP_MINIMUM_AMOUNT))));
          td.setValue(Double.valueOf(POSUtil.parseDouble("" + dObj.get(TicketDiscount.PROP_VALUE))));
          td.setTotalDiscountAmount(Double.valueOf(POSUtil.parseDouble("" + dObj.get(TicketDiscount.PROP_TOTAL_DISCOUNT_AMOUNT))));
          discounts.add(td);
        }
      }
    }
    return discounts;
  }
  
  public void addTodiscounts(TicketDiscount ticketDiscount) {
    discounts = getDiscounts();
    discounts.add(ticketDiscount);
    buildDiscounts();
  }
  
  public void buildDiscounts() {
    JSONArray jsonArray = new JSONArray();
    for (TicketDiscount ticketDiscount : discounts) {
      jsonArray.put(ticketDiscount.toJson());
    }
    setDiscountsProperty(jsonArray.toString());
  }
  
  public Map<String, String> getProperties()
  {
    Map<String, String> properties = super.getProperties();
    if (properties == null) {
      properties = new HashMap();
      super.setProperties(properties);
    }
    return properties;
  }
  
  public String getTicketTableNumbers()
  {
    String ticketTableNumbers = "";
    List<Integer> tableNumbers = getTableNumbers();
    Iterator iterator; if ((tableNumbers != null) && (tableNumbers.size() > 0)) {
      for (iterator = tableNumbers.iterator(); iterator.hasNext();) {
        Integer tableNo = (Integer)iterator.next();
        ticketTableNumbers = ticketTableNumbers + tableNo;
        if (iterator.hasNext())
          ticketTableNumbers = ticketTableNumbers + ",";
      }
    }
    return ticketTableNumbers;
  }
  
  public void setTicketTableNumbers(String ticketTableNumbers)
  {
    tableNumbers = new ArrayList();
    if (StringUtils.isNotEmpty(ticketTableNumbers)) {
      String[] tableNums = ticketTableNumbers.split(",");
      for (String tbl : tableNums) {
        int tableNo = POSUtil.parseInteger(tbl);
        if (tableNo > 0)
          tableNumbers.add(Integer.valueOf(tableNo));
      }
    }
    super.setTicketTableNumbers(ticketTableNumbers);
  }
  
  public List<Integer> getTableNumbers() {
    return tableNumbers;
  }
  
  public void setTableNumbers(List<Integer> tableNumbers) {
    this.tableNumbers = tableNumbers;
  }
  
  public List<ShopTable> getTables() {
    List<Integer> tableNumbers = getTableNumbers();
    if ((tableNumbers == null) || (tableNumbers.size() == 0)) {
      return new ArrayList(1);
    }
    if (tables == null) {
      tables = new ArrayList(1);
    }
    for (Integer tableId : tableNumbers) {
      ShopTable table = new ShopTable(tableId);
      if (!tables.contains(table)) {
        table = ShopTableDAO.getInstance().loadWithSeats(tableId);
        if (table != null) {
          tables.add(table);
        }
      }
    }
    return tables;
  }
  
  public void voidItem(TicketItem ticketItem, String voidReason, boolean itemWasted) {
    voidItem(ticketItem, voidReason, itemWasted, ticketItem.getQuantity().doubleValue());
  }
  
  public void voidItem(TicketItem ticketItem, String voidReason, boolean itemWasted, double quantity) {
    if (StringUtils.isEmpty(voidReason)) {
      throw new PosException("Void reason cannot be empty.");
    }
    if (quantity == 0.0D) {
      throw new PosException("Invalid quantity.");
    }
    if (quantity > Math.abs(ticketItem.getQuantity().doubleValue())) {
      throw new PosException("Void quantity cannot be greater than item quantity.");
    }
    TicketItem voidTicketItem = ticketItem.cloneAsNew();
    voidTicketItem.setId(null);
    voidTicketItem.setQuantity(Double.valueOf(-quantity));
    voidTicketItem.setPrintedToKitchen(Boolean.valueOf(false));
    voidTicketItem.setVoided(Boolean.valueOf(true));
    voidTicketItem.setVoidedItemId(ticketItem.getId());
    if (itemWasted) {
      voidTicketItem.setInventoryAdjustQty(Double.valueOf(-quantity));
    }
    else {
      voidTicketItem.setInventoryAdjustQty(Double.valueOf(0.0D));
    }
    
    if (voidTicketItem.isHasModifiers().booleanValue()) {
      List<TicketItemModifier> ticketItemModifiers = voidTicketItem.getTicketItemModifiers();
      if (ticketItemModifiers != null) {
        for (TicketItemModifier modifier : ticketItemModifiers) {
          if (!modifier.isInfoOnly().booleanValue())
          {

            modifier.setItemQuantity(Double.valueOf(-modifier.getItemQuantity().doubleValue())); }
        }
      }
    }
    VoidItem voidItem = ticketItem.createVoidItem(voidReason, itemWasted, quantity);
    if ((ticketItem.isComboItem().booleanValue()) && 
      (voidTicketItem.getComboItems() != null)) {
      for (TicketItem comboTicketItem : voidTicketItem.getComboItems()) {
        double ticketItemQuantity = ticketItem.getQuantity().doubleValue();
        double adjustQuantity = quantity - ticketItemQuantity;
        double toBeVoidComboQuantity = comboTicketItem.getQuantity().doubleValue() + adjustQuantity * comboTicketItem.getQuantity().doubleValue() / ticketItemQuantity;
        comboTicketItem.setQuantity(Double.valueOf(-toBeVoidComboQuantity));
        if (itemWasted) {
          voidTicketItem.setInventoryAdjustQty(Double.valueOf(-toBeVoidComboQuantity));
        }
        else {
          voidTicketItem.setInventoryAdjustQty(Double.valueOf(0.0D));
        }
        VoidItem comboVoidItem = comboTicketItem.createVoidItem(voidReason, itemWasted, toBeVoidComboQuantity);
        comboTicketItem.setVoided(Boolean.valueOf(true));
        comboTicketItem.setVoidItem(comboVoidItem);
      }
    }
    
    voidTicketItem.setVoidItem(voidItem);
    addToticketItems(voidTicketItem);
  }
  
  public boolean isPriceIncludesTax() {
    return super.isTaxIncluded().booleanValue();
  }
  
  public void setPriceIncludesTax(boolean priceIncludesTax) {
    super.setTaxIncluded(Boolean.valueOf(priceIncludesTax));
  }
}
