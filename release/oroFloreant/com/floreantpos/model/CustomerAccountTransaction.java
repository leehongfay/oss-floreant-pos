package com.floreantpos.model;

import com.floreantpos.model.base.BaseCustomerAccountTransaction;





public class CustomerAccountTransaction
  extends BaseCustomerAccountTransaction
{
  private static final long serialVersionUID = 1L;
  
  public CustomerAccountTransaction() {}
  
  public CustomerAccountTransaction(String id)
  {
    super(id);
  }
  






  public CustomerAccountTransaction(String id, String transactionType, String paymentType)
  {
    super(id, transactionType, paymentType);
  }
}
