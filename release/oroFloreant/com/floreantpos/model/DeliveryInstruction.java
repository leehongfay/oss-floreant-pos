package com.floreantpos.model;

import com.floreantpos.model.base.BaseDeliveryInstruction;





public class DeliveryInstruction
  extends BaseDeliveryInstruction
{
  private static final long serialVersionUID = 1L;
  
  public DeliveryInstruction() {}
  
  public DeliveryInstruction(String id)
  {
    super(id);
  }
  


  public String toString()
  {
    return super.getNotes();
  }
}
