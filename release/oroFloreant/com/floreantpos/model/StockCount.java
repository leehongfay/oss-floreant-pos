package com.floreantpos.model;

import com.floreantpos.model.base.BaseStockCount;





public class StockCount
  extends BaseStockCount
{
  private static final long serialVersionUID = 1L;
  private String checkVerification;
  
  public StockCount() {}
  
  public StockCount(String id)
  {
    super(id);
  }
  

  public String getCheckVerification()
  {
    if (getVerifiedBy() == null) {
      return "Verify";
    }
    return "";
  }
  
  public void setCheckVerification(String checkVerification) {
    this.checkVerification = checkVerification;
  }
}
