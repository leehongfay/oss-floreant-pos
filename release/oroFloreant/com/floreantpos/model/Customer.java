package com.floreantpos.model;

import com.floreantpos.model.base.BaseCustomer;
import com.floreantpos.model.dao.CustomerGroupDAO;
import javax.swing.ImageIcon;
import org.apache.commons.lang.StringUtils;
























public class Customer
  extends BaseCustomer
{
  private static final long serialVersionUID = 1L;
  public static final String APPETIZER_PREF = "appetizerPref";
  public static final String BILLINGSTATEMENT_PREF = "billingStatementPref";
  public static final String COCKTAIL_PREF = "cocktailPref";
  public static final String DESSERT_PREF = "dessertPref";
  public static final String ENTREE_PREF = "entreePref";
  public static final String GOLFBALL_PREF = "golfBallPref";
  public static final String MEAL_PREF = "mealPref";
  public static final String PAYMENT_PREF = "paymentPref";
  public static final String PILLOW_PREF = "pillowPref";
  public static final String ROLLAWAYCRIB_PREF = "rollawayCribPref";
  public static final String SEATING_PREF = "seatingPref";
  public static final String SMOKING_PREF = "smokingPref";
  public static final String WINE_PREF = "winePref";
  public static final String ROOM_ACCESS_PREF = "roomAccessPref";
  public static final String ROOM_LOC_PREF = "roomLocPref";
  public static final String ROOM_NUMBER_PREF = "roomNumberPref";
  public static final String REMAINING_CURRENT_QUARTER = "club62__Remaining_Current_Quarter_F_B_Spends__c";
  public static final String STATUS = "club62__Status__c";
  public static final String STREET_1 = "street_1";
  public static final String STREET_2 = "street_2";
  public static final String STREET_3 = "street_3";
  private Boolean selected;
  
  public Customer() {}
  
  public Customer(String id)
  {
    super(id);
  }
  


  public String toString()
  {
    String fName = getName();
    return fName;
  }
  
  public static enum MemberType
  {
    MEMBER(0, "Member"), 
    GUEST(1, "Guest"), 
    EMPLOYEE(2, "Employee");
    

    private int typeInt;
    private String name;
    
    private MemberType(int type, String name)
    {
      typeInt = type;
      this.name = name;
    }
    
    public int getTypeInt() {
      return typeInt;
    }
    
    public String getName() {
      return name;
    }
    
    public String toString()
    {
      return name;
    }
    
    public static MemberType fromInt(int type) {
      MemberType[] values = values();
      for (MemberType memberType : values) {
        if (memberType.getTypeInt() == type) {
          return memberType;
        }
      }
      
      return MEMBER;
    }
  }
  
  public ImageIcon getImage() {
    return null;
  }
  
  public String getName() {
    String name = super.getFirstName();
    if (StringUtils.isNotEmpty(super.getLastName())) {
      name = name + " " + super.getLastName();
    }
    return name;
  }
  
  public void setMobileNo(String mobileNo)
  {
    if (StringUtils.isNotEmpty(mobileNo)) {
      mobileNo = mobileNo.replaceAll("(\\+)?\\D", "$1");
    }
    super.setMobileNo(mobileNo);
  }
  
  public void setWorkPhoneNo(String workPhoneNo)
  {
    if (StringUtils.isNotEmpty(workPhoneNo)) {
      workPhoneNo = workPhoneNo.replaceAll("(\\+)?\\D", "$1");
    }
    super.setWorkPhoneNo(workPhoneNo);
  }
  
  public void setHomePhoneNo(String homePhoneNo)
  {
    if (StringUtils.isNotEmpty(homePhoneNo)) {
      homePhoneNo = homePhoneNo.replaceAll("(\\+)?\\D", "$1");
    }
    super.setHomePhoneNo(homePhoneNo);
  }
  
  public Boolean isSelected() {
    return selected == null ? Boolean.FALSE : selected;
  }
  
  public void setSelected(Boolean enable) {
    selected = enable;
  }
  
  public CustomerGroup getCustomerGroup() {
    if (StringUtils.isNotEmpty(getCustomerGroupId())) {
      return CustomerGroupDAO.getInstance().get(getCustomerGroupId());
    }
    return null;
  }
  
  public void setCustomerGroup(CustomerGroup customerGroup) {
    String customerGroupId = null;
    if (customerGroup != null) {
      customerGroupId = customerGroup.getId();
    }
    super.setCustomerGroupId(customerGroupId);
  }
}
