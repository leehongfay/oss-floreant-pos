package com.floreantpos.customPayment;

import com.floreantpos.Messages;
import com.floreantpos.actions.PosAction;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.ui.dialog.POSMessageDialog;
import javax.swing.JTabbedPane;

public class CustomPaymentBrowserAction extends PosAction
{
  public CustomPaymentBrowserAction()
  {
    super(Messages.getString("CustomPaymentBrowserAction.0"));
  }
  
  public void execute()
  {
    BackOfficeWindow backOfficeWindow = com.floreantpos.util.POSUtil.getBackOfficeWindow();
    try {
      CustomPaymentBrowser explorer = null;
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab(Messages.getString("CustomPaymentBrowserAction.1"));
      if (index == -1) {
        explorer = new CustomPaymentBrowser();
        tabbedPane.addTab(Messages.getString("CustomPaymentBrowserAction.2"), explorer);
      }
      else {
        explorer = (CustomPaymentBrowser)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(explorer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
