package com.floreantpos.customPayment;

import com.floreantpos.Messages;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.CustomPayment;
import com.floreantpos.model.dao.CustomPaymentDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import net.miginfocom.swing.MigLayout;
import org.hibernate.StaleObjectStateException;






public class CustomPaymentForm
  extends BeanEditor<CustomPayment>
{
  private JLabel lblName;
  private JLabel lblRefNumberFieldName;
  private FixedLengthTextField txtName;
  private FixedLengthTextField txtRefNumberFieldName;
  private JCheckBox cbRequiredRefNumber;
  private JCheckBox cbEnable;
  
  public CustomPaymentForm() { initComponent(); }
  
  public CustomPaymentForm(CustomPayment customPayment) {
    initComponent();
    setBean(customPayment);
  }
  
  private void initComponent() {
    cbRequiredRefNumber = new JCheckBox(Messages.getString("CustomPaymentForm.0"));
    lblRefNumberFieldName = new JLabel(Messages.getString("CustomPaymentForm.1"));
    lblName = new JLabel(Messages.getString("CustomPaymentForm.2"));
    txtName = new FixedLengthTextField(60);
    txtRefNumberFieldName = new FixedLengthTextField(60);
    cbEnable = new JCheckBox("Enable");
    
    setLayout(new MigLayout("fillx, hidemode 3", "[][]", ""));
    
    add(lblName, "");
    add(txtName, "growx, wrap");
    add(lblRefNumberFieldName, "");
    add(txtRefNumberFieldName, "growx, wrap");
    add(cbRequiredRefNumber, "skip 1, wrap");
    add(cbEnable, "skip 1, wrap");
    
    cbRequiredRefNumber.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        if (cbRequiredRefNumber.isSelected()) {
          lblRefNumberFieldName.setVisible(true);
          txtRefNumberFieldName.setVisible(true);
        }
        else {
          lblRefNumberFieldName.setVisible(false);
          txtRefNumberFieldName.setVisible(false);
        }
        
      }
    });
    lblRefNumberFieldName.setVisible(false);
    txtRefNumberFieldName.setVisible(false);
    txtName.setEnabled(false);
    cbRequiredRefNumber.setEnabled(false);
  }
  
  public void setFieldsEnable(boolean enable)
  {
    txtName.setEnabled(enable);
    cbRequiredRefNumber.setEnabled(enable);
    txtRefNumberFieldName.setEnabled(enable);
  }
  
  public void createNew()
  {
    setBean(new CustomPayment());
    lblRefNumberFieldName.setVisible(false);
    txtRefNumberFieldName.setVisible(false);
    cbRequiredRefNumber.setSelected(false);
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      CustomPayment payment = (CustomPayment)getBean();
      CustomPaymentDAO.getInstance().saveOrUpdate(payment);
      
      updateView();
      
      return true;
    }
    catch (IllegalModelStateException localIllegalModelStateException) {}catch (StaleObjectStateException e) {
      BOMessageDialog.showError(this, Messages.getString("CustomPaymentForm.10"));
    }
    return false;
  }
  

  protected void updateView()
  {
    CustomPayment payment = (CustomPayment)getBean();
    
    txtName.setText(payment.getName());
    cbEnable.setSelected(payment.isEnable().booleanValue());
    
    if (payment.isRequiredRefNumber().booleanValue()) {
      txtRefNumberFieldName.setVisible(true);
      lblRefNumberFieldName.setVisible(true);
      txtRefNumberFieldName.setText(payment.getRefNumberFieldName());
      cbRequiredRefNumber.setSelected(payment.isRequiredRefNumber().booleanValue());
    }
    else {
      lblRefNumberFieldName.setVisible(false);
      txtRefNumberFieldName.setVisible(false);
      cbRequiredRefNumber.setSelected(payment.isRequiredRefNumber().booleanValue());
    }
  }
  
  protected boolean updateModel()
    throws IllegalModelStateException
  {
    CustomPayment payment = (CustomPayment)getBean();
    
    if (txtName.getText().equals("")) {
      POSMessageDialog.showMessage(null, Messages.getString("CustomPaymentForm.12"));
      return false;
    }
    
    payment.setName(txtName.getText());
    payment.setEnable(Boolean.valueOf(cbEnable.isSelected()));
    
    if (cbRequiredRefNumber.isSelected()) {
      if (txtRefNumberFieldName.getText().equals("")) {
        POSMessageDialog.showMessage(null, Messages.getString("CustomPaymentForm.14"));
        return false;
      }
      payment.setRefNumberFieldName(txtRefNumberFieldName.getText());
      payment.setRequiredRefNumber(Boolean.valueOf(true));
    }
    else {
      payment.setRefNumberFieldName("");
      payment.setRequiredRefNumber(Boolean.valueOf(false));
    }
    return true;
  }
  
  public boolean delete()
  {
    try {
      CustomPayment payment = (CustomPayment)getBean();
      if (payment == null) {
        return false;
      }
      CustomPaymentDAO.getInstance().delete(payment);
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e);
    }
    return false;
  }
  
  public String getDisplayText()
  {
    CustomPayment customPayment = (CustomPayment)getBean();
    if (customPayment.getId() == null) {
      return "Add Custom Payment";
    }
    
    return "Edit Custom Payment";
  }
}
