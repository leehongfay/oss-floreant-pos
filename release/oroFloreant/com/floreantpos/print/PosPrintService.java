package com.floreantpos.print;

import com.floreantpos.Messages;
import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.CashBreakdown;
import com.floreantpos.model.CashDrawer;
import com.floreantpos.model.Currency;
import com.floreantpos.model.DrawerType;
import com.floreantpos.model.PosPrinters;
import com.floreantpos.model.Store;
import com.floreantpos.model.StoreSession;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.TipsCashoutReport;
import com.floreantpos.model.TipsCashoutReportTableModel;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.CashDrawerDAO;
import com.floreantpos.model.dao.StoreDAO;
import com.floreantpos.model.dao.VoidItemDAO;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.report.ReportUtil;
import com.floreantpos.services.report.CashDrawerReportService;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.PrintServiceUtil;
import java.awt.print.PrinterAbortException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;




















public class PosPrintService
{
  private static final SimpleDateFormat date_formatter = new SimpleDateFormat("dd MMM, yyyy hh:mm aaa");
  private static Log logger = LogFactory.getLog(PosPrintService.class);
  
  public PosPrintService() {}
  
  public static void printDrawerPullReport(CashDrawer cashDrawer) { try { JasperPrint jasperPrint = populateDrawerPullReportParameters(cashDrawer);
      
      JRPrintServiceExporter exporter = new JRPrintServiceExporter();
      exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
      exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE, 
        PrintServiceUtil.getPrintServiceForPrinter(jasperPrint.getProperty("printerName")));
      exporter.exportReport();
    }
    catch (PrinterAbortException localPrinterAbortException) {}catch (Exception e) {
      PosLog.error(PosPrintService.class, e);
    }
  }
  
  public static void printCashDrawerSummaryReport(CashDrawer cashDrawer) {
    try {
      JasperPrint jasperPrint = populateCashDrawerSummaryReportParameters(cashDrawer);
      
      JRPrintServiceExporter exporter = new JRPrintServiceExporter();
      exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
      exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE, 
        PrintServiceUtil.getPrintServiceForPrinter(jasperPrint.getProperty("printerName")));
      exporter.exportReport();
    }
    catch (PrinterAbortException localPrinterAbortException) {}catch (Exception e) {
      PosLog.error(PosPrintService.class, e);
    }
  }
  
  public static void printServerTipsReport(TipsCashoutReport report) {
    try {
      JasperPrint jasperPrint = createServerTipsReport(report);
      jasperPrint.setName("SERVER_TIPS_" + report.getServer());
      jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
      ReceiptPrintService.printQuitely(jasperPrint);
    }
    catch (Exception e) {
      PosLog.error(PosPrintService.class, e);
      logger.error("error print tips report", e);
    }
  }
  
  public static JasperPrint createServerTipsReport(TipsCashoutReport report) throws Exception {
    try {
      HashMap parameters = new HashMap();
      parameters.put("server", report.getServer());
      if (report.getFromDate() != null)
        parameters.put("fromDate", Application.formatDate(report.getFromDate()));
      if (report.getToDate() != null)
        parameters.put("toDate", Application.formatDate(report.getToDate()));
      parameters.put("reportDate", new SimpleDateFormat("dd MMM yyyy, hh:mm aaa").format(report.getReportTime()));
      parameters.put("transactionCount", "" + report.getDatas().size());
      parameters.put("cashTips", NumberUtil.formatNumber(Double.valueOf(report.getCashTipsAmount())));
      parameters.put("chargedTips", NumberUtil.formatNumber(Double.valueOf(report.getChargedTipsAmount())));
      parameters.put("declaredTips", Double.valueOf(report.getDeclaredTipsAmount()));
      parameters.put("tipsDue", Double.valueOf(report.getTipsDue()));
      
      Store store = StoreDAO.getRestaurant();
      
      parameters.put("headerLine1", store.getName());
      
      JasperReport mainReport = ReportUtil.getReport("ServerTipsReport");
      JRDataSource dataSource = new JRTableModelDataSource(new TipsCashoutReportTableModel(report.getDatas(), new String[] { "ticketId", "saleType", "ticketTotal", "tips", "chargedTips", "tipsPaid" }));
      
      return JasperFillManager.fillReport(mainReport, parameters, dataSource);
    } catch (Exception e) {
      throw e;
    }
  }
  
  public static JasperPrint populateDrawerPullReportParameters(CashDrawer cashDrawer) throws Exception
  {
    boolean staffBankReport = cashDrawer.getDrawerType() == DrawerType.STAFF_BANK;
    
    Terminal terminal = cashDrawer.getTerminal();
    HashMap parameters = new HashMap();
    Store store = StoreDAO.getRestaurant();
    
    parameters.put("headerLine1", store.getName());
    parameters.put("reportTitle", staffBankReport ? "Staff bank report" : "Cash drawer report");
    
    parameters.put("IS_IGNORE_PAGINATION", Boolean.valueOf(true));
    User assignedUser = cashDrawer.getAssignedUser();
    if (assignedUser != null) {
      String userInfo = assignedUser.getId() + " - " + assignedUser.getFullName();
      parameters.put("user", (staffBankReport ? "Staff: " : new StringBuilder().append(Messages.getString("PosPrintService.4")).append(" ").toString()) + userInfo);
    }
    if (cashDrawer.getStartTime() != null)
      parameters.put("startTime", "Started: " + date_formatter.format(cashDrawer.getStartTime()));
    parameters.put("date", "Report Time: " + date_formatter.format(new Date()));
    parameters.put("totalVoid", cashDrawer.getTotalVoid());
    
    parameters.put("declaredTips", cashDrawer.getDeclaredTips());
    
    JasperReport subReportCurrencyBalance = ReportUtil.getReport("drawer-currency-balance");
    JasperReport subReport = ReportUtil.getReport("drawer-pull-void-veport");
    
    parameters.put("currencyBalanceReport", subReportCurrencyBalance);
    parameters.put("subreportParameter", subReport);
    
    JasperReport mainReport = ReportUtil.getReport("drawer-pull-report");
    
    JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(Arrays.asList(new CashDrawer[] { cashDrawer }));
    JasperPrint jasperPrint = JasperFillManager.fillReport(mainReport, parameters, dataSource);
    
    PosPrinters printers = Application.getPrinters();
    if (printers != null)
      jasperPrint.setProperty("printerName", printers.getReceiptPrinter());
    jasperPrint.setName("DrawerPullReport" + cashDrawer.getId());
    return jasperPrint;
  }
  
  public static JasperPrint populateCashDrawerSummaryReportParameters(CashDrawer cashDrawer) throws Exception {
    HashMap parameters = new HashMap();
    Store store = StoreDAO.getRestaurant();
    
    parameters.put("headerLine1", store.getName());
    parameters.put("reportTitle", "End of day sales report");
    parameters.put("IS_IGNORE_PAGINATION", Boolean.valueOf(true));
    parameters.put("startTime", "Opening time: " + date_formatter.format(cashDrawer.getStartTime()));
    parameters.put("user", "Opened by: " + cashDrawer.getAssignedBy());
    if (cashDrawer.getReportTime() != null) {
      parameters.put("date", "Closing time: " + date_formatter.format(cashDrawer.getReportTime()));
      parameters.put("reportUser", "Closed by: " + cashDrawer.getClosedBy());
    }
    
    parameters.put("totalVoid", cashDrawer.getTotalVoid());
    parameters.put("declaredTips", cashDrawer.getDeclaredTips());
    
    JasperReport subReportCurrencyBalance = ReportUtil.getReport("drawer-currency-balance");
    JasperReport subReport = ReportUtil.getReport("drawer-pull-void-veport");
    
    parameters.put("currencyBalanceReport", subReportCurrencyBalance);
    parameters.put("subreportParameter", subReport);
    
    JasperReport mainReport = ReportUtil.getReport("drawer-pull-report");
    
    JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(Arrays.asList(new CashDrawer[] { cashDrawer }));
    JasperPrint jasperPrint = JasperFillManager.fillReport(mainReport, parameters, dataSource);
    
    jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
    jasperPrint.setName("DrawerPullReport" + cashDrawer.getId());
    return jasperPrint;
  }
  
  public static CashDrawer populateCashDrawerReportSummary(List<CashDrawer> reportList) {
    CashDrawer cashDrawersReportSummary = new CashDrawer();
    Map<String, CashBreakdown> summaryCashBreakdownMap = new HashMap();
    List<String> cashDrawerIds = new ArrayList();
    if (reportList != null) {
      for (CashDrawer report : reportList) {
        if (report.getReportTime() == null) {
          try {
            CashDrawerReportService reportService2 = new CashDrawerReportService(report);
            reportService2.populateReport();
          }
          catch (Exception localException) {}
        }
        cashDrawerIds.add(report.getId());
        cashDrawersReportSummary.setBeginCash(Double.valueOf(cashDrawersReportSummary.getBeginCash().doubleValue() + report.getBeginCash().doubleValue()));
        cashDrawersReportSummary.setNetSales(Double.valueOf(cashDrawersReportSummary.getNetSales().doubleValue() + report.getNetSales().doubleValue()));
        cashDrawersReportSummary.setSalesTax(Double.valueOf(cashDrawersReportSummary.getSalesTax().doubleValue() + report.getSalesTax().doubleValue()));
        cashDrawersReportSummary.setSalesDeliveryCharge(Double.valueOf(cashDrawersReportSummary.getSalesDeliveryCharge().doubleValue() + report.getSalesDeliveryCharge().doubleValue()));
        cashDrawersReportSummary.setChargedTips(Double.valueOf(cashDrawersReportSummary.getChargedTips().doubleValue() + report.getChargedTips().doubleValue()));
        
        cashDrawersReportSummary.setTotalVoid(Double.valueOf(cashDrawersReportSummary.getTotalVoid().doubleValue() + report.getTotalVoid().doubleValue()));
        cashDrawersReportSummary.setTotalVoidWst(Double.valueOf(cashDrawersReportSummary.getTotalVoidWst().doubleValue() + report.getTotalVoidWst().doubleValue()));
        cashDrawersReportSummary.setTipsPaid(Double.valueOf(cashDrawersReportSummary.getTipsPaid().doubleValue() + report.getTipsPaid().doubleValue()));
        cashDrawersReportSummary.setTotalDiscountCount(Integer.valueOf(cashDrawersReportSummary.getTotalDiscountCount().intValue() + report.getTotalDiscountCount().intValue()));
        cashDrawersReportSummary.setTotalDiscountAmount(Double.valueOf(cashDrawersReportSummary.getTotalDiscountAmount().doubleValue() + report.getTotalDiscountAmount().doubleValue()));
        
        cashDrawersReportSummary.setCashReceiptCount(Integer.valueOf(cashDrawersReportSummary.getCashReceiptCount().intValue() + report.getCashReceiptCount().intValue()));
        cashDrawersReportSummary.setCashReceiptAmount(Double.valueOf(cashDrawersReportSummary.getCashReceiptAmount().doubleValue() + report.getCashReceiptAmount().doubleValue()));
        cashDrawersReportSummary.setCreditCardReceiptCount(Integer.valueOf(cashDrawersReportSummary.getCreditCardReceiptCount().intValue() + report.getCreditCardReceiptCount().intValue()));
        cashDrawersReportSummary
          .setCreditCardReceiptAmount(Double.valueOf(cashDrawersReportSummary.getCreditCardReceiptAmount().doubleValue() + report.getCreditCardReceiptAmount().doubleValue()));
        cashDrawersReportSummary.setDebitCardReceiptCount(Integer.valueOf(cashDrawersReportSummary.getDebitCardReceiptCount().intValue() + report.getDebitCardReceiptCount().intValue()));
        cashDrawersReportSummary.setDebitCardReceiptAmount(Double.valueOf(cashDrawersReportSummary.getDebitCardReceiptAmount().doubleValue() + report.getDebitCardReceiptAmount().doubleValue()));
        cashDrawersReportSummary.setGiftCertReturnCount(Integer.valueOf(cashDrawersReportSummary.getGiftCertReturnCount().intValue() + report.getGiftCertReturnCount().intValue()));
        cashDrawersReportSummary.setGiftCertReturnAmount(Double.valueOf(cashDrawersReportSummary.getGiftCertReturnAmount().doubleValue() + report.getGiftCertReturnAmount().doubleValue()));
        cashDrawersReportSummary.setGiftCertChangeAmount(Double.valueOf(cashDrawersReportSummary.getGiftCertChangeAmount().doubleValue() + report.getGiftCertChangeAmount().doubleValue()));
        cashDrawersReportSummary.setCustomPaymentCount(Integer.valueOf(cashDrawersReportSummary.getCustomPaymentCount().intValue() + report.getCustomPaymentCount().intValue()));
        cashDrawersReportSummary.setCustomPaymentAmount(Double.valueOf(cashDrawersReportSummary.getCustomPaymentAmount().doubleValue() + report.getCustomPaymentAmount().doubleValue()));
        cashDrawersReportSummary.setRefundReceiptCount(Integer.valueOf(cashDrawersReportSummary.getRefundReceiptCount().intValue() + report.getRefundReceiptCount().intValue()));
        cashDrawersReportSummary.setRefundAmount(Double.valueOf(cashDrawersReportSummary.getRefundAmount().doubleValue() + report.getRefundAmount().doubleValue()));
        cashDrawersReportSummary.setPayOutCount(Integer.valueOf(cashDrawersReportSummary.getPayOutCount().intValue() + report.getPayOutCount().intValue()));
        cashDrawersReportSummary.setPayOutAmount(Double.valueOf(cashDrawersReportSummary.getPayOutAmount().doubleValue() + report.getPayOutAmount().doubleValue()));
        cashDrawersReportSummary.setDrawerBleedCount(Integer.valueOf(cashDrawersReportSummary.getDrawerBleedCount().intValue() + report.getDrawerBleedCount().intValue()));
        cashDrawersReportSummary.setDrawerBleedAmount(Double.valueOf(cashDrawersReportSummary.getDrawerBleedAmount().doubleValue() + report.getDrawerBleedAmount().doubleValue()));
        cashDrawersReportSummary.setCashTips(Double.valueOf(cashDrawersReportSummary.getCashTips().doubleValue() + report.getCashTips().doubleValue()));
        cashDrawersReportSummary.setChargedTips(Double.valueOf(cashDrawersReportSummary.getChargedTips().doubleValue() + report.getChargedTips().doubleValue()));
        cashDrawersReportSummary.setCashBack(Double.valueOf(cashDrawersReportSummary.getCashBack().doubleValue() + report.getCashBack().doubleValue()));
        
        cashDrawersReportSummary.setCashToDeposit(Double.valueOf(cashDrawersReportSummary.getCashToDeposit().doubleValue() + report.getCashToDeposit().doubleValue()));
        
        List<CashBreakdown> cashBreakdowns = report.getCashBreakdownList();
        if (cashBreakdowns != null) {
          for (CashBreakdown cashBreakdown : cashBreakdowns) {
            CashBreakdown breakdown = (CashBreakdown)summaryCashBreakdownMap.get(cashBreakdown.getCurrency().getName());
            if (breakdown == null) {
              breakdown = new CashBreakdown();
              summaryCashBreakdownMap.put(cashBreakdown.getCurrency().getName(), breakdown);
            }
            breakdown.setBalance(Double.valueOf(breakdown.getBalance().doubleValue() + cashBreakdown.getBalance().doubleValue()));
            breakdown.setBalance(Double.valueOf(breakdown.getTotalAmount().doubleValue() + cashBreakdown.getTotalAmount().doubleValue()));
          }
        }
        cashDrawersReportSummary.setStoreOperationData(report.getStoreOperationData());
      }
    }
    cashDrawersReportSummary.calculate();
    cashDrawersReportSummary.setVoidEntries(new HashSet(VoidItemDAO.getInstance().getVoidEntries(cashDrawerIds)));
    return cashDrawersReportSummary;
  }
  
  public static void printCashDrawerReportSummary(StoreSession currentData) {
    List<CashDrawer> drawerReports = CashDrawerDAO.getInstance().findByStoreOperationData(currentData, Boolean.valueOf(false));
    CashDrawer cashDrawersReportSummary = populateCashDrawerReportSummary(drawerReports);
    cashDrawersReportSummary.setStartTime(currentData.getOpenTime());
    cashDrawersReportSummary.setAssignedBy(currentData.getOpenedBy());
    cashDrawersReportSummary.setReportTime(currentData.getCloseTime());
    cashDrawersReportSummary.setClosedBy(currentData.getClosedBy());
    printCashDrawerSummaryReport(cashDrawersReportSummary);
  }
}
