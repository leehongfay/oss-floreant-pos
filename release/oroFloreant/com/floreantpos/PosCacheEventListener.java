package com.floreantpos;

import com.floreantpos.ui.views.order.actions.DataChangeListener;
import java.util.ArrayList;
import java.util.List;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.event.CacheEventListener;

public class PosCacheEventListener
  implements CacheEventListener
{
  private static PosCacheEventListener instance = new PosCacheEventListener();
  
  private List<DataChangeListener> dataChangeListeners = new ArrayList();
  
  public PosCacheEventListener() {}
  
  public void notifyElementRemoved(Ehcache cache, Element element)
    throws CacheException
  {}
  
  public void notifyElementPut(Ehcache cache, Element element) throws CacheException
  {
    for (DataChangeListener dataChangeListener : dataChangeListeners) {
      dataChangeListener.dataChanged(element);
    }
  }
  
  public void notifyElementUpdated(Ehcache cache, Element element)
    throws CacheException
  {
    for (DataChangeListener dataChangeListener : dataChangeListeners) {
      dataChangeListener.dataChanged(element);
    }
  }
  


  public void notifyElementExpired(Ehcache cache, Element element) {}
  


  public void notifyElementEvicted(Ehcache cache, Element element) {}
  


  public void notifyRemoveAll(Ehcache cache) {}
  

  public void dispose() {}
  

  public Object clone()
    throws CloneNotSupportedException
  {
    return instance;
  }
  
  public void addDataChangeListener(DataChangeListener listener) {
    dataChangeListeners.add(listener);
  }
  
  public static PosCacheEventListener getInstance() {
    return instance;
  }
}
