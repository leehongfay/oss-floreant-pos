package com.floreantpos;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

















public class Messages
{
  private static final String BUNDLE_NAME = "messages";
  private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("messages", new ResourceControl(null));
  
  private Messages() {}
  
  public static String getString(String key)
  {
    try {
      return RESOURCE_BUNDLE.getString(key);
    }
    catch (MissingResourceException e) {}
    return '!' + key + '!';
  }
  
  public static String getString(String key, String defaultValue)
  {
    try {
      return RESOURCE_BUNDLE.getString(key);
    }
    catch (MissingResourceException e) {}
    return defaultValue;
  }
  
  private static class ResourceControl extends ResourceBundle.Control
  {
    private ResourceControl() {}
    
    public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload) throws IllegalAccessException, InstantiationException, IOException {
      String bundlename = toBundleName(baseName, locale);
      String resName = toResourceName(bundlename, "properties");
      InputStream stream = loader.getResourceAsStream(resName);
      return new PropertyResourceBundle(new InputStreamReader(stream, "UTF-8"));
    }
  }
}
