package com.floreantpos.swing;

import com.floreantpos.actions.ActionCommand;
import com.floreantpos.actions.PosAction;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionListener;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;



















public class PosButton
  extends JButton
{
  public static Border border = new LineBorder(Color.BLACK, 1);
  static Insets margin = new Insets(2, 5, 2, 5);
  
  public static final int PREFERRED_WIDTH = 50;
  
  public static final int PREFERRED_HEIGHT = 50;
  static POSButtonUI ui = new POSButtonUI();
  
  static {
    UIManager.put("PosButtonUI", "com.floreantpos.swing.POSButtonUI");
  }
  
  public PosButton() {
    this("");
  }
  
  public PosButton(String text) {
    super(text);
    initCommonProperties();
  }
  
  public PosButton(String text, Action action) {
    super(action);
    setText(text);
    
    initCommonProperties();
  }
  
  public PosButton(Action a) {
    super(a);
    
    initCommonProperties();
  }
  
  public PosButton(ActionCommand command) {
    this(command.toString());
    setActionCommand(command.name());
    initCommonProperties();
  }
  
  public PosButton(String text, ActionCommand command) {
    this(text);
    setActionCommand(command.name());
    initCommonProperties();
  }
  
  public PosButton(ActionCommand command, ActionListener listener) {
    this(command.toString());
    setActionCommand(command.name());
    addActionListener(listener);
    initCommonProperties();
  }
  
  public PosButton(ImageIcon imageIcon) {
    super(imageIcon);
    initCommonProperties();
  }
  
  public String getUIClassID()
  {
    return "PosButtonUI";
  }
  
  public Dimension getPreferredSize()
  {
    Dimension size = null;
    
    if (isPreferredSizeSet()) {
      return super.getPreferredSize();
    }
    if (ui != null) {
      size = ui.getPreferredSize(this);
    }
    
    if (size == null) {
      size = new Dimension(PosUIManager.getSize(60, 45));
    }
    else {
      int width = width < 60 ? 60 : width;
      int height = height < 45 ? 45 : height;
      size.setSize(PosUIManager.getSize(width, height));
    }
    
    return size;
  }
  
  public void setAction(Action a)
  {
    super.setAction(a);
    
    if ((a instanceof PosAction)) {
      PosAction action = (PosAction)a;
      setVisible(action.isVisible());
    }
  }
  
  private void initCommonProperties() {
    setFocusable(false);
    setFocusPainted(false);
    setMargin(margin);
  }
}
