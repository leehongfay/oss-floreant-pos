package com.floreantpos.swing.event;

import java.util.EventListener;

public abstract interface KeypadEventListener
  extends EventListener
{
  public abstract void receiveKeypadEvent(KeypadEvent paramKeypadEvent);
}
