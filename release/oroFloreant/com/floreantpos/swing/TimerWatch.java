package com.floreantpos.swing;

import com.floreantpos.config.AppConfig;
import com.floreantpos.config.TerminalConfig;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.event.ChangeListener;
import net.miginfocom.swing.MigLayout;
import org.joda.time.Duration;
import org.joda.time.Instant;
import org.joda.time.Interval;






















public class TimerWatch
  extends JPanel
  implements ActionListener
{
  private Timer updateTimer = new Timer(1000, this);
  private JLabel timerLabel = new JLabel("");
  
  private final Date date;
  private final Color initialBgColor;
  private final Color initialFgColor;
  private final Color warningBgColor;
  private final Color warningFgColor;
  private final Color overBgColor;
  private final Color overFgColor;
  public Color backColor;
  public Color textColor;
  private ChangeListener colorChangeListener;
  
  public TimerWatch(Date date)
  {
    this(date, null);
  }
  
  public TimerWatch(Date date, ChangeListener colorChangeListener) {
    this.date = date;
    this.colorChangeListener = colorChangeListener;
    setOpaque(false);
    setLayout(new MigLayout("right,ins 0"));
    
    initialBgColor = TerminalConfig.getColor("kds.initial.bg", new Color(0, 135, 67));
    initialFgColor = TerminalConfig.getColor("kds.initial.fg", Color.white);
    warningBgColor = TerminalConfig.getColor("kds.warning.bg", new Color(247, 177, 55));
    warningFgColor = TerminalConfig.getColor("kds.warning.fg", Color.white);
    overBgColor = TerminalConfig.getColor("kds.over.bd", new Color(204, 0, 0));
    overFgColor = TerminalConfig.getColor("kds.over.fg", Color.white);
    
    backColor = initialBgColor;
    textColor = initialFgColor;
    
    timerLabel.setFont(timerLabel.getFont().deriveFont(1, PosUIManager.getDefaultFontSize() + 2));
    timerLabel.setHorizontalAlignment(4);
    timerLabel.setOpaque(false);
    timerLabel.setForeground(Color.black);
    
    actionPerformed(null);
    
    add(timerLabel);
  }
  
  public void actionPerformed(ActionEvent e)
  {
    long currentTimeMillis = new Instant().getMillis();
    long createTimeMillis = date.getTime();
    long diff = createTimeMillis - currentTimeMillis;
    
    Interval interval = null;
    if (diff > 0L) {
      interval = new Interval(currentTimeMillis, date.getTime());
    }
    else {
      interval = new Interval(date.getTime(), currentTimeMillis);
    }
    Duration duration = interval.toDuration();
    
    int timeOutValueYellow = 300;
    int timeOutValueRed = 600;
    
    if (AppConfig.getString("YellowTimeOut") != null) {
      timeOutValueYellow = Integer.parseInt(AppConfig.getString("YellowTimeOut"));
    }
    
    if (AppConfig.getString("RedTimeOut") != null) {
      timeOutValueRed = Integer.parseInt(AppConfig.getString("RedTimeOut"));
    }
    int oldColorCode = backColor != null ? backColor.getRGB() : -1;
    if ((timeOutValueYellow < duration.getStandardSeconds()) && (timeOutValueRed > duration.getStandardSeconds())) {
      backColor = warningBgColor;
      textColor = warningFgColor;
    }
    else if (timeOutValueRed < duration.getStandardSeconds()) {
      backColor = overBgColor;
      textColor = overFgColor;
    }
    else {
      backColor = initialBgColor;
      textColor = initialFgColor;
    }
    timerLabel.setForeground(textColor);
    timerLabel.setText(format(duration.getStandardHours()) + ":" + 
      format(duration.getStandardMinutes() % 60L) + ":" + format(duration.getStandardSeconds() % 60L));
    
    boolean colorChanged = (backColor != null) && (backColor.getRGB() != oldColorCode);
    if ((colorChangeListener != null) && (colorChanged)) {
      colorChangeListener.stateChanged(null);
    }
  }
  
  private String format(long value) {
    String stringValue = String.valueOf(value);
    return "0" + value;
  }
  
  public void start() {
    updateTimer.start();
  }
  
  public void stop() {
    updateTimer.stop();
  }
}
