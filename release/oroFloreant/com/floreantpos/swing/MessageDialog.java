package com.floreantpos.swing;

import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;

















public class MessageDialog
{
  public MessageDialog() {}
  
  private static Logger logger = Logger.getLogger(Application.class);
  
  public static void showError(String errorMessage) {
    JOptionPane.showMessageDialog(Application.getPosWindow(), errorMessage, Messages.getString("MessageDialog.0"), 0);
  }
  
  public static void showError(String errorMessage, Throwable t) {
    logger.error(errorMessage, t);
    JOptionPane.showMessageDialog(Application.getPosWindow(), errorMessage, Messages.getString("MessageDialog.0"), 0);
  }
  
  public static void showError(Throwable t) {
    logger.error(t.getMessage(), t);
    JOptionPane.showMessageDialog(Application.getPosWindow(), Messages.getString("GenericErrorMessage"), Messages.getString("MessageDialog.0"), 0);
  }
}
