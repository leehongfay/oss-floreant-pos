package com.floreantpos.swing;

import com.floreantpos.main.Application;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;



















public class PosOptionPane
  extends OkCancelOptionDialog
{
  private JTextArea taInputText;
  private String inputText;
  private QwertyKeyPad qwertyKeyPad;
  private String inputValidationPattern;
  private String validationMessage;
  
  private PosOptionPane()
  {
    super(POSUtil.getFocusedWindow());
    init();
  }
  
  private PosOptionPane(Window parent) {
    super(parent);
    init();
  }
  
  private void init() {
    setResizable(false);
    
    JPanel contentPane = getContentPanel();
    
    MigLayout layout = new MigLayout("inset 0");
    contentPane.setLayout(layout);
    
    Dimension size = PosUIManager.getSize(0, 100);
    
    taInputText = new JTextArea();
    taInputText.setFont(taInputText.getFont().deriveFont(1, PosUIManager.getFontSize(16)));
    taInputText.setFocusable(true);
    taInputText.requestFocus();
    taInputText.setLineWrap(true);
    taInputText.setBackground(Color.WHITE);
    taInputText.setPreferredSize(size);
    
    qwertyKeyPad = new QwertyKeyPad();
    
    JScrollPane scrollPane = new JScrollPane(taInputText);
    
    contentPane.add(scrollPane, "spanx, grow");
    contentPane.add(qwertyKeyPad, "spanx ,grow");
  }
  
  public void doOk()
  {
    String s = taInputText.getText();
    if (s.isEmpty()) {
      POSMessageDialog.showError(Application.getPosWindow(), "Please enter value");
      return;
    }
    String INPUT_VALIDATION_PATTERN = getInputValidationPattern();
    if (!StringUtils.isEmpty(INPUT_VALIDATION_PATTERN)) {
      Pattern pattern = Pattern.compile(INPUT_VALIDATION_PATTERN);
      Matcher matcher = pattern.matcher(s);
      if (matcher.matches()) {
        setValue(s);
        setCanceled(false);
        dispose();
      }
      else {
        POSMessageDialog.showError(POSUtil.getFocusedWindow(), getValidationMessage());
        return;
      }
    }
    else {
      setValue(taInputText.getText());
      setCanceled(false);
      dispose();
    }
  }
  
  public String getValue() {
    return inputText;
  }
  
  public void setValue(String value) {
    inputText = value;
  }
  
  private void setDefaultValue(String value) {
    taInputText.setText(value);
  }
  
  public static String showInputDialog(String title) {
    PosOptionPane dialog = new PosOptionPane();
    dialog.setCaption(title);
    dialog.setTitle(title);
    dialog.pack();
    dialog.open();
    if (dialog.isCanceled()) {
      return null;
    }
    return dialog.getValue();
  }
  
  public static String showInputDialog(String title, String inputValidationPattern, String validationMessage, boolean isValidationCheck)
  {
    PosOptionPane dialog = new PosOptionPane();
    dialog.setCaption(title);
    dialog.setTitle(title);
    dialog.setInputValidationPattern(inputValidationPattern);
    dialog.setValidationMessage(validationMessage);
    dialog.pack();
    dialog.open();
    if (dialog.isCanceled()) {
      return null;
    }
    return dialog.getValue();
  }
  
  public static String showInputDialog(String title, String defaultText) {
    PosOptionPane dialog = new PosOptionPane();
    dialog.setCaption(title);
    dialog.setTitle(title);
    dialog.setDefaultValue(defaultText);
    dialog.pack();
    dialog.open();
    if (dialog.isCanceled()) {
      return null;
    }
    return dialog.getValue();
  }
  
  public static String showInputDialog(Window parent, String title) {
    PosOptionPane dialog = new PosOptionPane(parent);
    dialog.setCaption(title);
    dialog.setTitle(title);
    dialog.pack();
    dialog.open();
    if (dialog.isCanceled()) {
      return null;
    }
    return dialog.getValue();
  }
  
  public String getInputValidationPattern() {
    return inputValidationPattern;
  }
  
  public void setInputValidationPattern(String inputValidationPattern) {
    this.inputValidationPattern = inputValidationPattern;
  }
  
  public String getValidationMessage() {
    return validationMessage;
  }
  
  public void setValidationMessage(String validation) {
    validationMessage = validation;
  }
}
