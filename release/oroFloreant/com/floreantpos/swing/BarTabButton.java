package com.floreantpos.swing;

import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.PasswordEntryDialog;
import java.awt.Color;
import org.apache.commons.lang.StringUtils;

















public class BarTabButton
  extends PosButton
{
  private User user;
  private Ticket ticket;
  
  public BarTabButton(Ticket ticket)
  {
    this.ticket = ticket;
    if (ticket.getId() != null) {
      setText(ticket.getId() + "");
    }
    update();
  }
  
  public String getId() {
    return ticket.getId();
  }
  
  public boolean equals(Object obj)
  {
    if (!(obj instanceof BarTabButton)) {
      return false;
    }
    
    BarTabButton that = (BarTabButton)obj;
    
    return ticket.equals(ticket);
  }
  
  public int hashCode()
  {
    return ticket.hashCode();
  }
  
  public String toString()
  {
    return ticket.toString();
  }
  
  public void update() {
    setEnabled(true);
    setBackground(Color.white);
    setForeground(Color.black);
  }
  
  public void setUser(User user) {
    if (user != null) {
      this.user = user;
    }
  }
  
  public User getUser() {
    return user;
  }
  
  public boolean hasUserAccess()
  {
    if (user == null) {
      return false;
    }
    User currentUser = Application.getCurrentUser();
    
    String currentUserId = currentUser.getId();
    String ticketUserId = user.getId();
    
    if (currentUserId == ticketUserId) {
      return true;
    }
    
    if ((currentUser.hasPermission(UserPermission.PERFORM_MANAGER_TASK)) || (currentUser.hasPermission(UserPermission.PERFORM_ADMINISTRATIVE_TASK))) {
      return true;
    }
    
    String password = PasswordEntryDialog.show(Application.getPosWindow(), Messages.getString("PosAction.0"));
    if (StringUtils.isEmpty(password)) {
      return false;
    }
    
    String inputUserId = UserDAO.getInstance().findUserBySecretKey(password).getId();
    if (inputUserId != user.getId()) {
      POSMessageDialog.showError(Application.getPosWindow(), "Incorrect password");
      return false;
    }
    return true;
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }
  
  public Ticket getTicket() {
    return ticket;
  }
}
