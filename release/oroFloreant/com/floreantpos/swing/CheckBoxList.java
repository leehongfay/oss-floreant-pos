package com.floreantpos.swing;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

















public class CheckBoxList<E>
  extends JTable
{
  public CheckBoxList()
  {
    setRowHeight(PosUIManager.getSize(getRowHeight()));
  }
  
  public CheckBoxList(E[] items) {
    setModel(items);
    setRowHeight(PosUIManager.getSize(getRowHeight()));
  }
  
  public CheckBoxList(List<E> items) {
    setModel(items);
    setRowHeight(PosUIManager.getSize(getRowHeight()));
  }
  
  public void setModel(E[] items) {
    setModel(items, new String[] { "", "" });
  }
  
  public void setModel(List<E> items) {
    setModel(items, new String[] { "", "" });
  }
  
  public void setModel(List<E> items, String[] header) {
    CheckBoxListModel<E> model = new CheckBoxListModel(header, items);
    setModel(model);
    init();
  }
  
  public void setModel(E[] items, String[] header) {
    CheckBoxListModel<E> model = new CheckBoxListModel(items, header);
    setModel(model);
    init();
  }
  
  public List<E> getCheckedValues() {
    List values = new ArrayList();
    CheckBoxListModel model = (CheckBoxListModel)getModel();
    int rowCount = model.getRowCount();
    for (int i = 0; i < rowCount; i++) {
      Entry<E> entry = (Entry)model.getRow(i);
      if (checked) {
        values.add(value);
      }
    }
    return values;
  }
  
  public Set<E> getCheckedValuesAsSet() {
    Set<E> values = new HashSet();
    CheckBoxListModel model = (CheckBoxListModel)getModel();
    int rowCount = model.getRowCount();
    for (int i = 0; i < rowCount; i++) {
      Entry<E> entry = (Entry)model.getRow(i);
      if (checked) {
        values.add(value);
      }
    }
    return values;
  }
  

  public void selectAll()
  {
    CheckBoxListModel model = (CheckBoxListModel)getModel();
    for (int i = 0; i < model.getRowCount(); i++) {
      Entry entry = (Entry)model.getRow(i);
      checked = true;
    }
    

    model.fireTableRowsUpdated(0, model.getRowCount());
  }
  
  public void selectItems(List types) {
    CheckBoxListModel model = (CheckBoxListModel)getModel();
    if (types != null) {
      for (int i = 0; i < model.getRowCount(); i++) {
        Entry entry = (Entry)model.getRow(i);
        for (int j = 0; j < types.size(); j++) {
          Object type = types.get(j);
          if (type.equals(value)) {
            checked = true;
            break;
          }
        }
      }
      model.fireTableRowsUpdated(0, model.getRowCount());
    }
  }
  
  public void selectItems(Set<E> types) {
    CheckBoxListModel model = (CheckBoxListModel)getModel();
    if (types != null) { Entry entry;
      for (int i = 0; i < model.getRowCount(); i++) {
        entry = (Entry)model.getRow(i);
        for (E e : types) {
          if (e.equals(value)) {
            checked = true;
            break;
          }
        }
      }
      model.fireTableRowsUpdated(0, model.getRowCount());
    }
  }
  
  public void selectItem(E item) {
    if (item == null) {
      return;
    }
    CheckBoxListModel model = (CheckBoxListModel)getModel();
    for (int i = 0; i < model.getRowCount(); i++) {
      Entry entry = (Entry)model.getRow(i);
      if (item.equals(value)) {
        checked = true;
        break;
      }
    }
    model.fireTableRowsUpdated(0, model.getRowCount());
  }
  
  public void unCheckAll() {
    CheckBoxListModel model = (CheckBoxListModel)getModel();
    for (int i = 0; i < model.getRowCount(); i++) {
      Entry entry = (Entry)model.getRow(i);
      checked = false;
    }
    

    model.fireTableRowsUpdated(0, model.getRowCount());
  }
  
  public Entry[] getValues() {
    CheckBoxListModel model = (CheckBoxListModel)getModel();
    return (Entry[])model.getItems().toArray();
  }
  
  public Object getSelectedValue() {
    int row = getSelectedRow();
    if (row == -1) {
      return null;
    }
    
    return getModel().getValueAt(row, 1);
  }
  


  public TableCellRenderer getCellRenderer(int row, int column)
  {
    TableCellRenderer cellRenderer = super.getCellRenderer(row, column);
    
    if ((cellRenderer instanceof JCheckBox)) {
      ((JCheckBox)cellRenderer).setEnabled(isEnabled());
    }
    return cellRenderer;
  }
  
  public void init() {
    getSelectionModel().setSelectionMode(0);
    setShowGrid(false);
    setAutoResizeMode(3);
    if ((getColumnModel() != null) && (getColumnCount() > 0)) {
      TableColumn column = getColumnModel().getColumn(0);
      int checkBoxWidth = JCheckBoxgetPreferredSizewidth;
      column.setPreferredWidth(checkBoxWidth);
      column.setMinWidth(checkBoxWidth);
      column.setWidth(checkBoxWidth);
      column.setMaxWidth(checkBoxWidth);
      column.setResizable(false);
    }
  }
  

  public static class Entry<E>
  {
    public boolean checked;
    
    public E value;
    
    public Entry(boolean checked, E value)
    {
      this.checked = checked;
      this.value = value;
    }
    
    public boolean isChecked()
    {
      return checked;
    }
    
    public Object getValue()
    {
      return value;
    }
    
    public void setChecked(boolean checked)
    {
      this.checked = checked;
    }
    
    public void setValue(E value)
    {
      this.value = value;
    }
  }
  
  public void setTableHeaderHide(boolean hideHeader) {
    if (hideHeader) {
      setTableHeader(null);
    }
  }
}
