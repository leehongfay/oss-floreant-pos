package com.floreantpos.swing;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractListModel;

















public class ListModel<E>
  extends AbstractListModel<E>
{
  private List<E> list;
  
  public ListModel()
  {
    list = new ArrayList();
  }
  
  public ListModel(List<E> list) {
    this.list = list;
  }
  
  public void setData(List<E> list) {
    this.list = list;
  }
  
  public List<E> getDataList() {
    return list;
  }
  





  public void addElement(E element)
  {
    int index = list.size();
    list.add(element);
    fireIntervalAdded(this, index, index);
  }
  





  public Iterator<E> iterator()
  {
    return list.iterator();
  }
  
  public int getSize()
  {
    return list.size();
  }
  
  public E getElementAt(int index)
  {
    return list.get(index);
  }
  
  public void remove(int index) {
    list.remove(index);
    fireContentsChanged(this, index, index);
  }
  
  public void removeElement(E element) {
    list.remove(element);
  }
}
