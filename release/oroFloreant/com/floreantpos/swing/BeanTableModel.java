package com.floreantpos.swing;

import com.floreantpos.PosLog;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.util.NumberUtil;
import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;


























public class BeanTableModel<M>
  extends AbstractTableModel
  implements PaginationSupport
{
  private int numRows;
  private int currentRowIndex;
  private int pageSize = 100;
  
  private List<M> rows = new ArrayList();
  private List<BeanColumn> columns = new ArrayList();
  private Class<?> beanClass;
  private BeanTableModel<M>.BeanTableCellEditor numericCellEditor;
  private BeanTableModel<M>.BeanTableCellEditor defaultCellEditor;
  private String sortBy;
  private boolean ascOrder;
  
  public static enum DataType
  {
    MONEY,  NUMBER,  DATE;
    
    private DataType() {} }
  
  public BeanTableModel(Class<?> beanClass) { this(beanClass, 50); }
  
  public BeanTableModel(Class<?> beanClass, int pageSize)
  {
    this.beanClass = beanClass;
    this.pageSize = pageSize;
    
    DoubleTextField numericCellEditorField = new DoubleTextField();
    numericCellEditorField.setHorizontalAlignment(4);
    numericCellEditor = new BeanTableCellEditor(numericCellEditorField);
    numericCellEditor.setClickCountToStart(1);
    
    JTextField tf = new JTextField();
    defaultCellEditor = new BeanTableCellEditor(tf);
    defaultCellEditor.setClickCountToStart(1);
  }
  
  public void addColumn(String columnGUIName, String beanAttribute, EditMode editable) {
    addColumn(columnGUIName, beanAttribute, editable, 10, null);
  }
  
  public void addColumn(String columnGUIName, String beanAttribute, EditMode editable, int horizontalAlignment, DataType dataType) {
    try {
      PropertyDescriptor descriptor = new PropertyDescriptor(beanAttribute, beanClass);
      columns.add(new BeanColumn(columnGUIName, editable, descriptor, horizontalAlignment, dataType));
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    }
  }
  
  public void addColumn(String columnGUIName, String beanAttribute) {
    addColumn(columnGUIName, beanAttribute, EditMode.NON_EDITABLE);
  }
  
  public void addColumn(String columnGUIName, String beanAttribute, int horizontalAlignment, DataType dataType) {
    addColumn(columnGUIName, beanAttribute, EditMode.NON_EDITABLE, horizontalAlignment, dataType);
  }
  
  public void addRow(M row) {
    rows.add(row);
    fireTableDataChanged();
  }
  
  public void removeRow(M row) {
    rows.remove(row);
    fireTableDataChanged();
  }
  
  public void removeRow(int index) {
    rows.remove(index);
    fireTableRowsDeleted(index, index);
  }
  
  public void removeAll() {
    rows.clear();
    fireTableDataChanged();
  }
  
  public void addRows(List<M> rows) {
    if (rows == null) {
      return;
    }
    for (M row : rows) {
      addRow(row);
    }
    fireTableDataChanged();
  }
  
  public void setRows(List rows) {
    this.rows = rows;
    fireTableDataChanged();
  }
  
  public int getColumnCount() {
    return columns.size();
  }
  
  public int getRowCount()
  {
    if (rows == null) {
      return 0;
    }
    return rows.size();
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    BeanColumn column = (BeanColumn)columns.get(columnIndex);
    M row = rows.get(rowIndex);
    
    Object result = null;
    try {
      result = descriptor.getReadMethod().invoke(row, new Object[0]);
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    }
    return result;
  }
  
  public void setValueAt(Object value, int rowIndex, int columnIndex) {
    M row = rows.get(rowIndex);
    BeanColumn column = (BeanColumn)columns.get(columnIndex);
    try
    {
      descriptor.getWriteMethod().invoke(row, new Object[] { value });
    } catch (Exception e) {
      PosLog.error(getClass(), e);
    }
  }
  
  public Class<?> getColumnClass(int columnIndex) {
    BeanColumn column = (BeanColumn)columns.get(columnIndex);
    Class<?> returnType = descriptor.getReadMethod().getReturnType();
    return returnType;
  }
  
  public String getColumnName(int column) {
    return columns.get(column)).columnGUIName;
  }
  
  public BeanColumn getColumn(int column) {
    return (BeanColumn)columns.get(column);
  }
  
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    return columns.get(columnIndex)).editable == EditMode.EDITABLE;
  }
  
  public void setRow(int index, M row) {
    getRows().set(index, row);
    fireTableRowsUpdated(index, index);
  }
  
  public M getRow(int index) {
    return getRows().get(index);
  }
  
  public List<M> getRows() {
    return rows;
  }
  
  public static enum EditMode {
    NON_EDITABLE,  EDITABLE;
    
    private EditMode() {} }
  
  public int getNumRows() { return numRows; }
  
  public void setNumRows(int numRows)
  {
    this.numRows = numRows;
  }
  
  public int getCurrentRowIndex() {
    return currentRowIndex;
  }
  
  public void setCurrentRowIndex(int currentRowIndex) {
    this.currentRowIndex = currentRowIndex;
  }
  
  public int getPageSize() {
    return pageSize;
  }
  
  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }
  
  public boolean hasNext() {
    return currentRowIndex + pageSize < numRows;
  }
  
  public boolean hasPrevious() {
    return currentRowIndex > 0;
  }
  
  public int getNextRowIndex() {
    if (numRows == 0) {
      return 0;
    }
    
    return getCurrentRowIndex() + getPageSize();
  }
  
  public int getPreviousRowIndex() {
    int i = getCurrentRowIndex() - getPageSize();
    if (i < 0) {
      i = 0;
    }
    
    return i;
  }
  
  public static class BeanColumn
  {
    private String columnGUIName;
    private BeanTableModel.EditMode editable;
    private PropertyDescriptor descriptor;
    private int horizontalAlignment = 10;
    


    private BeanTableModel.DataType dataType;
    


    public BeanColumn(String columnGUIName, BeanTableModel.EditMode editable, PropertyDescriptor descriptor, int horizontalAlignment, BeanTableModel.DataType dataType)
    {
      this.columnGUIName = columnGUIName;
      this.editable = editable;
      this.descriptor = descriptor;
      this.horizontalAlignment = horizontalAlignment;
      this.dataType = dataType;
    }
    
    public boolean isEditable() {
      if (editable == null) {
        return false;
      }
      if (editable == BeanTableModel.EditMode.EDITABLE) {
        return true;
      }
      return false;
    }
    
    public int getHorizontalAlignment() {
      return horizontalAlignment;
    }
    
    public void setHorizontalAlignment(int horizontalAlignment) {
      this.horizontalAlignment = horizontalAlignment;
    }
    
    public BeanTableModel.DataType getDataType() {
      return dataType;
    }
  }
  
  public void initTableRenderer(JTable table) {
    BeanTableCellRenderer renderer = new BeanTableCellRenderer();
    table.setDefaultRenderer(Object.class, renderer);
    table.setDefaultRenderer(Double.class, renderer);
    table.setDefaultRenderer(Number.class, renderer);
    table.setRowHeight(PosUIManager.getSize(30));
    
    for (int i = 0; i < columns.size(); i++) {
      BeanColumn beanColumn = (BeanColumn)columns.get(i);
      if (beanColumn.isEditable())
      {


        if ((beanColumn.getDataType() == DataType.MONEY) || (beanColumn.getDataType() == DataType.NUMBER)) {
          table.setDefaultEditor(table.getColumnClass(i), numericCellEditor);
        }
        else {
          table.setDefaultEditor(table.getColumnClass(i), defaultCellEditor);
        }
      }
    }
  }
  
  public static class BeanTableCellRenderer extends DefaultTableCellRenderer
  {
    private Border unselectedBorder = null;
    private Border selectedBorder = null;
    
    public BeanTableCellRenderer() {}
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
      BeanTableModel model = (BeanTableModel)table.getModel();
      BeanTableModel.BeanColumn beanColumn = model.getColumn(column);
      setHorizontalAlignment(beanColumn.getHorizontalAlignment());
      if (selectedBorder == null) {
        selectedBorder = BorderFactory.createMatteBorder(5, 5, 5, 5, table.getSelectionBackground());
      }
      if (unselectedBorder == null) {
        unselectedBorder = BorderFactory.createMatteBorder(5, 5, 5, 5, table.getBackground());
      }
      
      if ((value instanceof byte[])) {
        byte[] imageData = (byte[])value;
        ImageIcon image = new ImageIcon(imageData);
        image = new ImageIcon(image.getImage().getScaledInstance(100, 100, 4));
        if (imageData != null) {
          table.setRowHeight(row, 120);
        }
        
        JLabel l = new JLabel(image);
        if (isSelected) {
          l.setBorder(selectedBorder);
        }
        else {
          l.setBorder(unselectedBorder);
        }
        l.setHorizontalAlignment(0);
        return l;
      }
      
      if ((value instanceof Color)) {
        JLabel l = new JLabel();
        
        Color newColor = (Color)value;
        l.setOpaque(true);
        l.setBackground(newColor);
        if (isSelected) {
          l.setBorder(selectedBorder);
        }
        else {
          l.setBorder(unselectedBorder);
        }
        return l;
      }
      
      if ((value instanceof Date)) {
        String valueStr = DateUtil.formatFullDateAndTimeWithoutYearAsString((Date)value);
        return super.getTableCellRendererComponent(table, valueStr, isSelected, hasFocus, row, column);
      }
      
      BeanTableModel.DataType dataType = beanColumn.getDataType();
      if (dataType == null) {
        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      }
      
      switch (BeanTableModel.1.$SwitchMap$com$floreantpos$swing$BeanTableModel$DataType[dataType.ordinal()]) {
      case 1: 
        try {
          value = NumberUtil.getCurrencyFormat(value);
        }
        catch (Exception localException) {}
      
      case 2: 
        try
        {
          value = NumberUtil.formatNumberAcceptNegative((Double)value);
        }
        catch (Exception localException1) {}
      

      default: 
        value = "<html>" + value + "</html>";
      }
      
      
      return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }
  }
  
  class BeanTableCellEditor extends DefaultCellEditor
  {
    public BeanTableCellEditor(JTextField textField) {
      super();
    }
    
    public Object getCellEditorValue()
    {
      if ((editorComponent instanceof DoubleTextField)) {
        return Double.valueOf(((DoubleTextField)editorComponent).getDoubleOrZero());
      }
      if ((editorComponent instanceof IntegerTextField)) {
        return Integer.valueOf(((IntegerTextField)editorComponent).getInteger());
      }
      return super.getCellEditorValue();
    }
  }
  
  public void setSortBy(String propertyName, boolean ascOrder)
  {
    sortBy = propertyName;
    this.ascOrder = ascOrder;
  }
  
  public boolean isAscOrder() {
    return ascOrder;
  }
  
  public String getSortBy() {
    return sortBy;
  }
}
