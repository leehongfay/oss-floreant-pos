package com.floreantpos.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.AbstractCellEditor;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;






























public class ButtonColumn
  extends AbstractCellEditor
  implements TableCellRenderer, TableCellEditor, ActionListener, MouseListener
{
  private JTable table;
  private Action action;
  private int mnemonic;
  private Border originalBorder;
  private Border focusBorder;
  private PosButton renderButton;
  private PosButton editButton;
  private Object editorValue;
  private boolean isButtonColumnEditor;
  private boolean labelVisible;
  private JLabel label = new JLabel();
  



  private JPanel panel;
  




  public ButtonColumn(JTable table, Action action, int column)
  {
    this.table = table;
    this.action = action;
    
    panel = new JPanel(new BorderLayout());
    renderButton = new PosButton();
    editButton = new PosButton();
    editButton.setFocusPainted(false);
    editButton.addActionListener(this);
    originalBorder = editButton.getBorder();
    setFocusBorder(new LineBorder(Color.BLUE));
    
    label.setFocusable(false);
    label.setOpaque(false);
    
    TableColumnModel columnModel = table.getColumnModel();
    columnModel.getColumn(column).setCellRenderer(this);
    columnModel.getColumn(column).setCellEditor(this);
    table.addMouseListener(this);
  }
  
  public void showColumnValueInLabel(boolean showText) {
    if (showText) {
      labelVisible = showText;
      panel.add(editButton, "West");
      panel.add(label);
    }
  }
  




  public Border getFocusBorder()
  {
    return focusBorder;
  }
  




  public void setFocusBorder(Border focusBorder)
  {
    this.focusBorder = focusBorder;
    editButton.setBorder(focusBorder);
  }
  
  public void setUnselectedBorder(Border originalBorder) {
    this.originalBorder = originalBorder;
    editButton.setBorder(originalBorder);
    editButton.setOpaque(false);
  }
  
  public int getMnemonic() {
    return mnemonic;
  }
  




  public void setMnemonic(int mnemonic)
  {
    this.mnemonic = mnemonic;
    renderButton.setMnemonic(mnemonic);
    editButton.setMnemonic(mnemonic);
  }
  
  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
  {
    if (value == null) {
      label.setText("");
      editButton.setText("");
      editButton.setIcon(null);
    }
    else if ((value instanceof Icon)) {
      label.setText("");
      editButton.setText("");
      editButton.setIcon((Icon)value);

    }
    else if (labelVisible) {
      label.setText(value.toString());
    }
    else {
      editButton.setText(value.toString());
      editButton.setIcon(null);
    }
    
    editorValue = value;
    if (labelVisible) {
      return panel;
    }
    return editButton;
  }
  
  public Object getCellEditorValue()
  {
    return editorValue;
  }
  


  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
  {
    if (isSelected) {
      renderButton.setForeground(table.getSelectionForeground());
      renderButton.setBackground(table.getSelectionBackground());
      label.setBackground(table.getSelectionBackground());
      label.setForeground(table.getSelectionForeground());
    }
    else {
      renderButton.setForeground(table.getForeground());
      renderButton.setBackground(UIManager.getColor("Button.background"));
      label.setBackground(table.getBackground());
      label.setForeground(table.getForeground());
    }
    
    if (hasFocus) {
      renderButton.setBorder(focusBorder);
    }
    else {
      renderButton.setBorder(originalBorder);
    }
    

    if (value == null) {
      label.setText("");
      renderButton.setText("");
      renderButton.setIcon(null);
    }
    else if ((value instanceof Icon)) {
      label.setText("");
      renderButton.setText("");
      renderButton.setIcon((Icon)value);
    }
    else {
      if (labelVisible) {
        label.setText(value.toString());
      }
      else {
        renderButton.setText(value.toString());
      }
      renderButton.setIcon(null);
    }
    if (labelVisible) {
      if (isSelected) {
        panel.setBackground(table.getSelectionBackground());
      } else {
        panel.setBackground(table.getBackground());
      }
      return panel;
    }
    return renderButton;
  }
  





  public void actionPerformed(ActionEvent e)
  {
    int row = table.convertRowIndexToModel(table.getEditingRow());
    fireEditingStopped();
    


    ActionEvent event = new ActionEvent(table, 1001, "" + row);
    action.actionPerformed(event);
  }
  







  public void mousePressed(MouseEvent e)
  {
    if ((table.isEditing()) && (table.getCellEditor() == this))
      isButtonColumnEditor = true;
  }
  
  public boolean isButtonColumnEditor() {
    return isButtonColumnEditor;
  }
  
  public void mouseReleased(MouseEvent e) {
    if ((isButtonColumnEditor) && (table.isEditing())) {
      table.getCellEditor().stopCellEditing();
    }
    isButtonColumnEditor = false;
  }
  
  public void mouseClicked(MouseEvent e) {}
  
  public void mouseEntered(MouseEvent e) {}
  
  public void mouseExited(MouseEvent e) {}
}
