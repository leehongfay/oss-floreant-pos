package com.floreantpos.swing.time;

import com.floreantpos.swing.POSComboBox;
import java.awt.Component;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;



public class TimeComboBox
{
  public TimeComboBox() {}
  
  public JComboBox getDefaultTimeComboBox()
  {
    JComboBox timeComboBox = getTimeComboBox(0, 0, 23, 59, 30);
    return timeComboBox;
  }
  
  public POSComboBox getDefaultTimePosComboBox() {
    POSComboBox timeComboBox = getTimePosComboBox(0, 0, 23, 59, 15);
    return timeComboBox;
  }
  
  public JComboBox getTimeComboBox(int startHour, int startMin, int endHour, int endMin, int timeGap) {
    DefaultComboBoxModel<Date> model = setDateAndTime(startHour, startMin, endHour, endMin, timeGap);
    
    JComboBox<Date> cb = new JComboBox(model);
    cb.setRenderer(new DateFormattedListCellRenderer(new SimpleDateFormat("hh:mm a")));
    return cb;
  }
  
  public POSComboBox getTimePosComboBox(int startHour, int startMin, int endHour, int endMin, int timeGap) {
    DefaultComboBoxModel<Date> model = setDateAndTime(startHour, startMin, endHour, endMin, timeGap);
    
    POSComboBox cb = new POSComboBox();
    cb.setModel(model);
    cb.setRenderer(new DateFormattedListCellRenderer(new SimpleDateFormat("hh:mm a")));
    return cb;
  }
  
  private DefaultComboBoxModel<Date> setDateAndTime(int startHour, int startMin, int endHour, int endMin, int timeGap) {
    Calendar calendar = Calendar.getInstance();
    calendar.set(11, startHour);
    calendar.set(12, startMin);
    calendar.set(13, 0);
    calendar.set(14, 0);
    
    Calendar end = Calendar.getInstance();
    end.set(11, endHour);
    end.set(12, endMin);
    calendar.set(13, 59);
    calendar.set(14, 999);
    DefaultComboBoxModel<Date> model = new DefaultComboBoxModel();
    do {
      model.addElement(calendar.getTime());
      calendar.add(12, timeGap);
    } while (calendar.getTime().before(end.getTime()));
    return model;
  }
  
  public class DateFormattedListCellRenderer extends DefaultListCellRenderer
  {
    private DateFormat format;
    
    public DateFormattedListCellRenderer(DateFormat format) {
      this.format = format;
    }
    
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
      if ((value instanceof Date)) {
        value = format.format((Date)value);
      }
      return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
    }
  }
}
