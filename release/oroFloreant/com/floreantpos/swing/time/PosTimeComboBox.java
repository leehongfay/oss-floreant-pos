package com.floreantpos.swing.time;

import java.awt.Component;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;

public class PosTimeComboBox extends JComboBox
{
  public PosTimeComboBox()
  {
    DefaultComboBoxModel<Date> defaultComboBoxModel = setDateAndTime(0, 0, 23, 59, 15);
    DateFormattedListCellRenderer dateFormattedListCellRenderer = new DateFormattedListCellRenderer(new SimpleDateFormat("hh:mm a"));
    setRenderer(dateFormattedListCellRenderer);
    setModel(defaultComboBoxModel);
  }
  
  public PosTimeComboBox(int startHour, int startMin, int endHour, int endMin, int timeGap) {
    DefaultComboBoxModel<Date> defaultComboBoxModel = setDateAndTime(startHour, startMin, endHour, endMin, timeGap);
    DateFormattedListCellRenderer dateFormattedListCellRenderer = new DateFormattedListCellRenderer(new SimpleDateFormat("hh:mm a"));
    setRenderer(dateFormattedListCellRenderer);
    setModel(defaultComboBoxModel);
  }
  
  public DefaultComboBoxModel<Date> setDateAndTime(int startHour, int startMin, int endHour, int endMin, int timeGap) {
    Calendar calendar = Calendar.getInstance();
    calendar.set(11, startHour);
    calendar.set(12, startMin);
    
    Calendar end = Calendar.getInstance();
    end.set(11, endHour);
    end.set(12, endMin);
    DefaultComboBoxModel<Date> model = new DefaultComboBoxModel();
    model.addElement(null);
    do {
      model.addElement(calendar.getTime());
      calendar.add(12, timeGap);
    } while (calendar.getTime().before(end.getTime()));
    return model;
  }
  
  public class DateFormattedListCellRenderer extends DefaultListCellRenderer
  {
    private DateFormat format;
    
    public DateFormattedListCellRenderer(DateFormat format) {
      this.format = format;
    }
    
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
      if ((value instanceof Date)) {
        value = format.format((Date)value);
      }
      return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
    }
  }
}
