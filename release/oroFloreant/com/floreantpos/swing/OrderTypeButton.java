package com.floreantpos.swing;

import com.floreantpos.Messages;
import com.floreantpos.PosLog;
import com.floreantpos.actions.NewBarTabAction;
import com.floreantpos.customer.CustomerSelectorDialog;
import com.floreantpos.customer.CustomerSelectorFactory;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.extension.OrderServiceFactory;
import com.floreantpos.main.Application;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.UserType;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.tableselection.TableSelectorDialog;
import com.floreantpos.ui.tableselection.TableSelectorFactory;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.util.TicketAlreadyExistsException;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.Set;
import javax.swing.ImageIcon;

















public class OrderTypeButton
  extends PosButton
  implements ActionListener
{
  private OrderType orderType;
  
  public OrderTypeButton()
  {
    super("");
  }
  
  private Image getScaledImage(Image srcImg, int w, int h)
  {
    BufferedImage resizedImg = new BufferedImage(w, h, 2);
    Graphics2D g2 = resizedImg.createGraphics();
    g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    g2.drawImage(srcImg, 0, 0, w, h, null);
    g2.dispose();
    
    return resizedImg;
  }
  
  public OrderTypeButton(OrderType orderType)
  {
    this.orderType = orderType;
    if (orderType != null) {
      ImageIcon imgIcon = orderType.getImage();
      if (imgIcon != null) {
        Image image = imgIcon.getImage();
        if (orderType.isShowImageOnly().booleanValue()) {
          setIcon(new ImageIcon(getScaledImage(image, 80, 60)));
        }
        else {
          setHorizontalTextPosition(0);
          setVerticalTextPosition(3);
          setText(orderType.name());
          setBackground(orderType.getButtonColor());
          setForeground(orderType.getTextColor());
          setIcon(new ImageIcon(getScaledImage(image, 80, 60)));
        }
      } else {
        setHorizontalTextPosition(0);
        setVerticalTextPosition(3);
        setText(orderType.name());
        setBackground(orderType.getButtonColor());
        setForeground(orderType.getTextColor());
      }
    }
    addActionListener(this);
  }
  
  public void actionPerformed(ActionEvent e)
  {
    if (!hasPermission()) {
      POSMessageDialog.showError(Messages.getString("OrderTypeButton.1"));
      return;
    }
    try
    {
      if (orderType.isBarTab().booleanValue()) {
        new NewBarTabAction(orderType, null, Application.getPosWindow()).actionPerformed(e);
      } else if (orderType.isRetailOrder().booleanValue()) {
        try {
          OrderServiceFactory.getOrderService().createNewTicket(orderType, null, null);
          RootView.getInstance().showView("ORDER_VIEW");
        } catch (TicketAlreadyExistsException e1) {
          PosLog.error(getClass(), e1);
        }
      } else if (orderType.isShowTableSelection().booleanValue()) {
        TableSelectorDialog dialog = TableSelectorFactory.createTableSelectorDialog(orderType);
        dialog.setCreateNewTicket(true);
        dialog.updateView(true);
        dialog.openUndecoratedFullScreen();
        
        if (!dialog.isCanceled()) {
          return;
        }
      } else if (orderType.isRequiredCustomerData().booleanValue()) {
        CustomerSelectorDialog dialog = CustomerSelectorFactory.createCustomerSelectorDialog(orderType);
        dialog.setCreateNewTicket(true);
        dialog.updateView(true);
        dialog.openUndecoratedFullScreen();
        
        if (!dialog.isCanceled()) {
          return;
        }
      } else {
        OrderServiceFactory.getOrderService().createNewTicket(orderType, null, null);
      }
    } catch (Exception e2) {
      POSMessageDialog.showError(getParent(), e2.getMessage(), e2);
    }
  }
  
  private boolean hasPermission() {
    User user = Application.getCurrentUser();
    UserType userType = user.getType();
    if (userType != null) {
      Set<UserPermission> permissions = userType.getPermissions();
      for (UserPermission permission : permissions) {
        if (permission.equals(UserPermission.CREATE_TICKET)) {
          return true;
        }
      }
    }
    return false;
  }
}
