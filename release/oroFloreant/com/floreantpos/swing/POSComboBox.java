package com.floreantpos.swing;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.basic.ComboPopup;

public class POSComboBox extends JComboBox implements PopupMenuListener
{
  public POSComboBox()
  {
    setHeight(60);
    setFont(getFont().deriveFont(14.0F));
    addPopupMenuListener(this);
  }
  
  public POSComboBox(Object[] items) {
    super(items);
    setHeight(60);
    setFont(getFont().deriveFont(18.0F));
    addPopupMenuListener(this);
  }
  
  public void setHeight(int height) {
    setMinimumSize(PosUIManager.getSize(60, 40));
    Object popup = getUI().getAccessibleChild(this, 0);
    if ((popup instanceof ComboPopup)) {
      JList jlist = ((ComboPopup)popup).getList();
      jlist.setFixedCellHeight(PosUIManager.getSize(height));
    }
  }
  
  public POSComboBox(Vector items)
  {
    super(items);
    setHeight(60);
    setFont(getFont().deriveFont(18.0F));
  }
  

  public void popupMenuWillBecomeVisible(PopupMenuEvent e)
  {
    JComboBox comboBox = (JComboBox)e.getSource();
    Object popup = comboBox.getUI().getAccessibleChild(comboBox, 0);
    Component c = ((Container)popup).getComponent(0);
    JComponent popupComponent = (JComponent)popup;
    if (popupComponent.isPreferredSizeSet()) {
      return;
    }
    
    if ((c instanceof JScrollPane)) {
      JScrollPane scrollpane = (JScrollPane)c;
      JScrollBar scrollBar = scrollpane.getVerticalScrollBar();
      scrollBar.setPreferredSize(PosUIManager.getSize(50, getPreferredSizeheight));
    }
    if ((popup instanceof ComboPopup)) {
      Dimension comboboxSize = getSize();
      popupComponent.setPreferredSize(PosUIManager.getSize(width, 300));
      popupComponent.setLayout(new java.awt.GridLayout(1, 1));
    }
  }
  
  public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {}
  
  public void popupMenuCanceled(PopupMenuEvent e) {}
}
