package com.floreantpos.swing;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import javax.swing.JPanel;
import javax.swing.Scrollable;

public class ScrollableFlowPanel extends JPanel implements Scrollable
{
  private ScrollableFlowLayout layout;
  private JPanel contentPane;
  
  public ScrollableFlowPanel()
  {
    this(1);
  }
  
  public ScrollableFlowPanel(int alignment) {
    super(new BorderLayout());
    
    layout = new ScrollableFlowLayout(alignment);
    contentPane = new JPanel(layout);
    
    super.add(contentPane);
  }
  
  public Component add(Component comp)
  {
    return contentPane.add(comp);
  }
  
  public Dimension getPreferredScrollableViewportSize() {
    return getPreferredSize();
  }
  
  public Dimension getPreferredSize()
  {
    Dimension preferredSize = super.getPreferredSize();
    height = layout.getLayoutHeight();
    
    return preferredSize;
  }
  
  public JPanel getContentPane() {
    return contentPane;
  }
  
  public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
    return 10;
  }
  
  public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
    return (orientation == 1 ? height : width) - 10;
  }
  
  public boolean getScrollableTracksViewportWidth() {
    return true;
  }
  
  public boolean getScrollableTracksViewportHeight() {
    return false;
  }
  
  class ScrollableFlowLayout extends FlowLayout
  {
    private int layoutHeight;
    
    public ScrollableFlowLayout() {}
    
    public ScrollableFlowLayout(int align) {
      super();
    }
    
    public ScrollableFlowLayout(int align, int hgap, int vgap) {
      super(hgap, vgap);
    }
    
















    private int moveComponents(Container target, int x, int y, int width, int height, int rowStart, int rowEnd, boolean ltr, boolean useBaseline, int[] ascent, int[] descent)
    {
      switch (getAlignment()) {
      case 0: 
        x += (ltr ? 0 : width);
        break;
      case 1: 
        x += width / 2;
        break;
      case 2: 
        x += (ltr ? width : 0);
        break;
      case 3: 
        break;
      case 4: 
        x += width;
      }
      
      int maxAscent = 0;
      int nonbaselineHeight = 0;
      int baselineOffset = 0;
      if (useBaseline) {
        int maxDescent = 0;
        for (int i = rowStart; i < rowEnd; i++) {
          Component m = target.getComponent(i);
          if (m.isVisible()) {
            if (ascent[i] >= 0) {
              maxAscent = Math.max(maxAscent, ascent[i]);
              maxDescent = Math.max(maxDescent, descent[i]);
            }
            else {
              nonbaselineHeight = Math.max(m.getHeight(), nonbaselineHeight);
            }
          }
        }
        height = Math.max(maxAscent + maxDescent, nonbaselineHeight);
        baselineOffset = (height - maxAscent - maxDescent) / 2;
      }
      for (int i = rowStart; i < rowEnd; i++) {
        Component m = target.getComponent(i);
        if (m.isVisible()) { int cy;
          int cy;
          if ((useBaseline) && (ascent[i] >= 0)) {
            cy = y + baselineOffset + maxAscent - ascent[i];
          }
          else {
            cy = y + (height - getSizeheight) / 2;
          }
          if (ltr) {
            m.setLocation(x, cy);
          }
          else {
            m.setLocation(getSizewidth - x - getSizewidth, cy);
          }
          x += getSizewidth + getHgap();
        }
      }
      return height;
    }
    










    public void layoutContainer(Container target)
    {
      synchronized (target.getTreeLock()) {
        layoutHeight = 0;
        Insets insets = target.getInsets();
        int maxwidth = getSizewidth - (left + right + getHgap() * 2);
        int nmembers = target.getComponentCount();
        int x = 0;int y = top + getVgap();
        int rowh = 0;int start = 0;
        
        boolean ltr = target.getComponentOrientation().isLeftToRight();
        
        boolean useBaseline = getAlignOnBaseline();
        int[] ascent = null;
        int[] descent = null;
        
        if (useBaseline) {
          ascent = new int[nmembers];
          descent = new int[nmembers];
        }
        
        for (int i = 0; i < nmembers; i++) {
          Component m = target.getComponent(i);
          if (m.isVisible()) {
            Dimension d = m.getPreferredSize();
            m.setSize(width, height);
            
            if (useBaseline) {
              int baseline = m.getBaseline(width, height);
              if (baseline >= 0) {
                ascent[i] = baseline;
                descent[i] = (height - baseline);
              }
              else {
                ascent[i] = -1;
              }
            }
            if ((x == 0) || (x + width <= maxwidth)) {
              if (x > 0) {
                x += getHgap();
              }
              x += width;
              rowh = Math.max(rowh, height);
            }
            else {
              rowh = moveComponents(target, left + getHgap(), y, maxwidth - x, rowh, start, i, ltr, useBaseline, ascent, descent);
              x = width;
              y += getVgap() + rowh;
              rowh = height;
              start = i;
              layoutHeight += rowh + getVgap();
            }
          }
        }
        layoutHeight += moveComponents(target, left + getHgap(), y, maxwidth - x, rowh, start, nmembers, ltr, useBaseline, ascent, descent);
        layoutHeight += getVgap() * 2;
      }
    }
    
    public int getLayoutHeight() {
      return layoutHeight;
    }
  }
}
