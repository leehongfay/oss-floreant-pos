package com.floreantpos.swing;

import com.floreantpos.Messages;
import com.floreantpos.main.Application;
import com.floreantpos.model.Store;
import com.floreantpos.model.dao.StoreDAO;
import java.util.Calendar;
import java.util.Vector;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;




















public class TimeSelector
  extends JPanel
{
  private JToggleButton tbAM;
  private JToggleButton tbPM;
  private POSComboBox cbHour;
  private POSComboBox cbMin;
  
  public TimeSelector()
  {
    setLayout(new MigLayout("", "[][grow][][grow][][]", "[]"));
    
    JLabel lblHour = new JLabel(Messages.getString("TimeSelector.0"));
    add(lblHour, "cell 0 0,alignx trailing");
    
    Vector<Integer> hours = new Vector();
    for (int i = 1; i <= 12; i++) {
      hours.add(Integer.valueOf(i));
    }
    cbHour = new POSComboBox(hours);
    add(cbHour, "cell 1 0,growy");
    
    JLabel lblMin = new JLabel(Messages.getString("TimeSelector.1"));
    add(lblMin, "cell 2 0,alignx trailing");
    
    Vector<Integer> minutes = new Vector();
    for (int i = 0; i <= 45; i += 15) {
      minutes.add(Integer.valueOf(i));
    }
    
    cbMin = new POSComboBox(minutes);
    add(cbMin, "cell 3 0,growy");
    
    ButtonGroup group = new ButtonGroup();
    group.add(tbAM);
    group.add(tbPM);
    
    tbAM = new JToggleButton(Messages.getString("TimeSelector.9"));
    add(tbAM, "cell 4 0,w 70!,grow");
    
    tbPM = new JToggleButton(Messages.getString("TimeSelector.11"));
    add(tbPM, "cell 5 0,w 70!,grow");
    
    ButtonGroup buttonGroup = new ButtonGroup();
    buttonGroup.add(tbAM);
    buttonGroup.add(tbPM);
    
    Calendar calendar = Calendar.getInstance();
    int hour = calendar.get(10);
    int min = calendar.get(12);
    int i = calendar.get(9);
    Store store = Application.getInstance().getStore();
    if (store == null) {
      return;
    }
    String strPreparationTime = store.getProperty("deliveryConfig.preperationTime");
    if (StringUtils.isEmpty(strPreparationTime)) {
      strPreparationTime = "40";
      store.addProperty("deliveryConfig.preperationTime", strPreparationTime);
      StoreDAO.getInstance().saveOrUpdate(store);
      Application.getInstance().refreshStore();
    }
    int preparationTime = Integer.valueOf(strPreparationTime).intValue();
    int calMin = min + preparationTime;
    if (calMin > 60) {
      hour++;
      min = calMin % 60;
    }
    else {
      min += calMin;
    }
    
    if (min <= 15) {
      min = 15;
    }
    else if (min <= 30) {
      min = 30;
    }
    else if (min <= 45) {
      min = 45;
    }
    else {
      hour++;
      min = 0;
    }
    setSelectedHour(hour);
    setSelectedMin(min);
    
    if ((hour > 11) || (hour == 0)) {
      if (i == 0) {
        tbPM.setSelected(true);
      }
      else {
        tbAM.setSelected(true);
      }
      
    }
    else if (i == 0) {
      tbAM.setSelected(true);
    }
    else {
      tbPM.setSelected(true);
    }
  }
  
  public int getSelectedHour()
  {
    int hour = ((Integer)cbHour.getSelectedItem()).intValue();
    return hour;
  }
  
  public void setSelectedHour(int hour) {
    if (hour == 0) {
      hour = 12;
    }
    cbHour.setSelectedItem(Integer.valueOf(hour));
  }
  
  public int getSelectedMin() {
    int minute = ((Integer)cbMin.getSelectedItem()).intValue();
    return minute;
  }
  
  public void setSelectedMin(int min) {
    cbMin.setSelectedItem(Integer.valueOf(min));
  }
  
  public int getAmPm() {
    if (tbAM.isSelected()) {
      return 0;
    }
    
    return 1;
  }
  
  public void setAmPm(int amPm)
  {
    if (amPm == 0) {
      tbAM.setSelected(true);
    }
    else {
      tbPM.setSelected(true);
    }
  }
  
  public void setEnable(boolean enable) {
    cbHour.setEnabled(enable);
    cbMin.setEnabled(enable);
    tbAM.setEnabled(enable);
    tbPM.setEnabled(enable);
  }
}
