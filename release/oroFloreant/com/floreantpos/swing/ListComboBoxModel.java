package com.floreantpos.swing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.MutableComboBoxModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
















public class ListComboBoxModel
  extends AbstractListModel
  implements MutableComboBoxModel, Serializable, ListDataListener
{
  private List dataList;
  private Object selectedObject;
  
  public ListComboBoxModel()
  {
    this(new ArrayList());
  }
  
  public ListComboBoxModel(List list) {
    setDataList(list);
    
    if (getSize() > 0) {
      selectedObject = getElementAt(0);
    }
  }
  
  public void setDataList(List list) {
    dataList = list;
  }
  





  public void setSelectedItem(Object anObject)
  {
    if (((selectedObject != null) && (!selectedObject.equals(anObject))) || ((selectedObject == null) && (anObject != null))) {
      selectedObject = anObject;
      fireContentsChanged(this, -1, -1);
    }
  }
  
  public Object getSelectedItem()
  {
    return selectedObject;
  }
  
  public int getSize()
  {
    return dataList.size();
  }
  
  public Object getElementAt(int index)
  {
    if ((index >= 0) && (index < dataList.size())) {
      return dataList.get(index);
    }
    return null;
  }
  






  public int getIndexOf(Object anObject)
  {
    return dataList.indexOf(anObject);
  }
  
  public void addElement(Object anObject)
  {
    dataList.add(anObject);
    fireIntervalAdded(this, dataList.size() - 1, dataList.size() - 1);
    if ((dataList.size() == 1) && (selectedObject == null) && (anObject != null)) {
      setSelectedItem(anObject);
    }
  }
  
  public void insertElementAt(Object anObject, int index)
  {
    dataList.add(index, anObject);
    fireIntervalAdded(this, index, index);
  }
  
  public void removeElementAt(int index)
  {
    if (getElementAt(index) == selectedObject) {
      if (index == 0) {
        setSelectedItem(getSize() == 1 ? null : getElementAt(index + 1));
      } else {
        setSelectedItem(getElementAt(index - 1));
      }
    }
    
    dataList.remove(index);
    
    fireIntervalRemoved(this, index, index);
  }
  
  public void removeElement(Object anObject)
  {
    int index = dataList.indexOf(anObject);
    if (index != -1) {
      removeElementAt(index);
    }
  }
  


  public void removeAllElements()
  {
    if (dataList.size() > 0) {
      int firstIndex = 0;
      int lastIndex = dataList.size() - 1;
      dataList.clear();
      selectedObject = null;
      fireIntervalRemoved(this, firstIndex, lastIndex);
    } else {
      selectedObject = null;
    }
  }
  
  public void intervalAdded(ListDataEvent e) {
    int index0 = e.getIndex0();
    int index1 = e.getIndex1();
    fireIntervalAdded(this, index0, index1);
  }
  
  public void intervalRemoved(ListDataEvent e) {
    int index0 = e.getIndex0();
    int index1 = e.getIndex1();
    fireIntervalRemoved(this, index0, index1);
  }
  
  public void contentsChanged(ListDataEvent e) {
    int index0 = e.getIndex0();
    int index1 = e.getIndex1();
    fireContentsChanged(this, index0, index1);
  }
}
