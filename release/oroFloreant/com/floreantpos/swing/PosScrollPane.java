package com.floreantpos.swing;

import java.awt.Component;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

















public class PosScrollPane
  extends JScrollPane
{
  public PosScrollPane()
  {
    JScrollBar scrollBar = getVerticalScrollBar();
    if (scrollBar != null) {
      scrollBar.setPreferredSize(PosUIManager.getSize(40, 40));
      scrollBar.setUnitIncrement(25);
    }
  }
  
  public PosScrollPane(Component view) {
    super(view);
    
    JScrollBar scrollBar = getVerticalScrollBar();
    if (scrollBar != null) {
      scrollBar.setPreferredSize(PosUIManager.getSize(40, 40));
      scrollBar.setUnitIncrement(25);
    }
  }
  
  public PosScrollPane(int vsbPolicy, int hsbPolicy) {
    super(vsbPolicy, hsbPolicy);
    
    JScrollBar scrollBar = getVerticalScrollBar();
    if (scrollBar != null) {
      scrollBar.setPreferredSize(PosUIManager.getSize(40, 40));
      scrollBar.setUnitIncrement(25);
    }
  }
  
  public PosScrollPane(Component view, int vsbPolicy, int hsbPolicy) {
    super(view, vsbPolicy, hsbPolicy);
    
    JScrollBar scrollBar = getVerticalScrollBar();
    if (scrollBar != null) {
      scrollBar.setPreferredSize(PosUIManager.getSize(40, 40));
      scrollBar.setUnitIncrement(25);
    }
  }
}
