package com.floreantpos.swing;









public class FixedLengthTextField
  extends FocusedTextField
{
  private FixedLengthDocument fixedLengthDocument;
  








  public FixedLengthTextField()
  {
    this(30);
  }
  
  public FixedLengthTextField(int length) {
    super(length);
    
    fixedLengthDocument = new FixedLengthDocument(length);
    setDocument(fixedLengthDocument);
  }
  
  public FixedLengthTextField(int length, int documentLength) {
    super(length);
    
    fixedLengthDocument = new FixedLengthDocument(documentLength);
    setDocument(fixedLengthDocument);
  }
  
  public int getLength() {
    return fixedLengthDocument.getLength();
  }
  
  public void setLength(int length) {
    fixedLengthDocument.setMaximumLength(length);
  }
}
