package com.floreantpos.swing;

import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.table.TableModel;
import org.jdesktop.swingx.JXTable;




public class PosTable
  extends JXTable
{
  public PosTable() {}
  
  public PosTable(BeanTableModel model)
  {
    super(model);
    model.initTableRenderer(this);
  }
  
  public void changeSelection(int row, int column, boolean toggle, boolean extend)
  {
    super.changeSelection(row, column, toggle, extend);
    
    TableModel model = super.getModel();
    if (!(model instanceof BeanTableModel)) {
      return;
    }
    BeanTableModel beanTableModel = (BeanTableModel)model;
    BeanTableModel.BeanColumn beanColumn = beanTableModel.getColumn(column);
    if (beanColumn.isEditable()) {
      editCellAt(row, column);
      DefaultCellEditor editor = (DefaultCellEditor)getCellEditor(row, column);
      JTextField textField = (JTextField)editor.getComponent();
      textField.requestFocus();
      textField.selectAll();
    }
  }
}
