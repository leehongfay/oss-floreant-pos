package com.floreantpos.swing;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.swing.event.KeypadEvent;
import com.floreantpos.swing.event.KeypadEventListener;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.event.EventListenerList;
import javax.swing.text.JTextComponent;



















public class NumericKeypad
  extends JComponent
{
  private static final String CLEAR = "CLEAR";
  private final EventListenerList eventListeners = new EventListenerList();
  private final String text = "";
  private KeypadEvent keypadEvent = null;
  private boolean isProtected = false;
  private JPanel keypadPanel;
  
  public NumericKeypad() {
    initComponents();
  }
  
  public synchronized void removeKeypadEventListener(KeypadEventListener listener) {
    eventListeners.remove(KeypadEventListener.class, listener);
  }
  
  public synchronized void addKeypadEventListener(KeypadEventListener listener) {
    eventListeners.add(KeypadEventListener.class, listener);
  }
  
  protected synchronized void fireKeypadEvent(int eventId) {
    Object[] listeners = eventListeners.getListenerList();
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == KeypadEventListener.class) {
        keypadEvent = new KeypadEvent(this, eventId);
        ((KeypadEventListener)listeners[(i + 1)]).receiveKeypadEvent(keypadEvent);
      }
    }
  }
  

  private PosButton posButton0;
  private PosButton posButton1;
  private PosButton btnClear;
  private PosButton posButton2;
  private PosButton posButton3;
  private void initComponents()
  {
    keypadPanel = new JPanel();
    posButton7 = new PosButton();
    posButton7.setFocusable(false);
    posButton8 = new PosButton();
    posButton8.setFocusable(false);
    posButton9 = new PosButton();
    posButton9.setFocusable(false);
    posButton4 = new PosButton();
    posButton4.setFocusable(false);
    posButton5 = new PosButton();
    posButton5.setFocusable(false);
    posButton6 = new PosButton();
    posButton6.setFocusable(false);
    posButton1 = new PosButton();
    posButton1.setFocusable(false);
    posButton2 = new PosButton();
    posButton2.setFocusable(false);
    posButton3 = new PosButton();
    posButton3.setFocusable(false);
    posButton0 = new PosButton();
    posButton0.setFocusable(false);
    
    keypadPanel.setLayout(new GridLayout(4, 3, 5, 5));
    
    posButton7.setAction(goAction);
    posButton7.setIcon(IconFactory.getIcon("7.png"));
    posButton7.setActionCommand("7");
    keypadPanel.add(posButton7);
    
    posButton8.setAction(goAction);
    posButton8.setIcon(IconFactory.getIcon("8.png"));
    posButton8.setActionCommand("8");
    keypadPanel.add(posButton8);
    
    posButton9.setAction(goAction);
    posButton9.setIcon(IconFactory.getIcon("9.png"));
    posButton9.setActionCommand("9");
    keypadPanel.add(posButton9);
    
    posButton4.setAction(goAction);
    posButton4.setIcon(IconFactory.getIcon("4.png"));
    posButton4.setActionCommand("4");
    keypadPanel.add(posButton4);
    
    posButton5.setAction(goAction);
    posButton5.setIcon(IconFactory.getIcon("5.png"));
    posButton5.setActionCommand("5");
    keypadPanel.add(posButton5);
    
    posButton6.setAction(goAction);
    posButton6.setIcon(IconFactory.getIcon("6.png"));
    posButton6.setActionCommand("6");
    keypadPanel.add(posButton6);
    
    posButton1.setAction(goAction);
    posButton1.setIcon(IconFactory.getIcon("1.png"));
    posButton1.setActionCommand("1");
    keypadPanel.add(posButton1);
    
    posButton2.setAction(goAction);
    posButton2.setIcon(IconFactory.getIcon("2.png"));
    posButton2.setActionCommand("2");
    keypadPanel.add(posButton2);
    
    posButton3.setAction(goAction);
    posButton3.setIcon(IconFactory.getIcon("3.png"));
    posButton3.setActionCommand("3");
    keypadPanel.add(posButton3);
    
    btnDot = new PosButton();
    btnDot.setFocusable(false);
    btnDot.setAction(goAction);
    btnDot.setActionCommand(".");
    btnDot.setIcon(IconFactory.getIcon("dot.png"));
    keypadPanel.add(btnDot);
    
    posButton0.setAction(goAction);
    posButton0.setIcon(IconFactory.getIcon("0.png"));
    posButton0.setActionCommand("0");
    keypadPanel.add(posButton0);
    setLayout(new BorderLayout(0, 0));
    btnClear = new PosButton();
    btnClear.setFocusable(false);
    keypadPanel.add(btnClear);
    
    btnClear.setAction(goAction);
    btnClear.setIcon(IconFactory.getIcon("clear.png"));
    btnClear.setText(Messages.getString("NumericKeypad.0"));
    btnClear.setActionCommand("CLEAR");
    add(keypadPanel, "Center");
  }
  


  private PosButton posButton4;
  
  private PosButton posButton5;
  
  private PosButton posButton6;
  
  private PosButton posButton7;
  
  private PosButton posButton8;
  
  private PosButton posButton9;
  
  Action goAction = new AbstractAction()
  {
    public void actionPerformed(ActionEvent e) {
      Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
      JTextComponent focusedTextComponent = null;
      
      if (!(focusOwner instanceof JTextComponent)) {
        return;
      }
      
      focusedTextComponent = (JTextComponent)focusOwner;
      String command = e.getActionCommand();
      
      if ("CLEAR".equals(command)) {
        focusedTextComponent.setText("");
      }
      else {
        focusedTextComponent.replaceSelection(command);
      }
    }
  };
  private PosButton btnDot;
  
  public String getText() {
    return "";
  }
  
  public void setProtected(boolean isProtected) {
    this.isProtected = isProtected;
  }
  
  public boolean isProtected() {
    return isProtected;
  }
  
  public static void main(String[] args) {
    JPanel p = new JPanel(new BorderLayout());
    p.add(new NumericKeypad());
    JFrame frame = new JFrame();
    frame.getContentPane().add(p);
    frame.pack();
    frame.setDefaultCloseOperation(3);
    frame.setVisible(true);
  }
}
