package com.floreantpos.swing;

import com.floreantpos.POSConstants;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.UserType;
import com.floreantpos.ui.views.LoginView;
import com.floreantpos.util.POSUtil;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;
import javax.swing.ImageIcon;

















public class OrderTypeLoginButton
  extends PosButton
  implements ActionListener
{
  private OrderType orderType;
  
  public OrderTypeLoginButton()
  {
    super("");
  }
  
  public OrderTypeLoginButton(OrderType orderType)
  {
    this.orderType = orderType;
    if (orderType != null)
    {
      ImageIcon imgIcon = orderType.getImage();
      if (imgIcon != null) {
        Image image = imgIcon.getImage();
        
        if (orderType.isShowImageOnly().booleanValue()) {
          setIcon(new ImageIcon(POSUtil.getScaledImage(image, 80, 60)));
        }
        else {
          setHorizontalTextPosition(0);
          setVerticalTextPosition(3);
          setIcon(new ImageIcon(POSUtil.getScaledImage(image, 80, 60)));
          setBackground(orderType.getButtonColor());
          setForeground(orderType.getTextColor());
          setText(orderType.name());
        }
      }
      else {
        setText(orderType.name());
        setBackground(orderType.getButtonColor());
        setForeground(orderType.getTextColor());
      }
    }
    else
    {
      setText(POSConstants.TAKE_OUT_BUTTON_TEXT);
    }
    addActionListener(this);
  }
  
  public void actionPerformed(ActionEvent e)
  {
    TerminalConfig.setDefaultView(orderType.getName());
    LoginView.getInstance().doLogin();
  }
  
  private boolean hasPermission() {
    User user = Application.getCurrentUser();
    UserType userType = user.getType();
    if (userType != null) {
      Set<UserPermission> permissions = userType.getPermissions();
      for (UserPermission permission : permissions) {
        if (permission.equals(UserPermission.CREATE_TICKET)) {
          return true;
        }
      }
    }
    return false;
  }
}
