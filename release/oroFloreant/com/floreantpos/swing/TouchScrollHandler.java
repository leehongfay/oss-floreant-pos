package com.floreantpos.swing;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.AWTEventListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComponent;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;

public class TouchScrollHandler
  extends MouseAdapter implements AWTEventListener
{
  private Point origin;
  private boolean wasDragging;
  
  public TouchScrollHandler() {}
  
  public void mousePressed(MouseEvent e)
  {
    origin = new Point(e.getPoint());
  }
  
  public void mouseReleased(MouseEvent e)
  {
    if (wasDragging) {
      e.getComponent().setCursor(Cursor.getDefaultCursor());
      wasDragging = false;
    }
  }
  
  public void mouseDragged(MouseEvent e)
  {
    if (origin != null) {
      JViewport viewPort = (JViewport)SwingUtilities.getAncestorOfClass(JViewport.class, e.getComponent());
      if (viewPort != null)
      {

        Rectangle view = viewPort.getViewRect();
        



        int deltaX = origin.x - e.getX();
        int deltaY = origin.y - e.getY();
        
        x += deltaX;
        y += deltaY;
        
        e.getComponent().setCursor(Cursor.getPredefinedCursor(13));
        ((JComponent)e.getComponent()).scrollRectToVisible(view);
        wasDragging = true;
        e.consume();
      }
    }
  }
  

  public void eventDispatched(AWTEvent event)
  {
    switch (event.getID()) {
    case 501: 
      mousePressed((MouseEvent)event);
      break;
    
    case 502: 
      mouseReleased((MouseEvent)event);
      break;
    
    case 506: 
      mouseDragged((MouseEvent)event);
      break;
    }
  }
}
