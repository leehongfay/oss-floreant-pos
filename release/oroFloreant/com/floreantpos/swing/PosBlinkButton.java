package com.floreantpos.swing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.Timer;
















public class PosBlinkButton
  extends PosButton
  implements ActionListener
{
  private boolean blinking = false;
  private boolean blinked = false;
  
  private final Color originalBackColor = getBackground();
  private final Color originalForeColor = getForeground();
  private boolean originalOpaque = isOpaque();
  private Color blinkColor = Color.YELLOW;
  
  private Color blinkDimColor;
  private boolean blinkText = false;
  private int[] blinkRate = { 13, 8, 5, 3, 2, 1 };
  private int blinkRateIndex = 0;
  private Timer blinkTimer = new Timer(250, this);
  
  private int numOfBlink = 10;
  private int blinkCount;
  
  public PosBlinkButton()
  {
    addActionListener(this);
  }
  
  public PosBlinkButton(ImageIcon imageIcon) {
    super(imageIcon);
    addActionListener(this);
  }
  
  public PosBlinkButton(String text) {
    super(text);
    addActionListener(this);
  }
  
  public boolean isBlinking() {
    return blinking;
  }
  
  public void setBlinking(boolean blinking) {
    if ((blinking) && (this.blinking == blinking)) {
      return;
    }
    this.blinking = blinking;
    blinkCount = 0;
    blinkTimer.stop();
    
    if (blinking) {
      blinkRateIndex = 0;
      blinkTimer.setDelay(1000 / blinkRate[(blinkRateIndex++)]);
      









      blinkTimer.restart();
    }
    else
    {
      blinkRateIndex = 0;
      setOpaque(originalOpaque);
      setForeground(originalForeColor);
      setBackground(originalBackColor);
    }
  }
  
  public void actionPerformed(ActionEvent e)
  {
    if (e.getSource() == this) {
      setBlinking(false);
      return;
    }
    blinked = (!blinked);
    blinkCount += 1;
    if (blinkText) {
      blinkForeGround();
    }
    else {
      blinkBackground();
    }
    if ((numOfBlink > 0) && (blinkCount > numOfBlink)) {
      stopBlinking();
    }
  }
  
  private void stopBlinking() {
    blinkTimer.stop();
    blinking = false;
    blinkRateIndex = 0;
    if (blinkText) {
      setForeground(blinkColor);
    }
    else {
      setBackground(blinkColor);
    }
  }
  
  public void setNumOfBlink(int blinkTime) {
    numOfBlink = blinkTime;
  }
  
  private void blinkBackground() {
    setOpaque(true);
    if (blinked) {
      setBackground(blinkColor.darker());
    }
    else {
      setBackground(blinkColor);
    }
  }
  
  private void blinkForeGround() {
    if (blinked) {
      setForeground(blinkDimColor == null ? blinkColor.darker() : blinkDimColor);
    }
    else {
      setForeground(blinkColor);
    }
  }
  
  public Color getBlinkColor() {
    return blinkColor;
  }
  
  public void setBlinkColor(Color blinkColor) {
    if (blinkColor != null) {
      this.blinkColor = blinkColor;
    }
  }
  
  public boolean isBlinkText() {
    return blinkText;
  }
  
  public void setBlinkText(boolean blinkText) {
    this.blinkText = blinkText;
  }
  
  public void setOriginalOpaque(boolean originalOpaque) {
    this.originalOpaque = originalOpaque;
    setOpaque(originalOpaque);
  }
  
  public Color getBlinkDimColor() {
    return blinkDimColor;
  }
  
  public void setBlinkDimColor(Color blinkDimColor) {
    this.blinkDimColor = blinkDimColor;
  }
}
