package com.floreantpos.swing;

import com.floreantpos.ui.TitlePanel;
import java.awt.BorderLayout;
import javax.swing.JPanel;

















public class TitledView
  extends JPanel
{
  private TitlePanel titlePanel;
  private JPanel contentPane;
  
  public TitledView()
  {
    this("");
  }
  
  public TitledView(String title) {
    titlePanel = new TitlePanel();
    contentPane = new JPanel();
    
    setLayout(new BorderLayout());
    
    add(titlePanel, "North");
    add(contentPane);
    
    setTitle(title);
  }
  
  public void setTitle(String title) {
    titlePanel.setTitle(title);
  }
  
  public String getTitle() {
    return titlePanel.getTitle();
  }
  
  public void setTitlePaneVisible(boolean visible) {
    titlePanel.setVisible(visible);
  }
  
  public boolean isTitlePaneVisible() {
    return titlePanel.isVisible();
  }
  
  public JPanel getContentPane() {
    return contentPane;
  }
}
