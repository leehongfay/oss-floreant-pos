package com.floreantpos.swing;

import com.floreantpos.Messages;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.main.Application;
import com.floreantpos.model.ShopFloor;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.ShopTableStatus;
import com.floreantpos.model.TableStatus;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.services.TicketService;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.PasswordEntryDialog;
import java.awt.Color;
import java.util.Date;
import javax.swing.BorderFactory;
import org.apache.commons.lang.StringUtils;

















public class ShopTableButton
  extends PosButton
{
  private ShopTable shopTable;
  private User user;
  private Ticket ticket;
  int pressedX;
  int pressedY;
  
  public ShopTableButton(ShopTable shopTable)
  {
    this.shopTable = shopTable;
    setBorder(BorderFactory.createLineBorder(new Color(150, 0, 0, 127), 1, true));
    setBounds(shopTable.getX().intValue(), shopTable.getY().intValue(), TerminalConfig.getFloorButtonWidth(), TerminalConfig.getFloorButtonHeight());
    setBackground(shopTable.getFloor().getForegroundColor());
    update();
  }
  
  public int getId() {
    return shopTable.getId().intValue();
  }
  
  public void setShopTable(ShopTable shopTable) {
    this.shopTable = shopTable;
  }
  
  public ShopTable getShopTable() {
    return shopTable;
  }
  
  public String getTicketId() {
    return shopTable.getTicketId();
  }
  
  public boolean equals(Object obj)
  {
    if (!(obj instanceof ShopTableButton)) {
      return false;
    }
    
    ShopTableButton that = (ShopTableButton)obj;
    
    return shopTable.equals(shopTable);
  }
  
  public int hashCode()
  {
    return shopTable.hashCode();
  }
  
  public String toString()
  {
    return shopTable.toString();
  }
  
  public void update() {
    Terminal terminal = Application.getInstance().getTerminal();
    

    int priSize = terminal.getProperty("floorplan.primaryFontSize") == null ? 12 : Integer.valueOf(terminal.getProperty("floorplan.primaryFontSize")).intValue();
    
    int secSize = terminal.getProperty("floorplan.secondaryFontSize") == null ? 2 : Integer.valueOf(terminal.getProperty("floorplan.secondaryFontSize")).intValue();
    

    Color seatForeColor = StringUtils.isEmpty(terminal.getProperty("floorplan.seatForeColor")) ? null : new Color(Integer.parseInt(terminal.getProperty("floorplan.seatForeColor")));
    
    Color seatBgColor = StringUtils.isEmpty(terminal.getProperty("floorplan.seatBGColor")) ? null : new Color(Integer.parseInt(terminal.getProperty("floorplan.seatBGColor")));
    
    Color bookForeColor = StringUtils.isEmpty(terminal.getProperty("floorplan.bookForeColor")) ? null : new Color(Integer.parseInt(terminal.getProperty("floorplan.bookForeColor")));
    
    Color bookBgColor = StringUtils.isEmpty(terminal.getProperty("floorplan.bookBGColor")) ? null : new Color(Integer.parseInt(terminal.getProperty("floorplan.bookBGColor")));
    
    Color freeForeColor = StringUtils.isEmpty(terminal.getProperty("floorplan.freeForeColor")) ? null : new Color(Integer.parseInt(terminal.getProperty("floorplan.freeForeColor")));
    
    Color freeBgColor = StringUtils.isEmpty(terminal.getProperty("floorplan.freeBGColor")) ? null : new Color(Integer.parseInt(terminal.getProperty("floorplan.freeBGColor")));
    
    if ((shopTable.getTableStatus() == null) || (shopTable == null)) {
      setEnabled(true);
      setBackground(freeBgColor != null ? freeBgColor : Color.white);
      setForeground(freeForeColor != null ? freeForeColor : Color.black);
    }
    else if (shopTable.getTableStatus().equals(TableStatus.Seat)) {
      setBackground(seatBgColor != null ? seatBgColor : new Color(255, 102, 102));
      setForeground(seatForeColor != null ? seatForeColor : Color.BLACK);
    }
    else if (shopTable.getTableStatus().equals(TableStatus.Booked)) {
      setEnabled(false);
      setOpaque(true);
      setBackground(bookBgColor != null ? bookBgColor : Color.orange);
      setForeground(bookForeColor != null ? bookForeColor : Color.BLACK);
    }
    else {
      setEnabled(true);
      setBackground(freeBgColor != null ? freeBgColor : Color.white);
      setForeground(freeForeColor != null ? freeForeColor : Color.black);
    }
    String text = "<html><center>";
    
    text = text + "<font size=" + priSize + ">" + shopTable.getTableNumber() + "</font>";
    
    if ((!terminal.isShowTableNumber()) && (shopTable.getName() != null)) {
      text = "<html><center><font size=" + priSize + ">" + shopTable.getName() + "</font>";
    }
    
    String ticketId = shopTable.getTicketId();
    if ((ticketId != null) && (StringUtils.isNotEmpty(shopTable.getUserId()))) {
      if (terminal.isShowServerName()) {
        text = text + "<div><font size=" + secSize + ">" + shopTable.getUserName() + "</font></div>";
      }
      if (!shopTable.getUserId().toString().equals(Application.getCurrentUser().getId().toString())) {
        if (shopTable.getShopTableStatus().hasMultipleTickets()) {
          setBackground(seatBgColor != null ? seatBgColor : new Color(255, 102, 102));
          setForeground(seatForeColor != null ? seatForeColor : Color.BLACK);
        }
        else {
          setBackground(new Color(139, 0, 139));
          setForeground(Color.WHITE);
        }
      }
    }
    Date ticketCreateTime = shopTable.getTicketCreateTime();
    if ((ticketCreateTime != null) && (!shopTable.isShowStatus())) {
      text = text + "<div><font size=" + secSize + ">" + DateUtil.getElapsedTime(ticketCreateTime, new Date()) + "</font></div>";
    }
    else if (shopTable.isShowStatus()) {
      int availableCap = shopTable.getCapacity().intValue() - shopTable.getGuestNumber();
      text = text + "<div><font size=" + secSize + ">" + "Avail Cap: " + availableCap + "</font></div>";
      text = text + "<div><font size=" + secSize + ">" + "Total Cap: " + shopTable.getCapacity() + "</font></div>";
    }
    else if ((StringUtils.isNotEmpty(ticketId)) && (terminal.isShowTokenNum())) {
      text = text + "<div><font size=" + secSize + ">" + "#" + shopTable.getTicketShortId() + "</font></div>";
    }
    text = text + "</center></html>";
    
    setText(text);
  }
  
  public void setUser(User user)
  {
    if (user != null) {
      this.user = user;
    }
  }
  
  public User getUser() {
    return user;
  }
  
  public boolean hasUserAccess() {
    if (shopTable.getShopTableStatus().hasMultipleTickets()) {
      return true;
    }
    if (user == null) {
      return false;
    }
    User currentUser = Application.getCurrentUser();
    
    String currentUserId = currentUser.getId();
    String ticketUserId = user.getId();
    
    if (currentUserId.equals(ticketUserId)) {
      return true;
    }
    
    if ((currentUser.hasPermission(UserPermission.PERFORM_MANAGER_TASK)) || (currentUser.hasPermission(UserPermission.PERFORM_ADMINISTRATIVE_TASK))) {
      return true;
    }
    
    String password = PasswordEntryDialog.show(Application.getPosWindow(), Messages.getString("PosAction.0"));
    if (StringUtils.isEmpty(password)) {
      return false;
    }
    
    String inputUserId = UserDAO.getInstance().findUserBySecretKey(password).getId();
    if (inputUserId.equals(user.getId())) {
      POSMessageDialog.showError(Application.getPosWindow(), "Incorrect password");
      return false;
    }
    return true;
  }
  
  public void initializeUser() {
    String userId = shopTable.getUserId();
    if (StringUtils.isEmpty(userId)) {
      return;
    }
    if ((user != null) && (user.getId().equals(userId))) {
      return;
    }
    user = UserDAO.getInstance().get(shopTable.getUserId());
  }
  
  public void initializeTicket() {
    String ticketId = shopTable.getTicketId();
    if (StringUtils.isEmpty(ticketId)) {
      return;
    }
    if (shopTable.getTicketId() == null)
      return;
    if ((ticket != null) && (ticket.getId().equals(ticketId))) {
      return;
    }
    ticket = TicketService.getTicket(shopTable.getTicketId());
  }
  
  public Ticket getTicket()
  {
    return ticket;
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }
}
