package com.floreantpos.swing;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.plaf.basic.ComboPopup;

public class MultiSelectComboBox<E> extends JComboBox<E>
{
  private static final String ALL = "ALL";
  private boolean keepOpen;
  
  public MultiSelectComboBox()
  {
    initializeRenderer();
    setPreferredSize(new Dimension(200, 20));
  }
  
  public MultiSelectComboBox(List<E> items) {
    this();
    setItems(items);
  }
  
  public void setItems(List<E> itemList) {
    ComboBoxModel<MultiSelectComboBox<E>.CheckableItem> model = new ComboBoxModel();
    model.addElement(new CheckableItem("ALL", false));
    for (Object object : itemList) {
      MultiSelectComboBox<E>.CheckableItem checkableItem = new CheckableItem(object, false);
      model.addElement(checkableItem);
    }
    
    setModel(model);
  }
  
  public List<E> getSelectedItems() {
    ComboBoxModel m = (ComboBoxModel)getModel();
    List items = m.getDataList();
    List selectedItems = new ArrayList();
    for (Object object : items)
    {
      MultiSelectComboBox<E>.CheckableItem obj2 = (CheckableItem)object;
      if ((!(item instanceof String)) && (obj2.isSelected())) {
        selectedItems.add(item);
      }
    }
    
    return selectedItems.size() == 0 ? null : selectedItems;
  }
  
  private void initializeRenderer()
  {
    setRenderer(new CheckBoxCellRenderer(null));
    addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (e.getModifiers() == 16) {
          updateItem(getSelectedIndex());
          keepOpen = true;
        }
        
      }
    });
    getInputMap(0).put(KeyStroke.getKeyStroke(32, 0), "checkbox-select");
    getActionMap().put("checkbox-select", new AbstractAction()
    {
      public void actionPerformed(ActionEvent e) {
        Accessible a = getAccessibleContext().getAccessibleChild(0);
        if ((a instanceof ComboPopup)) {
          ComboPopup pop = (ComboPopup)a;
          updateItem(pop.getList().getSelectedIndex());
        }
      }
    });
  }
  
  public void updateItem(int index) {
    if (isPopupVisible()) {
      E item = getItemAt(index);
      MultiSelectComboBox<E>.CheckableItem items = (CheckableItem)item;
      
      selected ^= true;
      setSelectedIndex(-1);
      setSelectedItem(items);
      
      if (item.toString().equals("ALL")) {
        unselectAllItems();
        hidePopup();
      }
    }
  }
  
  public void unselectAllItems() {
    ComboBoxModel m = (ComboBoxModel)getModel();
    List dataList = m.getDataList();
    
    for (Object object : dataList) {
      if ((object instanceof CheckableItem)) {
        MultiSelectComboBox<E>.CheckableItem obj2 = (CheckableItem)object;
        selected = false;
      }
    }
  }
  
  public void setPopupVisible(boolean v)
  {
    if (keepOpen) {
      keepOpen = false;
    }
    else {
      super.setPopupVisible(v);
    }
  }
  
  private class CheckableItem {
    public Object item;
    public boolean selected;
    
    public CheckableItem(Object item, boolean selected) {
      this.item = item;
      this.selected = selected;
    }
    
    public String toString()
    {
      return item.toString();
    }
    
    public boolean isSelected() {
      return selected;
    }
  }
  
  private class CheckBoxCellRenderer implements ListCellRenderer {
    private final JLabel label = new JLabel(" ");
    private final JCheckBox check = new JCheckBox(" ");
    private final JLabel labelAll = new JLabel("ALL");
    
    private CheckBoxCellRenderer() {}
    
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) { MultiSelectComboBox<E>.CheckableItem item = (MultiSelectComboBox.CheckableItem)value;
      
      if ((item != null) && (item.toString().equals("ALL"))) {
        if (isSelected) {
          labelAll.setOpaque(true);
          labelAll.setBackground(list.getSelectionBackground());
          labelAll.setForeground(list.getSelectionForeground());
        }
        else {
          labelAll.setBackground(list.getBackground());
          labelAll.setForeground(list.getForeground());
        }
        return labelAll;
      }
      
      if (index < 0) {
        label.setText(getCheckedItemString(list.getModel()).replace("[", "").replace("]", ""));
        return label;
      }
      
      check.setText(java.util.Objects.toString(item, ""));
      check.setSelected(selected);
      if (isSelected) {
        check.setBackground(list.getSelectionBackground());
        check.setForeground(list.getSelectionForeground());
      }
      else {
        check.setBackground(list.getBackground());
        check.setForeground(list.getForeground());
      }
      
      return check;
    }
    
    private String getCheckedItemString(ListModel listModel)
    {
      List<String> sl = new ArrayList();
      for (int i = 0; i < listModel.getSize(); i++) {
        Object o = listModel.getElementAt(i);
        if (((o instanceof Object)) && (selected)) {
          sl.add(o.toString());
        }
      }
      return sl + "";
    }
  }
}
