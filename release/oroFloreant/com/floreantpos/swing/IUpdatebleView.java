package com.floreantpos.swing;

public abstract interface IUpdatebleView<E>
{
  public abstract void initView(E paramE);
  
  public abstract boolean updateModel(E paramE);
}
