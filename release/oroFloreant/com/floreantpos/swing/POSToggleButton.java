package com.floreantpos.swing;

import java.awt.Dimension;
import javax.swing.JToggleButton;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;





















public class POSToggleButton
  extends JToggleButton
{
  static
  {
    UIManager.put("POSToggleButtonUI", "com.floreantpos.swing.POSToggleButtonUI");
  }
  
  public POSToggleButton() {
    this(null);
  }
  
  public POSToggleButton(String text) {
    super(text);
    

    setFocusPainted(false);
    setFocusable(false);
  }
  

  public String getUIClassID()
  {
    return "POSToggleButtonUI";
  }
  
  public Dimension getPreferredSize()
  {
    Dimension size = null;
    
    if (isPreferredSizeSet()) {
      return super.getPreferredSize();
    }
    if (ui != null) {
      size = ui.getPreferredSize(this);
    }
    
    if (size == null) {
      size = new Dimension(PosUIManager.getSize(60, 45));
    }
    else {
      int width = width < 60 ? 60 : width;
      int height = height < 45 ? 45 : height;
      size.setSize(PosUIManager.getSize(width, height));
    }
    
    return size;
  }
}
