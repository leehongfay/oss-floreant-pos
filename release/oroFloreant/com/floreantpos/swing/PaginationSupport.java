package com.floreantpos.swing;

import java.util.List;

public abstract interface PaginationSupport
{
  public abstract int getNumRows();
  
  public abstract void setNumRows(int paramInt);
  
  public abstract int getCurrentRowIndex();
  
  public abstract void setCurrentRowIndex(int paramInt);
  
  public abstract int getPageSize();
  
  public abstract void setPageSize(int paramInt);
  
  public abstract boolean hasNext();
  
  public abstract boolean hasPrevious();
  
  public abstract int getNextRowIndex();
  
  public abstract int getPreviousRowIndex();
  
  public abstract void setRows(List paramList);
}
