package com.floreantpos.swing;

import com.floreantpos.POSConstants;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;



public abstract class ProgressDialog
  extends POSDialog
  implements ProgressObserver
{
  private JLabel lblProgress;
  private JPanel progressPanel;
  private JProgressBar jProgressBar = new JProgressBar();
  private PosButton btnCancel;
  private String successMsg;
  
  public ProgressDialog() {
    super(POSUtil.getFocusedWindow(), "", true);
    JPanel contentPanel = new JPanel();
    contentPanel.setLayout(new BorderLayout());
    progressPanel = new JPanel(new MigLayout("fillx,ins 20 20 5 20,center", "[250]", "[]"));
    
    lblProgress = new JLabel();
    progressPanel.add(lblProgress, "wrap");
    progressPanel.add(jProgressBar, "grow,span,wrap,h " + PosUIManager.getSize(50));
    
    btnCancel = new PosButton(POSConstants.CANCEL);
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        closeDialog();
      }
    });
    progressPanel.add(btnCancel, "center,gaptop 5,gapbottom 5");
    
    contentPanel.setMinimumSize(PosUIManager.getSize(300, 200));
    contentPanel.add(progressPanel);
    add(contentPanel);
  }
  
  public void setIndeterminate(boolean b) {
    jProgressBar.setIndeterminate(b);
  }
  
  public abstract void execute(ProgressObserver paramProgressObserver);
  
  public void setVisible(boolean b)
  {
    if (b) {
      Thread thread = new Thread(new Runnable()
      {
        public void run() {
          execute(ProgressDialog.this);
          closeDialog();
          if (StringUtils.isNotEmpty(successMsg))
            POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), successMsg);
        }
      });
      thread.start();
    }
    super.setVisible(b);
  }
  
  public void closeDialog() {
    setCanceled(false);
    dispose();
  }
  
  public void progress(int percent, String text) {
    lblProgress.setText(text);
    jProgressBar.setValue(percent);
  }
  
  public void progress(int percent) {
    jProgressBar.setValue(percent);
  }
  
  public void progress(String text) {
    lblProgress.setText(text);
  }
  
  public Component getParentComponent()
  {
    return POSUtil.getFocusedWindow();
  }
  
  public void setProgressLabelText(String text) {
    lblProgress.setText(text);
  }
  
  public void showCompleteMsg(String successMsg) {
    this.successMsg = successMsg;
  }
}
