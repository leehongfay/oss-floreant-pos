package com.floreantpos.extension;

import com.floreantpos.config.ui.ConfigurationView;
import com.floreantpos.ui.views.payment.CardProcessor;

public abstract class PaymentGatewayPlugin
  extends AbstractFloreantPlugin
{
  public PaymentGatewayPlugin() {}
  
  public abstract String getId();
  
  public abstract boolean shouldShowCardInputProcessor();
  
  public abstract ConfigurationView getConfigurationPane()
    throws Exception;
  
  public abstract CardProcessor getProcessor();
}
