package com.floreantpos.extension;

import com.floreantpos.main.Application;
import com.floreantpos.util.JarUtil;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import net.xeoh.plugins.base.Plugin;
import net.xeoh.plugins.base.PluginManager;
import net.xeoh.plugins.base.impl.PluginManagerFactory;
import net.xeoh.plugins.base.options.AddPluginsFromOption;
import net.xeoh.plugins.base.util.PluginManagerUtil;
import org.apache.commons.lang.StringUtils;

















public class ExtensionManager
{
  private List<FloreantPlugin> plugins;
  private static ExtensionManager instance;
  
  public ExtensionManager() {}
  
  private synchronized void initialize()
  {
    PluginManager pluginManager = PluginManagerFactory.createPluginManager();
    
    String jarLocation = JarUtil.getJarLocation(Application.class);
    URI uri = new File(jarLocation).toURI();
    pluginManager.addPluginsFrom(uri, new AddPluginsFromOption[0]);
    
    String pluginsPath = System.getProperty("pluginsPath");
    
    if (StringUtils.isNotEmpty(pluginsPath)) {
      String[] paths = pluginsPath.split(",");
      for (String string : paths) {
        pluginManager.addPluginsFrom(new File(string).toURI(), new AddPluginsFromOption[0]);
      }
    }
    else {
      pluginManager.addPluginsFrom(new File("plugins/").toURI(), new AddPluginsFromOption[0]);
    }
    
    PluginManagerUtil pmUtil = new PluginManagerUtil(pluginManager);
    Object allPlugins = (List)pmUtil.getPlugins();
    

    Collections.sort((List)allPlugins, new Comparator()
    {
      public int compare(Plugin o1, Plugin o2) {
        return o1.getClass().getName().compareToIgnoreCase(o2.getClass().getName());
      }
      
    });
    Object floreantPlugins = new ArrayList();
    
    for (Plugin plugin : (List)allPlugins) {
      if ((plugin instanceof FloreantPlugin)) {
        FloreantPlugin floreantPlugin = (FloreantPlugin)plugin;
        if (floreantPlugin.requireLicense()) {
          floreantPlugin.initLicense();
          if (floreantPlugin.hasValidLicense()) {
            ((List)floreantPlugins).add(floreantPlugin);
          }
        }
        else {
          ((List)floreantPlugins).add(floreantPlugin);
        }
      }
    }
    
    plugins = Collections.unmodifiableList((List)floreantPlugins);
  }
  
  public static List<FloreantPlugin> getPlugins() {
    return getInstanceplugins;
  }
  
  public static List<FloreantPlugin> getPlugins(Class pluginClass) {
    List<FloreantPlugin> list = new ArrayList();
    
    for (FloreantPlugin floreantPlugin : getInstanceplugins) {
      if (pluginClass.isAssignableFrom(floreantPlugin.getClass())) {
        list.add(floreantPlugin);
      }
    }
    
    return list;
  }
  
  public static FloreantPlugin getPlugin(Class pluginClass) {
    for (FloreantPlugin floreantPlugin : getInstanceplugins) {
      if (pluginClass.isAssignableFrom(floreantPlugin.getClass())) {
        return floreantPlugin;
      }
    }
    
    return null;
  }
  
  public static synchronized ExtensionManager getInstance() {
    if (instance == null) {
      instance = new ExtensionManager();
      instance.initialize();
    }
    
    return instance;
  }
}
