package com.floreantpos.extension;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.config.ui.ConfigurationView;
import com.floreantpos.config.ui.DefaultMerchantGatewayConfigurationView;
import com.floreantpos.ui.views.payment.CardProcessor;
import com.floreantpos.ui.views.payment.MercuryPayProcessor;
import net.xeoh.plugins.base.annotations.PluginImplementation;















@PluginImplementation
public class MercuryGatewayPlugin
  extends AuthorizeNetGatewayPlugin
{
  public MercuryGatewayPlugin() {}
  
  public boolean requireLicense()
  {
    return false;
  }
  
  public String getProductName()
  {
    return "Mercury Pay";
  }
  
  public ConfigurationView getConfigurationPane()
  {
    if (view == null) {
      view = new DefaultMerchantGatewayConfigurationView();
      view.setMerchantDefaultValue("118725340908147", "XYZ");
    }
    
    return view;
  }
  


  public void initLicense() {}
  


  public void initBackoffice(BackOfficeWindow backOfficeWindow) {}
  


  public String toString()
  {
    return getProductName();
  }
  
  public String getId()
  {
    return String.valueOf("Mercury Pay".hashCode());
  }
  
  public CardProcessor getProcessor()
  {
    return new MercuryPayProcessor();
  }
}
