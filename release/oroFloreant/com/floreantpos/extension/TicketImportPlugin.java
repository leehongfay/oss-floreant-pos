package com.floreantpos.extension;

public abstract interface TicketImportPlugin
  extends FloreantPlugin
{
  public abstract void startService();
  
  public abstract void stopService();
  
  public abstract void importTicket();
}
