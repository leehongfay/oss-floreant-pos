package com.floreantpos.extension;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.config.ui.ConfigurationView;
import com.floreantpos.config.ui.DefaultPosGiftCardConfigurationView;
import com.floreantpos.main.PosWindow;
import com.floreantpos.ui.views.payment.GiftCardProcessor;
import com.floreantpos.ui.views.payment.PosGiftCardProcessor;
import java.awt.Component;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JDialog;
import net.xeoh.plugins.base.annotations.PluginImplementation;



















@PluginImplementation
public class PosGiftCardPlugin
  extends GiftCardPaymentPlugin
{
  public static final String ID = String.valueOf("OroGiftCard".hashCode());
  protected DefaultPosGiftCardConfigurationView giftCardView;
  
  public PosGiftCardPlugin() {}
  
  public boolean requireLicense() {
    return false;
  }
  
  public String getProductName()
  {
    return "OroGiftCard";
  }
  
  public ConfigurationView getConfigurationPane() throws Exception
  {
    if (giftCardView == null) {
      giftCardView = new DefaultPosGiftCardConfigurationView();
      giftCardView.setMerchantDefaultValue("admin", "123456");
      giftCardView.initialize();
    }
    
    return giftCardView;
  }
  



  public void initLicense() {}
  


  public void initBackoffice(BackOfficeWindow backOfficeWindow) {}
  


  public void initConfigurationView(JDialog dialog) {}
  


  public String toString()
  {
    return getProductName();
  }
  
  public String getId()
  {
    return ID;
  }
  
  public GiftCardProcessor getProcessor()
  {
    return new PosGiftCardProcessor();
  }
  
  public boolean shouldShowCardInputProcessor()
  {
    return true;
  }
  
  public List<AbstractAction> getSpecialFunctionActions()
  {
    return null;
  }
  
  public boolean hasValidLicense()
  {
    return true;
  }
  


  public void initUI(PosWindow posWindow) {}
  

  public String getProductVersion()
  {
    return "1.0";
  }
  
  public Component getParent()
  {
    return null;
  }
}
