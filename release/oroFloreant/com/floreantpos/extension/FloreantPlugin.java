package com.floreantpos.extension;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.config.ui.ConfigurationSubEditor;
import com.floreantpos.main.PosWindow;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JDialog;
import net.xeoh.plugins.base.Plugin;

public abstract interface FloreantPlugin
  extends Plugin
{
  public abstract String getId();
  
  public abstract boolean requireLicense();
  
  public abstract boolean hasValidLicense();
  
  public abstract void initLicense();
  
  public abstract void initUI(PosWindow paramPosWindow);
  
  public abstract void initBackoffice(BackOfficeWindow paramBackOfficeWindow);
  
  public abstract void initConfigurationView(JDialog paramJDialog);
  
  public abstract List<ConfigurationSubEditor> getSubEditors();
  
  public abstract List<AbstractAction> getSpecialFunctionActions();
}
