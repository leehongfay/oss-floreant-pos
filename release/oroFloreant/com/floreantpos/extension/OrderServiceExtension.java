package com.floreantpos.extension;

import com.floreantpos.customer.CustomerSelector;
import com.floreantpos.model.Customer;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.Ticket;
import com.floreantpos.ui.views.IView;
import com.floreantpos.util.TicketAlreadyExistsException;
import java.util.List;
import javax.swing.JMenu;

public abstract class OrderServiceExtension
  extends AbstractFloreantPlugin
{
  public OrderServiceExtension() {}
  
  public abstract String getProductName();
  
  public abstract String getDescription();
  
  public abstract void createNewTicket(OrderType paramOrderType, List<ShopTable> paramList, Customer paramCustomer)
    throws TicketAlreadyExistsException;
  
  public abstract void createNewTicket(OrderType paramOrderType, List<ShopTable> paramList, Customer paramCustomer, int paramInt)
    throws TicketAlreadyExistsException;
  
  public abstract void setCustomerToTicket(String paramString);
  
  public abstract void setDeliveryDate(String paramString);
  
  public abstract void assignDriver(String paramString);
  
  public abstract boolean finishOrder(String paramString);
  
  public abstract void createCustomerMenu(JMenu paramJMenu);
  
  public abstract CustomerSelector createNewCustomerSelector();
  
  public abstract CustomerSelector createCustomerSelectorView();
  
  public abstract IView getDeliveryDispatchView(OrderType paramOrderType);
  
  public abstract IView getDriverView();
  
  public abstract void openDeliveryDispatchDialog(OrderType paramOrderType);
  
  public abstract void showDeliveryInfo(Ticket paramTicket, OrderType paramOrderType, Customer paramCustomer);
}
