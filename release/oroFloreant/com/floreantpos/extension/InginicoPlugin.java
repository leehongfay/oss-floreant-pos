package com.floreantpos.extension;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.config.ui.ConfigurationView;
import com.floreantpos.config.ui.InginicoConfigurationView;
import com.floreantpos.main.PosWindow;
import com.floreantpos.ui.views.payment.CardProcessor;
import java.awt.Component;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JDialog;
import net.xeoh.plugins.base.annotations.PluginImplementation;




@PluginImplementation
public class InginicoPlugin
  extends PaymentGatewayPlugin
{
  InginicoConfigurationView view;
  
  public InginicoPlugin() {}
  
  public String getProductName()
  {
    return "Ingenico IWL220 TGI";
  }
  
  public boolean requireLicense()
  {
    return false;
  }
  


  public void initLicense() {}
  


  public void initBackoffice(BackOfficeWindow backOfficeWindow) {}
  


  public void initConfigurationView(JDialog dialog) {}
  

  public String getId()
  {
    return String.valueOf("Inginico".hashCode());
  }
  
  public String toString()
  {
    return getProductName();
  }
  
  public ConfigurationView getConfigurationPane() throws Exception
  {
    if (view == null) {
      view = new InginicoConfigurationView();
      view.initialize();
    }
    
    return view;
  }
  
  public CardProcessor getProcessor()
  {
    return null;
  }
  
  public boolean shouldShowCardInputProcessor()
  {
    return true;
  }
  
  public List<AbstractAction> getSpecialFunctionActions()
  {
    return null;
  }
  
  public boolean hasValidLicense()
  {
    return true;
  }
  


  public void initUI(PosWindow posWindow) {}
  

  public String getProductVersion()
  {
    return "1.0";
  }
  
  public Component getParent()
  {
    return view;
  }
}
