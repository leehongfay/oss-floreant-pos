package com.floreantpos.extension;

import net.xeoh.plugins.base.Plugin;

public abstract interface TestExtension
  extends Plugin
{
  public abstract boolean canSayHello();
  
  public abstract void sayHell();
}
