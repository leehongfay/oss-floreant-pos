package com.floreantpos.extension;

import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.MenuItem;
import javax.swing.JTabbedPane;

public abstract class InventoryPlugin
  extends AbstractFloreantPlugin
{
  public InventoryPlugin() {}
  
  public abstract void addRecepieView(JTabbedPane paramJTabbedPane);
  
  public abstract void showInventoryUnitEntryDialog(InventoryUnit paramInventoryUnit);
  
  public abstract void addStockUnitView(JTabbedPane paramJTabbedPane, MenuItem paramMenuItem);
}
