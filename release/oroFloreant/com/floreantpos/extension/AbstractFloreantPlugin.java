package com.floreantpos.extension;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.config.ui.ConfigurationSubEditor;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.orocube.common.util.TerminalUtil;
import com.orocube.licensemanager.InvalidLicenseException;
import com.orocube.licensemanager.LicenseUtil;
import com.orocube.licensemanager.OroLicense;
import com.orocube.licensemanager.ui.InvalidPluginDialog;
import com.orocube.licensemanager.ui.LicenseSelectionListener;
import java.awt.Component;
import java.io.File;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JDialog;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

















public abstract class AbstractFloreantPlugin
  implements FloreantPlugin, LicenseSelectionListener
{
  private OroLicense license;
  
  public AbstractFloreantPlugin() {}
  
  public boolean requireLicense()
  {
    return true;
  }
  
  public void initLicense()
  {
    try {
      license = LicenseUtil.loadAndValidate(getProductName(), getProductVersion(), TerminalUtil.getSystemUID());
    } catch (InvalidLicenseException e) {
      LogFactory.getLog(getClass()).error(e);
      InvalidPluginDialog.show(this, Application.getPosWindow(), e.getMessage(), "Plugin error!", getProductName(), getProductVersion(), 
        TerminalUtil.getSystemUID());
    } catch (Exception e) {
      LogFactory.getLog(getClass()).error(e);
    }
  }
  
  public abstract String getId();
  
  public abstract String getProductName();
  
  public abstract String getProductVersion();
  
  public abstract Component getParent();
  
  public void licenseFileSelected(File pluginFile) throws Exception
  {
    try {
      OroLicense newLicense = LicenseUtil.loadAndValidate(pluginFile, getProductName(), getProductVersion(), TerminalUtil.getSystemUID());
      LicenseUtil.copyLicenseFile(pluginFile, getProductName());
      setLicense(newLicense);
    } catch (Exception e) {
      LogFactory.getLog(getClass()).error(e);
      throw e;
    }
  }
  
  public boolean hasValidLicense()
  {
    if (license != null) {
      return license.isValid();
    }
    return false;
  }
  
  public OroLicense getLicense() {
    return license;
  }
  
  public void setLicense(OroLicense license) {
    this.license = license;
  }
  


  public void initBackoffice(BackOfficeWindow backOfficeWindow) {}
  

  public void initConfigurationView(JDialog dialog) {}
  

  public List<AbstractAction> getSpecialFunctionActions()
  {
    return null;
  }
  
  public List<ConfigurationSubEditor> getSubEditors()
  {
    return null;
  }
  
  public void initUI(PosWindow posWindow) {}
  
  public void restartPOS(boolean restart) {}
}
