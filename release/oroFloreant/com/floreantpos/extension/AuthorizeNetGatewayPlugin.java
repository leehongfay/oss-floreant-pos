package com.floreantpos.extension;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.config.ui.ConfigurationView;
import com.floreantpos.config.ui.DefaultMerchantGatewayConfigurationView;
import com.floreantpos.main.PosWindow;
import com.floreantpos.ui.views.payment.AuthorizeDotNetProcessor;
import com.floreantpos.ui.views.payment.CardProcessor;
import java.awt.Component;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JDialog;
import net.xeoh.plugins.base.annotations.PluginImplementation;



















@PluginImplementation
public class AuthorizeNetGatewayPlugin
  extends PaymentGatewayPlugin
{
  public static final String ID = String.valueOf("Authorize.Net".hashCode());
  protected DefaultMerchantGatewayConfigurationView view;
  
  public AuthorizeNetGatewayPlugin() {}
  
  public boolean requireLicense() {
    return false;
  }
  
  public String getProductName()
  {
    return "Authorize.Net";
  }
  
  public ConfigurationView getConfigurationPane() throws Exception
  {
    if (view == null) {
      view = new DefaultMerchantGatewayConfigurationView();
      view.setMerchantDefaultValue("6tuU4N3H", "4k6955x3T8bCVPVm");
      view.initialize();
    }
    
    return view;
  }
  



  public void initLicense() {}
  


  public void initBackoffice(BackOfficeWindow backOfficeWindow) {}
  


  public void initConfigurationView(JDialog dialog) {}
  


  public String toString()
  {
    return getProductName();
  }
  
  public String getId()
  {
    return ID;
  }
  
  public CardProcessor getProcessor()
  {
    return new AuthorizeDotNetProcessor();
  }
  
  public boolean shouldShowCardInputProcessor()
  {
    return true;
  }
  
  public List<AbstractAction> getSpecialFunctionActions()
  {
    return null;
  }
  
  public boolean hasValidLicense()
  {
    return true;
  }
  


  public void initUI(PosWindow posWindow) {}
  

  public String getProductVersion()
  {
    return null;
  }
  
  public Component getParent()
  {
    return null;
  }
}
