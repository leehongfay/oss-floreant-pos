package com.floreantpos;

import com.floreantpos.config.AppConfig;
import com.floreantpos.model.Store;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.UserType;
import com.floreantpos.model.dao.StoreDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.model.dao.UserTypeDAO;
import com.floreantpos.model.dao._RootDAO;
import com.floreantpos.util.ReceiptUtil;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataBuilder;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;

public class DBCreator
{
  public DBCreator() {}
  
  public static void main(String[] args)
  {
    Database database = AppConfig.getDefaultDatabase();
    String connectString = AppConfig.getConnectString();
    String databaseUser = AppConfig.getDatabaseUser();
    String databasePassword = AppConfig.getDatabasePassword();
    
    System.out.println("connect string: " + connectString);
    System.out.println("user: " + databaseUser);
    System.out.println("pass: " + databasePassword);
    
    boolean databaseCreated = createDatabase(connectString, database.getHibernateDialect(), database
      .getHibernateConnectionDriverClass(), databaseUser, databasePassword, false);
    
    if (databaseCreated) {
      PosLog.info(DBCreator.class, Messages.getString("DBCreator.5"));
    }
    else {
      PosLog.info(DBCreator.class, Messages.getString("DBCreator.6"));
    }
    System.exit(0);
  }
  
  public static boolean createDatabase(String connectionString, String hibernateDialect, String hibernateConnectionDriverClass, String user, String password, boolean exportSampleData)
  {
    _RootDAO.releaseConnection();
    
    StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml");
    builder.applySetting("hibernate.dialect", hibernateDialect);
    builder.applySetting("hibernate.connection.driver_class", hibernateConnectionDriverClass);
    builder.applySetting("hibernate.connection.url", connectionString);
    builder.applySetting("hibernate.connection.username", user);
    builder.applySetting("hibernate.connection.password", password);
    builder.applySetting("hibernate.hbm2ddl.auto", "create");
    builder.applySetting("hibernate.c3p0.checkoutTimeout", "0");
    
    StandardServiceRegistry standardRegistry = builder.build();
    Metadata metaData = new MetadataSources(standardRegistry).getMetadataBuilder().build();
    
    SchemaExport schemaExport = new SchemaExport();
    EnumSet<TargetType> enumSet = EnumSet.of(TargetType.DATABASE, TargetType.STDOUT);
    schemaExport.create(enumSet, metaData);
    
    _RootDAO.setSessionFactory(metaData.buildSessionFactory());
    
    Store store = new Store();
    store.setId("1");
    store.setName("Sample Restaurant");
    store.setAddressLine1("Somewhere");
    store.setTelephone("+0123456789");
    store.setProperties(new HashMap());
    ReceiptUtil.populateDefaultTicketReceiptProperties(store);
    ReceiptUtil.populateDefaultKitchenReceiptProperties(store);
    StoreDAO.getInstance().saveOrUpdate(store);
    
    UserType administrator = new UserType();
    administrator.setName(POSConstants.ADMINISTRATOR);
    administrator.setPermissions(new HashSet(Arrays.asList(UserPermission.permissions)));
    UserTypeDAO.getInstance().saveOrUpdate(administrator);
    
    User administratorUser = new User();
    administratorUser.setId("123");
    administratorUser.setEncryptedPassword("1111");
    administratorUser.setFirstName("admin");
    administratorUser.setLastName("s");
    administratorUser.setType(administrator);
    administratorUser.setRoot(Boolean.valueOf(true));
    administratorUser.setActive(Boolean.valueOf(true));
    UserDAO.getInstance().saveOrUpdate(administratorUser);
    
    return true;
  }
}
