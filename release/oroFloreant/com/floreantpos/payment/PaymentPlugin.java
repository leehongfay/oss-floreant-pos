package com.floreantpos.payment;

import com.floreantpos.extension.FloreantPlugin;
import com.floreantpos.model.Ticket;
import com.floreantpos.ui.views.payment.SettleTicketProcessor;

public abstract interface PaymentPlugin
  extends FloreantPlugin
{
  public abstract String getName();
  
  public abstract void pay(Ticket paramTicket, double paramDouble, SettleTicketProcessor paramSettleTicketProcessor)
    throws Exception;
}
