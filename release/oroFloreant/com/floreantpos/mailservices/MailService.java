package com.floreantpos.mailservices;

import com.floreantpos.Messages;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.PosPrinters;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.util.AESencrp;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import org.apache.commons.lang.StringUtils;







public class MailService
{
  public MailService() {}
  
  public static void checkEmailConfiguration()
    throws Exception
  {
    Terminal terminal = Application.getInstance().refreshAndGetTerminal();
    
    String smtpHost = terminal.getSmtpHost();
    String senderEmail = terminal.getSmtpSender();
    String encryptedPassword = terminal.getSmtpPassword();
    String password = null;
    
    if (!StringUtils.isEmpty(encryptedPassword)) {
      password = AESencrp.decrypt(encryptedPassword);
    }
    
    if (StringUtils.isEmpty(smtpHost)) {
      throw new PosException("SMTP Host not found! Please set configuration!");
    }
    if (StringUtils.isEmpty(senderEmail)) {
      throw new PosException("Sender email not found! Please set configuration!");
    }
    if (StringUtils.isEmpty(password)) {
      throw new PosException("Password not found! Please set configuration!");
    }
  }
  
  public static void sendTicket(String email, Ticket ticket) throws Exception {
    if (!isValidEmail(email)) {
      throw new PosException("Invalid email address");
    }
    checkEmailConfiguration();
    
    JasperPrint jasperPrint = ReceiptPrintService.createJasperFileForTicketReceipt(ticket);
    jasperPrint.setName("TICKET_RECEIPT" + ticket.getId());
    jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
    
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    
    JRPdfExporter exporter = new JRPdfExporter();
    exporter.setParameter(JRPdfExporterParameter.JASPER_PRINT, jasperPrint);
    exporter.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
    try {
      exporter.exportReport();
    } catch (JRException e) {
      PosLog.error(MailService.class, e);
    }
    byte[] bytes = byteArrayOutputStream.toByteArray();
    ByteArrayDataSource bads = new ByteArrayDataSource(bytes, "application/pdf");
    if (!sendMail(email, "Ticket Receipt", "Ticket Receipt", "TICKET_RECEIPT_" + ticket.getId() + ".pdf", bads)) {
      throw new PosException("Email sending failed.");
    }
  }
  
  public static boolean isValidEmail(String email) {
    Pattern validEmailRegExp = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", 2);
    Matcher matcher = validEmailRegExp.matcher(email);
    return matcher.find();
  }
  
  public static boolean sendMail(String to, String subject, String text, String fileName, ByteArrayDataSource attachments) {
    try {
      Terminal terminal = Application.getInstance().getTerminal();
      String host = terminal.getSmtpHost();
      Integer hostPort = terminal.getSmtpPort();
      String email = terminal.getSmtpSender();
      String encryptedPassword = terminal.getSmtpPassword();
      if (encryptedPassword != null) {
        encryptedPassword = AESencrp.decrypt(encryptedPassword);
      }
      
      return sendMail(host, hostPort, email, encryptedPassword, to, subject, text, fileName, attachments);
    } catch (Exception e) {
      PosLog.error(MailService.class, e);
    }
    return false;
  }
  
  public static boolean sendMail(String smtpHost, Integer smtpPort, String senderEmail, final String senderPassword, String to, String subject, String text, String fileName, ByteArrayDataSource attachments)
  {
    Properties props = new Properties();
    
    props.put("mail.smtp.host", smtpHost);
    props.put("mail.smtp.socketFactory.port", String.valueOf(smtpPort));
    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.port", String.valueOf(smtpPort));
    
    Session session = Session.getDefaultInstance(props, new Authenticator() {
      protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(val$senderEmail, senderPassword);
      }
    });
    
    try
    {
      Message message = new MimeMessage(session);
      message.setFrom(new InternetAddress(senderEmail));
      message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
      message.setSubject(subject);
      
      BodyPart messageBodyPart1 = new MimeBodyPart();
      messageBodyPart1.setText(text);
      
      MimeBodyPart messageBodyPart2 = new MimeBodyPart();
      DataSource source = new DataSource()
      {
        public OutputStream getOutputStream() throws IOException
        {
          return val$attachments.getOutputStream();
        }
        
        public String getName()
        {
          return val$attachments.getName();
        }
        
        public InputStream getInputStream() throws IOException
        {
          return val$attachments.getInputStream();
        }
        
        public String getContentType()
        {
          return val$attachments.getContentType();
        }
        
      };
      Multipart multipart = new MimeMultipart();
      if (attachments != null) {
        messageBodyPart2.setDataHandler(new DataHandler(source));
        messageBodyPart2.setFileName(fileName);
        multipart.addBodyPart(messageBodyPart2);
      }
      
      multipart.addBodyPart(messageBodyPart1);
      
      message.setContent(multipart);
      Transport.send(message);
      
      PosLog.info(MailService.class, Messages.getString("MailService.11"));
      return true;
    }
    catch (MessagingException e) {
      throw new RuntimeException(e);
    }
  }
  
  public static void main(String[] args) {
    sendMail("cto@orocube.net", "Test", "Test", "Test", null);
  }
}
