package com.orostock.inventory.dao;

import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.InventoryVendorItems;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.swing.PaginationSupport;
import java.util.Iterator;
import java.util.List;
import net.authorize.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class InventoryDAO extends com.floreantpos.model.dao.GenericDAO
{
  public static InventoryDAO instance;
  
  public InventoryDAO() {}
  
  public static InventoryDAO getInstance()
  {
    if (null == instance)
      instance = new InventoryDAO();
    return instance;
  }
  
  public void loadInventoryItems(PaginationSupport model, MenuGroup menuGroup, String itemName, List<InventoryVendor> vendors) {
    Session session = null;
    Criteria criteria = null;
    try {
      session = createNewSession();
      if ((vendors != null) && (!vendors.isEmpty())) {
        criteria = findItemsByVendor(model, menuGroup, itemName, vendors, session);
      }
      else {
        criteria = session.createCriteria(MenuItem.class);
        criteria.add(Restrictions.eq(MenuItem.PROP_INVENTORY_ITEM, Boolean.valueOf(true)));
        itemName = itemName.trim();
        
        if (StringUtils.isNotEmpty(itemName)) {
          criteria.add(
            Restrictions.or(new org.hibernate.criterion.Criterion[] {Restrictions.ilike(MenuItem.PROP_NAME, itemName, MatchMode.ANYWHERE), 
            Restrictions.ilike(MenuItem.PROP_SKU, itemName, MatchMode.START), 
            Restrictions.ilike(MenuItem.PROP_BARCODE, itemName, MatchMode.START) }));
        }
        


        if (menuGroup != null) {
          criteria.add(Restrictions.eq(MenuItem.PROP_MENU_GROUP_ID, menuGroup.getId()));
        }
        
        criteria.setProjection(Projections.rowCount());
        Number rowCount = (Number)criteria.uniqueResult();
        model.setNumRows(rowCount.intValue());
        
        criteria.setProjection(null);
        criteria.addOrder(org.hibernate.criterion.Order.asc(MenuItem.PROP_ID));
        criteria.setFirstResult(model.getCurrentRowIndex());
        criteria.setMaxResults(model.getPageSize());
      }
      criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
      List<MenuItem> result = criteria.list();
      loadVendors(session, result);
      model.setRows(result);
    } finally {
      closeSession(session);
    }
  }
  
  private void loadVendors(Session session, List<MenuItem> result) { List<InventoryVendorItems> list;
    Iterator localIterator1;
    if (!result.isEmpty()) {
      Criteria criteria = session.createCriteria(InventoryVendorItems.class);
      criteria.add(Restrictions.in(InventoryVendorItems.PROP_ITEM, result));
      list = criteria.list();
      for (localIterator1 = result.iterator(); localIterator1.hasNext();) { item = (MenuItem)localIterator1.next();
        for (InventoryVendorItems inventoryVendorItems : list)
          if (item.getId().equals(inventoryVendorItems.getItem().getId())) {
            String name = StringUtils.isNotEmpty(item.getVendorNames()) ? item.getVendorNames() + ", " : "";
            item.setVendorNames(name + inventoryVendorItems.getVendor().getName());
          }
      }
    }
    MenuItem item;
  }
  
  private Criteria findItemsByVendor(PaginationSupport model, MenuGroup menuGroup, String itemName, List<InventoryVendor> vendors, Session session) {
    Criteria criteria = session.createCriteria(InventoryVendorItems.class);
    criteria.add(Restrictions.in(InventoryVendorItems.PROP_VENDOR, vendors));
    criteria.createAlias(InventoryVendorItems.PROP_ITEM, "menuItem");
    
    criteria.add(Restrictions.eq("menuItem." + MenuItem.PROP_INVENTORY_ITEM, Boolean.valueOf(true)));
    itemName = itemName.trim();
    
    if (StringUtils.isNotEmpty(itemName)) {
      criteria.add(
        Restrictions.or(new org.hibernate.criterion.Criterion[] {
        Restrictions.ilike("menuItem." + MenuItem.PROP_NAME, itemName, MatchMode.ANYWHERE), 
        Restrictions.ilike("menuItem." + MenuItem.PROP_SKU, itemName, MatchMode.START), 
        Restrictions.ilike("menuItem." + MenuItem.PROP_BARCODE, itemName, MatchMode.START) }));
    }
    


    if (menuGroup != null) {
      criteria.add(Restrictions.eq("menuItem." + MenuItem.PROP_MENU_GROUP_ID, menuGroup.getId()));
    }
    criteria.setProjection(Projections.rowCount());
    Number rowCount = (Number)criteria.uniqueResult();
    model.setNumRows(rowCount.intValue());
    
    criteria.setProjection(Projections.property(InventoryVendorItems.PROP_ITEM));
    criteria.addOrder(org.hibernate.criterion.Order.asc(InventoryVendorItems.PROP_VENDOR));
    criteria.setFirstResult(model.getCurrentRowIndex());
    criteria.setMaxResults(model.getPageSize());
    return criteria;
  }
}
