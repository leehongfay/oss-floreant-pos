package com.orostock.inventory.action;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.ui.recepie.RecipeTabExplorer;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;

public class RecipeBrowserAction
  extends AbstractAction
{
  public RecipeBrowserAction()
  {
    super("Recipe");
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = window.getTabbedPane();
      
      RecipeTabExplorer browser = null;
      
      int index = tabbedPane.indexOfTab("Recipe");
      if (index == -1) {
        browser = new RecipeTabExplorer();
        tabbedPane.addTab("Recipe", browser);
      }
      else {
        browser = (RecipeTabExplorer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(browser);
    } catch (Exception e) {
      POSMessageDialog.showError(window, e.getMessage(), e);
    }
  }
}
