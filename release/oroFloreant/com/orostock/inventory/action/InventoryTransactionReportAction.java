package com.orostock.inventory.action;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.report.InventoryTransactionReportView;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

public class InventoryTransactionReportAction extends AbstractAction
{
  public InventoryTransactionReportAction()
  {
    super("Inventory Transaction Report");
  }
  
  public InventoryTransactionReportAction(String name) {
    super(name);
  }
  
  public InventoryTransactionReportAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = window.getTabbedPane();
      
      InventoryTransactionReportView reportView = null;
      int index = tabbedPane.indexOfTab("Inventory Transaction Report");
      if (index == -1) {
        reportView = new InventoryTransactionReportView();
        tabbedPane.addTab("Inventory Transaction Report", reportView);
      }
      else {
        reportView = (InventoryTransactionReportView)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(reportView);
    } catch (Exception e) {
      POSMessageDialog.showError(window, e.getMessage(), e);
    }
  }
}
