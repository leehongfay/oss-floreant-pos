package com.orostock.inventory.action;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.ui.PrintLabelExplorer;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;

public class InventoryPrintLabelAction
  extends AbstractAction
{
  public InventoryPrintLabelAction()
  {
    super("Print Label");
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = window.getTabbedPane();
      
      PrintLabelExplorer browser = null;
      
      int index = tabbedPane.indexOfTab("Label Print");
      if (index == -1) {
        browser = new PrintLabelExplorer();
        tabbedPane.addTab("Label Print", browser);
      }
      else {
        browser = (PrintLabelExplorer)tabbedPane.getComponentAt(index);
      }
      
      tabbedPane.setSelectedComponent(browser);
    } catch (Exception e) {
      POSMessageDialog.showError(window, e.getMessage(), e);
    }
  }
}
