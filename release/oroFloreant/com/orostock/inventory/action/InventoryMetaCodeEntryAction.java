package com.orostock.inventory.action;

import com.floreantpos.model.InventoryMetaCode;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.ui.InventoryMetacodeEntryForm;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

public class InventoryMetaCodeEntryAction
  extends AbstractAction
{
  public InventoryMetaCodeEntryAction()
  {
    super("New Inventory Meta code");
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    try {
      InventoryMetacodeEntryForm form = new InventoryMetacodeEntryForm(new InventoryMetaCode());
      BeanEditorDialog dialog = new BeanEditorDialog(form);
      dialog.pack();
      dialog.open();
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    }
  }
}
