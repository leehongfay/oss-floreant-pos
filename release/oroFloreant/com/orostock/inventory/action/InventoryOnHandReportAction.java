package com.orostock.inventory.action;

import com.floreantpos.Messages;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.report.InventoryOnHandReportView;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

public class InventoryOnHandReportAction extends AbstractAction
{
  public InventoryOnHandReportAction()
  {
    super(Messages.getString("InventoryOnHandReportAction.0"));
  }
  
  public InventoryOnHandReportAction(String name) {
    super(name);
  }
  
  public InventoryOnHandReportAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = window.getTabbedPane();
      
      InventoryOnHandReportView reportView = null;
      int index = tabbedPane.indexOfTab("Inventory On Hand Report");
      if (index == -1) {
        reportView = new InventoryOnHandReportView();
        tabbedPane.addTab("Inventory On Hand Report", reportView);
      }
      else {
        reportView = (InventoryOnHandReportView)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(reportView);
    } catch (Exception e) {
      POSMessageDialog.showError(window, e.getMessage(), e);
    }
  }
}
