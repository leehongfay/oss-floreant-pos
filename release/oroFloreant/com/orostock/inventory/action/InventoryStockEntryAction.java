package com.orostock.inventory.action;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.ui.InventoryStockBrowser;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;


public class InventoryStockEntryAction
  extends AbstractAction
{
  public InventoryStockEntryAction()
  {
    super("Stock");
  }
  

  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = window.getTabbedPane();
      
      InventoryStockBrowser browser = null;
      
      int index = tabbedPane.indexOfTab("Inventory Stock");
      if (index == -1) {
        browser = new InventoryStockBrowser();
        tabbedPane.addTab("Inventory Stock", browser);
      }
      else {
        browser = (InventoryStockBrowser)tabbedPane.getComponentAt(index);
      }
      
      tabbedPane.setSelectedComponent(browser);
    } catch (Exception e) {
      POSMessageDialog.showError(window, e.getMessage(), e);
    }
  }
}
