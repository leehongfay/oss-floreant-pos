package com.orostock.inventory.action;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.ui.InventoryItemExplorer;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;

public class InventoryItemBrowserAction
  extends AbstractAction
{
  public InventoryItemBrowserAction()
  {
    super("Items");
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = window.getTabbedPane();
      
      InventoryItemExplorer browser = null;
      
      int index = tabbedPane.indexOfTab("Inventory Items");
      if (index == -1) {
        browser = new InventoryItemExplorer();
        tabbedPane.addTab("Inventory Items", browser);
      }
      else {
        browser = (InventoryItemExplorer)tabbedPane.getComponentAt(index);
      }
      
      tabbedPane.setSelectedComponent(browser);
    } catch (Exception e) {
      POSMessageDialog.showError(window, e.getMessage(), e);
    }
  }
}
