package com.orostock.inventory.action;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.ui.InventoryConfiguration;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;

public class InventoryConfigurationAction
  extends AbstractAction
{
  public InventoryConfigurationAction()
  {
    super("Configuration");
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = window.getTabbedPane();
      
      InventoryConfiguration browser = null;
      
      int index = tabbedPane.indexOfTab("Inventory Configuration");
      if (index == -1) {
        browser = new InventoryConfiguration();
        tabbedPane.addTab("Inventory Configuration", browser);
      }
      else {
        browser = (InventoryConfiguration)tabbedPane.getComponentAt(index);
      }
      
      tabbedPane.setSelectedComponent(browser);
    } catch (Exception e) {
      POSMessageDialog.showError(window, e.getMessage(), e);
    }
  }
}
