package com.orostock.inventory.action;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.report.InventoryAverageUnitCostReportView;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

public class InventoryAverageUnitCostReportAction extends AbstractAction
{
  public InventoryAverageUnitCostReportAction()
  {
    super("Average Cost Report");
  }
  
  public InventoryAverageUnitCostReportAction(String name) {
    super(name);
  }
  
  public InventoryAverageUnitCostReportAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = window.getTabbedPane();
      
      InventoryAverageUnitCostReportView reportView = null;
      int index = tabbedPane.indexOfTab("Average Cost Report");
      if (index == -1) {
        reportView = new InventoryAverageUnitCostReportView();
        tabbedPane.addTab("Average Cost Report", reportView);
      }
      else {
        reportView = (InventoryAverageUnitCostReportView)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(reportView);
    } catch (Exception e) {
      POSMessageDialog.showError(window, e.getMessage(), e);
    }
  }
}
