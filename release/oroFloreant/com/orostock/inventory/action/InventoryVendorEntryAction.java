package com.orostock.inventory.action;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.ui.InventoryVendorsBrowser;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;

public class InventoryVendorEntryAction
  extends AbstractAction
{
  public InventoryVendorEntryAction()
  {
    super("Vendors");
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = window.getTabbedPane();
      
      InventoryVendorsBrowser browser = null;
      
      int index = tabbedPane.indexOfTab("Vendors");
      if (index == -1) {
        browser = new InventoryVendorsBrowser();
        tabbedPane.addTab("Vendors", browser);
      }
      else {
        browser = (InventoryVendorsBrowser)tabbedPane.getComponentAt(index);
      }
      
      tabbedPane.setSelectedComponent(browser);
    } catch (Exception e) {
      POSMessageDialog.showError(window, e.getMessage(), e);
    }
  }
}
