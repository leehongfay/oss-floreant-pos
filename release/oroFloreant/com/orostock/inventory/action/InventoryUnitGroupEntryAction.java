package com.orostock.inventory.action;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.bo.ui.explorer.InventoryUnitGroupExplorer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;

public class InventoryUnitGroupEntryAction
  extends AbstractAction
{
  public InventoryUnitGroupEntryAction()
  {
    super("Inventory Groups");
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = window.getTabbedPane();
      
      InventoryUnitGroupExplorer browser = null;
      
      int index = tabbedPane.indexOfTab("Inventory Groups");
      if (index == -1) {
        browser = new InventoryUnitGroupExplorer();
        tabbedPane.addTab("Inventory Groups", browser);
      }
      else {
        browser = (InventoryUnitGroupExplorer)tabbedPane.getComponentAt(index);
      }
      
      tabbedPane.setSelectedComponent(browser);
    } catch (Exception e) {
      POSMessageDialog.showError(window, e.getMessage(), e);
    }
  }
}
