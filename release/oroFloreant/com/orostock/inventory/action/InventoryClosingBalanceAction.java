package com.orostock.inventory.action;

import com.orostock.inventory.ui.InventoryClosingBalanceDialog;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

public class InventoryClosingBalanceAction
  extends AbstractAction
{
  public InventoryClosingBalanceAction()
  {
    super("Generate closing balance");
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    InventoryClosingBalanceDialog dialog = new InventoryClosingBalanceDialog();
    dialog.setSize(500, 380);
    dialog.open();
  }
}
