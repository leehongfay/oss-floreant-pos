package com.orostock.inventory.action;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.ui.PurchaseOrderExplorer;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

















public class PurchaseOrderExplorerAction
  extends AbstractAction
{
  public PurchaseOrderExplorerAction()
  {
    super("Purchase");
  }
  
  public PurchaseOrderExplorerAction(String name) {
    super(name);
  }
  
  public PurchaseOrderExplorerAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      PurchaseOrderExplorer explorer = null;
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab("Purchase");
      if (index == -1) {
        explorer = new PurchaseOrderExplorer();
        tabbedPane.addTab("Purchase", explorer);
      }
      else {
        explorer = (PurchaseOrderExplorer)tabbedPane.getComponentAt(index);
      }
      explorer.updateView();
      tabbedPane.setSelectedComponent(explorer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
