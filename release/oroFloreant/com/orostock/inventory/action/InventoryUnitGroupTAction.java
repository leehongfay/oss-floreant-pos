package com.orostock.inventory.action;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.ui.InventoryUnitGroupBrowserTree;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;

public class InventoryUnitGroupTAction
  extends AbstractAction
{
  public InventoryUnitGroupTAction()
  {
    super("Units");
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = window.getTabbedPane();
      
      InventoryUnitGroupBrowserTree browser = null;
      
      int index = tabbedPane.indexOfTab("Inventory Units");
      if (index == -1) {
        browser = new InventoryUnitGroupBrowserTree();
        tabbedPane.addTab("Inventory Units", browser);
      }
      else {
        browser = (InventoryUnitGroupBrowserTree)tabbedPane.getComponentAt(index);
      }
      
      tabbedPane.setSelectedComponent(browser);
    } catch (Exception e) {
      POSMessageDialog.showError(window, e.getMessage(), e);
    }
  }
}
