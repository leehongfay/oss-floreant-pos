package com.orostock.inventory.action;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.ui.InventoryLocationBrowser;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;

public class InventoryLocationEntryAction
  extends AbstractAction
{
  public InventoryLocationEntryAction()
  {
    super("Locations");
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = window.getTabbedPane();
      
      InventoryLocationBrowser browser = null;
      
      int index = tabbedPane.indexOfTab("Inventory Locations");
      if (index == -1) {
        browser = new InventoryLocationBrowser();
        tabbedPane.addTab("Inventory Locations", browser);
      }
      else {
        browser = (InventoryLocationBrowser)tabbedPane.getComponentAt(index);
      }
      
      tabbedPane.setSelectedComponent(browser);
    } catch (Exception e) {
      POSMessageDialog.showError(window, e.getMessage(), e);
    }
  }
}
