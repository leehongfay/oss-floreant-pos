package com.orostock.inventory.action;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.ui.ShoppingListExplorer;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;

public class ShoppingListAction
  extends AbstractAction
{
  public ShoppingListAction()
  {
    super("Shopping List");
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = window.getTabbedPane();
      
      ShoppingListExplorer shoppingListForm = null;
      
      int index = tabbedPane.indexOfTab("Shopping List");
      if (index == -1) {
        shoppingListForm = new ShoppingListExplorer();
        tabbedPane.addTab("Shopping List", shoppingListForm);
      }
      else {
        shoppingListForm = (ShoppingListExplorer)tabbedPane.getComponentAt(index);
      }
      
      tabbedPane.setSelectedComponent(shoppingListForm);
    } catch (Exception e) {
      POSMessageDialog.showError(window, e.getMessage(), e);
    }
  }
}
