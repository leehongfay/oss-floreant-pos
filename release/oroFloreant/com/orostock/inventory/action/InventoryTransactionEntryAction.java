package com.orostock.inventory.action;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.ui.InventoryTransactionsBrowser;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;

public class InventoryTransactionEntryAction
  extends AbstractAction
{
  private static final long serialVersionUID = 1L;
  
  public InventoryTransactionEntryAction()
  {
    super("Transactions");
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = window.getTabbedPane();
      
      InventoryTransactionsBrowser browser = null;
      
      int index = tabbedPane.indexOfTab("Inventory Transaction");
      if (index == -1) {
        browser = new InventoryTransactionsBrowser();
        tabbedPane.addTab("Inventory Transaction", browser);
      }
      else {
        browser = (InventoryTransactionsBrowser)tabbedPane.getComponentAt(index);
      }
      
      tabbedPane.setSelectedComponent(browser);
    } catch (Exception e) {
      POSMessageDialog.showError(window, e.getMessage(), e);
    }
  }
}
