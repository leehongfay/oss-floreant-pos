package com.orostock.inventory.action;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.ui.StockCountExplorer;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

















public class StockCountExplorerAction
  extends AbstractAction
{
  public StockCountExplorerAction()
  {
    super("Stock Count");
  }
  
  public StockCountExplorerAction(String name) {
    super(name);
  }
  
  public StockCountExplorerAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      StockCountExplorer explorer = null;
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab("Stock Count");
      if (index == -1) {
        explorer = new StockCountExplorer();
        tabbedPane.addTab("Stock Count", explorer);
      }
      else {
        explorer = (StockCountExplorer)tabbedPane.getComponentAt(index);
      }
      explorer.updateView();
      tabbedPane.setSelectedComponent(explorer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
