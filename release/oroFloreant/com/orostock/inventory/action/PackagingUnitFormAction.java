package com.orostock.inventory.action;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.ui.PackagingUnitsBrowser;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;

public class PackagingUnitFormAction
  extends AbstractAction
{
  public PackagingUnitFormAction()
  {
    super("Packaging Units");
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = window.getTabbedPane();
      
      PackagingUnitsBrowser browser = null;
      
      int index = tabbedPane.indexOfTab("Packaging Units");
      if (index == -1) {
        browser = new PackagingUnitsBrowser();
        tabbedPane.addTab("Packaging Units", browser);
      }
      else {
        browser = (PackagingUnitsBrowser)tabbedPane.getComponentAt(index);
      }
      
      tabbedPane.setSelectedComponent(browser);
    } catch (Exception e) {
      POSMessageDialog.showError(window, e.getMessage(), e);
    }
  }
}
