package com.orostock.inventory.action;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.ui.InventoryUnitsBrowser;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;

public class InventoryUnitFormAction
  extends AbstractAction
{
  public InventoryUnitFormAction()
  {
    super("Units");
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    try {
      JTabbedPane tabbedPane = window.getTabbedPane();
      InventoryUnitsBrowser browser = null;
      
      int index = tabbedPane.indexOfTab("Units");
      if (index == -1) {
        browser = new InventoryUnitsBrowser();
        tabbedPane.addTab("Units", browser);
      }
      else {
        browser = (InventoryUnitsBrowser)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(browser);
    } catch (Exception e) {
      POSMessageDialog.showError(window, e.getMessage(), e);
    }
  }
}
