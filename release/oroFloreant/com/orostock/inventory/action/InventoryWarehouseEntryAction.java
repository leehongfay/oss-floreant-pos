package com.orostock.inventory.action;

import com.floreantpos.model.InventoryWarehouse;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.ui.InventoryWarehouseEntryForm;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

public class InventoryWarehouseEntryAction
  extends AbstractAction
{
  public InventoryWarehouseEntryAction()
  {
    super("New Inventory Warehouse");
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    try {
      InventoryWarehouseEntryForm form = new InventoryWarehouseEntryForm(new InventoryWarehouse());
      BeanEditorDialog dialog = new BeanEditorDialog(form);
      dialog.pack();
      dialog.open();
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    }
  }
}
