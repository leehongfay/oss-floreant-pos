package com.orostock.inventory;

import com.floreantpos.Messages;
import com.floreantpos.PosLog;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.extension.InventoryPlugin;
import com.floreantpos.main.Main;
import com.floreantpos.main.PosWindow;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.MenuItem;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.util.POSUtil;
import com.orocube.common.about.AboutPluginAction;
import com.orocube.common.util.ProductInfo;
import com.orostock.inventory.action.InventoryAverageUnitCostReportAction;
import com.orostock.inventory.action.InventoryConfigurationAction;
import com.orostock.inventory.action.InventoryItemBrowserAction;
import com.orostock.inventory.action.InventoryLocationEntryAction;
import com.orostock.inventory.action.InventoryOnHandReportAction;
import com.orostock.inventory.action.InventoryOnHandReportNewAction;
import com.orostock.inventory.action.InventoryPrintLabelAction;
import com.orostock.inventory.action.InventoryStockEntryAction;
import com.orostock.inventory.action.InventoryTransactionEntryAction;
import com.orostock.inventory.action.InventoryTransactionReportAction;
import com.orostock.inventory.action.InventoryUnitGroupTAction;
import com.orostock.inventory.action.InventoryVendorEntryAction;
import com.orostock.inventory.action.PackagingUnitFormAction;
import com.orostock.inventory.action.PurchaseOrderExplorerAction;
import com.orostock.inventory.action.RecipeBrowserAction;
import com.orostock.inventory.action.ShoppingListAction;
import com.orostock.inventory.action.StockCountExplorerAction;
import com.orostock.inventory.ui.InventoryUnitForm;
import com.orostock.inventory.ui.MenuItemPackagingUnitBrowser;
import java.awt.Component;
import java.net.URL;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import net.xeoh.plugins.base.annotations.PluginImplementation;














@PluginImplementation
public class OrocubeInventoryPlugin
  extends InventoryPlugin
  implements ProductInfo
{
  public static final String PRODUCT_NAME = "Inventory";
  public static final String PRODUCT_VERSION = "1.1.29";
  
  public OrocubeInventoryPlugin() {}
  
  public void initUI(PosWindow posWindow) {}
  
  public void addRecepieView(JTabbedPane tabbedPane) {}
  
  public void addStockUnitView(JTabbedPane tabbedPane, MenuItem menuItem)
  {
    tabbedPane.addTab("Packaging & Recipe Unit", new MenuItemPackagingUnitBrowser(menuItem));
  }
  
  public void showInventoryUnitEntryDialog(InventoryUnit unit)
  {
    InventoryUnitForm editor = new InventoryUnitForm(unit);
    BeanEditorDialog dialog = new BeanEditorDialog(editor);
    dialog.open();
    
    if (dialog.isCanceled()) {}
  }
  


  public void initBackoffice(BackOfficeWindow backOfficeWindow)
  {
    JMenuBar menuBar = backOfficeWindow.getBackOfficeMenuBar();
    



    JMenu inventoryMenu = new JMenu(Messages.getString("INVENTORY_MENU_TEXT"));
    
    inventoryMenu.add(new InventoryItemBrowserAction());
    inventoryMenu.add(new RecipeBrowserAction());
    inventoryMenu.add(new InventoryStockEntryAction());
    inventoryMenu.add(new InventoryTransactionEntryAction());
    inventoryMenu.add(new ShoppingListAction());
    inventoryMenu.add(new PurchaseOrderExplorerAction());
    inventoryMenu.add(new StockCountExplorerAction());
    inventoryMenu.add(new InventoryVendorEntryAction());
    inventoryMenu.add(new InventoryLocationEntryAction());
    inventoryMenu.add(new InventoryUnitGroupTAction());
    inventoryMenu.add(new PackagingUnitFormAction());
    inventoryMenu.add(new InventoryPrintLabelAction());
    inventoryMenu.add(new InventoryConfigurationAction());
    inventoryMenu.add(new JSeparator());
    inventoryMenu.add(new InventoryOnHandReportAction());
    inventoryMenu.add(new InventoryTransactionReportAction());
    inventoryMenu.add(new InventoryAverageUnitCostReportAction());
    inventoryMenu.add(new InventoryOnHandReportNewAction());
    inventoryMenu.add(new JSeparator());
    inventoryMenu.add(new AboutPluginAction(this, getLicense(), POSUtil.getBackOfficeWindow(), this));
    
    menuBar.add(inventoryMenu);
  }
  










  public String getId()
  {
    return String.valueOf("DefaultOroStockPlugin".hashCode());
  }
  


  public void initConfigurationView(JDialog dialog) {}
  

  public List<AbstractAction> getSpecialFunctionActions()
  {
    return null;
  }
  
  public void restartPOS(boolean restart)
  {
    if (restart) {
      try {
        Main.restart();
      } catch (Exception e) {
        PosLog.error(getClass(), e);
      }
    }
  }
  
  public Component getParent()
  {
    return POSUtil.getFocusedWindow();
  }
  
  public boolean requireLicense()
  {
    return true;
  }
  
  public String getProductName()
  {
    return "Inventory";
  }
  
  public String getProductVersion()
  {
    return "1.1.29";
  }
  
  public URL getChangeLogURL()
  {
    return getClass().getResource("/change.log.xml");
  }
}
