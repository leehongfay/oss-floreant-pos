package com.orostock.inventory.ui;

import com.floreantpos.config.AppProperties;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.StockCount;
import com.floreantpos.model.StockCountItem;
import com.floreantpos.report.ReportUtil;
import com.floreantpos.services.report.ReportService;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.util.NumberUtil;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.swing.JPanel;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;
















public class StockCountReport
{
  private StockCountReportModel itemReportModel;
  private StockCount stockCount;
  
  public StockCountReport(StockCount stockCount)
  {
    this.stockCount = stockCount;
  }
  
  public void showReport() throws Exception {
    createModels();
    
    HashMap map = new HashMap();
    ReportUtil.populateRestaurantProperties(map);
    map.put("reportTitle", "STOCK COUNT CHECK SHEET");
    map.put("reportDate", ReportService.formatFullDate(new Date()));
    map.put("line1", "  Count Date: " + ReportService.formatFullDate(stockCount.getCreatedDate()));
    map.put("line2", "  Counted By: " + stockCount.getUser());
    
    map.put("txt1", "SKU");
    map.put("txt2", "ITEM");
    map.put("txt3", "LOCATION");
    map.put("txt4", "UNIT");
    map.put("txt5", "ON HAND");
    map.put("txt6", "COUNTED");
    map.put("txt7", "COUNT VAR");
    map.put("txt8", "COST");
    map.put("txt9", "COST VAR");
    
    map.put("signature1", "Counted By");
    map.put("signature2", "Verified By");
    
    JasperReport masterReport = ReportUtil.getReport("item_count_report");
    JasperPrint print = JasperFillManager.fillReport(masterReport, map, new JRTableModelDataSource(itemReportModel));
    
    JRViewer viewer = new JRViewer(print);
    OkCancelOptionDialog dialog = new OkCancelOptionDialog()
    {

      public void doOk() {}

    };
    dialog.setDefaultCloseOperation(2);
    dialog.setTitle(AppProperties.getAppName());
    dialog.setCaption("Stock Count Check Sheet");
    dialog.getContentPanel().add(viewer);
    dialog.setOkButtonVisible(false);
    dialog.openFullScreen();
  }
  
  public void createModels() {
    itemReportModel = new StockCountReportModel();
    itemReportModel.setRows(stockCount.getCountItems());
  }
  
  public class StockCountReportModel extends ListTableModel<StockCountItem> {
    SimpleDateFormat dateFormat2 = new SimpleDateFormat("MMM-dd-yyyy hh:mm a");
    DecimalFormat decimalFormat = new DecimalFormat("0.00");
    
    public StockCountReportModel() {
      super();
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      StockCountItem data = (StockCountItem)rows.get(rowIndex);
      double countVariance = data.getUnitOnHand().doubleValue() - data.getActualUnit().doubleValue();
      switch (columnIndex) {
      case 0: 
        return data.getSku();
      
      case 1: 
        return data.getName();
      
      case 2: 
        return data.getInventoryLocation().getName();
      
      case 3: 
        return data.getUnit();
      
      case 4: 
        return NumberUtil.formatNumber(data.getUnitOnHand(), true);
      
      case 5: 
        return NumberUtil.formatNumber(data.getActualUnit(), true);
      case 6: 
        return NumberUtil.formatNumber(Double.valueOf(countVariance), true);
      case 7: 
        return NumberUtil.formatNumber(Double.valueOf(getLastPurchasingCost(data)), true);
      case 8: 
        return NumberUtil.formatNumber(Double.valueOf(countVariance * getLastPurchasingCost(data)), true);
      }
      return null;
    }
  }
  
  public double getLastPurchasingCost(StockCountItem data) {
    double lastPurchasedCost = data.getMenuItem().getLastPurchasedCost().doubleValue();
    if (lastPurchasedCost == 0.0D) {
      lastPurchasedCost = data.getMenuItem().getCost().doubleValue();
    }
    return lastPurchasedCost;
  }
}
