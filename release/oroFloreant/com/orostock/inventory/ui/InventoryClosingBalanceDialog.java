package com.orostock.inventory.ui;

import com.floreantpos.PosException;
import com.floreantpos.model.dao.InventoryClosingBalanceDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;


public class InventoryClosingBalanceDialog
  extends OkCancelOptionDialog
{
  private JComboBox<String> cbMonth;
  private JComboBox<Integer> cbYear = new JComboBox();
  private String[] months = { "Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec" };
  
  public InventoryClosingBalanceDialog() {
    setCaption("Generate closing balance");
    
    cbMonth = new JComboBox(months);
    
    Calendar currentDate = Calendar.getInstance();
    currentDate.setTime(new Date());
    populateYearData(currentDate);
    cbMonth.setSelectedItem(months[currentDate.get(2)]);
    cbYear.setSelectedItem(Integer.valueOf(currentDate.get(1)));
    
    JPanel inputPanel = new JPanel(new MigLayout("wrap 3,fillx", "[][grow][]", ""));
    inputPanel.add(new JLabel("Closing up to "));
    inputPanel.add(cbMonth, "growx");
    inputPanel.add(cbYear, "");
    
    getContentPanel().add(inputPanel);
  }
  
  private void populateYearData(Calendar currentDate)
  {
    Date transactionStartDate = InventoryClosingBalanceDAO.getInstance().getFirstInventoryTransactionDate();
    if (transactionStartDate == null) {
      transactionStartDate = new Date();
    }
    Calendar c = Calendar.getInstance();
    c.setTime(transactionStartDate);
    
    for (int i = c.get(1); i < currentDate.get(1) + 1; i++) {
      cbYear.addItem(Integer.valueOf(i));
    }
    if (currentDate.get(2) == 11) {
      cbYear.addItem(Integer.valueOf(currentDate.get(1) + 1));
    }
  }
  
  public void doOk()
  {
    try {
      int month = cbMonth.getSelectedIndex();
      
      Calendar c = Calendar.getInstance();
      c.set(2, month);
      c.set(1, ((Integer)cbYear.getSelectedItem()).intValue());
      
      Date closingMonth = DateUtil.startOfMonth(c.getTime());
      
      c.set(2, month + 1);
      Date closingdate = DateUtil.startOfMonth(c.getTime());
      
      InventoryClosingBalanceDAO dao = new InventoryClosingBalanceDAO();
      dao.closeStock(closingMonth, closingdate);
      dispose();
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Inventory closing for month " + new SimpleDateFormat("MMM, yyyy")
        .format(Long.valueOf(closingMonth.getTime())) + " successfully complete.");
    } catch (PosException e) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), e.getMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage(), e);
    }
  }
}
