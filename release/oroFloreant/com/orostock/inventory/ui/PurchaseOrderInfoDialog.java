package com.orostock.inventory.ui;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.mailservices.MailService;
import com.floreantpos.main.Application;
import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.PosPrinters;
import com.floreantpos.model.PurchaseOrder;
import com.floreantpos.model.PurchaseOrderItem;
import com.floreantpos.model.dao.PurchaseOrderDAO;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosOptionPane;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.TicketReceiptView;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.util.ByteArrayDataSource;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;















public class PurchaseOrderInfoDialog
  extends POSDialog
{
  private List<PurchaseOrder> purchaseOrders;
  private JPanel reportPanel;
  private boolean reorder = false;
  private PosButton btnReOrder;
  private PosButton btnPrint;
  private JPanel reportView;
  private PosButton btnEmail;
  
  public PurchaseOrderInfoDialog(List<PurchaseOrder> purchaseOrders) {
    super(POSUtil.getBackOfficeWindow());
    setTitle(Messages.getString("OrderInfoDialog.0"));
    this.purchaseOrders = purchaseOrders;
    createReportView();
  }
  
  public void createReportView() {
    reportView = new JPanel(new BorderLayout());
    reportPanel = new JPanel(new MigLayout("wrap 1, ax 50%", "", ""));
    PosScrollPane scrollPane = new PosScrollPane(reportPanel);
    scrollPane.getVerticalScrollBar().setUnitIncrement(20);
    try {
      createReport();
    } catch (Exception e1) {
      PosLog.error(PurchaseOrderInfoDialog.class, e1.getMessage(), e1);
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Error occured!");
    }
    setLayout(new BorderLayout());
    reportView.add(scrollPane);
    add(reportView);
    JPanel panel = new JPanel();
    getContentPane().add(panel, "South");
    
    btnReOrder = new PosButton("Reorder");
    btnReOrder.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setCanceled(true);
        dispose();
        Iterator iter = purchaseOrders.iterator(); if (iter.hasNext()) {
          PurchaseOrder purchaseOrder = (PurchaseOrder)iter.next();
          PurchaseOrderInfoDialog.this.createReOrder(purchaseOrder);
        }
        
      }
    });
    panel.add(btnReOrder);
    
    btnEmail = new PosButton("Email");
    btnEmail.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          PurchaseOrderInfoDialog.this.sendOrders();
        } catch (Exception e1) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Sending failed.");
        }
      }
    });
    panel.add(btnEmail);
    
    btnPrint = new PosButton("Print");
    btnPrint.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          print();
        } catch (Exception e1) {
          BOMessageDialog.showError(e1);
        }
      }
    });
    panel.add(btnPrint);
    
    PosButton btnClose = new PosButton();
    btnClose.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        dispose();
      }
    });
    btnClose.setText(Messages.getString("OrderInfoDialog.2"));
    panel.add(btnClose);
  }
  
  private void sendOrders() throws Exception {
    for (Iterator iter = purchaseOrders.iterator(); iter.hasNext();) {
      PurchaseOrder purchaseOrder = (PurchaseOrder)iter.next();
      try {
        JasperPrint jasperPrint = ReceiptPrintService.createPurchaseOrderPrint(purchaseOrder);
        jasperPrint.setName("PURCHASE_ORDER_" + purchaseOrder.getOrderId());
        jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
        doEmail(jasperPrint, purchaseOrder);
      } catch (Exception e) {
        throw e;
      }
    }
  }
  
  public void doEmail(JasperPrint report, PurchaseOrder purchaseOrder) {
    String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    String email = PosOptionPane.showInputDialog("Enter Email Address", purchaseOrder.getVendor().getEmail());
    if (email == null) {
      return;
    }
    Pattern pattern = Pattern.compile(EMAIL_PATTERN);
    Matcher matcher = pattern.matcher(email);
    if (matcher.matches()) {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      
      JRPdfExporter exporter = new JRPdfExporter();
      exporter.setParameter(JRPdfExporterParameter.JASPER_PRINT, report);
      exporter.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
      try {
        exporter.exportReport();
      } catch (JRException e) {
        PosLog.error(getClass(), e);
      }
      byte[] bytes = byteArrayOutputStream.toByteArray();
      ByteArrayDataSource bads = new ByteArrayDataSource(bytes, "application/pdf");
      MailService.sendMail(email, "Purchase Order", "Purchase Order", "PurchaseOrder_" + purchaseOrder.getOrderId() + ".pdf", bads);
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Purchase order successfully sent to vendor email.");
    }
    else {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Invalid email! Please enter valid email.");
      doEmail(report, purchaseOrder);
    }
  }
  
  public void createReport() throws Exception {
    for (int i = 0; i < purchaseOrders.size(); i++) {
      PurchaseOrder purchaseOrder = (PurchaseOrder)purchaseOrders.get(i);
      JasperPrint jasperPrint = ReceiptPrintService.createPurchaseOrderPrint(purchaseOrder);
      TicketReceiptView receiptView = new TicketReceiptView(jasperPrint);
      reportPanel.add(receiptView.getReportPanel());
    }
  }
  
  public void print() throws Exception {
    for (Iterator iter = purchaseOrders.iterator(); iter.hasNext();) {
      PurchaseOrder purchaseOrder = (PurchaseOrder)iter.next();
      try {
        JasperPrint jasperPrint = ReceiptPrintService.createPurchaseOrderPrint(purchaseOrder);
        jasperPrint.setName("PURCHASE_ORDER_" + purchaseOrder.getOrderId());
        jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
        ReceiptPrintService.printQuitely(jasperPrint);
      } catch (Exception e) {
        throw e;
      }
    }
  }
  
  public List<PurchaseOrder> getPurchaseOrders() {
    return purchaseOrders;
  }
  
  public JPanel getReportPanel() {
    return reportPanel;
  }
  
  private void createReOrder(PurchaseOrder purchaseOrder) {
    PurchaseOrder pOrder = new PurchaseOrder();
    pOrder.setProperties(purchaseOrder.getProperties());
    pOrder.setTerminal(Application.getInstance().getTerminal());
    

    Calendar currentTime = Calendar.getInstance();
    pOrder.setCreatedDate(currentTime.getTime());
    
    List<PurchaseOrderItem> newPurchaseOrderItems = new ArrayList();
    for (PurchaseOrderItem oldPurchaseOrderItem : purchaseOrder.getOrderItems()) {
      PurchaseOrderItem newPurchaseOrderItem = new PurchaseOrderItem();
      newPurchaseOrderItem.setItemQuantity(oldPurchaseOrderItem.getItemQuantity());
      newPurchaseOrderItem.setMenuItemId(oldPurchaseOrderItem.getMenuItemId());
      newPurchaseOrderItem.setName(oldPurchaseOrderItem.getName());
      newPurchaseOrderItem.setGroupName(oldPurchaseOrderItem.getGroupName());
      newPurchaseOrderItem.setCategoryName(oldPurchaseOrderItem.getCategoryName());
      newPurchaseOrderItem.setUnitPrice(oldPurchaseOrderItem.getUnitPrice());
      newPurchaseOrderItem.setItemUnitName(oldPurchaseOrderItem.getItemUnitName());
      newPurchaseOrderItem.setTaxRate(oldPurchaseOrderItem.getTaxRate());
      newPurchaseOrderItem.setBeverage(oldPurchaseOrderItem.isBeverage());
      newPurchaseOrderItem.setPurchaseOrder(pOrder);
      pOrder.addToorderItems(newPurchaseOrderItem);
    }
    doCreatePurchaseOrder(pOrder);
    reorder = true;
  }
  
  private void doCreatePurchaseOrder(PurchaseOrder pOrder) {
    try {
      final PurchaseOrderForm editor = new PurchaseOrderForm(pOrder);
      final BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      
      PosButton btnSaveAndReceiveOrder = new PosButton("<html><center>SAVE &<br> RECEIVE</center></html>");
      btnSaveAndReceiveOrder.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          if (!editor.updateModel())
            return;
          PurchaseOrder purchaseOrder = (PurchaseOrder)editor.getBean();
          for (PurchaseOrderItem item : purchaseOrder.getOrderItems()) {
            item.setQuantityToReceive(Double.valueOf(item.getItemQuantity().doubleValue() - item.getQuantityReceived().doubleValue()));
          }
          PurchaseOrderDAO.getInstance().saveOrUpdate(purchaseOrder, true);
          dialog.dispose();
        }
      });
      PosButton btnOk = (PosButton)dialog.getButtonPanel().getComponent(0);
      btnOk.setText("SAVE");
      dialog.getButtonPanel().add(btnSaveAndReceiveOrder, 0);
      dialog.openWithScale(830, 630);
      if (dialog.isCanceled()) {
        return;
      }
      PurchaseOrder purchaseOrder = (PurchaseOrder)editor.getBean();
      purchaseOrders.add(purchaseOrder);
      showMessage(purchaseOrder);
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void showMessage(PurchaseOrder purchaseOrder) throws Exception {
    try {
      if (POSMessageDialog.showMessageAndPromtToPrint(POSUtil.getBackOfficeWindow(), "Successfully " + purchaseOrder
        .getOrderId() + " purchase order has been created .")) {
        JasperPrint jasperPrint = ReceiptPrintService.createPurchaseOrderPrint(purchaseOrder);
        jasperPrint.setName("PURCHASE_ORDER_" + purchaseOrder.getOrderId());
        jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
        ReceiptPrintService.printQuitely(jasperPrint);
      }
    } catch (Exception e) {
      throw e;
    }
  }
  
  public boolean isReorder()
  {
    return reorder;
  }
}
