package com.orostock.inventory.ui;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.explorer.ExplorerButtonPanel;
import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.InventoryTransactionType;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.dao.InventoryVendorDAO;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.BeanTableModel.DataType;
import com.floreantpos.swing.MultiSelectComboBox;
import com.floreantpos.swing.PosTable;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.inv.InventoryStockInForm;
import com.floreantpos.ui.inv.InventoryTransactionEntryForm;
import com.floreantpos.ui.model.MenuItemForm;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.dao.InventoryDAO;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;
import org.hibernate.StaleObjectStateException;
import org.jdesktop.swingx.JXTable;




















public class InventoryItemExplorer
  extends TransparentPanel
{
  private JXTable table;
  private InventoryItemExplorerModel tableModel;
  private JComboBox<MenuGroup> cbGroups;
  private MultiSelectComboBox<InventoryVendor> cbVendors;
  private JTextField tfName;
  private JButton btnBack;
  private JButton btnForward;
  private JLabel lblNumberOfItem;
  
  public InventoryItemExplorer()
  {
    init();
    showInventoryItems();
  }
  
  private void init() {
    tableModel = new InventoryItemExplorerModel();
    tableModel.addColumn(Messages.getString("MenuItemExplorer.9"), "sku");
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "displayName");
    tableModel.addColumn("COST", "cost", 11, BeanTableModel.DataType.MONEY);
    tableModel.addColumn(Messages.getString("MenuItemExplorer.14"), "availableUnit", 11, null);
    tableModel.addColumn(Messages.getString("MenuItemExplorer.26"), "unitOnHand", 11, null);
    tableModel.addColumn("UNIT", "unit");
    tableModel.addColumn("VENDOR", "vendorNames");
    
    table = new PosTable(tableModel);
    
    btnBack = new JButton("<<< Previous");
    btnForward = new JButton("Next >>>");
    lblNumberOfItem = new JLabel();
    
    btnBack.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        InventoryItemExplorer.this.showInventoryItems(tableModel.getPreviousRowIndex());
      }
      
    });
    btnForward.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        InventoryItemExplorer.this.showInventoryItems(tableModel.getNextRowIndex());
      }
      
    });
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    add(createButtonPanel(), "South");
    add(buildSearchForm(), "North");
    resizeColumnWidth(table);
  }
  
  private JPanel buildSearchForm()
  {
    JPanel panel = new JPanel();
    panel.setLayout(new MigLayout("", "[][]15[][]15[][]15[]", "[]5[]"));
    try {
      JLabel lblGroup = new JLabel("Group:");
      cbGroups = new JComboBox();
      cbGroups.addItem(null);
      cbGroups.addItemListener(new ItemListener()
      {
        public void itemStateChanged(ItemEvent e)
        {
          InventoryItemExplorer.this.showInventoryItems();
        }
      });
      List<MenuGroup> menuGroups = MenuGroupDAO.getInstance().findGroupsWithInventoryItems();
      
      for (MenuGroup menuGroup : menuGroups) {
        cbGroups.addItem(menuGroup);
      }
      
      cbVendors = new MultiSelectComboBox();
      cbVendors.addItemListener(new ItemListener()
      {
        public void itemStateChanged(ItemEvent e)
        {
          InventoryItemExplorer.this.showInventoryItems();
        }
      });
      Object inventoryVendors = InventoryVendorDAO.getInstance().findAll();
      cbVendors.setItems((List)inventoryVendors);
      JLabel lblName = new JLabel(Messages.getString("MenuItemExplorer.0"));
      tfName = new JTextField(15);
      
      JButton searchBttn = new JButton(Messages.getString("MenuItemExplorer.3"));
      JButton btnClear = new JButton("Clear");
      btnClear.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          tfName.setText("");
          cbGroups.setSelectedItem(null);
          cbVendors.setSelectedItem(null);
          InventoryItemExplorer.this.showInventoryItems();
        }
      });
      panel.add(lblName, "align label");
      panel.add(tfName);
      panel.add(lblGroup);
      panel.add(cbGroups);
      panel.add(new JLabel("Vendor: "));
      panel.add(cbVendors);
      panel.add(searchBttn);
      panel.add(btnClear);
      
      Border loweredetched = BorderFactory.createEtchedBorder(1);
      TitledBorder title = BorderFactory.createTitledBorder(loweredetched, Messages.getString("MenuItemExplorer.30"));
      title.setTitleJustification(1);
      panel.setBorder(title);
      
      searchBttn.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          InventoryItemExplorer.this.showInventoryItems();
        }
      });
      tfName.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          InventoryItemExplorer.this.showInventoryItems();
        }
        
      });
      tfName.addKeyListener(new KeyAdapter()
      {
        public void keyReleased(KeyEvent e) {
          String searchItem = tfName.getText();
          if ((StringUtils.isNotEmpty(searchItem)) && (searchItem.length() > 3)) {
            InventoryItemExplorer.this.showInventoryItems();
          }
        }
      });
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
    return panel;
  }
  
  private void showInventoryItems() {
    showInventoryItems(0);
  }
  
  private void showInventoryItems(int rowIndex) {
    tableModel.setCurrentRowIndex(rowIndex);
    Object selectedType = cbGroups.getSelectedItem();
    MenuGroup menuGroup = null;
    if ((selectedType instanceof MenuGroup)) {
      menuGroup = (MenuGroup)selectedType;
    }
    
    List<InventoryVendor> vendors = cbVendors.getSelectedItems();
    
    String txName = tfName.getText();
    InventoryDAO.getInstance().loadInventoryItems(tableModel, menuGroup, txName, vendors);
    int startNumber = tableModel.getCurrentRowIndex() + 1;
    int endNumber = tableModel.getNextRowIndex();
    int totalNumber = tableModel.getNumRows();
    if (endNumber > totalNumber) {
      endNumber = totalNumber;
    }
    lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
    btnBack.setEnabled(tableModel.hasPrevious());
    btnForward.setEnabled(tableModel.hasNext());
  }
  
  private JPanel createButtonPanel() {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton editButton = explorerButton.getEditButton();
    JButton addButton = explorerButton.getAddButton();
    JButton deleteButton = explorerButton.getDeleteButton();
    
    addButton.setText(Messages.getString("MenuItemExplorer.17"));
    editButton.setText(Messages.getString("MenuItemExplorer.18"));
    deleteButton.setText(Messages.getString("MenuItemExplorer.19"));
    
    JButton updateStockAmount = new JButton("UPDATE STOCK AMOUNT");
    updateStockAmount.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        InventoryItemExplorer.this.doUpdateStockAmount();
      }
      
    });
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        InventoryItemExplorer.this.editSelectedRow();
      }
    });
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        InventoryItemExplorer.this.doCreateNewMenuItem();
      }
      

    });
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        InventoryItemExplorer.this.doDeleteMenuItem();
      }
      

    });
    JPanel bottomPanel = new JPanel(new MigLayout("fillx", "[][fill]"));
    TransparentPanel actionButtonPanel = new TransparentPanel();
    actionButtonPanel.add(addButton);
    actionButtonPanel.add(editButton);
    actionButtonPanel.add(updateStockAmount);
    actionButtonPanel.add(deleteButton);
    addInventoryButtonActions(actionButtonPanel);
    bottomPanel.add(actionButtonPanel, "");
    
    JPanel navigationPanel = new JPanel(new FlowLayout(4));
    navigationPanel.add(lblNumberOfItem);
    navigationPanel.add(btnBack);
    navigationPanel.add(btnForward);
    bottomPanel.add(navigationPanel, "grow");
    
    return bottomPanel;
  }
  
  private void addInventoryButtonActions(JPanel panel) {
    JButton inTransactionButton = new JButton("IN");
    JButton outTransactionButton = new JButton("OUT");
    JButton transferTransactionButton = new JButton("TRANSFER");
    inTransactionButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        InventoryItemExplorer.this.doShowStockInDialog(InventoryTransactionType.IN);
      }
      
    });
    outTransactionButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        InventoryItemExplorer.this.doAdjustStock(InventoryTransactionType.OUT, false);
      }
      
    });
    transferTransactionButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        InventoryItemExplorer.this.doAdjustStock(InventoryTransactionType.UNCHANGED, true);
      }
    });
    panel.add(inTransactionButton);
    panel.add(outTransactionButton);
    panel.add(transferTransactionButton);
  }
  
  private boolean doAdjustStock(InventoryTransactionType type, boolean transfer) {
    int index = table.getSelectedRow();
    if (index < 0) {
      return false;
    }
    MenuItem menuItem = (MenuItem)tableModel.getRow(index);
    MenuItemDAO.getInstance().initialize(menuItem);
    
    InventoryTransaction inventoryTransaction = new InventoryTransaction();
    inventoryTransaction.setTransactionType(type);
    if (transfer) {
      inventoryTransaction.setReason("TRANSFER");
    }
    


    inventoryTransaction.setMenuItem(menuItem);
    InventoryTransactionEntryForm inventoryTransactionEntryForm = new InventoryTransactionEntryForm(inventoryTransaction);
    
    BeanEditorDialog dialog = new BeanEditorDialog(inventoryTransactionEntryForm);
    dialog.setPreferredSize(PosUIManager.getSize(500, 500));
    dialog.open();
    
    if (dialog.isCanceled()) {
      return false;
    }
    showInventoryItems();
    return true;
  }
  
  private boolean doShowStockInDialog(InventoryTransactionType type) {
    int index = table.getSelectedRow();
    MenuItem menuItem = null;
    if (index >= 0) {
      menuItem = (MenuItem)tableModel.getRow(index);
      MenuItemDAO.getInstance().initialize(menuItem);
    }
    InventoryTransaction inventoryTransaction = new InventoryTransaction();
    inventoryTransaction.setTransactionType(type);
    
    InventoryStockInForm editor = null;
    if (menuItem == null) {
      editor = new InventoryStockInForm(inventoryTransaction);
    }
    else {
      inventoryTransaction.setMenuItem(menuItem);
      InventoryUnit unit = menuItem.getUnit();
      if (unit == null) {
        POSMessageDialog.showError(this, "No unit is set for the item. Please set item unit first.");
        return false;
      }
      inventoryTransaction.setUnit(unit.getUniqueCode());
      Double replenishLevel = menuItem.getReplenishLevel();
      inventoryTransaction.setQuantity(replenishLevel);
      Double cost = menuItem.getCost();
      inventoryTransaction.setUnitCost(cost);
      inventoryTransaction.setTotal(Double.valueOf(replenishLevel.doubleValue() * cost.doubleValue()));
      editor = new InventoryStockInForm(inventoryTransaction);
    }
    BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
    
    dialog.openWithScale(830, 630);
    if (dialog.isCanceled())
      return false;
    showInventoryItems();
    return true;
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(250));
    columnWidth.add(Integer.valueOf(60));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(80));
    columnWidth.add(Integer.valueOf(80));
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(140));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(200));
    
    return columnWidth;
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      MenuItem menuItem = (MenuItem)tableModel.getRow(index);
      MenuItemDAO.getInstance().initialize(menuItem);
      
      tableModel.setRow(index, menuItem);
      
      MenuItemForm editor = new MenuItemForm(menuItem);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.openFullScreen();
      if (dialog.isCanceled()) {
        return;
      }
      if (!menuItem.isInventoryItem().booleanValue())
        tableModel.removeRow(menuItem);
      table.repaint();
      showInventoryItems();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doUpdateStockAmount() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), Messages.getString("MenuItemExplorer.7"));
        return;
      }
      
      MenuItem menuItem = (MenuItem)tableModel.getRow(index);
      String amountString = JOptionPane.showInputDialog(POSUtil.getBackOfficeWindow(), Messages.getString("MenuItemExplorer.8"), menuItem
        .getAvailableUnit());
      
      if ((amountString == null) || (amountString.equals(""))) {
        return;
      }
      double stockAmount = Double.parseDouble(amountString);
      
      if (stockAmount < 0.0D) {
        POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), Messages.getString("MenuItemExplorer.10"));
        return;
      }
      
      menuItem.setAvailableUnit(Double.valueOf(stockAmount));
      MenuItemDAO.getInstance().saveOrUpdate(menuItem);
      table.repaint();
      showInventoryItems();
    } catch (NumberFormatException e1) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), Messages.getString("MenuItemExplorer.11"));
      return;
    } catch (Exception e2) {
      BOMessageDialog.showError(POSUtil.getBackOfficeWindow(), POSConstants.ERROR_MESSAGE, e2);
      return;
    }
  }
  
  private void doCreateNewMenuItem() {
    try {
      MenuItem menuItem = new MenuItem();
      
      Object selectedType = cbGroups.getSelectedItem();
      
      if ((selectedType instanceof OrderType)) {
        List types = new ArrayList();
        types.add((OrderType)selectedType);
        menuItem.setOrderTypeList(types);
      }
      
      MenuItemForm editor = new MenuItemForm(menuItem);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.openFullScreen();
      
      if (dialog.isCanceled()) {
        return;
      }
      MenuItem foodItem = (MenuItem)editor.getBean();
      
      if (foodItem.isInventoryItem().booleanValue())
        tableModel.addRow(foodItem);
      showInventoryItems();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doDeleteMenuItem() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
        return;
      }
      MenuItem item = (MenuItem)tableModel.getRow(index);
      
      MenuItemDAO foodItemDAO = new MenuItemDAO();
      if (item.getDiscounts().size() > 0) {
        foodItemDAO.releaseParentAndDelete(item);
      }
      else {
        foodItemDAO.delete(item);
      }
      
      tableModel.removeRow(index);
      showInventoryItems();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  class InventoryItemExplorerModel extends BeanTableModel<MenuItem>
  {
    public InventoryItemExplorerModel() {
      super();
    }
    
    public void setValueAt(Object value, int rowIndex, int columnIndex)
    {
      try {
        if (columnIndex != 2) {
          return;
        }
        int index = table.getSelectedRow();
        if (index < 0) {
          return;
        }
        index = table.convertRowIndexToModel(index);
        
        MenuItem menuItem = (MenuItem)tableModel.getRow(index);
        MenuItemDAO.getInstance().initialize(menuItem);
        
        String priceString = (String)value;
        if (priceString.isEmpty())
          return;
        double price = Double.parseDouble(priceString);
        menuItem.setPrice(Double.valueOf(price));
        MenuItemDAO.getInstance().saveOrUpdate(menuItem);
      } catch (StaleObjectStateException e) {
        POSMessageDialog.showError(InventoryItemExplorer.this, "This item has been modified by another person. Please reload this view and try again");
      } catch (Exception e) {
        POSMessageDialog.showError(InventoryItemExplorer.this, e.getMessage(), e);
      }
    }
  }
}
