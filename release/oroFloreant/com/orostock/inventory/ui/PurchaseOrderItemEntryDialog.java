package com.orostock.inventory.ui;

import com.floreantpos.config.AppProperties;
import com.floreantpos.model.IUnit;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.PurchaseOrderItem;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;




public class PurchaseOrderItemEntryDialog
  extends OkCancelOptionDialog
{
  private Component lblQuantity;
  private PurchaseOrderItem orderItem;
  private JLabel lblPurchaseOrderItemType;
  private JComboBox cbInventoryReasons;
  private JLabel lblUnitPrice;
  private DoubleTextField tfUnitPrice;
  private DoubleTextField tfQuantity;
  private POSTextField tfSku;
  private JButton btnSearchItem;
  private POSTextField tfSearchString;
  private JComboBox cbUnits;
  private JLabel lblOrderId;
  private JComboBox cbItems;
  
  public PurchaseOrderItemEntryDialog(PurchaseOrderItem orderItem)
  {
    this.orderItem = orderItem;
    initComponents();
    cbItems.setModel(new ComboBoxModel());
    cbItems.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if (e.getStateChange() != 1)
          return;
        MenuItem menuItem = (MenuItem)cbItems.getSelectedItem();
        ComboBoxModel aModel = (ComboBoxModel)cbUnits.getModel();
        if (menuItem != null) {
          MenuItemDAO.getInstance().initialize(menuItem);
          aModel.setDataList(menuItem.getUnits());
          InventoryUnit unit = menuItem.getUnit();
          Iterator iterator; if (unit != null) {
            for (iterator = aModel.getDataList().iterator(); iterator.hasNext();) {
              IUnit iUnit = (IUnit)iterator.next();
              if (iUnit.getUniqueCode().equals(unit.getUniqueCode())) {
                cbUnits.setSelectedItem(iUnit);
              }
              
            }
            
          } else if (aModel.getSize() > 0) {
            cbUnits.setSelectedIndex(0);
          }
          PurchaseOrderItemEntryDialog.this.updatePrice();
        }
      }
    });
    ComboBoxModel aModel = new ComboBoxModel();
    cbUnits.setModel(aModel);
    loadUnits();
    cbUnits.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        if (e.getStateChange() != 1)
          return;
        PurchaseOrderItemEntryDialog.this.updatePrice();
      }
    });
    updateView();
  }
  
  private void updatePrice() {
    MenuItem menuItem = (MenuItem)cbItems.getSelectedItem();
    if (menuItem == null)
      return;
    IUnit selectedUnit = (IUnit)cbUnits.getSelectedItem();
    if (selectedUnit != null) {
      tfUnitPrice.setText(NumberUtil.formatNumber(menuItem.getCost(selectedUnit)));
    }
    tfQuantity.setText(String.valueOf(menuItem.getReplenishLevel().doubleValue() == 0.0D ? 1.0D : menuItem.getReplenishLevel().doubleValue()));
  }
  
  private void loadUnits() {
    MenuItem menuItem = (MenuItem)cbItems.getSelectedItem();
    if (menuItem == null)
      return;
    ComboBoxModel model = (ComboBoxModel)cbUnits.getModel();
    model.removeAllElements();
    model.setDataList(menuItem.getUnits());
  }
  
  private void initComponents() {
    setTitle(AppProperties.getAppName());
    setCaption("Add item");
    JPanel itemInfoPanel = new JPanel(new MigLayout("hidemode 3", "[][grow][]", ""));
    lblOrderId = new JLabel("Barcode/SKU/Name");
    itemInfoPanel.add(lblOrderId, "cell 0 2,alignx trailing");
    
    tfSearchString = new POSTextField();
    itemInfoPanel.add(tfSearchString, "cell 1 2,growx");
    tfSearchString.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PurchaseOrderItemEntryDialog.this.doSearchItem();
      }
    });
    btnSearchItem = new JButton("..Search..");
    itemInfoPanel.add(btnSearchItem, "cell 2 2,growx");
    
    btnSearchItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PurchaseOrderItemEntryDialog.this.doSearchItem();
      }
      
    });
    JLabel lblItem = new JLabel("Item");
    itemInfoPanel.add(lblItem, "cell 0 3, alignx trailing");
    
    cbItems = new JComboBox();
    cbItems.setMinimumSize(PosUIManager.getSize(90, 0));
    itemInfoPanel.add(cbItems, "cell 1 3,grow");
    
    lblPurchaseOrderItemType = new JLabel("Reason Type");
    itemInfoPanel.add(lblPurchaseOrderItemType, "cell 0 4, alignx trailing");
    
    lblPurchaseOrderItemType.setVisible(false);
    
    cbInventoryReasons = new JComboBox();
    itemInfoPanel.add(cbInventoryReasons, "cell 1 4");
    
    cbInventoryReasons.setVisible(false);
    
    JLabel lblUnit = new JLabel("Unit");
    itemInfoPanel.add(lblUnit, "cell 0 7,alignx trailing");
    
    cbUnits = new JComboBox();
    cbUnits.setMinimumSize(PosUIManager.getSize(100, 0));
    itemInfoPanel.add(cbUnits, "cell 1 7");
    
    lblUnitPrice = new JLabel("Unit Cost (" + CurrencyUtil.getCurrencySymbol() + ")");
    itemInfoPanel.add(lblUnitPrice, "cell 0 8,alignx trailing");
    
    tfUnitPrice = new DoubleTextField();
    itemInfoPanel.add(tfUnitPrice, "cell 1 8,growx");
    
    lblQuantity = new JLabel("Quantity");
    itemInfoPanel.add(lblQuantity, "cell 0 9,alignx trailing");
    
    tfQuantity = new DoubleTextField();
    itemInfoPanel.add(tfQuantity, "cell 1 9,growx");
    getContentPanel().add(itemInfoPanel);
  }
  
  private void doSearchItem() {
    String searchString = tfSearchString.getText();
    List<MenuItem> menuItems = MenuItemDAO.getInstance().getMenuItems(searchString, "", null, null);
    if ((menuItems == null) || (menuItems.isEmpty())) {
      menuItems = MenuItemDAO.getInstance().getMenuItems("", searchString, null, null);
    }
    if ((menuItems == null) || (menuItems.isEmpty())) {
      menuItems = MenuItemDAO.getInstance().getMenuItems("", "", searchString, null);
    }
    ComboBoxModel model = (ComboBoxModel)cbItems.getModel();
    







    model.setDataList(menuItems);
    if (model.getSize() > 0) {
      cbItems.setSelectedIndex(0);
    }
  }
  









  public void updateView()
  {
    if (orderItem == null) {
      return;
    }
    MenuItem menuItem = orderItem.getMenuItem();
    if (menuItem != null) {
      ComboBoxModel model = (ComboBoxModel)cbItems.getModel();
      model.addElement(menuItem);
      cbItems.setSelectedItem(menuItem);
      loadUnits();
      ComboBoxModel unitModel = (ComboBoxModel)cbUnits.getModel();
      if (unitModel.getSize() > 0) {
        List<IUnit> units = unitModel.getDataList();
        for (IUnit unit : units) {
          if (orderItem.getItemUnitName().equals(unit.getUniqueCode())) {
            cbUnits.setSelectedItem(unit);
            break;
          }
        }
      }
      tfSearchString.setText(menuItem.getSku());
    }
    tfQuantity.setText(String.valueOf(orderItem.getItemQuantity()));
    tfUnitPrice.setText(NumberUtil.formatNumber(orderItem.getUnitPrice()));
  }
  
  public void doOk()
  {
    MenuItem menuItem = (MenuItem)cbItems.getSelectedItem();
    if (menuItem == null) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select an item");
      return;
    }
    IUnit unit = (IUnit)cbUnits.getSelectedItem();
    if (unit == null) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select item unit");
      return;
    }
    double quantity = tfQuantity.getDoubleOrZero();
    if (quantity <= 0.0D) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select valid quantity");
      return;
    }
    double price = tfUnitPrice.getDoubleOrZero();
    orderItem.setSku(menuItem.getSku());
    orderItem.setMenuItem(menuItem);
    orderItem.setMenuItemId(menuItem.getId());
    orderItem.setItemUnitName(unit.getUniqueCode());
    orderItem.setItemQuantity(Double.valueOf(quantity));
    orderItem.setName(menuItem.getDisplayName());
    orderItem.setQuantityToReceive(Double.valueOf(quantity));
    orderItem.setUnitPrice(Double.valueOf(price));
    orderItem.calculatePrice();
    if ((menuItem.getCost().doubleValue() == 0.0D) && (price > 0.0D)) {
      menuItem.setCost(Double.valueOf(price));
      MenuItemDAO.getInstance().saveOrUpdate(menuItem);
    }
    setCanceled(false);
    dispose();
  }
}
