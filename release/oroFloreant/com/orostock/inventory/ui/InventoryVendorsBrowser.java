package com.orostock.inventory.ui;

import com.floreantpos.bo.ui.ModelBrowser;
import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.dao.InventoryVendorDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import javax.swing.table.TableColumn;
import org.jdesktop.swingx.JXTable;

public class InventoryVendorsBrowser extends ModelBrowser<InventoryVendor>
{
  public InventoryVendorsBrowser()
  {
    super(new InventoryVendorEntryForm(new InventoryVendor()));
    BeanTableModel<InventoryVendor> tableModel = new BeanTableModel(InventoryVendor.class);
    tableModel.addColumn("NAME", InventoryVendor.PROP_NAME);
    init(tableModel);
    
    add(browserPanel, "West");
    add(beanPanel);
    refreshTable();
  }
  
  public void refreshTable()
  {
    java.util.List<InventoryVendor> locations = InventoryVendorDAO.getInstance().findAll();
    BeanTableModel tableModel = (BeanTableModel)browserTable.getModel();
    
    browserTable.getColumn(0).setPreferredWidth(PosUIManager.getSize(300));
    
    tableModel.removeAll();
    tableModel.addRows(locations);
  }
}
