package com.orostock.inventory.ui;

import javax.swing.JPanel;

public class PrintLabelExplorer extends JPanel
{
  public PrintLabelExplorer()
  {
    init();
  }
  
  private void init() {
    setLayout(new java.awt.BorderLayout());
    PrintLabelForm printLabelForm = new PrintLabelForm();
    add(printLabelForm, "Center");
  }
}
