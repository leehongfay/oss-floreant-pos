package com.orostock.inventory.ui;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.main.Application;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.MenuCategory;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.PosPrinters;
import com.floreantpos.model.PurchaseOrder;
import com.floreantpos.model.PurchaseOrderItem;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosTable;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.action.PurchaseOrderExplorerAction;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JasperPrint;
import org.jdesktop.swingx.JXTable;





public class ShoppingListExplorer
  extends TransparentPanel
{
  private JXTable table;
  private BeanTableModel<MenuItem> tableModel;
  private JComboBox cbGroup;
  private JTextField tfName;
  private JLabel lblNumberOfItem;
  private JButton btnBack;
  private JButton btnForward;
  
  public ShoppingListExplorer()
  {
    tableModel = new BeanTableModel(MenuItem.class);
    tableModel.addColumn(Messages.getString("MenuItemExplorer.9"), "sku");
    
    tableModel.addColumn("INVENTORY ITEM NAME", "displayName");
    tableModel.addColumn("COST (" + CurrencyUtil.getCurrencySymbol() + ")", "cost");
    tableModel.addColumn(Messages.getString("MenuItemExplorer.14"), "availableUnit");
    tableModel.addColumn(Messages.getString("MenuItemExplorer.26"), "unitOnHand");
    tableModel.addColumn("UNIT", MenuItem.PROP_UNIT_NAME);
    
    tableModel.addColumn("REORDER LEVEL", "reorderLevel");
    tableModel.addColumn("REPLENISH LEVEL", "replenishLevel");
    

    table = new PosTable(tableModel);
    
    setLayout(new BorderLayout(5, 5));
    btnBack = new JButton("<<< Previous");
    btnForward = new JButton("Next >>>");
    lblNumberOfItem = new JLabel();
    add(buildSearchForm(), "North");
    add(new JScrollPane(table));
    
    JPanel buttonPanel = new JPanel(new MigLayout("center"));
    PosButton createPurchaseOrder = new PosButton("Create Purchase Order");
    add(buttonPanel, "South");
    buttonPanel.add(createPurchaseOrder);
    
    createPurchaseOrder.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        ShoppingListExplorer.this.doCreatePurchaseOrder();
      }
      
    });
    showMenuItems();
  }
  
  private void doCreatePurchaseOrder() {
    try {
      List<MenuItem> inventoryList = new ArrayList();
      int[] selectedRows = table.getSelectedRows();
      for (int i = 0; i < selectedRows.length; i++) {
        selectedRows[i] = table.convertRowIndexToModel(selectedRows[i]);
      }
      
      PurchaseOrder newPurchaseOrder = new PurchaseOrder();
      int i; if (selectedRows.length == 0) {
        if (tableModel.getRowCount() > 0) {
          inventoryList.addAll(tableModel.getRows());
        }
      } else {
        for (i = 0; i < selectedRows.length; i++) {
          MenuItem item = (MenuItem)tableModel.getRow(selectedRows[i]);
          inventoryList.add(item);
        }
        newPurchaseOrder.calculatePrice();
      }
      if (inventoryList.isEmpty()) {
        return;
      }
      for (MenuItem item : inventoryList) {
        PurchaseOrderItem orderItem = new PurchaseOrderItem();
        orderItem.setSku(item.getSku());
        orderItem.setMenuItemId(item.getId());
        orderItem.setName(item.getDisplayName());
        orderItem.setUnitPrice(item.getCost());
        MenuGroup parent = item.getParent();
        if (parent != null) {
          orderItem.setGroupName(parent.getName());
          orderItem.setCategoryName(parent.getParent().getName());
        }
        orderItem.setItemQuantity(Double.valueOf(item.getReplenishLevel().doubleValue() == 0.0D ? 1.0D : item.getReplenishLevel().doubleValue()));
        InventoryUnit unit = item.getUnit();
        if (unit != null)
          orderItem.setItemUnitName(unit.getCode());
        orderItem.calculatePrice();
        orderItem.setPurchaseOrder(newPurchaseOrder);
        newPurchaseOrder.addToorderItems(orderItem);
      }
      newPurchaseOrder.calculatePrice();
      
      PurchaseOrderForm editor = new PurchaseOrderForm(newPurchaseOrder);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.openWithScale(1024, 630);
      if (dialog.isCanceled()) {
        return;
      }
      PurchaseOrder purchaseOrder = (PurchaseOrder)editor.getBean();
      if (POSMessageDialog.showMessageAndPromtToPrint(POSUtil.getBackOfficeWindow(), "Successfully " + purchaseOrder.getOrderId() + " purchase order has been created ."))
      {
        JasperPrint jasperPrint = ReceiptPrintService.createPurchaseOrderPrint(purchaseOrder);
        jasperPrint.setName("PURCHASE_ORDER_" + purchaseOrder.getOrderId());
        jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
        ReceiptPrintService.printQuitely(jasperPrint);
      }
      tableModel.setCurrentRowIndex(0);
      showMenuItems();
      new PurchaseOrderExplorerAction().actionPerformed(null);
    } catch (Exception ex) {
      BOMessageDialog.showError(ex);
    }
  }
  
  private JPanel buildSearchForm()
  {
    JPanel panel = new JPanel();
    panel.setLayout(new MigLayout("", "[][]15[][]15[][]15[]", "[]5[]"));
    
    JLabel lblName = new JLabel(Messages.getString("MenuItemExplorer.0"));
    
    JLabel lblGroup = new JLabel(Messages.getString("MenuItemExplorer.1"));
    cbGroup = new JComboBox();
    cbGroup.addItem(Messages.getString("MenuItemExplorer.5"));
    List<MenuGroup> groups = MenuGroupDAO.getInstance().findAll();
    for (MenuGroup group : groups) {
      cbGroup.addItem(group);
    }
    tfName = new JTextField(15);
    try
    {
      JButton searchBttn = new JButton(Messages.getString("MenuItemExplorer.3"));
      
      panel.add(lblName, "align label");
      panel.add(tfName);
      panel.add(lblGroup);
      panel.add(cbGroup);
      panel.add(searchBttn);
      
      Border loweredetched = BorderFactory.createEtchedBorder(1);
      TitledBorder title = BorderFactory.createTitledBorder(loweredetched, Messages.getString("MenuItemExplorer.30"));
      title.setTitleJustification(1);
      panel.setBorder(title);
      
      searchBttn.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          ShoppingListExplorer.this.showMenuItems();
        }
        
      });
      tfName.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          ShoppingListExplorer.this.showMenuItems();
        }
      });
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
    
    return panel;
  }
  
  private void showMenuItems() {
    String txName = tfName.getText();
    MenuGroup group = null;
    Object selectedGroup = cbGroup.getSelectedItem();
    if ((selectedGroup instanceof MenuGroup)) {
      group = (MenuGroup)selectedGroup;
    }
    tableModel.setNumRows(MenuItemDAO.getInstance().rowReOrderedItemCount(group, txName, false, Boolean.valueOf(false)));
    MenuItemDAO.getInstance().loadReorderedMenuItems(tableModel, group, txName, false, Boolean.valueOf(false));
    
    int startNumber = tableModel.getCurrentRowIndex() + 1;
    int endNumber = tableModel.getNextRowIndex();
    int totalNumber = tableModel.getNumRows();
    if (endNumber > totalNumber) {
      endNumber = totalNumber;
    }
    lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
    btnBack.setEnabled(tableModel.hasPrevious());
    btnForward.setEnabled(tableModel.hasNext());
  }
}
