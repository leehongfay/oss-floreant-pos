package com.orostock.inventory.ui;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.explorer.ExplorerButtonPanel;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.InventoryTransactionType;
import com.floreantpos.model.dao.InventoryLocationDAO;
import com.floreantpos.model.dao.InventoryTransactionDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.BeanTableModel.DataType;
import com.floreantpos.swing.PosTable;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.inv.InventoryStockInForm;
import com.floreantpos.ui.inv.InventoryTransactionEntryForm;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.action.InventoryClosingBalanceAction;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;








public class InventoryTransactionsBrowser
  extends TransparentPanel
{
  private static final long serialVersionUID = 1L;
  private JXTable table;
  private BeanTableModel<InventoryTransaction> tableModel;
  private JTextField tfName;
  private JComboBox cbLocation;
  private JButton btnBack;
  private JButton btnForward;
  private JLabel lblNumberOfItem;
  private JXDatePicker fromDatePicker;
  private JXDatePicker toDatePicker;
  private Date fromDate;
  private Date toDate;
  
  public InventoryTransactionsBrowser()
  {
    init();
    searchItem();
  }
  
  public void init() {
    tableModel = new BeanTableModel(InventoryTransaction.class, 20);
    tableModel.addColumn("TRANS#", InventoryTransaction.PROP_ID);
    tableModel.addColumn("ITEM NAME", InventoryTransaction.PROP_MENU_ITEM);
    tableModel.addColumn("TYPE", "transactionType");
    
    tableModel.addColumn("FROM LOCATION", "fromInventoryLocation");
    tableModel.addColumn("TO LOCATION", "toInventoryLocation");
    tableModel.addColumn("TIME", "transactionDateAsString");
    tableModel.addColumn("UNIT PRICE", "unitPriceDisplay", 11, BeanTableModel.DataType.MONEY);
    tableModel.addColumn("UNIT COST", "unitCostDisplay", 11, BeanTableModel.DataType.MONEY);
    tableModel.addColumn("QUANTITY", InventoryTransaction.PROP_QUANTITY, 11, BeanTableModel.DataType.NUMBER);
    tableModel.addColumn("UNIT", InventoryTransaction.PROP_UNIT);
    tableModel.addColumn("TOTAL", InventoryTransaction.PROP_TOTAL, 11, BeanTableModel.DataType.MONEY);
    tableModel.addColumn("INVENTORY TYPE", InventoryTransaction.PROP_REASON);
    
    btnBack = new JButton("<<< Previous");
    btnForward = new JButton("Next >>>");
    lblNumberOfItem = new JLabel();
    
    table = new PosTable(tableModel);
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    add(createButtonPanel(), "South");
    add(buildSearchForm(), "North");
    resizeColumnWidth(table);
    
    table.getColumnModel().getColumn(3).setCellRenderer(new DefaultTableCellRenderer()
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
      {
        BeanTableModel model = (BeanTableModel)table.getModel();
        InventoryTransaction transaction = (InventoryTransaction)model.getRow(row);
        
        if (value != null) {
          return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
        
        return super.getTableCellRendererComponent(table, transaction.getTicketId(), isSelected, hasFocus, row, column);

      }
      


    });
    btnBack.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        tableModel.setCurrentRowIndex(tableModel.getPreviousRowIndex());
        InventoryTransactionsBrowser.this.searchItem();
      }
      
    });
    btnForward.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        tableModel.setCurrentRowIndex(tableModel.getNextRowIndex());
        InventoryTransactionsBrowser.this.searchItem();
      }
    });
  }
  
  public void resizeColumnWidth(JTable table)
  {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(200));
    columnWidth.add(Integer.valueOf(60));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(100));
    columnWidth.add(Integer.valueOf(200));
    
    return columnWidth;
  }
  
  private JPanel createButtonPanel()
  {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton editButton = explorerButton.getEditButton();
    JButton deleteButton = explorerButton.getDeleteButton();
    
    editButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        InventoryTransactionsBrowser.this.editSelectedRow();
      }
      

    });
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          
          if (POSMessageDialog.showYesNoQuestionDialog(InventoryTransactionsBrowser.this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
            return;
          }
          
          InventoryTransaction item = (InventoryTransaction)tableModel.getRow(index);
          InventoryTransactionDAO inventoryTransactionDao = new InventoryTransactionDAO();
          inventoryTransactionDao.delete(item);
          
          tableModel.removeRow(index);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    JPanel bottomPanel = new JPanel(new MigLayout("fillx", "[33%][33%][33%]"));
    TransparentPanel actionButtonPanel = new TransparentPanel();
    JButton btnGenerateClosingBalance = new JButton(new InventoryClosingBalanceAction());
    

    bottomPanel.add(btnGenerateClosingBalance, "skip 1,center");
    JPanel navigationPanel = new JPanel(new FlowLayout(4));
    
    navigationPanel.add(lblNumberOfItem, "");
    navigationPanel.add(btnBack);
    navigationPanel.add(btnForward);
    
    bottomPanel.add(navigationPanel, "span,right");
    return bottomPanel;
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      InventoryTransaction inventoryTransaction = (InventoryTransaction)tableModel.getRow(index);
      BeanEditorDialog dialog = null;
      if (inventoryTransaction.getTransactionType() == InventoryTransactionType.IN) {
        InventoryStockInForm editor = new InventoryStockInForm(inventoryTransaction);
        dialog = new BeanEditorDialog(editor);
        dialog.openWithScale(830, 630);
      }
      else {
        InventoryTransactionEntryForm editor = new InventoryTransactionEntryForm(inventoryTransaction);
        dialog = new BeanEditorDialog(editor);
        dialog.setPreferredSize(PosUIManager.getSize(500, 600));
        dialog.open();
      }
      
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private JPanel buildSearchForm() {
    JPanel panel = new JPanel();
    panel.setLayout(new MigLayout("", "[][]15[][]15[][]15[]", "[]5[]"));
    
    JLabel lblName = new JLabel(Messages.getString("MenuItemExplorer.0"));
    JLabel lblGroup = new JLabel("Location");
    JLabel lblFromDate = new JLabel("From: ");
    JLabel lblToDate = new JLabel("To: ");
    fromDatePicker = UiUtil.getCurrentMonthStart();
    toDatePicker = UiUtil.getCurrentMonthEnd();
    
    tfName = new JTextField(15);
    try
    {
      cbLocation = new JComboBox();
      
      List<InventoryLocation> inventoryLocationList = InventoryLocationDAO.getInstance().findAll();
      cbLocation.addItem(Messages.getString("MenuItemExplorer.2"));
      for (InventoryLocation s : inventoryLocationList) {
        cbLocation.addItem(s);
      }
      
      JButton searchBttn = new JButton(Messages.getString("MenuItemExplorer.3"));
      
      panel.add(lblName, "align label");
      panel.add(tfName);
      panel.add(lblGroup);
      panel.add(cbLocation);
      panel.add(lblFromDate);
      panel.add(fromDatePicker);
      panel.add(lblToDate);
      panel.add(toDatePicker);
      panel.add(searchBttn);
      
      Border loweredetched = BorderFactory.createEtchedBorder(1);
      TitledBorder title = BorderFactory.createTitledBorder(loweredetched, "Search");
      title.setTitleJustification(1);
      panel.setBorder(title);
      
      searchBttn.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          InventoryTransactionsBrowser.this.searchItem();
        }
        
      });
      tfName.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          InventoryTransactionsBrowser.this.searchItem();
        }
      });
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
    
    return panel;
  }
  
  private void searchItem() {
    String txName = tfName.getText();
    Object selectedGroup = cbLocation.getSelectedItem();
    
    fromDate = fromDatePicker.getDate();
    toDate = toDatePicker.getDate();
    if (fromDate.after(toDate)) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), POSConstants.FROM_DATE_CANNOT_BE_GREATER_THAN_TO_DATE_);
      return;
    }
    
    InventoryTransactionDAO.getInstance().getInventoryTransactions(tableModel, txName, selectedGroup, fromDate, toDate);
    int startNumber = tableModel.getCurrentRowIndex() + 1;
    int endNumber = tableModel.getNextRowIndex();
    int totalNumber = tableModel.getNumRows();
    if (endNumber > totalNumber) {
      endNumber = totalNumber;
    }
    lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
    btnBack.setEnabled(tableModel.hasPrevious());
    btnForward.setEnabled(tableModel.hasNext());
  }
}
