package com.orostock.inventory.ui;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.InventoryStockUnit;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.InventoryUnitGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.PackagingUnit;
import com.floreantpos.model.dao.InventoryStockUnitDAO;
import com.floreantpos.model.dao.PackagingUnitDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import net.miginfocom.swing.MigLayout;



public class MenuItemPackagingUnitForm
  extends BeanEditor<InventoryStockUnit>
{
  private DoubleTextField tfFactor = new DoubleTextField(10);
  private JComboBox cbPackagingUnit = new JComboBox();
  private JButton btnAddNew;
  private MenuItem menuItem;
  private JLabel lblBaseUnit;
  private JComboBox cbItemUnits = new JComboBox();
  private boolean recipeUnit;
  
  public MenuItemPackagingUnitForm() {
    this(new InventoryStockUnit(), null);
  }
  
  public MenuItemPackagingUnitForm(InventoryStockUnit unit, MenuItem menuItem) {
    this(unit, menuItem, false);
  }
  
  public MenuItemPackagingUnitForm(InventoryStockUnit unit, MenuItem menuItem, boolean recipeUnit) {
    this.recipeUnit = recipeUnit;
    createUI();
    this.menuItem = menuItem;
    if (menuItem != null) {
      if (unit.getId() == null) {
        InventoryUnit invUnit = menuItem.getUnit();
        if (invUnit != null) {
          lblBaseUnit.setText(invUnit.getCode());
        }
      } else {
        lblBaseUnit.setText(unit.getUnit().getName());
      }
      List<PackagingUnit> stockUnits = PackagingUnitDAO.getInstance().findAll(recipeUnit);
      cbPackagingUnit.setModel(new ComboBoxModel(stockUnits));
      cbPackagingUnit.setSelectedItem(unit.getPackagingUnit());
      cbItemUnits.setModel(new ComboBoxModel(menuItem.getUnit().getUnitGroup().getUnits()));
      cbItemUnits.setSelectedItem(menuItem.getUnit());
    }
    setBean(unit);
  }
  
  private void createUI() {
    setLayout(new MigLayout("fillx"));
    
    add(new JLabel(recipeUnit ? "Recipe Unit" : "Packaging Unit"));
    add(cbPackagingUnit, "split 2,grow");
    
    btnAddNew = new JButton("...");
    btnAddNew.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuItemPackagingUnitForm.this.doAddPackagingUnit();
      }
    });
    add(btnAddNew, "wrap");
    
    add(new JLabel("="), "right");
    add(tfFactor, "split 3,grow");
    lblBaseUnit = new JLabel();
    add(cbItemUnits, "grow");
    JButton btnAddInventoryUnit = new JButton("...");
    btnAddInventoryUnit.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuItemPackagingUnitForm.this.doAddInventoryUnit();
      }
    });
    add(btnAddInventoryUnit);
  }
  
  private void doAddPackagingUnit()
  {
    try {
      PackagingUnitForm editor = new PackagingUnitForm(recipeUnit);
      BeanEditorDialog dialog = new BeanEditorDialog(editor);
      dialog.open();
      
      if (dialog.isCanceled()) {
        return;
      }
      PackagingUnit packagingUnit = (PackagingUnit)editor.getBean();
      ComboBoxModel model = (ComboBoxModel)cbPackagingUnit.getModel();
      model.addElement(packagingUnit);
      cbPackagingUnit.setSelectedItem(packagingUnit);
    }
    catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doAddInventoryUnit()
  {
    try {
      InventoryUnitForm editor = new InventoryUnitForm();
      BeanEditorDialog dialog = new BeanEditorDialog(editor);
      dialog.open();
      
      if (dialog.isCanceled()) {
        return;
      }
      InventoryUnit inventoryUnit = (InventoryUnit)editor.getBean();
      ComboBoxModel model = (ComboBoxModel)cbItemUnits.getModel();
      model.addElement(inventoryUnit);
      cbItemUnits.setSelectedItem(inventoryUnit);
    }
    catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public boolean save()
  {
    try
    {
      if (!updateModel()) {
        return false;
      }
      return true;
    }
    catch (IllegalModelStateException e) {
      POSMessageDialog.showError(this, e.getMessage());
    }
    
    return false;
  }
  
  public void createNew()
  {
    setBean(new InventoryStockUnit());
    clearFields();
  }
  
  public void setFieldsEnable(boolean enable)
  {
    tfFactor.setEnabled(enable);
    btnAddNew.setEnabled(enable);
  }
  
  public void clearFields()
  {
    tfFactor.setText("");
  }
  
  protected void updateView()
  {
    InventoryStockUnit inventoryUnit = (InventoryStockUnit)getBean();
    if (inventoryUnit == null) {
      return;
    }
    cbPackagingUnit.setSelectedItem(inventoryUnit.getPackagingUnit());
    tfFactor.setText(inventoryUnit.getConversionValue() + "");
    InventoryUnit unit = inventoryUnit.getUnit();
    if (unit != null) {
      lblBaseUnit.setText(unit.getName());
      cbItemUnits.setSelectedItem(unit);
    }
  }
  
  protected boolean updateModel() throws IllegalModelStateException
  {
    InventoryStockUnit stockUnit = (InventoryStockUnit)getBean();
    double conversionRate = tfFactor.getDouble();
    stockUnit.setPackagingUnit((PackagingUnit)cbPackagingUnit.getSelectedItem());
    stockUnit.setConversionValue(Double.valueOf(conversionRate));
    if (menuItem.getUnit() == null) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Select menu item unit first.");
      return false;
    }
    stockUnit.setUnit(menuItem.getUnit());
    stockUnit.setMenuItem(menuItem);
    stockUnit.setUnit((InventoryUnit)cbItemUnits.getSelectedItem());
    stockUnit.calculateBaseUnitValue();
    return true;
  }
  
  public boolean delete()
  {
    try
    {
      InventoryStockUnit inventoryUnit = (InventoryStockUnit)getBean();
      if (inventoryUnit == null) {
        return false;
      }
      
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), "Are you sure to delete selected item ?", "Confirm");
      if (option != 0) {
        return false;
      }
      
      InventoryStockUnitDAO.getInstance().delete(inventoryUnit);
      clearFields();
      return true;
    }
    catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e); }
    return false;
  }
  


  public String getDisplayText()
  {
    if (recipeUnit)
      return "Add/Edit recipe unit";
    return "Add/Edit packaging unit";
  }
}
