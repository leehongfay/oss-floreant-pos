package com.orostock.inventory.ui.model;

import com.floreantpos.model.base.BaseMenuItem;

public class ShoppingList extends BaseMenuItem {
  private String vendorName;
  private String inventoryItemName;
  private String totalPackage;
  private double purchagePrice;
  private double reorderLevel;
  private String packSize;
  
  public ShoppingList() {}
  
  public String getVendorName() { return vendorName; }
  
  public void setVendorName(String vendorName)
  {
    this.vendorName = vendorName;
  }
  
  public String getInventoryItemName() {
    return inventoryItemName;
  }
  
  public void setInventoryItemName(String inventoryItemName) {
    this.inventoryItemName = inventoryItemName;
  }
  
  public String getTotalPackage() {
    return totalPackage;
  }
  
  public void setTotalPackage(String totalPackage) {
    this.totalPackage = totalPackage;
  }
  
  public double getPurchagePrice() {
    return purchagePrice;
  }
  
  public void setPurchagePrice(double purchagePrice) {
    this.purchagePrice = purchagePrice;
  }
  
  public Double getReorderLevel() {
    return Double.valueOf(reorderLevel);
  }
  
  public void setReorderLevel(double reorderLevel) {
    this.reorderLevel = reorderLevel;
  }
  
  public String getPackSize() {
    return packSize;
  }
  
  public void setPackSize(String packSize) {
    this.packSize = packSize;
  }
}
