package com.orostock.inventory.ui;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.InventoryStock;
import com.floreantpos.model.InventoryTransaction;
import com.floreantpos.model.InventoryTransactionType;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.dao.InventoryLocationDAO;
import com.floreantpos.model.dao.InventoryStockDAO;
import com.floreantpos.model.dao.InventoryTransactionDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.BeanTableModel.DataType;
import com.floreantpos.swing.PosTable;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.WeightSelectionDialog;
import com.floreantpos.ui.inv.InventoryTransactionEntryForm;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jdesktop.swingx.JXTable;






public class InventoryStockBrowser
  extends TransparentPanel
{
  private static final long serialVersionUID = 1L;
  private JXTable table;
  private BeanTableModel<InventoryStock> tableModel;
  private JTextField tfName;
  private JComboBox cbLocation;
  
  public InventoryStockBrowser()
  {
    tableModel = new BeanTableModel(InventoryStock.class);
    tableModel.addColumn("SKU", "sku");
    tableModel.addColumn("ITEM", "itemName");
    tableModel.addColumn("LOCATION", "inventoryLocation");
    tableModel.addColumn("QUANTITY", "quantityInHand", 11, BeanTableModel.DataType.NUMBER);
    tableModel.addColumn("UNIT", "unit");
    
    tableModel.addRows(InventoryStockDAO.getInstance().findAll());
    
    table = new PosTable(tableModel);
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    add(createButtonPanel(), "South");
    add(buildSearchForm(), "North");
  }
  
  private Component createButtonPanel() {
    JPanel panel = new JPanel();
    addInventoryButtonActions(panel);
    return panel;
  }
  
  private void addInventoryButtonActions(JPanel panel) {
    JButton inTransactionButton = new JButton("IN");
    JButton outTransactionButton = new JButton("OUT");
    JButton transferTransactionButton = new JButton("TRANSFER");
    JButton btnConvert = new JButton("Convert");
    
    btnConvert.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        InventoryStockBrowser.this.doConvertion();
      }
      
    });
    inTransactionButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        int index = table.getSelectedRow();
        if (index < 0) {
          return;
        }
        InventoryStock stock = (InventoryStock)tableModel.getRow(index);
        MenuItem menuItem = MenuItemDAO.getInstance().loadInitialized(stock.getMenuItemId());
        InventoryTransaction inventoryTransaction = new InventoryTransaction();
        inventoryTransaction.setType(Integer.valueOf(InventoryTransactionType.IN.getType()));
        inventoryTransaction.setMenuItem(menuItem);
        inventoryTransaction.setToInventoryLocation(stock.getInventoryLocation());
        InventoryTransactionEntryForm inventoryTransactionEntryForm = new InventoryTransactionEntryForm(inventoryTransaction);
        
        BeanEditorDialog dialog = new BeanEditorDialog(inventoryTransactionEntryForm);
        dialog.setPreferredSize(PosUIManager.getSize(500, 500));
        dialog.open();
        
        if (dialog.isCanceled()) {}
      }
      

    });
    outTransactionButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        int index = table.getSelectedRow();
        if (index < 0) {
          return;
        }
        InventoryStock stock = (InventoryStock)tableModel.getRow(index);
        MenuItem menuItem = MenuItemDAO.getInstance().loadInitialized(stock.getMenuItemId());
        InventoryTransaction inventoryTransaction = new InventoryTransaction();
        inventoryTransaction.setType(Integer.valueOf(InventoryTransactionType.OUT.getType()));
        inventoryTransaction.setMenuItem(menuItem);
        inventoryTransaction.setFromInventoryLocation(stock.getInventoryLocation());
        InventoryTransactionEntryForm inventoryTransactionEntryForm = new InventoryTransactionEntryForm(inventoryTransaction);
        
        BeanEditorDialog dialog = new BeanEditorDialog(inventoryTransactionEntryForm);
        dialog.setPreferredSize(PosUIManager.getSize(500, 500));
        dialog.open();
        
        if (dialog.isCanceled()) {}
      }
      

    });
    transferTransactionButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        int index = table.getSelectedRow();
        if (index < 0) {
          return;
        }
        InventoryStock stock = (InventoryStock)tableModel.getRow(index);
        MenuItem menuItem = MenuItemDAO.getInstance().loadInitialized(stock.getMenuItemId());
        
        InventoryTransaction inventoryTransaction = new InventoryTransaction();
        inventoryTransaction.setReason("TRANSFER");
        inventoryTransaction.setMenuItem(menuItem);
        
        InventoryTransactionEntryForm transferForm = new InventoryTransactionEntryForm(inventoryTransaction);
        BeanEditorDialog dialog = new BeanEditorDialog(transferForm);
        dialog.setPreferredSize(PosUIManager.getSize(500, 500));
        dialog.open();
        
        if (dialog.isCanceled()) {}

      }
      


    });
    panel.add(btnConvert);
  }
  
  private Double getConvertQuantity(InventoryStock stock) {
    WeightSelectionDialog dialog = new WeightSelectionDialog(POSUtil.getBackOfficeWindow());
    dialog.setTitle("Enter convert quantity.");
    dialog.setFloatingPoint(true);
    dialog.setValue(1.0D);
    dialog.pack();
    dialog.open();
    if (dialog.isCanceled()) {
      return null;
    }
    Double inputQuantity = Double.valueOf(dialog.getValue());
    
    if (inputQuantity.doubleValue() > stock.getQuantityInHand().doubleValue()) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Inputed quantity is not avaiable in stock.");
      getConvertQuantity(stock);
    }
    
    if (inputQuantity.doubleValue() == 0.0D) {
      POSMessageDialog.showError("Unit can not be zero");
      getConvertQuantity(stock);
    }
    return inputQuantity;
  }
  
  private JPanel buildSearchForm() {
    JPanel panel = new JPanel();
    panel.setLayout(new MigLayout("", "[][]15[][]15[][]15[]", "[]5[]"));
    
    JLabel lblName = new JLabel(Messages.getString("MenuItemExplorer.0"));
    JLabel lblGroup = new JLabel("Location");
    tfName = new JTextField(15);
    
    try
    {
      cbLocation = new JComboBox();
      
      List<InventoryLocation> inventoryLocationList = InventoryLocationDAO.getInstance().findAll();
      cbLocation.addItem(Messages.getString("MenuItemExplorer.2"));
      for (InventoryLocation s : inventoryLocationList) {
        cbLocation.addItem(s);
      }
      
      JButton searchBttn = new JButton(Messages.getString("MenuItemExplorer.3"));
      
      panel.add(lblName, "align label");
      panel.add(tfName);
      panel.add(lblGroup);
      panel.add(cbLocation);
      panel.add(searchBttn);
      
      Border loweredetched = BorderFactory.createEtchedBorder(1);
      TitledBorder title = BorderFactory.createTitledBorder(loweredetched, "Search");
      title.setTitleJustification(1);
      panel.setBorder(title);
      
      searchBttn.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          InventoryStockBrowser.this.searchItem();
        }
        
      });
      tfName.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          InventoryStockBrowser.this.searchItem();
        }
      });
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
    
    return panel;
  }
  
  private void searchItem() {
    String txName = tfName.getText();
    Object selectedGroup = cbLocation.getSelectedItem();
    
    List<InventoryStock> similarItem = null;
    
    if ((selectedGroup instanceof InventoryStock)) {
      similarItem = InventoryStockDAO.getInstance().getInventoryStock(txName, selectedGroup);
    }
    else {
      similarItem = InventoryStockDAO.getInstance().getInventoryStock(txName, selectedGroup);
    }
    tableModel.removeAll();
    tableModel.addRows(similarItem);
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      InventoryStock inventoryStock = (InventoryStock)tableModel.getRow(index);
      inventoryStock = InventoryStockDAO.getInstance().initialize(inventoryStock);
      
      tableModel.setRow(index, inventoryStock);
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void refresh() {
    tableModel.removeAll();
    tableModel.addRows(InventoryStockDAO.getInstance().findAll());
  }
  
  private void doConvertion() {
    int index = table.getSelectedRow();
    if (index < 0) {
      return;
    }
    InventoryStock stock = (InventoryStock)tableModel.getRow(index);
    MenuItem menuItem = MenuItemDAO.getInstance().loadInitialized(stock.getMenuItemId());
    if (stock.getUnit().equals(menuItem.getUnit().getCode())) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Base unit cannot be converted.");
      return;
    }
    Double convertValue = getConvertQuantity(stock);
    if (convertValue == null) {
      return;
    }
    
    double baseQuantity = menuItem.getBaseUnitQuantity(stock.getUnit()) * convertValue.doubleValue();
    Session session = null;
    Transaction tx = null;
    try {
      InventoryTransaction outTrans = new InventoryTransaction();
      outTrans.setReason("TRANSFER");
      outTrans.setTransactionDate(new Date());
      outTrans.setMenuItem(menuItem);
      outTrans.setType(Integer.valueOf(InventoryTransactionType.OUT.getType()));
      outTrans.setQuantity(convertValue);
      outTrans.setUnit(stock.getUnit());
      outTrans.setFromInventoryLocation(stock.getInventoryLocation());
      
      InventoryTransaction inTrans = new InventoryTransaction();
      inTrans.setReason("TRANSFER");
      inTrans.setTransactionDate(new Date());
      inTrans.setMenuItem(menuItem);
      inTrans.setType(Integer.valueOf(InventoryTransactionType.IN.getType()));
      inTrans.setQuantity(Double.valueOf(baseQuantity));
      inTrans.setUnit(menuItem.getUnit().getCode());
      inTrans.setToInventoryLocation(stock.getInventoryLocation());
      
      session = InventoryTransactionDAO.getInstance().createNewSession();
      tx = session.beginTransaction();
      InventoryTransactionDAO.getInstance().adjustInventoryStock(outTrans, session);
      InventoryTransactionDAO.getInstance().adjustInventoryStock(inTrans, session);
      tx.commit();
      
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Successfully converted " + convertValue + " " + stock.getUnit() + " to " + baseQuantity + " " + menuItem
        .getUnit());
      
      refresh();
    }
    catch (Exception ex) {
      tx.rollback();
    } finally {
      session.close();
    }
  }
}
