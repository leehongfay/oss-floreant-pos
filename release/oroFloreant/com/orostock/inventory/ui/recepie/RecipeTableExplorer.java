package com.orostock.inventory.ui.recepie;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.RecipeTable;
import com.floreantpos.model.dao.RecipeTableDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;






















public class RecipeTableExplorer
  extends TransparentPanel
{
  private JXTable table;
  private BeanTableModel<RecipeTable> tableModel;
  private JTextField tfRecipeName = new JTextField(20);
  
  public RecipeTableExplorer()
  {
    tableModel = new BeanTableModel(RecipeTable.class);
    tableModel.addColumn("ITEM NAME", RecipeTable.PROP_MENU_ITEM);
    tableModel.addColumn("RECEPIE", "recipeList");
    tableModel.addRows(RecipeTableDAO.getInstance().findAll());
    
    table = new JXTable(tableModel);
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          RecipeTableExplorer.this.editSelectedRow();
        }
      }
    });
    table.setRowHeight(PosUIManager.getSize(30));
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    JPanel topSearchPanel = new JPanel(new MigLayout());
    topSearchPanel.add(new JLabel("Search by recipe or item name:"));
    topSearchPanel.add(tfRecipeName);
    
    tfRecipeName.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        RecipeTableExplorer.this.doSearchItems();
      }
      
    });
    JButton btnSearch = new JButton(POSConstants.SEARCH_ITEM_BUTTON_TEXT);
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        RecipeTableExplorer.this.doSearchItems();
      }
    });
    topSearchPanel.add(btnSearch);
    add(topSearchPanel, "North");
    
    addButtonPanel();
  }
  
  private void doSearchItems() {
    List<RecipeTable> items = RecipeTableDAO.getInstance().findRecipeTables(tfRecipeName.getText());
    tableModel.setRows(items);
  }
  
  private void addButtonPanel() {
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          RecipeTableEntryForm editor = new RecipeTableEntryForm(new RecipeTable());
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.openWithScale(600, 550);
          
          if (dialog.isCanceled()) {
            return;
          }
          RecipeTable recipe = (RecipeTable)editor.getBean();
          tableModel.addRow(recipe);
        }
        catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        RecipeTableExplorer.this.editSelectedRow();
      }
      
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          RecipeTable orderType = (RecipeTable)tableModel.getRow(index);
          
          if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
            return;
          }
          
          RecipeTableDAO dao = new RecipeTableDAO();
          dao.delete(orderType);
          
          tableModel.removeRow(index);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    add(panel, "South");
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      RecipeTable recipeTable = (RecipeTable)tableModel.getRow(index);
      
      RecipeTableEntryForm editor = new RecipeTableEntryForm(recipeTable);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.openWithScale(600, 550);
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
}
