package com.orostock.inventory.ui.recepie;

import com.orostock.inventory.ui.PackagingUnitsBrowser;
import com.orostock.inventory.ui.recepie.template.RecipeCostingTemplateExplorer;
import java.awt.BorderLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

public class RecipeTabExplorer extends JPanel
{
  private JTabbedPane mainTab;
  
  public RecipeTabExplorer()
  {
    initComponents();
  }
  
  private void initComponents() {
    mainTab = new JTabbedPane();
    mainTab.setUI(new BasicTabbedPaneUI());
    setLayout(new BorderLayout());
    final JComponent recepieListPanel = new JPanel(new BorderLayout());
    mainTab.addTab("Recipe", recepieListPanel);
    recepieListPanel.add(new RecipeExplorer());
    final JComponent recipeUnitPanel = new JPanel(new BorderLayout());
    mainTab.addTab("Recipe Unit", recipeUnitPanel);
    final JComponent recipeCostingTemplatePanel = new JPanel(new BorderLayout());
    mainTab.addTab("Recipe Costing Template", recipeCostingTemplatePanel);
    add(mainTab);
    mainTab.addChangeListener(new ChangeListener()
    {
      public void stateChanged(ChangeEvent evt) {
        JTabbedPane mainTab = (JTabbedPane)evt.getSource();
        
        int selectedTabIndex = mainTab.getSelectedIndex();
        if (selectedTabIndex == 0) {
          if (recepieListPanel.getComponentCount() == 0) {
            recepieListPanel.add(new RecipeExplorer());
          }
        } else if (selectedTabIndex == 1) {
          if (recipeUnitPanel.getComponentCount() == 0) {
            recipeUnitPanel.add(new PackagingUnitsBrowser(true));
          }
        }
        else if ((selectedTabIndex == 2) && 
          (recipeCostingTemplatePanel.getComponentCount() == 0)) {
          recipeCostingTemplatePanel.add(new RecipeCostingTemplateExplorer());
        }
      }
    });
  }
}
