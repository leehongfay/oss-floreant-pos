package com.orostock.inventory.ui.recepie;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.Recepie;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
















public class RecipeSelectionDialog
  extends POSDialog
  implements ActionListener
{
  private RecipeSelectionView itemSelectorPanel;
  private List<Recepie> recipeList;
  private int selectionMode;
  private TitlePanel titelpanel;
  private boolean subRecipeOnly;
  private static RecipeSelectionDialog instance;
  
  public RecipeSelectionDialog(List<Recepie> recipelist)
  {
    super(POSUtil.getFocusedWindow(), "");
    recipeList = recipelist;
    initComponents();
  }
  
  public RecipeSelectionDialog(List<Recepie> recipeList, boolean subRecipeOnly) {
    super(POSUtil.getFocusedWindow(), "");
    this.recipeList = recipeList;
    this.subRecipeOnly = subRecipeOnly;
    initComponents();
  }
  
  public RecipeSelectionDialog() {
    super(POSUtil.getFocusedWindow(), "");
    setTitle("Select recipe item");
    initComponents();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    setTitle("Select Menu Item");
    titelpanel = new TitlePanel();
    titelpanel.setTitle("Select recipe item");
    
    add(titelpanel, "North");
    
    JPanel contentPane = new JPanel(new MigLayout("fill,hidemode 3,inset 0 10 0 10"));
    itemSelectorPanel = new RecipeSelectionView(recipeList, subRecipeOnly);
    contentPane.add(itemSelectorPanel, "grow,span,wrap");
    
    PosButton btnOk = new PosButton("SELECT");
    btnOk.setActionCommand(POSConstants.OK);
    btnOk.setBackground(Color.GREEN);
    
    btnOk.setFocusable(false);
    btnOk.addActionListener(this);
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL);
    btnCancel.setFocusable(false);
    btnCancel.addActionListener(this);
    
    JPanel footerPanel = new JPanel(new MigLayout("center,ins 0 5 5 5", "", ""));
    
    PosButton btnEdit = new PosButton();
    PosButton btnAdd = new PosButton();
    
    btnAdd.setText(POSConstants.ADD.toUpperCase());
    btnEdit.setText(POSConstants.EDIT.toUpperCase());
    
    btnEdit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        RecipeSelectionDialog.this.editSelectedRow();
      }
      

    });
    btnAdd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          Recepie menuRecepie = new Recepie();
          BeanEditor<Recepie> editor = new RecipeEntryForm(menuRecepie);
          
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.setPreferredSize(PosUIManager.getSize(900, 650));
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          Recepie recipeItem = (Recepie)editor.getBean();
          itemSelectorPanel.getModel().addRow(recipeItem);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    footerPanel.add(btnAdd);
    footerPanel.add(btnEdit);
    footerPanel.add(btnOk);
    footerPanel.add(btnCancel);
    
    add(footerPanel, "South");
    
    add(contentPane);
  }
  
  public void setParentRecepie(Recepie selectedRecepie, boolean editMode) {
    itemSelectorPanel.setParentRecepie(selectedRecepie, editMode);
  }
  
  public void setSelectionMode(int selectionMode) {
    this.selectionMode = selectionMode;
    if (selectionMode == 0) {
      titelpanel.setTitle("SELECT A RECIPE");
    }
    else {
      titelpanel.setTitle("Select one or more recipe");
    }
    itemSelectorPanel.setSelectionMode(selectionMode);
  }
  
  private void doOk() {
    if (selectionMode == 0) {
      Recepie recipe = getSelectedRowData();
      if (recipe == null) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select recipe Item");
        return;
      }
    }
    else {
      List<Recepie> menuRecepies = itemSelectorPanel.getSelectedRecepieList();
      if ((menuRecepies == null) || (menuRecepies.isEmpty())) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select recipe Item");
        return;
      }
    }
    setCanceled(false);
    dispose();
  }
  
  private void doCancel() {
    setCanceled(true);
    dispose();
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    
    if (POSConstants.CANCEL.equalsIgnoreCase(actionCommand)) {
      doCancel();
    }
    else if (POSConstants.OK.equalsIgnoreCase(actionCommand)) {
      doOk();
    }
  }
  
  private void editSelectedRow() {
    try {
      int index = itemSelectorPanel.getSelectedRow();
      if (index < 0)
        return;
      Recepie recipe = (Recepie)itemSelectorPanel.getModel().getRow(index);
      itemSelectorPanel.getModel().setRow(index, recipe);
      
      BeanEditor<Recepie> editor = new RecipeEntryForm(recipe);
      
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      itemSelectorPanel.repaintTable();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public List<Recepie> getSelectedItems() {
    return itemSelectorPanel.getSelectedRecepieList();
  }
  
  public Recepie getSelectedRowData() {
    int index = itemSelectorPanel.getSelectedRow();
    if (index < 0)
      return null;
    return (Recepie)itemSelectorPanel.getModel().getRow(index);
  }
  
  public static RecipeSelectionDialog getInstance() {
    if (instance == null) {
      instance = new RecipeSelectionDialog();
    }
    return instance;
  }
}
