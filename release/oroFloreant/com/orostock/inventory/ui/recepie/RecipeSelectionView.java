package com.orostock.inventory.ui.recepie;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.bo.ui.explorer.ExplorerButtonPanel;
import com.floreantpos.model.Recepie;
import com.floreantpos.model.dao.RecepieDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;


















public class RecipeSelectionView
  extends JPanel
{
  private JXTable table;
  private BeanTableModel<Recepie> tableModel;
  private JTextField tfName;
  private Recepie parentRecepie;
  private PosButton btnNext;
  private PosButton btnPrev;
  private JLabel lblNumberOfItem = new JLabel();
  private JLabel lblName;
  private JButton btnSearch;
  private JPanel searchPanel;
  private Map<String, Recepie> addedRecepieMap = new HashMap();
  private JCheckBox chkShowSelected;
  private boolean subRecipeOnly;
  public static final int SINGLE_SELECTION = 0;
  public static final int MULTIPLE_SELECTION = 1;
  
  public RecipeSelectionView(List<Recepie> addedRecepies, boolean inventoryItemOnly)
  {
    subRecipeOnly = inventoryItemOnly;
    initComponents();
    tableModel.setCurrentRowIndex(0);
    setRecepies(addedRecepies);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(5, 5));
    tableModel = new BeanTableModel(Recepie.class);
    tableModel.addColumn("", "visible");
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "name");
    tableModel.addColumn("YIELD", "yield");
    tableModel.addColumn("YIELD UNIT", Recepie.PROP_YIELD_UNIT);
    tableModel.addColumn("PORTION", Recepie.PROP_PORTION);
    tableModel.addColumn("PORTION UNIT", Recepie.PROP_PORTION_UNIT);
    tableModel.setPageSize(10);
    table = new JXTable(tableModel);
    table.setSelectionMode(2);
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    table.setRowHeight(PosUIManager.getSize(40));
    table.getTableHeader().setPreferredSize(PosUIManager.getSize(0, 35));
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          RecipeSelectionView.this.editSelectedRow();
        }
        else {
          RecipeSelectionView.this.selectItem();
        }
        
      }
    });
    JPanel contentPanel = new JPanel(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(10, 5, 10, 5));
    JScrollPane scroll = new JScrollPane(table);
    scroll.setPreferredSize(PosUIManager.getSize(500, 250));
    contentPanel.add(scroll);
    contentPanel.add(buildSearchForm(), "North");
    
    add(contentPanel);
    resizeColumnWidth(table);
    
    JPanel paginationButtonPanel = new JPanel(new MigLayout("ins 5 0 0 0,fillx", "[left,grow][][][]", ""));
    paginationButtonPanel.add(createButtonPanel(), "left,split 2");
    
    chkShowSelected = new JCheckBox("Show selected");
    chkShowSelected.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        RecipeSelectionView.this.updateView();
      }
    });
    paginationButtonPanel.add(chkShowSelected);
    paginationButtonPanel.add(lblNumberOfItem, "split 3,center");
    
    btnPrev = new PosButton();
    btnPrev.setIcon(IconFactory.getIcon("/ui_icons/", "previous.png"));
    paginationButtonPanel.add(btnPrev, "center");
    
    PosButton btnDot = new PosButton();
    btnDot.setBorder(null);
    btnDot.setOpaque(false);
    btnDot.setContentAreaFilled(false);
    btnDot.setIcon(IconFactory.getIcon("/ui_icons/", "dot.png"));
    
    btnNext = new PosButton();
    btnNext.setIcon(IconFactory.getIcon("/ui_icons/", "next.png"));
    paginationButtonPanel.add(btnNext);
    paginationButtonPanel.add(new JSeparator(), "newline,span,grow");
    
    contentPanel.add(paginationButtonPanel, "South");
    
    ActionListener action = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          Object source = e.getSource();
          if (source == btnPrev) {
            RecipeSelectionView.this.scrollUp();
          }
          else if (source == btnNext) {
            RecipeSelectionView.this.scrollDown();
          }
        } catch (Exception e2) {
          POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e2.getMessage(), e2);
        }
        
      }
    };
    btnPrev.addActionListener(action);
    btnNext.addActionListener(action);
    
    btnNext.setEnabled(false);
    btnPrev.setEnabled(false);
  }
  
  private void updateView() {
    if (chkShowSelected.isSelected()) {
      tableModel.setRows(new ArrayList(addedRecepieMap.values()));
      updateRecepieSelection();
      chkShowSelected.setText("Show Selected (" + addedRecepieMap.values().size() + ")");
      lblNumberOfItem.setText("");
      btnPrev.setEnabled(false);
      btnNext.setEnabled(false);
      table.repaint();
    }
  }
  
  private JPanel buildSearchForm() {
    searchPanel = new JPanel();
    searchPanel.setLayout(new MigLayout("inset 0,fillx,hidemode 3", "", "[]10[]"));
    lblName = new JLabel(POSConstants.NAME + " / Barcode: ");
    tfName = new JTextField(15);
    btnSearch = new JButton(POSConstants.SEARCH_ITEM_BUTTON_TEXT);
    searchPanel.add(lblName, "align label,split 5");
    searchPanel.add(tfName, "growx");
    
    searchPanel.add(btnSearch);
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        tableModel.setCurrentRowIndex(0);
        RecipeSelectionView.this.doSearchItem();
      }
      
    });
    tfName.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tableModel.setCurrentRowIndex(0);
        RecipeSelectionView.this.doSearchItem();
      }
    });
    return searchPanel;
  }
  
  private void updateRecepieSelection() {
    List<Recepie> menuItems = tableModel.getRows();
    if (menuItems == null)
      return;
    for (Recepie menuItem : menuItems) {
      Recepie item = (Recepie)addedRecepieMap.get(menuItem.getId());
      menuItem.setVisible(Boolean.valueOf(item != null));
    }
  }
  
  private void updateButton() {
    int startNumber = tableModel.getCurrentRowIndex() + 1;
    int endNumber = tableModel.getNextRowIndex();
    int totalNumber = tableModel.getNumRows();
    if (endNumber > totalNumber) {
      endNumber = totalNumber;
    }
    lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
    btnPrev.setEnabled(tableModel.hasPrevious());
    btnNext.setEnabled(tableModel.hasNext());
    
    if (tableModel.getRowCount() > 0) {
      table.setRowSelectionInterval(0, 0);
    }
    chkShowSelected.setText("Show Selected (" + addedRecepieMap.values().size() + ")");
  }
  
  private TransparentPanel createButtonPanel() {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton btnEdit = explorerButton.getEditButton();
    JButton btnAdd = explorerButton.getAddButton();
    btnAdd.setText(POSConstants.ADD);
    btnEdit.setText(POSConstants.EDIT);
    
    btnEdit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        RecipeSelectionView.this.editSelectedRow();
      }
    });
    btnAdd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          Recepie menuItem = new Recepie();
          RecipeEntryForm editor = new RecipeEntryForm(menuItem);
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          Recepie foodItem = (Recepie)editor.getBean();
          tableModel.addRow(foodItem);
          tableModel.setNumRows(tableModel.getNumRows() + 1);
          RecipeSelectionView.this.updateButton();
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel(new MigLayout("center,ins 0", "sg,fill", ""));
    return panel;
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(220));
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(70));
    columnWidth.add(Integer.valueOf(50));
    columnWidth.add(Integer.valueOf(90));
    
    return columnWidth;
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      Recepie menuItem = (Recepie)tableModel.getRow(index);
      tableModel.setRow(index, menuItem);
      
      BeanEditor<Recepie> editor = new RecipeEntryForm(menuItem);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public List<Recepie> getSelectedRecepieList() {
    return new ArrayList(addedRecepieMap.values());
  }
  
  public void setRecepies(List<Recepie> menuItems) {
    if (menuItems != null) {
      for (Recepie item : menuItems) {
        addedRecepieMap.put(item.getId(), item);
        tableModel.addRow(item);
      }
    }
  }
  
  public void setParentRecepie(Recepie selectedRecepie, boolean editMode) {
    parentRecepie = selectedRecepie;
    if (editMode) {
      chkShowSelected.setSelected(true);
      updateView();
    }
    else {
      doSearchItem();
    }
  }
  
  private void doSearchItem() { tableModel.setNumRows(RecepieDAO.getInstance().rowCount(tfName.getText(), isSubRecipeOnly()));
    RecepieDAO.getInstance().loadRecepies(tableModel, tfName.getText(), isSubRecipeOnly());
    updateButton();
    updateRecepieSelection();
    table.repaint();
    chkShowSelected.setSelected(false);
  }
  
  private void scrollDown() {
    tableModel.setCurrentRowIndex(tableModel.getNextRowIndex());
    doSearchItem();
  }
  
  private void scrollUp() {
    tableModel.setCurrentRowIndex(tableModel.getPreviousRowIndex());
    doSearchItem();
  }
  
  private void selectItem() {
    if (table.getSelectedRow() < 0) {
      return;
    }
    int selectedRow = table.getSelectedRow();
    selectedRow = table.convertRowIndexToModel(selectedRow);
    Recepie item = (Recepie)tableModel.getRow(selectedRow);
    if ((parentRecepie != null) && (parentRecepie.getId() != null) && (parentRecepie.getId().equals(item.getId()))) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Parent item cannot be selected");
      return;
    }
    item.setVisible(Boolean.valueOf(!item.isVisible().booleanValue()));
    if (item.isVisible().booleanValue()) {
      addedRecepieMap.put(item.getId(), item);
    }
    else {
      addedRecepieMap.remove(item.getId());
    }
    chkShowSelected.setText("Show Selected (" + addedRecepieMap.values().size() + ")");
    table.repaint();
  }
  
  public BeanTableModel<Recepie> getModel() {
    return tableModel;
  }
  
  public int getSelectedRow() {
    int index = table.getSelectedRow();
    if (index < 0)
      return -1;
    return table.convertRowIndexToModel(index);
  }
  
  public void repaintTable() {
    table.repaint();
  }
  
  public void setSelectionMode(int selectionMode) {
    chkShowSelected.setVisible(selectionMode == 1);
    TableColumnModelExt columnModel = (TableColumnModelExt)table.getColumnModel();
    columnModel.getColumnExt(0).setVisible(selectionMode == 1);
    doSearchItem();
  }
  
  public void setEnableSearch(boolean enableSearch) {
    tfName.setVisible(enableSearch);
    lblName.setVisible(enableSearch);
    btnSearch.setVisible(enableSearch);
  }
  
  public boolean isSubRecipeOnly() {
    return subRecipeOnly;
  }
  
  public void setSubRecipeOnly(boolean subRecipeOnly) {
    this.subRecipeOnly = subRecipeOnly;
  }
}
