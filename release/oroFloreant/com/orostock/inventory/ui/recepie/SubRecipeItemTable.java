package com.orostock.inventory.ui.recepie;

import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.Recepie;
import com.floreantpos.model.RecepieItem;
import com.floreantpos.model.SubRecipe;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosUIManager;
import java.awt.Component;
import java.awt.Dimension;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class SubRecipeItemTable extends JTable
{
  private SubRecipeTableModel model;
  private ChangeListener listener;
  
  public SubRecipeItemTable(List<SubRecipe> recipeItems)
  {
    putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    
    model = new SubRecipeTableModel();
    setItems(recipeItems);
    setModel(model);
    
    getTableHeader().setPreferredSize(new Dimension(0, 25));
    setDefaultRenderer(Object.class, new CustomCellRenderer());
    setRowHeight(PosUIManager.getSize(25));
    
    TableColumn colQuantity = getColumnModel().getColumn(1);
    TextCellEditor quantityEditor = new TextCellEditor(new JTextField());
    colQuantity.setCellEditor(quantityEditor);
    
    resizeTableColumns();
  }
  
  private void resizeTableColumns() {
    setAutoResizeMode(4);
    setColumnWidth(1, PosUIManager.getSize(90));
    setColumnWidth(2, PosUIManager.getSize(90));
    setColumnWidth(3, PosUIManager.getSize(90));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  public boolean isCellEditable(int row, int column)
  {
    if (column == 1)
      return true;
    return super.isCellEditable(row, column);
  }
  
  public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend)
  {
    super.changeSelection(rowIndex, columnIndex, toggle, extend);
    if (columnIndex > -1)
    {
      if (editCellAt(rowIndex, columnIndex)) {
        Component editor = getEditorComponent();
        editor.requestFocusInWindow();
        if ((editor instanceof JTextField))
        {
          JTextField jf = (JTextField)editor;
          jf.select(0, jf.toString().length());
        }
      }
    }
  }
  
  private class SubRecipeTableModel extends ListTableModel<SubRecipe>
  {
    public SubRecipeTableModel() {
      super();
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      SubRecipe item = (SubRecipe)rows.get(rowIndex);
      if (item == null)
        return "";
      switch (columnIndex) {
      case 0: 
        return item.getName();
      
      case 1: 
        return item.getPortion();
      
      case 2: 
        return item.getYield();
      
      case 3: 
        return item.getCost();
      }
      
      return null;
    }
    
    public void setValueAt(Object aValue, int rowIndex, int columnIndex)
    {
      SubRecipe item = (SubRecipe)rows.get(rowIndex);
      switch (columnIndex) {
      case 1: 
        double portion = 1.0D;
        try {
          portion = Double.valueOf(String.valueOf(aValue)).doubleValue();
        }
        catch (Exception localException) {}
        item.setPortion(Double.valueOf(portion));
        if (listener != null) {
          listener.stateChanged(null);
        }
        break;
      default: 
        super.setValueAt(aValue, rowIndex, columnIndex);
      }
    }
  }
  
  public void setItems(List<SubRecipe> recepieItems) {
    if (recepieItems != null) {
      for (SubRecipe subRecipe : recepieItems) {
        List<RecepieItem> items = subRecipe.getRecipe().getRecepieItems();
        if (items != null) {
          for (RecepieItem recepieItem : items) {
            MenuItemDAO.getInstance().initialize(recepieItem.getInventoryItem());
          }
        }
      }
    }
    model.setRows(recepieItems);
  }
  
  public class TextCellEditor extends AbstractCellEditor implements javax.swing.table.TableCellEditor {
    JTextField jtextfield;
    
    public TextCellEditor(JTextField jtf) {
      jtextfield = jtf;
    }
    
    public Object getCellEditorValue() {
      return jtextfield.getText();
    }
    
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
      if ((!isSelected) || 
      
        (value == null)) {
        value = "";
      }
      value = value.toString();
      if ((value instanceof Integer)) {
        value = value.toString();
      }
      jtextfield.setText((String)value);
      return jtextfield;
    }
    
    public boolean stopCellEditing() {
      fireEditingStopped();
      return true;
    }
  }
  
  public void removeItem(int selectedRow)
  {
    model.deleteItem(selectedRow);
  }
  
  public SubRecipe getRow(int index) {
    return (SubRecipe)model.getRowData(index);
  }
  
  public void addRow(SubRecipe item) {
    model.addItem(item);
  }
  
  public List<SubRecipe> getRows() {
    return model.getRows();
  }
  
  public void setParentMenuItem(MenuItem e) {}
  
  public void setValueChangeListener(ChangeListener listener)
  {
    this.listener = listener;
  }
}
