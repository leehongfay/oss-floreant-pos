package com.orostock.inventory.ui.recepie;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.BeanTableModel.DataType;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;




public class InventoryItemSelector
  extends BeanEditorDialog
{
  private JLabel lblPercentage = new JLabel("Percentage");
  private BeanTableModel<MenuItem> tableModel;
  private JXTable table;
  DoubleTextField tfPercentage = new DoubleTextField(5);
  MenuItem selectedItem;
  double percentage;
  JCheckBox cbInventoryDeductable = new JCheckBox("Deduct from inventory");
  boolean inventoryDeductable = true;
  
  boolean dataLoaded;
  private JLabel lblName;
  private JTextField tfName;
  private JButton searchBttn;
  private JPanel searchPanel;
  private JLabel lblNumberOfItem = new JLabel();
  private Component btnPrev;
  private Component btnNext;
  
  public InventoryItemSelector() {
    setBeanEditor(new InventoryItemSelectorView());
  }
  
  public MenuItem getSelectedItem()
  {
    return selectedItem;
  }
  
  public double getPercentage() {
    return percentage;
  }
  
  public boolean isInventoryDeductable() {
    return inventoryDeductable;
  }
  
  public void loadData() {
    if (dataLoaded) {
      return;
    }
    

    tableModel.setNumRows(MenuItemDAO.getInstance().getRowCount(tfName.getText()));
    MenuItemDAO.getInstance().loadInventoryItems(tfName.getText(), tableModel);
    updateButton();
    dataLoaded = true;
  }
  
  public void dispose()
  {
    BeanEditor editor = getBeanEditor();
    
    super.dispose();
    
    setBeanEditor(editor);
  }
  
  class InventoryItemSelectorView extends BeanEditor<MenuItem>
  {
    public InventoryItemSelectorView() {
      setLayout(new BorderLayout(5, 5));
      
      tableModel = new BeanTableModel(MenuItem.class);
      tableModel.addColumn("Name", "name");
      tableModel.addColumn("Cost", "cost", 11, BeanTableModel.DataType.MONEY);
      tableModel.setPageSize(10);
      table = new JXTable(tableModel);
      table.setRowHeight(PosUIManager.getSize(40));
      
      JPanel contentPanel = new JPanel(new BorderLayout());
      contentPanel.setBorder(new EmptyBorder(10, 5, 10, 5));
      JScrollPane scroll = new JScrollPane(table);
      scroll.setPreferredSize(PosUIManager.getSize(500, 250));
      contentPanel.add(scroll);
      contentPanel.add(buildSearchForm(), "North");
      
      add(contentPanel);
      
      JPanel paginationButtonPanel = new JPanel(new MigLayout("fillx", "[][grow][]"));
      
      btnPrev = new PosButton();
      ((AbstractButton)btnPrev).setIcon(IconFactory.getIcon("/ui_icons/", "previous.png"));
      
      PosButton btnDot = new PosButton();
      btnDot.setBorder(null);
      btnDot.setOpaque(false);
      btnDot.setContentAreaFilled(false);
      btnDot.setIcon(IconFactory.getIcon("/ui_icons/", "dot.png"));
      
      btnNext = new PosButton();
      ((AbstractButton)btnNext).setIcon(IconFactory.getIcon("/ui_icons/", "next.png"));
      
      paginationButtonPanel.add(lblNumberOfItem, "right");
      paginationButtonPanel.add(btnPrev, "right,split 2");
      paginationButtonPanel.add(btnNext, "");
      
      contentPanel.add(paginationButtonPanel, "South");
      
      ActionListener action = new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try {
            Object source = e.getSource();
            if (source == btnPrev) {
              InventoryItemSelector.this.scrollUp();
            }
            else if (source == btnNext) {
              InventoryItemSelector.this.scrollDown();
            }
          } catch (Exception e2) {
            POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e2.getMessage(), e2);
          }
          
        }
      };
      ((AbstractButton)btnPrev).addActionListener(action);
      ((AbstractButton)btnNext).addActionListener(action);
      
      btnNext.setEnabled(false);
      btnPrev.setEnabled(false);
      
      JPanel panel = new JPanel(new MigLayout());
      panel.add(contentPanel, "wrap,grow");
      
      panel.add(lblPercentage, "gap left 8,split 3");
      panel.add(tfPercentage, "grow");
      panel.add(cbInventoryDeductable);
      
      add(panel);
      
      tfPercentage.setText("100");
      cbInventoryDeductable.setSelected(true);
    }
    
    public boolean save()
    {
      int index = table.getSelectedRow();
      selectedItem = ((MenuItem)tableModel.getRow(index));
      percentage = ((float)tfPercentage.getDouble());
      inventoryDeductable = cbInventoryDeductable.isSelected();
      
      return true;
    }
    

    protected void updateView() {}
    
    protected boolean updateModel()
      throws IllegalModelStateException
    {
      return true;
    }
    
    public String getDisplayText()
    {
      return "Select inventory item";
    }
  }
  
  private void scrollDown()
  {
    tableModel.setCurrentRowIndex(tableModel.getNextRowIndex());
    searchItem();
  }
  
  private void scrollUp() {
    tableModel.setCurrentRowIndex(tableModel.getPreviousRowIndex());
    searchItem();
  }
  
  public JPanel buildSearchForm() {
    searchPanel = new JPanel(new MigLayout("fillx", "[][grow]"));
    lblName = new JLabel(POSConstants.NAME);
    tfName = new JTextField();
    searchBttn = new JButton(POSConstants.SEARCH_ITEM_BUTTON_TEXT);
    searchPanel.add(lblName);
    searchPanel.add(tfName, "grow");
    
    JButton btnSearch = new JButton("Search");
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tableModel.setCurrentRowIndex(0);
        searchItem();
      }
    });
    searchPanel.add(btnSearch, "wrap");
    
    searchBttn.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        tableModel.setCurrentRowIndex(0);
        searchItem();
      }
      
    });
    tfName.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        tableModel.setCurrentRowIndex(0);
        searchItem();
      }
    });
    return searchPanel;
  }
  
  public void searchItem() {
    tableModel.setNumRows(MenuItemDAO.getInstance().getRowCount(tfName.getText()));
    MenuItemDAO.getInstance().loadInventoryItems(tfName.getText(), tableModel);
    updateButton();
    table.repaint();
  }
  
  private void updateButton()
  {
    int startNumber = tableModel.getCurrentRowIndex() + 1;
    int endNumber = tableModel.getNextRowIndex();
    int totalNumber = tableModel.getNumRows();
    if (endNumber > totalNumber) {
      endNumber = totalNumber;
    }
    lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
    btnPrev.setEnabled(tableModel.hasPrevious());
    btnNext.setEnabled(tableModel.hasNext());
    
    if (tableModel.getRowCount() > 0) {
      table.setRowSelectionInterval(0, 0);
    }
  }
  
  public void setPercentage(double percentage) {
    tfPercentage.setText(String.valueOf(percentage));
  }
  
  public void setInventoryDeductable(boolean inventoryDeductable) {
    cbInventoryDeductable.isSelected();
  }
}
