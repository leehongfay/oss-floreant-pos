package com.orostock.inventory.ui.recepie;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.Recepie;
import com.floreantpos.model.RecipeTable;
import com.floreantpos.model.dao.RecepieDAO;
import com.floreantpos.model.dao.RecipeTableDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.MenuItemSelectionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orostock.inventory.ui.recepie.template.RecipePreparationEntryForm;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.collections.CollectionUtils;
import org.jdesktop.swingx.JXTable;






















public class RecipeExplorer
  extends TransparentPanel
{
  private JXTable table;
  private BeanTableModel<Recepie> tableModel;
  private JTextField tfRecipeName = new JTextField(20);
  
  public RecipeExplorer()
  {
    tableModel = new BeanTableModel(Recepie.class);
    tableModel.addColumn("ITEM NAME", "menuItemName");
    tableModel.addColumn("RECIPE " + POSConstants.NAME.toUpperCase(), Recepie.PROP_NAME);
    tableModel.addColumn("YIELD", Recepie.PROP_YIELD);
    tableModel.addColumn("YIELD UNIT", Recepie.PROP_YIELD_UNIT);
    tableModel.addColumn("PORTION", Recepie.PROP_PORTION);
    tableModel.addColumn("PORTION UNIT", Recepie.PROP_PORTION_UNIT);
    tableModel.addRows(RecepieDAO.getInstance().findAll());
    
    table = new JXTable(tableModel);
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          RecipeExplorer.this.editSelectedRow();
        }
      }
    });
    table.setRowHeight(PosUIManager.getSize(35));
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    JPanel topSearchPanel = new JPanel(new MigLayout());
    topSearchPanel.add(new JLabel("Search by recipe or item name:"));
    topSearchPanel.add(tfRecipeName);
    
    tfRecipeName.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        RecipeExplorer.this.doSearchItems();
      }
      
    });
    JButton btnSearch = new JButton(POSConstants.SEARCH_ITEM_BUTTON_TEXT);
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        RecipeExplorer.this.doSearchItems();
      }
    });
    topSearchPanel.add(btnSearch);
    add(topSearchPanel, "North");
    addButtonPanel();
  }
  
  private void doSearchItems() {
    tableModel.setCurrentRowIndex(0);
    RecepieDAO.getInstance().loadRecepies(tableModel, tfRecipeName.getText(), false);
    table.repaint();
  }
  
  private void addButtonPanel() {
    JButton btnSelectMenuItem = new JButton("Select Items");
    btnSelectMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        RecipeExplorer.this.applyRecipeToMenuItem();
      }
    });
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        RecipeExplorer.this.doAddNewRecipe();
      }
      
    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        RecipeExplorer.this.editSelectedRow();
      }
      
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          Recepie orderType = (Recepie)tableModel.getRow(index);
          
          if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
            return;
          }
          
          RecepieDAO dao = new RecepieDAO();
          dao.delete(orderType);
          tableModel.removeRow(index);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton btnPreparation = new JButton("Preparation");
    btnPreparation.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        RecipeExplorer.this.doPreparationRecipe();
      }
      

    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(btnPreparation);
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    add(panel, "South");
  }
  
  private void doPreparationRecipe() {
    RecipePreparationEntryForm form = new RecipePreparationEntryForm();
    form.setSize(650, 550);
    form.open();
  }
  
  private void doAddNewRecipe() {
    try {
      RecipeEntryForm editor = new RecipeEntryForm(new Recepie());
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.openWithScale(700, 650);
      
      if (dialog.isCanceled()) {
        return;
      }
      Recepie recipe = (Recepie)editor.getBean();
      tableModel.addRow(recipe);
    }
    catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void applyRecipeToMenuItem() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      Recepie recipe = (Recepie)tableModel.getRow(index);
      
      List<MenuItem> items = RecipeTableDAO.getInstance().findMenuItems(recipe);
      
      MenuItemSelectionDialog dialog = new MenuItemSelectionDialog(items);
      dialog.setSize(700, 600);
      dialog.open();
      if (dialog.isCanceled())
        return;
      List<MenuItem> menuItems = dialog.getSelectedItems();
      
      if (CollectionUtils.isEqualCollection(items, menuItems)) {
        return;
      }
      List<RecipeTable> recipeTables = new ArrayList();
      List<MenuItem> deletedRecipeMenuItems = new ArrayList();
      if (menuItems != null) {
        for (MenuItem menuItem : menuItems) {
          RecipeTable bean = new RecipeTable();
          bean.setMenuItem(menuItem);
          recipeTables.add(bean);
        }
      }
      if (items != null) {
        for (MenuItem menuItem : items) {
          if (!menuItems.contains(menuItem)) {
            deletedRecipeMenuItems.add(menuItem);
          }
        }
      }
      RecipeTableDAO.getInstance().saveRecipeTables(deletedRecipeMenuItems, recipeTables.toArray());
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      Recepie recipe = (Recepie)tableModel.getRow(index);
      
      RecipeEntryForm editor = new RecipeEntryForm(recipe);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.openWithScale(700, 650);
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
}
