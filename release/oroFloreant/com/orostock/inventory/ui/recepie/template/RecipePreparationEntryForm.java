package com.orostock.inventory.ui.recepie.template;

import com.floreantpos.model.Recepie;
import com.floreantpos.model.dao.RecepieDAO;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.util.List;
import javax.swing.JPanel;















public class RecipePreparationEntryForm
  extends OkCancelOptionDialog
{
  RecipePreparationExplorer explorer;
  
  public RecipePreparationEntryForm()
  {
    setCaption("Recipe preparation");
    explorer = new RecipePreparationExplorer();
    getContentPanel().add(explorer);
  }
  
  public void doOk()
  {
    try {
      List<Recepie> recipes = explorer.getRows();
      if ((recipes == null) || (recipes.size() <= 0)) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select recipe.");
        return;
      }
      for (Recepie recepie : recipes) {
        Double actualYieldValue = recepie.getYield();
        Double cookingYield = recepie.getCookingYield();
        recepie.setPortion(Double.valueOf(cookingYield.doubleValue() / actualYieldValue.doubleValue() * recepie.getPortion().doubleValue()));
      }
      RecepieDAO.getInstance().adjustInventory(recipes);
      setCanceled(false);
      dispose();
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Preparation successfully complete.");
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), e.getMessage());
    }
  }
}
