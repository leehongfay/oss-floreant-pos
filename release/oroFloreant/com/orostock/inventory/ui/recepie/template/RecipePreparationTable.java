package com.orostock.inventory.ui.recepie.template;

import com.floreantpos.POSConstants;
import com.floreantpos.model.Recepie;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.BeanTableModel.BeanColumn;
import com.floreantpos.swing.BeanTableModel.EditMode;
import com.floreantpos.swing.PosUIManager;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.jdesktop.swingx.JXTable;

class RecipePreparationTable extends JXTable
{
  private BeanTableModel<Recepie> tableModel;
  
  public RecipePreparationTable()
  {
    tableModel = new BeanTableModel(Recepie.class);
    tableModel.addColumn("MENU ITEM", "menuItemName");
    tableModel.addColumn("RECIPE " + POSConstants.NAME.toUpperCase(), Recepie.PROP_NAME);
    tableModel.addColumn("YIELD", "cookingYield", BeanTableModel.EditMode.EDITABLE);
    tableModel.addColumn("YIELD UNIT", Recepie.PROP_YIELD_UNIT);
    setModel(tableModel);
    
    setEditable(true);
    getTableHeader().setPreferredSize(PosUIManager.getSize(0, 35));
    setRowHeight(PosUIManager.getSize(35));
  }
  
  public void resizeColumnWidth(JTable table) {
    TableColumnModel columnModel = table.getColumnModel();
    for (int column = 0; column < table.getColumnCount(); column++) {
      columnModel.getColumn(column).setPreferredWidth(((Integer)getColumnWidth().get(column)).intValue());
    }
  }
  
  private List getColumnWidth() {
    List<Integer> columnWidth = new ArrayList();
    columnWidth.add(Integer.valueOf(200));
    columnWidth.add(Integer.valueOf(90));
    return columnWidth;
  }
  
  public void changeSelection(int row, int column, boolean toggle, boolean extend)
  {
    super.changeSelection(row, column, toggle, extend);
    javax.swing.table.TableModel model = super.getModel();
    BeanTableModel beanTableModel = (BeanTableModel)model;
    BeanTableModel.BeanColumn beanColumn = beanTableModel.getColumn(column);
    if (beanColumn.isEditable()) {
      editCellAt(row, column);
      DefaultCellEditor editor = (DefaultCellEditor)getCellEditor(row, column);
      JTextField textField = (JTextField)editor.getComponent();
      textField.requestFocus();
      textField.selectAll();
    }
  }
  


  public void setValueAt(Object yield, int row, int column)
  {
    Recepie recepie = (Recepie)tableModel.getRow(row);
    super.setValueAt(yield, row, column);
    if (column == 2) {
      recepie.setCookingYield((Double)yield);
    }
  }
  
  public BeanTableModel<Recepie> getTableModel() {
    return tableModel;
  }
}
