package com.orostock.inventory.ui.recepie.template;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.Recepie;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.TransparentPanel;
import com.orostock.inventory.ui.recepie.RecipeSelectionDialog;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import net.miginfocom.swing.MigLayout;

class RecipePreparationExplorer
  extends TransparentPanel
{
  private RecipePreparationTable table;
  private BeanTableModel<Recepie> tableModel;
  private JPanel buttonPanel;
  
  public RecipePreparationExplorer()
  {
    setLayout(new BorderLayout());
    table = new RecipePreparationTable();
    tableModel = table.getTableModel();
    
    add(new JScrollPane(table));
    buttonPanel = addButtonPanel();
    add(buttonPanel, "South");
  }
  
  private JPanel addButtonPanel() {
    JButton btnSelectMenuItem = new JButton("ADD ITEMS");
    btnSelectMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        RecipePreparationExplorer.this.doAddRecipes();
      }
    });
    JButton deleteButton = new JButton(POSConstants.REMOVE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          tableModel.removeRow(index);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    JButton btnClearItems = new JButton(POSConstants.CLEAR);
    btnClearItems.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          tableModel.removeAll();
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.setLayout(new MigLayout("left"));
    panel.add(btnSelectMenuItem);
    panel.add(deleteButton);
    panel.add(btnClearItems);
    return panel;
  }
  
  private void doAddRecipes() {
    try {
      RecipeSelectionDialog dialog = new RecipeSelectionDialog(new ArrayList());
      dialog.setSize(650, 550);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      List<Recepie> recepies = dialog.getSelectedItems();
      tableModel.addRows(recepies);
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public List<Recepie> getRows() {
    return tableModel.getRows();
  }
  
  public Container getButtonPanel() {
    return buttonPanel;
  }
}
