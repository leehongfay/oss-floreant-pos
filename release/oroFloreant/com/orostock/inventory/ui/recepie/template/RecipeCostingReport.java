package com.orostock.inventory.ui.recepie.template;

import com.floreantpos.model.Recepie;
import com.floreantpos.report.Report;
import com.floreantpos.report.ReportUtil;
import com.floreantpos.services.report.ReportService;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JRViewer;



















public class RecipeCostingReport
  extends Report
{
  private List<Recepie> recipes;
  
  public RecipeCostingReport() {}
  
  public void setRecepies(List<Recepie> recipes)
  {
    this.recipes = recipes;
  }
  
  public void refresh() throws Exception
  {
    RecipeReportModel reportModel = new RecipeReportModel();
    reportModel.setItems(recipes);
    
    HashMap map = new HashMap();
    ReportUtil.populateRestaurantProperties(map);
    map.put("reportTitle", "Recipe Cost Estimate");
    map.put("reportTime", ReportService.formatFullDate(new Date()));
    
    JasperReport masterReport = ReportUtil.getReport("recipe_costing_report");
    JasperPrint print = JasperFillManager.fillReport(masterReport, map, new JRTableModelDataSource(reportModel));
    viewer = new JRViewer(print);
    viewer.setZoomRatio(0.8F);
  }
  
  public boolean isDateRangeSupported()
  {
    return false;
  }
  
  public boolean isTypeSupported()
  {
    return false;
  }
  
  public JRViewer getViewer()
  {
    return viewer;
  }
}
