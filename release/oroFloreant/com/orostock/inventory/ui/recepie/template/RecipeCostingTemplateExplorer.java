package com.orostock.inventory.ui.recepie.template;

import com.floreantpos.swing.TransparentPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;

















public class RecipeCostingTemplateExplorer
  extends TransparentPanel
{
  private RecipeCostingReport report;
  private TransparentPanel reportPanel;
  RecipePreparationExplorer explorer;
  
  public RecipeCostingTemplateExplorer()
  {
    init();
    showReport();
  }
  
  private void init() {
    setLayout(new BorderLayout(5, 5));
    
    JPanel leftMenuPanel = new JPanel(new BorderLayout());
    explorer = new RecipePreparationExplorer();
    leftMenuPanel.add(explorer);
    JPanel container = new JPanel(new BorderLayout());
    
    report = new RecipeCostingReport();
    reportPanel = new TransparentPanel();
    reportPanel.setLayout(new BorderLayout());
    
    container.add(reportPanel);
    
    JButton btnShowReport = new JButton("Show Report");
    btnShowReport.setBackground(Color.GREEN);
    btnShowReport.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        RecipeCostingTemplateExplorer.this.showReport();
      }
    });
    explorer.getButtonPanel().add(btnShowReport);
    
    add(container);
    
    add(leftMenuPanel, "West");
    showReport();
  }
  
  private void showReport() {
    reportPanel.removeAll();
    reportPanel.revalidate();
    try {
      report.setRecepies(explorer.getRows());
      report.refresh();
    } catch (Exception e) {
      e.printStackTrace();
    }
    if ((report != null) && (report.getViewer() != null)) {
      reportPanel.add(report.getViewer());
      reportPanel.revalidate();
      reportPanel.repaint();
    }
  }
}
