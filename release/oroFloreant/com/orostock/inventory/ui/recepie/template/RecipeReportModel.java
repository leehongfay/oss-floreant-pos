package com.orostock.inventory.ui.recepie.template;

import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.Recepie;
import com.floreantpos.model.RecepieItem;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.util.CopyUtil;
import com.floreantpos.util.NumberUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.swing.table.AbstractTableModel;


















public class RecipeReportModel
  extends AbstractTableModel
{
  private String[] columnNames = { "itemGroup", "items", "name", "unit", "quantity", "cost", "onhandvalue", "group", "rcp", "yieldQty", "yieldUnit", "additionalInfo" };
  
  private List<RecepieItem> items;
  

  public RecipeReportModel() {}
  
  public int getRowCount()
  {
    if (items == null) {
      return 0;
    }
    return items.size();
  }
  
  public int getColumnCount() {
    return columnNames.length;
  }
  
  public String getColumnName(int column)
  {
    return columnNames[column];
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    RecepieItem recipeItem = (RecepieItem)items.get(rowIndex);
    MenuItem item = recipeItem.getInventoryItem();
    Recepie recepie = recipeItem.getRecepie();
    switch (columnIndex) {
    case 0: 
      return "";
    case 1: 
      return recipeItem.getGroupName();
    case 2: 
      return item.getDisplayName();
    
    case 3: 
      return item.getUnit().getUniqueCode();
    
    case 4: 
      return recipeItem.getQuantity();
    
    case 5: 
      return item.getCost();
    case 6: 
      return Double.valueOf(recipeItem.getQuantity().doubleValue() * item.getCost().doubleValue());
    
    case 7: 
      return recipeItem.getGroupId();
    
    case 8: 
      return recepie.getCode();
    case 9: 
      return NumberUtil.trimDecilamIfNotNeeded(recepie.getCookingYield());
    case 10: 
      return recepie.getYieldUnit();
    case 11: 
      int cookingMin = recepie.getCookingMin();
      if (cookingMin > 0) {
        return cookingMin + "m";
      }
      return "";
    }
    
    return null;
  }
  
  public void setItems(List<Recepie> recipes) {
    if ((recipes == null) || (recipes.size() == 0))
      return;
    items = buildItemMapForInventoryAdjustment(recipes);
    Iterator iterator; if (items.size() > 0) {
      for (iterator = items.iterator(); iterator.hasNext();) {
        RecepieItem item = (RecepieItem)iterator.next();
        MenuItemDAO.getInstance().initialize(item.getInventoryItem());
      }
    }
  }
  
  private List<RecepieItem> buildItemMapForInventoryAdjustment(List<Recepie> items) {
    List<RecepieItem> recipeItems = new ArrayList();
    
    for (Iterator localIterator = items.iterator(); localIterator.hasNext();) { recipe = (Recepie)localIterator.next();
      HashMap<String, RecepieItem> itemMap = new HashMap();
      recipe.populateRecipeItems(itemMap, recipe.getCookingYield().doubleValue() / recipe.getYield().doubleValue(), recipe, false);
      if (itemMap.size() > 0) {
        recipeItems.addAll(new ArrayList(itemMap.values()));
      }
    }
    Object summaryMap = new HashMap();
    for (Recepie recipe = items.iterator(); recipe.hasNext();) { recipe = (Recepie)recipe.next();
      recipe.populateRecipeItems((HashMap)summaryMap, recipe.getCookingYield().doubleValue() / recipe.getYield().doubleValue(), recipe, true); }
    Recepie recipe;
    Collections.sort(recipeItems, new Comparator()
    {
      public int compare(RecepieItem o1, RecepieItem o2)
      {
        return o1.getGroupName().compareTo(o2.getGroupName());
      }
    });
    if (((HashMap)summaryMap).size() > 0) {
      List<RecepieItem> filteredItems = new ArrayList(((HashMap)summaryMap).values());
      for (RecepieItem recipeItem : filteredItems) {
        try {
          RecepieItem summaryRecipeItem = (RecepieItem)CopyUtil.deepCopy(recipeItem);
          summaryRecipeItem.setGroupName("Summary");
          summaryRecipeItem.setGroupId("");
          recipeItems.add(summaryRecipeItem);
        }
        catch (Exception localException) {}
      }
    }
    return recipeItems;
  }
}
