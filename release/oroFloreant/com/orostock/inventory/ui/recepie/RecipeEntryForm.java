package com.orostock.inventory.ui.recepie;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.IUnit;
import com.floreantpos.model.InventoryStockUnit;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.Recepie;
import com.floreantpos.model.RecepieItem;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.model.dao.RecepieDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.MenuItemSelectionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.CopyUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;






public class RecipeEntryForm
  extends BeanEditor<Recepie>
  implements ChangeListener
{
  private RecipeItemTable table;
  private JButton btnAddItem = new JButton("Add Recipe Item");
  private JButton btnDeleteItem = new JButton("Remove");
  private DoubleTextField tfTotalCost;
  private FixedLengthTextField tfRecepieName = new FixedLengthTextField();
  private FixedLengthTextField tfCode = new FixedLengthTextField();
  private DoubleTextField tfPortion = new DoubleTextField(6);
  private DoubleTextField tfCookingMinute = new DoubleTextField(4);
  private DoubleTextField tfYield = new DoubleTextField(6);
  private JTextArea taInstruction = new JTextArea(4, 4);
  private JRadioButton rbBatchProcess = new JRadioButton("Batch");
  private JRadioButton rbContinuousProcess = new JRadioButton("Continuous");
  private DoubleTextField tfAdjustment = new DoubleTextField(8);
  private DoubleTextField tfLaborCost = new DoubleTextField(8);
  private JComboBox cbPortionUnits = new JComboBox();
  private JComboBox cbYieldUnits = new JComboBox();
  
  private FixedLengthTextField tfMenuItemName = new FixedLengthTextField();
  private MenuItem selectedMenuItem;
  
  public RecipeEntryForm(Recepie recepie) {
    initComponents();
    cbYieldUnits.setModel(new ComboBoxModel());
    cbYieldUnits.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        IUnit unit = (IUnit)cbYieldUnits.getSelectedItem();
        if (unit != null) {
          if ((unit instanceof InventoryStockUnit)) {
            InventoryStockUnit stockUnit = (InventoryStockUnit)unit;
            tfPortion.setText(String.valueOf(stockUnit.getConversionValue()));
          }
          else {
            tfPortion.setText("1");
          }
        }
      }
    });
    cbPortionUnits.setModel(new ComboBoxModel());
    setBean(recepie);
  }
  
  private void renderUnits() {
    if (selectedMenuItem != null) {
      MenuItemDAO.getInstance().initialize(selectedMenuItem);
      List<IUnit> itemUnits = new ArrayList();
      itemUnits.add(selectedMenuItem.getUnit());
      List yieldUnits = new ArrayList();
      yieldUnits.add(selectedMenuItem.getUnit());
      yieldUnits.addAll(selectedMenuItem.getStockUnits());
      ComboBoxModel yieldModel = (ComboBoxModel)cbYieldUnits.getModel();
      yieldModel.setDataList(yieldUnits);
      ComboBoxModel portionModel = (ComboBoxModel)cbPortionUnits.getModel();
      portionModel.setDataList(itemUnits);
      cbPortionUnits.setSelectedIndex(0);
      cbYieldUnits.setSelectedIndex(0);
    }
  }
  
  public void setMenuItem(MenuItem menuItem) {
    selectedMenuItem = menuItem;
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
    JPanel topPanel = new JPanel(new MigLayout("fillx,wrap 2", "[][grow]", ""));
    
    JLabel lblRecipeTableName = new JLabel("Menu Item");
    tfMenuItemName.setLength(128);
    tfMenuItemName.setEditable(false);
    
    topPanel.add(lblRecipeTableName, "");
    topPanel.add(tfMenuItemName, "growx,split 2");
    
    JButton btnAddMenuItem = new JButton("Select Menu Item");
    btnAddMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuItemSelectionDialog dialog = new MenuItemSelectionDialog(new ArrayList());
        dialog.setSelectionMode(0);
        dialog.setSize(PosUIManager.getSize(600, 515));
        dialog.open();
        if (dialog.isCanceled()) {
          return;
        }
        selectedMenuItem = dialog.getSelectedRowData();
        tfMenuItemName.setText(selectedMenuItem.getName());
        RecipeEntryForm.this.renderUnits();
      }
    });
    topPanel.add(btnAddMenuItem, "");
    
    JLabel lblRecepieName = new JLabel("Recipe Name");
    tfRecepieName.setLength(128);
    tfCode.setLength(16);
    
    topPanel.add(new JLabel("Code"), "");
    topPanel.add(tfCode, "growx,wrap");
    
    topPanel.add(lblRecepieName, "");
    topPanel.add(tfRecepieName, "growx,split 2");
    
    JButton btnSelectFromExistingRecipe = new JButton("Select recipe");
    btnSelectFromExistingRecipe.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (selectedMenuItem == null) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select menu item first.");
          return;
        }
        List<Recepie> recipes = new ArrayList();
        RecipeSelectionDialog dialog = new RecipeSelectionDialog(recipes, false);
        dialog.setSelectionMode(0);
        dialog.setSize(PosUIManager.getSize(600, 515));
        dialog.open();
        if (dialog.isCanceled()) {
          return;
        }
        try {
          Recepie recipe = (Recepie)CopyUtil.deepCopy(dialog.getSelectedRowData());
          recipe.setId(null);
          recipe.setMenuItem(selectedMenuItem);
          List<RecepieItem> recepieItems = recipe.getRecepieItems();
          if (recepieItems != null) {
            for (RecepieItem item : recepieItems) {
              item.setId(null);
              item.setRecepie(recipe);
            }
          }
          recipe.setPortion(Double.valueOf(1.0D));
          setBean(recipe);
        } catch (Exception e1) {
          POSMessageDialog.showError(e1.getMessage());
        }
      }
    });
    topPanel.add(btnSelectFromExistingRecipe, "wrap");
    
    topPanel.add(new JLabel("Yield"), "right");
    topPanel.add(tfYield, "split 5");
    topPanel.add(cbYieldUnits);
    tfYield.setEditable(false);
    
    topPanel.add(new JLabel("Portion"));
    tfPortion.setEditable(false);
    topPanel.add(tfPortion);
    topPanel.add(cbPortionUnits);
    
    topPanel.add(new JLabel("Instruction"), "right");
    topPanel.add(new JScrollPane(taInstruction), "growx");
    
    JPanel timePanel = new JPanel(new MigLayout("left"));
    timePanel.setBorder(new TitledBorder("Cooking Process & Time"));
    

    timePanel.add(rbBatchProcess, "split 2");
    timePanel.add(rbContinuousProcess);
    
    timePanel.add(new JLabel("Time:"), "gapleft 20,split 3");
    timePanel.add(tfCookingMinute);
    timePanel.add(new JLabel("Min"));
    
    topPanel.add(timePanel, "skip 1,growx");
    
    ButtonGroup group = new ButtonGroup();
    group.add(rbBatchProcess);
    group.add(rbContinuousProcess);
    
    rbBatchProcess.setSelected(true);
    
    tfPortion.setHorizontalAlignment(4);
    tfYield.setHorizontalAlignment(4);
    
    taInstruction.setLineWrap(true);
    
    add(topPanel, "North");
    
    table = new RecipeItemTable(new ArrayList());
    table.setValueChangeListener(this);
    
    JPanel recipeItemPanel = new JPanel(new BorderLayout());
    recipeItemPanel.add(new JScrollPane(table));
    
    JPanel buttonPanel1 = new JPanel(new MigLayout("fillx,ins 5 5 5 0", "[][grow]", ""));
    buttonPanel1.add(btnAddItem, "split 2");
    buttonPanel1.add(btnDeleteItem);
    
    JLabel lblCost = new JLabel("Total:");
    tfTotalCost = new DoubleTextField(8);
    tfTotalCost.setEditable(false);
    tfTotalCost.setHorizontalAlignment(4);
    recipeItemPanel.add(buttonPanel1, "South");
    
    buttonPanel1.add(new JLabel("Labor Cost:"), "split 2,span,right");
    buttonPanel1.add(tfLaborCost, "wrap");
    
    buttonPanel1.add(new JLabel("Adjustment:"), "split 2,span,right");
    buttonPanel1.add(tfAdjustment, "wrap");
    tfLaborCost.setHorizontalAlignment(4);
    tfAdjustment.setHorizontalAlignment(4);
    tfAdjustment.addKeyListener(new KeyListener()
    {
      public void keyTyped(KeyEvent e) {}
      


      public void keyReleased(KeyEvent e)
      {
        stateChanged(null);
      }
      



      public void keyPressed(KeyEvent e) {}
    });
    buttonPanel1.add(lblCost, "split 2,span,right");
    buttonPanel1.add(tfTotalCost);
    
    JPanel container = new JPanel(new MigLayout("fillx,wrap 1", "", "[grow]"));
    container.add(recipeItemPanel, "grow");
    
    add(container);
    
    btnAddItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        addInventoryItem();
      }
      
    });
    btnDeleteItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          int selectedRow = table.getSelectedRow();
          if (selectedRow < 0) {
            return;
          }
          if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
            return;
          }
          table.removeItem(selectedRow);
          stateChanged(null);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
      }
    });
  }
  
  protected void addInventoryItem()
  {
    Map<String, RecepieItem> itemMap = new HashMap();
    List<MenuItem> inventoryItems = new ArrayList();
    Collection<? extends RecepieItem> rows = table.getRows();
    if (rows != null) {
      for (RecepieItem recepieItem : rows) {
        inventoryItems.add(recepieItem.getInventoryItem());
        itemMap.put(recepieItem.getInventoryItem().getId(), recepieItem);
      }
    }
    MenuItemSelectionDialog dialog = new MenuItemSelectionDialog(inventoryItems, true);
    dialog.setSize(600, 500);
    dialog.open();
    
    if (dialog.isCanceled()) {
      return;
    }
    List<MenuItem> selectedItems = dialog.getSelectedItems();
    if (selectedItems != null) {
      for (MenuItem menuItem : selectedItems) {
        RecepieItem recepieItem = (RecepieItem)itemMap.get(menuItem.getId());
        if (recepieItem == null) {
          MenuItemDAO.getInstance().initialize(menuItem);
          recepieItem = new RecepieItem();
          recepieItem.setInventoryItem(menuItem);
          recepieItem.setQuantity(Double.valueOf(1.0D));
          InventoryUnit unit = menuItem.getUnit();
          recepieItem.setUnit(unit);
          if (unit != null) {
            recepieItem.setUnitCode(unit.getCode());
          }
          itemMap.put(menuItem.getId(), recepieItem);
        }
      }
    }
    table.setItems(new ArrayList(itemMap.values()));
    stateChanged(null);
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      Recepie bean = (Recepie)getBean();
      RecepieDAO.getInstance().saveOrUpdate(bean);
    } catch (Exception e) {
      POSMessageDialog.showError(e.getMessage());
    }
    return true;
  }
  
  protected void updateView()
  {
    Recepie bean = (Recepie)getBean();
    if (bean == null) {
      tfPortion.setText("1.00");
      tfYield.setText("1.00");
      tfTotalCost.setText("0.00");
      return;
    }
    List<RecepieItem> recepieItems = bean.getRecepieItems();
    if (recepieItems != null) {
      for (RecepieItem recepieItem : recepieItems) {
        MenuItemDAO.getInstance().initialize(recepieItem.getInventoryItem());
      }
    }
    tfCode.setText(bean.getCode());
    tfRecepieName.setText(bean.getName());
    selectedMenuItem = bean.getMenuItem();
    
    renderUnits();
    if (selectedMenuItem != null) {
      table.setParentMenuItem(selectedMenuItem);
      tfMenuItemName.setText(selectedMenuItem.getDisplayName());
    }
    
    tfPortion.setText(String.valueOf(bean.getPortion()));
    tfYield.setText(String.valueOf(bean.getYield()));
    taInstruction.setText(bean.getDescription());
    tfAdjustment.setText(NumberUtil.formatNumber(bean.getAdjustmentAmount()));
    tfLaborCost.setText(NumberUtil.formatNumber(bean.getLaborCost()));
    
    rbContinuousProcess.setSelected(!bean.isBatchProcess().booleanValue());
    rbBatchProcess.setSelected(bean.isBatchProcess().booleanValue());
    
    String yieldUnit = bean.getYieldUnit();
    Iterator iterator; if (StringUtils.isNotEmpty(yieldUnit)) {
      ComboBoxModel model = (ComboBoxModel)cbYieldUnits.getModel();
      if (model.getSize() > 0) {
        for (iterator = model.getDataList().iterator(); iterator.hasNext();) {
          IUnit packagingUnit = (IUnit)iterator.next();
          if (packagingUnit != null)
          {
            if (packagingUnit.getUniqueCode().equals(yieldUnit))
              cbYieldUnits.setSelectedItem(packagingUnit);
          }
        }
      }
    }
    String portionUnit = bean.getPortionUnit();
    Iterator iterator; if (StringUtils.isNotEmpty(portionUnit)) {
      ComboBoxModel model = (ComboBoxModel)cbPortionUnits.getModel();
      if (model.getSize() > 0) {
        for (iterator = model.getDataList().iterator(); iterator.hasNext();) {
          IUnit packagingUnit = (IUnit)iterator.next();
          if (packagingUnit != null)
          {
            if (packagingUnit.getUniqueCode().equals(portionUnit)) {
              cbPortionUnits.setSelectedItem(packagingUnit);
            }
          }
        }
      }
    }
    setTimeFieldsValue(bean.getCookingTime().intValue());
    table.setItems(recepieItems);
    stateChanged(null);
  }
  
  public void setTimeFieldsValue(int timeInSeconds) {
    int hours = timeInSeconds / 3600;
    int secondsLeft = timeInSeconds - hours * 3600;
    int minutes = secondsLeft / 60;
    
    tfCookingMinute.setText(String.valueOf(minutes));
  }
  
  protected boolean updateModel() throws IllegalModelStateException
  {
    Recepie bean = (Recepie)getBean();
    String name = tfRecepieName.getText();
    if (selectedMenuItem == null) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select menu item");
      return false;
    }
    if (StringUtils.isEmpty(name)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please enter recepie name");
      return false;
    }
    if (table.getRowCount() <= 0) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select recepie items or sub recipe");
      return false;
    }
    bean.setCode(tfCode.getText());
    bean.setName(name);
    bean.setPortion(Double.valueOf(tfPortion.getDoubleOrZero()));
    bean.setYield(Double.valueOf(tfYield.getDoubleOrZero()));
    bean.setDescription(taInstruction.getText());
    
    List<RecepieItem> rows = new ArrayList(table.getRows());
    for (RecepieItem recepieItem : rows) {
      recepieItem.setRecepie(bean);
    }
    int seconds = (int)(tfCookingMinute.getDoubleOrZero() * 60.0D);
    bean.setCookingTime(Integer.valueOf(seconds));
    bean.setRecepieItems(rows);
    bean.setBatchProcess(Boolean.valueOf(rbBatchProcess.isSelected()));
    bean.setAdjustmentAmount(Double.valueOf(tfAdjustment.getDoubleOrZero()));
    bean.setLaborCost(Double.valueOf(tfLaborCost.getDoubleOrZero()));
    
    bean.setMenuItem(selectedMenuItem);
    
    IUnit yieldUnit = (IUnit)cbYieldUnits.getSelectedItem();
    if (yieldUnit != null) {
      bean.setYieldUnit(yieldUnit.getUniqueCode());
    }
    else {
      bean.setYieldUnit(null);
    }
    IUnit portionUnit = (IUnit)cbPortionUnits.getSelectedItem();
    if (portionUnit != null) {
      bean.setPortionUnit(portionUnit.getUniqueCode());
    }
    else {
      bean.setPortionUnit(null);
    }
    return true;
  }
  
  public String getDisplayText()
  {
    return ((Recepie)getBean()).getId() == null ? "Create New Recipe" : "Edit Recipe";
  }
  
  public void stateChanged(ChangeEvent e)
  {
    double totalRecipeItemCost = 0.0D;
    Collection<? extends RecepieItem> rows = table.getRows();
    if (rows != null) {
      for (RecepieItem recepieItem : rows) {
        recepieItem.calculatePercentage();
        totalRecipeItemCost += recepieItem.getCost();
      }
    }
    tfTotalCost.setText(NumberUtil.formatNumber(Double.valueOf(totalRecipeItemCost + tfAdjustment.getDoubleOrZero())));
  }
}
