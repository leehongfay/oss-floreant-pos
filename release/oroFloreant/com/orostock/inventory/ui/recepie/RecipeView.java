package com.orostock.inventory.ui.recepie;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.Recepie;
import com.floreantpos.model.RecepieItem;
import com.floreantpos.model.RecipeTable;
import com.floreantpos.model.dao.RecipeTableDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.IUpdatebleView;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import com.jidesoft.swing.TitledSeparator;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;




public class RecipeView
  extends JPanel
  implements IUpdatebleView<MenuItem>
{
  private RecipeItemTable table;
  private DoubleTextField tfTotalCost = new DoubleTextField(8);
  private JButton btnChangeRecipe = new JButton("Select Existing Recipe");
  boolean inited = false;
  protected Recepie recipe;
  private JTextField tfRecipeName;
  private RecipeTable recipeTable;
  private DoubleTextField tfPortion = new DoubleTextField(8);
  private JLabel lblPortionUnit;
  
  public RecipeView() {
    setLayout(new BorderLayout());
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    
    table = new RecipeItemTable(new ArrayList());
    table.setEnabled(false);
    
    JLabel lblRecipeItems = new JLabel("Recipe Items");
    Color fg = new Color(49, 106, 196);
    lblRecipeItems.setForeground(fg);
    JLabel lblSubRecipe = new JLabel("Sub Receipe");
    lblSubRecipe.setForeground(fg);
    
    TitledSeparator sep1 = new TitledSeparator(lblRecipeItems, 0);
    tfPortion.setHorizontalAlignment(4);
    JPanel tablePanel = new JPanel(new MigLayout("fill,wrap 1"));
    tablePanel.add(sep1, "span,grow");
    tablePanel.add(new JLabel("Portion"), "split 3,span,right");
    tablePanel.add(tfPortion);
    lblPortionUnit = new JLabel("");
    tablePanel.add(lblPortionUnit);
    tablePanel.add(new JScrollPane(table), "span,grow");
    tablePanel.add(new JLabel("Total Cost:"), "right,split 2");
    tablePanel.add(tfTotalCost);
    tfTotalCost.setEditable(false);
    tfTotalCost.setHorizontalAlignment(4);
    
    add(tablePanel);
    
    btnChangeRecipe.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        List<Recepie> recipes = new ArrayList();
        if (recipe != null) {
          recipes.add(recipe);
        }
        RecipeSelectionDialog dialog = new RecipeSelectionDialog(recipes, false);
        dialog.setSelectionMode(0);
        dialog.setSize(PosUIManager.getSize(600, 515));
        dialog.open();
        if (dialog.isCanceled()) {
          return;
        }
        recipe = dialog.getSelectedRowData();
        RecipeView.this.updateView();
      }
      
    });
    JPanel buttonPanel = new JPanel(new MigLayout("center"));
    
    JButton btnAddRecipe = new JButton("Create New Recipe");
    btnAddRecipe.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          RecipeEntryForm editor = new RecipeEntryForm(new Recepie());
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.openWithScale(700, 650);
          
          if (dialog.isCanceled()) {
            return;
          }
          recipe = ((Recepie)editor.getBean());
          RecipeView.this.updateView();
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
      }
    });
    JButton btnRemove = new JButton("Remove");
    btnRemove.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          recipe = null;
          RecipeView.this.updateView();
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
      }
    });
    JLabel lblRecipeName = new JLabel("Recipe Name:");
    tfRecipeName = new JTextField(30);
    tfRecipeName.setEditable(false);
    
    buttonPanel.add(lblRecipeName);
    buttonPanel.add(tfRecipeName, "growx");
    buttonPanel.add(btnChangeRecipe);
    buttonPanel.add(btnAddRecipe);
    buttonPanel.add(btnRemove);
    
    add(buttonPanel, "North");
  }
  
  public boolean updateModel(MenuItem e)
  {
    if ((recipe == null) && (recipeTable != null)) {
      RecipeTableDAO.getInstance().delete(recipeTable);
      return true;
    }
    if (recipeTable == null) {
      recipeTable = new RecipeTable();
    }
    if ((recipe != null) && (recipeTable.getPortion().intValue() != tfPortion.getDoubleOrZero())) {
      double portion = tfPortion.getDoubleOrZero();
      recipeTable.setPortion(Double.valueOf(portion <= 0.0D ? 1.0D : portion));
      recipeTable.setMenuItem(e);
      if (e.getId() == null) {
        TerminalDAO.getInstance().performBatchSave(new Object[] { e, recipeTable });
      }
      else {
        RecipeTableDAO.getInstance().saveOrUpdate(recipeTable);
      }
    }
    return true;
  }
  
  public void initView(MenuItem e)
  {
    if (inited)
      return;
    recipeTable = RecipeTableDAO.getInstance().findBy(e.getId());
    updateView();
    inited = true;
  }
  
  private void updateView() {
    if (recipe != null) {
      table.setItems(recipe.getRecepieItems());
      tfRecipeName.setText(recipe.getName());
      if (recipeTable == null) {
        tfPortion.setText("1");
      }
      else
        tfPortion.setText(String.valueOf(recipeTable.getPortion().doubleValue() == 0.0D ? 1.0D : recipeTable.getPortion().doubleValue()));
      lblPortionUnit.setText(recipe.getPortionUnit());
    }
    else {
      table.getRows().clear();
      tfRecipeName.setText("");
      table.revalidate();
      table.repaint();
      tfPortion.setText("");
      lblPortionUnit.setText("");
    }
    tfPortion.setEditable(recipe != null);
    updateTotalCost();
  }
  
  private void updateTotalCost() {
    double totalRecipeItemCost = 0.0D;
    Collection<? extends RecepieItem> rows = table.getRows();
    if (rows != null) {
      for (RecepieItem recepieItem : rows) {
        recepieItem.calculatePercentage();
        totalRecipeItemCost += recepieItem.getCost();
      }
    }
    tfTotalCost.setText(NumberUtil.formatNumber(Double.valueOf(totalRecipeItemCost)));
  }
}
