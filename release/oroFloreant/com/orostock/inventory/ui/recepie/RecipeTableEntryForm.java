package com.orostock.inventory.ui.recepie;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.Recepie;
import com.floreantpos.model.RecepieItem;
import com.floreantpos.model.RecipeTable;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.model.dao.RecepieDAO;
import com.floreantpos.model.dao.RecipeTableDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.MenuItemSelectionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import com.jidesoft.swing.TitledSeparator;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;






public class RecipeTableEntryForm
  extends BeanEditor<RecipeTable>
{
  private JComboBox cbRecipe = new JComboBox();
  private FixedLengthTextField tfMenuItemName = new FixedLengthTextField();
  private MenuItem selectedMenuItem;
  private RecipeItemTable table;
  private DoubleTextField tfTotalCost = new DoubleTextField(8);
  private DoubleTextField tfPortion = new DoubleTextField(8);
  private JLabel lblPortionUnit = new JLabel();
  
  public RecipeTableEntryForm(RecipeTable recepie) {
    initComponents();
    List<Recepie> recipeList = new ArrayList();
    recipeList.add(null);
    recipeList.addAll(RecepieDAO.getInstance().findAll());
    cbRecipe.setModel(new ComboBoxModel(recipeList));
    setBean(recepie);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
    
    JLabel lblRecipeTableName = new JLabel("Menu Item");
    tfMenuItemName.setLength(128);
    tfMenuItemName.setEditable(false);
    
    JPanel topPanel = new JPanel(new MigLayout("fillx,wrap 3", "[][grow][sg,fill]", ""));
    topPanel.add(lblRecipeTableName, "");
    topPanel.add(tfMenuItemName, "growx");
    
    JButton btnAddMenuItem = new JButton("Select Menu Item");
    btnAddMenuItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MenuItemSelectionDialog dialog = new MenuItemSelectionDialog(new ArrayList());
        dialog.setSelectionMode(0);
        dialog.setSize(PosUIManager.getSize(600, 515));
        dialog.open();
        if (dialog.isCanceled()) {
          return;
        }
        selectedMenuItem = dialog.getSelectedRowData();
        tfMenuItemName.setText(selectedMenuItem.getName());
      }
    });
    topPanel.add(btnAddMenuItem, "");
    
    topPanel.add(new JLabel("Recipe"), "");
    topPanel.add(cbRecipe, "growx");
    cbRecipe.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        Recepie recipe = (Recepie)cbRecipe.getSelectedItem();
        if (recipe == null) {
          table.setItems(null);
          return;
        }
        table.setItems(recipe.getRecepieItems());
        RecipeTableEntryForm.this.updateTotalCost();
      }
      
    });
    JButton btnAddRecipe = new JButton("Create New");
    btnAddRecipe.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try
        {
          RecipeEntryForm editor = new RecipeEntryForm(new Recepie());
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.openWithScale(700, 650);
          
          if (dialog.isCanceled()) {
            return;
          }
          Recepie recipe = (Recepie)editor.getBean();
          ComboBoxModel model = (ComboBoxModel)cbRecipe.getModel();
          model.addElement(recipe);
          cbRecipe.setSelectedItem(recipe);
          RecipeTableEntryForm.this.updateTotalCost();
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
      }
    });
    topPanel.add(btnAddRecipe, "");
    topPanel.add(new JLabel("Portion"), "");
    topPanel.add(tfPortion, "grow,split 2");
    topPanel.add(lblPortionUnit);
    
    add(topPanel, "North");
    
    table = new RecipeItemTable(new ArrayList());
    table.setEnabled(false);
    
    JLabel lblRecipeItems = new JLabel("Recipe Items");
    Color fg = new Color(49, 106, 196);
    lblRecipeItems.setForeground(fg);
    JLabel lblSubRecipe = new JLabel("Sub Receipe");
    lblSubRecipe.setForeground(fg);
    
    TitledSeparator sep1 = new TitledSeparator(lblRecipeItems, 0);
    
    JPanel tablePanel = new JPanel(new MigLayout("fill,wrap 1"));
    tablePanel.add(sep1, "span,grow");
    tablePanel.add(new JScrollPane(table), "span,grow");
    tablePanel.add(new JLabel("Total Cost:"), "right,split 2");
    tablePanel.add(tfTotalCost);
    tfTotalCost.setEditable(false);
    tfTotalCost.setHorizontalAlignment(4);
    
    add(tablePanel);
  }
  
  public boolean save()
  {
    try
    {
      if (!updateModel()) {
        return false;
      }
      RecipeTable bean = (RecipeTable)getBean();
      RecipeTableDAO.getInstance().saveOrUpdate(bean);
    } catch (Exception e) {
      POSMessageDialog.showError(e.getMessage());
    }
    return true;
  }
  
  protected void updateView()
  {
    RecipeTable bean = (RecipeTable)getBean();
    selectedMenuItem = bean.getMenuItem();
    if (selectedMenuItem != null) {
      tfMenuItemName.setText(selectedMenuItem.getName());
    }
    tfPortion.setText(String.valueOf(bean.getPortion().doubleValue() == 0.0D ? 1.0D : bean.getPortion().doubleValue()));
    updateTotalCost();
  }
  
  protected boolean updateModel() throws IllegalModelStateException
  {
    RecipeTable bean = (RecipeTable)getBean();
    String name = tfMenuItemName.getText();
    if (StringUtils.isEmpty(name)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please enter recepie name");
      return false;
    }
    bean.setMenuItem(selectedMenuItem);
    double portion = tfPortion.getDoubleOrZero();
    bean.setPortion(Double.valueOf(portion <= 0.0D ? 1.0D : portion));
    return true;
  }
  
  public String getDisplayText()
  {
    return ((RecipeTable)getBean()).getId() == null ? "Create New Recipe Table" : "Edit Recipe Table";
  }
  
  private void updateTotalCost() {
    double totalRecipeItemCost = 0.0D;
    Collection<? extends RecepieItem> rows = table.getRows();
    if (rows != null) {
      for (RecepieItem recepieItem : rows) {
        MenuItemDAO.getInstance().initialize(recepieItem.getInventoryItem());
        recepieItem.calculatePercentage();
        totalRecipeItemCost += recepieItem.getCost();
      }
    }
    tfTotalCost.setText(NumberUtil.formatNumber(Double.valueOf(totalRecipeItemCost)));
  }
}
