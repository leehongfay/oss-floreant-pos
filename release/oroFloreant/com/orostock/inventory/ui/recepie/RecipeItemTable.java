package com.orostock.inventory.ui.recepie;

import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.IUnit;
import com.floreantpos.model.InventoryStockUnit;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.RecepieItem;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.util.NumberUtil;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.AbstractCellEditor;
import javax.swing.ComboBoxEditor;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.text.JTextComponent;

public class RecipeItemTable extends JTable
{
  private RecepieTableModel model;
  private JComboBox cbUnits;
  private ChangeListener listener;
  private int currentRowIndex;
  private int currentColumnIndex;
  
  public RecipeItemTable(List<RecepieItem> recipeItems)
  {
    putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    
    model = new RecepieTableModel();
    model.setRows(recipeItems);
    setModel(model);
    
    getTableHeader().setPreferredSize(new Dimension(0, 25));
    setDefaultRenderer(Object.class, new CustomCellRenderer());
    setRowHeight(PosUIManager.getSize(25));
    
    DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer()
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        setHorizontalAlignment(4);
        return c;
      }
    };
    getColumnModel().getColumn(3).setCellRenderer(cellRenderer);
    getColumnModel().getColumn(1).setCellRenderer(cellRenderer);
    
    cbUnits = new JComboBox();
    cbUnits.setModel(new ComboBoxModel(new ArrayList()));
    cbUnits.setSelectedItem("");
    JTextComponent editor = (JTextComponent)cbUnits.getEditor().getEditorComponent();
    
    editor.addKeyListener(new KeyListener()
    {
      public void keyTyped(KeyEvent e) {}
      


      public void keyPressed(KeyEvent e)
      {
        if (e.getKeyChar() == '\n') {
          JComponent ja = (JComponent)e.getSource();
          JComboBox jcbloc = (JComboBox)ja.getParent();
          JTable jtb = (JTable)jcbloc.getParent();
          jtb.changeSelection(0, 1, false, false);
        }
      }
      




      public void keyReleased(KeyEvent e) {}
    });
    editor.addFocusListener(new FocusListener()
    {
      public void focusGained(FocusEvent e)
      {
        JComponent ja = (JComponent)e.getSource();
        JComponent jcbloc = (JComponent)ja.getParent();
        JComboBox jcb = (JComboBox)jcbloc;
        jcb.setPopupVisible(true);
        JTextComponent editor = (JTextComponent)jcb.getEditor().getEditorComponent();
        editor.setSelectionStart(0);
        editor.setSelectionEnd(editor.getText().length());
      }
      



      public void focusLost(FocusEvent e) {}
    });
    TableColumn colCombo = getColumnModel().getColumn(2);
    cbUnits.setEditable(true);
    ComboBoxEditor cbe = new ComboBoxEditor(cbUnits);
    colCombo.setCellEditor(cbe);
    
    TableColumn colQuantity = getColumnModel().getColumn(1);
    TextCellEditor quantityEditor = new TextCellEditor(new JTextField());
    colQuantity.setCellEditor(quantityEditor);
    
    resizeTableColumns();
  }
  
  private void resizeTableColumns() {
    setAutoResizeMode(4);
    setColumnWidth(1, PosUIManager.getSize(90));
    setColumnWidth(2, PosUIManager.getSize(90));
    setColumnWidth(3, PosUIManager.getSize(90));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  public boolean isCellEditable(int row, int column)
  {
    if ((column == 1) || (column == 2))
      return true;
    return super.isCellEditable(row, column);
  }
  
  public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend)
  {
    super.changeSelection(rowIndex, columnIndex, toggle, extend);
    if (columnIndex > -1)
    {
      if (editCellAt(rowIndex, columnIndex)) {
        Component editor = getEditorComponent();
        editor.requestFocusInWindow();
        if ((editor instanceof JTextField))
        {
          JTextField jf = (JTextField)editor;
          jf.select(0, jf.toString().length());
        }
        if ((editor instanceof JComboBox)) {
          JComboBox jcb = (JComboBox)editor;
          String selectedUnit = (String)jcb.getSelectedItem();
          RecepieItem item = (RecepieItem)model.getRows().get(rowIndex);
          List<String> units = new ArrayList();
          List<IUnit> stockUnits = item.getInventoryItem().getUnits();
          for (IUnit inventoryStockUnit : stockUnits) {
            units.add(inventoryStockUnit.getUniqueCode());
          }
          currentRowIndex = rowIndex;
          currentColumnIndex = columnIndex;
          if (stockUnits != null) {
            ComboBoxModel comboBoxModel = (ComboBoxModel)jcb.getModel();
            comboBoxModel.removeAllElements();
            comboBoxModel.setDataList(units);
            jcb.setSelectedItem(selectedUnit);
          }
          jcb.setPopupVisible(true);
          JTextComponent editorCombo = (JTextComponent)jcb.getEditor().getEditorComponent();
          editorCombo.setSelectionStart(0);
          editorCombo.setSelectionEnd(editorCombo.getText().length());
        }
      }
    }
  }
  
  private class RecepieTableModel extends ListTableModel<RecepieItem>
  {
    public RecepieTableModel() {
      super();
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      RecepieItem item = (RecepieItem)rows.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return item.getInventoryItem();
      
      case 1: 
        Double quantity = Double.valueOf(item.getQuantity().doubleValue() == 0.0D ? 1.0D : item.getQuantity().doubleValue());
        return NumberUtil.formatNumber(quantity);
      
      case 2: 
        IUnit unit = item.getUnit();
        if (unit == null)
          return "";
        return unit.getUniqueCode();
      
      case 3: 
        return NumberUtil.formatNumber(Double.valueOf(item.getCost()));
      }
      
      return null;
    }
    
    public void setValueAt(Object aValue, int rowIndex, int columnIndex)
    {
      RecepieItem item = (RecepieItem)rows.get(rowIndex);
      switch (columnIndex) {
      case 1: 
        double quantity = 1.0D;
        try {
          quantity = Double.valueOf(String.valueOf(aValue)).doubleValue();
        }
        catch (Exception localException) {}
        if (item.getQuantity().doubleValue() == quantity) {
          fireTableRowsUpdated(rowIndex, rowIndex);
          return;
        }
        item.setQuantity(Double.valueOf(quantity));
        item.calculatePercentage();
        fireTableRowsUpdated(rowIndex, rowIndex);
        if (listener != null)
          listener.stateChanged(null);
        break;
      case 2: 
        String unit = (String)aValue;
        if (unit.equals(item.getUnitCode())) {
          fireTableRowsUpdated(rowIndex, rowIndex);
          return;
        }
        item.setUnitCode(unit);
        item.calculatePercentage();
        fireTableRowsUpdated(rowIndex, rowIndex);
        if (listener != null) {
          listener.stateChanged(null);
        }
        break;
      default: 
        super.setValueAt(aValue, rowIndex, columnIndex);
      }
    }
  }
  
  public void setItems(List<RecepieItem> recepieItems) {
    if (recepieItems != null) {
      for (RecepieItem recepieItem : recepieItems) {
        MenuItemDAO.getInstance().initialize(recepieItem.getInventoryItem());
        recepieItem.calculatePercentage();
      }
    }
    model.setRows(recepieItems);
  }
  
  public class ComboBoxEditor extends AbstractCellEditor implements TableCellEditor {
    JComboBox comboBox;
    
    public ComboBoxEditor(JComboBox jcb) {
      comboBox = jcb;
    }
    
    public Object getCellEditorValue() {
      return comboBox.getSelectedItem();
    }
    
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
      comboBox.setSelectedItem(value);
      return comboBox;
    }
    
    public boolean stopCellEditing() {
      fireEditingStopped();
      return true;
    }
  }
  
  public class TextCellEditor extends AbstractCellEditor implements TableCellEditor
  {
    JTextField jtextfield;
    
    public TextCellEditor(JTextField jtf) {
      jtextfield = jtf;
    }
    
    public Object getCellEditorValue() {
      return jtextfield.getText();
    }
    
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
      if ((!isSelected) || 
      
        (value == null)) {
        value = "";
      }
      value = value.toString();
      if ((value instanceof Integer)) {
        value = value.toString();
      }
      jtextfield.setText((String)value);
      return jtextfield;
    }
    
    public boolean stopCellEditing() {
      fireEditingStopped();
      return true;
    }
  }
  
  public void removeItem(int selectedRow)
  {
    model.deleteItem(selectedRow);
  }
  
  public RecepieItem getRow(int index) {
    return (RecepieItem)model.getRowData(index);
  }
  
  public void addRow(RecepieItem item) {
    model.addItem(item);
  }
  
  public Collection<? extends RecepieItem> getRows() {
    return model.getRows();
  }
  
  public void setParentMenuItem(MenuItem e) {
    List<InventoryStockUnit> stockUnits = e.getStockUnits();
    cbUnits.setModel(new ComboBoxModel(stockUnits));
  }
  
  public void setValueChangeListener(ChangeListener listener) {
    this.listener = listener;
  }
}
