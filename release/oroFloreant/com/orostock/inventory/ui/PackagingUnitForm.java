package com.orostock.inventory.ui;

import com.floreantpos.model.PackagingUnit;
import com.floreantpos.model.dao.PackagingUnitDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import javax.swing.JLabel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;




public class PackagingUnitForm
  extends BeanEditor<PackagingUnit>
{
  private FixedLengthTextField tfName = new FixedLengthTextField(30);
  private FixedLengthTextField tfShortName = new FixedLengthTextField(10);
  private boolean recipeUnit;
  
  public PackagingUnitForm() {
    this(false);
  }
  
  public PackagingUnitForm(boolean recipeUnit) {
    this(new PackagingUnit(), recipeUnit);
  }
  
  public PackagingUnitForm(PackagingUnit pu) {
    this(pu, false);
  }
  
  public PackagingUnitForm(PackagingUnit pu, boolean recipeUnit) {
    this.recipeUnit = recipeUnit;
    createUI();
    setBean(pu);
  }
  
  private void createUI() {
    setLayout(new MigLayout("fillx"));
    
    add(new JLabel("Code"));
    add(tfShortName, "grow, wrap");
    
    add(new JLabel("Name"));
    add(tfName, "grow, wrap");
  }
  
  public boolean save()
  {
    try
    {
      if (!updateModel()) {
        return false;
      }
      PackagingUnitDAO.getInstance().save((PackagingUnit)getBean());
      
      return true;
    }
    catch (IllegalModelStateException e) {
      POSMessageDialog.showError(this, e.getMessage());
    }
    
    return false;
  }
  
  public void createNew()
  {
    setBean(new PackagingUnit());
    clearFields();
  }
  
  public void setFieldsEnable(boolean enable)
  {
    tfName.setEnabled(enable);
    tfShortName.setEnabled(enable);
  }
  
  public void clearFields()
  {
    tfName.setText("");
    tfShortName.setText("");
  }
  
  protected void updateView()
  {
    PackagingUnit packagingUnit = (PackagingUnit)getBean();
    if (packagingUnit == null) {
      return;
    }
    
    tfName.setText(packagingUnit.getName());
    tfShortName.setText(packagingUnit.getCode());
  }
  
  protected boolean updateModel() throws IllegalModelStateException
  {
    PackagingUnit packagingUnit = (PackagingUnit)getBean();
    String name = tfName.getText();
    String shortName = tfShortName.getText();
    
    if (StringUtils.isEmpty(name)) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), "Please enter unit name");
      return false;
    }
    
    if (PackagingUnitDAO.getInstance().nameExists(name)) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), "A packaging unit with that name already exists");
      return false;
    }
    
    packagingUnit.setName(name);
    packagingUnit.setCode(shortName);
    packagingUnit.setRecipeUnit(Boolean.valueOf(recipeUnit));
    
    return true;
  }
  
  public boolean delete()
  {
    try
    {
      PackagingUnit packagingUnit = (PackagingUnit)getBean();
      if (packagingUnit == null) {
        return false;
      }
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), "Are you sure to delete selected item ?", "Confirm");
      if (option != 0) {
        return false;
      }
      clearFields();
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e); }
    return false;
  }
  


  public String getDisplayText()
  {
    if (recipeUnit)
      return "Add/Edit recipe unit";
    return "Add/Edit packaging unit";
  }
}
