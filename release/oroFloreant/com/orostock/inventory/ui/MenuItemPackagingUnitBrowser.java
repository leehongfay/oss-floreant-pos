package com.orostock.inventory.ui;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.model.InventoryStockUnit;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.InventoryUnitGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.dao.InventoryStockUnitDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.IUpdatebleView;
import com.floreantpos.swing.PosTable;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;

public class MenuItemPackagingUnitBrowser
  extends TransparentPanel implements IUpdatebleView<MenuItem>
{
  private JXTable table;
  private BeanTableModel<InventoryStockUnit> tableModel;
  private JXTable tableRecipeUnit;
  private BeanTableModel<InventoryStockUnit> tableModelRecipeUnit;
  private MenuItem menuItem;
  private boolean inited;
  
  public MenuItemPackagingUnitBrowser(MenuItem menuItem)
  {
    this.menuItem = menuItem;
    tableModel = new BeanTableModel(InventoryStockUnit.class);
    tableModel.addColumn("PACK UNIT", "packagingUnit");
    tableModel.addColumn("CONVERSION", "conversionValue");
    tableModel.addColumn("UNIT", "unit");
    
    table = new PosTable(tableModel);
    
    tableModelRecipeUnit = new BeanTableModel(InventoryStockUnit.class);
    tableModelRecipeUnit.addColumn("RECIPE UNIT", "packagingUnit");
    tableModelRecipeUnit.addColumn("CONVERSION", "conversionValue");
    tableModelRecipeUnit.addColumn("UNIT", "unit");
    
    tableRecipeUnit = new PosTable(tableModelRecipeUnit);
    
    tableRecipeUnit.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        if (!e.getValueIsAdjusting())
          return;
        table.clearSelection();
      }
    });
    table.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        if (!e.getValueIsAdjusting())
          return;
        tableRecipeUnit.clearSelection();
      }
    });
    table.getColumnModel().getColumn(1).setCellRenderer(new CustomCellRenderer());
    tableRecipeUnit.getColumnModel().getColumn(1).setCellRenderer(new CustomCellRenderer());
    
    setLayout(new BorderLayout(5, 5));
    setBorder(new EmptyBorder(20, 20, 20, 20));
    JPanel contentPanel = new JPanel(new MigLayout("fill,wrap 1", "", "[50%][][50%][]"));
    contentPanel.add(new JScrollPane(table), "grow,span");
    contentPanel.add(createPackagingUnitButtonPanel());
    contentPanel.add(new JScrollPane(tableRecipeUnit), "grow,span");
    contentPanel.add(createRecipeUnitButtonPanel());
    add(contentPanel);
  }
  
  private TransparentPanel createPackagingUnitButtonPanel() {
    JButton addButton = new JButton("Add");
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          if (menuItem.getUnit() == null) {
            POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please set item unit!");
            return;
          }
          MenuItemPackagingUnitForm editor = new MenuItemPackagingUnitForm(new InventoryStockUnit(), menuItem);
          BeanEditorDialog dialog = new BeanEditorDialog(editor);
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          InventoryStockUnit stockUnit = (InventoryStockUnit)editor.getBean();
          tableModel.addRow(stockUnit);
        }
        catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        editPackagingUnitRow();
      }
      
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          InventoryStockUnit stockUnit = (InventoryStockUnit)tableModel.getRow(index);
          
          if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
            return;
          }
          
          InventoryStockUnitDAO dao = new InventoryStockUnitDAO();
          dao.delete(stockUnit);
          
          tableModel.removeRow(index);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    return panel;
  }
  
  private TransparentPanel createRecipeUnitButtonPanel() {
    JButton addButton = new JButton("Add");
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          if (menuItem.getUnit() == null) {
            POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please set item unit!");
            return;
          }
          MenuItemPackagingUnitForm editor = new MenuItemPackagingUnitForm(new InventoryStockUnit(), menuItem, true);
          BeanEditorDialog dialog = new BeanEditorDialog(editor);
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          InventoryStockUnit stockUnit = (InventoryStockUnit)editor.getBean();
          stockUnit.setRecipeUnit(Boolean.valueOf(true));
          tableModelRecipeUnit.addRow(stockUnit);
        }
        catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        editRecipeUnitRow();
      }
      
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = tableRecipeUnit.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = tableRecipeUnit.convertRowIndexToModel(index);
          InventoryStockUnit stockUnit = (InventoryStockUnit)tableModelRecipeUnit.getRow(index);
          
          if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
            return;
          }
          
          InventoryStockUnitDAO dao = new InventoryStockUnitDAO();
          dao.delete(stockUnit);
          
          tableModelRecipeUnit.removeRow(index);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    return panel;
  }
  
  public void editPackagingUnitRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      InventoryStockUnit stockUnit = (InventoryStockUnit)tableModel.getRow(index);
      
      MenuItemPackagingUnitForm editor = new MenuItemPackagingUnitForm(stockUnit, menuItem);
      BeanEditorDialog dialog = new BeanEditorDialog(editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public void editRecipeUnitRow() {
    try {
      int index = tableRecipeUnit.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = tableRecipeUnit.convertRowIndexToModel(index);
      InventoryStockUnit stockUnit = (InventoryStockUnit)tableModelRecipeUnit.getRow(index);
      
      MenuItemPackagingUnitForm editor = new MenuItemPackagingUnitForm(stockUnit, menuItem, true);
      BeanEditorDialog dialog = new BeanEditorDialog(editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      tableRecipeUnit.repaint();
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public void refreshTable() {
    if (menuItem == null) {
      return;
    }
    List<InventoryStockUnit> packagingUnits = new ArrayList();
    List<InventoryStockUnit> recipeUnits = new ArrayList();
    List<InventoryStockUnit> units = menuItem.getStockUnits();
    if (units != null) {
      for (InventoryStockUnit inventoryStockUnit : units) {
        if (inventoryStockUnit.isRecipeUnit().booleanValue()) {
          recipeUnits.add(inventoryStockUnit);
        } else
          packagingUnits.add(inventoryStockUnit);
      }
      tableModel.addRows(packagingUnits);
      tableModelRecipeUnit.addRows(recipeUnits);
    }
  }
  
  public void initView(MenuItem e)
  {
    if (inited) {
      return;
    }
    menuItem = e;
    refreshTable();
    inited = true;
  }
  
  public boolean updateModel(MenuItem e)
  {
    List<InventoryStockUnit> rows = new ArrayList();
    List<InventoryStockUnit> rowsPackagingUnits = tableModel.getRows();
    List<InventoryStockUnit> rowsRecipeUnits = tableModelRecipeUnit.getRows();
    if (rowsPackagingUnits != null) {
      rows.addAll(rowsPackagingUnits);
    }
    if (rowsRecipeUnits != null)
      rows.addAll(rowsRecipeUnits);
    List<InventoryUnit> groupUnits;
    if (e.getUnit() != null) {
      groupUnits = e.getUnit().getUnitGroup().getUnits();
      for (InventoryStockUnit stockUnit : rows) {
        if (!groupUnits.contains(stockUnit.getUnit())) {
          stockUnit.setUnit(e.getUnit());
          stockUnit.calculateBaseUnitValue();
        }
        stockUnit.setMenuItem(e);
      }
    }
    e.setStockUnits(rows);
    return true;
  }
}
