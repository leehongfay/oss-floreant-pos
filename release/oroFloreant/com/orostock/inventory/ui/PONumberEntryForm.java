package com.orostock.inventory.ui;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.model.MenuItem;
import com.floreantpos.report.PurchaseReportView;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.util.POSUtil;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import net.miginfocom.swing.MigLayout;

public class PONumberEntryForm
  extends OkCancelOptionDialog
{
  private POSTextField tfName;
  List<MenuItem> inventoryList = new ArrayList();
  
  public PONumberEntryForm(List<MenuItem> inventoryList) {
    this.inventoryList = inventoryList;
    createGUI();
  }
  
  private void createGUI() {
    setTitle("P.O.#");
    setTitle("Set Purchase Order Number:");
    JPanel panel = getContentPanel();
    panel.setLayout(new MigLayout("", "[][grow]", "[][]"));
    
    JLabel lblName = new JLabel("P.O. Number: # ");
    panel.add(lblName, "cell 0 0,alignx trailing");
    
    tfName = new POSTextField();
    panel.add(tfName, "cell 1 0,growx");
  }
  

  public void doOk()
  {
    BackOfficeWindow window = POSUtil.getBackOfficeWindow();
    JTabbedPane tabbedPane = window.getTabbedPane();
    
    PurchaseReportView reportView = null;
    int index = tabbedPane.indexOfTab("Purchase Report");
    if (index == -1) {
      reportView = new PurchaseReportView(inventoryList);
      tabbedPane.addTab("Purchase Report", reportView);
    }
    else {
      reportView = (PurchaseReportView)tabbedPane.getComponentAt(index);
    }
    tabbedPane.setSelectedComponent(reportView);
    dispose();
  }
}
