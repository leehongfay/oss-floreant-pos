package com.orostock.inventory.ui;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.explorer.ExplorerButtonPanel;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.InventoryUnitGroup;
import com.floreantpos.model.dao.InventoryUnitDAO;
import com.floreantpos.model.dao.InventoryUnitGroupDAO;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.tree.TreePath;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;
import org.jdesktop.swingx.treetable.TreeTableNode;


public class InventoryUnitGroupBrowserTree
  extends TransparentPanel
  implements ActionListener, ListSelectionListener
{
  private static final long serialVersionUID = 1L;
  private JXTreeTable treeTable;
  private InvUnitGroupTreeTableModel noRootTreeTableModel;
  private List<InventoryUnitGroup> rootUnitGroupList;
  
  public InventoryUnitGroupBrowserTree()
  {
    setLayout(new BorderLayout(5, 5));
    treeTable = new JXTreeTable();
    treeTable.setRowHeight(PosUIManager.getSize(30));
    treeTable.setRootVisible(false);
    
    treeTable.setDefaultRenderer(Object.class, new PosTableRenderer());
    loadData();
    treeTable.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me)
      {
        int col = treeTable.columnAtPoint(me.getPoint());
        
        if ((me.getClickCount() == 2) && (col == 0))
        {
          treeTable.expandPath(treeTable.getPathForRow(treeTable.getSelectedRow()));
        }
        else if ((me.getClickCount() == 2) && (col != 0)) {
          InventoryUnitGroupBrowserTree.this.editSelectedRow();
        }
        
      }
    });
    add(new JScrollPane(treeTable), "Center");
    treeTable.getColumnModel().getColumn(3).setCellRenderer(new DefaultTableCellRenderer()
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component tableCellRendererComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        setHorizontalAlignment(4);
        return tableCellRendererComponent;
      }
      
    });
    createButtonPanel();
    
    JPanel topPanel = new JPanel(new MigLayout());
    JButton btnRefresh = new JButton("Refresh");
    btnRefresh.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        InventoryUnitGroupBrowserTree.this.loadData();
      }
    });
    topPanel.add(btnRefresh);
    add(topPanel, "North");
  }
  
  private void loadData() {
    rootUnitGroupList = InventoryUnitGroupDAO.getInstance().findAll();
    createTree();
    treeTable.expandAll();
  }
  
  private void createTree() {
    if (rootUnitGroupList != null) {
      InventoryUnitGroup demo = new InventoryUnitGroup();
      demo.setId("0");
      demo.setName("Root");
      DefaultMutableTreeTableNode rootNode = new DefaultMutableTreeTableNode(demo);
      rootNode.setUserObject(demo);
      
      for (InventoryUnitGroup inventoryUnitGroup : rootUnitGroupList) {
        DefaultMutableTreeTableNode node = new DefaultMutableTreeTableNode(inventoryUnitGroup);
        rootNode.add(node);
        buildLocationTree(node);
      }
      
      noRootTreeTableModel = new InvUnitGroupTreeTableModel(rootNode);
      treeTable.setTreeTableModel(noRootTreeTableModel);
    }
  }
  


  private void buildLocationTree(DefaultMutableTreeTableNode defaultMutableTreeTableNode)
  {
    InventoryUnitGroup unitGroup = (InventoryUnitGroup)defaultMutableTreeTableNode.getUserObject();
    if (unitGroup == null) {
      return;
    }
    
    defaultMutableTreeTableNode.setAllowsChildren(true);
    List<InventoryUnit> unitList = unitGroup.getUnits();
    for (InventoryUnit inventoryUnit : unitList) {
      DefaultMutableTreeTableNode node = new DefaultMutableTreeTableNode(inventoryUnit);
      defaultMutableTreeTableNode.add(node);
    }
  }
  
  class InvUnitGroupTreeTableModel extends DefaultTreeTableModel
  {
    private final String[] COLUMN_NAMES = { "Group", "Unit Code", "Unit Name", "Conversion Rate", "Base Unit" };
    
    public InvUnitGroupTreeTableModel(DefaultMutableTreeTableNode rootLocation) {
      super();
    }
    
    public void setRoot(TreeTableNode root)
    {
      super.setRoot(root);
    }
    
    public int getColumnCount()
    {
      return COLUMN_NAMES.length;
    }
    
    public String getColumnName(int column)
    {
      return COLUMN_NAMES[column];
    }
    
    public boolean isCellEditable(Object node, int column)
    {
      return false;
    }
    
    public Object getValueAt(Object node, int column)
    {
      if ((node instanceof DefaultMutableTreeTableNode)) {
        if ((((DefaultMutableTreeTableNode)node).getUserObject() instanceof InventoryUnitGroup)) {
          InventoryUnitGroup inventoryLocation = (InventoryUnitGroup)((DefaultMutableTreeTableNode)node).getUserObject();
          if (inventoryLocation == null) {
            return "";
          }
          switch (column) {
          case 0: 
            return inventoryLocation.getName();
          


          }
          
        }
        else if ((((DefaultMutableTreeTableNode)node).getUserObject() instanceof InventoryUnit)) {
          InventoryUnit inventoryUnit = (InventoryUnit)((DefaultMutableTreeTableNode)node).getUserObject();
          if (inventoryUnit == null) {
            return "";
          }
          switch (column)
          {

          case 1: 
            return inventoryUnit.getCode();
          case 2: 
            return inventoryUnit.getName();
          case 3: 
            return inventoryUnit.getConversionRate();
          case 4: 
            return inventoryUnit.isBaseUnit();
          }
          
        }
      }
      return null;
    }
  }
  
  private void createButtonPanel() {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    
    JButton btn_newGroup = new JButton("New Group");
    JButton btn_newUnit = new JButton("New Unit");
    JButton editButton = explorerButton.getEditButton();
    JButton deleteButton = explorerButton.getDeleteButton();
    
    btn_newGroup.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          String groupName = JOptionPane.showInputDialog(POSUtil.getFocusedWindow(), "Enter group name");
          if (groupName == null) {
            return;
          }
          if (groupName.equals("")) {
            BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Group name cannot be empty.");
            return;
          }
          
          if (groupName.length() > 30) {
            BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Group name too long.");
            return;
          }
          
          InventoryUnitGroup inventoryUnitGroup = new InventoryUnitGroup();
          inventoryUnitGroup.setName(groupName);
          
          InventoryUnitGroupDAO inventoryUnitGroupDAO = new InventoryUnitGroupDAO();
          inventoryUnitGroupDAO.saveOrUpdate(inventoryUnitGroup);
          







          if (inventoryUnitGroup != null) {
            MutableTreeTableNode root = (MutableTreeTableNode)noRootTreeTableModel.getRoot();
            noRootTreeTableModel.insertNodeInto(new DefaultMutableTreeTableNode(inventoryUnitGroup), root, root.getChildCount());
          }
          
          treeTable.expandAll();
        } catch (Exception e2) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, e2);
        }
        
      }
    });
    btn_newUnit.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        InventoryUnitFormTree inventoryUnitFormTree = new InventoryUnitFormTree(new InventoryUnit());
        BeanEditorDialog dialog = new BeanEditorDialog(inventoryUnitFormTree);
        dialog.setPreferredSize(PosUIManager.getSize(500, 600));
        dialog.open();
        if (dialog.isCanceled())
          return;
        InventoryUnit unit = (InventoryUnit)inventoryUnitFormTree.getBean();
        InventoryUnitGroup parentOfNewLoc = unit.getUnitGroup();
        
        if (unit != null) {
          MutableTreeTableNode root = (MutableTreeTableNode)noRootTreeTableModel.getRoot();
          if (parentOfNewLoc != null) {
            MutableTreeTableNode parentNode = findTreeNodeForLocation(root, parentOfNewLoc.getId());
            if (parentNode != null) {
              noRootTreeTableModel.insertNodeInto(new DefaultMutableTreeTableNode(unit), parentNode, parentNode
                .getChildCount());
            }
          }
          else {
            MutableTreeTableNode parentNode = findTreeNodeForLocation(root, "0");
            if (parentNode != null) {
              noRootTreeTableModel.insertNodeInto(new DefaultMutableTreeTableNode(unit), parentNode, parentNode
                .getChildCount());
            }
          }
        }
        
        treeTable.expandAll();
      }
      

    });
    editButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        InventoryUnitGroupBrowserTree.this.editSelectedRow();
      }
      

    });
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        deleteInventoryLocation();
      }
      
      private void deleteInventoryLocation() {
        try {
          int index = treeTable.getSelectedRow();
          if (index < 0) {
            return;
          }
          TreePath treePath = treeTable.getPathForRow(index);
          DefaultMutableTreeTableNode lastPathComponent = (DefaultMutableTreeTableNode)treePath.getLastPathComponent();
          















          if ((lastPathComponent.getUserObject() instanceof InventoryUnitGroup))
          {
            InventoryUnitGroup unitGroup = (InventoryUnitGroup)lastPathComponent.getUserObject();
            
            if (POSMessageDialog.showYesNoQuestionDialog(getRootPane(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
              return;
            }
            InventoryUnitGroupDAO inventoryUnitGroupDAO = new InventoryUnitGroupDAO();
            inventoryUnitGroupDAO.delete(unitGroup);
            
            MutableTreeTableNode tableNode = findTreeNodeForLocation((MutableTreeTableNode)noRootTreeTableModel.getRoot(), unitGroup.getId());
            if (tableNode.getParent() != null) {
              noRootTreeTableModel.removeNodeFromParent(tableNode);
            }
          }
          else if ((lastPathComponent.getUserObject() instanceof InventoryUnit))
          {
            InventoryUnit unit = (InventoryUnit)lastPathComponent.getUserObject();
            if (POSMessageDialog.showYesNoQuestionDialog(getRootPane(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
              return;
            }
            InventoryUnitDAO inventoryUnitDAO = new InventoryUnitDAO();
            inventoryUnitDAO.delete(unit);
            MutableTreeTableNode tableNode = findTreeNodeForLocation((MutableTreeTableNode)noRootTreeTableModel.getRoot(), unit.getId());
            if (tableNode.getParent() != null) {
              noRootTreeTableModel.removeNodeFromParent(tableNode);
            }
          }
          treeTable.repaint();
          treeTable.revalidate();
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    TransparentPanel panel = new TransparentPanel();
    
    panel.add(btn_newGroup);
    panel.add(btn_newUnit);
    panel.add(editButton);
    panel.add(deleteButton);
    add(panel, "South");
  }
  










  public void actionPerformed(ActionEvent e) {}
  










  public void valueChanged(ListSelectionEvent e) {}
  









  private void editSelectedRow()
  {
    try
    {
      int index = treeTable.getSelectedRow();
      if (index < 0)
        return;
      TreePath treePath = treeTable.getPathForRow(index);
      
      DefaultMutableTreeTableNode lastPathComponent = (DefaultMutableTreeTableNode)treePath.getLastPathComponent();
      
      if ((lastPathComponent.getUserObject() instanceof InventoryUnitGroup))
      {
        InventoryUnitGroup unitGroup = (InventoryUnitGroup)lastPathComponent.getUserObject();
        
        String groupName = JOptionPane.showInputDialog(POSUtil.getFocusedWindow(), "Group name", unitGroup.getName());
        if (groupName == null) {
          BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Group name cannot be empty.");
          return;
        }
        if (groupName.length() > 30) {
          BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Group name too long.");
          return;
        }
        
        unitGroup.setName(groupName);
        InventoryUnitGroupDAO inventoryUnitGroupDAO = new InventoryUnitGroupDAO();
        inventoryUnitGroupDAO.saveOrUpdate(unitGroup);
      }
      else if ((lastPathComponent.getUserObject() instanceof InventoryUnit))
      {
        InventoryUnit unit = (InventoryUnit)lastPathComponent.getUserObject();
        
        InventoryUnitFormTree inventoryUnitFormTree = new InventoryUnitFormTree(unit);
        BeanEditorDialog dialog = new BeanEditorDialog(inventoryUnitFormTree);
        dialog.setPreferredSize(PosUIManager.getSize(500, 600));
        dialog.open();
        if (dialog.isCanceled()) {
          return;
        }
      }
      loadData();













    }
    catch (Throwable x)
    {












      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  

  public MutableTreeTableNode findTreeNodeForLocation(MutableTreeTableNode locationNode, String locationId)
  {
    if ((((DefaultMutableTreeTableNode)locationNode).getUserObject() instanceof InventoryUnitGroup)) {
      InventoryUnitGroup groupLocation = (InventoryUnitGroup)locationNode.getUserObject();
      


      if (locationId.equals(groupLocation.getId())) {
        return locationNode;
      }
      
      Enumeration<? extends TreeTableNode> children = locationNode.children();
      while (children.hasMoreElements()) {
        MutableTreeTableNode treeTableNode = (MutableTreeTableNode)children.nextElement();
        MutableTreeTableNode findLocById = findTreeNodeForLocation(treeTableNode, locationId);
        if (findLocById != null) {
          return findLocById;
        }
      }
    }
    else if ((((DefaultMutableTreeTableNode)locationNode).getUserObject() instanceof InventoryUnit)) {
      InventoryUnit unitLocation = (InventoryUnit)locationNode.getUserObject();
      


      if (locationId.equals(unitLocation.getId())) {
        return locationNode;
      }
      
      Enumeration<? extends TreeTableNode> children = locationNode.children();
      while (children.hasMoreElements()) {
        MutableTreeTableNode treeTableNode = (MutableTreeTableNode)children.nextElement();
        MutableTreeTableNode findLocById = findTreeNodeForLocation(treeTableNode, locationId);
        if (findLocById != null) {
          return findLocById;
        }
      }
    }
    
    return null;
  }
}
