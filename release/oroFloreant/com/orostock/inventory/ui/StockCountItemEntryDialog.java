package com.orostock.inventory.ui;

import com.floreantpos.config.AppProperties;
import com.floreantpos.model.IUnit;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.InventoryStock;
import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.MenuGroup;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.StockCountItem;
import com.floreantpos.model.dao.InventoryLocationDAO;
import com.floreantpos.model.dao.InventoryStockDAO;
import com.floreantpos.model.dao.InventoryVendorDAO;
import com.floreantpos.model.dao.MenuGroupDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;




public class StockCountItemEntryDialog
  extends OkCancelOptionDialog
{
  private static final long serialVersionUID = 1L;
  private Component lblQuantity;
  private StockCountItem orderItem;
  private JLabel lblStockCountItemType;
  private JLabel lblUnitPrice;
  private DoubleTextField tfUnitOnHand;
  private DoubleTextField tfActualUnit;
  private JButton btnSearchItem;
  private POSTextField tfSearchString;
  private JComboBox<IUnit> cbUnits;
  private JLabel lblOrderId;
  private JComboBox cbItems;
  private JComboBox<InventoryLocation> cbLocations;
  private BeanTableModel<StockCountItem> tableModel;
  private boolean isEditMode = false;
  private JComboBox cbGroups;
  private JComboBox cbVendors;
  
  public StockCountItemEntryDialog(BeanTableModel<StockCountItem> tableModel) {
    this.tableModel = tableModel;
    setCaption("Add items");
    initComponents();
    initData();
    setOkButtonText("ADD");
    setCancelButtonText("CLOSE");
  }
  
  public StockCountItemEntryDialog(StockCountItem orderItem) {
    this.orderItem = orderItem;
    isEditMode = true;
    setCaption("Edit item");
    initComponents();
    initData();
    updateView();
  }
  
  private void initData() {
    cbItems.setModel(new com.floreantpos.swing.ComboBoxModel());
    cbItems.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        MenuItem menuItem = (MenuItem)cbItems.getSelectedItem();
        com.floreantpos.swing.ComboBoxModel aModel = (com.floreantpos.swing.ComboBoxModel)cbUnits.getModel();
        if (menuItem != null) {
          MenuItemDAO.getInstance().initialize(menuItem);
          aModel.setDataList(menuItem.getUnits());
          if (aModel.getSize() > 0) {
            cbUnits.setSelectedIndex(0);
          }
          StockCountItemEntryDialog.this.loadStockQuantity();
        }
        
      }
    });
    com.floreantpos.swing.ComboBoxModel aModel = new com.floreantpos.swing.ComboBoxModel();
    cbUnits.setModel(aModel);
    cbUnits.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        StockCountItemEntryDialog.this.loadStockQuantity();
      }
      
    });
    com.floreantpos.swing.ComboBoxModel<InventoryLocation> locationModel = new com.floreantpos.swing.ComboBoxModel();
    locationModel.addElement(null);
    List<InventoryLocation> locations = InventoryLocationDAO.getInstance().findAll();
    for (InventoryLocation inventoryLocation : locations) {
      locationModel.addElement(inventoryLocation);
    }
    
    cbLocations.setModel(locationModel);
    cbLocations.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        StockCountItemEntryDialog.this.loadStockQuantity();
      }
      
    });
    Object groupsModel = new com.floreantpos.swing.ComboBoxModel();
    ((com.floreantpos.swing.ComboBoxModel)groupsModel).addElement(null);
    List<MenuGroup> menuGroups = MenuGroupDAO.getInstance().findGroupsWithInventoryItems();
    for (MenuGroup menuGroup : menuGroups) {
      ((com.floreantpos.swing.ComboBoxModel)groupsModel).addElement(menuGroup);
    }
    
    cbGroups.setModel((javax.swing.ComboBoxModel)groupsModel);
    cbGroups.addItemListener(new ItemListener()
    {

      public void itemStateChanged(ItemEvent e) {}

    });
    Object vendorsModel = new com.floreantpos.swing.ComboBoxModel();
    ((com.floreantpos.swing.ComboBoxModel)vendorsModel).addElement(null);
    List<InventoryVendor> vendors = InventoryVendorDAO.getInstance().findAll();
    for (InventoryVendor vendor : vendors) {
      ((com.floreantpos.swing.ComboBoxModel)vendorsModel).addElement(vendor);
    }
    
    cbVendors.setModel((javax.swing.ComboBoxModel)vendorsModel);
    cbVendors.addItemListener(new ItemListener()
    {

      public void itemStateChanged(ItemEvent e) {}


    });
    loadUnits();
  }
  
  private void loadUnits() {
    com.floreantpos.swing.ComboBoxModel model = (com.floreantpos.swing.ComboBoxModel)cbUnits.getModel();
    model.removeAllElements();
    MenuItem menuItem = (MenuItem)cbItems.getSelectedItem();
    if (menuItem == null)
      return;
    model.setDataList(menuItem.getUnits());
  }
  
  private void initComponents() {
    setTitle(AppProperties.getAppName());
    JPanel itemInfoPanel = new JPanel(new MigLayout("hidemode 3", "[][grow]", ""));
    
    JLabel lblGroup = new JLabel("Menu group:");
    cbGroups = new JComboBox();
    
    if (!isEditMode) {
      itemInfoPanel.add(lblGroup, "alignx trailing");
      itemInfoPanel.add(cbGroups, "growx, wrap");
    }
    JLabel lblVendor = new JLabel("Vendor:");
    cbVendors = new JComboBox();
    if (!isEditMode) {
      itemInfoPanel.add(lblVendor, "alignx trailing");
      itemInfoPanel.add(cbVendors, "growx, wrap");
    }
    JLabel lblLocation = new JLabel("Location:");
    cbLocations = new JComboBox();
    if (isEditMode) {
      cbLocations.setEnabled(false);
    }
    itemInfoPanel.add(lblLocation, "alignx trailing");
    itemInfoPanel.add(cbLocations, "growx, wrap");
    
    lblOrderId = new JLabel("Barcode/SKU/Name:");
    if (!isEditMode) {
      itemInfoPanel.add(lblOrderId, "alignx trailing");
    }
    tfSearchString = new POSTextField();
    if (!isEditMode) {
      itemInfoPanel.add(tfSearchString, "growx, split 2,");
    }
    tfSearchString.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        StockCountItemEntryDialog.this.doSearchItem();
      }
    });
    btnSearchItem = new JButton("..Search..");
    if (!isEditMode) {
      itemInfoPanel.add(btnSearchItem, " wrap");
    }
    
    btnSearchItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        StockCountItemEntryDialog.this.doSearchItem();
      }
      
    });
    JLabel lblItem = new JLabel("Item:");
    itemInfoPanel.add(lblItem, "alignx trailing");
    
    cbItems = new JComboBox();
    cbItems.setMinimumSize(PosUIManager.getSize(90, 0));
    itemInfoPanel.add(cbItems, "grow, wrap");
    if (isEditMode) {
      cbItems.setEnabled(false);
    }
    
    lblStockCountItemType = new JLabel("Reason Type:");
    itemInfoPanel.add(lblStockCountItemType, " alignx trailing");
    
    lblStockCountItemType.setVisible(false);
    
    JLabel lblUnit = new JLabel("Unit:");
    itemInfoPanel.add(lblUnit, "alignx trailing");
    
    cbUnits = new JComboBox();
    cbUnits.setPreferredSize(PosUIManager.getSize(120, 0));
    itemInfoPanel.add(cbUnits, "grow, wrap");
    
    lblUnitPrice = new JLabel("On Hand:");
    itemInfoPanel.add(lblUnitPrice, "alignx trailing");
    
    tfUnitOnHand = new DoubleTextField();
    tfUnitOnHand.setEditable(false);
    itemInfoPanel.add(tfUnitOnHand, "growx, wrap");
    
    lblQuantity = new JLabel("Counted:");
    itemInfoPanel.add(lblQuantity, "alignx trailing");
    
    tfActualUnit = new DoubleTextField();
    itemInfoPanel.add(tfActualUnit, "growx");
    getContentPanel().add(itemInfoPanel);
  }
  
  private void doSearchItem() {
    String searchText = tfSearchString.getText();
    MenuGroup menuGroup = (MenuGroup)cbGroups.getSelectedItem();
    InventoryVendor vendor = (InventoryVendor)cbVendors.getSelectedItem();
    InventoryLocation location = (InventoryLocation)cbLocations.getSelectedItem();
    
    List<MenuItem> menuItems = MenuItemDAO.getInstance().findMenuItemsForStockCount(searchText, menuGroup, vendor, location);
    
    com.floreantpos.swing.ComboBoxModel model = (com.floreantpos.swing.ComboBoxModel)cbItems.getModel();
    model.removeAllElements();
    model.setDataList(menuItems);
    if (model.getSize() > 0) {
      cbItems.setSelectedIndex(0);
    }
    loadUnits();
    com.floreantpos.swing.ComboBoxModel unitModel = (com.floreantpos.swing.ComboBoxModel)cbUnits.getModel();
    if (unitModel.getSize() > 0) {
      cbUnits.setSelectedIndex(0);
    }
    loadStockQuantity();
  }
  
  public void updateView() {
    if (orderItem == null) {
      return;
    }
    MenuItem menuItem = orderItem.getMenuItem();
    if (menuItem != null) {
      com.floreantpos.swing.ComboBoxModel model = (com.floreantpos.swing.ComboBoxModel)cbItems.getModel();
      model.addElement(menuItem);
      cbItems.setSelectedItem(menuItem);
      loadUnits();
      com.floreantpos.swing.ComboBoxModel unitModel = (com.floreantpos.swing.ComboBoxModel)cbUnits.getModel();
      if (unitModel.getSize() > 0) {
        List<IUnit> units = unitModel.getDataList();
        for (IUnit unit : units) {
          if (orderItem.getUnit().equals(unit.getUniqueCode())) {
            cbUnits.setSelectedItem(unit);
            break;
          }
        }
      }
    }
    if (orderItem.getInventoryLocation() == null) {
      InventoryLocation location = InventoryLocationDAO.getInstance().getDefaultInInventoryLocation();
      cbLocations.setSelectedItem(location);
    }
    else {
      cbLocations.setSelectedItem(orderItem.getInventoryLocation());
    }
    tfActualUnit.setText(String.valueOf(orderItem.getActualUnit()));
    tfUnitOnHand.setText(String.valueOf(orderItem.getMenuItem().getUnitOnHand()));
  }
  
  public void doOk()
  {
    if (isEditMode) {
      if (orderItem == null) {
        orderItem = new StockCountItem();
      }
    }
    else {
      orderItem = new StockCountItem();
    }
    
    MenuItem menuItem = (MenuItem)cbItems.getSelectedItem();
    if (menuItem == null) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select an item");
      return;
    }
    IUnit unit = (IUnit)cbUnits.getSelectedItem();
    if (unit == null) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select item unit");
      return;
    }
    InventoryLocation location = (InventoryLocation)cbLocations.getSelectedItem();
    if (location == null) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select location");
      return;
    }
    double quantity = tfActualUnit.getDoubleOrZero();
    orderItem.setSku(menuItem.getSku());
    orderItem.setMenuItem(menuItem);
    orderItem.setItemId(menuItem.getId());
    orderItem.setUnit(unit.getUniqueCode());
    orderItem.setInventoryLocation(location);
    orderItem.setActualUnit(Double.valueOf(quantity));
    InventoryStock inventoryStock = InventoryStockDAO.getInstance().getInventoryStock(menuItem, location, unit == null ? null : unit.getUniqueCode());
    orderItem.setUnitOnHand(Double.valueOf(inventoryStock == null ? 0.0D : inventoryStock.getQuantityInHand().doubleValue()));
    orderItem.setName(menuItem.getDisplayName());
    
    if (isEditMode) {
      setCanceled(false);
      dispose();
      return;
    }
    
    List<StockCountItem> items = tableModel.getRows();
    StockCountItem existedItem = null;
    for (StockCountItem stockCountItem : items) {
      if ((stockCountItem.getItemId().equals(orderItem.getItemId())) && (stockCountItem.getInventoryLocation() == orderItem.getInventoryLocation())) {
        existedItem = stockCountItem;
        break;
      }
    }
    if (existedItem == null) {
      tableModel.addRow(orderItem);
      return;
    }
    
    int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Item already added, do you want to update count?", "Item count modification");
    if (option != 0) {
      return;
    }
    
    existedItem.setActualUnit(Double.valueOf(quantity));
    tableModel.fireTableDataChanged();
    tfSearchString.requestFocus();
    tfSearchString.selectAll();
  }
  
  private void loadStockQuantity()
  {
    MenuItem menuItem = (MenuItem)cbItems.getSelectedItem();
    IUnit unit = (IUnit)cbUnits.getSelectedItem();
    InventoryLocation location = (InventoryLocation)cbLocations.getSelectedItem();
    InventoryStock inventoryStock = null;
    if ((menuItem != null) && (location != null)) {
      inventoryStock = InventoryStockDAO.getInstance().getInventoryStock(menuItem, location, unit == null ? null : unit.getUniqueCode());
    }
    double quantity = 0.0D;
    if (inventoryStock != null) {
      quantity = inventoryStock.getQuantityInHand().doubleValue();
    }
    


    tfActualUnit.setText(String.valueOf(quantity > 0.0D ? quantity : 0.0D));
    tfUnitOnHand.setText(String.valueOf(quantity > 0.0D ? quantity : 0.0D));
  }
}
