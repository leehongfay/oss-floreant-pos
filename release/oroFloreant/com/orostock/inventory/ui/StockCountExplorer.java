package com.orostock.inventory.ui;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.config.AppProperties;
import com.floreantpos.main.Application;
import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.StockCount;
import com.floreantpos.model.dao.StockCountDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.ButtonColumn;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosTable;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;


















public class StockCountExplorer
  extends TransparentPanel
{
  private JXTable table;
  private BeanTableModel<StockCount> tableModel;
  private JButton editButton;
  private JButton deleteButton;
  private JTextField tfRefNumber;
  private JCheckBox chkShowClosed;
  private JXDatePicker dpFromDate;
  private JXDatePicker dpEndDate;
  
  public StockCountExplorer()
  {
    tableModel = new BeanTableModel(StockCount.class)
    {
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 6)
          return true;
        return false;
      }
    };
    tableModel.addColumn("SL NO", "refNumber");
    tableModel.addColumn("Count Date", "createdDate");
    tableModel.addColumn("Count By", "user");
    tableModel.addColumn("Verify Date", "varificationDate");
    tableModel.addColumn("Verified By", "verifiedBy");
    tableModel.addColumn("Last Modified Date", "lastModifiedDate");
    tableModel.addColumn("--", "checkVerification");
    
    table = new PosTable(tableModel);
    table.setSortable(false);
    
    AbstractAction action = new AbstractAction()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          int row = Integer.parseInt(e.getActionCommand());
          if (row < 0) {
            return;
          }
          StockCount stockCount = (StockCount)tableModel.getRow(row);
          
          OrderVerificationDialog dialog = new OrderVerificationDialog();
          dialog.setTitle(AppProperties.getAppName());
          dialog.setCaption("Verify Stock Count");
          dialog.setSize(PosUIManager.getSize(350, 250));
          dialog.open();
          if (dialog.isCanceled())
            return;
          stockCount.setVarificationDate(dialog.getSelectedDate());
          stockCount.setVerifiedBy(Application.getCurrentUser());
          stockCount.setLastModifiedDate(new Date());
          
          StockCountDAO.getInstance().saveOrUpdate(stockCount, true);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
      }
    };
    ButtonColumn buttonColumn = new ButtonColumn(table, action, 6)
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        StockCount order = (StockCount)tableModel.getRow(row);
        if (order.getVerifiedBy() != null) {
          return new JLabel("Closed");
        }
        return super.getTableCellRendererComponent(table, value, false, hasFocus, row, column);
      }
      
      public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
      {
        return super.getTableCellEditorComponent(table, value, false, row, column);
      }
      
    };
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    JButton addButton = new JButton("New Stock Count");
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        StockCountExplorer.this.doCreateStockCount();
      }
      
    });
    editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0)
            return;
          index = table.convertRowIndexToModel(index);
          StockCount stockCount = (StockCount)tableModel.getRow(index);
          if (stockCount.getVerifiedBy() != null) {
            POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), "Verified stock count cannot be edited.");
            return;
          }
          StockCountForm stockCountForm = new StockCountForm(stockCount);
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), stockCountForm);
          dialog.openFullScreen();
          if (dialog.isCanceled()) {
            return;
          }
          table.repaint();
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0)
            return;
          index = table.convertRowIndexToModel(index);
          StockCount stockCount = (StockCount)tableModel.getRow(index);
          if (ConfirmDeleteDialog.showMessage(StockCountExplorer.this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0)
          {
            StockCountDAO.getInstance().delete(stockCount);
            tableModel.removeRow(index);
          }
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    MatteBorder selectedBorder = BorderFactory.createMatteBorder(2, 10, 2, 10, table.getBackground());
    MatteBorder unselectedBorder = BorderFactory.createMatteBorder(2, 10, 2, 10, table.getBackground());
    
    Border border1 = new CompoundBorder(selectedBorder, editButton.getBorder());
    Border border2 = new CompoundBorder(unselectedBorder, editButton.getBorder());
    
    buttonColumn.setUnselectedBorder(border1);
    buttonColumn.setFocusBorder(border2);
    
    JButton btnStockCountInfo = new JButton("Stock Count Info");
    btnStockCountInfo.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        StockCountExplorer.this.showStockCountInfo();
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    panel.add(btnStockCountInfo);
    add(buildSearchForm(), "North");
    add(panel, "South");
    resizeTableColumns();
  }
  
  private void resizeTableColumns() {
    table.setAutoResizeMode(4);
    setColumnWidth(0, PosUIManager.getSize(100));
    setColumnWidth(1, PosUIManager.getSize(100));
    setColumnWidth(3, PosUIManager.getSize(100));
    setColumnWidth(4, PosUIManager.getSize(100));
    setColumnWidth(5, PosUIManager.getSize(120));
    setColumnWidth(6, PosUIManager.getSize(120));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = table.getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  private JPanel buildSearchForm() {
    JPanel panel = new JPanel(new MigLayout("fillx", "", ""));
    
    JLabel lblName = new JLabel("Ref:");
    tfRefNumber = new JTextField(15);
    
    dpFromDate = UiUtil.getCurrentMonthStart();
    dpEndDate = UiUtil.getCurrentMonthEnd();
    
    chkShowClosed = new JCheckBox("Show Closed");
    chkShowClosed.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        StockCountExplorer.this.showStockCounts();
      }
    });
    try {
      JButton searchBttn = new JButton("Search");
      panel.add(lblName, "align label,split 7");
      panel.add(tfRefNumber);
      panel.add(new JLabel("From"));
      panel.add(dpFromDate);
      panel.add(new JLabel("To"));
      panel.add(dpEndDate);
      panel.add(searchBttn);
      panel.add(chkShowClosed, "right");
      
      Border loweredetched = BorderFactory.createEtchedBorder(1);
      TitledBorder title = BorderFactory.createTitledBorder(loweredetched, Messages.getString("MenuItemExplorer.30"));
      title.setTitleJustification(1);
      panel.setBorder(title);
      
      searchBttn.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          StockCountExplorer.this.showStockCounts();
        }
        
      });
      tfRefNumber.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          StockCountExplorer.this.showStockCounts();
        }
      });
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
    return panel;
  }
  
  private void showStockCounts() {
    String txtRefId = tfRefNumber.getText();
    Boolean showVerified = Boolean.valueOf(chkShowClosed.isSelected());
    InventoryVendor vendor = null;
    Date from = DateUtil.startOfDay(dpFromDate.getDate());
    Date to = DateUtil.endOfDay(dpEndDate.getDate());
    List stockCountList = StockCountDAO.getInstance().findBy(txtRefId, from, to, showVerified);
    tableModel.setRows(stockCountList);
  }
  
  private void showStockCountInfo() {
    int index = table.getSelectedRow();
    if (index < 0) {
      return;
    }
    StockCount stockCount = (StockCount)tableModel.getRow(index);
    StockCountReport report = new StockCountReport(stockCount);
    try {
      report.showReport();
    } catch (Exception e) {
      e.printStackTrace();
    }
    table.repaint();
  }
  
  private void doCreateStockCount() {
    try {
      StockCountForm editor = new StockCountForm();
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      
      PosButton btnOk = (PosButton)dialog.getButtonPanel().getComponent(0);
      btnOk.setText("SAVE");
      dialog.openFullScreen();
      if (dialog.isCanceled()) {
        return;
      }
      StockCount stockCount = (StockCount)editor.getBean();
      tableModel.addRow(stockCount);
      doPrintStockCount(stockCount);
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  





  private void doPrintStockCount(StockCount stockCount)
    throws Exception
  {}
  





  public void updateView()
  {
    showStockCounts();
  }
}
