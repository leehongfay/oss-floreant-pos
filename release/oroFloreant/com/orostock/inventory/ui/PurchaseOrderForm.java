package com.orostock.inventory.ui;

import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.PurchaseOrder;
import com.floreantpos.model.PurchaseOrderItem;
import com.floreantpos.model.Store;
import com.floreantpos.model.dao.InventoryLocationDAO;
import com.floreantpos.model.dao.InventoryVendorDAO;
import com.floreantpos.model.dao.InventoryVendorItemsDAO;
import com.floreantpos.model.dao.MenuItemDAO;
import com.floreantpos.model.dao.PurchaseOrderDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.NumberSelectionDialog2;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.inv.InventoryLocationSelector;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.NumberUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;




public class PurchaseOrderForm
  extends BeanEditor<PurchaseOrder>
{
  private JButton btnParentLoc;
  private POSTextField tfLocation;
  private JLabel lblLoc;
  private JLabel lblVendor;
  private PurchaseOrder purchaseOrder;
  private JComboBox cbVendor;
  private POSTextField tfOrderId;
  private JLabel lblOrderId;
  private InventoryLocation location;
  private TitledBorder titleBorder;
  private JXDatePicker dpPurchaseOrderDate;
  private JXDatePicker dpReceivedDate;
  private JXTable table;
  private PurchaseItemTableModel tableModel;
  private DoubleTextField tfSubTotalAmount;
  private List<PurchaseOrderItem> items = new ArrayList();
  private boolean receive;
  private JButton btnAddItem;
  private JButton btnDeleteItem;
  private boolean updateAvailableUnit;
  
  public PurchaseOrderForm() {
    this(new PurchaseOrder());
  }
  
  public PurchaseOrderForm(PurchaseOrder purchaseOrder) {
    this(purchaseOrder, false);
  }
  
  public PurchaseOrderForm(PurchaseOrder purchaseOrder, boolean receive) {
    this.purchaseOrder = purchaseOrder;
    this.receive = receive;
    updateAvailableUnit = Application.getInstance().getStore().isUpdateOnHandBlncForPORec();
    if (purchaseOrder.getOrderItems() != null) {
      items.addAll(purchaseOrder.getOrderItems());
    }
    if (receive) {
      for (PurchaseOrderItem item : purchaseOrder.getOrderItems()) {
        item.setQuantityToReceive(Double.valueOf(item.getItemQuantity().doubleValue() - item.getQuantityReceived().doubleValue()));
      }
    }
    
    initComponents();
    
    InventoryVendorDAO inventoryVendorDao = new InventoryVendorDAO();
    List<InventoryVendor> inventoryVendors = inventoryVendorDao.findAll();
    cbVendor.setModel(new ComboBoxModel(inventoryVendors));
    setBean(purchaseOrder);
    
    tfLocation.setEditable(false);
    btnParentLoc.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        InventoryLocation selectedLocation = InventoryLocationSelector.openLocationSelector();
        if (selectedLocation == null)
          return;
        location = selectedLocation;
        tfLocation.setText(location.getName());
      }
    });
    updatePrice();
  }
  
  public void setReceive(boolean receive, boolean updateAvailableUnit) {
    this.receive = receive;
    this.updateAvailableUnit = updateAvailableUnit;
  }
  
  private void updatePrice() {
    double subtotalAmount = 0.0D;
    if (items == null)
      return;
    for (PurchaseOrderItem item : items) {
      item.calculatePrice();
      subtotalAmount += item.getSubtotalAmount().doubleValue();
    }
    tfSubTotalAmount.setText(NumberUtil.formatNumber(Double.valueOf(subtotalAmount)));
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    setBorder(new EmptyBorder(10, 10, 10, 10));
    JPanel itemInfoPanel = new JPanel(new MigLayout("hidemode 3,wrap 2", "[][grow][]", ""));
    titleBorder = new TitledBorder("");
    itemInfoPanel.setBorder(titleBorder);
    add(itemInfoPanel, "North");
    
    lblOrderId = new JLabel("Purchase Order No.");
    itemInfoPanel.add(lblOrderId, "alignx trailing");
    
    tfOrderId = new POSTextField();
    itemInfoPanel.add(tfOrderId, "growx");
    
    lblVendor = new JLabel("Vendor");
    itemInfoPanel.add(lblVendor, "alignx trailing");
    
    cbVendor = new JComboBox();
    cbVendor.setMinimumSize(PosUIManager.getSize(90, 0));
    itemInfoPanel.add(cbVendor, "");
    cbVendor.setEnabled(!receive);
    
    lblLoc = new JLabel("Location");
    itemInfoPanel.add(lblLoc, "alignx trailing");
    
    tfLocation = new POSTextField();
    itemInfoPanel.add(tfLocation, "split 2,growx");
    tfLocation.setEditable(false);
    
    btnParentLoc = new JButton("..LOC..");
    itemInfoPanel.add(btnParentLoc);
    
    dpPurchaseOrderDate = UiUtil.getDeafultDate();
    if (!receive) {
      dpPurchaseOrderDate.setDate(new Date());
      itemInfoPanel.add(new JLabel("Order Date"), "alignx trailing");
      itemInfoPanel.add(dpPurchaseOrderDate);
    }
    dpReceivedDate = UiUtil.getDeafultDate();
    if (receive) {
      dpReceivedDate.setDate(new Date());
      itemInfoPanel.add(new JLabel("Received Date"), "alignx trailing");
      itemInfoPanel.add(dpReceivedDate);
    }
    tableModel = new PurchaseItemTableModel();
    table = new JXTable(tableModel) {
      public void changeSelection(int row, int column, boolean toggle, boolean extend) {
        super.changeSelection(row, column, toggle, extend);
        table.editCellAt(row, column);
        DefaultCellEditor editor = (DefaultCellEditor)table.getCellEditor(row, column);
        DoubleTextField textField = (DoubleTextField)editor.getComponent();
        textField.requestFocus();
        textField.selectAll();
      }
      
      public void setValueAt(Object value, int rowIndex, int columnIndex)
      {
        PurchaseOrderItem purchaseOrderItem = tableModel.getRowData(rowIndex);
        
        String receiveStr = (String)value;
        if (receiveStr.isEmpty())
          return;
        double quantity = Double.parseDouble(receiveStr);
        if (columnIndex == 3) {
          if (receive) {
            purchaseOrderItem.setQuantityToReceive(Double.valueOf(quantity));
            
            List<PurchaseOrderItem> orderItems = purchaseOrder.getOrderItems();
            for (PurchaseOrderItem orderItem : orderItems) {
              if (orderItem.equals(purchaseOrderItem)) {
                orderItem.setQuantityToReceive(purchaseOrderItem.getQuantityToReceive());
              }
            }
          }
          else {
            purchaseOrderItem.setUnitPrice(Double.valueOf(quantity));
            purchaseOrderItem.calculatePrice();
          }
        }
        if ((columnIndex == 2) && 
          (!receive)) {
          purchaseOrderItem.setItemQuantity(Double.valueOf(quantity));
        }
        
        PurchaseOrderForm.this.updatePrice();
        table.repaint();
      }
    };
    table.setDefaultRenderer(Object.class, new DefaultTableCellRenderer()
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if ((column == 0) || (column == 1)) {
          setHorizontalAlignment(2);
        }
        else {
          setHorizontalAlignment(4);
        }
        return c;
      }
    });
    table.setSelectionMode(0);
    
    table.getInputMap().put(KeyStroke.getKeyStroke(32, 0), "startEditing");
    
    DoubleTextField tfEditField = new DoubleTextField();
    
    tfEditField.setHorizontalAlignment(2);
    DefaultCellEditor editor = new DefaultCellEditor(tfEditField);
    editor.setClickCountToStart(1);
    
    if (!receive) {
      table.setDefaultEditor(table.getColumnClass(2), editor);
    }
    
    table.setDefaultEditor(table.getColumnClass(3), editor);
    table.setDefaultEditor(table.getColumnClass(4), editor);
    
    table.setRowHeight(PosUIManager.getSize(30));
    add(new JScrollPane(table));
    
    TableColumnModelExt columnModel = (TableColumnModelExt)table.getColumnModel();
    columnModel.getColumnExt(3).setVisible(receive);
    if (receive) {
      columnModel.getColumnExt(4).setEditable(false);
    }
    columnModel.getColumnExt(5).setVisible(!receive);
    
    JPanel southActionPanel = new JPanel(new MigLayout("fillx,hidemode 3"));
    
    btnAddItem = new JButton("Add Item");
    JButton btnEditItem = new JButton("Edit");
    btnDeleteItem = new JButton("Delete");
    
    btnAddItem.setVisible(!receive);
    btnDeleteItem.setVisible(!receive);
    
    btnAddItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PurchaseOrderItem orderItem = new PurchaseOrderItem();
        PurchaseOrderItemEntryDialog dialog = new PurchaseOrderItemEntryDialog(orderItem);
        dialog.setSize(500, 400);
        dialog.open();
        if (dialog.isCanceled())
          return;
        tableModel.addPurchaseOrderItem(orderItem);
        PurchaseOrderForm.this.updatePrice();
      }
    });
    btnEditItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        int row = table.getSelectedRow();
        if (row == -1)
          return;
        int index = table.convertRowIndexToModel(row);
        
        PurchaseOrderItem item = (PurchaseOrderItem)items.get(index);
        if (item == null)
          return;
        if (receive) {
          double pendingReceivedQty = item.getItemQuantity().doubleValue() - item.getQuantityReceived().doubleValue();
          double quantity = NumberSelectionDialog2.takeDoubleInput("Enter Received Quantity", pendingReceivedQty);
          if (quantity > pendingReceivedQty) {
            POSMessageDialog.showMessage(POSUtil.getBackOfficeWindow(), "Enter valid quantity.");
            return;
          }
          item.setQuantityToReceive(Double.valueOf(quantity));
        }
        else {
          PurchaseOrderItemEntryDialog dialog = new PurchaseOrderItemEntryDialog(item);
          dialog.setSize(500, 400);
          dialog.open();
          if (dialog.isCanceled()) {
            return;
          }
        }
        table.repaint();
        PurchaseOrderForm.this.updatePrice();
      }
      
    });
    btnDeleteItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        int row = table.getSelectedRow();
        if (row == -1)
          return;
        int index = table.convertRowIndexToModel(row);
        
        PurchaseOrderItem item = (PurchaseOrderItem)purchaseOrder.getOrderItems().get(index);
        if (item == null) {
          return;
        }
        tableModel.deletePurchaseOrder(item, index);
      }
    });
    southActionPanel.add(btnAddItem, "split 3,left");
    southActionPanel.add(btnEditItem);
    southActionPanel.add(btnDeleteItem);
    
    tfSubTotalAmount = new DoubleTextField(10);
    tfSubTotalAmount.setHorizontalAlignment(4);
    tfSubTotalAmount.setEditable(false);
    southActionPanel.add(new JLabel("Sub-total Amount: (" + CurrencyUtil.getCurrencySymbol() + ")"), "split 2,right");
    southActionPanel.add(tfSubTotalAmount, "");
    
    add(southActionPanel, "South");
    resizeTableColumns();
  }
  
  private void resizeTableColumns() {
    table.setAutoResizeMode(4);
    if (!receive) {
      setColumnWidth(0, PosUIManager.getSize(100));
      setColumnWidth(2, PosUIManager.getSize(80));
      setColumnWidth(3, PosUIManager.getSize(100));
      setColumnWidth(4, PosUIManager.getSize(80));
      setColumnWidth(5, PosUIManager.getSize(120));
    }
    else {
      setColumnWidth(0, PosUIManager.getSize(100));
      setColumnWidth(2, PosUIManager.getSize(80));
    }
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = table.getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  public boolean save() {
    try {
      if (!updateModel()) {
        return false;
      }
      PurchaseOrder purchaseOrder = (PurchaseOrder)getBean();
      Store store = Application.getInstance().getStore();
      if (receive) {
        InventoryVendorItemsDAO.getInstance().saveItems(purchaseOrder);
        PurchaseOrderDAO.getInstance().saveOrUpdate(purchaseOrder, true, updateAvailableUnit);
        MenuItemDAO.getInstance().updateLastPurchaseCost(purchaseOrder, !Application.getInstance().getStore().isInventoryAvgPricingMethod());

      }
      else if (store.isUpdateAvlBlncForPOCreated()) {
        PurchaseOrderDAO.getInstance().saveOrUpdateWithItemAvailBalance(purchaseOrder);
      }
      else {
        PurchaseOrderDAO.getInstance().saveOrUpdate(purchaseOrder);
      }
      

      return true;
    } catch (Exception e) {
      PosLog.error(getClass(), e);
      POSMessageDialog.showError(e.getMessage());
    }
    return false;
  }
  
  public void updateView() {
    PurchaseOrder purchaseOrder = (PurchaseOrder)getBean();
    if (purchaseOrder.getInventoryLocation() == null) {
      location = InventoryLocationDAO.getInstance().getDefaultInInventoryLocation();
      if (location != null) {
        tfLocation.setText(location.getName());
      }
    }
    else {
      tfLocation.setText(purchaseOrder.getInventoryLocation().getName());
      location = purchaseOrder.getInventoryLocation();
    }
    if (purchaseOrder.getId() == null) {
      tfOrderId.setText(String.valueOf(PurchaseOrderDAO.getInstance().getNextOrderSequenceNumber()));
      return;
    }
    dpPurchaseOrderDate.setDate(purchaseOrder.getCreatedDate());
    cbVendor.setSelectedItem(purchaseOrder.getVendor());
    tfOrderId.setText(purchaseOrder.getOrderId());
    
    updatePrice();
  }
  
  public boolean updateModel() {
    PurchaseOrder purchaseOrder = (PurchaseOrder)getBean();
    
    if (location != null) {
      purchaseOrder.setInventoryLocation(location);
    }
    else {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select location");
      return false;
    }
    String orderId = tfOrderId.getText();
    if (StringUtils.isEmpty(orderId)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Order no cannot be empty");
      return false;
    }
    purchaseOrder.setOrderId(orderId);
    purchaseOrder.setCreatedDate(dpPurchaseOrderDate.getDate());
    purchaseOrder.setVendor((InventoryVendor)cbVendor.getSelectedItem());
    purchaseOrder.setInventoryLocation(location);
    purchaseOrder.setType("DEBIT");
    
    if (receive) {
      purchaseOrder.setReceivingDate(dpReceivedDate.getDate());
    }
    if ((items == null) || (items.isEmpty())) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Order item cannot be empty");
      return false;
    }
    if (purchaseOrder.getOrderItems() != null)
      purchaseOrder.getOrderItems().clear();
    for (PurchaseOrderItem item : items) {
      item.setPurchaseOrder(purchaseOrder);
      purchaseOrder.addToorderItems(item);
    }
    purchaseOrder.calculatePrice();
    return true; }
  
  class PurchaseItemTableModel extends ListTableModel<PurchaseOrderItem> { PurchaseItemTableModel() {}
    
    String[] columnNames = { "SKU", "ITEM", "QTY", "RECEIVE", "COST (" + CurrencyUtil.getCurrencySymbol() + ")", "UNIT", "TOTAL (" + 
      CurrencyUtil.getCurrencySymbol() + ")" };
    
    public int getRowCount() {
      if (items == null) {
        return 0;
      }
      return items.size();
    }
    
    public int getColumnCount() {
      return columnNames.length;
    }
    
    public String getColumnName(int column)
    {
      return columnNames[column];
    }
    
    public PurchaseOrderItem getRowData(int row)
    {
      return (PurchaseOrderItem)items.get(row);
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      if ((columnIndex == 2) && (!receive)) {
        return true;
      }
      if ((columnIndex == 3) || (columnIndex == 4))
        return true;
      return false;
    }
    
    public Object getValueAt(int rowIndex, int columnIndex) {
      if (items == null) {
        return "";
      }
      PurchaseOrderItem item = (PurchaseOrderItem)items.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return item.getSku();
      case 1: 
        return item.getName();
      case 2: 
        return item.getItemQuantity();
      case 3: 
        return item.getQuantityToReceive();
      case 4: 
        return NumberUtil.formatNumber(item.getUnitPrice());
      case 5: 
        return item.getItemUnitName();
      case 6: 
        return NumberUtil.formatNumber(item.getTotalAmount());
      }
      
      return null;
    }
    
    public void addPurchaseOrderItem(PurchaseOrderItem item) {
      int size = items.size();
      items.add(item);
      fireTableRowsInserted(size, size);
    }
    
    public void deletePurchaseOrder(PurchaseOrderItem item, int index) {
      items.remove(item);
      fireTableRowsDeleted(index, index);
    }
  }
  
  public String getDisplayText()
  {
    return "Purchase Order";
  }
  
  public List<PurchaseOrderItem> getItems() {
    return items;
  }
}
