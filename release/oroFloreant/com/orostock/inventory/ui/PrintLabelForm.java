package com.orostock.inventory.ui;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.main.Application;
import com.floreantpos.model.LabelItem;
import com.floreantpos.model.MenuItem;
import com.floreantpos.model.PosPrinters;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.BeanTableModel.DataType;
import com.floreantpos.swing.BeanTableModel.EditMode;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosTable;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.MenuItemSelectionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.KeyStroke;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JasperPrint;




public class PrintLabelForm
  extends JPanel
{
  private JButton addButton;
  private JButton deleteButton;
  private PosButton btnNext;
  private PosButton btnPrev;
  private PrintLabelTable table;
  private BeanTableModel tableModel;
  private JPanel bottomPanel;
  private JButton deleteAllButton;
  
  public PrintLabelForm()
  {
    init();
  }
  
  public void init() {
    setLayout(new BorderLayout(5, 5));
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Print Label");
    add(titlePanel, "North");
    JPanel centerPanel = new JPanel(new MigLayout("fill"));
    
    tableModel = new BeanTableModel(LabelItem.class, 10);
    tableModel.addColumn("BARCODE", "barcode");
    tableModel.addColumn("NAME", "name");
    tableModel.addColumn("MEMBER PRICE (" + CurrencyUtil.getCurrencySymbol() + ")", "memberPrice", BeanTableModel.EditMode.NON_EDITABLE, 11, BeanTableModel.DataType.MONEY);
    
    tableModel.addColumn("RETAIL PRICE (" + CurrencyUtil.getCurrencySymbol() + ")", "retailPrice", BeanTableModel.EditMode.NON_EDITABLE, 11, BeanTableModel.DataType.MONEY);
    
    tableModel.addColumn("QTY", "printQuantity", BeanTableModel.EditMode.EDITABLE, 11, BeanTableModel.DataType.NUMBER);
    
    table = new PrintLabelTable(tableModel);
    
    table.setRowHeight(PosUIManager.getSize(40));
    table.setAutoResizeMode(3);
    
    table.setGridColor(Color.LIGHT_GRAY);
    
    table.getInputMap().put(KeyStroke.getKeyStroke(32, 0), "startEditing");
    
    IntegerTextField tfEditField = new IntegerTextField();
    tfEditField.setHorizontalAlignment(11);
    DefaultCellEditor editor = new DefaultCellEditor(tfEditField);
    editor.setClickCountToStart(1);
    
    table.setDefaultEditor(table.getColumnClass(4), editor);
    
    PosScrollPane scrollPane = new PosScrollPane(table, 20, 31);
    JScrollBar scrollBar = scrollPane.getVerticalScrollBar();
    scrollBar.setPreferredSize(PosUIManager.getSize(30, 60));
    
    centerPanel = new JPanel(new MigLayout("fill"));
    TitledBorder border = BorderFactory.createTitledBorder(null, "Items", 2, 0);
    
    addButton = new JButton("Add Item");
    addButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PrintLabelForm.this.doAddItems();
      }
      
    });
    deleteButton = new JButton("Delete");
    deleteButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PrintLabelForm.this.doDeleteItems();
      }
      
    });
    deleteAllButton = new JButton("Delete All");
    deleteAllButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PrintLabelForm.this.doDeleteAllItems();
      }
      
    });
    btnPrev = new PosButton();
    btnPrev.setIcon(IconFactory.getIcon("/ui_icons/", "previous.png"));
    
    btnNext = new PosButton();
    btnNext.setIcon(IconFactory.getIcon("/ui_icons/", "next.png"));
    
    centerPanel.setBorder(border);
    centerPanel.add(scrollPane, "span,grow, wrap");
    centerPanel.add(addButton, "split 3");
    centerPanel.add(deleteButton);
    centerPanel.add(deleteAllButton);
    add(centerPanel, "Center");
    
    bottomPanel = new JPanel();
    PosButton btnLabelPrint = new PosButton("PRINT");
    btnLabelPrint.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PrintLabelForm.this.doPrintItems();
      }
      
    });
    bottomPanel.add(btnLabelPrint);
    add(bottomPanel, "South");
  }
  
  public JPanel getBottomPanel()
  {
    return bottomPanel;
  }
  
  public void setButtonVisibility(boolean isVisible) {
    addButton.setVisible(isVisible);
    deleteButton.setVisible(isVisible);
    deleteAllButton.setVisible(isVisible);
  }
  
  private void doPrintOnReport(List<LabelItem> items) {
    try {
      JasperPrint jasperPrint = ReceiptPrintService.createPurchaseOrderItemsBarcodePrint(items);
      jasperPrint.setName("BARCODE_REPORT");
      if (Application.getPrinters().getLabelPrinter() == null) {
        POSMessageDialog.showError(POSUtil.getFocusedWindow(), "No label printer found!");
        return;
      }
      jasperPrint.setProperty("printerName", Application.getPrinters().getLabelPrinter());
      ReceiptPrintService.printQuitely(jasperPrint);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  private void doAddItems() {
    MenuItemSelectionDialog dialog = new MenuItemSelectionDialog(new ArrayList(), true);
    dialog.setSelectionMode(1);
    dialog.setSize(PosUIManager.getSize(600, 515));
    dialog.open();
    if (dialog.isCanceled()) {
      return;
    }
    List<MenuItem> menuItemList = dialog.getSelectedItems();
    doAddItems(menuItemList);
  }
  
  public void doAddItems(List<MenuItem> items) {
    List<LabelItem> labelItemList = tableModel.getRows();
    if (labelItemList == null) {
      labelItemList = new ArrayList();
    }
    
    for (Iterator iterator = items.iterator(); iterator.hasNext();) {
      MenuItem menuItem = (MenuItem)iterator.next();
      LabelItem labelItem = new LabelItem();
      labelItem.setMenuItem(menuItem);
      labelItem.setPrintQuantity(1);
      labelItemList.add(labelItem);
    }
    
    tableModel.setRows(labelItemList);
    table.revalidate();
    table.repaint();
  }
  
  public void doAddLabelItems(List<LabelItem> items) {
    List<LabelItem> labelItemList = tableModel.getRows();
    if (labelItemList == null) {
      labelItemList = new ArrayList();
    }
    labelItemList.addAll(items);
    tableModel.setRows(labelItemList);
    table.revalidate();
    table.repaint();
  }
  
  private void doDeleteItems() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Are you sure to remove this item?", "Delete Confirmation:");
      
      if (option != 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      tableModel.removeRow(index);
    }
    catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doDeleteAllItems()
  {
    try {
      if ((tableModel.getRows() == null) || (tableModel.getRows().size() <= 0)) {
        return;
      }
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Are you sure to remove all item?", "Delete Confirmation:");
      
      if (option != 0) {
        return;
      }
      tableModel.removeAll();
    }
    catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doPrintItems() {
    try {
      boolean isValid = true;
      List<LabelItem> printLabelItems = tableModel.getRows();
      if ((printLabelItems == null) || (printLabelItems.size() <= 0)) {
        POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Please add item first!");
        return;
      }
      List<LabelItem> labelItems = new ArrayList();
      
      for (LabelItem labelItem : printLabelItems) {
        if (labelItem.getPrintQuantity() < 1) {
          isValid = false;
          break;
        }
        for (int i = 0; i < labelItem.getPrintQuantity(); i++) {
          labelItems.add(labelItem);
        }
      }
      
      if (!isValid) {
        POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Quantity must be greater than 0!");
        return;
      }
      
      doPrintOnReport(labelItems);
    } catch (Exception e) {
      PosLog.error(PrintLabelForm.class, e.getMessage(), e);
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Error occured!");
    }
  }
  
  private class PrintTableModel extends ListTableModel<LabelItem> {
    public PrintTableModel() {
      super();
    }
    
    public Object getValueAt(int row, int column)
    {
      LabelItem labelItem = (LabelItem)rows.get(row);
      if (labelItem == null) {
        return "";
      }
      MenuItem menuItem = labelItem.getMenuItem();
      switch (column) {
      case 0: 
        return menuItem.getBarcode();
      case 1: 
        return menuItem.getName();
      case 2: 
        return menuItem.getPrice();
      case 3: 
        return menuItem.getRetailPrice();
      case 4: 
        return Integer.valueOf(labelItem.getPrintQuantity());
      }
      
      return null;
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      if (columnIndex == 3)
        return true;
      return false;
    }
    
    public void removeAll() {
      rows.clear();
      fireTableDataChanged();
    }
  }
  
  private class PrintLabelTable extends PosTable
  {
    public PrintLabelTable()
    {
      super();
    }
    
    public void setValueAt(Object value, int rowIndex, int columnIndex)
    {
      LabelItem printLabeltem = (LabelItem)tableModel.getRow(rowIndex);
      
      if ((value instanceof String)) {
        String receiveStr = (String)value;
        if (receiveStr.isEmpty()) {
          return;
        }
        int quantity = Integer.parseInt(receiveStr);
        
        if (columnIndex == 4) {
          printLabeltem.setPrintQuantity(quantity);
        }
      }
    }
  }
}
