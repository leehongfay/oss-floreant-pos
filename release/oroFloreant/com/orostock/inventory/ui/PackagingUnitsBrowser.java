package com.orostock.inventory.ui;

import com.floreantpos.bo.ui.ModelBrowser;
import com.floreantpos.model.PackagingUnit;
import com.floreantpos.model.dao.PackagingUnitDAO;
import com.floreantpos.swing.BeanTableModel;
import java.awt.Dimension;
import javax.swing.JPanel;

public class PackagingUnitsBrowser extends ModelBrowser<PackagingUnit>
{
  private boolean recipeUnit;
  
  public PackagingUnitsBrowser()
  {
    this(false);
  }
  
  public PackagingUnitsBrowser(boolean recipeUnit) {
    super(new PackagingUnitForm(recipeUnit));
    this.recipeUnit = recipeUnit;
    BeanTableModel<PackagingUnit> tableModel = new BeanTableModel(PackagingUnit.class);
    tableModel.addColumn("CODE", PackagingUnit.PROP_CODE);
    tableModel.addColumn("NAME", PackagingUnit.PROP_NAME);
    init(tableModel);
    browserPanel.setPreferredSize(new Dimension(500, 0));
    refreshTable();
  }
  
  public void refreshTable()
  {
    java.util.List<PackagingUnit> locations = PackagingUnitDAO.getInstance().findAll(recipeUnit);
    BeanTableModel tableModel = (BeanTableModel)browserTable.getModel();
    tableModel.removeAll();
    tableModel.addRows(locations);
  }
}
