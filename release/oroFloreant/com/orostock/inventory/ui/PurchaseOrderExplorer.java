package com.orostock.inventory.ui;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.config.AppProperties;
import com.floreantpos.main.Application;
import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.LabelItem;
import com.floreantpos.model.PosPrinters;
import com.floreantpos.model.PurchaseOrder;
import com.floreantpos.model.PurchaseOrderItem;
import com.floreantpos.model.dao.InventoryVendorDAO;
import com.floreantpos.model.dao.PurchaseOrderDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.BeanTableModel.DataType;
import com.floreantpos.swing.ButtonColumn;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosTable;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.util.UiUtil;
import com.floreantpos.util.CurrencyUtil;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JasperPrint;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;



















public class PurchaseOrderExplorer
  extends TransparentPanel
{
  private JXTable table;
  private BeanTableModel<PurchaseOrder> tableModel;
  private JButton editButton;
  private JButton deleteButton;
  private JComboBox cbVendors;
  private JTextField tfPONumber;
  private JCheckBox chkShowClosed;
  private JXDatePicker dpFromDate;
  private JXDatePicker dpEndDate;
  private JButton btnNewPurchaseOrder;
  
  public PurchaseOrderExplorer()
  {
    tableModel = new BeanTableModel(PurchaseOrder.class)
    {
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 8)
          return true;
        return false;
      }
    };
    tableModel.addColumn("P.O NO", "orderId");
    tableModel.addColumn("Order Date", "createdDate");
    tableModel.addColumn("Vendor", "vendor");
    tableModel.addColumn("Location", "inventoryLocation");
    tableModel.addColumn("Total Cost (" + CurrencyUtil.getCurrencySymbol() + ")", "totalAmount", 11, BeanTableModel.DataType.MONEY);
    tableModel.addColumn("Status", "orderStatusDisplay");
    tableModel.addColumn("Verified Date", "varificationDate");
    tableModel.addColumn("Received Date", "receivingDate");
    tableModel.addColumn("--", "statusDisplay");
    table = new PosTable(tableModel);
    table.setSortable(false);
    
    AbstractAction action = new AbstractAction()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          int row = Integer.parseInt(e.getActionCommand());
          if (row < 0)
            return;
          PurchaseOrder purchaseOrder = (PurchaseOrder)tableModel.getRow(row);
          if (purchaseOrder.getStatus().intValue() == 0) {
            OrderVerificationDialog dialog = new OrderVerificationDialog();
            dialog.setTitle(AppProperties.getAppName());
            dialog.setCaption("Verify purchase order");
            dialog.setSize(PosUIManager.getSize(350, 250));
            dialog.open();
            if (dialog.isCanceled())
              return;
            purchaseOrder.setVarificationDate(dialog.getSelectedDate());
            purchaseOrder.setStatus(Integer.valueOf(1));
            PurchaseOrderDAO.getInstance().saveOrUpdate(purchaseOrder);
            table.repaint();
          }
          else if ((purchaseOrder.getStatus().intValue() == 1) || (purchaseOrder.getStatus().intValue() == 5)) {
            PurchaseOrderForm purchaseOrderForm = new PurchaseOrderForm(purchaseOrder, true);
            BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), purchaseOrderForm);
            dialog.openWithScale(830, 630);
            if (dialog.isCanceled())
              return;
          }
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
      }
    };
    ButtonColumn buttonColumn = new ButtonColumn(table, action, 8)
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        PurchaseOrder order = (PurchaseOrder)tableModel.getRow(row);
        if ((column == 8) && 
          (order.getStatus().intValue() != 0) && (order.getStatus().intValue() != 1) && (order.getStatus().intValue() != 5)) {
          return new JLabel("Closed", 0);
        }
        
        return super.getTableCellRendererComponent(table, value, false, hasFocus, row, column);
      }
      
      public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
      {
        return super.getTableCellEditorComponent(table, value, false, row, column);
      }
      
    };
    setLayout(new BorderLayout(5, 5));
    add(new JScrollPane(table));
    
    JButton btnPrint = new JButton("Print Barcode");
    
    btnPrint.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PurchaseOrderExplorer.this.doPrintBarcode();
      }
      
    });
    btnNewPurchaseOrder = new JButton("New Purchase Order");
    btnNewPurchaseOrder.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        PurchaseOrderExplorer.this.doCreatePurchaseOrder();
      }
      
    });
    editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        PurchaseOrderExplorer.this.doEditPurchaseOrder();
      }
      
    });
    deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        PurchaseOrderExplorer.this.doDeletePurchaseOrder();
      }
      
    });
    MatteBorder selectedBorder = BorderFactory.createMatteBorder(2, 10, 2, 10, table.getBackground());
    MatteBorder unselectedBorder = BorderFactory.createMatteBorder(2, 10, 2, 10, table.getBackground());
    
    Border border1 = new CompoundBorder(selectedBorder, editButton.getBorder());
    Border border2 = new CompoundBorder(unselectedBorder, editButton.getBorder());
    
    buttonColumn.setUnselectedBorder(border1);
    buttonColumn.setFocusBorder(border2);
    
    JButton btnPurchaseOrderInfo = new JButton("Order Info");
    btnPurchaseOrderInfo.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PurchaseOrderExplorer.this.showPurchaseOrderInfo();
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(btnPrint);
    panel.add(btnNewPurchaseOrder);
    panel.add(editButton);
    panel.add(deleteButton);
    panel.add(btnPurchaseOrderInfo);
    add(buildSearchForm(), "North");
    add(panel, "South");
    resizeTableColumns();
  }
  
  public void print(List<LabelItem> items) throws Exception {
    JasperPrint jasperPrint = ReceiptPrintService.createPurchaseOrderItemsBarcodePrint(items);
    jasperPrint.setName("BARCODE_REPORT");
    if (Application.getPrinters().getLabelPrinter() == null) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), "No label printer found!");
      return;
    }
    jasperPrint.setProperty("printerName", Application.getPrinters().getLabelPrinter());
    ReceiptPrintService.printQuitely(jasperPrint);
  }
  
  private void resizeTableColumns() {
    table.setAutoResizeMode(4);
    setColumnWidth(0, PosUIManager.getSize(100));
    setColumnWidth(1, PosUIManager.getSize(100));
    setColumnWidth(3, PosUIManager.getSize(100));
    setColumnWidth(4, PosUIManager.getSize(100));
    setColumnWidth(5, PosUIManager.getSize(120));
    setColumnWidth(6, PosUIManager.getSize(120));
    setColumnWidth(7, PosUIManager.getSize(120));
    setColumnWidth(8, PosUIManager.getSize(100));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = table.getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  private JPanel buildSearchForm() {
    JPanel panel = new JPanel(new MigLayout("fillx", "", ""));
    
    JLabel lblOrderType = new JLabel("Vendor");
    cbVendors = new JComboBox();
    cbVendors.addItem("<All>");
    
    List<InventoryVendor> vendors = InventoryVendorDAO.getInstance().findAll();
    for (InventoryVendor vendor : vendors) {
      cbVendors.addItem(vendor);
    }
    JLabel lblName = new JLabel("P.O Number");
    tfPONumber = new JTextField(15);
    
    dpFromDate = UiUtil.getCurrentMonthStart();
    dpEndDate = UiUtil.getCurrentMonthEnd();
    
    chkShowClosed = new JCheckBox("Show Closed");
    chkShowClosed.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        PurchaseOrderExplorer.this.showPurchaseOrders();
      }
    });
    try {
      JButton searchBttn = new JButton("Search");
      panel.add(lblName, "align label,split 9");
      panel.add(tfPONumber);
      panel.add(lblOrderType);
      panel.add(cbVendors);
      panel.add(new JLabel("From"));
      panel.add(dpFromDate);
      panel.add(new JLabel("To"));
      panel.add(dpEndDate);
      panel.add(searchBttn);
      panel.add(chkShowClosed, "right");
      
      Border loweredetched = BorderFactory.createEtchedBorder(1);
      TitledBorder title = BorderFactory.createTitledBorder(loweredetched, Messages.getString("MenuItemExplorer.30"));
      title.setTitleJustification(1);
      panel.setBorder(title);
      
      searchBttn.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e) {
          PurchaseOrderExplorer.this.showPurchaseOrders();
        }
        
      });
      tfPONumber.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          PurchaseOrderExplorer.this.showPurchaseOrders();
        }
      });
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
    return panel;
  }
  
  private void showPurchaseOrders() {
    String txName = tfPONumber.getText();
    Boolean showClosed = Boolean.valueOf(chkShowClosed.isSelected());
    InventoryVendor vendor = null;
    Object selecteditem = cbVendors.getSelectedItem();
    
    Date from = DateUtil.startOfDay(dpFromDate.getDate());
    Date to = DateUtil.endOfDay(dpEndDate.getDate());
    
    if ((selecteditem instanceof InventoryVendor)) {
      vendor = (InventoryVendor)selecteditem;
    }
    List purchaseOrderList = PurchaseOrderDAO.getInstance().findBy(txName, vendor, from, to, showClosed);
    tableModel.setRows(purchaseOrderList);
  }
  
  private void showPurchaseOrderInfo() {
    List<PurchaseOrder> orderList = new ArrayList();
    int index = table.getSelectedRow();
    if (index < 0)
      return;
    index = table.convertRowIndexToModel(index);
    PurchaseOrder purchaseOrder = (PurchaseOrder)tableModel.getRow(index);
    orderList.add(purchaseOrder);
    
    PurchaseOrderInfoDialog dialog = new PurchaseOrderInfoDialog(orderList);
    dialog.setSize(PosUIManager.getSize(900, 650));
    dialog.open();
    if (dialog.isCanceled()) {
      return;
    }
    table.repaint();
  }
  
  private void doCreatePurchaseOrder() {
    try {
      final PurchaseOrderForm editor = new PurchaseOrderForm();
      final BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      
      PosButton btnSaveAndReceiveOrder = new PosButton("<html><center>SAVE &<br> RECEIVE</center></html>");
      btnSaveAndReceiveOrder.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          if (!editor.updateModel())
            return;
          PurchaseOrder purchaseOrder = (PurchaseOrder)editor.getBean();
          for (PurchaseOrderItem item : purchaseOrder.getOrderItems()) {
            item.setQuantityToReceive(Double.valueOf(item.getItemQuantity().doubleValue() - item.getQuantityReceived().doubleValue()));
          }
          Date today = new Date();
          purchaseOrder.setVarificationDate(today);
          purchaseOrder.setCreatedDate(today);
          purchaseOrder.setReceivingDate(today);
          purchaseOrder.setClosingDate(today);
          editor.setReceive(true, true);
          if (!editor.save())
            return;
          dialog.dispose();
        }
      });
      PosButton btnOk = (PosButton)dialog.getButtonPanel().getComponent(0);
      btnOk.setText("SAVE");
      dialog.getButtonPanel().add(btnSaveAndReceiveOrder, 0);
      dialog.openWithScale(830, 630);
      if (dialog.isCanceled()) {
        return;
      }
      PurchaseOrder purchaseOrder = (PurchaseOrder)editor.getBean();
      tableModel.addRow(purchaseOrder);
      doPrintPurchaseOrder(purchaseOrder);
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doPrintPurchaseOrder(PurchaseOrder purchaseOrder) throws Exception {
    try {
      if (POSMessageDialog.showMessageAndPromtToPrint(POSUtil.getBackOfficeWindow(), "Successfully " + purchaseOrder.getOrderId() + " purchase order has been created ."))
      {
        JasperPrint jasperPrint = ReceiptPrintService.createPurchaseOrderPrint(purchaseOrder);
        jasperPrint.setName("PURCHASE_ORDER_" + purchaseOrder.getOrderId());
        jasperPrint.setProperty("printerName", Application.getPrinters().getReceiptPrinter());
        ReceiptPrintService.printQuitely(jasperPrint);
      }
    } catch (Exception e) {
      throw e;
    }
  }
  
  public void updateView()
  {
    showPurchaseOrders();
  }
  
  private void doPrintBarcode() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please select an order.");
        return;
      }
      index = table.convertRowIndexToModel(index);
      PurchaseOrder purchaseOrder = (PurchaseOrder)tableModel.getRow(index);
      List<PurchaseOrderItem> orderItems = purchaseOrder.getOrderItems();
      List<LabelItem> labelItems = new ArrayList();
      for (PurchaseOrderItem purchaseOrderItem : orderItems) {
        LabelItem labelItem = new LabelItem();
        labelItem.setMenuItem(purchaseOrderItem.getMenuItem());
        if (purchaseOrder.getStatus().intValue() == 5) {
          double quantity = purchaseOrderItem.getQuantityReceived().doubleValue();
          labelItem.setPrintQuantity((int)quantity);
        }
        else {
          double itemQuantity = purchaseOrderItem.getItemQuantity().doubleValue();
          labelItem.setPrintQuantity((int)itemQuantity);
        }
        
        labelItems.add(labelItem);
      }
      PrintLabelDialog dialog = new PrintLabelDialog();
      dialog.setButtonVisibility(false);
      dialog.doAddLabelItems(labelItems);
      dialog.setSize(PosUIManager.getSize(650, 600));
      dialog.open();
    } catch (Exception e) {
      PosLog.error(PurchaseOrderExplorer.class, e.getMessage(), e);
    }
  }
  
  private void doEditPurchaseOrder() {
    try {
      int index = table.getSelectedRow();
      if (index < 0)
        return;
      index = table.convertRowIndexToModel(index);
      PurchaseOrder purchaseOrder = (PurchaseOrder)tableModel.getRow(index);
      PurchaseOrderForm purchaseOrderForm = new PurchaseOrderForm(purchaseOrder);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), purchaseOrderForm);
      dialog.openWithScale(830, 630);
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doDeletePurchaseOrder() {
    try {
      int index = table.getSelectedRow();
      if (index < 0)
        return;
      index = table.convertRowIndexToModel(index);
      PurchaseOrder purchaseOrder = (PurchaseOrder)tableModel.getRow(index);
      if (ConfirmDeleteDialog.showMessage(this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0) {
        PurchaseOrderDAO.getInstance().delete(purchaseOrder);
        tableModel.removeRow(index);
      }
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
}
