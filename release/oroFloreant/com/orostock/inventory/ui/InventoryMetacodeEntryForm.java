package com.orostock.inventory.ui;

import com.floreantpos.model.InventoryMetaCode;
import com.floreantpos.model.dao.InventoryMetaCodeDAO;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;


public class InventoryMetacodeEntryForm
  extends BeanEditor<InventoryMetaCode>
{
  private JLabel lblType;
  private JLabel lblName;
  private JLabel lblNo;
  private JLabel lblDescription;
  private POSTextField tfType;
  private POSTextField tfName;
  private IntegerTextField tfNo;
  private POSTextField tfDescription;
  
  public InventoryMetacodeEntryForm(InventoryMetaCode ig)
  {
    setLayout(new BorderLayout());
    
    createUI();
    
    setBean(ig);
  }
  


  private void createUI()
  {
    JPanel panel = new JPanel();
    add(panel, "Center");
    panel.setLayout(new MigLayout("", "[][grow]", "[][][][]"));
    
    lblType = new JLabel("Type");
    panel.add(lblType, "cell 0 0,alignx trailing");
    
    tfType = new POSTextField();
    panel.add(tfType, "cell 1 0,growx");
    
    lblName = new JLabel("Name");
    panel.add(lblName, "cell 0 1,alignx trailing");
    
    tfName = new POSTextField();
    panel.add(tfName, "cell 1 1,growx");
    
    lblNo = new JLabel("No");
    panel.add(lblNo, "cell 0 2,alignx trailing");
    
    tfNo = new IntegerTextField();
    panel.add(tfNo, "cell 1 2,growx");
    
    lblDescription = new JLabel("Description");
    panel.add(lblDescription, "cell 0 3,alignx trailing");
    
    tfDescription = new POSTextField();
    panel.add(tfDescription, "cell 1 3,growx");
  }
  





















  public void updateView()
  {
    InventoryMetaCode model = (InventoryMetaCode)getBean();
    
    if (model == null) {
      return;
    }
    
    tfType.setText(model.getType());
    tfName.setText(model.getCodeText());
    tfNo.setText("" + model.getCodeNo());
    tfDescription.setText(model.getDescription());
  }
  
  public boolean updateModel() {
    InventoryMetaCode model = (InventoryMetaCode)getBean();
    
    if (model == null) {
      model = new InventoryMetaCode();
    }
    
    model.setType(tfType.getText());
    model.setCodeText(tfName.getText());
    model.setCodeNo(Integer.valueOf(tfNo.getInteger()));
    model.setDescription(tfDescription.getText());
    
    return true;
  }
  
  public String getDisplayText()
  {
    return "Add inventory MetaData";
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      
      InventoryMetaCode model = (InventoryMetaCode)getBean();
      InventoryMetaCodeDAO.getInstance().saveOrUpdate(model);
      
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(e.getMessage());
    }
    
    return false;
  }
}
