package com.orostock.inventory.ui;

import com.floreantpos.bo.ui.ModelBrowser;
import com.floreantpos.model.InventoryUnitGroup;
import com.floreantpos.model.dao.InventoryUnitGroupDAO;
import com.floreantpos.swing.BeanTableModel;
import java.util.List;

public class InventoryUnitGroupBrowser extends ModelBrowser<InventoryUnitGroup>
{
  public InventoryUnitGroupBrowser()
  {
    super(new InventoryUnitGroupEntryForm(new InventoryUnitGroup()));
    BeanTableModel<InventoryUnitGroup> tableModel = new BeanTableModel(InventoryUnitGroup.class);
    tableModel.addColumn("NAME", InventoryUnitGroup.PROP_NAME);
    init(tableModel);
    refreshTable();
  }
  
  public void refreshTable()
  {
    List<InventoryUnitGroup> locations = InventoryUnitGroupDAO.getInstance().findAll();
    BeanTableModel tableModel = (BeanTableModel)browserTable.getModel();
    tableModel.removeAll();
    tableModel.addRows(locations);
  }
}
