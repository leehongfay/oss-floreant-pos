package com.orostock.inventory.ui;

import com.floreantpos.config.AppProperties;
import com.floreantpos.model.LabelItem;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JPanel;

public class PrintLabelDialog extends POSDialog
{
  private PrintLabelForm printLabelForm;
  
  public PrintLabelDialog()
  {
    super(POSUtil.getBackOfficeWindow(), true);
    setTitle(AppProperties.getAppName());
    init();
  }
  
  private void init() {
    setLayout(new BorderLayout());
    printLabelForm = new PrintLabelForm();
    JPanel bottomPanel = printLabelForm.getBottomPanel();
    PosButton btnClose = new PosButton("CLOSE");
    btnClose.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dispose();
      }
    });
    bottomPanel.add(btnClose);
    add(printLabelForm, "Center");
  }
  
  public void setButtonVisibility(boolean isVisible) {
    printLabelForm.setButtonVisibility(isVisible);
  }
  
  public void doAddLabelItems(List<LabelItem> labelItems)
  {
    printLabelForm.doAddLabelItems(labelItems);
  }
}
