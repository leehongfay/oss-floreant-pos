package com.orostock.inventory.ui;

import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.dao.InventoryLocationDAO;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.TreeDisplayDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;



public class InventoryLocationEntryForm
  extends BeanEditor<InventoryLocation>
{
  private POSTextField tfName;
  private JLabel lblSortOrder;
  private IntegerTextField tfSortOrder;
  InventoryLocation inventoryLocation;
  private JLabel lblTranslatedName;
  private POSTextField tfTranslatedName;
  private JButton btnParentLoc;
  private POSTextField tfParentLocation;
  private JLabel lblParentLoc;
  private POSTextField tfAddress;
  private JLabel lblAddress;
  private JCheckBox cbDefaultInLocation;
  private JCheckBox cbDefaultOutLocation;
  private InventoryLocation parentLocation;
  private JLabel lblCode;
  private POSTextField tfCode;
  private JLabel lblDescription;
  private JTextArea taDescription;
  
  public InventoryLocationEntryForm(InventoryLocation inventoryLocation)
  {
    this.inventoryLocation = inventoryLocation;
    setLayout(new BorderLayout());
    
    createUI();
    setBean(inventoryLocation);
    
    updateView();
    
    btnParentLoc.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        List<InventoryLocation> rootLocations = InventoryLocationDAO.getInstance().getRootLocations();
        DefaultMutableTreeNode root = new DefaultMutableTreeNode();
        for (InventoryLocation inventoryLocation : rootLocations) {
          DefaultMutableTreeNode node = new DefaultMutableTreeNode(inventoryLocation);
          root.add(node);
          InventoryLocationEntryForm.this.buildTree(node);
        }
        
        JTree tree = new JTree(root);
        for (int i = 1; i < tree.getRowCount() * 2; i++) {
          tree.expandRow(i);
        }
        tree.setRootVisible(false);
        
        TreeDisplayDialog treeDisplayDialog = new TreeDisplayDialog(tree);
        treeDisplayDialog.setPreferredSize(PosUIManager.getSize(500, 600));
        treeDisplayDialog.open();
        if (treeDisplayDialog.isCanceled()) {
          return;
        }
        DefaultMutableTreeNode path = treeDisplayDialog.getPath();
        parentLocation = ((InventoryLocation)path.getUserObject());
        tfParentLocation.setText(String.valueOf(parentLocation));
        tfParentLocation.setEditable(false);
      }
    });
  }
  
  public InventoryLocationEntryForm() {
    setLayout(new BorderLayout());
    createUI();
  }
  
  private void createUI() {
    JPanel panel = new JPanel();
    add(panel, "Center");
    panel.setLayout(new MigLayout("", "[][grow][]", ""));
    
    JLabel lblName = new JLabel("Name");
    panel.add(lblName, "cell 0 0,alignx trailing");
    
    tfName = new POSTextField();
    panel.add(tfName, "cell 1 0,growx");
    
    lblTranslatedName = new JLabel("Translated Name");
    panel.add(lblTranslatedName, "cell 0 1,alignx trailing ");
    
    tfTranslatedName = new POSTextField();
    panel.add(tfTranslatedName, "cell 1 1,growx");
    
    lblAddress = new JLabel("Address");
    panel.add(lblAddress, "cell 0 2, alignx trailing");
    
    tfAddress = new POSTextField();
    panel.add(tfAddress, "cell 1 2, growx");
    
    lblCode = new JLabel("Code");
    panel.add(lblCode, "cell 0 3, alignx trailing");
    
    tfCode = new POSTextField();
    panel.add(tfCode, "cell 1 3, growx");
    
    lblDescription = new JLabel("Description");
    panel.add(lblDescription, "cell 0 4, alignx trailing");
    
    taDescription = new JTextArea();
    taDescription.setLineWrap(true);
    JScrollPane scrlDescription = new JScrollPane(taDescription, 20, 30);
    scrlDescription.setPreferredSize(PosUIManager.getSize(0, 100));
    panel.add(scrlDescription, "cell 1 4, growx");
    
    lblParentLoc = new JLabel("Parent Location");
    panel.add(lblParentLoc, "cell 0 5, alignx trailing");
    
    tfParentLocation = new POSTextField();
    tfParentLocation.setEditable(false);
    panel.add(tfParentLocation, "cell 1 5,split 3, growx");
    
    btnParentLoc = new JButton("Select");
    panel.add(btnParentLoc);
    
    JButton btnClear = new JButton("Clear");
    panel.add(btnClear);
    btnClear.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        parentLocation = null;
        tfParentLocation.setText("");
        ((InventoryLocation)getBean()).setParentLocation(null);
      }
      
    });
    lblSortOrder = new JLabel("Sort order");
    panel.add(lblSortOrder, "cell 0 6,alignx trailing");
    
    tfSortOrder = new IntegerTextField();
    panel.add(tfSortOrder, "cell 1 6,growx");
    
    cbDefaultInLocation = new JCheckBox("Default In Location", false);
    panel.add(cbDefaultInLocation, "cell 1 7");
    
    cbDefaultOutLocation = new JCheckBox("Default Out Location", false);
    panel.add(cbDefaultOutLocation, "cell 1 8");
  }
  
  private void buildTree(DefaultMutableTreeNode node)
  {
    InventoryLocation location = (InventoryLocation)node.getUserObject();
    if (location == null) {
      return;
    }
    List<InventoryLocation> children = location.getChildren();
    
    if (children == null) {
      return;
    }
    
    for (InventoryLocation inventoryLocation : children) {
      DefaultMutableTreeNode newChild = new DefaultMutableTreeNode(inventoryLocation);
      node.add(newChild);
      buildTree(newChild);
    }
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      
      InventoryLocation model = (InventoryLocation)getBean();
      InventoryLocationDAO.getInstance().saveOrUpdate(model);
      
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(e.getMessage());
    }
    
    return false;
  }
  
  public void updateView() {
    InventoryLocation model = (InventoryLocation)getBean();
    
    if (model == null) {
      return;
    }
    
    tfName.setText(model.getName());
    tfTranslatedName.setText(model.getTranslatedName());
    tfAddress.setText(model.getAddress());
    if (model.getParentLocation() != null)
      tfParentLocation.setText(model.getParentLocation().getName());
    tfSortOrder.setText(String.valueOf(model.getSortOrder()));
    

    cbDefaultInLocation.setSelected(model.isDefaultInLocation().booleanValue());
    cbDefaultOutLocation.setSelected(model.isDefaultOutLocation().booleanValue());
    
    tfCode.setText(model.getCode());
    taDescription.setText(model.getDescription());
  }
  
  public boolean updateModel()
  {
    InventoryLocation model = (InventoryLocation)getBean();
    
    String nameString = tfName.getText();
    if (StringUtils.isEmpty(nameString)) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), "Name cannot be empty");
      return false;
    }
    
    String translatedName = tfTranslatedName.getText();
    String address = tfAddress.getText();
    
    if (parentLocation != null) {
      model.setParentLocation(parentLocation);
    }
    
    model.setName(nameString);
    model.setTranslatedName(translatedName);
    model.setAddress(address);
    model.setSortOrder(Integer.valueOf(tfSortOrder.getInteger()));
    
    if (model.getParentLocation() == null) {
      model.setRoot(Boolean.valueOf(true));
    }
    else {
      model.setRoot(Boolean.valueOf(false));
    }
    
    List<InventoryLocation> inventoryLocationList = InventoryLocationDAO.getInstance().findAll();
    Iterator iterator;
    if (cbDefaultInLocation.isSelected()) {
      for (iterator = inventoryLocationList.iterator(); iterator.hasNext();) {
        InventoryLocation inventoryLocation = (InventoryLocation)iterator.next();
        
        if ((inventoryLocation.isDefaultInLocation().booleanValue()) && (!inventoryLocation.equals(model))) {
          inventoryLocation.setDefaultInLocation(Boolean.valueOf(false));
          InventoryLocationDAO.getInstance().saveOrUpdate(inventoryLocation);
        }
      }
    }
    model.setDefaultInLocation(Boolean.valueOf(cbDefaultInLocation.isSelected()));
    Iterator iterator;
    if (cbDefaultOutLocation.isSelected()) {
      for (iterator = inventoryLocationList.iterator(); iterator.hasNext();) {
        InventoryLocation inventoryLocation = (InventoryLocation)iterator.next();
        
        if ((inventoryLocation.isDefaultOutLocation().booleanValue()) && (inventoryLocation != model)) {
          inventoryLocation.setDefaultOutLocation(Boolean.valueOf(false));
          InventoryLocationDAO.getInstance().saveOrUpdate(inventoryLocation);
        }
      }
    }
    
    model.setDefaultOutLocation(Boolean.valueOf(cbDefaultOutLocation.isSelected()));
    
    model.setCode(tfCode.getText());
    model.setDescription(taDescription.getText());
    
    return true;
  }
  

  public String getDisplayText()
  {
    return "Add Inventory Location";
  }
}
