package com.orostock.inventory.ui;

import com.floreantpos.model.InventoryVendor;
import com.floreantpos.model.dao.InventoryVendorDAO;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;




public class InventoryVendorEntryForm
  extends BeanEditor<InventoryVendor>
{
  private JCheckBox chkVisible;
  private FixedLengthTextField tfName;
  private JTextArea tfAddress;
  private FixedLengthTextField tfCity;
  private FixedLengthTextField tfState;
  private FixedLengthTextField tfZip;
  private FixedLengthTextField tfCountry;
  private FixedLengthTextField tfPhone;
  private FixedLengthTextField tfFax;
  private FixedLengthTextField tfEmail;
  
  public InventoryVendorEntryForm()
  {
    setLayout(new BorderLayout());
    
    createUI();
  }
  
  public InventoryVendorEntryForm(InventoryVendor ig)
  {
    setLayout(new BorderLayout());
    
    createUI();
    
    setBean(ig);
  }
  
  private void createUI() {
    JPanel panel = new JPanel(new MigLayout("fillx,wrap 2", "", ""));
    
    JLabel lblName = new JLabel("Vendor Name");
    tfName = new FixedLengthTextField();
    tfName.setLength(60);
    
    chkVisible = new JCheckBox("Visible", true);
    
    JLabel addressLabel = new JLabel("Address");
    tfAddress = new JTextArea(4, 4);
    tfAddress.setLineWrap(true);
    
    JLabel cityLabel = new JLabel("City");
    tfCity = new FixedLengthTextField();
    tfCity.setLength(60);
    
    JLabel stateLabel = new JLabel("State");
    tfState = new FixedLengthTextField();
    tfState.setLength(60);
    
    JLabel zipLabel = new JLabel("Zip");
    tfZip = new FixedLengthTextField();
    tfZip.setLength(60);
    
    JLabel countryLabel = new JLabel("Country");
    tfCountry = new FixedLengthTextField();
    tfCountry.setLength(60);
    
    JLabel emailLabel = new JLabel("Email");
    tfEmail = new FixedLengthTextField();
    tfEmail.setLength(60);
    
    JLabel phoneLabel = new JLabel("Phone");
    tfPhone = new FixedLengthTextField();
    tfPhone.setLength(60);
    
    JLabel faxLabel = new JLabel("Fax");
    tfFax = new FixedLengthTextField();
    tfFax.setLength(60);
    
    panel.add(lblName, "alignx trailing");
    panel.add(tfName, "growx,span");
    panel.add(addressLabel, "alignx trailing");
    panel.add(new JScrollPane(tfAddress), "growx");
    panel.add(cityLabel, "alignx trailing");
    panel.add(tfCity, "growx");
    panel.add(stateLabel, "alignx trailing");
    panel.add(tfState, "growx");
    panel.add(zipLabel, "alignx trailing");
    panel.add(tfZip, "growx");
    panel.add(countryLabel, "alignx trailing");
    panel.add(tfCountry, "growx");
    panel.add(emailLabel, "alignx trailing");
    panel.add(tfEmail, "growx");
    panel.add(phoneLabel, "alignx trailing");
    panel.add(tfPhone, "growx");
    panel.add(faxLabel, "alignx trailing");
    panel.add(tfFax, "growx");
    panel.add(chkVisible, "skip 1");
    





















    add(panel, "East");
  }
  
  public void setFieldsEnable(boolean enable)
  {
    tfName.setEnabled(enable);
    tfAddress.setEnabled(enable);
    tfCity.setEnabled(enable);
    tfState.setEnabled(enable);
    tfZip.setEnabled(enable);
    tfCountry.setEnabled(enable);
    tfEmail.setEnabled(enable);
    tfPhone.setEnabled(enable);
    tfFax.setEnabled(enable);
    chkVisible.setEnabled(enable);
  }
  
  public void updateView() {
    InventoryVendor model = (InventoryVendor)getBean();
    
    if (model == null) {
      return;
    }
    
    tfName.setText(model.getName());
    tfAddress.setText(model.getAddress());
    tfCity.setText(model.getCity());
    tfCountry.setText(model.getCountry());
    tfFax.setText(model.getFax());
    tfPhone.setText(model.getPhone());
    tfState.setText(model.getState());
    tfZip.setText(model.getZip());
    tfEmail.setText(model.getEmail());
    
    if (model.getId() != null) {
      chkVisible.setSelected(POSUtil.getBoolean(model.isVisible()));
    }
  }
  
  public void createNew() {
    setBean(new InventoryVendor());
    clearFields();
  }
  
  public void clearFields()
  {
    tfName.setText("");
    tfAddress.setText("");
    tfCity.setText("");
    tfCountry.setText("");
    tfFax.setText("");
    tfPhone.setText("");
    tfState.setText("");
    tfZip.setText("");
    tfEmail.setText("");
    chkVisible.setSelected(false);
  }
  
  public boolean updateModel() {
    InventoryVendor model = (InventoryVendor)getBean();
    
    String nameString = tfName.getText();
    String addressString = tfAddress.getText();
    String cityString = tfCity.getText();
    String stateString = tfState.getText();
    String zipString = tfZip.getText();
    String countryString = tfCountry.getText();
    String emailString = tfEmail.getText();
    String phoneString = tfPhone.getText();
    String faxString = tfFax.getText();
    
    if (StringUtils.isEmpty(nameString)) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), "Name cannot be empty");
      return false;
    }
    




























    model.setName(nameString);
    model.setAddress(addressString);
    model.setCity(cityString);
    model.setState(stateString);
    model.setZip(zipString);
    model.setCountry(countryString);
    model.setEmail(emailString);
    model.setPhone(phoneString);
    model.setFax(faxString);
    
    model.setVisible(Boolean.valueOf(chkVisible.isSelected()));
    
    return true;
  }
  
  public String getDisplayText()
  {
    return "Add new vendor";
  }
  
  public boolean delete()
  {
    try
    {
      InventoryVendor inventoryVendor = (InventoryVendor)getBean();
      if (inventoryVendor == null) {
        return false;
      }
      
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), "Are you sure to delete selected item ?", "Confirm");
      if (option != 0) {
        return false;
      }
      
      InventoryVendorDAO.getInstance().delete(inventoryVendor);
      clearFields();
      return true;
    }
    catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e); }
    return false;
  }
  
  public boolean save()
  {
    try
    {
      if (!updateModel()) {
        return false;
      }
      
      InventoryVendor model = (InventoryVendor)getBean();
      InventoryVendorDAO.getInstance().saveOrUpdate(model);
      
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(e.getMessage());
    }
    
    return false;
  }
}
