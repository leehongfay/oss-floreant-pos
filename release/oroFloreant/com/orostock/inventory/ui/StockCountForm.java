package com.orostock.inventory.ui;

import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.StockCount;
import com.floreantpos.model.StockCountItem;
import com.floreantpos.model.dao.StockCountDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.BeanTableModel.BeanTableCellRenderer;
import com.floreantpos.swing.BeanTableModel.DataType;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.DateTimePicker;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.tree.DefaultMutableTreeNode;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.JXTable;





public class StockCountForm
  extends BeanEditor<StockCount>
{
  private POSTextField tfOrderId;
  private JLabel lblOrderId;
  private TitledBorder titleBorder;
  private DateTimePicker dpStockCountDate;
  private JXTable table;
  private BeanTableModel<StockCountItem> tableModel;
  private List<StockCountItem> items = new ArrayList();
  private JButton btnAddItem;
  private JButton btnDeleteItem;
  
  public StockCountForm() {
    this(new StockCount());
  }
  
  public StockCountForm(StockCount stockCount) {
    this(stockCount, false);
  }
  
  public StockCountForm(StockCount stockCount, boolean receive) {
    if (stockCount.getCountItems() != null) {
      items.addAll(stockCount.getCountItems());
    }
    initComponents();
    setBean(stockCount);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    setBorder(new EmptyBorder(10, 10, 10, 10));
    JPanel itemInfoPanel = new JPanel(new MigLayout("hidemode 3,wrap 2", "[][grow][]", ""));
    titleBorder = new TitledBorder("");
    itemInfoPanel.setBorder(titleBorder);
    add(itemInfoPanel, "North");
    
    lblOrderId = new JLabel("Ref Number.");
    itemInfoPanel.add(lblOrderId, "alignx trailing");
    
    tfOrderId = new POSTextField();
    itemInfoPanel.add(tfOrderId, "growx");
    
    dpStockCountDate = new DateTimePicker();
    dpStockCountDate.setDate(new Date());
    itemInfoPanel.add(new JLabel("Count Date"), "alignx trailing");
    itemInfoPanel.add(dpStockCountDate);
    tableModel = new BeanTableModel(StockCountItem.class);
    tableModel.addColumn("SKU", "sku");
    tableModel.addColumn("ITEM", "name");
    tableModel.addColumn("LOCATION", "inventoryLocation");
    tableModel.addColumn("UNIT", "unit");
    tableModel.addColumn("ON HAND", "unitOnHand", 11, BeanTableModel.DataType.NUMBER);
    tableModel.addColumn("COUNTED", "actualUnit", 11, BeanTableModel.DataType.NUMBER);
    tableModel.addColumn("COUNT VARIANCE", "countVariance", 11, BeanTableModel.DataType.NUMBER);
    tableModel.addColumn("COST", "cost", 11, BeanTableModel.DataType.MONEY);
    tableModel.addColumn("COST VARIANCE", "costVariance", 11, BeanTableModel.DataType.MONEY);
    tableModel.setRows(items);
    table = new JXTable(tableModel);
    BeanTableModel.BeanTableCellRenderer tableCellRenderer = new BeanTableModel.BeanTableCellRenderer();
    table.setDefaultRenderer(Object.class, tableCellRenderer);
    table.setDefaultRenderer(Number.class, tableCellRenderer);
    
    table.setRowHeight(30);
    






    table.setSelectionMode(0);
    table.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        int index = table.getSelectedRow();
        if (index < 0) {}
      }
      
    });
    add(new JScrollPane(table));
    resizeTableColumns();
    add(createSouthActionButtonPanel(), "South");
  }
  
  private JPanel createSouthActionButtonPanel() {
    JPanel southActionPanel = new JPanel(new MigLayout("fillx,hidemode 3"));
    
    btnAddItem = new JButton("Add Item");
    JButton btnEditItem = new JButton("Edit");
    btnDeleteItem = new JButton("Delete");
    
    btnAddItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        StockCountForm.this.doAddStockItem();
      }
    });
    btnEditItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        StockCountForm.this.doEditStockItem();
      }
    });
    btnDeleteItem.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        StockCountForm.this.doDeleteStockItem();
      }
    });
    southActionPanel.add(btnAddItem, "split 3,left");
    southActionPanel.add(btnEditItem);
    southActionPanel.add(btnDeleteItem);
    return southActionPanel;
  }
  
  private void resizeTableColumns() {
    table.setAutoResizeMode(4);
    setColumnWidth(0, PosUIManager.getSize(150));
    setColumnWidth(1, PosUIManager.getSize(350));
  }
  







  private void setColumnWidth(int columnNumber, int width)
  {
    TableColumn column = table.getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  private void buildTree(DefaultMutableTreeNode node) {
    InventoryLocation location = (InventoryLocation)node.getUserObject();
    List<InventoryLocation> children = null;
    
    if (location != null) {
      children = location.getChildren();
    }
    

    if (children == null) {
      return;
    }
    
    for (InventoryLocation inventoryLocation : children) {
      DefaultMutableTreeNode newChild = new DefaultMutableTreeNode(inventoryLocation);
      node.add(newChild);
      buildTree(newChild);
    }
  }
  
  public boolean save()
  {
    try {
      if (!updateModel()) {
        return false;
      }
      StockCount stockCount = (StockCount)getBean();
      StockCountDAO.getInstance().saveOrUpdate(stockCount);
      return true;
    } catch (Exception e) {
      PosLog.error(getClass(), e);
      POSMessageDialog.showError(e.getMessage());
    }
    return false;
  }
  
  public void updateView() {
    StockCount stockCount = (StockCount)getBean();
    if (stockCount.getRefNumber() == null) {
      tfOrderId.setText(String.valueOf(StockCountDAO.getInstance().getNextStockCountSequenceNumber()));
      return;
    }
    dpStockCountDate.setDate(stockCount.getCreatedDate());
    tfOrderId.setText(String.valueOf(stockCount.getRefNumber()));
  }
  
  public boolean updateModel() {
    StockCount stockCount = (StockCount)getBean();
    String refId = tfOrderId.getText();
    if (StringUtils.isEmpty(refId)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Order no cannot be empty");
      return false;
    }
    if ((items == null) || (items.isEmpty())) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Order item cannot be empty");
      return false;
    }
    stockCount.setRefNumber(refId);
    stockCount.setCreatedDate(dpStockCountDate.getDate());
    stockCount.setUser(Application.getCurrentUser());
    stockCount.setLastModifiedDate(new Date());
    if (stockCount.getCountItems() != null)
      stockCount.getCountItems().clear();
    for (StockCountItem item : items) {
      item.setStockCount(stockCount);
      stockCount.addTocountItems(item);
    }
    return true;
  }
  





































































  public String getDisplayText()
  {
    return "Stock Count Sheet";
  }
  
  private void doAddStockItem() {
    StockCountItemEntryDialog dialog = new StockCountItemEntryDialog(tableModel);
    dialog.setSize(PosUIManager.getSize(700, 450));
    dialog.open();
    if (dialog.isCanceled()) {}
  }
  
  private void doEditStockItem()
  {
    int row = table.getSelectedRow();
    if (row == -1)
      return;
    int index = table.convertRowIndexToModel(row);
    
    StockCountItem item = (StockCountItem)tableModel.getRow(index);
    if (item == null)
      return;
    StockCountItemEntryDialog dialog = new StockCountItemEntryDialog(item);
    dialog.setSize(PosUIManager.getSize(700, 450));
    dialog.open();
    if (dialog.isCanceled()) {
      return;
    }
    table.repaint();
  }
  
  private void doDeleteStockItem() {
    int row = table.getSelectedRow();
    if (row == -1)
      return;
    int index = table.convertRowIndexToModel(row);
    
    StockCountItem item = (StockCountItem)items.get(index);
    if (item == null) {
      return;
    }
    tableModel.removeRow(item);
  }
}
