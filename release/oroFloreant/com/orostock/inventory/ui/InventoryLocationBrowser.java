package com.orostock.inventory.ui;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.explorer.ExplorerButtonPanel;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.dao.InventoryLocationDAO;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.PosTableRenderer;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.tree.TreePath;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;
import org.jdesktop.swingx.treetable.TreeTableNode;



public class InventoryLocationBrowser
  extends TransparentPanel
  implements ActionListener, ListSelectionListener
{
  private static final long serialVersionUID = 1L;
  private JXTreeTable treeTable;
  private InvLocationTreeTableModel noRootTreeTableModel;
  private List<InventoryLocation> rootLocationList;
  
  public InventoryLocationBrowser()
  {
    setLayout(new BorderLayout(5, 5));
    treeTable = new JXTreeTable();
    treeTable.setRowHeight(PosUIManager.getSize(30));
    treeTable.setRootVisible(false);
    
    treeTable.setDefaultRenderer(Object.class, new PosTableRenderer());
    
    rootLocationList = InventoryLocationDAO.getInstance().getRootLocations();
    
    createTree();
    treeTable.expandAll();
    treeTable.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me)
      {
        int col = treeTable.columnAtPoint(me.getPoint());
        
        if ((me.getClickCount() == 2) && (col == 0))
        {
          treeTable.expandPath(treeTable.getPathForRow(treeTable.getSelectedRow()));
        }
        else if ((me.getClickCount() == 2) && (col != 0)) {
          InventoryLocationBrowser.this.editSelectedRow();
        }
        
      }
    });
    add(new JScrollPane(treeTable), "Center");
    
    createButtonPanel();
  }
  
  private void createTree() {
    if (rootLocationList != null) {
      InventoryLocation demo = new InventoryLocation();
      demo.setId("0");
      demo.setName("Root");
      DefaultMutableTreeTableNode rootNode = new DefaultMutableTreeTableNode(demo);
      rootNode.setUserObject(demo);
      
      for (InventoryLocation inventoryLocation : rootLocationList) {
        DefaultMutableTreeTableNode node = new DefaultMutableTreeTableNode(inventoryLocation);
        rootNode.add(node);
        buildLocationTree(node);
      }
      
      noRootTreeTableModel = new InvLocationTreeTableModel(rootNode);
      treeTable.setTreeTableModel(noRootTreeTableModel);
    }
  }
  


  private void buildLocationTree(DefaultMutableTreeTableNode defaultMutableTreeTableNode)
  {
    InventoryLocation loc = (InventoryLocation)defaultMutableTreeTableNode.getUserObject();
    if (loc == null) {
      return;
    }
    if ((loc.getChildren() == null) || (loc.getChildren().size() == 0)) {
      return;
    }
    
    defaultMutableTreeTableNode.setAllowsChildren(true);
    List<InventoryLocation> children = loc.getChildren();
    for (InventoryLocation inventoryLocation : children) {
      DefaultMutableTreeTableNode node = new DefaultMutableTreeTableNode(inventoryLocation);
      defaultMutableTreeTableNode.add(node);
      buildLocationTree(node);
    }
  }
  
  class InvLocationTreeTableModel extends DefaultTreeTableModel {
    private final String[] COLUMN_NAMES = { "Name", "Translated Name", "Address", "Parent", "Sort", "Default In", "Default Out" };
    
    public InvLocationTreeTableModel(DefaultMutableTreeTableNode rootLocation) {
      super();
    }
    
    public void setRoot(TreeTableNode root)
    {
      super.setRoot(root);
    }
    
    public int getColumnCount()
    {
      return COLUMN_NAMES.length;
    }
    
    public String getColumnName(int column)
    {
      return COLUMN_NAMES[column];
    }
    
    public boolean isCellEditable(Object node, int column)
    {
      return false;
    }
    
    public Object getValueAt(Object node, int column)
    {
      if ((node instanceof DefaultMutableTreeTableNode)) {
        InventoryLocation inventoryLocation = (InventoryLocation)((DefaultMutableTreeTableNode)node).getUserObject();
        if (inventoryLocation == null) {
          return "";
        }
        switch (column) {
        case 0: 
          return inventoryLocation.getName();
        case 1: 
          return inventoryLocation.getTranslatedName();
        case 2: 
          return inventoryLocation.getAddress();
        case 3: 
          return inventoryLocation.getParentLocation();
        case 4: 
          return inventoryLocation.getSortOrder();
        case 5: 
          return inventoryLocation.isDefaultInLocation();
        case 6: 
          return inventoryLocation.isDefaultOutLocation();
        }
        
      }
      return null;
    }
  }
  
  private void createButtonPanel() {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    
    JButton btn_new = new JButton("New");
    JButton editButton = explorerButton.getEditButton();
    JButton deleteButton = explorerButton.getDeleteButton();
    
    btn_new.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        int index = treeTable.getSelectedRow();
        InventoryLocation location = null;
        if (index >= 0) {
          TreePath treePath = treeTable.getPathForRow(index);
          DefaultMutableTreeTableNode lastPathComponent = (DefaultMutableTreeTableNode)treePath.getLastPathComponent();
          location = (InventoryLocation)lastPathComponent.getUserObject();
        }
        
        InventoryLocation inventoryLocation = new InventoryLocation();
        if (location != null) {
          inventoryLocation.setParentLocation(location);
        }
        InventoryLocationEntryForm inventoryLocationEntryForm = new InventoryLocationEntryForm(inventoryLocation);
        BeanEditorDialog dialog = new BeanEditorDialog(inventoryLocationEntryForm);
        dialog.setPreferredSize(PosUIManager.getSize(500, 600));
        dialog.open();
        if (dialog.isCanceled())
          return;
        InventoryLocation newLoc = (InventoryLocation)inventoryLocationEntryForm.getBean();
        InventoryLocation parentOfNewLoc = newLoc.getParentLocation();
        
        if (newLoc != null) {
          MutableTreeTableNode root = (MutableTreeTableNode)noRootTreeTableModel.getRoot();
          if (parentOfNewLoc != null) {
            MutableTreeTableNode parentNode = findTreeNodeForLocation(root, parentOfNewLoc.getId());
            if (parentNode != null) {
              noRootTreeTableModel.insertNodeInto(new DefaultMutableTreeTableNode(newLoc), parentNode, parentNode
                .getChildCount());
            }
          }
          else {
            MutableTreeTableNode parentNode = findTreeNodeForLocation(root, "0");
            if (parentNode != null) {
              noRootTreeTableModel.insertNodeInto(new DefaultMutableTreeTableNode(newLoc), parentNode, parentNode
                .getChildCount());
            }
          }
        }
        
        rootLocationList = InventoryLocationDAO.getInstance().getRootLocations();
        InventoryLocationBrowser.this.createTree();
        treeTable.expandAll();


















      }
      



















    });
    editButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        InventoryLocationBrowser.this.editSelectedRow();
      }
      

    });
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        deleteInventoryLocation();
      }
      
      private void deleteInventoryLocation() {
        try {
          int index = treeTable.getSelectedRow();
          if (index < 0) {
            return;
          }
          TreePath treePath = treeTable.getPathForRow(index);
          DefaultMutableTreeTableNode lastPathComponent = (DefaultMutableTreeTableNode)treePath.getLastPathComponent();
          
          InventoryLocation location = (InventoryLocation)lastPathComponent.getUserObject();
          
          if (POSMessageDialog.showYesNoQuestionDialog(getRootPane(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
            return;
          }
          
          MutableTreeTableNode tableNode = findTreeNodeForLocation((MutableTreeTableNode)noRootTreeTableModel.getRoot(), location.getId());
          if (tableNode.getParent() != null)
          {
            noRootTreeTableModel.removeNodeFromParent(tableNode);
          }
          
          InventoryLocationDAO inventoryLocationdao = new InventoryLocationDAO();
          inventoryLocationdao.delete(location);
          
          treeTable.repaint();
          treeTable.revalidate();
        }
        catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      

    });
    TransparentPanel panel = new TransparentPanel();
    
    panel.add(btn_new);
    panel.add(editButton);
    panel.add(deleteButton);
    add(panel, "South");
  }
  










  public void actionPerformed(ActionEvent e) {}
  










  public void valueChanged(ListSelectionEvent e) {}
  









  private void editSelectedRow()
  {
    try
    {
      int index = treeTable.getSelectedRow();
      if (index < 0)
        return;
      TreePath treePath = treeTable.getPathForRow(index);
      
      DefaultMutableTreeTableNode lastPathComponent = (DefaultMutableTreeTableNode)treePath.getLastPathComponent();
      
      InventoryLocation location = (InventoryLocation)lastPathComponent.getUserObject();
      
      InventoryLocationEntryForm inventoryLocationEntryForm = new InventoryLocationEntryForm(location);
      BeanEditorDialog dialog = new BeanEditorDialog(inventoryLocationEntryForm);
      dialog.setPreferredSize(PosUIManager.getSize(500, 600));
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      rootLocationList = InventoryLocationDAO.getInstance().getRootLocations();
      createTree();
      treeTable.expandAll();















    }
    catch (Throwable x)
    {















      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public MutableTreeTableNode findTreeNodeForLocation(MutableTreeTableNode locationNode, String locationId) {
    InventoryLocation location = (InventoryLocation)locationNode.getUserObject();
    


    if (locationId.equals(location.getId())) {
      return locationNode;
    }
    
    Enumeration<? extends TreeTableNode> children = locationNode.children();
    while (children.hasMoreElements()) {
      MutableTreeTableNode treeTableNode = (MutableTreeTableNode)children.nextElement();
      MutableTreeTableNode findLocById = findTreeNodeForLocation(treeTableNode, locationId);
      if (findLocById != null) {
        return findLocById;
      }
    }
    
    return null;
  }
}
