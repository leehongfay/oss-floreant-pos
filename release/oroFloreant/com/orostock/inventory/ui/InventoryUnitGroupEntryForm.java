package com.orostock.inventory.ui;

import com.floreantpos.model.InventoryUnitGroup;
import com.floreantpos.model.dao.InventoryUnitGroupDAO;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;




public class InventoryUnitGroupEntryForm
  extends BeanEditor<InventoryUnitGroup>
{
  private JCheckBox chkVisible;
  private POSTextField tfName;
  
  public InventoryUnitGroupEntryForm(InventoryUnitGroup inventoryGroup)
  {
    createUI();
    
    setBean(inventoryGroup);
  }
  
  private void createUI() {
    setLayout(new BorderLayout(0, 0));
    
    JPanel panel = new JPanel();
    add(panel);
    panel.setLayout(new MigLayout("", "[][grow]", "[][]"));
    
    JLabel lblName = new JLabel("Name");
    panel.add(lblName, "cell 0 0,alignx trailing");
    
    tfName = new POSTextField();
    panel.add(tfName, "cell 1 0,growx");
    
    chkVisible = new JCheckBox("Visible", true);
    panel.add(chkVisible, "cell 1 1");
  }
  
  public void createNew()
  {
    setBean(new InventoryUnitGroup());
    clearFields();
  }
  
  public void clearFields()
  {
    tfName.setText("");
    chkVisible.setSelected(false);
  }
  
  public void setFieldsEnable(boolean enable)
  {
    tfName.setEnabled(enable);
    chkVisible.setEnabled(enable);
  }
  
  public void updateView() {
    InventoryUnitGroup inventoryGroup = (InventoryUnitGroup)getBean();
    
    if (inventoryGroup == null) {
      clearFields();
      return;
    }
    
    tfName.setText(inventoryGroup.getName());
  }
  
  public boolean updateModel()
  {
    InventoryUnitGroup inventoryGroup = (InventoryUnitGroup)getBean();
    
    if (inventoryGroup == null) {
      inventoryGroup = new InventoryUnitGroup();
    }
    
    String nameString = tfName.getText();
    if (StringUtils.isEmpty(nameString)) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), "Name cannot be empty");
      return false;
    }
    
    inventoryGroup.setName(nameString);
    

    return true;
  }
  
  public String getDisplayText()
  {
    return "Add inventory Group";
  }
  
  public boolean save()
  {
    try
    {
      if (!updateModel()) {
        return false;
      }
      
      InventoryUnitGroup model = (InventoryUnitGroup)getBean();
      InventoryUnitGroupDAO.getInstance().saveOrUpdate(model);
      
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(e.getMessage());
    }
    
    return false;
  }
  
  public boolean delete()
  {
    try
    {
      InventoryUnitGroup inventoryGroup = (InventoryUnitGroup)getBean();
      if (inventoryGroup == null) {
        return false;
      }
      
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), "Are you sure to delete selected item ?", "Confirm");
      if (option != 0) {
        return false;
      }
      
      InventoryUnitGroupDAO.getInstance().delete(inventoryGroup);
      clearFields();
      return true;
    }
    catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e); }
    return false;
  }
}
