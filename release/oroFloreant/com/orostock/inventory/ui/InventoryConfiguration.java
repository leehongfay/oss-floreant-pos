package com.orostock.inventory.ui;

import com.floreantpos.main.Application;
import com.floreantpos.model.InventoryLocation;
import com.floreantpos.model.Store;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.dao.InventoryLocationDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.PosGuiUtil;
import com.jidesoft.swing.TitledSeparator;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.combobox.ListComboBoxModel;



























public class InventoryConfiguration
  extends TransparentPanel
{
  private static final long serialVersionUID = 1L;
  private Store store;
  private JCheckBox chkUpdateOnHandBalanceOnSale;
  private JCheckBox chkUpdateAvailBalanceOnSale;
  private JCheckBox chkAllowNegetiveOnHandBalance;
  private JRadioButton btnUpdateAvailBalanceForPurchaseOrderCreated;
  private JRadioButton btnUpdateAvailBalanceForPurchaseOrderReceived;
  private JRadioButton btnLastPurchasePrice;
  private JRadioButton btnAvgPurchasePrice;
  private JComboBox<InventoryLocation> cbInLocation = new JComboBox();
  private JComboBox<InventoryLocation> cbOutLocation = new JComboBox();
  
  public InventoryConfiguration() {
    initComponents();
    updateView();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    chkUpdateOnHandBalanceOnSale = new JCheckBox("On sale, update on hand balance");
    chkUpdateAvailBalanceOnSale = new JCheckBox("On sale, update available balance");
    chkAllowNegetiveOnHandBalance = new JCheckBox("Allow negative on hand balance");
    
    chkUpdateOnHandBalanceOnSale.setSelected(true);
    chkUpdateAvailBalanceOnSale.setSelected(true);
    
    btnUpdateAvailBalanceForPurchaseOrderCreated = new JRadioButton("Update available balance when purchase order is created");
    
    btnUpdateAvailBalanceForPurchaseOrderReceived = new JRadioButton("Update available balance when purchase order item is received");
    
    ButtonGroup btnGroup = new ButtonGroup();
    
    btnGroup.add(btnUpdateAvailBalanceForPurchaseOrderCreated);
    btnGroup.add(btnUpdateAvailBalanceForPurchaseOrderReceived);
    
    btnUpdateAvailBalanceForPurchaseOrderReceived.setSelected(true);
    
    JLabel lblCostingRule = new JLabel("Costing Rule");
    
    btnLastPurchasePrice = new JRadioButton("Last purchase price");
    btnLastPurchasePrice.setSelected(true);
    
    btnAvgPurchasePrice = new JRadioButton("Average purchase price");
    ButtonGroup buttonGroup = new ButtonGroup();
    buttonGroup.add(btnAvgPurchasePrice);
    buttonGroup.add(btnLastPurchasePrice);
    
    JPanel topPanel = new JPanel(new MigLayout(""));
    topPanel.setBorder(new TitledBorder(null, "Configuration", 2, 2, null, null));
    topPanel.add(chkUpdateOnHandBalanceOnSale, "grow, wrap");
    topPanel.add(chkUpdateAvailBalanceOnSale, "grow, wrap");
    topPanel.add(chkAllowNegetiveOnHandBalance, "grow, wrap");
    JSeparator separator = new JSeparator();
    topPanel.add(separator, "grow, wrap");
    topPanel.add(btnUpdateAvailBalanceForPurchaseOrderCreated, "grow, wrap");
    topPanel.add(btnUpdateAvailBalanceForPurchaseOrderReceived, "grow, wrap");
    
    TitledSeparator sep2 = new TitledSeparator(lblCostingRule, 10);
    topPanel.add(sep2, "grow, wrap");
    topPanel.add(btnLastPurchasePrice, "grow, wrap");
    topPanel.add(btnAvgPurchasePrice, "grow, wrap");
    
    topPanel.add(new TitledSeparator("This Terminal"), "growx");
    topPanel.add(new JLabel("Inventory in location"), "newline, split 2, alignx trailing");
    topPanel.add(cbInLocation, "growx");
    topPanel.add(new JLabel("Inventory out location"), "newline, split 2, alignx trailing");
    topPanel.add(cbOutLocation, "growx");
    
    JPanel buttonPanel = new JPanel(new MigLayout("center"));
    PosButton btnSave = new PosButton("SAVE");
    btnSave.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        if (InventoryConfiguration.this.updateModel()) {
          Terminal currentTerminal = DataProvider.get().getCurrentTerminal();
          TerminalDAO.getInstance().performBatchSave(new Object[] { store, currentTerminal });
          
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Configuration successfully updated.");
        }
        
      }
    });
    buttonPanel.add(btnSave);
    add(topPanel, "Center");
    add(buttonPanel, "South");
  }
  

  private void updateView()
  {
    store = Application.getInstance().getStore();
    
    if (store.isUpdateOnHandBlncForSale()) {
      chkUpdateOnHandBalanceOnSale.setSelected(store.isUpdateOnHandBlncForSale());
    }
    
    if (store.isUpdateAvlBlncForSale()) {
      chkUpdateAvailBalanceOnSale.setSelected(store.isUpdateAvlBlncForSale());
    }
    
    if (store.isUpdateAvlBlncForPOCreated()) {
      btnUpdateAvailBalanceForPurchaseOrderCreated.setSelected(store.isUpdateAvlBlncForPOCreated());
    }
    
    if (store.isUpdateOnHandBlncForPORec()) {
      btnUpdateAvailBalanceForPurchaseOrderReceived.setSelected(store.isUpdateOnHandBlncForPORec());
    }
    
    if (store.isAllwNegOnHandBlnce()) {
      chkAllowNegetiveOnHandBalance.setSelected(store.isAllwNegOnHandBlnce());
    }
    if (store.isInventoryAvgPricingMethod()) {
      btnAvgPurchasePrice.setSelected(true);
    }
    else {
      btnLastPurchasePrice.setSelected(true);
    }
    
    List<InventoryLocation> locations = InventoryLocationDAO.getInstance().findAll();
    List<InventoryLocation> inLocations = new ArrayList();
    inLocations.add(null);
    inLocations.addAll(locations);
    List<InventoryLocation> outLocations = new ArrayList();
    outLocations.add(null);
    outLocations.addAll(locations);
    
    cbInLocation.setModel(new ListComboBoxModel(inLocations));
    cbOutLocation.setModel(new ListComboBoxModel(inLocations));
    
    Terminal currentTerminal = DataProvider.get().getCurrentTerminal();
    PosGuiUtil.selectComboItemById(cbInLocation, currentTerminal.getInventoryInLocationId());
    PosGuiUtil.selectComboItemById(cbOutLocation, currentTerminal.getInventoryOutLocationId());
  }
  
  private boolean updateModel() {
    store = Application.getInstance().getStore();
    boolean isUpdateAvailBlncOnSaleSelected = chkUpdateAvailBalanceOnSale.isSelected();
    boolean isUpdateOnHandBlnceOnSaleSelected = chkUpdateOnHandBalanceOnSale.isSelected();
    boolean isUpdateAvailBlncePOCreatedSelected = btnUpdateAvailBalanceForPurchaseOrderCreated.isSelected();
    boolean isUpdateAvlBlnceForPOReceivedSelected = btnUpdateAvailBalanceForPurchaseOrderReceived.isSelected();
    boolean isAllowNegetiveOnHandBalance = chkAllowNegetiveOnHandBalance.isSelected();
    
    store.addProperty("inventory.updateAvlBlnceForSale", String.valueOf(isUpdateAvailBlncOnSaleSelected));
    store.addProperty("inventory.updateOnHandBlnceForSale", String.valueOf(isUpdateOnHandBlnceOnSaleSelected));
    store.addProperty("inventory.updateAvailBalanceForPurchaseOrderCreated", String.valueOf(isUpdateAvailBlncePOCreatedSelected));
    store.addProperty("inventory.updateOnHandBalanceForPurchaseOrderReceived", String.valueOf(isUpdateAvlBlnceForPOReceivedSelected));
    store.addProperty("inventory.allowNegetiveOnHandBalance", String.valueOf(isAllowNegetiveOnHandBalance));
    
    if (btnLastPurchasePrice.isSelected()) {
      store.addProperty("inventory.pricing.method", "last_purchase");
    }
    else {
      store.addProperty("inventory.pricing.method", "avg");
    }
    
    InventoryLocation inLocaton = (InventoryLocation)cbInLocation.getSelectedItem();
    InventoryLocation outLocaton = (InventoryLocation)cbOutLocation.getSelectedItem();
    
    Terminal currentTerminal = DataProvider.get().getCurrentTerminal();
    currentTerminal.setInventoryInLocationId(inLocaton == null ? null : inLocaton.getId());
    currentTerminal.setInventoryOutLocationId(outLocaton == null ? null : outLocaton.getId());
    
    return true;
  }
}
