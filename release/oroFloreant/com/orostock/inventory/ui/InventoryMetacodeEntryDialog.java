package com.orostock.inventory.ui;

import com.floreantpos.model.InventoryMetaCode;
import com.floreantpos.model.dao.InventoryMetaCodeDAO;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSBackofficeDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;

public class InventoryMetacodeEntryDialog
  extends POSBackofficeDialog
{
  private InventoryMetaCode model;
  private TitlePanel titlePanel;
  private JPanel panel_1;
  private JButton psbtnOk;
  private JButton psbtnCancel;
  private JLabel lblType;
  private JLabel lblName;
  private JLabel lblNo;
  private JLabel lblDescription;
  private POSTextField tfType;
  private POSTextField tfName;
  private IntegerTextField tfNo;
  private POSTextField tfDescription;
  
  public InventoryMetacodeEntryDialog(InventoryMetaCode ig)
  {
    model = ig;
    
    createUI();
    
    setDefaultCloseOperation(2);
    updateView();
  }
  
  private void createUI() {
    titlePanel = new TitlePanel();
    getContentPane().add(titlePanel, "North");
    
    JPanel panel = new JPanel();
    getContentPane().add(panel, "Center");
    panel.setLayout(new MigLayout("", "[][grow]", "[][][][]"));
    
    lblType = new JLabel("Type");
    panel.add(lblType, "cell 0 0,alignx trailing");
    
    tfType = new POSTextField();
    panel.add(tfType, "cell 1 0,growx");
    
    lblName = new JLabel("Name");
    panel.add(lblName, "cell 0 1,alignx trailing");
    
    tfName = new POSTextField();
    panel.add(tfName, "cell 1 1,growx");
    
    lblNo = new JLabel("No");
    panel.add(lblNo, "cell 0 2,alignx trailing");
    
    tfNo = new IntegerTextField();
    panel.add(tfNo, "cell 1 2,growx");
    
    lblDescription = new JLabel("Description");
    panel.add(lblDescription, "cell 0 3,alignx trailing");
    
    tfDescription = new POSTextField();
    panel.add(tfDescription, "cell 1 3,growx");
    
    panel_1 = new JPanel();
    getContentPane().add(panel_1, "South");
    
    psbtnOk = new JButton();
    psbtnOk.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        saveAndDispose();
      }
    });
    psbtnOk.setText("OK");
    panel_1.add(psbtnOk);
    
    psbtnCancel = new JButton();
    psbtnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
    });
    psbtnCancel.setText("CANCEL");
    panel_1.add(psbtnCancel);
  }
  
  protected void saveAndDispose() {
    try {
      updateModel();
      
      InventoryMetaCodeDAO.getInstance().saveOrUpdate(model);
      setCanceled(false);
      dispose();
    }
    catch (Exception e) {
      POSMessageDialog.showError(e.getMessage());
    }
  }
  
  private void updateView() {
    if (model == null) {
      return;
    }
    
    tfType.setText(model.getType());
    tfName.setText(model.getCodeText());
    tfNo.setText("" + model.getCodeNo());
    tfDescription.setText(model.getDescription());
  }
  
  private boolean updateModel() {
    if (model == null) {
      model = new InventoryMetaCode();
    }
    
    model.setType(tfType.getText());
    model.setCodeText(tfName.getText());
    model.setCodeNo(Integer.valueOf(tfNo.getInteger()));
    model.setDescription(tfDescription.getText());
    
    return true;
  }
  
  public void setTitle(String title) {
    titlePanel.setTitle(title);
  }
  
  public InventoryMetaCode getModel() {
    return model;
  }
}
