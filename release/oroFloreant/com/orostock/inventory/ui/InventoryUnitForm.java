package com.orostock.inventory.ui;

import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.InventoryUnitGroup;
import com.floreantpos.model.dao.InventoryUnitDAO;
import com.floreantpos.model.dao.InventoryUnitGroupDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;




public class InventoryUnitForm
  extends BeanEditor<InventoryUnit>
{
  private FixedLengthTextField tfName = new FixedLengthTextField(30);
  private FixedLengthTextField tfCode = new FixedLengthTextField(10);
  private DoubleTextField tfFactor = new DoubleTextField(10);
  
  private JComboBox cbUnitGroups = new JComboBox();
  private List<InventoryUnitGroup> groups;
  private JButton btnAddNew;
  private JCheckBox chkBaseUnit;
  
  public InventoryUnitForm() {
    this(new InventoryUnit());
  }
  
  public InventoryUnitForm(InventoryUnit unit) {
    createUI();
    setBean(unit);
    groups = InventoryUnitGroupDAO.getInstance().findAll();
    if (groups != null)
      cbUnitGroups.setModel(new DefaultComboBoxModel(groups.toArray()));
  }
  
  private void createUI() {
    setLayout(new MigLayout("fillx"));
    
    add(new JLabel("Code"));
    add(tfCode, "grow, wrap");
    
    add(new JLabel("Name"));
    add(tfName, "grow, wrap");
    
    add(new JLabel("Conversion Rate"));
    add(tfFactor, "wrap");
    
    add(new JLabel("Unit Group"));
    add(cbUnitGroups, "split 2");
    
    btnAddNew = new JButton("New");
    btnAddNew.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        String groupName = JOptionPane.showInputDialog(POSUtil.getFocusedWindow(), "Enter group name");
        if (groupName == null) {
          BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Group name cannot be empty.");
          return;
        }
        if (groupName.length() > 30) {
          BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Group name too long.");
          return;
        }
        for (InventoryUnitGroup group : groups) {
          if (group.getName().equals(groupName)) {
            BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Group name already exists.");
            return;
          }
        }
        InventoryUnitGroup group = new InventoryUnitGroup();
        group.setName(groupName);
        InventoryUnitGroupDAO.getInstance().saveOrUpdate(group);
        groups.add(group);
        cbUnitGroups.setModel(new DefaultComboBoxModel(groups.toArray()));
        cbUnitGroups.setSelectedItem(group);
      }
    });
    add(btnAddNew, "wrap");
    
    chkBaseUnit = new JCheckBox("Base Unit");
    add(chkBaseUnit, "skip 1,wrap");
  }
  
  public boolean save()
  {
    try
    {
      if (!updateModel()) {
        return false;
      }
      InventoryUnitDAO.getInstance().saveOrUpdate((InventoryUnit)getBean());
      
      return true;
    }
    catch (IllegalModelStateException e) {
      POSMessageDialog.showError(this, e.getMessage());
    }
    
    return false;
  }
  
  public void createNew()
  {
    setBean(new InventoryUnit());
    clearFields();
  }
  
  public void setFieldsEnable(boolean enable)
  {
    tfName.setEnabled(enable);
    tfCode.setEnabled(enable);
    tfFactor.setEnabled(enable);
    cbUnitGroups.setEnabled(enable);
    btnAddNew.setEnabled(enable);
    chkBaseUnit.setEnabled(enable);
  }
  
  public void clearFields()
  {
    tfName.setText("");
    tfCode.setText("");
    tfFactor.setText("");
  }
  
  protected void updateView()
  {
    InventoryUnit inventoryUnit = (InventoryUnit)getBean();
    if (inventoryUnit == null) {
      return;
    }
    
    tfName.setText(inventoryUnit.getName());
    tfCode.setText(inventoryUnit.getCode());
    tfFactor.setText(inventoryUnit.getConversionRate() + "");
    cbUnitGroups.setSelectedItem(inventoryUnit.getUnitGroupId());
    chkBaseUnit.setSelected(inventoryUnit.isBaseUnit().booleanValue());
  }
  
  protected boolean updateModel() throws IllegalModelStateException
  {
    InventoryUnit inventoryUnit = (InventoryUnit)getBean();
    String code = tfCode.getText();
    String name = tfName.getText();
    double conversionRate = tfFactor.getDouble();
    InventoryUnitGroup group = (InventoryUnitGroup)cbUnitGroups.getSelectedItem();
    
    if (StringUtils.isEmpty(code)) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), "Please enter unit name");
      return false;
    }
    
    if (InventoryUnitDAO.getInstance().nameExists(inventoryUnit, code)) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), "A unit with that name already exists");
      return false;
    }
    
    if ((Double.isNaN(conversionRate)) || (conversionRate <= 0.0D)) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), "Please make sure that factor is >= 1");
      return false;
    }
    
    if (group == null) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), "Please select a group");
      return false;
    }
    
    inventoryUnit.setName(name);
    inventoryUnit.setCode(code);
    inventoryUnit.setConversionRate(Double.valueOf(conversionRate));
    inventoryUnit.setUnitGroupId(group.getId());
    inventoryUnit.setBaseUnit(Boolean.valueOf(chkBaseUnit.isSelected()));
    
    return true;
  }
  
  public boolean delete()
  {
    try
    {
      InventoryUnit inventoryUnit = (InventoryUnit)getBean();
      if (inventoryUnit == null) {
        return false;
      }
      
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), "Are you sure to delete selected item ?", "Confirm");
      if (option != 0) {
        return false;
      }
      
      InventoryUnitDAO.getInstance().delete(inventoryUnit);
      clearFields();
      return true;
    }
    catch (Exception e) {
      POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e.getMessage(), e); }
    return false;
  }
  


  public String getDisplayText()
  {
    return "Add/Edit unit";
  }
}
