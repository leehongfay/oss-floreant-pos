package com.orostock.inventory.ui;

import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.util.DateTimePicker;
import com.floreantpos.util.POSUtil;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;



public class OrderVerificationDialog
  extends OkCancelOptionDialog
{
  DateTimePicker dpDate = new DateTimePicker();
  
  public OrderVerificationDialog() {
    super(POSUtil.getFocusedWindow());
    initComponents();
  }
  
  private void initComponents() {
    JPanel panel = getContentPanel();
    panel.setLayout(new MigLayout("center"));
    dpDate.setFormats(new String[] { "dd-MM-yyyy HH:mm:ss" });
    dpDate.setDate(new Date());
    panel.add(new JLabel("Verifaction Date:"));
    panel.add(dpDate, "grow, wrap");
  }
  
  public Date getSelectedDate() {
    return dpDate.getDate();
  }
  
  public void doOk()
  {
    setCanceled(false);
    dispose();
  }
}
