package com.orostock.inventory.ui;

import com.floreantpos.bo.ui.ModelBrowser;
import com.floreantpos.model.InventoryUnit;
import com.floreantpos.model.dao.InventoryUnitDAO;
import com.floreantpos.swing.BeanTableModel;
import java.awt.Dimension;
import java.util.List;
import javax.swing.JPanel;
import org.jdesktop.swingx.JXTable;



public class InventoryUnitsBrowser
  extends ModelBrowser<InventoryUnit>
{
  public InventoryUnitsBrowser()
  {
    super(new InventoryUnitForm());
    BeanTableModel<InventoryUnit> tableModel = new BeanTableModel(InventoryUnit.class);
    tableModel.addColumn("CODE", "code");
    tableModel.addColumn("NAME", "name");
    tableModel.addColumn("GROUP", "unitGroup");
    tableModel.addColumn("CONVERSION RATE", "conversionRate");
    tableModel.addColumn("BASE UNIT", "baseUnit");
    init(tableModel);
    browserPanel.setPreferredSize(new Dimension(700, 0));
    refreshTable();
  }
  
  public void refreshTable()
  {
    List<InventoryUnit> units = InventoryUnitDAO.getInstance().findAll();
    BeanTableModel tableModel = (BeanTableModel)browserTable.getModel();
    tableModel.removeAll();
    tableModel.addRows(units);
  }
}
