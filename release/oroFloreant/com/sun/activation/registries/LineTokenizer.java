package com.sun.activation.registries;

import java.util.NoSuchElementException;
import java.util.Vector;


















































































































































































































class LineTokenizer
{
  private int currentPosition;
  private int maxPosition;
  private String str;
  private Vector stack = new Vector();
  


  private static final String singles = "=";
  


  public LineTokenizer(String str)
  {
    currentPosition = 0;
    this.str = str;
    maxPosition = str.length();
  }
  


  private void skipWhiteSpace()
  {
    while ((currentPosition < maxPosition) && (Character.isWhitespace(str.charAt(currentPosition))))
    {
      currentPosition += 1;
    }
  }
  





  public boolean hasMoreTokens()
  {
    if (stack.size() > 0)
      return true;
    skipWhiteSpace();
    return currentPosition < maxPosition;
  }
  






  public String nextToken()
  {
    int size = stack.size();
    if (size > 0) {
      String t = (String)stack.elementAt(size - 1);
      stack.removeElementAt(size - 1);
      return t;
    }
    skipWhiteSpace();
    
    if (currentPosition >= maxPosition) {
      throw new NoSuchElementException();
    }
    
    int start = currentPosition;
    char c = str.charAt(start);
    if (c == '"') {
      currentPosition += 1;
      boolean filter = false;
      while (currentPosition < maxPosition) {
        c = str.charAt(currentPosition++);
        if (c == '\\') {
          currentPosition += 1;
          filter = true;
        } else if (c == '"') {
          String s;
          String s;
          if (filter) {
            StringBuffer sb = new StringBuffer();
            for (int i = start + 1; i < currentPosition - 1; i++) {
              c = str.charAt(i);
              if (c != '\\')
                sb.append(c);
            }
            s = sb.toString();
          } else {
            s = str.substring(start + 1, currentPosition - 1); }
          return s;
        }
      }
    } else if ("=".indexOf(c) >= 0) {
      currentPosition += 1;
    }
    else {
      while ((currentPosition < maxPosition) && ("=".indexOf(str.charAt(currentPosition)) < 0) && (!Character.isWhitespace(str.charAt(currentPosition))))
      {
        currentPosition += 1;
      }
    }
    return str.substring(start, currentPosition);
  }
  
  public void pushToken(String token) {
    stack.addElement(token);
  }
}
