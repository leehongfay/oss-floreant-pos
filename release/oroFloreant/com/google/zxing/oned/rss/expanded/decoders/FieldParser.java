package com.google.zxing.oned.rss.expanded.decoders;

import com.google.zxing.NotFoundException;































final class FieldParser
{
  private static final Object VARIABLE_LENGTH = new Object();
  
  private static final Object[][] TWO_DIGIT_DATA_LENGTH = { { "00", 
  



    Integer.valueOf(18) }, { "01", 
    Integer.valueOf(14) }, { "02", 
    Integer.valueOf(14) }, { "10", VARIABLE_LENGTH, 
    
    Integer.valueOf(20) }, { "11", 
    Integer.valueOf(6) }, { "12", 
    Integer.valueOf(6) }, { "13", 
    Integer.valueOf(6) }, { "15", 
    Integer.valueOf(6) }, { "17", 
    Integer.valueOf(6) }, { "20", 
    
    Integer.valueOf(2) }, { "21", VARIABLE_LENGTH, 
    Integer.valueOf(20) }, { "22", VARIABLE_LENGTH, 
    Integer.valueOf(29) }, { "30", VARIABLE_LENGTH, 
    
    Integer.valueOf(8) }, { "37", VARIABLE_LENGTH, 
    Integer.valueOf(8) }, { "90", VARIABLE_LENGTH, 
    

    Integer.valueOf(30) }, { "91", VARIABLE_LENGTH, 
    Integer.valueOf(30) }, { "92", VARIABLE_LENGTH, 
    Integer.valueOf(30) }, { "93", VARIABLE_LENGTH, 
    Integer.valueOf(30) }, { "94", VARIABLE_LENGTH, 
    Integer.valueOf(30) }, { "95", VARIABLE_LENGTH, 
    Integer.valueOf(30) }, { "96", VARIABLE_LENGTH, 
    Integer.valueOf(30) }, { "97", VARIABLE_LENGTH, 
    Integer.valueOf(30) }, { "98", VARIABLE_LENGTH, 
    Integer.valueOf(30) }, { "99", VARIABLE_LENGTH, 
    Integer.valueOf(30) } };
  

  private static final Object[][] THREE_DIGIT_DATA_LENGTH = { { "240", VARIABLE_LENGTH, 
  

    Integer.valueOf(30) }, { "241", VARIABLE_LENGTH, 
    Integer.valueOf(30) }, { "242", VARIABLE_LENGTH, 
    Integer.valueOf(6) }, { "250", VARIABLE_LENGTH, 
    Integer.valueOf(30) }, { "251", VARIABLE_LENGTH, 
    Integer.valueOf(30) }, { "253", VARIABLE_LENGTH, 
    Integer.valueOf(17) }, { "254", VARIABLE_LENGTH, 
    Integer.valueOf(20) }, { "400", VARIABLE_LENGTH, 
    
    Integer.valueOf(30) }, { "401", VARIABLE_LENGTH, 
    Integer.valueOf(30) }, { "402", 
    Integer.valueOf(17) }, { "403", VARIABLE_LENGTH, 
    Integer.valueOf(30) }, { "410", 
    Integer.valueOf(13) }, { "411", 
    Integer.valueOf(13) }, { "412", 
    Integer.valueOf(13) }, { "413", 
    Integer.valueOf(13) }, { "414", 
    Integer.valueOf(13) }, { "420", VARIABLE_LENGTH, 
    Integer.valueOf(20) }, { "421", VARIABLE_LENGTH, 
    Integer.valueOf(15) }, { "422", 
    Integer.valueOf(3) }, { "423", VARIABLE_LENGTH, 
    Integer.valueOf(15) }, { "424", 
    Integer.valueOf(3) }, { "425", 
    Integer.valueOf(3) }, { "426", 
    Integer.valueOf(3) } };
  

  private static final Object[][] THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH = { { "310", 
  

    Integer.valueOf(6) }, { "311", 
    Integer.valueOf(6) }, { "312", 
    Integer.valueOf(6) }, { "313", 
    Integer.valueOf(6) }, { "314", 
    Integer.valueOf(6) }, { "315", 
    Integer.valueOf(6) }, { "316", 
    Integer.valueOf(6) }, { "320", 
    Integer.valueOf(6) }, { "321", 
    Integer.valueOf(6) }, { "322", 
    Integer.valueOf(6) }, { "323", 
    Integer.valueOf(6) }, { "324", 
    Integer.valueOf(6) }, { "325", 
    Integer.valueOf(6) }, { "326", 
    Integer.valueOf(6) }, { "327", 
    Integer.valueOf(6) }, { "328", 
    Integer.valueOf(6) }, { "329", 
    Integer.valueOf(6) }, { "330", 
    Integer.valueOf(6) }, { "331", 
    Integer.valueOf(6) }, { "332", 
    Integer.valueOf(6) }, { "333", 
    Integer.valueOf(6) }, { "334", 
    Integer.valueOf(6) }, { "335", 
    Integer.valueOf(6) }, { "336", 
    Integer.valueOf(6) }, { "340", 
    Integer.valueOf(6) }, { "341", 
    Integer.valueOf(6) }, { "342", 
    Integer.valueOf(6) }, { "343", 
    Integer.valueOf(6) }, { "344", 
    Integer.valueOf(6) }, { "345", 
    Integer.valueOf(6) }, { "346", 
    Integer.valueOf(6) }, { "347", 
    Integer.valueOf(6) }, { "348", 
    Integer.valueOf(6) }, { "349", 
    Integer.valueOf(6) }, { "350", 
    Integer.valueOf(6) }, { "351", 
    Integer.valueOf(6) }, { "352", 
    Integer.valueOf(6) }, { "353", 
    Integer.valueOf(6) }, { "354", 
    Integer.valueOf(6) }, { "355", 
    Integer.valueOf(6) }, { "356", 
    Integer.valueOf(6) }, { "357", 
    Integer.valueOf(6) }, { "360", 
    Integer.valueOf(6) }, { "361", 
    Integer.valueOf(6) }, { "362", 
    Integer.valueOf(6) }, { "363", 
    Integer.valueOf(6) }, { "364", 
    Integer.valueOf(6) }, { "365", 
    Integer.valueOf(6) }, { "366", 
    Integer.valueOf(6) }, { "367", 
    Integer.valueOf(6) }, { "368", 
    Integer.valueOf(6) }, { "369", 
    Integer.valueOf(6) }, { "390", VARIABLE_LENGTH, 
    Integer.valueOf(15) }, { "391", VARIABLE_LENGTH, 
    Integer.valueOf(18) }, { "392", VARIABLE_LENGTH, 
    Integer.valueOf(15) }, { "393", VARIABLE_LENGTH, 
    Integer.valueOf(18) }, { "703", VARIABLE_LENGTH, 
    Integer.valueOf(30) } };
  

  private static final Object[][] FOUR_DIGIT_DATA_LENGTH = { { "7001", 
  

    Integer.valueOf(13) }, { "7002", VARIABLE_LENGTH, 
    Integer.valueOf(30) }, { "7003", 
    Integer.valueOf(10) }, { "8001", 
    
    Integer.valueOf(14) }, { "8002", VARIABLE_LENGTH, 
    Integer.valueOf(20) }, { "8003", VARIABLE_LENGTH, 
    Integer.valueOf(30) }, { "8004", VARIABLE_LENGTH, 
    Integer.valueOf(30) }, { "8005", 
    Integer.valueOf(6) }, { "8006", 
    Integer.valueOf(18) }, { "8007", VARIABLE_LENGTH, 
    Integer.valueOf(30) }, { "8008", VARIABLE_LENGTH, 
    Integer.valueOf(12) }, { "8018", 
    Integer.valueOf(18) }, { "8020", VARIABLE_LENGTH, 
    Integer.valueOf(25) }, { "8100", 
    Integer.valueOf(6) }, { "8101", 
    Integer.valueOf(10) }, { "8102", 
    Integer.valueOf(2) }, { "8110", VARIABLE_LENGTH, 
    Integer.valueOf(70) }, { "8200", VARIABLE_LENGTH, 
    Integer.valueOf(70) } };
  
  private FieldParser() {}
  
  static String parseFieldsInGeneralPurpose(String rawInformation)
    throws NotFoundException
  {
    if (rawInformation.isEmpty()) {
      return null;
    }
    


    if (rawInformation.length() < 2) {
      throw NotFoundException.getNotFoundInstance();
    }
    
    String firstTwoDigits = rawInformation.substring(0, 2);
    
    Object[][] arrayOfObject1 = TWO_DIGIT_DATA_LENGTH;int i = arrayOfObject1.length; for (Object[] arrayOfObject3 = 0; arrayOfObject3 < i; arrayOfObject3++) { dataLength = arrayOfObject1[arrayOfObject3];
      if (dataLength[0].equals(firstTwoDigits)) {
        if (dataLength[1] == VARIABLE_LENGTH) {
          return processVariableAI(2, ((Integer)dataLength[2]).intValue(), rawInformation);
        }
        return processFixedAI(2, ((Integer)dataLength[1]).intValue(), rawInformation);
      }
    }
    
    if (rawInformation.length() < 3) {
      throw NotFoundException.getNotFoundInstance();
    }
    
    String firstThreeDigits = rawInformation.substring(0, 3);
    
    Object[][] arrayOfObject2 = THREE_DIGIT_DATA_LENGTH;arrayOfObject3 = arrayOfObject2.length; for (Object[] dataLength = 0; dataLength < arrayOfObject3; dataLength++) { Object[] dataLength = arrayOfObject2[dataLength];
      if (dataLength[0].equals(firstThreeDigits)) {
        if (dataLength[1] == VARIABLE_LENGTH) {
          return processVariableAI(3, ((Integer)dataLength[2]).intValue(), rawInformation);
        }
        return processFixedAI(3, ((Integer)dataLength[1]).intValue(), rawInformation);
      }
    }
    

    arrayOfObject2 = THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH;Object localObject = arrayOfObject2.length; for (dataLength = 0; dataLength < localObject; dataLength++) { dataLength = arrayOfObject2[dataLength];
      if (dataLength[0].equals(firstThreeDigits)) {
        if (dataLength[1] == VARIABLE_LENGTH) {
          return processVariableAI(4, ((Integer)dataLength[2]).intValue(), rawInformation);
        }
        return processFixedAI(4, ((Integer)dataLength[1]).intValue(), rawInformation);
      }
    }
    
    if (rawInformation.length() < 4) {
      throw NotFoundException.getNotFoundInstance();
    }
    
    String firstFourDigits = rawInformation.substring(0, 4);
    
    localObject = FOUR_DIGIT_DATA_LENGTH;dataLength = localObject.length; for (Object[] dataLength = 0; dataLength < dataLength; dataLength++) { Object[] dataLength = localObject[dataLength];
      if (dataLength[0].equals(firstFourDigits)) {
        if (dataLength[1] == VARIABLE_LENGTH) {
          return processVariableAI(4, ((Integer)dataLength[2]).intValue(), rawInformation);
        }
        return processFixedAI(4, ((Integer)dataLength[1]).intValue(), rawInformation);
      }
    }
    
    throw NotFoundException.getNotFoundInstance();
  }
  
  private static String processFixedAI(int aiSize, int fieldSize, String rawInformation) throws NotFoundException {
    if (rawInformation.length() < aiSize) {
      throw NotFoundException.getNotFoundInstance();
    }
    
    String ai = rawInformation.substring(0, aiSize);
    
    if (rawInformation.length() < aiSize + fieldSize) {
      throw NotFoundException.getNotFoundInstance();
    }
    
    String field = rawInformation.substring(aiSize, aiSize + fieldSize);
    String remaining = rawInformation.substring(aiSize + fieldSize);
    String result = '(' + ai + ')' + field;
    String parsedAI = parseFieldsInGeneralPurpose(remaining);
    return result + parsedAI;
  }
  
  private static String processVariableAI(int aiSize, int variableFieldSize, String rawInformation) throws NotFoundException
  {
    String ai = rawInformation.substring(0, aiSize);
    int maxSize;
    int maxSize; if (rawInformation.length() < aiSize + variableFieldSize) {
      maxSize = rawInformation.length();
    } else {
      maxSize = aiSize + variableFieldSize;
    }
    String field = rawInformation.substring(aiSize, maxSize);
    String remaining = rawInformation.substring(maxSize);
    String result = '(' + ai + ')' + field;
    String parsedAI = parseFieldsInGeneralPurpose(remaining);
    return result + parsedAI;
  }
}
