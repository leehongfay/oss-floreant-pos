package com.google.zxing.oned.rss.expanded.decoders;











final class CurrentParsingState
{
  private int position;
  








  private State encoding;
  









  private static enum State
  {
    NUMERIC, 
    ALPHA, 
    ISO_IEC_646;
    
    private State() {} }
  
  CurrentParsingState() { position = 0;
    encoding = State.NUMERIC;
  }
  
  int getPosition() {
    return position;
  }
  
  void setPosition(int position) {
    this.position = position;
  }
  
  void incrementPosition(int delta) {
    position += delta;
  }
  
  boolean isAlpha() {
    return encoding == State.ALPHA;
  }
  
  boolean isNumeric() {
    return encoding == State.NUMERIC;
  }
  
  boolean isIsoIec646() {
    return encoding == State.ISO_IEC_646;
  }
  
  void setNumeric() {
    encoding = State.NUMERIC;
  }
  
  void setAlpha() {
    encoding = State.ALPHA;
  }
  
  void setIsoIec646() {
    encoding = State.ISO_IEC_646;
  }
}
