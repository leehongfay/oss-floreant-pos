package com.google.zxing.oned.rss.expanded.decoders;








final class DecodedInformation
  extends DecodedObject
{
  private final String newString;
  






  private final int remainingValue;
  






  private final boolean remaining;
  







  DecodedInformation(int newPosition, String newString)
  {
    super(newPosition);
    this.newString = newString;
    remaining = false;
    remainingValue = 0;
  }
  
  DecodedInformation(int newPosition, String newString, int remainingValue) {
    super(newPosition);
    remaining = true;
    this.remainingValue = remainingValue;
    this.newString = newString;
  }
  
  String getNewString() {
    return newString;
  }
  
  boolean isRemaining() {
    return remaining;
  }
  
  int getRemainingValue() {
    return remainingValue;
  }
}
