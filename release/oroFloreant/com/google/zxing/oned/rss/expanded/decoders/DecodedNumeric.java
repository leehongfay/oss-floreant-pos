package com.google.zxing.oned.rss.expanded.decoders;

import com.google.zxing.FormatException;





























final class DecodedNumeric
  extends DecodedObject
{
  private final int firstDigit;
  private final int secondDigit;
  static final int FNC1 = 10;
  
  DecodedNumeric(int newPosition, int firstDigit, int secondDigit)
    throws FormatException
  {
    super(newPosition);
    
    if ((firstDigit < 0) || (firstDigit > 10) || (secondDigit < 0) || (secondDigit > 10)) {
      throw FormatException.getFormatInstance();
    }
    
    this.firstDigit = firstDigit;
    this.secondDigit = secondDigit;
  }
  
  int getFirstDigit() {
    return firstDigit;
  }
  
  int getSecondDigit() {
    return secondDigit;
  }
  
  int getValue() {
    return firstDigit * 10 + secondDigit;
  }
  
  boolean isFirstDigitFNC1() {
    return firstDigit == 10;
  }
  
  boolean isSecondDigitFNC1() {
    return secondDigit == 10;
  }
  
  boolean isAnyFNC1() {
    return (firstDigit == 10) || (secondDigit == 10);
  }
}
