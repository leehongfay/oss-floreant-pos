package com.google.zxing.oned.rss.expanded;

import com.google.zxing.oned.rss.DataCharacter;
import com.google.zxing.oned.rss.FinderPattern;
































final class ExpandedPair
{
  private final boolean mayBeLast;
  private final DataCharacter leftChar;
  private final DataCharacter rightChar;
  private final FinderPattern finderPattern;
  
  ExpandedPair(DataCharacter leftChar, DataCharacter rightChar, FinderPattern finderPattern, boolean mayBeLast)
  {
    this.leftChar = leftChar;
    this.rightChar = rightChar;
    this.finderPattern = finderPattern;
    this.mayBeLast = mayBeLast;
  }
  
  boolean mayBeLast() {
    return mayBeLast;
  }
  
  DataCharacter getLeftChar() {
    return leftChar;
  }
  
  DataCharacter getRightChar() {
    return rightChar;
  }
  
  FinderPattern getFinderPattern() {
    return finderPattern;
  }
  
  public boolean mustBeLast() {
    return rightChar == null;
  }
  


  public String toString()
  {
    return "[ " + leftChar + " , " + rightChar + " : " + (finderPattern == null ? "null" : Integer.valueOf(finderPattern.getValue())) + " ]";
  }
  
  public boolean equals(Object o)
  {
    if (!(o instanceof ExpandedPair)) {
      return false;
    }
    ExpandedPair that = (ExpandedPair)o;
    


    return (equalsOrNull(leftChar, leftChar)) && (equalsOrNull(rightChar, rightChar)) && (equalsOrNull(finderPattern, finderPattern));
  }
  
  private static boolean equalsOrNull(Object o1, Object o2) {
    return o1 == null ? false : o2 == null ? true : o1.equals(o2);
  }
  
  public int hashCode()
  {
    return hashNotNull(leftChar) ^ hashNotNull(rightChar) ^ hashNotNull(finderPattern);
  }
  
  private static int hashNotNull(Object o) {
    return o == null ? 0 : o.hashCode();
  }
}
