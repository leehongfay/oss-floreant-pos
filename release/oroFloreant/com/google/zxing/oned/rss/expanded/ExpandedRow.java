package com.google.zxing.oned.rss.expanded;

import java.util.ArrayList;
import java.util.List;




















final class ExpandedRow
{
  private final List<ExpandedPair> pairs;
  private final int rowNumber;
  private final boolean wasReversed;
  
  ExpandedRow(List<ExpandedPair> pairs, int rowNumber, boolean wasReversed)
  {
    this.pairs = new ArrayList(pairs);
    this.rowNumber = rowNumber;
    this.wasReversed = wasReversed;
  }
  
  List<ExpandedPair> getPairs() {
    return pairs;
  }
  
  int getRowNumber() {
    return rowNumber;
  }
  
  boolean isReversed() {
    return wasReversed;
  }
  
  boolean isEquivalent(List<ExpandedPair> otherPairs) {
    return pairs.equals(otherPairs);
  }
  
  public String toString()
  {
    return "{ " + pairs + " }";
  }
  



  public boolean equals(Object o)
  {
    if (!(o instanceof ExpandedRow)) {
      return false;
    }
    ExpandedRow that = (ExpandedRow)o;
    return (pairs.equals(that.getPairs())) && (wasReversed == wasReversed);
  }
  
  public int hashCode()
  {
    return pairs.hashCode() ^ Boolean.valueOf(wasReversed).hashCode();
  }
}
