package com.google.zxing.oned.rss.expanded.decoders;











final class BlockParsedResult
{
  private final DecodedInformation decodedInformation;
  









  private final boolean finished;
  









  BlockParsedResult(boolean finished)
  {
    this(null, finished);
  }
  
  BlockParsedResult(DecodedInformation information, boolean finished) {
    this.finished = finished;
    decodedInformation = information;
  }
  
  DecodedInformation getDecodedInformation() {
    return decodedInformation;
  }
  
  boolean isFinished() {
    return finished;
  }
}
