package com.google.zxing.oned.rss.expanded.decoders;

import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.common.BitArray;































final class GeneralAppIdDecoder
{
  private final BitArray information;
  private final CurrentParsingState current = new CurrentParsingState();
  private final StringBuilder buffer = new StringBuilder();
  
  GeneralAppIdDecoder(BitArray information) {
    this.information = information;
  }
  
  String decodeAllCodes(StringBuilder buff, int initialPosition) throws NotFoundException, FormatException {
    int currentPosition = initialPosition;
    String remaining = null;
    for (;;) {
      DecodedInformation info = decodeGeneralPurposeField(currentPosition, remaining);
      String parsedFields = FieldParser.parseFieldsInGeneralPurpose(info.getNewString());
      if (parsedFields != null) {
        buff.append(parsedFields);
      }
      if (info.isRemaining()) {
        remaining = String.valueOf(info.getRemainingValue());
      } else {
        remaining = null;
      }
      
      if (currentPosition == info.getNewPosition()) {
        break;
      }
      currentPosition = info.getNewPosition();
    }
    
    return buff.toString();
  }
  

  private boolean isStillNumeric(int pos)
  {
    if (pos + 7 > information.getSize()) {
      return pos + 4 <= information.getSize();
    }
    
    for (int i = pos; i < pos + 3; i++) {
      if (information.get(i)) {
        return true;
      }
    }
    
    return information.get(pos + 3);
  }
  
  private DecodedNumeric decodeNumeric(int pos) throws FormatException {
    if (pos + 7 > information.getSize()) {
      int numeric = extractNumericValueFromBitArray(pos, 4);
      if (numeric == 0) {
        return new DecodedNumeric(information.getSize(), 10, 10);
      }
      return new DecodedNumeric(information.getSize(), numeric - 1, 10);
    }
    int numeric = extractNumericValueFromBitArray(pos, 7);
    
    int digit1 = (numeric - 8) / 11;
    int digit2 = (numeric - 8) % 11;
    
    return new DecodedNumeric(pos + 7, digit1, digit2);
  }
  
  int extractNumericValueFromBitArray(int pos, int bits) {
    return extractNumericValueFromBitArray(information, pos, bits);
  }
  
  static int extractNumericValueFromBitArray(BitArray information, int pos, int bits) {
    int value = 0;
    for (int i = 0; i < bits; i++) {
      if (information.get(pos + i)) {
        value |= 1 << bits - i - 1;
      }
    }
    
    return value;
  }
  
  DecodedInformation decodeGeneralPurposeField(int pos, String remaining) throws FormatException {
    buffer.setLength(0);
    
    if (remaining != null) {
      buffer.append(remaining);
    }
    
    current.setPosition(pos);
    
    DecodedInformation lastDecoded = parseBlocks();
    if ((lastDecoded != null) && (lastDecoded.isRemaining())) {
      return new DecodedInformation(current.getPosition(), buffer.toString(), lastDecoded.getRemainingValue());
    }
    return new DecodedInformation(current.getPosition(), buffer.toString());
  }
  
  private DecodedInformation parseBlocks() throws FormatException {
    BlockParsedResult result;
    boolean isFinished;
    boolean positionChanged;
    do { int initialPosition = current.getPosition();
      boolean isFinished;
      if (current.isAlpha()) {
        BlockParsedResult result = parseAlphaBlock();
        isFinished = result.isFinished(); } else { boolean isFinished;
        if (current.isIsoIec646()) {
          BlockParsedResult result = parseIsoIec646Block();
          isFinished = result.isFinished();
        } else {
          result = parseNumericBlock();
          isFinished = result.isFinished();
        }
      }
      positionChanged = initialPosition != current.getPosition();
    } while (((positionChanged) || (isFinished)) && 
    

      (!isFinished));
    
    return result.getDecodedInformation();
  }
  
  private BlockParsedResult parseNumericBlock() throws FormatException {
    while (isStillNumeric(current.getPosition())) {
      DecodedNumeric numeric = decodeNumeric(current.getPosition());
      current.setPosition(numeric.getNewPosition());
      
      if (numeric.isFirstDigitFNC1()) { DecodedInformation information;
        DecodedInformation information;
        if (numeric.isSecondDigitFNC1()) {
          information = new DecodedInformation(current.getPosition(), buffer.toString());
        } else {
          information = new DecodedInformation(current.getPosition(), buffer.toString(), numeric.getSecondDigit());
        }
        return new BlockParsedResult(information, true);
      }
      buffer.append(numeric.getFirstDigit());
      
      if (numeric.isSecondDigitFNC1()) {
        DecodedInformation information = new DecodedInformation(current.getPosition(), buffer.toString());
        return new BlockParsedResult(information, true);
      }
      buffer.append(numeric.getSecondDigit());
    }
    
    if (isNumericToAlphaNumericLatch(current.getPosition())) {
      current.setAlpha();
      current.incrementPosition(4);
    }
    return new BlockParsedResult(false);
  }
  
  private BlockParsedResult parseIsoIec646Block() throws FormatException {
    while (isStillIsoIec646(current.getPosition())) {
      DecodedChar iso = decodeIsoIec646(current.getPosition());
      current.setPosition(iso.getNewPosition());
      
      if (iso.isFNC1()) {
        DecodedInformation information = new DecodedInformation(current.getPosition(), buffer.toString());
        return new BlockParsedResult(information, true);
      }
      buffer.append(iso.getValue());
    }
    
    if (isAlphaOr646ToNumericLatch(current.getPosition())) {
      current.incrementPosition(3);
      current.setNumeric();
    } else if (isAlphaTo646ToAlphaLatch(current.getPosition())) {
      if (current.getPosition() + 5 < this.information.getSize()) {
        current.incrementPosition(5);
      } else {
        current.setPosition(this.information.getSize());
      }
      
      current.setAlpha();
    }
    return new BlockParsedResult(false);
  }
  
  private BlockParsedResult parseAlphaBlock() {
    while (isStillAlpha(current.getPosition())) {
      DecodedChar alpha = decodeAlphanumeric(current.getPosition());
      current.setPosition(alpha.getNewPosition());
      
      if (alpha.isFNC1()) {
        DecodedInformation information = new DecodedInformation(current.getPosition(), buffer.toString());
        return new BlockParsedResult(information, true);
      }
      
      buffer.append(alpha.getValue());
    }
    
    if (isAlphaOr646ToNumericLatch(current.getPosition())) {
      current.incrementPosition(3);
      current.setNumeric();
    } else if (isAlphaTo646ToAlphaLatch(current.getPosition())) {
      if (current.getPosition() + 5 < this.information.getSize()) {
        current.incrementPosition(5);
      } else {
        current.setPosition(this.information.getSize());
      }
      
      current.setIsoIec646();
    }
    return new BlockParsedResult(false);
  }
  
  private boolean isStillIsoIec646(int pos) {
    if (pos + 5 > information.getSize()) {
      return false;
    }
    
    int fiveBitValue = extractNumericValueFromBitArray(pos, 5);
    if ((fiveBitValue >= 5) && (fiveBitValue < 16)) {
      return true;
    }
    
    if (pos + 7 > information.getSize()) {
      return false;
    }
    
    int sevenBitValue = extractNumericValueFromBitArray(pos, 7);
    if ((sevenBitValue >= 64) && (sevenBitValue < 116)) {
      return true;
    }
    
    if (pos + 8 > information.getSize()) {
      return false;
    }
    
    int eightBitValue = extractNumericValueFromBitArray(pos, 8);
    return (eightBitValue >= 232) && (eightBitValue < 253);
  }
  
  private DecodedChar decodeIsoIec646(int pos) throws FormatException
  {
    int fiveBitValue = extractNumericValueFromBitArray(pos, 5);
    if (fiveBitValue == 15) {
      return new DecodedChar(pos + 5, '$');
    }
    
    if ((fiveBitValue >= 5) && (fiveBitValue < 15)) {
      return new DecodedChar(pos + 5, (char)(48 + fiveBitValue - 5));
    }
    
    int sevenBitValue = extractNumericValueFromBitArray(pos, 7);
    
    if ((sevenBitValue >= 64) && (sevenBitValue < 90)) {
      return new DecodedChar(pos + 7, (char)(sevenBitValue + 1));
    }
    
    if ((sevenBitValue >= 90) && (sevenBitValue < 116)) {
      return new DecodedChar(pos + 7, (char)(sevenBitValue + 7));
    }
    
    int eightBitValue = extractNumericValueFromBitArray(pos, 8);
    char c;
    char c; char c; char c; char c; char c; char c; char c; char c; char c; char c; char c; char c; char c; char c; char c; char c; char c; char c; char c; char c; switch (eightBitValue) {
    case 232: 
      c = '!';
      break;
    case 233: 
      c = '"';
      break;
    case 234: 
      c = '%';
      break;
    case 235: 
      c = '&';
      break;
    case 236: 
      c = '\'';
      break;
    case 237: 
      c = '(';
      break;
    case 238: 
      c = ')';
      break;
    case 239: 
      c = '*';
      break;
    case 240: 
      c = '+';
      break;
    case 241: 
      c = ',';
      break;
    case 242: 
      c = '-';
      break;
    case 243: 
      c = '.';
      break;
    case 244: 
      c = '/';
      break;
    case 245: 
      c = ':';
      break;
    case 246: 
      c = ';';
      break;
    case 247: 
      c = '<';
      break;
    case 248: 
      c = '=';
      break;
    case 249: 
      c = '>';
      break;
    case 250: 
      c = '?';
      break;
    case 251: 
      c = '_';
      break;
    case 252: 
      c = ' ';
      break;
    default: 
      throw FormatException.getFormatInstance(); }
    char c;
    return new DecodedChar(pos + 8, c);
  }
  
  private boolean isStillAlpha(int pos) {
    if (pos + 5 > information.getSize()) {
      return false;
    }
    

    int fiveBitValue = extractNumericValueFromBitArray(pos, 5);
    if ((fiveBitValue >= 5) && (fiveBitValue < 16)) {
      return true;
    }
    
    if (pos + 6 > information.getSize()) {
      return false;
    }
    
    int sixBitValue = extractNumericValueFromBitArray(pos, 6);
    return (sixBitValue >= 16) && (sixBitValue < 63);
  }
  
  private DecodedChar decodeAlphanumeric(int pos) {
    int fiveBitValue = extractNumericValueFromBitArray(pos, 5);
    if (fiveBitValue == 15) {
      return new DecodedChar(pos + 5, '$');
    }
    
    if ((fiveBitValue >= 5) && (fiveBitValue < 15)) {
      return new DecodedChar(pos + 5, (char)(48 + fiveBitValue - 5));
    }
    
    int sixBitValue = extractNumericValueFromBitArray(pos, 6);
    
    if ((sixBitValue >= 32) && (sixBitValue < 58))
      return new DecodedChar(pos + 6, (char)(sixBitValue + 33));
    char c;
    char c;
    char c;
    char c; char c; switch (sixBitValue) {
    case 58: 
      c = '*';
      break;
    case 59: 
      c = ',';
      break;
    case 60: 
      c = '-';
      break;
    case 61: 
      c = '.';
      break;
    case 62: 
      c = '/';
      break;
    default: 
      throw new IllegalStateException("Decoding invalid alphanumeric value: " + sixBitValue); }
    char c;
    return new DecodedChar(pos + 6, c);
  }
  
  private boolean isAlphaTo646ToAlphaLatch(int pos) {
    if (pos + 1 > information.getSize()) {
      return false;
    }
    
    for (int i = 0; (i < 5) && (i + pos < information.getSize()); i++) {
      if (i == 2) {
        if (!information.get(pos + 2)) {
          return false;
        }
      } else if (information.get(pos + i)) {
        return false;
      }
    }
    
    return true;
  }
  
  private boolean isAlphaOr646ToNumericLatch(int pos)
  {
    if (pos + 3 > information.getSize()) {
      return false;
    }
    
    for (int i = pos; i < pos + 3; i++) {
      if (information.get(i)) {
        return false;
      }
    }
    return true;
  }
  

  private boolean isNumericToAlphaNumericLatch(int pos)
  {
    if (pos + 1 > information.getSize()) {
      return false;
    }
    
    for (int i = 0; (i < 4) && (i + pos < information.getSize()); i++) {
      if (information.get(pos + i)) {
        return false;
      }
    }
    return true;
  }
}
