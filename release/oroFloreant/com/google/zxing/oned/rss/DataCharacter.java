package com.google.zxing.oned.rss;






public class DataCharacter
{
  private final int value;
  




  private final int checksumPortion;
  





  public DataCharacter(int value, int checksumPortion)
  {
    this.value = value;
    this.checksumPortion = checksumPortion;
  }
  
  public final int getValue() {
    return value;
  }
  
  public final int getChecksumPortion() {
    return checksumPortion;
  }
  
  public final String toString()
  {
    return value + "(" + checksumPortion + ')';
  }
  
  public final boolean equals(Object o)
  {
    if (!(o instanceof DataCharacter)) {
      return false;
    }
    DataCharacter that = (DataCharacter)o;
    return (value == value) && (checksumPortion == checksumPortion);
  }
  
  public final int hashCode()
  {
    return value ^ checksumPortion;
  }
}
