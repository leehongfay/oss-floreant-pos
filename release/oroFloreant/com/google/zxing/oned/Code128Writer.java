package com.google.zxing.oned;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

























public final class Code128Writer
  extends OneDimensionalCodeWriter
{
  private static final int CODE_START_B = 104;
  private static final int CODE_START_C = 105;
  private static final int CODE_CODE_B = 100;
  private static final int CODE_CODE_C = 99;
  private static final int CODE_STOP = 106;
  private static final char ESCAPE_FNC_1 = 'ñ';
  private static final char ESCAPE_FNC_2 = 'ò';
  private static final char ESCAPE_FNC_3 = 'ó';
  private static final char ESCAPE_FNC_4 = 'ô';
  private static final int CODE_FNC_1 = 102;
  private static final int CODE_FNC_2 = 97;
  private static final int CODE_FNC_3 = 96;
  private static final int CODE_FNC_4_B = 100;
  
  public Code128Writer() {}
  
  public BitMatrix encode(String contents, BarcodeFormat format, int width, int height, Map<EncodeHintType, ?> hints)
    throws WriterException
  {
    if (format != BarcodeFormat.CODE_128) {
      throw new IllegalArgumentException("Can only encode CODE_128, but got " + format);
    }
    return super.encode(contents, format, width, height, hints);
  }
  
  public boolean[] encode(String contents)
  {
    int length = contents.length();
    
    if ((length < 1) || (length > 80)) {
      throw new IllegalArgumentException("Contents length should be between 1 and 80 characters, but got " + length);
    }
    

    for (int i = 0; i < length; i++) {
      char c = contents.charAt(i);
      if ((c < ' ') || (c > '~')) {
        switch (c) {
        case 'ñ': 
        case 'ò': 
        case 'ó': 
        case 'ô': 
          break;
        default: 
          throw new IllegalArgumentException("Bad character in input: " + c);
        }
        
      }
    }
    Collection<int[]> patterns = new ArrayList();
    int checkSum = 0;
    int checkWeight = 1;
    int codeSet = 0;
    int position = 0;
    int newCodeSet;
    while (position < length)
    {
      int requiredDigitCount = codeSet == 99 ? 2 : 4;
      int newCodeSet;
      if (isDigits(contents, position, requiredDigitCount)) {
        newCodeSet = 99;
      } else {
        newCodeSet = 100;
      }
      
      int patternIndex;
      
      if (newCodeSet == codeSet) { int patternIndex;
        int patternIndex;
        int patternIndex;
        int patternIndex; switch (contents.charAt(position)) {
        case 'ñ': 
          patternIndex = 102;
          break;
        case 'ò': 
          patternIndex = 97;
          break;
        case 'ó': 
          patternIndex = 96;
          break;
        case 'ô': 
          patternIndex = 100;
          break;
        default: 
          int patternIndex;
          if (codeSet == 100) {
            patternIndex = contents.charAt(position) - ' ';
          } else {
            int patternIndex = Integer.parseInt(contents.substring(position, position + 2));
            position++;
          }
          break; }
        position++;
      }
      else {
        int patternIndex;
        if (codeSet == 0) {
          int patternIndex;
          if (newCodeSet == 100) {
            patternIndex = 104;
          }
          else {
            patternIndex = 105;
          }
        }
        else {
          patternIndex = newCodeSet;
        }
        codeSet = newCodeSet;
      }
      

      patterns.add(Code128Reader.CODE_PATTERNS[patternIndex]);
      

      checkSum += patternIndex * checkWeight;
      if (position != 0) {
        checkWeight++;
      }
    }
    

    checkSum %= 103;
    patterns.add(Code128Reader.CODE_PATTERNS[checkSum]);
    

    patterns.add(Code128Reader.CODE_PATTERNS[106]);
    

    int codeWidth = 0;
    for (int[] pattern : patterns) {
      for (int width : pattern) {
        codeWidth += width;
      }
    }
    

    boolean[] result = new boolean[codeWidth];
    int pos = 0;
    for (??? = patterns.iterator(); ((Iterator)???).hasNext();) { int[] pattern = (int[])((Iterator)???).next();
      pos += appendPattern(result, pos, pattern, true);
    }
    
    return result;
  }
  
  private static boolean isDigits(CharSequence value, int start, int length) {
    int end = start + length;
    int last = value.length();
    for (int i = start; (i < end) && (i < last); i++) {
      char c = value.charAt(i);
      if ((c < '0') || (c > '9')) {
        if (c != 'ñ') {
          return false;
        }
        end++;
      }
    }
    return end <= last;
  }
}
