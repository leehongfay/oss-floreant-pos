package com.google.zxing.qrcode.detector;

import com.google.zxing.DecodeHintType;
import com.google.zxing.NotFoundException;
import com.google.zxing.ResultPoint;
import com.google.zxing.ResultPointCallback;
import com.google.zxing.common.BitMatrix;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;































public class FinderPatternFinder
{
  private static final int CENTER_QUORUM = 2;
  protected static final int MIN_SKIP = 3;
  protected static final int MAX_MODULES = 57;
  private final BitMatrix image;
  private final List<FinderPattern> possibleCenters;
  private boolean hasSkipped;
  private final int[] crossCheckStateCount;
  private final ResultPointCallback resultPointCallback;
  
  public FinderPatternFinder(BitMatrix image)
  {
    this(image, null);
  }
  
  public FinderPatternFinder(BitMatrix image, ResultPointCallback resultPointCallback) {
    this.image = image;
    possibleCenters = new ArrayList();
    crossCheckStateCount = new int[5];
    this.resultPointCallback = resultPointCallback;
  }
  
  protected final BitMatrix getImage() {
    return image;
  }
  
  protected final List<FinderPattern> getPossibleCenters() {
    return possibleCenters;
  }
  
  final FinderPatternInfo find(Map<DecodeHintType, ?> hints) throws NotFoundException {
    boolean tryHarder = (hints != null) && (hints.containsKey(DecodeHintType.TRY_HARDER));
    boolean pureBarcode = (hints != null) && (hints.containsKey(DecodeHintType.PURE_BARCODE));
    int maxI = image.getHeight();
    int maxJ = image.getWidth();
    






    int iSkip = 3 * maxI / 228;
    if ((iSkip < 3) || (tryHarder)) {
      iSkip = 3;
    }
    
    boolean done = false;
    int[] stateCount = new int[5];
    for (int i = iSkip - 1; (i < maxI) && (!done); i += iSkip)
    {
      stateCount[0] = 0;
      stateCount[1] = 0;
      stateCount[2] = 0;
      stateCount[3] = 0;
      stateCount[4] = 0;
      int currentState = 0;
      for (int j = 0; j < maxJ; j++) {
        if (image.get(j, i))
        {
          if ((currentState & 0x1) == 1) {
            currentState++;
          }
          stateCount[currentState] += 1;
        }
        else if ((currentState & 0x1) == 0) {
          if (currentState == 4) {
            if (foundPatternCross(stateCount)) {
              boolean confirmed = handlePossibleCenter(stateCount, i, j, pureBarcode);
              if (confirmed)
              {

                iSkip = 2;
                if (hasSkipped) {
                  done = haveMultiplyConfirmedCenters();
                } else {
                  int rowSkip = findRowSkip();
                  if (rowSkip > stateCount[2])
                  {







                    i += rowSkip - stateCount[2] - iSkip;
                    j = maxJ - 1;
                  }
                }
              } else {
                stateCount[0] = stateCount[2];
                stateCount[1] = stateCount[3];
                stateCount[2] = stateCount[4];
                stateCount[3] = 1;
                stateCount[4] = 0;
                currentState = 3;
                continue;
              }
              
              currentState = 0;
              stateCount[0] = 0;
              stateCount[1] = 0;
              stateCount[2] = 0;
              stateCount[3] = 0;
              stateCount[4] = 0;
            } else {
              stateCount[0] = stateCount[2];
              stateCount[1] = stateCount[3];
              stateCount[2] = stateCount[4];
              stateCount[3] = 1;
              stateCount[4] = 0;
              currentState = 3;
            }
          } else {
            stateCount[(++currentState)] += 1;
          }
        } else {
          stateCount[currentState] += 1;
        }
      }
      
      if (foundPatternCross(stateCount)) {
        boolean confirmed = handlePossibleCenter(stateCount, i, maxJ, pureBarcode);
        if (confirmed) {
          iSkip = stateCount[0];
          if (hasSkipped)
          {
            done = haveMultiplyConfirmedCenters();
          }
        }
      }
    }
    
    FinderPattern[] patternInfo = selectBestPatterns();
    ResultPoint.orderBestPatterns(patternInfo);
    
    return new FinderPatternInfo(patternInfo);
  }
  



  private static float centerFromEnd(int[] stateCount, int end)
  {
    return end - stateCount[4] - stateCount[3] - stateCount[2] / 2.0F;
  }
  




  protected static boolean foundPatternCross(int[] stateCount)
  {
    int totalModuleSize = 0;
    for (int i = 0; i < 5; i++) {
      int count = stateCount[i];
      if (count == 0) {
        return false;
      }
      totalModuleSize += count;
    }
    if (totalModuleSize < 7) {
      return false;
    }
    float moduleSize = totalModuleSize / 7.0F;
    float maxVariance = moduleSize / 2.0F;
    





    return (Math.abs(moduleSize - stateCount[0]) < maxVariance) && (Math.abs(moduleSize - stateCount[1]) < maxVariance) && (Math.abs(3.0F * moduleSize - stateCount[2]) < 3.0F * maxVariance) && (Math.abs(moduleSize - stateCount[3]) < maxVariance) && (Math.abs(moduleSize - stateCount[4]) < maxVariance);
  }
  
  private int[] getCrossCheckStateCount() {
    crossCheckStateCount[0] = 0;
    crossCheckStateCount[1] = 0;
    crossCheckStateCount[2] = 0;
    crossCheckStateCount[3] = 0;
    crossCheckStateCount[4] = 0;
    return crossCheckStateCount;
  }
  











  private boolean crossCheckDiagonal(int startI, int centerJ, int maxCount, int originalStateCountTotal)
  {
    int[] stateCount = getCrossCheckStateCount();
    

    int i = 0;
    while ((startI >= i) && (centerJ >= i) && (image.get(centerJ - i, startI - i))) {
      stateCount[2] += 1;
      i++;
    }
    
    if ((startI < i) || (centerJ < i)) {
      return false;
    }
    

    while ((startI >= i) && (centerJ >= i) && (!image.get(centerJ - i, startI - i)) && (stateCount[1] <= maxCount))
    {
      stateCount[1] += 1;
      i++;
    }
    

    if ((startI < i) || (centerJ < i) || (stateCount[1] > maxCount)) {
      return false;
    }
    

    while ((startI >= i) && (centerJ >= i) && (image.get(centerJ - i, startI - i)) && (stateCount[0] <= maxCount))
    {
      stateCount[0] += 1;
      i++;
    }
    if (stateCount[0] > maxCount) {
      return false;
    }
    
    int maxI = image.getHeight();
    int maxJ = image.getWidth();
    

    i = 1;
    while ((startI + i < maxI) && (centerJ + i < maxJ) && (image.get(centerJ + i, startI + i))) {
      stateCount[2] += 1;
      i++;
    }
    

    if ((startI + i >= maxI) || (centerJ + i >= maxJ)) {
      return false;
    }
    
    while ((startI + i < maxI) && (centerJ + i < maxJ) && (!image.get(centerJ + i, startI + i)) && (stateCount[3] < maxCount))
    {
      stateCount[3] += 1;
      i++;
    }
    
    if ((startI + i >= maxI) || (centerJ + i >= maxJ) || (stateCount[3] >= maxCount)) {
      return false;
    }
    
    while ((startI + i < maxI) && (centerJ + i < maxJ) && (image.get(centerJ + i, startI + i)) && (stateCount[4] < maxCount))
    {
      stateCount[4] += 1;
      i++;
    }
    
    if (stateCount[4] >= maxCount) {
      return false;
    }
    


    int stateCountTotal = stateCount[0] + stateCount[1] + stateCount[2] + stateCount[3] + stateCount[4];
    

    return (Math.abs(stateCountTotal - originalStateCountTotal) < 2 * originalStateCountTotal) && (foundPatternCross(stateCount));
  }
  











  private float crossCheckVertical(int startI, int centerJ, int maxCount, int originalStateCountTotal)
  {
    BitMatrix image = this.image;
    
    int maxI = image.getHeight();
    int[] stateCount = getCrossCheckStateCount();
    

    int i = startI;
    while ((i >= 0) && (image.get(centerJ, i))) {
      stateCount[2] += 1;
      i--;
    }
    if (i < 0) {
      return NaN.0F;
    }
    while ((i >= 0) && (!image.get(centerJ, i)) && (stateCount[1] <= maxCount)) {
      stateCount[1] += 1;
      i--;
    }
    
    if ((i < 0) || (stateCount[1] > maxCount)) {
      return NaN.0F;
    }
    while ((i >= 0) && (image.get(centerJ, i)) && (stateCount[0] <= maxCount)) {
      stateCount[0] += 1;
      i--;
    }
    if (stateCount[0] > maxCount) {
      return NaN.0F;
    }
    

    i = startI + 1;
    while ((i < maxI) && (image.get(centerJ, i))) {
      stateCount[2] += 1;
      i++;
    }
    if (i == maxI) {
      return NaN.0F;
    }
    while ((i < maxI) && (!image.get(centerJ, i)) && (stateCount[3] < maxCount)) {
      stateCount[3] += 1;
      i++;
    }
    if ((i == maxI) || (stateCount[3] >= maxCount)) {
      return NaN.0F;
    }
    while ((i < maxI) && (image.get(centerJ, i)) && (stateCount[4] < maxCount)) {
      stateCount[4] += 1;
      i++;
    }
    if (stateCount[4] >= maxCount) {
      return NaN.0F;
    }
    


    int stateCountTotal = stateCount[0] + stateCount[1] + stateCount[2] + stateCount[3] + stateCount[4];
    
    if (5 * Math.abs(stateCountTotal - originalStateCountTotal) >= 2 * originalStateCountTotal) {
      return NaN.0F;
    }
    
    return foundPatternCross(stateCount) ? centerFromEnd(stateCount, i) : NaN.0F;
  }
  





  private float crossCheckHorizontal(int startJ, int centerI, int maxCount, int originalStateCountTotal)
  {
    BitMatrix image = this.image;
    
    int maxJ = image.getWidth();
    int[] stateCount = getCrossCheckStateCount();
    
    int j = startJ;
    while ((j >= 0) && (image.get(j, centerI))) {
      stateCount[2] += 1;
      j--;
    }
    if (j < 0) {
      return NaN.0F;
    }
    while ((j >= 0) && (!image.get(j, centerI)) && (stateCount[1] <= maxCount)) {
      stateCount[1] += 1;
      j--;
    }
    if ((j < 0) || (stateCount[1] > maxCount)) {
      return NaN.0F;
    }
    while ((j >= 0) && (image.get(j, centerI)) && (stateCount[0] <= maxCount)) {
      stateCount[0] += 1;
      j--;
    }
    if (stateCount[0] > maxCount) {
      return NaN.0F;
    }
    
    j = startJ + 1;
    while ((j < maxJ) && (image.get(j, centerI))) {
      stateCount[2] += 1;
      j++;
    }
    if (j == maxJ) {
      return NaN.0F;
    }
    while ((j < maxJ) && (!image.get(j, centerI)) && (stateCount[3] < maxCount)) {
      stateCount[3] += 1;
      j++;
    }
    if ((j == maxJ) || (stateCount[3] >= maxCount)) {
      return NaN.0F;
    }
    while ((j < maxJ) && (image.get(j, centerI)) && (stateCount[4] < maxCount)) {
      stateCount[4] += 1;
      j++;
    }
    if (stateCount[4] >= maxCount) {
      return NaN.0F;
    }
    


    int stateCountTotal = stateCount[0] + stateCount[1] + stateCount[2] + stateCount[3] + stateCount[4];
    
    if (5 * Math.abs(stateCountTotal - originalStateCountTotal) >= originalStateCountTotal) {
      return NaN.0F;
    }
    
    return foundPatternCross(stateCount) ? centerFromEnd(stateCount, j) : NaN.0F;
  }
  

















  protected final boolean handlePossibleCenter(int[] stateCount, int i, int j, boolean pureBarcode)
  {
    int stateCountTotal = stateCount[0] + stateCount[1] + stateCount[2] + stateCount[3] + stateCount[4];
    
    float centerJ = centerFromEnd(stateCount, j);
    float centerI = crossCheckVertical(i, (int)centerJ, stateCount[2], stateCountTotal);
    if (!Float.isNaN(centerI))
    {
      centerJ = crossCheckHorizontal((int)centerJ, (int)centerI, stateCount[2], stateCountTotal);
      if ((!Float.isNaN(centerJ)) && ((!pureBarcode) || 
        (crossCheckDiagonal((int)centerI, (int)centerJ, stateCount[2], stateCountTotal)))) {
        float estimatedModuleSize = stateCountTotal / 7.0F;
        boolean found = false;
        for (int index = 0; index < possibleCenters.size(); index++) {
          FinderPattern center = (FinderPattern)possibleCenters.get(index);
          
          if (center.aboutEquals(estimatedModuleSize, centerI, centerJ)) {
            possibleCenters.set(index, center.combineEstimate(centerI, centerJ, estimatedModuleSize));
            found = true;
            break;
          }
        }
        if (!found) {
          FinderPattern point = new FinderPattern(centerJ, centerI, estimatedModuleSize);
          possibleCenters.add(point);
          if (resultPointCallback != null) {
            resultPointCallback.foundPossibleResultPoint(point);
          }
        }
        return true;
      }
    }
    return false;
  }
  





  private int findRowSkip()
  {
    int max = possibleCenters.size();
    if (max <= 1) {
      return 0;
    }
    ResultPoint firstConfirmedCenter = null;
    for (FinderPattern center : possibleCenters) {
      if (center.getCount() >= 2) {
        if (firstConfirmedCenter == null) {
          firstConfirmedCenter = center;


        }
        else
        {

          hasSkipped = true;
          
          return (int)(Math.abs(firstConfirmedCenter.getX() - center.getX()) - Math.abs(firstConfirmedCenter.getY() - center.getY())) / 2;
        }
      }
    }
    return 0;
  }
  




  private boolean haveMultiplyConfirmedCenters()
  {
    int confirmedCount = 0;
    float totalModuleSize = 0.0F;
    int max = possibleCenters.size();
    for (FinderPattern pattern : possibleCenters) {
      if (pattern.getCount() >= 2) {
        confirmedCount++;
        totalModuleSize += pattern.getEstimatedModuleSize();
      }
    }
    if (confirmedCount < 3) {
      return false;
    }
    



    float average = totalModuleSize / max;
    float totalDeviation = 0.0F;
    for (FinderPattern pattern : possibleCenters) {
      totalDeviation += Math.abs(pattern.getEstimatedModuleSize() - average);
    }
    return totalDeviation <= 0.05F * totalModuleSize;
  }
  





  private FinderPattern[] selectBestPatterns()
    throws NotFoundException
  {
    int startSize = possibleCenters.size();
    if (startSize < 3)
    {
      throw NotFoundException.getNotFoundInstance();
    }
    
    float square;
    if (startSize > 3)
    {
      float totalModuleSize = 0.0F;
      square = 0.0F;
      for (FinderPattern center : possibleCenters) {
        float size = center.getEstimatedModuleSize();
        totalModuleSize += size;
        square += size * size;
      }
      float average = totalModuleSize / startSize;
      float stdDev = (float)Math.sqrt(square / startSize - average * average);
      
      Collections.sort(possibleCenters, new FurthestFromAverageComparator(average, null));
      
      float limit = Math.max(0.2F * average, stdDev);
      
      for (int i = 0; (i < possibleCenters.size()) && (possibleCenters.size() > 3); i++) {
        FinderPattern pattern = (FinderPattern)possibleCenters.get(i);
        if (Math.abs(pattern.getEstimatedModuleSize() - average) > limit) {
          possibleCenters.remove(i);
          i--;
        }
      }
    }
    
    if (possibleCenters.size() > 3)
    {

      float totalModuleSize = 0.0F;
      for (FinderPattern possibleCenter : possibleCenters) {
        totalModuleSize += possibleCenter.getEstimatedModuleSize();
      }
      
      float average = totalModuleSize / possibleCenters.size();
      
      Collections.sort(possibleCenters, new CenterComparator(average, null));
      
      possibleCenters.subList(3, possibleCenters.size()).clear();
    }
    



    return new FinderPattern[] {(FinderPattern)possibleCenters.get(0), (FinderPattern)possibleCenters.get(1), (FinderPattern)possibleCenters.get(2) };
  }
  
  private static final class FurthestFromAverageComparator
    implements Comparator<FinderPattern>, Serializable
  {
    private final float average;
    
    private FurthestFromAverageComparator(float f)
    {
      average = f;
    }
    
    public int compare(FinderPattern center1, FinderPattern center2) {
      float dA = Math.abs(center2.getEstimatedModuleSize() - average);
      float dB = Math.abs(center1.getEstimatedModuleSize() - average);
      return dA == dB ? 0 : dA < dB ? -1 : 1;
    }
  }
  
  private static final class CenterComparator implements Comparator<FinderPattern>, Serializable
  {
    private final float average;
    
    private CenterComparator(float f)
    {
      average = f;
    }
    
    public int compare(FinderPattern center1, FinderPattern center2) {
      if (center2.getCount() == center1.getCount()) {
        float dA = Math.abs(center2.getEstimatedModuleSize() - average);
        float dB = Math.abs(center1.getEstimatedModuleSize() - average);
        return dA == dB ? 0 : dA < dB ? 1 : -1;
      }
      return center2.getCount() - center1.getCount();
    }
  }
}
