package com.google.zxing.qrcode.decoder;

import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.ReaderException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.DecoderResult;
import com.google.zxing.common.reedsolomon.GenericGF;
import com.google.zxing.common.reedsolomon.ReedSolomonDecoder;
import com.google.zxing.common.reedsolomon.ReedSolomonException;
import java.util.Map;






















public final class Decoder
{
  private final ReedSolomonDecoder rsDecoder;
  
  public Decoder()
  {
    rsDecoder = new ReedSolomonDecoder(GenericGF.QR_CODE_FIELD_256);
  }
  
  public DecoderResult decode(boolean[][] image) throws ChecksumException, FormatException {
    return decode(image, null);
  }
  









  public DecoderResult decode(boolean[][] image, Map<DecodeHintType, ?> hints)
    throws ChecksumException, FormatException
  {
    int dimension = image.length;
    BitMatrix bits = new BitMatrix(dimension);
    for (int i = 0; i < dimension; i++) {
      for (int j = 0; j < dimension; j++) {
        if (image[i][j] != 0) {
          bits.set(j, i);
        }
      }
    }
    return decode(bits, hints);
  }
  
  public DecoderResult decode(BitMatrix bits) throws ChecksumException, FormatException {
    return decode(bits, null);
  }
  










  public DecoderResult decode(BitMatrix bits, Map<DecodeHintType, ?> hints)
    throws FormatException, ChecksumException
  {
    BitMatrixParser parser = new BitMatrixParser(bits);
    FormatException fe = null;
    ChecksumException ce = null;
    try {
      return decode(parser, hints);
    } catch (FormatException e) {
      fe = e;
    } catch (ChecksumException e) {
      ce = e;
    }
    

    try
    {
      parser.remask();
      

      parser.setMirror(true);
      

      parser.readVersion();
      

      parser.readFormatInformation();
      







      parser.mirror();
      
      DecoderResult result = decode(parser, hints);
      

      result.setOther(new QRCodeDecoderMetaData(true));
      
      return result;
    }
    catch (FormatException|ChecksumException e)
    {
      if (fe != null) {
        throw fe;
      }
      if (ce != null) {
        throw ce;
      }
      throw e;
    }
  }
  
  private DecoderResult decode(BitMatrixParser parser, Map<DecodeHintType, ?> hints)
    throws FormatException, ChecksumException
  {
    Version version = parser.readVersion();
    ErrorCorrectionLevel ecLevel = parser.readFormatInformation().getErrorCorrectionLevel();
    

    byte[] codewords = parser.readCodewords();
    
    DataBlock[] dataBlocks = DataBlock.getDataBlocks(codewords, version, ecLevel);
    

    int totalBytes = 0;
    for (dataBlock : dataBlocks) {
      totalBytes += dataBlock.getNumDataCodewords();
    }
    byte[] resultBytes = new byte[totalBytes];
    int resultOffset = 0;
    

    DataBlock[] arrayOfDataBlock2 = dataBlocks;DataBlock dataBlock = arrayOfDataBlock2.length; for (DataBlock localDataBlock1 = 0; localDataBlock1 < dataBlock; localDataBlock1++) { DataBlock dataBlock = arrayOfDataBlock2[localDataBlock1];
      byte[] codewordBytes = dataBlock.getCodewords();
      int numDataCodewords = dataBlock.getNumDataCodewords();
      correctErrors(codewordBytes, numDataCodewords);
      for (int i = 0; i < numDataCodewords; i++) {
        resultBytes[(resultOffset++)] = codewordBytes[i];
      }
    }
    

    return DecodedBitStreamParser.decode(resultBytes, version, ecLevel, hints);
  }
  






  private void correctErrors(byte[] codewordBytes, int numDataCodewords)
    throws ChecksumException
  {
    int numCodewords = codewordBytes.length;
    
    int[] codewordsInts = new int[numCodewords];
    for (int i = 0; i < numCodewords; i++) {
      codewordBytes[i] &= 0xFF;
    }
    int numECCodewords = codewordBytes.length - numDataCodewords;
    try {
      rsDecoder.decode(codewordsInts, numECCodewords);
    } catch (ReedSolomonException ignored) {
      throw ChecksumException.getChecksumInstance();
    }
    

    for (int i = 0; i < numDataCodewords; i++) {
      codewordBytes[i] = ((byte)codewordsInts[i]);
    }
  }
}
