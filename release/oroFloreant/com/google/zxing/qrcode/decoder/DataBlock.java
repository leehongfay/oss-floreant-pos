package com.google.zxing.qrcode.decoder;









final class DataBlock
{
  private final int numDataCodewords;
  






  private final byte[] codewords;
  







  private DataBlock(int numDataCodewords, byte[] codewords)
  {
    this.numDataCodewords = numDataCodewords;
    this.codewords = codewords;
  }
  













  static DataBlock[] getDataBlocks(byte[] rawCodewords, Version version, ErrorCorrectionLevel ecLevel)
  {
    if (rawCodewords.length != version.getTotalCodewords()) {
      throw new IllegalArgumentException();
    }
    


    Version.ECBlocks ecBlocks = version.getECBlocksForLevel(ecLevel);
    

    int totalBlocks = 0;
    Version.ECB[] ecBlockArray = ecBlocks.getECBlocks();
    for (ecBlock : ecBlockArray) {
      totalBlocks += ecBlock.getCount();
    }
    

    DataBlock[] result = new DataBlock[totalBlocks];
    int numResultBlocks = 0;
    Version.ECB[] arrayOfECB2 = ecBlockArray;Version.ECB ecBlock = arrayOfECB2.length; for (Version.ECB localECB1 = 0; localECB1 < ecBlock; localECB1++) { Version.ECB ecBlock = arrayOfECB2[localECB1];
      for (int i = 0; i < ecBlock.getCount(); i++) {
        int numDataCodewords = ecBlock.getDataCodewords();
        int numBlockCodewords = ecBlocks.getECCodewordsPerBlock() + numDataCodewords;
        result[(numResultBlocks++)] = new DataBlock(numDataCodewords, new byte[numBlockCodewords]);
      }
    }
    


    int shorterBlocksTotalCodewords = 0codewords.length;
    int longerBlocksStartAt = result.length - 1;
    while (longerBlocksStartAt >= 0) {
      int numCodewords = codewords.length;
      if (numCodewords == shorterBlocksTotalCodewords) {
        break;
      }
      longerBlocksStartAt--;
    }
    longerBlocksStartAt++;
    
    int shorterBlocksNumDataCodewords = shorterBlocksTotalCodewords - ecBlocks.getECCodewordsPerBlock();
    

    int rawCodewordsOffset = 0;
    for (int i = 0; i < shorterBlocksNumDataCodewords; i++) {
      for (int j = 0; j < numResultBlocks; j++) {
        codewords[i] = rawCodewords[(rawCodewordsOffset++)];
      }
    }
    
    for (int j = longerBlocksStartAt; j < numResultBlocks; j++) {
      codewords[shorterBlocksNumDataCodewords] = rawCodewords[(rawCodewordsOffset++)];
    }
    
    int max = 0codewords.length;
    for (int i = shorterBlocksNumDataCodewords; i < max; i++) {
      for (int j = 0; j < numResultBlocks; j++) {
        int iOffset = j < longerBlocksStartAt ? i : i + 1;
        codewords[iOffset] = rawCodewords[(rawCodewordsOffset++)];
      }
    }
    return result;
  }
  
  int getNumDataCodewords() {
    return numDataCodewords;
  }
  
  byte[] getCodewords() {
    return codewords;
  }
}
