package com.google.zxing.qrcode.decoder;
























public enum ErrorCorrectionLevel
{
  L(1), 
  
  M(0), 
  
  Q(3), 
  
  H(2);
  
  private static final ErrorCorrectionLevel[] FOR_BITS = { M, L, H, Q };
  private final int bits;
  
  private ErrorCorrectionLevel(int bits)
  {
    this.bits = bits;
  }
  
  public int getBits() {
    return bits;
  }
  



  public static ErrorCorrectionLevel forBits(int bits)
  {
    if ((bits < 0) || (bits >= FOR_BITS.length)) {
      throw new IllegalArgumentException();
    }
    return FOR_BITS[bits];
  }
}
