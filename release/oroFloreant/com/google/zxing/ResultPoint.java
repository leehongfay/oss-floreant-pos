package com.google.zxing;

import com.google.zxing.common.detector.MathUtils;






















public class ResultPoint
{
  private final float x;
  private final float y;
  
  public ResultPoint(float x, float y)
  {
    this.x = x;
    this.y = y;
  }
  
  public final float getX() {
    return x;
  }
  
  public final float getY() {
    return y;
  }
  
  public final boolean equals(Object other)
  {
    if ((other instanceof ResultPoint)) {
      ResultPoint otherPoint = (ResultPoint)other;
      return (x == x) && (y == y);
    }
    return false;
  }
  
  public final int hashCode()
  {
    return 31 * Float.floatToIntBits(x) + Float.floatToIntBits(y);
  }
  
  public final String toString()
  {
    StringBuilder result = new StringBuilder(25);
    result.append('(');
    result.append(x);
    result.append(',');
    result.append(y);
    result.append(')');
    return result.toString();
  }
  







  public static void orderBestPatterns(ResultPoint[] patterns)
  {
    float zeroOneDistance = distance(patterns[0], patterns[1]);
    float oneTwoDistance = distance(patterns[1], patterns[2]);
    float zeroTwoDistance = distance(patterns[0], patterns[2]);
    
    ResultPoint pointC;
    ResultPoint pointB;
    ResultPoint pointA;
    ResultPoint pointC;
    if ((oneTwoDistance >= zeroOneDistance) && (oneTwoDistance >= zeroTwoDistance)) {
      ResultPoint pointB = patterns[0];
      ResultPoint pointA = patterns[1];
      pointC = patterns[2]; } else { ResultPoint pointC;
      if ((zeroTwoDistance >= oneTwoDistance) && (zeroTwoDistance >= zeroOneDistance)) {
        ResultPoint pointB = patterns[1];
        ResultPoint pointA = patterns[0];
        pointC = patterns[2];
      } else {
        pointB = patterns[2];
        pointA = patterns[0];
        pointC = patterns[1];
      }
    }
    



    if (crossProductZ(pointA, pointB, pointC) < 0.0F) {
      ResultPoint temp = pointA;
      pointA = pointC;
      pointC = temp;
    }
    
    patterns[0] = pointA;
    patterns[1] = pointB;
    patterns[2] = pointC;
  }
  





  public static float distance(ResultPoint pattern1, ResultPoint pattern2)
  {
    return MathUtils.distance(x, y, x, y);
  }
  




  private static float crossProductZ(ResultPoint pointA, ResultPoint pointB, ResultPoint pointC)
  {
    float bX = x;
    float bY = y;
    return (x - bX) * (y - bY) - (y - bY) * (x - bX);
  }
}
