package com.google.zxing.aztec.detector;

import com.google.zxing.NotFoundException;
import com.google.zxing.ResultPoint;
import com.google.zxing.aztec.AztecDetectorResult;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.GridSampler;
import com.google.zxing.common.detector.MathUtils;
import com.google.zxing.common.detector.WhiteRectangleDetector;
import com.google.zxing.common.reedsolomon.GenericGF;
import com.google.zxing.common.reedsolomon.ReedSolomonDecoder;
import com.google.zxing.common.reedsolomon.ReedSolomonException;
























public final class Detector
{
  private final BitMatrix image;
  private boolean compact;
  private int nbLayers;
  private int nbDataBlocks;
  private int nbCenterLayers;
  private int shift;
  
  public Detector(BitMatrix image)
  {
    this.image = image;
  }
  
  public AztecDetectorResult detect() throws NotFoundException {
    return detect(false);
  }
  







  public AztecDetectorResult detect(boolean isMirror)
    throws NotFoundException
  {
    Point pCenter = getMatrixCenter();
    


    ResultPoint[] bullsEyeCorners = getBullsEyeCorners(pCenter);
    
    if (isMirror) {
      ResultPoint temp = bullsEyeCorners[0];
      bullsEyeCorners[0] = bullsEyeCorners[2];
      bullsEyeCorners[2] = temp;
    }
    

    extractParameters(bullsEyeCorners);
    

    BitMatrix bits = sampleGrid(image, bullsEyeCorners[(shift % 4)], bullsEyeCorners[((shift + 1) % 4)], bullsEyeCorners[((shift + 2) % 4)], bullsEyeCorners[((shift + 3) % 4)]);
    





    ResultPoint[] corners = getMatrixCornerPoints(bullsEyeCorners);
    
    return new AztecDetectorResult(bits, corners, compact, nbDataBlocks, nbLayers);
  }
  




  private void extractParameters(ResultPoint[] bullsEyeCorners)
    throws NotFoundException
  {
    if ((!isValid(bullsEyeCorners[0])) || (!isValid(bullsEyeCorners[1])) || 
      (!isValid(bullsEyeCorners[2])) || (!isValid(bullsEyeCorners[3]))) {
      throw NotFoundException.getNotFoundInstance();
    }
    int length = 2 * nbCenterLayers;
    




    int[] sides = {sampleLine(bullsEyeCorners[0], bullsEyeCorners[1], length), sampleLine(bullsEyeCorners[1], bullsEyeCorners[2], length), sampleLine(bullsEyeCorners[2], bullsEyeCorners[3], length), sampleLine(bullsEyeCorners[3], bullsEyeCorners[0], length) };
    





    shift = getRotation(sides, length);
    

    long parameterData = 0L;
    for (int i = 0; i < 4; i++) {
      int side = sides[((shift + i) % 4)];
      if (compact)
      {
        parameterData <<= 7;
        parameterData += (side >> 1 & 0x7F);
      }
      else {
        parameterData <<= 10;
        parameterData += (side >> 2 & 0x3E0) + (side >> 1 & 0x1F);
      }
    }
    


    int correctedData = getCorrectedParameterData(parameterData, compact);
    
    if (compact)
    {
      nbLayers = ((correctedData >> 6) + 1);
      nbDataBlocks = ((correctedData & 0x3F) + 1);
    }
    else {
      nbLayers = ((correctedData >> 11) + 1);
      nbDataBlocks = ((correctedData & 0x7FF) + 1);
    }
  }
  
  private static final int[] EXPECTED_CORNER_BITS = { 3808, 476, 2107, 1799 };
  












  private static int getRotation(int[] sides, int length)
    throws NotFoundException
  {
    int cornerBits = 0;
    for (int side : sides)
    {
      int t = (side >> length - 2 << 1) + (side & 0x1);
      cornerBits = (cornerBits << 3) + t;
    }
    


    cornerBits = ((cornerBits & 0x1) << 11) + (cornerBits >> 1);
    


    for (int shift = 0; shift < 4; shift++) {
      if (Integer.bitCount(cornerBits ^ EXPECTED_CORNER_BITS[shift]) <= 2) {
        return shift;
      }
    }
    throw NotFoundException.getNotFoundInstance();
  }
  


  private static int getCorrectedParameterData(long parameterData, boolean compact)
    throws NotFoundException
  {
    int numDataCodewords;
    
    int numCodewords;
    
    int numDataCodewords;
    
    if (compact) {
      int numCodewords = 7;
      numDataCodewords = 2;
    } else {
      numCodewords = 10;
      numDataCodewords = 4;
    }
    
    int numECCodewords = numCodewords - numDataCodewords;
    int[] parameterWords = new int[numCodewords];
    for (int i = numCodewords - 1; i >= 0; i--) {
      parameterWords[i] = ((int)parameterData & 0xF);
      parameterData >>= 4;
    }
    try {
      ReedSolomonDecoder rsDecoder = new ReedSolomonDecoder(GenericGF.AZTEC_PARAM);
      rsDecoder.decode(parameterWords, numECCodewords);
    } catch (ReedSolomonException ignored) {
      throw NotFoundException.getNotFoundInstance();
    }
    
    int result = 0;
    for (int i = 0; i < numDataCodewords; i++) {
      result = (result << 4) + parameterWords[i];
    }
    return result;
  }
  








  private ResultPoint[] getBullsEyeCorners(Point pCenter)
    throws NotFoundException
  {
    Point pina = pCenter;
    Point pinb = pCenter;
    Point pinc = pCenter;
    Point pind = pCenter;
    
    boolean color = true;
    
    for (nbCenterLayers = 1; nbCenterLayers < 9; nbCenterLayers += 1) {
      Point pouta = getFirstDifferent(pina, color, 1, -1);
      Point poutb = getFirstDifferent(pinb, color, 1, 1);
      Point poutc = getFirstDifferent(pinc, color, -1, 1);
      Point poutd = getFirstDifferent(pind, color, -1, -1);
      




      if (nbCenterLayers > 2) {
        float q = distance(poutd, pouta) * nbCenterLayers / (distance(pind, pina) * (nbCenterLayers + 2));
        if ((q < 0.75D) || (q > 1.25D) || (!isWhiteOrBlackRectangle(pouta, poutb, poutc, poutd))) {
          break;
        }
      }
      
      pina = pouta;
      pinb = poutb;
      pinc = poutc;
      pind = poutd;
      
      color = !color;
    }
    
    if ((nbCenterLayers != 5) && (nbCenterLayers != 7)) {
      throw NotFoundException.getNotFoundInstance();
    }
    
    compact = (nbCenterLayers == 5);
    


    ResultPoint pinax = new ResultPoint(pina.getX() + 0.5F, pina.getY() - 0.5F);
    ResultPoint pinbx = new ResultPoint(pinb.getX() + 0.5F, pinb.getY() + 0.5F);
    ResultPoint pincx = new ResultPoint(pinc.getX() - 0.5F, pinc.getY() + 0.5F);
    ResultPoint pindx = new ResultPoint(pind.getX() - 0.5F, pind.getY() - 0.5F);
    


    return expandSquare(new ResultPoint[] { pinax, pinbx, pincx, pindx }, 2 * nbCenterLayers - 3, 2 * nbCenterLayers);
  }
  


  private Point getMatrixCenter()
  {
    ResultPoint pointA;
    

    ResultPoint pointB;
    

    ResultPoint pointC;
    

    ResultPoint pointD;
    
    try
    {
      ResultPoint[] cornerPoints = new WhiteRectangleDetector(image).detect();
      ResultPoint pointA = cornerPoints[0];
      ResultPoint pointB = cornerPoints[1];
      ResultPoint pointC = cornerPoints[2];
      pointD = cornerPoints[3];
    }
    catch (NotFoundException e)
    {
      ResultPoint pointD;
      
      int cx = image.getWidth() / 2;
      int cy = image.getHeight() / 2;
      pointA = getFirstDifferent(new Point(cx + 7, cy - 7), false, 1, -1).toResultPoint();
      pointB = getFirstDifferent(new Point(cx + 7, cy + 7), false, 1, 1).toResultPoint();
      pointC = getFirstDifferent(new Point(cx - 7, cy + 7), false, -1, 1).toResultPoint();
      pointD = getFirstDifferent(new Point(cx - 7, cy - 7), false, -1, -1).toResultPoint();
    }
    


    int cx = MathUtils.round((pointA.getX() + pointD.getX() + pointB.getX() + pointC.getX()) / 4.0F);
    int cy = MathUtils.round((pointA.getY() + pointD.getY() + pointB.getY() + pointC.getY()) / 4.0F);
    


    try
    {
      ResultPoint[] cornerPoints = new WhiteRectangleDetector(image, 15, cx, cy).detect();
      pointA = cornerPoints[0];
      pointB = cornerPoints[1];
      pointC = cornerPoints[2];
      pointD = cornerPoints[3];
    }
    catch (NotFoundException e)
    {
      pointA = getFirstDifferent(new Point(cx + 7, cy - 7), false, 1, -1).toResultPoint();
      pointB = getFirstDifferent(new Point(cx + 7, cy + 7), false, 1, 1).toResultPoint();
      pointC = getFirstDifferent(new Point(cx - 7, cy + 7), false, -1, 1).toResultPoint();
      pointD = getFirstDifferent(new Point(cx - 7, cy - 7), false, -1, -1).toResultPoint();
    }
    

    cx = MathUtils.round((pointA.getX() + pointD.getX() + pointB.getX() + pointC.getX()) / 4.0F);
    cy = MathUtils.round((pointA.getY() + pointD.getY() + pointB.getY() + pointC.getY()) / 4.0F);
    
    return new Point(cx, cy);
  }
  





  private ResultPoint[] getMatrixCornerPoints(ResultPoint[] bullsEyeCorners)
  {
    return expandSquare(bullsEyeCorners, 2 * nbCenterLayers, getDimension());
  }
  








  private BitMatrix sampleGrid(BitMatrix image, ResultPoint topLeft, ResultPoint topRight, ResultPoint bottomRight, ResultPoint bottomLeft)
    throws NotFoundException
  {
    GridSampler sampler = GridSampler.getInstance();
    int dimension = getDimension();
    
    float low = dimension / 2.0F - nbCenterLayers;
    float high = dimension / 2.0F + nbCenterLayers;
    
    return sampler.sampleGrid(image, dimension, dimension, low, low, high, low, high, high, low, high, topLeft
    





      .getX(), topLeft.getY(), topRight
      .getX(), topRight.getY(), bottomRight
      .getX(), bottomRight.getY(), bottomLeft
      .getX(), bottomLeft.getY());
  }
  







  private int sampleLine(ResultPoint p1, ResultPoint p2, int size)
  {
    int result = 0;
    
    float d = distance(p1, p2);
    float moduleSize = d / size;
    float px = p1.getX();
    float py = p1.getY();
    float dx = moduleSize * (p2.getX() - p1.getX()) / d;
    float dy = moduleSize * (p2.getY() - p1.getY()) / d;
    for (int i = 0; i < size; i++) {
      if (image.get(MathUtils.round(px + i * dx), MathUtils.round(py + i * dy))) {
        result |= 1 << size - i - 1;
      }
    }
    return result;
  }
  







  private boolean isWhiteOrBlackRectangle(Point p1, Point p2, Point p3, Point p4)
  {
    int corr = 3;
    
    p1 = new Point(p1.getX() - corr, p1.getY() + corr);
    p2 = new Point(p2.getX() - corr, p2.getY() - corr);
    p3 = new Point(p3.getX() + corr, p3.getY() - corr);
    p4 = new Point(p4.getX() + corr, p4.getY() + corr);
    
    int cInit = getColor(p4, p1);
    
    if (cInit == 0) {
      return false;
    }
    
    int c = getColor(p1, p2);
    
    if (c != cInit) {
      return false;
    }
    
    c = getColor(p2, p3);
    
    if (c != cInit) {
      return false;
    }
    
    c = getColor(p3, p4);
    
    return c == cInit;
  }
  





  private int getColor(Point p1, Point p2)
  {
    float d = distance(p1, p2);
    float dx = (p2.getX() - p1.getX()) / d;
    float dy = (p2.getY() - p1.getY()) / d;
    int error = 0;
    
    float px = p1.getX();
    float py = p1.getY();
    
    boolean colorModel = image.get(p1.getX(), p1.getY());
    
    for (int i = 0; i < d; i++) {
      px += dx;
      py += dy;
      if (image.get(MathUtils.round(px), MathUtils.round(py)) != colorModel) {
        error++;
      }
    }
    
    float errRatio = error / d;
    
    if ((errRatio > 0.1F) && (errRatio < 0.9F)) {
      return 0;
    }
    
    return errRatio <= 0.1F == colorModel ? 1 : -1;
  }
  


  private Point getFirstDifferent(Point init, boolean color, int dx, int dy)
  {
    int x = init.getX() + dx;
    int y = init.getY() + dy;
    
    while ((isValid(x, y)) && (image.get(x, y) == color)) {
      x += dx;
      y += dy;
    }
    
    x -= dx;
    y -= dy;
    
    while ((isValid(x, y)) && (image.get(x, y) == color)) {
      x += dx;
    }
    x -= dx;
    
    while ((isValid(x, y)) && (image.get(x, y) == color)) {
      y += dy;
    }
    y -= dy;
    
    return new Point(x, y);
  }
  







  private static ResultPoint[] expandSquare(ResultPoint[] cornerPoints, float oldSide, float newSide)
  {
    float ratio = newSide / (2.0F * oldSide);
    float dx = cornerPoints[0].getX() - cornerPoints[2].getX();
    float dy = cornerPoints[0].getY() - cornerPoints[2].getY();
    float centerx = (cornerPoints[0].getX() + cornerPoints[2].getX()) / 2.0F;
    float centery = (cornerPoints[0].getY() + cornerPoints[2].getY()) / 2.0F;
    
    ResultPoint result0 = new ResultPoint(centerx + ratio * dx, centery + ratio * dy);
    ResultPoint result2 = new ResultPoint(centerx - ratio * dx, centery - ratio * dy);
    
    dx = cornerPoints[1].getX() - cornerPoints[3].getX();
    dy = cornerPoints[1].getY() - cornerPoints[3].getY();
    centerx = (cornerPoints[1].getX() + cornerPoints[3].getX()) / 2.0F;
    centery = (cornerPoints[1].getY() + cornerPoints[3].getY()) / 2.0F;
    ResultPoint result1 = new ResultPoint(centerx + ratio * dx, centery + ratio * dy);
    ResultPoint result3 = new ResultPoint(centerx - ratio * dx, centery - ratio * dy);
    
    return new ResultPoint[] { result0, result1, result2, result3 };
  }
  
  private boolean isValid(int x, int y) {
    return (x >= 0) && (x < image.getWidth()) && (y > 0) && (y < image.getHeight());
  }
  
  private boolean isValid(ResultPoint point) {
    int x = MathUtils.round(point.getX());
    int y = MathUtils.round(point.getY());
    return isValid(x, y);
  }
  
  private static float distance(Point a, Point b) {
    return MathUtils.distance(a.getX(), a.getY(), b.getX(), b.getY());
  }
  
  private static float distance(ResultPoint a, ResultPoint b) {
    return MathUtils.distance(a.getX(), a.getY(), b.getX(), b.getY());
  }
  
  private int getDimension() {
    if (compact) {
      return 4 * nbLayers + 11;
    }
    if (nbLayers <= 4) {
      return 4 * nbLayers + 15;
    }
    return 4 * nbLayers + 2 * ((nbLayers - 4) / 8 + 1) + 15;
  }
  
  static final class Point {
    private final int x;
    private final int y;
    
    ResultPoint toResultPoint() {
      return new ResultPoint(getX(), getY());
    }
    
    Point(int x, int y) {
      this.x = x;
      this.y = y;
    }
    
    int getX() {
      return x;
    }
    
    int getY() {
      return y;
    }
    
    public String toString()
    {
      return "<" + x + ' ' + y + '>';
    }
  }
}
