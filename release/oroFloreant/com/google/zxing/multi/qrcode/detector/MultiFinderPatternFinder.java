package com.google.zxing.multi.qrcode.detector;

import com.google.zxing.DecodeHintType;
import com.google.zxing.NotFoundException;
import com.google.zxing.ResultPoint;
import com.google.zxing.ResultPointCallback;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.detector.FinderPattern;
import com.google.zxing.qrcode.detector.FinderPatternFinder;
import com.google.zxing.qrcode.detector.FinderPatternInfo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;































final class MultiFinderPatternFinder
  extends FinderPatternFinder
{
  private static final FinderPatternInfo[] EMPTY_RESULT_ARRAY = new FinderPatternInfo[0];
  


  private static final float MAX_MODULE_COUNT_PER_EDGE = 180.0F;
  


  private static final float MIN_MODULE_COUNT_PER_EDGE = 9.0F;
  


  private static final float DIFF_MODSIZE_CUTOFF_PERCENT = 0.05F;
  


  private static final float DIFF_MODSIZE_CUTOFF = 0.5F;
  



  private static final class ModuleSizeComparator
    implements Comparator<FinderPattern>, Serializable
  {
    private ModuleSizeComparator() {}
    



    public int compare(FinderPattern center1, FinderPattern center2)
    {
      float value = center2.getEstimatedModuleSize() - center1.getEstimatedModuleSize();
      return value > 0.0D ? 1 : value < 0.0D ? -1 : 0;
    }
  }
  




  MultiFinderPatternFinder(BitMatrix image)
  {
    super(image);
  }
  
  MultiFinderPatternFinder(BitMatrix image, ResultPointCallback resultPointCallback) {
    super(image, resultPointCallback);
  }
  




  private FinderPattern[][] selectMutipleBestPatterns()
    throws NotFoundException
  {
    List<FinderPattern> possibleCenters = getPossibleCenters();
    int size = possibleCenters.size();
    
    if (size < 3)
    {
      throw NotFoundException.getNotFoundInstance();
    }
    



    if (size == 3)
    {



      return new FinderPattern[][] { {(FinderPattern)possibleCenters.get(0), (FinderPattern)possibleCenters.get(1), (FinderPattern)possibleCenters.get(2) } };
    }
    



    Collections.sort(possibleCenters, new ModuleSizeComparator(null));
    















    List<FinderPattern[]> results = new ArrayList();
    
    for (int i1 = 0; i1 < size - 2; i1++) {
      FinderPattern p1 = (FinderPattern)possibleCenters.get(i1);
      if (p1 != null)
      {


        for (int i2 = i1 + 1; i2 < size - 1; i2++) {
          FinderPattern p2 = (FinderPattern)possibleCenters.get(i2);
          if (p2 != null)
          {




            float vModSize12 = (p1.getEstimatedModuleSize() - p2.getEstimatedModuleSize()) / Math.min(p1.getEstimatedModuleSize(), p2.getEstimatedModuleSize());
            float vModSize12A = Math.abs(p1.getEstimatedModuleSize() - p2.getEstimatedModuleSize());
            if ((vModSize12A > 0.5F) && (vModSize12 >= 0.05F)) {
              break;
            }
            


            for (int i3 = i2 + 1; i3 < size; i3++) {
              FinderPattern p3 = (FinderPattern)possibleCenters.get(i3);
              if (p3 != null)
              {




                float vModSize23 = (p2.getEstimatedModuleSize() - p3.getEstimatedModuleSize()) / Math.min(p2.getEstimatedModuleSize(), p3.getEstimatedModuleSize());
                float vModSize23A = Math.abs(p2.getEstimatedModuleSize() - p3.getEstimatedModuleSize());
                if ((vModSize23A > 0.5F) && (vModSize23 >= 0.05F)) {
                  break;
                }
                


                FinderPattern[] test = { p1, p2, p3 };
                ResultPoint.orderBestPatterns(test);
                

                FinderPatternInfo info = new FinderPatternInfo(test);
                float dA = ResultPoint.distance(info.getTopLeft(), info.getBottomLeft());
                float dC = ResultPoint.distance(info.getTopRight(), info.getBottomLeft());
                float dB = ResultPoint.distance(info.getTopLeft(), info.getTopRight());
                

                float estimatedModuleCount = (dA + dB) / (p1.getEstimatedModuleSize() * 2.0F);
                if ((estimatedModuleCount <= 180.0F) && (estimatedModuleCount >= 9.0F))
                {




                  float vABBC = Math.abs((dA - dB) / Math.min(dA, dB));
                  if (vABBC < 0.1F)
                  {



                    float dCpy = (float)Math.sqrt(dA * dA + dB * dB);
                    
                    float vPyC = Math.abs((dC - dCpy) / Math.min(dC, dCpy));
                    
                    if (vPyC < 0.1F)
                    {



                      results.add(test); }
                  }
                }
              }
            } } } } }
    if (!results.isEmpty()) {
      return (FinderPattern[][])results.toArray(new FinderPattern[results.size()][]);
    }
    

    throw NotFoundException.getNotFoundInstance();
  }
  
  public FinderPatternInfo[] findMulti(Map<DecodeHintType, ?> hints) throws NotFoundException {
    boolean tryHarder = (hints != null) && (hints.containsKey(DecodeHintType.TRY_HARDER));
    boolean pureBarcode = (hints != null) && (hints.containsKey(DecodeHintType.PURE_BARCODE));
    BitMatrix image = getImage();
    int maxI = image.getHeight();
    int maxJ = image.getWidth();
    






    int iSkip = (int)(maxI / 228.0F * 3.0F);
    if ((iSkip < 3) || (tryHarder)) {
      iSkip = 3;
    }
    
    int[] stateCount = new int[5];
    int j; for (int i = iSkip - 1; i < maxI; i += iSkip)
    {
      stateCount[0] = 0;
      stateCount[1] = 0;
      stateCount[2] = 0;
      stateCount[3] = 0;
      stateCount[4] = 0;
      int currentState = 0;
      for (j = 0; j < maxJ; j++) {
        if (image.get(j, i))
        {
          if ((currentState & 0x1) == 1) {
            currentState++;
          }
          stateCount[currentState] += 1;
        }
        else if ((currentState & 0x1) == 0) {
          if (currentState == 4) {
            if ((foundPatternCross(stateCount)) && (handlePossibleCenter(stateCount, i, j, pureBarcode)))
            {
              currentState = 0;
              stateCount[0] = 0;
              stateCount[1] = 0;
              stateCount[2] = 0;
              stateCount[3] = 0;
              stateCount[4] = 0;
            } else {
              stateCount[0] = stateCount[2];
              stateCount[1] = stateCount[3];
              stateCount[2] = stateCount[4];
              stateCount[3] = 1;
              stateCount[4] = 0;
              currentState = 3;
            }
          } else {
            stateCount[(++currentState)] += 1;
          }
        } else {
          stateCount[currentState] += 1;
        }
      }
      

      if (foundPatternCross(stateCount)) {
        handlePossibleCenter(stateCount, i, maxJ, pureBarcode);
      }
    }
    FinderPattern[][] patternInfo = selectMutipleBestPatterns();
    List<FinderPatternInfo> result = new ArrayList();
    for (FinderPattern[] pattern : patternInfo) {
      ResultPoint.orderBestPatterns(pattern);
      result.add(new FinderPatternInfo(pattern));
    }
    
    if (result.isEmpty()) {
      return EMPTY_RESULT_ARRAY;
    }
    return (FinderPatternInfo[])result.toArray(new FinderPatternInfo[result.size()]);
  }
}
