package com.google.zxing.common;

import com.google.zxing.Binarizer;
import com.google.zxing.LuminanceSource;
import com.google.zxing.NotFoundException;



























public class GlobalHistogramBinarizer
  extends Binarizer
{
  private static final int LUMINANCE_BITS = 5;
  private static final int LUMINANCE_SHIFT = 3;
  private static final int LUMINANCE_BUCKETS = 32;
  private static final byte[] EMPTY = new byte[0];
  private byte[] luminances;
  private final int[] buckets;
  
  public GlobalHistogramBinarizer(LuminanceSource source)
  {
    super(source);
    luminances = EMPTY;
    buckets = new int[32];
  }
  
  public BitArray getBlackRow(int y, BitArray row)
    throws NotFoundException
  {
    LuminanceSource source = getLuminanceSource();
    int width = source.getWidth();
    if ((row == null) || (row.getSize() < width)) {
      row = new BitArray(width);
    } else {
      row.clear();
    }
    
    initArrays(width);
    byte[] localLuminances = source.getRow(y, luminances);
    int[] localBuckets = buckets;
    for (int x = 0; x < width; x++) {
      int pixel = localLuminances[x] & 0xFF;
      localBuckets[(pixel >> 3)] += 1;
    }
    int blackPoint = estimateBlackPoint(localBuckets);
    
    int left = localLuminances[0] & 0xFF;
    int center = localLuminances[1] & 0xFF;
    for (int x = 1; x < width - 1; x++) {
      int right = localLuminances[(x + 1)] & 0xFF;
      
      int luminance = (center * 4 - left - right) / 2;
      if (luminance < blackPoint) {
        row.set(x);
      }
      left = center;
      center = right;
    }
    return row;
  }
  
  public BitMatrix getBlackMatrix()
    throws NotFoundException
  {
    LuminanceSource source = getLuminanceSource();
    int width = source.getWidth();
    int height = source.getHeight();
    BitMatrix matrix = new BitMatrix(width, height);
    


    initArrays(width);
    int[] localBuckets = buckets;
    for (int y = 1; y < 5; y++) {
      int row = height * y / 5;
      byte[] localLuminances = source.getRow(row, luminances);
      int right = width * 4 / 5;
      for (int x = width / 5; x < right; x++) {
        int pixel = localLuminances[x] & 0xFF;
        localBuckets[(pixel >> 3)] += 1;
      }
    }
    int blackPoint = estimateBlackPoint(localBuckets);
    



    byte[] localLuminances = source.getMatrix();
    for (int y = 0; y < height; y++) {
      int offset = y * width;
      for (int x = 0; x < width; x++) {
        int pixel = localLuminances[(offset + x)] & 0xFF;
        if (pixel < blackPoint) {
          matrix.set(x, y);
        }
      }
    }
    
    return matrix;
  }
  
  public Binarizer createBinarizer(LuminanceSource source)
  {
    return new GlobalHistogramBinarizer(source);
  }
  
  private void initArrays(int luminanceSize) {
    if (luminances.length < luminanceSize) {
      luminances = new byte[luminanceSize];
    }
    for (int x = 0; x < 32; x++) {
      buckets[x] = 0;
    }
  }
  
  private static int estimateBlackPoint(int[] buckets) throws NotFoundException
  {
    int numBuckets = buckets.length;
    int maxBucketCount = 0;
    int firstPeak = 0;
    int firstPeakSize = 0;
    for (int x = 0; x < numBuckets; x++) {
      if (buckets[x] > firstPeakSize) {
        firstPeak = x;
        firstPeakSize = buckets[x];
      }
      if (buckets[x] > maxBucketCount) {
        maxBucketCount = buckets[x];
      }
    }
    

    int secondPeak = 0;
    int secondPeakScore = 0;
    for (int x = 0; x < numBuckets; x++) {
      int distanceToBiggest = x - firstPeak;
      
      int score = buckets[x] * distanceToBiggest * distanceToBiggest;
      if (score > secondPeakScore) {
        secondPeak = x;
        secondPeakScore = score;
      }
    }
    

    if (firstPeak > secondPeak) {
      int temp = firstPeak;
      firstPeak = secondPeak;
      secondPeak = temp;
    }
    


    if (secondPeak - firstPeak <= numBuckets / 16) {
      throw NotFoundException.getNotFoundInstance();
    }
    

    int bestValley = secondPeak - 1;
    int bestValleyScore = -1;
    for (int x = secondPeak - 1; x > firstPeak; x--) {
      int fromFirst = x - firstPeak;
      int score = fromFirst * fromFirst * (secondPeak - x) * (maxBucketCount - buckets[x]);
      if (score > bestValleyScore) {
        bestValley = x;
        bestValleyScore = score;
      }
    }
    
    return bestValley << 3;
  }
}
