package com.google.zxing.common.detector;

import com.google.zxing.NotFoundException;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitMatrix;
























public final class MonochromeRectangleDetector
{
  private static final int MAX_MODULES = 32;
  private final BitMatrix image;
  
  public MonochromeRectangleDetector(BitMatrix image)
  {
    this.image = image;
  }
  








  public ResultPoint[] detect()
    throws NotFoundException
  {
    int height = image.getHeight();
    int width = image.getWidth();
    int halfHeight = height / 2;
    int halfWidth = width / 2;
    int deltaY = Math.max(1, height / 256);
    int deltaX = Math.max(1, width / 256);
    
    int top = 0;
    int bottom = height;
    int left = 0;
    int right = width;
    ResultPoint pointA = findCornerFromCenter(halfWidth, 0, left, right, halfHeight, -deltaY, top, bottom, halfWidth / 2);
    
    top = (int)pointA.getY() - 1;
    ResultPoint pointB = findCornerFromCenter(halfWidth, -deltaX, left, right, halfHeight, 0, top, bottom, halfHeight / 2);
    
    left = (int)pointB.getX() - 1;
    ResultPoint pointC = findCornerFromCenter(halfWidth, deltaX, left, right, halfHeight, 0, top, bottom, halfHeight / 2);
    
    right = (int)pointC.getX() + 1;
    ResultPoint pointD = findCornerFromCenter(halfWidth, 0, left, right, halfHeight, deltaY, top, bottom, halfWidth / 2);
    
    bottom = (int)pointD.getY() + 1;
    

    pointA = findCornerFromCenter(halfWidth, 0, left, right, halfHeight, -deltaY, top, bottom, halfWidth / 4);
    

    return new ResultPoint[] { pointA, pointB, pointC, pointD };
  }
  
























  private ResultPoint findCornerFromCenter(int centerX, int deltaX, int left, int right, int centerY, int deltaY, int top, int bottom, int maxWhiteRun)
    throws NotFoundException
  {
    int[] lastRange = null;
    int y = centerY;int x = centerX;
    for (; (y < bottom) && (y >= top) && (x < right) && (x >= left); 
        x += deltaX) { int[] range;
      int[] range;
      if (deltaX == 0)
      {
        range = blackWhiteRange(y, maxWhiteRun, left, right, true);
      }
      else {
        range = blackWhiteRange(x, maxWhiteRun, top, bottom, false);
      }
      if (range == null) {
        if (lastRange == null) {
          throw NotFoundException.getNotFoundInstance();
        }
        
        if (deltaX == 0) {
          int lastY = y - deltaY;
          if (lastRange[0] < centerX) {
            if (lastRange[1] > centerX)
            {
              return new ResultPoint(deltaY > 0 ? lastRange[0] : lastRange[1], lastY);
            }
            return new ResultPoint(lastRange[0], lastY);
          }
          return new ResultPoint(lastRange[1], lastY);
        }
        
        int lastX = x - deltaX;
        if (lastRange[0] < centerY) {
          if (lastRange[1] > centerY) {
            return new ResultPoint(lastX, deltaX < 0 ? lastRange[0] : lastRange[1]);
          }
          return new ResultPoint(lastX, lastRange[0]);
        }
        return new ResultPoint(lastX, lastRange[1]);
      }
      

      lastRange = range;y += deltaY;
    }
    throw NotFoundException.getNotFoundInstance();
  }
  














  private int[] blackWhiteRange(int fixedDimension, int maxWhiteRun, int minDim, int maxDim, boolean horizontal)
  {
    int center = (minDim + maxDim) / 2;
    

    int start = center;
    while (start >= minDim) {
      if (horizontal ? image.get(start, fixedDimension) : image.get(fixedDimension, start)) {
        start--;
      } else {
        int whiteRunStart = start;
        do {
          start--;
        } while ((start >= minDim) && (horizontal ? !image.get(start, fixedDimension) : 
          !image.get(fixedDimension, start)));
        int whiteRunSize = whiteRunStart - start;
        if ((start < minDim) || (whiteRunSize > maxWhiteRun)) {
          start = whiteRunStart;
          break;
        }
      }
    }
    start++;
    

    int end = center;
    while (end < maxDim) {
      if (horizontal ? image.get(end, fixedDimension) : image.get(fixedDimension, end)) {
        end++;
      } else {
        int whiteRunStart = end;
        do {
          end++;
        } while ((end < maxDim) && (horizontal ? !image.get(end, fixedDimension) : 
          !image.get(fixedDimension, end)));
        int whiteRunSize = end - whiteRunStart;
        if ((end >= maxDim) || (whiteRunSize > maxWhiteRun)) {
          end = whiteRunStart;
          break;
        }
      }
    }
    end--;
    
    return end > start ? new int[] { start, end } : null;
  }
}
