package com.google.zxing.common.detector;

import com.google.zxing.NotFoundException;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitMatrix;


























public final class WhiteRectangleDetector
{
  private static final int INIT_SIZE = 10;
  private static final int CORR = 1;
  private final BitMatrix image;
  private final int height;
  private final int width;
  private final int leftInit;
  private final int rightInit;
  private final int downInit;
  private final int upInit;
  
  public WhiteRectangleDetector(BitMatrix image)
    throws NotFoundException
  {
    this(image, 10, image.getWidth() / 2, image.getHeight() / 2);
  }
  





  public WhiteRectangleDetector(BitMatrix image, int initSize, int x, int y)
    throws NotFoundException
  {
    this.image = image;
    height = image.getHeight();
    width = image.getWidth();
    int halfsize = initSize / 2;
    leftInit = (x - halfsize);
    rightInit = (x + halfsize);
    upInit = (y - halfsize);
    downInit = (y + halfsize);
    if ((upInit < 0) || (leftInit < 0) || (downInit >= height) || (rightInit >= width)) {
      throw NotFoundException.getNotFoundInstance();
    }
  }
  













  public ResultPoint[] detect()
    throws NotFoundException
  {
    int left = leftInit;
    int right = rightInit;
    int up = upInit;
    int down = downInit;
    boolean sizeExceeded = false;
    boolean aBlackPointFoundOnBorder = true;
    boolean atLeastOneBlackPointFoundOnBorder = false;
    
    boolean atLeastOneBlackPointFoundOnRight = false;
    boolean atLeastOneBlackPointFoundOnBottom = false;
    boolean atLeastOneBlackPointFoundOnLeft = false;
    boolean atLeastOneBlackPointFoundOnTop = false;
    
    while (aBlackPointFoundOnBorder)
    {
      aBlackPointFoundOnBorder = false;
      



      boolean rightBorderNotWhite = true;
      while (((rightBorderNotWhite) || (!atLeastOneBlackPointFoundOnRight)) && (right < width)) {
        rightBorderNotWhite = containsBlackPoint(up, down, right, false);
        if (rightBorderNotWhite) {
          right++;
          aBlackPointFoundOnBorder = true;
          atLeastOneBlackPointFoundOnRight = true;
        } else if (!atLeastOneBlackPointFoundOnRight) {
          right++;
        }
      }
      
      if (right >= width) {
        sizeExceeded = true;
        break;
      }
      



      boolean bottomBorderNotWhite = true;
      while (((bottomBorderNotWhite) || (!atLeastOneBlackPointFoundOnBottom)) && (down < height)) {
        bottomBorderNotWhite = containsBlackPoint(left, right, down, true);
        if (bottomBorderNotWhite) {
          down++;
          aBlackPointFoundOnBorder = true;
          atLeastOneBlackPointFoundOnBottom = true;
        } else if (!atLeastOneBlackPointFoundOnBottom) {
          down++;
        }
      }
      
      if (down >= height) {
        sizeExceeded = true;
        break;
      }
      



      boolean leftBorderNotWhite = true;
      while (((leftBorderNotWhite) || (!atLeastOneBlackPointFoundOnLeft)) && (left >= 0)) {
        leftBorderNotWhite = containsBlackPoint(up, down, left, false);
        if (leftBorderNotWhite) {
          left--;
          aBlackPointFoundOnBorder = true;
          atLeastOneBlackPointFoundOnLeft = true;
        } else if (!atLeastOneBlackPointFoundOnLeft) {
          left--;
        }
      }
      
      if (left < 0) {
        sizeExceeded = true;
        break;
      }
      



      boolean topBorderNotWhite = true;
      while (((topBorderNotWhite) || (!atLeastOneBlackPointFoundOnTop)) && (up >= 0)) {
        topBorderNotWhite = containsBlackPoint(left, right, up, true);
        if (topBorderNotWhite) {
          up--;
          aBlackPointFoundOnBorder = true;
          atLeastOneBlackPointFoundOnTop = true;
        } else if (!atLeastOneBlackPointFoundOnTop) {
          up--;
        }
      }
      
      if (up < 0) {
        sizeExceeded = true;
        break;
      }
      
      if (aBlackPointFoundOnBorder) {
        atLeastOneBlackPointFoundOnBorder = true;
      }
    }
    

    if ((!sizeExceeded) && (atLeastOneBlackPointFoundOnBorder))
    {
      int maxSize = right - left;
      
      ResultPoint z = null;
      for (int i = 1; i < maxSize; i++) {
        z = getBlackPointOnSegment(left, down - i, left + i, down);
        if (z != null) {
          break;
        }
      }
      
      if (z == null) {
        throw NotFoundException.getNotFoundInstance();
      }
      
      ResultPoint t = null;
      
      for (int i = 1; i < maxSize; i++) {
        t = getBlackPointOnSegment(left, up + i, left + i, up);
        if (t != null) {
          break;
        }
      }
      
      if (t == null) {
        throw NotFoundException.getNotFoundInstance();
      }
      
      ResultPoint x = null;
      
      for (int i = 1; i < maxSize; i++) {
        x = getBlackPointOnSegment(right, up + i, right - i, up);
        if (x != null) {
          break;
        }
      }
      
      if (x == null) {
        throw NotFoundException.getNotFoundInstance();
      }
      
      ResultPoint y = null;
      
      for (int i = 1; i < maxSize; i++) {
        y = getBlackPointOnSegment(right, down - i, right - i, down);
        if (y != null) {
          break;
        }
      }
      
      if (y == null) {
        throw NotFoundException.getNotFoundInstance();
      }
      
      return centerEdges(y, z, x, t);
    }
    
    throw NotFoundException.getNotFoundInstance();
  }
  
  private ResultPoint getBlackPointOnSegment(float aX, float aY, float bX, float bY)
  {
    int dist = MathUtils.round(MathUtils.distance(aX, aY, bX, bY));
    float xStep = (bX - aX) / dist;
    float yStep = (bY - aY) / dist;
    
    for (int i = 0; i < dist; i++) {
      int x = MathUtils.round(aX + i * xStep);
      int y = MathUtils.round(aY + i * yStep);
      if (image.get(x, y)) {
        return new ResultPoint(x, y);
      }
    }
    return null;
  }
  





















  private ResultPoint[] centerEdges(ResultPoint y, ResultPoint z, ResultPoint x, ResultPoint t)
  {
    float yi = y.getX();
    float yj = y.getY();
    float zi = z.getX();
    float zj = z.getY();
    float xi = x.getX();
    float xj = x.getY();
    float ti = t.getX();
    float tj = t.getY();
    
    if (yi < width / 2.0F) {
      return new ResultPoint[] { new ResultPoint(ti - 1.0F, tj + 1.0F), new ResultPoint(zi + 1.0F, zj + 1.0F), new ResultPoint(xi - 1.0F, xj - 1.0F), new ResultPoint(yi + 1.0F, yj - 1.0F) };
    }
    



    return new ResultPoint[] { new ResultPoint(ti + 1.0F, tj + 1.0F), new ResultPoint(zi + 1.0F, zj - 1.0F), new ResultPoint(xi - 1.0F, xj + 1.0F), new ResultPoint(yi - 1.0F, yj - 1.0F) };
  }
  














  private boolean containsBlackPoint(int a, int b, int fixed, boolean horizontal)
  {
    if (horizontal) {
      for (int x = a; x <= b; x++) {
        if (image.get(x, fixed)) {
          return true;
        }
      }
    } else {
      for (int y = a; y <= b; y++) {
        if (image.get(fixed, y)) {
          return true;
        }
      }
    }
    
    return false;
  }
}
