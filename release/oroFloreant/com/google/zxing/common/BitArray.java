package com.google.zxing.common;

import java.util.Arrays;




















public final class BitArray
  implements Cloneable
{
  private int[] bits;
  private int size;
  
  public BitArray()
  {
    size = 0;
    bits = new int[1];
  }
  
  public BitArray(int size) {
    this.size = size;
    bits = makeArray(size);
  }
  
  BitArray(int[] bits, int size)
  {
    this.bits = bits;
    this.size = size;
  }
  
  public int getSize() {
    return size;
  }
  
  public int getSizeInBytes() {
    return (size + 7) / 8;
  }
  
  private void ensureCapacity(int size) {
    if (size > bits.length * 32) {
      int[] newBits = makeArray(size);
      System.arraycopy(bits, 0, newBits, 0, bits.length);
      bits = newBits;
    }
  }
  



  public boolean get(int i)
  {
    return (bits[(i / 32)] & 1 << (i & 0x1F)) != 0;
  }
  




  public void set(int i)
  {
    bits[(i / 32)] |= 1 << (i & 0x1F);
  }
  




  public void flip(int i)
  {
    bits[(i / 32)] ^= 1 << (i & 0x1F);
  }
  





  public int getNextSet(int from)
  {
    if (from >= size) {
      return size;
    }
    int bitsOffset = from / 32;
    int currentBits = bits[bitsOffset];
    
    currentBits &= ((1 << (from & 0x1F)) - 1 ^ 0xFFFFFFFF);
    while (currentBits == 0) {
      bitsOffset++; if (bitsOffset == bits.length) {
        return size;
      }
      currentBits = bits[bitsOffset];
    }
    int result = bitsOffset * 32 + Integer.numberOfTrailingZeros(currentBits);
    return result > size ? size : result;
  }
  




  public int getNextUnset(int from)
  {
    if (from >= size) {
      return size;
    }
    int bitsOffset = from / 32;
    int currentBits = bits[bitsOffset] ^ 0xFFFFFFFF;
    
    currentBits &= ((1 << (from & 0x1F)) - 1 ^ 0xFFFFFFFF);
    while (currentBits == 0) {
      bitsOffset++; if (bitsOffset == bits.length) {
        return size;
      }
      currentBits = bits[bitsOffset] ^ 0xFFFFFFFF;
    }
    int result = bitsOffset * 32 + Integer.numberOfTrailingZeros(currentBits);
    return result > size ? size : result;
  }
  






  public void setBulk(int i, int newBits)
  {
    bits[(i / 32)] = newBits;
  }
  





  public void setRange(int start, int end)
  {
    if (end < start) {
      throw new IllegalArgumentException();
    }
    if (end == start) {
      return;
    }
    end--;
    int firstInt = start / 32;
    int lastInt = end / 32;
    for (int i = firstInt; i <= lastInt; i++) {
      int firstBit = i > firstInt ? 0 : start & 0x1F;
      int lastBit = i < lastInt ? 31 : end & 0x1F;
      int mask;
      int mask; if ((firstBit == 0) && (lastBit == 31)) {
        mask = -1;
      } else {
        mask = 0;
        for (int j = firstBit; j <= lastBit; j++) {
          mask |= 1 << j;
        }
      }
      bits[i] |= mask;
    }
  }
  


  public void clear()
  {
    int max = bits.length;
    for (int i = 0; i < max; i++) {
      bits[i] = 0;
    }
  }
  








  public boolean isRange(int start, int end, boolean value)
  {
    if (end < start) {
      throw new IllegalArgumentException();
    }
    if (end == start) {
      return true;
    }
    end--;
    int firstInt = start / 32;
    int lastInt = end / 32;
    for (int i = firstInt; i <= lastInt; i++) {
      int firstBit = i > firstInt ? 0 : start & 0x1F;
      int lastBit = i < lastInt ? 31 : end & 0x1F;
      int mask;
      int mask; if ((firstBit == 0) && (lastBit == 31)) {
        mask = -1;
      } else {
        mask = 0;
        for (int j = firstBit; j <= lastBit; j++) {
          mask |= 1 << j;
        }
      }
      


      if ((bits[i] & mask) != (value ? mask : 0)) {
        return false;
      }
    }
    return true;
  }
  
  public void appendBit(boolean bit) {
    ensureCapacity(size + 1);
    if (bit) {
      bits[(size / 32)] |= 1 << (size & 0x1F);
    }
    size += 1;
  }
  







  public void appendBits(int value, int numBits)
  {
    if ((numBits < 0) || (numBits > 32)) {
      throw new IllegalArgumentException("Num bits must be between 0 and 32");
    }
    ensureCapacity(size + numBits);
    for (int numBitsLeft = numBits; numBitsLeft > 0; numBitsLeft--) {
      appendBit((value >> numBitsLeft - 1 & 0x1) == 1);
    }
  }
  
  public void appendBitArray(BitArray other) {
    int otherSize = size;
    ensureCapacity(size + otherSize);
    for (int i = 0; i < otherSize; i++) {
      appendBit(other.get(i));
    }
  }
  
  public void xor(BitArray other) {
    if (bits.length != bits.length) {
      throw new IllegalArgumentException("Sizes don't match");
    }
    for (int i = 0; i < bits.length; i++)
    {

      bits[i] ^= bits[i];
    }
  }
  







  public void toBytes(int bitOffset, byte[] array, int offset, int numBytes)
  {
    for (int i = 0; i < numBytes; i++) {
      int theByte = 0;
      for (int j = 0; j < 8; j++) {
        if (get(bitOffset)) {
          theByte |= 1 << 7 - j;
        }
        bitOffset++;
      }
      array[(offset + i)] = ((byte)theByte);
    }
  }
  



  public int[] getBitArray()
  {
    return bits;
  }
  


  public void reverse()
  {
    int[] newBits = new int[bits.length];
    
    int len = (size - 1) / 32;
    int oldBitsLen = len + 1;
    for (int i = 0; i < oldBitsLen; i++) {
      long x = bits[i];
      x = x >> 1 & 0x55555555 | (x & 0x55555555) << 1;
      x = x >> 2 & 0x33333333 | (x & 0x33333333) << 2;
      x = x >> 4 & 0xF0F0F0F | (x & 0xF0F0F0F) << 4;
      x = x >> 8 & 0xFF00FF | (x & 0xFF00FF) << 8;
      x = x >> 16 & 0xFFFF | (x & 0xFFFF) << 16;
      newBits[(len - i)] = ((int)x);
    }
    
    if (size != oldBitsLen * 32) {
      int leftOffset = oldBitsLen * 32 - size;
      int mask = 1;
      for (int i = 0; i < 31 - leftOffset; i++) {
        mask = mask << 1 | 0x1;
      }
      int currentInt = newBits[0] >> leftOffset & mask;
      for (int i = 1; i < oldBitsLen; i++) {
        int nextInt = newBits[i];
        currentInt |= nextInt << 32 - leftOffset;
        newBits[(i - 1)] = currentInt;
        currentInt = nextInt >> leftOffset & mask;
      }
      newBits[(oldBitsLen - 1)] = currentInt;
    }
    bits = newBits;
  }
  
  private static int[] makeArray(int size) {
    return new int[(size + 31) / 32];
  }
  
  public boolean equals(Object o)
  {
    if (!(o instanceof BitArray)) {
      return false;
    }
    BitArray other = (BitArray)o;
    return (size == size) && (Arrays.equals(bits, bits));
  }
  
  public int hashCode()
  {
    return 31 * size + Arrays.hashCode(bits);
  }
  
  public String toString()
  {
    StringBuilder result = new StringBuilder(size);
    for (int i = 0; i < size; i++) {
      if ((i & 0x7) == 0) {
        result.append(' ');
      }
      result.append(get(i) ? 'X' : '.');
    }
    return result.toString();
  }
  
  public BitArray clone()
  {
    return new BitArray((int[])bits.clone(), size);
  }
}
