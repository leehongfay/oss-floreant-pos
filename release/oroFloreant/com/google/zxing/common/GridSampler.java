package com.google.zxing.common;

import com.google.zxing.NotFoundException;






























public abstract class GridSampler
{
  private static GridSampler gridSampler = new DefaultGridSampler();
  



  public GridSampler() {}
  



  public static void setGridSampler(GridSampler newGridSampler)
  {
    gridSampler = newGridSampler;
  }
  


  public static GridSampler getInstance()
  {
    return gridSampler;
  }
  


















  public abstract BitMatrix sampleGrid(BitMatrix paramBitMatrix, int paramInt1, int paramInt2, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8, float paramFloat9, float paramFloat10, float paramFloat11, float paramFloat12, float paramFloat13, float paramFloat14, float paramFloat15, float paramFloat16)
    throws NotFoundException;
  


















  public abstract BitMatrix sampleGrid(BitMatrix paramBitMatrix, int paramInt1, int paramInt2, PerspectiveTransform paramPerspectiveTransform)
    throws NotFoundException;
  


















  protected static void checkAndNudgePoints(BitMatrix image, float[] points)
    throws NotFoundException
  {
    int width = image.getWidth();
    int height = image.getHeight();
    
    boolean nudged = true;
    for (int offset = 0; (offset < points.length) && (nudged); offset += 2) {
      int x = (int)points[offset];
      int y = (int)points[(offset + 1)];
      if ((x < -1) || (x > width) || (y < -1) || (y > height)) {
        throw NotFoundException.getNotFoundInstance();
      }
      nudged = false;
      if (x == -1) {
        points[offset] = 0.0F;
        nudged = true;
      } else if (x == width) {
        points[offset] = (width - 1);
        nudged = true;
      }
      if (y == -1) {
        points[(offset + 1)] = 0.0F;
        nudged = true;
      } else if (y == height) {
        points[(offset + 1)] = (height - 1);
        nudged = true;
      }
    }
    
    nudged = true;
    for (int offset = points.length - 2; (offset >= 0) && (nudged); offset -= 2) {
      int x = (int)points[offset];
      int y = (int)points[(offset + 1)];
      if ((x < -1) || (x > width) || (y < -1) || (y > height)) {
        throw NotFoundException.getNotFoundInstance();
      }
      nudged = false;
      if (x == -1) {
        points[offset] = 0.0F;
        nudged = true;
      } else if (x == width) {
        points[offset] = (width - 1);
        nudged = true;
      }
      if (y == -1) {
        points[(offset + 1)] = 0.0F;
        nudged = true;
      } else if (y == height) {
        points[(offset + 1)] = (height - 1);
        nudged = true;
      }
    }
  }
}
