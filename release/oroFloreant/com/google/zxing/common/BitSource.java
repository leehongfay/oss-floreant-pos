package com.google.zxing.common;








public final class BitSource
{
  private final byte[] bytes;
  






  private int byteOffset;
  






  private int bitOffset;
  







  public BitSource(byte[] bytes)
  {
    this.bytes = bytes;
  }
  


  public int getBitOffset()
  {
    return bitOffset;
  }
  


  public int getByteOffset()
  {
    return byteOffset;
  }
  





  public int readBits(int numBits)
  {
    if ((numBits < 1) || (numBits > 32) || (numBits > available())) {
      throw new IllegalArgumentException(String.valueOf(numBits));
    }
    
    int result = 0;
    

    if (bitOffset > 0) {
      int bitsLeft = 8 - bitOffset;
      int toRead = numBits < bitsLeft ? numBits : bitsLeft;
      int bitsToNotRead = bitsLeft - toRead;
      int mask = 255 >> 8 - toRead << bitsToNotRead;
      result = (bytes[byteOffset] & mask) >> bitsToNotRead;
      numBits -= toRead;
      bitOffset += toRead;
      if (bitOffset == 8) {
        bitOffset = 0;
        byteOffset += 1;
      }
    }
    

    if (numBits > 0) {
      while (numBits >= 8) {
        result = result << 8 | bytes[byteOffset] & 0xFF;
        byteOffset += 1;
        numBits -= 8;
      }
      

      if (numBits > 0) {
        int bitsToNotRead = 8 - numBits;
        int mask = 255 >> bitsToNotRead << bitsToNotRead;
        result = result << numBits | (bytes[byteOffset] & mask) >> bitsToNotRead;
        bitOffset += numBits;
      }
    }
    
    return result;
  }
  


  public int available()
  {
    return 8 * (bytes.length - byteOffset) - bitOffset;
  }
}
