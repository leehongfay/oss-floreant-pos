package com.google.zxing.common.reedsolomon;




























public final class GenericGF
{
  public static final GenericGF AZTEC_DATA_12 = new GenericGF(4201, 4096, 1);
  public static final GenericGF AZTEC_DATA_10 = new GenericGF(1033, 1024, 1);
  public static final GenericGF AZTEC_DATA_6 = new GenericGF(67, 64, 1);
  public static final GenericGF AZTEC_PARAM = new GenericGF(19, 16, 1);
  public static final GenericGF QR_CODE_FIELD_256 = new GenericGF(285, 256, 0);
  public static final GenericGF DATA_MATRIX_FIELD_256 = new GenericGF(301, 256, 1);
  public static final GenericGF AZTEC_DATA_8 = DATA_MATRIX_FIELD_256;
  public static final GenericGF MAXICODE_FIELD_64 = AZTEC_DATA_6;
  

  private final int[] expTable;
  

  private final int[] logTable;
  

  private final GenericGFPoly zero;
  
  private final GenericGFPoly one;
  
  private final int size;
  
  private final int primitive;
  
  private final int generatorBase;
  

  public GenericGF(int primitive, int size, int b)
  {
    this.primitive = primitive;
    this.size = size;
    generatorBase = b;
    
    expTable = new int[size];
    logTable = new int[size];
    int x = 1;
    for (int i = 0; i < size; i++) {
      expTable[i] = x;
      x *= 2;
      if (x >= size) {
        x ^= primitive;
        x &= size - 1;
      }
    }
    for (int i = 0; i < size - 1; i++) {
      logTable[expTable[i]] = i;
    }
    
    zero = new GenericGFPoly(this, new int[] { 0 });
    one = new GenericGFPoly(this, new int[] { 1 });
  }
  
  GenericGFPoly getZero() {
    return zero;
  }
  
  GenericGFPoly getOne() {
    return one;
  }
  


  GenericGFPoly buildMonomial(int degree, int coefficient)
  {
    if (degree < 0) {
      throw new IllegalArgumentException();
    }
    if (coefficient == 0) {
      return zero;
    }
    int[] coefficients = new int[degree + 1];
    coefficients[0] = coefficient;
    return new GenericGFPoly(this, coefficients);
  }
  




  static int addOrSubtract(int a, int b)
  {
    return a ^ b;
  }
  


  int exp(int a)
  {
    return expTable[a];
  }
  


  int log(int a)
  {
    if (a == 0) {
      throw new IllegalArgumentException();
    }
    return logTable[a];
  }
  


  int inverse(int a)
  {
    if (a == 0) {
      throw new ArithmeticException();
    }
    return expTable[(size - logTable[a] - 1)];
  }
  


  int multiply(int a, int b)
  {
    if ((a == 0) || (b == 0)) {
      return 0;
    }
    return expTable[((logTable[a] + logTable[b]) % (size - 1))];
  }
  
  public int getSize() {
    return size;
  }
  
  public int getGeneratorBase() {
    return generatorBase;
  }
  
  public String toString()
  {
    return "GF(0x" + Integer.toHexString(primitive) + ',' + size + ')';
  }
}
