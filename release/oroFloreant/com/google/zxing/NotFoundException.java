package com.google.zxing;






















public final class NotFoundException
  extends ReaderException
{
  private static final NotFoundException INSTANCE = new NotFoundException();
  
  static { INSTANCE.setStackTrace(NO_TRACE); }
  




  public static NotFoundException getNotFoundInstance()
  {
    return INSTANCE;
  }
  
  private NotFoundException() {}
}
