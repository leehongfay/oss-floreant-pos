package com.google.zxing.datamatrix.encoder;








class C40Encoder
  implements Encoder
{
  C40Encoder() {}
  






  public int getEncodingMode()
  {
    return 1;
  }
  

  public void encode(EncoderContext context)
  {
    StringBuilder buffer = new StringBuilder();
    while (context.hasMoreCharacters()) {
      char c = context.getCurrentChar();
      pos += 1;
      
      int lastCharSize = encodeChar(c, buffer);
      
      int unwritten = buffer.length() / 3 * 2;
      
      int curCodewordCount = context.getCodewordCount() + unwritten;
      context.updateSymbolInfo(curCodewordCount);
      int available = context.getSymbolInfo().getDataCapacity() - curCodewordCount;
      
      if (!context.hasMoreCharacters())
      {
        StringBuilder removed = new StringBuilder();
        if ((buffer.length() % 3 == 2) && (
          (available < 2) || (available > 2))) {
          lastCharSize = backtrackOneCharacter(context, buffer, removed, lastCharSize);
        }
        

        while ((buffer.length() % 3 == 1) && (((lastCharSize <= 3) && (available != 1)) || (lastCharSize > 3)))
        {
          lastCharSize = backtrackOneCharacter(context, buffer, removed, lastCharSize);
        }
      }
      

      int count = buffer.length();
      if (count % 3 == 0) {
        int newMode = HighLevelEncoder.lookAheadTest(context.getMessage(), pos, getEncodingMode());
        if (newMode != getEncodingMode()) {
          context.signalEncoderChange(newMode);
          break;
        }
      }
    }
    handleEOD(context, buffer);
  }
  
  private int backtrackOneCharacter(EncoderContext context, StringBuilder buffer, StringBuilder removed, int lastCharSize)
  {
    int count = buffer.length();
    buffer.delete(count - lastCharSize, count);
    pos -= 1;
    char c = context.getCurrentChar();
    lastCharSize = encodeChar(c, removed);
    context.resetSymbolInfo();
    return lastCharSize;
  }
  
  static void writeNextTriplet(EncoderContext context, StringBuilder buffer) {
    context.writeCodewords(encodeToCodewords(buffer, 0));
    buffer.delete(0, 3);
  }
  





  void handleEOD(EncoderContext context, StringBuilder buffer)
  {
    int unwritten = buffer.length() / 3 * 2;
    int rest = buffer.length() % 3;
    
    int curCodewordCount = context.getCodewordCount() + unwritten;
    context.updateSymbolInfo(curCodewordCount);
    int available = context.getSymbolInfo().getDataCapacity() - curCodewordCount;
    
    if (rest == 2) {
      buffer.append('\000');
      while (buffer.length() >= 3) {
        writeNextTriplet(context, buffer);
      }
      if (context.hasMoreCharacters()) {
        context.writeCodeword('þ');
      }
    } else if ((available == 1) && (rest == 1)) {
      while (buffer.length() >= 3) {
        writeNextTriplet(context, buffer);
      }
      if (context.hasMoreCharacters()) {
        context.writeCodeword('þ');
      }
      
      pos -= 1;
    } else if (rest == 0) {
      while (buffer.length() >= 3) {
        writeNextTriplet(context, buffer);
      }
      if ((available > 0) || (context.hasMoreCharacters())) {
        context.writeCodeword('þ');
      }
    } else {
      throw new IllegalStateException("Unexpected case. Please report!");
    }
    context.signalEncoderChange(0);
  }
  
  int encodeChar(char c, StringBuilder sb) {
    if (c == ' ') {
      sb.append('\003');
      return 1; }
    if ((c >= '0') && (c <= '9')) {
      sb.append((char)(c - '0' + 4));
      return 1; }
    if ((c >= 'A') && (c <= 'Z')) {
      sb.append((char)(c - 'A' + 14));
      return 1; }
    if ((c >= 0) && (c <= '\037')) {
      sb.append('\000');
      sb.append(c);
      return 2; }
    if ((c >= '!') && (c <= '/')) {
      sb.append('\001');
      sb.append((char)(c - '!'));
      return 2; }
    if ((c >= ':') && (c <= '@')) {
      sb.append('\001');
      sb.append((char)(c - ':' + 15));
      return 2; }
    if ((c >= '[') && (c <= '_')) {
      sb.append('\001');
      sb.append((char)(c - '[' + 22));
      return 2; }
    if ((c >= '`') && (c <= '')) {
      sb.append('\002');
      sb.append((char)(c - '`'));
      return 2; }
    if (c >= '') {
      sb.append("\001\036");
      int len = 2;
      len += encodeChar((char)(c - ''), sb);
      return len;
    }
    throw new IllegalArgumentException("Illegal character: " + c);
  }
  
  private static String encodeToCodewords(CharSequence sb, int startPos)
  {
    char c1 = sb.charAt(startPos);
    char c2 = sb.charAt(startPos + 1);
    char c3 = sb.charAt(startPos + 2);
    int v = 'ـ' * c1 + '(' * c2 + c3 + 1;
    char cw1 = (char)(v / 256);
    char cw2 = (char)(v % 256);
    return new String(new char[] { cw1, cw2 });
  }
}
