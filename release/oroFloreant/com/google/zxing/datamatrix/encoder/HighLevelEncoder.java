package com.google.zxing.datamatrix.encoder;

import com.google.zxing.Dimension;
import java.util.Arrays;

































































































public final class HighLevelEncoder
{
  private static final char PAD = '';
  static final char LATCH_TO_C40 = 'æ';
  static final char LATCH_TO_BASE256 = 'ç';
  static final char UPPER_SHIFT = 'ë';
  private static final char MACRO_05 = 'ì';
  private static final char MACRO_06 = 'í';
  static final char LATCH_TO_ANSIX12 = 'î';
  static final char LATCH_TO_TEXT = 'ï';
  static final char LATCH_TO_EDIFACT = 'ð';
  static final char C40_UNLATCH = 'þ';
  static final char X12_UNLATCH = 'þ';
  private static final String MACRO_05_HEADER = "[)>\03605\035";
  private static final String MACRO_06_HEADER = "[)>\03606\035";
  private static final String MACRO_TRAILER = "\036\004";
  static final int ASCII_ENCODATION = 0;
  static final int C40_ENCODATION = 1;
  static final int TEXT_ENCODATION = 2;
  static final int X12_ENCODATION = 3;
  static final int EDIFACT_ENCODATION = 4;
  static final int BASE256_ENCODATION = 5;
  
  private HighLevelEncoder() {}
  
  private static char randomize253State(char ch, int codewordPosition)
  {
    int pseudoRandom = 149 * codewordPosition % 253 + 1;
    int tempVariable = ch + pseudoRandom;
    return tempVariable <= 254 ? (char)tempVariable : (char)(tempVariable - 254);
  }
  






  public static String encodeHighLevel(String msg)
  {
    return encodeHighLevel(msg, SymbolShapeHint.FORCE_NONE, null, null);
  }
  














  public static String encodeHighLevel(String msg, SymbolShapeHint shape, Dimension minSize, Dimension maxSize)
  {
    Encoder[] encoders = { new ASCIIEncoder(), new C40Encoder(), new TextEncoder(), new X12Encoder(), new EdifactEncoder(), new Base256Encoder() };
    



    EncoderContext context = new EncoderContext(msg);
    context.setSymbolShape(shape);
    context.setSizeConstraints(minSize, maxSize);
    
    if ((msg.startsWith("[)>\03605\035")) && (msg.endsWith("\036\004"))) {
      context.writeCodeword('ì');
      context.setSkipAtEnd(2);
      pos += "[)>\03605\035".length();
    } else if ((msg.startsWith("[)>\03606\035")) && (msg.endsWith("\036\004"))) {
      context.writeCodeword('í');
      context.setSkipAtEnd(2);
      pos += "[)>\03606\035".length();
    }
    
    int encodingMode = 0;
    while (context.hasMoreCharacters()) {
      encoders[encodingMode].encode(context);
      if (context.getNewEncoding() >= 0) {
        encodingMode = context.getNewEncoding();
        context.resetEncoderSignal();
      }
    }
    int len = context.getCodewordCount();
    context.updateSymbolInfo();
    int capacity = context.getSymbolInfo().getDataCapacity();
    if ((len < capacity) && 
      (encodingMode != 0) && (encodingMode != 5)) {
      context.writeCodeword('þ');
    }
    

    StringBuilder codewords = context.getCodewords();
    if (codewords.length() < capacity) {
      codewords.append('');
    }
    while (codewords.length() < capacity) {
      codewords.append(randomize253State('', codewords.length() + 1));
    }
    
    return context.getCodewords().toString();
  }
  
  static int lookAheadTest(CharSequence msg, int startpos, int currentMode) {
    if (startpos >= msg.length()) {
      return currentMode;
    }
    float[] charCounts;
    float[] charCounts;
    if (currentMode == 0) {
      charCounts = new float[] { 0.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.25F };
    } else {
      charCounts = new float[] { 1.0F, 2.0F, 2.0F, 2.0F, 2.0F, 2.25F };
      charCounts[currentMode] = 0.0F;
    }
    
    int charsProcessed = 0;
    for (;;)
    {
      if (startpos + charsProcessed == msg.length()) {
        int min = Integer.MAX_VALUE;
        byte[] mins = new byte[6];
        int[] intCharCounts = new int[6];
        min = findMinimums(charCounts, intCharCounts, min, mins);
        int minCount = getMinimumCount(mins);
        
        if (intCharCounts[0] == min) {
          return 0;
        }
        if ((minCount == 1) && (mins[5] > 0)) {
          return 5;
        }
        if ((minCount == 1) && (mins[4] > 0)) {
          return 4;
        }
        if ((minCount == 1) && (mins[2] > 0)) {
          return 2;
        }
        if ((minCount == 1) && (mins[3] > 0)) {
          return 3;
        }
        return 1;
      }
      
      char c = msg.charAt(startpos + charsProcessed);
      charsProcessed++;
      

      if (isDigit(c)) {
        int tmp232_231 = 0; float[] tmp232_230 = charCounts;tmp232_230[tmp232_231] = ((float)(tmp232_230[tmp232_231] + 0.5D));
      } else if (isExtendedASCII(c)) {
        charCounts[0] = ((int)Math.ceil(charCounts[0]));
        charCounts[0] += 2.0F;
      } else {
        charCounts[0] = ((int)Math.ceil(charCounts[0]));
        charCounts[0] += 1.0F;
      }
      

      if (isNativeC40(c)) {
        charCounts[1] += 0.6666667F;
      } else if (isExtendedASCII(c)) {
        charCounts[1] += 2.6666667F;
      } else {
        charCounts[1] += 1.3333334F;
      }
      

      if (isNativeText(c)) {
        charCounts[2] += 0.6666667F;
      } else if (isExtendedASCII(c)) {
        charCounts[2] += 2.6666667F;
      } else {
        charCounts[2] += 1.3333334F;
      }
      

      if (isNativeX12(c)) {
        charCounts[3] += 0.6666667F;
      } else if (isExtendedASCII(c)) {
        charCounts[3] += 4.3333335F;
      } else {
        charCounts[3] += 3.3333333F;
      }
      

      if (isNativeEDIFACT(c)) {
        charCounts[4] += 0.75F;
      } else if (isExtendedASCII(c)) {
        charCounts[4] += 4.25F;
      } else {
        charCounts[4] += 3.25F;
      }
      

      if (isSpecialB256(c)) {
        charCounts[5] += 4.0F;
      } else {
        charCounts[5] += 1.0F;
      }
      

      if (charsProcessed >= 4) {
        int[] intCharCounts = new int[6];
        byte[] mins = new byte[6];
        findMinimums(charCounts, intCharCounts, Integer.MAX_VALUE, mins);
        int minCount = getMinimumCount(mins);
        
        if ((intCharCounts[0] < intCharCounts[5]) && (intCharCounts[0] < intCharCounts[1]) && (intCharCounts[0] < intCharCounts[2]) && (intCharCounts[0] < intCharCounts[3]) && (intCharCounts[0] < intCharCounts[4]))
        {



          return 0;
        }
        if ((intCharCounts[5] < intCharCounts[0]) || (mins[1] + mins[2] + mins[3] + mins[4] == 0))
        {
          return 5;
        }
        if ((minCount == 1) && (mins[4] > 0)) {
          return 4;
        }
        if ((minCount == 1) && (mins[2] > 0)) {
          return 2;
        }
        if ((minCount == 1) && (mins[3] > 0)) {
          return 3;
        }
        if ((intCharCounts[1] + 1 < intCharCounts[0]) && (intCharCounts[1] + 1 < intCharCounts[5]) && (intCharCounts[1] + 1 < intCharCounts[4]) && (intCharCounts[1] + 1 < intCharCounts[2]))
        {


          if (intCharCounts[1] < intCharCounts[3]) {
            return 1;
          }
          if (intCharCounts[1] == intCharCounts[3]) {
            int p = startpos + charsProcessed + 1;
            while (p < msg.length()) {
              char tc = msg.charAt(p);
              if (isX12TermSep(tc)) {
                return 3;
              }
              if (!isNativeX12(tc)) {
                break;
              }
              p++;
            }
            return 1;
          }
        }
      }
    }
  }
  
  private static int findMinimums(float[] charCounts, int[] intCharCounts, int min, byte[] mins) {
    Arrays.fill(mins, (byte)0);
    for (int i = 0; i < 6; i++) {
      intCharCounts[i] = ((int)Math.ceil(charCounts[i]));
      int current = intCharCounts[i];
      if (min > current) {
        min = current;
        Arrays.fill(mins, (byte)0);
      }
      if (min == current) {
        int tmp57_55 = i; byte[] tmp57_54 = mins;tmp57_54[tmp57_55] = ((byte)(tmp57_54[tmp57_55] + 1));
      }
    }
    
    return min;
  }
  
  private static int getMinimumCount(byte[] mins) {
    int minCount = 0;
    for (int i = 0; i < 6; i++) {
      minCount += mins[i];
    }
    return minCount;
  }
  
  static boolean isDigit(char ch) {
    return (ch >= '0') && (ch <= '9');
  }
  
  static boolean isExtendedASCII(char ch) {
    return (ch >= '') && (ch <= 'ÿ');
  }
  
  private static boolean isNativeC40(char ch) {
    return (ch == ' ') || ((ch >= '0') && (ch <= '9')) || ((ch >= 'A') && (ch <= 'Z'));
  }
  
  private static boolean isNativeText(char ch) {
    return (ch == ' ') || ((ch >= '0') && (ch <= '9')) || ((ch >= 'a') && (ch <= 'z'));
  }
  
  private static boolean isNativeX12(char ch) {
    return (isX12TermSep(ch)) || (ch == ' ') || ((ch >= '0') && (ch <= '9')) || ((ch >= 'A') && (ch <= 'Z'));
  }
  
  private static boolean isX12TermSep(char ch) {
    return (ch == '\r') || (ch == '*') || (ch == '>');
  }
  

  private static boolean isNativeEDIFACT(char ch)
  {
    return (ch >= ' ') && (ch <= '^');
  }
  
  private static boolean isSpecialB256(char ch) {
    return false;
  }
  






  public static int determineConsecutiveDigitCount(CharSequence msg, int startpos)
  {
    int count = 0;
    int len = msg.length();
    int idx = startpos;
    if (idx < len) {
      char ch = msg.charAt(idx);
      while ((isDigit(ch)) && (idx < len)) {
        count++;
        idx++;
        if (idx < len) {
          ch = msg.charAt(idx);
        }
      }
    }
    return count;
  }
  
  static void illegalCharacter(char c) {
    String hex = Integer.toHexString(c);
    hex = "0000".substring(0, 4 - hex.length()) + hex;
    throw new IllegalArgumentException("Illegal character: " + c + " (0x" + hex + ')');
  }
}
