package com.google.zxing.datamatrix.encoder;

abstract interface Encoder
{
  public abstract int getEncodingMode();
  
  public abstract void encode(EncoderContext paramEncoderContext);
}
