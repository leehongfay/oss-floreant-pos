package com.google.zxing.datamatrix.encoder;








final class ASCIIEncoder
  implements Encoder
{
  ASCIIEncoder() {}
  






  public int getEncodingMode()
  {
    return 0;
  }
  

  public void encode(EncoderContext context)
  {
    int n = HighLevelEncoder.determineConsecutiveDigitCount(context.getMessage(), pos);
    if (n >= 2) {
      context.writeCodeword(encodeASCIIDigits(context.getMessage().charAt(pos), context
        .getMessage().charAt(pos + 1)));
      pos += 2;
    } else {
      char c = context.getCurrentChar();
      int newMode = HighLevelEncoder.lookAheadTest(context.getMessage(), pos, getEncodingMode());
      if (newMode != getEncodingMode()) {
        switch (newMode) {
        case 5: 
          context.writeCodeword('ç');
          context.signalEncoderChange(5);
          return;
        case 1: 
          context.writeCodeword('æ');
          context.signalEncoderChange(1);
          return;
        case 3: 
          context.writeCodeword('î');
          context.signalEncoderChange(3);
          break;
        case 2: 
          context.writeCodeword('ï');
          context.signalEncoderChange(2);
          break;
        case 4: 
          context.writeCodeword('ð');
          context.signalEncoderChange(4);
          break;
        default: 
          throw new IllegalStateException("Illegal mode: " + newMode);
        }
      } else if (HighLevelEncoder.isExtendedASCII(c)) {
        context.writeCodeword('ë');
        context.writeCodeword((char)(c - '' + 1));
        pos += 1;
      } else {
        context.writeCodeword((char)(c + '\001'));
        pos += 1;
      }
    }
  }
  
  private static char encodeASCIIDigits(char digit1, char digit2)
  {
    if ((HighLevelEncoder.isDigit(digit1)) && (HighLevelEncoder.isDigit(digit2))) {
      int num = (digit1 - '0') * 10 + (digit2 - '0');
      return (char)(num + 130);
    }
    throw new IllegalArgumentException("not digits: " + digit1 + digit2);
  }
}
