package com.google.zxing.datamatrix.encoder;








final class X12Encoder
  extends C40Encoder
{
  X12Encoder() {}
  






  public int getEncodingMode()
  {
    return 3;
  }
  

  public void encode(EncoderContext context)
  {
    StringBuilder buffer = new StringBuilder();
    while (context.hasMoreCharacters()) {
      char c = context.getCurrentChar();
      pos += 1;
      
      encodeChar(c, buffer);
      
      int count = buffer.length();
      if (count % 3 == 0) {
        writeNextTriplet(context, buffer);
        
        int newMode = HighLevelEncoder.lookAheadTest(context.getMessage(), pos, getEncodingMode());
        if (newMode != getEncodingMode()) {
          context.signalEncoderChange(newMode);
          break;
        }
      }
    }
    handleEOD(context, buffer);
  }
  
  int encodeChar(char c, StringBuilder sb)
  {
    if (c == '\r') {
      sb.append('\000');
    } else if (c == '*') {
      sb.append('\001');
    } else if (c == '>') {
      sb.append('\002');
    } else if (c == ' ') {
      sb.append('\003');
    } else if ((c >= '0') && (c <= '9')) {
      sb.append((char)(c - '0' + 4));
    } else if ((c >= 'A') && (c <= 'Z')) {
      sb.append((char)(c - 'A' + 14));
    } else {
      HighLevelEncoder.illegalCharacter(c);
    }
    return 1;
  }
  
  void handleEOD(EncoderContext context, StringBuilder buffer)
  {
    context.updateSymbolInfo();
    int available = context.getSymbolInfo().getDataCapacity() - context.getCodewordCount();
    int count = buffer.length();
    pos -= count;
    if ((context.getRemainingCharacters() > 1) || (available > 1) || 
      (context.getRemainingCharacters() != available)) {
      context.writeCodeword('þ');
    }
    if (context.getNewEncoding() < 0) {
      context.signalEncoderChange(0);
    }
  }
}
