package com.google.zxing.datamatrix.encoder;








final class TextEncoder
  extends C40Encoder
{
  TextEncoder() {}
  






  public int getEncodingMode()
  {
    return 2;
  }
  
  int encodeChar(char c, StringBuilder sb)
  {
    if (c == ' ') {
      sb.append('\003');
      return 1;
    }
    if ((c >= '0') && (c <= '9')) {
      sb.append((char)(c - '0' + 4));
      return 1;
    }
    if ((c >= 'a') && (c <= 'z')) {
      sb.append((char)(c - 'a' + 14));
      return 1;
    }
    if ((c >= 0) && (c <= '\037')) {
      sb.append('\000');
      sb.append(c);
      return 2;
    }
    if ((c >= '!') && (c <= '/')) {
      sb.append('\001');
      sb.append((char)(c - '!'));
      return 2;
    }
    if ((c >= ':') && (c <= '@')) {
      sb.append('\001');
      sb.append((char)(c - ':' + 15));
      return 2;
    }
    if ((c >= '[') && (c <= '_')) {
      sb.append('\001');
      sb.append((char)(c - '[' + 22));
      return 2;
    }
    if (c == '`') {
      sb.append('\002');
      sb.append((char)(c - '`'));
      return 2;
    }
    if ((c >= 'A') && (c <= 'Z')) {
      sb.append('\002');
      sb.append((char)(c - 'A' + 1));
      return 2;
    }
    if ((c >= '{') && (c <= '')) {
      sb.append('\002');
      sb.append((char)(c - '{' + 27));
      return 2;
    }
    if (c >= '') {
      sb.append("\001\036");
      int len = 2;
      len += encodeChar((char)(c - ''), sb);
      return len;
    }
    HighLevelEncoder.illegalCharacter(c);
    return -1;
  }
}
