package com.google.zxing.datamatrix.encoder;

import com.google.zxing.Dimension;
import java.nio.charset.Charset;


















final class EncoderContext
{
  private final String msg;
  private SymbolShapeHint shape;
  private Dimension minSize;
  private Dimension maxSize;
  private final StringBuilder codewords;
  int pos;
  private int newEncoding;
  private SymbolInfo symbolInfo;
  private int skipAtEnd;
  
  EncoderContext(String msg)
  {
    byte[] msgBinary = msg.getBytes(Charset.forName("ISO-8859-1"));
    StringBuilder sb = new StringBuilder(msgBinary.length);
    int i = 0; for (int c = msgBinary.length; i < c; i++) {
      char ch = (char)(msgBinary[i] & 0xFF);
      if ((ch == '?') && (msg.charAt(i) != '?')) {
        throw new IllegalArgumentException("Message contains characters outside ISO-8859-1 encoding.");
      }
      sb.append(ch);
    }
    this.msg = sb.toString();
    shape = SymbolShapeHint.FORCE_NONE;
    codewords = new StringBuilder(msg.length());
    newEncoding = -1;
  }
  
  public void setSymbolShape(SymbolShapeHint shape) {
    this.shape = shape;
  }
  
  public void setSizeConstraints(Dimension minSize, Dimension maxSize) {
    this.minSize = minSize;
    this.maxSize = maxSize;
  }
  
  public String getMessage() {
    return msg;
  }
  
  public void setSkipAtEnd(int count) {
    skipAtEnd = count;
  }
  
  public char getCurrentChar() {
    return msg.charAt(pos);
  }
  
  public char getCurrent() {
    return msg.charAt(pos);
  }
  
  public StringBuilder getCodewords() {
    return codewords;
  }
  
  public void writeCodewords(String codewords) {
    this.codewords.append(codewords);
  }
  
  public void writeCodeword(char codeword) {
    codewords.append(codeword);
  }
  
  public int getCodewordCount() {
    return codewords.length();
  }
  
  public int getNewEncoding() {
    return newEncoding;
  }
  
  public void signalEncoderChange(int encoding) {
    newEncoding = encoding;
  }
  
  public void resetEncoderSignal() {
    newEncoding = -1;
  }
  
  public boolean hasMoreCharacters() {
    return pos < getTotalMessageCharCount();
  }
  
  private int getTotalMessageCharCount() {
    return msg.length() - skipAtEnd;
  }
  
  public int getRemainingCharacters() {
    return getTotalMessageCharCount() - pos;
  }
  
  public SymbolInfo getSymbolInfo() {
    return symbolInfo;
  }
  
  public void updateSymbolInfo() {
    updateSymbolInfo(getCodewordCount());
  }
  
  public void updateSymbolInfo(int len) {
    if ((symbolInfo == null) || (len > symbolInfo.getDataCapacity())) {
      symbolInfo = SymbolInfo.lookup(len, shape, minSize, maxSize, true);
    }
  }
  
  public void resetSymbolInfo() {
    symbolInfo = null;
  }
}
