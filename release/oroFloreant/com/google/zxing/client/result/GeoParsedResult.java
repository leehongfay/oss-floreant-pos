package com.google.zxing.client.result;





public final class GeoParsedResult
  extends ParsedResult
{
  private final double latitude;
  



  private final double longitude;
  


  private final double altitude;
  


  private final String query;
  



  GeoParsedResult(double latitude, double longitude, double altitude, String query)
  {
    super(ParsedResultType.GEO);
    this.latitude = latitude;
    this.longitude = longitude;
    this.altitude = altitude;
    this.query = query;
  }
  
  public String getGeoURI() {
    StringBuilder result = new StringBuilder();
    result.append("geo:");
    result.append(latitude);
    result.append(',');
    result.append(longitude);
    if (altitude > 0.0D) {
      result.append(',');
      result.append(altitude);
    }
    if (query != null) {
      result.append('?');
      result.append(query);
    }
    return result.toString();
  }
  


  public double getLatitude()
  {
    return latitude;
  }
  


  public double getLongitude()
  {
    return longitude;
  }
  


  public double getAltitude()
  {
    return altitude;
  }
  


  public String getQuery()
  {
    return query;
  }
  
  public String getDisplayResult()
  {
    StringBuilder result = new StringBuilder(20);
    result.append(latitude);
    result.append(", ");
    result.append(longitude);
    if (altitude > 0.0D) {
      result.append(", ");
      result.append(altitude);
      result.append('m');
    }
    if (query != null) {
      result.append(" (");
      result.append(query);
      result.append(')');
    }
    return result.toString();
  }
}
