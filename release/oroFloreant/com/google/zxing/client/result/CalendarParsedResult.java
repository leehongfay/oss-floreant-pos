package com.google.zxing.client.result;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;




















public final class CalendarParsedResult
  extends ParsedResult
{
  private static final Pattern RFC2445_DURATION = Pattern.compile("P(?:(\\d+)W)?(?:(\\d+)D)?(?:T(?:(\\d+)H)?(?:(\\d+)M)?(?:(\\d+)S)?)?");
  private static final long[] RFC2445_DURATION_FIELD_UNITS = { 604800000L, 86400000L, 3600000L, 60000L, 1000L };
  






  private static final Pattern DATE_TIME = Pattern.compile("[0-9]{8}(T[0-9]{6}Z?)?");
  
  private final String summary;
  
  private final Date start;
  
  private final boolean startAllDay;
  
  private final Date end;
  
  private final boolean endAllDay;
  
  private final String location;
  
  private final String organizer;
  
  private final String[] attendees;
  
  private final String description;
  private final double latitude;
  private final double longitude;
  
  public CalendarParsedResult(String summary, String startString, String endString, String durationString, String location, String organizer, String[] attendees, String description, double latitude, double longitude)
  {
    super(ParsedResultType.CALENDAR);
    this.summary = summary;
    try
    {
      start = parseDate(startString);
    } catch (ParseException pe) {
      throw new IllegalArgumentException(pe.toString());
    }
    
    if (endString == null) {
      long durationMS = parseDurationMS(durationString);
      end = (durationMS < 0L ? null : new Date(start.getTime() + durationMS));
    } else {
      try {
        end = parseDate(endString);
      } catch (ParseException pe) {
        throw new IllegalArgumentException(pe.toString());
      }
    }
    
    startAllDay = (startString.length() == 8);
    endAllDay = ((endString != null) && (endString.length() == 8));
    
    this.location = location;
    this.organizer = organizer;
    this.attendees = attendees;
    this.description = description;
    this.latitude = latitude;
    this.longitude = longitude;
  }
  
  public String getSummary() {
    return summary;
  }
  


  public Date getStart()
  {
    return start;
  }
  


  public boolean isStartAllDay()
  {
    return startAllDay;
  }
  



  public Date getEnd()
  {
    return end;
  }
  


  public boolean isEndAllDay()
  {
    return endAllDay;
  }
  
  public String getLocation() {
    return location;
  }
  
  public String getOrganizer() {
    return organizer;
  }
  
  public String[] getAttendees() {
    return attendees;
  }
  
  public String getDescription() {
    return description;
  }
  
  public double getLatitude() {
    return latitude;
  }
  
  public double getLongitude() {
    return longitude;
  }
  
  public String getDisplayResult()
  {
    StringBuilder result = new StringBuilder(100);
    maybeAppend(summary, result);
    maybeAppend(format(startAllDay, start), result);
    maybeAppend(format(endAllDay, end), result);
    maybeAppend(location, result);
    maybeAppend(organizer, result);
    maybeAppend(attendees, result);
    maybeAppend(description, result);
    return result.toString();
  }
  





  private static Date parseDate(String when)
    throws ParseException
  {
    if (!DATE_TIME.matcher(when).matches()) {
      throw new ParseException(when, 0);
    }
    if (when.length() == 8)
    {
      return buildDateFormat().parse(when);
    }
    
    Date date;
    if ((when.length() == 16) && (when.charAt(15) == 'Z')) {
      Date date = buildDateTimeFormat().parse(when.substring(0, 15));
      Calendar calendar = new GregorianCalendar();
      long milliseconds = date.getTime();
      
      milliseconds += calendar.get(15);
      

      calendar.setTime(new Date(milliseconds));
      milliseconds += calendar.get(16);
      date = new Date(milliseconds);
    } else {
      date = buildDateTimeFormat().parse(when);
    }
    return date;
  }
  
  private static String format(boolean allDay, Date date)
  {
    if (date == null) {
      return null;
    }
    

    DateFormat format = allDay ? DateFormat.getDateInstance(2) : DateFormat.getDateTimeInstance(2, 2);
    return format.format(date);
  }
  
  private static long parseDurationMS(CharSequence durationString) {
    if (durationString == null) {
      return -1L;
    }
    Matcher m = RFC2445_DURATION.matcher(durationString);
    if (!m.matches()) {
      return -1L;
    }
    long durationMS = 0L;
    for (int i = 0; i < RFC2445_DURATION_FIELD_UNITS.length; i++) {
      String fieldValue = m.group(i + 1);
      if (fieldValue != null) {
        durationMS += RFC2445_DURATION_FIELD_UNITS[i] * Integer.parseInt(fieldValue);
      }
    }
    return durationMS;
  }
  
  private static DateFormat buildDateFormat() {
    DateFormat format = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
    


    format.setTimeZone(TimeZone.getTimeZone("GMT"));
    return format;
  }
  
  private static DateFormat buildDateTimeFormat() {
    return new SimpleDateFormat("yyyyMMdd'T'HHmmss", Locale.ENGLISH);
  }
}
