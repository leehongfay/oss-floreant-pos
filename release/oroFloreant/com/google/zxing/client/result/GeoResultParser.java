package com.google.zxing.client.result;

import com.google.zxing.Result;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


























public final class GeoResultParser
  extends ResultParser
{
  private static final Pattern GEO_URL_PATTERN = Pattern.compile("geo:([\\-0-9.]+),([\\-0-9.]+)(?:,([\\-0-9.]+))?(?:\\?(.*))?", 2);
  
  public GeoResultParser() {}
  
  public GeoParsedResult parse(Result result) { CharSequence rawText = getMassagedText(result);
    Matcher matcher = GEO_URL_PATTERN.matcher(rawText);
    if (!matcher.matches()) {
      return null;
    }
    
    String query = matcher.group(4);
    


    try
    {
      double latitude = Double.parseDouble(matcher.group(1));
      if ((latitude > 90.0D) || (latitude < -90.0D)) {
        return null;
      }
      double longitude = Double.parseDouble(matcher.group(2));
      if ((longitude > 180.0D) || (longitude < -180.0D))
        return null;
      double altitude;
      if (matcher.group(3) == null) {
        altitude = 0.0D;
      } else {
        double altitude = Double.parseDouble(matcher.group(3));
        if (altitude < 0.0D) {
          return null;
        }
      }
    } catch (NumberFormatException ignored) {
      return null; }
    double altitude;
    double longitude; double latitude; return new GeoParsedResult(latitude, longitude, altitude, query);
  }
}
