package com.google.zxing.client.result;



public final class AddressBookParsedResult
  extends ParsedResult
{
  private final String[] names;
  

  private final String[] nicknames;
  

  private final String pronunciation;
  

  private final String[] phoneNumbers;
  

  private final String[] phoneTypes;
  

  private final String[] emails;
  
  private final String[] emailTypes;
  
  private final String instantMessenger;
  
  private final String note;
  
  private final String[] addresses;
  
  private final String[] addressTypes;
  
  private final String org;
  
  private final String birthday;
  
  private final String title;
  
  private final String[] urls;
  
  private final String[] geo;
  

  public AddressBookParsedResult(String[] names, String[] phoneNumbers, String[] phoneTypes, String[] emails, String[] emailTypes, String[] addresses, String[] addressTypes)
  {
    this(names, null, null, phoneNumbers, phoneTypes, emails, emailTypes, null, null, addresses, addressTypes, null, null, null, null, null);
  }
  





























  public AddressBookParsedResult(String[] names, String[] nicknames, String pronunciation, String[] phoneNumbers, String[] phoneTypes, String[] emails, String[] emailTypes, String instantMessenger, String note, String[] addresses, String[] addressTypes, String org, String birthday, String title, String[] urls, String[] geo)
  {
    super(ParsedResultType.ADDRESSBOOK);
    this.names = names;
    this.nicknames = nicknames;
    this.pronunciation = pronunciation;
    this.phoneNumbers = phoneNumbers;
    this.phoneTypes = phoneTypes;
    this.emails = emails;
    this.emailTypes = emailTypes;
    this.instantMessenger = instantMessenger;
    this.note = note;
    this.addresses = addresses;
    this.addressTypes = addressTypes;
    this.org = org;
    this.birthday = birthday;
    this.title = title;
    this.urls = urls;
    this.geo = geo;
  }
  
  public String[] getNames() {
    return names;
  }
  
  public String[] getNicknames() {
    return nicknames;
  }
  





  public String getPronunciation()
  {
    return pronunciation;
  }
  
  public String[] getPhoneNumbers() {
    return phoneNumbers;
  }
  



  public String[] getPhoneTypes()
  {
    return phoneTypes;
  }
  
  public String[] getEmails() {
    return emails;
  }
  



  public String[] getEmailTypes()
  {
    return emailTypes;
  }
  
  public String getInstantMessenger() {
    return instantMessenger;
  }
  
  public String getNote() {
    return note;
  }
  
  public String[] getAddresses() {
    return addresses;
  }
  



  public String[] getAddressTypes()
  {
    return addressTypes;
  }
  
  public String getTitle() {
    return title;
  }
  
  public String getOrg() {
    return org;
  }
  
  public String[] getURLs() {
    return urls;
  }
  


  public String getBirthday()
  {
    return birthday;
  }
  


  public String[] getGeo()
  {
    return geo;
  }
  
  public String getDisplayResult()
  {
    StringBuilder result = new StringBuilder(100);
    maybeAppend(names, result);
    maybeAppend(nicknames, result);
    maybeAppend(pronunciation, result);
    maybeAppend(title, result);
    maybeAppend(org, result);
    maybeAppend(addresses, result);
    maybeAppend(phoneNumbers, result);
    maybeAppend(emails, result);
    maybeAppend(instantMessenger, result);
    maybeAppend(urls, result);
    maybeAppend(birthday, result);
    maybeAppend(geo, result);
    maybeAppend(note, result);
    return result.toString();
  }
}
