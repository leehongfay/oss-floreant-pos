package com.google.zxing.maxicode.decoder;

import com.google.zxing.common.DecoderResult;
import java.text.DecimalFormat;
import java.text.NumberFormat;
























final class DecodedBitStreamParser
{
  private static final char SHIFTA = '￰';
  private static final char SHIFTB = '￱';
  private static final char SHIFTC = '￲';
  private static final char SHIFTD = '￳';
  private static final char SHIFTE = '￴';
  private static final char TWOSHIFTA = '￵';
  private static final char THREESHIFTA = '￶';
  private static final char LATCHA = '￷';
  private static final char LATCHB = '￸';
  private static final char LOCK = '￹';
  private static final char ECI = '￺';
  private static final char NS = '￻';
  private static final char PAD = '￼';
  private static final char FS = '\034';
  private static final char GS = '\035';
  private static final char RS = '\036';
  private static final NumberFormat NINE_DIGITS = new DecimalFormat("000000000");
  private static final NumberFormat THREE_DIGITS = new DecimalFormat("000");
  
  private static final String[] SETS = { "\nABCDEFGHIJKLMNOPQRSTUVWXYZ￺\034\035\036￻ ￼\"#$%&'()*+,-./0123456789:￱￲￳￴￸", "`abcdefghijklmnopqrstuvwxyz￺\034\035\036￻{￼}~;<=>?[\\]^_ ,./:@!|￼￵￶￼￰￲￳￴￷", "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚ￺\034\035\036ÛÜÝÞßª¬±²³µ¹º¼½¾￷ ￹￳￴￸", "àáâãäåæçèéêëìíîïðñòóôõö÷øùú￺\034\035\036￻ûüýþÿ¡¨«¯°´·¸»¿￷ ￲￹￴￸", "\000\001\002\003\004\005\006\007\b\t\n\013\f\r\016\017\020\021\022\023\024\025\026\027\030\031\032￺￼￼\033￻\034\035\036\037 ¢£¤¥¦§©­®¶￷ ￲￳￹￸", "\000\001\002\003\004\005\006\007\b\t\n\013\f\r\016\017\020\021\022\023\024\025\026\027\030\031\032\033\034\035\036\037 !\"#$%&'()*+,-./0123456789:;<=>?" };
  




  private DecodedBitStreamParser() {}
  



  static DecoderResult decode(byte[] bytes, int mode)
  {
    StringBuilder result = new StringBuilder(144);
    switch (mode) {
    case 2: case 3: 
      String postcode;
      String postcode;
      if (mode == 2) {
        int pc = getPostCode2(bytes);
        NumberFormat df = new DecimalFormat("0000000000".substring(0, getPostCode2Length(bytes)));
        postcode = df.format(pc);
      } else {
        postcode = getPostCode3(bytes);
      }
      String country = THREE_DIGITS.format(getCountry(bytes));
      String service = THREE_DIGITS.format(getServiceClass(bytes));
      result.append(getMessage(bytes, 10, 84));
      if (result.toString().startsWith("[)>\03601\035")) {
        result.insert(9, postcode + '\035' + country + '\035' + service + '\035');
      } else {
        result.insert(0, postcode + '\035' + country + '\035' + service + '\035');
      }
      break;
    case 4: 
      result.append(getMessage(bytes, 1, 93));
      break;
    case 5: 
      result.append(getMessage(bytes, 1, 77));
    }
    
    return new DecoderResult(bytes, result.toString(), null, String.valueOf(mode));
  }
  
  private static int getBit(int bit, byte[] bytes) {
    bit--;
    return (bytes[(bit / 6)] & 1 << 5 - bit % 6) == 0 ? 0 : 1;
  }
  
  private static int getInt(byte[] bytes, byte[] x) {
    if (x.length == 0) {
      throw new IllegalArgumentException();
    }
    int val = 0;
    for (int i = 0; i < x.length; i++) {
      val += (getBit(x[i], bytes) << x.length - i - 1);
    }
    return val;
  }
  
  private static int getCountry(byte[] bytes) {
    return getInt(bytes, new byte[] { 53, 54, 43, 44, 45, 46, 47, 48, 37, 38 });
  }
  
  private static int getServiceClass(byte[] bytes) {
    return getInt(bytes, new byte[] { 55, 56, 57, 58, 59, 60, 49, 50, 51, 52 });
  }
  
  private static int getPostCode2Length(byte[] bytes) {
    return getInt(bytes, new byte[] { 39, 40, 41, 42, 31, 32 });
  }
  
  private static int getPostCode2(byte[] bytes) {
    return getInt(bytes, new byte[] { 33, 34, 35, 36, 25, 26, 27, 28, 29, 30, 19, 20, 21, 22, 23, 24, 13, 14, 15, 16, 17, 18, 7, 8, 9, 10, 11, 12, 1, 2 });
  }
  
  private static String getPostCode3(byte[] bytes)
  {
    return String.valueOf(new char[] {SETS[0]
    
      .charAt(getInt(bytes, new byte[] { 39, 40, 41, 42, 31, 32 })), SETS[0]
      .charAt(getInt(bytes, new byte[] { 33, 34, 35, 36, 25, 26 })), SETS[0]
      .charAt(getInt(bytes, new byte[] { 27, 28, 29, 30, 19, 20 })), SETS[0]
      .charAt(getInt(bytes, new byte[] { 21, 22, 23, 24, 13, 14 })), SETS[0]
      .charAt(getInt(bytes, new byte[] { 15, 16, 17, 18, 7, 8 })), SETS[0]
      .charAt(getInt(bytes, new byte[] { 9, 10, 11, 12, 1, 2 })) });
  }
  

  private static String getMessage(byte[] bytes, int start, int len)
  {
    StringBuilder sb = new StringBuilder();
    int shift = -1;
    int set = 0;
    int lastset = 0;
    for (int i = start; i < start + len; i++) {
      char c = SETS[set].charAt(bytes[i]);
      switch (c) {
      case '￷': 
        set = 0;
        shift = -1;
        break;
      case '￸': 
        set = 1;
        shift = -1;
        break;
      case '￰': 
      case '￱': 
      case '￲': 
      case '￳': 
      case '￴': 
        lastset = set;
        set = c - 65520;
        shift = 1;
        break;
      case '￵': 
        lastset = set;
        set = 0;
        shift = 2;
        break;
      case '￶': 
        lastset = set;
        set = 0;
        shift = 3;
        break;
      case '￻': 
        int nsval = (bytes[(++i)] << 24) + (bytes[(++i)] << 18) + (bytes[(++i)] << 12) + (bytes[(++i)] << 6) + bytes[(++i)];
        sb.append(NINE_DIGITS.format(nsval));
        break;
      case '￹': 
        shift = -1;
        break;
      case '￺': default: 
        sb.append(c);
      }
      if (shift-- == 0) {
        set = lastset;
      }
    }
    while ((sb.length() > 0) && (sb.charAt(sb.length() - 1) == 65532)) {
      sb.setLength(sb.length() - 1);
    }
    return sb.toString();
  }
}
