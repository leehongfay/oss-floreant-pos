package com.google.zxing;

import com.google.zxing.aztec.AztecWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.datamatrix.DataMatrixWriter;
import com.google.zxing.oned.CodaBarWriter;
import com.google.zxing.oned.Code128Writer;
import com.google.zxing.oned.Code39Writer;
import com.google.zxing.oned.EAN13Writer;
import com.google.zxing.oned.EAN8Writer;
import com.google.zxing.oned.ITFWriter;
import com.google.zxing.oned.UPCAWriter;
import com.google.zxing.pdf417.PDF417Writer;
import com.google.zxing.qrcode.QRCodeWriter;
import java.util.Map;

























public final class MultiFormatWriter
  implements Writer
{
  public MultiFormatWriter() {}
  
  public BitMatrix encode(String contents, BarcodeFormat format, int width, int height)
    throws WriterException { return encode(contents, format, width, height, null); }
  
  public BitMatrix encode(String contents, BarcodeFormat format, int width, int height, Map<EncodeHintType, ?> hints) throws WriterException { Writer writer;
    Writer writer;
    Writer writer;
    Writer writer;
    Writer writer;
    Writer writer;
    Writer writer;
    Writer writer;
    Writer writer; Writer writer; Writer writer; switch (1.$SwitchMap$com$google$zxing$BarcodeFormat[format.ordinal()]) {
    case 1: 
      writer = new EAN8Writer();
      break;
    case 2: 
      writer = new EAN13Writer();
      break;
    case 3: 
      writer = new UPCAWriter();
      break;
    case 4: 
      writer = new QRCodeWriter();
      break;
    case 5: 
      writer = new Code39Writer();
      break;
    case 6: 
      writer = new Code128Writer();
      break;
    case 7: 
      writer = new ITFWriter();
      break;
    case 8: 
      writer = new PDF417Writer();
      break;
    case 9: 
      writer = new CodaBarWriter();
      break;
    case 10: 
      writer = new DataMatrixWriter();
      break;
    case 11: 
      writer = new AztecWriter();
      break;
    default: 
      throw new IllegalArgumentException("No encoder available for format " + format); }
    Writer writer;
    return writer.encode(contents, format, width, height, hints);
  }
}
