package com.google.zxing;

import com.google.zxing.common.BitArray;
import com.google.zxing.common.BitMatrix;
























public abstract class Binarizer
{
  private final LuminanceSource source;
  
  protected Binarizer(LuminanceSource source)
  {
    this.source = source;
  }
  
  public final LuminanceSource getLuminanceSource() {
    return source;
  }
  







  public abstract BitArray getBlackRow(int paramInt, BitArray paramBitArray)
    throws NotFoundException;
  







  public abstract BitMatrix getBlackMatrix()
    throws NotFoundException;
  







  public abstract Binarizer createBinarizer(LuminanceSource paramLuminanceSource);
  







  public final int getWidth()
  {
    return source.getWidth();
  }
  
  public final int getHeight() {
    return source.getHeight();
  }
}
