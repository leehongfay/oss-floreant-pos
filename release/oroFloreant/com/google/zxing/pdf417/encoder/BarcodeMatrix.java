package com.google.zxing.pdf417.encoder;






public final class BarcodeMatrix
{
  private final BarcodeRow[] matrix;
  




  private int currentRow;
  




  private final int height;
  




  private final int width;
  




  BarcodeMatrix(int height, int width)
  {
    matrix = new BarcodeRow[height];
    
    int i = 0; for (int matrixLength = matrix.length; i < matrixLength; i++) {
      matrix[i] = new BarcodeRow((width + 4) * 17 + 1);
    }
    this.width = (width * 17);
    this.height = height;
    currentRow = -1;
  }
  
  void set(int x, int y, byte value) {
    matrix[y].set(x, value);
  }
  





  void startRow()
  {
    currentRow += 1;
  }
  
  BarcodeRow getCurrentRow() {
    return matrix[currentRow];
  }
  
  public byte[][] getMatrix() {
    return getScaledMatrix(1, 1);
  }
  





  public byte[][] getScaledMatrix(int xScale, int yScale)
  {
    byte[][] matrixOut = new byte[height * yScale][width * xScale];
    int yMax = height * yScale;
    for (int i = 0; i < yMax; i++) {
      matrixOut[(yMax - i - 1)] = matrix[(i / yScale)].getScaledRow(xScale);
    }
    return matrixOut;
  }
}
