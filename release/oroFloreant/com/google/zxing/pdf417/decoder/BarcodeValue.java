package com.google.zxing.pdf417.decoder;

import com.google.zxing.pdf417.PDF417Common;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;




















final class BarcodeValue
{
  private final Map<Integer, Integer> values = new HashMap();
  
  BarcodeValue() {}
  
  void setValue(int value)
  {
    Integer confidence = (Integer)values.get(Integer.valueOf(value));
    if (confidence == null) {
      confidence = Integer.valueOf(0);
    }
    Integer localInteger1 = confidence;Integer localInteger2 = confidence = Integer.valueOf(confidence.intValue() + 1);
    values.put(Integer.valueOf(value), confidence);
  }
  



  int[] getValue()
  {
    int maxConfidence = -1;
    Collection<Integer> result = new ArrayList();
    for (Map.Entry<Integer, Integer> entry : values.entrySet()) {
      if (((Integer)entry.getValue()).intValue() > maxConfidence) {
        maxConfidence = ((Integer)entry.getValue()).intValue();
        result.clear();
        result.add(entry.getKey());
      } else if (((Integer)entry.getValue()).intValue() == maxConfidence) {
        result.add(entry.getKey());
      }
    }
    return PDF417Common.toIntArray(result);
  }
  
  public Integer getConfidence(int value) {
    return (Integer)values.get(Integer.valueOf(value));
  }
}
