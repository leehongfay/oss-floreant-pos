package com.orocube.orocust.callerid;

public abstract interface CallButtonClickListener
{
  public abstract void callButtonClicked(int paramInt, String paramString1, String paramString2);
}
