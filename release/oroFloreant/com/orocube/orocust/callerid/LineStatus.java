package com.orocube.orocust.callerid;

public enum LineStatus {
  RING_ON("Ringing"),  MISSED_CALL("Missed Call"),  RECEIVED("Received"),  CALL_IN_PROGRESS("Call In Progress"),  CALL_ENDED("Call Ended"),  NOTHING(" ");
  
  private String status;
  private static boolean callInProgress = false;
  
  private LineStatus(String status) {
    this.status = status;
  }
  
  public String toString()
  {
    return status;
  }
  
  public LineStatus getStatus(int event, int param) {
    if (event == 8) {
      if (param == 7) {
        return RING_ON;
      }
      if (param == 8) {
        return RING_ON;
      }
    } else {
      if (event == 170) {
        return MISSED_CALL;
      }
      if (event == 187) {
        if (isCallInProgress()) {
          return CALL_IN_PROGRESS;
        }
        callInProgress = true;
        return RECEIVED;
      } }
    return NOTHING;
  }
  
  private static boolean isCallInProgress() {
    return callInProgress;
  }
}
