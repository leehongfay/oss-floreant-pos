package com.orocube.orocust.callerid;

import com.floreantpos.main.Application;
import com.orocube.orocust.callerid.ad101.AD101Device;
import com.orocube.orocust.callerid.ad101.AD101Emulator;
import com.sun.jna.Native;















public class CallerIdDeviceFactory
{
  public CallerIdDeviceFactory() {}
  
  public static AD101Device getAD101Device()
  {
    if (Application.getInstance().isDevelopmentMode()) {
      return new AD101Emulator();
    }
    
    return (AD101Device)Native.loadLibrary("AD101Device", AD101Device.class);
  }
}
