package com.orocube.orocust.callerid.ad101;

import com.sun.jna.Pointer;

public class DemoAD101Device implements AD101Device {
  public DemoAD101Device() {}
  
  public int AD101_GetCurDevCount() {
    return 0;
  }
  
  public int AD101_GetDevice()
  {
    return 0;
  }
  
  public int AD101_InitDevice(long hWnd)
  {
    return 0;
  }
  


  public void AD101_SetDialOutStartTalkingTime(int nSecond) {}
  


  public void AD101_SetRingOffTime(int nSecond) {}
  


  public void AD101_SetEventCallbackFun(AD101Device.AD101Callback fun) {}
  


  public int AD101_GetCallerID(int dwChannel, Pointer szCallerIDBuffer, Pointer szName, Pointer szTime)
  {
    return 0;
  }
}
