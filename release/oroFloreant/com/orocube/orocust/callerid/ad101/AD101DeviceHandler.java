package com.orocube.orocust.callerid.ad101;

import com.floreantpos.PosLog;
import com.orocube.orocust.callerid.CallListener;
import com.orocube.orocust.callerid.CallerIdDeviceFactory;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import org.apache.commons.lang.StringUtils;

public class AD101DeviceHandler implements AD101Device.AD101Callback
{
  private CallListener callListener;
  private AD101Device ad101Device;
  
  public AD101DeviceHandler(CallListener callListener)
  {
    this.callListener = callListener;
    
    ad101Device = CallerIdDeviceFactory.getAD101Device();
    






    if (ad101Device.AD101_InitDevice(0L) == 0) {
      PosLog.info(getClass(), "AD101 device could not be initialized or already initialized.");
      return;
    }
    
    ad101Device.AD101_SetEventCallbackFun(this);
  }
  
  public void EVENTCALLBACKPROC(int line, int event, int param)
  {
    PosLog.info(getClass(), "line: " + line + ", event: " + event + ", param: " + param);
    
    if (event == 9) {
      Pointer szCallerID = new Memory(128L);
      Pointer szName = new Memory(128L);
      Pointer szTime = new Memory(128L);
      
      String callerId = null;
      String callerName = null;
      String callTime = null;
      
      ad101Device.AD101_GetCallerID(line, szCallerID, szName, szTime);
      
      callerId = szCallerID.getString(0L);
      callerName = szName.getString(0L);
      callTime = szTime.getString(0L);
      
      PosLog.info(getClass(), "caller id:" + callerId);
      PosLog.info(getClass(), "name:" + callerName);
      PosLog.info(getClass(), "time:" + callTime);
      
      if ((StringUtils.isEmpty(callerId)) || (callerId.startsWith("BB1"))) {
        callListener.callEnd(line);
        return;
      }
      
      callListener.callArrived(line, callerId, callerName, callTime);
      return;
    }
    
    if (event == 170) {
      callListener.callMissed(line);
    }
    else if (event == 187) {
      callListener.callReceived(line);
    }
  }
}
