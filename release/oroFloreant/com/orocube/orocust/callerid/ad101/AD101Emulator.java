package com.orocube.orocust.callerid.ad101;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.sun.jna.Pointer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.border.LineBorder;
import net.miginfocom.swing.MigLayout;




















public class AD101Emulator
  extends POSDialog
  implements ActionListener, AD101Device
{
  private int defaultValue;
  private static JTextField tfNumber;
  private PosButton btnCall;
  private PosButton btnEndCall;
  private PosButton btnReceiveCall;
  private JPanel headerPanel;
  private JLabel lblHeader;
  private boolean floatingPoint;
  Font f = new Font("Verdana", 1, 16);
  Color color = Color.WHITE;
  
  private JPanel contentPane;
  
  private JPanel callingPane;
  private Timer callingTimer;
  private JLabel lblNumber;
  private JLabel lblTime = new JLabel("");
  
  private JPanel centerPanel;
  private static AD101Device.AD101Callback fun;
  int line;
  private CallerServiceListener listener;
  
  public AD101Emulator()
  {
    super(POSUtil.getFocusedWindow(), "AD101");
    setIconImage(Application.getApplicationIcon().getImage());
    init();
    tfNumber.requestFocus();
  }
  
  private void init() {
    setLayout(new BorderLayout(0, 0));
    setResizable(false);
    setLocationRelativeTo(null);
    setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
    setModalityType(Dialog.ModalityType.MODELESS);
    
    lblHeader = new JLabel("Call");
    lblHeader.setFont(f);
    lblHeader.setForeground(color);
    
    headerPanel = new JPanel(new MigLayout("inset 0,fill"));
    headerPanel.add(lblHeader, "h 40!,wrap");
    headerPanel.setBackground(Color.black);
    
    JSeparator sep = new JSeparator();
    sep.setBackground(Color.white);
    sep.setForeground(Color.white);
    
    headerPanel.add(sep, "grow,h 1!,span");
    
    add(headerPanel, "North");
    
    Dimension size = PosUIManager.getSize_w100_h70();
    
    centerPanel = new JPanel(new MigLayout("inset 0,fill, hidemode 3"));
    contentPane = new JPanel(new MigLayout("fill"));
    
    tfNumber = new JTextField();
    tfNumber.setText(String.valueOf(defaultValue));
    tfNumber.setFont(tfNumber.getFont().deriveFont(0, PosUIManager.getNumberFieldFontSize() + 4));
    tfNumber.setFocusable(true);
    tfNumber.setBorder(null);
    tfNumber.setHorizontalAlignment(0);
    tfNumber.setBackground(new Color(10, 21, 24));
    tfNumber.setForeground(color);
    
    contentPane.add(tfNumber, "span,h 60!, grow");
    contentPane.setBackground(new Color(0, 21, 26));
    
    String[][] numbers = { { "7", "8", "9" }, { "4", "5", "6" }, { "1", "2", "3" }, { "+", "0", "X" } };
    Font font = new Font("Arail", 1, 30);
    
    for (int i = 0; i < numbers.length; i++) {
      for (int j = 0; j < numbers[i].length; j++) {
        PosButton posButton = new PosButton();
        posButton.setFocusable(false);
        String buttonText = String.valueOf(numbers[i][j]);
        
        posButton.setText(buttonText);
        posButton.setFont(font);
        posButton.setForeground(new Color(2, 175, 215));
        posButton.setBackground(new Color(0, 21, 26));
        posButton.setBorder(new LineBorder(new Color(0, 27, 30), 1, true));
        
        posButton.setActionCommand(buttonText);
        posButton.addActionListener(this);
        String constraints = "grow,w " + width + "!,h " + height + "!";
        if (j == numbers[i].length - 1) {
          constraints = constraints + ",wrap";
        }
        contentPane.add(posButton, constraints);
      }
    }
    
    JSeparator jSeparator1 = new JSeparator();
    
    jSeparator1.setBackground(Color.black);
    jSeparator1.setForeground(Color.black);
    
    btnCall = new PosButton(IconFactory.getIcon("/images/", "call.png"));
    btnEndCall = new PosButton(IconFactory.getIcon("/images/", "callend.png"));
    btnReceiveCall = new PosButton(IconFactory.getIcon("/images/", "callreceive.png"));
    
    btnCall.setBorder(null);
    btnEndCall.setBorder(null);
    btnReceiveCall.setBorder(null);
    
    JPanel southPanel = new JPanel(new MigLayout("fill, inset 0"));
    
    Dimension btnSize = PosUIManager.getSize(100, 70);
    
    southPanel.add(jSeparator1, "newline, grow,h 3!, span");
    
    btnCall.setPreferredSize(btnSize);
    btnCall.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        doCall();
      }
      
    });
    southPanel.add(btnCall, "grow");
    
    btnReceiveCall.setPreferredSize(btnSize);
    btnReceiveCall.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        doReceiveCall();
      }
      
    });
    southPanel.add(btnReceiveCall, "grow");
    
    btnEndCall.setPreferredSize(btnSize);
    btnEndCall.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        endCall();
      }
      
    });
    southPanel.add(btnEndCall, "grow");
    
    btnEndCall.setBackground(new Color(2, 32, 38));
    btnCall.setBackground(new Color(2, 32, 38));
    btnReceiveCall.setBackground(new Color(2, 32, 38));
    
    btnReceiveCall.setEnabled(false);
    
    southPanel.setBackground(new Color(2, 32, 38));
    
    centerPanel.add(contentPane, "grow,wrap");
    
    add(centerPanel, "Center");
    add(southPanel, "South");
    
    createCallingPanel();
  }
  
  public void createCallingPanel() {
    callingPane = new JPanel(new MigLayout("fill"));
    
    lblNumber = new JLabel("");
    lblNumber.setFont(lblNumber.getFont().deriveFont(0, PosUIManager.getNumberFieldFontSize() + 4));
    lblNumber.setBorder(null);
    lblNumber.setHorizontalAlignment(0);
    lblNumber.setBackground(new Color(10, 21, 24));
    lblNumber.setForeground(color);
    
    lblTime.setFont(lblNumber.getFont().deriveFont(0, PosUIManager.getNumberFieldFontSize() + 4));
    lblTime.setBorder(null);
    lblTime.setHorizontalAlignment(0);
    lblTime.setBackground(new Color(10, 21, 24));
    lblTime.setForeground(color);
    
    callingPane.add(lblNumber, "span, grow");
    callingPane.add(lblTime, "span, grow");
    callingPane.setBackground(new Color(89, 117, 8));
    
    centerPanel.add(callingPane, "grow,wrap");
    callingPane.setVisible(false);
  }
  
  public void doCall() {
    String number = tfNumber.getText();
    if ((fun == null) || (number.isEmpty())) {
      return;
    }
    listener = new CallerServiceListener(null);
    listener.callButtonPressed();
    
    callingTimer = new Timer(1000, listener);
    listener.update(0, 8, 7);
    callingTimer.start();
    
    lblNumber.setText("<html>" + number + "<br><h1>Calling....</h1><html>");
    contentPane.setVisible(false);
    callingPane.setVisible(true);
    btnCall.setEnabled(false);
    btnReceiveCall.setEnabled(true);
    btnEndCall.setEnabled(true);
  }
  
  protected void doReceiveCall() {
    if (fun == null) {
      return;
    }
    if ((callingTimer != null) && (callingTimer.isRunning())) {
      callingTimer.stop();
    }
    listener = new CallerServiceListener(null);
    listener.callReceived();
    
    callingTimer = new Timer(1000, listener);
    listener.update(0, 187, 0);
    callingTimer.start();
    
    String number = tfNumber.getText();
    lblNumber.setText("<html>" + number + "<br><h1>Call In Progress..</h1><html>");
    contentPane.setVisible(false);
    callingPane.setVisible(true);
    btnCall.setEnabled(false);
    btnReceiveCall.setEnabled(false);
    btnEndCall.setEnabled(true);
  }
  
  protected void endCall() {
    if ((fun == null) || (listener == null)) {
      return;
    }
    lblNumber.setText("");
    contentPane.setVisible(true);
    callingPane.setVisible(false);
    
    if (fun == null) {
      return;
    }
    if ((callingTimer != null) && (callingTimer.isRunning())) {
      callingTimer.stop();
    }
    
    if (!listener.isCallInProgress()) {
      listener.missedCall();
    }
    else {
      listener.endCall();
    }
    lblNumber.setText("");
    tfNumber.requestFocus();
    btnCall.setEnabled(true);
    btnReceiveCall.setEnabled(false);
  }
  
  public void setCallerService(AD101Device.AD101Callback fun, int line) {
    fun = fun;
    this.line = line;
  }
  
  private void doClearAll() {
    tfNumber.setText("");
  }
  
  private void doInsertNumber(String number) {
    String s = tfNumber.getText();
    s = s + number;
    if (!validate(s)) {
      POSMessageDialog.showError(this, POSConstants.INVALID_NUMBER);
      return;
    }
    tfNumber.setText(s);
  }
  
  private void doInsertDot() {
    String string = tfNumber.getText() + "+";
    if (!validate(string)) {
      POSMessageDialog.showError(this, POSConstants.INVALID_NUMBER);
      return;
    }
    tfNumber.setText(string);
  }
  
  public void actionPerformed(ActionEvent e) {
    String actionCommand = e.getActionCommand();
    
    if (POSConstants.OK.equalsIgnoreCase(actionCommand)) {
      doCall();
    }
    else if (actionCommand.equals("X")) {
      doClearAll();
    }
    else if (actionCommand.equals("+")) {
      doInsertDot();
    }
    else {
      doInsertNumber(actionCommand);
    }
  }
  
  private boolean validate(String str) {
    try {
      Integer.parseInt(str);
    } catch (Exception x) {
      return false;
    }
    return true;
  }
  
  public void setTitle(String title) {
    super.setTitle(title);
  }
  
  public void setDialogTitle(String title) {
    super.setTitle(title);
  }
  
  public static void main(String[] args) {
    AD101Emulator dialog2 = new AD101Emulator();
    dialog2.pack();
    dialog2.setVisible(true); }
  
  private class CallerServiceListener implements ActionListener { int line;
    int event;
    int param;
    boolean callInProgress;
    
    private CallerServiceListener() {}
    Calendar cal = Calendar.getInstance();
    
    public void update(int line, int event, int param) {
      this.line = line;
      this.event = event;
      this.param = param;
    }
    
    public void endCall() {
      AD101Emulator.fun.EVENTCALLBACKPROC(0, 8, 1);
      AD101Emulator.tfNumber.setText("");
      lblTime.setText("");
    }
    
    public void missedCall() {
      AD101Emulator.fun.EVENTCALLBACKPROC(0, 170, 0);
      AD101Emulator.tfNumber.setText("");
      lblTime.setText("");
    }
    
    public void callReceived() {
      AD101Emulator.fun.EVENTCALLBACKPROC(0, 187, 0);
      callInProgress = true;
      cal.set(10, 0);
      cal.set(12, 0);
      cal.set(13, 0);
    }
    
    public void callButtonPressed() {
      callInProgress = false;
      AD101Emulator.fun.EVENTCALLBACKPROC(0, 9, 0);
    }
    
    public void actionPerformed(ActionEvent e)
    {
      AD101Emulator.fun.EVENTCALLBACKPROC(line, event, param);
      if (callInProgress) {
        cal.set(13, cal.get(13) + 1);
        lblTime.setText("Duration:" + cal.get(12) + ":" + cal.get(13));
      }
    }
    
    public boolean isCallInProgress() {
      return callInProgress;
    }
  }
  
  public int AD101_GetCurDevCount()
  {
    return 0;
  }
  
  public int AD101_GetDevice()
  {
    return 0;
  }
  
  public int AD101_InitDevice(long hWnd)
  {
    AD101Emulator dialog2 = new AD101Emulator();
    dialog2.pack();
    dialog2.setVisible(true);
    return 1;
  }
  
  public void setVisible(boolean b)
  {
    setLocationRelativeTo(getParent());
    super.setVisible(b);
  }
  


  public void AD101_SetDialOutStartTalkingTime(int nSecond) {}
  


  public void AD101_SetRingOffTime(int nSecond) {}
  


  public void AD101_SetEventCallbackFun(AD101Device.AD101Callback fun)
  {
    fun = fun;
  }
  
  public int AD101_GetCallerID(int dwChannel, Pointer szCallerIDBuffer, Pointer szName, Pointer szTime)
  {
    szCallerIDBuffer.setString(0L, tfNumber.getText());
    return 1;
  }
}
