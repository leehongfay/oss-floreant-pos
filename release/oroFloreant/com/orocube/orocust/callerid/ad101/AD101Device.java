package com.orocube.orocust.callerid.ad101;

import com.sun.jna.Pointer;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.StdCallLibrary.StdCallCallback;

public abstract interface AD101Device
  extends StdCallLibrary
{
  public abstract int AD101_GetCurDevCount();
  
  public abstract int AD101_GetDevice();
  
  public abstract int AD101_InitDevice(long paramLong);
  
  public abstract void AD101_SetDialOutStartTalkingTime(int paramInt);
  
  public abstract void AD101_SetRingOffTime(int paramInt);
  
  public abstract void AD101_SetEventCallbackFun(AD101Callback paramAD101Callback);
  
  public abstract int AD101_GetCallerID(int paramInt, Pointer paramPointer1, Pointer paramPointer2, Pointer paramPointer3);
  
  public static abstract interface AD101Callback
    extends StdCallLibrary.StdCallCallback
  {
    public abstract void EVENTCALLBACKPROC(int paramInt1, int paramInt2, int paramInt3);
  }
}
