package com.orocube.orocust.callerid.whozz;

import com.floreantpos.PosLog;
import com.orocube.orocust.callerid.CallListener;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;













public class WhozzCallReceiver
  extends Thread
{
  private boolean stopped = false;
  private DatagramSocket socket;
  private final String logEventMsg = "Starting phone call event listening on port 3520";
  private CallListener callListener;
  
  public WhozzCallReceiver(CallListener callListener) throws Exception
  {
    this.callListener = callListener;
    socket = new DatagramSocket(3520);
  }
  
  public void halt() {
    stopped = true;
  }
  
  public void run() {
    byte[] buffer = new byte[65507];
    for (;;) {
      if (stopped)
        return;
      DatagramPacket dp = new DatagramPacket(buffer, buffer.length);
      try {
        socket.receive(dp);
        String s = new String(dp.getData(), 0, dp.getLength());
        

        PosLog.info(getClass(), "Starting phone call event listening on port 3520");
        PosLog.info(getClass(), s);
        handleCall(s);
        
        Thread.yield();
      } catch (IOException ex) {
        PosLog.error(getClass(), ex);
      }
    }
  }
  
  public void handleCall(String data) {
    String myData = data;
    
    Integer myLine = Integer.valueOf(0);
    String myType = "";
    String myIndicator = "";
    String myDuration = "";
    String myCheckSum = "";
    String ringType = "";
    String myDateTime = "";
    String myNumber = "";
    String myName = "";
    

    Pattern myPattern = Pattern.compile(".*(\\d\\d) ([IO]) ([ES]) (\\d{4}) ([GB]) (.)(\\d) (\\d\\d/\\d\\d \\d\\d:\\d\\d [AP]M) (.{8,15})(.*)");
    Matcher matcher = myPattern.matcher(myData);
    
    if (matcher.find() == true)
    {
      myLine = Integer.valueOf(Integer.parseInt(matcher.group(1)));
      myType = matcher.group(2);
      
      if ((myType.equals("I")) || (myType.equals("O")))
      {
        myIndicator = matcher.group(3);
        myDuration = matcher.group(4);
        myCheckSum = matcher.group(5);
        ringType = matcher.group(6);
        myDateTime = matcher.group(8);
        myNumber = matcher.group(9);
        myName = matcher.group(10);
      }
    }
    


    PosLog.info(getClass(), "myIndicator: " + myIndicator);
    PosLog.info(getClass(), "myDuration: " + myDuration);
    PosLog.info(getClass(), "myCheckSum: " + myCheckSum);
    PosLog.info(getClass(), "myRings: " + ringType);
    PosLog.info(getClass(), "myName: " + myName);
    
    Pattern myPatternDetailed = Pattern.compile(".*(\\d\\d) ([NFR]) {13}(\\d\\d/\\d\\d \\d\\d:\\d\\d:\\d\\d)");
    Matcher matcherDetailed = myPatternDetailed.matcher(myData);
    
    if (matcherDetailed.find() == true)
    {
      myLine = Integer.valueOf(Integer.parseInt(matcherDetailed.group(1)));
      myType = matcherDetailed.group(2);
      
      if ((myType.equals("N")) || (myType.equals("F")) || (myType.equals("R"))) {
        myDateTime = matcherDetailed.group(3);
      }
    }
    

    String command = myType + myIndicator;
    
    PosLog.info(getClass(), "line: " + myLine);
    PosLog.info(getClass(), "callerId: " + myNumber);
    PosLog.info(getClass(), "callerName: " + myName);
    PosLog.info(getClass(), "time: " + myDateTime);
    PosLog.info(getClass(), "command: " + command);
    

    switch (command)
    {


    case "R": 
      PosLog.info(getClass(), "ring on: number: " + myNumber);
      callListener.callArrived(myLine.intValue(), myNumber, myName, myDateTime);
      
      break;
    


    case "IS": 
      callListener.callArrived(myLine.intValue(), myNumber, myName, myDateTime);
      
      if (Integer.parseInt(myDuration) > 0) {
        callListener.callReceived(myLine.intValue());
      }
      


      break;
    case "F": 
      callListener.callReceived(myLine.intValue());
      break;
    
    case "N": 
      callListener.callEnd(myLine.intValue());
      
      break;
    
    case "IE": 
      if (Integer.parseInt(myDuration) > 0) {
        callListener.callReceived(myLine.intValue());
      }
      callListener.callEnd(myLine.intValue());
      break;
    case "OS": 
      break;
    }
    
  }
}
