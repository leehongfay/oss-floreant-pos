package com.orocube.orocust.callerid;

public abstract interface CallListener
{
  public abstract void callArrived(int paramInt, String paramString1, String paramString2, String paramString3);
  
  public abstract void callReceived(int paramInt);
  
  public abstract void callMissed(int paramInt);
  
  public abstract void callEnd(int paramInt);
}
