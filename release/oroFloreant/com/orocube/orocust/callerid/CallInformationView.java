package com.orocube.orocust.callerid;

import com.floreantpos.PosLog;
import com.floreantpos.main.Application;
import com.floreantpos.model.Customer;
import com.floreantpos.model.Store;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.swing.PaginatedListModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.forms.QuickCustomerForm;
import com.floreantpos.ui.views.order.SelectionView;
import com.orocube.orocust.callerid.ad101.AD101DeviceHandler;
import com.orocube.orocust.callerid.whozz.WhozzCallReceiver;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;

























public class CallInformationView
  extends SelectionView
  implements CallListener
{
  private static final long serialVersionUID = 1L;
  HashMap<String, PhoneCallButton> callerIdMap = new HashMap();
  HashMap<Integer, PhoneCallButton> lineMap = new HashMap();
  
  private CallButtonClickListener callButtonClickListener;
  private AD101DeviceHandler ad101DeviceHandler;
  private ActionListener listener;
  private PosButton btnClearAll;
  
  public CallInformationView(CallButtonClickListener callButtonClickListener)
  {
    super("LINE", new MigLayout("inset 5 2 0 2,alignx center, aligny top,wrap 1,fillx", "sg, fill", ""), PosUIManager.getSize(160), 
      PosUIManager.getSize(110));
    this.callButtonClickListener = callButtonClickListener;
    listener = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        CallInformationView.PhoneCallButton button = (CallInformationView.PhoneCallButton)e.getSource();
        button.buttonClicked();
      }
    };
    setDataModel(new PaginatedListModel(100));
    
    setPreferredSize(new Dimension(PosUIManager.getSize(180, 130)));
    
    btnPrev.setText("UP");
    btnNext.setText("DOWN");
    
    btnClearAll = new PosButton("Clear All");
    btnClearAll.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        List<CallInformationView.PhoneCallButton> list = dataModel.getDataList();
        if (list == null) {
          return;
        }
        lineMap.clear();
        callerIdMap.clear();
        dataModel.setData(new ArrayList());
        renderItems();
      }
    });
    actionButtonPanel.add(btnClearAll, "newline,split 2,growx,gapbottom 5");
    try
    {
      String callerIdDevice = Application.getInstance().getStore().getProperty("callerId.device");
      PosLog.info(getClass(), "Caller id device: " + callerIdDevice);
      if (callerIdDevice.equals("Whozz calling")) {
        WhozzCallReceiver whozzCallReceiver = new WhozzCallReceiver(this);
        whozzCallReceiver.start();
      }
      else if (callerIdDevice.equals("AD101")) {
        ad101DeviceHandler = new AD101DeviceHandler(this);
      }
    }
    catch (Exception e) {
      PosLog.error(getClass(), "Could not register device.");
    }
  }
  
  protected void renderItems()
  {
    boolean existsCallList = (dataModel.getDataList() != null) && (!dataModel.getDataList().isEmpty());
    if (existsCallList) {
      Collections.sort(dataModel.getDataList(), new Comparator()
      {
        public int compare(CallInformationView.PhoneCallButton o1, CallInformationView.PhoneCallButton o2)
        {
          return o2.getSortOrder() - o1.getSortOrder();
        }
      });
    }
    super.renderItems();
    setVisible(existsCallList);
    if (btnClearAll != null)
      btnClearAll.setVisible(existsCallList);
  }
  
  public void initialize() {
    dataModel.setData(new ArrayList());
  }
  
  protected AbstractButton createItemButton(Object item)
  {
    PhoneCallButton button = (PhoneCallButton)item;
    if (button != null) {
      button.addActionListener(listener);
    }
    return button;
  }
  
  public void setCallButtonClickListener(CallButtonClickListener callButtonClickListener) {
    this.callButtonClickListener = callButtonClickListener;
  }
  
  protected void updateView(PhoneCallButton lineButton, boolean newButton)
  {
    List list = dataModel.getDataList();
    
    if (list == null) {
      list = new ArrayList();
    }
    
    if (!list.contains(lineButton)) {
      list.add(lineButton);
    }
    dataModel.setData(list);
    renderItems();
  }
  
  private void removeCallButton(PhoneCallButton callButton)
  {
    List<PhoneCallButton> list = dataModel.getDataList();
    if (list == null) {
      return;
    }
    lineMap.remove(Integer.valueOf(line));
    callerIdMap.remove(callerId);
    callButton.removeActionListener(listener);
    list.remove(callButton);
    renderItems();
  }
  
  public void cleanup() {}
  
  private void createNewCallButton(final int line, final String callerId, final String callerName, final String callTime)
  {
    Thread thread = new Thread(new Runnable()
    {
      public void run()
      {
        List<Customer> customerList = CustomerDAO.getInstance().findByMobileNumber(callerId);
        Customer customer = null;
        
        if ((customerList != null) && (customerList.size() > 0)) {
          customer = (Customer)customerList.get(0);
        }
        
        CallInformationView.PhoneCallButton callButton = new CallInformationView.PhoneCallButton(CallInformationView.this, line, callerId, callerName, callTime);
        callButton.setCustomer(customer);
        callButton.setStatus(LineStatus.RING_ON);
        callButton.setSortOrder((int)System.currentTimeMillis());
        
        updateView(callButton, true);
        
        callerIdMap.put(StringUtils.deleteWhitespace(callerId).replaceAll("-", ""), callButton);
        lineMap.put(Integer.valueOf(line), callButton);
      }
    });
    thread.start();
  }
  
  public void callArrived(int line, String callerId, String callerName, String callTime) {
    PhoneCallButton existingButton = (PhoneCallButton)callerIdMap.get(StringUtils.deleteWhitespace(callerId).replaceAll("-", ""));
    





    if (existingButton == null) {
      createNewCallButton(line, callerId, callerName, callTime);
      return;
    }
    
    if ((existingButton.getStatus() == LineStatus.MISSED_CALL) || (existingButton.getStatus() == LineStatus.CALL_ENDED)) {
      existingButton.setStatus(LineStatus.RING_ON);
      existingButton.setSortOrder((int)System.currentTimeMillis());
      lineMap.put(Integer.valueOf(line), existingButton);
      updateView(existingButton, false);
    }
    else {
      createNewCallButton(line, callerId, callerName, callTime);
    }
  }
  
  public void callReceived(int line) {
    PhoneCallButton existingButton = (PhoneCallButton)lineMap.get(Integer.valueOf(line));
    if (existingButton == null) {
      return;
    }
    
    existingButton.setStatus(LineStatus.CALL_IN_PROGRESS);
    updateView(existingButton, false);
  }
  
  public void callMissed(int line) {
    PhoneCallButton existingButton = (PhoneCallButton)lineMap.get(Integer.valueOf(line));
    if (existingButton == null) {
      return;
    }
    
    existingButton.setStatus(LineStatus.MISSED_CALL);
    updateView(existingButton, false);
  }
  
  public void callEnd(int line) {
    PhoneCallButton existingButton = (PhoneCallButton)lineMap.get(Integer.valueOf(line));
    if (existingButton == null)
    {
      return;
    }
    
    if (existingButton.getStatus().equals(LineStatus.CALL_IN_PROGRESS)) {
      existingButton.setStatus(LineStatus.CALL_ENDED);
    }
    else {
      existingButton.setStatus(LineStatus.MISSED_CALL);
    }
    lineMap.remove(Integer.valueOf(line));
    
    updateView(existingButton, false);
  }
  
  class PhoneCallButton extends PosButton
  {
    Customer customer;
    private int line;
    private String callerId = "";
    private String callerName = "";
    private String callTime = "";
    
    private int missedCallCount;
    
    private LineStatus lineStatus;
    private int sortOrder;
    
    public PhoneCallButton() {}
    
    public PhoneCallButton(int line, String callerId, String callerName, String time)
    {
      this.line = line;
      this.callerId = StringUtils.deleteWhitespace(callerId).replaceAll("-", "");
      this.callerName = callerName;
      callTime = time;
    }
    
    public void update() {
      String text = "<html>";
      text = text + "<b>LINE- " + line + "</b><br>";
      
      if (customer != null) {
        if (customer.getMobileNo() != null) {
          text = text + customer.getMobileNo() + "<br>";
        }
      } else {
        text = text + callerId + "<br>";
      }
      
      text = text + lineStatus.toString();
      if (lineStatus == LineStatus.MISSED_CALL) {
        text = text + " (" + missedCallCount + ")";
      }
      text = text + "<br>";
      text = text + "</html>";
      
      setText(text);
    }
    
    public boolean equals(Object o)
    {
      if (!(o instanceof PhoneCallButton)) {
        return false;
      }
      
      PhoneCallButton that = (PhoneCallButton)o;
      
      String thisCallerId = callerId;
      String thatCallerId = callerId;
      
      return StringUtils.equals(thisCallerId, thatCallerId);
    }
    
    public void buttonClicked() {
      if (callButtonClickListener != null) {
        callButtonClickListener.callButtonClicked(line, callerId, callerName);
      }
      
      CallInformationView.this.removeCallButton(this);
    }
    
    public LineStatus getStatus() {
      return lineStatus;
    }
    
    public void setStatus(LineStatus status) {
      if (lineStatus == status) {
        return;
      }
      lineStatus = status;
      if (lineStatus == LineStatus.MISSED_CALL) {
        missedCallCount += 1;
      }
      update();
    }
    
    protected void doCreateNewCustomer(String phoneNo) {
      boolean setKeyPad = true;
      
      QuickCustomerForm form = new QuickCustomerForm(setKeyPad);
      form.setPhoneNo(phoneNo);
      

      form.enableCustomerFields(true);
      BeanEditorDialog dialog = new BeanEditorDialog(form);
      dialog.setResizable(false);
      dialog.open();
      
      if (!dialog.isCanceled()) {
        customer = ((Customer)form.getBean());
      }
    }
    
    public Customer getCustomer() {
      return customer;
    }
    
    public void setCustomer(Customer customer) {
      this.customer = customer;
    }
    
    public int getLine() {
      return line;
    }
    
    public void setLine(int line) {
      this.line = line;
    }
    
    public String getCallerId() {
      return callerId;
    }
    
    public void setCallerId(String callerId) {
      this.callerId = callerId;
    }
    
    public String getCallerName() {
      return callerName;
    }
    
    public void setCallerName(String callerName) {
      this.callerName = callerName;
    }
    
    public String getCallTime() {
      return callTime;
    }
    
    public void setCallTime(String callTime) {
      this.callTime = callTime;
    }
    
    public LineStatus getLineStatus() {
      return lineStatus;
    }
    
    public void setLineStatus(LineStatus lineStatus) {
      this.lineStatus = lineStatus;
    }
    
    public int getMissedCallCount() {
      return missedCallCount;
    }
    
    public void setSortOrder(int currentTimeMillis) {
      sortOrder = currentTimeMillis;
    }
    
    public int getSortOrder() {
      return sortOrder;
    }
  }
}
