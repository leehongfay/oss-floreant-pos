package com.orocube.orocust.ui.util;

import com.floreantpos.model.DeliveryAddress;
import com.floreantpos.model.Store;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.util.NumberUtil;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.GeocodingApiRequest;
import com.google.maps.PlacesApi;
import com.google.maps.QueryAutocompleteRequest;
import com.google.maps.model.AutocompletePrediction;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.Distance;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.DistanceMatrixElement;
import com.google.maps.model.Duration;
import com.google.maps.model.EncodedPolyline;
import com.google.maps.model.GeocodingResult;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;















public class GMapUtil
{
  private static GeoApiContext context = new GeoApiContext();
  
  static {
    Store store = DataProvider.get().getStore();
    String googleMapApiKey = store.getProperty("google.map.api.key", "AIzaSyDc-5LFTSC-bB9kQcZkM74LHUxwndRy_XM");
    context.setApiKey(googleMapApiKey).setConnectTimeout(30L, TimeUnit.SECONDS).setReadTimeout(30L, TimeUnit.SECONDS).setWriteTimeout(30L, TimeUnit.SECONDS);
  }
  
  public static List<DeliveryAddress> getPlaces(String searchString)
  {
    searchString = searchString.replaceAll(" ", "+");
    List<DeliveryAddress> list = new ArrayList();
    try {
      AutocompletePrediction[] response = (AutocompletePrediction[])PlacesApi.queryAutocomplete(context, searchString).awaitIgnoreError();
      for (int i = 0; i < response.length; i++) {
        DeliveryAddress address = new DeliveryAddress();
        address.setAddress(description);
        list.add(address);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
    return list;
  }
  
  public static List<DeliveryAddress> getNearByPlaces(String searchString) {
    searchString = searchString.replaceAll(" ", "+");
    List<DeliveryAddress> list = new ArrayList();
    try {
      GeocodingResult[] results = (GeocodingResult[])GeocodingApi.newRequest(context).address(searchString).await();
      for (int i = 0; i < results.length; i++) {
        DeliveryAddress address = new DeliveryAddress();
        address.setAddress(formattedAddress);
        list.add(address);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
    return list;
  }
  
  public static String getDistance(String source, String destination) {
    String outputString = "";
    String[] origins = { source };
    String[] destinations = { destination };
    try {
      DistanceMatrix matrix = (DistanceMatrix)DistanceMatrixApi.getDistanceMatrix(context, origins, destinations).await();
      double distance = rows[0].elements[0].distance.inMeters;
      
      distance = NumberUtil.roundToTwoDigit(distance * 6.21371E-4D);
      outputString = outputString + "Distance : " + distance + " " + "MILE".toLowerCase();
      




      outputString = outputString + ", Duration : " + rows[0].elements[0].duration.humanReadable;
    } catch (Exception e) {
      return "";
    }
    return outputString;
  }
  
  public static String[] getDistanceAndDuration(String source, String destination) {
    String[] outputString = { "0", "0" };
    String[] origins = { source };
    String[] destinations = { destination };
    try {
      DistanceMatrix matrix = (DistanceMatrix)DistanceMatrixApi.getDistanceMatrix(context, origins, destinations).await();
      double distance = rows[0].elements[0].distance.inMeters;
      outputString[0] = String.valueOf(distance);
      outputString[1] = rows[0].elements[0].duration.humanReadable;
    } catch (Exception e) {
      return outputString;
    }
    return outputString;
  }
  
  public static double getDistanceAsDouble(String source, String destination) {
    double distance = 0.0D;
    String[] origins = { source };
    String[] destinations = { destination };
    try {
      DistanceMatrix matrix = (DistanceMatrix)DistanceMatrixApi.getDistanceMatrix(context, origins, destinations).await();
      distance = rows[0].elements[0].distance.inMeters;
    }
    catch (Exception e) {
      return 0.0D;
    }
    return distance;
  }
  
  public static String getDirection(String source, String destination) {
    String outputString = "";
    try {
      DirectionsResult result = (DirectionsResult)DirectionsApi.getDirections(context, source, destination).await();
      DirectionsRoute path = routes[0];
      outputString = overviewPolyline.getEncodedPath();
    } catch (Exception e) {
      return "";
    }
    return outputString;
  }
  
  public GMapUtil() {}
}
