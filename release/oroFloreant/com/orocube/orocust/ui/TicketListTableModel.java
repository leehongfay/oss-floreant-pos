package com.orocube.orocust.ui;

import com.floreantpos.Messages;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.PaginatedTableModel;

public class TicketListTableModel extends PaginatedTableModel
{
  public TicketListTableModel()
  {
    super(new String[] { "TOKEN", "ZIP CODE", "CUSTOMER", "PHONE", "ADDRESS", "READY", "NEEDED ON", "OUT AT", "DRIVER" });
  }
  
  public Object getValueAt(int rowIndex, int columnIndex) {
    Ticket ticket = (Ticket)rows.get(rowIndex);
    
    switch (columnIndex)
    {
    case 0: 
      return ticket.getTokenNo();
    
    case 1: 
      String customerZipCode = ticket.getProperty("CUSTOMER_ZIP_CODE");
      if (customerZipCode == null) {
        return "";
      }
      return customerZipCode;
    
    case 2: 
      String customerName = ticket.getProperty("CUSTOMER_NAME");
      
      if ((customerName != null) && (!customerName.equals(""))) {
        return customerName;
      }
      return Messages.getString("TicketListView.6");
    

    case 3: 
      String customerMobile = ticket.getProperty("CUSTOMER_MOBILE");
      
      if (customerMobile != null) {
        return customerMobile;
      }
      return "";
    
    case 4: 
      return ticket.getDeliveryAddress();
    
    case 5: 
      String status = "";
      if (ticket.isPaid().booleanValue()) {
        status = Messages.getString("TicketListView.8");
      }
      else {
        status = Messages.getString("TicketListView.9");
      }
      
      if (ticket.isVoided().booleanValue()) {
        status = Messages.getString("TicketListView.12");
      }
      else if (ticket.isClosed().booleanValue()) {
        status = Messages.getString("TicketListView.13");
      }
      
      return status;
    case 6: 
      java.util.Date deliveryDate = ticket.getDeliveryDate();
      if (deliveryDate == null) {
        return "";
      }
      if (DateUtil.isToday(deliveryDate)) {
        return DateUtil.formatAsTodayDate(deliveryDate);
      }
      return DateUtil.formatFullDateAndTimeAsString(deliveryDate);
    
    case 7: 
      return ticket.getProperty("OUT_AT");
    
    case 8: 
      if (ticket.isCustomerWillPickup().booleanValue()) {
        return "PICK UP";
      }
      if (ticket.getAssignedDriver() != null) {
        return ticket.getAssignedDriver().getFullName();
      }
      return "";
    }
    return null;
  }
}
