package com.orocube.orocust.ui;

import com.floreantpos.IconFactory;
import com.floreantpos.model.Ticket;
import com.floreantpos.util.NumberUtil;
import java.awt.Component;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

























public class DispatchTableCellRenderer
  extends DefaultTableCellRenderer
{
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, h:m a");
  
  public DispatchTableCellRenderer() {}
  
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) { JLabel rendererComponent = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    rendererComponent.setIcon(null);
    TicketListTableModel model = (TicketListTableModel)table.getModel();
    Ticket ticket = (Ticket)model.getRowData(row);
    
    if (column == 0) {
      String property = ticket.getProperty("source");
      if (property == null) {
        return rendererComponent;
      }
      if (property.equals("online")) {
        rendererComponent.setIcon(IconFactory.getIcon("/ui_icons/", "cloud.png"));
      }
    }
    

    return rendererComponent;
  }
  
  protected void setValue(Object value)
  {
    if (value == null) {
      setText("");
      return;
    }
    
    String text = value.toString();
    
    if (((value instanceof Double)) || ((value instanceof Float))) {
      text = NumberUtil.formatNumber(Double.valueOf(((Number)value).doubleValue()));
      setHorizontalAlignment(4);
    }
    else if ((value instanceof Integer)) {
      setHorizontalAlignment(4);
    }
    else if ((value instanceof Date)) {
      text = dateFormat.format(value);
      setHorizontalAlignment(2);
    }
    else {
      setHorizontalAlignment(2);
    }
    
    setText(" " + text + " ");
  }
}
