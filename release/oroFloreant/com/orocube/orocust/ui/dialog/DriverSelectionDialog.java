package com.orocube.orocust.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jdesktop.swingx.JXTable;


public class DriverSelectionDialog
  extends JDialog
{
  private List<Ticket> tickets;
  private JXTable driverTable;
  private PosButton btnCancel;
  private PosButton btnDone;
  protected boolean canceled = true;
  private User selectedDriver;
  
  public DriverSelectionDialog(Frame parent)
  {
    super(parent, "Assign Driver", true);
    createUI();
    
    setDefaultCloseOperation(2);
  }
  
  private void createUI() {
    setLayout(new BorderLayout());
    
    JPanel centerPanel = new JPanel(new MigLayout("fill"));
    
    driverTable = new JXTable();
    JScrollPane scrollPane = new JScrollPane();
    scrollPane.setViewportView(driverTable);
    
    centerPanel.add(scrollPane, "grow");
    add(centerPanel, "Center");
    
    JPanel buttonPanel = new JPanel();
    
    btnCancel = new PosButton();
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        canceled = true;
        dispose();
      }
    });
    btnCancel.setText(POSConstants.CANCEL.toUpperCase());
    
    btnDone = new PosButton();
    btnDone.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        selectedDriver = DriverSelectionDialog.this.doSelectDriver();
        
        if (selectedDriver == null) {
          return;
        }
        if ((tickets == null) || (tickets.isEmpty())) {
          canceled = false;
          dispose();
        }
        else {
          saveTickets();
        }
      }
    });
    btnDone.setText(POSConstants.SAVE_BUTTON_TEXT);
    
    buttonPanel.add(btnDone);
    buttonPanel.add(btnCancel);
    
    add(buttonPanel, "South");
    
    driverTable.setModel(new DriverTableModel());
    driverTable.setFocusable(false);
    driverTable.setRowHeight(35);
    driverTable.getSelectionModel().setSelectionMode(0);
  }
  
  private User doSelectDriver() {
    int selectedRow = driverTable.getSelectedRow();
    
    if (selectedRow < 0) {
      POSMessageDialog.showError("Please select a driver");
      return null;
    }
    
    DriverTableModel model = (DriverTableModel)driverTable.getModel();
    User driver = model.getDriver(selectedRow);
    





    return driver;
  }
  
  protected void saveTickets()
  {
    Session session = TicketDAO.getInstance().createNewSession();
    Transaction transaction = null;
    
    try
    {
      transaction = session.beginTransaction();
      
      for (Ticket ticket : tickets) {
        ticket.setAssignedDriver(selectedDriver);
        

        ticket.addProperty("OUT_AT", "Driver has been assigned.");
        ticket.setStatus("Driving");
        session.saveOrUpdate(ticket);
      }
      transaction.commit();
      
      for (Iterator iter = tickets.iterator(); iter.hasNext();) {
        Ticket ticket = (Ticket)iter.next();
        ReceiptPrintService.printTicket(ticket, "Customer Copy");
        ReceiptPrintService.printTicket(ticket, "Driver Copy");
      }
      canceled = false;
      dispose();
    }
    catch (Exception e) {
      LogFactory.getLog(DeliverySelectionDialog.class).error(e);
      POSMessageDialog.showError(e.getMessage());
      
      if (transaction != null)
        transaction.rollback();
    } finally {
      session.close();
    }
  }
  
  public void setData(List<Ticket> tickets, List<User> drivers) {
    this.tickets = tickets;
    driverTable.setModel(new DriverTableModel(drivers));
  }
  
  class DriverTableModel extends AbstractTableModel {
    private final String[] columns = { "NAME", "DRIVER'S PHONE", "STATUS" };
    
    private List<User> drivers;
    
    public DriverTableModel() {}
    
    public DriverTableModel()
    {
      drivers = customers;
    }
    
    public int getRowCount()
    {
      if (drivers == null) {
        return 0;
      }
      
      return drivers.size();
    }
    
    public int getColumnCount()
    {
      return columns.length;
    }
    
    public String getColumnName(int column)
    {
      return columns[column];
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      if (drivers == null) {
        return null;
      }
      
      User driver = (User)drivers.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return driver.getFirstName() + " " + driver.getLastName();
      case 1: 
        return driver.getPhoneNo();
      case 2: 
        return driver.getStatus();
      }
      return null;
    }
    
    public List<User> getDrivers() {
      return drivers;
    }
    
    public User getDriver(int index) {
      if (drivers == null) {
        return null;
      }
      
      if ((index < 0) || (index >= drivers.size())) {
        return null;
      }
      
      return (User)drivers.get(index);
    }
  }
  
  public boolean isCanceled()
  {
    return canceled;
  }
  
  public User getSelectedDriver() {
    return selectedDriver;
  }
  
  public void setSelectedDriver(User selectedDriver) {
    this.selectedDriver = selectedDriver;
  }
}
