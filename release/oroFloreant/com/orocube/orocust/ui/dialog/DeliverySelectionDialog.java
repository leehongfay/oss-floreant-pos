package com.orocube.orocust.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.PosException;
import com.floreantpos.PosLog;
import com.floreantpos.customer.CustomerSelectorDialog;
import com.floreantpos.customer.CustomerSelectorFactory;
import com.floreantpos.main.Application;
import com.floreantpos.model.Customer;
import com.floreantpos.model.DeliveryAddress;
import com.floreantpos.model.DeliveryCharge;
import com.floreantpos.model.DeliveryInstruction;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Store;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.dao.DeliveryChargeDAO;
import com.floreantpos.model.dao.TerminalDAO;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosOptionPane;
import com.floreantpos.swing.PosSmallButton;
import com.floreantpos.swing.QwertyKeyPad;
import com.floreantpos.swing.TimeSelector;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.LengthInputDialog;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orocube.orocust.Messages;
import com.orocube.orocust.ui.util.GMapUtil;
import com.orocube.orocust.ui.view.MapBrowser;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXMonthView;
import org.jdesktop.swingx.calendar.CalendarUtils;





public class DeliverySelectionDialog
  extends POSDialog
{
  private Date selectedDate;
  private String shippingAddress;
  private String zipCode;
  private JXDatePicker datePicker;
  private JTextArea taDeliveryAddress;
  private TimeSelector timeSelector;
  private JTextField tfRecipientName;
  private JTextArea taExtraInstruction;
  private JTextArea taManagerInstruction;
  private PosSmallButton btnCancel;
  private DoubleTextField tfDeliveryChargeAmount;
  private JComboBox<Object> cbDeliveryChargeList;
  private FixedLengthTextField tfPhoneNo;
  private FixedLengthTextField tfPhoneExtension;
  private FixedLengthTextField tfRoomNo;
  private POSToggleButton cbCustomerPickup;
  private Customer customer;
  private OrderType orderType;
  private QwertyKeyPad qwertyKeyPad;
  private DeliveryAddress deliveryAddress;
  private DeliveryInstruction deliveryInstruction;
  private MapBrowser mapPanel;
  private String terminalLocation;
  private String unit;
  private Timer timer;
  private PosButton btnSelectCustomer;
  private PosButton btnAddDeliveryCharge;
  private PosButton btnDeliveryAddress;
  private boolean chargeByZipcode;
  private double distance;
  private Ticket ticket;
  
  public DeliverySelectionDialog(JFrame parent, Ticket ticket, OrderType orderType, Customer customer)
  {
    super(parent, true);
    this.orderType = orderType;
    this.ticket = ticket;
    this.customer = customer;
    createUI();
    initData();
  }
  
  private void initData() {
    CustomerDAO.getInstance().initialize(customer);
    setCustomer(customer);
    terminalLocation = Application.getInstance().getTerminal().getLocation();
    unit = "MILE";
    Store store = Application.getInstance().getStore();
    chargeByZipcode = Boolean.valueOf(store.getProperty("deliveryConfig.zipcode")).booleanValue();
    
    if ((customer != null) && ((customer.getDeliveryAddresses() == null) || (customer.getDeliveryAddresses().isEmpty()))) {
      DeliveryAddress deliveryAddress = new DeliveryAddress();
      deliveryAddress.setAddress(customer.getAddress());
      customer.addTodeliveryAddresses(deliveryAddress);
    }
    
    List<DeliveryCharge> deliveryChargeList = DeliveryChargeDAO.getInstance().findAll();
    Collections.sort(deliveryChargeList, new Comparator()
    {
      public int compare(DeliveryCharge o1, DeliveryCharge o2) {
        return (int)(o1.getChargeAmount().doubleValue() - o2.getChargeAmount().doubleValue());
      }
    });
    cbDeliveryChargeList.addItem("Select Range");
    for (DeliveryCharge deliveryCharge : deliveryChargeList) {
      cbDeliveryChargeList.addItem(deliveryCharge);
    }
    
    String address = "";
    if (ticket != null) {
      address = ticket.getDeliveryAddress();
    }
    else if (customer != null) {
      address = customer.getAddress();
      if (StringUtils.isNotEmpty(address)) {
        address = address + ", ";
      }
      if (StringUtils.isNotEmpty(customer.getCity())) {
        address = address + customer.getCity() + ", ";
      }
      if (StringUtils.isNotEmpty(customer.getState())) {
        address = address + customer.getState() + " ";
      }
      if (StringUtils.isNotEmpty(customer.getZipCode())) {
        address = address + customer.getZipCode() + " ";
      }
    }
    
    taDeliveryAddress.setText(address);
    applyDeliveryChargeByZipCode();
    
    setTicket(ticket);
  }
  
  private void createUI() {
    getContentPane().setLayout(new BorderLayout());
    
    JPanel contentPanel = new JPanel();
    contentPanel.setLayout(new MigLayout("fill,inset 10 10 0 10", "[50%][50%]", ""));
    setDefaultCloseOperation(2);
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("Enter Delivery Address");
    
    JPanel topPanel = new JPanel(new MigLayout("", "[grow][][][]", "[][][][][][][][][][][]"));
    topPanel.setBorder(new TitledBorder(null, "Delivery information", 4, 2, null, null));
    
    getContentPane().add(titlePanel, "North");
    
    JLabel lblRecepientName = new JLabel(Messages.getString("DeliverySelectionView.9"));
    
    tfRecipientName = new JTextField();
    tfRecipientName.setEditable(false);
    tfRecipientName.setColumns(10);
    
    btnSelectCustomer = new PosButton("Select");
    btnSelectCustomer.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        doSelectCustomer();
      }
      
    });
    JLabel label_1 = new JLabel("");
    
    JLabel lblDeliveryDate = new JLabel(Messages.getString("DeliverySelectionView.15"));
    
    datePicker = new JXDatePicker();
    Calendar calendar = datePicker.getMonthView().getCalendar();
    calendar.setTime(new Date());
    datePicker.getMonthView().setLowerBound(calendar.getTime());
    CalendarUtils.endOfWeek(calendar);
    calendar.add(calendar.get(4), 4);
    datePicker.getMonthView().setUpperBound(calendar.getTime());
    datePicker.setDate(new Date());
    datePicker.getEditor().setEditable(false);
    
    timeSelector = new TimeSelector();
    
    JLabel lblShippingAddress = new JLabel(Messages.getString("DeliverySelectionView.20"));
    
    JScrollPane scrollPane = new JScrollPane();
    scrollPane.setPreferredSize(new Dimension(3, 80));
    
    PosButton btnSearchMap = new PosButton("Search Map");
    btnSearchMap.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        DeliverySelectionDialog.this.doSearchAddresses();
      }
      
    });
    btnDeliveryAddress = new PosButton("<html><center>Previous Delivery Address</center></html>");
    btnDeliveryAddress.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        DeliverySelectionDialog.this.doSelectPreviousDeliveryAddress();
      }
      
    });
    taDeliveryAddress = new JTextArea();
    scrollPane.setViewportView(taDeliveryAddress);
    
    JLabel lblExtraInstruction = new JLabel(Messages.getString("DeliverySelectionView.23"));
    
    JScrollPane scrollPane_1 = new JScrollPane();
    scrollPane_1.setPreferredSize(new Dimension(3, 80));
    
    PosButton btnExtranInstructions = new PosButton("<html><center>Previous Instructions</center></html>");
    btnExtranInstructions.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        DeliverySelectionDialog.this.doShowAllExtraInfo();
      }
    });
    if (taExtraInstruction == null) {
      taExtraInstruction = new JTextArea();
    }
    taExtraInstruction.setPreferredSize(new Dimension(0, 40));
    taExtraInstruction.setRows(3);
    scrollPane_1.setViewportView(taExtraInstruction);
    
    taManagerInstruction = new JTextArea();
    taManagerInstruction.setPreferredSize(new Dimension(0, 40));
    taManagerInstruction.setRows(3);
    
    JScrollPane scrollPane_2 = new JScrollPane();
    scrollPane_2.setPreferredSize(new Dimension(3, 80));
    
    scrollPane_2.setViewportView(taManagerInstruction);
    
    tfDeliveryChargeAmount = new DoubleTextField(10);
    
    cbDeliveryChargeList = new JComboBox();
    cbDeliveryChargeList.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e) {
        Object object = e.getItem();
        DeliverySelectionDialog.this.doSetDeliveryCharge(object);
      }
      
    });
    btnAddDeliveryCharge = new PosButton("<html><center>Add</center></html>");
    btnAddDeliveryCharge.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        double distance = LengthInputDialog.takeDoubleInput("Enter Distance", 0.0D);
        if (distance < 0.0D) {
          return;
        }
        DeliverySelectionDialog.this.updateDeliveryCharge(distance);
      }
      
    });
    JLabel lblPhone = new JLabel("Phone", 4);
    topPanel.add(lblPhone, "cell 1 0,alignx trailing");
    tfPhoneNo = new FixedLengthTextField();
    tfPhoneNo.setLength(20);
    topPanel.add(tfPhoneNo, "flowx,cell 2 0,grow");
    
    JLabel label = new JLabel("Extension");
    topPanel.add(label, "cell 2 0,alignx trailing");
    
    tfPhoneExtension = new FixedLengthTextField();
    tfPhoneExtension.setLength(5);
    topPanel.add(tfPhoneExtension, "cell 2 0, width 100!");
    topPanel.add(btnSelectCustomer, "cell 3 0, spany 2, grow");
    
    topPanel.add(lblRecepientName, "cell 1 1,alignx trailing");
    topPanel.add(tfRecipientName, "cell 2 1,growx");
    topPanel.add(label_1, "cell 3 1");
    
    cbCustomerPickup = new POSToggleButton(Messages.getString("DeliverySelectionView.14"));
    cbCustomerPickup.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        boolean enabled = cbCustomerPickup.isSelected();
        setPickupEnable(enabled);
      }
    });
    topPanel.add(cbCustomerPickup, "cell 2 2");
    topPanel.add(lblDeliveryDate, "cell 1 3,alignx trailing,growy");
    topPanel.add(datePicker, "cell 2 3,growy,height 40!");
    topPanel.add(timeSelector, "cell 2 3,growy");
    topPanel.add(lblShippingAddress, "cell 1 4,alignx trailing,aligny top");
    topPanel.add(scrollPane, "cell 2 4,grow");
    topPanel.add(btnDeliveryAddress, "cell 3 4,aligny top,grow");
    JLabel label_2 = new JLabel("Apartment/Room No");
    topPanel.add(label_2, "cell 1 5,alignx trailing,aligny top");
    
    tfRoomNo = new FixedLengthTextField();
    tfRoomNo.setLength(20);
    topPanel.add(tfRoomNo, "cell 2 5,width 150!");
    topPanel.add(btnSearchMap, "cell 2 6");
    topPanel.add(lblExtraInstruction, "cell 1 7,alignx trailing,aligny top");
    topPanel.add(scrollPane_1, "cell 2 7,aligny top,grow");
    topPanel.add(btnExtranInstructions, "cell 3 7 2 1,aligny top,grow");
    
    topPanel.add(new JLabel("Manager's Note :"), "cell 1 8,alignx trailing,aligny top");
    topPanel.add(scrollPane_2, "cell 2 8,aligny top,grow");
    
    topPanel.add(new JLabel("Delivery Charge :"), "cell 1 9,aligny top,gapy 5,grow");
    topPanel.add(tfDeliveryChargeAmount, "cell 2 9,growy,aligny top,gapy 5");
    topPanel.add(cbDeliveryChargeList, "cell 2 9,height 35!,aligny top,gapy 5,grow");
    topPanel.add(btnAddDeliveryCharge, "cell 3 9,height 35!,aligny top,gapy 5,grow");
    
    contentPanel.add(topPanel, "cell 0 0,grow");
    
    mapPanel = new MapBrowser();
    mapPanel.setBorder(BorderFactory.createTitledBorder("Map"));
    contentPanel.add(mapPanel, "cell 1 0, grow");
    
    getContentPane().add(contentPanel, "Center");
    
    JPanel footerPanel = new JPanel(new BorderLayout(10, 10));
    footerPanel.setBorder(new EmptyBorder(0, 10, 0, 10));
    
    qwertyKeyPad = new QwertyKeyPad();
    qwertyKeyPad.setCollapsed(true);
    
    footerPanel.add(qwertyKeyPad, "North");
    
    JPanel panel_1 = new JPanel(new MigLayout("inset 0 0 5 0,al center", "sg, fill", ""));
    
    PosButton btnKeyboard = new PosButton(IconFactory.getIcon("/images/", "keyboard.png"));
    btnKeyboard.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        qwertyKeyPad.setCollapsed(!qwertyKeyPad.isCollapsed());
      }
      
    });
    panel_1.add(btnKeyboard, "grow");
    
    btnCancel = new PosSmallButton();
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
    });
    btnCancel.setText(Messages.getString("DeliverySelectionView.27"));
    panel_1.add(btnCancel, "grow");
    
    PosSmallButton btnSaveProceed = new PosSmallButton();
    btnSaveProceed.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (finish()) {
          setCanceled(false);
          dispose();
        }
      }
    });
    btnSaveProceed.setText(Messages.getString("DeliverySelectionView.28"));
    panel_1.add(btnSaveProceed, "grow");
    
    footerPanel.add(new JSeparator(), "Center");
    
    footerPanel.add(panel_1, "South");
    getContentPane().add(footerPanel, "South");
  }
  
  protected void doSelectCustomer() {
    CustomerSelectorDialog dialog = CustomerSelectorFactory.createCustomerSelectorDialog(Application.getPosWindow(), orderType);
    dialog.setCreateNewTicket(false);
    dialog.setCallerId(tfPhoneNo.getText());
    dialog.updateView(true);
    dialog.openUndecoratedFullScreen();
    
    if (dialog.isCanceled()) {
      return;
    }
    customer = dialog.getSelectedCustomer();
    if (customer == null) {
      return;
    }
    tfPhoneNo.setText(customer.getMobileNo());
    tfRecipientName.setText(customer.getName());
    if (ticket != null) {
      ticket.setCustomer(customer);
    }
  }
  
  protected void applyDeliveryChargeByZipCode() {
    DeliveryChargeDAO dao = DeliveryChargeDAO.getInstance();
    List<DeliveryCharge> deliveryChargeList = null;
    if (chargeByZipcode) {
      deliveryChargeList = dao.findByZipCode(customer.getZipCode());
      if ((deliveryChargeList != null) && (!deliveryChargeList.isEmpty())) {
        cbDeliveryChargeList.setSelectedItem(deliveryChargeList.get(0));
      }
      else {
        cbDeliveryChargeList.setSelectedIndex(0);
      }
    }
  }
  
  private void doSearchAddresses() {
    if (taDeliveryAddress.getText().isEmpty()) {
      return;
    }
    
    if ((terminalLocation == null) || (terminalLocation.isEmpty())) {
      terminalLocation = PosOptionPane.showInputDialog("Enter Terminal Location");
      if (terminalLocation == null) {
        return;
      }
      TerminalDAO terminalDAO = TerminalDAO.getInstance();
      Terminal terminal = terminalDAO.get(Application.getInstance().getTerminal().getId());
      terminal.setLocation(terminalLocation);
      terminalDAO.saveOrUpdate(terminal);
      Application.getInstance().refreshAndGetTerminal();
    }
    
    List<DeliveryAddress> deliveryAddresses = GMapUtil.getPlaces(taDeliveryAddress.getText());
    
    DeliveryAddressSelectionDialog dialog = new DeliveryAddressSelectionDialog(deliveryAddresses);
    dialog.setSize(600, 415);
    dialog.open();
    
    if (!dialog.isCanceled()) {
      taDeliveryAddress.setText(dialog.getSelectedAddress().getAddress());
      mapPanel.load(terminalLocation, taDeliveryAddress.getText());
      try
      {
        distance = mapPanel.getDistance().doubleValue();
        updateDeliveryCharge(distance / 1000.0D);
      } catch (Exception ex) {
        ex = 
        

          ex;ex.printStackTrace();
      }
      finally {}
    }
  }
  
  private void updateDeliveryCharge(double distance) {
    if (chargeByZipcode) {
      return;
    }
    if (distance <= 0.0D) {
      cbDeliveryChargeList.setSelectedIndex(0);
    }
    if (unit.equals("MILE")) {
      distance *= 0.621371D;
    }
    distance = Math.round(distance);
    
    DeliveryChargeDAO dao = DeliveryChargeDAO.getInstance();
    List<DeliveryCharge> deliveryChargeList = dao.findByDistance(distance);
    
    if ((deliveryChargeList != null) && (!deliveryChargeList.isEmpty())) {
      cbDeliveryChargeList.setSelectedItem(deliveryChargeList.get(0));
    }
    else {
      Double maxRange = dao.findMaxRange();
      if (maxRange != null) {
        if (distance > maxRange.doubleValue()) {
          cbDeliveryChargeList.setSelectedIndex(cbDeliveryChargeList.getModel().getSize() - 1);
        }
        else {
          cbDeliveryChargeList.setSelectedIndex(0);
        }
      }
    }
  }
  
  private Date captureDeliveryDate() {
    Date date = datePicker.getDate();
    if (date == null) {
      POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Please select date.");
      return null;
    }
    int hour = timeSelector.getSelectedHour();
    int min = timeSelector.getSelectedMin();
    
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.set(10, hour == 12 ? 0 : hour);
    calendar.set(12, min);
    calendar.set(9, timeSelector.getAmPm());
    
    return calendar.getTime();
  }
  
  private String captureShippingAddress() {
    return taDeliveryAddress.getText();
  }
  
  public Date getDeliveryDate() {
    return selectedDate;
  }
  
  public String getShippingAddress() {
    return shippingAddress;
  }
  
  public String getExtraDeliveryInfo() {
    return taExtraInstruction.getText();
  }
  
  public String getManagerInstruction() {
    return taManagerInstruction.getText();
  }
  
  public void setExtraDeliveryInfo(String extraInfo) {
    taExtraInstruction.setText(extraInfo);
  }
  
  public String getName()
  {
    return Messages.getString("DeliverySelectionView.33");
  }
  
  public void setRecipientName(String name) {
    tfRecipientName.setText(name);
  }
  
  public void setDeliveryAddress(String shippingAddress) {
    taDeliveryAddress.setText(shippingAddress);
    timer = new Timer(1000, new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        loadMap();
      }
    });
    timer.setRepeats(false);
    timer.start();
  }
  
  public void loadMap() {
    if ((customer.getAddress() != null) && (!customer.getAddress().isEmpty())) {
      mapPanel.load(terminalLocation, taDeliveryAddress.getText());
      try
      {
        double distance = mapPanel.getDistance().doubleValue();
        distance /= 1000.0D;
        updateDeliveryCharge(distance);
      } catch (Exception ex) {
        ex = 
        

          ex;LogFactory.getLog(DeliverySelectionDialog.class).error(ex);
      } finally {}
    }
  }
  
  public boolean willCustomerPickup() { return cbCustomerPickup.isSelected(); }
  
  public boolean finish() {
    try {
      String phoneNo;
      if (customer == null) {
        phoneNo = tfPhoneNo.getText();
        if (StringUtils.isEmpty(phoneNo)) {
          POSMessageDialog.showError(this, "Please enter phone no.");
          return false;
        }
        List<Customer> customers = CustomerDAO.getInstance().findByMobileNumber(phoneNo);
        if (customers.size() > 0) {
          customer = ((Customer)customers.get(0));
        }
        else {
          customer = new Customer();
          customer.setMobileNo(phoneNo);
          customer.setFirstName(phoneNo);
          customer.setName(phoneNo);
          CustomerDAO.getInstance().save(customer);
        }
      }
      
      selectedDate = captureDeliveryDate();
      if (selectedDate == null) {
        return false;
      }
      
      if (selectedDate.compareTo(new Date()) < 0) {
        POSMessageDialog.showError(POSUtil.getFocusedWindow(), "Please select valid delivery date.");
        return false;
      }
      
      shippingAddress = captureShippingAddress();
      if ((customer != null) && (customer.getDeliveryAddresses() != null)) {
        for (DeliveryAddress address : customer.getDeliveryAddresses()) {
          if ((address.getAddress() != null) && (address.getAddress().equals(shippingAddress))) {
            deliveryAddress = address;
          }
        }
      }
      if (deliveryAddress == null) {
        deliveryAddress = new DeliveryAddress();
        deliveryAddress.setCustomer(customer);
        customer.addTodeliveryAddresses(deliveryAddress);
      }
      if (deliveryAddress.getDistance().doubleValue() <= 0.0D) {
        deliveryAddress.setDistance(Double.valueOf(distance));
      }
      deliveryAddress.setAddress(taDeliveryAddress.getText());
      
      deliveryAddress.setPhoneExtension(tfPhoneExtension.getText());
      deliveryAddress.setRoomNo(tfRoomNo.getText());
      
      if (customer.getDeliveryInstructions() != null) {
        for (DeliveryInstruction instruction : customer.getDeliveryInstructions()) {
          if ((instruction.getNotes() != null) && (instruction.getNotes().equals(taExtraInstruction.getText()))) {
            deliveryInstruction = instruction;
          }
        }
      }
      if (deliveryInstruction == null) {
        deliveryInstruction = new DeliveryInstruction();
        deliveryInstruction.setCustomer(customer);
        customer.addTodeliveryInstructions(deliveryInstruction);
      }
      deliveryInstruction.setNotes(taExtraInstruction.getText());
      CustomerDAO.getInstance().saveOrUpdate(customer);
      
      if (ticket != null) {
        ticket.addProperty("CUSTOMER_NAME", tfRecipientName.getText());
        ticket.addProperty("CUSTOMER_MOBILE", tfPhoneNo.getText());
        ticket.addProperty("PHONE_EXTENSION", getPhoneExtension());
        ticket.setDeliveryDate(selectedDate);
        ticket.setDeliveryAddress(shippingAddress);
        ticket.setExtraDeliveryInfo(getExtraDeliveryInfo());
        ticket.addProperty("MANAGER_INSTRUCTION", getManagerInstruction());
        ticket.setDeliveryCharge(Double.valueOf(getDeliveryCharge()));
        ticket.setCustomerWillPickup(Boolean.valueOf(cbCustomerPickup.isSelected()));
        ticket.setCustomer(customer);
      }
      
      return true;
    } catch (PosException e) {
      POSMessageDialog.showError(e.getMessage()); }
    return false;
  }
  
  public void setCustomer(Customer customer)
  {
    this.customer = customer;
    if (customer != null) {
      setRecipientName(customer.getName());
    }
  }
  
  public void setCustomerWillPickUp(boolean enabled) {
    cbCustomerPickup.setSelected(enabled);
    setPickupEnable(enabled);
  }
  
  public void setPickupEnable(boolean enabled) {
    taDeliveryAddress.setEditable(!enabled);
    tfRoomNo.setEditable(!enabled);
    tfDeliveryChargeAmount.setText("");
    tfDeliveryChargeAmount.setEditable(!enabled);
    cbDeliveryChargeList.setEnabled(!enabled);
    btnAddDeliveryCharge.setEnabled(!enabled);
    btnDeliveryAddress.setEnabled(!enabled);
  }
  
  public String getDeliveryAddress() {
    return tfRoomNo.getText() + " " + getShippingAddress();
  }
  
  public String getZipCode() {
    if (zipCode == null) {
      return customer.getZipCode();
    }
    return zipCode;
  }
  
  public OrderType getTicketType() {
    return orderType;
  }
  
  public void setDeliveryCharge(Double value) {
    tfDeliveryChargeAmount.setText(String.valueOf(value));
  }
  
  public double getDeliveryCharge() {
    if (tfDeliveryChargeAmount.isEmpty()) {
      return 0.0D;
    }
    return tfDeliveryChargeAmount.getDouble();
  }
  
  public String getPhoneExtension() {
    return tfPhoneExtension.getText();
  }
  
  public Ticket getTicket() {
    return ticket;
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
    if (ticket == null) {
      return;
    }
    setDeliveryTime(ticket);
    String phone = ticket.getProperty("CUSTOMER_MOBILE");
    if ((StringUtils.isEmpty(phone)) && (customer != null)) {
      phone = customer.getMobileNo();
    }
    tfPhoneNo.setText(phone);
    tfPhoneExtension.setText(ticket.getProperty("PHONE_EXTENSION"));
    cbCustomerPickup.setSelected(ticket.isCustomerWillPickup().booleanValue());
    taExtraInstruction.setText(ticket.getExtraDeliveryInfo());
    taManagerInstruction.setText(ticket.getProperty("MANAGER_INSTRUCTION"));
  }
  
  private void setDeliveryTime(Ticket ticket) {
    Date deliveryDate = ticket.getDeliveryDate();
    if (deliveryDate == null) {
      return;
    }
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(deliveryDate);
    
    datePicker.setDate(deliveryDate);
    timeSelector.setSelectedHour(calendar.get(10));
    timeSelector.setSelectedMin(calendar.get(12));
    timeSelector.setAmPm(calendar.get(9));
  }
  
  private void doSelectPreviousDeliveryAddress() {
    if (customer.getDeliveryAddresses() == null) {
      POSMessageDialog.showMessage("No record found");
      return;
    }
    DeliveryAddressSelectionDialog explorer = new DeliveryAddressSelectionDialog(customer);
    explorer.setSize(600, 415);
    explorer.open();
    
    if (!explorer.isCanceled()) {
      deliveryAddress = explorer.getSelectedAddress();
      taDeliveryAddress.setText(deliveryAddress.getAddress());
      tfPhoneExtension.setText(deliveryAddress.getPhoneExtension());
      tfRoomNo.setText(deliveryAddress.getRoomNo());
      mapPanel.load(terminalLocation, deliveryAddress.getAddress());
      try {
        Double distance = deliveryAddress.getDistance();
        if (distance.doubleValue() <= 0.0D) {
          distance = Double.valueOf(GMapUtil.getDistanceAsDouble(terminalLocation, taDeliveryAddress.getText()));
          distance = Double.valueOf(distance.doubleValue() / 1000.0D);
        }
        updateDeliveryCharge(distance.doubleValue());
      }
      catch (Exception ex) {
        PosLog.error(getClass(), ex.getMessage(), ex);
      }
    }
  }
  
  private void doShowAllExtraInfo() {
    if ((customer.getDeliveryInstructions() == null) || (customer.getDeliveryInstructions().isEmpty())) {
      POSMessageDialog.showMessage("No record found");
      return;
    }
    DeliveryInstructionSelectionDialog explorer = new DeliveryInstructionSelectionDialog(customer);
    explorer.setSize(600, 400);
    explorer.open();
    
    if (!explorer.isCanceled()) {
      deliveryInstruction = explorer.getSelectedInstruction();
      taExtraInstruction.setText(deliveryInstruction.getNotes());
    }
  }
  
  private void doSetDeliveryCharge(Object object) {
    if ((object instanceof DeliveryCharge)) {
      DeliveryCharge deliveryCharge = (DeliveryCharge)object;
      tfDeliveryChargeAmount.setText(String.valueOf(deliveryCharge.getChargeAmount()));
    }
    else {
      tfDeliveryChargeAmount.setText("0");
    }
  }
}
