package com.orocube.orocust.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.Customer;
import com.floreantpos.model.DeliveryInstruction;
import com.floreantpos.model.dao.DeliveryInstructionDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;





















public class DeliveryInstructionSelectionDialog
  extends POSDialog
{
  private JList<DeliveryInstruction> list;
  private DefaultListModel<DeliveryInstruction> listModel;
  private DeliveryInstruction selectedDeliveryInstruction;
  private JPanel centerPanel;
  Customer customer;
  
  public DeliveryInstructionSelectionDialog(Customer customer)
  {
    this.customer = customer;
    initComponent();
    for (DeliveryInstruction deliveryInstruction : customer.getDeliveryInstructions()) {
      if (!deliveryInstruction.getNotes().isEmpty())
        listModel.addElement(deliveryInstruction);
    }
  }
  
  private void initComponent() {
    setLayout(new BorderLayout(5, 5));
    
    centerPanel = new JPanel(new BorderLayout(5, 5));
    centerPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
    
    listModel = new DefaultListModel();
    
    JScrollPane scrollPane = new JScrollPane();
    list = new JList(listModel);
    list.setFixedCellHeight(60);
    list.setFocusable(true);
    scrollPane.setViewportView(list);
    
    centerPanel.add(scrollPane, "Center");
    
    add(centerPanel);
    
    addButtonPanel();
  }
  
  private void addButtonPanel() {
    PosButton addButton = new PosButton("ADD");
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          String address = JOptionPane.showInputDialog(POSUtil.getFocusedWindow(), "Address");
          if (address == null) {
            BOMessageDialog.showError(POSUtil.getFocusedWindow(), "Address cannot be empty.");
            return;
          }
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
    });
    PosButton selectButton = new PosButton("SELECT");
    selectButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          selectedDeliveryInstruction = ((DeliveryInstruction)list.getSelectedValue());
          setCanceled(false);
          dispose();
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
      }
    });
    PosButton deleteButton = new PosButton(POSConstants.DELETE.toUpperCase());
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          selectedDeliveryInstruction = ((DeliveryInstruction)list.getSelectedValue());
          
          if (POSMessageDialog.showYesNoQuestionDialog(DeliveryInstructionSelectionDialog.this, POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
            return;
          }
          
          DeliveryInstructionDAO dao = new DeliveryInstructionDAO();
          dao.delete(selectedDeliveryInstruction);
          
          listModel.removeElement(selectedDeliveryInstruction);
        } catch (Exception x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    PosButton cancelButton = new PosButton(POSConstants.CANCEL.toUpperCase());
    cancelButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(selectButton);
    
    panel.add(cancelButton);
    centerPanel.add(panel, "South");
  }
  
  public DeliveryInstruction getSelectedInstruction() {
    return selectedDeliveryInstruction;
  }
}
