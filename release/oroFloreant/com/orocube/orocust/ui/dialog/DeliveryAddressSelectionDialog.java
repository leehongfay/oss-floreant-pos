package com.orocube.orocust.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.Customer;
import com.floreantpos.model.DeliveryAddress;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.POSDialog;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;




















public class DeliveryAddressSelectionDialog
  extends POSDialog
{
  private JList<DeliveryAddress> list;
  private DefaultListModel<DeliveryAddress> listModel;
  private DeliveryAddress selectedDeliveryAddress;
  private JPanel centerPanel;
  
  public DeliveryAddressSelectionDialog(Customer customer)
  {
    init();
    setDeliveryAddresses(customer.getDeliveryAddresses());
  }
  
  public DeliveryAddressSelectionDialog(List<DeliveryAddress> deliveryAddressList)
  {
    init();
    setDeliveryAddresses(deliveryAddressList);
  }
  
  private void setDeliveryAddresses(List<DeliveryAddress> deliveryAddressList) {
    if (deliveryAddressList != null) {
      for (DeliveryAddress deliveryAddress : deliveryAddressList) {
        if (!deliveryAddress.getAddress().isEmpty())
          listModel.addElement(deliveryAddress);
      }
    }
  }
  
  private void init() {
    setLayout(new BorderLayout(5, 5));
    
    centerPanel = new JPanel(new BorderLayout(5, 5));
    centerPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
    
    listModel = new DefaultListModel();
    
    JScrollPane scrollPane = new JScrollPane();
    list = new JList(listModel);
    list.setFixedCellHeight(60);
    list.setFocusable(true);
    scrollPane.setViewportView(list);
    
    centerPanel.add(scrollPane, "Center");
    
    add(centerPanel);
    
    addButtonPanel();
  }
  
  private void addButtonPanel()
  {
    PosButton selectButton = new PosButton("SELECT");
    selectButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          selectedDeliveryAddress = ((DeliveryAddress)list.getSelectedValue());
          setCanceled(false);
          dispose();
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
      }
    });
    PosButton cancelButton = new PosButton(POSConstants.CANCEL.toUpperCase());
    cancelButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    panel.add(selectButton);
    panel.add(cancelButton);
    centerPanel.add(panel, "South");
  }
  
  public DeliveryAddress getSelectedAddress() {
    return selectedDeliveryAddress;
  }
}
