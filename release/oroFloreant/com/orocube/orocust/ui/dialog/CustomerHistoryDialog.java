package com.orocube.orocust.ui.dialog;

import com.floreantpos.ITicketList;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.actions.SettleTicketAction;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.main.Application;
import com.floreantpos.model.Customer;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.TicketItemDiscount;
import com.floreantpos.model.TicketItemModifier;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.UserType;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TicketListUpdateListener;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.VoidTicketDialog;
import com.floreantpos.ui.order.TicketListView;
import com.floreantpos.ui.views.OrderInfoDialog;
import com.floreantpos.ui.views.OrderInfoView;
import com.floreantpos.ui.views.order.DefaultOrderServiceExtension;
import com.floreantpos.ui.views.order.OrderController;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.ShiftUtil;
import com.orocube.orocust.ui.view.CustomerListView;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;


public class CustomerHistoryDialog
  extends POSDialog
  implements ActionListener, ITicketList, TicketListUpdateListener
{
  private OrderServiceExtension orderServiceExtension;
  private PosButton btnEditTicket = new PosButton(POSConstants.EDIT_TICKET_BUTTON_TEXT);
  private PosButton btnOrderInfo = new PosButton(POSConstants.ORDER_INFO_BUTTON_TEXT);
  private PosButton btnReOrder = new PosButton(POSConstants.REORDER_TICKET_BUTTON_TEXT);
  private PosButton btnSettleTicket = new PosButton(POSConstants.SETTLE_TICKET_BUTTON_TEXT);
  private PosButton btnVoidTicket = new PosButton(POSConstants.VOID_TICKET_BUTTON_TEXT);
  private PosButton btnCloseOrder = new PosButton(POSConstants.CLOSE_ORDER_BUTTON_TEXT);
  
  private TicketListView ticketList;
  private TitledBorder ticketsListPanelBorder;
  private String memberId;
  private CustomerListView owner;
  
  public CustomerHistoryDialog(String memberId, CustomerListView owner)
  {
    setTitle("Member History");
    setLayout(new BorderLayout());
    this.memberId = memberId;
    this.owner = owner;
    
    ticketList = new TicketListView(memberId, true);
    initComponents();
    ticketList.addTicketListUpateListener(this);
    
    btnEditTicket.addActionListener(this);
    btnOrderInfo.addActionListener(this);
    btnReOrder.addActionListener(this);
    btnSettleTicket.addActionListener(this);
    btnVoidTicket.addActionListener(this);
    
    orderServiceExtension = ((OrderServiceExtension)ExtensionManager.getPlugin(OrderServiceExtension.class));
    
    if (orderServiceExtension == null) {
      orderServiceExtension = new DefaultOrderServiceExtension();
    }
    applyComponentOrientation(ComponentOrientation.getOrientation(Locale.getDefault()));
    updateCustomerTicketList(memberId);
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(10, 10));
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("CUSTOMER HISTORY");
    
    add(titlePanel, "North");
    
    JPanel centerPanel = new JPanel(new BorderLayout(5, 5));
    JPanel ticketsAndActivityPanel = new JPanel(new BorderLayout(5, 5));
    
    ticketsListPanelBorder = BorderFactory.createTitledBorder(null, POSConstants.OPEN_TICKETS_AND_ACTIVITY, 2, 0);
    

    JPanel activityPanel = createActivityPanel();
    
    ticketsAndActivityPanel.setBorder(new CompoundBorder(ticketsListPanelBorder, new EmptyBorder(2, 2, 2, 2)));
    ticketsAndActivityPanel.add(ticketList, "Center");
    ticketsAndActivityPanel.add(activityPanel, "South");
    
    btnCloseOrder.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doCloseOrder();
      }
      
    });
    centerPanel.add(ticketsAndActivityPanel, "Center");
    add(centerPanel, "Center");
  }
  
  private JPanel createActivityPanel() {
    JPanel activityPanel = new JPanel(new BorderLayout(5, 5));
    JPanel innerActivityPanel = new JPanel(new MigLayout("hidemode 3, fill, ins 0", "fill, grow", ""));
    
    JPanel firstRowButtonPanel = new JPanel(new GridLayout(1, 0, 5, 5));
    
    if (Application.getInstance().getTerminal().isHasCashDrawer().booleanValue()) {
      firstRowButtonPanel.add(btnOrderInfo);
      firstRowButtonPanel.add(btnEditTicket);
      firstRowButtonPanel.add(btnSettleTicket);
      firstRowButtonPanel.add(btnCloseOrder);
      
      firstRowButtonPanel.add(btnReOrder);
      firstRowButtonPanel.add(btnVoidTicket);
    }
    else {
      firstRowButtonPanel.add(btnOrderInfo);
      firstRowButtonPanel.add(btnEditTicket);
      firstRowButtonPanel.add(btnCloseOrder);
      
      firstRowButtonPanel.add(btnReOrder);
      firstRowButtonPanel.add(btnVoidTicket);
    }
    
    innerActivityPanel.add(firstRowButtonPanel);
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL.toUpperCase());
    btnCancel.setPreferredSize(new Dimension(78, 0));
    
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        CustomerHistoryDialog.this.closeDialog(false);
      }
      
    });
    activityPanel.add(innerActivityPanel);
    activityPanel.add(btnCancel, "East");
    
    return activityPanel;
  }
  
  protected void doCloseOrder() {
    Ticket ticket = getFirstSelectedTicket();
    
    if (ticket == null) {
      return;
    }
    
    ticket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
    
    int due = (int)POSUtil.getDouble(ticket.getDueAmount());
    if (due != 0) {
      POSMessageDialog.showError(this, Messages.getString("SwitchboardView.5"));
      return;
    }
    
    int option = JOptionPane.showOptionDialog(Application.getPosWindow(), 
      Messages.getString("SwitchboardView.6") + ticket.getId() + Messages.getString("SwitchboardView.7"), POSConstants.CONFIRM, 2, 1, null, null, null);
    

    if (option != 0) {
      return;
    }
    
    OrderController.closeOrder(ticket);
    
    updateCustomerTicketList(memberId);
  }
  
  private void doReOrderTicket()
  {
    try {
      Ticket selectedTicket = null;
      
      List<Ticket> selectedTickets = ticketList.getSelectedTickets();
      
      if (selectedTickets.size() > 0) {
        selectedTicket = (Ticket)selectedTickets.get(0);
      }
      else {
        POSMessageDialog.showMessage("Select one ticket to reorder");
        return;
      }
      
      Ticket ticket = TicketDAO.getInstance().loadFullTicket(selectedTicket.getId());
      



























      createReOrder(ticket);
      closeDialog(true);
      owner.closeDialog(false);
    }
    catch (PosException e) {
      POSMessageDialog.showError(this, e.getLocalizedMessage());
    } catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  private void doSettleTicket() {
    try {
      if (!POSUtil.checkDrawerAssignment()) {
        return;
      }
      
      Ticket ticket = null;
      
      List<Ticket> selectedTickets = ticketList.getSelectedTickets();
      
      if (selectedTickets.size() > 0) {
        ticket = (Ticket)selectedTickets.get(0);
      }
      else {
        POSMessageDialog.showMessage(POSConstants.SELECT_ONE_TICKET_TO_SETTLE);
        return;
      }
      new SettleTicketAction(ticket).actionPerformed(null);
      updateCustomerTicketList(memberId);
    } catch (PosException e) {
      POSMessageDialog.showError(this, e.getMessage());
    } catch (Exception e) {
      e.printStackTrace();
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  private void doShowOrderInfo() {
    doShowOrderInfo(ticketList.getSelectedTickets());
  }
  
  private void doShowOrderInfo(List<Ticket> tickets)
  {
    try {
      if (tickets.size() == 0) {
        POSMessageDialog.showMessage("Please select one ticket");
        return;
      }
      
      List<Ticket> ticketsToShow = new ArrayList();
      
      for (int i = 0; i < tickets.size(); i++) {
        Ticket ticket = (Ticket)tickets.get(i);
        ticketsToShow.add(TicketDAO.getInstance().loadFullTicket(ticket.getId()));
      }
      
      OrderInfoView view = new OrderInfoView(ticketsToShow);
      OrderInfoDialog dialog = new OrderInfoDialog(view);
      dialog.setSize(400, 600);
      dialog.setDefaultCloseOperation(2);
      dialog.setLocationRelativeTo(Application.getPosWindow());
      dialog.setVisible(true);
      
      if (dialog.isReorder()) {
        closeDialog(true);
      }
    }
    catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  private boolean doEditTicket() {
    try {
      Ticket ticket = null;
      
      List<Ticket> selectedTickets = ticketList.getSelectedTickets();
      
      if (selectedTickets.size() > 0) {
        ticket = (Ticket)selectedTickets.get(0);
      }
      else {
        POSMessageDialog.showMessage(POSConstants.SELECT_ONE_TICKET_TO_EDIT);
        return false;
      }
      if (!editTicket(ticket)) {
        return false;
      }
    } catch (PosException e) {
      POSMessageDialog.showError(this, e.getMessage());
      return false;
    } catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage(), e);
    }
    return true;
  }
  
  private boolean editTicket(Ticket ticket) {
    if (ticket.isPaid().booleanValue()) {
      POSMessageDialog.showMessage(this, Messages.getString("SwitchboardView.14"));
      return false;
    }
    
    Ticket ticketToEdit = TicketDAO.getInstance().loadFullTicket(ticket.getId());
    
    OrderView.getInstance().setCurrentTicket(ticketToEdit);
    RootView.getInstance().showView("ORDER_VIEW");
    
    return true;
  }
  
  public void doHomeDelivery(OrderType ticketType) {}
  
  public void doVoidTicket()
  {
    Ticket ticket = ticketList.getSelectedTicket();
    
    if (ticket == null) {
      POSMessageDialog.showMessage(POSConstants.SELECT_ONE_TICKET_TO_VOID);
      return;
    }
    Ticket ticketToVoid = TicketDAO.getInstance().loadFullTicket(ticket.getId());
    
    VoidTicketDialog voidTicketDialog = new VoidTicketDialog();
    voidTicketDialog.setTicket(ticketToVoid);
    voidTicketDialog.open();
    
    if (!voidTicketDialog.isCanceled()) {
      ticketList.updateCustomerTicketList(memberId);
    }
  }
  
  public void updateView() {
    User user = Application.getCurrentUser();
    UserType userType = user.getType();
    if (userType != null) {
      Set<UserPermission> permissions = userType.getPermissions();
      if (permissions != null)
      {
        btnEditTicket.setEnabled(false);
        btnReOrder.setEnabled(false);
        btnSettleTicket.setEnabled(false);
        
        for (UserPermission permission : permissions) {
          if (permission.equals(UserPermission.VOID_TICKET)) {
            btnVoidTicket.setEnabled(true);
          }
          else if (permission.equals(UserPermission.SETTLE_TICKET)) {
            btnSettleTicket.setEnabled(true);
          }
          else if (permission.equals(UserPermission.REOPEN_TICKET)) {
            btnReOrder.setEnabled(true);
          }
          else if (!permission.equals(UserPermission.SPLIT_TICKET))
          {
            if (permission.equals(UserPermission.CREATE_TICKET))
              btnEditTicket.setEnabled(true);
          }
        }
      }
    }
    updateCustomerTicketList(memberId);
  }
  
  public void updateCustomerTicketList(String customerId)
  {
    ticketList.updateCustomerTicketList(customerId);
  }
  
  private void closeDialog(boolean canceled) {
    setCanceled(false);
    dispose();
    
    if (canceled) {}
  }
  



  public void setVisible(boolean visible)
  {
    super.setVisible(visible);
    
    if (visible) {
      updateView();
      ticketList.setAutoUpdateCheck(true);
    }
    else {
      ticketList.setAutoUpdateCheck(false);
    }
  }
  
  public void actionPerformed(ActionEvent e) {
    Object source = e.getSource();
    
    if (source == btnEditTicket) {
      if (doEditTicket()) {
        closeDialog(true);
        owner.closeDialog(false);
      }
    }
    else if (source == btnOrderInfo) {
      doShowOrderInfo();
    }
    else if (source == btnReOrder) {
      doReOrderTicket();
    }
    else if (source == btnSettleTicket) {
      doSettleTicket();
    }
    else if (source == btnVoidTicket) {
      doVoidTicket();
    }
  }
  
  public Ticket getFirstSelectedTicket() {
    List<Ticket> selectedTickets = ticketList.getSelectedTickets();
    
    if ((selectedTickets.size() == 0) || (selectedTickets.size() > 1)) {
      POSMessageDialog.showMessage(this, Messages.getString("SwitchboardView.22"));
      return null;
    }
    
    Ticket ticket = (Ticket)selectedTickets.get(0);
    
    return ticket;
  }
  
  public Ticket getSelectedTicket() {
    List<Ticket> selectedTickets = ticketList.getSelectedTickets();
    
    if ((selectedTickets.size() == 0) || (selectedTickets.size() > 1)) {
      return null;
    }
    
    Ticket ticket = (Ticket)selectedTickets.get(0);
    
    return ticket;
  }
  
  private void createReOrder(Ticket oldticket) {
    Ticket ticket = new Ticket();
    ticket.setPriceIncludesTax(oldticket.isPriceIncludesTax());
    ticket.setOrderType(oldticket.getOrderType());
    ticket.setTerminal(Application.getInstance().getTerminal());
    ticket.setOwner(Application.getCurrentUser());
    ticket.setShift(ShiftUtil.getCurrentShift());
    ticket.setNumberOfGuests(oldticket.getNumberOfGuests());
    ticket.setCustomerId(oldticket.getCustomerId());
    ticket.addProperty("CUSTOMER_ID", oldticket.getProperty("CUSTOMER_ID"));
    ticket.addProperty("CUSTOMER_NAME", oldticket.getProperty("CUSTOMER_NAME"));
    ticket.addProperty("CUSTOMER_MOBILE", oldticket.getProperty("CUSTOMER_MOBILE"));
    
    Calendar currentTime = DateUtil.getServerTimeCalendar();
    ticket.setCreateDate(currentTime.getTime());
    ticket.setCreationHour(Integer.valueOf(currentTime.get(11)));
    
    List<TicketItem> newTicketItems = new ArrayList();
    for (TicketItem oldTicketItem : oldticket.getTicketItems()) {
      TicketItem newTicketItem = new TicketItem();
      newTicketItem.setQuantity(oldTicketItem.getQuantity());
      newTicketItem.setMenuItemId(oldTicketItem.getMenuItemId());
      newTicketItem.setName(oldTicketItem.getName());
      newTicketItem.setGroupName(oldTicketItem.getGroupName());
      newTicketItem.setCategoryName(oldTicketItem.getCategoryName());
      newTicketItem.setUnitPrice(oldTicketItem.getUnitPrice());
      newTicketItem.setFractionalUnit(oldTicketItem.isFractionalUnit());
      newTicketItem.setUnitName(oldTicketItem.getUnitName());
      
      List<TicketItemDiscount> discounts = oldTicketItem.getDiscounts();
      if (discounts != null) {
        List<TicketItemDiscount> newDiscounts = new ArrayList();
        for (TicketItemDiscount ticketItemDiscount : discounts) {
          TicketItemDiscount newDiscount = new TicketItemDiscount(ticketItemDiscount);
          newDiscount.setTicketItem(newTicketItem);
          newDiscounts.add(newDiscount);
        }
        newTicketItem.setDiscounts(newDiscounts);
      }
      
      List<TicketItemModifier> ticketItemModifiers = oldTicketItem.getTicketItemModifiers();
      if (ticketItemModifiers != null) {
        for (TicketItemModifier ticketItemModifier : ticketItemModifiers) {
          TicketItemModifier newModifier = new TicketItemModifier();
          newModifier.setItemId(ticketItemModifier.getItemId());
          newModifier.setGroupId(ticketItemModifier.getGroupId());
          newModifier.setItemCount(ticketItemModifier.getItemCount());
          newModifier.setName(ticketItemModifier.getName());
          newModifier.setUnitPrice(ticketItemModifier.getUnitPrice());
          newModifier.setTaxes(ticketItemModifier.getTaxes());
          newModifier.setTaxAmount(ticketItemModifier.getTaxAmount());
          newModifier.setModifierType(ticketItemModifier.getModifierType());
          newModifier.setPrintedToKitchen(Boolean.valueOf(false));
          newModifier.setShouldPrintToKitchen(ticketItemModifier.isShouldPrintToKitchen());
          newModifier.setTicketItem(newTicketItem);
          newTicketItem.addToticketItemModifiers(newModifier);
        }
      }
      newTicketItem.setTaxes(oldTicketItem.getTaxes());
      newTicketItem.setTaxAmount(oldTicketItem.getTaxAmount());
      newTicketItem.setBeverage(oldTicketItem.isBeverage());
      newTicketItem.setShouldPrintToKitchen(oldTicketItem.isShouldPrintToKitchen());
      newTicketItem.setPrinterGroup(oldTicketItem.getPrinterGroup());
      newTicketItem.setPrintedToKitchen(Boolean.valueOf(false));
      
      newTicketItem.setTicket(ticket);
      newTicketItems.add(newTicketItem);
    }
    ticket.getTicketItems().addAll(newTicketItems);
    
    getDeliveryInfo(ticket, oldticket);
    
    OrderView.getInstance().setCurrentTicket(ticket);
    RootView.getInstance().showView("ORDER_VIEW");
  }
  





  private void getDeliveryInfo(Ticket newTicket, Ticket oldTicketItem)
  {
    Customer customer = CustomerDAO.getInstance().findById(oldTicketItem.getCustomerId());
    
    DeliverySelectionDialog deliveryDialog = new DeliverySelectionDialog(Application.getPosWindow(), newTicket, oldTicketItem.getOrderType(), customer);
    deliveryDialog.setLocationRelativeTo(Application.getPosWindow());
    if (customer != null) {
      deliveryDialog.setRecipientName(customer.getName());
      deliveryDialog.setDeliveryAddress(oldTicketItem.getDeliveryAddress());
    }
    
    deliveryDialog.openUndecoratedFullScreen();
    
    if (deliveryDialog.isCanceled()) {}
  }
  

















  public void ticketListUpdated()
  {
    String title = POSConstants.OPEN_TICKETS_AND_ACTIVITY;
    
    ticketsListPanelBorder.setTitle(title);
  }
  
  public String getMemberId()
  {
    return memberId;
  }
  
  public void updateTicketList() {}
}
