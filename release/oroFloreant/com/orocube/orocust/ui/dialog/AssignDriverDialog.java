package com.orocube.orocust.ui.dialog;

import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.PosSmallButton;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.table.AbstractTableModel;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jdesktop.swingx.JXTable;

public class AssignDriverDialog
  extends JDialog
{
  private JTextField tfRecipientName;
  private JTextField tfDeliveryDate;
  private Ticket ticket;
  private JTextArea tfDeliveryAddress;
  private JTextArea tfExtraInstruction;
  private JXTable driverTable;
  private PosSmallButton btnCancel;
  private PosSmallButton btnSave;
  protected boolean canceled = true;
  
  public AssignDriverDialog(Frame parent) {
    super(parent, "Assign Driver", true);
    createUI();
    
    setDefaultCloseOperation(2);
  }
  
  private void createUI() {
    getContentPane().setLayout(new MigLayout("", "[][grow]", "[][][60px,grow,shrink 0][60px,shrink 0][grow][shrink 0]"));
    
    JLabel lblRecipientName = new JLabel("Recipient Name");
    getContentPane().add(lblRecipientName, "cell 0 0,alignx trailing");
    
    tfRecipientName = new JTextField();
    tfRecipientName.setEnabled(true);
    tfRecipientName.setEditable(false);
    tfRecipientName.setFocusable(false);
    getContentPane().add(tfRecipientName, "cell 1 0,growx");
    tfRecipientName.setColumns(10);
    
    JLabel lblDeliveryDate = new JLabel("Delivery Date");
    getContentPane().add(lblDeliveryDate, "cell 0 1,alignx trailing");
    
    tfDeliveryDate = new JTextField();
    tfDeliveryDate.setEnabled(true);
    tfDeliveryDate.setEditable(false);
    tfDeliveryDate.setFocusable(false);
    getContentPane().add(tfDeliveryDate, "cell 1 1,growx");
    tfDeliveryDate.setColumns(10);
    
    JLabel lblDeliveryAddress = new JLabel("Delivery Address");
    getContentPane().add(lblDeliveryAddress, "cell 0 2,aligny top");
    
    tfDeliveryAddress = new JTextArea();
    tfDeliveryAddress.setEnabled(true);
    tfDeliveryAddress.setEditable(false);
    tfDeliveryAddress.setFocusable(false);
    tfDeliveryAddress.setBorder(new LineBorder(Color.LIGHT_GRAY));
    tfDeliveryAddress.setRows(4);
    getContentPane().add(tfDeliveryAddress, "cell 1 2,grow");
    
    JLabel lblExtraInstruction = new JLabel("Extra Instruction");
    getContentPane().add(lblExtraInstruction, "cell 0 3,aligny top");
    
    tfExtraInstruction = new JTextArea();
    tfExtraInstruction.setEnabled(true);
    tfExtraInstruction.setEditable(false);
    tfExtraInstruction.setFocusable(true);
    tfExtraInstruction.setBorder(new LineBorder(Color.LIGHT_GRAY));
    tfExtraInstruction.setRows(4);
    getContentPane().add(tfExtraInstruction, "cell 1 3,grow");
    
    JPanel panel = new JPanel();
    getContentPane().add(panel, "cell 0 4 2 1,grow");
    panel.setLayout(new BorderLayout(0, 0));
    
    JScrollPane scrollPane = new JScrollPane();
    panel.add(scrollPane, "Center");
    
    driverTable = new JXTable();
    scrollPane.setViewportView(driverTable);
    
    JPanel panel_1 = new JPanel();
    getContentPane().add(panel_1, "cell 0 5 2 1,grow");
    
    btnCancel = new PosSmallButton();
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        canceled = true;
        dispose();
      }
    });
    btnCancel.setText("CANCEL");
    panel_1.add(btnCancel);
    
    btnSave = new PosSmallButton();
    btnSave.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        saveTicket();
      }
    });
    btnSave.setText("SAVE");
    panel_1.add(btnSave);
    
    driverTable.setModel(new DriverTableModel());
    driverTable.setFocusable(false);
    driverTable.setRowHeight(35);
    driverTable.getSelectionModel().setSelectionMode(0);
  }
  
  protected void saveTicket() {
    int selectedRow = driverTable.getSelectedRow();
    
    if (selectedRow < 0) {
      POSMessageDialog.showError("Please select a driver");
      return;
    }
    
    DriverTableModel model = (DriverTableModel)driverTable.getModel();
    User driver = model.getDriver(selectedRow);
    











    ticket.setAssignedDriver(driver);
    
    Session session = TicketDAO.getInstance().createNewSession();
    Transaction transaction = null;
    
    try
    {
      transaction = session.beginTransaction();
      




      session.saveOrUpdate(ticket);
      
      transaction.commit();
      
      canceled = false;
      dispose();
    }
    catch (Exception e) {
      e.printStackTrace();
      POSMessageDialog.showError(e.getMessage());
      
      if (transaction != null) transaction.rollback();
    } finally {
      session.close();
    }
  }
  
  public void setData(Ticket ticket, List<User> drivers) {
    this.ticket = ticket;
    
    if (StringUtils.isNotEmpty(ticket.getProperty("CUSTOMER_NAME"))) {
      tfRecipientName.setText(ticket.getProperty("CUSTOMER_NAME"));
    }
    
    if (ticket.getDeliveryDate() != null) {
      tfDeliveryDate.setText(ticket.getDeliveryDate().toString());
    }
    
    tfDeliveryAddress.setText(ticket.getDeliveryAddress());
    tfExtraInstruction.setText(ticket.getExtraDeliveryInfo());
    
    driverTable.setModel(new DriverTableModel(drivers));
  }
  
  class DriverTableModel extends AbstractTableModel {
    private final String[] columns = { "DRIVER'S PHONE", "NAME", "AVAILABLE" };
    
    private List<User> drivers;
    
    public DriverTableModel() {}
    
    public DriverTableModel()
    {
      drivers = customers;
    }
    
    public int getRowCount()
    {
      if (drivers == null) {
        return 0;
      }
      
      return drivers.size();
    }
    
    public int getColumnCount()
    {
      return columns.length;
    }
    
    public String getColumnName(int column)
    {
      return columns[column];
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      if (drivers == null) {
        return null;
      }
      
      User driver = (User)drivers.get(rowIndex);
      
      switch (columnIndex) {
      case 0: 
        return driver.getPhoneNo();
      case 1: 
        return driver.getFirstName() + " " + driver.getLastName();
      
      case 2: 
        return driver.isAvailableForDelivery().booleanValue() ? "Yes" : "No";
      }
      return null;
    }
    
    public List<User> getDrivers() {
      return drivers;
    }
    
    public User getDriver(int index) {
      if (drivers == null) {
        return null;
      }
      
      if ((index < 0) || (index >= drivers.size())) {
        return null;
      }
      
      return (User)drivers.get(index);
    }
  }
  
  public boolean isCanceled()
  {
    return canceled;
  }
}
