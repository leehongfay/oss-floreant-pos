package com.orocube.orocust.ui.dialog;

import com.floreantpos.IconFactory;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class AboutLicenseDialog
  extends POSDialog
{
  private static final long serialVersionUID = 1L;
  private URI url;
  private Log logging = LogFactory.getLog(AboutLicenseDialog.class);
  
  public AboutLicenseDialog() {
    super(POSUtil.getBackOfficeWindow(), "About Orocust");
  }
  

  protected void initUI()
  {
    JPanel contentPanel = new JPanel(new BorderLayout(20, 20));
    Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
    contentPanel.setLocation(width / 2 - getSizewidth / 2, height / 2 - getSizeheight / 2);
    contentPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
    JLabel logoLabel = new JLabel(IconFactory.getIcon("/icons/", "fp_logo128x128.png"));
    contentPanel.add(logoLabel, "West");
    JPanel labelPanel = new JPanel(new BorderLayout());
    JPanel btnPanel = new JPanel(new FlowLayout(1, 15, 15));
    
    JButton btnByeLicense = new JButton("Buy License");
    btnByeLicense.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          url = new URI("http://www.orocube.com");
          Desktop.getDesktop().browse(url);
        } catch (URISyntaxException|IOException e1) {
          logging.error(e1);













        }
        












      }
      













    });
    JButton btnClose = new JButton("Close");
    btnClose.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        dispose();








      }
      








    });
    add(contentPanel);
  }
}
