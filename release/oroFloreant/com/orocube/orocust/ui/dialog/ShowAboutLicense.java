package com.orocube.orocust.ui.dialog;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

public class ShowAboutLicense extends AbstractAction
{
  private static final long serialVersionUID = 1L;
  
  public ShowAboutLicense()
  {
    super("About Customer");
  }
  


  public void actionPerformed(ActionEvent e)
  {
    AboutLicenseDialog dialog = new AboutLicenseDialog();
    dialog.setResizable(false);
    dialog.pack();
    dialog.open();
  }
}
