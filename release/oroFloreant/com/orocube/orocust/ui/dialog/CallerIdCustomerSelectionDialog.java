package com.orocube.orocust.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.main.Application;
import com.floreantpos.model.Customer;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;




























public class CallerIdCustomerSelectionDialog
  extends POSDialog
{
  private JPanel buttonsPanel;
  private Customer selectedCustomer;
  private TitlePanel titlePane;
  
  public CallerIdCustomerSelectionDialog(List<Customer> customers)
  {
    super(Application.getPosWindow(), "SELECT CUSTOMER");
    initComponent();
    rendererCustomers(customers);
  }
  
  private void initComponent() {
    JPanel contentPanel = new JPanel(new BorderLayout(5, 5));
    contentPanel.setBorder(new EmptyBorder(0, 10, 0, 10));
    setLayout(new BorderLayout());
    buttonsPanel = new JPanel(new MigLayout("fill,aligny top,wrap 3", "sg,fill,grow", ""));
    
    titlePane = new TitlePanel();
    add(titlePane, "North");
    
    JScrollPane scrollPane = new PosScrollPane(buttonsPanel, 20, 31);
    scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(80, 0));
    scrollPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5), null));
    
    contentPanel.add(scrollPane, "Center");
    
    JPanel footerPanel = new JPanel(new BorderLayout());
    footerPanel.add(new JSeparator(), "North");
    PosButton btnCancel = new PosButton(POSConstants.CANCEL.toUpperCase());
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setCanceled(true);
        dispose();
      }
      
    });
    btnCancel.setBorder(null);
    footerPanel.add(btnCancel, "South");
    add(footerPanel, "South");
    add(contentPanel, "Center");
  }
  
  private void rendererCustomers(List<Customer> customers) {
    try {
      Dimension size = PosUIManager.getSize(150, 80);
      for (int i = 0; i < 10; i++) {
        for (Customer customer : customers) {
          CustomerButton btnCustomer = new CustomerButton(customer);
          btnCustomer.setMinimumSize(size);
          buttonsPanel.add(btnCustomer, "grow");
        }
      }
    } catch (PosException e) {
      POSMessageDialog.showError(this, e.getLocalizedMessage(), e);
    }
  }
  
  public void setTitlePaneText(String title) {
    titlePane.setTitle(title);
  }
  
  public Customer getSelectedCustomer() {
    return selectedCustomer;
  }
  
  private class CustomerButton extends PosButton implements ActionListener {
    private Customer customer;
    
    CustomerButton(Customer customer) {
      this.customer = customer;
      setFont(getFont().deriveFont(1, PosUIManager.getFontSize(18)));
      setText("<html><body><center>" + customer.getName() + "<br>" + customer.getAddress() + "</center></body></html>");
      addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e) {
      selectedCustomer = customer;
      setCanceled(false);
      dispose();
    }
  }
}
