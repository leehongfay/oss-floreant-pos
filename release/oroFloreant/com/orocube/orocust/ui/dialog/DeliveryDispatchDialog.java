package com.orocube.orocust.ui.dialog;

import com.floreantpos.model.OrderType;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.orocube.orocust.ui.view.DeliveryDispatchTicketActivity;
import java.awt.BorderLayout;
import javax.swing.border.EmptyBorder;

public class DeliveryDispatchDialog
  extends POSDialog
{
  private OrderType orderType;
  private DeliveryDispatchTicketActivity activityPanel;
  
  public DeliveryDispatchDialog(OrderType orderType)
  {
    setTitle("DELIVERY DISPATCH");
    setLayout(new BorderLayout());
    this.orderType = orderType;
    initComponents();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout(10, 10));
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("DELIVERY DISPATCH");
    titlePanel.setTextAlignment(0);
    
    add(titlePanel, "North");
    
    activityPanel = new DeliveryDispatchTicketActivity(orderType);
    activityPanel.setBorder(new EmptyBorder(0, 10, 0, 10));
    add(activityPanel, "Center");
  }
  
  public OrderType getOrderType() {
    return orderType;
  }
}
