package com.orocube.orocust.ui.dialog;

import com.floreantpos.POSConstants;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;































public class OptionDialog
  extends POSDialog
{
  private JPanel buttonsPanel;
  private PosButton btnCreateNew = new PosButton("Create new customer");
  private PosButton btnExistingCustomer = new PosButton("Select existing customer");
  private PosButton btnCanel = new PosButton(POSConstants.CANCEL);
  private boolean createNewCustomer;
  
  public OptionDialog()
  {
    setTitle("Select Operation");
    initComponent();
    setResizable(false);
  }
  
  private void initComponent() {
    buttonsPanel = new JPanel(new MigLayout("fill", "sg,fill", ""));
    
    TitlePanel titlePane = new TitlePanel();
    titlePane.setTitle("SELECT OPERATION");
    
    btnCreateNew.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        createNewCustomer = true;
        setCanceled(false);
        dispose();
      }
      
    });
    btnExistingCustomer.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        createNewCustomer = false;
        setCanceled(false);
        dispose();
      }
      
    });
    btnCanel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setCanceled(true);
        dispose();
      }
      
    });
    buttonsPanel.add(btnCreateNew);
    buttonsPanel.add(btnExistingCustomer);
    buttonsPanel.add(btnCanel);
    
    add(titlePane, "North");
    add(buttonsPanel, "Center");
  }
  
  public boolean createNewCustomer() {
    return createNewCustomer;
  }
}
