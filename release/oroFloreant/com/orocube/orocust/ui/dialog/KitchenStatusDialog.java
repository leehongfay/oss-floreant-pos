package com.orocube.orocust.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.main.Application;
import com.floreantpos.model.ITicketItem;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.ext.KitchenStatus;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TimerWatch;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.kitchendisplay.KitchenTicketStatusSelector;
import com.floreantpos.ui.ticket.TicketItemRowCreator;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;


















public class KitchenStatusDialog
  extends POSDialog
{
  Ticket ticket;
  JLabel ticketId = new JLabel();
  protected final HashMap<String, ITicketItem> tableRows = new LinkedHashMap();
  
  KitchenTicketItemTableModel tableModel;
  JTable table;
  KitchenTicketStatusSelector statusSelector;
  private TimerWatch timerWatch;
  private JScrollPane scrollPane;
  private JPanel headerPanel;
  private JLabel ticketInfo;
  private JLabel tableInfo;
  private JLabel serverInfo;
  private JPanel contentPanel;
  
  public KitchenStatusDialog(Ticket ticket)
  {
    setLayout(new BorderLayout());
    this.ticket = ticket;
    contentPanel = new JPanel();
    contentPanel.setLayout(new BorderLayout(1, 1));
    contentPanel.setBorder(new EmptyBorder(5, 10, 10, 10));
    
    TitlePanel titlePane = new TitlePanel();
    titlePane.setTitle("KITCHEN STATUS");
    
    add(titlePane, "North");
    
    setTitle("KITCHEN STATUS");
    
    createHeader(ticket);
    createTable(ticket);
    updateTable();
    createButtonPanel();
    
    statusSelector = new KitchenTicketStatusSelector((Frame)SwingUtilities.getWindowAncestor(this));
    statusSelector.pack();
    
    timerWatch.start();
    
    contentPanel.addAncestorListener(new AncestorListener()
    {
      public void ancestorRemoved(AncestorEvent event) {
        timerWatch.stop();
      }
      



      public void ancestorMoved(AncestorEvent event) {}
      


      public void ancestorAdded(AncestorEvent event) {}
    });
    add(contentPanel, "Center");
  }
  
  public void stopTimer() {
    timerWatch.stop();
  }
  
  private void updateTable() {
    ticket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
    TicketItemRowCreator.calculateTicketRows(ticket, tableRows, true, true, false);
    List<ITicketItem> list = new ArrayList();
    for (ITicketItem ticketItem : tableRows.values()) {
      list.add(ticketItem);
    }
    tableModel.setItems(list);
  }
  
  private void createHeader(Ticket ticket) {
    ticketInfo = new JLabel("Ticket# " + ticket.getId());
    Terminal terminal = Application.getInstance().getTerminal();
    tableInfo = new JLabel();
    if (!terminal.isShowTableNumber()) {
      tableInfo.setText("Table# " + ticket.getTableNames());

    }
    else if ((ticket.getTableNumbers() != null) && (ticket.getTableNumbers().size() > 0)) {
      String tableNumbers = ticket.getTableNumbers().toString();
      tableNumbers = tableNumbers.replace("[", "").replace("]", "");
      tableInfo.setText("Table# " + tableNumbers);
    }
    

    serverInfo = new JLabel();
    if (ticket.getOwner() != null) {
      serverInfo.setText("Server: " + ticket.getOwner());
    }
    
    Font font = new Font("Verdana", 1, 13);
    
    ticketInfo.setFont(font);
    tableInfo.setFont(font);
    serverInfo.setFont(font);
    
    timerWatch = new TimerWatch(ticket.getCreateDate());
    timerWatch.setPreferredSize(new Dimension(100, 30));
    
    headerPanel = new JPanel(new MigLayout("fill", "sg, fill", ""));
    headerPanel.setBorder(BorderFactory.createLineBorder(Color.gray));
    headerPanel.add(ticketInfo, "split 2");
    headerPanel.add(timerWatch, "right,wrap, span");
    headerPanel.add(tableInfo, "split 2, grow");
    headerPanel.add(serverInfo, "right,span");
    
    contentPanel.add(headerPanel, "North");
  }
  
  private void createTable(Ticket ticket) {
    table = new JTable();
    tableModel = new KitchenTicketItemTableModel();
    table.setModel(tableModel);
    table.setRowSelectionAllowed(false);
    table.setCellSelectionEnabled(false);
    table.setRowHeight(30);
    
    table.setDefaultRenderer(Object.class, new DefaultTableCellRenderer()
    {
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component rendererComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        
        ITicketItem object = (ITicketItem)tableModel.getRowData(row);
        
        TicketItem ticketItem = null;
        if ((object instanceof TicketItem)) {
          ticketItem = (TicketItem)object;
        }
        else {
          return rendererComponent;
        }
        
        if (ticketItem != null) {
          if ((ticketItem.isShouldPrintToKitchen().booleanValue()) && (ticketItem.isPrintedToKitchen().booleanValue())) {
            rendererComponent.setBackground(Color.yellow);
          }
          else if ((ticketItem.isBeverage().booleanValue()) || (ticketItem.getKitchenStatusValue() == KitchenStatus.BUMP)) {
            rendererComponent.setBackground(Color.green);
          }
          else if (ticketItem.getKitchenStatusValue() == KitchenStatus.VOID) {
            rendererComponent.setBackground(new Color(128, 0, 128));
          }
          else {
            rendererComponent.setBackground(Color.white);
          }
        }
        
        KitchenStatusDialog.this.updateHeaderView();
        return rendererComponent;
      }
    });
    resizeTableColumns();
    
    scrollPane = new JScrollPane(table);
    contentPanel.add(scrollPane);
  }
  
  private void updateHeaderView() {
    headerPanel.setBackground(timerWatch.backColor);
    ticketInfo.setForeground(timerWatch.textColor);
    tableInfo.setForeground(timerWatch.textColor);
    serverInfo.setForeground(timerWatch.textColor);
  }
  
  private void createButtonPanel() {
    JPanel buttonPanel = new JPanel(new GridLayout(1, 0, 5, 5));
    
    PosButton btnDone = new PosButton(POSConstants.BUMP);
    btnDone.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        KitchenStatusDialog.this.doChangeStatus("Ready");
      }
      
    });
    btnDone.setPreferredSize(PosUIManager.getSize(100, 40));
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL_BUTTON_TEXT);
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
      
    });
    btnCancel.setPreferredSize(PosUIManager.getSize(100, 40));
    
    buttonPanel.add(btnDone);
    buttonPanel.add(btnCancel);
    
    contentPanel.add(buttonPanel, "South");
  }
  
  private void resizeTableColumns() {
    table.setAutoResizeMode(4);
    setColumnWidth(1, PosUIManager.getSize(60));
    setColumnWidth(2, PosUIManager.getSize(100));
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = table.getColumnModel().getColumn(columnNumber);
    
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
  
  class KitchenTicketItemTableModel extends ListTableModel<ITicketItem>
  {
    KitchenTicketItemTableModel() {
      super();
    }
    
    KitchenTicketItemTableModel() {
      super(list);
    }
    
    public void setItems(List list) {
      setRows(list);
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      if (columnIndex == 2) {
        return true;
      }
      
      return false;
    }
    
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      ITicketItem ticketItem = (ITicketItem)getRowData(rowIndex);
      
      if ((ticketItem instanceof TicketItem)) {
        ticketItem = (TicketItem)ticketItem;
      }
      
      switch (columnIndex) {
      case 0: 
        return ticketItem.getNameDisplay();
      
      case 1: 
        return ticketItem.getItemQuantityDisplay();
      case 2: 
        return ticketItem.getKitchenStatusValue();
      }
      return ticketItem;
    }
  }
  
  public Ticket getTicket()
  {
    return ticket;
  }
  
  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }
  
  private void doChangeStatus(String status) {
    try {
      stopTimer();
      
      for (TicketItem ticketItem : ticket.getTicketItems()) {
        ticketItem.setKitchenStatus(status);
      }
      ticket.setStatus(status);
      
      TicketDAO.getInstance().saveOrUpdate(ticket);
      updateTable();
      tableModel.fireTableDataChanged();
      revalidate();
      repaint();
    } catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage(), e);
    }
  }
}
