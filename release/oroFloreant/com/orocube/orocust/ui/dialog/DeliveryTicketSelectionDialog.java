package com.orocube.orocust.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.PosException;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.ScrollableFlowPanel;
import com.floreantpos.ui.dialog.OkCancelOptionDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import org.apache.commons.lang3.StringUtils;





























public class DeliveryTicketSelectionDialog
  extends OkCancelOptionDialog
{
  private ScrollableFlowPanel buttonsPanel;
  private List<Ticket> addedTicketListModel = new ArrayList();
  private List<Ticket> tickets;
  
  public DeliveryTicketSelectionDialog() {
    super(POSUtil.getFocusedWindow());
    initComponent();
    setResizable(true);
  }
  
  public DeliveryTicketSelectionDialog(List<Ticket> tickets) {
    super(POSUtil.getFocusedWindow());
    initComponent();
    Collections.sort(tickets, new Comparator()
    {
      public int compare(Ticket o1, Ticket o2)
      {
        String zipCode1 = o1.getProperty("CUSTOMER_ZIP_CODE");
        String zipCode2 = o2.getProperty("CUSTOMER_ZIP_CODE");
        if (zipCode1 == null) {
          zipCode1 = "";
        }
        if (zipCode2 == null) {
          zipCode2 = "";
        }
        return zipCode1.compareTo(zipCode2);
      }
    });
    rendererTickets(tickets);
    setResizable(true);
  }
  
  protected void setTickets(List<Ticket> tickets) {
    this.tickets = tickets;
    rendererTickets(tickets);
  }
  
  private void initComponent() {
    setOkButtonText(Messages.getString("TicketSelectionDialog.3"));
    buttonsPanel = new ScrollableFlowPanel(3);
    
    JScrollPane scrollPane = new PosScrollPane(buttonsPanel, 20, 31);
    scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(80, 0));
    scrollPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5), scrollPane.getBorder()));
    
    getContentPanel().add(scrollPane, "Center");
  }
  
  private void rendererTickets(List<Ticket> tickets) {
    try {
      for (Ticket ticket : tickets) {
        if (ticket.getDueAmount().doubleValue() > 0.0D)
        {

          TicketButton btnTicket = new TicketButton(ticket);
          Color bgColor = ticket.getAssignedDriver() != null ? Color.RED : Color.WHITE;
          btnTicket.setBackground(bgColor);
          btnTicket.setPreferredSize(PosUIManager.getSize(198, 162));
          buttonsPanel.add(btnTicket);
        }
      }
    } catch (PosException e) {
      POSMessageDialog.showError(this, e.getLocalizedMessage(), e);
    }
  }
  
  public void doOk()
  {
    if (addedTicketListModel.isEmpty()) {
      POSMessageDialog.showMessage(Messages.getString("TicketSelectionDialog.5"));
      return;
    }
    setCanceled(false);
    dispose();
  }
  
  public void doCancel() {
    addedTicketListModel.clear();
    setCanceled(true);
    dispose();
  }
  
  public List<Ticket> getSelectedTickets() {
    return addedTicketListModel;
  }
  
  private class TicketButton extends POSToggleButton implements ActionListener {
    private Ticket ticket;
    
    TicketButton(Ticket ticket) {
      this.ticket = ticket;
      String customerName = ticket.getProperty("CUSTOMER_NAME");
      if (customerName == null) {
        customerName = "";
      }
      
      String customerMobile = ticket.getProperty("CUSTOMER_MOBILE");
      if (customerMobile == null) {
        customerMobile = "";
      }
      
      String customerZipCode = ticket.getProperty("CUSTOMER_ZIP_CODE");
      if (customerZipCode == null) {
        customerZipCode = "";
      }
      
      String customerAddress = ticket.getDeliveryAddress();
      if ((customerAddress == null) || (StringUtils.isEmpty(customerAddress)) || (customerAddress.equals(" "))) {
        customerAddress = "";
      }
      

      String token = ticket.getNumberToDisplay();
      String customer = StringUtils.isNotEmpty(customerName) ? "<br/><b>Customer: </b>" + customerName : "";
      String delivery = StringUtils.isNotEmpty(customerAddress) ? "<br/><b>Delivery to: </b>" + customerAddress : "";
      String deliveryTime = StringUtils.isNotEmpty(customerAddress) ? "<br/><b>Needed on: </b>" + DateUtil.formatSmall(ticket.getDeliveryDate()) : "";
      String driver; String driver = ticket.getAssignedDriver() != null ? (driver = "<br/><b>Driver: </b>" + ticket.getAssignedDriver().getFullName()) : "";
      
      setText("<html><body><center><h3>" + token + "</h3>" + customer + delivery + deliveryTime + driver + "</center></body></html>");
      
      addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e) {
      if (isSelected()) {
        addedTicketListModel.add(ticket);
      }
      else {
        addedTicketListModel.remove(ticket);
      }
    }
  }
}
