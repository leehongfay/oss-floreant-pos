package com.orocube.orocust.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosException;
import com.floreantpos.actions.SettleTicketAction;
import com.floreantpos.extension.ExtensionManager;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.main.Application;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TitlePanel;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.OrderInfoDialog;
import com.floreantpos.ui.views.OrderInfoView;
import com.floreantpos.ui.views.SwitchboardView;
import com.floreantpos.ui.views.order.DefaultOrderServiceExtension;
import com.floreantpos.ui.views.order.OrderController;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.util.POSUtil;
import com.orocube.orocust.ui.view.CustomerListView;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



















public class TicketActivitySelectionDialog
  extends POSDialog
  implements ActionListener
{
  private PosButton btnEditTicket = new PosButton(POSConstants.EDIT_TICKET_BUTTON_TEXT);
  private PosButton btnOrderInfo = new PosButton(POSConstants.ORDER_INFO_BUTTON_TEXT);
  private PosButton btnSettleTicket = new PosButton(POSConstants.SETTLE_TICKET_BUTTON_TEXT);
  private PosButton btnCloseOrder = new PosButton(POSConstants.CLOSE_ORDER_BUTTON_TEXT);
  private PosButton btnAssignDriver = new PosButton(POSConstants.ASSIGN_DRIVER_BUTTON_TEXT);
  
  private JPanel centerPanel;
  private Ticket ticket;
  private CustomerListView parent;
  
  public TicketActivitySelectionDialog(Ticket ticket, CustomerListView parent)
  {
    setLayout(new BorderLayout(5, 5));
    setTitle("Select ticket activity");
    setResizable(false);
    this.ticket = ticket;
    this.parent = parent;
    
    TitlePanel titlePanel = new TitlePanel();
    titlePanel.setTitle("CHK# " + ticket.getId().toString());
    
    centerPanel = new JPanel(new BorderLayout(5, 5));
    centerPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
    
    JPanel actionButtonPanel = new JPanel(new MigLayout("fill,inset 0", "sg,fill", ""));
    
    int width = PosUIManager.getSize(150);
    int height = PosUIManager.getSize(130);
    
    actionButtonPanel.add(btnOrderInfo, "grow,w " + width + "!,h " + height + "!");
    actionButtonPanel.add(btnEditTicket, "grow,w " + width + "!,h " + height + "!");
    actionButtonPanel.add(btnSettleTicket, "grow,w " + width + "!,h " + height + "!");
    actionButtonPanel.add(btnCloseOrder, "grow,w " + width + "!,h " + height + "!");
    actionButtonPanel.add(btnAssignDriver, "grow,w " + width + "!,h " + height + "!");
    
    btnEditTicket.addActionListener(this);
    btnOrderInfo.addActionListener(this);
    btnSettleTicket.addActionListener(this);
    btnCloseOrder.addActionListener(this);
    btnAssignDriver.addActionListener(this);
    
    PosButton cancelButton = new PosButton(POSConstants.CANCEL.toUpperCase());
    cancelButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
      
    });
    actionButtonPanel.add(cancelButton, "grow,w " + width + "!,h " + height + "!");
    

    centerPanel.add(actionButtonPanel, "Center");
    
    add(centerPanel);
  }
  
  public void actionPerformed(ActionEvent e)
  {
    Object source = e.getSource();
    
    boolean closeParent = false;
    
    if (source == btnEditTicket) {
      if (doEditTicket()) {
        closeParent = true;
      }
    }
    else if (source == btnOrderInfo) {
      doShowOrderInfo();
    }
    else if (source == btnSettleTicket) {
      doSettleTicket();
    }
    else if (source == btnCloseOrder) {
      doCloseOrder();
    }
    else if (source == btnAssignDriver) {
      doAssignDriver();
    }
    closeDialog(closeParent);
  }
  
  protected void doCloseOrder() {
    ticket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
    
    int due = (int)POSUtil.getDouble(ticket.getDueAmount());
    if (due != 0) {
      POSMessageDialog.showError(this, Messages.getString("SwitchboardView.5"));
      return;
    }
    
    int option = JOptionPane.showOptionDialog(Application.getPosWindow(), 
      Messages.getString("SwitchboardView.6") + ticket.getId() + Messages.getString("SwitchboardView.7"), POSConstants.CONFIRM, 2, 1, null, null, null);
    

    if (option != 0) {
      return;
    }
    
    OrderController.closeOrder(ticket);
  }
  
  protected void doAssignDriver()
  {
    try {
      if (!ticket.getOrderType().isDelivery().booleanValue()) {
        POSMessageDialog.showError(this, Messages.getString("SwitchboardView.8"));
        return;
      }
      
      User assignedDriver = ticket.getAssignedDriver();
      if (assignedDriver != null) {
        int option = JOptionPane.showOptionDialog(Application.getPosWindow(), Messages.getString("SwitchboardView.9"), POSConstants.CONFIRM, 0, 3, null, null, null);
        

        if (option != 0) {
          return;
        }
      }
      OrderServiceExtension orderServiceExtension = (OrderServiceExtension)ExtensionManager.getPlugin(OrderServiceExtension.class);
      
      if (orderServiceExtension == null) {
        btnAssignDriver.setEnabled(false);
        
        orderServiceExtension = new DefaultOrderServiceExtension();
      }
      orderServiceExtension.assignDriver(ticket.getId());
    } catch (Exception e) {
      e.printStackTrace();
      POSMessageDialog.showError(this, e.getMessage());
      LogFactory.getLog(SwitchboardView.class).error(e);
    }
  }
  
  private boolean doEditTicket() {
    try {
      if (!editTicket(ticket)) {
        return false;
      }
    } catch (PosException e) {
      POSMessageDialog.showError(this, e.getMessage());
      return false;
    } catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage(), e);
    }
    return true;
  }
  
  private boolean editTicket(Ticket ticket) {
    if (ticket.isPaid().booleanValue()) {
      POSMessageDialog.showMessage(this, Messages.getString("SwitchboardView.14"));
      return false;
    }
    
    Ticket ticketToEdit = TicketDAO.getInstance().loadFullTicket(ticket.getId());
    
    OrderView.getInstance().setCurrentTicket(ticketToEdit);
    RootView.getInstance().showView("ORDER_VIEW");
    
    return true;
  }
  
  private void doSettleTicket() {
    try {
      if (!POSUtil.checkDrawerAssignment()) {
        return;
      }
      new SettleTicketAction(ticket).actionPerformed(null);
    } catch (PosException e) {
      POSMessageDialog.showError(this, e.getMessage());
    } catch (Exception e) {
      e.printStackTrace();
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  private void doShowOrderInfo()
  {
    try {
      List<Ticket> ticketsToShow = new ArrayList();
      
      ticketsToShow.add(TicketDAO.getInstance().loadFullTicket(ticket.getId()));
      
      OrderInfoView view = new OrderInfoView(ticketsToShow);
      OrderInfoDialog dialog = new OrderInfoDialog(view);
      dialog.setSize(400, 600);
      dialog.setDefaultCloseOperation(2);
      dialog.setLocationRelativeTo(Application.getPosWindow());
      dialog.setVisible(true);
      
      if (dialog.isReorder()) {
        closeDialog(true);
      }
    }
    catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  private void closeDialog(boolean parentClose) {
    if (parentClose) {
      parent.closeDialog(parentClose);
    }
    setCanceled(true);
    dispose();
  }
}
