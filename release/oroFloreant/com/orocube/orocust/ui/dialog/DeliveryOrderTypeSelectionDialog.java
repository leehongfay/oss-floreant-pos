package com.orocube.orocust.ui.dialog;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.model.OrderType;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.dialog.POSDialog;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

















public class DeliveryOrderTypeSelectionDialog
  extends POSDialog
{
  private OrderType selectedOrderType;
  private List<OrderType> orderTypes;
  
  public DeliveryOrderTypeSelectionDialog(List<OrderType> orderTypes)
  {
    this.orderTypes = orderTypes;
    initializeComponent();
  }
  
  private void initializeComponent() {
    setTitle(Messages.getString("OrderTypeSelectionDialog.0"));
    setResizable(false);
    setLayout(new BorderLayout(5, 5));
    
    JPanel orderTypePanel = new JPanel(new GridLayout(1, 0, 10, 10));
    orderTypePanel.setBorder(new EmptyBorder(10, 10, 10, 10));
    
    if (orderTypes != null) {
      for (OrderType orderType : orderTypes) {
        DeliveryOrderTypeButton btnDeliveryOrderType = new DeliveryOrderTypeButton(orderType);
        orderTypePanel.add(btnDeliveryOrderType);
      }
    }
    
    PosButton btnCancel = new PosButton(POSConstants.CANCEL_BUTTON_TEXT);
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        setCanceled(true);
        dispose();
      }
      
    });
    orderTypePanel.add(btnCancel, "growx, span");
    add(orderTypePanel);
  }
  
  public OrderType getSelectedOrderType() {
    return selectedOrderType;
  }
  
  private class DeliveryOrderTypeButton extends PosButton implements ActionListener {
    OrderType orderType;
    
    public DeliveryOrderTypeButton(OrderType orderType) {
      this.orderType = orderType;
      setText(orderType.getName());
      addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e)
    {
      selectedOrderType = orderType;
      setCanceled(false);
      dispose();
    }
  }
}
