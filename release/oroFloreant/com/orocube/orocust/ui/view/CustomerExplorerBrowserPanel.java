package com.orocube.orocust.ui.view;

import com.floreantpos.bo.ui.ModelBrowser;
import com.floreantpos.model.Customer;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.ui.forms.CustomerForm;
import java.util.List;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.jdesktop.swingx.JXTable;



public class CustomerExplorerBrowserPanel
  extends ModelBrowser<Customer>
{
  public CustomerExplorerBrowserPanel()
  {
    super(new CustomerForm(), new CustomerSearchPanel());
    
    BeanTableModel<Customer> tableModel = new BeanTableModel(Customer.class);
    tableModel.addColumn("FIRST NAME", Customer.PROP_FIRST_NAME);
    tableModel.addColumn("MOBILE", Customer.PROP_MOBILE_NO);
    tableModel.addColumn("LOYALTY NO", Customer.PROP_LOYALTY_NO);
    init(tableModel);
    
    browserTable.setAutoResizeMode(4);
  }
  



  public void refreshTable()
  {
    List<Customer> tables = CustomerDAO.getInstance().findAll();
    BeanTableModel tableModel = (BeanTableModel)browserTable.getModel();
    tableModel.removeAll();
    tableModel.addRows(tables);
  }
  
  private void setColumnWidth(int columnNumber, int width) {
    TableColumn column = browserTable.getColumnModel().getColumn(columnNumber);
    column.setPreferredWidth(width);
    column.setMaxWidth(width);
    column.setMinWidth(width);
  }
}
