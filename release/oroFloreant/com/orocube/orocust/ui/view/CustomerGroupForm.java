package com.orocube.orocust.ui.view;

import com.floreantpos.PosLog;
import com.floreantpos.bo.ui.modifierdesigner.CustomerSelectionDialog;
import com.floreantpos.model.Customer;
import com.floreantpos.model.CustomerGroup;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.dao.CustomerGroupDAO;
import com.floreantpos.swing.ComboBoxModel;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.orocube.orocust.Messages;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.Session;






public class CustomerGroupForm
  extends BeanEditor<CustomerGroup>
{
  private JLabel lblCustomerGroupName;
  private FixedLengthTextField tfCustomerGroupName;
  private JLabel lblCustomerGroupCode;
  private FixedLengthTextField tfCustomerGroupCode;
  private JLabel lblCustomerGroupDescription;
  private JTextArea txtCustomerGroupDescription;
  private JList listCustomers;
  private ComboBoxModel customerListModel;
  
  public CustomerGroupForm(CustomerGroup customerGroup)
  {
    setLayout(new BorderLayout());
    
    createUI();
    setBean(customerGroup);
  }
  
  private void createUI()
  {
    setLayout(new BorderLayout());
    JPanel itemInfoPanel = new JPanel();
    JScrollPane scrollPane = new JScrollPane(itemInfoPanel, 20, 31);
    
    scrollPane.setBorder(null);
    
    add(itemInfoPanel, "North");
    itemInfoPanel.setLayout(new MigLayout("fill", "[][grow][]", ""));
    
    lblCustomerGroupName = new JLabel(Messages.getString("CustomerGroupForm.3"));
    itemInfoPanel.add(lblCustomerGroupName);
    
    tfCustomerGroupName = new FixedLengthTextField();
    itemInfoPanel.add(tfCustomerGroupName, "growx,wrap");
    tfCustomerGroupName.setLength(30);
    
    lblCustomerGroupCode = new JLabel(Messages.getString("CustomerGroupForm.0"));
    itemInfoPanel.add(lblCustomerGroupCode, "");
    
    tfCustomerGroupCode = new FixedLengthTextField();
    tfCustomerGroupCode.setLength(6);
    
    itemInfoPanel.add(tfCustomerGroupCode, "growx,wrap");
    
    lblCustomerGroupDescription = new JLabel(Messages.getString("CustomerGroupForm.8"));
    itemInfoPanel.add(lblCustomerGroupDescription);
    
    txtCustomerGroupDescription = new JTextArea(4, 4);
    txtCustomerGroupDescription.setLineWrap(true);
    itemInfoPanel.add(new JScrollPane(txtCustomerGroupDescription), "growx,h 90!,wrap");
    
    JPanel customerPanel = new JPanel(new MigLayout("fill"));
    JPanel custmoneListPanel = new JPanel(new BorderLayout());
    JPanel buttonPanel = new JPanel(new MigLayout(""));
    
    Border loweredetched = BorderFactory.createEtchedBorder(1);
    TitledBorder title = BorderFactory.createTitledBorder(loweredetched, Messages.getString("CustomerGroupForm.12"));
    title.setTitleJustification(1);
    
    JButton btnAddCustomer = new JButton(Messages.getString("CustomerGroupForm.13"));
    btnAddCustomer.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        CustomerGroupForm.this.doAddCustomer();
      }
      
    });
    JButton btnRemoveCustomer = new JButton(Messages.getString("CustomerGroupForm.14"));
    btnRemoveCustomer.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        Customer customer = (Customer)listCustomers.getSelectedValue();
        if (customer == null)
          return;
        customerListModel.removeElement(customer);
      }
    });
    listCustomers = new JList();
    JScrollPane listCustomerScrollPane = new JScrollPane(listCustomers);
    custmoneListPanel.setBorder(title);
    custmoneListPanel.add(listCustomerScrollPane);
    
    buttonPanel.add(btnAddCustomer);
    buttonPanel.add(btnRemoveCustomer);
    
    customerPanel.add(custmoneListPanel, "grow, span");
    add(buttonPanel, "South");
    add(customerPanel, "Center");
  }
  
  public boolean save()
  {
    try
    {
      if (!updateModel()) {
        return false;
      }
      
      CustomerGroup customerGroup = (CustomerGroup)getBean();
      CustomerGroupDAO.getInstance().saveOrUpdate(customerGroup);
      
      return true;
    } catch (Exception e) {
      POSMessageDialog.showError(e.getMessage());
    }
    
    return false;
  }
  
  public void updateView()
  {
    CustomerGroup customerGroup = (CustomerGroup)getBean();
    
    if ((customerGroup.getId() != null) && (!Hibernate.isInitialized(customerGroup.getCustomers()))) {
      CustomerDAO dao = new CustomerDAO();
      Session session = dao.getSession();
      customerGroup = (CustomerGroup)session.merge(customerGroup);
      Hibernate.initialize(customerGroup.getCustomers());
      session.close();
    }
    
    customerListModel = new ComboBoxModel();
    if (customerGroup.getCustomers() != null) {
      customerListModel.setDataList(customerGroup.getCustomers());
    }
    else {
      customerListModel.setDataList(new ArrayList());
    }
    listCustomers.setModel(customerListModel);
    try {
      CustomerGroup customerGroup1 = (CustomerGroup)getBean();
      
      if (customerGroup1 == null)
      {
        return;
      }
      
      tfCustomerGroupName.setText(customerGroup1.getName());
      tfCustomerGroupCode.setText(customerGroup1.getCode());
      txtCustomerGroupDescription.setText(customerGroup1.getDescription());
    }
    catch (Exception e) {
      PosLog.error(getClass(), e);
    }
  }
  
  public boolean updateModel() {
    CustomerGroup customerGroup = (CustomerGroup)getBean();
    
    String name = tfCustomerGroupName.getText();
    if (StringUtils.isEmpty(name)) {
      POSMessageDialog.showError(Messages.getString("CustomerGroupForm.16"));
      return false;
    }
    customerGroup.setName(name);
    customerGroup.setCode(tfCustomerGroupCode.getText());
    customerGroup.setDescription(txtCustomerGroupDescription.getText());
    customerGroup.setCustomers(customerListModel.getDataList());
    
    return true;
  }
  
  public String getDisplayText() {
    CustomerGroup customerGroup = (CustomerGroup)getBean();
    if (customerGroup.getId() == null) {
      return Messages.getString("CustomerGroupForm.17");
    }
    
    return Messages.getString("CustomerGroupForm.18");
  }
  
  private void doAddCustomer()
  {
    CustomerSelectionDialog customerSelectionDialog = new CustomerSelectionDialog(getEditorDialog(), customerListModel.getDataList());
    customerSelectionDialog.setSize(800, 600);
    customerSelectionDialog.open();
    
    if (customerSelectionDialog.isCanceled())
      return;
    List<Customer> selectedCustomerList = customerSelectionDialog.getSelectedCustomerList();
    customerListModel.removeAllElements();
    for (Customer customerList : selectedCustomerList) {
      customerListModel.addElement(customerList);
    }
  }
}
