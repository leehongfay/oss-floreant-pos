package com.orocube.orocust.ui.view;

import com.floreantpos.ITicketList;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.actions.SettleTicketAction;
import com.floreantpos.customer.CustomerSelectorDialog;
import com.floreantpos.customer.CustomerSelectorFactory;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.extension.OrderServiceFactory;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.model.Customer;
import com.floreantpos.model.Gratuity;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.UserPermission;
import com.floreantpos.model.UserType;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.TicketListUpdateListener;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.PasswordEntryDialog;
import com.floreantpos.ui.dialog.SendToKitchenOptionSelectionDialog;
import com.floreantpos.ui.views.OrderInfoDialog;
import com.floreantpos.ui.views.OrderInfoView;
import com.floreantpos.ui.views.order.OrderController;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.ui.views.payment.GratuityInputDialog;
import com.floreantpos.ui.views.payment.SettleTicketDialog;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.TicketAlreadyExistsException;
import com.orocube.orocust.ui.dialog.DeliveryOrderTypeSelectionDialog;
import com.orocube.orocust.ui.dialog.DeliverySelectionDialog;
import com.orocube.orocust.ui.dialog.DeliveryTicketSelectionDialog;
import com.orocube.orocust.ui.dialog.DriverSelectionDialog;
import com.orocube.orocust.ui.dialog.KitchenStatusDialog;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;



public class DeliveryDispatchTicketActivity
  extends JPanel
  implements ActionListener, ITicketList, TicketListUpdateListener
{
  private PosButton btnNewTicket;
  private PosButton btnEditTicket;
  private PosButton btnOrderInfo;
  private PosButton btnKitchenStatus;
  private PosButton btnSendToKitchen;
  private PosButton btnEditDeliveryInfo;
  private PosButton btnAssignDriver;
  private PosButton btnUnassignDriver;
  private PosButton btnCloseOrder;
  private PosButton btnSettleTicket;
  private OrderType orderType;
  private DeliveryDispatchTicketListView ticketList;
  private PosButton btnCancel;
  private PosButton btnTips;
  
  public DeliveryDispatchTicketActivity(OrderType orderType)
  {
    this.orderType = orderType;
    setLayout(new BorderLayout());
    initComponents();
    
    applyComponentOrientation(ComponentOrientation.getOrientation(Locale.getDefault()));
    updateTicketList();
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    
    JPanel centerPanel = new JPanel(new BorderLayout(5, 5));
    
    ticketList = new DeliveryDispatchTicketListView();
    ticketList.setOrderType(orderType);
    ticketList.addTicketListUpateListener(this);
    
    JPanel ticketsAndActivityPanel = new JPanel(new BorderLayout(5, 5));
    
    TitledBorder border = BorderFactory.createTitledBorder(null, "DISPATCH VIEW", 2, 0);
    
    JPanel activityPanel = createActivityPanel();
    
    ticketsAndActivityPanel.setBorder(BorderFactory.createCompoundBorder(border, new EmptyBorder(2, 2, 2, 2)));
    ticketsAndActivityPanel.add(ticketList, "Center");
    ticketsAndActivityPanel.add(activityPanel, "South");
    
    btnCloseOrder.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doCloseOrder();
      }
      
    });
    btnSettleTicket.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        Ticket ticket = getFirstSelectedTicket();
        if (ticket == null) {
          return;
        }
        new SettleTicketAction(ticket).actionPerformed(e);
      }
      

    });
    centerPanel.add(ticketsAndActivityPanel, "Center");
    add(centerPanel, "Center");
  }
  
  private JPanel createActivityPanel()
  {
    JPanel activityPanel = new JPanel(new BorderLayout(5, 5));
    JPanel innerActivityPanel = new JPanel(new MigLayout("hidemode 3, fill, ins 0", "fill, grow", ""));
    
    JPanel actionButtonPanel = new JPanel(new MigLayout("fillx"));
    
    btnNewTicket = new PosButton("NEW ORDER");
    btnEditTicket = new PosButton("EDIT ORDER");
    btnOrderInfo = new PosButton(POSConstants.ORDER_INFO_BUTTON_TEXT);
    btnKitchenStatus = new PosButton("KITCHEN STATUS");
    btnSendToKitchen = new PosButton("SEND");
    btnTips = new PosButton("TIPS");
    btnEditDeliveryInfo = new PosButton("EDIT DELIVERY INFO");
    btnAssignDriver = new PosButton(POSConstants.ASSIGN_DRIVER_BUTTON_TEXT);
    btnUnassignDriver = new PosButton("UNASSIGN DRIVER");
    btnCloseOrder = new PosButton(POSConstants.CLOSE_ORDER_BUTTON_TEXT);
    btnSettleTicket = new PosButton(POSConstants.SETTLE_TICKET_BUTTON_TEXT);
    
    btnNewTicket.addActionListener(this);
    btnEditTicket.addActionListener(this);
    btnOrderInfo.addActionListener(this);
    btnKitchenStatus.addActionListener(this);
    btnSendToKitchen.addActionListener(this);
    btnEditDeliveryInfo.addActionListener(this);
    btnAssignDriver.addActionListener(this);
    btnUnassignDriver.addActionListener(this);
    btnTips.addActionListener(this);
    
    actionButtonPanel.add(btnOrderInfo, "grow");
    actionButtonPanel.add(btnKitchenStatus, "grow");
    actionButtonPanel.add(btnSendToKitchen, "grow");
    actionButtonPanel.add(btnTips, "grow");
    actionButtonPanel.add(btnNewTicket, "grow");
    actionButtonPanel.add(btnEditTicket, "grow");
    actionButtonPanel.add(btnEditDeliveryInfo, "grow");
    actionButtonPanel.add(btnSettleTicket, "grow");
    actionButtonPanel.add(btnCloseOrder, "grow");
    actionButtonPanel.add(btnAssignDriver, "grow");
    actionButtonPanel.add(btnUnassignDriver, "grow");
    
    innerActivityPanel.add(actionButtonPanel);
    
    btnCancel = new PosButton(POSConstants.CANCEL.toUpperCase());
    btnCancel.setPreferredSize(new Dimension(78, 0));
    
    btnCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        closeDialog(false);
      }
    });
    btnCancel.setVisible(false);
    
    activityPanel.add(innerActivityPanel);
    activityPanel.add(btnCancel, "East");
    
    return activityPanel;
  }
  
  private void closeTicket(Ticket selectedTicket) {
    User currentUser = Application.getCurrentUser();
    User assignedDriver = selectedTicket.getAssignedDriver();
    if (doUserBaseTicketClose(selectedTicket, currentUser, assignedDriver)) {
      return;
    }
    User user = PasswordEntryDialog.getUser(Application.getPosWindow(), Messages.getString("LoginView.1"), Messages.getString("LoginView.2"));
    if (user == null) {
      return;
    }
    
    if (doUserBaseTicketClose(selectedTicket, user, assignedDriver)) {
      return;
    }
    
    POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "You do not have permission to close this ticket.");
  }
  
  private boolean doUserBaseTicketClose(Ticket selectedTicket, User currentUser, User assignedDriver) {
    if ((currentUser.isAdministrator()) || (currentUser.isManager())) {
      doTicketClose(selectedTicket);
      return true;
    }
    
    if ((assignedDriver != null) && (currentUser.equals(assignedDriver))) {
      doTicketClose(selectedTicket);
      return true;
    }
    
    if ((assignedDriver == null) && (currentUser.equals(selectedTicket.getOwner()))) {
      doTicketClose(selectedTicket);
      return true;
    }
    return false;
  }
  
  private void doTicketClose(Ticket selectedTicket) {
    Ticket checkedTicket = doCheckTicketPaid(selectedTicket);
    if (checkedTicket != null) {
      TicketDAO.closeOrders(new Ticket[] { selectedTicket });
    }
  }
  
  protected void doCloseOrder()
  {
    Ticket selectedTicket = getSelectedTicket();
    User currentUser = Application.getCurrentUser();
    List<Ticket> selectedTickets = new ArrayList();
    
    if (selectedTicket != null) {
      closeTicket(selectedTicket);
      updateTicketList();
      return;
    }
    List<Ticket> tickets = ticketList.getTickets();
    if (tickets == null) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "You have no tickets to close.");
      return;
    }
    List<Ticket> userTickets = new ArrayList();
    for (Iterator localIterator = tickets.iterator(); localIterator.hasNext();) { ticket = (Ticket)localIterator.next();
      if ((currentUser.isAdministrator()) || (currentUser.isManager())) {
        userTickets.add(ticket);
      }
      else {
        User ticketUser = ticket.getOwner();
        User assignedDriver = ticket.getAssignedDriver();
        if ((assignedDriver != null) && (currentUser.equals(assignedDriver))) {
          userTickets.add(ticket);

        }
        else if ((assignedDriver == null) && (ticketUser.getId().equals(currentUser.getId())))
          userTickets.add(ticket);
      }
    }
    Ticket ticket;
    if (userTickets.isEmpty()) {
      POSMessageDialog.showMessage("You have no tickets to close.");
      return;
    }
    
    DeliveryTicketSelectionDialog dialog = new DeliveryTicketSelectionDialog(userTickets);
    dialog.setTitle("SELECT TICKET");
    dialog.setCaption("Select tickets to close.");
    dialog.openFullScreen();
    
    if (dialog.isCanceled()) {
      return;
    }
    
    for (Ticket ticket : dialog.getSelectedTickets())
    {
      Ticket checkedTicket = doCheckTicketPaid(ticket);
      if (checkedTicket != null) {
        selectedTickets.add(checkedTicket);
      }
    }
    if ((selectedTickets == null) || (selectedTickets.size() == 0)) {
      return;
    }
    int option = POSMessageDialog.showYesNoQuestionDialog(Application.getPosWindow(), "Are you sure to close tickets?", "Confirmation to close");
    
    if (option == 0) {
      TicketDAO.closeOrders((Ticket[])selectedTickets.toArray(new Ticket[selectedTickets.size()]));
    }
    
    updateTicketList();
  }
  
  private Ticket doCheckTicketPaid(Ticket ticket)
  {
    if (ticket.getDueAmount().doubleValue() > 0.0D) {
      int option = POSMessageDialog.showYesNoQuestionDialog(Application.getPosWindow(), Messages.getString("DefaultOrderServiceExtension.3") + ticket.getId() + " is not paid. Do you want to settle ticket?", Messages.getString("DefaultOrderServiceExtension.5"));
      
      if (option == 0) {
        if (!POSUtil.checkDrawerAssignment()) {
          return null;
        }
        ticket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
        SettleTicketDialog posDialog = new SettleTicketDialog(ticket, Application.getCurrentUser());
        
        posDialog.setSize(Application.getPosWindow().getSize());
        posDialog.setDefaultCloseOperation(2);
        posDialog.openUndecoratedFullScreen();
        
        if (posDialog.isCanceled()) {
          return null;
        }
        if (ticket.isPaid().booleanValue()) {
          return ticket;
        }
      }
    }
    else {
      return ticket;
    }
    return null;
  }
  
  private void doShowOrderInfo() {
    List<Ticket> tickets = ticketList.getSelectedTickets();
    
    try
    {
      if (tickets.size() == 0) {
        POSMessageDialog.showMessage("Please select one ticket");
        return;
      }
      
      List<Ticket> ticketsToShow = new ArrayList();
      
      for (int i = 0; i < tickets.size(); i++) {
        Ticket ticket = (Ticket)tickets.get(i);
        ticketsToShow.add(TicketDAO.getInstance().loadFullTicket(ticket.getId()));
      }
      
      OrderInfoView view = new OrderInfoView(ticketsToShow);
      OrderInfoDialog dialog = new OrderInfoDialog(view);
      dialog.updateView();
      dialog.pack();
      dialog.setSize(getSizewidth + 50, PosUIManager.getSize(650));
      dialog.setLocationRelativeTo(Application.getPosWindow());
      dialog.setVisible(true);
      
      if (dialog.isReorder()) {
        closeDialog(true);
      }
    }
    catch (Exception e) {
      POSMessageDialog.showError(this, POSConstants.ERROR_MESSAGE, e);
    }
  }
  
  private void doEditDeliveryInfo() {
    Ticket ticket = getFirstSelectedTicket();
    
    if (ticket == null) {
      return;
    }
    TicketDAO.getInstance().loadFullTicket(ticket);
    Customer customer = null;
    if (ticket.getCustomerId() != null) {
      customer = CustomerDAO.getInstance().findById(ticket.getCustomerId());
    }
    
    if (!ticket.getOrderType().isDelivery().booleanValue()) {
      return;
    }
    DeliverySelectionDialog deliveryDialog = new DeliverySelectionDialog(Application.getPosWindow(), ticket, ticket.getOrderType(), customer);
    deliveryDialog.setLocationRelativeTo(Application.getPosWindow());
    deliveryDialog.setRecipientName(customer.getName());
    deliveryDialog.setCustomerWillPickUp(ticket.isCustomerWillPickup().booleanValue());
    deliveryDialog.setDeliveryAddress(ticket.getDeliveryAddress());
    deliveryDialog.setExtraDeliveryInfo(ticket.getExtraDeliveryInfo());
    deliveryDialog.setTicket(ticket);
    
    deliveryDialog.openUndecoratedFullScreen();
    
    if (deliveryDialog.isCanceled()) {
      return;
    }
    













    OrderController.saveOrder(ticket);
    updateTicketList();
  }
  
  private boolean doEditTicket()
  {
    Ticket ticket = getFirstSelectedTicket();
    
    if (ticket == null) {
      return false;
    }
    try {
      if (!editTicket(ticket)) {
        return false;
      }
    } catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage(), e);
    }
    updateTicketList();
    return true;
  }
  
  private boolean editTicket(Ticket ticket) {
    if (ticket.isPaid().booleanValue()) {
      POSMessageDialog.showMessage(this, Messages.getString("SwitchboardView.14"));
      return false;
    }
    
    Ticket ticketToEdit = TicketDAO.getInstance().loadFullTicket(ticket.getId());
    
    OrderView.getInstance().setCurrentTicket(ticketToEdit);
    RootView.getInstance().showView("ORDER_VIEW");
    
    return true;
  }
  
  public void updateView() {
    User user = Application.getCurrentUser();
    UserType userType = user.getType();
    if (userType != null) {
      Set<UserPermission> permissions = userType.getPermissions();
      if (permissions != null)
      {
        btnEditTicket.setEnabled(false);
        
        for (UserPermission permission : permissions) {
          if (permission.equals(UserPermission.CREATE_TICKET)) {
            btnEditTicket.setEnabled(true);
          }
        }
      }
    }
    updateTicketList();
  }
  
  private User captureDriver() {
    User currentUser = Application.getCurrentUser();
    if (currentUser.isDriver().booleanValue()) {
      return currentUser;
    }
    
    List<User> drivers = UserDAO.getInstance().findDrivers();
    
    if ((drivers == null) || (drivers.size() == 0)) {
      POSMessageDialog.showError(Application.getPosWindow(), "No driver found to assign");
      return null;
    }
    DriverSelectionDialog dialog = new DriverSelectionDialog(Application.getPosWindow());
    dialog.setSize(550, 450);
    dialog.setData(null, drivers);
    dialog.setLocationRelativeTo(Application.getPosWindow());
    dialog.setVisible(true);
    
    if (dialog.isCanceled()) {
      return null;
    }
    return dialog.getSelectedDriver();
  }
  
  private List<Ticket> getReadyToDeliveryTicketList()
  {
    List<Ticket> readyTicketList = new ArrayList();
    for (Ticket ticket : ticketList.getTickets()) {
      if ((!ticket.isCustomerWillPickup().booleanValue()) && 
      


        (!isDriverOut(ticket)))
      {

        readyTicketList.add(ticket);
      }
    }
    return readyTicketList;
  }
  
  private boolean isDriverOut(Ticket ticket) {
    String driverOutTime = ticket.getProperty("OUT_AT");
    if (StringUtils.isNotEmpty(driverOutTime)) {
      return true;
    }
    return false;
  }
  
  protected void doAssignDriver() {
    try {
      List<Ticket> tickets = new ArrayList();
      User driver = null;
      
      Ticket selectedTicket = getSelectedTicket();
      List<Ticket> readyToDeliveryTicketList;
      if (selectedTicket != null) {
        if (selectedTicket.isCustomerWillPickup().booleanValue()) {
          POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Order is not for delivery.");
          return;
        }
        
        if (isTicketOnDelivery(selectedTicket)) {
          return;
        }
        driver = captureDriver();
        if (driver == null) {
          return;
        }
        
        tickets.add(selectedTicket);
      }
      else
      {
        driver = captureDriver();
        if (driver == null) {
          return;
        }
        
        readyToDeliveryTicketList = getReadyToDeliveryTicketList();
        DeliveryTicketSelectionDialog dialog = new DeliveryTicketSelectionDialog(readyToDeliveryTicketList);
        dialog.setTitle("SELECT TICKET");
        dialog.setCaption("Select tickets to assign driver.");
        dialog.openFullScreen();
        
        if (dialog.isCanceled()) {
          return;
        }
        tickets = dialog.getSelectedTickets();
      }
      
      if (tickets.size() == 0) {
        return;
      }
      
      for (Ticket ticket : tickets) {
        TicketDAO.getInstance().loadFullTicket(ticket);
        ticket.setAssignedDriver(driver);
        TicketDAO.getInstance().saveOrUpdate(ticket);
        ReceiptPrintService.printTicket(ticket);
      }
      tickets.clear();
      updateTicketList();
      
      if (selectedTicket != null) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Successfully driver assigned for " + selectedTicket.getNumberToDisplay());
      }
      else {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Successfully driver assigned.");
      }
    } catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage());
    }
  }
  
  protected void doUnassignDriver()
  {
    try {
      Ticket selectedTicket = getFirstSelectedTicket();
      if (selectedTicket == null) {
        return;
      }
      TicketDAO.getInstance().loadFullTicket(selectedTicket);
      if (selectedTicket.isCustomerWillPickup().booleanValue()) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Order is not for delivery.");
        return;
      }
      
      if (isTicketOnDelivery(selectedTicket)) {
        return;
      }
      
      User assignedDriver = selectedTicket.getAssignedDriver();
      if (assignedDriver == null) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Driver not assigned for CHK#: " + selectedTicket.getId() + ". Please assign driver first.");
        return;
      }
      
      int option = POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Do you want to unassign driver?", "Unassign driver");
      
      if (option != 0) {
        return;
      }
      selectedTicket.setAssignedDriver(null);
      TicketDAO.getInstance().saveOrUpdate(selectedTicket);
      updateTicketList();
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Successfully driver unassigned for CHK#: " + selectedTicket.getId());
    } catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage());
    }
  }
  
  private boolean isTicketOnDelivery(Ticket selectedTicket) {
    if (isDriverOut(selectedTicket)) {
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Cannot assign to driver. \nTicket is already out for delivery.");
      return true;
    }
    return false;
  }
  
  private void doAddTips()
  {
    try {
      Ticket ticket = getFirstSelectedTicket();
      if (ticket == null) {
        return;
      }
      if (ticket.getAssignedDriver() == null) {
        POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Please assign driver first.");
        return;
      }
      GratuityInputDialog d = new GratuityInputDialog();
      d.pack();
      d.setResizable(false);
      d.open();
      
      if (d.isCanceled()) {
        return;
      }
      
      ticket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
      double gratuityAmount = d.getGratuityAmount();
      Gratuity gratuity = ticket.getGratuity();
      if (gratuity == null) {
        gratuity = ticket.createGratuity();
        gratuity.setAmount(Double.valueOf(gratuityAmount));
      }
      else {
        gratuityAmount += gratuity.getAmount().doubleValue();
        gratuity.setAmount(Double.valueOf(gratuityAmount));
      }
      ticket.setGratuity(gratuity);
      ticket.calculatePrice();
      OrderController.saveOrder(ticket);
      updateTicketList();
      POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Tips successfully added to CHK#: " + ticket.getId());
    } catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage());
    }
  }
  
  void closeDialog(boolean closeParent)
  {
    Window windowAncestor = SwingUtilities.getWindowAncestor(this);
    if ((windowAncestor instanceof POSDialog)) {
      ((POSDialog)windowAncestor).setCanceled(true);
      windowAncestor.dispose();
    }
  }
  
  public void setVisible(boolean visible)
  {
    super.setVisible(visible);
    
    if (visible) {
      updateView();
      ticketList.setAutoUpdateCheck(true);
    }
    else {
      ticketList.setAutoUpdateCheck(false);
    }
  }
  
  public void actionPerformed(ActionEvent e) {
    Object source = e.getSource();
    
    if (source == btnEditTicket) {
      if (doEditTicket()) {
        closeDialog(true);
      }
    }
    else if (source == btnNewTicket) {
      doCreateNewTicket();
    }
    else if (source == btnOrderInfo) {
      doShowOrderInfo();
    }
    else if (source == btnKitchenStatus) {
      doShowKitchenInfo();
    }
    else if (source == btnSendToKitchen) {
      doSendToKitchen();
    }
    else if (source == btnEditDeliveryInfo) {
      doEditDeliveryInfo();
    }
    else if (source == btnAssignDriver) {
      doAssignDriver();
    }
    else if (source == btnUnassignDriver) {
      doUnassignDriver();
    }
    else if (source == btnTips) {
      doAddTips();
    }
  }
  
  private void doSendToKitchen() {
    Ticket ticket = getFirstSelectedTicket();
    if (ticket == null) {
      return;
    }
    ticket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
    
    SendToKitchenOptionSelectionDialog sendTicketDialog = new SendToKitchenOptionSelectionDialog(ticket);
    sendTicketDialog.pack();
    sendTicketDialog.open();
    if (sendTicketDialog.isCanceled()) {
      return;
    }
    updateTicketList();
  }
  
  private void doShowKitchenInfo() {
    Ticket ticket = getFirstSelectedTicket();
    
    if (ticket == null) {
      return;
    }
    KitchenStatusDialog dialog = new KitchenStatusDialog(ticket);
    dialog.setSize(PosUIManager.getSize(500, 680));
    dialog.open();
  }
  
  private void doCreateNewTicket() {
    if (this.orderType == null) {
      List<OrderType> orderTypes = Application.getInstance().getOrderTypes();
      
      List<OrderType> deliveryOrderTypes = new ArrayList();
      if (orderTypes != null) {
        for (OrderType orderType : orderTypes) {
          if (orderType.isDelivery().booleanValue())
          {
            deliveryOrderTypes.add(orderType); }
        }
      }
      if (deliveryOrderTypes.size() > 1) {
        DeliveryOrderTypeSelectionDialog orderTypeSelector = new DeliveryOrderTypeSelectionDialog(deliveryOrderTypes);
        orderTypeSelector.pack();
        orderTypeSelector.open();
        
        if (orderTypeSelector.isCanceled()) {
          return;
        }
        this.orderType = orderTypeSelector.getSelectedOrderType();
      }
      else {
        this.orderType = ((OrderType)deliveryOrderTypes.get(0));
      }
    }
    
    if (this.orderType.isRequiredCustomerData().booleanValue()) {
      CustomerSelectorDialog dialog = CustomerSelectorFactory.createCustomerSelectorDialog(Application.getPosWindow(), this.orderType);
      dialog.setCreateNewTicket(true);
      dialog.updateView(true);
      dialog.openUndecoratedFullScreen();
      
      if (!dialog.isCanceled()) {
        return;
      }
    }
    else {
      try {
        OrderServiceFactory.getOrderService().createNewTicket(this.orderType, null, null);
      } catch (TicketAlreadyExistsException e) {
        POSMessageDialog.showError(this, "Could not create ticket due to an unexpected error.\nPlease try again.", e);
      }
    }
    
    updateTicketList();
  }
  
  public Ticket getFirstSelectedTicket() {
    List<Ticket> selectedTickets = ticketList.getSelectedTickets();
    
    if ((selectedTickets.size() == 0) || (selectedTickets.size() > 1)) {
      POSMessageDialog.showMessage(this, Messages.getString("SwitchboardView.22"));
      return null;
    }
    
    Ticket ticket = (Ticket)selectedTickets.get(0);
    
    if (ticket.isClosed().booleanValue()) {
      POSMessageDialog.showError(this, "Ticket is already closed");
      return null;
    }
    
    return ticket;
  }
  
  public Ticket getSelectedTicket() {
    List<Ticket> selectedTickets = ticketList.getSelectedTickets();
    
    if ((selectedTickets.size() == 0) || (selectedTickets.size() > 1)) {
      return null;
    }
    
    Ticket ticket = (Ticket)selectedTickets.get(0);
    
    return ticket;
  }
  
  public void updateTicketList()
  {
    ticketList.updateTicketList();
  }
  

  public void ticketListUpdated() {}
  

  public void updateCustomerTicketList(String customerId) {}
  

  public OrderType getOrderType()
  {
    return orderType;
  }
  
  public void setOrderType(OrderType orderType) {
    this.orderType = orderType;
    ticketList.setOrderType(orderType);
  }
}
