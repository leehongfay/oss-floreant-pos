package com.orocube.orocust.ui.view;

import com.floreantpos.customer.CustomerSelectorDialog;
import com.floreantpos.customer.CustomerSelectorFactory;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.extension.OrderServiceFactory;
import com.floreantpos.main.Application;
import com.floreantpos.model.Customer;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Store;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.dao.OrderTypeDAO;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.ViewPanel;
import com.floreantpos.util.TicketAlreadyExistsException;
import com.orocube.orocust.callerid.CallButtonClickListener;
import com.orocube.orocust.callerid.CallInformationView;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.util.List;
import java.util.Locale;
import javax.swing.JPanel;

public class DeliveryDispatchView
  extends ViewPanel
  implements CallButtonClickListener
{
  private static final String VIEW_NAME = "DELIVERY_DISPATCH";
  private DeliveryDispatchTicketActivity ticketActivityPanel;
  private static DeliveryDispatchView instance;
  private OrderType orderType;
  private CallInformationView lineView;
  
  private DeliveryDispatchView(OrderType orderType)
  {
    setOrderType(orderType);
    
    setLayout(new BorderLayout());
    initComponents();
    
    applyComponentOrientation(ComponentOrientation.getOrientation(Locale.getDefault()));
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    
    JPanel centerPanel = new JPanel(new BorderLayout(5, 5));
    
    ticketActivityPanel = new DeliveryDispatchTicketActivity(orderType);
    
    JPanel ticketsAndActivityPanel = new JPanel(new BorderLayout(5, 5));
    
    ticketsAndActivityPanel.add(ticketActivityPanel, "Center");
    centerPanel.add(ticketsAndActivityPanel, "Center");
    add(centerPanel, "Center");
    
    String enableProperty = Application.getInstance().getStore().getProperty("callerId.CallerIdEnable");
    
    if (Boolean.valueOf(enableProperty).booleanValue() == true) {
      lineView = new CallInformationView(this);
      lineView.setVisible(false);
      add(lineView, "West");
    }
  }
  
  public static DeliveryDispatchView getInstance(OrderType orderType) {
    if (instance == null) {
      instance = new DeliveryDispatchView(orderType);
    }
    else {
      instanceticketActivityPanel.updateTicketList();
    }
    instance.setOrderType(orderType);
    return instance;
  }
  
  public String getViewName() {
    return "DELIVERY_DISPATCH";
  }
  
  public void setOrderType(OrderType orderType) {
    this.orderType = orderType;
    
    if (ticketActivityPanel != null) {
      ticketActivityPanel.setOrderType(orderType);
    }
  }
  
  public OrderType getOrderType() {
    if (orderType == null) {
      orderType = OrderTypeDAO.getInstance().getHomeDeliveryOrderType();
    }
    return orderType;
  }
  
  public void callButtonClicked(int line, String callerId, String callerName)
  {
    OrderType orderType2 = getOrderType();
    if (orderType2.isRequiredCustomerData().booleanValue()) {
      CustomerSelectorDialog dialog = CustomerSelectorFactory.createCustomerSelectorDialog(orderType2);
      dialog.setCreateNewTicket(true);
      dialog.setCallerId(callerId);
      dialog.updateView(true);
      dialog.openUndecoratedFullScreen();
      if (!dialog.isCanceled()) {
        return;
      }
    }
    else {
      try {
        List<Customer> customers = CustomerDAO.getInstance().findByMobileNumber(callerId);
        Customer selectedCustomer = null;
        if (customers.size() > 0) {
          selectedCustomer = (Customer)customers.get(0);
        }
        else {
          selectedCustomer = new Customer();
          selectedCustomer.setMobileNo(callerId);
          selectedCustomer.setFirstName(callerName);
          selectedCustomer.setName(callerName);
          CustomerDAO.getInstance().save(selectedCustomer);
        }
        
        OrderServiceFactory.getOrderService().createNewTicket(orderType2, null, selectedCustomer);
      } catch (TicketAlreadyExistsException e) {
        POSMessageDialog.showError(this, "Could not create ticket due to an unexpected error.\nPlease try again.", e);
      }
    }
  }
}
