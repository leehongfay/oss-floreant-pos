package com.orocube.orocust.ui.view;

import com.floreantpos.ITicketList;
import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.customer.CustomerSelectorDialog;
import com.floreantpos.customer.CustomerSelectorFactory;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.model.Customer;
import com.floreantpos.model.DataUpdateInfo;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Store;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.TicketItem;
import com.floreantpos.model.dao.DataUpdateInfoDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosBlinkButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.DeliveryDispatchTicketFilterPanel;
import com.floreantpos.ui.DeliveryDispatchTicketFilterPanel.DeliveryDispatchTicketFilterListener;
import com.floreantpos.ui.TicketListUpdateListener;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.PosGuiUtil;
import com.orocube.orocust.ui.DispatchTableCellRenderer;
import com.orocube.orocust.ui.TicketListTableModel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.Timer;
import javax.swing.border.TitledBorder;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.table.ColumnControlButton;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.joda.time.DateTime;
















public class DeliveryDispatchTicketListView
  extends JPanel
  implements ITicketList, DeliveryDispatchTicketFilterPanel.DeliveryDispatchTicketFilterListener
{
  private JXTable ticketListTable;
  private TicketListTableModel tableModel;
  private PosBlinkButton btnRefresh;
  private PosButton btnPrevious;
  private PosButton btnNext;
  private TableColumnModelExt columnModel;
  private ArrayList<TicketListUpdateListener> ticketUpdateListenerList = new ArrayList();
  
  private boolean isCustomerHistoryOpen;
  private List<Ticket> addedTicketList = new ArrayList();
  
  private Date lastUpdateTime;
  private Timer lastUpateCheckTimer = new Timer(15000, new TaskLastUpdateCheck(null));
  
  private DeliveryDispatchTicketFilterPanel filterPanel;
  
  private POSToggleButton btnOrderFilters;
  
  private DispatchTableCellRenderer renderer;
  
  private OrderType orderType;
  private Map<String, JToggleButton> seletedButtonList = new HashMap();
  private JToggleButton toggleButton;
  private JTextField tfCustomerName;
  private String selectedCustomerId;
  private POSToggleButton btnUnassigned;
  
  public DeliveryDispatchTicketListView()
  {
    setLayout(new BorderLayout());
    
    createTicketTable();
    updateTicketList();
    updateButtonStatus();
  }
  
  public DeliveryDispatchTicketListView(Integer customerId, boolean customerHistory) {
    isCustomerHistoryOpen = customerHistory;
    setLayout(new BorderLayout());
    
    createTicketTable();
    updateTicketList();
    updateButtonStatus();
  }
  
  private void createTicketTable() {
    ticketListTable = new JXTable();
    ticketListTable.setSortable(true);
    ticketListTable.setSelectionMode(0);
    ticketListTable.setColumnControlVisible(true);
    tableModel = new TicketListTableModel();
    tableModel.setPageSize(10);
    ticketListTable.setModel(tableModel);
    ticketListTable.setRowHeight(PosUIManager.getSize(60));
    ticketListTable.setAutoResizeMode(3);
    renderer = new DispatchTableCellRenderer();
    ticketListTable.setDefaultRenderer(Object.class, renderer);
    ticketListTable.setAutoscrolls(true);
    ticketListTable.setShowGrid(true);
    ticketListTable.setBorder(null);
    ticketListTable.setFocusable(false);
    ticketListTable.setDefaultRenderer(Object.class, new DispatchTableCellRenderer());
    ticketListTable.setGridColor(Color.LIGHT_GRAY);
    ticketListTable.getTableHeader().setPreferredSize(new Dimension(100, PosUIManager.getSize(40)));
    ticketListTable.setHighlighters(new Highlighter[] { new ColorHighlighter(new TicketTableColorHighlighterPredicate(), Color.white, Color.red) });
    
    columnModel = ((TableColumnModelExt)ticketListTable.getColumnModel());
    columnModel.getColumn(0).setPreferredWidth(80);
    columnModel.getColumn(1).setPreferredWidth(20);
    columnModel.getColumn(2).setPreferredWidth(100);
    columnModel.getColumn(3).setPreferredWidth(100);
    columnModel.getColumn(5).setPreferredWidth(30);
    columnModel.getColumn(6).setPreferredWidth(50);
    
    createScrollPane();
  }
  
  private void createScrollPane()
  {
    btnOrderFilters = new POSToggleButton();
    btnOrderFilters.setText("<html>" + Messages.getString("SwitchboardView.2") + "</html>");
    
    btnRefresh = new PosBlinkButton(Messages.getString("TicketListView.3"));
    btnPrevious = new PosButton(Messages.getString("TicketListView.4"));
    btnNext = new PosButton(Messages.getString("TicketListView.5"));
    
    createActionHandlers();
    
    PosScrollPane scrollPane = new PosScrollPane(ticketListTable, 20, 31);
    
    int height = PosUIManager.getSize(40);
    
    JPanel topButtonPanel = new JPanel(new MigLayout("ins 0", "grow", ""));
    ColumnControlButton controlButton = new ColumnControlButton(ticketListTable);
    if (!isCustomerHistoryOpen) {
      topButtonPanel.add(controlButton, "h " + height + "!, grow, wrap");
    }
    topButtonPanel.add(btnRefresh, "h " + height + "!, grow, wrap");
    topButtonPanel.add(btnPrevious, "h " + height + "!, grow, wrap");
    
    JPanel downButtonPanel = new JPanel(new MigLayout("ins 0", "grow", ""));
    downButtonPanel.add(btnNext, "h " + height + "!, grow, wrap");
    downButtonPanel.add(btnOrderFilters, "h " + height + "!, grow, wrap");
    
    JPanel tableButtonPanel = new JPanel(new BorderLayout());
    tableButtonPanel.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));
    tableButtonPanel.setPreferredSize(new Dimension(PosUIManager.getSize(80), 0));
    tableButtonPanel.add(topButtonPanel, "North");
    tableButtonPanel.add(downButtonPanel, "South");
    tableButtonPanel.add(scrollPane.getVerticalScrollBar());
    
    List<String> filters = new ArrayList();
    filters.add(POSConstants.ALL);
    filters.add(POSConstants.TODAY);
    filters.add(POSConstants.TOMORROW);
    filters.add("ONLINE");
    filters.add("PICK UP");
    filters.add("DELIVERY");
    
    JPanel customerFitlerPanel = new JPanel(new MigLayout("fillx"));
    customerFitlerPanel.setBorder(new TitledBorder(""));
    
    filterPanel = new DeliveryDispatchTicketFilterPanel(filters);
    filterPanel.addFilterListener(this);
    
    JLabel lblByCustomer = new JLabel("CUSTOMER");
    tfCustomerName = new JTextField(15);
    tfCustomerName.setEditable(false);
    
    PosButton btnSelect = new PosButton("SELECT");
    btnSelect.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        CustomerSelectorDialog dialog = CustomerSelectorFactory.createCustomerSelectorDialog(orderType);
        dialog.setLocationRelativeTo(Application.getPosWindow());
        dialog.setCreateNewTicket(false);
        dialog.setVisibleOnlySelectionButtons(true);
        dialog.openUndecoratedFullScreen();
        
        if (dialog.isCanceled())
          return;
        Customer customer = dialog.getSelectedCustomer();
        if (customer == null)
          return;
        tfCustomerName.setText(customer.getName());
        selectedCustomerId = customer.getId();
        updateTicketList();
      }
    });
    PosButton btnClear = new PosButton("CLEAR");
    btnClear.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        selectedCustomerId = null;
        tfCustomerName.setText("");
        updateTicketList();
      }
    });
    customerFitlerPanel.add(lblByCustomer, "split 4");
    customerFitlerPanel.add(tfCustomerName, "grow");
    customerFitlerPanel.add(btnSelect, "w " + PosUIManager.getSize(70));
    customerFitlerPanel.add(btnClear, "w " + PosUIManager.getSize(70));
    
    JPanel filterUnassignedPanel = new JPanel(new MigLayout("fillx"));
    btnUnassigned = new POSToggleButton("UNASSIGNED");
    btnUnassigned.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        updateTicketList();
      }
    });
    filterUnassignedPanel.setBorder(BorderFactory.createTitledBorder(""));
    filterUnassignedPanel.add(btnUnassigned, "growx,w " + PosUIManager.getSize(150));
    filterPanel.getContentPane().add(filterUnassignedPanel);
    filterPanel.getContentPane().add(customerFitlerPanel, "growx,span");
    
    add(filterPanel, "North");
    add(scrollPane);
    add(tableButtonPanel, "East");
  }
  
  public void createActionHandlers()
  {
    btnPrevious.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        if (tableModel.hasPrevious()) {
          tableModel.setCurrentRowIndex(tableModel.getPreviousRowIndex());
          DeliveryDispatchTicketListView.this.doPerformFilter();
        }
        updateButtonStatus();
      }
      
    });
    btnNext.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        if (tableModel.hasNext()) {
          tableModel.setCurrentRowIndex(tableModel.getNextRowIndex());
          DeliveryDispatchTicketListView.this.doPerformFilter();
        }
        updateButtonStatus();
      }
      
    });
    btnRefresh.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        getTableModel().setCurrentRowIndex(0);
        DeliveryDispatchTicketListView.this.doPerformFilter();
        updateButtonStatus();
        setTicketNeedSpecialAttension();
      }
      
    });
    btnOrderFilters.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        filterPanel.setCollapsed(!filterPanel.isCollapsed());
      }
    });
  }
  
  public void updateButtonStatus() {
    btnNext.setEnabled(tableModel.hasNext());
    btnPrevious.setEnabled(tableModel.hasPrevious());
  }
  
  public synchronized void updateTicketList() {
    toggleButton = filterPanel.getToggleButton();
    doPerformFilter();
  }
  
  public synchronized void setTicketList() {
    lastUpateCheckTimer.stop();
    try
    {
      if (orderType == null) {
        TicketListTableModel ticketListTableModel = getTableModel();
        ticketListTableModel.setCurrentRowIndex(0);
        ticketListTableModel.setNumRows(TicketDAO.getInstance().getNumTickets());
        TicketDAO.getInstance().loadTickets(ticketListTableModel);
        return;
      }
      Application.getPosWindow().setGlassPaneVisible(true);
      
      Date startDate = null;Date endDate = null;
      DateTime now = DateTime.now();
      DateTime todayStart = now.withTimeAtStartOfDay();
      DateTime tomorrowStart = now.plusDays(1).withTimeAtStartOfDay();
      DateTime tomorrowEnd = tomorrowStart.plusDays(1).withTimeAtStartOfDay();
      
      boolean isOnline = false;
      boolean isPickup = false;
      boolean isDelivery = false;
      
      JToggleButton todayToggleButton = (JToggleButton)seletedButtonList.get(POSConstants.TODAY);
      JToggleButton tomorrowToggleButton = (JToggleButton)seletedButtonList.get(POSConstants.TOMORROW);
      JToggleButton onlineToggleButton = (JToggleButton)seletedButtonList.get("ONLINE");
      JToggleButton pickUpToggleButton = (JToggleButton)seletedButtonList.get("PICK UP");
      JToggleButton deliveryToggleButton = (JToggleButton)seletedButtonList.get("DELIVERY");
      
      if ((todayToggleButton != null) && (todayToggleButton.isSelected())) {
        startDate = todayStart.toDate();
        endDate = tomorrowStart.toDate();
      }
      if ((tomorrowToggleButton != null) && (tomorrowToggleButton.isSelected())) {
        startDate = tomorrowStart.toDate();
        endDate = tomorrowEnd.toDate();
      }
      
      TicketListTableModel ticketListTableModel = getTableModel();
      ticketListTableModel.setNumRows(TicketDAO.getInstance().getNumTickets(startDate, endDate));
      
      if ((onlineToggleButton != null) && (onlineToggleButton.isSelected())) {
        isOnline = true;
      }
      if ((pickUpToggleButton != null) && (pickUpToggleButton.isSelected())) {
        isPickup = true;
      }
      if ((deliveryToggleButton != null) && (deliveryToggleButton.isSelected())) {
        isDelivery = true;
      }
      
      TicketDAO.getInstance().findTicketForDeliveryDispath(ticketListTableModel, orderType, selectedCustomerId, startDate, endDate, isOnline, isPickup, isDelivery, btnUnassigned
        .isSelected());
      updateButtonStatus();
      btnRefresh.setBlinking(false);
      
      for (int i = 0; i < ticketUpdateListenerList.size(); i++) {
        TicketListUpdateListener listener = (TicketListUpdateListener)ticketUpdateListenerList.get(i);
        listener.ticketListUpdated();
      }
    }
    catch (Exception e) {
      POSMessageDialog.showError(this, Messages.getString("SwitchboardView.19"), e);
    } finally {
      Application.getPosWindow().setGlassPaneVisible(false);
    }
    
    try
    {
      DataUpdateInfo lastUpdateInfo = DataUpdateInfoDAO.getLastUpdateInfo();
      
      if (lastUpdateInfo != null) {
        lastUpdateTime = new Date(lastUpdateInfo.getLastUpdateTime().getTime());
      }
    }
    catch (Exception e) {
      POSMessageDialog.showError(this, Messages.getString("SwitchboardView.20"), e);
    }
    
    lastUpateCheckTimer.restart();
  }
  

  public void addTicketListUpateListener(TicketListUpdateListener l) { ticketUpdateListenerList.add(l); }
  
  private class TaskLastUpdateCheck implements ActionListener {
    private TaskLastUpdateCheck() {}
    
    public void actionPerformed(ActionEvent e) {
      try { if (PosGuiUtil.isModalDialogShowing()) {
          return;
        }
        
        lastUpateCheckTimer.stop();
        
        DataUpdateInfo lastUpdateInfo = DataUpdateInfoDAO.getLastUpdateInfo();
        
        if ((lastUpdateInfo != null) && 
          (lastUpdateInfo.getLastUpdateTime().after(lastUpdateTime))) {
          btnRefresh.setBlinking(true);
        }
        


        setTicketNeedSpecialAttension();
        ticketListTable.repaint();
      } finally {
        lastUpateCheckTimer.restart();
      }
    }
  }
  
  public void setTicketNeedSpecialAttension()
  {
    Store store = Application.getInstance().getStore();
    if (store == null) {
      return;
    }
    String strMin = store.getProperty("deliveryConfig.highlightBeforeMinute");
    if (StringUtils.isEmpty(strMin)) {
      return;
    }
    int minutes = Integer.valueOf(strMin).intValue();
    if (minutes < 0) {
      return;
    }
    List ticketList = tableModel.getRows();
    if ((ticketList == null) || (ticketList.size() == 0)) {
      return;
    }
    long currentTimeMillis = System.currentTimeMillis();
    for (Iterator iterator = ticketList.iterator(); iterator.hasNext();) {
      Ticket ticket = (Ticket)iterator.next();
      Date deliveryDate = ticket.getDeliveryDate();
      if (deliveryDate != null)
      {

        long deliveryTimeMillis = deliveryDate.getTime();
        long timeDiffInMillis = deliveryTimeMillis - currentTimeMillis;
        long timeDiffInMin = timeDiffInMillis / 1000L / 60L;
        if (timeDiffInMin < minutes)
          ticket.setNeedSpecialAttention(true);
      }
    }
  }
  
  public void setTickets(List<Ticket> tickets) {
    List<Ticket> newTicketList = new ArrayList();
    if ((tickets != null) && (!tickets.isEmpty())) {
      for (Ticket ticket : tickets) {
        ticket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
        
        boolean sentToKitchen = false;
        boolean readyToDelivery = true;
        for (TicketItem ticketItem : ticket.getTicketItems()) {
          if (ticketItem.isPrintedToKitchen().booleanValue()) {
            sentToKitchen = true;
          }
          if (!ticketItem.getKitchenStatus().equals("Ready")) {
            readyToDelivery = false;
          }
        }
        if (sentToKitchen) {
          if (readyToDelivery) {
            ticket.setStatus("Ready");
            ticket.setSortOrder("1");
          }
          else {
            ticket.setStatus("Waiting (Kitchen)");
            ticket.setSortOrder("2");
          }
        }
        else {
          ticket.setStatus("Not Sent");
          ticket.setSortOrder("3");
        }
        newTicketList.add(ticket);
      }
      
      Collections.sort(newTicketList, new Comparator()
      {
        public int compare(Ticket o1, Ticket o2)
        {
          return o1.getSortOrder().compareTo(o2.getSortOrder());
        }
      });
    }
    tableModel.setRows(newTicketList);
  }
  
  public List<Ticket> getTickets() {
    return tableModel.getRows();
  }
  
  public void addTicket(Ticket ticket)
  {
    tableModel.addItem(ticket);
  }
  
  public Ticket getSelectedTicket() {
    int selectedRow = ticketListTable.getSelectedRow();
    if (selectedRow < 0) {
      return null;
    }
    
    return (Ticket)tableModel.getRowData(ticketListTable.convertRowIndexToModel(selectedRow));
  }
  
  public List<Ticket> getSelectedTickets() {
    int[] selectedRows = ticketListTable.getSelectedRows();
    
    ArrayList<Ticket> tickets = new ArrayList(selectedRows.length);
    
    for (int i = 0; i < selectedRows.length; i++) {
      Ticket ticket = (Ticket)tableModel.getRowData(ticketListTable.convertRowIndexToModel(selectedRows[i]));
      tickets.add(ticket);
    }
    
    return tickets;
  }
  
  public List<Ticket> getAddedTickets() {
    return addedTicketList;
  }
  
  public Ticket getFirstSelectedTicket() {
    List<Ticket> selectedTickets = getSelectedTickets();
    
    if ((selectedTickets.size() == 0) || (selectedTickets.size() > 1)) {
      POSMessageDialog.showMessage(Messages.getString("TicketListView.14"));
      return null;
    }
    
    Ticket ticket = (Ticket)selectedTickets.get(0);
    
    return ticket;
  }
  
  public String getFirstSelectedTicketId() {
    Ticket ticket = getFirstSelectedTicket();
    if (ticket == null) {
      return null;
    }
    
    return ticket.getId();
  }
  
  public JTable getTable() {
    return ticketListTable;
  }
  
  public TicketListTableModel getTableModel() {
    return tableModel;
  }
  
  public void setCurrentRowIndexZero() {
    getTableModel().setCurrentRowIndex(0);
  }
  
  public void setAutoUpdateCheck(boolean check)
  {
    if (check) {
      lastUpateCheckTimer.restart();
    }
    else {
      lastUpateCheckTimer.stop();
    }
  }
  

  public void updateCustomerTicketList(String customerId) {}
  
  private void doPerformFilter()
  {
    if (toggleButton == null) {
      return;
    }
    setTicketList();
  }
  
  public void filterSelected(JToggleButton toggleButton)
  {
    this.toggleButton = toggleButton;
    String actionCommand = toggleButton.getActionCommand();
    if (actionCommand.equals("PICK UP")) {
      JToggleButton deliveryButton = (JToggleButton)seletedButtonList.get("DELIVERY");
      if (deliveryButton != null) {
        deliveryButton.setSelected(false);
      }
    }
    else if (actionCommand.equals("DELIVERY")) {
      JToggleButton pickUpBtn = (JToggleButton)seletedButtonList.get("PICK UP");
      if (pickUpBtn != null) {
        pickUpBtn.setSelected(false);
      }
    }
    seletedButtonList.put(actionCommand, toggleButton);
    doPerformFilter();
  }
  
  public void setOrderType(OrderType orderType) {
    this.orderType = orderType;
  }
  
  public void filterUnSelected(JToggleButton toggleButton)
  {
    seletedButtonList.put(toggleButton.getActionCommand(), null);
    doPerformFilter();
  }
  
  class TicketTableColorHighlighterPredicate implements HighlightPredicate {
    TicketTableColorHighlighterPredicate() {}
    
    public boolean isHighlighted(Component renderer, ComponentAdapter adapter) { Ticket rowData = (Ticket)tableModel.getRowData(row);
      if (rowData.isNeedSpecialAttention()) {
        return true;
      }
      return false;
    }
  }
}
