package com.orocube.orocust.ui.view;

import com.floreantpos.bo.ui.ModelBrowser;
import com.floreantpos.model.Customer;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.ui.SearchPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;


public class CustomerSearchPanel
  extends SearchPanel<Customer>
{
  public JLabel lblName;
  public JTextField tfName;
  public JButton btnSearch;
  
  public CustomerSearchPanel()
  {
    setLayout(new MigLayout("", "[][]30[][]30[]", "[]20[]"));
    
    lblName = new JLabel("Name");
    tfName = new JTextField(15);
    
    btnSearch = new JButton("Search");
    add(lblName, "align label");
    add(tfName);
    add(btnSearch);
    


    Border loweredetched = BorderFactory.createEtchedBorder(1);
    TitledBorder title = BorderFactory.createTitledBorder(loweredetched, "Search Customer");
    title.setTitleJustification(1);
    setBorder(title);
    
    btnSearch.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        String name = tfName.getText();
        
        List<Customer> customerList = CustomerDAO.getInstance().findByName(name);
        modelBrowser.setModels(customerList);
      }
    });
  }
}
