package com.orocube.orocust.ui.view;

import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.DeliveryCharge;
import com.floreantpos.model.dao.DeliveryChargeDAO;
import com.floreantpos.swing.DoubleTextField;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.MessageDialog;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.util.POSUtil;
import javax.swing.JLabel;
import net.miginfocom.swing.MigLayout;





























public class DeliveryChargeForm
  extends BeanEditor
{
  private DoubleTextField tfStartRange;
  private DoubleTextField tfEndRange;
  private DoubleTextField tfChargeAmount;
  private FixedLengthTextField tfName;
  private FixedLengthTextField tfZipCode;
  
  public DeliveryChargeForm()
  {
    this(new DeliveryCharge());
  }
  
  public DeliveryChargeForm(DeliveryCharge deliveryCharge) {
    initComponents();
    setBean(deliveryCharge);
  }
  
  private void initComponents() {
    setLayout(new MigLayout("fill"));
    
    JLabel lblName = new JLabel("Name :", 4);
    JLabel lblStartRange = new JLabel("Start range:", 4);
    JLabel lblEndRange = new JLabel("End range:", 4);
    JLabel lblChargeAmount = new JLabel("Charge amount :", 4);
    JLabel lblZipCode = new JLabel("Zip Code", 4);
    
    tfStartRange = new DoubleTextField();
    tfEndRange = new DoubleTextField();
    tfChargeAmount = new DoubleTextField();
    tfName = new FixedLengthTextField();
    tfName.setLength(220);
    tfZipCode = new FixedLengthTextField();
    
    add(lblName, "grow");
    add(tfName, "grow, wrap");
    
    add(lblZipCode, "grow");
    add(tfZipCode, "grow,wrap");
    
    add(lblStartRange, "grow");
    add(tfStartRange, "grow, wrap");
    
    add(lblEndRange, "grow");
    add(tfEndRange, "grow, wrap");
    
    add(lblChargeAmount, "grow");
    add(tfChargeAmount, "grow, wrap");
  }
  
  public boolean save()
  {
    try
    {
      if (!updateModel()) {
        return false;
      }
      DeliveryCharge deliveryCharge = (DeliveryCharge)getBean();
      DeliveryChargeDAO dao = new DeliveryChargeDAO();
      dao.saveOrUpdate(deliveryCharge);
    } catch (Exception e) {
      MessageDialog.showError(e);
      return false;
    }
    return true;
  }
  
  protected void updateView()
  {
    DeliveryCharge deliveryCharge = (DeliveryCharge)getBean();
    if (deliveryCharge.getId() == null) {
      tfName.setText("");
      return;
    }
    tfName.setText(deliveryCharge.getName());
    tfZipCode.setText(deliveryCharge.getZipCode());
    tfStartRange.setText(String.valueOf(deliveryCharge.getStartRange()));
    tfEndRange.setText(String.valueOf(deliveryCharge.getEndRange()));
    tfChargeAmount.setText(String.valueOf(deliveryCharge.getChargeAmount()));
  }
  
  protected boolean updateModel()
  {
    DeliveryCharge deliveryCharge = (DeliveryCharge)getBean();
    
    String name = tfName.getText();
    String startRange = tfStartRange.getText();
    String endRange = tfEndRange.getText();
    String chargeAmount = tfChargeAmount.getText();
    
    if (POSUtil.isBlankOrNull(name)) {
      BOMessageDialog.showError("Name cannot be empty.");
      return false;
    }
    if (POSUtil.isBlankOrNull(startRange)) {
      BOMessageDialog.showError("Start Range cannot be empty.");
      return false;
    }
    if (POSUtil.isBlankOrNull(endRange)) {
      BOMessageDialog.showError("End Range cannot be empty.");
      return false;
    }
    if (POSUtil.isBlankOrNull(chargeAmount)) {
      BOMessageDialog.showError("Charge Amount cannot be empty.");
      return false;
    }
    
    deliveryCharge.setName(tfName.getText());
    deliveryCharge.setZipCode(tfZipCode.getText());
    deliveryCharge.setStartRange(Double.valueOf(tfStartRange.getDouble()));
    deliveryCharge.setEndRange(Double.valueOf(tfEndRange.getDouble()));
    deliveryCharge.setChargeAmount(Double.valueOf(tfChargeAmount.getDouble()));
    
    return true;
  }
  
  public String getDisplayText() {
    DeliveryCharge deliveryCharge = (DeliveryCharge)getBean();
    if (deliveryCharge.getId() == null) {
      return "New Delivery Charge";
    }
    return "Edit Delivery Charge";
  }
}
