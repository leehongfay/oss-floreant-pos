package com.orocube.orocust.ui.view;

import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.explorer.ExplorerButtonPanel;
import com.floreantpos.model.CustomerGroup;
import com.floreantpos.model.dao.CustomerGroupDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orocube.orocust.Messages;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.jdesktop.swingx.JXTable;





public class CustomerGroupExplorer
  extends TransparentPanel
{
  private static final long serialVersionUID = 1L;
  private JXTable table;
  private BeanTableModel<CustomerGroup> tableModel;
  
  public CustomerGroupExplorer()
  {
    setLayout(new BorderLayout(5, 5));
    tableModel = new BeanTableModel(CustomerGroup.class);
    tableModel.addColumn(Messages.getString("CustomerGroupExplorer.0"), "name");
    tableModel.addColumn(Messages.getString("CustomerGroupExplorer.1"), "code");
    tableModel.addColumn(Messages.getString("CustomerGroupExplorer.2"), "description");
    
    tableModel.addRows(CustomerGroupDAO.getInstance().findAll());
    
    table = new JXTable(tableModel);
    table.setRowHeight(PosUIManager.getSize(30));
    table.addMouseListener(new MouseAdapter()
    {
      public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {
          CustomerGroupExplorer.this.editSelectedRow();
        }
      }
    });
    add(new JScrollPane(table));
    add(createButtonPanel(), "South");
    add(buildSearchForm(), "North");
  }
  

  private TransparentPanel createButtonPanel()
  {
    ExplorerButtonPanel explorerButton = new ExplorerButtonPanel();
    JButton detailButton = explorerButton.getDetailButton();
    JButton editButton = explorerButton.getEditButton();
    JButton addButton = explorerButton.getAddButton();
    JButton deleteButton = explorerButton.getDeleteButton();
    
    detailButton.addActionListener(new ActionListener()
    {

      public void actionPerformed(ActionEvent e)
      {
        openCustomerList(true);
      }
      
    });
    addButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e) {
        try {
          CustomerGroupForm editor = new CustomerGroupForm(new CustomerGroup());
          
          BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
          dialog.setPreferredSize(PosUIManager.getSize(500, 550));
          dialog.open();
          
          if (dialog.isCanceled()) {
            return;
          }
          CustomerGroup customerGroup = (CustomerGroup)editor.getBean();
          
          tableModel.addRow(customerGroup);
        }
        catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    editButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        CustomerGroupExplorer.this.editSelectedRow();
      }
      
    });
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          int index = table.getSelectedRow();
          if (index < 0) {
            return;
          }
          index = table.convertRowIndexToModel(index);
          
          if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) != 0) {
            return;
          }
          CustomerGroup customerGroup = (CustomerGroup)tableModel.getRow(index);
          
          CustomerGroupDAO customerGroupDAO = new CustomerGroupDAO();
          customerGroupDAO.delete(customerGroup);
          
          tableModel.removeRow(index);
        } catch (Throwable x) {
          BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
        }
        
      }
      
    });
    TransparentPanel panel = new TransparentPanel();
    
    panel.add(detailButton);
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    
    return panel;
  }
  
  private JPanel buildSearchForm() {
    JPanel panel = new JPanel();
    
    return panel;
  }
  
  private void editSelectedRow() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      
      CustomerGroup customerGroup = (CustomerGroup)tableModel.getRow(index);
      CustomerGroupDAO.getInstance().initialize(customerGroup);
      
      tableModel.setRow(index, customerGroup);
      CustomerGroupForm editor = new CustomerGroupForm(customerGroup);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.setPreferredSize(PosUIManager.getSize(500, 550));
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  public void openCustomerList(boolean viewMode) {
    try { int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      CustomerGroup customerGroup = (CustomerGroup)tableModel.getRow(index);
      
      CustomerGroupDetailForm editor = new CustomerGroupDetailForm(customerGroup);
      editor.setEditable(!viewMode);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      if (viewMode)
        dialog.getButtonPanel().remove(0);
      dialog.openWithScale(650, 600);
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    }
    catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
}
