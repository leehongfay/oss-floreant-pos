package com.orocube.orocust.ui.view;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.bo.ui.CustomCellRenderer;
import com.floreantpos.main.Application;
import com.floreantpos.model.DeliveryCharge;
import com.floreantpos.model.Store;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.dao.DeliveryChargeDAO;
import com.floreantpos.model.dao.StoreDAO;
import com.floreantpos.swing.BeanTableModel;
import com.floreantpos.swing.FixedLengthDocument;
import com.floreantpos.swing.FixedLengthTextField;
import com.floreantpos.swing.IntegerTextField;
import com.floreantpos.swing.TransparentPanel;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.ConfirmDeleteDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.net.URI;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.TableColumnExt;
import org.jdesktop.swingx.table.TableColumnModelExt;




















public class OrocustConfigurationView
  extends JPanel
{
  private JXTable table;
  private BeanTableModel<DeliveryCharge> tableModel;
  private JComboBox cbUnits;
  private JCheckBox chkChargeByZipCode;
  private JTextArea taTerminalLocation;
  private FixedLengthTextField tfMapApiKey;
  private TableColumnModelExt columnModel;
  private JLabel lblHighlightMin;
  private IntegerTextField tfMin;
  private Store store;
  private JLabel lblDefaultOrderPrepTime;
  private IntegerTextField tfPreperationTime;
  private JCheckBox chkCallerIdEnable;
  private JComboBox cbCallerIds;
  
  public OrocustConfigurationView()
  {
    initComponents();
    initializeData();
  }
  
  public String getName()
  {
    return POSConstants.CONFIG_TAB_DELIVERY;
  }
  
  public void initializeData() {
    store = Application.getInstance().getStore();
    String strChargeByZipCode = store.getProperty("deliveryConfig.zipcode");
    String strHighlightBeforeMin = store.getProperty("deliveryConfig.highlightBeforeMinute");
    String strLengthUnitName = store.getProperty("deliveryConfig.unitName");
    String strPreperationTime = store.getProperty("deliveryConfig.preperationTime");
    if ((StringUtils.isEmpty(strChargeByZipCode)) || (StringUtils.isEmpty(strHighlightBeforeMin)) || (StringUtils.isEmpty(strPreperationTime))) {
      strChargeByZipCode = "false";
      strHighlightBeforeMin = "0";
      strPreperationTime = "40";
      
      store.addProperty("deliveryConfig.highlightBeforeMinute", strHighlightBeforeMin);
      store.addProperty("deliveryConfig.zipcode", strHighlightBeforeMin);
      store.addProperty("deliveryConfig.preperationTime", strPreperationTime);
      StoreDAO.getInstance().saveOrUpdate(store);
      Application.getInstance().refreshStore();
    }
    
    if (StringUtils.isNotEmpty(strLengthUnitName)) {
      if (strLengthUnitName.equals("MILE")) {
        cbUnits.setSelectedItem("MILE");
      }
      else {
        cbUnits.setSelectedItem("KM");
      }
    }
    
    tfPreperationTime.setText(strPreperationTime);
    
    cbCallerIds.setSelectedItem(store.getProperty("callerId.device"));
    chkCallerIdEnable.setSelected(Boolean.valueOf(store.getProperty("callerId.CallerIdEnable")).booleanValue());
    setVisiblity(chkCallerIdEnable.isSelected());
    
    Terminal terminal = Application.getInstance().getTerminal();
    taTerminalLocation.setText(terminal.getLocation());
    
    boolean isChargeByZipCode = Boolean.valueOf(strChargeByZipCode).booleanValue();
    chkChargeByZipCode.setSelected(isChargeByZipCode);
    Integer highlightBeforeMinute = Integer.valueOf(strHighlightBeforeMin);
    if (highlightBeforeMinute != null) {
      tfMin.setText(String.valueOf(highlightBeforeMinute));
    }
    
    String map_api_key = store.getProperty("google.map.api.key", "AIzaSyDc-5LFTSC-bB9kQcZkM74LHUxwndRy_XM");
    



    tfMapApiKey.setText(map_api_key);
    columnModel.getColumnExt("START RANGE").setVisible(!chkChargeByZipCode.isSelected());
    columnModel.getColumnExt("END RANGE").setVisible(!chkChargeByZipCode.isSelected());
  }
  
  public boolean save() {
    String unit = cbUnits.getSelectedItem().toString();
    int min = tfMin.getInteger();
    int preperationTime = tfPreperationTime.getInteger();
    
    store.addProperty("deliveryConfig.unitName", unit);
    store.addProperty("deliveryConfig.zipcode", String.valueOf(chkChargeByZipCode.isSelected()));
    
    store.addProperty("deliveryConfig.highlightBeforeMinute", String.valueOf(min));
    store.addProperty("deliveryConfig.preperationTime", String.valueOf(preperationTime));
    
    store.addProperty("callerId.CallerIdEnable", String.valueOf(chkCallerIdEnable.isSelected()));
    store.addProperty("callerId.device", String.valueOf(cbCallerIds.getSelectedItem()));
    store.addProperty("google.map.api.key", tfMapApiKey.getText());
    
    Terminal terminal = Application.getInstance().getTerminal();
    terminal.setLocation(taTerminalLocation.getText());
    









    StoreDAO.getInstance().saveOrUpdate(store);
    Application.getInstance().refreshStore();
    POSMessageDialog.showMessage(POSUtil.getFocusedWindow(), "Successfuly Saved");
    return true;
  }
  
  private void initComponents() {
    setLayout(new BorderLayout());
    
    chkCallerIdEnable = new JCheckBox(Messages.getString("PeripheralConfigurationView.24"));
    
    Vector callerIds = new Vector();
    callerIds.add("NONE");
    callerIds.add("AD101");
    callerIds.add("Whozz calling");
    cbCallerIds = new JComboBox(callerIds);
    
    JPanel topConfigPanel = new JPanel(new MigLayout("", "[][][]", ""));
    lblHighlightMin = new JLabel("Highlight ticket if delivery time is less then:");
    tfMin = new IntegerTextField(5);
    
    lblDefaultOrderPrepTime = new JLabel("Default order preperation time:");
    tfPreperationTime = new IntegerTextField(5);
    
    chkCallerIdEnable.addItemListener(new ItemListener()
    {
      public void itemStateChanged(ItemEvent e)
      {
        JCheckBox chkbox = (JCheckBox)e.getSource();
        OrocustConfigurationView.this.setVisiblity(chkbox.isSelected());
      }
      

    });
    topConfigPanel.add(chkCallerIdEnable, "skip 1,wrap");
    topConfigPanel.add(new JLabel("Caller Id device:"), "right");
    topConfigPanel.add(cbCallerIds, "wrap,grow");
    topConfigPanel.add(lblDefaultOrderPrepTime, "right");
    topConfigPanel.add(tfPreperationTime, "growx");
    topConfigPanel.add(new JLabel("Min"), "wrap");
    
    topConfigPanel.add(lblHighlightMin, "");
    topConfigPanel.add(tfMin, "growx");
    topConfigPanel.add(new JLabel("Min"));
    
    JPanel contentPanel = new JPanel();
    contentPanel.setLayout(new MigLayout("fill", "", ""));
    contentPanel.setBorder(new TitledBorder("Delivery Config"));
    
    JButton addButton = new JButton(POSConstants.ADD);
    addButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        OrocustConfigurationView.this.doAdd();
      }
      

    });
    JButton editButton = new JButton(POSConstants.EDIT);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        OrocustConfigurationView.this.doEdit();
      }
      
    });
    JButton deleteButton = new JButton(POSConstants.DELETE);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        OrocustConfigurationView.this.doDelete();
      }
      

    });
    tableModel = new BeanTableModel(DeliveryCharge.class);
    tableModel.addColumn(POSConstants.NAME.toUpperCase(), "name");
    tableModel.addColumn("ZIP CODE", "zipCode");
    tableModel.addColumn("START RANGE", "startRange");
    tableModel.addColumn("END RANGE", "endRange");
    tableModel.addColumn("CHARGE AMOUNT", "chargeAmount");
    
    tableModel.addRows(DeliveryChargeDAO.getInstance().findAll());
    
    table = new JXTable(tableModel);
    table.setDefaultRenderer(Object.class, new CustomCellRenderer());
    
    columnModel = ((TableColumnModelExt)table.getColumnModel());
    
    JPanel deliveryChargePanel = new JPanel(new MigLayout("fill"));
    deliveryChargePanel.setBorder(new TitledBorder("Delivery Charge"));
    
    TransparentPanel panel = new TransparentPanel();
    panel.add(addButton);
    panel.add(editButton);
    panel.add(deleteButton);
    deliveryChargePanel.add(new JScrollPane(table), "grow,wrap");
    deliveryChargePanel.add(panel, "grow,center");
    
    JLabel lblLengthUnit = new JLabel("Length unit: ");
    Vector units = new Vector();
    units.add("KM");
    units.add("MILE");
    
    cbUnits = new JComboBox(units);
    taTerminalLocation = new JTextArea(new FixedLengthDocument(320));
    
    chkChargeByZipCode = new JCheckBox("Charge by zip code");
    chkChargeByZipCode.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        columnModel.getColumnExt("START RANGE").setVisible(!chkChargeByZipCode.isSelected());
        columnModel.getColumnExt("END RANGE").setVisible(!chkChargeByZipCode.isSelected());
      }
      
    });
    tfMapApiKey = new FixedLengthTextField();
    tfMapApiKey.setLength(220);
    
    JButton btnGetMapKey = new JButton("Get Map Key");
    btnGetMapKey.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        String link = "https://console.developers.google.com/flows/enableapi?apiid=places_backend&keyType=SERVER_SIDE";
        try {
          OrocustConfigurationView.this.openBrowser(link);
        }
        catch (Exception localException) {}
      }
    });
    btnGetMapKey.setFont(new Font(getFont().getName(), 1, 11));
    
    JPanel centerConfigPanel = new JPanel(new MigLayout("left", "[][]", ""));
    centerConfigPanel.add(lblLengthUnit, "right");
    centerConfigPanel.add(cbUnits, "w 150!,wrap");
    centerConfigPanel.add(new JLabel("Map API Key: "), "right");
    centerConfigPanel.add(tfMapApiKey, "split 2");
    centerConfigPanel.add(btnGetMapKey, "wrap");
    JLabel lblLocation = new JLabel("Terminal location: ");
    centerConfigPanel.add(lblLocation, "right");
    centerConfigPanel.add(new JScrollPane(taTerminalLocation), "w 500!, h 100!,wrap");
    centerConfigPanel.add(chkChargeByZipCode, "skip 1");
    
    contentPanel.add(centerConfigPanel, "grow, wrap");
    contentPanel.add(deliveryChargePanel, "grow");
    

    JPanel saveBtnPanel = new JPanel(new MigLayout("center"));
    JButton btnSave = new JButton(POSConstants.SAVE);
    saveBtnPanel.add(btnSave, "center");
    
    btnSave.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        save();
      }
      
    });
    add(topConfigPanel, "North");
    add(contentPanel, "Center");
    
    add(saveBtnPanel, "South");
  }
  
  private void openBrowser(String link) throws Exception {
    URI uri = new URI(link);
    if (Desktop.isDesktopSupported()) {
      Desktop.getDesktop().browse(uri);
    }
  }
  
  private void doAdd() {
    try {
      DeliveryChargeForm editor = new DeliveryChargeForm();
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), editor);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      DeliveryCharge deliveryCharge = (DeliveryCharge)editor.getBean();
      tableModel.addRow(deliveryCharge);
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doEdit() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      DeliveryCharge deliveryCharge = (DeliveryCharge)tableModel.getRow(index);
      
      DeliveryChargeForm deliveryChargeForm = new DeliveryChargeForm(deliveryCharge);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getBackOfficeWindow(), deliveryChargeForm);
      dialog.open();
      if (dialog.isCanceled()) {
        return;
      }
      table.repaint();
    } catch (Throwable x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void doDelete() {
    try {
      int index = table.getSelectedRow();
      if (index < 0) {
        return;
      }
      index = table.convertRowIndexToModel(index);
      DeliveryCharge deliveryCharge = (DeliveryCharge)tableModel.getRow(index);
      
      if (ConfirmDeleteDialog.showMessage(POSUtil.getBackOfficeWindow(), POSConstants.CONFIRM_DELETE, POSConstants.DELETE) == 0)
      {
        DeliveryChargeDAO.getInstance().delete(deliveryCharge);
        tableModel.removeRow(index);
      }
    } catch (Exception x) {
      BOMessageDialog.showError(POSConstants.ERROR_MESSAGE, x);
    }
  }
  
  private void setVisiblity(boolean isVisible) {
    cbCallerIds.setEnabled(isVisible);
    tfMin.setEnabled(isVisible);
    tfPreperationTime.setEnabled(isVisible);
  }
}
