package com.orocube.orocust.ui.view;

import com.floreantpos.IconFactory;
import com.floreantpos.Messages;
import com.floreantpos.PosLog;
import com.floreantpos.customer.CustomerListTableModel;
import com.floreantpos.customer.CustomerSelector;
import com.floreantpos.customer.CustomerTable;
import com.floreantpos.extension.OrderServiceExtension;
import com.floreantpos.extension.OrderServiceFactory;
import com.floreantpos.model.Customer;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.swing.POSTextField;
import com.floreantpos.swing.PosBlinkButton;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.swing.QwertyKeyPad;
import com.floreantpos.ui.dialog.BeanEditorDialog;
import com.floreantpos.ui.dialog.POSDialog;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.forms.QuickCustomerForm;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.TicketAlreadyExistsException;
import com.orocube.orocust.ui.dialog.CustomerHistoryDialog;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.JTableHeader;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang.StringUtils;























public class CustomerListView
  extends CustomerSelector
{
  private PosBlinkButton btnCreateNewCustomer;
  private CustomerTable customerTable;
  private POSTextField tfSearchString;
  private PosButton btnInfo;
  protected Customer selectedCustomer;
  private PosButton btnRemoveCustomer;
  private Ticket ticket;
  private QwertyKeyPad qwertyKeyPad;
  private PosButton btnCancel;
  private static CustomerListView instance;
  private PosBlinkButton btnSelect;
  private CustomerListTableModel customerListTableModel;
  private PosButton btnNext;
  private PosButton btnPrevious;
  private JLabel lblNumberOfItem;
  private PosButton btnHistory;
  private PosButton btnSearch;
  
  public CustomerListView()
  {
    initUI();
  }
  
  public void initUI() {
    setLayout(new BorderLayout(5, 5));
    setBorder(new EmptyBorder(5, 0, 0, 0));
    JPanel searchPanel = new JPanel(new MigLayout("fill,inset 5 0 5 0"));
    
    tfSearchString = new POSTextField(16);
    tfSearchString.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        customerListTableModel.setCurrentRowIndex(0);
        doSearchCustomer();
      }
      
    });
    PosButton btnKeyboard = new PosButton(IconFactory.getIcon("/images/", "keyboard.png"));
    btnKeyboard.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        qwertyKeyPad.setCollapsed(!qwertyKeyPad.isCollapsed());
      }
      
    });
    btnSearch = new PosButton(Messages.getString("CustomerSelectionDialog.15"));
    btnSearch.setFocusable(false);
    btnSearch.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        customerListTableModel.setCurrentRowIndex(0);
        doSearchCustomer();
      }
    });
    searchPanel.add(tfSearchString, "split 3,grow,span");
    searchPanel.add(btnKeyboard, "h " + PosUIManager.getSize(45) + "!");
    searchPanel.add(btnSearch, "w " + PosUIManager.getSize(100) + "!,h " + PosUIManager.getSize(45) + "!");
    
    JPanel centerPanel = new JPanel(new BorderLayout());
    centerPanel.setBorder(new TitledBorder(null, "SEARCH BY PHONE OR NAME", 2, 2, null, null));
    
    JPanel customerListPanel = new JPanel(new BorderLayout(5, 5));
    customerListPanel.setBorder(new EmptyBorder(5, 5, 2, 5));
    
    customerTable = new CustomerTable();
    customerListTableModel = new CustomerListTableModel();
    customerListTableModel.setPageSize(20);
    customerTable.setModel(customerListTableModel);
    customerTable.setFocusable(false);
    customerTable.setRowHeight(35);
    customerTable.getTableHeader().setPreferredSize(new Dimension(100, 35));
    customerTable.getSelectionModel().setSelectionMode(0);
    customerTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        selectedCustomer = customerTable.getSelectedCustomer();
        btnSelect.setBlinking(true);
        btnCreateNewCustomer.setBlinking(false);
      }
    });
    PosScrollPane scrollPane = new PosScrollPane();
    scrollPane.setFocusable(false);
    scrollPane.setViewportView(customerTable);
    
    customerListPanel.add(searchPanel, "North");
    customerListPanel.add(scrollPane, "Center");
    
    qwertyKeyPad = new QwertyKeyPad();
    qwertyKeyPad.setCollapsed(false);
    
    customerListPanel.add(qwertyKeyPad, "South");
    
    centerPanel.add(customerListPanel, "Center");
    
    add(centerPanel, "Center");
    
    JPanel panel = new JPanel(new MigLayout("fillx, ins 0 0 0 0", "[center, grow][]", ""));
    btnInfo = new PosButton(Messages.getString("CustomerSelectionDialog.23"));
    btnInfo.setFocusable(false);
    btnInfo.setEnabled(false);
    
    btnHistory = new PosButton("<html><center>CUSTOMER HISTORY<center><html>");
    btnHistory.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Customer customer = customerTable.getSelectedCustomer();
        selectedCustomer = customer;
        
        if (customer == null) {
          POSMessageDialog.showError(POSUtil.getFocusedWindow(), Messages.getString("CustomerSelectionDialog.27"));
          return;
        }
        CustomerListView.this.openCustomerHistory(customer);
      }
      
    });
    btnCreateNewCustomer = new PosBlinkButton("<html><center>CREATE NEW CUSTOMER<center><html>");
    btnCreateNewCustomer.setFocusable(false);
    btnCreateNewCustomer.setBlinking(false);
    
    btnCreateNewCustomer.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btnCreateNewCustomer.setBlinking(false);
        doCreateNewCustomer();
      }
      
    });
    btnRemoveCustomer = new PosButton(Messages.getString("CustomerSelectionDialog.26"));
    btnRemoveCustomer.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        doRemoveCustomerFromTicket();
      }
      
    });
    btnSelect = new PosBlinkButton(Messages.getString("CustomerSelectionDialog.28"));
    btnSelect.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        btnSelect.setBlinking(false);
        if (isCreateNewTicket()) {
          CustomerListView.this.doCreateNewTicket();
        }
        else {
          closeDialog(false);
        }
      }
    });
    btnSelect.setBlinking(false);
    
    btnCancel = new PosButton(Messages.getString("CustomerSelectionDialog.29"));
    btnCancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        closeDialog(true);
      }
      
    });
    btnNext = new PosButton("NEXT");
    btnNext.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (customerListTableModel.hasNext()) {
          customerListTableModel.setCurrentRowIndex(customerListTableModel.getNextRowIndex());
          doSearchCustomer();
        }
        
      }
    });
    btnPrevious = new PosButton("PREVIOUS");
    btnPrevious.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if (customerListTableModel.hasPrevious()) {
          customerListTableModel.setCurrentRowIndex(customerListTableModel.getPreviousRowIndex());
          doSearchCustomer();
        }
        updateButtonStatus();
      }
      
    });
    Dimension size = PosUIManager.getSize(96, 50);
    
    panel.add(btnHistory, "w " + width + "!, h " + height + "!, split 4");
    panel.add(btnCreateNewCustomer, "w " + width + "!, h " + height + "!");
    
    panel.add(btnSelect, "w " + width + "!, h " + height + "!");
    panel.add(btnCancel, "w " + width + "!, h " + height + "!");
    
    lblNumberOfItem = new JLabel();
    panel.add(lblNumberOfItem);
    panel.add(btnPrevious);
    panel.add(btnNext);
    
    centerPanel.add(panel, "South");
    updateButtonStatus();
  }
  
  public void updateButtonStatus() {
    btnNext.setEnabled(customerListTableModel.hasNext());
    btnPrevious.setEnabled(customerListTableModel.hasPrevious());
  }
  
  private void openCustomerHistory(Customer customer) {
    CustomerHistoryDialog dialog = new CustomerHistoryDialog(customer.getId(), this);
    dialog.openUndecoratedFullScreen();
  }
  
  private void loadCustomer() {
    Customer customer = getCustomer();
    customerListTableModel.setCurrentRowIndex(0);
    if (customer != null) {
      tfSearchString.setText(customer.getMobileNo());
      doSearchCustomer();
    }
    else {
      tfSearchString.setText(getCallerId());
      doSearchCustomer();
    }
  }
  
  protected void doSetCustomer(Customer customer) {
    if (ticket != null) {
      ticket.setCustomer(customer);
      TicketDAO.getInstance().saveOrUpdate(ticket);
    }
  }
  
  protected void doRemoveCustomerFromTicket() {
    int option = POSMessageDialog.showYesNoQuestionDialog(this, 
      Messages.getString("CustomerSelectionDialog.2"), Messages.getString("CustomerSelectionDialog.32"));
    if (option != 0) {
      return;
    }
    
    ticket.removeCustomer();
    TicketDAO.getInstance().saveOrUpdate(ticket);
  }
  
  private void doCreateNewTicket()
  {
    try
    {
      Customer selectedCustomer = getSelectedCustomer();
      
      if (selectedCustomer == null) {
        POSMessageDialog.showError(POSUtil.getFocusedWindow(), Messages.getString("CustomerSelectionDialog.27"));
        return;
      }
      closeDialog(false);
      OrderServiceFactory.getOrderService().createNewTicket(getOrderType(), null, selectedCustomer);
    }
    catch (TicketAlreadyExistsException localTicketAlreadyExistsException) {}
  }
  
  protected void doSearchCustomer()
  {
    try
    {
      setCursor(Cursor.getPredefinedCursor(3));
      qwertyKeyPad.setCollapsed(true);
      String searchString = tfSearchString.getText();
      
      if (StringUtils.isEmpty(searchString)) {
        customerListTableModel.setNumRows(CustomerDAO.getInstance().getNumberOfCustomers());
        CustomerDAO.getInstance().loadCustomers(customerListTableModel);
      }
      else {
        customerListTableModel.setNumRows(CustomerDAO.getInstance().getNumberOfCustomers(searchString));
        CustomerDAO.getInstance().findByPhoneOrName(searchString, customerListTableModel);
      }
      if (customerListTableModel.getRows().size() == 0) {
        btnSelect.setBlinking(false);
        btnCreateNewCustomer.setBlinking(true);
      }
      else {
        btnSelect.setBlinking(true);
        btnCreateNewCustomer.setBlinking(false);
      }
      
      int startNumber = customerListTableModel.getCurrentRowIndex() + 1;
      int endNumber = customerListTableModel.getNextRowIndex();
      int totalNumber = customerListTableModel.getNumRows();
      if (endNumber > totalNumber) {
        endNumber = totalNumber;
      }
      lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
      
      customerListTableModel.fireTableDataChanged();
      customerTable.repaint();
      updateButtonStatus();
    } catch (Exception e) {
      PosLog.error(CustomerListView.class, e);
      e.printStackTrace();
    } finally {
      setCursor(Cursor.getDefaultCursor());
    }
  }
  
  public void closeDialog(boolean canceled) {
    Window windowAncestor = SwingUtilities.getWindowAncestor(this);
    if ((windowAncestor instanceof POSDialog)) {
      ((POSDialog)windowAncestor).setCanceled(canceled);
      windowAncestor.dispose();
    }
  }
  
  protected void doCreateNewCustomer() {
    try {
      boolean setKeyPad = true;
      
      QuickCustomerForm form = new QuickCustomerForm(setKeyPad);
      
      form.enableCustomerFields(true);
      String phoneNo = tfSearchString.getText();
      if (StringUtils.isEmpty(phoneNo)) {
        phoneNo = getCallerId();
      }
      form.setPhoneNo(phoneNo);
      BeanEditorDialog dialog = new BeanEditorDialog(POSUtil.getFocusedWindow(), form);
      dialog.setResizable(false);
      dialog.open();
      
      if (!dialog.isCanceled()) {
        selectedCustomer = ((Customer)form.getBean());
        
        CustomerListTableModel model = (CustomerListTableModel)customerTable.getModel();
        model.addItem(selectedCustomer);
        if (selectedCustomer.getMobileNo() == null) {
          tfSearchString.setText(selectedCustomer.getFirstName());
        }
        else {
          tfSearchString.setText(selectedCustomer.getMobileNo());
        }
        customerListTableModel.setCurrentRowIndex(0);
        doSearchCustomer();
        customerTable.getSelectionModel().addSelectionInterval(0, 0);
      }
    } catch (Exception e) {
      POSMessageDialog.showError(this, e.getMessage(), e);
    }
  }
  
  public String getName()
  {
    return "C";
  }
  
  public Customer getSelectedCustomer() {
    return selectedCustomer;
  }
  
  public void setRemoveButtonEnable(boolean enable) {
    btnRemoveCustomer.setEnabled(enable);
  }
  
  public void updateView(boolean update)
  {
    btnCreateNewCustomer.setVisible(!isVisibleOnlySelectionButtons());
    btnHistory.setVisible(!isVisibleOnlySelectionButtons());
    if (update) {
      btnCancel.setVisible(true);
    }
    else {
      btnCancel.setVisible(false);
    }
    loadCustomer();
  }
  

  public void redererCustomers() {}
  
  public static CustomerListView getInstance()
  {
    if (instance == null) {
      instance = new CustomerListView();
    }
    return instance;
  }
}
