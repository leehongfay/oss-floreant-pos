package com.orocube.orocust.ui.view;

import com.floreantpos.model.Store;
import com.floreantpos.model.util.DataProvider;
import com.floreantpos.swing.PosButton;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.NumberUtil;
import com.orocube.orocust.ui.util.GMapUtil;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;
import net.miginfocom.swing.MigLayout;

























public class MapBrowser
  extends JPanel
{
  private JLabel lblDistance;
  private JPanel headerPanel;
  private MapPanel mapView;
  private JPanel footerPanel;
  private String source;
  private String destination;
  private static int zoom = 12;
  private double distanceValue;
  private String lengthUnit;
  private String googleMapApiKey;
  
  public MapBrowser()
  {
    setLayout(new BorderLayout());
    headerPanel = new JPanel(new MigLayout());
    lblDistance = new JLabel();
    PosButton btnZoomPlus = new PosButton("+");
    btnZoomPlus.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MapBrowser.access$008();
        MapBrowser.LoadMapTask map = new MapBrowser.LoadMapTask(MapBrowser.this, source, destination);
        map.execute();
      }
    });
    PosButton btnZoomMinus = new PosButton("-");
    btnZoomMinus.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        MapBrowser.access$010();
        MapBrowser.LoadMapTask map = new MapBrowser.LoadMapTask(MapBrowser.this, source, destination);
        map.execute();
      }
      
    });
    headerPanel.add(lblDistance, "grow");
    
    mapView = new MapPanel(null);
    JScrollPane scrollPane = new JScrollPane(mapView);
    scrollPane.setBorder(null);
    
    footerPanel = new JPanel(new MigLayout("al right"));
    footerPanel.add(btnZoomPlus, "w 30!, h 20!,grow,span 2");
    footerPanel.add(btnZoomMinus, "w 30!, h 20!,grow,span");
    
    add(headerPanel, "North");
    add(scrollPane, "Center");
    add(footerPanel, "South");
    
    Store store = DataProvider.get().getStore();
    lengthUnit = store.getProperty("deliveryConfig.unitName", "KM");
    googleMapApiKey = store.getProperty("google.map.api.key", "AIzaSyDc-5LFTSC-bB9kQcZkM74LHUxwndRy_XM");
  }
  
  public void load(String source, String destination) {
    this.source = source;
    this.destination = destination;
    new LoadMapTask(source, destination).execute();
  }
  
  private class MapPanel extends JPanel { private BufferedImage mapImage;
    
    private MapPanel() {}
    
    public void setImage(BufferedImage image) { mapImage = image;
      repaint();
    }
    
    protected void paintComponent(Graphics g)
    {
      super.paintComponent(g);
      if (mapImage != null) {
        int x = (getWidth() - mapImage.getWidth()) / 2;
        int y = (getHeight() - mapImage.getHeight()) / 2;
        g.drawImage(mapImage, x, y, this);
      }
    }
  }
  
  public Double getDistance() {
    return Double.valueOf(distanceValue);
  }
  
  private class LoadMapTask extends SwingWorker<BufferedImage, Object> {
    private String source;
    private String destination;
    private String distance;
    private String duration;
    private String direction;
    
    public LoadMapTask(String source, String destination) {
      if (source != null) {
        source = source.replaceAll(" ", "+");
      }
      if (destination != null) {
        destination = destination.replaceAll(" ", "+");
      }
      
      this.source = source;
      this.destination = destination;
    }
    
    protected BufferedImage doInBackground() throws Exception
    {
      BufferedImage mapImage = null;
      direction = GMapUtil.getDirection(source, destination);
      String[] output = GMapUtil.getDistanceAndDuration(source, destination);
      double distanceInMeter = Double.valueOf(output[0]).doubleValue();
      if ("KM".equals(lengthUnit)) {
        distanceValue = NumberUtil.roundToTwoDigit(distanceInMeter / 1000.0D);
      }
      else {
        distanceValue = NumberUtil.roundToTwoDigit(distanceInMeter * 6.21371E-4D);
      }
      distance = String.valueOf(distanceValue);
      duration = output[1];
      try
      {
        URL mapUrl = new URL("https://maps.googleapis.com/maps/api/staticmap?&zoom=" + MapBrowser.zoom + "&markers=color:green%7Clabel:G%7C" + source + "&markers=color:red%7Clabel:C%7C" + destination + "&size=712x712&path=enc:" + direction + "&key=" + googleMapApiKey);
        mapImage = ImageIO.read(mapUrl);
      }
      catch (Exception localException) {}
      return mapImage;
    }
    
    protected void done()
    {
      try {
        lblDistance.setText("<html><b> Distance: " + distance + " " + lengthUnit.toLowerCase() + ", Duration: " + duration + "</b></html>");
        mapView.setImage((BufferedImage)get());
      } catch (Exception ex) {
        POSMessageDialog.showError("Error" + ex.getMessage());
      }
    }
  }
}
