package com.orocube.orocust.ui.view;

import com.floreantpos.Messages;
import com.floreantpos.POSConstants;
import com.floreantpos.PosLog;
import com.floreantpos.actions.ClockInOutAction;
import com.floreantpos.main.Application;
import com.floreantpos.main.PosWindow;
import com.floreantpos.model.PaymentType;
import com.floreantpos.model.PosTransaction;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.report.ReceiptPrintService;
import com.floreantpos.services.PosTransactionService;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.views.order.ViewPanel;
import com.floreantpos.ui.views.payment.SettleTicketDialog;
import com.floreantpos.util.POSUtil;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;


public class DriverView
  extends ViewPanel
{
  public static final String VIEW_NAME = "DRIVER_VIEW";
  private DriverTicketListView ticketListPanel;
  private static DriverView instance;
  private User user;
  private PosButton btnDriverIn;
  private PosButton btnDriverOut;
  private PosButton btnClose;
  private PosButton btnQuickCash;
  
  public DriverView(User user)
  {
    this.user = user;
    setLayout(new BorderLayout());
    initComponent();
  }
  
  private void setDriver(User user) {
    this.user = user;
    ticketListPanel.setUser(user);
  }
  
  private void initComponent() {
    ticketListPanel = new DriverTicketListView(user);
    ticketListPanel.setBorder(new TitledBorder(null, "DRIVER VIEW", 2, 2));
    add(ticketListPanel, "Center");
    
    JPanel actionButtonPanel = new JPanel(new MigLayout("hidemode 3,wrap 1", "sg,fill", ""));
    actionButtonPanel.setBorder(new TitledBorder(null, "-", 2, 2));
    
    btnDriverOut = new PosButton("DRIVER OUT");
    btnDriverOut.setMinimumSize(PosUIManager.getSize(100, 0));
    btnDriverOut.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (ticketListPanel.getSelectedTickets().isEmpty()) {
          POSMessageDialog.showMessage("Please select ticket.");
          return;
        }
        DriverView.this.doAssignTicketsForDriverOut();
      }
    });
    actionButtonPanel.add(btnDriverOut);
    
    btnDriverIn = new PosButton("DRIVER IN");
    btnDriverIn.setMinimumSize(PosUIManager.getSize(100, 0));
    btnDriverIn.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (user.isDriver().booleanValue()) {
          new ClockInOutAction().performDriverIn(user);
          DriverView.this.updateButton();
        }
      }
    });
    actionButtonPanel.add(btnDriverIn);
    
    btnQuickCash = new PosButton("QUICK CASH");
    btnQuickCash.setMinimumSize(PosUIManager.getSize(100, 0));
    btnQuickCash.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        settleTickets();
      }
    });
    actionButtonPanel.add(btnQuickCash);
    
    btnClose = new PosButton(POSConstants.CLOSE.toUpperCase() + " ORDER");
    btnClose.setMinimumSize(PosUIManager.getSize(100, 0));
    btnClose.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        DriverView.this.closeTickets();
      }
    });
    actionButtonPanel.add(btnClose);
    
    add(actionButtonPanel, "East");
  }
  
  protected void settleTickets() {
    try {
      List<Ticket> tickets = ticketListPanel.getSelectedTickets();
      if (tickets.isEmpty()) {
        POSMessageDialog.showError(POSConstants.SELECT_A_TICKET_FROM_THE_OPEN_TICKET_LIST);
        return;
      }
      if (POSMessageDialog.showYesNoQuestionDialog(POSUtil.getFocusedWindow(), "Selected tickets will be settled. Do you want to continue?", "Confirm") == 1)
      {
        return;
      }
      for (Ticket ticket : tickets) {
        ticket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
        PosTransaction transaction = PaymentType.CASH.createTransaction();
        transaction.setTicket(ticket);
        transaction.setCaptured(Boolean.valueOf(true));
        transaction.setTenderAmount(ticket.getDueAmount());
        transaction.setAmount(ticket.getDueAmount());
        PosTransactionService transactionService = PosTransactionService.getInstance();
        transactionService.settleTicket(ticket, transaction, Application.getCurrentUser());
      }
      POSMessageDialog.showMessage("Done");
    } catch (Exception ex) {
      PosLog.error(getClass(), ex);
    }
    updateTicketList();
  }
  
  private void doAssignTicketsForDriverOut() {
    List<Ticket> tickets = ticketListPanel.getSelectedTickets();
    List<User> drivers = new ArrayList();
    TicketDAO.getInstance().doDriverOut(tickets, drivers);
    for (Iterator iter = tickets.iterator(); iter.hasNext();) {
      Ticket ticket = (Ticket)iter.next();
      ReceiptPrintService.printTicket(ticket, "Customer Copy");
      ReceiptPrintService.printTicket(ticket, "Driver Copy");
    }
    if (!user.isDriver().booleanValue()) {
      for (User driver : drivers) {
        new ClockInOutAction().performDriverOut(driver);
      }
      
    } else {
      new ClockInOutAction().performDriverOut(user);
    }
    tickets.clear();
    updateTicketList();
  }
  
  private void closeTickets() {
    try {
      List<Ticket> tickets = ticketListPanel.getSelectedTickets();
      if (tickets.isEmpty()) {
        POSMessageDialog.showError(POSConstants.SELECT_A_TICKET_FROM_THE_OPEN_TICKET_LIST);
        return;
      }
      List<Ticket> selectedTickets = new ArrayList();
      for (Ticket ticket : tickets) {
        if (ticket.getDueAmount().doubleValue() > 0.0D) {
          int option = JOptionPane.showOptionDialog(Application.getPosWindow(), 
            Messages.getString("DefaultOrderServiceExtension.3") + ticket.getId() + " is not paid. Do you want to settle ticket?", 
            Messages.getString("DefaultOrderServiceExtension.5"), 2, 1, null, null, null);
          

          if (option == 0) {
            if (!POSUtil.checkDrawerAssignment()) {
              return;
            }
            ticket = TicketDAO.getInstance().loadFullTicket(ticket.getId());
            SettleTicketDialog posDialog = new SettleTicketDialog(ticket, Application.getCurrentUser());
            
            posDialog.setSize(Application.getPosWindow().getSize());
            posDialog.setDefaultCloseOperation(2);
            posDialog.openUndecoratedFullScreen();
            
            if (posDialog.isCanceled()) {
              continue;
            }
            if (ticket.isPaid().booleanValue()) {
              selectedTickets.add(ticket);
            }
          }
        }
        else {
          selectedTickets.add(ticket);
        }
      }
      TicketDAO.closeOrders((Ticket[])selectedTickets.toArray(new Ticket[0]));
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    updateTicketList();
  }
  
  private void updateTicketList() {
    List<Ticket> tickets = TicketDAO.getInstance().findOpenTickets();
    ticketListPanel.setTickets(tickets);
    updateButton();
  }
  
  private void updateButton() {
    btnDriverOut.setEnabled(user.isAvailableForDelivery().booleanValue());
    btnDriverIn.setEnabled((user.isDriver().booleanValue()) && (!user.isAvailableForDelivery().booleanValue()));
  }
  
  public String getViewName()
  {
    return "DRIVER_VIEW";
  }
  
  public static DriverView getInstance(User driver) {
    if (instance == null) {
      instance = new DriverView(driver);
    } else {
      instance.setDriver(driver);
    }
    instance.updateTicketList();
    return instance;
  }
}
