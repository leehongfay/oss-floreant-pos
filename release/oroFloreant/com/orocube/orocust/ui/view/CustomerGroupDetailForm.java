package com.orocube.orocust.ui.view;

import com.floreantpos.IconFactory;
import com.floreantpos.POSConstants;
import com.floreantpos.bo.ui.BOMessageDialog;
import com.floreantpos.model.Customer;
import com.floreantpos.model.CustomerGroup;
import com.floreantpos.model.dao.CustomerDAO;
import com.floreantpos.model.dao.CustomerGroupDAO;
import com.floreantpos.model.util.IllegalModelStateException;
import com.floreantpos.swing.ListTableModel;
import com.floreantpos.swing.PaginatedListModel;
import com.floreantpos.swing.PosButton;
import com.floreantpos.swing.PosUIManager;
import com.floreantpos.ui.BeanEditor;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;



















public class CustomerGroupDetailForm
  extends BeanEditor<CustomerGroup>
  implements ActionListener
{
  private JTable customerListItemTable;
  private CustomerListExplorerTableModel customerListTableModel;
  private boolean viewMode;
  private JLabel lblNumberOfItem = new JLabel();
  private PosButton btnNext;
  private PosButton btnPrev;
  protected PaginatedListModel dataModel = new PaginatedListModel();
  private boolean edit = false;
  
  public CustomerGroupDetailForm(CustomerGroup customerGroup) {
    initComponents();
    
    customerListTableModel = new CustomerListExplorerTableModel();
    customerListItemTable.setModel(customerListTableModel);
    setBean(customerGroup);
  }
  



  private void initComponents()
  {
    setLayout(new MigLayout("fill", "[][grow]", ""));
    setBorder(new EmptyBorder(10, 10, 10, 10));
    
    customerListItemTable = new JTable();
    customerListItemTable.setRowHeight(PosUIManager.getSize(30));
    add(new JScrollPane(customerListItemTable), "newline,split 2,span,grow");
    addButtonPanel();
  }
  
  private void scrollDown() {
    CustomerGroup customerGroup = (CustomerGroup)getBean();
    if (edit) {
      CustomerGroupDAO.getInstance().saveOrUpdate(customerGroup);
      edit = false;
    }
    dataModel.setCurrentRowIndex(dataModel.getNextRowIndex());
    updateData();
  }
  
  private void updateButton() {
    int startNumber = dataModel.getCurrentRowIndex() + 1;
    int endNumber = dataModel.getNextRowIndex();
    int totalNumber = dataModel.getNumRows();
    if (endNumber > totalNumber) {
      endNumber = totalNumber;
    }
    lblNumberOfItem.setText(String.format("Showing %s to %s of %s", new Object[] { Integer.valueOf(startNumber), Integer.valueOf(endNumber), Integer.valueOf(totalNumber) }));
    btnPrev.setEnabled(dataModel.hasPrevious());
    btnNext.setEnabled(dataModel.hasNext());
  }
  
  private void updateData() {
    CustomerGroup customerGroup = (CustomerGroup)getBean();
    dataModel.setNumRows(CustomerDAO.getInstance().getNumberOfCustomers(customerGroup));
    CustomerDAO.getInstance().loadCustomers(customerGroup, dataModel);
    customerListTableModel.setItems(dataModel.getDataList());
    updateButton();
    customerListItemTable.revalidate();
    customerListItemTable.repaint();
  }
  
  private void scrollUp() {
    CustomerGroup customerGroup = (CustomerGroup)getBean();
    if (edit) {
      CustomerGroupDAO.getInstance().saveOrUpdate(customerGroup);
      edit = false;
    }
    dataModel.setCurrentRowIndex(dataModel.getPreviousRowIndex());
    updateData();
  }
  
  private void addButtonPanel()
  {
    JPanel paginationButtonPanel = new JPanel(new MigLayout("ins 5 0 0 0,fillx", "[right,grow][]", ""));
    
    paginationButtonPanel.add(lblNumberOfItem, "split 3,span");
    
    btnPrev = new PosButton();
    btnPrev.setIcon(IconFactory.getIcon("/ui_icons/", "previous.png"));
    paginationButtonPanel.add(btnPrev, "center");
    
    btnNext = new PosButton();
    btnNext.setIcon(IconFactory.getIcon("/ui_icons/", "next.png"));
    paginationButtonPanel.add(btnNext);
    
    add(paginationButtonPanel, "South");
    
    ActionListener action = new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        try {
          Object source = e.getSource();
          if (source == btnPrev) {
            CustomerGroupDetailForm.this.scrollUp();
          }
          else if (source == btnNext) {
            CustomerGroupDetailForm.this.scrollDown();
          }
        } catch (Exception e2) {
          POSMessageDialog.showError(POSUtil.getBackOfficeWindow(), e2.getMessage(), e2);
        }
        
      }
    };
    updateButton();
    btnPrev.addActionListener(action);
    btnNext.addActionListener(action);
    btnNext.setEnabled(false);
    btnPrev.setEnabled(false);
  }
  

  public void actionPerformed(ActionEvent e)
  {
    String actionCommand = e.getActionCommand();
    if (POSConstants.DELETE.equals(actionCommand)) {
      int index = customerListItemTable.getSelectedRow();
      if (index < 0) {
        BOMessageDialog.showError(POSConstants.SELECT_ITEM_TO_DELETE);
        return;
      }
    }
  }
  
  private class CustomerListExplorerTableModel extends ListTableModel<Customer>
  {
    String[] columnNames = { "ID", "NAME", "MOBILE_NO", "EMAIL" };
    List<Customer> customerList;
    
    public CustomerListExplorerTableModel() {
      customerList = new ArrayList();
    }
    
    public void setItems(List<Customer> customers) {
      if (customers == null)
        return;
      customerList.clear();
      customerList.addAll(customers);
      fireTableDataChanged();
    }
    
    public List<Customer> getItems() {
      return customerList;
    }
    
    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
      return false;
    }
    
    public int getRowCount()
    {
      if (customerList == null) {
        return 0;
      }
      return customerList.size();
    }
    
    public int getColumnCount()
    {
      return columnNames.length;
    }
    
    public String getColumnName(int index)
    {
      return columnNames[index];
    }
    
    public Object getValueAt(int row, int column)
    {
      if (customerList == null) {
        return "";
      }
      Customer customer = (Customer)customerList.get(row);
      if (customer == null) {
        return "";
      }
      
      switch (column) {
      case 0: 
        return customer.getId();
      case 1: 
        return customer.getName();
      case 2: 
        return customer.getMobileNo();
      case 3: 
        return customer.getEmail();
      }
      
      return null;
    }
  }
  
  public boolean save()
  {
    try
    {
      if (!updateModel())
        return false;
      CustomerGroup customerGroup = (CustomerGroup)getBean();
      CustomerGroupDAO.getInstance().saveOrUpdate(customerGroup);
      return true;
    } catch (IllegalModelStateException e) {
      e.printStackTrace();
    }
    return false;
  }
  
  protected void updateView()
  {
    CustomerGroup customerGroup = (CustomerGroup)getBean();
    
    if (customerGroup.getId() == null) {
      return;
    }
    dataModel.setCurrentRowIndex(0);
    updateData();
  }
  
  protected boolean updateModel()
    throws IllegalModelStateException
  {
    return true;
  }
  

  public String getDisplayText()
  {
    return "Details of " + ((CustomerGroup)getBean()).getName();
  }
  
  public void setEditable(boolean b)
  {
    viewMode = (!b);
    if (viewMode)
    {



      customerListItemTable.setEnabled(false);
    }
  }
}
