package com.orocube.orocust.ui.view;

import com.floreantpos.Messages;
import com.floreantpos.PosException;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.swing.POSToggleButton;
import com.floreantpos.swing.PosScrollPane;
import com.floreantpos.swing.ScrollableFlowPanel;
import com.floreantpos.ui.dialog.POSMessageDialog;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

public class DriverTicketListView extends JPanel
{
  private ScrollableFlowPanel buttonsPanel;
  private List<Ticket> addedTicketListModel = new ArrayList();
  private User user;
  private JScrollPane scrollPane;
  
  public DriverTicketListView(User driver)
  {
    user = driver;
    initComponent();
  }
  
  protected void setUser(User user) {
    this.user = user;
  }
  
  protected void setTickets(List<Ticket> tickets) {
    reset();
    List<Ticket> deliveryTicketList = new ArrayList();
    for (Iterator localIterator = tickets.iterator(); localIterator.hasNext();) { ticket = (Ticket)localIterator.next();
      if ((ticket.getOrderType().isDelivery().booleanValue()) && (!ticket.isCustomerWillPickup().booleanValue()) && (!ticket.isClosed().booleanValue()))
        deliveryTicketList.add(ticket);
    }
    Ticket ticket;
    Object driverTickets = new ArrayList();
    if (user.isDriver().booleanValue()) {
      for (Ticket ticket : deliveryTicketList) {
        User ticketUser = ticket.getAssignedDriver();
        if ((ticketUser != null) && (ticketUser.getId().equals(user.getId())))
        {

          ((List)driverTickets).add(ticket);
        }
      }
    } else {
      ((List)driverTickets).addAll(deliveryTicketList);
    }
    
    if (!((List)driverTickets).isEmpty()) {
      rendererTickets((List)driverTickets);
    }
    buttonsPanel.revalidate();
    buttonsPanel.repaint();
  }
  
  private void reset() {
    addedTicketListModel.clear();
    buttonsPanel.getContentPane().removeAll();
  }
  
  private void initComponent() {
    setLayout(new BorderLayout(5, 5));
    buttonsPanel = new ScrollableFlowPanel(3);
    
    scrollPane = new PosScrollPane(buttonsPanel, 20, 31);
    scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(80, 0));
    scrollPane.setBorder(null);
    
    add(scrollPane, "Center");
  }
  
  private void rendererTickets(List<Ticket> tickets) {
    Collections.sort(tickets, new Comparator()
    {
      public int compare(Ticket o1, Ticket o2)
      {
        String zipCode1 = o1.getProperty("CUSTOMER_ZIP_CODE");
        String zipCode2 = o2.getProperty("CUSTOMER_ZIP_CODE");
        if (zipCode1 == null) {
          zipCode1 = "";
        }
        if (zipCode2 == null) {
          zipCode2 = "";
        }
        return zipCode1.compareTo(zipCode2);
      }
    });
    Collections.sort(tickets, new Comparator()
    {
      public int compare(Ticket o1, Ticket o2)
      {
        Date deliveryDate1 = o1.getDeliveryDate();
        Date deliveryDate2 = o2.getDeliveryDate();
        return deliveryDate2.compareTo(deliveryDate1);
      }
    });
    try {
      for (Ticket ticket : tickets) {
        if (ticket.getDueAmount().doubleValue() > 0.0D)
        {

          TicketButton btnTicket = new TicketButton(ticket);
          btnTicket.setBackground(Color.white);
          btnTicket.setPreferredSize(com.floreantpos.swing.PosUIManager.getSize(198, 162));
          buttonsPanel.add(btnTicket);
        }
      }
    } catch (PosException e) {
      POSMessageDialog.showError(this, e.getLocalizedMessage(), e);
    }
  }
  
  public void doOk() {
    if (addedTicketListModel.isEmpty()) {
      POSMessageDialog.showMessage(Messages.getString("TicketSelectionDialog.5"));
      return;
    }
  }
  
  public void doCancel() {
    addedTicketListModel.clear();
  }
  
  public List<Ticket> getSelectedTickets() {
    return addedTicketListModel;
  }
  
  private class TicketButton extends POSToggleButton implements ActionListener {
    private Ticket ticket;
    
    TicketButton(Ticket ticket) {
      this.ticket = ticket;
      String customerName = ticket.getProperty("CUSTOMER_NAME");
      if (customerName == null) {
        customerName = "";
      }
      
      String customerMobile = ticket.getProperty("CUSTOMER_MOBILE");
      if (customerMobile == null) {
        customerMobile = "";
      }
      
      String customerZipCode = ticket.getProperty("CUSTOMER_ZIP_CODE");
      if (customerZipCode == null) {
        customerZipCode = "";
      }
      
      String customerAddress = ticket.getDeliveryAddress();
      if (customerAddress == null) {
        customerAddress = "";
      }
      
      Date deliveryDate = ticket.getDeliveryDate();
      SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
      String date = dateFormat.format(deliveryDate);
      
      if (DateUtil.isToday(deliveryDate)) {
        date = DateUtil.formatAsTodayDate(deliveryDate);
      }
      
      setText("<html><body><center><h3>" + ticket.getNumberToDisplay() + "<br>" + customerZipCode + "</h3>" + customerName + "<br>" + customerMobile + "</br><br>" + customerAddress + "<br><h4>Needed On " + date.replaceAll("TODAY", "") + "</h4></center></body></html>");
      addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e) {
      if (isSelected()) {
        addedTicketListModel.add(ticket);
      }
      else {
        addedTicketListModel.remove(ticket);
      }
    }
  }
}
