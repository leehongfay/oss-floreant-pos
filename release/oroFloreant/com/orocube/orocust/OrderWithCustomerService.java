package com.orocube.orocust;

import com.floreantpos.Messages;
import com.floreantpos.PosLog;
import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.config.TerminalConfig;
import com.floreantpos.customer.CustomerSelector;
import com.floreantpos.customer.CustomerSelectorDialog;
import com.floreantpos.customer.CustomerSelectorFactory;
import com.floreantpos.main.Application;
import com.floreantpos.main.Main;
import com.floreantpos.model.Customer;
import com.floreantpos.model.OrderType;
import com.floreantpos.model.ShopTable;
import com.floreantpos.model.TableStatus;
import com.floreantpos.model.Terminal;
import com.floreantpos.model.Ticket;
import com.floreantpos.model.User;
import com.floreantpos.model.dao.TicketDAO;
import com.floreantpos.model.dao.UserDAO;
import com.floreantpos.model.util.DateUtil;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.ui.dialog.PasswordEntryDialog;
import com.floreantpos.ui.views.IView;
import com.floreantpos.ui.views.order.DefaultOrderServiceExtension;
import com.floreantpos.ui.views.order.OrderController;
import com.floreantpos.ui.views.order.OrderView;
import com.floreantpos.ui.views.order.RootView;
import com.floreantpos.util.DrawerUtil;
import com.floreantpos.util.POSUtil;
import com.floreantpos.util.PosGuiUtil;
import com.floreantpos.util.ShiftUtil;
import com.floreantpos.util.TicketAlreadyExistsException;
import com.orocube.common.about.AboutPluginAction;
import com.orocube.common.util.ProductInfo;
import com.orocube.orocust.actions.CustomerExplorerAction;
import com.orocube.orocust.actions.DeliveryDispatchViewAction;
import com.orocube.orocust.actions.OrocustConfigurationViewAction;
import com.orocube.orocust.actions.ShowCustomerGroupBrowseAction;
import com.orocube.orocust.ui.dialog.AssignDriverDialog;
import com.orocube.orocust.ui.dialog.DeliveryDispatchDialog;
import com.orocube.orocust.ui.dialog.DeliverySelectionDialog;
import com.orocube.orocust.ui.view.CustomerListView;
import com.orocube.orocust.ui.view.DeliveryDispatchView;
import com.orocube.orocust.ui.view.DriverView;
import java.awt.Component;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import net.xeoh.plugins.base.annotations.PluginImplementation;

@PluginImplementation
public class OrderWithCustomerService
  extends DefaultOrderServiceExtension
  implements ProductInfo
{
  public static final String PRODUCT_NAME = "OroCust";
  public static final String PRODUCT_VERSION = "1.0.25";
  
  public OrderWithCustomerService() {}
  
  public String getDescription()
  {
    return "This extension enables storing customer information with ticekt";
  }
  


  public void setCustomerToTicket(String ticketId) {}
  

  public void setDeliveryDate(String ticketId) {}
  

  public void createNewTicket(OrderType ticketType, List<ShopTable> selectedTables, Customer customer)
    throws TicketAlreadyExistsException
  {
    int numberOfGuests = 0;
    
    if (ticketType.isShowGuestSelection().booleanValue()) {
      numberOfGuests = PosGuiUtil.captureGuestNumber();
    }
    if (TerminalConfig.isActiveCustomerDisplay()) {
      DrawerUtil.setCustomerDisplayMessage(TerminalConfig.getCustomerDisplayPort(), "Welcome");
    }
    Ticket ticket = new Ticket();
    Date deliveryDate = null;
    boolean customerWillPickUp = false;
    String shippingAddress = null;
    String extraDeliveryInfo = null;
    String phoneExtension = null;
    String extraManagerInstruction = null;
    Double deliveryChargeAmount = Double.valueOf(0.0D);
    
    if ((ticketType.isRequiredCustomerData().booleanValue()) && (customer == null)) {
      CustomerSelectorDialog dialog = CustomerSelectorFactory.createCustomerSelectorDialog(ticketType);
      dialog.setCreateNewTicket(false);
      dialog.updateView(true);
      dialog.openUndecoratedFullScreen();
      
      if (dialog.isCanceled()) {
        customer = null;
      }
      else {
        customer = dialog.getSelectedCustomer();
      }
    }
    if (ticketType.isDelivery().booleanValue()) {
      DeliverySelectionDialog deliveryDialog = new DeliverySelectionDialog(Application.getPosWindow(), ticket, ticketType, customer);
      deliveryDialog.setLocationRelativeTo(Application.getPosWindow());
      if (customer != null) {
        deliveryDialog.setRecipientName(customer.getName());
        deliveryDialog.setDeliveryAddress(customer.getAddress());
      }
      deliveryDialog.openUndecoratedFullScreen();
      
      if (deliveryDialog.isCanceled()) {
        return;
      }
      deliveryDate = deliveryDialog.getDeliveryDate();
      shippingAddress = deliveryDialog.getDeliveryAddress();
      extraDeliveryInfo = deliveryDialog.getExtraDeliveryInfo();
      phoneExtension = deliveryDialog.getPhoneExtension();
      extraManagerInstruction = deliveryDialog.getManagerInstruction();
      customerWillPickUp = deliveryDialog.willCustomerPickup();
      deliveryChargeAmount = Double.valueOf(deliveryDialog.getDeliveryCharge());
    }
    Application application = Application.getInstance();
    
    ticket.setTaxIncluded(Boolean.valueOf(application.isPriceIncludesTax()));
    ticket.setOrderType(ticketType);
    ticket.setNumberOfGuests(Integer.valueOf(numberOfGuests));
    
    if (customerWillPickUp) {
      ticket.setCustomerWillPickup(Boolean.valueOf(true));
    }
    else {
      ticket.setDeliveryDate(deliveryDate);
      ticket.setDeliveryAddress(shippingAddress);
      ticket.setExtraDeliveryInfo(extraDeliveryInfo);
      ticket.addProperty("PHONE_EXTENSION", phoneExtension);
      ticket.addProperty("MANAGER_INSTRUCTION", extraManagerInstruction);
      ticket.setDeliveryCharge(deliveryChargeAmount);
      ticket.setCustomerWillPickup(Boolean.valueOf(false));
    }
    
    if (customer != null) {
      ticket.setCustomer(customer);
    }
    Terminal terminal = application.getTerminal();
    ticket.setTerminal(terminal);
    
    ticket.setOwner(Application.getCurrentUser());
    ticket.setShift(ShiftUtil.getCurrentShift());
    
    if (selectedTables != null) {
      for (ShopTable shopTable : selectedTables) {
        shopTable.setTableStatus(TableStatus.Seat);
        ticket.addTable(shopTable.getTableNumber().intValue());
      }
    }
    
    Calendar currentTime = DateUtil.getServerTimeCalendar();
    ticket.setCreateDate(currentTime.getTime());
    ticket.setCreationHour(Integer.valueOf(currentTime.get(11)));
    
    OrderView.getInstance().setCurrentTicket(ticket);
    RootView.getInstance().showView("ORDER_VIEW");
  }
  
  public CustomerSelector createNewCustomerSelector()
  {
    CustomerListView customerListView = new CustomerListView();
    return customerListView;
  }
  
  public IView getDeliveryDispatchView(OrderType orderType)
  {
    return DeliveryDispatchView.getInstance(orderType);
  }
  
  public IView getDriverView()
  {
    User user = PasswordEntryDialog.getUser(Application.getPosWindow(), Messages.getString("LoginView.1"), Messages.getString("LoginView.2"));
    if (user == null) {
      return null;
    }
    if ((!user.isDriver().booleanValue()) && ((!user.isAdministrator()) || (!user.isManager()))) {
      POSMessageDialog.showMessage("Not a driver.");
      return null;
    }
    Application.getInstance().initCurrentUser(user);
    
    return DriverView.getInstance(user);
  }
  
  public CustomerSelector createCustomerSelectorView()
  {
    CustomerListView customerListView = new CustomerListView();
    return customerListView;
  }
  
  public void assignDriver(String ticketId)
  {
    List<User> drivers = UserDAO.getInstance().findDrivers();
    
    if ((drivers == null) || (drivers.size() == 0)) {
      POSMessageDialog.showError(Application.getPosWindow(), "No driver found to assign");
      return;
    }
    
    Ticket ticket = TicketDAO.getInstance().get(ticketId);
    
    AssignDriverDialog dialog = new AssignDriverDialog(Application.getPosWindow());
    dialog.setData(ticket, drivers);
    dialog.setSize(550, 450);
    dialog.setLocationRelativeTo(Application.getPosWindow());
    dialog.setVisible(true);
  }
  
  public boolean finishOrder(String ticketId)
  {
    Ticket ticket = TicketDAO.getInstance().get(ticketId);
    





    int due = (int)POSUtil.getDouble(ticket.getDueAmount());
    if (due != 0) {
      POSMessageDialog.showError(Application.getPosWindow(), "Ticket is not fully paid");
      return false;
    }
    
    int option = JOptionPane.showOptionDialog(Application.getPosWindow(), "Ticket# " + ticket.getId() + " will be closed.", "Confirm", 2, 1, null, null, null);
    
    if (option != 0) {
      return false;
    }
    
    OrderController.closeOrder(ticket);
    
    return true;
  }
  
  public void createCustomerMenu(JMenu menu)
  {
    menu.add(new CustomerExplorerAction());
  }
  
  public void initBackoffice(BackOfficeWindow backOfficeWindow)
  {
    JMenuBar menuBar = backOfficeWindow.getBackOfficeMenuBar();
    JMenu customerExplorerMenu = new JMenu("OroCust");
    customerExplorerMenu.add(new JMenuItem(new CustomerExplorerAction()));
    customerExplorerMenu.add(new JMenuItem(new ShowCustomerGroupBrowseAction()));
    customerExplorerMenu.add(new JMenuItem(new OrocustConfigurationViewAction()));
    customerExplorerMenu.add(new JMenuItem(new AboutPluginAction(this, getLicense(), POSUtil.getBackOfficeWindow(), this)));
    
    menuBar.add(customerExplorerMenu);
  }
  


  public void initConfigurationView(JDialog dialog) {}
  

  public void openDeliveryDispatchDialog(OrderType orderType)
  {
    DeliveryDispatchDialog dialog = new DeliveryDispatchDialog(orderType);
    dialog.openUndecoratedFullScreen();
  }
  
  public List<AbstractAction> getSpecialFunctionActions()
  {
    List<AbstractAction> posActions = new ArrayList();
    posActions.add(new DeliveryDispatchViewAction());
    return posActions;
  }
  
  public void restartPOS(boolean restart)
  {
    if (restart) {
      try {
        Main.restart();
      } catch (Exception e) {
        PosLog.error(getClass(), e);
      }
    }
  }
  
  public boolean hasValidLicense()
  {
    return getLicense() != null;
  }
  
  public boolean requireLicense()
  {
    return true;
  }
  
  public void showDeliveryInfo(Ticket ticket, OrderType ticketType, Customer customer)
  {
    DeliverySelectionDialog deliveryDialog = new DeliverySelectionDialog(Application.getPosWindow(), ticket, ticketType, customer);
    deliveryDialog.setLocationRelativeTo(Application.getPosWindow());
    deliveryDialog.openUndecoratedFullScreen();
    
    if (deliveryDialog.isCanceled()) {
      return;
    }
    
    if (ticket.getId() != null) {
      OrderController.saveOrder(ticket);
    }
  }
  
  public String getProductName()
  {
    return "OroCust";
  }
  
  public String getProductVersion()
  {
    return "1.0.25";
  }
  
  public Component getParent()
  {
    return POSUtil.getFocusedWindow();
  }
  
  public URL getChangeLogURL()
  {
    return getClass().getResource("/change.log.xml");
  }
}
