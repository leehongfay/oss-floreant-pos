package com.orocube.orocust.actions;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orocube.orocust.Messages;
import com.orocube.orocust.ui.view.CustomerGroupExplorer;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;

public class ShowCustomerGroupBrowseAction
  extends AbstractAction
{
  private static final String CUSTOMERGROUP = Messages.getString("ShowCustomerGroupBrowseAction.0");
  
  public ShowCustomerGroupBrowseAction() {
    super(CUSTOMERGROUP);
  }
  
  public void actionPerformed(ActionEvent ev)
  {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      CustomerGroupExplorer customerGroupExplorer = null;
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab(CUSTOMERGROUP);
      if (index == -1) {
        customerGroupExplorer = new CustomerGroupExplorer();
        tabbedPane.addTab(CUSTOMERGROUP, customerGroupExplorer);
      }
      else {
        customerGroupExplorer = (CustomerGroupExplorer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(customerGroupExplorer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
