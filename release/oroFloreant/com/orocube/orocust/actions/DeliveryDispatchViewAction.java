package com.orocube.orocust.actions;

import com.floreantpos.actions.PosAction;
import com.floreantpos.ui.views.order.RootView;
import com.orocube.orocust.ui.view.DeliveryDispatchView;
















public class DeliveryDispatchViewAction
  extends PosAction
{
  public DeliveryDispatchViewAction()
  {
    super("DELIVERY DISPATCH");
  }
  
  public void execute()
  {
    RootView.getInstance().showView(DeliveryDispatchView.getInstance(null));
  }
}
