package com.orocube.orocust.actions;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orocube.orocust.ui.view.OrocustConfigurationView;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

public class OrocustConfigurationViewAction
  extends AbstractAction
{
  public OrocustConfigurationViewAction()
  {
    super("Configuration");
  }
  
  public OrocustConfigurationViewAction(String name) {
    super(name);
  }
  
  public OrocustConfigurationViewAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      OrocustConfigurationView explorer = null;
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab("OroCust Configuration");
      if (index == -1) {
        explorer = new OrocustConfigurationView();
        tabbedPane.addTab("OroCust Configuration", explorer);
      }
      else {
        explorer = (OrocustConfigurationView)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(explorer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
