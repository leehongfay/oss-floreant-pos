package com.orocube.orocust.actions;

import com.floreantpos.bo.ui.BackOfficeWindow;
import com.floreantpos.customer.CustomerExplorer;
import com.floreantpos.ui.dialog.POSMessageDialog;
import com.floreantpos.util.POSUtil;
import com.orocube.orocust.Messages;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JTabbedPane;

public class CustomerExplorerAction
  extends AbstractAction
{
  public CustomerExplorerAction()
  {
    super(Messages.getString("CustomerExplorerAction.CUSTOMER_EXPLORER"));
  }
  
  public CustomerExplorerAction(String name) {
    super(name);
  }
  
  public CustomerExplorerAction(String name, Icon icon) {
    super(name, icon);
  }
  
  public void actionPerformed(ActionEvent ev) {
    BackOfficeWindow backOfficeWindow = POSUtil.getBackOfficeWindow();
    try {
      CustomerExplorer explorer = null;
      JTabbedPane tabbedPane = backOfficeWindow.getTabbedPane();
      int index = tabbedPane.indexOfTab(Messages.getString("CustomerExplorerAction.CUSTOMER_EXPLORER"));
      if (index == -1) {
        explorer = new CustomerExplorer();
        tabbedPane.addTab(Messages.getString("CustomerExplorerAction.CUSTOMER_EXPLORER"), explorer);
      }
      else {
        explorer = (CustomerExplorer)tabbedPane.getComponentAt(index);
      }
      tabbedPane.setSelectedComponent(explorer);
    } catch (Exception e) {
      POSMessageDialog.showError(backOfficeWindow, e.getMessage(), e);
    }
  }
}
