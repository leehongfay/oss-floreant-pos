package com.jstatcom.component;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import javax.swing.JPanel;




































































public class CardPanel
  extends JPanel
{
  private static class Layout
    implements LayoutManager
  {
    private Layout() {}
    
    public void addLayoutComponent(String name, Component child)
    {
      if (name != null) {
        child.setName(name);
      }
      child.setVisible(child.getParent().getComponentCount() == 1);
    }
    



    public void removeLayoutComponent(Component child)
    {
      if (child.isVisible()) {
        Container parent = child.getParent();
        if (parent.getComponentCount() > 0) {
          parent.getComponent(0).setVisible(true);
        }
      }
    }
    


    public Dimension preferredLayoutSize(Container parent)
    {
      int nChildren = parent.getComponentCount();
      Insets insets = parent.getInsets();
      int width = left + right;
      int height = top + bottom;
      
      for (int i = 0; i < nChildren; i++) {
        Dimension d = parent.getComponent(i).getPreferredSize();
        if (width > width) {
          width = width;
        }
        if (height > height) {
          height = height;
        }
      }
      return new Dimension(width, height);
    }
    


    public Dimension minimumLayoutSize(Container parent)
    {
      int nChildren = parent.getComponentCount();
      Insets insets = parent.getInsets();
      int width = left + right;
      int height = top + bottom;
      
      for (int i = 0; i < nChildren; i++) {
        Dimension d = parent.getComponent(i).getMinimumSize();
        if (width > width) {
          width = width;
        }
        if (height > height) {
          height = height;
        }
      }
      return new Dimension(width, height);
    }
    
    public void layoutContainer(Container parent) {
      int nChildren = parent.getComponentCount();
      Insets insets = parent.getInsets();
      for (int i = 0; i < nChildren; i++) {
        Component child = parent.getComponent(i);
        if (child.isVisible()) {
          Rectangle r = parent.getBounds();
          int width = width - left + right;
          int height = height - top + bottom;
          child.setBounds(left, top, width, height);
          break;
        }
      }
    }
  }
  





  public CardPanel()
  {
    super(new Layout(null));
  }
  




  protected int getVisibleChildIndex()
  {
    int nChildren = getComponentCount();
    for (int i = 0; i < nChildren; i++) {
      Component child = getComponent(i);
      if (child.isVisible()) {
        return i;
      }
    }
    return -1;
  }
  



  public void showCard(Component card)
  {
    if (card.getParent() != this) {
      add(card);
    }
    int index = getVisibleChildIndex();
    if (index != -1) {
      getComponent(index).setVisible(false);
    }
    card.setVisible(true);
    revalidate();
    repaint();
  }
  




  public void showCard(String name)
  {
    int nChildren = getComponentCount();
    for (int i = 0; i < nChildren; i++) {
      Component child = getComponent(i);
      if (child.getName().equals(name)) {
        showCard(child);
        break;
      }
    }
  }
  


  public void showFirstCard()
  {
    if (getComponentCount() <= 0) {
      return;
    }
    showCard(getComponent(0));
  }
  


  public void showLastCard()
  {
    if (getComponentCount() <= 0) {
      return;
    }
    showCard(getComponent(getComponentCount() - 1));
  }
  




  public void showNextCard()
  {
    if (getComponentCount() <= 0) {
      return;
    }
    int index = getVisibleChildIndex();
    if (index == -1) {
      showCard(getComponent(0));
    } else if (index == getComponentCount() - 1) {
      showCard(getComponent(0));
    } else {
      showCard(getComponent(index + 1));
    }
  }
  




  public void showPreviousCard()
  {
    if (getComponentCount() <= 0) {
      return;
    }
    int index = getVisibleChildIndex();
    if (index == -1) {
      showCard(getComponent(0));
    } else if (index == 0) {
      showCard(getComponent(getComponentCount() - 1));
    } else {
      showCard(getComponent(index - 1));
    }
  }
}
