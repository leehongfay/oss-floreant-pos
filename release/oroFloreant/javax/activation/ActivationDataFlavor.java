package javax.activation;

import java.awt.datatransfer.DataFlavor;


















































public class ActivationDataFlavor
  extends DataFlavor
{
  private String mimeType = null;
  private MimeType mimeObject = null;
  private String humanPresentableName = null;
  private Class representationClass = null;
  

















  public ActivationDataFlavor(Class representationClass, String mimeType, String humanPresentableName)
  {
    super(mimeType, humanPresentableName);
    

    this.mimeType = mimeType;
    this.humanPresentableName = humanPresentableName;
    this.representationClass = representationClass;
  }
  

















  public ActivationDataFlavor(Class representationClass, String humanPresentableName)
  {
    super(representationClass, humanPresentableName);
    mimeType = super.getMimeType();
    this.representationClass = representationClass;
    this.humanPresentableName = humanPresentableName;
  }
  














  public ActivationDataFlavor(String mimeType, String humanPresentableName)
  {
    super(mimeType, humanPresentableName);
    this.mimeType = mimeType;
    try {
      representationClass = Class.forName("java.io.InputStream");
    }
    catch (ClassNotFoundException ex) {}
    
    this.humanPresentableName = humanPresentableName;
  }
  




  public String getMimeType()
  {
    return mimeType;
  }
  




  public Class getRepresentationClass()
  {
    return representationClass;
  }
  




  public String getHumanPresentableName()
  {
    return humanPresentableName;
  }
  




  public void setHumanPresentableName(String humanPresentableName)
  {
    this.humanPresentableName = humanPresentableName;
  }
  







  public boolean equals(DataFlavor dataFlavor)
  {
    return (isMimeTypeEqual(dataFlavor)) && (dataFlavor.getRepresentationClass() == representationClass);
  }
  












  public boolean isMimeTypeEqual(String mimeType)
  {
    MimeType mt = null;
    try {
      if (mimeObject == null)
        mimeObject = new MimeType(this.mimeType);
      mt = new MimeType(mimeType);
    }
    catch (MimeTypeParseException e) {}
    return mimeObject.match(mt);
  }
  












  /**
   * @deprecated
   */
  protected String normalizeMimeTypeParameter(String parameterName, String parameterValue)
  {
    return parameterValue;
  }
  








  /**
   * @deprecated
   */
  protected String normalizeMimeType(String mimeType)
  {
    return mimeType;
  }
}
